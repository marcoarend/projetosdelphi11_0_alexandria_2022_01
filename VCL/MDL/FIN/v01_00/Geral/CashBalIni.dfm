object FmCashBalIni: TFmCashBalIni
  Left = 339
  Top = 185
  Align = alClient
  Caption = 'FIN-BALAN-002 :: Defini'#231#227'o de Saldos Iniciais'
  ClientHeight = 574
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  ExplicitWidth = 320
  ExplicitHeight = 240
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 526
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtAcao: TBitBtn
      Tag = 10010
      Left = 12
      Top = 4
      Width = 200
      Height = 40
      Caption = '&A'#231#227'o para a guia selecionada'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtAcaoClick
    end
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Defini'#231#227'o de Saldos Iniciais'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 478
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter2: TSplitter
      Left = 0
      Top = 161
      Width = 1008
      Height = 5
      Cursor = crVSplit
      Align = alTop
      Color = clActiveCaption
      ParentColor = False
      ExplicitTop = 248
      ExplicitWidth = 784
    end
    object PnDivergencias: TPanel
      Left = 0
      Top = 166
      Width = 1008
      Height = 312
      Align = alClient
      TabOrder = 0
      object LaDiverge: TLabel
        Left = 1
        Top = 1
        Width = 1006
        Height = 14
        Align = alTop
        Alignment = taCenter
        Caption = 'Existem diverg'#234'ncias entre Lan'#231'amentos / Carteiras!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 242
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
        ExplicitWidth = 292
      end
      object PageControl1: TPageControl
        Left = 1
        Top = 15
        Width = 1006
        Height = 296
        ActivePage = TabSheet3
        Align = alClient
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = ' Saldos das carteiras'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 228
          object Panel3: TPanel
            Left = 0
            Top = 0
            Width = 998
            Height = 268
            Align = alClient
            ParentBackground = False
            TabOrder = 0
            ExplicitHeight = 228
            object Splitter1: TSplitter
              Left = 497
              Top = 1
              Width = 5
              Height = 266
              Color = clActiveCaption
              ParentColor = False
              ExplicitLeft = 383
              ExplicitHeight = 204
            end
            object Panel6: TPanel
              Left = 502
              Top = 1
              Width = 495
              Height = 266
              Align = alClient
              TabOrder = 0
              ExplicitHeight = 226
              object Label2: TLabel
                Left = 1
                Top = 1
                Width = 220
                Height = 14
                Align = alTop
                Alignment = taCenter
                Caption = 'Pesquisa pelos dados dos lan'#231'amentos'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = 6626119
                Font.Height = -11
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Panel9: TPanel
                Left = 1
                Top = 239
                Width = 493
                Height = 26
                Align = alBottom
                Alignment = taLeftJustify
                Caption = ' Soma dos saldos das carteiras:'
                TabOrder = 0
                ExplicitTop = 199
                object EdDivLan: TdmkEdit
                  Left = 228
                  Top = 2
                  Width = 134
                  Height = 21
                  Alignment = taRightJustify
                  ReadOnly = True
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                end
              end
              object DBGrid1: TDBGrid
                Left = 1
                Top = 15
                Width = 493
                Height = 224
                Align = alClient
                DataSource = DsDivLan
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Carteira'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Nome'
                    Title.Caption = 'Nome da carteira'
                    Width = 232
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Inicial'
                    Title.Caption = 'Valor'
                    Width = 80
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Saldo'
                    Width = 80
                    Visible = True
                  end>
              end
            end
            object Panel7: TPanel
              Left = 1
              Top = 1
              Width = 496
              Height = 266
              Align = alLeft
              Caption = 'Panel6'
              TabOrder = 1
              ExplicitHeight = 226
              object Label4: TLabel
                Left = 1
                Top = 1
                Width = 197
                Height = 14
                Align = alTop
                Alignment = taCenter
                Caption = 'Pesquisa pelos dados das carteiras'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = 6626119
                Font.Height = -11
                Font.Name = 'Arial'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object DBGrid2: TDBGrid
                Left = 1
                Top = 15
                Width = 494
                Height = 224
                Align = alClient
                DataSource = DsDivCart
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Carteira'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Nome'
                    Title.Caption = 'Nome da carteira'
                    Width = 232
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Inicial'
                    Title.Caption = 'Valor'
                    Width = 80
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Saldo'
                    Width = 80
                    Visible = True
                  end>
              end
              object Panel8: TPanel
                Left = 1
                Top = 239
                Width = 494
                Height = 26
                Align = alBottom
                Alignment = taLeftJustify
                Caption = ' Soma dos saldos das carteiras:'
                TabOrder = 1
                ExplicitTop = 199
                object EdDivCart: TdmkEdit
                  Left = 228
                  Top = 2
                  Width = 134
                  Height = 21
                  Alignment = taRightJustify
                  ReadOnly = True
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                end
              end
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' Lan'#231'amentos divergentes'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 228
          object DBGLctDvr: TDBGrid
            Left = 0
            Top = 0
            Width = 998
            Height = 268
            Align = alClient
            DataSource = DsLctDvr
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
        object TabSheet3: TTabSheet
          Caption = ' Saldo do plano de contas '
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid5: TDBGrid
            Left = 0
            Top = 0
            Width = 593
            Height = 268
            Align = alLeft
            DataSource = DsSNG
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Nivel'
                Width = 18
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NomeNi'
                Title.Caption = 'Descri'#231#227'o'
                Width = 58
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Genero'
                Title.Caption = 'C'#243'digo'
                Width = 47
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NomeGe'
                Title.Caption = 'Descri'#231#227'o'
                Width = 222
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IniOld'
                Title.Caption = 'Sdo.Ini.(depr)'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SdoFim'
                Title.Caption = 'Saldo'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DIFERENCA'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = 275673
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Title.Caption = 'Diferen'#231'a'
                Width = 68
                Visible = True
              end>
          end
          object Panel4: TPanel
            Left = 593
            Top = 0
            Width = 405
            Height = 268
            Align = alClient
            ParentBackground = False
            TabOrder = 1
            object Panel5: TPanel
              Left = 1
              Top = 1
              Width = 403
              Height = 180
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object DBGrid6: TDBGrid
                Left = 84
                Top = 17
                Width = 319
                Height = 115
                Align = alClient
                DataSource = DsQLN
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Width = 38
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Nome'
                    Width = 182
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Lanctos'
                    Title.Caption = 'Lan'#231'tos'
                    Width = 62
                    Visible = True
                  end>
              end
              object Panel16: TPanel
                Left = 0
                Top = 0
                Width = 403
                Height = 17
                Align = alTop
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clRed
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
                object Label1: TLabel
                  Left = 1
                  Top = 1
                  Width = 353
                  Height = 13
                  Align = alTop
                  Alignment = taCenter
                  Caption = ' Itens do n'#237'vel selecionado usados pela entidade selecionada'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 242
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
              end
              object RGNiveis1: TRadioGroup
                Left = 0
                Top = 17
                Width = 84
                Height = 115
                Align = alLeft
                Caption = ' N'#237'vel: '
                ItemIndex = 4
                Items.Strings = (
                  'TODOS'
                  'Conta'
                  'Sub-grupo'
                  'Grupo'
                  'Conjunto'
                  'Plano')
                TabOrder = 2
                OnClick = RGNiveis1Click
              end
              object Panel17: TPanel
                Left = 0
                Top = 132
                Width = 403
                Height = 48
                Align = alBottom
                TabOrder = 3
                object BtInclui: TBitBtn
                  Tag = 10
                  Left = 4
                  Top = 4
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Hint = 'Inclui novo banco'
                  Caption = '&Inclui'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtIncluiClick
                end
                object BtAltera: TBitBtn
                  Tag = 11
                  Left = 96
                  Top = 4
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Hint = 'Altera banco atual'
                  Caption = '&Altera'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                  OnClick = BtAlteraClick
                end
                object BtExclui: TBitBtn
                  Tag = 12
                  Left = 188
                  Top = 4
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Hint = 'Exclui banco atual'
                  Caption = '&Exclui'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 2
                  OnClick = BtExcluiClick
                end
              end
            end
            object DBGPrevLct1: TDBGrid
              Left = 1
              Top = 181
              Width = 403
              Height = 86
              Align = alClient
              DataSource = DsPrevLct1
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Carteira'
                  Width = 48
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Genero'
                  Title.Caption = 'G'#234'nero'
                  Width = 48
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Credito'
                  Title.Caption = 'Cr'#233'dito'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Debito'
                  Title.Caption = 'D'#233'bito'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Descricao'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 300
                  Visible = True
                end>
            end
          end
        end
        object TabSheet4: TTabSheet
          Caption = ' Saldos iniciais do novo modo '
          ImageIndex = 3
          ExplicitLeft = -4
          ExplicitTop = 20
          ExplicitWidth = 0
          ExplicitHeight = 228
          object DBGrid7: TDBGrid
            Left = 0
            Top = 0
            Width = 998
            Height = 268
            Align = alClient
            DataSource = DsSdoNew
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Carteira'
                Width = 44
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECART'
                Title.Caption = 'Descri'#231#227'o'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Plano'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEPLANO'
                Title.Caption = 'Descri'#231#227'o'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Conjunto'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECONJUNTO'
                Title.Caption = 'Descri'#231#227'o'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Grupo'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEGRUPO'
                Title.Caption = 'Descri'#231#227'o'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SubGrupo'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMESUBGRUPO'
                Title.Caption = 'Descri'#231#227'o'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Genero'
                Title.Caption = 'Conta'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECONTA'
                Title.Caption = 'Descri'#231#227'o'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Data'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Credito'
                Title.Caption = 'Cr'#233'dito'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Debito'
                Title.Caption = 'D'#233'bito'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descricao'
                Title.Caption = 'Hist'#243'rico'
                Width = 180
                Visible = True
              end>
          end
        end
      end
    end
    object Panel10: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 161
      Align = alTop
      TabOrder = 1
      object Splitter3: TSplitter
        Left = 501
        Top = 49
        Width = 5
        Height = 111
        Color = clActiveCaption
        ParentColor = False
        ExplicitLeft = 509
        ExplicitTop = 21
        ExplicitHeight = 172
      end
      object Panel11: TPanel
        Left = 1
        Top = 1
        Width = 1006
        Height = 48
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label5: TLabel
          Left = 624
          Top = 4
          Width = 45
          Height = 13
          Caption = 'Entidade:'
          FocusControl = DBEdit6
        end
        object Label6: TLabel
          Left = 8
          Top = 4
          Width = 44
          Height = 13
          Caption = 'Empresa:'
          FocusControl = DBEdit8
        end
        object Label3: TLabel
          Left = 564
          Top = 4
          Width = 39
          Height = 13
          Caption = 'Periodo:'
          FocusControl = DBEdit6
        end
        object DBEdit6: TDBEdit
          Left = 624
          Top = 20
          Width = 57
          Height = 21
          DataField = 'Codigo'
          DataSource = DsEmpresa
          TabOrder = 0
        end
        object DBEdit7: TDBEdit
          Left = 68
          Top = 20
          Width = 493
          Height = 21
          DataField = 'NO_EMPRESA'
          DataSource = DsEmpresa
          TabOrder = 1
        end
        object DBEdit8: TDBEdit
          Left = 8
          Top = 20
          Width = 56
          Height = 21
          DataField = 'CO_SHOW'
          DataSource = DsEmpresa
          TabOrder = 2
        end
        object EdPeriodo_TXT: TdmkEdit
          Left = 564
          Top = 20
          Width = 57
          Height = 21
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00/0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '00/0000'
        end
      end
      object Panel12: TPanel
        Left = 506
        Top = 49
        Width = 501
        Height = 111
        Align = alClient
        Caption = 'Panel6'
        TabOrder = 1
        object Label7: TLabel
          Left = 1
          Top = 1
          Width = 264
          Height = 14
          Align = alTop
          Alignment = taCenter
          Caption = 'Saldos iniciais de contas (G'#234'neros) (Deprecado)'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clHotLight
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBGrid3: TDBGrid
          Left = 1
          Top = 15
          Width = 499
          Height = 69
          Align = alClient
          DataSource = DsSdo
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECTA'
              Title.Caption = 'Descri'#231#227'o da conta (G'#234'nero)'
              Width = 320
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SdoIni'
              Title.Caption = 'Valor'
              Width = 80
              Visible = True
            end>
        end
        object Panel13: TPanel
          Left = 1
          Top = 84
          Width = 499
          Height = 26
          Align = alBottom
          Alignment = taLeftJustify
          Caption = ' Soma dos saldos das carteiras:'
          TabOrder = 1
          object DBEdit9: TDBEdit
            Left = 228
            Top = 2
            Width = 134
            Height = 21
            DataField = 'SdoIni'
            DataSource = DsSSdo
            TabOrder = 0
          end
        end
      end
      object Panel14: TPanel
        Left = 1
        Top = 49
        Width = 500
        Height = 111
        Align = alLeft
        Caption = 'Panel6'
        TabOrder = 2
        object Label8: TLabel
          Left = 1
          Top = 1
          Width = 223
          Height = 14
          Align = alTop
          Alignment = taCenter
          Caption = 'Saldos iniciais das carteiras (Deprecado)'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clHotLight
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBGrid4: TDBGrid
          Left = 1
          Top = 15
          Width = 498
          Height = 69
          Align = alClient
          DataSource = DsCrt
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Nome da carteira'
              Width = 240
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Inicial'
              Title.Caption = 'Valor'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DIFERENCA'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 275673
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Title.Caption = 'Diferen'#231'a'
              Visible = True
            end>
        end
        object Panel15: TPanel
          Left = 1
          Top = 84
          Width = 498
          Height = 26
          Align = alBottom
          Alignment = taLeftJustify
          Caption = ' Soma dos saldos das carteiras:'
          TabOrder = 1
          object DBEdit10: TDBEdit
            Left = 228
            Top = 2
            Width = 134
            Height = 21
            DataField = 'Inicial'
            DataSource = DsSCrt
            TabOrder = 0
          end
        end
      end
    end
  end
  object QrCrt: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCrtAfterOpen
    BeforeClose = QrCrtBeforeClose
    OnCalcFields = QrCrtCalcFields
    SQL.Strings = (
      'SELECT Codigo, Nome, Inicial '
      'FROM carteiras'
      'WHERE ForneceI=:P0'
      'AND Inicial <> 0')
    Left = 40
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCrtCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCrtNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCrtInicial: TFloatField
      FieldName = 'Inicial'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrCrtDIFERENCA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DIFERENCA'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
      Calculated = True
    end
  end
  object DsEmpresa: TDataSource
    DataSet = FmCashBal.QrEmp
    Left = 12
    Top = 8
  end
  object DsCrt: TDataSource
    DataSet = QrCrt
    Left = 68
    Top = 8
  end
  object QrSdo: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSdoAfterOpen
    BeforeClose = QrSdoBeforeClose
    SQL.Strings = (
      'SELECT cta.Codigo,  cta.Nome NOMECTA, sdo.SdoIni'
      'FROM contas cta'
      'LEFT JOIN contassdo sdo ON cta.Codigo=sdo.Codigo '
      'WHERE cta.Codigo<>0'
      'AND sdo.Entidade=:P0'
      'AND sdo.SdoIni <> 0'
      'ORDER BY cta.Nome')
    Left = 96
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSdoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSdoNOMECTA: TWideStringField
      FieldName = 'NOMECTA'
      Size = 50
    end
    object QrSdoSdoIni: TFloatField
      FieldName = 'SdoIni'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsSdo: TDataSource
    DataSet = QrSdo
    Left = 124
    Top = 8
  end
  object QrSCrt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Inicial) Inicial'
      'FROM carteiras'
      'WHERE ForneceI=:P0'
      'AND Inicial <> 0'
      '')
    Left = 40
    Top = 36
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSCrtInicial: TFloatField
      FieldName = 'Inicial'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsSCrt: TDataSource
    DataSet = QrSCrt
    Left = 68
    Top = 36
  end
  object QrSSdo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(sdo.SdoIni) SdoIni'
      'FROM contas cta'
      'LEFT JOIN contassdo sdo ON cta.Codigo=sdo.Codigo '
      'WHERE cta.Codigo<>0'
      'AND sdo.Entidade=:P0'
      'AND sdo.SdoIni <> 0'
      '')
    Left = 96
    Top = 36
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSSdoSdoIni: TFloatField
      FieldName = 'SdoIni'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsSSdo: TDataSource
    DataSet = QrSSdo
    Left = 124
    Top = 36
  end
  object QrDivCart: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Inicial, car.Nome, lan.Carteira, '
      'SUM(lan.Credito-lan.Debito) SdoMov,'
      'car.Inicial + SUM(lan.Credito-lan.Debito) Saldo '
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.carteira'
      'WHERE car.ForneceI=:P0'
      'AND car.Tipo <> 2'
      'AND Data < :P1'
      'GROUP BY lan.Carteira'
      'ORDER BY car.Nome')
    Left = 684
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDivCartInicial: TFloatField
      FieldName = 'Inicial'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrDivCartNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrDivCartCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrDivCartSdoMov: TFloatField
      FieldName = 'SdoMov'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrDivCartSaldo: TFloatField
      FieldName = 'Saldo'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsDivCart: TDataSource
    DataSet = QrDivCart
    Left = 712
  end
  object QrDivLan: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Inicial, car.Nome, lan.Carteira, '
      'SUM(lan.Credito-lan.Debito) SdoMov,'
      'car.Inicial + SUM(lan.Credito-lan.Debito) Saldo '
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.carteira'
      'WHERE lan.CliInt=:P0'
      'AND lan.Tipo <> 2'
      'AND Data < :P1'
      'GROUP BY lan.Carteira'
      'ORDER BY car.Nome')
    Left = 740
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDivLanInicial: TFloatField
      FieldName = 'Inicial'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrDivLanNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrDivLanCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrDivLanSdoMov: TFloatField
      FieldName = 'SdoMov'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrDivLanSaldo: TFloatField
      FieldName = 'Saldo'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsDivLan: TDataSource
    DataSet = QrDivLan
    Left = 768
  end
  object PMAcao1: TPopupMenu
    OnPopup = PMAcao1Popup
    Left = 116
    Top = 544
    object Colocarcarteiracorretanoslanamentos1: TMenuItem
      Caption = 'Colocar carteira correta nos lan'#231'amentos das linhas selecionadas'
      OnClick = Colocarcarteiracorretanoslanamentos1Click
    end
  end
  object QrLctDvr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lan.Controle, lan.Sub, '
      'lan.Genero, lan.Credito, lan.Debito, lan.CliInt,'
      'car.Tipo CAR_TIPO, car.ForneceI CAR_FORNECEI'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.carteira'
      'WHERE car.ForneceI=:P0'
      'AND car.Tipo <> 2'
      'AND (lan.CliInt <> car.ForneceI'
      '  OR'
      '  lan.Tipo <> car.Tipo)')
    Left = 796
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLctDvrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLctDvrSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrLctDvrGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLctDvrCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrLctDvrDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrLctDvrCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLctDvrCAR_TIPO: TIntegerField
      FieldName = 'CAR_TIPO'
      Required = True
    end
    object QrLctDvrCAR_FORNECEI: TIntegerField
      FieldName = 'CAR_FORNECEI'
    end
  end
  object DsLctDvr: TDataSource
    DataSet = QrLctDvr
    Left = 824
  end
  object DsSNG: TDataSource
    DataSet = QrSNG
    Left = 712
    Top = 28
  end
  object QrSNG: TmySQLQuery
    Database = DModG.MyPID_DB
    OnCalcFields = QrSNGCalcFields
    SQL.Strings = (
      'SELECT 0 KGT, sn.*'
      'FROM sdoniveis sn'
      'ORDER BY sn.Ordena')
    Left = 684
    Top = 28
    object QrSNGNivel: TIntegerField
      FieldName = 'Nivel'
      Origin = 'sdoniveis.Nivel'
    end
    object QrSNGGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'sdoniveis.Genero'
    end
    object QrSNGNomeGe: TWideStringField
      FieldName = 'NomeGe'
      Origin = 'sdoniveis.NomeGe'
      Size = 100
    end
    object QrSNGNomeNi: TWideStringField
      FieldName = 'NomeNi'
      Origin = 'sdoniveis.NomeNi'
    end
    object QrSNGSumMov: TFloatField
      FieldName = 'SumMov'
      Origin = 'sdoniveis.SumMov'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSNGSdoAnt: TFloatField
      FieldName = 'SdoAnt'
      Origin = 'sdoniveis.SdoAnt'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSNGSumCre: TFloatField
      FieldName = 'SumCre'
      Origin = 'sdoniveis.SumCre'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSNGSumDeb: TFloatField
      FieldName = 'SumDeb'
      Origin = 'sdoniveis.SumDeb'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSNGSdoFim: TFloatField
      FieldName = 'SdoFim'
      Origin = 'sdoniveis.SdoFim'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSNGCtrla: TSmallintField
      FieldName = 'Ctrla'
      Origin = 'sdoniveis.Ctrla'
    end
    object QrSNGSeleci: TSmallintField
      FieldName = 'Seleci'
      Origin = 'sdoniveis.Seleci'
    end
    object QrSNGCodPla: TIntegerField
      FieldName = 'CodPla'
    end
    object QrSNGCodCjt: TIntegerField
      FieldName = 'CodCjt'
    end
    object QrSNGCodGru: TIntegerField
      FieldName = 'CodGru'
    end
    object QrSNGCodSgr: TIntegerField
      FieldName = 'CodSgr'
    end
    object QrSNGCODIGOS_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CODIGOS_TXT'
      Size = 50
      Calculated = True
    end
    object QrSNGOrdena: TWideStringField
      FieldName = 'Ordena'
      Size = 100
    end
    object QrSNGKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
    object QrSNGIniOld: TFloatField
      FieldName = 'IniOld'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSNGDIFERENCA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DIFERENCA'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
      Calculated = True
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(sdo.SdoIni) SdoIni'
      'FROM contas cta'
      'LEFT JOIN contassdo sdo ON cta.Codigo=sdo.Codigo '
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto'
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano'
      'WHERE cta.Codigo<>0'
      'AND sdo.Entidade=1'
      'AND sdo.SdoIni <> 0'
      'ORDER BY cta.Nome')
    Left = 68
    Top = 168
    object QrPesqSdoIni: TFloatField
      FieldName = 'SdoIni'
    end
  end
  object QrQLN: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrQLNAfterScroll
    SQL.Strings = (
      'SELECT cjt.Codigo, cjt.Nome,'
      'COUNT(lan.Genero) Lanctos '
      'FROM lanctos lan'
      'LEFT JOIN contas    cta ON cta.Codigo=lan.Genero'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto'
      'WHERE lan.Tipo <> 2'
      'AND lan.Genero > 0'
      'AND lan.CliInt=:P0'
      'GROUP BY cjt.Codigo'
      'ORDER BY cjt.Codigo')
    Left = 740
    Top = 28
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrQLNCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrQLNNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrQLNLanctos: TLargeintField
      FieldName = 'Lanctos'
      Required = True
    end
  end
  object DsQLN: TDataSource
    DataSet = QrQLN
    Left = 768
    Top = 28
  end
  object QrPrevLct1: TmySQLQuery
    Database = DModG.MyPID_DB
    Left = 796
    Top = 28
    object QrPrevLct1Controle: TAutoIncField
      FieldName = 'Controle'
    end
    object QrPrevLct1Data: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPrevLct1Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrPrevLct1Carteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrPrevLct1Genero: TIntegerField
      FieldName = 'Genero'
    end
    object QrPrevLct1Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPrevLct1Debito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPrevLct1Credito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPrevLct1Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPrevLct1Entidade: TIntegerField
      FieldName = 'Entidade'
    end
  end
  object DsPrevLct1: TDataSource
    DataSet = QrPrevLct1
    Left = 824
    Top = 28
  end
  object QrOCF_Cart: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT SUM(Credito-Debito) VALOR'
      'FROM prevlct1'
      'WHERE Carteira=:P0')
    Left = 176
    Top = 92
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOCF_CartVALOR: TFloatField
      FieldName = 'VALOR'
    end
  end
  object QrOCF_Ctas: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT SUM(Credito-Debito) VALOR'
      'FROM prevlct1'
      'WHERE NivNum=:P0'
      'AND NivGen=:P1'
      '')
    Left = 204
    Top = 92
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrOCF_CtasVALOR: TFloatField
      FieldName = 'VALOR'
    end
  end
  object PMAcao2: TPopupMenu
    OnPopup = PMAcao1Popup
    Left = 144
    Top = 544
    object Confirmaracriacaodospre_lanamentos1: TMenuItem
      Caption = '&Confirmar a inclus'#227'o dos pr'#233'-lan'#231'amentos '
      OnClick = Confirmaracriacaodospre_lanamentos1Click
    end
    object Revertersaldosiniciais1: TMenuItem
      Caption = '&Reverter saldos iniciais (do novo para o antigo)'
      OnClick = Revertersaldosiniciais1Click
    end
  end
  object PMInclui: TPopupMenu
    Left = 548
    Top = 444
    object Pelovalordacontadeprecada1: TMenuItem
      Caption = 'Pelo valor da conta deprecada'
      OnClick = Pelovalordacontadeprecada1Click
    end
    object Peladiferencadacarteiradeprecada1: TMenuItem
      Caption = 'Pelo diferenca da carteira deprecada'
      OnClick = Peladiferencadacarteiradeprecada1Click
    end
  end
  object QrSdoNew: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT la.Data, la.Carteira, la.Controle, '
      'la.Genero, la.Descricao, la.Credito, la.Debito, '
      'ca.Nome NOMECART, ct.Nome NOMECONTA, '
      'ct.SubGrupo, sg.Nome NOMESUBGRUPO,'
      'sg.Grupo, gr.Nome NOMEGRUPO, '
      'gr.Conjunto, cj.Nome NOMECONJUNTO,'
      'cj.Plano, pl.Nome NOMEPLANO'
      'FROM lanctos la'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      
        'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = ' +
        '0'
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo'
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN plano pl ON pl.Codigo=cj.Plano'
      'WHERE la.FatID = -999999999'
      'AND la.CliInt=:P0'
      'ORDER BY pl.OrdemLista, pl.Nome, cj.OrdemLista, '
      'cj.Nome, gr.OrdemLista, gr.Nome, sg.OrdemLista,'
      'sg.Nome, ct.OrdemLista, ct.Nome')
    Left = 852
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSdoNewData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSdoNewCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrSdoNewControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSdoNewGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrSdoNewDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrSdoNewCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSdoNewDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSdoNewNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Size = 100
    end
    object QrSdoNewNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrSdoNewSubGrupo: TIntegerField
      FieldName = 'SubGrupo'
    end
    object QrSdoNewNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 50
    end
    object QrSdoNewGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrSdoNewNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 50
    end
    object QrSdoNewConjunto: TIntegerField
      FieldName = 'Conjunto'
    end
    object QrSdoNewNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Size = 50
    end
    object QrSdoNewPlano: TIntegerField
      FieldName = 'Plano'
    end
    object QrSdoNewNOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Size = 50
    end
  end
  object DsSdoNew: TDataSource
    DataSet = QrSdoNew
    Left = 880
  end
end
