unit UnFinanceiroJan;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, DBGrids,
  UnInternalConsts2, ComCtrls, dmkEdit, dmkDBLookupComboBox, dmkGeral,
  dmkEditCB, mySQLDBTables, UnDmkEnums, DmkDAC_PF, AdvToolBar;

type
  TUnFinanceiroJan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraCopiaDoc(QueryLct: TMySQLQuery; GradeLct: TDBGrid; TabLct: String);
    procedure CadastroDeContas(Codigo: Integer);
    procedure CadastroDeCarteiras(Codigo: Integer);
  end;

var
  FinanceiroJan: TUnFinanceiroJan;

implementation

uses MyDBCheck, CopiaDoc, Contas, Carteiras;

procedure TUnFinanceiroJan.CadastroDeCarteiras(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCarteiras, FmCarteiras, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmCarteiras.LocCod(Codigo, Codigo);
    FmCarteiras.ShowModal;
    FmCarteiras.Destroy;
  end;
end;

procedure TUnFinanceiroJan.CadastroDeContas(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmContas.LocCod(Codigo, Codigo);
    FmContas.ShowModal;
    FmContas.Destroy;
  end;
end;

procedure TUnFinanceiroJan.MostraCopiaDoc(QueryLct: TMySQLQuery;
  GradeLct: TDBGrid; TabLct: String);
begin
  if DBCheck.CriaFm(TFmCopiaDoc, FmCopiaDoc, afmoNegarComAviso) then
  begin
    FmCopiaDoc.FDBG   := GradeLct;
    FmCopiaDoc.FQrLct := QueryLct;
    FmCopiaDoc.ShowModal;
    FmCopiaDoc.Destroy;
  end;
end;

end.
