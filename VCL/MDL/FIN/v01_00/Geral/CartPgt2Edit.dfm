object FmCartPgt2Edit: TFmCartPgt2Edit
  Left = 334
  Top = 231
  Caption = 'FIN-PGTOS-003 :: Edi'#231#227'o de pagamentos de lan'#231'amentos'
  ClientHeight = 356
  ClientWidth = 779
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 308
    Width = 779
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 36
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object BtDesiste: TBitBtn
      Tag = 15
      Left = 652
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 102
    Width = 779
    Height = 206
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 228
      Top = 64
      Width = 26
      Height = 13
      Caption = 'Data:'
    end
    object LaDeb: TLabel
      Left = 666
      Top = 64
      Width = 27
      Height = 13
      Caption = 'Valor:'
    end
    object LaVencimento: TLabel
      Left = 630
      Top = 111
      Width = 59
      Height = 13
      Caption = 'Vencimento:'
      Enabled = False
      Visible = False
    end
    object Label13: TLabel
      Left = 163
      Top = 111
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object Label14: TLabel
      Left = 36
      Top = 64
      Width = 36
      Height = 13
      Caption = 'Lan'#231'to:'
    end
    object Label15: TLabel
      Left = 232
      Top = 16
      Width = 76
      Height = 13
      Caption = 'Item da carteira:'
    end
    object LaDescoPor: TLabel
      Left = 36
      Top = 155
      Width = 110
      Height = 13
      Caption = 'Fatura descontada por:'
      Visible = False
    end
    object LaDescoVal: TLabel
      Left = 673
      Top = 155
      Width = 52
      Height = 13
      Caption = 'Valor juros:'
      Visible = False
    end
    object LaDoc: TLabel
      Left = 36
      Top = 111
      Width = 104
      Height = 13
      Caption = 'S'#233'rie  e docum. (CH) :'
    end
    object Label7: TLabel
      Left = 346
      Top = 64
      Width = 39
      Height = 13
      Caption = '% Juros:'
    end
    object Label9: TLabel
      Left = 426
      Top = 64
      Width = 40
      Height = 13
      Caption = '% Multa:'
    end
    object Label8: TLabel
      Left = 506
      Top = 64
      Width = 37
      Height = 13
      Caption = '$ Juros:'
    end
    object Label10: TLabel
      Left = 586
      Top = 64
      Width = 38
      Height = 13
      Caption = '$ Multa:'
    end
    object TPData: TdmkEditDateTimePicker
      Left = 228
      Top = 80
      Width = 112
      Height = 21
      Date = 37617.480364108800000000
      Time = 37617.480364108800000000
      Color = clWhite
      TabOrder = 4
      OnClick = TPDataClick
      OnChange = TPDataChange
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object EdValor: TdmkEdit
      Left = 666
      Top = 80
      Width = 76
      Height = 21
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdValorExit
    end
    object TPVencimento: TdmkEditDateTimePicker
      Left = 630
      Top = 127
      Width = 112
      Height = 21
      Date = 37617.480364108800000000
      Time = 37617.480364108800000000
      Color = clWhite
      Enabled = False
      TabOrder = 13
      Visible = False
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object EdDescricao: TdmkEdit
      Left = 163
      Top = 127
      Width = 461
      Height = 21
      Color = clWhite
      TabOrder = 12
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdCodigo: TdmkEdit
      Left = 36
      Top = 80
      Width = 189
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8281908
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdCarteira: TdmkEditCB
      Left = 232
      Top = 32
      Width = 77
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCarteira
      IgnoraDBLookupComboBox = False
    end
    object CBCarteira: TdmkDBLookupComboBox
      Left = 312
      Top = 32
      Width = 430
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCarteiras
      TabOrder = 2
      dmkEditCB = EdCarteira
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object RGCarteira: TRadioGroup
      Left = 36
      Top = 8
      Width = 193
      Height = 45
      Caption = ' Carteira: '
      Columns = 3
      Items.Strings = (
        'Caixa'
        'Banco'
        'Emiss'#227'o')
      TabOrder = 0
      TabStop = True
      OnClick = RGCarteiraClick
    end
    object CBDescoPor: TdmkDBLookupComboBox
      Left = 84
      Top = 171
      Width = 582
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'NOMEENTIDADE'
      TabOrder = 15
      Visible = False
      dmkEditCB = EdDescoPor
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdDescoPor: TdmkEditCB
      Left = 36
      Top = 171
      Width = 45
      Height = 21
      Alignment = taRightJustify
      TabOrder = 14
      Visible = False
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBDescoPor
      IgnoraDBLookupComboBox = False
    end
    object EdDescoVal: TdmkEdit
      Left = 673
      Top = 171
      Width = 69
      Height = 21
      Alignment = taRightJustify
      TabOrder = 16
      Visible = False
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object EdSerieCH: TdmkEdit
      Left = 38
      Top = 127
      Width = 49
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 10
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'SerieCH'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdDoc: TdmkEdit
      Left = 86
      Top = 127
      Width = 73
      Height = 21
      Alignment = taRightJustify
      TabOrder = 11
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 6
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '000000'
      QryCampo = 'Documento'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdMoraDia: TdmkEdit
      Left = 346
      Top = 80
      Width = 75
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdMoraDiaExit
      OnKeyDown = EdMoraDiaKeyDown
    end
    object EdMulta: TdmkEdit
      Left = 426
      Top = 80
      Width = 75
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdMultaExit
      OnKeyDown = EdMultaKeyDown
    end
    object EdMoraVal: TdmkEdit
      Left = 506
      Top = 80
      Width = 75
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdMoraValExit
    end
    object EdMultaVal: TdmkEdit
      Left = 586
      Top = 80
      Width = 75
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdMultaValExit
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 779
    Height = 40
    Align = alTop
    Caption = 'Edi'#231#227'o de pagamentos de lan'#231'amentos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 700
      Top = 1
      Width = 78
      Height = 38
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 617
      ExplicitTop = 2
      ExplicitHeight = 36
    end
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 699
      Height = 38
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 615
      ExplicitHeight = 36
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 40
    Width = 779
    Height = 62
    Align = alTop
    Enabled = False
    TabOrder = 3
    object GroupBox1: TGroupBox
      Left = 36
      Top = 1
      Width = 621
      Height = 60
      Caption = ' Dados para altera'#231#227'o '
      TabOrder = 0
      object Label2: TLabel
        Left = 140
        Top = 16
        Width = 59
        Height = 13
        Caption = 'Vencimento:'
      end
      object Label3: TLabel
        Left = 12
        Top = 16
        Width = 42
        Height = 13
        Caption = 'Controle:'
      end
      object Label4: TLabel
        Left = 76
        Top = 16
        Width = 22
        Height = 13
        Caption = 'Sub:'
      end
      object Label5: TLabel
        Left = 256
        Top = 16
        Width = 24
        Height = 13
        Caption = 'Tipo:'
      end
      object Label6: TLabel
        Left = 320
        Top = 16
        Width = 39
        Height = 13
        Caption = 'Carteira:'
      end
      object Label11: TLabel
        Left = 384
        Top = 16
        Width = 30
        Height = 13
        Caption = 'Saldo:'
      end
      object EdOldControle: TdmkEdit
        Left = 12
        Top = 32
        Width = 60
        Height = 21
        Alignment = taRightJustify
        Color = clInactiveCaption
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object TPOldData: TdmkEditDateTimePicker
        Left = 140
        Top = 32
        Width = 112
        Height = 21
        Date = 40568.480364108800000000
        Time = 40568.480364108800000000
        Color = clInactiveCaption
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        ReadOnly = True
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object EdOldSub: TdmkEdit
        Left = 76
        Top = 32
        Width = 60
        Height = 21
        Alignment = taRightJustify
        Color = clInactiveCaption
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdOldTipo: TdmkEdit
        Left = 256
        Top = 32
        Width = 60
        Height = 21
        Alignment = taRightJustify
        Color = clInactiveCaption
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdOldCarteira: TdmkEdit
        Left = 320
        Top = 32
        Width = 60
        Height = 21
        Alignment = taRightJustify
        Color = clInactiveCaption
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdOldSaldo: TdmkEdit
        Left = 384
        Top = 32
        Width = 102
        Height = 21
        Alignment = taRightJustify
        Color = clInactiveCaption
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clInactiveCaptionText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
    end
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT co.*, sg.Nome NOMESUBGRUPO ,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial'
      'ELSE cl.Nome END NOMEEMPRESA'
      'FROM contas co, Subgrupos sg,'
      'Grupos gr, Conjuntos cj,  Entidades cl'
      'WHERE sg.Codigo=co.Subgrupo'
      'AND gr.Codigo=sg.Grupo'
      'AND cj.Codigo=gr.Conjunto'
      'AND cl.Codigo=co.Empresa'
      '')
    Left = 460
    Top = 48
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrContasNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object QrContasNome3: TWideStringField
      FieldName = 'Nome3'
      Required = True
      Size = 50
    end
    object QrContasID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrContasSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrContasEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrContasCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContasDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContasMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContasExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrContasMensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrContasMensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrContasMensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrContasMenscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrContasMensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrContasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContasTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrContasExcel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrContasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContasUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrContasUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrContasNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Required = True
      Size = 50
    end
    object QrContasNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Required = True
      Size = 50
    end
    object QrContasNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Required = True
      Size = 50
    end
    object QrContasNOMEEMPRESA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 488
    Top = 48
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM carteiras '
      'WHERE Tipo=:P0'
      'AND Fornecei=:P1')
    Left = 572
    Top = 68
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.carteiras.Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.carteiras.Nome'
      FixedChar = True
      Size = 18
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCarteirasInicial: TFloatField
      FieldName = 'Inicial'
    end
    object QrCarteirasBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrCarteirasID: TWideStringField
      FieldName = 'ID'
      Size = 100
    end
    object QrCarteirasFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrCarteirasID_Fat: TWideStringField
      FieldName = 'ID_Fat'
      Size = 50
    end
    object QrCarteirasSaldo: TFloatField
      FieldName = 'Saldo'
    end
    object QrCarteirasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCarteirasEmCaixa: TFloatField
      FieldName = 'EmCaixa'
    end
    object QrCarteirasFechamento: TIntegerField
      FieldName = 'Fechamento'
    end
    object QrCarteirasPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrCarteirasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCarteirasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCarteirasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCarteirasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCarteirasPagRec: TIntegerField
      FieldName = 'PagRec'
    end
    object QrCarteirasDiaMesVence: TSmallintField
      FieldName = 'DiaMesVence'
    end
    object QrCarteirasExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrCarteirasNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 100
    end
    object QrCarteirasContato1: TWideStringField
      FieldName = 'Contato1'
      Size = 100
    end
    object QrCarteirasTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
    object QrCarteirasBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrCarteirasAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrCarteirasConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrCarteirasCheque1: TIntegerField
      FieldName = 'Cheque1'
    end
    object QrCarteirasAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrCarteirasContab: TWideStringField
      FieldName = 'Contab'
    end
    object QrCarteirasFuturoC: TFloatField
      FieldName = 'FuturoC'
    end
    object QrCarteirasFuturoD: TFloatField
      FieldName = 'FuturoD'
    end
    object QrCarteirasFuturoS: TFloatField
      FieldName = 'FuturoS'
    end
    object QrCarteirasOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrCarteirasForneceN: TSmallintField
      FieldName = 'ForneceN'
    end
    object QrCarteirasExclusivo: TSmallintField
      FieldName = 'Exclusivo'
    end
    object QrCarteirasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCarteirasRecebeBloq: TSmallintField
      FieldName = 'RecebeBloq'
    end
    object QrCarteirasEntiDent: TIntegerField
      FieldName = 'EntiDent'
    end
    object QrCarteirasCodCedente: TWideStringField
      FieldName = 'CodCedente'
    end
    object QrCarteirasValMorto: TFloatField
      FieldName = 'ValMorto'
    end
    object QrCarteirasAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 600
    Top = 68
  end
  object QrDescoPor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Account,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece1="V"'
      'OR Fornece2="V"'
      'OR Fornece3="V"'
      'OR Fornece2="V"'
      'ORDER BY NomeENTIDADE')
    Left = 464
    Top = 112
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object StringField3: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object IntegerField4: TIntegerField
      FieldName = 'Account'
      Required = True
    end
  end
  object DsDescoPor: TDataSource
    DataSet = QrDescoPor
    Left = 492
    Top = 112
  end
end
