unit CorrigePagtos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, DB, mySQLDbTables,
  mySQLDirectQuery, UMySQLModule, dmkGeral, UnDmkEnums;

type
  TFmCorrigePagtos = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrPagtos1: TmySQLQuery;
    PB1: TProgressBar;
    QrPagtos1Controle: TIntegerField;
    QrPagtos1ID_Pgto: TIntegerField;
    Edit1: TEdit;
    Label1: TLabel;
    Edit2: TEdit;
    Label2: TLabel;
    QrUpd: TmySQLDirectQuery;
    Memo1: TMemo;
    OpenDialog1: TOpenDialog;
    Memo2: TMemo;
    BitBtn1: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmCorrigePagtos: TFmCorrigePagtos;

implementation

uses UnMyObjects, Module, UnFinanceiro;

{$R *.DFM}

procedure TFmCorrigePagtos.BitBtn1Click(Sender: TObject);
const
  Step = 10;
var
  Conta, Total, Falta, Atual: Integer;
  T1, T2, T3: TDateTime;
//var
  i, j: integer;
  s, x, Ctrl, Data: String;
begin
  if OpenDialog1.Execute then
  begin
    Screen.Cursor := crHourGlass;
    T1 := Now();
    Memo1.Lines.LoadFromFile(OpenDialog1.FileName);
    Total := Memo1.Lines.Count;
    if Total = 0 then Total := 1;
    PB1.Max := Total;
    Conta := 0;
    Atual := 0;
     //
    for i := 0 to Memo1.Lines.Count - 1 do
    begin
      Atual := Atual + 1;
      Conta := Conta + 1;
      if (Conta div step = Conta / step) then
      begin
        { @3
        Dmod.MyDB.Execute(Texto);
        Texto := '';
        }
        PB1.Position := PB1.Position + step;
        Conta := 0;
        T2 := Now();
        Falta := Total - Atual;
        T3 := (T2 - T1) /  Atual * Falta;
        //
        Edit1.Text := FormatDateTime('hh:nn:ss', T3);
        Edit2.Text := IntToStr(Atual) + ' de ' +
                      IntToStr(Total) + '. Faltam ' + IntToStr(Falta);
        //
        Update;
        Application.ProcessMessages;
      end;
      s := Memo1.Lines[i];
      j := pos(#9, s);
      Ctrl := Copy(s, 1, j-1);
      Data := Copy(s, j+1);
      {
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET Compensado=:P0 WHERE Controle=:P1');
      Dmod.QrUpd.Params[00].AsString := Data;
      Dmod.QrUpd.Params[01].AsString := Ctrl;
      Dmod.QrUpd.ExecSQL;
      }
      UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
      'Compensado'], ['Controle'], [Data], [Ctrl], True, '');
      //
      x := Ctrl + ' ' + Data;
      Memo2.Lines.Add(FormatFloat('000', i) + ' - ' + x);
    end;
    PB1.Position := PB1.Position + Conta;
    Screen.Cursor := crDefault;
    Application.MessageBox(
      'Atualização de compensado finalizado!', 'Aviso', MB_OK+MB_ICONINFORMATION);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCorrigePagtos.BtOKClick(Sender: TObject);
const
  Step = 10;
var
  Conta, Total, Falta: Integer;
  T1, T2, T3: TDateTime;
  Texto: String;
begin
  Screen.Cursor := crHourGlass;
  T1 := Now();
  QrPagtos1.Close;
  QrPagtos1.Open;
  //
  PB1.Position := 0;
  Total := QrPagtos1.RecordCount;
  if Total = 0 then Total := 1;
  //
  {@2
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET CtrlQuitPg=:P0');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
  }
  PB1.Max := Total;
  Conta := 0;
  Texto := '';
  while not QrPagtos1.Eof do
  begin
    { @3
    Texto := Texto + 'UPDATE lan ctos SET CtrlQuitPg=' + FormatFloat('0',
    QrPagtos1Controle.Value) + ' WHERE Controle=' + FormatFloat('0',
    QrPagtos1ID_Pgto.Value) + ';'#13+#10;
    }
    Conta := Conta + 1;
    if (Conta div step = Conta / step) then
    begin
      { @3
      Dmod.MyDB.Execute(Texto);
      Texto := '';
      }
      PB1.Position := PB1.Position + step;
      Conta := 0;
      T2 := Now();
      Falta := Total - QrPagtos1.RecNo;
      T3 := (T2 - T1) /  QrPagtos1.RecNo * Falta;
      //
      Edit1.Text := FormatDateTime('hh:nn:ss', T3);
      Edit2.Text := IntToStr(QrPagtos1.RecNo) + ' de ' +
                    IntToStr(Total) + '. Faltam ' + IntToStr(Falta);
      //
      Update;
      Application.ProcessMessages;
    end;
    { @2
    Dmod.QrUpd.Params[00].AsInteger := QrPagtos1Controle.Value;
    Dmod.QrUpd.Params[01].AsInteger := QrPagtos1ID_Pgto.Value;
    Dmod.QrUpd.ExecSQL;
    }
    {@1}
    {
    Dmod.MyDB.Execute('UPDATE lan ctos SET CtrlQuitPg=' + FormatFloat('0',
    QrPagtos1Controle.Value) + ' WHERE Controle=' + FormatFloat('0',
    QrPagtos1ID_Pgto.Value));
    }
    UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, ['CtrlQuitPg'], [
    'Controle'], [QrPagtos1Controle.Value], [QrPagtos1ID_Pgto.Value], True, '');
    {}
    //
    QrPagtos1.Next;
  end;
  PB1.Position := PB1.Position + Conta;
  Screen.Cursor := crDefault;
  Application.MessageBox(
    'Ligamento finalizado!', 'Aviso', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmCorrigePagtos.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCorrigePagtos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCorrigePagtos.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.

