unit ModuleFin;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mySQLDbTables, frxClass, frxDBSet, comctrls, Variants, dmkGeral, ABSMain,
  StdCtrls, ExtCtrls, DBCtrls, DBGrids, TypInfo, dmkEditDateTimePicker,
  UnDmkProcFunc, DmkDAC_PF, UnDmkEnums, UCreateFin;

type
  TDmodFin = class(TDataModule)
    frxDsCopyCH: TfrxDBDataset;
    QrCopyCH: TmySQLQuery;
    QrCopyCHDebito: TFloatField;
    QrCopyCHBanco1: TWideStringField;
    QrCopyCHAgencia1: TWideStringField;
    QrCopyCHConta1: TWideStringField;
    QrCopyCHSerieCH: TWideStringField;
    QrCopyCHDocumento: TWideStringField;
    QrCopyCHData: TDateField;
    QrCopyCHDescricao: TWideStringField;
    QrCopyCHFornecedor: TIntegerField;
    QrCopyCHNOMEFORNECE: TWideStringField;
    QrCopyCHNOMEBANCO: TWideStringField;
    QrCopyCHControle: TWideStringField;
    QrCopyCHCRUZADO_SIM: TBooleanField;
    QrCopyCHCRUZADO_NAO: TBooleanField;
    QrCopyCHVISADO_SIM: TBooleanField;
    QrCopyCHVISADO_NAO: TBooleanField;
    QrCopyCHTipoCH: TSmallintField;
    QrAdress1: TmySQLQuery;
    QrAdress1Codigo: TIntegerField;
    QrAdress1Cadastro: TDateField;
    QrAdress1NOMEDONO: TWideStringField;
    QrAdress1CNPJ_CPF: TWideStringField;
    QrAdress1IE_RG: TWideStringField;
    QrAdress1NIRE_: TWideStringField;
    QrAdress1RUA: TWideStringField;
    QrAdress1NUMERO: TLargeintField;
    QrAdress1COMPL: TWideStringField;
    QrAdress1BAIRRO: TWideStringField;
    QrAdress1CIDADE: TWideStringField;
    QrAdress1NOMELOGRAD: TWideStringField;
    QrAdress1NOMEUF: TWideStringField;
    QrAdress1Pais: TWideStringField;
    QrAdress1Lograd: TLargeintField;
    QrAdress1Tipo: TSmallintField;
    QrAdress1CEP: TLargeintField;
    QrAdress1TE1: TWideStringField;
    QrAdress1FAX: TWideStringField;
    QrAdress1ENatal: TDateField;
    QrAdress1PNatal: TDateField;
    QrAdress1ECEP_TXT: TWideStringField;
    QrAdress1NUMERO_TXT: TWideStringField;
    QrAdress1E_ALL: TWideStringField;
    QrAdress1CNPJ_TXT: TWideStringField;
    QrAdress1FAX_TXT: TWideStringField;
    QrAdress1TE1_TXT: TWideStringField;
    QrAdress1NATAL_TXT: TWideStringField;
    QrAdress1Respons1: TWideStringField;
    frxDsAdress1: TfrxDBDataset;
    QrCopyCHDATA_DE_HOJE: TWideStringField;
    QrCopyCHEXTENSO: TWideStringField;
    QrAtzCar: TmySQLQuery;
    QrAtzCarINICIAL: TFloatField;
    QrAtzCarATU_S: TFloatField;
    QrAtzCarFUT_C: TFloatField;
    QrAtzCarFUT_D: TFloatField;
    QrAtzCarFUT_S: TFloatField;
    QrAtzCarTipo: TIntegerField;
    QrUpd: TmySQLQuery;
    QrLocLct: TmySQLQuery;
    QrLocLctData: TDateField;
    QrLocLctTipo: TSmallintField;
    QrLocLctCarteira: TIntegerField;
    QrLocLctControle: TIntegerField;
    QrLocLctSub: TSmallintField;
    QrLocLctAutorizacao: TIntegerField;
    QrLocLctGenero: TIntegerField;
    QrLocLctDescricao: TWideStringField;
    QrLocLctNotaFiscal: TIntegerField;
    QrLocLctDebito: TFloatField;
    QrLocLctCredito: TFloatField;
    QrLocLctCompensado: TDateField;
    QrLocLctDocumento: TFloatField;
    QrLocLctSit: TIntegerField;
    QrLocLctVencimento: TDateField;
    QrLocLctLk: TIntegerField;
    QrLocLctFatID: TIntegerField;
    QrLocLctFatParcela: TIntegerField;
    QrLocLctID_Pgto: TIntegerField;
    QrLocLctID_Sub: TSmallintField;
    QrLocLctFatura: TWideStringField;
    QrLocLctBanco: TIntegerField;
    QrLocLctLocal: TIntegerField;
    QrLocLctCartao: TIntegerField;
    QrLocLctLinha: TIntegerField;
    QrLocLctOperCount: TIntegerField;
    QrLocLctLancto: TIntegerField;
    QrLocLctPago: TFloatField;
    QrLocLctMez: TIntegerField;
    QrLocLctFornecedor: TIntegerField;
    QrLocLctCliente: TIntegerField;
    QrLocLctMoraDia: TFloatField;
    QrLocLctMulta: TFloatField;
    QrLocLctProtesto: TDateField;
    QrLocLctDataCad: TDateField;
    QrLocLctDataAlt: TDateField;
    QrLocLctUserCad: TSmallintField;
    QrLocLctUserAlt: TSmallintField;
    QrLocLctDataDoc: TDateField;
    QrLocLctCtrlIni: TIntegerField;
    QrLocLctNivel: TIntegerField;
    QrLocLctVendedor: TIntegerField;
    QrLocLctAccount: TIntegerField;
    QrSaldo: TmySQLQuery;
    QrSaldoValor: TFloatField;
    DsDuplNF: TDataSource;
    QrDuplNF: TmySQLQuery;
    QrDuplNFData: TDateField;
    QrDuplNFControle: TIntegerField;
    QrDuplNFDescricao: TWideStringField;
    QrDuplNFCredito: TFloatField;
    QrDuplNFDebito: TFloatField;
    QrDuplNFNotaFiscal: TIntegerField;
    QrDuplNFCompensado: TDateField;
    QrDuplNFMez: TIntegerField;
    QrDuplNFFornecedor: TIntegerField;
    QrDuplNFCliente: TIntegerField;
    QrDuplNFNOMECART: TWideStringField;
    QrDuplNFNOMECLI: TWideStringField;
    QrDuplNFNOMEFNC: TWideStringField;
    QrDuplNFDocumento: TFloatField;
    QrDuplNFSerieCH: TWideStringField;
    QrDuplNFCarteira: TIntegerField;
    QrDuplNFTERCEIRO: TWideStringField;
    QrDuplNFMES: TWideStringField;
    QrDuplNFCHEQUE: TWideStringField;
    QrDuplNFCOMP_TXT: TWideStringField;
    DsDuplCH: TDataSource;
    QrDuplCH: TmySQLQuery;
    QrDuplCHData: TDateField;
    QrDuplCHControle: TIntegerField;
    QrDuplCHDescricao: TWideStringField;
    QrDuplCHCredito: TFloatField;
    QrDuplCHDebito: TFloatField;
    QrDuplCHNotaFiscal: TIntegerField;
    QrDuplCHCompensado: TDateField;
    QrDuplCHMez: TIntegerField;
    QrDuplCHFornecedor: TIntegerField;
    QrDuplCHCliente: TIntegerField;
    QrDuplCHNOMECART: TWideStringField;
    QrDuplCHNOMECLI: TWideStringField;
    QrDuplCHNOMEFNC: TWideStringField;
    QrDuplCHDocumento: TFloatField;
    QrDuplCHSerieCH: TWideStringField;
    QrDuplCHCarteira: TIntegerField;
    QrDuplCHTERCEIRO: TWideStringField;
    QrDuplCHMES: TWideStringField;
    QrDuplCHCHEQUE: TWideStringField;
    QrDuplCHCOMP_TXT: TWideStringField;
    QrVerifHis: TmySQLQuery;
    QrVerifHisCodigo: TIntegerField;
    QrVerifHisNome: TWideStringField;
    QrVerifHisSumCre: TFloatField;
    QrVerifHisSumDeb: TFloatField;
    QrVerifHisSdoFim: TFloatField;
    QrVerifHisValor: TFloatField;
    DsVerifHis: TDataSource;
    QrLocLctFatNum: TFloatField;
    QrSNC: TmySQLQuery;
    QrSNCNOME: TWideStringField;
    QrSNCSUMMOV: TFloatField;
    QrSNCSDOANT: TFloatField;
    QrSNCSUMCRE: TFloatField;
    QrSNCSUMDEB: TFloatField;
    QrSNCSDOFIM: TFloatField;
    QrSNCCodigo: TIntegerField;
    frxDsSNG: TfrxDBDataset;
    QrSNG: TmySQLQuery;
    QrSNGNivel: TIntegerField;
    QrSNGGenero: TIntegerField;
    QrSNGNomeGe: TWideStringField;
    QrSNGNomeNi: TWideStringField;
    QrSNGSumMov: TFloatField;
    QrSNGSdoAnt: TFloatField;
    QrSNGSumCre: TFloatField;
    QrSNGSumDeb: TFloatField;
    QrSNGSdoFim: TFloatField;
    QrSNGCtrla: TSmallintField;
    QrSNGSeleci: TSmallintField;
    QrSNCCodPla: TIntegerField;
    QrSNCCodCjt: TIntegerField;
    QrSNCCodGru: TIntegerField;
    QrSNCCodSgr: TIntegerField;
    QrSNGCodPla: TIntegerField;
    QrSNGCodCjt: TIntegerField;
    QrSNGCodGru: TIntegerField;
    QrSNGCodSgr: TIntegerField;
    QrSNGCODIGOS_TXT: TWideStringField;
    QrSNGOrdena: TWideStringField;
    QrCTCN: TmySQLQuery;
    QrSTCP: TmySQLQuery;
    QrSTCPSUMMOV: TFloatField;
    QrSTCPSDOANT: TFloatField;
    QrSTCPSUMCRE: TFloatField;
    QrSTCPSUMDEB: TFloatField;
    QrSTCPSDOFIM: TFloatField;
    frxDsSTCP: TfrxDBDataset;
    QrSNGKGT: TLargeintField;
    QrUDAPC: TmySQLQuery;
    QrTransf: TmySQLQuery;
    QrTransfData: TDateField;
    QrTransfTipo: TSmallintField;
    QrTransfCarteira: TIntegerField;
    QrTransfControle: TIntegerField;
    QrTransfGenero: TIntegerField;
    QrTransfDebito: TFloatField;
    QrTransfCredito: TFloatField;
    QrTransfDocumento: TFloatField;
    QrTransfSerieCH: TWideStringField;
    DsCartSum: TDataSource;
    QrCartSum: TmySQLQuery;
    QrCartSumSALDO: TFloatField;
    QrCartSumFuturoC: TFloatField;
    QrCartSumFuturoD: TFloatField;
    QrCartSumFuturoS: TFloatField;
    QrCartSumEmCaixa: TFloatField;
    QrCartSumDifere: TFloatField;
    QrCartSumSDO_FUT: TFloatField;
    QrCarts: TmySQLQuery;
    QrCartsCodigo: TIntegerField;
    QrCartsTipo: TIntegerField;
    QrCartsNome: TWideStringField;
    QrCartsInicial: TFloatField;
    QrCartsBanco: TIntegerField;
    QrCartsID: TWideStringField;
    QrCartsFatura: TWideStringField;
    QrCartsID_Fat: TWideStringField;
    QrCartsSaldo: TFloatField;
    QrCartsLk: TIntegerField;
    QrCartsEmCaixa: TFloatField;
    QrCartsDIFERENCA: TFloatField;
    QrCartsFechamento: TIntegerField;
    QrCartsPrazo: TSmallintField;
    QrCartsTIPOPRAZO: TWideStringField;
    QrCartsDataCad: TDateField;
    QrCartsDataAlt: TDateField;
    QrCartsUserCad: TSmallintField;
    QrCartsUserAlt: TSmallintField;
    QrCartsNOMEPAGREC: TWideStringField;
    QrCartsPagRec: TSmallintField;
    QrCartsFuturoC: TFloatField;
    QrCartsFuturoD: TFloatField;
    QrCartsFuturoS: TFloatField;
    DsCarts: TDataSource;
    QrLctos: TmySQLQuery;
    QrLctosData: TDateField;
    QrLctosTipo: TSmallintField;
    QrLctosCarteira: TIntegerField;
    QrLctosAutorizacao: TIntegerField;
    QrLctosGenero: TIntegerField;
    QrLctosDescricao: TWideStringField;
    QrLctosNotaFiscal: TIntegerField;
    QrLctosDebito: TFloatField;
    QrLctosCredito: TFloatField;
    QrLctosCompensado: TDateField;
    QrLctosDocumento: TFloatField;
    QrLctosSit: TIntegerField;
    QrLctosVencimento: TDateField;
    QrLctosLk: TIntegerField;
    QrLctosFatID: TIntegerField;
    QrLctosFatParcela: TIntegerField;
    QrLctosCONTA: TIntegerField;
    QrLctosNOMECONTA: TWideStringField;
    QrLctosNOMEEMPRESA: TWideStringField;
    QrLctosNOMESUBGRUPO: TWideStringField;
    QrLctosNOMEGRUPO: TWideStringField;
    QrLctosNOMECONJUNTO: TWideStringField;
    QrLctosNOMESIT: TWideStringField;
    QrLctosAno: TFloatField;
    QrLctosMENSAL: TWideStringField;
    QrLctosMENSAL2: TWideStringField;
    QrLctosBanco: TIntegerField;
    QrLctosLocal: TIntegerField;
    QrLctosFatura: TWideStringField;
    QrLctosSub: TSmallintField;
    QrLctosCartao: TIntegerField;
    QrLctosLinha: TIntegerField;
    QrLctosPago: TFloatField;
    QrLctosSALDO: TFloatField;
    QrLctosID_Sub: TSmallintField;
    QrLctosMez: TIntegerField;
    QrLctosFornecedor: TIntegerField;
    QrLctoscliente: TIntegerField;
    QrLctosMoraDia: TFloatField;
    QrLctosNOMECLIENTE: TWideStringField;
    QrLctosNOMEFORNECEDOR: TWideStringField;
    QrLctosTIPOEM: TWideStringField;
    QrLctosNOMERELACIONADO: TWideStringField;
    QrLctosOperCount: TIntegerField;
    QrLctosLancto: TIntegerField;
    QrLctosMulta: TFloatField;
    QrLctosATRASO: TFloatField;
    QrLctosJUROS: TFloatField;
    QrLctosDataDoc: TDateField;
    QrLctosNivel: TIntegerField;
    QrLctosVendedor: TIntegerField;
    QrLctosAccount: TIntegerField;
    QrLctosMes2: TLargeintField;
    QrLctosProtesto: TDateField;
    QrLctosDataCad: TDateField;
    QrLctosDataAlt: TDateField;
    QrLctosUserCad: TSmallintField;
    QrLctosUserAlt: TSmallintField;
    QrLctosControle: TIntegerField;
    QrLctosID_Pgto: TIntegerField;
    QrLctosCtrlIni: TIntegerField;
    QrLctosFatID_Sub: TIntegerField;
    QrLctosICMS_P: TFloatField;
    QrLctosICMS_V: TFloatField;
    QrLctosDuplicata: TWideStringField;
    QrLctosCOMPENSADO_TXT: TWideStringField;
    QrLctosCliInt: TIntegerField;
    QrLctosDepto: TIntegerField;
    QrLctosDescoPor: TIntegerField;
    QrLctosPrazo: TSmallintField;
    QrLctosForneceI: TIntegerField;
    QrLctosQtde: TFloatField;
    QrLctosEmitente: TWideStringField;
    QrLctosContaCorrente: TWideStringField;
    QrLctosCNPJCPF: TWideStringField;
    QrLctosDescoVal: TFloatField;
    QrLctosDescoControle: TIntegerField;
    QrLctosUnidade: TIntegerField;
    QrLctosNFVal: TFloatField;
    QrLctosAntigo: TWideStringField;
    QrLctosExcelGru: TIntegerField;
    QrLctosSerieCH: TWideStringField;
    QrLctosSERIE_CHEQUE: TWideStringField;
    QrLctosDoc2: TWideStringField;
    QrLctosNOMEFORNECEI: TWideStringField;
    QrLctosMoraVal: TFloatField;
    QrLctosMultaVal: TFloatField;
    QrLctosCNAB_Sit: TSmallintField;
    QrLctosBanco1: TIntegerField;
    QrLctosAgencia1: TIntegerField;
    QrLctosConta1: TWideStringField;
    QrLctosTipoCH: TSmallintField;
    QrLctosFatNum: TFloatField;
    DsLctos: TDataSource;
    QrSomaM: TmySQLQuery;
    QrSomaMValor: TFloatField;
    QrLocCta: TmySQLQuery;
    QrLocCtaMensal: TWideStringField;
    QrLPE: TmySQLQuery;
    QrLPEControle: TIntegerField;
    QrLPEData: TDateField;
    QrLPEDescricao: TWideStringField;
    QrLPECredito: TFloatField;
    QrLPEDebito: TFloatField;
    QrLPECarteira: TIntegerField;
    QrLPETipo: TSmallintField;
    QrLPENOMECLIINT: TWideStringField;
    DsLPE: TDataSource;
    QrAPL: TmySQLQuery;
    QrVLA: TmySQLQuery;
    QrErrQuit: TmySQLQuery;
    QrErrQuitControle1: TIntegerField;
    QrErrQuitControle2: TIntegerField;
    QrErrQuitData: TDateField;
    QrErrQuitSit: TIntegerField;
    QrErrQuitDescricao: TWideStringField;
    QrAPLCredito: TFloatField;
    QrAPLDebito: TFloatField;
    QrAPLMoraVal: TFloatField;
    QrAPLMultaVal: TFloatField;
    QrAPLData: TDateField;
    QrVLACredito: TFloatField;
    QrVLADebito: TFloatField;
    QrLPECLIENTE_INTERNO: TIntegerField;
    QrPesqCart: TmySQLQuery;
    QrPesqCartTipo: TIntegerField;
    QrCambioDia: TmySQLQuery;
    QrCambioUsu: TmySQLQuery;
    QrCambioUsuCodigo: TIntegerField;
    QrCambioDiaCodigo: TIntegerField;
    QrCambioDiaValor: TFloatField;
    QrTrfCtas: TmySQLQuery;
    QrTrfCtasData: TDateField;
    QrTrfCtasTipo: TSmallintField;
    QrTrfCtasCarteira: TIntegerField;
    QrTrfCtasControle: TIntegerField;
    QrTrfCtasGenero: TIntegerField;
    QrTrfCtasDebito: TFloatField;
    QrTrfCtasCredito: TFloatField;
    QrTrfCtasDocumento: TFloatField;
    QrTrfCtasSerieCH: TWideStringField;
    QrLctosSerieNF: TWideStringField;
    QrEndossan: TmySQLQuery;
    QrLctosNO_ENDOSSADO: TWideStringField;
    QrSomaLinhas: TmySQLQuery;
    QrSomaLinhasCREDITO: TFloatField;
    QrSomaLinhasDEBITO: TFloatField;
    QrSomaLinhasSALDO: TFloatField;
    DsSomaLinhas: TDataSource;
    QrSumBol: TmySQLQuery;
    QrSumBolVALOR: TFloatField;
    frxDsExtratos: TfrxDBDataset;
    frxDsSdoCtas: TfrxDBDataset;
    QrPesqAgr: TmySQLQuery;
    QrPesqAgrContasAgr: TIntegerField;
    QrPesqAgrNOMECART: TWideStringField;
    QrPesqAgrNOME2CART: TWideStringField;
    QrPesqAgrCarteira: TIntegerField;
    QrPesqAgrData: TDateField;
    QrPesqAgrDescricao: TWideStringField;
    QrPesqAgrCREDITO: TFloatField;
    QrPesqAgrDEBITO: TFloatField;
    QrPesqAgrSerieCH: TWideStringField;
    QrPesqAgrDocumento: TFloatField;
    QrPesqAgrDoc2: TWideStringField;
    QrPesqAgrMez: TIntegerField;
    QrPesqAgrApto: TIntegerField;
    QrPesqAgrNOMEAGR: TWideStringField;
    QrPesqAgrInfoDescri: TSmallintField;
    QrPesqAgrMensal: TSmallintField;
    QrPesqAgrNOMECLI: TWideStringField;
    QrPesqAgrOrdemCart: TIntegerField;
    QrPesqCar: TmySQLQuery;
    QrPesqCarCarteira: TIntegerField;
    QrPesqCarInicial: TFloatField;
    QrPesqCarSALDO: TFloatField;
    QrPesqCarNome: TWideStringField;
    QrPesqCarNome2: TWideStringField;
    QrPesqCarOrdemCart: TIntegerField;
    QrPesqRes: TmySQLQuery;
    QrPesqResNOMECART: TWideStringField;
    QrPesqResNOME2CART: TWideStringField;
    QrPesqResCarteira: TIntegerField;
    QrPesqResData: TDateField;
    QrPesqResDescricao: TWideStringField;
    QrPesqResCredito: TFloatField;
    QrPesqResDebito: TFloatField;
    QrPesqResMez: TIntegerField;
    QrPesqResSerieCH: TWideStringField;
    QrPesqResDocumento: TFloatField;
    QrPesqResOrdemCart: TIntegerField;
    QrPesqResDepto: TIntegerField;
    QrPesqSum: TmySQLQuery;
    QrPesqSumContasAgr: TIntegerField;
    QrPesqSumNOMECART: TWideStringField;
    QrPesqSumNOME2CART: TWideStringField;
    QrPesqSumCarteira: TIntegerField;
    QrPesqSumData: TDateField;
    QrPesqSumDescricao: TWideStringField;
    QrPesqSumCREDITO: TFloatField;
    QrPesqSumDEBITO: TFloatField;
    QrPesqSumMez: TIntegerField;
    QrPesqSumOrdemCart: TIntegerField;
    QrSdoTrf: TmySQLQuery;
    QrSdoMov: TmySQLQuery;
    QrSdoMovCarteira: TIntegerField;
    QrSdoMovSDO_CRE: TFloatField;
    QrSdoMovSDO_DEB: TFloatField;
    QrSdoCtas: TmySQLQuery;
    QrSdoCtasCarteira: TIntegerField;
    QrSdoCtasNome: TWideStringField;
    QrSdoCtasNome2: TWideStringField;
    QrSdoCtasOrdemCart: TIntegerField;
    QrSdoCtasSDO_CAD: TFloatField;
    QrSdoCtasSDO_ANT: TFloatField;
    QrSdoCtasSDO_INI: TFloatField;
    QrSdoCtasMOV_CRE: TFloatField;
    QrSdoCtasMOV_DEB: TFloatField;
    QrSdoCtasTRF_CRE: TFloatField;
    QrSdoCtasSDO_FIM: TFloatField;
    QrSdoCtasTRF_DEB: TFloatField;
    QrSdoCtasKGT: TLargeintField;
    DsExtratos: TDataSource;
    QrSdoCtasTipo: TIntegerField;
    QrSdoCtasNO_TipoCart: TWideStringField;
    QrPesqCarTipoCart: TIntegerField;
    QrPesqAgrTipoCart: TIntegerField;
    QrPesqResTipoCart: TIntegerField;
    QrPesqSumTipoCart: TIntegerField;
    QrSdoExcl: TmySQLQuery;
    frxDsSdoExcl: TfrxDBDataset;
    QrSdoExclCarteira: TIntegerField;
    QrSdoExclNome: TWideStringField;
    QrSdoExclNome2: TWideStringField;
    QrSdoExclOrdemCart: TIntegerField;
    QrSdoExclSDO_CAD: TFloatField;
    QrSdoExclSDO_ANT: TFloatField;
    QrSdoExclSDO_INI: TFloatField;
    QrSdoExclMOV_CRE: TFloatField;
    QrSdoExclMOV_DEB: TFloatField;
    QrSdoExclTRF_CRE: TFloatField;
    QrSdoExclSDO_FIM: TFloatField;
    QrSdoExclTRF_DEB: TFloatField;
    QrSdoExclKGT: TLargeintField;
    QrSdoExclTipo: TIntegerField;
    QrSdoExclNO_TipoCart: TWideStringField;
    QrELSCI: TmySQLQuery;
    QrSdoCarts: TmySQLQuery;
    QrSdoCartsInicial: TFloatField;
    QrSdoCtSdo: TmySQLQuery;
    QrSdoCtSdoSdoIni: TFloatField;
    QrContasNiv: TmySQLQuery;
    QrContasNivNivel: TIntegerField;
    QrContasNivGenero: TIntegerField;
    QrNiv1: TmySQLQuery;
    QrNiv1Genero: TIntegerField;
    QrNiv1Subgrupo: TIntegerField;
    QrNiv1Grupo: TIntegerField;
    QrNiv1Conjunto: TIntegerField;
    QrNiv1Plano: TIntegerField;
    QrNiv1Movim: TFloatField;
    QrNiv1NO_Cta: TWideStringField;
    QrNiv1OR_Cta: TIntegerField;
    QrNiv1NO_Sgr: TWideStringField;
    QrNiv1OR_SGr: TIntegerField;
    QrNiv1NO_Gru: TWideStringField;
    QrNiv1OR_Gru: TIntegerField;
    QrNiv1NO_Cjt: TWideStringField;
    QrNiv1OR_Cjt: TIntegerField;
    QrNiv1NO_Pla: TWideStringField;
    QrNiv1OR_Pla: TIntegerField;
    QrNivMov: TmySQLQuery;
    QrNivMovCredito: TFloatField;
    QrNivMovDebito: TFloatField;
    DsSaldosNiv: TDataSource;
    QrNiv2: TmySQLQuery;
    QrNiv3: TmySQLQuery;
    QrNiv4: TmySQLQuery;
    QrNiv5: TmySQLQuery;
    DsSdoPar: TDataSource;
    frxDsSaldosNiv: TfrxDBDataset;
    QrNiv5Genero: TLargeintField;
    QrNiv5Subgrupo: TLargeintField;
    QrNiv5Grupo: TLargeintField;
    QrNiv5Conjunto: TLargeintField;
    QrNiv5Plano: TIntegerField;
    QrNiv5Movim: TFloatField;
    QrNiv5NO_Cta: TWideStringField;
    QrNiv5OR_Cta: TLargeintField;
    QrNiv5NO_Sgr: TWideStringField;
    QrNiv5OR_SGr: TLargeintField;
    QrNiv5NO_Gru: TWideStringField;
    QrNiv5OR_Gru: TLargeintField;
    QrNiv5NO_Cjt: TWideStringField;
    QrNiv5OR_Cjt: TLargeintField;
    QrNiv5NO_Pla: TWideStringField;
    QrNiv5OR_Pla: TIntegerField;
    QrNiv4Genero: TLargeintField;
    QrNiv4Subgrupo: TLargeintField;
    QrNiv4Grupo: TLargeintField;
    QrNiv4Conjunto: TIntegerField;
    QrNiv4Plano: TIntegerField;
    QrNiv4Movim: TFloatField;
    QrNiv4NO_Cta: TWideStringField;
    QrNiv4OR_Cta: TLargeintField;
    QrNiv4NO_Sgr: TWideStringField;
    QrNiv4OR_SGr: TLargeintField;
    QrNiv4NO_Gru: TWideStringField;
    QrNiv4OR_Gru: TLargeintField;
    QrNiv4NO_Cjt: TWideStringField;
    QrNiv4OR_Cjt: TIntegerField;
    QrNiv4NO_Pla: TWideStringField;
    QrNiv4OR_Pla: TIntegerField;
    QrNiv3Genero: TLargeintField;
    QrNiv3Subgrupo: TLargeintField;
    QrNiv3Grupo: TIntegerField;
    QrNiv3Conjunto: TIntegerField;
    QrNiv3Plano: TIntegerField;
    QrNiv3Movim: TFloatField;
    QrNiv3NO_Cta: TWideStringField;
    QrNiv3OR_Cta: TLargeintField;
    QrNiv3NO_Sgr: TWideStringField;
    QrNiv3OR_SGr: TLargeintField;
    QrNiv3NO_Gru: TWideStringField;
    QrNiv3OR_Gru: TIntegerField;
    QrNiv3NO_Cjt: TWideStringField;
    QrNiv3OR_Cjt: TIntegerField;
    QrNiv3NO_Pla: TWideStringField;
    QrNiv3OR_Pla: TIntegerField;
    QrNiv2Genero: TLargeintField;
    QrNiv2Subgrupo: TIntegerField;
    QrNiv2Grupo: TIntegerField;
    QrNiv2Conjunto: TIntegerField;
    QrNiv2Plano: TIntegerField;
    QrNiv2Movim: TFloatField;
    QrNiv2NO_Cta: TWideStringField;
    QrNiv2OR_Cta: TLargeintField;
    QrNiv2NO_Sgr: TWideStringField;
    QrNiv2OR_SGr: TIntegerField;
    QrNiv2NO_Gru: TWideStringField;
    QrNiv2OR_Gru: TIntegerField;
    QrNiv2NO_Cjt: TWideStringField;
    QrNiv2OR_Cjt: TIntegerField;
    QrNiv2NO_Pla: TWideStringField;
    QrNiv2OR_Pla: TIntegerField;
    frxDsCreditos: TfrxDBDataset;
    DsCreditos: TDataSource;
    QrCreditos: TmySQLQuery;
    QrCreditosMez: TIntegerField;
    QrCreditosCredito: TFloatField;
    QrCreditosNOMECON: TWideStringField;
    QrCreditosNOMESGR: TWideStringField;
    QrCreditosNOMEGRU: TWideStringField;
    QrCreditosMES: TWideStringField;
    QrCreditosNOMECON_2: TWideStringField;
    QrCreditosSubPgto1: TIntegerField;
    QrCreditosControle: TIntegerField;
    QrCreditosSub: TSmallintField;
    QrCreditosCarteira: TIntegerField;
    QrCreditosCartao: TIntegerField;
    QrCreditosVencimento: TDateField;
    QrCreditosCompensado: TDateField;
    QrCreditosSit: TIntegerField;
    QrCreditosGenero: TIntegerField;
    QrCreditosTipo: TSmallintField;
    QrSaldoA: TmySQLQuery;
    QrSaldoAInicial: TFloatField;
    QrSaldoASALDO: TFloatField;
    QrSaldoATOTAL: TFloatField;
    frxDsSaldoA: TfrxDBDataset;
    frxDsResumo: TfrxDBDataset;
    QrResumo: TmySQLQuery;
    QrResumoCredito: TFloatField;
    QrResumoDebito: TFloatField;
    QrResumoSALDO: TFloatField;
    QrResumoFINAL: TFloatField;
    DsDebitos: TDataSource;
    frxDsDebitos: TfrxDBDataset;
    QrDebitos: TmySQLQuery;
    QrDebitosCOMPENSADO_TXT: TWideStringField;
    QrDebitosDATA: TWideStringField;
    QrDebitosDescricao: TWideStringField;
    QrDebitosDEBITO: TFloatField;
    QrDebitosNOTAFISCAL: TLargeintField;
    QrDebitosSERIECH: TWideStringField;
    QrDebitosDOCUMENTO: TFloatField;
    QrDebitosMEZ: TLargeintField;
    QrDebitosCompensado: TDateField;
    QrDebitosNOMECON: TWideStringField;
    QrDebitosNOMESGR: TWideStringField;
    QrDebitosNOMEGRU: TWideStringField;
    QrDebitosITENS: TLargeintField;
    QrDebitosMES: TWideStringField;
    QrDebitosSERIE_DOC: TWideStringField;
    QrDebitosNF_TXT: TWideStringField;
    QrDebitosMES2: TWideStringField;
    QrDebitosControle: TIntegerField;
    QrDebitosSub: TSmallintField;
    QrDebitosCarteira: TIntegerField;
    QrDebitosCartao: TIntegerField;
    QrDebitosVencimento: TDateField;
    QrDebitosSit: TIntegerField;
    QrDebitosGenero: TIntegerField;
    QrDebitosTipo: TSmallintField;
    QrPesqAgrNotaFiscal: TIntegerField;
    QrPesqSumNotaFiscal: TIntegerField;
    QrPesqResNotaFiscal: TIntegerField;
    QrRealizado: TmySQLQuery;
    QrRealizadoValor: TFloatField;
    QrCIni: TmySQLQuery;
    QrCIniCodigo: TIntegerField;
    QrCIniMinimo: TIntegerField;
    QrCIniMaximo: TIntegerField;
    QrCIniPendenMesSeg: TSmallintField;
    QrCIniCalculMesSeg: TSmallintField;
    QrCIniNome: TWideStringField;
    QrCIts1: TmySQLQuery;
    QrCIts1Codigo: TIntegerField;
    QrCIts1Nome: TWideStringField;
    QrCIts1Periodo: TIntegerField;
    QrCIts1Tipo: TIntegerField;
    QrCIts1Fator: TFloatField;
    QrCIts2: TmySQLQuery;
    QrCIts2Codigo: TIntegerField;
    QrCIts2Nome: TWideStringField;
    QrCIts2Periodo: TIntegerField;
    QrCIts2Tipo: TIntegerField;
    QrCIts2Fator: TFloatField;
    QrItsCtas: TmySQLQuery;
    QrItsCtasConta: TIntegerField;
    QrValFator: TmySQLQuery;
    QrValFatorValor: TFloatField;
    QrCtasResMes2: TmySQLQuery;
    QrCtasResMes2SeqImp: TIntegerField;
    QrCtasResMes2Conta: TIntegerField;
    QrCtasResMes2Nome: TWideStringField;
    QrCtasResMes2Periodo: TIntegerField;
    QrCtasResMes2Tipo: TIntegerField;
    QrCtasResMes2Fator: TFloatField;
    QrCtasResMes2ValFator: TFloatField;
    QrCtasResMes2Devido: TFloatField;
    QrCtasResMes2Pago: TFloatField;
    QrCtasResMes2Diferenca: TFloatField;
    QrCtasResMes2Acumulado: TFloatField;
    QrCtasAnt: TmySQLQuery;
    QrCtasAntConta: TIntegerField;
    QrCtasAntAcumulado: TFloatField;
    QrCtasAntNome: TWideStringField;
    QrLctosNO_Carteira: TWideStringField;
    QrLocLcto: TmySQLQuery;
    QrLocLctoData: TDateField;
    QrLocLctoControle: TIntegerField;
    QrLocLctoTipo: TSmallintField;
    QrLocLctoCarteira: TIntegerField;
    QrLocLctoCliInt: TIntegerField;
    QrEndossanValor: TFloatField;
    QrCartsNOMEFORNECEI: TWideStringField;
    QrCartsNOMEDOBANCO: TWideStringField;
    QrCartsDiaMesVence: TSmallintField;
    QrCartsExigeNumCheque: TSmallintField;
    QrCartsForneceI: TIntegerField;
    QrCartsNome2: TWideStringField;
    QrCartsContato1: TWideStringField;
    QrCartsTipoDoc: TSmallintField;
    QrCartsBanco1: TIntegerField;
    QrCartsAgencia1: TIntegerField;
    QrCartsConta1: TWideStringField;
    QrCartsCheque1: TIntegerField;
    QrCartsAntigo: TWideStringField;
    QrCartsContab: TWideStringField;
    QrCartsOrdem: TIntegerField;
    QrCartsForneceN: TSmallintField;
    QrCartsExclusivo: TSmallintField;
    QrCartsAlterWeb: TSmallintField;
    QrCartsRecebeBloq: TSmallintField;
    QrCartsEntiDent: TIntegerField;
    QrCartsCodCedente: TWideStringField;
    QrCartsValMorto: TFloatField;
    QrCartsAtivo: TSmallintField;
    QrConsigna: TmySQLQuery;
    QrConsignaNome: TWideStringField;
    QrConsignaSaldo: TFloatField;
    QrTotalSaldo: TmySQLQuery;
    QrTotalSaldoNome: TWideStringField;
    QrTotalSaldoSaldo: TFloatField;
    QrTotalSaldoTipo: TIntegerField;
    QrTotalSaldoNOMETIPO: TWideStringField;
    frxSaldos: TfrxReport;
    QrSaldos: TmySQLQuery;
    QrSaldosNome: TWideStringField;
    QrSaldosSaldo: TFloatField;
    frxDsTotalSaldo: TfrxDBDataset;
    QrEndossad: TmySQLQuery;
    QrEndossadValor: TFloatField;
    QrVLAEndossan: TFloatField;
    QrVLAEndossad: TFloatField;
    QrVLATipo: TSmallintField;
    QrCartsIgnorSerie: TSmallintField;
    QrDuplVal: TmySQLQuery;
    DsDuplVal: TDataSource;
    QrDuplValData: TDateField;
    QrDuplValControle: TIntegerField;
    QrDuplValDescricao: TWideStringField;
    QrDuplValCredito: TFloatField;
    QrDuplValDebito: TFloatField;
    QrDuplValNotaFiscal: TIntegerField;
    QrDuplValCompensado: TDateField;
    QrDuplValMez: TIntegerField;
    QrDuplValFornecedor: TIntegerField;
    QrDuplValCliente: TIntegerField;
    QrDuplValNOMECART: TWideStringField;
    QrDuplValNOMECLI: TWideStringField;
    QrDuplValNOMEFNC: TWideStringField;
    QrDuplValDocumento: TFloatField;
    QrDuplValSerieCH: TWideStringField;
    QrDuplValCarteira: TIntegerField;
    QrDuplValTERCEIRO: TWideStringField;
    QrDuplValMES: TWideStringField;
    QrDuplValCHEQUE: TWideStringField;
    QrDuplValCOMP_TXT: TWideStringField;
    QrELSCIData: TDateField;
    QrELSCITipo: TSmallintField;
    QrELSCICarteira: TIntegerField;
    QrELSCIAutorizacao: TIntegerField;
    QrELSCIGenero: TIntegerField;
    QrELSCIDescricao: TWideStringField;
    QrELSCINotaFiscal: TIntegerField;
    QrELSCIDebito: TFloatField;
    QrELSCICredito: TFloatField;
    QrELSCICompensado: TDateField;
    QrELSCIDocumento: TFloatField;
    QrELSCISit: TIntegerField;
    QrELSCIVencimento: TDateField;
    QrELSCILk: TIntegerField;
    QrELSCIFatID: TIntegerField;
    QrELSCIFatParcela: TIntegerField;
    QrELSCICONTA: TIntegerField;
    QrELSCINOMECONTA: TWideStringField;
    QrELSCINOMEEMPRESA: TWideStringField;
    QrELSCINOMESUBGRUPO: TWideStringField;
    QrELSCINOMEGRUPO: TWideStringField;
    QrELSCINOMECONJUNTO: TWideStringField;
    QrELSCINOMESIT: TWideStringField;
    QrELSCIAno: TFloatField;
    QrELSCIMENSAL: TWideStringField;
    QrELSCIMENSAL2: TWideStringField;
    QrELSCIBanco: TIntegerField;
    QrELSCILocal: TIntegerField;
    QrELSCIFatura: TWideStringField;
    QrELSCISub: TSmallintField;
    QrELSCICartao: TIntegerField;
    QrELSCILinha: TIntegerField;
    QrELSCIPago: TFloatField;
    QrELSCISALDO: TFloatField;
    QrELSCIID_Sub: TSmallintField;
    QrELSCIMez: TIntegerField;
    QrELSCIFornecedor: TIntegerField;
    QrELSCIcliente: TIntegerField;
    QrELSCIMoraDia: TFloatField;
    QrELSCINOMECLIENTE: TWideStringField;
    QrELSCINOMEFORNECEDOR: TWideStringField;
    QrELSCITIPOEM: TWideStringField;
    QrELSCINOMERELACIONADO: TWideStringField;
    QrELSCIOperCount: TIntegerField;
    QrELSCILancto: TIntegerField;
    QrELSCIMulta: TFloatField;
    QrELSCIATRASO: TFloatField;
    QrELSCIJUROS: TFloatField;
    QrELSCIDataDoc: TDateField;
    QrELSCINivel: TIntegerField;
    QrELSCIVendedor: TIntegerField;
    QrELSCIAccount: TIntegerField;
    QrELSCIMes2: TLargeintField;
    QrELSCIProtesto: TDateField;
    QrELSCIDataCad: TDateField;
    QrELSCIDataAlt: TDateField;
    QrELSCIUserCad: TSmallintField;
    QrELSCIUserAlt: TSmallintField;
    QrELSCIControle: TIntegerField;
    QrELSCIID_Pgto: TIntegerField;
    QrELSCICtrlIni: TIntegerField;
    QrELSCIFatID_Sub: TIntegerField;
    QrELSCIICMS_P: TFloatField;
    QrELSCIICMS_V: TFloatField;
    QrELSCIDuplicata: TWideStringField;
    QrELSCICOMPENSADO_TXT: TWideStringField;
    QrELSCICliInt: TIntegerField;
    QrELSCIDepto: TIntegerField;
    QrELSCIDescoPor: TIntegerField;
    QrELSCIPrazo: TSmallintField;
    QrELSCIForneceI: TIntegerField;
    QrELSCIQtde: TFloatField;
    QrELSCIEmitente: TWideStringField;
    QrELSCIContaCorrente: TWideStringField;
    QrELSCICNPJCPF: TWideStringField;
    QrELSCIDescoVal: TFloatField;
    QrELSCIDescoControle: TIntegerField;
    QrELSCIUnidade: TIntegerField;
    QrELSCINFVal: TFloatField;
    QrELSCIAntigo: TWideStringField;
    QrELSCIExcelGru: TIntegerField;
    QrELSCISerieCH: TWideStringField;
    QrELSCISERIE_CHEQUE: TWideStringField;
    QrELSCIDoc2: TWideStringField;
    QrELSCINOMEFORNECEI: TWideStringField;
    QrELSCIMoraVal: TFloatField;
    QrELSCIMultaVal: TFloatField;
    QrELSCICNAB_Sit: TSmallintField;
    QrELSCIBanco1: TIntegerField;
    QrELSCIAgencia1: TIntegerField;
    QrELSCIConta1: TWideStringField;
    QrELSCITipoCH: TSmallintField;
    QrELSCIAlterWeb: TSmallintField;
    QrELSCIReparcel: TIntegerField;
    QrELSCIID_Quit: TIntegerField;
    QrELSCIAtrelado: TIntegerField;
    QrELSCIAtivo: TSmallintField;
    QrELSCIFatNum: TFloatField;
    QrELSCIProtocolo: TIntegerField;
    QrELSCIPagMul: TFloatField;
    QrELSCIPagJur: TFloatField;
    QrELSCICART_DONO: TIntegerField;
    QrELSCINOMECARTEIRA: TWideStringField;
    QrSaldosNiv: TmySQLQuery;
    QrSdoPar: TmySQLQuery;
    QrSdoParSdoAnt: TFloatField;
    QrSdoParCredito: TFloatField;
    QrSdoParDebito: TFloatField;
    QrSdoParMovim: TFloatField;
    QrExtratos: TmySQLQuery;
    QrSdoTrfCarteira: TLargeintField;
    QrSdoTrfSDO_CRE: TFloatField;
    QrSdoTrfSDO_DEB: TFloatField;
    QrExtratosLinha: TAutoIncField;
    QrExtratosDataM: TDateField;
    QrExtratosTexto: TWideStringField;
    QrExtratosDocum: TWideStringField;
    QrExtratosNotaF: TIntegerField;
    QrExtratosCredi: TFloatField;
    QrExtratosDebit: TFloatField;
    QrExtratosSaldo: TFloatField;
    QrExtratosCartO: TIntegerField;
    QrExtratosCartC: TIntegerField;
    QrExtratosCartN: TWideStringField;
    QrExtratosSdIni: TIntegerField;
    QrExtratosTipoI: TIntegerField;
    QrExtratosUnida: TWideStringField;
    QrExtratosCTipN: TWideStringField;
    QrExtratosDebCr: TWideStringField;
    QrZer: TmySQLQuery;
    QrZerCartC: TIntegerField;
    QrZerCredi: TFloatField;
    QrZerDebit: TFloatField;
    QrLocPerX: TmySQLQuery;
    QrLocPerXCodigo: TIntegerField;
    frxDsPrevItO: TfrxDBDataset;
    QrPrevItO: TmySQLQuery;
    QrPrevItOCodigo: TIntegerField;
    QrPrevItOControle: TIntegerField;
    QrPrevItOConta: TIntegerField;
    QrPrevItOValor: TFloatField;
    QrPrevItOPrevBaI: TIntegerField;
    QrPrevItOTexto: TWideStringField;
    QrPrevItOLk: TIntegerField;
    QrPrevItODataCad: TDateField;
    QrPrevItODataAlt: TDateField;
    QrPrevItOUserCad: TIntegerField;
    QrPrevItOUserAlt: TIntegerField;
    QrPrevItOPrevBaC: TIntegerField;
    QrPrevItOEmitVal: TFloatField;
    QrPrevItOEmitSit: TIntegerField;
    QrPrevItOSubGrupo: TIntegerField;
    QrPrevItONOMECONTA: TWideStringField;
    QrPrevItONOMESUBGRUPO: TWideStringField;
    QrPgBloq: TmySQLQuery;
    QrPgBloqCredito: TFloatField;
    QrPgBloqMultaVal: TFloatField;
    QrPgBloqMoraVal: TFloatField;
    QrPgBloqNOMEPROPRIET: TWideStringField;
    QrPgBloqUH: TWideStringField;
    QrPgBloqNOMECONTA: TWideStringField;
    QrPgBloqMez: TIntegerField;
    QrPgBloqVencimento: TDateField;
    QrPgBloqDocumento: TFloatField;
    QrPgBloqORIGINAL: TFloatField;
    QrPgBloqData: TDateField;
    QrPgBloqMES: TWideStringField;
    QrPgBloqDATA_TXT: TWideStringField;
    frxDsPgBloq: TfrxDBDataset;
    frxDsSuBloq: TfrxDBDataset;
    QrSuBloq: TmySQLQuery;
    QrSuBloqMultaVal: TFloatField;
    QrSuBloqMoraVal: TFloatField;
    QrSuBloqNOMECONTA: TWideStringField;
    QrSuBloqORIGINAL: TFloatField;
    QrSuBloqKGT: TLargeintField;
    QrSuBloqPAGO: TFloatField;
    QrPendG: TmySQLQuery;
    QrPendGCliente: TIntegerField;
    QrPendGFornecedor: TIntegerField;
    QrPendGMez: TIntegerField;
    QrPendGCredito: TFloatField;
    QrPendGNOMEPROPRIET: TWideStringField;
    QrPendGUnidade: TWideStringField;
    QrPendGNOMEMEZ: TWideStringField;
    QrPendGVencimento: TDateField;
    QrPendGFatNum: TFloatField;
    frxDsPendG: TfrxDBDataset;
    frxDsRep: TfrxDBDataset;
    QrRepBPP: TmySQLQuery;
    QrRepBPPCodigo: TIntegerField;
    QrRepBPPControle: TAutoIncField;
    QrRepBPPParcela: TIntegerField;
    QrRepBPPVlrOrigi: TFloatField;
    QrRepBPPVlrMulta: TFloatField;
    QrRepBPPVlrJuros: TFloatField;
    QrRepBPPLk: TIntegerField;
    QrRepBPPDataCad: TDateField;
    QrRepBPPDataAlt: TDateField;
    QrRepBPPUserCad: TIntegerField;
    QrRepBPPUserAlt: TIntegerField;
    QrRepBPPAlterWeb: TSmallintField;
    QrRepBPPLancto: TIntegerField;
    QrRepBPPValBol: TFloatField;
    QrRepBPPVencimento: TDateField;
    QrRepBPPVlrTotal: TFloatField;
    QrRepBPPAtivo: TSmallintField;
    QrRepBPPFatNum: TFloatField;
    frxDsRepBPP: TfrxDBDataset;
    QrRep: TmySQLQuery;
    QrRepCodigo: TIntegerField;
    QrReploginID: TWideStringField;
    QrRepautentica: TWideMemoField;
    QrRepCodCliEsp: TIntegerField;
    QrRepCodCliEnt: TIntegerField;
    QrRepCodigoEnt: TIntegerField;
    QrRepCodigoEsp: TIntegerField;
    QrRepVlrOrigi: TFloatField;
    QrRepVlrMulta: TFloatField;
    QrRepVlrJuros: TFloatField;
    QrRepVlrTotal: TFloatField;
    QrRepDataP: TDateTimeField;
    QrRepNovo: TSmallintField;
    QrRepTxaJur: TFloatField;
    QrRepTxaMul: TFloatField;
    QrRepLk: TIntegerField;
    QrRepDataCad: TDateField;
    QrRepDataAlt: TDateField;
    QrRepUserCad: TIntegerField;
    QrRepUserAlt: TIntegerField;
    QrRepAlterWeb: TSmallintField;
    QrRepAtivo: TSmallintField;
    QrRepUnidade: TWideStringField;
    QrRepNOMEPROP: TWideStringField;
    QrRepori: TmySQLQuery;
    QrReporiBloqOrigi: TIntegerField;
    QrReporiVlrOrigi: TFloatField;
    QrReporiVlrMulta: TFloatField;
    QrReporiVlrJuros: TFloatField;
    QrReporiVlrTotal: TFloatField;
    QrReplan: TmySQLQuery;
    QrReplanCredito: TFloatField;
    QrReplanVencimento: TDateField;
    QrReplanVLRORIG: TFloatField;
    QrReplanVLRMULTA: TFloatField;
    QrReplanVLRJUROS: TFloatField;
    QrReplanVLRTOTAL: TFloatField;
    QrReplanReparcel: TIntegerField;
    QrReplanFatNum: TFloatField;
    QrRepAbe: TmySQLQuery;
    QrRepAbeUnidade: TWideStringField;
    QrRepAbeData: TDateField;
    QrRepAbeVencimento: TDateField;
    QrRepAbeControle: TIntegerField;
    QrRepAbeDescricao: TWideStringField;
    QrRepAbeCredito: TFloatField;
    QrRepAbeCompensado: TDateField;
    QrRepAbeMoraDia: TFloatField;
    QrRepAbeMulta: TFloatField;
    QrRepAbeDepto: TIntegerField;
    QrRepAbeSit: TIntegerField;
    QrRepAbePago: TFloatField;
    QrRepAbePagMul: TFloatField;
    QrRepAbePagJur: TFloatField;
    QrRepAbeNOMEPROP: TWideStringField;
    QrRepAbeAtrelado: TIntegerField;
    QrRepAbeCompensado_TXT: TWideStringField;
    QrRepAbeFatNum: TFloatField;
    frxDsRepLan: TfrxDBDataset;
    frxDsRepAbe: TfrxDBDataset;
    QrCliInt: TmySQLQuery;
    QrCliIntPBB: TSmallintField;
    QrSaldosTipo: TIntegerField;
    QrSaldosCodigo: TIntegerField;
    frxDsPendAll: TfrxDBDataset;
    QrPendAll: TmySQLQuery;
    QrPendAllUnidade: TWideStringField;
    QrPendAllCredito: TFloatField;
    QrPendAllTIPO: TWideStringField;
    QrPendAllKGT: TLargeintField;
    QrPendAllBLOQUETOS: TLargeintField;
    QrPendSum: TmySQLQuery;
    QrPendSumCredito: TFloatField;
    frxDsPendSum: TfrxDBDataset;
    QrLctosUH: TWideStringField;
    QrAPLItens: TLargeintField;
    QrLctTipCart: TmySQLQuery;
    QrLctTipCartTipoA: TSmallintField;
    QrLctTipCartTipoB: TIntegerField;
    QrLctTipCartData: TDateField;
    QrLctTipCartCarteira: TIntegerField;
    QrLctTipCartControle: TIntegerField;
    QrLctTipCartSub: TSmallintField;
    QrLctTipCartCredito: TFloatField;
    QrLctTipCartDebito: TFloatField;
    DsLctTipCart: TDataSource;
    QrLctosID_Quit: TIntegerField;
    QrLctosReparcel: TIntegerField;
    QrLctosAtrelado: TIntegerField;
    QrLctosPagMul: TFloatField;
    QrLctosPagJur: TFloatField;
    QrLctosSubPgto1: TIntegerField;
    QrLctosMultiPgto: TIntegerField;
    QrLctosProtocolo: TIntegerField;
    QrLctosCtrlQuitPg: TIntegerField;
    QrLctosEndossas: TSmallintField;
    QrLctosEndossan: TFloatField;
    QrLctosEndossad: TFloatField;
    QrLctosCancelado: TSmallintField;
    QrLctosEventosCad: TIntegerField;
    QrLctosEncerrado: TIntegerField;
    QrLctosErrCtrl: TIntegerField;
    QrLctosAlterWeb: TSmallintField;
    QrLctosAtivo: TSmallintField;
    QrDelLct: TmySQLQuery;
    QrFats3: TmySQLQuery;
    QrFats3Data: TDateField;
    QrFats3Tipo: TSmallintField;
    QrFats3Carteira: TIntegerField;
    QrFats3Controle: TIntegerField;
    QrFats3Sub: TSmallintField;
    QrFats2: TmySQLQuery;
    QrFats2Data: TDateField;
    QrFats2Tipo: TSmallintField;
    QrFats2Carteira: TIntegerField;
    QrFats2Controle: TIntegerField;
    QrFats2Sub: TSmallintField;
    QrFats1: TmySQLQuery;
    QrFats1Data: TDateField;
    QrFats1Tipo: TSmallintField;
    QrFats1Carteira: TIntegerField;
    QrFats1Controle: TIntegerField;
    QrFats1Sub: TSmallintField;
    QrLcts: TmySQLQuery;
    QrLctsData: TDateField;
    QrLctsTipo: TSmallintField;
    QrLctsCarteira: TIntegerField;
    QrLctsControle: TIntegerField;
    QrLctsSub: TSmallintField;
    QrTransfSub: TSmallintField;
    QrTrfCtasSub: TSmallintField;
    QrLctosIndiPag: TIntegerField;
    QrDebitosNO_FORNECE: TWideStringField;
    QrLctosAgencia: TIntegerField;
    QrELSCIAgencia: TIntegerField;
    procedure QrCopyCHCalcFields(DataSet: TDataSet);
    procedure QrAdress1CalcFields(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure QrDuplCHCalcFields(DataSet: TDataSet);
    procedure QrDuplNFCalcFields(DataSet: TDataSet);
    procedure QrSNGCalcFields(DataSet: TDataSet);
    procedure QrLctosBeforeClose(DataSet: TDataSet);
    procedure QrLctosCalcFields(DataSet: TDataSet);
    procedure QrCartsAfterClose(DataSet: TDataSet);
    procedure QrCartsAfterScroll(DataSet: TDataSet);
    procedure QrCartsBeforeClose(DataSet: TDataSet);
    procedure QrCartsCalcFields(DataSet: TDataSet);
    procedure QrSomaLinhasCalcFields(DataSet: TDataSet);
    procedure QrSdoCtasCalcFields(DataSet: TDataSet);
    procedure QrSdoExclCalcFields(DataSet: TDataSet);
    procedure QrCreditosCalcFields(DataSet: TDataSet);
    procedure QrDebitosCalcFields(DataSet: TDataSet);
    procedure QrSaldoACalcFields(DataSet: TDataSet);
    procedure QrResumoCalcFields(DataSet: TDataSet);
    procedure QrTotalSaldoCalcFields(DataSet: TDataSet);
    procedure QrDuplValCalcFields(DataSet: TDataSet);
    procedure QrPgBloqCalcFields(DataSet: TDataSet);
    procedure QrPendGCalcFields(DataSet: TDataSet);
    procedure QrRepAfterScroll(DataSet: TDataSet);
    procedure QrRepAbeCalcFields(DataSet: TDataSet);
    procedure frxSaldosGetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
    FSaldos, FSdoNiveis, FExtratoCC, FSaldosNiv: String;
    procedure DefineDataSets_frxSaldos(Frx: TfrxReport);
  public
    { Public declarations }
    FTipoData: Integer;
    FCtasResMes: String;
    //FLDataIni, FLDataFim: TDateTime; Usar VAR_FL_DataIni e VAR_FL_DataFim
    FTPDataIni, FTPDataFim: TdmkEditDateTimePicker;
    FRGIntervalo: TRadioButton;
    FCamposLctz: String;
    //
    procedure PagarRolarEmissao(QrLct, QrCrt: TmySQLQuery);
    procedure AtualizaTodasCarteiras(QrCrt: TmySQLQuery);
    procedure ReabreSoLct(QrLct: TmySQLQuery;
              (*TipoCarteira,*) ItemCarteira, Controle, Sub: Integer;
              TPDataIni, TPDataFim: TDateTimePicker; Apto: Variant);
    function  AbreQrCopyCHeques(Controle: Integer): Boolean;
    function  Adress1DeEntidade(Entidade, TipoDeAdress1: Integer): String;
    (* Fonciona bem. Desativado porque usava tabela local. 
    function  SaldosNiveisPeriodo_1(Entidade, Periodo, Controla: Integer;
              TodosGeneros: Boolean; PB1, PB2, PB3, PB4, PB5: TProgressBar):
              Boolean;
    *)
    function  SaldosNiveisPeriodo_2(Entidade, Periodo, Controla: Integer;
              TodosGeneros: Boolean; PB1, PB2, PB3, PB4, PB5: TProgressBar;
              QrToOpen: TmySQLQuery): Boolean;
    function AlteraLanctoEsp(Query: TmySQLQuery; Acao: Integer;
            EspPercJuros, EspPercMulta: Double;
            EspTPDataIni, EspTPDataFim: TDateTimePicker; EspDepto: Variant;
            Finalidade: TIDFinalidadeLct): Integer;
    function CriaFmCartEditEsp(QrCrt, QrLct: TmySQLQuery;
             Altera, Tipo, Genero, Sit, Cartao, NF,
             Carteira, Fornecedor, Cliente, Vendedor, Account, CliInt,
             ForneceI, Depto, CNAB_Sit, ID_Pgto, TipoCH: Integer;
             Controle: Int64; Sub: Integer;
             Credito, Debito, Documento, MoraDia, Multa, ICMS_P, ICMS_V, Qtde,
             MultaVal, MoraVal: Double;
             Data, Vencimento, DataDoc, Compensado: TDateTime;
             Mensal, Descricao, Duplicata, SerieCH: String;
             Query: TmySQLQuery; SoTestar: Boolean; FatID, FatID_Sub, FatNum,
             FatParcela, Nivel, DescoPor, CtrlIni, Unidade: Integer;
             DescoVal, NFVal: Double; Doc2: String;
             EspPercJuros, EspPercMulta: Double;
             EspTPDataIni, EspTPDataFim: TDateTimePicker;
             EspDepto: Variant;
             EspCodigo, Finalidade: Integer): Integer;
    procedure VeSeReabreLct(TPDataIni, TPDataFim: TdmkEditDateTimePicker;
              Apto: Variant; Controle, Sub: Integer; QrCrt, QrLct:
              TmySQLQuery; ForcaAbertura: Boolean = False);
    function DefParams(QrCrt, QrCartSum: TmySQLQuery;
             AvisaErro: Boolean; Aviso: String): Boolean;
    procedure ReabreCarteiras(LocCart: Integer;
              QrCrt, QrCartSum: TmySQLQuery; Aviso: String);
    function LocCod(Atual, Codigo: Integer; QrCrt: TmySQLQuery;
             Aviso: String): Boolean;
    {$IfNDef SemCotacoes}
    procedure VerificaCotacoesDia();
    {$EndIf}
    function GeraBalanceteExtrato(Periodo, CliInt: Integer; PB: TProgressBar;
             LaAviso1: TLabel): Boolean;
    //function GeraBalanceteExtrato_A(Periodo, CliInt: Integer; PB: TProgressBar): Double;
    function ExisteLancamentoSemCliInt(InverteAviso: Boolean = False): Boolean;
    procedure AtualizaContasHistSdo3(CliInt: Integer; LaAviso: TLabel);
    function ConfereSalddosIniciais_Carteiras_x_Contas(CliInt: Integer): Boolean;
    //procedure SaldosDeContasControladas3(CliInt, Mez: Integer);
    procedure SaldosDeContasControladas3(CliInt, Mez: Integer);
    procedure ReopenCreditosEDebitos(CliInt: Integer; DataI, DataF: String;
              Acordos, Periodos, Textos, NaoAgruparNada: Boolean;
              CarteiraUnica: Integer);
    procedure ReopenSaldoAEResumo(CliInt, CarteiraUnica: Integer;
              DataI, DataF: String);
    function Reopen_APL_VLA(const ID_Pgto, SitAnt: Integer;
             var Data: String;
             var Pago, MoraVal, MultaVal: Double; var SitAtu: Integer;
             const DataCompQuit: String): Boolean;
    function GeraBalanceteRecDebi(Periodo, CliInt: Integer;
             Acordos, Periodos, Textos: Boolean): Boolean;
    procedure AtualizaContasMensais2(PeriodoIni, PeriodoFim, Genero: Integer;
              PB1, PB2, PB3: TProgressBar; ST1, ST2, ST3: TLabel);
    procedure ImprimeSaldos(CliInt: Integer);
    procedure AtualizaEndossos(Controle, Sub, OriCtrl, OriSub: Integer);
    function ReopenCliInt(CliInt: Integer): Boolean;
    function ReopenPrevItO(CliInt, Periodo: Integer): Boolean;
    function BloquetosDeCobrancaProcessados(Periodo, CliInt: Integer): Boolean;
    function ReopenPendG(Entidade, CliInt: Integer): Boolean;
    function ReopenReparcelNew(CliEnti, Periodo, PBB: Integer): Boolean;
    function ReopenReparcelAbe(CliEnti: Integer): Boolean;
    function ReopenPendAll(Entidade, CliInt, Periodo: Integer): Boolean;
    function CarregaItensTipoDoc(Compo: TControl): Boolean;
    function GeraDadosSaldos(Data: TDateTime; Exclusivo, NaoZerados:
             Boolean): Boolean;
  end;

var
  DmodFin: TDmodFin;

implementation

uses UnMyObjects, UnMLAGeral, UnInternalConsts, Module, SelfGer, UnFinanceiro,
CartPgt2, MyDBCheck, UMySQLModule, LctEdit, Periodo, UCreate, ContasHistAtz,
{$IfNDef SemCotacoes} CambioCot, {$EndIf} FinOrfao1, ModuleGeral;

{$R *.DFM}

{
function TDmodFin.GeraBalanceteExtrato(Periodo, CliInt: Integer; PB: TProgressBar): Double;
var
  DataA, DataI, DataF, Texto, Docum, NomeCart, DebCr: String;
  Saldo, Valor, Credito, Debito: Double;
  CartC: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  DataA := Geral.FDT(MLAGeral.PrimeiroDiaDoPeriodo_Date(Periodo)-1, 1);
  DataI := Geral.FDT(MLAGeral.PrimeiroDiaDoPeriodo_Date(Periodo), 1);
  DataF := Geral.FDT(MLAGeral.UltimoDiaDoPeriodo_Date(Periodo), 1);
  //
  QrExtratos.Close;
  QrExtratos.SQL.Clear;
  QrExtratos.SQL.Add('  DROP TABLE extratocc;                      ');
  QrExtratos.SQL.Add('  CREATE TABLE extratocc (                   ');
  QrExtratos.SQL.Add('  Linha        AUTOINCINTEGER,               ');
  QrExtratos.SQL.Add('  DataM        DATE,                         ');
  QrExtratos.SQL.Add('  Texto        VARCHAR(100),                 ');
  QrExtratos.SQL.Add('  Docum        VARCHAR(30),                  ');
  QrExtratos.SQL.Add('  NotaF        INTEGER,                      ');
  QrExtratos.SQL.Add('  Credi        FLOAT,                        ');
  QrExtratos.SQL.Add('  Debit        FLOAT,                        ');
  QrExtratos.SQL.Add('  Saldo        FLOAT,                        ');
  QrExtratos.SQL.Add('  CartO        INTEGER,                      ');
  QrExtratos.SQL.Add('  CartC        INTEGER,                      ');
  QrExtratos.SQL.Add('  CartN        VARCHAR(100),                 ');
  QrExtratos.SQL.Add('  SdIni        INTEGER,                      ');
  QrExtratos.SQL.Add('  TipoI        INTEGER,                      ');
  QrExtratos.SQL.Add('  Unida        VARCHAR(20),                  ');
  QrExtratos.SQL.Add('  CTipN        VARCHAR(30),                  ');
  QrExtratos.SQL.Add('  DebCr        CHAR(1)                       ');
  QrExtratos.SQL.Add('  );');
  //
  ABSUpd1.SQL.Clear;
  ABSUpd1.SQL.Add('INSERT INTO extratocc (DataM,Texto,Docum,NotaF,Credi,Debit,');
  ABSUpd1.SQL.Add('Saldo,CartO,CartC,CartN,SdIni,TipoI,Unida,CTipN,DebCr)');
  ABSUpd1.SQL.Add('Values(:P00,:P01,:P02,:P03,:P04,:P05,:P06,:P07,:P08,:P09,');
  ABSUpd1.SQL.Add(':P10,:P11,:P12,:P13,:P14)');
  //
  QrExtratos.SQL.Add('SELECT * FROM extratocc;');
  QrExtratos.Open;
   //  Saldos Iniciais
  QrPesqCar.Close;
  QrPesqCar.Params[00].AsString  := DataI;
  QrPesqCar.Params[01].AsInteger := CliInt;
  QrPesqCar.Open;
  // Agrupamentos de Contas
  QrPesqAgr.Close;
  QrPesqAgr.Params[00].AsInteger := CliInt;
  QrPesqAgr.Params[01].AsString  := DataI;
  QrPesqAgr.Params[02].AsString  := DataF;
  QrPesqAgr.Open;
  // Soma itens semelhantes
  QrPesqSum.Close;
  QrPesqSum.Params[00].AsInteger := CliInt;
  QrPesqSum.Params[01].AsString  := DataI;
  QrPesqSum.Params[02].AsString  := DataF;
  QrPesqSum.Open;
  // Soma itens restantes
  QrPesqRes.Close;
  QrPesqRes.Params[00].AsInteger := CliInt;
  QrPesqRes.Params[01].AsString  := DataI;
  QrPesqRes.Params[02].AsString  := DataF;
  QrPesqRes.Open;

  //
  if PB <> nil then
  begin
    PB.Position := 0;
    PB.Visible := True;
    PB.Max := QrPesqCar.RecordCount + QrPesqAgr.RecordCount +
              QrPesqSum.RecordCount + QrPesqRes.RecordCount;
    PB.Update;
    Application.ProcessMessages;
  end;

  //  Saldos Iniciais
  if QrPesqCar.RecordCount > 0 then
  begin
    QrPesqCar.First;
    while not QrPesqCar.Eof do
    begin
      if PB <> nil then PB.Position := PB.Position + 1;
      Valor := QrPesqCarInicial.Value + QrPesqCarSALDO.Value;
      if Valor < 0 then
      begin
        Credito := 0;
        Debito := -Valor;
      end else begin
        Credito := Valor;
        Debito  := 0;
      end;
      Docum := '';
      if Credito > 0 then DebCr := 'C' else DebCr := 'D';
      if Trim(QrPesqCarNome2.Value) <> '' then NomeCart := QrPesqCarNome2.Value
      else NomeCart := QrPesqCarNome.Value;
      ABSUpd1.Params[00].AsString  := DataA;
      ABSUpd1.Params[01].AsString  := 'S A L D O';//Texto;
      ABSUpd1.Params[02].AsString  := '';//Docum;
      ABSUpd1.Params[03].AsInteger := 0;// NotaF
      ABSUpd1.Params[04].AsFloat   := Credito;
      ABSUpd1.Params[05].AsFloat   := Debito;
      ABSUpd1.Params[06].AsFloat   := 0;// n�o tem como calcular aqui
      ABSUpd1.Params[07].AsInteger := QrPesqCarOrdemCart.Value;
      ABSUpd1.Params[08].AsInteger := QrPesqCarCarteira.Value;
      ABSUpd1.Params[09].AsString  := NomeCart;
      ABSUpd1.Params[10].AsInteger := 1; // Sim
      ABSUpd1.Params[11].AsInteger := 0; // Saldo Inicial
      ABSUpd1.Params[12].AsString  := DebCr;
      ABSUpd1.Params[13].AsString  := MLAGeral.GetNomeTipoCart(QrPesqCarTipoCart.Value, True);
      ABSUpd1.Params[14].AsString  := '';
      ABSUpd1.ExecSQL;
      //
      QrPesqCar.Next;
    end;
  end;

  // Agrupamentos de Contas
  if QrPesqAgr.RecordCount > 0 then
  begin
    QrPesqAgr.First;
    while not QrPesqAgr.Eof do
    begin
      if PB <> nil then PB.Position := PB.Position + 1;
      Texto := QrPesqAgrNOMEAGR.Value;
      case QrPesqAgrInfoDescri.Value of
        //1: Texto := Texto + ':' + QrPesqAgrUH.Value;
        2: Texto := Texto + ':' + QrPesqAgrNOMECLI.Value;
      end;
      if (QrPesqAgrMensal.Value > 0) and (QrPesqAgrMez.Value > 0)then
        Texto := Texto + ' - ' + MLAGeral.MezToFDT(QrPesqAgrMez.Value, 0, 104);
      //
      Docum := QrPesqAgrSerieCH.Value;
      if Trim(QrPesqAgrNOME2CART.Value) <> '' then
        NomeCart := QrPesqAgrNOME2CART.Value
      else
        NomeCart := QrPesqAgrNOMECART.Value;
      if QrPesqAgrDocumento.Value >0 then
        Docum := Docum + FormatFloat('000000', QrPesqAgrDocumento.Value);
      if QrPesqAgrCREDITO.Value > 0 then
        DebCr := 'C'
      else
        DebCr := 'D';
      ABSUpd1.Params[00].AsString  := Geral.FDT(QrPesqAgrData.Value, 1);
      ABSUpd1.Params[01].AsString  := Texto;
      ABSUpd1.Params[02].AsString  := Docum;
      ABSUpd1.Params[03].AsInteger := QrPesqAgrNotaFiscal.Value;
      ABSUpd1.Params[04].AsFloat   := QrPesqAgrCREDITO.Value;
      ABSUpd1.Params[05].AsFloat   := QrPesqAgrDEBITO.Value;
      ABSUpd1.Params[06].AsFloat   := 0; // n�o tem como calcular aqui
      ABSUpd1.Params[07].AsInteger := QrPesqAgrOrdemCart.Value;
      ABSUpd1.Params[08].AsInteger := QrPesqAgrCarteira.Value;
      ABSUpd1.Params[09].AsString  := NomeCart;
      ABSUpd1.Params[10].AsInteger := 0; // N�o
      ABSUpd1.Params[11].AsInteger := 1; // Agrupamentos de contas
      ABSUpd1.Params[12].AsString  := DebCr;
      ABSUpd1.Params[13].AsString  := MLAGeral.GetNomeTipoCart(QrPesqAgrTipoCart.Value, True);
      ABSUpd1.Params[14].AsString  := '';//QrPesqAgrUH.Value;
      ABSUpd1.ExecSQL;
      //
      QrPesqAgr.Next;
    end;
  end;

  // Soma itens semelhantes
  if QrPesqSum.RecordCount > 0 then
  begin
    QrPesqSum.First;
    while not QrPesqSum.Eof do
    begin
      if PB <> nil then PB.Position := PB.Position + 1;
      //
      Docum := '';
      if Trim(QrPesqSumNOME2CART.Value) <> '' then
        NomeCart := QrPesqSumNOME2CART.Value
      else
        NomeCart := QrPesqSumNOMECART.Value;
      if QrPesqSumCREDITO.Value > 0 then DebCr := 'C' else DebCr := 'D';
      ABSUpd1.Params[00].AsString  := Geral.FDT(QrPesqSumData.Value, 1);
      ABSUpd1.Params[01].AsString  := QrPesqSumDescricao.Value;
      ABSUpd1.Params[02].AsString  := Docum;
      ABSUpd1.Params[03].AsInteger := QrPesqSumNotaFiscal.Value;
      ABSUpd1.Params[04].AsFloat   := QrPesqSumCREDITO.Value;
      ABSUpd1.Params[05].AsFloat   := QrPesqSumDEBITO.Value;
      ABSUpd1.Params[06].AsFloat   := 0; // n�o tem como calcular aqui
      ABSUpd1.Params[07].AsInteger := QrPesqSumOrdemCart.Value;
      ABSUpd1.Params[08].AsInteger := QrPesqSumCarteira.Value;
      ABSUpd1.Params[09].AsString  := NomeCart;
      ABSUpd1.Params[10].AsInteger := 0; // N�o
      ABSUpd1.Params[11].AsInteger := 2; // Soma itens semelhantes
      ABSUpd1.Params[12].AsString  := DebCr;
      ABSUpd1.Params[13].AsString  := MLAGeral.GetNomeTipoCart(QrPesqSumTipoCart.Value, True);
      ABSUpd1.Params[14].AsString  := '';
      ABSUpd1.ExecSQL;
      //
      QrPesqSum.Next;
    end;
  end;

  // Soma itens restantantes
  if QrPesqRes.RecordCount > 0 then
  begin
    QrPesqRes.First;
    while not QrPesqRes.Eof do
    begin
      if PB <> nil then PB.Position := PB.Position + 1;
      //
      Docum := QrPesqResSerieCH.Value;
      if Trim(QrPesqResNOME2CART.Value) <> '' then
        NomeCart := QrPesqResNOME2CART.Value
      else
        NomeCart := QrPesqResNOMECART.Value;
      if QrPesqResDocumento.Value >0 then
        Docum := Docum + FormatFloat('000000', QrPesqResDocumento.Value);
      //
      if Trim(QrPesqResNOME2CART.Value) <> '' then
        NomeCart := QrPesqResNOME2CART.Value
      else
        NomeCart := QrPesqResNOMECART.Value;
      if QrPesqResCredito.Value > 0 then DebCr := 'C' else DebCr := 'D';
      ABSUpd1.Params[00].AsString  := Geral.FDT(QrPesqResData.Value, 1);
      ABSUpd1.Params[01].AsString  := QrPesqResDescricao.Value;
      ABSUpd1.Params[02].AsString  := Docum;
      ABSUpd1.Params[03].AsInteger := QrPesqResNotaFiscal.Value;
      ABSUpd1.Params[04].AsFloat   := QrPesqResCredito.Value;
      ABSUpd1.Params[05].AsFloat   := QrPesqResDebito.Value;
      ABSUpd1.Params[06].AsFloat   := 0; // n�o tem como calcular aqui
      ABSUpd1.Params[07].AsInteger := QrPesqResOrdemCart.Value;
      ABSUpd1.Params[08].AsInteger := QrPesqResCarteira.Value;
      ABSUpd1.Params[09].AsString  := NomeCart;
      ABSUpd1.Params[10].AsInteger := 0; // N�o
      ABSUpd1.Params[11].AsInteger := 3; // Soma itens restantes
      ABSUpd1.Params[12].AsString  := DebCr;
      ABSUpd1.Params[13].AsString  := MLAGeral.GetNomeTipoCart(QrPesqResTipoCart.Value, True);
      ABSUpd1.Params[14].AsString  := '';//QrPesqResUnidade.Value;
      ABSUpd1.ExecSQL;
      //
      QrPesqRes.Next;
    end;
  end;
  //
  // Excluir carteiras que n�o tem movimento e nem saldo
  ABSUpd2.SQL.Clear;
  ABSUpd2.SQL.Add('DELETE FROM extratocc WHERE CartC=:P0');
  QrZer.Close;
  QrZer.Open;
  while not QrZer.Eof do
  begin
    if (QrZerCredi.Value = 0) and (QrZerDebit.Value = 0) then
    begin
      ABSUpd2.Params[0].AsInteger := QrZerCartC.Value;
      ABSUpd2.ExecSQL;
    end;
    //
    QrZer.Next;
  end;
  // FIM - Excluir carteiras que n�o tem movimento e nem saldo
  //
  ABSUpd2.SQL.Clear;
  ABSUpd2.SQL.Add('UPDATE extratocc SET Saldo=:P0 ');
  ABSUpd2.SQL.Add('WHERE Linha=:P1 ');
  ABSUpd2.SQL.Add('');
  Saldo := 0;
  QrExtratos.Close;
  QrExtratos.SQL.Clear;
  QrExtratos.SQL.Add('SELECT * FROM extratocc');
  QrExtratos.SQL.Add('ORDER BY CartO, CartN, CartC, DataM, SdIni, DebCr, ');
  QrExtratos.SQL.Add('Unida,  Docum, TipoI');
  QrExtratos.Open;
  QrExtratos.First;
  CartC := -1000;
  while not QrExtratos.Eof do
  begin
    if CartC <> QrExtratosCartC.Value then
    begin
      Saldo := QrExtratosCredi.Value - QrExtratosDebit.Value;
      CartC := QrExtratosCartC.Value;
    end else begin
      Saldo := Saldo + QrExtratosCredi.Value - QrExtratosDebit.Value;
    end;
    //
    ABSUpd2.Params[00].AsFloat   := Saldo;
    ABSUpd2.Params[01].AsInteger := QrExtratosLinha.Value;
    ABSUpd2.ExecSQL;
    //
    QrExtratos.Next;
  end;
  QrExtratos.Close;
  QrExtratos.Open;
  //
  QrSdoTrf.Close;
  QrSdoTrf.Params[00].AsInteger := CliInt;
  QrSdoTrf.Params[01].AsString  := DataI;
  QrSdoTrf.Params[02].AsString  := DataF;
  QrSdoTrf.Open;
  //
  QrSdoMov.Close;
  QrSdoMov.Params[00].AsInteger := CliInt;
  QrSdoMov.Params[01].AsString  := DataI;
  QrSdoMov.Params[02].AsString  := DataF;
  QrSdoMov.Open;
  //
  QrSdoCtas.Close;
  QrSdoCtas.Params[00].AsString  := DataI;
  QrSdoCtas.Params[01].AsInteger := CliInt;
  QrSdoCtas.Open;
  //
  QrSdoExcl.Close;
  QrSdoExcl.Params[00].AsString  := DataI;
  QrSdoExcl.Params[01].AsInteger := CliInt;
  QrSdoExcl.Open;
  //
  // N�o ocultar. � usado depois
  //if PB <> nil then PB.Visible := False;
  //
  Screen.Cursor := crDefault;
  //
  Result := 0;
end;
}

function TDmodFin.GeraBalanceteExtrato(Periodo, CliInt: Integer; PB: TProgressBar;
LaAviso1: TLabel): Boolean;
var
  DataA, DataI, DataF, Texto, Docum, NomeCart, DebCr: String;
  Saldo, Valor, Credito, Debito: Double;
  CartC: Integer;
begin
  MyObjects.Informa(LaAviso1, True, 'Preparando pesquisa para gerar extrato');
  Screen.Cursor := crHourGlass;
  //
  DataA := Geral.FDT(MLAGeral.PrimeiroDiaDoPeriodo_Date(Periodo)-1, 1);
  DataI := Geral.FDT(MLAGeral.PrimeiroDiaDoPeriodo_Date(Periodo), 1);
  DataF := Geral.FDT(MLAGeral.UltimoDiaDoPeriodo_Date(Periodo), 1);
  //
  // informar o saldo total mesmo que n�o informe os saldos dos n�veis
  QrSTCP.Close;
  QrSTCP.Params[0].AsInteger := CliInt;
  QrSTCP.Params[1].AsInteger := Periodo;
  QrSTCP.Open;
  //
  QrExtratos.Close;
  QrExtratos.DataBase := DModG.MyPID_DB;
  QrZer.Close;
  QrZer.DataBase := DModG.MyPID_DB;
  //
  FExtratoCC := UCriar.RecriaTempTable('ExtratoCC', DModG.QrUpdPID1, False);
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO extratocc (DataM,Texto,Docum,NotaF,');
  DModG.QrUpdPID1.SQL.Add('Credi,Debit,Saldo,CartO,CartC,CartN,SdIni,TipoI,');
  DModG.QrUpdPID1.SQL.Add('Unida,CTipN,DebCr) Values(:P00,:P01,:P02,:P03,:P04,');
  DModG.QrUpdPID1.SQL.Add(':P05,:P06,:P07,:P08,:P09,:P10,:P11,:P12,:P13,:P14)');

   //  Saldos Iniciais
  MyObjects.Informa(LaAviso1, True, 'Pesquisando saldos iniciais das carteiras');
  QrPesqCar.Close;
  QrPesqCar.Params[00].AsString  := DataI;
  QrPesqCar.Params[01].AsInteger := CliInt;
  QrPesqCar.Open;
  // Agrupamentos de Contas
  MyObjects.Informa(LaAviso1, True, 'Pesquisando movimento de agrupamentos de contas no per�odo');
  QrPesqAgr.Close;
  QrPesqAgr.Params[00].AsInteger := CliInt;
  QrPesqAgr.Params[01].AsString  := DataI;
  QrPesqAgr.Params[02].AsString  := DataF;
  QrPesqAgr.Open;
  // Soma itens semelhantes
  QrPesqSum.Close;
  MyObjects.Informa(LaAviso1, True, 'Pesquisando movimento de contas semelhantes no per�odo');
  QrPesqSum.Params[00].AsInteger := CliInt;
  QrPesqSum.Params[01].AsString  := DataI;
  QrPesqSum.Params[02].AsString  := DataF;
  QrPesqSum.Open;
  // Soma itens restantes
  MyObjects.Informa(LaAviso1, True, 'Pesquisando movimento de contas n�o agrup�veis nem semelhantes no per�odo');
  QrPesqRes.Close;
  QrPesqRes.Params[00].AsInteger := CliInt;
  QrPesqRes.Params[01].AsString  := DataI;
  QrPesqRes.Params[02].AsString  := DataF;
  QrPesqRes.Open;

  //
  MyObjects.Informa(LaAviso1, True, '');
  if PB <> nil then
  begin
    PB.Position := 0;
    PB.Visible := True;
    PB.Max := QrPesqCar.RecordCount + QrPesqAgr.RecordCount +
              QrPesqSum.RecordCount + QrPesqRes.RecordCount;
    PB.Update;
    Application.ProcessMessages;
  end;

  //  Saldos Iniciais
  MyObjects.Informa(LaAviso1, True, 'Definindo saldos iniciais das carteiras');
  if QrPesqCar.RecordCount > 0 then
  begin
    QrPesqCar.First;
    while not QrPesqCar.Eof do
    begin
      if PB <> nil then PB.Position := PB.Position + 1;
      Valor := QrPesqCarInicial.Value + QrPesqCarSALDO.Value;
      if Valor < 0 then
      begin
        Credito := 0;
        Debito := -Valor;
      end else begin
        Credito := Valor;
        Debito  := 0;
      end;
      Docum := '';
      if Credito > 0 then DebCr := 'C' else DebCr := 'D';
      if Trim(QrPesqCarNome2.Value) <> '' then NomeCart := QrPesqCarNome2.Value
      else NomeCart := QrPesqCarNome.Value;
      DModG.QrUpdPID1.Params[00].AsString  := DataA;
      DModG.QrUpdPID1.Params[01].AsString  := 'S A L D O';//Texto;
      DModG.QrUpdPID1.Params[02].AsString  := '';//Docum;
      DModG.QrUpdPID1.Params[03].AsInteger := 0;// NotaF
      DModG.QrUpdPID1.Params[04].AsFloat   := Credito;
      DModG.QrUpdPID1.Params[05].AsFloat   := Debito;
      DModG.QrUpdPID1.Params[06].AsFloat   := 0;// n�o tem como calcular aqui
      DModG.QrUpdPID1.Params[07].AsInteger := QrPesqCarOrdemCart.Value;
      DModG.QrUpdPID1.Params[08].AsInteger := QrPesqCarCarteira.Value;
      DModG.QrUpdPID1.Params[09].AsString  := NomeCart;
      DModG.QrUpdPID1.Params[10].AsInteger := 1; // Sim
      DModG.QrUpdPID1.Params[11].AsInteger := 0; // Saldo Inicial
      DModG.QrUpdPID1.Params[12].AsString  := DebCr;
      DModG.QrUpdPID1.Params[13].AsString  := MLAGeral.GetNomeTipoCart(QrPesqCarTipoCart.Value, True);
      DModG.QrUpdPID1.Params[14].AsString  := '';
      DModG.QrUpdPID1.ExecSQL;
      //
      QrPesqCar.Next;
    end;
  end;

  // Agrupamentos de Contas
  MyObjects.Informa(LaAviso1, True, 'Definindo movimento de agrupamentos de contas no per�odo');
  if QrPesqAgr.RecordCount > 0 then
  begin
    QrPesqAgr.First;
    while not QrPesqAgr.Eof do
    begin
      if PB <> nil then PB.Position := PB.Position + 1;
      Texto := QrPesqAgrNOMEAGR.Value;
      case QrPesqAgrInfoDescri.Value of
        //1: Texto := Texto + ':' + QrPesqAgrUH.Value;
        2: Texto := Texto + ':' + QrPesqAgrNOMECLI.Value;
      end;
      if (QrPesqAgrMensal.Value > 0) and (QrPesqAgrMez.Value > 0)then
        Texto := Texto + ' - ' + MLAGeral.MezToFDT(QrPesqAgrMez.Value, 0, 104);
      //
      Docum := QrPesqAgrSerieCH.Value;
      if Trim(QrPesqAgrNOME2CART.Value) <> '' then
        NomeCart := QrPesqAgrNOME2CART.Value
      else
        NomeCart := QrPesqAgrNOMECART.Value;
      if QrPesqAgrDocumento.Value >0 then
        Docum := Docum + FormatFloat('000000', QrPesqAgrDocumento.Value);
      if QrPesqAgrCREDITO.Value > 0 then
        DebCr := 'C'
      else
        DebCr := 'D';
      DModG.QrUpdPID1.Params[00].AsString  := Geral.FDT(QrPesqAgrData.Value, 1);
      DModG.QrUpdPID1.Params[01].AsString  := Texto;
      DModG.QrUpdPID1.Params[02].AsString  := Docum;
      DModG.QrUpdPID1.Params[03].AsInteger := QrPesqAgrNotaFiscal.Value;
      DModG.QrUpdPID1.Params[04].AsFloat   := QrPesqAgrCREDITO.Value;
      DModG.QrUpdPID1.Params[05].AsFloat   := QrPesqAgrDEBITO.Value;
      DModG.QrUpdPID1.Params[06].AsFloat   := 0; // n�o tem como calcular aqui
      DModG.QrUpdPID1.Params[07].AsInteger := QrPesqAgrOrdemCart.Value;
      DModG.QrUpdPID1.Params[08].AsInteger := QrPesqAgrCarteira.Value;
      DModG.QrUpdPID1.Params[09].AsString  := NomeCart;
      DModG.QrUpdPID1.Params[10].AsInteger := 0; // N�o
      DModG.QrUpdPID1.Params[11].AsInteger := 1; // Agrupamentos de contas
      DModG.QrUpdPID1.Params[12].AsString  := DebCr;
      DModG.QrUpdPID1.Params[13].AsString  := MLAGeral.GetNomeTipoCart(QrPesqAgrTipoCart.Value, True);
      DModG.QrUpdPID1.Params[14].AsString  := '';//QrPesqAgrUH.Value;
      DModG.QrUpdPID1.ExecSQL;
      //
      QrPesqAgr.Next;
    end;
  end;

  // Soma itens semelhantes
  MyObjects.Informa(LaAviso1, True, 'Definindo movimento de contas semelhantes no per�odo');
  if QrPesqSum.RecordCount > 0 then
  begin
    QrPesqSum.First;
    while not QrPesqSum.Eof do
    begin
      if PB <> nil then PB.Position := PB.Position + 1;
      //
      Docum := '';
      if Trim(QrPesqSumNOME2CART.Value) <> '' then
        NomeCart := QrPesqSumNOME2CART.Value
      else
        NomeCart := QrPesqSumNOMECART.Value;
      if QrPesqSumCREDITO.Value > 0 then DebCr := 'C' else DebCr := 'D';
      DModG.QrUpdPID1.Params[00].AsString  := Geral.FDT(QrPesqSumData.Value, 1);
      DModG.QrUpdPID1.Params[01].AsString  := QrPesqSumDescricao.Value;
      DModG.QrUpdPID1.Params[02].AsString  := Docum;
      DModG.QrUpdPID1.Params[03].AsInteger := QrPesqSumNotaFiscal.Value;
      DModG.QrUpdPID1.Params[04].AsFloat   := QrPesqSumCREDITO.Value;
      DModG.QrUpdPID1.Params[05].AsFloat   := QrPesqSumDEBITO.Value;
      DModG.QrUpdPID1.Params[06].AsFloat   := 0; // n�o tem como calcular aqui
      DModG.QrUpdPID1.Params[07].AsInteger := QrPesqSumOrdemCart.Value;
      DModG.QrUpdPID1.Params[08].AsInteger := QrPesqSumCarteira.Value;
      DModG.QrUpdPID1.Params[09].AsString  := NomeCart;
      DModG.QrUpdPID1.Params[10].AsInteger := 0; // N�o
      DModG.QrUpdPID1.Params[11].AsInteger := 2; // Soma itens semelhantes
      DModG.QrUpdPID1.Params[12].AsString  := DebCr;
      DModG.QrUpdPID1.Params[13].AsString  := MLAGeral.GetNomeTipoCart(QrPesqSumTipoCart.Value, True);
      DModG.QrUpdPID1.Params[14].AsString  := '';
      DModG.QrUpdPID1.ExecSQL;
      //
      QrPesqSum.Next;
    end;
  end;

  // Soma itens restantantes
  MyObjects.Informa(LaAviso1, True, 'Defifnindo movimento de contas n�o agrup�veis nem semelhantes no per�odo');
  if QrPesqRes.RecordCount > 0 then
  begin
    QrPesqRes.First;
    while not QrPesqRes.Eof do
    begin
      if PB <> nil then PB.Position := PB.Position + 1;
      //
      Docum := QrPesqResSerieCH.Value;
      if Trim(QrPesqResNOME2CART.Value) <> '' then
        NomeCart := QrPesqResNOME2CART.Value
      else
        NomeCart := QrPesqResNOMECART.Value;
      if QrPesqResDocumento.Value >0 then
        Docum := Docum + FormatFloat('000000', QrPesqResDocumento.Value);
      //
      if Trim(QrPesqResNOME2CART.Value) <> '' then
        NomeCart := QrPesqResNOME2CART.Value
      else
        NomeCart := QrPesqResNOMECART.Value;
      if QrPesqResCredito.Value > 0 then DebCr := 'C' else DebCr := 'D';
      DModG.QrUpdPID1.Params[00].AsString  := Geral.FDT(QrPesqResData.Value, 1);
      DModG.QrUpdPID1.Params[01].AsString  := QrPesqResDescricao.Value;
      DModG.QrUpdPID1.Params[02].AsString  := Docum;
      DModG.QrUpdPID1.Params[03].AsInteger := QrPesqResNotaFiscal.Value;
      DModG.QrUpdPID1.Params[04].AsFloat   := QrPesqResCredito.Value;
      DModG.QrUpdPID1.Params[05].AsFloat   := QrPesqResDebito.Value;
      DModG.QrUpdPID1.Params[06].AsFloat   := 0; // n�o tem como calcular aqui
      DModG.QrUpdPID1.Params[07].AsInteger := QrPesqResOrdemCart.Value;
      DModG.QrUpdPID1.Params[08].AsInteger := QrPesqResCarteira.Value;
      DModG.QrUpdPID1.Params[09].AsString  := NomeCart;
      DModG.QrUpdPID1.Params[10].AsInteger := 0; // N�o
      DModG.QrUpdPID1.Params[11].AsInteger := 3; // Soma itens restantes
      DModG.QrUpdPID1.Params[12].AsString  := DebCr;
      DModG.QrUpdPID1.Params[13].AsString  := MLAGeral.GetNomeTipoCart(QrPesqResTipoCart.Value, True);
      DModG.QrUpdPID1.Params[14].AsString  := '';//QrPesqResUnidade.Value;
      DModG.QrUpdPID1.ExecSQL;
      //
      QrPesqRes.Next;
    end;
  end;
  //
  // Excluir carteiras que n�o tem movimento e nem saldo
  MyObjects.Informa(LaAviso1, True, 'Limpando dados desnecess�rios');
  DModG.QrUpdPID2.SQL.Clear;
  DModG.QrUpdPID2.SQL.Add('DELETE FROM extratocc WHERE CartC=:P0');
  QrZer.Close;
  QrZer.Open;
  while not QrZer.Eof do
  begin
    if (QrZerCredi.Value = 0) and (QrZerDebit.Value = 0) then
    begin
      DModG.QrUpdPID2.Params[0].AsInteger := QrZerCartC.Value;
      DModG.QrUpdPID2.ExecSQL;
    end;
    //
    QrZer.Next;
  end;
  // FIM - Excluir carteiras que n�o tem movimento e nem saldo
  //
  MyObjects.Informa(LaAviso1, True, 'Atribuindo saldo final de cada linha do extrato');
  DModG.QrUpdPID2.SQL.Clear;
  DModG.QrUpdPID2.SQL.Add('UPDATE extratocc SET Saldo=:P0 ');
  DModG.QrUpdPID2.SQL.Add('WHERE Linha=:P1 ');
  DModG.QrUpdPID2.SQL.Add('');
  Saldo := 0;
  QrExtratos.Close;
  QrExtratos.SQL.Clear;
  QrExtratos.SQL.Add('SELECT * FROM extratocc');
  QrExtratos.SQL.Add('ORDER BY CartO, CartN, CartC, DataM, SdIni, DebCr, ');
  QrExtratos.SQL.Add('Unida,  Docum, TipoI');
  QrExtratos.Open;
  QrExtratos.First;
  CartC := -1000;
  while not QrExtratos.Eof do
  begin
    if CartC <> QrExtratosCartC.Value then
    begin
      Saldo := QrExtratosCredi.Value - QrExtratosDebit.Value;
      CartC := QrExtratosCartC.Value;
    end else begin
      Saldo := Saldo + QrExtratosCredi.Value - QrExtratosDebit.Value;
    end;
    //
    DModG.QrUpdPID2.Params[00].AsFloat   := Saldo;
    DModG.QrUpdPID2.Params[01].AsInteger := QrExtratosLinha.Value;
    DModG.QrUpdPID2.ExecSQL;
    //
    QrExtratos.Next;
  end;
  MyObjects.Informa(LaAviso1, True, 'Abrindo resultados que ir�o gerar o relat�rio');
  QrExtratos.Close;
  QrExtratos.Open;
  //
  QrSdoTrf.Close;
  QrSdoTrf.Params[00].AsInteger := CliInt;
  QrSdoTrf.Params[01].AsString  := DataI;
  QrSdoTrf.Params[02].AsString  := DataF;
  QrSdoTrf.Open;
  //
  QrSdoMov.Close;
  QrSdoMov.Params[00].AsInteger := CliInt;
  QrSdoMov.Params[01].AsString  := DataI;
  QrSdoMov.Params[02].AsString  := DataF;
  QrSdoMov.Open;
  //
  QrSdoCtas.Close;
  QrSdoCtas.Params[00].AsString  := DataI;
  QrSdoCtas.Params[01].AsInteger := CliInt;
  QrSdoCtas.Open;
  //
  QrSdoExcl.Close;
  QrSdoExcl.Params[00].AsString  := DataI;
  QrSdoExcl.Params[01].AsInteger := CliInt;
  QrSdoExcl.Open;
  //
  MyObjects.Informa(LaAviso1, False, '...');
  // N�o ocultar. � usado depois
  //if PB <> nil then PB.Visible := False;
  if PB <> nil then
    PB.Position := 0;
  Screen.Cursor := crDefault;
  //
  Result := True;
end;

procedure TDmodFin.QrCopyCHCalcFields(DataSet: TDataSet);
begin
  QrCopyCHDATA_DE_HOJE.Value :=
    FormatDateTime(VAR_FORMATDATE6, QrCopyCHData.Value);
  QrCopyCHVISADO_SIM.Value  := MLAGeral.IntInConjunto(1, QrCopyCHTipoCH.Value);
  QrCopyCHCRUZADO_SIM.Value := MLAGeral.IntInConjunto(2, QrCopyCHTipoCH.Value);
  QrCopyCHVISADO_NAO.Value  := not QrCopyCHVISADO_SIM.Value;
  QrCopyCHCRUZADO_NAO.Value := not QrCopyCHCRUZADO_SIM.Value;
  QrCopyCHEXTENSO.Value     := dmkPF.ExtensoMoney(Geral.FFT(
    QrCopyCHDebito.Value, 2, siPositivo));
end;

procedure TDmodFin.QrCreditosCalcFields(DataSet: TDataSet);
var
  SubPgto1: String;
begin
  if QrCreditosSubPgto1.Value = 0 then SubPgto1 := ''
    else SubPgto1 := ' RECEB. ACORDO EXTRAJUDICIAL';

  QrCreditosNOMECON_2.Value := QrCreditosNOMECON.Value + SubPgto1;

  //

  QrCreditosMES.Value := dmkPF.MezToMesEAno(QrCreditosMez.Value);

end;

function TDmodFin.Adress1DeEntidade(Entidade, TipoDeAdress1: Integer): String;
begin
  QrAdress1.Close;
  QrAdress1.Params[0].AsInteger := Entidade;
  QrAdress1.Open;
  if QrAdress1.RecordCount > 0 then
  begin
    case TipoDeAdress1 of
      0: Result := QrAdress1E_ALL.Value;
      //1: Result := QrAdress1CIDADE.Value;
      else Result := '************';
    end;
  end else Result := '';
  // N�o fechar
  //QrAdress1.Close;
end;

procedure TDmodFin.QrAdress1CalcFields(DataSet: TDataSet);
var
  Natal: TDateTime;
begin
  QrAdress1TE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrAdress1Te1.Value);
  QrAdress1FAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrAdress1Fax.Value);
  QrAdress1CNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrAdress1CNPJ_CPF.Value);
  //
  QrAdress1NUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrAdress1Rua.Value, QrAdress1Numero.Value, False);
  QrAdress1E_ALL.Value := QrAdress1NOMELOGRAD.Value;
  if Trim(QrAdress1E_ALL.Value) <> '' then QrAdress1E_ALL.Value :=
    QrAdress1E_ALL.Value + ' ';
  QrAdress1E_ALL.Value := QrAdress1E_ALL.Value + QrAdress1Rua.Value;
  if Trim(QrAdress1Rua.Value) <> '' then QrAdress1E_ALL.Value :=
    QrAdress1E_ALL.Value + ', ' + QrAdress1NUMERO_TXT.Value;
  if Trim(QrAdress1Compl.Value) <>  '' then QrAdress1E_ALL.Value :=
    QrAdress1E_ALL.Value + ' ' + QrAdress1Compl.Value;
  if Trim(QrAdress1Bairro.Value) <>  '' then QrAdress1E_ALL.Value :=
    QrAdress1E_ALL.Value + ' - ' + QrAdress1Bairro.Value;
  if QrAdress1CEP.Value > 0 then QrAdress1E_ALL.Value :=
    QrAdress1E_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrAdress1CEP.Value);
  if Trim(QrAdress1Cidade.Value) <>  '' then QrAdress1E_ALL.Value :=
    QrAdress1E_ALL.Value + ' - ' + QrAdress1Cidade.Value;
  //
  QrAdress1ECEP_TXT.Value :=Geral.FormataCEP_NT(QrAdress1CEP.Value);
  //
  if QrAdress1Tipo.Value = 0 then Natal := QrAdress1ENatal.Value
  else Natal := QrAdress1PNatal.Value;
  if Natal < 2 then QrAdress1NATAL_TXT.Value := ''
  else QrAdress1NATAL_TXT.Value := FormatDateTime(VAR_FORMATDATE2, Natal);
end;

function TDmodFin.AbreQrCopyCHeques(Controle: Integer): Boolean;
begin
  QrCopyCH.Close;
  QrCopyCH.Params[0].AsInteger := Controle;
  QrCopyCH.Open;
  //
  Result := QrCopyCH.RecordCount > 0;
end;

procedure TDmodFin.DataModuleCreate(Sender: TObject);
var
  i: Integer;
begin
  DefineDataSets_frxSaldos(frxSaldos);
  //
  FCamposLctz := '';
  if Dmod = nil then
  begin
    Geral.MensagemBox('� necess�rio criar o "DMod" antes do "ModuleFin"',
    'ERRO', MB_OK+MB_ICONERROR);
    Halt(0);
  end;
  for i := 0 to ComponentCount - 1 do
  begin
    if (Components[i] is TmySQLQuery) then
    begin
      TmySQLQuery(Components[i]).Close;
      //N�o funciona se Dmod ainda n�o tiver sido criado!
      TmySQLQuery(Components[i]).Database := Dmod.MyDB;
    end;
  end;
  QrSNG.Database := DModG.MyPID_DB;
end;

procedure TDmodFin.QrDebitosCalcFields(DataSet: TDataSet);
begin
  QrDebitosMES.Value := dmkPF.MezToMesEAno(QrDebitosMez.Value);
  QrDebitosMES2.Value := dmkPF.MezToMesEAnoCurto(QrDebitosMez.Value);
  //
  QrDebitosSERIE_DOC.Value := Trim(QrDebitosSerieCH.Value);
  if QrDebitosDocumento.Value <> 0 then QrDebitosSERIE_DOC.Value :=
    QrDebitosSERIE_DOC.Value + FormatFloat('000000', QrDebitosDocumento.Value);
  //
  QrDebitosNF_TXT.Value := FormatFloat('000000;-000000; ', QrDebitosNotaFiscal.Value);
  //
end;

procedure TDmodFin.QrDuplCHCalcFields(DataSet: TDataSet);
begin
  QrDuplCHTERCEIRO.Value := QrDuplCHNOMEFNC.Value + QrDuplCHNOMECLI.Value;
  QrDuplCHMES.Value := MLAGeral.MezToFDT(QrDuplCHMez.Value, 0, 104);
  QrDuplCHCHEQUE.Value := Trim(QrDuplCHSerieCH.Value) +
    FormatFloat('000000', QrDuplCHDocumento.Value);
  if QrDuplCHCompensado.Value = 0 then QrDuplCHCOMP_TXT.Value := '' else
    QrDuplCHCOMP_TXT.Value := Geral.FDT(QrDuplCHCompensado.Value, 2);
end;

procedure TDmodFin.QrDuplNFCalcFields(DataSet: TDataSet);
begin
  QrDuplNFTERCEIRO.Value := QrDuplNFNOMEFNC.Value + QrDuplNFNOMECLI.Value;
  QrDuplNFMES.Value := MLAGeral.MezToFDT(QrDuplNFMez.Value, 0, 104);
  QrDuplNFCHEQUE.Value := Trim(QrDuplNFSerieCH.Value) +
    FormatFloat('000000', QrDuplNFDocumento.Value);
  if QrDuplNFCompensado.Value = 0 then QrDuplNFCOMP_TXT.Value := '' else
    QrDuplNFCOMP_TXT.Value := Geral.FDT(QrDuplNFCompensado.Value, 2);
end;

procedure TDmodFin.QrDuplValCalcFields(DataSet: TDataSet);
begin
  QrDuplValTERCEIRO.Value := QrDuplValNOMEFNC.Value + QrDuplValNOMECLI.Value;
  QrDuplValMES.Value := MLAGeral.MezToFDT(QrDuplValMez.Value, 0, 104);
  QrDuplValCHEQUE.Value := Trim(QrDuplValSerieCH.Value) +
    FormatFloat('000000', QrDuplValDocumento.Value);
  if QrDuplValCompensado.Value = 0 then QrDuplValCOMP_TXT.Value := '' else
    QrDuplValCOMP_TXT.Value := Geral.FDT(QrDuplValCompensado.Value, 2);
end;

{
procedure TDmodFin.SaldosDeContasControladas3(CliInt, Mez: Integer);
var
  SdoAnt, Credito, Debito, SdoFim: Double;
  ID_SEQ: Integer;
begin
  QrSaldosNiv.Close;
  QrSaldosNiv.SQL.Clear;
  QrSaldosNiv.SQL.Add('  DROP TABLE SaldosNiv;                      ');
  QrSaldosNiv.SQL.Add('  CREATE TABLE SaldosNiv (                   ');
  QrSaldosNiv.SQL.Add('  Nivel        INTEGER,                      ');
  // C�digo
  QrSaldosNiv.SQL.Add('  CO_Cta       INTEGER,                      ');
  QrSaldosNiv.SQL.Add('  CO_SGr       INTEGER,                      ');
  QrSaldosNiv.SQL.Add('  CO_Gru       INTEGER,                      ');
  QrSaldosNiv.SQL.Add('  CO_Cjt       INTEGER,                      ');
  QrSaldosNiv.SQL.Add('  CO_Pla       INTEGER,                      ');
  // Nome
  QrSaldosNiv.SQL.Add('  NO_Cta       VARCHAR(100),                 ');
  QrSaldosNiv.SQL.Add('  NO_SGr       VARCHAR(100),                 ');
  QrSaldosNiv.SQL.Add('  NO_Gru       VARCHAR(100),                 ');
  QrSaldosNiv.SQL.Add('  NO_Cjt       VARCHAR(100),                 ');
  QrSaldosNiv.SQL.Add('  NO_Pla       VARCHAR(100),                 ');
  // Ordem Lista
  QrSaldosNiv.SQL.Add('  OL_Cta       INTEGER,                      ');
  QrSaldosNiv.SQL.Add('  OL_SGr       INTEGER,                      ');
  QrSaldosNiv.SQL.Add('  OL_Gru       INTEGER,                      ');
  QrSaldosNiv.SQL.Add('  OL_Cjt       INTEGER,                      ');
  QrSaldosNiv.SQL.Add('  OL_Pla       INTEGER,                      ');
  // Valores
  QrSaldosNiv.SQL.Add('  SdoAnt       CURRENCY,                     ');
  QrSaldosNiv.SQL.Add('  Credito      CURRENCY,                     ');
  QrSaldosNiv.SQL.Add('  Debito       CURRENCY,                     ');
  QrSaldosNiv.SQL.Add('  Movim        CURRENCY,                     ');
  // Extra (C�pia do c�digo)
  QrSaldosNiv.SQL.Add('  EX_Cta       INTEGER,                      ');
  QrSaldosNiv.SQL.Add('  EX_SGr       INTEGER,                      ');
  QrSaldosNiv.SQL.Add('  EX_Gru       INTEGER,                      ');
  QrSaldosNiv.SQL.Add('  EX_Cjt       INTEGER,                      ');
  QrSaldosNiv.SQL.Add('  EX_Pla       INTEGER,                      ');
  //
  QrSaldosNiv.SQL.Add('  ID_SEQ       INTEGER,                      ');
  QrSaldosNiv.SQL.Add('  Ativo        SHORTINT                      ');
  QrSaldosNiv.SQL.Add('  );');
  QrSaldosNiv.SQL.Add('SELECT * FROM saldosniv;');
  QrSaldosNiv.Open;
  //
  ABSUpd1.SQL.Clear;
  ABSUpd1.SQL.Add('INSERT INTO saldosniv (Nivel,CO_Cta,CO_SGr,CO_Gru,CO_Cjt,' +
  'CO_Pla,NO_Cta,NO_SGr,NO_Gru,NO_Cjt,NO_Pla,OL_Cta,OL_SGr,OL_Gru,OL_Cjt,' +
  'OL_Pla,SdoAnt,Credito,Debito,Movim,EX_Cta,EX_SGr,EX_Gru,EX_Cjt,EX_Pla,' +
  'ID_SEQ,Ativo) VALUES (:P0,:P1,:P2,:P3,:P4,:P5,:P6,:P7,:P8,:P9,:P10,:P11,' +
  ':P12,:P13,:P14,:P15,:P16,:P17,:P18,:P19,:P20,:P21,:P22,:P23,:P24,:P25,:P26)');
  ID_SEQ := 0;
  //  CONTAS

  QrNiv1.Close;
  QrNiv1.Params[00].AsInteger := CliInt;
  QrNiv1.Params[01].AsInteger := CliInt;
  QrNiv1.Params[02].AsInteger := Mez;
  QrNiv1.Open;
  if QrNiv1.RecordCount > 0 then
  begin
    ID_SEQ := ID_SEQ + 1;
    QrNivMov.Close;
    QrNivMov.SQL.Clear;
    QrNivMov.SQL.Add('SELECT mov.Credito, mov.Debito');
    QrNivMov.SQL.Add('FROM contasmov mov');
    QrNivMov.SQL.Add('WHERE mov.CliInt=' + dmkPF.FFP(CliInt, 0));
    QrNivMov.SQL.Add('AND mov.Mez=' + dmkPF.FFP(Mez, 0));
    QrNivMov.SQL.Add('AND Genero=:P0');
    while not QrNiv1.Eof do
    begin
      QrNivMov.Close;
      QrNivMov.Params[0].AsInteger := QrNiv1Genero.Value;
      QrNivMov.Open;
      SdoAnt := QrNiv1Movim.Value - QrNivMovCredito.Value + QrNivMovDebito.Value;
      ABSUpd1.Params[00].AsInteger := 1;

      ABSUpd1.Params[01].AsInteger := QrNiv1Genero.Value;
      ABSUpd1.Params[02].AsInteger := QrNiv1Subgrupo.Value;
      ABSUpd1.Params[03].AsInteger := QrNiv1Grupo.Value;
      ABSUpd1.Params[04].AsInteger := QrNiv1Conjunto.Value;
      ABSUpd1.Params[05].AsInteger := QrNiv1Plano.Value;

      ABSUpd1.Params[06].AsString  := QrNiv1NO_Cta.Value;
      ABSUpd1.Params[07].AsString  := QrNiv1NO_SGr.Value;
      ABSUpd1.Params[08].AsString  := QrNiv1NO_Gru.Value;
      ABSUpd1.Params[09].AsString  := QrNiv1NO_Cjt.Value;
      ABSUpd1.Params[10].AsString  := QrNiv1NO_Pla.Value;

      ABSUpd1.Params[11].AsInteger := QrNiv1OR_Cta.Value;
      ABSUpd1.Params[12].AsInteger := QrNiv1OR_Sgr.Value;
      ABSUpd1.Params[13].AsInteger := QrNiv1OR_Gru.Value;
      ABSUpd1.Params[14].AsInteger := QrNiv1OR_Cjt.Value;
      ABSUpd1.Params[15].AsInteger := QrNiv1OR_Pla.Value;

      ABSUpd1.Params[16].AsFloat   := SdoAnt;
      ABSUpd1.Params[17].AsFloat   := QrNivMovCredito.Value;
      ABSUpd1.Params[18].AsFloat   := QrNivMovDebito.Value;
      ABSUpd1.Params[19].AsFloat   := QrNiv1Movim.Value;

      ABSUpd1.Params[20].AsInteger := QrNiv1Genero.Value;
      ABSUpd1.Params[21].AsInteger := QrNiv1Subgrupo.Value;
      ABSUpd1.Params[22].AsInteger := QrNiv1Grupo.Value;
      ABSUpd1.Params[23].AsInteger := QrNiv1Conjunto.Value;
      ABSUpd1.Params[24].AsInteger := QrNiv1Plano.Value;

      ABSUpd1.Params[25].AsInteger := ID_SEQ;
      ABSUpd1.Params[26].AsInteger := 1;
      //
      ABSUpd1.ExecSQL;
      QrNiv1.Next;
    end;
  end;

  // SUBGRUPOS

  QrNiv2.Close;
  QrNiv2.Params[00].AsInteger := CliInt;
  QrNiv2.Params[01].AsInteger := CliInt;
  QrNiv2.Params[02].AsInteger := Mez;
  QrNiv2.Open;
  if QrNiv2.RecordCount > 0 then
  begin
    ID_SEQ := ID_SEQ + 1;
    QrNivMov.Close;
    QrNivMov.SQL.Clear;
    QrNivMov.SQL.Add('SELECT SUM(mov.Credito) Credito, SUM(mov.Debito) Debito');
    QrNivMov.SQL.Add('FROM contasmov mov');
    QrNivMov.SQL.Add('WHERE mov.CliInt=' + dmkPF.FFP(CliInt, 0));
    QrNivMov.SQL.Add('AND mov.Mez=' + dmkPF.FFP(Mez, 0));
    QrNivMov.SQL.Add('AND Subgrupo=:P0');
    while not QrNiv2.Eof do
    begin
      QrSdoPar.Close;
      QrSdoPar.SQL.Clear;
      QrSdoPar.SQL.Add('SELECT SUM(SdoAnt) SdoAnt, SUM(Credito) Credito, ');
      QrSdoPar.SQL.Add('SUM(Debito) Debito, SUM(Movim) Movim ');
      QrSdoPar.SQL.Add('FROM saldosniv WHERE CO_SGr=:P0;');
      QrSdoPar.Params[00].AsInteger := QrNiv2Subgrupo.Value;
      QrSdoPar.Open;
      //
      QrNivMov.Close;
      QrNivMov.Params[0].AsInteger := QrNiv2Subgrupo.Value;
      QrNivMov.Open;
      SdoAnt  := QrNiv2Movim.Value   - QrNivMovCredito.Value +
                 QrNivMovDebito.Value  - QrSdoParSdoAnt.Value;
      Credito := QrNivMovCredito.Value - QrSdoParCredito.Value;
      Debito  := QrNivMovDebito.Value  - QrSdoParDebito.Value;
      SdoFim  := QrNiv2Movim.Value   - QrSdoParMovim.Value;
      //
      if (Credito <> 0) or (Debito <> 0) or (SdoAnt <> 0) or (SdoFim <> 0) then
      begin
        ABSUpd1.Params[00].AsInteger := 1;

        ABSUpd1.Params[01].AsInteger := QrNiv2Genero.Value;
        ABSUpd1.Params[02].AsInteger := QrNiv2Subgrupo.Value;
        ABSUpd1.Params[03].AsInteger := QrNiv2Grupo.Value;
        ABSUpd1.Params[04].AsInteger := QrNiv2Conjunto.Value;
        ABSUpd1.Params[05].AsInteger := QrNiv2Plano.Value;

        ABSUpd1.Params[06].AsString  := QrNiv2NO_Cta.Value;
        ABSUpd1.Params[07].AsString  := QrNiv2NO_SGr.Value;
        ABSUpd1.Params[08].AsString  := QrNiv2NO_Gru.Value;
        ABSUpd1.Params[09].AsString  := QrNiv2NO_Cjt.Value;
        ABSUpd1.Params[10].AsString  := QrNiv2NO_Pla.Value;

        ABSUpd1.Params[11].AsInteger := QrNiv2OR_Cta.Value;
        ABSUpd1.Params[12].AsInteger := QrNiv2OR_Sgr.Value;
        ABSUpd1.Params[13].AsInteger := QrNiv2OR_Gru.Value;
        ABSUpd1.Params[14].AsInteger := QrNiv2OR_Cjt.Value;
        ABSUpd1.Params[15].AsInteger := QrNiv2OR_Pla.Value;

        ABSUpd1.Params[16].AsFloat   := SdoAnt;
        ABSUpd1.Params[17].AsFloat   := Credito;
        ABSUpd1.Params[18].AsFloat   := Debito;
        ABSUpd1.Params[19].AsFloat   := SdoFim;

        ABSUpd1.Params[20].AsInteger := QrNiv2Genero.Value;
        ABSUpd1.Params[21].AsInteger := QrNiv2Subgrupo.Value;
        ABSUpd1.Params[22].AsInteger := QrNiv2Grupo.Value;
        ABSUpd1.Params[23].AsInteger := QrNiv2Conjunto.Value;
        ABSUpd1.Params[24].AsInteger := QrNiv2Plano.Value;

        ABSUpd1.Params[25].AsInteger := ID_SEQ;
        ABSUpd1.Params[26].AsInteger := 2;
        //
        ABSUpd1.ExecSQL;
      end;
      QrNiv2.Next;
    end;
  end;

  // GRUPOS

  QrNiv3.Close;
  QrNiv3.Params[00].AsInteger := CliInt;
  QrNiv3.Params[01].AsInteger := CliInt;
  QrNiv3.Params[02].AsInteger := Mez;
  QrNiv3.Open;
  if QrNiv3.RecordCount > 0 then
  begin
    ID_SEQ := ID_SEQ + 1;
    QrNivMov.Close;
    QrNivMov.SQL.Clear;
    QrNivMov.SQL.Add('SELECT SUM(mov.Credito) Credito, SUM(mov.Debito) Debito');
    QrNivMov.SQL.Add('FROM contasmov mov');
    QrNivMov.SQL.Add('WHERE mov.CliInt=' + dmkPF.FFP(CliInt, 0));
    QrNivMov.SQL.Add('AND mov.Mez=' + dmkPF.FFP(Mez, 0));
    QrNivMov.SQL.Add('AND Grupo=:P0');
    while not QrNiv3.Eof do
    begin
      QrSdoPar.Close;
      QrSdoPar.SQL.Clear;
      QrSdoPar.SQL.Add('SELECT SUM(SdoAnt) SdoAnt, SUM(Credito) Credito, ');
      QrSdoPar.SQL.Add('SUM(Debito) Debito, SUM(Movim) Movim ');
      QrSdoPar.SQL.Add('FROM saldosniv WHERE CO_Gru=:P0;');
      QrSdoPar.Params[00].AsInteger := QrNiv3Grupo.Value;
      QrSdoPar.Open;
      //
      QrNivMov.Close;
      QrNivMov.Params[0].AsInteger := QrNiv3Grupo.Value;
      QrNivMov.Open;
      SdoAnt  := QrNiv3Movim.Value   - QrNivMovCredito.Value +
                 QrNivMovDebito.Value  - QrSdoParSdoAnt.Value;
      Credito := QrNivMovCredito.Value - QrSdoParCredito.Value;
      Debito  := QrNivMovDebito.Value  - QrSdoParDebito.Value;
      SdoFim  := QrNiv3Movim.Value   - QrSdoParMovim.Value;
      //
      if (Credito <> 0) or (Debito <> 0) or (SdoAnt <> 0) or (SdoFim <> 0) then
      begin
        ABSUpd1.Params[00].AsInteger := 1;

        ABSUpd1.Params[01].AsInteger := QrNiv3Genero.Value;
        ABSUpd1.Params[02].AsInteger := QrNiv3Subgrupo.Value;
        ABSUpd1.Params[03].AsInteger := QrNiv3Grupo.Value;
        ABSUpd1.Params[04].AsInteger := QrNiv3Conjunto.Value;
        ABSUpd1.Params[05].AsInteger := QrNiv3Plano.Value;

        ABSUpd1.Params[06].AsString  := QrNiv3NO_Cta.Value;
        ABSUpd1.Params[07].AsString  := QrNiv3NO_SGr.Value;
        ABSUpd1.Params[08].AsString  := QrNiv3NO_Gru.Value;
        ABSUpd1.Params[09].AsString  := QrNiv3NO_Cjt.Value;
        ABSUpd1.Params[10].AsString  := QrNiv3NO_Pla.Value;

        ABSUpd1.Params[11].AsInteger := QrNiv3OR_Cta.Value;
        ABSUpd1.Params[12].AsInteger := QrNiv3OR_Sgr.Value;
        ABSUpd1.Params[13].AsInteger := QrNiv3OR_Gru.Value;
        ABSUpd1.Params[14].AsInteger := QrNiv3OR_Cjt.Value;
        ABSUpd1.Params[15].AsInteger := QrNiv3OR_Pla.Value;

        ABSUpd1.Params[16].AsFloat   := SdoAnt;
        ABSUpd1.Params[17].AsFloat   := Credito;
        ABSUpd1.Params[18].AsFloat   := Debito;
        ABSUpd1.Params[19].AsFloat   := SdoFim;

        ABSUpd1.Params[20].AsInteger := QrNiv3Genero.Value;
        ABSUpd1.Params[21].AsInteger := QrNiv3Subgrupo.Value;
        ABSUpd1.Params[22].AsInteger := QrNiv3Grupo.Value;
        ABSUpd1.Params[23].AsInteger := QrNiv3Conjunto.Value;
        ABSUpd1.Params[24].AsInteger := QrNiv3Plano.Value;

        ABSUpd1.Params[25].AsInteger := ID_SEQ;
        ABSUpd1.Params[26].AsInteger := 3;
        //
        ABSUpd1.ExecSQL;
      end;
      QrNiv3.Next;
    end;
  end;

  // CONJUNTOS

  QrNiv4.Close;
  QrNiv4.Params[00].AsInteger := CliInt;
  QrNiv4.Params[01].AsInteger := CliInt;
  QrNiv4.Params[02].AsInteger := Mez;
  QrNiv4.Open;
  if QrNiv4.RecordCount > 0 then
  begin
    ID_SEQ := ID_SEQ + 1;
    QrNivMov.Close;
    QrNivMov.SQL.Clear;
    QrNivMov.SQL.Add('SELECT SUM(mov.Credito) Credito, SUM(mov.Debito) Debito');
    QrNivMov.SQL.Add('FROM contasmov mov');
    QrNivMov.SQL.Add('WHERE mov.CliInt=' + dmkPF.FFP(CliInt, 0));
    QrNivMov.SQL.Add('AND mov.Mez=' + dmkPF.FFP(Mez, 0));
    QrNivMov.SQL.Add('AND Conjunto=:P0');
    while not QrNiv4.Eof do
    begin
      QrSdoPar.Close;
      QrSdoPar.SQL.Clear;
      QrSdoPar.SQL.Add('SELECT SUM(SdoAnt) SdoAnt, SUM(Credito) Credito, ');
      QrSdoPar.SQL.Add('SUM(Debito) Debito, SUM(Movim) Movim ');
      QrSdoPar.SQL.Add('FROM saldosniv WHERE CO_Cjt=:P0;');
      QrSdoPar.Params[00].AsInteger := QrNiv4Conjunto.Value;
      QrSdoPar.Open;
      //
      QrNivMov.Close;
      QrNivMov.Params[0].AsInteger := QrNiv4Conjunto.Value;
      QrNivMov.Open;
      SdoAnt  := QrNiv4Movim.Value   - QrNivMovCredito.Value +
                 QrNivMovDebito.Value  - QrSdoParSdoAnt.Value;
      Credito := QrNivMovCredito.Value - QrSdoParCredito.Value;
      Debito  := QrNivMovDebito.Value  - QrSdoParDebito.Value;
      SdoFim  := QrNiv4Movim.Value   - QrSdoParMovim.Value;
      //
      if (Credito <> 0) or (Debito <> 0) or (SdoAnt <> 0) or (SdoFim <> 0) then
      begin
        ABSUpd1.Params[00].AsInteger := 1;

        ABSUpd1.Params[01].AsInteger := QrNiv4Genero.Value;
        ABSUpd1.Params[02].AsInteger := QrNiv4Subgrupo.Value;
        ABSUpd1.Params[03].AsInteger := QrNiv4Grupo.Value;
        ABSUpd1.Params[04].AsInteger := QrNiv4Conjunto.Value;
        ABSUpd1.Params[05].AsInteger := QrNiv4Plano.Value;

        ABSUpd1.Params[06].AsString  := QrNiv4NO_Cta.Value;
        ABSUpd1.Params[07].AsString  := QrNiv4NO_SGr.Value;
        ABSUpd1.Params[08].AsString  := QrNiv4NO_Gru.Value;
        ABSUpd1.Params[09].AsString  := QrNiv4NO_Cjt.Value;
        ABSUpd1.Params[10].AsString  := QrNiv4NO_Pla.Value;

        ABSUpd1.Params[11].AsInteger := QrNiv4OR_Cta.Value;
        ABSUpd1.Params[12].AsInteger := QrNiv4OR_Sgr.Value;
        ABSUpd1.Params[13].AsInteger := QrNiv4OR_Gru.Value;
        ABSUpd1.Params[14].AsInteger := QrNiv4OR_Cjt.Value;
        ABSUpd1.Params[15].AsInteger := QrNiv4OR_Pla.Value;

        ABSUpd1.Params[16].AsFloat   := SdoAnt;
        ABSUpd1.Params[17].AsFloat   := Credito;
        ABSUpd1.Params[18].AsFloat   := Debito;
        ABSUpd1.Params[19].AsFloat   := SdoFim;

        ABSUpd1.Params[20].AsInteger := QrNiv4Genero.Value;
        ABSUpd1.Params[21].AsInteger := QrNiv4Subgrupo.Value;
        ABSUpd1.Params[22].AsInteger := QrNiv4Grupo.Value;
        ABSUpd1.Params[23].AsInteger := QrNiv4Conjunto.Value;
        ABSUpd1.Params[24].AsInteger := QrNiv4Plano.Value;

        ABSUpd1.Params[25].AsInteger := ID_SEQ;
        ABSUpd1.Params[26].AsInteger := 4;
        //
        ABSUpd1.ExecSQL;
      end;
      QrNiv4.Next;
    end;
  end;

  // PLANOS

  QrNiv5.Close;
  QrNiv5.Params[00].AsInteger := CliInt;
  QrNiv5.Params[01].AsInteger := CliInt;
  QrNiv5.Params[02].AsInteger := Mez;
  QrNiv5.Open;
  if QrNiv5.RecordCount > 0 then
  begin
    ID_SEQ := ID_SEQ + 1;
    QrNivMov.Close;
    QrNivMov.SQL.Clear;
    QrNivMov.SQL.Add('SELECT SUM(mov.Credito) Credito, SUM(mov.Debito) Debito');
    QrNivMov.SQL.Add('FROM contasmov mov');
    QrNivMov.SQL.Add('WHERE mov.CliInt=' + dmkPF.FFP(CliInt, 0));
    QrNivMov.SQL.Add('AND mov.Mez=' + dmkPF.FFP(Mez, 0));
    QrNivMov.SQL.Add('AND Plano=:P0');
    while not QrNiv5.Eof do
    begin
      QrSdoPar.Close;
      QrSdoPar.SQL.Clear;
      QrSdoPar.SQL.Add('SELECT SUM(SdoAnt) SdoAnt, SUM(Credito) Credito, ');
      QrSdoPar.SQL.Add('SUM(Debito) Debito, SUM(Movim) Movim ');
      QrSdoPar.SQL.Add('FROM saldosniv WHERE CO_Pla=:P0;');
      QrSdoPar.Params[00].AsInteger := QrNiv5Plano.Value;
      QrSdoPar.Open;
      //
      QrNivMov.Close;
      QrNivMov.Params[0].AsInteger := QrNiv5Plano.Value;
      QrNivMov.Open;
      SdoAnt  := QrNiv5Movim.Value   - QrNivMovCredito.Value +
                 QrNivMovDebito.Value  - QrSdoParSdoAnt.Value;
      Credito := QrNivMovCredito.Value - QrSdoParCredito.Value;
      Debito  := QrNivMovDebito.Value  - QrSdoParDebito.Value;
      SdoFim  := QrNiv5Movim.Value   - QrSdoParMovim.Value;
      //
      if (Credito <> 0) or (Debito <> 0) or (SdoAnt <> 0) or (SdoFim <> 0) then
      begin
        ABSUpd1.Params[00].AsInteger := 1;

        ABSUpd1.Params[01].AsInteger := QrNiv5Genero.Value;
        ABSUpd1.Params[02].AsInteger := QrNiv5Subgrupo.Value;
        ABSUpd1.Params[03].AsInteger := QrNiv5Grupo.Value;
        ABSUpd1.Params[04].AsInteger := QrNiv5Conjunto.Value;
        ABSUpd1.Params[05].AsInteger := QrNiv5Plano.Value;

        ABSUpd1.Params[06].AsString  := QrNiv5NO_Cta.Value;
        ABSUpd1.Params[07].AsString  := QrNiv5NO_SGr.Value;
        ABSUpd1.Params[08].AsString  := QrNiv5NO_Gru.Value;
        ABSUpd1.Params[09].AsString  := QrNiv5NO_Cjt.Value;
        ABSUpd1.Params[10].AsString  := QrNiv5NO_Pla.Value;

        ABSUpd1.Params[11].AsInteger := QrNiv5OR_Cta.Value;
        ABSUpd1.Params[12].AsInteger := QrNiv5OR_Sgr.Value;
        ABSUpd1.Params[13].AsInteger := QrNiv5OR_Gru.Value;
        ABSUpd1.Params[14].AsInteger := QrNiv5OR_Cjt.Value;
        ABSUpd1.Params[15].AsInteger := QrNiv5OR_Pla.Value;

        ABSUpd1.Params[16].AsFloat   := SdoAnt;
        ABSUpd1.Params[17].AsFloat   := Credito;
        ABSUpd1.Params[18].AsFloat   := Debito;
        ABSUpd1.Params[19].AsFloat   := SdoFim;

        ABSUpd1.Params[20].AsInteger := QrNiv5Genero.Value;
        ABSUpd1.Params[21].AsInteger := QrNiv5Subgrupo.Value;
        ABSUpd1.Params[22].AsInteger := QrNiv5Grupo.Value;
        ABSUpd1.Params[23].AsInteger := QrNiv5Conjunto.Value;
        ABSUpd1.Params[24].AsInteger := QrNiv5Plano.Value;

        ABSUpd1.Params[25].AsInteger := ID_SEQ;
        ABSUpd1.Params[26].AsInteger := 5;
        //
        ABSUpd1.ExecSQL;
      end;
      QrNiv5.Next;
    end;
  end;

  //

  QrSaldosNiv.Close;
  QrSaldosNiv.SQL.Clear;
  QrSaldosNiv.SQL.Add('SELECT * FROM saldosniv');
  QrSaldosNiv.SQL.Add('ORDER BY OL_Pla, NO_Pla, OL_Cjt, NO_Cjt, OL_Gru, ');
  QrSaldosNiv.SQL.Add('NO_Gru, OL_SGr, NO_SGr, OL_Cta, NO_Cta;');
  QrSaldosNiv.Open;
  //
end;
}

procedure TDmodFin.SaldosDeContasControladas3(CliInt, Mez: Integer);
var
  SdoAnt, Credito, Debito, SdoFim: Double;
  ID_SEQ: Integer;
begin
  QrSaldosNiv.Close;
  QrSaldosNiv.DataBase := DModG.MyPID_DB;
  QrSdoPar.Close;
  QrSdoPar.DataBase := DModG.MyPID_DB;
  //
  FSaldosNiv := UCriar.RecriaTempTable('SaldosNiv', DModG.QrUpdPID1, False);
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add(
  'INSERT INTO saldosniv (Nivel,CO_Cta,CO_SGr,CO_Gru,CO_Cjt,CO_Pla,' +
  'NO_Cta,NO_SGr,NO_Gru,NO_Cjt,NO_Pla,OL_Cta,OL_SGr,OL_Gru,OL_Cjt,' +
  'OL_Pla,SdoAnt,Credito,Debito,Movim,EX_Cta,EX_SGr,EX_Gru,EX_Cjt,EX_Pla,' +
  'ID_SEQ,Ativo) VALUES (:P0,:P1,:P2,:P3,:P4,:P5,:P6,:P7,:P8,:P9,:P10,:P11,' +
  ':P12,:P13,:P14,:P15,:P16,:P17,:P18,:P19,:P20,:P21,:P22,:P23,:P24,:P25,:P26)');
  //
  ID_SEQ := 0;

  //  CONTAS
  QrNiv1.Close;
  QrNiv1.Params[00].AsInteger := CliInt;
  QrNiv1.Params[01].AsInteger := CliInt;
  QrNiv1.Params[02].AsInteger := Mez;
  QrNiv1.Open;
  if QrNiv1.RecordCount > 0 then
  begin
    ID_SEQ := ID_SEQ + 1;
    QrNivMov.Close;
    QrNivMov.SQL.Clear;
    QrNivMov.SQL.Add('SELECT mov.Credito, mov.Debito');
    QrNivMov.SQL.Add('FROM contasmov mov');
    QrNivMov.SQL.Add('WHERE mov.CliInt=' + dmkPF.FFP(CliInt, 0));
    QrNivMov.SQL.Add('AND mov.Mez=' + dmkPF.FFP(Mez, 0));
    QrNivMov.SQL.Add('AND Genero=:P0');
    while not QrNiv1.Eof do
    begin
      QrNivMov.Close;
      QrNivMov.Params[0].AsInteger := QrNiv1Genero.Value;
      QrNivMov.Open;
      SdoAnt := QrNiv1Movim.Value - QrNivMovCredito.Value + QrNivMovDebito.Value;
      DModG.QrUpdPID1.Params[00].AsInteger := 1;

      DModG.QrUpdPID1.Params[01].AsInteger := QrNiv1Genero.Value;
      DModG.QrUpdPID1.Params[02].AsInteger := QrNiv1Subgrupo.Value;
      DModG.QrUpdPID1.Params[03].AsInteger := QrNiv1Grupo.Value;
      DModG.QrUpdPID1.Params[04].AsInteger := QrNiv1Conjunto.Value;
      DModG.QrUpdPID1.Params[05].AsInteger := QrNiv1Plano.Value;

      DModG.QrUpdPID1.Params[06].AsString  := QrNiv1NO_Cta.Value;
      DModG.QrUpdPID1.Params[07].AsString  := QrNiv1NO_SGr.Value;
      DModG.QrUpdPID1.Params[08].AsString  := QrNiv1NO_Gru.Value;
      DModG.QrUpdPID1.Params[09].AsString  := QrNiv1NO_Cjt.Value;
      DModG.QrUpdPID1.Params[10].AsString  := QrNiv1NO_Pla.Value;

      DModG.QrUpdPID1.Params[11].AsInteger := QrNiv1OR_Cta.Value;
      DModG.QrUpdPID1.Params[12].AsInteger := QrNiv1OR_Sgr.Value;
      DModG.QrUpdPID1.Params[13].AsInteger := QrNiv1OR_Gru.Value;
      DModG.QrUpdPID1.Params[14].AsInteger := QrNiv1OR_Cjt.Value;
      DModG.QrUpdPID1.Params[15].AsInteger := QrNiv1OR_Pla.Value;

      DModG.QrUpdPID1.Params[16].AsFloat   := SdoAnt;
      DModG.QrUpdPID1.Params[17].AsFloat   := QrNivMovCredito.Value;
      DModG.QrUpdPID1.Params[18].AsFloat   := QrNivMovDebito.Value;
      DModG.QrUpdPID1.Params[19].AsFloat   := QrNiv1Movim.Value;

      DModG.QrUpdPID1.Params[20].AsInteger := QrNiv1Genero.Value;
      DModG.QrUpdPID1.Params[21].AsInteger := QrNiv1Subgrupo.Value;
      DModG.QrUpdPID1.Params[22].AsInteger := QrNiv1Grupo.Value;
      DModG.QrUpdPID1.Params[23].AsInteger := QrNiv1Conjunto.Value;
      DModG.QrUpdPID1.Params[24].AsInteger := QrNiv1Plano.Value;

      DModG.QrUpdPID1.Params[25].AsInteger := ID_SEQ;
      DModG.QrUpdPID1.Params[26].AsInteger := 1;
      //
      DModG.QrUpdPID1.ExecSQL;
      QrNiv1.Next;
    end;
  end;

  // SUBGRUPOS

  QrNiv2.Close;
  QrNiv2.Params[00].AsInteger := CliInt;
  QrNiv2.Params[01].AsInteger := CliInt;
  QrNiv2.Params[02].AsInteger := Mez;
  QrNiv2.Open;
  if QrNiv2.RecordCount > 0 then
  begin
    ID_SEQ := ID_SEQ + 1;
    QrNivMov.Close;
    QrNivMov.SQL.Clear;
    QrNivMov.SQL.Add('SELECT SUM(mov.Credito) Credito, SUM(mov.Debito) Debito');
    QrNivMov.SQL.Add('FROM contasmov mov');
    QrNivMov.SQL.Add('WHERE mov.CliInt=' + dmkPF.FFP(CliInt, 0));
    QrNivMov.SQL.Add('AND mov.Mez=' + dmkPF.FFP(Mez, 0));
    QrNivMov.SQL.Add('AND Subgrupo=:P0');
    while not QrNiv2.Eof do
    begin
      QrSdoPar.Close;
      QrSdoPar.SQL.Clear;
      QrSdoPar.SQL.Add('SELECT SUM(SdoAnt) SdoAnt, SUM(Credito) Credito, ');
      QrSdoPar.SQL.Add('SUM(Debito) Debito, SUM(Movim) Movim ');
      QrSdoPar.SQL.Add('FROM saldosniv WHERE CO_SGr=:P0;');
      QrSdoPar.Params[00].AsInteger := QrNiv2Subgrupo.Value;
      QrSdoPar.Open;
      //
      QrNivMov.Close;
      QrNivMov.Params[0].AsInteger := QrNiv2Subgrupo.Value;
      QrNivMov.Open;
      SdoAnt  := QrNiv2Movim.Value   - QrNivMovCredito.Value +
                 QrNivMovDebito.Value  - QrSdoParSdoAnt.Value;
      Credito := QrNivMovCredito.Value - QrSdoParCredito.Value;
      Debito  := QrNivMovDebito.Value  - QrSdoParDebito.Value;
      SdoFim  := QrNiv2Movim.Value   - QrSdoParMovim.Value;
      //
      if (Credito <> 0) or (Debito <> 0) or (SdoAnt <> 0) or (SdoFim <> 0) then
      begin
        DModG.QrUpdPID1.Params[00].AsInteger := 1;

        DModG.QrUpdPID1.Params[01].AsInteger := QrNiv2Genero.Value;
        DModG.QrUpdPID1.Params[02].AsInteger := QrNiv2Subgrupo.Value;
        DModG.QrUpdPID1.Params[03].AsInteger := QrNiv2Grupo.Value;
        DModG.QrUpdPID1.Params[04].AsInteger := QrNiv2Conjunto.Value;
        DModG.QrUpdPID1.Params[05].AsInteger := QrNiv2Plano.Value;

        DModG.QrUpdPID1.Params[06].AsString  := QrNiv2NO_Cta.Value;
        DModG.QrUpdPID1.Params[07].AsString  := QrNiv2NO_SGr.Value;
        DModG.QrUpdPID1.Params[08].AsString  := QrNiv2NO_Gru.Value;
        DModG.QrUpdPID1.Params[09].AsString  := QrNiv2NO_Cjt.Value;
        DModG.QrUpdPID1.Params[10].AsString  := QrNiv2NO_Pla.Value;

        DModG.QrUpdPID1.Params[11].AsInteger := QrNiv2OR_Cta.Value;
        DModG.QrUpdPID1.Params[12].AsInteger := QrNiv2OR_Sgr.Value;
        DModG.QrUpdPID1.Params[13].AsInteger := QrNiv2OR_Gru.Value;
        DModG.QrUpdPID1.Params[14].AsInteger := QrNiv2OR_Cjt.Value;
        DModG.QrUpdPID1.Params[15].AsInteger := QrNiv2OR_Pla.Value;

        DModG.QrUpdPID1.Params[16].AsFloat   := SdoAnt;
        DModG.QrUpdPID1.Params[17].AsFloat   := Credito;
        DModG.QrUpdPID1.Params[18].AsFloat   := Debito;
        DModG.QrUpdPID1.Params[19].AsFloat   := SdoFim;

        DModG.QrUpdPID1.Params[20].AsInteger := QrNiv2Genero.Value;
        DModG.QrUpdPID1.Params[21].AsInteger := QrNiv2Subgrupo.Value;
        DModG.QrUpdPID1.Params[22].AsInteger := QrNiv2Grupo.Value;
        DModG.QrUpdPID1.Params[23].AsInteger := QrNiv2Conjunto.Value;
        DModG.QrUpdPID1.Params[24].AsInteger := QrNiv2Plano.Value;

        DModG.QrUpdPID1.Params[25].AsInteger := ID_SEQ;
        DModG.QrUpdPID1.Params[26].AsInteger := 2;
        //
        DModG.QrUpdPID1.ExecSQL;
      end;
      QrNiv2.Next;
    end;
  end;

  // GRUPOS

  QrNiv3.Close;
  QrNiv3.Params[00].AsInteger := CliInt;
  QrNiv3.Params[01].AsInteger := CliInt;
  QrNiv3.Params[02].AsInteger := Mez;
  QrNiv3.Open;
  if QrNiv3.RecordCount > 0 then
  begin
    ID_SEQ := ID_SEQ + 1;
    QrNivMov.Close;
    QrNivMov.SQL.Clear;
    QrNivMov.SQL.Add('SELECT SUM(mov.Credito) Credito, SUM(mov.Debito) Debito');
    QrNivMov.SQL.Add('FROM contasmov mov');
    QrNivMov.SQL.Add('WHERE mov.CliInt=' + dmkPF.FFP(CliInt, 0));
    QrNivMov.SQL.Add('AND mov.Mez=' + dmkPF.FFP(Mez, 0));
    QrNivMov.SQL.Add('AND Grupo=:P0');
    while not QrNiv3.Eof do
    begin
      QrSdoPar.Close;
      QrSdoPar.SQL.Clear;
      QrSdoPar.SQL.Add('SELECT SUM(SdoAnt) SdoAnt, SUM(Credito) Credito, ');
      QrSdoPar.SQL.Add('SUM(Debito) Debito, SUM(Movim) Movim ');
      QrSdoPar.SQL.Add('FROM saldosniv WHERE CO_Gru=:P0;');
      QrSdoPar.Params[00].AsInteger := QrNiv3Grupo.Value;
      QrSdoPar.Open;
      //
      QrNivMov.Close;
      QrNivMov.Params[0].AsInteger := QrNiv3Grupo.Value;
      QrNivMov.Open;
      SdoAnt  := QrNiv3Movim.Value   - QrNivMovCredito.Value +
                 QrNivMovDebito.Value  - QrSdoParSdoAnt.Value;
      Credito := QrNivMovCredito.Value - QrSdoParCredito.Value;
      Debito  := QrNivMovDebito.Value  - QrSdoParDebito.Value;
      SdoFim  := QrNiv3Movim.Value   - QrSdoParMovim.Value;
      //
      if (Credito <> 0) or (Debito <> 0) or (SdoAnt <> 0) or (SdoFim <> 0) then
      begin
        DModG.QrUpdPID1.Params[00].AsInteger := 1;

        DModG.QrUpdPID1.Params[01].AsInteger := QrNiv3Genero.Value;
        DModG.QrUpdPID1.Params[02].AsInteger := QrNiv3Subgrupo.Value;
        DModG.QrUpdPID1.Params[03].AsInteger := QrNiv3Grupo.Value;
        DModG.QrUpdPID1.Params[04].AsInteger := QrNiv3Conjunto.Value;
        DModG.QrUpdPID1.Params[05].AsInteger := QrNiv3Plano.Value;

        DModG.QrUpdPID1.Params[06].AsString  := QrNiv3NO_Cta.Value;
        DModG.QrUpdPID1.Params[07].AsString  := QrNiv3NO_SGr.Value;
        DModG.QrUpdPID1.Params[08].AsString  := QrNiv3NO_Gru.Value;
        DModG.QrUpdPID1.Params[09].AsString  := QrNiv3NO_Cjt.Value;
        DModG.QrUpdPID1.Params[10].AsString  := QrNiv3NO_Pla.Value;

        DModG.QrUpdPID1.Params[11].AsInteger := QrNiv3OR_Cta.Value;
        DModG.QrUpdPID1.Params[12].AsInteger := QrNiv3OR_Sgr.Value;
        DModG.QrUpdPID1.Params[13].AsInteger := QrNiv3OR_Gru.Value;
        DModG.QrUpdPID1.Params[14].AsInteger := QrNiv3OR_Cjt.Value;
        DModG.QrUpdPID1.Params[15].AsInteger := QrNiv3OR_Pla.Value;

        DModG.QrUpdPID1.Params[16].AsFloat   := SdoAnt;
        DModG.QrUpdPID1.Params[17].AsFloat   := Credito;
        DModG.QrUpdPID1.Params[18].AsFloat   := Debito;
        DModG.QrUpdPID1.Params[19].AsFloat   := SdoFim;

        DModG.QrUpdPID1.Params[20].AsInteger := QrNiv3Genero.Value;
        DModG.QrUpdPID1.Params[21].AsInteger := QrNiv3Subgrupo.Value;
        DModG.QrUpdPID1.Params[22].AsInteger := QrNiv3Grupo.Value;
        DModG.QrUpdPID1.Params[23].AsInteger := QrNiv3Conjunto.Value;
        DModG.QrUpdPID1.Params[24].AsInteger := QrNiv3Plano.Value;

        DModG.QrUpdPID1.Params[25].AsInteger := ID_SEQ;
        DModG.QrUpdPID1.Params[26].AsInteger := 3;
        //
        DModG.QrUpdPID1.ExecSQL;
      end;
      QrNiv3.Next;
    end;
  end;

  // CONJUNTOS

  QrNiv4.Close;
  QrNiv4.Params[00].AsInteger := CliInt;
  QrNiv4.Params[01].AsInteger := CliInt;
  QrNiv4.Params[02].AsInteger := Mez;
  QrNiv4.Open;
  if QrNiv4.RecordCount > 0 then
  begin
    ID_SEQ := ID_SEQ + 1;
    QrNivMov.Close;
    QrNivMov.SQL.Clear;
    QrNivMov.SQL.Add('SELECT SUM(mov.Credito) Credito, SUM(mov.Debito) Debito');
    QrNivMov.SQL.Add('FROM contasmov mov');
    QrNivMov.SQL.Add('WHERE mov.CliInt=' + dmkPF.FFP(CliInt, 0));
    QrNivMov.SQL.Add('AND mov.Mez=' + dmkPF.FFP(Mez, 0));
    QrNivMov.SQL.Add('AND Conjunto=:P0');
    while not QrNiv4.Eof do
    begin
      QrSdoPar.Close;
      QrSdoPar.SQL.Clear;
      QrSdoPar.SQL.Add('SELECT SUM(SdoAnt) SdoAnt, SUM(Credito) Credito, ');
      QrSdoPar.SQL.Add('SUM(Debito) Debito, SUM(Movim) Movim ');
      QrSdoPar.SQL.Add('FROM saldosniv WHERE CO_Cjt=:P0;');
      QrSdoPar.Params[00].AsInteger := QrNiv4Conjunto.Value;
      QrSdoPar.Open;
      //
      QrNivMov.Close;
      QrNivMov.Params[0].AsInteger := QrNiv4Conjunto.Value;
      QrNivMov.Open;
      SdoAnt  := QrNiv4Movim.Value   - QrNivMovCredito.Value +
                 QrNivMovDebito.Value  - QrSdoParSdoAnt.Value;
      Credito := QrNivMovCredito.Value - QrSdoParCredito.Value;
      Debito  := QrNivMovDebito.Value  - QrSdoParDebito.Value;
      SdoFim  := QrNiv4Movim.Value   - QrSdoParMovim.Value;
      //
      if (Credito <> 0) or (Debito <> 0) or (SdoAnt <> 0) or (SdoFim <> 0) then
      begin
        DModG.QrUpdPID1.Params[00].AsInteger := 1;

        DModG.QrUpdPID1.Params[01].AsInteger := QrNiv4Genero.Value;
        DModG.QrUpdPID1.Params[02].AsInteger := QrNiv4Subgrupo.Value;
        DModG.QrUpdPID1.Params[03].AsInteger := QrNiv4Grupo.Value;
        DModG.QrUpdPID1.Params[04].AsInteger := QrNiv4Conjunto.Value;
        DModG.QrUpdPID1.Params[05].AsInteger := QrNiv4Plano.Value;

        DModG.QrUpdPID1.Params[06].AsString  := QrNiv4NO_Cta.Value;
        DModG.QrUpdPID1.Params[07].AsString  := QrNiv4NO_SGr.Value;
        DModG.QrUpdPID1.Params[08].AsString  := QrNiv4NO_Gru.Value;
        DModG.QrUpdPID1.Params[09].AsString  := QrNiv4NO_Cjt.Value;
        DModG.QrUpdPID1.Params[10].AsString  := QrNiv4NO_Pla.Value;

        DModG.QrUpdPID1.Params[11].AsInteger := QrNiv4OR_Cta.Value;
        DModG.QrUpdPID1.Params[12].AsInteger := QrNiv4OR_Sgr.Value;
        DModG.QrUpdPID1.Params[13].AsInteger := QrNiv4OR_Gru.Value;
        DModG.QrUpdPID1.Params[14].AsInteger := QrNiv4OR_Cjt.Value;
        DModG.QrUpdPID1.Params[15].AsInteger := QrNiv4OR_Pla.Value;

        DModG.QrUpdPID1.Params[16].AsFloat   := SdoAnt;
        DModG.QrUpdPID1.Params[17].AsFloat   := Credito;
        DModG.QrUpdPID1.Params[18].AsFloat   := Debito;
        DModG.QrUpdPID1.Params[19].AsFloat   := SdoFim;

        DModG.QrUpdPID1.Params[20].AsInteger := QrNiv4Genero.Value;
        DModG.QrUpdPID1.Params[21].AsInteger := QrNiv4Subgrupo.Value;
        DModG.QrUpdPID1.Params[22].AsInteger := QrNiv4Grupo.Value;
        DModG.QrUpdPID1.Params[23].AsInteger := QrNiv4Conjunto.Value;
        DModG.QrUpdPID1.Params[24].AsInteger := QrNiv4Plano.Value;

        DModG.QrUpdPID1.Params[25].AsInteger := ID_SEQ;
        DModG.QrUpdPID1.Params[26].AsInteger := 4;
        //
        DModG.QrUpdPID1.ExecSQL;
      end;
      QrNiv4.Next;
    end;
  end;

  // PLANOS

  QrNiv5.Close;
  QrNiv5.Params[00].AsInteger := CliInt;
  QrNiv5.Params[01].AsInteger := CliInt;
  QrNiv5.Params[02].AsInteger := Mez;
  QrNiv5.Open;
  if QrNiv5.RecordCount > 0 then
  begin
    ID_SEQ := ID_SEQ + 1;
    QrNivMov.Close;
    QrNivMov.SQL.Clear;
    QrNivMov.SQL.Add('SELECT SUM(mov.Credito) Credito, SUM(mov.Debito) Debito');
    QrNivMov.SQL.Add('FROM contasmov mov');
    QrNivMov.SQL.Add('WHERE mov.CliInt=' + dmkPF.FFP(CliInt, 0));
    QrNivMov.SQL.Add('AND mov.Mez=' + dmkPF.FFP(Mez, 0));
    QrNivMov.SQL.Add('AND Plano=:P0');
    while not QrNiv5.Eof do
    begin
      QrSdoPar.Close;
      QrSdoPar.SQL.Clear;
      QrSdoPar.SQL.Add('SELECT SUM(SdoAnt) SdoAnt, SUM(Credito) Credito, ');
      QrSdoPar.SQL.Add('SUM(Debito) Debito, SUM(Movim) Movim ');
      QrSdoPar.SQL.Add('FROM saldosniv WHERE CO_Pla=:P0;');
      QrSdoPar.Params[00].AsInteger := QrNiv5Plano.Value;
      QrSdoPar.Open;
      //
      QrNivMov.Close;
      QrNivMov.Params[0].AsInteger := QrNiv5Plano.Value;
      QrNivMov.Open;
      SdoAnt  := QrNiv5Movim.Value   - QrNivMovCredito.Value +
                 QrNivMovDebito.Value  - QrSdoParSdoAnt.Value;
      Credito := QrNivMovCredito.Value - QrSdoParCredito.Value;
      Debito  := QrNivMovDebito.Value  - QrSdoParDebito.Value;
      SdoFim  := QrNiv5Movim.Value   - QrSdoParMovim.Value;
      //
      if (Credito <> 0) or (Debito <> 0) or (SdoAnt <> 0) or (SdoFim <> 0) then
      begin
        DModG.QrUpdPID1.Params[00].AsInteger := 1;

        DModG.QrUpdPID1.Params[01].AsInteger := QrNiv5Genero.Value;
        DModG.QrUpdPID1.Params[02].AsInteger := QrNiv5Subgrupo.Value;
        DModG.QrUpdPID1.Params[03].AsInteger := QrNiv5Grupo.Value;
        DModG.QrUpdPID1.Params[04].AsInteger := QrNiv5Conjunto.Value;
        DModG.QrUpdPID1.Params[05].AsInteger := QrNiv5Plano.Value;

        DModG.QrUpdPID1.Params[06].AsString  := QrNiv5NO_Cta.Value;
        DModG.QrUpdPID1.Params[07].AsString  := QrNiv5NO_SGr.Value;
        DModG.QrUpdPID1.Params[08].AsString  := QrNiv5NO_Gru.Value;
        DModG.QrUpdPID1.Params[09].AsString  := QrNiv5NO_Cjt.Value;
        DModG.QrUpdPID1.Params[10].AsString  := QrNiv5NO_Pla.Value;

        DModG.QrUpdPID1.Params[11].AsInteger := QrNiv5OR_Cta.Value;
        DModG.QrUpdPID1.Params[12].AsInteger := QrNiv5OR_Sgr.Value;
        DModG.QrUpdPID1.Params[13].AsInteger := QrNiv5OR_Gru.Value;
        DModG.QrUpdPID1.Params[14].AsInteger := QrNiv5OR_Cjt.Value;
        DModG.QrUpdPID1.Params[15].AsInteger := QrNiv5OR_Pla.Value;

        DModG.QrUpdPID1.Params[16].AsFloat   := SdoAnt;
        DModG.QrUpdPID1.Params[17].AsFloat   := Credito;
        DModG.QrUpdPID1.Params[18].AsFloat   := Debito;
        DModG.QrUpdPID1.Params[19].AsFloat   := SdoFim;

        DModG.QrUpdPID1.Params[20].AsInteger := QrNiv5Genero.Value;
        DModG.QrUpdPID1.Params[21].AsInteger := QrNiv5Subgrupo.Value;
        DModG.QrUpdPID1.Params[22].AsInteger := QrNiv5Grupo.Value;
        DModG.QrUpdPID1.Params[23].AsInteger := QrNiv5Conjunto.Value;
        DModG.QrUpdPID1.Params[24].AsInteger := QrNiv5Plano.Value;

        DModG.QrUpdPID1.Params[25].AsInteger := ID_SEQ;
        DModG.QrUpdPID1.Params[26].AsInteger := 5;
        //
        DModG.QrUpdPID1.ExecSQL;
      end;
      QrNiv5.Next;
    end;
  end;

  //

  QrSaldosNiv.Close;
  QrSaldosNiv.SQL.Clear;
  QrSaldosNiv.SQL.Add('SELECT * FROM saldosniv');
  QrSaldosNiv.SQL.Add('ORDER BY OL_Pla, NO_Pla, OL_Cjt, NO_Cjt, OL_Gru, ');
  QrSaldosNiv.SQL.Add('NO_Gru, OL_SGr, NO_SGr, OL_Cta, NO_Cta;');
  QrSaldosNiv.Open;
end;

(*
function TDmodFin.SaldosNiveisPeriodo_1(Entidade, Periodo, Controla: Integer;
TodosGeneros: Boolean; PB1, PB2, PB3, PB4, PB5: TProgressBar): Boolean;
var
  i: integer;
  NomeNi, Tab, Ord: String;
  PB: TProgressBar;
begin
  NomeNi := '';
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('DELETE FROM sdoniveis');
  DModG.QrUpdPID1.ExecSQL;
  // informar o saldo total mesmo que n�o informe os saldos dos n�veis
  QrSTCP.Close;
  QrSTCP.Params[0].AsInteger := Entidade;
  QrSTCP.Params[1].AsInteger := Periodo;
  QrSTCP.Open;
  // QrClienteTemContasNiv
  QrCTCN.Close;
  QrCTCN.Params[0].AsInteger := Entidade;
  QrCTCN.Open;
  //
  if DmodFin.QrCTCN.RecordCount = 0 then
  begin
    Application.MessageBox(PChar('O Cliente interno selecionado n�o possui ' +
    'nenhum cadastro de n�veis de contas controlados!' + #13+#10 + 'Para ' +
    'configurar, clique no bot�o "Config." (caso haja no formul�rio atual).' +
    'Ou selecione no menu geral: Cadastros > Financeiros > Plano de contas > ' +
    'N�veis de saldos (Config.)'), 'Aviso', MB_OK+MB_ICONWARNING);
    Result := False;
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    for i := 1 to 5 do
    begin
      case i of
        1: PB := PB1;
        2: PB := PB2;
        3: PB := PB3;
        4: PB := PB4;
        5: PB := PB5;
        else PB := nil;
      end;
      if PB <> nil then
        PB.Position := 0;
    end;
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO sdoniveis SET');
    DModG.QrUpdPID1.SQL.Add('Nivel=:P00, ');
    DModG.QrUpdPID1.SQL.Add('Genero=:P01, ');
    DModG.QrUpdPID1.SQL.Add('NomeGe=:P02, ');
    DModG.QrUpdPID1.SQL.Add('NomeNi=:P03, ');
    DModG.QrUpdPID1.SQL.Add('SumMov=:P04, ');
    DModG.QrUpdPID1.SQL.Add('SdoAnt=:P05, ');
    DModG.QrUpdPID1.SQL.Add('SumCre=:P06, ');
    DModG.QrUpdPID1.SQL.Add('SumDeb=:P07, ');
    DModG.QrUpdPID1.SQL.Add('SdoFim=:P08, ');
    DModG.QrUpdPID1.SQL.Add('CodPla=:P09, ');
    DModG.QrUpdPID1.SQL.Add('CodCjt=:P10, ');
    DModG.QrUpdPID1.SQL.Add('CodGru=:P11, ');
    DModG.QrUpdPID1.SQL.Add('CodSgr=:P12, ');
    DModG.QrUpdPID1.SQL.Add('Ordena=:P13 ');


    {
    DModG.QrUpdPID1.SQL.Add('Ctrla =:P09, ');
    DModG.QrUpdPID1.SQL.Add('Seleci=:P10 ');
    }
    // N�veis
    for i := 1 to 5 do
    begin
      case i of
        1: Tab := 'Cta';
        2: Tab := 'Sgr';
        3: Tab := 'Gru';
        4: Tab := 'Cjt';
        5: Tab := 'Pla';
        else Tab := '???';
      end;
      case I of
        1: NomeNi := 'Conta';
        2: NomeNi := 'Sub-grupo';
        3: NomeNi := 'Grupo';
        4: NomeNi := 'Conjunto';
        5: NomeNi := 'Plano';
        else NomeNi := '? ? ?';
      end;
      QrSNC.Close;
      QrSNC.SQL.Clear;
      QrSNC.SQL.Add('SELECT ' + Tab + '.Codigo,  ' + Tab + '.Nome NOME, ');
      QrSNC.SQL.Add('SUM(his.SumCre-his.SumDeb) SUMMOV,');
      QrSNC.SQL.Add('SUM(his.SdoAnt) SDOANT, SUM(his.SumCre) SUMCRE,');
      QrSNC.SQL.Add('SUM(his.SumDeb) SUMDEB, SUM(his.SdoFim) SDOFIM,');
      QrSNC.SQL.Add('pla.Codigo CodPla, cjt.Codigo CodCjt, ');
      QrSNC.SQL.Add('gru.Codigo CodGru, sgr.Codigo CodSgr ');
      QrSNC.SQL.Add('FROM contashis his');
      QrSNC.SQL.Add('LEFT JOIN contas    cta ON cta.Codigo=his.Codigo');
      QrSNC.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo');
      QrSNC.SQL.Add('LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo');
      QrSNC.SQL.Add('LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto');
      QrSNC.SQL.Add('LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano');
      if not TodosGeneros then
        QrSNC.SQL.Add('LEFT JOIN contasniv niv ON ' + Tab + '.Codigo=niv.Genero');
      QrSNC.SQL.Add('WHERE his.Entidade=' + FormatFloat('0', Entidade));
      QrSNC.SQL.Add('AND his.Periodo=' + FormatFloat('0', Periodo));
      if TodosGeneros then
        QrSNC.SQL.Add('AND ' + Tab + '.CtrlaSdo>' + FormatFloat('0', Controla))
      else
        QrSNC.SQL.Add('AND niv.Nivel=' + IntToStr(i) +
        ' AND niv.Entidade=' + FormatFloat('0', Entidade));
      QrSNC.SQL.Add('');
      QrSNC.SQL.Add('GROUP BY ' + Tab + '.Codigo');
      QrSNC.SQL.Add('ORDER BY ' + Tab + '.OrdemLista, ' + Tab + '.Nome');
      //
      QrSNC.Open;
      case i of
        1: PB := PB1;
        2: PB := PB2;
        3: PB := PB3;
        4: PB := PB4;
        5: PB := PB5;
        else PB := nil;
      end;
      if PB <> nil then
      begin
        PB.Max := QrSnc.RecordCount;
        if PB.Max = 0 then PB.Max := 1;
      end;
      QrSNC.First;
      while not QrSNC.Eof do
      begin
        if PB <> nil then
          PB.Position := PB.Position + 1;
        Ord := FormatFloat('0', QrSNCCodPla.Value);
        if i < 5 then Ord := Ord + '.' + FormatFloat('00', QrSNCCodCjt.Value);
        if i < 4 then Ord := Ord + '.' + FormatFloat('000', QrSNCCodGru.Value);
        if i < 3 then Ord := Ord + '.' + FormatFloat('0000', QrSNCCodSgr.Value);
        if i < 2 then Ord := Ord + '.' + FormatFloat('00000', QrSNCCodigo.Value);
        DModG.QrUpdPID1.Params[00].AsInteger := i; // n�vel
        DModG.QrUpdPID1.Params[01].AsInteger := QrSNCCodigo.Value;
        DModG.QrUpdPID1.Params[02].AsString  := QrSNCNOME.Value;
        DModG.QrUpdPID1.Params[03].AsString  := NomeNi; // Precisa?
        DModG.QrUpdPID1.Params[04].AsFloat   := QrSNCSUMMOV.Value;
        DModG.QrUpdPID1.Params[05].AsFloat   := QrSNCSDOANT.Value;
        DModG.QrUpdPID1.Params[06].AsFloat   := QrSNCSUMCRE.Value;
        DModG.QrUpdPID1.Params[07].AsFloat   := QrSNCSUMDEB.Value;
        DModG.QrUpdPID1.Params[08].AsFloat   := QrSNCSDOFIM.Value;
        DModG.QrUpdPID1.Params[09].AsInteger := QrSNCCodPla.Value;
        DModG.QrUpdPID1.Params[10].AsInteger := QrSNCCodCjt.Value;
        DModG.QrUpdPID1.Params[11].AsInteger := QrSNCCodGru.Value;
        DModG.QrUpdPID1.Params[12].AsInteger := QrSNCCodSgr.Value;
        DModG.QrUpdPID1.Params[13].AsString  := Ord;

        DModG.QrUpdPID1.ExecSQL;
        //
        QrSNC.Next;
      end;
      if PB <> nil then
        PB.Position := PB.Max;
    end;
    QrSNG.Close;
    QrSNG.DataBase := DMod.MyLocDatabase;
    QrSNG.Open;
    Result := True;
    if PB1 <> nil then
      PB1.Position := 0;
    if PB2 <> nil then
      PB2.Position := 0;
    if PB3 <> nil then
      PB3.Position := 0;
    if PB4 <> nil then
      PB4.Position := 0;
    if PB5 <> nil then
      PB5.Position := 0;
  finally
    Screen.Cursor := crDefault;
  end;
end;
*)

function TDmodFin.SaldosNiveisPeriodo_2(Entidade, Periodo, Controla: Integer;
TodosGeneros: Boolean; PB1, PB2, PB3, PB4, PB5: TProgressBar;
QrToOpen: TmySQLQuery): Boolean;
var
  i: integer;
  NomeNi, Tab, Ord: String;
  PB: TProgressBar;
begin
  FSdoNiveis := UCriar.RecriaTempTable('sdoniveis', DModG.QrUpdPID1, False);
  //
  NomeNi := '';
  // informar o saldo total mesmo que n�o informe os saldos dos n�veis
  QrSTCP.Close;
  QrSTCP.Params[0].AsInteger := Entidade;
  QrSTCP.Params[1].AsInteger := Periodo;
  QrSTCP.Open;
  // QrClienteTemContasNiv
  QrCTCN.Close;
  QrCTCN.Params[0].AsInteger := Entidade;
  QrCTCN.Open;
  //
  if DmodFin.QrCTCN.RecordCount = 0 then
  begin
    Application.MessageBox(PChar('O Cliente interno selecionado n�o possui ' +
    'nenhum cadastro de n�veis de contas controlados!' + #13+#10 + 'Para ' +
    'configurar, clique no bot�o "Config." (caso haja no formul�rio atual).' +
    'Ou selecione no menu geral: Cadastros > Financeiros > Plano de contas > ' +
    'N�veis de saldos (Config.)'), 'Aviso', MB_OK+MB_ICONWARNING);
    Result := False;
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    for i := 1 to 5 do
    begin
      case i of
        1: PB := PB1;
        2: PB := PB2;
        3: PB := PB3;
        4: PB := PB4;
        5: PB := PB5;
        else PB := nil;
      end;
      if PB <> nil then
        PB.Position := 0;
    end;
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('INSERT INTO sdoniveis SET');
    DmodG.QrUpdPID1.SQL.Add('Nivel=:P00, ');
    DmodG.QrUpdPID1.SQL.Add('Genero=:P01, ');
    DmodG.QrUpdPID1.SQL.Add('NomeGe=:P02, ');
    DmodG.QrUpdPID1.SQL.Add('NomeNi=:P03, ');
    DmodG.QrUpdPID1.SQL.Add('SumMov=:P04, ');
    DmodG.QrUpdPID1.SQL.Add('SdoAnt=:P05, ');
    DmodG.QrUpdPID1.SQL.Add('SumCre=:P06, ');
    DmodG.QrUpdPID1.SQL.Add('SumDeb=:P07, ');
    DmodG.QrUpdPID1.SQL.Add('SdoFim=:P08, ');
    DmodG.QrUpdPID1.SQL.Add('CodPla=:P09, ');
    DmodG.QrUpdPID1.SQL.Add('CodCjt=:P10, ');
    DmodG.QrUpdPID1.SQL.Add('CodGru=:P11, ');
    DmodG.QrUpdPID1.SQL.Add('CodSgr=:P12, ');
    DmodG.QrUpdPID1.SQL.Add('Ordena=:P13 ');


    {
    DmodG.QrUpdPID1.SQL.Add('Ctrla =:P09, ');
    DmodG.QrUpdPID1.SQL.Add('Seleci=:P10 ');
    }
    // N�veis
    for i := 1 to 5 do
    begin
      case i of
        1: Tab := 'Cta';
        2: Tab := 'Sgr';
        3: Tab := 'Gru';
        4: Tab := 'Cjt';
        5: Tab := 'Pla';
        else Tab := '???';
      end;
      case I of
        1: NomeNi := 'Conta';
        2: NomeNi := 'Sub-grupo';
        3: NomeNi := 'Grupo';
        4: NomeNi := 'Conjunto';
        5: NomeNi := 'Plano';
        else NomeNi := '? ? ?';
      end;
      QrSNC.Close;
      QrSNC.SQL.Clear;
      QrSNC.SQL.Add('SELECT ' + Tab + '.Codigo,  ' + Tab + '.Nome NOME, ');
      QrSNC.SQL.Add('SUM(his.SumCre-his.SumDeb) SUMMOV,');
      QrSNC.SQL.Add('SUM(his.SdoAnt) SDOANT, SUM(his.SumCre) SUMCRE,');
      QrSNC.SQL.Add('SUM(his.SumDeb) SUMDEB, SUM(his.SdoFim) SDOFIM,');
      QrSNC.SQL.Add('pla.Codigo CodPla, cjt.Codigo CodCjt, ');
      QrSNC.SQL.Add('gru.Codigo CodGru, sgr.Codigo CodSgr ');
      QrSNC.SQL.Add('FROM contashis his');
      QrSNC.SQL.Add('LEFT JOIN contas    cta ON cta.Codigo=his.Codigo');
      QrSNC.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo');
      QrSNC.SQL.Add('LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo');
      QrSNC.SQL.Add('LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto');
      QrSNC.SQL.Add('LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano');
      if not TodosGeneros then
        QrSNC.SQL.Add('LEFT JOIN contasniv niv ON ' + Tab + '.Codigo=niv.Genero');
      QrSNC.SQL.Add('WHERE his.Entidade=' + FormatFloat('0', Entidade));
      QrSNC.SQL.Add('AND his.Periodo=' + FormatFloat('0', Periodo));
      if TodosGeneros then
        QrSNC.SQL.Add('AND ' + Tab + '.CtrlaSdo>' + FormatFloat('0', Controla))
      else
        QrSNC.SQL.Add('AND niv.Nivel=' + IntToStr(i) +
        ' AND niv.Entidade=' + FormatFloat('0', Entidade));
      QrSNC.SQL.Add('');
      QrSNC.SQL.Add('GROUP BY ' + Tab + '.Codigo');
      QrSNC.SQL.Add('ORDER BY ' + Tab + '.OrdemLista, ' + Tab + '.Nome');
      //
      QrSNC.Open;
      case i of
        1: PB := PB1;
        2: PB := PB2;
        3: PB := PB3;
        4: PB := PB4;
        5: PB := PB5;
        else PB := nil;
      end;
      if PB <> nil then
      begin
        PB.Max := QrSnc.RecordCount;
        if PB.Max = 0 then PB.Max := 1;
      end;
      QrSNC.First;
      while not QrSNC.Eof do
      begin
        if PB <> nil then
          PB.Position := PB.Position + 1;
        Ord := FormatFloat('0', QrSNCCodPla.Value);
        if i < 5 then Ord := Ord + '.' + FormatFloat('00', QrSNCCodCjt.Value);
        if i < 4 then Ord := Ord + '.' + FormatFloat('000', QrSNCCodGru.Value);
        if i < 3 then Ord := Ord + '.' + FormatFloat('0000', QrSNCCodSgr.Value);
        if i < 2 then Ord := Ord + '.' + FormatFloat('00000', QrSNCCodigo.Value);
        DmodG.QrUpdPID1.Params[00].AsInteger := i; // n�vel
        DmodG.QrUpdPID1.Params[01].AsInteger := QrSNCCodigo.Value;
        DmodG.QrUpdPID1.Params[02].AsString  := QrSNCNOME.Value;
        DmodG.QrUpdPID1.Params[03].AsString  := NomeNi; // Precisa?
        DmodG.QrUpdPID1.Params[04].AsFloat   := QrSNCSUMMOV.Value;
        DmodG.QrUpdPID1.Params[05].AsFloat   := QrSNCSDOANT.Value;
        DmodG.QrUpdPID1.Params[06].AsFloat   := QrSNCSUMCRE.Value;
        DmodG.QrUpdPID1.Params[07].AsFloat   := QrSNCSUMDEB.Value;
        DmodG.QrUpdPID1.Params[08].AsFloat   := QrSNCSDOFIM.Value;
        DmodG.QrUpdPID1.Params[09].AsInteger := QrSNCCodPla.Value;
        DmodG.QrUpdPID1.Params[10].AsInteger := QrSNCCodCjt.Value;
        DmodG.QrUpdPID1.Params[11].AsInteger := QrSNCCodGru.Value;
        DmodG.QrUpdPID1.Params[12].AsInteger := QrSNCCodSgr.Value;
        DmodG.QrUpdPID1.Params[13].AsString  := Ord;

        DmodG.QrUpdPID1.ExecSQL;
        //
        QrSNC.Next;
      end;
      if PB <> nil then
        PB.Position := PB.Max;
    end;
    QrToOpen.Close;
    QrToOpen.DataBase := DModG.MyPID_DB;
    QrToOpen.Open;
    //
    Result := True;
    if PB1 <> nil then
      PB1.Position := 0;
    if PB2 <> nil then
      PB2.Position := 0;
    if PB3 <> nil then
      PB3.Position := 0;
    if PB4 <> nil then
      PB4.Position := 0;
    if PB5 <> nil then
      PB5.Position := 0;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TDmodFin.QrSaldoACalcFields(DataSet: TDataSet);
begin
  QrSaldoATOTAL.Value := QrSaldoAInicial.Value + QrSaldoASALDO.Value;
end;

procedure TDmodFin.QrSdoCtasCalcFields(DataSet: TDataSet);
begin
  QrSdoCtasSDO_INI.Value := QrSdoCtasSDO_CAD.Value + QrSdoCtasSDO_ANT.Value;
  QrSdoCtasSDO_FIM.Value := QrSdoCtasSDO_INI.Value +
                            QrSdoCtasMOV_CRE.Value - QrSdoCtasMOV_DEB.Value +
                            QrSdoCtasTRF_CRE.Value - QrSdoCtasTRF_DEB.Value;
end;

procedure TDmodFin.QrSdoExclCalcFields(DataSet: TDataSet);
begin
  QrSdoExclSDO_INI.Value := QrSdoExclSDO_CAD.Value + QrSdoExclSDO_ANT.Value;
  QrSdoExclSDO_FIM.Value := QrSdoExclSDO_INI.Value +
                            QrSdoExclMOV_CRE.Value - QrSdoExclMOV_DEB.Value +
                            QrSdoExclTRF_CRE.Value - QrSdoExclTRF_DEB.Value;
end;

procedure TDmodFin.QrSNGCalcFields(DataSet: TDataSet);
begin
  QrSNGCODIGOS_TXT.Value := FormatFloat('0', QrSNGCodPla.Value);
  if QrSNGNivel.Value < 5 then QrSNGCODIGOS_TXT.Value :=
    QrSNGCODIGOS_TXT.Value + '.' + FormatFloat('00', QrSNGCodCjt.Value);
  if QrSNGNivel.Value < 4 then QrSNGCODIGOS_TXT.Value :=
    QrSNGCODIGOS_TXT.Value + '.' + FormatFloat('000', QrSNGCodGru.Value);
  if QrSNGNivel.Value < 3 then QrSNGCODIGOS_TXT.Value :=
    QrSNGCODIGOS_TXT.Value + '.' + FormatFloat('0000', QrSNGCodSgr.Value);
  if QrSNGNivel.Value < 2 then QrSNGCODIGOS_TXT.Value :=
    QrSNGCODIGOS_TXT.Value + '.' + FormatFloat('00000', QrSNGGenero.Value);
  QrSNGCODIGOS_TXT.Value := QrSNGCODIGOS_TXT.Value + ' - ' +QrSNGNomeGe.Value;
  //
  { N�o � CalcField
  case QrSNGNivel.Value of
    1: QrSNGNomeNi.Value := 'Conta';
    2: QrSNGNomeNi.Value := 'Sub-grupo';
    3: QrSNGNomeNi.Value := 'Grupo';
    4: QrSNGNomeNi.Value := 'Conjunto';
    5: QrSNGNomeNi.Value := 'Plano';
    else QrSNGNomeNi.Value := '? ? ?';
  end;
  }
end;

procedure TDmodFin.QrSomaLinhasCalcFields(DataSet: TDataSet);
begin
  QrSomaLinhasSALDO.Value := QrSomaLinhasCREDITO.Value - QrSomaLinhasDEBITO.Value;
end;

procedure TDmodFin.QrTotalSaldoCalcFields(DataSet: TDataSet);
begin
  if QrTotalSaldoTipo.Value = 1 then
       QrTotalSaldoNOMETIPO.Value := 'Em carteira'
  else QrTotalSaldoNOMETIPO.Value := 'Consignado';
end;

procedure TDmodFin.QrLctosBeforeClose(DataSet: TDataSet);
begin
  VAR_LANCTO := QrLctosControle.Value;
end;

procedure TDmodFin.QrLctosCalcFields(DataSet: TDataSet);
begin
  if QrLctosMes2.Value > 0 then
    QrLctosMENSAL.Value := FormatFloat('00', QrLctosMes2.Value)+'/'
    +Copy(FormatFloat('0000', QrLctosAno.Value), 3, 2)
   else QrLctosMENSAL.Value := CO_VAZIO;
  if QrLctosMes2.Value > 0 then
    QrLctosMENSAL2.Value := FormatFloat('0000', QrLctosAno.Value)+'/'+
    FormatFloat('00', QrLctosMes2.Value)+'/01'
   else QrLctosMENSAL2.Value := CO_VAZIO;
  //
  QrLctosNOMESIT.Value := UFinanceiro.NomeSitLancto(QrLctosSit.Value,
    QrLctosTipo.Value, QrLctosPrazo.Value, QrLctosVencimento.Value,
    QrLctosReparcel.Value);
  //
  case QrLctosSit.Value of
    0: QrLctosSALDO.Value := QrLctosCredito.Value - QrLctosDebito.Value;
    1: QrLctosSALDO.Value := (QrLctosCredito.Value - QrLctosDebito.Value) - QrLctosPago.Value;
    else QrLctosSALDO.Value := 0;
  end;
  QrLctosNOMERELACIONADO.Value := CO_VAZIO;
  if QrLctoscliente.Value <> 0 then
  QrLctosNOMERELACIONADO.Value := QrLctosNOMECLIENTE.Value;
  if QrLctosFornecedor.Value <> 0 then
  QrLctosNOMERELACIONADO.Value := QrLctosNOMERELACIONADO.Value +
  QrLctosNOMEFORNECEDOR.Value;
  //
  if QrLctosVencimento.Value > Date then QrLctosATRASO.Value := 0
    else QrLctosATRASO.Value := Date - QrLctosVencimento.Value;
  //
  QrLctosJUROS.Value :=
    Trunc(QrLctosATRASO.Value * QrLctosMoraDia.Value * 100)/100;
  //
  if QrLctosCompensado.Value = 0 then
     QrLctosCOMPENSADO_TXT.Value := '' else
     QrLctosCOMPENSADO_TXT.Value :=
     FormatDateTime(VAR_FORMATDATE3, QrLctosCompensado.Value);
  QrLctosSERIE_CHEQUE.Value := QrLctosSerieCH.Value +
    FormatFloat('000000;-0; ', QrLctosDocumento.Value);
end;

procedure TDmodFin.QrPendGCalcFields(DataSet: TDataSet);
begin
  if QrPendGMez.Value > 0 then QrPendGNOMEMEZ.Value :=
    MLAGeral.MezToFDT(QrPendGMez.Value, 0, 104)
  else QrPendGNOMEMEZ.Value := '';
end;

procedure TDmodFin.QrPgBloqCalcFields(DataSet: TDataSet);
begin
  //QrPgBloqMES.Value := dmkPF.MezToMesEAno(QrPgBloqMez.Value);
end;

procedure TDmodFin.QrRepAbeCalcFields(DataSet: TDataSet);
begin
  QrRepAbeCompensado_TXT.Value := dmkPF.FDT_NULO(QrRepAbeCompensado.Value, 12);
end;

procedure TDmodFin.QrRepAfterScroll(DataSet: TDataSet);
begin
  QrRepBPP.Close;
  QrRepBPP.Params[0].AsInteger := QrRepCodigo.Value;
  QrRepBPP.Open;
  //
  QrRepori.Close;
  QrRepori.Params[0].AsInteger := QrRepCodigo.Value;
  QrRepori.Open;
  //
  QrReplan.Close;
  QrReplan.Params[0].AsInteger := QrRepCodigo.Value;
  QrReplan.Open;
  //
end;

procedure TDmodFin.QrResumoCalcFields(DataSet: TDataSet);
begin
  QrResumoSALDO.Value := QrResumoCredito.Value + QrResumoDebito.Value;
  QrResumoFINAL.Value := QrResumoSALDO.Value + QrSaldoATOTAL.Value;
end;

procedure TDmodFin.QrCartsAfterClose(DataSet: TDataSet);
begin
  QrLctos.Close;
end;

procedure TDmodFin.QrCartsAfterScroll(DataSet: TDataSet);
var
  Controle, Sub: Integer;
begin
  Controle := 0;
  Sub := 1;
  if QrLctos.State = dsBrowse then
  begin
    Controle := QrLctosControle.Value;
    Sub := QrLctosSub.Value;
  end;
  ReabreSoLct(QrLctos, (*QrCartsTipo.Value*,*) QrCartsCodigo.Value,
    Controle, Sub, FTPDataIni, FTPDataFim, Null);
end;

procedure TDmodFin.QrCartsBeforeClose(DataSet: TDataSet);
begin
  QrLctos.Close;
end;

procedure TDmodFin.QrCartsCalcFields(DataSet: TDataSet);
begin
  QrCartsDIFERENCA.Value :=
    QrCartsEmCaixa.Value - QrCartsSaldo.Value;
  case QrCartsPrazo.Value of
    0: QrCartsTIPOPRAZO.Value := 'F';
    1: QrCartsTIPOPRAZO.Value := 'V';
  end;
  case QrCartsPagRec.Value of
   -1: QrCartsNOMEPAGREC.Value := 'CONTAS A PAGAR';
    0: QrCartsNOMEPAGREC.Value := 'CONTAS A PAGAR E RECEBER';
    1: QrCartsNOMEPAGREC.Value := 'CONTAS A RECEBER';
  end;
end;

procedure TDmodFin.AtualizaContasHistSdo3(CliInt: Integer; LaAviso: TLabel);
begin
  Screen.Cursor := crHourGlass;
  //
  MyObjects.Informa(LaAviso, True, 'Consultando itens orf�os');
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT COUNT(*) Itens');
  Dmod.QrAux.SQL.Add('FROM ' + VAR_LCT + '');
  Dmod.QrAux.SQL.Add('WHERE Genero=-5');
  Dmod.QrAux.Open;
  if Dmod.QrAux.FieldByName('Itens').AsInteger > 0 then
  begin
    if DBCheck.CriaFm(TFmContasHistAtz, FmContasHistAtz, afmoLiberado) then
    begin
      with FmContasHistAtz do
      begin
        Show;
        QrOrfaos.Close;
        QrOrfaos.Open;
        //
        ConsertaLancamento();
        if QrOrfaos.RecordCount = 0 then
          Destroy
        else begin
          LaAviso.Caption := 'ERRO... ' + FormatFloat('0', QrOrfaos.RecordCount) +
          ' registros n�o puderam ser corrigidos.';
          PainelConfirma.Visible := True;
        end;
      end;
    end;
  end;
  //
  //
  MyObjects.Informa(LaAviso, True, 'Atualizando tabela de saldos de contas');
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO contassdo');
  Dmod.QrUpd.SQL.Add('SELECT Codigo, :P0 Entidade, 0 SdoIni,');
  Dmod.QrUpd.SQL.Add('0 SdoAtu, 0 SdoFut, 0 Lk, Date(SYSDATE()) DataCad,');
  Dmod.QrUpd.SQL.Add('0 DataAlt, :P1 UserCad, 0 UserAlt, 0 PerAnt, 0 PerAtu, ');
  Dmod.QrUpd.SQL.Add('"" Texto, 0 info, 1 AlterWeb, 1 Ativo, 0 ValMorto, 0 ValIniOld');
  Dmod.QrUpd.SQL.Add('FROM contas');
  Dmod.QrUpd.SQL.Add('WHERE Codigo>0');
  Dmod.QrUpd.SQL.Add('ON DUPLICATE KEY UPDATE ');
  Dmod.QrUpd.SQL.Add('DataAlt=Date(SYSDATE()), UserAlt=:P2, AlterWeb=1');
  Dmod.QrUpd.Params[00].AsInteger := CliInt;
  Dmod.QrUpd.Params[01].AsInteger := VAR_USUARIO;
  Dmod.QrUpd.Params[02].AsInteger := VAR_USUARIO;
  Dmod.QrUpd.ExecSQL;
  //
  //
  MyObjects.Informa(LaAviso, True, 'Excluindo itens da tabela de saldos de n�veis');
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM contasmov WHERE CliInt=:P0');
  Dmod.QrUpd.Params[0].AsInteger := CliInt;
  Dmod.QrUpd.ExecSQL;
  //
  //
  MyObjects.Informa(LaAviso, True, 'Inserindo itens na tabela de saldos de n�veis');
  //
  // CUIDADO Inclus�o segue ordem de campos na cria��o da tabela!!!!
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO contasmov');
  Dmod.QrUpd.SQL.Add('SELECT lan.CliInt, lan.Genero,');
  Dmod.QrUpd.SQL.Add('((YEAR(lan.Data)-2000) * 100)+MONTH(lan.Data) MEZ,');
  Dmod.QrUpd.SQL.Add('cta.Subgrupo, sgr.Grupo, gru.Conjunto, cjt.Plano,');
  Dmod.QrUpd.SQL.Add('SUM(lan.Credito) Credito, SUM(lan.Debito) Debito,');
  Dmod.QrUpd.SQL.Add('SUM(lan.Credito-lan.Debito) Movim,');
  Dmod.QrUpd.SQL.Add('0 Lk, Date(SYSDATE()) DataCad, 0 DataAlt,');
  Dmod.QrUpd.SQL.Add(':P0 UserCad, 0 UserAlt, 1 Ativo, 1 AlterWeb');
  // Colocar aqui em sequencia de cria��o os campos novos!!!
  Dmod.QrUpd.SQL.Add('FROM ' + VAR_LCT + ' lan');
  Dmod.QrUpd.SQL.Add('LEFT JOIN contas cta ON cta.Codigo=lan.Genero');
  Dmod.QrUpd.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo');
  Dmod.QrUpd.SQL.Add('LEFT JOIN grupos gru ON gru.Codigo=sgr.Grupo');
  Dmod.QrUpd.SQL.Add('LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto');
  //Dmod.QrUpd.SQL.Add('WHERE lan.Genero > 0');
  //
  //if Uppercase(Application.Title = 'SYNDIC') then
    //Dmod.QrUpd.SQL.Add('AND ((lan.Tipo<>2) OR (lan.Tipo=2 AND FatID=0))');
  //
  Dmod.QrUpd.SQL.Add('WHERE lan.CliInt=:P1');

  //Erro nos saldos das contas, pois nos extratso tem a conta -5 => Outras carteiras proveniente do Tipo 2 (emiss�o)
  Dmod.QrUpd.SQL.Add('AND lan.Tipo <> 2');

  Dmod.QrUpd.SQL.Add('GROUP BY lan.CliInt, lan.Genero, MEZ');
  Dmod.QrUpd.SQL.Add('ORDER BY lan.CliInt, lan.Genero, MEZ');
  Dmod.QrUpd.Params[00].AsInteger := VAR_USUARIO;
  Dmod.QrUpd.Params[01].AsInteger := CliInt;
  Dmod.QrUpd.ExecSQL;
  //
  //
  MyObjects.Informa(LaAviso, False, '...');
  //
  //
  Screen.Cursor := crDefault;
end;

procedure TDmodFin.AtualizaTodasCarteiras(QrCrt: TmySQLQuery);
var
  Carteira: Integer;
begin
  Screen.Cursor := crHourGlass;
  Carteira := QrCrt.FieldByName('Codigo').AsInteger;
  UFinanceiro.AtualizaVencimentos;
  QrCrt.DisableControls;
  QrCrt.First;
  while not QrCrt.Eof do
  begin
    UFinanceiro.RecalcSaldoCarteira(QrCrt.FieldByName('Codigo').AsInteger, nil, False, False);
    QrCrt.Next;
  end;
  QrCrt.Locate('Codigo', Carteira, []);
  QrCrt.EnableControls;
  Screen.Cursor := crDefault;
end;


function TDmodFin.BloquetosDeCobrancaProcessados(Periodo, CliInt: Integer(*;
Acordos, Periodos, Textos: Boolean*)): Boolean;
var
  //DataI, DataF: String;
  Mez: Integer;
begin
  Screen.Cursor := crHourGlass;
{
  DataI := Geral.FDT(MLAGeral.PrimeiroDiaDoPeriodo_Date(Periodo), 1);
  DataF := Geral.FDT(MLAGeral.UltimoDiaDoPeriodo_Date(Periodo), 1);
}
  Mez := MLAGeral.PeriodoToMez(Periodo-1);
  //
  // Verificar erro de soma de pagamento
  // por enquanto estou usando "credito" + "PagJur" + "PagMul" o certo � "Pago"
  QrPgBloq.Close;
  QrPgBloq.SQL.Clear;
  if (Uppercase(Application.Title) = 'SYNDIC') or (UpperCase(Application.Title) = 'SYNKER') then
  begin
    QrPgBloq.SQL.Add('SELECT IF(lan.Compensado=0, 0, IF(car.Tipo=2,');
    QrPgBloq.SQL.Add('lan.Credito + lan.PagMul + lan.PagJur, lan.Credito)) Credito,');
    QrPgBloq.SQL.Add('IF(lan.Compensado=0, 0, IF(car.Tipo=2, lan.PagMul, lan.MultaVal)) MultaVal,');
    QrPgBloq.SQL.Add('IF(lan.Compensado=0, 0, IF(car.Tipo=2, lan.PagJur, lan.MoraVal)) MoraVal,');
    QrPgBloq.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE');
    QrPgBloq.SQL.Add('ent.Nome END NOMEPROPRIET, imv.Unidade UH,');
    QrPgBloq.SQL.Add('con.Nome NOMECONTA, lan.Mez, lan.Vencimento,');
    QrPgBloq.SQL.Add('lan.Documento, lan.Credito ORIGINAL, lan.Compensado Data,');
    QrPgBloq.SQL.Add('IF(lan.Compensado=0, "", DATE_FORMAT(lan.Compensado, "%d/%m/%Y")) DATA_TXT');
    QrPgBloq.SQL.Add('FROM ' + VAR_LCT + ' lan');
    QrPgBloq.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    QrPgBloq.SQL.Add('LEFT JOIN condimov imv ON imv.Conta=lan.Depto');
    QrPgBloq.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=lan.ForneceI');
    QrPgBloq.SQL.Add('LEFT JOIN contas con ON con.Codigo=lan.Genero');
    QrPgBloq.SQL.Add('WHERE car.Tipo in (0,2)');
    QrPgBloq.SQL.Add('AND lan.FatID in (600,601)');
    QrPgBloq.SQL.Add('AND car.ForneceI=' + FormatFloat('0', CliInt));
    QrPgBloq.SQL.Add('AND lan.Mez='+FormatFloat('0', Mez));
    QrPgBloq.SQL.Add('');
    QrPgBloq.SQL.Add('ORDER BY UH, lan.Documento, con.OrdemLista, NOMECONTA');
    QrPgBloq.SQL.Add('');

    (*
    QrPgBloq.Params[00].AsInteger := CliInt;
    QrPgBloq.Params[01].AsInteger := Mez;
    *)
    QrPgBloq.Open;

    //ShowMessage(IntToStr(QrPgBloq.RecordCount));
    //   DEMORADO
    // Mesmo erro acima colocado linha "AND lan.Compensado>0" al�m de
    //  "credito" + "PagJur" + "PagMul" o certo � "Pago"
    // QrSuBloqKGT.Value > KeepGroupTogether
    QrSuBloq.Close;
    QrSuBloq.SQL.Clear;
    QrSuBloq.SQL.Add('SELECT SUM(IF(lan.Compensado=0, 0, IF(car.Tipo=2,');
    QrSuBloq.SQL.Add('lan.Credito + lan.PagMul + lan.PagJur, lan.Credito))) PAGO,');
    QrSuBloq.SQL.Add('SUM(IF(lan.Compensado=0, 0, IF(car.Tipo=2, lan.PagMul, lan.MultaVal))) MultaVal,');
    QrSuBloq.SQL.Add('SUM(IF(lan.Compensado=0, 0, IF(car.Tipo=2, lan.PagJur, lan.MoraVal))) MoraVal,');
    QrSuBloq.SQL.Add('con.Nome NOMECONTA, SUM(lan.Credito) ORIGINAL, 0 KGT');
    QrSuBloq.SQL.Add('FROM ' + VAR_LCT + ' lan');
    QrSuBloq.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    QrSuBloq.SQL.Add('LEFT JOIN condimov imv ON imv.Conta=lan.Depto');
    QrSuBloq.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=lan.ForneceI');
    QrSuBloq.SQL.Add('LEFT JOIN contas con ON con.Codigo=lan.Genero');
    QrSuBloq.SQL.Add('WHERE car.Tipo in (0,2)');
    QrSuBloq.SQL.Add('AND lan.FatID in (600,601)');
    QrSuBloq.SQL.Add('AND car.ForneceI=' + FormatFloat('0', CliInt));
    QrSuBloq.SQL.Add('AND lan.Mez='+FormatFloat('0', Mez));
    QrSuBloq.SQL.Add('');
    QrSuBloq.SQL.Add('GROUP BY NOMECONTA');
    (*
    QrSuBloq.Params[00].AsInteger := CliInt;
    QrSuBloq.Params[01].AsInteger := Mez;
    *)
    QrSuBloq.Open;
    Result := True;
  end else Result := False;
  //
  if Result = False then Geral.MensagemBox(
  'N�o foi poss�vel processar os bloquetos de cobran�a!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Screen.Cursor := crDefault;
end;

procedure TDmodFin.ReabreSoLct(QrLct: TmySQLQuery; (*TipoCarteira,*)
ItemCarteira, Controle, Sub: Integer; TPDataIni, TPDataFim: TDateTimePicker;
Apto: Variant);
var
  Ini, Fim, Campo: String;
  MeuC, MeuS: Integer;
begin
  QrLct.Close;

  // Fazer mais r�pido inclus�es multiplas!
  if VAR_NaoReabrirLct then
    Exit;

  if TPDataIni <> nil then
    VAR_FL_DataIni := TPDataIni.Date;
  if TPDataFim <> nil then
    VAR_FL_DataFim := TPDataFim.Date;
  //
  Ini := FormatDateTime(VAR_FORMATDATE, VAR_FL_DataIni);
  Fim := FormatDateTime(VAR_FORMATDATE, VAR_FL_DataFim);
  //
  QrLct.SQL.Clear;
  QrLct.SQL.Add('SELECT MOD(la.Mez, 100) Mes2,');
  QrLct.SQL.Add('  ((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,');
  if VAR_KIND_DEPTO = kdUH then
    QrLct.SQL.Add('ci.Unidade UH, ')
  else
    QrLct.SQL.Add('"" UH, ');
  QrLct.SQL.Add('la.*, ct.Codigo CONTA, ca.Prazo, ca.Nome NO_Carteira, ');
  QrLct.SQL.Add('ca.Banco1, ca.Agencia1, ca.Conta1,');
  QrLct.SQL.Add('ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,');
  QrLct.SQL.Add('gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,');
  QrLct.SQL.Add('IF(em.Tipo=0, em.RazaoSocial, em.Nome) NOMEEMPRESA,');
  QrLct.SQL.Add('IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE,');
  QrLct.SQL.Add('IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEFORNECEDOR,');
  QrLct.SQL.Add('IF(la.ForneceI=0, "",');
  QrLct.SQL.Add('  IF(fi.Tipo=0, fi.RazaoSocial, fi.Nome)) NOMEFORNECEI,');
  QrLct.SQL.Add('IF(la.Sit<2, la.Credito-la.Debito-(la.Pago*la.Sit), 0) SALDO,');
  QrLct.SQL.Add('IF(la.Cliente>0,');
  QrLct.SQL.Add('  IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome),');
  QrLct.SQL.Add('  IF (la.Fornecedor>0,');
  QrLct.SQL.Add('    IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome), "")) NOMERELACIONADO, ');
  QrLct.SQL.Add('ELT(la.Endossas, "Endossado", "Endossante", "Ambos", "? ? ? ?") NO_ENDOSSADO ');
  QrLct.SQL.Add('FROM ' + VAR_LCT + ' la');
  QrLct.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
  QrLct.SQL.Add('LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = 0');
  QrLct.SQL.Add('LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo');
  QrLct.SQL.Add('LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo');
  QrLct.SQL.Add('LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto');
  QrLct.SQL.Add('LEFT JOIN entidades em ON em.Codigo=ct.Empresa');
  QrLct.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
  QrLct.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor');
  QrLct.SQL.Add('LEFT JOIN entidades fi ON fi.Codigo=la.ForneceI');
  if VAR_KIND_DEPTO = kdUH then
    QrLct.SQL.Add('LEFT JOIN condimov  ci ON ci.Conta=la.Depto');

  case FTipoData of
    1: Campo := 'la.Vencimento';
    2: Campo := 'la.Compensado';
    else Campo := 'la.Data';
  end;
  QrLct.SQL.Add('WHERE ' + Campo + ' BETWEEN "'+Ini+'" AND "'+Fim+'"');
  // est� gerando problemas
  //QrLct.SQL.Add('AND la.Tipo='+IntToStr(TipoCarteira));
  QrLct.SQL.Add('AND la.Carteira='+IntToStr(ItemCarteira));
  //
  if Apto <> Null then
    QrLct.SQL.Add('AND la.Depto='+IntToStr(Apto));
  //
  QrLct.SQL.Add('ORDER BY la.Data, la.Controle');
  QrLct.Open;
  //
  if Controle = -1 then QrLct.Last else
  begin
    if Controle > 0 then
    begin
      MeuC := Controle;
      MeuS := Sub;
    end else begin
      MeuC := VAR_CONTROLE;
      MeuS := 0;
    end;
    if not QrLct.Locate('Controle;Sub', VarArrayOf([MeuC, MeuS]),[]) then
      QrLct.Last;
  end;
end;

procedure TDmodFin.PagarRolarEmissao(QrLct, QrCrt: TmySQLQuery);
var
  Lancto: Integer;
begin
  Lancto := QrLct.FieldByName('Controle').AsInteger;
  if DBCheck.CriaFm(TFmCartPgt2, FmCartPgt2, afmoNegarComAviso) then
  begin
    FmCartPgt2.FQrLct              := QrLct;
    FmCartPgt2.FQrCarteiras        := QrCrt;
    FmCartPgt2.DataSource1.DataSet := QrLct;
    FmCartPgt2.LaControle.Caption := FloatToStr(QrLct.FieldByName('Controle').AsInteger);
    FmCartPgt2.PagtosReopen(0);
    FmCartPgt2.ShowModal;
    FmCartPgt2.Destroy;
  end;
  VAR_LANCTO2 := Lancto;
  UFinanceiro.RecalcSaldoCarteira(QrCrt.FieldByName('Codigo').AsInteger,
    QrCrt, True, True);
  QrLct.Locate('Controle', Lancto, []);
end;

function TDmodFin.AlteraLanctoEsp(Query: TmySQLQuery; Acao: Integer;
EspPercJuros, EspPercMulta: Double;
EspTPDataIni, EspTPDataFim: TDateTimePicker; EspDepto: Variant;
Finalidade: TIDFinalidadeLct): Integer;
var
  Tipo, Genero, Sit, Cartao, NF, Carteira, Depto, CNAB_Sit, ID_Pgto,
  Fornecedor, Cliente, CliInt, ForneceI, Vendedor, Account, TipoCH,
  FatID, FatID_Sub, FatNum, FatParcela, Nivel, CtrlIni, DescoPor,
  Unidade, Sub: Integer;
  Controle: Int64;
  Qtde, Credito, Debito, Documento, MoraDia, Multa, ICMS_P, ICMS_V, MultaVal,
  DescoVal, NFVal, MoraVal: Double;
  Data, DataDoc, Vencimento, Compensado: TDateTime;
  Mensal, Descricao, Duplicata, SerieCH, Doc2: String;
  SoTestar: Boolean;
  IDFinalidade: Integer;
begin
  // fazer por FLAN_
  Tipo        := Query.FieldByName('Tipo').AsInteger;// Dmod.QrLctTipo.Value;
  Genero      := Query.FieldByName('Genero').AsInteger;// Dmod.QrLctGenero.Value;
  Sit         := Query.FieldByName('Sit').AsInteger;// Dmod.QrLctSit.Value;
  Cartao      := Query.FieldByName('Cartao').AsInteger;// Dmod.QrLctCartao.Value;
  NF          := Query.FieldByName('NotaFiscal').AsInteger;// Dmod.QrLctNotaFiscal.Value;
  Carteira    := Query.FieldByName('Carteira').AsInteger;// Dmod.QrCrtCodigo.Value;
  Fornecedor  := Query.FieldByName('Fornecedor').AsInteger;// Dmod.QrLctFornecedor.Value;
  Cliente     := Query.FieldByName('Cliente').AsInteger;// Dmod.QrLctCliente.Value;
  CliInt      := Query.FieldByName('CliInt').AsInteger;// Dmod.QrLctCliInt.Value;
  ForneceI    := Query.FieldByName('ForneceI').AsInteger;// Dmod.QrLctForneceI.Value;
  Vendedor    := Query.FieldByName('Vendedor').AsInteger;// Dmod.QrLctVendedor.Value;
  Account     := Query.FieldByName('Account').AsInteger;// Dmod.QrLctAccount.Value;
  Depto       := Query.FieldByName('Depto').AsInteger;// Dmod.QrLctAccount.Value;
  //
  Controle    := Query.FieldByName('Controle').AsInteger;// Dmod.QrLctControle.Value;
  //
  Sub         := Query.FieldByName('Sub').AsInteger;// Dmod.QrLctSub.Value;
  //
  Qtde        := Query.FieldByName('Qtde').AsFloat;
  Credito     := Query.FieldByName('Credito').AsFloat;//Dmod.QrLctCredito.Value;
  Debito      := Query.FieldByName('Debito').AsFloat;// Dmod.QrLctDebito.Value;
  Documento   := Query.FieldByName('Documento').AsFloat;// Dmod.QrLctDocumento.Value;
  MoraDia     := Query.FieldByName('MoraDia').AsFloat;// Dmod.QrLctMoraDia.Value;
  Multa       := Query.FieldByName('Multa').AsFloat;// Dmod.QrLctMulta.Value;
  MultaVal    := Query.FieldByName('MultaVal').AsFloat;// Dmod.QrLctMultaVal.Value;
  MoraVal     := Query.FieldByName('MoraVal').AsFloat;// Dmod.QrLctMoraVal.Value;
  ICMS_P      := Query.FieldByName('ICMS_P').AsFloat;// Dmod.QrLctICMS_P.Value;
  ICMS_V      := Query.FieldByName('ICMS_V').AsFloat;// Dmod.QrLctICMS_V.Value;
  //
  Data        := Query.FieldByName('Data').AsDateTime;// Dmod.QrLctData.Value;
  DataDoc     := Query.FieldByName('DataDoc').AsDateTime;// Dmod.QrLctDataDoc.Value;
  Vencimento  := Query.FieldByName('Vencimento').AsDateTime;// Dmod.QrLctVencimento.Value;
  Compensado  := Query.FieldByName('Compensado').AsDateTime;// Dmod.QrLctCompensado.Value;
  //
  Mensal      := Query.FieldByName('MENSAL').AsString;// Dmod.QrLctMENSAL.Value;
  Descricao   := Query.FieldByName('Descricao').AsString;// Dmod.QrLctDescricao.Value;
  Duplicata   := Query.FieldByName('Duplicata').AsString;// Dmod.QrLctDuplicata.Value;
  SerieCH     := Query.FieldByName('SerieCH').AsString;// Dmod.QrLctSerieCH.Value;
  //
  CNAB_Sit    := Query.FieldByName('CNAB_Sit').AsInteger;// Dmod.QrLctCNAB_Sit.Value;
  ID_Pgto     := Query.FieldByName('ID_Pgto').AsInteger;// Dmod.QrLctID_Pgto.Value;
  TipoCH      := Query.FieldByName('TipoCH').AsInteger;// Dmod.QrLctTipoCH.Value;
  //
  FatID       := Query.FieldByName('FatID').AsInteger;// Dmod.QrLctFatID.Value;
  FatID_Sub   := Query.FieldByName('FatID_Sub').AsInteger;// Dmod.QrLctFatID_Sub.Value;
  FatNum      := Query.FieldByName('FatNum').AsInteger;// Dmod.QrLctFatNum.Value;
  FatParcela  := Query.FieldByName('FatParcela').AsInteger;// Dmod.QrLctFatParcela.Value;
  //
  DescoPor    := Query.FieldByName('DescoPor').AsInteger;
  Unidade     := Query.FieldByName('Unidade').AsInteger;
  Nivel       := Query.FieldByName('Nivel').AsInteger;
  CtrlIni     := Query.FieldByName('CtrlIni').AsInteger;
  DescoVal    := Query.FieldByName('DescoVal').AsFloat;
  Doc2        := Query.FieldByName('Doc2').AsString;
  //
  IDFinalidade := Integer(Finalidade);
  if Acao = 0 then SoTestar := True else SoTestar := False;
  Result := CriaFmCartEditEsp(QrCarts, QrLctos,
  Acao, Tipo, Genero, Sit, Cartao, NF, Carteira,
  Fornecedor, Cliente, Vendedor, Account, CliInt, ForneceI, Depto, CNAB_Sit,
  ID_Pgto, TipoCH, Controle, Sub, Credito, Debito, Documento, MoraDia, Multa,
  ICMS_P, ICMS_V, Qtde, MultaVal, MoraVal, Data, Vencimento, DataDoc,
  Compensado, Mensal, Descricao, Duplicata, SerieCH, Query, SoTestar,
  FatID, FatID_Sub, FatNum, FatParcela, Nivel, DescoPor, CtrlIni, Unidade,
  DescoVal, NFVal, Doc2, EspPercJuros, EspPercMulta,
  EspTPDataIni, EspTPDataFim, EspDepto, CliInt, IDFinalidade);
end;

function TDmodFin.CriaFmCartEditEsp(QrCrt, QrLct: TmySQLQuery;
          Altera, Tipo, Genero, Sit, Cartao, NF, Carteira, Fornecedor, Cliente,
          Vendedor, Account, CliInt, ForneceI, Depto, CNAB_Sit, ID_Pgto,
          TipoCH: Integer; Controle: Int64; Sub: Integer;
          Credito, Debito, Documento, MoraDia, Multa, ICMS_P, ICMS_V, Qtde,
          MultaVal, MoraVal: Double;
          Data, Vencimento, DataDoc, Compensado: TDateTime;
          Mensal, Descricao, Duplicata, SerieCH: String;
          Query: TmySQLQuery; SoTestar: Boolean; FatID, FatID_Sub, FatNum,
          FatParcela, Nivel, DescoPor, CtrlIni, Unidade: Integer;
          DescoVal, NFVal: Double; Doc2: String;
          EspPercJuros, EspPercMulta: Double;
          EspTPDataIni, EspTPDataFim: TDateTimePicker;
          EspDepto: Variant;
          EspCodigo, Finalidade: Integer): Integer;
var
  Cursor: TCursor;
  Mes, SQL: String;
  Ano: Integer;
begin
  Result := 0;
  if Altera > 1 then
  if not SoTestar then
    if UMyMod.SelLockInt64Y(Controle, Dmod.MyDB, VAR_LCT, 'Controle') then Exit;
  try
    if Altera > 1 then
    begin
      if Mensal <> CO_VAZIO then
      begin
        Ano := Geral.IMV(Mensal[4]+Mensal[5]);
        if Ano > 90 then Ano := Ano + 1900 else Ano := Ano + 2000;
        Mes := FormatDateTime('yyyy/mm/dd', EncodeDate(Ano,
        Geral.IMV(Mensal[1]+Mensal[2]), 1));
      end else Mes := CO_VAZIO;
      if not SoTestar then
        UMyMod.UpdLockInt64Y(Controle, Dmod.MyDB, VAR_LCT, 'Controle');
    end;
    if Altera = 2 then
    begin
      if Genero < 0 then
      begin
        if SoTestar then Result := -1
        else begin
          case Genero of
            -1: UFinanceiro.CriarTransferCart(1, QrLct, QrCrt, True,
                CliInt, 0, 0, 0);
            else
            begin
              Application.MessageBox(PChar('Lan�amento protegido. Para editar '+
              'este item selecione seu caminho correto!'), 'Erro', MB_OK+MB_ICONERROR);
              Exit;
            end;
          end;
        end;
        Exit;
      end;
      if (Genero = 0) and (Controle = 0) then
      begin
        if SoTestar then Result := -2 else
        Application.MessageBox('N�o h� dados para editar', 'Erro', MB_OK+MB_ICONERROR);
        Exit;
      end;
      if (Cartao > 0) and not VAR_FATURANDO then
      begin
        if SoTestar then Result := -3 else
        Application.MessageBox(PChar('Esta emiss�o n�o pode ser editada pois '+
        'pertence a uma fatura!'), 'Erro', MB_OK+MB_ICONERROR);
        Exit;
      end;
      if (Genero > 0) and (Sub > 0) then
      begin
        Geral.MensagemBox('Esta emiss�o n�o pode ser editada pois '+
        'pertence a transfer�ncia entre contas!', 'Erro', MB_OK+MB_ICONERROR);
        Exit;
      end;
    end;
    if SoTestar then
    begin
      Result := 1;
      Exit;
    end;
    //if UMyMod.AcessoNegadoAoForm('Perfis', 'EditACxas', 0) then Exit;
    if Finalidade = 0 then
    begin
      Application.MessageBox('N�o foi definida nenhuma finalidade!', 'Aviso',
      MB_OK+MB_ICONWARNING);
      Exit;
    end;
    Cursor := Screen.Cursor;
    Screen.Cursor := crHourglass;
    try
      if DBCheck.CriaFm(TFmLctEdit, FmLctEdit, afmoNegarComAviso) then
      begin
        with FmLctEdit do
        begin
          if Altera = 1 then
          begin
            LaTipo.Caption := CO_INCLUSAO;
            dmkCBGenero.KeyValue := 0;
            dmkEdTPData.Date := MLAGeral.ObtemDataInserir;
            dmkEdTPVencto.Date := Date;
            dmkEdTPDataDoc.Date := Date;
          end else begin
            dmkEdCartAnt.Text := IntToStr(Carteira);
            dmkEdTipoAnt.Text := IntToStr(Tipo);
            case Altera of
              2: LaTipo.Caption := CO_ALTERACAO;
              3: LaTipo.Caption := CO_INCLUSAO;
            end;
            if Altera = 3 then dmkEdControle.Text := '0'
            else dmkEdControle.Text := Geral.TFT(FloatToStr(Controle), 0, siPositivo);
            dmkEdTPData.Date        := Data;
            dmkEdTPDataDoc.Date     := DataDoc;
            dmkEdCBGenero.Text      := IntToStr(Genero);
            dmkCBGenero.KeyValue    := Genero;
            dmkEdCBFornece.Text     := IntToStr(Fornecedor);
            dmkCBFornece.KeyValue   := Fornecedor;
            dmkEdCBCliente.Text     := IntToStr(Cliente);
            dmkCBCliente.KeyValue   := Cliente;
            dmkEdCBCliInt.Text      := IntToStr(CliInt);
            dmkCBCliInt.KeyValue    := CliInt;
            dmkEdCBForneceI.Text      := IntToStr(ForneceI);
            dmkCBForneceI.KeyValue    := ForneceI;
            dmkEdCBVendedor.Text    := IntToStr(Vendedor);
            dmkCBVendedor.KeyValue  := Vendedor;
            dmkEdCBAccount.Text     := IntToStr(Account);
            dmkCBAccount.KeyValue   := Account;
            dmkEdMes.Text           := MLAGeral.TST(Mensal, False);
            if Qtde <> 0 then
            dmkEdQtde.Text          := Geral.TFT(FloatToStr(Qtde), 3, siPositivo);
            dmkEdCred.Text          := Geral.TFT(FloatToStr(Credito), 2, siPositivo);
            dmkEdDeb.Text           := Geral.TFT(FloatToStr(Debito), 2, siPositivo);
            dmkEdMoraDia.Text       := Geral.TFT(FloatToStr(MoraDia), 2, siPositivo);
            dmkEdMulta.Text         := Geral.TFT(FloatToStr(Multa), 2, siPositivo);
            dmkEdMultaVal.Text      := Geral.TFT(FloatToStr(MultaVal), 2, siPositivo);
            dmkEdMoraVal.Text       := Geral.TFT(FloatToStr(MoraVal), 2, siPositivo);
            dmkEdICMS_P.Text        := Geral.FFT(ICMS_P, 2, siPositivo);
            dmkEdICMS_V.Text        := Geral.FFT(ICMS_V, 2, siPositivo);
            dmkEdNF.Text            := IntToStr(NF);
            dmkEdDoc.Text           := Geral.TFD(FloatToStr(Documento), 6, siPositivo);
            dmkEdTPVencto.Date      := Vencimento;
            if Compensado > 0 then
            dmkEdTPCompensado.Date  := Compensado;
            dmkEdDescricao.Text     := Descricao;
            dmkEdDuplicata.Text     := Duplicata;
            dmkEdSerieCH.Text       := SerieCH;
            dmkEdCBDepto.Text       := IntToStr(Depto);
            dmkCBDepto.KeyValue     := Depto;
            {
            FCNAB_Sit               := CNAB_Sit;
            FID_Pgto                := ID_Pgto;
            FCNAB_Sit               := FCNAB_Sit;
            FFatID                  := FatID;
            FFatID_Sub              := FatID_Sub;
            FFatNum                 := FatNum;
            FFAtPArcela             := FatParcela;
            FNivel                  := Nivel;
            FDescoPor               := DescoPor;
            FCtrlIni                := CtrlIni;
            FUnidade                := Unidade;
            FDescoVal               := DescoVal;
            FNFVal                  := NFVal;
            FDoc2                   := Doc2;
            }

            dmkRGTipoCH.ItemIndex   := TipoCH;
          end;
          dmkEdCarteira.Text := IntToStr(Carteira);
          dmkCBCarteira.KeyValue := Carteira;
          LaCliInt.Caption := 'Espon�nio (Cliente interno)';
          if (Sit > 1) and (Tipo = 2) then
          begin
            if (Cartao <> 0) and not VAR_FATURANDO then
            begin
              ShowMessage('Esta emiss�o j� est� quitada!');
              dmkEdDeb.Enabled       := False;
              dmkEdCred.Enabled      := False;
              dmkEdDoc.Enabled       := False;
              dmkEdDescricao.Enabled := False;
            end;
            dmkEdCarteira.Enabled := False;
            dmkCBCarteira.Enabled := False;
            VAR_BAIXADO := Sit;
          end else VAR_BAIXADO := -2;
          {
          FEspPercJuros := EspPercJuros;
          FEspPercMulta := EspPercMulta;
          FTPDataIni     := EspTPDataIni;
          FTPDataFim     := EspTPDataFim;
          FDepto         := EspDepto;
          }
          dmkEdOneCliInt.ValueVariant    := EspCodigo;
          LaFinalidade.Caption    := FormatFloat('000', Finalidade);
          case Finalidade of
            1:
            begin
              dmkEdCBDepto.Enabled := False;
              LaDepto.Caption := 'Unidade habitacional do Espom�nio:';
              LaForneceI.Caption := 'Propriet�rio da unidade habitacional:';
            end;
            2:
            begin
              dmkEdCBDepto.Enabled := True;
              LaDepto.Caption := 'Des�gnio (quando houver):';
              LaForneceI.Caption := 'S�cio do des�gnio:';
            end;
            else
            begin
              LaDepto.Caption := '????:';
              LaForneceI.Caption := '????:';
            end;
          end;
        end;
      end;
    finally
      Screen.Cursor := Cursor;
    end;
    FmLctEdit.ShowModal;
    FmLctEdit.Destroy;
    UMyMod.UpdUnLockInt64Y(Controle, Dmod.MyDB, VAR_LCT, 'Controle');
  finally
    ;
  end;
end;

procedure TDmodFin.VeSeReabreLct(TPDataIni, TPDataFim: TdmkEditDateTimePicker;
Apto: Variant; Controle, Sub: Integer; QrCrt, QrLct: TmySQLQuery;
ForcaAbertura: Boolean = False);
begin
  if (QrCrt.State = dsBrowse) and ((QrLct.State = dsBrowse) or ForcaAbertura) then
  ReabreSoLct(QrLct, (*QrCrt.FieldByName('Tipo').AsInteger,*)
    QrCrt.FieldByName('Codigo').AsInteger,
    Controle, Sub, TPDataIni, TPDataFim, Apto);
end;

function TDmodFin.DefParams(QrCrt, QrCartSum: TmySQLQuery;
AvisaErro: Boolean; Aviso: String): Boolean;
var
  Carteira: Integer;
begin
  if QrCrt <> nil then
  begin
    if QrCrt.State <> dsBrowse then
      Carteira := 0
    else begin
      if QrCrt.RecordCount = 0 then Carteira := 0
      else
        Carteira := QrCrt.FieldByName('Codigo').AsInteger;
    end;
    ReabreCarteiras(Carteira, QrCrt, QrCartSum, Aviso + ' > TDmodFin.DefParams');
    Result := True;
  end else begin
    Result := False;
    if AvisaErro then
      Geral.MensagemBox(
      '"QrCrt" n�o definido na procedure "DefParams"', 'ERRO',
      MB_OK+MB_ICONERROR);
  end;
end;

function TDmodFin.ExisteLancamentoSemCliInt(InverteAviso: Boolean = False): Boolean;
begin
  QrELSCI.Close;
  QrELSCI.Open;
  //
  Result := QrELSCI.RecordCount > 0;
  if Result then
  begin
    if InverteAviso then
    Geral.MB_Erro(
    'A��o abortada! Existem lan�amentos com cliente interno indefinido!' +
    sLineBreak + 'AVISE A DERMATEK!');
    //
    if DBCheck.CriaFm(TFmFinOrfao1, FmFinOrfao1, afmoNegarComAviso) then
    begin
      FmFinOrfao1.ShowModal;
      FmFinOrfao1.Destroy;
    end;
  end else
    if InverteAviso then
      Geral.MB_Info('N�o existem lan�amentos com cliente interno indefinido!');
end;

procedure TDmodFin.frxSaldosGetValue(const VarName: string; var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := DModG.QrEmpresasNOMEFILIAL.Value;
end;

procedure TDmodFin.ReabreCarteiras((*CliInt, *)LocCart: Integer;
QrCrt, QrCartSum: TmySQLQuery; Aviso: String);
var
  Empresas: String;
begin
  if VAR_LIB_EMPRESAS = '' then
    Empresas := '-100000000'
  else
    Empresas := VAR_LIB_EMPRESAS;
  QrCrt.Close;
  QrCrt.SQL.Clear;
  QrCrt.SQL.Text :=
    'SELECT DISTINCT ca.*, ba.Nome NOMEDOBANCO, '      + #13#10 +
    'CASE WHEN en.Tipo=0 THEN en.RazaoSocial '         + #13#10 +
    'ELSE en.Nome END NOMEFORNECEI '                   + #13#10 +
    'FROM carteiras ca '                               + #13#10 +
    'LEFT JOIN carteiras ba ON ba.Codigo=ca.Banco '    + #13#10 +
    'LEFT JOIN entidades en ON en.Codigo=ca.ForneceI ' + #13#10 +
    'WHERE ca.ForneceI in (' + Empresas + ')'  + #13#10 +
    'ORDER BY ca.Nome';
  UMyMod.AbreQuery(QrCrt, QrCrt.Database, Aviso + ' > TDmodFin.ReabreCarteiras() > QrCarteira');
  //
  QrCrt.Locate('Codigo', LocCart, []);
  //
  if QrCartSum <> nil  then
  begin
    QrCartSum.Close;
    QrCartSum.SQL.Text :=
      'SELECT SUM(Saldo) SALDO, SUM(FuturoC) FuturoC,'   + #13#10 +
      'SUM(FuturoD) FuturoD, SUM(FuturoS) FuturoS,'      + #13#10 +
      'SUM(EmCaixa) EmCaixa, SUM(EmCaixa-Saldo) Difere,' + #13#10 +
      'SUM(Saldo+FuturoS) SDO_FUT'                       + #13#10 +
      'FROM carteiras ca'                                + #13#10 +
      'WHERE ca.ForneceI in (' + Empresas + ')';
    UMyMod.AbreQuery(QrCartSum, QrCartSum.Database, Aviso + 'TDmodFin.ReabreCarteiras() > QrCartSum');
  end;
  //
end;

function TDmodFin.LocCod(Atual, Codigo: Integer; QrCrt: TmySQLQuery;
(*CliInt: Integer; *)Aviso: String): Boolean;
begin
  Result := False;
  DefParams(QrCrt, nil, True, Aviso + ' > TDmodFin.LocCod(');
  if not QrCrt.Locate('Codigo', Codigo, []) then
    QrCrt.Locate('Codigo', Atual, [])
  else Result := True;
end;

{$IfNDef SemCotacoes}
procedure TDmodFin.VerificaCotacoesDia();
begin
  QrCambioUsu.Close;
  QrCambioUsu.Params[0].AsInteger := VAR_USUARIO;
  QrCambioUsu.Open;
  //
  if QrCambioUsuCodigo.Value > 0 then
  begin
    QrCambioDia.Close;
    QrCambioDia.Params[0].AsString := Geral.FDT(Date, 1);
    QrCambioDia.Open;
    //
    if QrCambioDia.RecordCount = 0 then
    begin
      if DBCheck.CriaFm(TFmCambioCot, FmCambioCot, afmoNegarComAviso) then
      begin
        FmCambioCot.TPDataC.Date := Date;
        FmCambioCot.ShowModal;
        FmCambioCot.Destroy;
      end;
    end;
  end;
end;
{$EndIf}

function TDmodFin.CarregaItensTipoDoc(Compo: TControl): Boolean;
var
  PropInfo: PPropInfo;
begin
  Result := False;
  if Compo <> nil then
  begin
    PropInfo := GetPropInfo(Compo.ClassInfo, 'Items');
    if PropInfo <> nil then
    begin
      TRadioGroup(Compo).Items.Clear;
      TRadioGroup(Compo).Items.Add('Outros');
      TRadioGroup(Compo).Items.Add('Cheque');
      TRadioGroup(Compo).Items.Add('DOC');
      TRadioGroup(Compo).Items.Add('TED');
      TRadioGroup(Compo).Items.Add('Esp�cie');
      TRadioGroup(Compo).Items.Add('Bloqueto');
      TRadioGroup(Compo).Items.Add('Duplicata');
      TRadioGroup(Compo).Items.Add('TEF');
      TRadioGroup(Compo).Items.Add('D�bito c/c');
    end;
    PropInfo := GetPropInfo(Compo.ClassInfo, 'Values');
    if PropInfo <> nil then
    begin
      TDBRadioGroup(Compo).Values.Clear;
      TDBRadioGroup(Compo).Values.Add('0');
      TDBRadioGroup(Compo).Values.Add('1');
      TDBRadioGroup(Compo).Values.Add('2');
      TDBRadioGroup(Compo).Values.Add('3');
      TDBRadioGroup(Compo).Values.Add('4');
      TDBRadioGroup(Compo).Values.Add('5');
      TDBRadioGroup(Compo).Values.Add('6');
      TDBRadioGroup(Compo).Values.Add('7');
      TDBRadioGroup(Compo).Values.Add('8');
    end;
  end;
end;

function TDmodFin.ConfereSalddosIniciais_Carteiras_x_Contas(CliInt: Integer): Boolean;
begin
  QrSdoCarts.Close;
  QrSdoCarts.Params[0].AsInteger := CliInt;
  QrSdoCarts.Open;
  //
  QrSdoCtSdo.Close;
  QrSdoCtSdo.Params[0].AsInteger := CliInt;
  QrSdoCtSdo.Open;
  //
  if QrSdoCartsInicial.Value <> QrSdoCtSdoSdoIni.Value then
  begin
    Result := False;
    Application.MessageBox(PChar('Saldos iniciais das carteiras n�o conferem ' +
    'com os saldos iniciais das contas do cliente interno ' + IntToStr(CliInt) +
    ':'#13#10+'Saldo das carteiras: '+ Geral.FFT(QrSdoCartsInicial.Value, 2,
    siNegativo)+#13#10+'Saldo das contas: ' + Geral.FFT(
    QrSdoCtSdoSdoIni.Value, 2, siNegativo)), 'Aviso', MB_OK+MB_ICONWARNING);
  end else Result := True;
end;

function TDmodFin.ReopenCliInt(CliInt: Integer): Boolean;
begin
  DmodFin.QrCliInt.Close;
  if Trim(VAR_NOME_TAB_CLIINT) <> '' then
  begin
    DmodFin.QrCliInt.SQL.Clear;
    DmodFin.QrCliInt.SQL.Add('SELECT PBB');
    DmodFin.QrCliInt.SQL.Add('FROM ' + LowerCase(VAR_NOME_TAB_CLIINT));
    DmodFin.QrCliInt.SQL.Add('WHERE Codigo=' + FormatFloat('0', CliInt));
    DmodFin.QrCliInt.Open;
    //
    Result := DmodFin.QrCliInt.RecordCount > 0;
  end else Result := False;
end;

procedure TDmodFin.ReopenCreditosEDebitos(CliInt: Integer; DataI, DataF: String;
Acordos, Periodos, Textos, NaoAgruparNada: Boolean; CarteiraUnica: Integer);
begin
  //// C R � D I T O S
  QrCreditos.Close;
  QrCreditos.SQL.Clear;
  if NaoAgruparNada = True then
    QrCreditos.SQL.Add('SELECT lan.Mez, lan.Credito, ')
  else
    QrCreditos.SQL.Add('SELECT lan.Mez, SUM(lan.Credito) Credito, ');
  QrCreditos.SQL.Add('lan.Controle, lan.Sub, lan.Carteira, lan.Cartao, lan.Tipo,');
  QrCreditos.SQL.Add('lan.Vencimento, lan.Compensado, lan.Sit, lan.Genero, ');
  QrCreditos.SQL.Add('lan.SubPgto1, ');
  QrCreditos.SQL.Add('con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU');
  QrCreditos.SQL.Add('FROM ' + VAR_LCT + ' lan');
  QrCreditos.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrCreditos.SQL.Add('LEFT JOIN contas    con ON con.Codigo=lan.Genero');
  QrCreditos.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo');
  QrCreditos.SQL.Add('LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo');
  QrCreditos.SQL.Add('WHERE lan.Tipo <> 2');
  QrCreditos.SQL.Add('AND lan.Credito > 0');
  QrCreditos.SQL.Add('AND lan.Genero>0');
  if CarteiraUnica <> 0 then
    QrCreditos.SQL.Add('AND car.Codigo=' + FormatFloat('0', CarteiraUnica))
  else
    QrCreditos.SQL.Add('AND car.ForneceI=' + FormatFloat('0', CliInt));
  QrCreditos.SQL.Add('AND lan.Data BETWEEN "' + DataI + '" AND "' + DataF + '"');
  if NaoAgruparNada = False then
  begin
    QrCreditos.SQL.Add('GROUP BY lan.Genero ');
    if Periodos then
      QrCreditos.SQL.Add(', lan.Mez');
    if Acordos then
      QrCreditos.SQL.Add(', lan.SubPgto1');
  end;
  QrCreditos.SQL.Add('ORDER BY gru.OrdemLista, NOMEGRU, sgr.OrdemLista,');
  QrCreditos.SQL.Add('NOMESGR, con.OrdemLista, NOMECON, Mez, Data');
  QrCreditos.SQL.Add('');
  //
(*
  QrCreditos.Params[00].AsInteger := CliInt;
  QrCreditos.Params[01].AsString  := DataI;
  QrCreditos.Params[02].AsString  := DataF;
*)
  UMyMod.AbreQuery(QrCreditos, Dmod.MyDB, 'TDmodFin.ReopenCreditosEDebitos()');
  //


  //// D � B I T O S
  QrDebitos.Close;
  QrDebitos.SQL.Clear;
  if Textos and (NaoAgruparNada = False) then
  begin
    QrDebitos.SQL.Add('SELECT IF(COUNT(lan.Compensado) > 1, "V�rias", IF(lan.Compensado=0,"",');
    QrDebitos.SQL.Add('DATE_FORMAT(lan.Compensado, "%d/%m"))) COMPENSADO_TXT,');
    QrDebitos.SQL.Add('IF(COUNT(lan.Data)>1, "V�rias", DATE_FORMAT(lan.Data, "%d/%m/%y")) DATA,');
    QrDebitos.SQL.Add('lan.Descricao, SUM(lan.Debito) DEBITO,');
    QrDebitos.SQL.Add('IF(COUNT(lan.Data)>1, 0, lan.NotaFiscal) NOTAFISCAL,');
    QrDebitos.SQL.Add('IF(COUNT(lan.Data)>1, "", lan.SerieCH) SERIECH,');
    QrDebitos.SQL.Add('IF(COUNT(lan.Data)>1, 0, lan.Documento) DOCUMENTO,');
    QrDebitos.SQL.Add('IF(COUNT(lan.Data)>1, 0, lan.Mez) MEZ, lan.Compensado,');
    QrDebitos.SQL.Add('con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU,');
    QrDebitos.SQL.Add('COUNT(lan.Data) ITENS, ');
  end else begin
    QrDebitos.SQL.Add('SELECT IF(lan.Compensado=0,"", DATE_FORMAT(');
    QrDebitos.SQL.Add('lan.Compensado, "%d/%m")) COMPENSADO_TXT,');
    QrDebitos.SQL.Add('DATE_FORMAT(lan.Data, "%d/%m/%y") DATA, ');
    QrDebitos.SQL.Add('lan.Descricao, lan.Debito, ');
    QrDebitos.SQL.Add('IF(lan.NotaFiscal = 0, 0, lan.NotaFiscal) NOTAFISCAL,');
    QrDebitos.SQL.Add('IF(lan.SerieCH = "", "", lan.SerieCH) SERIECH,');
    QrDebitos.SQL.Add('IF(lan.Documento = 0, 0, lan.Documento) DOCUMENTO,');
    QrDebitos.SQL.Add('IF(lan.Mez = 0, 0, lan.Mez) MEZ, lan.Compensado,');
    QrDebitos.SQL.Add('con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU,');
    QrDebitos.SQL.Add('0 ITENS, ');
  end;
  QrDebitos.SQL.Add('lan.Vencimento, lan.Compensado, lan.Sit, lan.Genero, ');
  QrDebitos.SQL.Add('lan.Tipo, lan.Controle, lan.Sub, lan.Carteira, lan.Cartao,');
  QrDebitos.SQL.Add('IF(frn.Codigo=0, "", IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome)) NO_FORNECE');
  QrDebitos.SQL.Add('FROM ' + VAR_LCT + ' lan');
  QrDebitos.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrDebitos.SQL.Add('LEFT JOIN contas    con ON con.Codigo=lan.Genero');
  QrDebitos.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo');
  QrDebitos.SQL.Add('LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo');
  QrDebitos.SQL.Add('LEFT JOIN entidades frn ON frn.Codigo=lan.Fornecedor');
  QrDebitos.SQL.Add('WHERE lan.Tipo <> 2');
  QrDebitos.SQL.Add('AND lan.Debito > 0');
  QrDebitos.SQL.Add('AND lan.Genero>0');
  if CarteiraUnica <> 0 then
    QrDebitos.SQL.Add('AND car.Codigo=' + FormatFloat('0', CarteiraUnica))
  else
    QrDebitos.SQL.Add('AND car.ForneceI=' + FormatFloat('0', CliInt));
  QrDebitos.SQL.Add('AND lan.Data BETWEEN "' + DataI + '" AND "' + DataF + '"');

  if NaoAgruparNada = False then
  begin
    if Textos then
      QrDebitos.SQL.Add('GROUP BY lan.Descricao');
  end;
  QrDebitos.SQL.Add('ORDER BY gru.OrdemLista, NOMEGRU, sgr.OrdemLista,');
  QrDebitos.SQL.Add('NOMESGR, con.OrdemLista, NOMECON, Compensado, Descricao');
  QrDebitos.SQL.Add('');
  QrDebitos.SQL.Add('');
(*
  QrDebitos.Params[00].AsInteger := CliInt;
  QrDebitos.Params[01].AsString  := DataI;
  QrDebitos.Params[02].AsString  := DataF;
*)
  UMyMod.AbreQuery(QrDebitos, Dmod.MyDB, 'TDmodFin.ReopenCreditosEDebitos()');
end;

{
function TDmodFin.ReopenPendAll(Entidade, CliInt, Periodo: Integer): Boolean;
var
  DataFim: String;
begin
  try
    DataFim := Geral.FDT(MLAGeral.UltimoDiaDoPeriodo_Date(Periodo), 1);
    //
    QrPendAll.Close;
    QrPendAll.SQL.Clear;
    QrPendSum.Close;
    QrPendSum.SQL.Clear;
    if Uppercase(Application.Title) = 'SYNDIC' then
    begin
      QrPendAll.SQL.Add('SELECT imv.Unidade, SUM(lan.Credito) Credito,');
      QrPendAll.SQL.Add('"Quota condominial" TIPO, 0 KGT,');
      QrPendAll.SQL.Add('COUNT(DISTINCT lan.FatNum) BLOQUETOS');
      QrPendAll.SQL.Add('FROM ' + VAR_LCT + ' lan');
      QrPendAll.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
      QrPendAll.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=lan.Depto');
      QrPendAll.SQL.Add('WHERE car.Tipo=2');
      QrPendAll.SQL.Add('AND lan.Reparcel=0');
      QrPendAll.SQL.Add('AND car.ForneceI=:P0');
      QrPendAll.SQL.Add('AND imv.Codigo=:P1');
      QrPendAll.SQL.Add('AND lan.Depto>0');
      QrPendAll.SQL.Add('AND lan.Sit<2');
      QrPendAll.SQL.Add('AND lan.Vencimento <= :P2');
      QrPendAll.SQL.Add('GROUP BY imv.Unidade');
      QrPendAll.SQL.Add('');
      QrPendAll.SQL.Add('UNION');
      QrPendAll.SQL.Add('');
      QrPendAll.SQL.Add('SELECT imv.Unidade, SUM(lan.Credito) Credito,');
      QrPendAll.SQL.Add('"Reparcelamento" TIPO, 0 KGT,');
      QrPendAll.SQL.Add('COUNT(DISTINCT lan.FatNum) BLOQUETOS');
      QrPendAll.SQL.Add('FROM ' + VAR_LCT + ' lan');
      QrPendAll.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
      QrPendAll.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=lan.Depto');
      QrPendAll.SQL.Add('WHERE lan.Atrelado>0');
      QrPendAll.SQL.Add('AND lan.Genero=-10');
      QrPendAll.SQL.Add('AND car.Tipo=2');
      QrPendAll.SQL.Add('AND lan.Sit < 2');
      QrPendAll.SQL.Add('AND car.ForneceI=:P3');
      QrPendAll.SQL.Add('AND lan.Vencimento <= :P4');
      QrPendAll.SQL.Add('GROUP BY imv.Unidade');
      QrPendAll.SQL.Add('');
      QrPendAll.SQL.Add('ORDER BY Unidade');
      QrPendAll.Params[00].AsInteger := Entidade;
      QrPendAll.Params[01].AsInteger := CliInt;
      QrPendAll.Params[02].AsString  := DataFim;
      QrPendAll.Params[03].AsInteger := Entidade;
      QrPendAll.Params[04].AsString  := DataFim;
      QrPendAll.Open;
      //
      QrPendSum.SQL.Add('SELECT SUM(lan.Credito) Credito');
      QrPendSum.SQL.Add('FROM ' + VAR_LCT + ' lan');
      QrPendSum.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
      QrPendSum.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=lan.Depto');
      QrPendSum.SQL.Add('WHERE (car.Tipo=2');
      QrPendSum.SQL.Add('AND lan.Reparcel=0');
      QrPendSum.SQL.Add('AND car.ForneceI=:P0');
      QrPendSum.SQL.Add('AND imv.Codigo=:P1');
      QrPendSum.SQL.Add('AND lan.Depto>0');
      QrPendSum.SQL.Add('AND lan.Sit<2');
      QrPendSum.SQL.Add('AND lan.Vencimento <=:P2)');
      QrPendSum.SQL.Add('');
      QrPendSum.SQL.Add('OR');
      QrPendSum.SQL.Add('');
      QrPendSum.SQL.Add('(lan.Atrelado>0');
      QrPendSum.SQL.Add('AND lan.Genero=-10');
      QrPendSum.SQL.Add('AND car.Tipo=2');
      QrPendSum.SQL.Add('AND car.ForneceI=:P3');
      QrPendSum.SQL.Add('AND lan.Vencimento <=:P4');
      QrPendSum.SQL.Add('AND lan.Sit < 2)');
      QrPendSum.Params[00].AsInteger := Entidade;
      QrPendSum.Params[01].AsInteger := CliInt;
      QrPendSum.Params[02].AsInteger := Entidade;
      QrPendSum.Open;
      //
      Result := True;
    end else begin
      Geral.MensagemBox('Este aplicativo n�o possui implementa��o para o ' +
      #13#10 + 'relat�rio selecionado de "Inadimpl�ncia de Unidades".' +
      #13#10 + 'SOLICITE � DERMATEK', 'Aviso', MB_OK+MB_ICONWARNING);
      // Compatibilidade com o FastReport ?
      QrPendAll.SQL.Add('SELECT "?????" Unidade, 0.00000 Credito,');
      QrPendAll.SQL.Add('"??????" TIPO, 0 KGT, 0 BLOQUETOS');
      QrPendAll.Open;
      //
      QrPendSum.SQL.Add('SELECT 0.00000 Credito');
      QrPendSum.Open;
      //
      Result := False;
    end;
  except
    Result := False;
    raise;
  end;
end;
}

function TDmodFin.ReopenPendAll(Entidade, CliInt, Periodo: Integer): Boolean;
var
  DataFim: String;
begin
  try
    DataFim := Geral.FDT(MLAGeral.UltimoDiaDoPeriodo_Date(Periodo), 1);
    //
    QrPendAll.Close;
    QrPendAll.SQL.Clear;
    QrPendSum.Close;
    QrPendSum.SQL.Clear;
    QrPendAll.SQL.Add('SELECT SUM(lan.Credito) Credito,');
    if Uppercase(Application.Title) = 'SYNDIC' then
    begin
      QrPendAll.SQL.Add('imv.Unidade, "Quota condominial" TIPO, 0 KGT,');
    end else begin
      QrPendAll.SQL.Add('"  " Unidade, "   " TIPO, 0 KGT,');
    end;
    QrPendAll.SQL.Add('COUNT(DISTINCT lan.FatNum) BLOQUETOS');
    QrPendAll.SQL.Add('FROM ' + VAR_LCT + ' lan');
    QrPendAll.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    if Uppercase(Application.Title) = 'SYNDIC' then
      QrPendAll.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=lan.Depto');
    QrPendAll.SQL.Add('WHERE car.Tipo=2');
    QrPendAll.SQL.Add('AND lan.Reparcel=0');
    QrPendAll.SQL.Add('AND car.ForneceI=' + FormatFloat('0', Entidade));
    if Uppercase(Application.Title) = 'SYNDIC' then
      QrPendAll.SQL.Add('AND imv.Codigo=' + FormatFloat('0', CliInt));
    QrPendAll.SQL.Add('AND lan.Depto>0');
    QrPendAll.SQL.Add('AND lan.Sit<2');
    QrPendAll.SQL.Add('AND lan.Vencimento <= "'+DataFim+'"');
    if Uppercase(Application.Title) = 'SYNDIC' then
      QrPendAll.SQL.Add('GROUP BY imv.Unidade')
    else
      QrPendAll.SQL.Add('GROUP BY lan.Depto');
    QrPendAll.SQL.Add('');
    QrPendAll.SQL.Add('UNION');
    QrPendAll.SQL.Add('');
    QrPendAll.SQL.Add('SELECT SUM(lan.Credito) Credito,');
    if Uppercase(Application.Title) = 'SYNDIC' then
    begin
      QrPendAll.SQL.Add('imv.Unidade, "Reparcelamento" TIPO, 0 KGT,');
    end else begin
      QrPendAll.SQL.Add('"  " Unidade, "   " TIPO, 0 KGT,');
    end;
    QrPendAll.SQL.Add('COUNT(DISTINCT lan.FatNum) BLOQUETOS');
    QrPendAll.SQL.Add('FROM ' + VAR_LCT + ' lan');
    QrPendAll.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    if Uppercase(Application.Title) = 'SYNDIC' then
      QrPendAll.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=lan.Depto');
    QrPendAll.SQL.Add('WHERE lan.Atrelado>0');
    QrPendAll.SQL.Add('AND lan.Genero=-10');
    QrPendAll.SQL.Add('AND car.Tipo=2');
    QrPendAll.SQL.Add('AND lan.Sit < 2');
    QrPendAll.SQL.Add('AND car.ForneceI=' + FormatFloat('0', Entidade));
    QrPendAll.SQL.Add('AND lan.Vencimento <= "'+DataFim+'"');
    if Uppercase(Application.Title) = 'SYNDIC' then
      QrPendAll.SQL.Add('GROUP BY imv.Unidade')
    else
      QrPendAll.SQL.Add('GROUP BY lan.Depto');
    QrPendAll.SQL.Add('');
    QrPendAll.SQL.Add('ORDER BY Unidade');
    (*
    QrPendAll.Params[00].AsInteger := Entidade;
    QrPendAll.Params[01].AsInteger := CliInt;
    QrPendAll.Params[02].AsString  := DataFim;
    QrPendAll.Params[03].AsInteger := Entidade;
    QrPendAll.Params[04].AsString  := DataFim;
    *)
    UMyMod.AbreQuery(QrPendAll, Dmod.MyDB, 'ReopenPendAll()');
    //
    QrPendSum.SQL.Add('SELECT SUM(lan.Credito) Credito');
    QrPendSum.SQL.Add('FROM ' + VAR_LCT + ' lan');
    QrPendSum.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    if Uppercase(Application.Title) = 'SYNDIC' then
      QrPendSum.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=lan.Depto');
    QrPendSum.SQL.Add('WHERE (car.Tipo=2');
    QrPendSum.SQL.Add('AND lan.Reparcel=0');
    QrPendSum.SQL.Add('AND car.ForneceI=' + FormatFloat('0', Entidade));
    if Uppercase(Application.Title) = 'SYNDIC' then
      QrPendSum.SQL.Add('AND imv.Codigo=' + FormatFloat('0', CliInt));
    QrPendSum.SQL.Add('AND lan.Depto>0');
    QrPendSum.SQL.Add('AND lan.Sit<2');
    QrPendSum.SQL.Add('AND lan.Vencimento <="'+DataFim+'")');
    QrPendSum.SQL.Add('');
    QrPendSum.SQL.Add('OR');
    QrPendSum.SQL.Add('');
    QrPendSum.SQL.Add('(lan.Atrelado>0');
    QrPendSum.SQL.Add('AND lan.Genero=-10');
    QrPendSum.SQL.Add('AND car.Tipo=2');
    QrPendSum.SQL.Add('AND car.ForneceI=' + FormatFloat('0', Entidade));
    QrPendSum.SQL.Add('AND lan.Vencimento <="'+DataFim+'"');
    QrPendSum.SQL.Add('AND lan.Sit < 2)');
    UMyMod.AbreQuery(QrPendSum, Dmod.MyDB, 'ReopenPendAll()');
    //
    Result := True;
  except
    //Result := False;
    raise;
  end;
end;

function TDmodFin.ReopenPendG(Entidade, CliInt: Integer): Boolean;
begin
  try
    QrPendG.Close;
    QrPendG.SQL.Clear;
    QrPendG.SQL.Add('SELECT lan.Cliente, lan.Fornecedor, lan.FatNum,');
    QrPendG.SQL.Add('lan.Mez, SUM(lan.Credito) Credito, lan.Vencimento,');
    QrPendG.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
    QrPendG.SQL.Add('ELSE ent.Nome END NOMEPROPRIET');
    if Uppercase(Application.Title) = 'SYNDIC' then
      QrPendG.SQL.Add(', imv.Unidade')
    else
      QrPendG.SQL.Add(', "? ? ?" Unidade');
    QrPendG.SQL.Add('FROM ' + VAR_LCT + ' lan');
    QrPendG.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    QrPendG.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=lan.ForneceI');
    if Uppercase(Application.Title) = 'SYNDIC' then
      QrPendG.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=lan.Depto');
    QrPendG.SQL.Add('WHERE car.Tipo=2');
    QrPendG.SQL.Add('AND lan.Reparcel=0');
    QrPendG.SQL.Add('AND car.ForneceI=' + FormatFloat('0', Entidade)); // //QrCondCliente.Value;
    if Uppercase(Application.Title) = 'SYNDIC' then
      QrPendG.SQL.Add('AND imv.Codigo=' + FormatFloat('0', CliInt)); //QrCondCodigo.Value;
    QrPendG.SQL.Add('AND lan.Depto>0');
    QrPendG.SQL.Add('AND lan.Sit<2');
    QrPendG.SQL.Add('AND lan.Vencimento < SYSDATE()');
    if Uppercase(Application.Title) = 'SYNDIC' then
    begin
      QrPendG.SQL.Add('GROUP BY imv.Controle, imv.Unidade, lan.Mez, lan.Vencimento, lan.FatNum');
      QrPendG.SQL.Add('ORDER BY imv.Controle, imv.Unidade, lan.Mez, lan.Vencimento, lan.FatNum');
    end else begin
      QrPendG.SQL.Add('GROUP BY lan.Depto, lan.Mez, lan.Vencimento, lan.FatNum');
      QrPendG.SQL.Add('ORDER BY lan.Depto, lan.Mez, lan.Vencimento, lan.FatNum');
    end;
    UMyMod.AbreQuery(QrPendG, Dmod.MyDB, 'Pend�ncias de integrantes do cliente');
    //
    Result := True;
  except
    //Result := False;
    raise;
  end;
end;

function TDmodFin.ReopenPrevItO(CliInt, Periodo: Integer): Boolean;
var
  Codigo: Integer;
begin
  Result := False;
  try
    QrLocPerX.Close;
    QrLocPerX.Params[0].AsInteger := CliInt;
    QrLocPerX.Params[1].AsInteger := Periodo - 1;
    QrLocPerX.Open;
    Codigo := QrLocPerXCodigo.Value;
    if Codigo = 0 then Codigo := -10000;
    //
    QrPrevItO.Close;
    QrPrevItO.Params[0].AsInteger := Codigo;
    QrPrevItO.Open;
    //
    Result := True;
  except
    Geral.MensagemBox(
    'N�o foi poss�vel abrir as tabelas de Provis�o de Arrecada��o!',
    'AVISE A DERMATEK', MB_OK+MB_ICONERROR);
  end;
end;

function TDmodFin.ReopenReparcelNew(CliEnti, Periodo, PBB: Integer): Boolean;
var
  DataI, DataF: String;
begin
  //Result := False;
  try
    DataI := Geral.FDT(MLAGeral.PrimeiroDiaDoPeriodo_Date(Periodo - 1 + PBB), 1);
    DataF := Geral.FDT(MLAGeral.UltimoDiaDoPeriodo_Date(Periodo - 1 + PBB), 1);
    //
    QrRep.Close;
    if Uppercase(Application.Title) = 'SYNDIC' then
    begin
      QrRep.SQL.Clear;
      QrRep.SQL.Add('SELECT bpa.*, cim.Unidade,');
      QrRep.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP');
      QrRep.SQL.Add('FROM bloqparc bpa');
      QrRep.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=bpa.CodigoEnt');
      QrRep.SQL.Add('LEFT JOIN condimov  cim ON cim.Conta=bpa.CodigoEsp');
      QrRep.SQL.Add('WHERE bpa.CodCliEnt=:P0');
      QrRep.SQL.Add('AND bpa.DataP BETWEEN :P1 AND :P2');
      QrRep.SQL.Add('ORDER BY bpa.DataP, cim.Unidade');
      QrRep.SQL.Add('');
      QrRep.Params[00].AsInteger := CliEnti;//QrCondCliente.Value;
      QrRep.Params[01].AsString  := DataI;
      QrRep.Params[02].AsString  := DataF;
      QrRep.Open;
    end;
    //
    Result := QrRep.State <> dsInactive;
    if not Result then Geral.MensagemBox(
    'N�o foi poss�vel identificar reparcelamentos!',
    'Aviso', MB_OK+MB_ICONWARNING);
  except
    raise;
  end;
end;

procedure TDmodFin.ReopenSaldoAEResumo(CliInt, CarteiraUnica: Integer;
DataI, DataF: String);
var
  Lin1, lin2: String;
begin
  if CarteiraUnica <> 0 then
  begin
    Lin1 := '  AND crt.Codigo=' + FormatFloat('0', CarteiraUnica);
    Lin2 := 'AND car.Codigo=' + FormatFloat('0', CarteiraUnica);
  end else begin
    Lin1 := '  AND crt.ForneceI=' + FormatFloat('0', CliInt);
    Lin2 := 'AND car.ForneceI=' + FormatFloat('0', CliInt);
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrSaldoA, Dmod.MyDB, [
  'SELECT SUM(car.Inicial) Inicial,',
  '(',
  '  SELECT SUM(lan.Credito-lan.Debito) ',
  '  FROM lanctos lan',
  '  LEFT JOIN carteiras crt ON crt.Codigo=lan.Carteira',
  '  WHERE crt.Tipo <> 2',
  Lin1,
  '  AND lan.Data < "' + DataI + '"',
  ') SALDO',
  'FROM carteiras car',
  'WHERE car.Tipo <> 2',
  Lin2,
  '']);
(*
  QrSaldoA.Close;
  QrSaldoA.Params[00].AsInteger := CliInt;
  QrSaldoA.Params[01].AsString  := DataI;
  QrSaldoA.Params[02].AsInteger := CliInt;
  QrSaldoA.Open;
*)
  // Deve ser ap�s SaldoA
  UnDmkDAC_PF.AbreMySQLQuery0(QrResumo, Dmod.MyDB, [
  'SELECT SUM(lan.Credito) Credito, -SUM(lan.Debito) Debito',
  'FROM lanctos lan',
  'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira',
  'WHERE lan.Tipo <> 2',
  'AND lan.Genero > 0',
  Lin2,
  'AND lan.Data BETWEEN "' + DataI + '" AND "' + DataF + '"',
  '']);
(*
  QrResumo.Close;
  QrResumo.Params[00].AsInteger := CliInt;
  QrResumo.Params[01].AsString  := DataI;
  QrResumo.Params[02].AsString  := DataF;
  QrResumo.Open;
*)
end;

function TDmodFin.ReopenReparcelAbe(CliEnti: Integer): Boolean;
begin
  Result := False;
  try
    QrRepAbe.Close;
    if Uppercase(Application.Title) = 'SYNDIC' then
    begin
      QrRepAbe.SQL.Clear;
      QrRepAbe.SQL.Add('SELECT imv.Unidade, lan.Data, lan.Vencimento, lan.Controle,');
      QrRepAbe.SQL.Add('lan.Descricao, lan.Credito, lan.Compensado, lan.FatNum,');
      QrRepAbe.SQL.Add('lan.MoraDia, lan.Multa, lan.Depto, lan.Sit,');
      QrRepAbe.SQL.Add('lan.Pago, lan.PagMul, lan.PagJur,  lan.Atrelado,');
      QrRepAbe.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP');
      QrRepAbe.SQL.Add('FROM ' + VAR_LCT + ' lan');
      QrRepAbe.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
      QrRepAbe.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=lan.ForneceI');
      QrRepAbe.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=lan.Depto');
      QrRepAbe.SQL.Add('WHERE lan.Atrelado>0');
      QrRepAbe.SQL.Add('AND lan.Genero=-10');
      QrRepAbe.SQL.Add('AND car.Tipo=2');
      QrRepAbe.SQL.Add('AND car.ForneceI=:P0');
      QrRepAbe.SQL.Add('AND lan.Sit < 2');
      QrRepAbe.SQL.Add('ORDER BY NOMEPROP, lan.Atrelado, lan.Vencimento');
      QrRepAbe.Params[0].AsInteger := CliEnti;//QrCondCliente.Value;
      QrRepAbe.Open;
      Result := QrRepAbe.State <> dsInactive;
      if not Result then Geral.MensagemBox(
      'N�o foi poss�vel detectar reparcelamentos em aberto!',
      'Aviso', MB_OK+MB_ICONWARNING);
    end;  
  except
    raise;
  end;
end;

function TDmodFin.Reopen_APL_VLA(const ID_Pgto, SitAnt: Integer;
             var Data: String;
             var Pago, MoraVal, MultaVal: Double; var SitAtu: Integer;
             const DataCompQuit: String): Boolean;
var
  D_APL, C_APL, D_VLA, C_VLA,
  SaldoDeb, SaldoCred, SaldoXXX, Endossan, Endossad: Double;
begin
  Result := False;
  SitAtu := -9;
  Data := '';
  // Endosso
  QrVLA.Close;
{
SELECT Credito, Debito, Endossad, Endossan
FROM ' + VAR_LCT + '
WHERE Controle=:P0
ORDER BY Sub
}
  QrVLA.Params[0].AsInteger := ID_Pgto;
  QrVLA.Open;
  //
  // N�o � emiss�o
  if QrVLATipo.Value <> 2 then Exit;
  //
  // Pagamentos emitidos
  QrAPL.Close;
{
SELECT SUM(Credito) Credito, SUM(Debito) Debito,
SUM(MoraVal) MoraVal, SUM(MultaVal) MultaVal,
MAX(Data) Data, COUNT(Controle) Itens
FROM ' + VAR_LCT + ' WHERE ID_Pgto=:P0
}
  QrAPL.Params[0].AsInteger := ID_Pgto;
  QrAPL.Open;
  //

  if QrVLA.RecordCount <> 1 then
  begin
    // Atualiza��o de saldo de lan�amento de transfer�ncia. ERRO. Fazer espec�fico?
    Geral.MensagemBox('Atualiza��o de saldo de lan�amento inv�lido!' +
    #13#10 + 'ID_Pgto = ' + IntToStr(ID_Pgto) + #13#10 + 'AVISE A DERMATEK',
    'Aviso', MB_OK + MB_ICONWARNING);
    //Endossan    := 0;
    //Endossad    := 0;
    //SaldoDeb    := 0;
    //SaldoCred   := 0;
    Pago        := 0;
    //Debito      := 0;
    //Credito     := 0;
    MoraVal     := 0;
    MultaVal    := 0;
  end else begin
    //Result := True;
    //
    Endossan   := QrVLAEndossan.Value;
    Endossad   := QrVLAEndossad.Value;
    //
    D_APL      := QrAPLDebito.Value;
    C_APL      := QrAPLCredito.Value;
    Pago       := C_APL + D_APL  + Endossan + Endossad;
    //
    MoraVal    := QrAPLMoraVal.Value;
    MultaVal   := QrAPLMultaVal.Value;
    //
    D_VLA      := QrVLADebito.Value;//QrAPLDebito.Value;
    C_VLA      := QrVLACredito.Value;//QrAPLCredito.Value;
    //
    if UpperCase(Application.Title) = 'SYNKER' then
    begin
      if QrAPL.RecordCount > 0 then
        Pago := Pago - QrAPLMultaVal.Value - QrAPLMoraVal.Value;
    end;
    //
    if D_VLA > 0 then
    begin
      //SaldoCred  := 0;
      if Pago > D_VLA then
        SaldoDeb := 0
      else
        SaldoDeb := - (D_VLA  - Pago);
      Pago       := - Pago;
      SaldoXXX   := SaldoDeb;
    end else begin
      if Pago > C_VLA then
        SaldoCred := 0
      else
        SaldoCred    := C_VLA  - Pago;
      //SaldoDeb    := 0;
      SaldoXXX    := SaldoCred;
    end;

    // SIT
    if SaldoXXX = 0 then
    begin
      if QrAPLItens.Value > 1 then
        SitAtu := 2
      else
        SitAtu := 3;
    end else
    if Pago <> 0 then
      SitAtu := 1
    else
      SitAtu := 0;

    // DATA
    if (SitAtu in ([2,3])) then
    begin
      if QrAPL.RecordCount > 0 then
        Data := Geral.FDT(QrAPLData.Value, 1)
      else
      if (DataCompQuit <> '') and
      (DataCompQuit <> '0000-00-00') and (DataCompQuit <> '1899-12-30') and
      (DataCompQuit <> '0000/00/00') and (DataCompQuit <> '1899/12/30') then
        Data := DataCompQuit
      else begin
        Geral.MensagemBox(
        'ERRO na obten��o de dados para compensa��o / quita��o' + #13#10 +
        'Avise a Dermatek!', 'ERRO', MB_OK + MB_ICONERROR);
        Exit;
      end;
    end;
    //
    Result := True;
  end;
end;

function TDmodFin.GeraBalanceteRecDebi(Periodo, CliInt: Integer;
Acordos, Periodos, Textos: Boolean): Boolean;
var
  DataI, DataF: String;
  //Mez: Integer;
begin
  Screen.Cursor := crHourGlass;
  DataI := Geral.FDT(MLAGeral.PrimeiroDiaDoPeriodo_Date(Periodo), 1);
  DataF := Geral.FDT(MLAGeral.UltimoDiaDoPeriodo_Date(Periodo), 1);
  //Mez := MLAGeral.PeriodoToMez(Periodo-1);
  //
  ReopenCreditosEDebitos(CliInt, DataI, DataF, Acordos, Periodos, Textos, False, 0);
  {
  QrCreditos.Close;
  QrCreditos.Params[00].AsInteger := CliInt;
  QrCreditos.Params[01].AsString  := DataI;
  QrCreditos.Params[02].AsString  := DataF;
  QrCreditos.Open;
  //
  QrDebitos.Close;
  QrDebitos.Params[00].AsInteger := CliInt;
  QrDebitos.Params[01].AsString  := DataI;
  QrDebitos.Params[02].AsString  := DataF;
  QrDebitos.Open;
  }//
  ReopenSaldoAEResumo(CliInt, 0, DataI, DataF);
{
  QrSaldoA.Close;
  QrSaldoA.Params[00].AsInteger := CliInt;
  QrSaldoA.Params[01].AsString  := DataI;
  QrSaldoA.Params[02].AsInteger := CliInt;
  QrSaldoA.Open;
  // Deve ser ap�s SaldoA
  QrResumo.Close;
  QrResumo.Params[00].AsInteger := CliInt;
  QrResumo.Params[01].AsString  := DataI;
  QrResumo.Params[02].AsString  := DataF;
  QrResumo.Open;
}

  // Criado para agilizar as SQL abaixo (por LOOKUP ao inv�s de LEFT JOIN )
  {
  QrLanct2.Close;
  QrLanct2.Params[00].AsInteger := CliInt;
  QrLanct2.Open;
  //   DEMORADO...  ok
  QrPgCNAB.Close;
  QrPgCNAB.Params[00].AsInteger := Mez;
  QrPgCNAB.Params[01].AsInteger := CliInt;
  QrPgCNAB.Open;
  //   DEMORADO
  QrSuCNAB.Close;
  QrSuCNAB.Params[00].AsInteger := Mez;
  QrSuCNAB.Params[01].AsInteger := CliInt;
  QrSuCNAB.Open;
  }
  //   DEMORADO
  //n�o funciona o lookup
  {
  QrPgBloq_.Close;
  QrPgBloq_.Params[00].AsInteger := CliInt;
  QrPgBloq_.Params[01].AsInteger := Mez;
  QrPgBloq_.Open;
  //
  }
{  S� no SYNDIC ?


  // Verificar erro de soma de pagamento
  // por enquanto estou usando "credito" + "PagJur" + "PagMul" o certo � "Pago"
  QrPgBloq.Close;
  QrPgBloq.Params[00].AsInteger := CliInt;
  QrPgBloq.Params[01].AsInteger := Mez;
  QrPgBloq.Open;
  //ShowMessage(IntToStr(QrPgBloq.RecordCount));
  //   DEMORADO
  // Mesmo erro acima colocado linha "AND lan.Compensado>0" al�m de
  //  "credito" + "PagJur" + "PagMul" o certo � "Pago"
  // QrSuBloqKGT.Value > KeepGroupTogether
  QrSuBloq.Close;
  QrSuBloq.Params[00].AsInteger := CliInt;
  QrSuBloq.Params[01].AsInteger := Mez;
  QrSuBloq.Open;
  //
  Screen.Cursor := crDefault;
  //
  Result := True;

}
  Result := True;
end;

function TDmodFin.GeraDadosSaldos(Data: TDateTime; Exclusivo, NaoZerados:
Boolean): Boolean;
  procedure InsereRegistros(Qry: TmySQLQuery; Tipo: Integer);
  begin
    Qry.First;
    //
    while not Qry.Eof do
    begin
      if (NaoZerados = False) or (Qry.FieldByName('Saldo').AsFloat <> 0) then
      begin
        UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FSaldos, False, [
        'CartCodi', 'CartTipo', 'CartNiv2',
        'Nome', 'Saldo', 'Tipo'], [
        ], [
        Qry.FieldByName('CartCodi').AsInteger,
        Qry.FieldByName('CartTipo').AsInteger,
        Qry.FieldByName('CartNiv2').AsInteger,
        Qry.FieldByName('Nome').AsString,
        Qry.FieldByName('Saldo').AsFloat,
        Tipo], [
        ], False);
      end;
      //
      Qry.Next;
    end;
  end;
var
  DtTxt: String;
  Qry: TmySQLQuery;
begin
  //Result := False;
  //
  Qry:= TmySQLQuery.Create(DmodFin);
  Qry.Database := Dmod.MyDB;
  //
  DtTxt := FormatDateTime(VAR_FORMATDATE, Data);
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  FSaldos := UCriarFin.RecriaTempTableNovo(ntrtt_Saldos, DModG.QrUpdPID1, False);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT ca.Codigo CartCodi, ca.Tipo CartTipo, ca.CartNiv2, ',
  'ca.Nome, (SUM(la.Credito-la.Debito) + ca.Inicial) Saldo',
  'FROM lanctos la',
  'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira',
  'WHERE la.Tipo < 2',
  'AND la.Data<="' + DtTxt + '"',
  'AND ca.Exclusivo<=' + FormatFloat('0', Geral.BoolToInt(Exclusivo)),
  'GROUP BY ca.Nome',
  '']);
  InsereRegistros(Qry, 1);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT 0 CartCodi, -1 CartTipo, 0 CartNiv2, ',
  'co.Descricao Nome, SUM(ci.Debito-ci.Credito) Saldo',
  'FROM consignacoesits ci, Consignacoes co',
  'WHERE ci.Codigo=co.Codigo',
  'AND ci.Data<="' + DtTxt + '"',
  'GROUP BY co.Codigo',
  '']);
  InsereRegistros(Qry, 2);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT 0 CartCodi, -1 CartTipo, 0 CartNiv2, ',
  'ca.Nome,',
  ' SUM(la.Credito - la.Debito) SALDO',
  'FROM ' + VAR_LCT + ' la',
  'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira',
  'WHERE la.Tipo=2',
  'AND la.Data<="' + DtTxt + '"',
  'AND (la.Compensado="1899/12/30" ',
  ' OR Compensado="" OR la.Compensado=0 ',
  '  OR la.Compensado>"' + DtTxt + '")',
  'GROUP BY la.Carteira',
  '']);
  InsereRegistros(Qry, 3);
  //
  Result := True;
end;

procedure TDmodFin.AtualizaContasMensais2(PeriodoIni, PeriodoFim, Genero: Integer;
 PB1, PB2, PB3: TProgressBar; ST1, ST2, ST3: TLabel);
  procedure InsereRegistro(Codigo, Periodo, Tipo: Integer;
    Fator, ValFator, Devido: Double; Nome: String);
  var
    Pago: Double;
  begin
    QrRealizado.Close;
    QrRealizado.Params[0].AsInteger := Codigo;
    QrRealizado.Params[1].AsInteger := Periodo;
    QrRealizado.Open;
    Pago := QrRealizadoValor.Value;
    DModG.QrUpdPID1.Params[00].AsInteger := Codigo;
    DModG.QrUpdPID1.Params[01].AsString  := Nome;
    DModG.QrUpdPID1.Params[02].AsInteger := Periodo;
    DModG.QrUpdPID1.Params[03].AsInteger := Tipo;
    DModG.QrUpdPID1.Params[04].AsFloat   := Fator;
    DModG.QrUpdPID1.Params[05].AsFloat   := ValFator;
    DModG.QrUpdPID1.Params[06].AsFloat   := -Devido;
    DModG.QrUpdPID1.Params[07].AsFloat   := -Pago;
    DModG.QrUpdPID1.ExecSQL;
  end;
const
  Txt = 'WHERE la.Genero in (%s)';
var
  ValFator, Devido, Acuml, Difer, Fator: Double;
  i, n, p, PeriodoHoje, Conta: Integer;
  ContasSelc: String;
begin
  //////////////////////////////////////////////////////////////////////////////
  //  Contas Mensais
  //////////////////////////////////////////////////////////////////////////////
  PeriodoHoje := Geral.Periodo2000(date);
  Conta       := 0;
  Acuml       := 0;
  //
  FCtasResMes := UCriar.RecriaTempTable('CtasResMes', DModG.QrUpdPID1, False);
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO '+FCtasResMes+' SET ');
  DModG.QrUpdPID1.SQL.Add('Conta     =:P00,  ');
  DModG.QrUpdPID1.SQL.Add('Nome      =:P01, ');
  DModG.QrUpdPID1.SQL.Add('Periodo   =:P02, ');
  DModG.QrUpdPID1.SQL.Add('Tipo      =:P03, ');
  DModG.QrUpdPID1.SQL.Add('Fator     =:P04, ');
  DModG.QrUpdPID1.SQL.Add('ValFator  =:P05, ');
  DModG.QrUpdPID1.SQL.Add('Devido    =:P06, ');
  DModG.QrUpdPID1.SQL.Add('Pago      =:P07');
  //
  QrCIni.Close;
  QrCIni.SQL.Clear;
  QrCIni.SQL.Add('SELECT co.Codigo, MIN(ci.Periodo) Minimo, co.Nome, ');
  QrCIni.SQL.Add('MAX(ci.Periodo) Maximo, co.PendenMesSeg, co.CalculMesSeg');
  QrCIni.SQL.Add('FROM contas co');
  QrCIni.SQL.Add('LEFT JOIN contasits ci ON ci.Codigo=co.Codigo');
  QrCIni.SQL.Add('WHERE ci.Codigo IS NOT NULL');
  QrCIni.SQL.Add('AND co.Ativo=1');
  if Genero <> 0 then
    QrCIni.SQL.Add('AND co.Codigo="' + IntToStr(Genero)+'"');
  if PeriodoFim > PeriodoIni then
    QrCIni.SQL.Add('AND ci.Periodo<="' + IntToStr(PeriodoFim)+'"');
  QrCIni.SQL.Add('GROUP BY co.Codigo');
  //
  QrCIni.Open;
  if PB1 <> nil then
  begin
    PB1.Position := 0;
    PB1.Max := QrCIni.RecordCount;
  end;
  if ST1 <> nil then
  begin
    ST1.Caption := 'Pesquisando valores de pend�ncias por conta mensal...';
    ST1.Update;
  end;  
  Application.ProcessMessages;
  while not QrCIni.Eof do
  begin
    if PB1 <> nil then PB1.Position := PB1.Position + 1;
    if PeriodoFim < QrCIniMaximo.Value then
      n := PeriodoFim else n := QrCIniMaximo.Value;
    if PB2 <> nil then
    begin
      PB2.Position := 0;
      PB2.Max := n - QrCIniMinimo.Value + 1;
    end;
    if ST2 <> nil then
    begin
      ST2.Caption := 'Informando pend�ncias de "' + QrCIniNome.Value + '"...';
      ST2.Update;
    end;  
    Application.ProcessMessages;
    for i := QrCIniMinimo.Value to n do
    begin
      if PB2 <> nil then PB2.Position := PB2.Position + 1;
      QrCIts1.Close;
      QrCIts1.Params[00].AsInteger := QrCIniCodigo.Value;
      QrCIts1.Params[01].AsInteger := i;
      QrCIts1.Open;
      if QrCIniCalculMesSeg.Value = 1 then
      begin
        QrCIts2.Close;
        QrCIts2.Params[00].AsInteger := QrCIniCodigo.Value;
        QrCIts2.Params[01].AsInteger := i-1;
        QrCIts2.Open;
      end;
      //
      if QrCIniCalculMesSeg.Value = 1 then
        Devido := QrCIts2Fator.Value
      else
        Devido := QrCIts1Fator.Value;
      if QrCIts1Tipo.Value = 0 then
      begin
        ValFator := QrCIts1Fator.Value;
      end else begin
        p := i;
        if QrCIniCalculMesSeg.Value = 1 then p := p - 1;
        QrItsCtas.Close;
        QrItsCtas.Params[0].AsInteger := QrCIniCodigo.Value;
        QrItsCtas.Params[1].AsInteger := p;
        QrItsCtas.Open;
        ContasSelc := '';
        if PB3 <> nil then
        begin
          PB3.Position := 0;
          PB3.Max      := QrItsCtas.RecordCount;
        end;
        if ST3 <> nil then
        begin
          ST3.Caption := 'Pesquisando contas fonte de "' + QrCIniNome.Value + '"...';
          ST3.Update;
        end;
        Application.ProcessMessages;
        while not QrItsCtas.Eof do
        begin
          if PB3 <> nil then PB3.Position := PB3.Position + 1;
          ContasSelc := ContasSelc + IntToStr(QrItsCtasConta.Value) +',';
          QrItsCtas.Next;
        end;
        ContasSelc := Copy(ContasSelc, 1, Length(ContasSelc)-1);
        //
        if Trim(ContasSelc) = '' then
        begin
          ValFator := 0;
          Devido   := 0;
        end else begin
          QrValFator.Close;
          QrValFator.SQL[2] := Format(Txt, [ContasSelc]);
          QrValFator.Params[0].AsInteger := p;

          QrValFator.Open;
          ValFator := QrValFatorValor.Value;
          Devido := QrValFatorValor.Value * Devido / 100;
        end;
      end;
      case QrCIniPendenMesSeg.Value of
        0: if PeriodoHoje < i then Fator := 0 else Fator := 1;
        1: Fator := 1;
        2:
        begin
         if PeriodoHoje = i then Fator := MLAGeral.PorcentagemMes
         else if PeriodoHoje < i then Fator := 0 else Fator := 1;
        end;
        else Fator := 1;
      end;
      //ver positivos ou s� negativos!
      if Devido > 0 then
        Devido := Devido * Fator
      else
        if Devido < 0 then
          ShowMessage(Geral.FFT(Devido, 2, siNegativo));  
      InsereRegistro(QrCIniCodigo.Value, i, QrCIts1Tipo.Value,
        QrCIts1Fator.Value, ValFator, Devido, QrCIniNome.Value);
    end;
    if ST2 <> nil then
    begin
      ST2.Caption := '...';
      ST2.Update;
    end;
    if ST3 <> nil then
    begin
      ST3.Caption := '...';
      ST3.Update;
    end;
    Application.ProcessMessages;
    QrCIni.Next;
  end;
  //
  QrCtasResMes2.Close;
  QrCtasResMes2.Database := DModG.MyPID_DB;
  QrCtasResMes2.SQL.Clear;
  QrCtasResMes2.SQL.Add('SELECT * FROM ' + FCtasResMes);
  QrCtasResMes2.Open;
  if PB1 <> nil then
  begin
    PB1.Position := 0;
    PB1.Max      := QrCtasResMes2.RecordCount;
  end;
  if ST1 <> nil then
  begin
    ST1.Caption := 'Atualizando valores acumulados das pend�ncias ...';
    ST1.Update;
  end;
  Application.ProcessMessages;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE '+FCtasResMes+' SET ');
  DModG.QrUpdPID1.SQL.Add('Diferenca =:P00,');
  DModG.QrUpdPID1.SQL.Add('Acumulado =:P01 WHERE ');
  DModG.QrUpdPID1.SQL.Add('Conta     =:Pa AND ');
  DModG.QrUpdPID1.SQL.Add('Periodo   =:Pb');
  while not QrCtasResMes2.Eof do
  begin
    if PB1 <> nil then PB1.Position := PB1.Position + 1;
    if Conta <> QrCtasResMes2Conta.Value then
    begin
      Conta := QrCtasResMes2Conta.Value;
      Acuml := 0;
    end;
    Difer := QrCtasResMes2Devido.Value + QrCtasResMes2Pago.Value;
    Acuml := Acuml + Difer;
    DModG.QrUpdPID1.Params[00].AsFloat   := Difer;
    DModG.QrUpdPID1.Params[01].AsFloat   := Acuml;
    DModG.QrUpdPID1.Params[02].AsFloat   := QrCtasResMes2Conta.Value;
    DModG.QrUpdPID1.Params[03].AsFloat   := QrCtasResMes2Periodo.Value;
    DModG.QrUpdPID1.ExecSQL;
    QrCtasResMes2.Next;
  end;
  QrCtasAnt.Close;
  QrCtasAnt.Database := DModG.MyPID_DB;
  QrCtasAnt.Params[0].AsInteger := PeriodoIni;
  QrCtasAnt.Open;
  if PB1 <> nil then
  begin
    PB1.Position := 0;
    PB1.Max      := QrCtasAnt.RecordCount;
  end;
  if ST1 <> nil then
  begin
    ST1.Caption := 'Pesquisando pend�ncias anteriores das contas mensais...';
    ST1.Update;
  end;
  Application.ProcessMessages;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO '+FCtasResMes+' SET ');
  DModG.QrUpdPID1.SQL.Add('Conta     =:P00, ');
  DModG.QrUpdPID1.SQL.Add('Nome      =:P01, ');
  DModG.QrUpdPID1.SQL.Add('Periodo   =:P02, ');
  DModG.QrUpdPID1.SQL.Add('Tipo      =:P03, ');
  DModG.QrUpdPID1.SQL.Add('Acumulado =:P04, ');
  DModG.QrUpdPID1.SQL.Add('SeqImp    =-1');
  while not QrCtasAnt.Eof do
  begin
    if PB1 <> nil then PB1.Position := PB1.Position + 1;
    DModG.QrUpdPID1.Params[00].AsInteger := QrCtasAntConta.Value;
    DModG.QrUpdPID1.Params[01].AsString  := QrCtasAntNome.Value;
    DModG.QrUpdPID1.Params[02].AsInteger := -1;
    DModG.QrUpdPID1.Params[03].AsInteger := -1;
    DModG.QrUpdPID1.Params[04].AsFloat   := QrCtasAntAcumulado.Value;
    DModG.QrUpdPID1.ExecSQL;
    QrCtasAnt.Next;
  end;
  if ST1 <> nil then ST1.Caption := '...';
  if ST2 <> nil then ST2.Caption := '...';
  if ST3 <> nil then ST3.Caption := '...';
  Application.ProcessMessages;
end;

procedure TDmodFin.ImprimeSaldos(CliInt: Integer);
begin
  QrSaldos.Close;
  QrSaldos.Params[0].AsInteger := CliInt;
  QrSaldos.Open;
  while not QrSaldos.Eof do
  begin
    UFinanceiro.RecalcSaldoCarteira(QrSaldosCodigo.Value, nil, False, False);
    QrSaldos.Next;
  end;

  //


  FSaldos := UCriarFin.RecriaTempTableNovo(ntrtt_Saldos, DModG.QrUpdPID1, False);
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO saldos SET ');
  DModG.QrUpdPID1.SQL.Add('Nome=:P0, Saldo=:P1, Tipo=:P2');
  QrSaldos.Close;
  QrSaldos.Params[0].AsInteger := CliInt;
  QrSaldos.Open;
  while not QrSaldos.Eof do
  begin
    DModG.QrUpdPID1.Params[00].AsString  := QrSaldosNome.Value;
    DModG.QrUpdPID1.Params[01].AsFloat   := QrSaldosSaldo.Value;
    DModG.QrUpdPID1.Params[02].AsInteger := 1;
    DModG.QrUpdPID1.ExecSQL;
    QrSaldos.Next;
  end;
  //
  QrConsigna.Close;
  QrConsigna.Params[0].AsInteger := CliInt;
  QrConsigna.Open;
  while not QrConsigna.Eof do
  begin
    DModG.QrUpdPID1.Params[00].AsString  := QrConsignaNome.Value;
    DModG.QrUpdPID1.Params[01].AsFloat   := QrConsignaSaldo.Value;
    DModG.QrUpdPID1.Params[02].AsInteger := 2;
    DModG.QrUpdPID1.ExecSQL;
    QrConsigna.Next;
  end;
  //
  QrSaldos.Close;
  QrConsigna.Close;
  QrTotalSaldo.Close;
  QrTotalSaldo.Database := DModG.MyPID_DB;
  QrTotalSaldo.Close;
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  MyObjects.frxMostra(frxSaldos, 'Saldos');
end;

procedure TDmodFin.AtualizaEndossos(Controle, Sub, OriCtrl, OriSub: Integer);
  procedure Atualiza_Atual(C, S: Integer);
  var
    D, N: Double;
    Sit1, Sit2: Boolean;
    Status: Byte;
  begin
    if (C = 0) and (S = 0) then Exit;
    //
    QrEndossad.Close;
    QrEndossad.Params[00].AsInteger := C;
    QrEndossad.Params[01].AsInteger := S;
    QrEndossad.Open;
    D := QrEndossadValor.Value;
    //
    QrEndossan.Close;
    QrEndossan.Params[00].AsInteger := C;
    QrEndossan.Params[01].AsInteger := S;
    QrEndossan.Open;
    N := QrEndossanValor.Value;
    //
    Sit1 := D <> 0;
    Sit2 := N <> 0;
    //
    if Sit1 and Sit2 then
      Status := 3 //  Ambos
    else
    if Sit1 then
      Status := 1 // Endossado
    else
    if Sit2 then
      Status := 2  // Endossante
    else Status := 0; // Nenhum
    //
    {
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET ');
    Dmod.QrUpd.SQL.Add('Endossas=:P0, Endossad=:P1, Endossan=:P2 ');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa AND Sub=:Pb');
    //
    Dmod.QrUpd.Params[00].AsInteger := Status;
    Dmod.QrUpd.Params[01].AsFloat   := D;
    Dmod.QrUpd.Params[02].AsFloat   := N;
    //
    Dmod.QrUpd.Params[03].AsInteger := C;
    Dmod.QrUpd.Params[04].AsInteger := S;
    //
    Dmod.QrUpd.ExecSQL;
    }
    UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
    'Endossas', 'Endossad', 'Endossan'], ['Controle', 'Sub'], [
    Status, D, N], [C, S], True, '');
    //
    UFinanceiro.AtualizaEmissaoMasterRapida(C, 0, 0, '');
  end;
begin
  Atualiza_Atual(Controle, Sub);
  Atualiza_Atual(OriCtrl, OriSub);
end;

procedure TDmodFin.DefineDataSets_frxSaldos(Frx: TfrxReport);
begin
  Frx.DataSets.Clear;
  Frx.DataSets.Add(DModG.frxDsDono);
  Frx.DataSets.Add(DModG.frxDsMaster);
  Frx.DataSets.Add(DmodFin.frxDsTotalSaldo);
end;

end.

