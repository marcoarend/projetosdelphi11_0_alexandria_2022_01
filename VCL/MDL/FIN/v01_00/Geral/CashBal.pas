unit CashBal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkEdit, dmkEditCB, Db,
  mySQLDbTables, DBCtrls, dmkDBLookupComboBox, ComCtrls, dmkEditDateTimePicker,
  frxClass, frxDBSet, dmkGeral, Grids, DBGrids, dmkCheckGroup, Math, dmkDBGrid,
  dmkDBGridDAC, dmkCheckBox, UnDmkProcFunc, UnDmkEnums;

type
  TFmCashBal = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    QrEmp: TmySQLQuery;
    DsEmp: TDataSource;
    Panel4: TPanel;
    frxBal_A_01: TfrxReport;
    frxBal_A_02: TfrxReport;
    QrEntiRespon: TmySQLQuery;
    QrEntiResponNO_RESPON: TWideStringField;
    QrEntiResponNO_CARGO: TWideStringField;
    frxDsEntiRespon: TfrxDBDataset;
    frxBal: TfrxReport;
    QrEmpCodigo: TIntegerField;
    QrEmpNO_UF: TWideStringField;
    QrEmpNO_EMPRESA: TWideStringField;
    QrEmpCIDADE: TWideStringField;
    QrEntiCfgRel_01: TmySQLQuery;
    QrEntiCfgRel_01Codigo: TIntegerField;
    QrEntiCfgRel_01Controle: TIntegerField;
    QrEntiCfgRel_01RelTipo: TIntegerField;
    QrEntiCfgRel_01RelTitu: TWideStringField;
    QrEntiCfgRel_01Ordem: TIntegerField;
    QrEntiCfgRel_01RelPrcr: TWideMemoField;
    QrEntiCfgRel_01RelItem: TIntegerField;
    QrEntiCfgRel_01Genero: TIntegerField;
    frxDsEntiCfgRel_01: TfrxDBDataset;
    frxBal_A_03: TfrxReport;
    QrArr1: TmySQLQuery;
    QrArr1NO_ENT: TWideStringField;
    QrArr1Cliente: TIntegerField;
    QrArr1ANO: TLargeintField;
    QrArr1MES: TLargeintField;
    QrArr1Valor: TFloatField;
    QrAnt1: TmySQLQuery;
    QrAnt1Cliente: TIntegerField;
    QrAnt1Valor: TFloatField;
    frxDsArre1: TfrxDBDataset;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    DsArre1: TDataSource;
    QrArr2: TmySQLQuery;
    QrArr2NO_ENT: TWideStringField;
    QrArr2Cliente: TIntegerField;
    QrArr2Mez: TIntegerField;
    QrArr2Valor: TFloatField;
    frxDsArre2: TfrxDBDataset;
    DBGrid2: TDBGrid;
    DsArre2: TDataSource;
    frxBal_A_04: TfrxReport;
    QrAnt2: TmySQLQuery;
    QrAnt2NO_ENT: TWideStringField;
    QrAnt2Cliente: TIntegerField;
    QrAnt2Valor: TFloatField;
    frxBal_A_05: TfrxReport;
    TabSheet4: TTabSheet;
    DBGrid3: TDBGrid;
    TabSheet5: TTabSheet;
    DBGrid4: TDBGrid;
    TabSheet6: TTabSheet;
    DBGrid5: TDBGrid;
    frxBal_A_06: TfrxReport;
    QrLCS: TmySQLQuery;
    QrLCSNOMEPLANO: TWideStringField;
    QrLCSPLANO: TIntegerField;
    QrLCSNOMECONJUNTO: TWideStringField;
    QrLCSConjunto: TIntegerField;
    QrLCSNOMEGRUPO: TWideStringField;
    QrLCSGrupo: TIntegerField;
    QrLCSNOMESUBGRUPO: TWideStringField;
    QrLCSSubGrupo: TIntegerField;
    QrLCSNOMECONTA: TWideStringField;
    QrLCSGenero: TIntegerField;
    QrLCSValor: TFloatField;
    frxDsLCS: TfrxDBDataset;
    frxBal_A_07: TfrxReport;
    frxBal_A_08: TfrxReport;
    QrCtasResMes: TmySQLQuery;
    QrCtasResMesConta: TIntegerField;
    QrCtasResMesNome: TWideStringField;
    QrCtasResMesPeriodo: TIntegerField;
    QrCtasResMesTipo: TIntegerField;
    QrCtasResMesFator: TFloatField;
    QrCtasResMesValFator: TFloatField;
    QrCtasResMesDevido: TFloatField;
    QrCtasResMesPago: TFloatField;
    QrCtasResMesDiferenca: TFloatField;
    QrCtasResMesAcumulado: TFloatField;
    QrCtasResMesNOME_PERIODO: TWideStringField;
    frxDsCtasResMes: TfrxDBDataset;
    frxBal_A_09: TfrxReport;
    QrExclusivos: TmySQLQuery;
    QrExclusivosMovim: TFloatField;
    frxDsExclusivos: TfrxDBDataset;
    QrExclMov: TmySQLQuery;
    QrExclMovCredito: TFloatField;
    QrExclMovDebito: TFloatField;
    QrExclusivosCREDITO: TFloatField;
    QrExclusivosDEBITO: TFloatField;
    QrExclusivosANTERIOR: TFloatField;
    QrOrdinarios: TmySQLQuery;
    QrOrdiMov: TmySQLQuery;
    QrOrdiMovCredito: TFloatField;
    QrOrdiMovDebito: TFloatField;
    QrOrdinariosMovim: TFloatField;
    QrOrdinariosCREDITO: TFloatField;
    QrOrdinariosDEBITO: TFloatField;
    QrOrdinariosANTERIOR: TFloatField;
    frxDsOrdinarios: TfrxDBDataset;
    QrEmpCO_SHOW: TLargeintField;
    QrAnt1NO_ENT: TWideStringField;
    BitBtn1: TBitBtn;
    QrEntiCfgRel_01LogoPath: TWideStringField;
    Panel7: TPanel;
    EdTempo: TdmkEdit;
    Label3: TLabel;
    QrDat1: TmySQLQuery;
    QrDat2: TmySQLQuery;
    QrDat1Cliente: TIntegerField;
    QrDat1NO_ENT: TWideStringField;
    QrDat1Valr01: TFloatField;
    QrDat1Valr02: TFloatField;
    QrDat1Valr03: TFloatField;
    QrDat1Valr04: TFloatField;
    QrDat1Valr05: TFloatField;
    QrDat1Valr06: TFloatField;
    QrDat1Valr07: TFloatField;
    QrDat1Valr08: TFloatField;
    QrDat1Valr09: TFloatField;
    QrDat1Valr10: TFloatField;
    QrDat1Valr11: TFloatField;
    QrDat1Valr12: TFloatField;
    QrDat1ValrA0: TFloatField;
    QrDat1ValrM0: TFloatField;
    QrDat1ValrA1: TFloatField;
    QrDat1ValrM1: TFloatField;
    QrDat1SEQ: TIntegerField;
    QrDat2Cliente: TIntegerField;
    QrDat2NO_ENT: TWideStringField;
    QrDat2Valr01: TFloatField;
    QrDat2Valr02: TFloatField;
    QrDat2Valr03: TFloatField;
    QrDat2Valr04: TFloatField;
    QrDat2Valr05: TFloatField;
    QrDat2Valr06: TFloatField;
    QrDat2Valr07: TFloatField;
    QrDat2Valr08: TFloatField;
    QrDat2Valr09: TFloatField;
    QrDat2Valr10: TFloatField;
    QrDat2Valr11: TFloatField;
    QrDat2Valr12: TFloatField;
    QrDat2ValrA0: TFloatField;
    QrDat2ValrM0: TFloatField;
    QrDat2ValrA1: TFloatField;
    QrDat2ValrM1: TFloatField;
    QrDat2SEQ: TIntegerField;
    BtSdoIni: TBitBtn;
    QrLC2: TmySQLQuery;
    QrSSdo: TmySQLQuery;
    QrSSdoCodigo: TIntegerField;
    QrSSdoValor: TFloatField;
    QrLC2NOMEPLANO: TWideStringField;
    QrLC2PLANO: TIntegerField;
    QrLC2NOMECONJUNTO: TWideStringField;
    QrLC2Conjunto: TIntegerField;
    QrLC2NOMEGRUPO: TWideStringField;
    QrLC2Grupo: TIntegerField;
    QrLC2NOMESUBGRUPO: TWideStringField;
    QrLC2SubGrupo: TIntegerField;
    QrLC2NOMECONTA: TWideStringField;
    QrLC2Genero: TIntegerField;
    QrLC2Valor: TFloatField;
    QrLC2OL_PLA: TIntegerField;
    QrLC2OL_CJT: TIntegerField;
    QrLC2OL_GRU: TIntegerField;
    QrLC2OL_SGR: TIntegerField;
    QrLC2OL_CON: TIntegerField;
    frxBal_A_07_: TfrxReport;
    QrSN1: TmySQLQuery;
    QrSN2: TmySQLQuery;
    QrSN1Movim: TFloatField;
    QrSN2Valor: TFloatField;
    QrLC2vSGR: TFloatField;
    QrLC2vGRU: TFloatField;
    QrLC2vCJT: TFloatField;
    QrLC2vPLA: TFloatField;
    QrEntiResponObserv: TWideStringField;
    frxBal_A_11: TfrxReport;
    frxBal_A_12B: TfrxReport;
    frxBal_A_12A: TfrxReport;
    QrEntiCfgRel_01FonteTam: TSmallintField;
    QrEmpCliInt: TIntegerField;
    frxBal_A_13: TfrxReport;
    frxBal_A_14: TfrxReport;
    frxBal_A_15: TfrxReport;
    Panel5: TPanel;
    Panel8: TPanel;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    RGFormato: TRadioGroup;
    GroupBox2: TGroupBox;
    CkAcordos: TCheckBox;
    CkPeriodos: TCheckBox;
    CkTextos: TCheckBox;
    GroupBox1: TGroupBox;
    LaAno: TLabel;
    LaMes: TLabel;
    CBMes: TComboBox;
    CBAno: TComboBox;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    LaAviso: TLabel;
    Panel6: TPanel;
    LaSub1: TLabel;
    LaSub2: TLabel;
    LaSub3: TLabel;
    PB1: TProgressBar;
    PB2: TProgressBar;
    PB3: TProgressBar;
    Panel9: TPanel;
    QrLista: TmySQLQuery;
    DsLista: TDataSource;
    QrListaControle: TIntegerField;
    QrListaRelTitu: TWideStringField;
    QrListaOrdem: TIntegerField;
    QrListaAtivo: TSmallintField;
    DBGLista: TdmkDBGridDAC;
    QrSels: TmySQLQuery;
    QrSelsItens: TLargeintField;
    frxBalancete2: TfrxReport;
    CGResumido: TdmkCheckGroup;
    QrDebitos: TmySQLQuery;
    QrDebitosDebito: TFloatField;
    QrDebitosNOMECON: TWideStringField;
    QrDebitosNOMESGR: TWideStringField;
    QrDebitosNOMEGRU: TWideStringField;
    frxDsDebitos: TfrxDBDataset;
    CkPaginar: TdmkCheckBox;
    TPData: TdmkEditDateTimePicker;
    Label2: TLabel;
    CGTipoDoc: TdmkCheckGroup;
    frxBal_A_16: TfrxReport;
    Qr16: TmySQLQuery;
    TabSheet3: TTabSheet;
    DBGrid6: TDBGrid;
    Ds16: TDataSource;
    frxDs16: TfrxDBDataset;
    Qr16NO_CART: TWideStringField;
    Qr16Carteira: TIntegerField;
    Qr16SerieCH: TWideStringField;
    Qr16Documento: TFloatField;
    Qr16Debito: TFloatField;
    Qr16Qtde: TFloatField;
    Qr16Descricao: TWideStringField;
    Qr16Data: TDateField;
    Qr16Genero: TIntegerField;
    Qr16NO_GENERO: TWideStringField;
    Qr16NotaFiscal: TIntegerField;
    Qr16Vencimento: TDateField;
    Qr16Compensado: TDateField;
    Qr16Controle: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrEmpCalcFields(DataSet: TDataSet);
    procedure frxBal_A_01GetValue(const VarName: string; var Value: Variant);
    procedure QrCtasResMesCalcFields(DataSet: TDataSet);
    procedure QrExclusivosCalcFields(DataSet: TDataSet);
    procedure QrOrdinariosCalcFields(DataSet: TDataSet);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdEmpresaExit(Sender: TObject);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrDat1CalcFields(DataSet: TDataSet);
    procedure QrDat2CalcFields(DataSet: TDataSet);
    procedure BtSdoIniClick(Sender: TObject);
    procedure RGFormatoClick(Sender: TObject);
  private
    { Private declarations }
    FDataI, FDataF: String;
    FAtzContasMensais2: Boolean ;
    FCfgRel: String;
    FPeriodoAtu, FPeriodoAnt, FPeriodoIni, FPeriodoFim: Integer;
    FArre1, FArre2, FDataAnt, FDataIni, FDataAtu: String;
    FResumido_01, FResumido_02, FResumido_03,
    FResumido_04, FResumido_05, FResumido_06: Boolean;
    FIni, FFim, FTempo: TDateTime;
    //function PeriodoNaoDefinido(CliInt, Periodo: Integer): Boolean;
    function frxComposite(frxAtu: TfrxReport; var ClearLastReport: Boolean):
             Boolean;
    //procedure PreparaBal_A_03();
    //procedure PreparaBal_A_04();
    procedure PreparaBal_A_03();
    procedure PreparaBal_A_04();
    procedure AtualizaItensAImprimir();
    procedure ImprimeReduzido();
    procedure ImprimeCompleto(Mez, PBB: Integer);
    function ChCompensadosPeriodo(Empresa: Integer): Boolean;
  public
    { Public declarations }
    //FPBB: Integer;
    FPaginar: Boolean;
    FEntidade,
    FClienteInicial,
    FPeriodoInicial: Integer;
    FConfiguInicial: Boolean;
  end;

  var
  FmCashBal: TFmCashBal;

implementation

uses UnInternalConsts, UnFinanceiro, ModuleGeral, ModuleFin, Module, UCreate,
UnMyObjects, MeuFrx, Resmes, ContasHistSdo3, CashBalIni, MyDBCheck;

{$R *.DFM}

procedure TFmCashBal.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCashBal.BtTudoClick(Sender: TObject);
begin
  //CGItensImp.Value := MLAGeral.MaxValueCheckGroup(CGItensImp.Items.Count);
  DBGLista.AtivaTodos;
end;

function TFmCashBal.ChCompensadosPeriodo(Empresa: Integer): Boolean;
var
  TiposDoc: String;
begin
  Result := False;
  TiposDoc := CGTipoDoc.GetIndexesChecked(True, 0);
  if TiposDoc = '' then
  begin
    Geral.MensagemBox('Nenhum tipo de documento foi definido'+#13#10+
    'O relat�rio de documentos compensados no per�odo n�o ser� gerado!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  try
    Qr16.Close;
    Qr16.SQL.Clear;
    Qr16.SQL.Add('SELECT car.Nome NO_CART, lan.Carteira, lan.SerieCH, ');
    Qr16.SQL.Add('lan.Documento,lan.Debito, lan.Qtde, lan.Descricao, lan.Data, ');
    Qr16.SQL.Add('lan.Genero,cta.Nome NO_GENERO, lan.Controle, lan.NotaFiscal,');
    Qr16.SQL.Add('lan.Vencimento, lan.Compensado');
    Qr16.SQL.Add('FROM lanctos lan');
    Qr16.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    Qr16.SQL.Add('LEFT JOIN contas cta ON cta.Codigo=lan.Genero');
    Qr16.SQL.Add('WHERE car.Tipo=2');
    Qr16.SQL.Add('AND lan.Debito > 0');
    Qr16.SQL.Add('AND car.TipoDoc IN ('+TiposDoc+')');
    Qr16.SQL.Add('AND car.ForneceI=' + FormatFloat('0', Empresa));
    Qr16.SQL.Add('AND lan.Compensado BETWEEN "' + FDataI + '" AND "' + FDataF + '"');
    Qr16.SQL.Add('ORDER BY car.TipoDoc, car.Nome, car.Codigo, SerieCH, Documento');
    Qr16.Open;
    //
    Result := True;
  finally
    //
  end;
end;

procedure TFmCashBal.EdEmpresaChange(Sender: TObject);
begin
  if not EdEmpresa.Focused then
    AtualizaItensAImprimir();
end;

procedure TFmCashBal.EdEmpresaExit(Sender: TObject);
begin
  AtualizaItensAImprimir();
end;

procedure TFmCashBal.FormActivate(Sender: TObject);
var
  Mes, Ano: Word;
begin
  MyObjects.CorIniComponente();
  if not FConfiguInicial then
  begin
    FConfiguInicial := True;
    if FClienteInicial <> 0 then
    begin
      if QrEmp.Locate('Cliente', FClienteInicial, []) then
      begin
        EdEmpresa.ValueVariant := QrEmpCodigo.Value;
        CBEmpresa.KeyValue     := QrEmpCodigo.Value;
      end;
    end;
    if FPeriodoInicial > -1000 then
    begin
      dmkPF.PeriodoDecode(FPeriodoInicial, Ano, Mes);
      CBAno.Text      := FormatFloat('0', Ano);
      CBMes.ItemIndex := Mes - 1;
    end;
  end;
end;

procedure TFmCashBal.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCashBal.frxBal_A_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_PERIODO_TXT' then
    Value := Geral.Maiusculas(MyObjects.CBAnoECBMesToPeriodoTxt(CBAno, CBMes), False)
  else if VarName = 'VARF_PAGINAR' then
    Value := CkPaginar.Checked
  else if AnsiCompareText(VarName, 'VARF_ULTIMODIA') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE3,
    MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoAtu))
  else if AnsiCompareText(VarName, 'VARF_PERIODO') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE6,
    MLAGeral.PrimeiroDiaDoPeriodo_Date(FPeriodoAtu)) + ' at� ' +
    FormatDateTime(VAR_FORMATDATE6,
    MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoAtu))
  {else if AnsiCompareText(VarName, 'VARF_SALDO_INICIAL') = 0 then Value :=
    DCond.SaldoInicialCarteira(MLAGeral.PrimeiroDiaDoPeriodo_Date(
      FPeriodoAtu), QrExtratosCarteira.Value)}
  else if AnsiCompareText(VarName, 'MES_ANO ') = 0 then Value :=
    MLAGeral.MesEAnoDoPeriodoLongo(FPeriodoAtu)
  else if AnsiCompareText(VarName, 'CIDADE  ') = 0 then Value :=
    QrEmpCIDADE.Value
  else if AnsiCompareText(VarName, 'SIGLA_UF') = 0 then Value :=
    QrEmpNO_UF.Value
  else if AnsiCompareText(VarName, 'DATA_ATU') = 0 then Value :=
    Geral.FDT(Date, 6)
  else if AnsiCompareText(VarName, 'DATA_FIM') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE6,
    MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoAtu))
  //
  //Evitar erro de usu�rio ( ou pr�prio)
  //
  {else begin
    Application.MessageBox(PChar('A vari�vel "' + VarName + '" n�o est� ' +
    'implementada nesta impress�o! Verifique o texto do Parecer do Conselho ' +
    'no cadastro do condom�nio (Gerencia Clientes). Se o problema persistir ' +
    'entre em contato com a DERMATEK!'), 'Aviso', MB_OK+MB_ICONWARNING);
    Value := '';
  end;}
  else if AnsiCompareText(VarName, 'VAR_DATA_PER_ANT') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE2,
    MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoAtu-1))
  else if AnsiCompareText(VarName, 'VAR_DATA_PER_FIM') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE2,
    MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoAtu))
  {
  else if AnsiCompareText(VarName, 'VARF_TOPONOMCON') = 0 then
    Value := int(Dmod.QrControleBAL_TopoNomCon.Value / VAR_frCM)
  else if AnsiCompareText(VarName, 'VARF_TOPOTITULO') = 0 then
    Value := int(Dmod.QrControleBAL_TopoTitulo.Value / VAR_frCM)
  else if AnsiCompareText(VarName, 'VARF_TOPOPERIOD') = 0 then
    Value := int(Dmod.QrControleBAL_TopoPeriod.Value / VAR_frCM)
  }
  else if Copy(VarName, 1, 5) = 'VMES_' then
    Value := Copy(VarName, 6) + ' / ' + CBAno.Text
  else if VarName = 'VARF_ANO1' then
    Value := 'Total ' + CBAno.Text
  else if VarName = 'VARF_ANO0' then
    Value := 'Total ' + FormatFloat('0', Geral.IMV(CBAno.Text) -1)
  else if VarName = 'VARF_MID1' then
    Value := 'M�dia ' + CBAno.Text
  else if VarName = 'VARF_MID0' then
    Value := 'M�dia ' + FormatFloat('0', Geral.IMV(CBAno.Text) -1)
  else if VarName = 'LogoBalanceteExiste' then
    Value := FileExists(QrEntiCfgRel_01LogoPath.Value)
  else if VarName = 'LogoBalancetePath' then
    Value := QrEntiCfgRel_01LogoPath.Value
  else if VarName = 'VARF_EMPRESA' then
    Value := CBEmpresa.Text
  else if VarName = 'VARF_RESUMIDO_01' then Value := FResumido_01
  else if VarName = 'VARF_RESUMIDO_02' then Value := FResumido_02
  else if VarName = 'VARF_RESUMIDO_03' then Value := FResumido_03
  else if VarName = 'VARF_RESUMIDO_04' then Value := FResumido_04
  else if VarName = 'VARF_RESUMIDO_05' then Value := FResumido_05
  else if VarName = 'VARF_RESUMIDO_06' then Value := FResumido_06
  else if VarName ='VARF_DATA' then Value := TPData.Date
  else
end;

procedure TFmCashBal.PreparaBal_A_03();
  procedure IncluiLinhaSQLAnoAtu(CliCod: Integer; CliNom, SQL: String; ValMeses: Array of Double);
  var
    I: Integer;
    Vals: String;
    Tot, Mid: Double;
  begin
    Vals := '';
    Vals := Vals + dmkPF.FFP(CliCod, 0) + ',';
    Vals := Vals + '"' + CliNom + '",';
    Tot := 0;
    for I := 0 to 11 do
    begin
      Vals := Vals + dmkPF.FFP(ValMeses[I], 2) + ',';
      Tot := Tot + ValMeses[I];
    end;
    Mid := Tot / (CBMes.ItemIndex + 1);
    Vals := Vals + dmkPF.FFP(Tot, 2) + ',';
    Vals := Vals + dmkPF.FFP(Mid, 2) + ',';
    if QrAnt1.Locate('Cliente', CliCod, []) then
      Tot := QrAnt1Valor.Value
    else Tot := 0;
    Mid := Tot / 12;
    Vals := Vals + dmkPF.FFP(Tot, 2) + ',';
    Vals := Vals + dmkPF.FFP(Mid, 2) + ',';
    Vals := Vals + '1);';
    //
    QrDat1.SQL.Add(SQL + Vals);
  end;
  procedure IncluiLinhaSQLAnoAnt(CliCod: Integer; CliNom, SQL: String; ValAnt: Double);
  var
    I: Integer;
    Vals: String;
    Mid: Double;
  begin
    Vals := '';
    Vals := Vals + dmkPF.FFP(CliCod, 0) + ',';
    Vals := Vals + '"' + CliNom + '",';
    for I := 0 to 13 do
      Vals := Vals + dmkPF.FFP(0, 2) + ',';
    Mid := ValAnt / 12;
    Vals := Vals + dmkPF.FFP(ValAnt, 2) + ',';
    Vals := Vals + dmkPF.FFP(Mid, 2) + ',';
    Vals := Vals + '1);';
    //
    QrDat1.SQL.Add(SQL + Vals);
  end;
var
  ValMeses: array[0..11] of Double;
  I, Cli: Integer;
  Vals, Nom, SQL: String;
begin
  MyObjects.Informa(LaSub1, True, 'Abrindo tabelas de consulta');
  //FArre1 := UCriar.RecriaTempTable('Arre', DmodG.QrUpdPID1, False, 0, 'Arre1');
  FArre1 := UCriar.RecriaTempTableNovo(ntrttArre, DmodG.QrUpdPID1, False, 0, 'Arre1');
  //
  QrDat1.Close;
  QrDat1.SQL.Clear;
  //
  QrAnt1.Close;
  QrAnt1.Params[00].AsInteger := QrEntiCfgRel_01Genero.Value;
  QrAnt1.Params[01].AsString  := FDataAnt;
  QrAnt1.Params[02].AsString  := FDataIni;
  QrAnt1.Params[03].AsInteger := FEntidade;
  QrAnt1.Open;
  //
  QrArr1.Close;
  QrArr1.Params[00].AsInteger := QrEntiCfgRel_01Genero.Value;
  QrArr1.Params[01].AsString  := FDataIni;
  QrArr1.Params[02].AsString  := FDataAtu;
  QrArr1.Params[03].AsInteger := FEntidade;
  QrArr1.Open;
  //
  SQL := 'INSERT INTO arre1 (Cliente,NO_ENT,Valr01,Valr02,Valr03,Valr04,' +
  'Valr05,Valr06,Valr07,Valr08,Valr09,Valr10,Valr11,Valr12,ValrA1,ValrM1,' +
  'ValrA0,ValrM0,Ativo) Values(';
  QrArr1.First;
  Vals := '';
  Cli := QrArr1Cliente.Value;
  Nom := QrArr1NO_ENT.Value;
  for I := 0 to 11 do
    ValMeses[I] := 0;
 //
  MyObjects.Informa(LaSub1, True, 'Incluindo dados do per�odo selecionado');
  PB1.Position := 0;
  PB1.Max := QrArr1.RecordCount;
  while not QrArr1.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    MyObjects.Informa(LaSub2, True, QrArr1NO_ENT.Value);
    //
    if Cli <> QrArr1Cliente.Value then
    begin
      if Cli > -100000 then
      begin
        IncluiLinhaSQLAnoAtu(Cli, Nom, SQL, ValMeses);
        Cli := QrArr1Cliente.Value;
        Nom := QrArr1NO_ENT.Value;
      end;
      for I := 0 to 11 do
        ValMeses[I] := 0;
    end;
    ValMeses[QrArr1MES.Value-1] := QrArr1Valor.Value;
    QrArr1.Next;
  end;
  IncluiLinhaSQLAnoAtu(Cli, Nom, SQL, ValMeses);
  //
  // Entidades que tem movimento no ano anterior mas n�o no atual!!
  for I := 0 to 11 do
    ValMeses[I] := 0;
  QrAnt1.First;
  //
  MyObjects.Informa(LaSub1, True, 'Incluindo dados do per�odo anterior');
  PB1.Position := 0;
  PB1.Max := QrAnt1.RecordCount;
  while not QrAnt1.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    MyObjects.Informa(LaSub2, True, QrAnt1NO_ENT.Value);
    //
    Cli := QrAnt1Cliente.Value;
    Nom := QrAnt1NO_ENT.Value;
    if not QrArr1.Locate('Cliente', Cli, []) then
      IncluiLinhaSQLAnoAnt(Cli, Nom, SQL, QrAnt1Valor.Value);
    QrAnt1.Next;
  end;
  //
  MyObjects.Informa(LaSub1, True, 'Abrindo tabela com resultados');
  QrDat1.ExecSQL;
  //
  QrDat1.SQL.Clear;
  QrDat1.SQL.Add('SELECT * FROM arre1 ');
  QrDat1.SQL.Add('ORDER BY NO_ENT; ');
  QrDat1.Open;
  //
  MyObjects.Informa(LaSub1, False, '...');
  MyObjects.Informa(LaSub2, False, '...');
  PB1.Position := 0;
end;

procedure TFmCashBal.PreparaBal_A_04();
  procedure IncluiLinhaSQLAnoAtu(CliCod: Integer; CliNom, SQL: String; ValMeses: Array of Double);
  var
    I: Integer;
    Vals: String;
    Tot, Mid: Double;
  begin
    Vals := '';
    Vals := Vals + dmkPF.FFP(CliCod, 0) + ',';
    Vals := Vals + '"' + CliNom + '",';
    Tot := 0;
    for I := 0 to 11 do
    begin
      Vals := Vals + dmkPF.FFP(ValMeses[I], 2) + ',';
      Tot := Tot + ValMeses[I];
    end;
    Mid := Tot / (CBMes.ItemIndex + 1);
    Vals := Vals + dmkPF.FFP(Tot, 2) + ',';
    Vals := Vals + dmkPF.FFP(Mid, 2) + ',';
    if QrAnt2.Locate('Cliente', CliCod, []) then
      Tot := QrAnt2Valor.Value
    else Tot := 0;
    Mid := Tot / 12;
    Vals := Vals + dmkPF.FFP(Tot, 2) + ',';
    Vals := Vals + dmkPF.FFP(Mid, 2) + ',';
    Vals := Vals + '1);';
    //
    QrDat2.SQL.Add(SQL + Vals);
  end;
  procedure IncluiLinhaSQLAnoAnt(CliCod: Integer; CliNom, SQL: String; ValAnt: Double);
  var
    I: Integer;
    Vals: String;
    Mid: Double;
  begin
    Vals := '';
    Vals := Vals + dmkPF.FFP(CliCod, 0) + ',';
    Vals := Vals + '"' + CliNom + '",';
    for I := 0 to 13 do
      Vals := Vals + dmkPF.FFP(0, 2) + ',';
    Mid := ValAnt / 12;
    Vals := Vals + dmkPF.FFP(ValAnt, 2) + ',';
    Vals := Vals + dmkPF.FFP(Mid, 2) + ',';
    Vals := Vals + '1);';
    //
    QrDat2.SQL.Add(SQL + Vals);
  end;
var
  ValMeses: array[0..11] of Double;
  I, Cli, Mes: Integer;
  Vals, Nom, SQL: String;
begin
  MyObjects.Informa(LaSub1, True, 'Abrindo tabelas de consulta');
  //FArre2 := UCriar.RecriaTempTable('Arre', DmodG.QrUpdPID1, False, 0, 'Arre2');
  FArre2 := UCriar.RecriaTempTableNovo(ntrttArre, DmodG.QrUpdPID1, False, 0, 'Arre2');
  QrDat2.Close;
  QrDat2.SQL.Clear;
  //
  QrAnt2.Close;
  QrAnt2.Params[00].AsInteger := QrEntiCfgRel_01Genero.Value;
  QrAnt2.Params[01].AsInteger := FPeriodoAnt;
  QrAnt2.Params[02].AsInteger := FPeriodoIni;
  QrAnt2.Params[03].AsString  := FDataAtu;
  QrAnt2.Params[04].AsInteger := FEntidade;
  QrAnt2.Open;
  //
  QrArr2.Close;
  QrArr2.Params[00].AsInteger := QrEntiCfgRel_01Genero.Value;
  QrArr2.Params[01].AsInteger := FPeriodoIni;
  QrArr2.Params[02].AsInteger := FPeriodoFim;
  QrArr2.Params[03].AsString  := FDataAtu;
  QrArr2.Params[04].AsInteger := FEntidade;
  QrArr2.Open;
  //
  SQL := 'INSERT INTO arre2 (Cliente,NO_ENT,Valr01,Valr02,Valr03,Valr04,' +
  'Valr05,Valr06,Valr07,Valr08,Valr09,Valr10,Valr11,Valr12,ValrA1,ValrM1,' +
  'ValrA0,ValrM0,Ativo) Values(';
  QrArr2.First;
  Vals := '';
  Cli := QrArr2Cliente.Value;
  Nom := QrArr2NO_ENT.Value;
  for I := 0 to 11 do
    ValMeses[I] := 0;
  //
  MyObjects.Informa(LaSub1, True, 'Incluindo dados do per�odo selecionado');
  PB1.Position := 0;
  PB1.Max := QrArr2.RecordCount;
  while not QrArr2.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    MyObjects.Informa(LaSub2, True, QrArr2NO_ENT.Value);
    //
    if Cli <> QrArr2Cliente.Value then
    begin
      if Cli > -100000 then
      begin
        IncluiLinhaSQLAnoAtu(Cli, Nom, SQL, ValMeses);
        Cli := QrArr2Cliente.Value;
        Nom := QrArr2NO_ENT.Value;
      end;
      for I := 0 to 11 do
        ValMeses[I] := 0;
    end;
    Mes := QrArr2Mez.Value mod 100;
    ValMeses[Mes-1] := QrArr2Valor.Value;
    QrArr2.Next;
  end;
  IncluiLinhaSQLAnoAtu(Cli, Nom, SQL, ValMeses);
  //
  // Entidades que tem movimento no ano anterior mas n�o no atual!!
  for I := 0 to 11 do
    ValMeses[I] := 0;
  QrAnt2.First;
  //
  MyObjects.Informa(LaSub1, True, 'Incluindo dados do per�odo anterior');
  PB1.Position := 0;
  PB1.Max := QrAnt2.RecordCount;
  while not QrAnt2.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    MyObjects.Informa(LaSub2, True, QrAnt2NO_ENT.Value);
    //
    Cli := QrAnt2Cliente.Value;
    Nom := QrAnt2NO_ENT.Value;
    if not QrArr2.Locate('Cliente', Cli, []) then
      IncluiLinhaSQLAnoAnt(Cli, Nom, SQL, QrAnt2Valor.Value);
    QrAnt2.Next;
  end;
  //
  MyObjects.Informa(LaSub1, True, 'Abrindo tabela com resultados');
  if QrDat2.SQL.Text <> '' then
    QrDat2.ExecSQL;
  //
  QrDat2.SQL.Clear;
  QrDat2.SQL.Add('SELECT * FROM arre2 ');
  QrDat2.SQL.Add('ORDER BY NO_ENT; ');
  QrDat2.Open;
  //
  MyObjects.Informa(LaSub1, False, '...');
  MyObjects.Informa(LaSub2, False, '...');
  PB1.Position := 0;
end;

{
procedure TFmCashBal.PreparaBal_A_03();
  procedure IncluiLinhaSQLAnoAtu(CliCod: Integer; CliNom, SQL: String; ValMeses: Array of Double);
  var
    I: Integer;
    Vals: String;
    Tot, Mid: Double;
  begin
    Vals := '';
    Vals := Vals + dmkPF.FFP(CliCod, 0) + ',';
    Vals := Vals + '"' + CliNom + '",';
    Tot := 0;
    for I := 0 to 11 do
    begin
      Vals := Vals + dmkPF.FFP(ValMeses[I], 2) + ',';
      Tot := Tot + ValMeses[I];
    end;
    Mid := Tot / (CBMes.ItemIndex + 1);
    Vals := Vals + dmkPF.FFP(Tot, 2) + ',';
    Vals := Vals + dmkPF.FFP(Mid, 2) + ',';
    if QrAnt1.Locate('Cliente', CliCod, []) then
      Tot := QrAnt1Valor.Value
    else Tot := 0;
    Mid := Tot / 12;
    Vals := Vals + dmkPF.FFP(Tot, 2) + ',';
    Vals := Vals + dmkPF.FFP(Mid, 2) + ',';
    Vals := Vals + '1);';
    //
    QrArre1.SQL.Add(SQL + Vals);
  end;
  procedure IncluiLinhaSQLAnoAnt(CliCod: Integer; CliNom, SQL: String; ValAnt: Double);
  var
    I: Integer;
    Vals: String;
    Mid: Double;
  begin
    Vals := '';
    Vals := Vals + dmkPF.FFP(CliCod, 0) + ',';
    Vals := Vals + '"' + CliNom + '",';
    for I := 0 to 13 do
      Vals := Vals + dmkPF.FFP(0, 2) + ',';
    Mid := ValAnt / 12;
    Vals := Vals + dmkPF.FFP(ValAnt, 2) + ',';
    Vals := Vals + dmkPF.FFP(Mid, 2) + ',';
    Vals := Vals + '1);';
    //
    QrArre1.SQL.Add(SQL + Vals);
  end;
var
  ValMeses: array[0..11] of Double;
  I, Cli: Integer;
  Vals, Nom, SQL: String;
begin
  QrAnt1.Close;
  QrAnt1.Params[00].AsInteger := QrEntiCfgRel_01Genero.Value;
  QrAnt1.Params[01].AsString  := FDataAnt;
  QrAnt1.Params[02].AsString  := FDataIni;
  QrAnt1.Params[03].AsInteger := FEntidade;
  QrAnt1.Open;
  //
  QrArr1.Close;
  QrArr1.Params[00].AsInteger := QrEntiCfgRel_01Genero.Value;
  QrArr1.Params[01].AsString  := FDataIni;
  QrArr1.Params[02].AsString  := FDataAtu;
  QrArr1.Params[03].AsInteger := FEntidade;
  QrArr1.Open;
  //
  QrArre1.Close;
  QrArre1.SQL.Clear;
  QrArre1.SQL.Add('  DROP TABLE Arre1;                          ');
  QrArre1.SQL.Add('  CREATE TABLE Arre1 (                       ');
  QrArre1.SQL.Add('  Cliente      INTEGER,                      ');
  QrArre1.SQL.Add('  NO_ENT       VARCHAR(100),                 ');
  QrArre1.SQL.Add('  Valr01       FLOAT,                        ');
  QrArre1.SQL.Add('  Valr02       FLOAT,                        ');
  QrArre1.SQL.Add('  Valr03       FLOAT,                        ');
  QrArre1.SQL.Add('  Valr04       FLOAT,                        ');
  QrArre1.SQL.Add('  Valr05       FLOAT,                        ');
  QrArre1.SQL.Add('  Valr06       FLOAT,                        ');
  QrArre1.SQL.Add('  Valr07       FLOAT,                        ');
  QrArre1.SQL.Add('  Valr08       FLOAT,                        ');
  QrArre1.SQL.Add('  Valr09       FLOAT,                        ');
  QrArre1.SQL.Add('  Valr10       FLOAT,                        ');
  QrArre1.SQL.Add('  Valr11       FLOAT,                        ');
  QrArre1.SQL.Add('  Valr12       FLOAT,                        ');
  QrArre1.SQL.Add('  ValrA1       FLOAT,                        ');
  QrArre1.SQL.Add('  ValrM1       FLOAT,                        ');
  QrArre1.SQL.Add('  ValrA0       FLOAT,                        ');
  QrArre1.SQL.Add('  ValrM0       FLOAT,                        ');
  QrArre1.SQL.Add('  Ativo        SHORTINT                      ');
  QrArre1.SQL.Add('  );');
  //
  SQL := 'INSERT INTO arre1 (Cliente,NO_ENT,Valr01,Valr02,Valr03,Valr04,' +
  'Valr05,Valr06,Valr07,Valr08,Valr09,Valr10,Valr11,Valr12,ValrA1,ValrM1,' +
  'ValrA0,ValrM0,Ativo) Values(';
  QrArr1.First;
  Vals := '';
  Cli := QrArr1Cliente.Value;
  Nom := QrArr1NO_ENT.Value;
  for I := 0 to 11 do
    ValMeses[I] := 0;
 //
  MyObjects.Informa(LaSub1, True, 'Incluindo dados do per�odo selecionado');
  PB1.Position := 0;
  PB1.Max := QrArr1.RecordCount;
  while not QrArr1.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    MyObjects.Informa(LaSub2, True, QrArr1NO_ENT.Value);
    //
    if Cli <> QrArr1Cliente.Value then
    begin
      if Cli > -100000 then
      begin
        IncluiLinhaSQLAnoAtu(Cli, Nom, SQL, ValMeses);
        Cli := QrArr1Cliente.Value;
        Nom := QrArr1NO_ENT.Value;
      end;
      for I := 0 to 11 do
        ValMeses[I] := 0;
    end;
    ValMeses[QrArr1MES.Value-1] := QrArr1Valor.Value;
    QrArr1.Next;
  end;
  IncluiLinhaSQLAnoAtu(Cli, Nom, SQL, ValMeses);
  //
  // Entidades que tem movimento no ano anterior mas n�o no atual!!
  for I := 0 to 11 do
    ValMeses[I] := 0;
  QrAnt1.First;
  //
  MyObjects.Informa(LaSub1, True, 'Incluindo dados do per�odo anterior');
  PB1.Position := 0;
  PB1.Max := QrAnt1.RecordCount;
  while not QrAnt1.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    MyObjects.Informa(LaSub2, True, QrAnt1NO_ENT.Value);
    //
    Cli := QrAnt1Cliente.Value;
    Nom := QrAnt1NO_ENT.Value;
    if not QrArr1.Locate('Cliente', Cli, []) then
      IncluiLinhaSQLAnoAnt(Cli, Nom, SQL, QrAnt1Valor.Value);
    QrAnt1.Next;
  end;
  //
  MyObjects.Informa(LaSub1, True, 'Abrindo tabela com resultados');
  QrArre1.SQL.Add('SELECT * FROM arre1 ');
  QrArre1.SQL.Add('ORDER BY NO_ENT; ');
  QrArre1.Open;
  //
  MyObjects.Informa(LaSub1, False, '...');
  MyObjects.Informa(LaSub2, False, '...');
  PB1.Position := 0;
end;

procedure TFmCashBal.PreparaBal_A_04();
  procedure IncluiLinhaSQLAnoAtu(CliCod: Integer; CliNom, SQL: String; ValMeses: Array of Double);
  var
    I: Integer;
    Vals: String;
    Tot, Mid: Double;
  begin
    Vals := '';
    Vals := Vals + dmkPF.FFP(CliCod, 0) + ',';
    Vals := Vals + '"' + CliNom + '",';
    Tot := 0;
    for I := 0 to 11 do
    begin
      Vals := Vals + dmkPF.FFP(ValMeses[I], 2) + ',';
      Tot := Tot + ValMeses[I];
    end;
    Mid := Tot / (CBMes.ItemIndex + 1);
    Vals := Vals + dmkPF.FFP(Tot, 2) + ',';
    Vals := Vals + dmkPF.FFP(Mid, 2) + ',';
    if QrAnt2.Locate('Cliente', CliCod, []) then
      Tot := QrAnt2Valor.Value
    else Tot := 0;
    Mid := Tot / 12;
    Vals := Vals + dmkPF.FFP(Tot, 2) + ',';
    Vals := Vals + dmkPF.FFP(Mid, 2) + ',';
    Vals := Vals + '1);';
    //
    QrArre2.SQL.Add(SQL + Vals);
  end;
  procedure IncluiLinhaSQLAnoAnt(CliCod: Integer; CliNom, SQL: String; ValAnt: Double);
  var
    I: Integer;
    Vals: String;
    Mid: Double;
  begin
    Vals := '';
    Vals := Vals + dmkPF.FFP(CliCod, 0) + ',';
    Vals := Vals + '"' + CliNom + '",';
    for I := 0 to 13 do
      Vals := Vals + dmkPF.FFP(0, 2) + ',';
    Mid := ValAnt / 12;
    Vals := Vals + dmkPF.FFP(ValAnt, 2) + ',';
    Vals := Vals + dmkPF.FFP(Mid, 2) + ',';
    Vals := Vals + '1);';
    //
    QrArre2.SQL.Add(SQL + Vals);
  end;
var
  ValMeses: array[0..11] of Double;
  I, Cli, Mes: Integer;
  Vals, Nom, SQL: String;
begin
  MyObjects.Informa(LaSub1, True, 'Abrindo tabelas de consulta');
  //FArre2 := UCriar.RecriaTempTable('Arre', DmodG.QrUpdPID1, False, 0, 'Arre2');
  FArre2 := UCriar.RecriaTempTableNovo(ntrttArre, DmodG.QrUpdPID1, False, 0, 'Arre2');
 ///
  QrAnt2.Close;
  QrAnt2.Params[00].AsInteger := QrEntiCfgRel_01Genero.Value;
  QrAnt2.Params[01].AsInteger := FPeriodoAnt;
  QrAnt2.Params[02].AsInteger := FPeriodoIni;
  QrAnt2.Params[03].AsString  := FDataAtu;
  QrAnt2.Params[04].AsInteger := FEntidade;
  QrAnt2.Open;
  //
  QrArr2.Close;
  QrArr2.Params[00].AsInteger := QrEntiCfgRel_01Genero.Value;
  QrArr2.Params[01].AsInteger := FPeriodoIni;
  QrArr2.Params[02].AsInteger := FPeriodoFim;
  QrArr2.Params[03].AsString  := FDataAtu;
  QrArr2.Params[04].AsInteger := FEntidade;
  QrArr2.Open;
  //
  QrArre2.Close;
  QrArre2.SQL.Clear;
  QrArre2.SQL.Add('  DROP TABLE Arre2;                          ');
  QrArre2.SQL.Add('  CREATE TABLE Arre2 (                       ');
  QrArre2.SQL.Add('  Cliente      INTEGER,                      ');
  QrArre2.SQL.Add('  NO_ENT       VARCHAR(100),                 ');
  QrArre2.SQL.Add('  Valr01       FLOAT,                        ');
  QrArre2.SQL.Add('  Valr02       FLOAT,                        ');
  QrArre2.SQL.Add('  Valr03       FLOAT,                        ');
  QrArre2.SQL.Add('  Valr04       FLOAT,                        ');
  QrArre2.SQL.Add('  Valr05       FLOAT,                        ');
  QrArre2.SQL.Add('  Valr06       FLOAT,                        ');
  QrArre2.SQL.Add('  Valr07       FLOAT,                        ');
  QrArre2.SQL.Add('  Valr08       FLOAT,                        ');
  QrArre2.SQL.Add('  Valr09       FLOAT,                        ');
  QrArre2.SQL.Add('  Valr10       FLOAT,                        ');
  QrArre2.SQL.Add('  Valr11       FLOAT,                        ');
  QrArre2.SQL.Add('  Valr12       FLOAT,                        ');
  QrArre2.SQL.Add('  ValrA1       FLOAT,                        ');
  QrArre2.SQL.Add('  ValrM1       FLOAT,                        ');
  QrArre2.SQL.Add('  ValrA0       FLOAT,                        ');
  QrArre2.SQL.Add('  ValrM0       FLOAT,                        ');
  QrArre2.SQL.Add('  Ativo        SHORTINT                      ');
  QrArre2.SQL.Add('  );');
  //
  SQL := 'INSERT INTO arre2 (Cliente,NO_ENT,Valr01,Valr02,Valr03,Valr04,' +
  'Valr05,Valr06,Valr07,Valr08,Valr09,Valr10,Valr11,Valr12,ValrA1,ValrM1,' +
  'ValrA0,ValrM0,Ativo) Values(';
  QrArr2.First;
  Vals := '';
  Cli := QrArr2Cliente.Value;
  Nom := QrArr2NO_ENT.Value;
  for I := 0 to 11 do
    ValMeses[I] := 0;
  //
  MyObjects.Informa(LaSub1, True, 'Incluindo dados do per�odo selecionado');
  PB1.Position := 0;
  PB1.Max := QrArr2.RecordCount;
  while not QrArr2.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    MyObjects.Informa(LaSub2, True, QrArr2NO_ENT.Value);
    //
    if Cli <> QrArr2Cliente.Value then
    begin
      if Cli > -100000 then
      begin
        IncluiLinhaSQLAnoAtu(Cli, Nom, SQL, ValMeses);
        Cli := QrArr2Cliente.Value;
        Nom := QrArr2NO_ENT.Value;
      end;
      for I := 0 to 11 do
        ValMeses[I] := 0;
    end;
    Mes := QrArr2Mez.Value mod 100;
    ValMeses[Mes-1] := QrArr2Valor.Value;
    QrArr2.Next;
  end;
  IncluiLinhaSQLAnoAtu(Cli, Nom, SQL, ValMeses);
  //
  // Entidades que tem movimento no ano anterior mas n�o no atual!!
  for I := 0 to 11 do
    ValMeses[I] := 0;
  QrAnt2.First;
  //
  MyObjects.Informa(LaSub1, True, 'Incluindo dados do per�odo anterior');
  PB1.Position := 0;
  PB1.Max := QrAnt2.RecordCount;
  while not QrAnt2.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    MyObjects.Informa(LaSub2, True, QrAnt2NO_ENT.Value);
    //
    Cli := QrAnt2Cliente.Value;
    Nom := QrAnt2NO_ENT.Value;
    if not QrArr2.Locate('Cliente', Cli, []) then
      IncluiLinhaSQLAnoAnt(Cli, Nom, SQL, QrAnt2Valor.Value);
    QrAnt2.Next;
  end;
  //
  MyObjects.Informa(LaSub1, True, 'Abrindo tabela com resultados');
  QrArre2.SQL.Add('SELECT * FROM arre2 ');
  QrArre2.SQL.Add('ORDER BY NO_ENT; ');
  QrArre2.Open;
  //
  MyObjects.Informa(LaSub1, False, '...');
  MyObjects.Informa(LaSub2, False, '...');
  PB1.Position := 0;
end;
}

procedure TFmCashBal.QrCtasResMesCalcFields(DataSet: TDataSet);
begin
  QrCtasResMesNOME_PERIODO.Value :=
    MLAGeral.MesEAnoDoPeriodo(QrCtasResMesPeriodo.Value);
end;

procedure TFmCashBal.QrDat1CalcFields(DataSet: TDataSet);
begin
  QrDat1SEQ.Value := QrDat1.RecNo;
end;

procedure TFmCashBal.QrDat2CalcFields(DataSet: TDataSet);
begin
  QrDat2SEQ.Value := QrDat2.RecNo;
end;

procedure TFmCashBal.QrEmpCalcFields(DataSet: TDataSet);
begin
{
  if QrEmpCedente.Value = 0 then
    QrEmpNOMECED_IMP.Value := QrEmpNOMECLI.Value
  else
    QrEmpNOMECED_IMP.Value := QrEmpNOMECED.Value;

  //

  if QrEmpSacadAvali.Value = 0 then
    QrEmpNOMESAC_IMP.Value := ''
  else
    QrEmpNOMESAC_IMP.Value := QrEmpNOMESAC.Value;

}
end;

procedure TFmCashBal.QrExclusivosCalcFields(DataSet: TDataSet);
begin
  QrExclusivosCREDITO.Value := QrExclMovCredito.Value;
  QrExclusivosDEBITO.Value := QrExclMovDebito.Value;
  QrExclusivosANTERIOR.Value := QrExclusivosMovim.Value -
    QrExclMovDebito.Value + QrExclMovCredito.Value;
end;

procedure TFmCashBal.QrOrdinariosCalcFields(DataSet: TDataSet);
begin
  QrOrdinariosCREDITO.Value := QrOrdiMovCredito.Value;
  QrOrdinariosDEBITO.Value := QrOrdiMovDebito.Value;
  QrOrdinariosANTERIOR.Value := QrOrdinariosMovim.Value +
    QrOrdiMovDebito.Value - QrOrdiMovCredito.Value;
end;

procedure TFmCashBal.RGFormatoClick(Sender: TObject);
begin
  DBGLista.Visible   := False;
  CGResumido.Visible := False;
  case RGFormato.ItemIndex of
    0: DBGLista.Visible   := True;
    1: CGResumido.Visible := True;
  end;
end;

procedure TFmCashBal.FormCreate(Sender: TObject);
begin
  DmodFin.CarregaItensTipoDoc(CGTipoDoc);
  CGTipoDoc.Value := 2;
  CGResumido.Align := alClient;
  CGResumido.SetMaxValue;
  FCfgRel := UCriar.RecriaTempTable('CfgRel', DModG.QrUpdPID1, False);
  //
  FClienteInicial := 0;
  FPeriodoInicial := -1000;
  FConfiguInicial := False;
  QrEmp.Open;
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, -1);
  //////////////////////////////////////////////////////////////////////////
  MyObjects.Informa(LaAviso, False, '...');
  MyObjects.Informa(LaSub1, False, '...');
  MyObjects.Informa(LaSub2, False, '...');
  MyObjects.Informa(LaSub3, False, '...');
  //
  QrDat1.Database       := DModG.MyPID_DB;
  QrDat2.Database       := DModG.MyPID_DB;
  QrSSdo.Database       := DModG.MyPID_DB;
  QrSN1.Database        := DModG.MyPID_DB;
  QrSN2.Database        := DModG.MyPID_DB;
  QrSels.Database       := DModG.MyPID_DB;
  QrLista.Database      := DModG.MyPID_DB;
  QrCtasResMes.Database := DModG.MyPID_DB;
  //
  DmodFin.QrExtratos.Database := DModG.MyPID_DB;
  //
  TPData.Date := Date;
end;

procedure TFmCashBal.AtualizaItensAImprimir;
{
var
  RelItem: Integer;
  Valor: Extended;
}
begin
  {
  QrEntiCfgRel_01.Close;
  Valor := 0;
  if EdEmpresa.ValueVariant <> 0 then
  begin
    QrEntiCfgRel_01.Params[0].AsInteger := QrEmpCodigo.Value;
    QrEntiCfgRel_01.Open;
    //
    while not QrEntiCfgRel_01.Eof do
    begin
      RelItem := QrEntiCfgRel_01RelItem.Value;
      Valor := Valor + MLAGeral.IndexFromInt(RelItem);
      //
      QrEntiCfgRel_01.Next;
    end;
    //
    BtSdoIni.Enabled := QrEmpCodigo.Value <> 0;
    //
  end;
  CGItensImp.Value := Trunc(Valor);
  }
  //
  // N O V O
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('DELETE FROM cfgrel;');
  DModG.QrUpdPID1.SQL.Add('INSERT INTO cfgrel');
  DModG.QrUpdPID1.SQL.Add('SELECT Controle, RelTitu,');
  DModG.QrUpdPID1.SQL.Add('Ordem, 1 Ativo');
  DModG.QrUpdPID1.SQL.Add('FROM '+TMeuDB+'.enticfgrel');
  DModG.QrUpdPID1.SQL.Add('WHERE RelTipo=1');
  DModG.QrUpdPID1.SQL.Add('AND Codigo='+FormatFloat('0', QrEmpCodigo.Value)+';');
  DModG.QrUpdPID1.ExecSQL;
  QrLista.Close;
  QrLista.Open;
  //
  BtSdoIni.Enabled := (EdEmpresa.ValueVariant <> 0) and (QrEmpCodigo.Value <> 0);
end;

procedure TFmCashBal.BitBtn1Click(Sender: TObject);
var
  DataI, DataF: String;
  Ano, Mes: Word;
begin
  // N�o fazer!!! N�o vale a pena!!!
  /////////////////////
  EXIT;
  /////////////////
  if DmodFin.ExisteLancamentoSemCliInt() then Exit;
  Screen.Cursor := crHourGlass;
  //
  //AindaNaoAtzCtas := True;
  //
  FPeriodoAtu := ((Geral.IMV(CBAno.Text)-2000) * 12) + CBMes.ItemIndex + 1;
  DataI := Geral.FDT(MLAGeral.PrimeiroDiaDoPeriodo_Date(FPeriodoAtu), 1);
  DataF := Geral.FDT(MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoAtu), 1);
  //
  Ano := Geral.IMV(CBAno.Text);
  Mes := CBMes.ItemIndex + 1;
  //Mez := ((Ano - 2000) * 100) + Mes;
  FDataAnt := Geral.FDT(EncodeDate(Ano-1,1,1), 1);
  FDataIni := Geral.FDT(EncodeDate(Ano  ,1,1), 1);
  FPeriodoIni := (Ano - 2000) * 100 + 1;
  FPeriodoFim := (Ano - 2000) * 100 + 12;
  FPeriodoAnt := FPeriodoIni - 100;
  if Mes = 12 then
  begin
    Ano := Ano + 1;
    Mes := 1;
  end else Mes := Mes + 1;
  FDataAtu := Geral.FDT(EncodeDate(Ano, Mes, 1)-1, 1);
  //
  //
  if Application.MessageBox(PChar('Confirma o envio de todos lan�amentos de ' +
  'contas corentes e caixas emitidos at� a data abaixo e os lan�amentos de ' +
  'emiss�es quitados ou totalmente pagos at� a data abaixo para o cliente ' +
  'interno "' + QrEmpNO_EMPRESA.Value + '"?'#13#10'Data limite: ' +
  Geral.FDT(MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoAtu), 2)), 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('UPDATE ' + VAR_LCT + ' lan, carteiras car');
    DMod.QrUpd.SQL.Add('SET lan.Sit=3');
    DMod.QrUpd.SQL.Add('WHERE lan.Carteira=car.Codigo');
    DMod.QrUpd.SQL.Add('AND car.Tipo <> 2');
    DMod.QrUpd.SQL.Add('AND lan.Sit < 2');
    DMod.QrUpd.ExecSQL;
    //
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('REPLACE INTO lanctoz ');
    DMod.QrUpd.SQL.Add('SELECT lan.*');
    DMod.QrUpd.SQL.Add('FROM ' + VAR_LCT + ' lan');
    DMod.QrUpd.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    DMod.QrUpd.SQL.Add('WHERE Sit>1');
    DMod.QrUpd.SQL.Add('AND car.ForneceI=:P0');
    DMod.QrUpd.SQL.Add('AND Data <=:P1');
    DMod.QrUpd.Params[00].AsInteger := QrEmpCodigo.Value;
    DMod.QrUpd.Params[01].AsString  := DataF;
    DMod.QrUpd.ExecSQL;
    //
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('DELETE lan.*');  // DELETE FROM lanctos
    DMod.QrUpd.SQL.Add('FROM ' + VAR_LCT + ' lan, carteiras car');
    DMod.QrUpd.SQL.Add('WHERE car.Codigo=lan.Carteira');
    DMod.QrUpd.SQL.Add('AND lan.Sit>1');
    DMod.QrUpd.SQL.Add('AND car.ForneceI=:P0');
    DMod.QrUpd.SQL.Add('AND Data <=:P1');
    DMod.QrUpd.Params[00].AsInteger := QrEmpCodigo.Value;
    DMod.QrUpd.Params[01].AsString  := DataF;
    DMod.QrUpd.ExecSQL;
    //
  end;

  //
  Screen.Cursor := crDefault;
end;

procedure TFmCashBal.BtSdoIniClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    FPeriodoAtu := ((Geral.IMV(CBAno.Text) - 2000) * 12) + CBMes.ItemIndex + 1;
    // Atualizar hist�rico dos saldos das contas
    DmodFin.AtualizaContasHistSdo3(QrEmpCodigo.Value, LaSub1);
    //
    if DBCheck.CriaFm(TFmCashBalIni, FmCashBalIni, afmoNegarComAviso) then
    begin
      // Abrir tabelas dos saldos de contas e seus n�veis (sub-grupos, grupos, conjuntos e planos)
      DModFin.SaldosNiveisPeriodo_2(QrEmpCodigo.Value, FPeriodoAtu, -1,
        False, PB1, PB1, PB1, PB1, PB1, FmCashBalIni.QrSNG);
      //
      MyObjects.Informa(LaSub1, True, 'Verificando saldos iniciais das contas controladas');
      FmCashBalIni.AtualizaIniOld();
      MyObjects.Informa(LaSub1, False, '...');
      FmCashBalIni.ShowModal;
      FmCashBalIni.Destroy;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCashBal.BtNenhumClick(Sender: TObject);
begin
  //CGItensImp.Value := 0;
  DBGLista.DesativaTodos();
end;

procedure TFmCashBal.BtOKClick(Sender: TObject);
var
  //CLR: Boolean; //CLR = ClearLastReport
  PBB, (*RelItem, *)Mez, Ano, Mes: Integer;
begin
  FAtzContasMensais2 := False;
  FIni := Now();
  Screen.Cursor := crHourGlass;
  QrSels.Close;
  QrSels.Open;
  if MyObjects.FIC(QrSelsItens.Value = 0, nil,
    'Selecione pelo menos um relat�rio!') then Exit;
  FEntidade := QrEmpCodigo.Value;
  //
  MyObjects.Informa(LaAviso, True, 'Verificando lan�amentos sem Cliente Interno');
  if DmodFin.ExisteLancamentoSemCliInt() then
  begin
    MyObjects.Informa(LaAviso, False, '...');
    Screen.Cursor := crDefault;
    Exit;
  end;
  MyObjects.Informa(LaAviso, True, 'Preparando consultas');
  //
  FPeriodoAtu := ((Geral.IMV(CBAno.Text)-2000) * 12) + CBMes.ItemIndex + 1;
  FDataI := Geral.FDT(MLAGeral.PrimeiroDiaDoPeriodo_Date(FPeriodoAtu), 1);
  FDataF := Geral.FDT(MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoAtu), 1);
  // Abre tabela com dados espec�ficos do cliente interno selecionado!
  if DmodFin.ReopenCliInt(FEntidade) then
    PBB := DmodFin.QrCliIntPBB.Value
  else PBB := 1;
  //
  Ano := Geral.IMV(CBAno.Text);
  Mes := CBMes.ItemIndex + 1;
  Mez := ((Ano - 2000) * 100) + Mes;
  FDataAnt := Geral.FDT(EncodeDate(Ano-1,1,1), 1);
  FDataIni := Geral.FDT(EncodeDate(Ano  ,1,1), 1);
  FPeriodoIni := (Ano - 2000) * 100 + 1;
  FPeriodoFim := (Ano - 2000) * 100 + 12;
  FPeriodoAnt := FPeriodoIni - 100;
  if Mes = 12 then
  begin
    Ano := Ano + 1;
    Mes := 1;
  end else Mes := Mes + 1;
  FDataAtu := Geral.FDT(EncodeDate(Ano, Mes, 1)-1, 1);
  //
  QrExclMov.Close;
  QrExclMov.Params[00].AsInteger := QrEmpCodigo.Value;
  QrExclMov.Params[01].AsInteger := Mez;
  QrExclMov.Open;
  //
  QrExclusivos.Close;
  QrExclusivos.Params[00].AsInteger := QrEmpCodigo.Value;
  QrExclusivos.Params[01].AsInteger := Mez;
  QrExclusivos.Open;
  //
  case RGFormato.ItemIndex of
    0: ImprimeCompleto(Mez, PBB);
    1: ImprimeReduzido();
    else Geral.MensagemBox('Formato de relat�rio n�o implementado!',
    'Erro', MB_OK+MB_ICONERROR);
  end;
end;

function TFmCashBal.frxComposite(frxAtu: TfrxReport; var ClearLastReport:
Boolean): Boolean;
var
  s: TMemoryStream;
begin
  Result := False;
  try
    if frxAtu <> nil then
    begin
      frxBal.OnGetValue := frxAtu.OnGetValue;
      frxBal.OnUserFunction := frxAtu.OnUserFunction;
      frxBal.ReportOptions.Name := 'Balancete';
      //  mudado de frxBal para frxAtu
      frxAtu.Variables['MeuLogoExiste'] := VAR_MEULOGOEXISTE;
      frxAtu.Variables['MeuLogoCaminho'] := QuotedStr(VAR_MEULOGOPATH);
      frxAtu.Variables['LogoNFExiste'] := VAR_LOGONFEXISTE;
      frxAtu.Variables['LogoNFCaminho'] := QuotedStr(VAR_LOGONFPATH);
      //end;
      //
      //frxAtu.Variables['LogoBancoExiste'] := True;//FileExists(QrCondBcoLogoPath.Value);
      //frxAtu.Variables['LogoBancoPath'] := QuotedStr(QrCondBcoLogoPath.Value);
      //
      //frxAtu.Variables['LogoEmpresaExiste'] := True;//FileExists(QrCondCliLogoPath.Value);
      //frxAtu.Variables['LogoEmpresaPath'] := QuotedStr(QrCondCliLogoPath.Value);
       //  FIM mudado de frxBal para frxAtu


      // agregar frx selecionado ao frxBal (frx virtual que est� agrupando v�rios frx para imprimir tudo junto)
      s := TMemoryStream.Create;
      frxAtu.SaveToStream(s);
      s.Position := 0;
      frxBal.LoadFromStream(s);
      frxBal.PrepareReport(ClearLastReport);
      ClearLastReport := False;
      //
      Result := True;
    end else
      Application.MessageBox('ERRO. "frxCond?" n�o definido.',
      'ERRO', MB_OK+MB_ICONERROR);
  except
    raise;
  end;
end;

procedure TFmCashBal.ImprimeCompleto(Mez, PBB: Integer);
var
  CLR, AindaNaoAtzCtas: Boolean;
  ComparaSdoNiveis, RelItem, Responsaveis: Integer;
  Num10, ImpA_11, ImpA_12, ImpA_13, ImpA_14, ImpA_15, ImpA_16: Boolean;
  Diferenca: Double;
begin
  ImpA_11 := False;
  ImpA_12 := False;
  ImpA_13 := False;
  ImpA_14 := False;
  ImpA_15 := False;
  ImpA_16 := False;
  Num10 := False;
  CLR   := True;
  AindaNaoAtzCtas := True;
  ComparaSdoNiveis := 0;
  //
  QrEntiCfgRel_01.Close;
  QrEntiCfgRel_01.Params[0].AsInteger := QrEmpCodigo.Value;
  QrEntiCfgRel_01.Open;
  //
  QrOrdiMov.Close;
  QrOrdiMov.Params[00].AsInteger := QrEmpCodigo.Value;
  QrOrdiMov.Params[01].AsInteger := Mez;
  QrOrdiMov.Open;
  //
  QrOrdinarios.Close;
  QrOrdinarios.Params[00].AsInteger := QrEmpCodigo.Value;
  QrOrdinarios.Params[01].AsInteger := Mez;
  QrOrdinarios.Open;
  //
  while not QrEntiCfgRel_01.Eof do
  begin
    RelItem := QrEntiCfgRel_01RelItem.Value;
    if QrLista.Locate('Controle', QrEntiCfgRel_01Controle.Value, [])
    and (QrListaAtivo.Value = 1) then
    begin
      MyObjects.Informa(LaAviso, True, 'Gerando sub-relat�rio ID = ' + IntToStr(RelItem));
      case RelItem of
        02:
        begin
          QrEntiRespon.Close;
          QrEntiRespon.SQL.Clear;
          QrEntiRespon.SQL.Add('SELECT er.Nome NO_RESPON, ec.Nome NO_CARGO,');
          QrEntiRespon.SQL.Add('er.Observ, er.MandatoIni, er.MandatoFim');
          QrEntiRespon.SQL.Add('FROM entirespon er');
          QrEntiRespon.SQL.Add('LEFT JOIN enticargos ec ON ec.Codigo=er.Cargo');
          QrEntiRespon.SQL.Add('WHERE er.Codigo=:P0');
          QrEntiRespon.SQL.Add('ORDER BY er.OrdemLista, er.Nome');
          QrEntiRespon.Params[00].AsInteger := QrEmpCodigo.Value;
          QrEntiRespon.Open;
          Responsaveis := QrEntiRespon.RecordCount;
          //
          QrEntiRespon.Close;
          QrEntiRespon.SQL.Clear;
          QrEntiRespon.SQL.Add('SELECT er.Nome NO_RESPON, ec.Nome NO_CARGO,');
          QrEntiRespon.SQL.Add('er.Observ, er.MandatoIni, er.MandatoFim');
          QrEntiRespon.SQL.Add('FROM entirespon er');
          QrEntiRespon.SQL.Add('LEFT JOIN enticargos ec ON ec.Codigo=er.Cargo');
          QrEntiRespon.SQL.Add('WHERE er.Codigo=:P0');
          QrEntiRespon.SQL.Add('AND :P1 BETWEEN MandatoIni AND MandatoFim');
          QrEntiRespon.SQL.Add('ORDER BY er.OrdemLista, er.Nome');
          QrEntiRespon.Params[00].AsInteger := QrEmpCodigo.Value;
          QrEntiRespon.Params[01].AsString  := FDataIni;
          QrEntiRespon.Open;
          if Responsaveis > QrEntiRespon.RecordCount then Geral.MensagemBox(
          IntToStr(Responsaveis - QrEntiRespon.RecordCount) +
          ' respons�veis que assinam foram desconsiderados por estarem fora ' +
          'do mandato neste per�odo!', 'Aviso', MB_OK+MB_ICONWARNING);
        end;
        03: PreparaBal_A_03();
        04: PreparaBal_A_04();
        05:
        begin
          // Extrato
          DModFin.GeraBalanceteExtrato(FPeriodoAtu, QrEmpCodigo.Value, PB1, LaSub1);
          // Saldos das contas controladas
          DModFin.SaldosNiveisPeriodo_2(QrEmpCodigo.Value, FPeriodoAtu, -1,
            False, PB1, PB1, PB1, PB1, PB1, DModFin.QrSNG);
        end;
        06:
        begin
          if AindaNaoAtzCtas then
          begin
            DmodFin.AtualizaContasHistSdo3(QrEmpCodigo.Value, LaSub1);
            AindaNaoAtzCtas := False;
          end;
          DmodFin.SaldosDeContasControladas3(QrEmpCodigo.Value, Mez);
          //
          ComparaSdoNiveis := ComparaSdoNiveis + 1;
        end;
        07:
        begin
          if AindaNaoAtzCtas then
          begin
            DmodFin.AtualizaContasHistSdo3(QrEmpCodigo.Value, LaSub1);
            AindaNaoAtzCtas := False;
          end;
          (*
          QrLCS.Close;
          QrLCS.Params[00].AsInteger := QrEmpCodigo.Value;
          QrLCS.Params[01].AsInteger := Mez;
          QrLCS.Open;
          *)
          DModG.QrUpdPID1.SQL.Clear;
          DModG.QrUpdPID1.SQL.Add('DROP TABLE IF EXISTS _LCS_;');
          DModG.QrUpdPID1.ExecSQL;
          //
          DModG.QrUpdPID1.SQL.Clear;
          DModG.QrUpdPID1.SQL.Add('CREATE TABLE _LCS_');
          DModG.QrUpdPID1.SQL.Add('SELECT pla.Nome NOMEPLANO, pla.Codigo PLANO,');
          DModG.QrUpdPID1.SQL.Add('cjt.Nome NOMECONJUNTO, gru.Conjunto,');
          DModG.QrUpdPID1.SQL.Add('gru.Nome NOMEGRUPO, sgr.Grupo,');
          DModG.QrUpdPID1.SQL.Add('sgr.Nome NOMESUBGRUPO, con.SubGrupo,');
          DModG.QrUpdPID1.SQL.Add('con.Nome NOMECONTA, mov.Genero,');
          DModG.QrUpdPID1.SQL.Add('SUM(mov.Movim) Valor, ');
          DModG.QrUpdPID1.SQL.Add('pla.OrdemLista OL_PLA, cjt.OrdemLista OL_CJT,');
          DModG.QrUpdPID1.SQL.Add('gru.OrdemLista OL_GRU, sgr.OrdemLista OL_SGR,');
          DModG.QrUpdPID1.SQL.Add('con.OrdemLista OL_CON,');
          DModG.QrUpdPID1.SQL.Add('SUM(0.00) vSGR, SUM(0.00) vGRU, SUM(0.00) vCJT, SUM(0.00) vPLA');
          DModG.QrUpdPID1.SQL.Add('FROM '+TMeuDB+'.contasmov mov');
          DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.contas    con ON con.Codigo=mov.Genero');
          DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.subgrupos sgr ON sgr.Codigo=mov.SubGrupo');
          DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.grupos    gru ON gru.Codigo=mov.Grupo');
          DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.conjuntos cjt ON cjt.Codigo=mov.Conjunto');
          DModG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.plano     pla ON pla.Codigo=mov.Plano');
          DModG.QrUpdPID1.SQL.Add('WHERE mov.CliInt=:P0');
          DModG.QrUpdPID1.SQL.Add('AND mov.Mez <=:P1');
          DModG.QrUpdPID1.SQL.Add('AND mov.Movim <> 0');
          DModG.QrUpdPID1.SQL.Add('GROUP BY mov.Genero');
          DModG.QrUpdPID1.SQL.Add('ORDER BY OL_PLA, NOMEPLANO, OL_CJT,');
          DModG.QrUpdPID1.SQL.Add('NOMECONJUNTO, OL_GRU, NOMEGRUPO, OL_SGR,');
          DModG.QrUpdPID1.SQL.Add('NOMESUBGRUPO, OL_CON, NOMECONTA, mov.Mez');
          DModG.QrUpdPID1.SQL.Add('');
          DModG.QrUpdPID1.SQL.Add('');
          DModG.QrUpdPID1.SQL.Add('');
          DModG.QrUpdPID1.SQL.Add('');
          DModG.QrUpdPID1.SQL.Add('');
          DModG.QrUpdPID1.Params[00].AsInteger := QrEmpCodigo.Value;
          DModG.QrUpdPID1.Params[01].AsInteger := Mez;
          DModG.QrUpdPID1.ExecSQL;
          // SUB-GRUPO
          QrSSdo.Close;
          QrSSdo.SQL.Clear;
          QrSSdo.SQL.Add('SELECT SubGrupo Codigo, SUM(Valor) Valor');
          QrSSdo.SQL.Add('FROM _lcs_');
          QrSSdo.SQL.Add('GROUP BY SubGrupo');
          QrSSdo.Open;
          while not QrSSdo.Eof do
          begin
            DModG.QrUpdPID1.SQL.Clear;
            DModG.QrUpdPID1.SQL.Add('UPDATE _lcs_ SET vSGR=:P0');
            DModG.QrUpdPID1.SQL.Add('WHERE SubGrupo=:P1');
            DModG.QrUpdPID1.Params[00].AsFloat   := QrSSdoValor.Value;
            DModG.QrUpdPID1.Params[01].AsInteger := QrSSdoCodigo.Value;
            DModG.QrUpdPID1.ExecSQL;
            QrSSdo.Next;
          end;
          // GRUPO
          QrSSdo.Close;
          QrSSdo.SQL.Clear;
          QrSSdo.SQL.Add('SELECT Grupo Codigo, SUM(Valor) Valor');
          QrSSdo.SQL.Add('FROM _lcs_');
          QrSSdo.SQL.Add('GROUP BY Grupo');
          QrSSdo.Open;
          while not QrSSdo.Eof do
          begin
            DModG.QrUpdPID1.SQL.Clear;
            DModG.QrUpdPID1.SQL.Add('UPDATE _lcs_ SET vGRU=:P0');
            DModG.QrUpdPID1.SQL.Add('WHERE Grupo=:P1');
            DModG.QrUpdPID1.Params[00].AsFloat   := QrSSdoValor.Value;
            DModG.QrUpdPID1.Params[01].AsInteger := QrSSdoCodigo.Value;
            DModG.QrUpdPID1.ExecSQL;
            QrSSdo.Next;
          end;
          //
          // CONJUNTO
          QrSSdo.Close;
          QrSSdo.SQL.Clear;
          QrSSdo.SQL.Add('SELECT Conjunto Codigo, SUM(Valor) Valor');
          QrSSdo.SQL.Add('FROM _lcs_');
          QrSSdo.SQL.Add('GROUP BY Conjunto');
          QrSSdo.Open;
          while not QrSSdo.Eof do
          begin
            DModG.QrUpdPID1.SQL.Clear;
            DModG.QrUpdPID1.SQL.Add('UPDATE _lcs_ SET vCJT=:P0');
            DModG.QrUpdPID1.SQL.Add('WHERE Conjunto=:P1');
            DModG.QrUpdPID1.Params[00].AsFloat   := QrSSdoValor.Value;
            DModG.QrUpdPID1.Params[01].AsInteger := QrSSdoCodigo.Value;
            DModG.QrUpdPID1.ExecSQL;
            QrSSdo.Next;
          end;
          // PLANO
          QrSSdo.Close;
          QrSSdo.SQL.Clear;
          QrSSdo.SQL.Add('SELECT PLANO Codigo, SUM(Valor) Valor');
          QrSSdo.SQL.Add('FROM _lcs_');
          QrSSdo.SQL.Add('GROUP BY PLANO');
          QrSSdo.Open;
          while not QrSSdo.Eof do
          begin
            DModG.QrUpdPID1.SQL.Clear;
            DModG.QrUpdPID1.SQL.Add('UPDATE _lcs_ SET vPLA=:P0');
            DModG.QrUpdPID1.SQL.Add('WHERE PLANO=:P1');
            DModG.QrUpdPID1.Params[00].AsFloat   := QrSSdoValor.Value;
            DModG.QrUpdPID1.Params[01].AsInteger := QrSSdoCodigo.Value;
            DModG.QrUpdPID1.ExecSQL;
            QrSSdo.Next;
          end;
          //
          QrLC2.Close;
          QrLC2.Database := DModG.MyPID_DB;
          QrLC2.Open;
          ComparaSdoNiveis := ComparaSdoNiveis + 1;
        end;
        08:
        begin
          DmodFin.GeraBalanceteRecDebi(FPeriodoAtu, QrEmpCodigo.Value,
            CkAcordos.Checked, CkPeriodos.Checked, CkTextos.Checked);
        end;
        09:
        begin
          DmodFin.AtualizaContasMensais2(FPeriodoAtu -11, FPeriodoAtu, 0,
            PB1, PB2, PB3, LaSub1, LaSub2, LaSub3);
          QrCtasResMes.Close;
          QrCtasResMes.Params[0].AsInteger := FPeriodoAtu-11;
          QrCtasResMes.Params[1].AsInteger := FPeriodoAtu;
          QrCtasResMes.Open;
        end;
        10:
        begin
          if MyObjects.CriaForm_AcessoTotal(TFmResmes, FmResMes) then
          begin
            Num10 := True;
            FmResMes.FEntidade := QrEmpCodigo.Value;
            FmResMes.FPaginar  := CkPaginar.Checked;
            FmResMes.ResultadoGeral(FPeriodoAtu-11, PB1, PB2, PB3, LaSub1, LaSub2, LaSub3);
          end;
        end;
        11: ImpA_11 := DModFin.ReopenPrevItO(QrEmpCodigo.Value, FPeriodoAtu);
        12: ImpA_12 := DModFin.BloquetosDeCobrancaProcessados(FPeriodoAtu, QrEmpCodigo.Value);
        13: ImpA_13 := DmodFin.ReopenPendG(QrEmpCodigo.Value, QrEmpCliInt.Value);
        14: ImpA_14 := DModFin.ReopenReparcelNew(QrEmpCodigo.Value, FPeriodoAtu, PBB);
        15: ImpA_15 := DModFin.ReopenReparcelAbe(QrEmpCodigo.Value);
        16: ImpA_16 := ChCompensadosPeriodo(QrEmpCodigo.Value);
      end;
      MyObjects.Informa(LaAviso, True, 'Gerando sub-relatorio ID = ' + IntToStr(RelItem));
      case RelItem of
        01: frxComposite(frxBal_A_01, CLR); // Capa
        02: frxComposite(frxBal_A_02, CLR); // Parecer revisores
        03: frxComposite(frxBal_A_03, CLR); // Entradas na data
        04: frxComposite(frxBal_A_04, CLR); // Entradas por m�s de compet�ncia
        05: frxComposite(frxBal_A_05, CLR); // Extrato contas correntes e caixas
        06: frxComposite(frxBal_A_06, CLR); // Saldos de contas controladas
        07: frxComposite(frxBal_A_07, CLR); // Saldos de todas contas
        08: frxComposite(frxBal_A_08, CLR); // Demosntr. Receitas e Despesas Periodo
        09: frxComposite(frxBal_A_09, CLR); // Mensalidades pr�-estipuladas
        10: frxComposite(FmResMes.ImprimeResMesExclusivo(42, PB1, PB2, LaSub1), CLR);
        11: if ImpA_11 then frxComposite(frxBal_A_11, CLR); // Composi��o da provis�o efetuada para a arrecada��o
        12: if ImpA_12 then // Processamento de bloquetos de cobran�a
            begin
              case QrEntiCfgRel_01FonteTam.Value of
                0:   frxComposite(frxBal_A_12A, CLR);
                1,2: frxComposite(frxBal_A_12B, CLR);
              end;
            end;
        13: if ImpA_13 then frxComposite(frxBal_A_13, CLR); // Pend�ncias de Clientes (Cond�minos)
        14: if ImpA_14 then frxComposite(frxBal_A_14, CLR); // Reparcelamentos de integrantes no per�odo (Cond�minos)
        15: if ImpA_15 then frxComposite(frxBal_A_15, CLR); // Reparcelamentos de integrantes em aberto (Cond�minos)
        16: if ImpA_16 then frxComposite(frxBal_A_16, CLR); // Cheques compensados no per�odo

        else Application.MessageBox(PChar('O item "' + IntToStr(RelItem) +
        '" n�o foi implementado no balancete!' + #13#10 + 'AVISE A DERMATEK!'),
        'Aviso', MB_OK+MB_ICONWARNING);
      end;
    end;
    QrEntiCfgRel_01.Next;
  end;
  MyObjects.Informa(LaAviso, True, 'Preparando Balancete');
  Application.CreateForm(TFmMeufrx, FmMeufrx);
  frxBal.Preview := FmMeufrx.PvVer;
  //
  FmMeufrx.PvVer.OutlineWidth := frxBal.PreviewOptions.OutlineWidth;
  FmMeufrx.PvVer.Zoom := frxBal.PreviewOptions.Zoom;
  FmMeufrx.PvVer.ZoomMode := frxBal.PreviewOptions.ZoomMode;
  FmMeufrx.UpdateZoom;
  //
  FFim := Now();
  FTempo := FFim - FIni;
  EdTempo.ValueVariant := FTempo;
  //
  if ComparaSdoNiveis = 2 then
  begin
    QrSN1.Close;
    QrSN1.Open;
    QrSN2.Close;
    QrSN2.Open;
    Diferenca := QrSN1Movim.Value -QrSN2Valor.Value;
    if (Diferenca >= 0.01) or (Diferenca <= -0.01) then
      Geral.MensagemBox(
      'ATEN��O: Saldo total dos N�veis do Plano de Contas ' + #13#10 +
      'n�o confere com a soma de saldos das contas controladas!' + #13#10 +
      'Verifique se n�o h� n�veis contas n�o controlas com saldo!' + #13#10 +
      'DIFEREN�A: ' + FormatFloat('#,###,###,###,##0.00', Diferenca),
      'AVISO', MB_OK+MB_ICONWARNING);
  end;
  FmMeufrx.ShowModal;
  FmMeufrx.Destroy;
  MyObjects.Informa(LaAviso, False, '...');
  if Num10 then
    FmResmes.Destroy;
  Screen.Cursor := crDefault;
end;

procedure TFmCashBal.ImprimeReduzido();
begin
  FResumido_01 := False;
  FResumido_02 := False;
  FResumido_03 := False;
  FResumido_04 := False;
  FResumido_05 := False;
  FResumido_06 := False;
  //
  if Geral.IntInConjunto(1, CGResumido.Value) then
    FResumido_01 :=
    DmodFin.GeraBalanceteRecDebi(FPeriodoAtu, QrEmpCodigo.Value,
    CkAcordos.Checked, CkPeriodos.Checked, CkTextos.Checked);
  //
  if Geral.IntInConjunto(2, CGResumido.Value) then
  begin
    QrDebitos.Close;
    QrDebitos.Params[00].AsInteger := QrEmpCodigo.Value;
    QrDebitos.Params[01].AsString  := FDataI;
    QrDebitos.Params[02].AsString  := FDataF;
    QrDebitos.Open;
    FResumido_02 := True;
    (*
    FResumido_02 := FResumido_01;
    if not FResumido_02 then
      FResumido_02 :=
      DmodFin.GeraBalanceteRecDebi(FPeriodoAtu, QrEmpCodigo.Value,
      CkAcordos.Checked, CkPeriodos.Checked, CkTextos.Checked);
    *)
  end;
  //
  if Geral.IntInConjunto(4, CGResumido.Value) then
  //begin
    FResumido_03 :=
    DmodFin.ReopenPendAll(QrEmpCodigo.Value, QrEmpCliInt.Value, FPeriodoAtu);
  {
  end else begin
      // Compatibilidade com o FastReport
      // O FastReport abre mesmo pedindo para ocultar!
      DmodFin.QrPendAll.Close;
      DmodFin.QrPendAll.SQL.Clear;
      DmodFin.QrPendAll.SQL.Add('SELECT "?????" Unidade, 0.00000 Credito,');
      DmodFin.QrPendAll.SQL.Add('"??????" TIPO, 0 KGT, 0 BLOQUETOS');
      //DmodFin.QrPendAll.Open;
      //
      DmodFin.QrPendSum.Close;
      DmodFin.QrPendSum.SQL.Clear;
      DmodFin.QrPendSum.SQL.Add('SELECT 0.00000 Credito');
      //DmodFin.QrPendSum.Open;
  end;
  }
  //
  if Geral.IntInConjunto(8, CGResumido.Value) then
    FResumido_04 :=
    DModFin.GeraBalanceteExtrato(FPeriodoAtu, QrEmpCodigo.Value, PB1, LaSub1);
  //
  if Geral.IntInConjunto(16, CGResumido.Value) then
    FResumido_05 :=
    DModFin.SaldosNiveisPeriodo_2(QrEmpCodigo.Value, FPeriodoAtu, -1,
    False, PB1, PB1, PB1, PB1, PB1, DModFin.QrSNG);
  if Geral.IntInConjunto(32, CGResumido.Value) then
  begin
    if (FResumido_01 = False) then
      FResumido_06 :=
      DmodFin.GeraBalanceteRecDebi(FPeriodoAtu, QrEmpCodigo.Value,
      CkAcordos.Checked, CkPeriodos.Checked, CkTextos.Checked)
    else FResumido_06 := True;
    if FResumido_06 and (FResumido_04 = False) then
      //FResumido_06 :=
      DmodFin.ReopenPendAll(QrEmpCodigo.Value, QrEmpCliInt.Value, FPeriodoAtu);
    // a function DmodFin.ReopenPendAll retornar� false seo aplicativo for diferente de "S Y N D I C"
    FResumido_06 := True;
  end;
  //
  MyObjects.frxMostra(frxBalancete2, 'Balancete Demonstrativo (Reduzido)');
end;

end.

