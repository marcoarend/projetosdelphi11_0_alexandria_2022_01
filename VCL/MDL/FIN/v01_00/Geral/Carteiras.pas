unit Carteiras;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Menus,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, Variants, dmkPermissoes, Grids,
  DBGrids, dmkDBGrid, ComCtrls, MyDBCheck, dmkGeral, dmkCheckBox, UnDmkProcFunc,
  UnDmkEnums;

type
  TFmCarteiras = class(TForm)
    DsCarteiras: TDataSource;
    QrCarteiras: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasTipo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasInicial: TFloatField;
    QrCarteirasBanco: TIntegerField;
    QrCarteirasID: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasID_Fat: TWideStringField;
    QrCarteirasSaldo: TFloatField;
    QrCarteirasEmCaixa: TFloatField;
    QrCarteirasFechamento: TIntegerField;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasPagRec: TIntegerField;
    QrCarteirasDiaMesVence: TSmallintField;
    QrCarteirasExigeNumCheque: TSmallintField;
    QrCarteirasForneceI: TIntegerField;
    QrCarteirasLk: TIntegerField;
    QrCarteirasDataCad: TDateField;
    QrCarteirasDataAlt: TDateField;
    QrCarteirasUserCad: TIntegerField;
    QrCarteirasUserAlt: TIntegerField;
    QrCarteirasNome2: TWideStringField;
    QrCarteirasTipoDoc: TSmallintField;
    QrCarteirasBanco1: TIntegerField;
    QrCarteirasAgencia1: TIntegerField;
    QrCarteirasConta1: TWideStringField;
    QrCarteirasCheque1: TIntegerField;
    QrCarteirasContato1: TWideStringField;
    QrCarteirasNOMETIPO: TWideStringField;
    QrCarteirasNOMEDOBANCO: TWideStringField;
    QrCarteirasNOMEFORNECEI: TWideStringField;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PMCarteira: TPopupMenu;
    Caixa1: TMenuItem;
    Banco1: TMenuItem;
    Emisso1: TMenuItem;
    QrBancos: TmySQLQuery;
    QrBancosCodigo: TIntegerField;
    QrBancosNome: TWideStringField;
    DsBancos: TDataSource;
    QrForneceI: TmySQLQuery;
    QrForneceICodigo: TIntegerField;
    QrForneceINOMEENTIDADE: TWideStringField;
    DsForneceI: TDataSource;
    QrCarteirasAntigo: TWideStringField;
    QrCarteirasContab: TWideStringField;
    QrCarteirasOrdem: TIntegerField;
    QrCarteirasForneceN: TSmallintField;
    QrCarteirasFuturoC: TFloatField;
    QrCarteirasFuturoD: TFloatField;
    QrCarteirasFuturoS: TFloatField;
    QrCarteirasExclusivo: TSmallintField;
    QrCarteirasAlterWeb: TSmallintField;
    QrCarteirasAtivo: TSmallintField;
    QrCarteirasRecebeBloq: TSmallintField;
    QrEntiDent: TmySQLQuery;
    DsEntiDent: TDataSource;
    QrCarteirasEntiDent: TIntegerField;
    QrEntiDentCodigo: TIntegerField;
    QrEntiDentNOMEENT: TWideStringField;
    QrCarteirasNOMEENTIDENT: TWideStringField;
    dmkPermissoes1: TdmkPermissoes;
    Panel4: TPanel;
    PnBancEdit: TPanel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    EdInicial_B: TdmkEdit;
    EdBanco1: TdmkEdit;
    EdAgencia1: TdmkEdit;
    EdConta1: TdmkEdit;
    PnCaixEdit: TPanel;
    Label23: TLabel;
    CkPrazo: TCheckBox;
    EdInicial_C: TdmkEdit;
    CkRecebeBloq: TCheckBox;
    PnEmisEdit: TPanel;
    Label21: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label37: TLabel;
    GBFatura2: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    Label20: TLabel;
    EdIDFat: TdmkEdit;
    EdDdFechamento: TdmkEdit;
    EdDiaMesVence: TdmkEdit;
    CkFatura: TCheckBox;
    CBBanco: TdmkDBLookupComboBox;
    CkExigeNumCheque: TCheckBox;
    RGTipoDoc: TRadioGroup;
    EdBanco: TdmkEditCB;
    EdBanco1_1: TdmkEdit;
    EdAgencia1_1: TdmkEdit;
    EdConta1_1: TdmkEdit;
    CheckBox1: TCheckBox;
    Panel1: TPanel;
    Label9: TLabel;
    LaForneceI: TLabel;
    Label10: TLabel;
    Label14: TLabel;
    LaID2: TLabel;
    Label28: TLabel;
    Label46: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    EdCodigo: TdmkEdit;
    EdForneceI: TdmkEditCB;
    RGPagRec: TRadioGroup;
    CBForneceI: TdmkDBLookupComboBox;
    EdNome: TdmkEdit;
    EdNome2: TdmkEdit;
    CkAtivo: TCheckBox;
    EdID: TdmkEdit;
    EdContato1: TdmkEdit;
    EdContab: TdmkEdit;
    EdOrdem: TdmkEdit;
    CkForneceN: TCheckBox;
    CkExclusivo: TCheckBox;
    EdEntiDent: TdmkEditCB;
    CBEntiDent: TdmkDBLookupComboBox;
    QrCliInt: TmySQLQuery;
    QrCliIntCodigo: TIntegerField;
    QrCliIntNOMECLI: TWideStringField;
    QrCliIntCliInt: TIntegerField;
    DsCliInt: TDataSource;
    QrCliCart: TmySQLQuery;
    DsCliCart: TDataSource;
    QrCliCartCodigo: TIntegerField;
    QrCliCartTipo: TIntegerField;
    QrCliCartNome: TWideStringField;
    QrCliCartNOME_TIPO: TWideStringField;
    QrCliCartNOMEBCO: TWideStringField;
    QrCliCartBanco1: TIntegerField;
    QrCliCartAgencia1: TIntegerField;
    QrCliCartConta1: TWideStringField;
    PMUsuario: TPopupMenu;
    Adiciona1: TMenuItem;
    Retira1: TMenuItem;
    QrCarteirasU: TmySQLQuery;
    QrCarteirasUControle: TIntegerField;
    QrCarteirasUUsuario: TIntegerField;
    QrCarteirasULogin: TWideStringField;
    QrCarteirasUFuncionario: TIntegerField;
    QrCarteirasUNOMEUSU: TWideStringField;
    DsCarteirasU: TDataSource;
    PainelDados: TPanel;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel9: TPanel;
    BtSaida: TBitBtn;
    BtUsuario: TBitBtn;
    PnFast: TPanel;
    Panel7: TPanel;
    EdCliInt: TEdit;
    dmkDBGrid1: TdmkDBGrid;
    Panel6: TPanel;
    Panel8: TPanel;
    EdCliCart: TEdit;
    CkSohAtivos: TCheckBox;
    PnCartAtu: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel10: TPanel;
    PainelData: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label12: TLabel;
    Label19: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label22: TLabel;
    Label31: TLabel;
    Label38: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit1: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    DBRGPagRec: TDBRadioGroup;
    DBEdit9: TDBEdit;
    EdDBNome: TDBEdit;
    EdDBID: TDBEdit;
    EdDBContato: TDBEdit;
    EdDBContab: TDBEdit;
    DBEdit13: TDBEdit;
    DBCheckBox5: TDBCheckBox;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    PnTipoShow: TPanel;
    PnCaixShow: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox6: TDBCheckBox;
    PnBancShow: TPanel;
    Label4: TLabel;
    Label13: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    DBEdit2: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    PnEmisShow: TPanel;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    GroupBox1: TGroupBox;
    LaIDFat: TLabel;
    Label6: TLabel;
    Label11: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit8: TDBEdit;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    DBRGTipoDoc: TDBRadioGroup;
    TabSheet2: TTabSheet;
    DBGFunci: TdmkDBGrid;
    Label36: TLabel;
    EdCodCedente: TdmkEdit;
    QrCarteirasCodCedente: TWideStringField;
    DBEdit16: TDBEdit;
    Label39: TLabel;
    QrCliCartSaldo: TFloatField;
    QrCliCartInicial: TFloatField;
    RGTipo: TRadioGroup;
    DBRGTipo: TDBRadioGroup;
    Panel2: TPanel;
    dmkDBGrid2: TdmkDBGrid;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    QrLan: TmySQLQuery;
    QrLanCliInt: TIntegerField;
    QrCarteirasIgnorSerie: TSmallintField;
    DBCheckBox7: TDBCheckBox;
    CkIgnorSerie: TdmkCheckBox;
    Panel11: TPanel;
    BtDesiste: TBitBtn;
    CkNotConcBco: TdmkCheckBox;
    QrCarteirasNotConcBco: TSmallintField;
    DBCheckBox8: TDBCheckBox;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCarteirasAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrCarteirasAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCarteirasBeforeOpen(DataSet: TDataSet);
    procedure QrCarteirasCalcFields(DataSet: TDataSet);
    procedure EdCliIntChange(Sender: TObject);
    procedure EdCliCartChange(Sender: TObject);
    procedure QrCliIntAfterScroll(DataSet: TDataSet);
    procedure QrCliCartAfterScroll(DataSet: TDataSet);
    procedure CkSohAtivosClick(Sender: TObject);
    procedure BtUsuarioClick(Sender: TObject);
    procedure Adiciona1Click(Sender: TObject);
    procedure Retira1Click(Sender: TObject);
    procedure RGTipoClick(Sender: TObject);
    procedure Caixa1Click(Sender: TObject);
    procedure Banco1Click(Sender: TObject);
    procedure Emisso1Click(Sender: TObject);
    procedure DBRGTipoChange(Sender: TObject);
    procedure DBRGTipoClick(Sender: TObject);
    procedure EdNomeExit(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; Status: String; Codigo, Tipo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenCliInt();
    procedure ReopenCliCart(Carteira: Integer);
    procedure DefinePanelEdit();
    procedure DefinePanelShow();
  public
    { Public declarations }
    FSeq: Integer;
    procedure ReopenCarteirasU(Controle: Integer);
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmCarteiras: TFmCarteiras;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleFin, UnFinanceiro, CarteirasU, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCarteiras.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCarteiras.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCarteirasCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCarteiras.DefParams;
begin
  VAR_GOTOTABELA := 'Carteiras';
  VAR_GOTOMYSQLTABLE := QrCarteiras;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT DISTINCT ca.*, ba.Nome NOMEDOBANCO,');
  VAR_SQLx.Add('CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  VAR_SQLx.Add('ELSE en.Nome END NOMEFORNECEI, ' );
  VAR_SQLx.Add('IF(eg.Tipo=0, eg.RazaoSocial, eg.Nome) NOMEENTIDENT');
  VAR_SQLx.Add('FROM carteiras ca');
  VAR_SQLx.Add('LEFT JOIN carteiras ba ON ba.Codigo=ca.Banco');
  VAR_SQLx.Add('LEFT JOIN entidades en ON en.Codigo=ca.ForneceI');
  VAR_SQLx.Add('LEFT JOIN entidades eg ON eg.Codigo=ca.EntiDent');
  VAR_SQLx.Add('WHERE ca.Codigo>-1000000');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND ca.Codigo=:P0');
  //
  VAR_SQLa.Add('AND ca.Nome Like :P0');
  //
end;

procedure TFmCarteiras.MostraEdicao(Mostra: Integer; Status: String; Codigo,
  Tipo: Integer);
  procedure ReopenForneceI_Normal();
  begin
    QrForneceI.Close;
    QrForneceI.SQL.Clear;
    QrForneceI.SQL.Add('SELECT Codigo,');
    QrForneceI.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrForneceI.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrForneceI.SQL.Add('FROM entidades');
    QrForneceI.SQL.Add('ORDER BY NomeENTIDADE');
    UMyMod.AbreQuery(QrForneceI, Dmod.MyDB, ' > TFmCarteiras.MostraEdicao()');
  end;
var
  //Habilita: Boolean;
  EntiTxt, CartTxt: String;
begin
  VAR_CARTEIRA   := QrCarteirasTipo.Value;
  CartTxt := FormatFloat('0', QrCarteirasCodigo.Value);
  // Evitar erro!!!
  EdNome.Enabled := True;
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible   := True;
      PainelEdita.Visible   := False;
      PnCaixEdit.Visible    := False;
      PnBancEdit.Visible    := False;
      PnEmisEdit.Visible    := False;
    end;
    1:
    begin
      PainelEdita.Visible   := True;
      PainelDados.Visible   := False;
      if Status = CO_INCLUSAO then
      begin
        ReopenForneceI_Normal();
        //
        EdCodigo.Text           := IntToStr(Codigo);
        EdNome.Text             := CO_VAZIO;
        EdNome2.Text            := CO_VAZIO;
        EdID.Text               := CO_VAZIO;
        EdIDFat.Text            := CO_VAZIO;
        EdInicial_C.Text        := CO_VAZIO;
        EdInicial_B.Text        := CO_VAZIO;
        EdDdFechamento.Text     := CO_VAZIO;
        CkFatura.Checked        := False;
        EdBanco.Text            := '';
        CBBanco.KeyValue        := Null;
        EdForneceI.Text         := '';
        CBForneceI.KeyValue     := Null;
        RGTipoDoc.ItemIndex     := 0;
        EdBanco1.Text           := '';
        EdAgencia1.Text         := '';
        EdConta1.Text           := '';
        EdContato1.Text         := '';
        EdContab.Text           := '';
        CkAtivo.Checked         := True;
        EdOrdem.Text            := '';
        CkForneceN.Checked      := False;
        CkExclusivo.Checked     := False;
        CkRecebeBloq.Checked    := False;
        CkIgnorSerie.Checked    := True;
        CkNotConcBco.Checked    := False;
        EdEntiDent.ValueVariant := 0;
        CBEntiDent.KeyValue     := 0;
        EdCodCedente.Text       := '';
        RGTipo.ItemIndex        := Tipo;
      end else begin
        QrLan.Close;
        QrLan.SQL.Clear;
        QrLan.SQL.Add('SELECT DISTINCT CliInt');
        QrLan.SQL.Add('FROM ' + VAR_LCT + '');
        QrLan.SQL.Add('WHERE CliInt <> 0');
        QrLan.SQL.Add('AND Carteira=' + CartTxt);
        QrLan.Open;
        {
        QrLan.SQL.Add('SELECT COUNT(*) Itens');
        QrLan.SQL.Add('FROM ' + VAR_LCT + '');
        QrLan.SQL.Add('WHERE Carteira=' + CartTxt);
        QrLan.Open;
        }
        if QrLan.RecordCount > 0 then
        begin
          EntiTxt := '';
          QrLan.First;
          while not QrLan.Eof do
          begin
            if QrLan.RecordCount = QrLan.RecNo then
              EntiTxt := EntiTxt + FormatFloat('0', QrLanCliInt.Value)
            else
              EntiTxt := EntiTxt + FormatFloat('0', QrLanCliInt.Value) + ', ';
            QrLan.Next;
          end;
{
          QrForneceI.Close;
          QrForneceI.SQL.Clear;
          QrForneceI.SQL.Add('SELECT Codigo,');
          QrForneceI.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
          QrForneceI.SQL.Add('ELSE Nome END NOMEENTIDADE');
          QrForneceI.SQL.Add('FROM entidades');
          QrForneceI.SQL.Add('WHERE Codigo IN (');
          QrForneceI.SQL.Add('     SELECT DISTINCT CliInt');
          QrForneceI.SQL.Add('     FROM ' + VAR_LCT + '');
          QrForneceI.SQL.Add('     WHERE Carteira=' + CartTxt);
          QrForneceI.SQL.Add(')');
          QrForneceI.SQL.Add('ORDER BY NomeENTIDADE');
          QrForneceI.Open;
}
          QrForneceI.Close;
          QrForneceI.SQL.Clear;
          QrForneceI.SQL.Add('SELECT Codigo,');
          QrForneceI.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
          QrForneceI.SQL.Add('ELSE Nome END NOMEENTIDADE');
          QrForneceI.SQL.Add('FROM entidades');
          QrForneceI.SQL.Add('WHERE CliInt <> 0 ');
          //
{
          QrForneceI.SQL.Add('AND Codigo IN (');
          QrForneceI.SQL.Add('     SELECT DISTINCT CliInt');
          QrForneceI.SQL.Add('     FROM ' + VAR_LCT + '');
          QrForneceI.SQL.Add('     WHERE Carteira=' + CartTxt);
}
          if EntiTxt <> '' then
            QrForneceI.SQL.Add('AND Codigo IN (' + EntiTxt);
          //
          QrForneceI.SQL.Add(')');
          QrForneceI.SQL.Add('ORDER BY NomeENTIDADE');
          UMyMod.AbreQuery(QrForneceI, Dmod.MyDB, 'TFmCarteiras.MostraEdicao()');
        end else ReopenForneceI_Normal();
        RGTipo.ItemIndex        := DBRGTipo.ItemIndex;
        RGPagRec.ItemIndex      := DBRGPagRec.ItemIndex;
        EdCodigo.Text           := DBEdCodigo.Text;
        EdNome.Text             := DBEdNome.Text;
        EdNome2.Text            := QrCarteirasNome2.Value;
        EdInicial_B.Text        := Geral.FFT(QrCarteirasInicial.Value, 2, siPositivo);
        EdInicial_C.Text        := Geral.FFT(QrCarteirasInicial.Value, 2, siPositivo);
        EdID.Text               := QrCarteirasID.Value;
        EdBanco.Text            := IntToStr(QrCarteirasBanco.Value);
        CBBanco.KeyValue        := QrCarteirasBanco.Value;
        if QrCarteirasFatura.Value = 'V' then CkFatura.Checked := True
        else CkFatura.Checked   := False;
        CkPrazo.Checked         := Geral.IntToBool_0(QrCarteirasPrazo.Value);
        EdIDFat.Text            := QrCarteirasID_Fat.Value;
        EdDdFechamento.Text     := IntToStr(QrCarteirasFechamento.Value);
        EdDiaMesVence.Text      := IntToStr(QrCarteirasDiaMesVence.Value);
        CkExigeNumCheque.Checked := Geral.IntToBool_0(QrCarteirasExigeNumCheque.Value);
        EdForneceI.Text         := IntToStr(QrCarteirasForneceI.Value);
        CBForneceI.KeyValue     := QrCarteirasForneceI.Value;
        RGTipoDoc.ItemIndex     := QrCarteirasTipoDoc.Value;
        //
        EdBanco1.Text           := MLAGeral.FFD(QrCarteirasBanco1.Value, 3, siPositivo);
        EdAgencia1.Text         := MLAGeral.FFD(QrCarteirasAgencia1.Value, 4, siPositivo);
        EdConta1.Text           := QrCarteirasConta1.Value;
        //
        EdBanco1_1.Text         := MLAGeral.FFD(QrCarteirasBanco1.Value, 3, siPositivo);
        EdAgencia1_1.Text       := MLAGeral.FFD(QrCarteirasAgencia1.Value, 4, siPositivo);
        EdConta1_1.Text         := QrCarteirasConta1.Value;
        //
        EdContato1.Text         := QrCarteirasContato1.Value;
        EdContab.Text           := QrCarteirasContab.Value;
        CkAtivo.Checked         := Geral.IntToBool_0(QrCarteirasAtivo.Value);
        //
        EdOrdem.Text            := IntToStr(QrCarteirasOrdem.Value);
        //
        CkForneceN.Checked      := Geral.IntToBool_0(QrCarteirasForneceN.Value);
        CkExclusivo.Checked     := Geral.IntToBool_0(QrCarteirasExclusivo.Value);
        CkRecebeBloq.Checked    := Geral.IntToBool_0(QrCarteirasRecebeBloq.Value);
        CkIgnorSerie.Checked    := Geral.IntToBool_0(QrCarteirasIgnorSerie.Value);
        CkNotConcBco.Checked    := Geral.IntToBool_0(QrCarteirasNotConcBco.Value);
        //
        EdEntiDent.ValueVariant := QrCarteirasEntiDent.Value;
        CBEntiDent.KeyValue     := QrCarteirasEntiDent.Value;
        EdCodCedente.Text       := QrCarteirasCodCedente.Value;
      end;
      EdNome.SetFocus;
      DefinePanelEdit();
    end;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmCarteiras.Retira1Click(Sender: TObject);
var
  Controle: Integer; 
begin
  if QrCarteirasU.RecordCount > 0 then
  begin
    Controle := QrCarteirasUControle.Value;
    //
    if Application.MessageBox(PChar('Confirma a exclus�o do usu�rio '+
    QrCarteirasUNOMEUSU.Value +'?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES
    then begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM carteirasu WHERE Controle=:P0');
      Dmod.QrUpd.Params[00].AsInteger := Controle;
      Dmod.QrUpd.ExecSQL;
      //
      ReopenCarteirasU(0);
    end;
  end else
  begin
    Application.MessageBox('N�o foi localizado nenhum usu�rio.',
      'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmCarteiras.RGTipoClick(Sender: TObject);
begin
  DefinePanelEdit();
end;

procedure TFmCarteiras.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCarteiras.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCarteiras.ReopenCliCart(Carteira: Integer);
begin
  QrCliCart.Close;
  QrCliCart.SQL.Clear;
  QrCliCart.SQL.Add('SELECT car.Codigo, car.Tipo, car.Nome,');
  QrCliCart.SQL.Add('ELT(car.Tipo+1,"Caixa","C/C Banco",');
  QrCliCart.SQL.Add('"Emiss�o") NOME_TIPO,');
  QrCliCart.SQL.Add('IF(car.Banco=0,"",bco.Nome) NOMEBCO,');
  QrCliCart.SQL.Add('car.Banco1, car.Agencia1, car.Conta1, ');
  QrCliCart.SQL.Add('car.Inicial, car.Saldo, car.ForneceI');
  QrCliCart.SQL.Add('FROM carteiras car');
  QrCliCart.SQL.Add('LEFT JOIN carteiras bco ON bco.Codigo=car.Banco');
  QrCliCart.SQL.Add('WHERE car.ForneceI=' +
    dmkPF.FFP(QrCliIntCodigo.Value, 0));
  if Trim(EdCliCart.Text) <> '' then
    QrCliCart.SQL.Add('AND car.Nome LIKE "%' + EdCliCart.Text + '%"');
  if CkSohAtivos.Checked then
    QrCliCart.SQL.Add('AND car.Ativo=1');
  UMyMod.AbreQuery(QrCliCart, Dmod.MyDB, 'TFmCarteiras.ReopenCliCart()');
  //
  QrCliCart.Locate('Codigo', Carteira, []);
end;

procedure TFmCarteiras.ReopenCliInt();
begin
  QrCliInt.Close;
  QrCliInt.SQL.Clear;
  QrCliInt.SQL.Add('SELECT DISTINCT ent.Codigo, ent.CliInt,');
  QrCliInt.SQL.Add('IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMECLI');
  QrCliInt.SQL.Add('FROM entidades ent');
  QrCliInt.SQL.Add('LEFT JOIN carteiras car ON car.ForneceI=ent.Codigo');
  QrCliInt.SQL.Add('WHERE (ent.CliInt<>0');
  QrCliInt.SQL.Add('OR car.ForneceI <> 0)');
  QrCliInt.SQL.Add('');
  QrCliInt.SQL.Add('');
  if Trim(EdCliInt.Text) <> '' then
    QrCliInt.SQL.Add('AND (IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome)) LIKE "%' +
    EdCliInt.Text + '%"');
   UMyMod.AbreQuery(QrCliInt, Dmod.MyDB, 'TFmCarteiras.ReopenCliInt()');
end;

procedure TFmCarteiras.DBRGTipoChange(Sender: TObject);
begin
  DefinePanelShow();
end;

procedure TFmCarteiras.DBRGTipoClick(Sender: TObject);
begin
  DefinePanelShow();
end;

procedure TFmCarteiras.DefineONomeDoForm;
begin
end;

procedure TFmCarteiras.DefinePanelEdit();
begin
  case RGTipo.ItemIndex of
    -1:
    begin
      PnCaixEdit.Visible := False;
      PnBancEdit.Visible := False;
      PnEmisEdit.Visible := False;
    end;
    0:
    begin
      PnCaixEdit.Visible := True;
      PnBancEdit.Visible := False;
      PnEmisEdit.Visible := False;
    end;
    1:
    begin
      PnBancEdit.Visible := True;
      PnCaixEdit.Visible := False;
      PnEmisEdit.Visible := False;
    end;
    2:
    begin
      PnEmisEdit.Visible := True;
      PnCaixEdit.Visible := False;
      PnBancEdit.Visible := False;
    end;
  end;
end;

procedure TFmCarteiras.DefinePanelShow;
begin
  case DBRGTipo.ItemIndex of
    -1:
    begin
      PnCaixShow.Visible := False;
      PnBancShow.Visible := False;
      PnEmisShow.Visible := False;
    end;
    0:
    begin
      PnCaixShow.Visible := True;
      PnBancShow.Visible := False;
      PnEmisShow.Visible := False;
    end;
    1:
    begin
      PnBancShow.Visible := True;
      PnCaixShow.Visible := False;
      PnEmisShow.Visible := False;
    end;
    2:
    begin
      PnEmisShow.Visible := True;
      PnCaixShow.Visible := False;
      PnBancShow.Visible := False;
    end;
  end;
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCarteiras.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCarteiras.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCarteiras.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCarteiras.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCarteiras.BtIncluiClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMCarteira, BtInclui);
end;

procedure TFmCarteiras.Banco1Click(Sender: TObject);
begin
  MostraEdicao(1, CO_INCLUSAO, 0, 1);
end;

procedure TFmCarteiras.BtAlteraClick(Sender: TObject);
var
  Carteiras : Integer;
begin
  PageControl1.ActivePageIndex := 0;
  Carteiras := QrCarteirasCodigo.Value;
  if not UMyMod.SelLockY(Carteiras, Dmod.MyDB, 'Carteiras', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Carteiras, Dmod.MyDB, 'Carteiras', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0, -1);
      DefinePanelEdit();
      if QrCarteirasCodigo.Value = 0 then EdNome.Enabled := False
      else EdNome.Enabled := True;
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmCarteiras.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCarteirasCodigo.Value;
  Close;
end;

procedure TFmCarteiras.BtUsuarioClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMUsuario, BtUsuario);
end;

procedure TFmCarteiras.BtConfirmaClick(Sender: TObject);
var
  DiaMesVence, ExigeNumCheque, Codigo, Banco, Fechamento, Prazo, ForneceI,
  Banco1, Agencia1: Integer;
  Nome, Nome2, Fatura, Conta1: String;
  Inicial: Double;
begin
  DiaMesVence := Geral.IMV(EdDiaMesVence.Text);
  ExigeNumCheque := MLAGeral.BoolToInt(CkExigeNumCheque.Checked);
  Prazo := MLAGeral.BoolToInt(CkPrazo.Checked);
  Nome := EdNome.Text;
  Nome2 := EdNome2.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Fechamento := Geral.IMV(EdDdFechamento.Text);
  if CBBanco.KeyValue <> Null then Banco := CBBanco.KeyValue else
  begin
    if RGTipo.ItemIndex = 2 then
    begin
      ShowMessage('Defina um banco.');
      CBBanco.SetFocus;
      Exit;
    end else Banco := 0;
  end;
  ForneceI := Geral.IMV(EdForneceI.Text);
  if ForneceI = 0 then ForneceI := DmodG.QrMasterDono.Value;
  if CkFatura.Checked = True then Fatura := 'V' else Fatura := 'F';
  //
  Banco1   := 0;
  Agencia1 := 0;
  Conta1   := '';
  Inicial  := Geral.DMV(EdInicial_B.Text);

  if PnCaixEdit.Visible then
  begin
    Inicial := Geral.DMV(EdInicial_C.Text)
  end else if PnBancEdit.Visible then
  begin
    Banco1   := Geral.IMV(EdBanco1.Text);
    Agencia1 := Geral.IMV(EdAgencia1.Text);
    Conta1   := EdConta1.Text;
  end else if PnEmisEdit.Visible then
  begin
    Banco1   := Geral.IMV(EdBanco1_1.Text);
    Agencia1 := Geral.IMV(EdAgencia1_1.Text);
    Conta1   := EdConta1_1.Text;
  end;
  //if
  //
  Codigo := UMyMod.BuscaEmLivreY_Def_Old('Carteiras', 'Codigo', LaTipo.Caption, QrCarteirasCodigo.Value);
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, LaTipo.Caption, 'Carteiras', False,
  [
    'Nome', 'ID', 'Inicial', 'Banco', 'Fatura', 'ID_Fat',
    'Fechamento', 'Prazo',
    'PagRec', 'DiaMesVence', 'ExigeNumCheque',
    'ForneceI', 'Nome2', 'TipoDoc', 'Banco1', 'Agencia1', 'Conta1',
    'Ativo', 'Contato1', 'Contab', 'Ordem', 'ForneceN',
    'Exclusivo', 'RecebeBloq', 'EntiDent',
    'CodCedente', 'Tipo', 'IgnorSerie', 'NotConcBco'
  ], ['Codigo'],
  [
    Nome, EdID.Text, Inicial, Banco, Fatura, EdIDFat.Text,
    Fechamento, Prazo,
    RGPagRec.ItemIndex - 1, DiaMesVence, ExigeNumCheque,
    ForneceI, Nome2, RGTipoDoc.ItemIndex, Banco1, Agencia1, Conta1,
    MLAGeral.BoolToInt(CkAtivo.Checked), EdContato1.Text, EdContab.Text,
    Geral.IMV(EdOrdem.Text), MLAGeral.BoolToInt(CkForneceN.Checked),
    MLAGeral.BoolToInt(CkExclusivo.Checked),
    MLAGeral.BoolToInt(CkRecebeBloq.Checked), EdEntiDent.ValueVariant,
    Geral.SoNumero_TT(EdCodCedente.Text), RGTipo.ItemIndex,
    MLAGeral.BoolToInt(CkIgnorSerie.Checked),
    MLAGeral.BoolToInt(CkNotConcBco.Checked)
  ], [Codigo]) then
  begin
    // Atualizar lan�amentos j� emitidos para a carteira
    UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False,
      ['Tipo'], ['Carteira'], [RGTipo.ItemIndex], [Codigo], True, '');
    //
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Carteiras', 'Codigo');
    MostraEdicao(0, CO_TRAVADO, 0, -1);
    LocCod(Codigo, Codigo);
    UFinanceiro.RecalcSaldoCarteira(Codigo, QrCarteiras, True, True);
    Dmod.QrCarteiras.Close;
    UMyMod.AbreQuery(Dmod.QrCarteiras, Dmod.MyDB, 'TFmCarteiras.BtConfirmaClick()');
    QrBancos.Close;
    UMyMod.AbreQuery(QrBancos, Dmod.MyDB, 'TFmCarteiras.BtConfirmaClick()');
    //
    ReopenCliCart(Codigo);

  end;
end;

procedure TFmCarteiras.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Carteiras', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Carteiras', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0, -1);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Carteiras', 'Codigo');
end;

procedure TFmCarteiras.FormCreate(Sender: TObject);
begin
  DmodFin.CarregaItensTipoDoc(DBRGTipoDoc);
  DmodFin.CarregaItensTipoDoc(RGTipoDoc);
  FSeq := 0;
  PageControl1.ActivePageIndex := 0;
  if UpperCase(Application.Title) <> 'LESEW' then
  begin
    TabSheet2.TabVisible := False;
    BtUsuario.Visible    := False;
  end else
  begin
    TabSheet2.TabVisible := True;
    BtUsuario.Visible    := True;
  end;
  PnCartAtu.Align := alClient;
  PnTipoShow.Align  := alClient;
//  PainelDados.Align := alClient;
//  PnFast.Align      := alClient;
  CriaOForm;
  UMyMod.AbreQuery(QrBancos, Dmod.MyDB, 'TFmCarteiras.FormCreate()');
  UMyMod.AbreQuery(QrEntiDent, Dmod.MyDB, 'TFmCarteiras.FormCreate()');
  ReopenCliInt;
end;

procedure TFmCarteiras.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCarteirasCodigo.Value,LaRegistro.Caption);
end;

procedure TFmCarteiras.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCarteiras.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  VAR_MARCA := QrCarteirasCodigo.Value;
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmCarteiras.QrCarteirasAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCarteiras.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Carteiras', 'Livres', 99) then
  //BtInclui.Enabled := False;
  if FSeq = 1 then MostraEdicao(1, CO_INCLUSAO, 0, -1);
end;

procedure TFmCarteiras.QrCarteirasAfterScroll(DataSet: TDataSet);
begin
  //BtAltera.Enabled := GOTOy.BtEnabled(QrCarteirasCodigo.Value, False);
  ReopenCarteirasU(0);
  BtExclui.Enabled := GOTOy.BtEnabled(QrCarteirasCodigo.Value, False);
end;

procedure TFmCarteiras.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCarteirasCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Carteiras', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCarteiras.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmCarteiras.QrCarteirasBeforeOpen(DataSet: TDataSet);
begin
  QrCarteirasCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCarteiras.QrCarteirasCalcFields(DataSet: TDataSet);
begin
  QrCarteirasNOMETIPO.Value :=
    MLAGeral.TipoDeCarteiraCashier(QrCarteirasTipo.Value, False);
end;

procedure TFmCarteiras.QrCliCartAfterScroll(DataSet: TDataSet);
begin
  LocCod(QrCliCartCodigo.Value, QrCliCartCodigo.Value);
end;

procedure TFmCarteiras.QrCliIntAfterScroll(DataSet: TDataSet);
begin
  ReopenCliCart(0);
end;

procedure TFmCarteiras.EdNomeExit(Sender: TObject);
begin
  if EdNome2.Text = '' then
    EdNome2.Text := EdNome.Text;
end;

procedure TFmCarteiras.EdCliCartChange(Sender: TObject);
begin
  ReopenCliCart(0);
end;

procedure TFmCarteiras.EdCliIntChange(Sender: TObject);
begin
  ReopenCliInt();
end;

procedure TFmCarteiras.Adiciona1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCarteirasU, FmCarteirasU, afmoNegarComAviso) then
  begin
    FmCarteirasU.ShowModal;
    FmCarteirasU.Destroy;
  end;
end;

procedure TFmCarteiras.CkSohAtivosClick(Sender: TObject);
begin
  ReopenCliCart(0);
end;

procedure TFmCarteiras.Caixa1Click(Sender: TObject);
begin
  MostraEdicao(1, CO_INCLUSAO, 0, 0);
end;

procedure TFmCarteiras.Emisso1Click(Sender: TObject);
begin
  MostraEdicao(1, CO_INCLUSAO, 0, 2);
end;

procedure TFmCarteiras.ReopenCarteirasU(Controle: Integer);
begin
  QrCarteirasU.Close;
  QrCarteirasU.Params[0].AsInteger := QrCarteirasCodigo.Value;
  UMyMod.AbreQuery(QrCarteirasU, Dmod.MyDB, 'TFmCarteiras.ReopenCarteirasU()');
  //
  QrCarteirasU.Locate('Controle', Controle, []);
end;

end.

