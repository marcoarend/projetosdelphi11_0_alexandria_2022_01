unit UnFinanceiro;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mySQLDbTables, ComCtrls, dmkDBGrid, MyDBCheck, DBCtrls, dmkEditCB,
  dmkDBLookupComboBox, dmkEdit, dmkEditDateTimePicker, TypInfo, stdctrls,
  UnMyVCLref, dmkGeral, dmkLabel, dmkRadioGroup, dmkMemo, dmkdbEdit, DBGrids,
  dmkCheckBox, UnMLAGeral, DmkDAC_PF, UnDmkProcFunc, UnDmkEnums;

type
  TGerencimantoDeRegistro = (tgrInclui, tgrAltera, tgrDuplica, tgrExclui, tgrTeste);
  TInsAltReopenLct = function(): Boolean;
  TLanctoFinalidade = (lfUnknow, lfProprio, lfCondominio, lfCicloCurso);
  TUnFinanceiro = class(TObject)
  private
    { Private declarations }
    function TextoFinalidadeLancto(Finalidade: TLanctoFinalidade): String;
    //function InsAltReopenLct(f : TInsAltReopenLct): Boolean;
    function  CriaLanctoEditor(InstanceClass: TComponentClass; var Reference;
              ModoAcesso: TAcessFmModo; QrSelect: TmySQLQuery;
              Controle, Sub: Integer; Finalidade: TLanctoFinalidade): Boolean;

  public
    { Public declarations }
    function SQLInsUpd_Lct(QrUpd: TmySQLQuery; Tipo: TSQLType;
             Auto_increment: Boolean;
             SQLCampos, SQLIndex: array of String;
             ValCampos, ValIndex: array of Variant;
             UserDataAlterweb: Boolean; ComplUpd: String;
             // Compatibilidade
             TabLct: String = ''): Boolean;
    procedure AtzDataAtzSdoPlaCta(Entidade: Integer; Data: TDateTime);
            //InsAltLancamento
    function  InclusaoLancamento(
              InstanceClass: TComponentClass; var Reference;
              Finalidade: TLanctoFinalidade; ModoAcesso: TAcessFmModo;
              QrLct, QrCrt: TmySQLQuery; Acao: TGerencimantoDeRegistro;
              Controle, Sub, Genero: Integer; PercJuroM, PercMulta: Double;
              SetaVars: TInsAltReopenLct; FatID, FatID_Sub, FatNum, Carteira:
              Integer; Credito, Debito: Double; AlteraAtehFatID: Boolean;
              Cliente, Fornecedor, CliInt, ForneceI, Account, Vendedor: Integer;
              LockCliInt, LockForneceI, LockAccount, LockVendedor: Boolean;
              Data, Vencto,
              // adicionado 2011-01-25
              Compensado,
              DataDoc: TDateTime; IDFinalidade: Integer;
              // adicionado 2010-03-13
              Mes: TDateTime): Integer;
    function  AlteracaoLancamento(QrCrt, QrLct: TmySQLQuery; Finalidade: TLanctoFinalidade;
              OneAccount, OneCliInt: Integer; LockCliInt: Boolean): Boolean;
    function  InsereLancamento(): Boolean;
    function  SaldoAqui(Tipo: Integer; Atual: String;
              QrLct, QrCrt: TmySQLQuery): String;
    function  TransferenciaCorrompidaCart(Lancto: Double; QrCrt:
              TmySQLQuery): Boolean;
    function  TransferenciaCorrompidaCtas(Lancto: Double; QrCrt:
              TmySQLQuery): Boolean;
    function  LocalizarlanctoCliInt(Controle, Sub, CliInt: Integer;
              TPDataIni: TDateTimePicker; QrLctos, QrCarts, QrCartSum: TmySQLQuery;
              AvisaQuandoNaoEncontra: Boolean): Boolean;
    //  Forms
    function  NaoDuplicarLancto(LaTipo: String; Cheque: Extended; PesqCH:
              Boolean; SerieCH: String; NF: Integer; PesqNF: Boolean;
              Credito, Debito: Double; Conta: Integer; Mes: String;
              PesqVal: Boolean; CliInt: Integer; EstahEmLoop: Boolean;
              Carteira, Cliente, Fornecedor: Integer;
              LaAviso: TLabel): Integer;
    procedure LocalizarLancamento(DTPicker: TDateTimePicker;
              QrCrt, QrLct: TmySQLQuery; LocSohCliInt: Boolean;
              QuemChamou: Integer);
    procedure CriaFmQuitaDocs(Data: TDateTime; Documento, Controle,
              CtrlIni: Double; Sub, Carteira: Integer);
    procedure ImprimePlanoContas();
    {
    procedure CriarTransfer(Tipo: Integer; QrLct, QrCrt: TmySQLQuery;
              MostraForm: Boolean);
    }         // CriarTransfer2
    procedure CriarTransferCart(Tipo: Integer; QrLct, QrCrt: TmySQLQuery;
              MostraForm: Boolean; CliInt, FatID, FatID_Sub, FatNum: Integer);
    procedure CriarTransferCtas(Tipo: Integer; QrLct, QrCrt:
              TmySQLQuery; MostraForm: Boolean; CliInt, FatID, FatID_Sub, FatNum: Integer);
    //  Fim forms?

    //procedure ConfiguraFmTransfer;
    procedure ConfiguraFmTransferCart();
    procedure ConfiguraFmTransferCtas();
    procedure QuitaItemLanctoAutomatico(Data: TDateTime;
              QrLct, QrCrt: TmySQLQuery);
    procedure QuitacaoAutomaticaDmk(dmkDBGLct: TdmkDBGrid;
              QrLct, QrCrt: TmySQLQuery);
    procedure MudaValorEmCaixa(QrLct, QrCrt: TmySQLQuery);
    procedure ReverterPagtoEmissao(QrLct, QrCrt: TmySQLQuery;
              Pergunta, LocLancto, LocCarteira: Boolean);
    procedure ConsertaLctQuitados();
    procedure LancamentoDefaultVARS;
    procedure LancamentoDuplica(QrAux: TmySQLQuery; Controle, Sub: Integer);
    procedure ExcluiLanctoGrade(DBGLct: TDBGrid;
              QrLct, QrCrt: TmySQLQuery; ExcluiAtehFatID: Boolean);
    procedure LocalizaPagamentosDeEmissao(Controle: Integer;
              TPDataIni, TPDataFim: TDateTimePicker; Query: TmySQLQuery);
    function  ExcluiLct_FatParcela(DB: TmySQLDataBase;
              FatID: Integer; FatNum: Double; FatParcela, Carteira: Integer):
              Boolean;
    function  ExcluiLct_FatNum(DB: TmySQLDataBase; FatID: Integer;
              FatNum: Double; Entidade, Carteira: Integer;
              Pergunta: Boolean): Boolean;
    function  ExcluiLct_FatID(DB: TmySQLDataBase; FatID, Entidade: Integer;
              Carteira: Integer; Pergunta: Boolean): Boolean;
    function  ExcluiLct_EnviaArquivoMorto(TabLctMorto, TabLct: String; 
              Tipo, Carteira, Sub: Integer; Controle: Double;
              Query: TmySQLQuery; DataBase: TmySQLDatabase): Boolean;
    procedure ExcluiLct_Unico(ResetaConfig: Boolean; DB: TmySQLDataBase;
              Data: TDateTime; Tipo, Carteira: Integer; Controle: Double; Sub: Integer;
              Pergunta: Boolean);
    procedure ExcluiItemCarteira(Controle: Int64; Data: TDateTime;
              Sub, Genero, Cartao, Sit, Tipo, Chamada, ID_Pgto: Integer;
              QrLct, QrCrt: TmySQLQuery;
              Pergunta: Boolean; CartAtz, Carteira: Integer);
    procedure RecalcSaldoCarteira(Carteira: Integer;
              QrCrt: TmySQLQuery; Reabre, Localiza: Boolean);
    {
    procedure AtualizaEmissaoMasterExtra_Antigo(ID_Pgto: Integer; FQuery: TmySQLQuery;
              EdDeb, EdCred, EdSaldoDeb, EdSaldoCred: T L M D Edit);
    }
    procedure AtualizaEmissaoMasterExtra_Novo(ID_Pgto: Integer; FQuery: TmySQLQuery;
              EdDeb, EdCred, EdSaldoDeb, EdSaldoCred: TdmkEdit; DataCompQuit, Tabela: String);
    procedure AtualizaEmissaoMasterRapida(ID_Pgto, _Sit, Controle: Integer; DataCompQuit: String);
    procedure AtualizaVencimentos;
    procedure CriaFmQuitaDoc2(Data: TDateTime; Documento, Controle, CtrlIni:
              Double; Sub: Integer; QrCrt: TmySQLQuery);
    {
    function CarregaSQLInsUpd(QrUpd: TmySQLQuery; Tipo: TSQLType; Tabela: String;
             Auto_increment: Boolean;
             SQLCampos, SQLIndex: array of String;
             ValCampos, ValIndex: array of Variant;
             UserDataAlterweb, IGNORE: Boolean): Boolean;
    function SQLInsUpd(QrUpd: TmySQLQuery; Tipo: TSQLType; Tabela: String;
             Auto_increment: Boolean;
             SQLCampos, SQLIndex: array of String;
             ValCampos, ValIndex: array of Variant;
             UserDataAlterweb: Boolean): Boolean;
    function SQLInsUpd_IGNORE(QrUpd: TmySQLQuery; Tipo: TSQLType; Tabela: String;
             Auto_increment: Boolean;
             SQLCampos, SQLIndex: array of String;
             ValCampos, ValIndex: array of Variant;
             UserDataAlterweb: Boolean): Boolean;
    function SQLInsUpd_UpdExtra(QrUpd: TmySQLQuery; Tipo: TSQLType; Tabela: String;
             Auto_increment: Boolean;
             SQLCampos, SQLIndex, SQLSoUpd: array of String;
             ValCampos, ValIndex, ValSoUpd: array of Variant;
             UserDataAlterWeb: Boolean): Boolean;
    procedure CriaFormInsUpd(InstanceClass: TComponentClass;
              var Reference; ModoAcesso: TAcessFormModo; Query: TmySQLQuery;
              Acao: TSQLType);
    function ExecSQLInsUpdFm(Form: TForm; Acao: TSQLType; Tabela:
             String; NewItem: Variant; QrExec: TmySQLQuery): Boolean;
    function BuscaEmLivreY_Def(Table, Field: String; Acao: TSQLType; Atual: Integer): Integer;
    }
    function SQLLoc1(Query: TmySQLQuery; Tabela, Campo: String;
             Valor: Variant; MsgLoc, MsgNaoLoc: String): Integer;
    function AtualizaPagamentosAVista(QrLctos: TmySQLQuery): Boolean;
    function AtualizaSaldosDeContas(Entidade, Periodo: Integer;
             MostraVerif: Boolean): Boolean;
    function ProximoRegistro(Query: TmySQLQuery; Campo: String; Atual:
             Integer): Integer;
    {
    N�o usar. usar do UMySQLModule!
    function SQLDel1(QrExec, QrData: TmySQLQuery; Tabela, Campo: String;
             Valor: Variant; PerguntaSeExclui: Boolean;
             PerguntaAlternativa: String): Boolean;
    }
    function CST_A_Get(Codigo: Integer): String;
    function CST_B_Get(Codigo: Integer): String;
    function CST_A_Lista(): MyArrayLista;
    function CST_B_Lista(): MyArrayLista;
    function ListaDeSituacaoTributaria(): String;
    function ListaDeTributacaoPeloICMS(): String;
    //
    function CST_IPI_Get(Codigo: Integer): String;
    function CST_IPI_Lista(): MyArrayLista;
    function ListaDeCST_IPI(): String;
    //
    function CST_PIS_Get(Codigo: Integer): String;
    function CST_PIS_Lista(): MyArrayLista;
    function ListaDeCST_PIS(): String;
    //
    function CST_COFINS_Get(Codigo: Integer): String;
    function CST_COFINS_Lista(): MyArrayLista;
    function ListaDeCST_COFINS(): String;
    //
    function CSOSN_Get(CRT, Codigo: Integer): String;
    function CSOSN_Lista(): MyArrayLista;
    function ListaDeCSOSN(): String;
    //
    function CRT_Get(Codigo: Integer): String;
    function CRT_Lista(): MyArrayLista;
    function ListaDeCRT(): String;
    //
    function IndProc_Get(Codigo: Integer): String;
    function IndProc_Lista(): MyArrayLista;
    function ListaDeIndProc(): String;
    //
    function SPED_Tipo_Item_Get(Codigo: Integer): String;
    function SPED_Tipo_Item_Lista(): MyArrayLista;
    function ListaDeSPED_Tipo_Item(): String;
    //
    function SPED_EFD_IND_PERFIL_Get(Codigo: String): String;
    function SPED_EFD_IND_PERFIL_Lista(): MyArrayLista;
    function ListaDeSPED_EFD_IND_PERFIL(): String;
    //
    function SPED_EFD_IND_ATIV_Get(Codigo: Integer): String;
    function SPED_EFD_IND_ATIV_Lista(): MyArrayLista;
    function ListaDeSPED_EFD_IND_ATIV(): String;
    //
    //procedure AtualizaEndossos(Controle, Sub, OriCtrl, OriSub: Integer);
    //
    procedure ReabreCarteirasCliInt(CliInt, LocCart: Integer;
              QrCrt, QrCartSum: TmySQLQuery);
    function  NomeSitLancto(LctSit, LanctoTipo, CarteiraPrazo: Integer;
              LanctoVencto: TDateTime; Reparcelamento: Integer;
              ConverteCompensadoEmQuitado: Boolean = False): String;
    procedure VerificaID_Pgto_x_Compensado(LaAviso: TLabel; PB1: TProgressBar;
              QrNC: TmySQLQuery);
    function  LancamentosComProblemas(Tabela: String = 'lanctos'): Boolean;
    procedure AtualizaPagos();
    procedure CalculaValMultaEJuros(ValOrig: Double; DataNew, DataVencto: TDateTime;
              var MultaPerc, JuroPerc, ValorNew, MultaVal, JuroVal: Double;
              const PermitePercZero: Boolean);
    procedure CalculaPercMultaEJuros(ValOrig, MultaVal, JuroVal: Double;
              DataNew, DataVencto: TDateTime; var ValorNew, MultaPerc, JuroPerc: Double);
  end;
var
  UFinanceiro: TUnFinanceiro;
    FLAN_VERIFICA_CLIINT: Boolean;
    //
    FLAN_Data,
    FLAN_Vencimento,
    FLAN_DataCad,
    FLAN_Mez,
    FLAN_Descricao,
    FLAN_Compensado,
    FLAN_DataDoc,
    FLAN_Duplicata,
    FLAN_Doc2,
    FLAN_SerieCH,
    FLAN_Emitente,
    FLAN_CNPJCPF,
    //FLAN_Banco, int
    FLAN_ContaCorrente,
    FLAN_SerieNF: String;

    FLAN_Documento,
    FLAN_FatNum: Double;

    FLAN_Tipo,
    FLAN_Carteira,
    FLAN_Genero,
    FLAN_NotaFiscal,
    FLAN_Sit,
    FLAN_Controle,
    FLAN_ID_Pgto,
    FLAN_Cartao,
    FLAN_Linha,
    FLAN_Fornecedor,
    FLAN_Cliente,
    FLAN_UserCad,
    FLAN_Vendedor,
    FLAN_Account,
    FLAN_CliInt,
    FLAN_Depto,
    FLAN_DescoPor,
    FLAN_ForneceI,
    FLAN_Unidade,
    FLAN_FatID,
    FLAN_FatID_Sub,
    FLAN_FatParcela,
    FLAN_CtrlIni,
    FLAN_Nivel,
    FLAN_CNAB_Sit,
    FLAN_TipoCH,
    FLAN_Atrelado,
    FLAN_Sub,
    FLAN_Banco,
    FLAN_SubPgto1,
    FLAN_MultiPgto,
    FLAN_Protocolo,
    FLAN_EventosCad,
    FLAN_Agencia,
    FLAN_IndiPag: Integer;

    FLAN_Credito,
    FLAN_Debito,
    FLAN_MoraDia,
    FLAN_Multa,
    FLAN_ICMS_P,
    FLAN_ICMS_V,
    FLAN_DescoVal,
    FLAN_NFVal,
    FLAN_Qtde,
    FLAN_MoraVal,
    FLAN_MultaVal,
    FLAN_Pago: Double;

    VLAN_QRCARTEIRAS, VLAN_QRLCT: TmySQLQuery;

implementation

uses UnMyObjects, ModuleGeral, UMySQLModule, UnInternalConsts, UnMsgInt,
  //Forms:
  GetData, LocDefs, SomaDinheiro, LocPgto, ContasSdoImp, Transfer2, LctAjustes,
  TransferCtas, ContasHistAtz2, ContasHisVerif, SelOnStringGrid,
  LctEdita,
  // Fim forms
  ModuleFin, Module, UnMySQLCuringa, LctDuplic, QuitaDoc2;


procedure TUnFinanceiro.LocalizaPagamentosDeEmissao(Controle: Integer;
TPDataIni, TPDataFim: TDateTimePicker; Query: TmySQLQuery);
begin
  DmodFin.QrLPE.Close;
  DmodFin.QrLPE.Params[0].AsInteger := Controle;
  DmodFin.QrLPE.Open;
  //
  if DmodFin.QrLPE.RecordCount = 0 then
  begin
    Application.MessageBox(
      'N�o foi localizado nunhum pagamento para este lan�amento!', 'Aviso',
      MB_OK+MB_ICONWARNING);
  end else begin
    MyObjects.CriaForm_AcessoTotal(TFmLocPgto, FmLocPgto);
    FmLocPgto.FQuery := Query;
    FmLocPgto.FDTPDataIni := TPDataIni;
    FmLocPgto.FDTPDataFim := TPDataFim;
    FmLocPgto.ShowModal;
    FmLocPgto.Destroy;
  end;
end;

procedure TUnFinanceiro.ExcluiLanctoGrade(DBGLct: TDBGrid;
QrLct, QrCrt: TmySQLQuery; ExcluiAtehFatID: Boolean);
var
  i, k: Integer;
begin
  if QrLct = nil then
  begin
    Application.MessageBox(PChar('A grade ' + DBGLct.Name +
      ' n�o tem defini��o de "QrLct"!'), 'Aviso', MB_OK+MB_ICONERROR);
    Exit;
  end;

  if (ExcluiAtehFatID = False)
  and (QrLct.FieldByName('FatID').AsInteger <> 0) then
  begin
    Application.MessageBox(PChar('Exclus�o cancelada! Este lan�amento tem ' +
    'seu controle espec�fico em outra janela!'), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;

  if DBGLct.SelectedRows.Count > 1 then
  begin
    if Application.MessageBox(PChar('Confirma a exclus�o '+
    'dos itens selecionados?'), 'Pergunta', MB_ICONQUESTION+MB_YESNOCANCEL) =
    ID_YES then
    begin
      with DBGLct.DataSource.DataSet do
      for i:= 0 to DBGLct.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(DBGLct.SelectedRows.Items[i]));
        ExcluiItemCarteira(
          QrLct.FieldByName('Controle').AsInteger,
          QrLct.FieldByName('Data').AsDateTime,
          QrLct.FieldByName('Sub').AsInteger,
          QrLct.FieldByName('Genero').AsInteger,
          QrLct.FieldByName('Cartao').AsInteger,
          QrLct.FieldByName('Sit').AsInteger,
          QrLct.FieldByName('Tipo').AsInteger,
          0,
          QrLct.FieldByName('ID_Pgto').AsInteger,
          QrLct, QrCrt, False,
          QrLct.FieldByName('Carteira').AsInteger,
          QrLct.FieldByName('Carteira').AsInteger
          );
      end;
      k := UMyMod.ProximoRegistro(QrLct, 'Controle',  0);
      RecalcSaldoCarteira(QrLct.FieldByName('Carteira').AsInteger,
        QrCrt, True, True);
      QrLct.Locate('Controle', k, []);
    end;
  end else ExcluiItemCarteira(
    QrLct.FieldByName('Controle').AsInteger,
    QrLct.FieldByName('Data').AsDateTime,
    QrLct.FieldByName('Sub').AsInteger,
    QrLct.FieldByName('Genero').AsInteger,
    QrLct.FieldByName('Cartao').AsInteger,
    QrLct.FieldByName('Sit').AsInteger,
    QrLct.FieldByName('Tipo').AsInteger, 0,
    QrLct.FieldByName('ID_Pgto').AsInteger,
    QrLct, QrCrt, True,
    QrLct.FieldByName('Carteira').AsInteger,
    QrLct.FieldByName('Carteira').AsInteger
    );
end;

function TUnFinanceiro.ExcluiLct_FatParcela(DB: TmySQLDataBase;
FatID: Integer; FatNum: Double; FatParcela, Carteira: Integer): Boolean;
begin
  Result := False;
  if (FatID = 0) or (FatNum = 0) then
  Geral.MensagemBox('Remo��o de lan�amento cancelada!' + #13#10 +
  '"FatID" ou "FatNum" igual a zero n�o pode ser exclu�do',
  'Aviso', MB_OK+MB_ICONWARNING);
  begin
    DModFin.QrFats3.Close;
    DModFin.QrFats3.Params[00].AsInteger := FatID;
    DModFin.QrFats3.Params[01].AsFloat   := FatNum;
    DModFin.QrFats3.Params[02].AsInteger := FatParcela;
    DModFin.QrFats3.Open;
    //
    if DModFin.QrFats3.RecordCount = 1 then
    begin
      ExcluiLct_Unico(True, DB, DModFin.QrFats3Data.Value, DModFin.QrFats3Tipo.Value,
      DModFin.QrFats3Carteira.Value, DModFin.QrFats3Controle.Value,
      DModFin.QrFats3Sub.Value, True);
      Result := True;
      //
      DMod.QrUpdM.Close;
      DMod.QrUpdM.SQL.Clear;
      DMod.QrUpdM.SQL.Add('UPDATE ' + VAR_LCT + ' SET FatParcela=FatParcela-1');
      DMod.QrUpdM.SQL.Add('WHERE FatParcela>:P0 AND FatID=:P1 AND FatNum=:P2');
      DMod.QrUpdM.Params[00].AsInteger := FatParcela;
      DMod.QrUpdM.Params[01].AsInteger := FatID;
      DMod.QrUpdM.Params[02].AsFloat   := FatNum;
      DMod.QrUpdM.ExecSQL;
      //
      RecalcSaldoCarteira(Carteira, nil, False, False);
    end else
    Geral.MensagemBox('Remo��o de lan�amento cancelada!' + #13#10 +
    'Foi localizado mais de um lan�amento para o mesmo "FatID" e "FatNum"!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

function TUnFinanceiro.ExcluiLct_FatNum(DB: TmySQLDataBase; FatID: Integer;
FatNum: Double; Entidade, Carteira: Integer; Pergunta: Boolean): Boolean;
begin
  Result := False;
  if (FatID = 0) or (FatNum = 0) or (Entidade = 0) then
  begin
    Geral.MensagemBox('Remo��o de lan�amento cancelada!' + #13#10 +
    '"FatID" ou "FatNum" ou "Empresa" igual a zero n�o pode ser exclu�do.' +
    #13#10 + 'FatID = ' + FormatFloat('0', FatID) + #13#10 + 'FatNum = ' +
    FormatFloat('0', FatNum) + #13#10 + 'Empresa = ' + FormatFloat('0', Entidade),
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  begin
    DModFin.QrFats2.Close;
    DModFin.QrFats2.Params[00].AsInteger := FatID;
    DModFin.QrFats2.Params[01].AsFloat   := FatNum;
    DModFin.QrFats2.Params[02].AsInteger := Entidade;
    DModFin.QrFats2.Open;
    //
    if DModFin.QrFats2.RecordCount > 0 then
    begin
      if (Pergunta = False) or (Geral.MensagemBox(
      'Confirma a EXCLUS�O DE TODOS LAN�AMENTOS do faturamento selecionado?' + #13#10 +
      'FatID = ' + FormatFloat('0', FatID) + #13#10 +
      'FatNum = ' + FormatFloat('0', FatNum),
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES) then
      begin
        DModFin.QrFats2.First;
        while not DModFin.QrFats2.Eof do
        begin
          ExcluiLct_Unico(True, DB, DModFin.QrFats2Data.Value, DModFin.QrFats2Tipo.Value,
          DModFin.QrFats2Carteira.Value, DModFin.QrFats2Controle.Value,
          DModFin.QrFats2Sub.Value, False);
          //
          DModFin.QrFats2.Next;
        end;
        //Result := True;
        RecalcSaldoCarteira(Carteira, nil, False, False);
      end;
      Result := True;
    end;
  end;
  //
{
  Geral.MensagemBox('Remo��o de lan�amentos n�o realizada!' + #13#10 +
  'Remova manualmente na carteira de emiss�o!',
  'Aviso', MB_OK+MB_ICONWARNING);
}
end;

function TUnFinanceiro.ExcluiLct_EnviaArquivoMorto(TabLctMorto, TabLct: String;
  Tipo, Carteira, Sub: Integer; Controle: Double; Query: TmySQLQuery;
  DataBase: TmySQLDatabase): Boolean;
var
  CamposLctz, Dta: String;
begin
  Dta := Geral.FDT(DModG.ObtemAgora(), 105);
  //
  CamposLctz := UMyMod.ObtemCamposDeTabelaIdentica(DataBase, TabLctMorto, '');
  CamposLctz := Geral.Substitui(CamposLctz,
    ', DataDel', ', "' + Dta + '" DataDel');
  CamposLctz := Geral.Substitui(CamposLctz,
    ', UserDel', ', ' + FormatFloat('0', VAR_USUARIO) + ' UserDel');
  CamposLctz := Geral.Substitui(CamposLctz,
    ', MotvDel', ', ' + FormatFloat('0', 0) + ' MotvDel');
  //
  CamposLctz := 'INSERT INTO ' + TabLctMorto + ' SELECT ' + #13#10 +
  CamposLctz + #13#10 +
  'FROM ' + TabLct + #13#10 +
  'WHERE Tipo=:P0 AND Carteira=:P1 AND Controle=:P2 AND Sub=:P3;';
  //
  Query.SQL.Text := CamposLctz;
  //
  if Query.Database <> DataBase then
    Query.Database := DataBase;
  //
  Query.Params[00].AsInteger := Tipo;
  Query.Params[01].AsInteger := Carteira;
  Query.Params[02].AsFloat   := Controle;
  Query.Params[03].AsInteger := Sub;
  //
  UMyMod.ExecutaQuery(Query);
  //
  Result := True;
end;

function TUnFinanceiro.ExcluiLct_FatID(DB: TmySQLDataBase;
FatID, Entidade: Integer; Carteira: Integer; Pergunta: Boolean): Boolean;
begin
  Result := False;
  if FatID = 0 then
  Geral.MensagemBox('Remo��o de lan�amento cancelada!' + #13#10 +
  '"FatID" igual a zero n�o pode ser exclu�do', 'Aviso', MB_OK+MB_ICONWARNING);
  begin
    DModFin.QrFats1.Close;
    DModFin.QrFats1.Params[00].AsInteger := FatID;
    DModFin.QrFats1.Params[01].AsInteger := Entidade;
    DModFin.QrFats1.Open;
    //
    if DModFin.QrFats1.RecordCount > 0 then
    begin
      if (Pergunta = False) or (Geral.MensagemBox('Confirma a EXCLUS�O DE TODOS '
      + IntToStr(DModFin.QrFats1.RecordCount) + 'LAN�AMENTOS do IDFat = ' +
      IntToStr(FatID) + '?' + #13#10 +
      'FatID = ' + FormatFloat('0', FatID),
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES) then
      begin
        DModFin.QrFats1.First;
        while not DModFin.QrFats1.Eof do
        begin
          ExcluiLct_Unico(True, DB, DModFin.QrFats1Data.Value, DModFin.QrFats1Tipo.Value,
          DModFin.QrFats1Carteira.Value, DModFin.QrFats1Controle.Value,
          DModFin.QrFats1Sub.Value, False);
          //
          DModFin.QrFats1.Next;
        end;
        Result := True;
        RecalcSaldoCarteira(Carteira, nil, False, False);
      end;
    end;
  end;
end;

procedure TUnFinanceiro.ExcluiLct_Unico(ResetaConfig: Boolean; DB: TmySQLDataBase;
Data: TDateTime; Tipo, Carteira: Integer; Controle: Double; Sub: Integer;
Pergunta: Boolean);
var
  Dta, CamposLctz: String;
begin
  if Pergunta then
  begin
    if Geral.MensagemBox('Confirma a exclus�o do lan�amento:' + #13#10 +
    'Controle: ' + FormatFloat('0', Controle) + ' - ' + FormatFloat('0', Sub) +
    #13#10 + 'Data: ' + Geral.FDT(Data, 2) + #13#10 + 'Carteira: ' +
    FormatFloat('0', Carteira), 'Pergunta', MB_YESNOCANCEL + MB_ICONQUESTION)
    <> ID_YES then Exit;
  end;
  if ResetaConfig then
  begin
    (*
    Dta := Geral.FDT(DModG.ObtemAgora(), 105);
    DmodFin.FCamposLctz := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, 'lanctoz', '');
    DmodFin.FCamposLctz := Geral.Substitui(DmodFin.FCamposLctz,
      ', DataDel', ', "' + Dta + '" DataDel');
    DmodFin.FCamposLctz := Geral.Substitui(DmodFin.FCamposLctz,
      ', UserDel', ', ' + FormatFloat('0', VAR_USUARIO) + ' UserDel');
    //
    DmodFin.FCamposLctz := 'INSERT INTO lanctoz SELECT ' + #13#10 +
    DmodFin.FCamposLctz + #13#10 +
    'FROM ' + VAR_LCT + #13#10 +
    'WHERE Data=:P0 AND Tipo=:P1 AND Carteira=:P2 AND Controle=:P3 AND Sub=:P4;' +
    #13#10 + DELETE_FROM + VAR_LCT + #13#10 +
    'WHERE Data=:P5 AND Tipo=:P6 AND Carteira=:P7 AND Controle=:P8 AND Sub=:P9;';
    DModFin.QrDelLct.SQL.Text := DmodFin.FCamposLctz;
    *)
    ExcluiLct_EnviaArquivoMorto('lanctoz', VAR_LCT, Tipo, Carteira,
      Sub, Controle, DModFin.QrDelLct, Dmod.MyDB);
  end;
  CamposLctz := DELETE_FROM + VAR_LCT + #13#10 +
    'WHERE Data=:P0 AND Tipo=:P1 AND Carteira=:P2 AND Controle=:P3 AND Sub=:P4;';
  DModFin.QrDelLct.SQL.Text := CamposLctz;
  //
  if DModFin.QrDelLct.Database <> DB then
    DModFin.QrDelLct.Database := DB;
  Dta := Geral.FDT(Data, 1);
  DModFin.QrDelLct.Params[00].AsString  := Dta;
  DModFin.QrDelLct.Params[01].AsInteger := Tipo;
  DModFin.QrDelLct.Params[02].AsInteger := Carteira;
  DModFin.QrDelLct.Params[03].AsFloat   := Controle;
  DModFin.QrDelLct.Params[04].AsInteger := Sub;
  UMyMod.ExecutaQuery(DModFin.QrDelLct);
  (*
  DModFin.QrDelLct.Params[00].AsString  := Dta;
  DModFin.QrDelLct.Params[01].AsInteger := Tipo;
  DModFin.QrDelLct.Params[02].AsInteger := Carteira;
  DModFin.QrDelLct.Params[03].AsFloat   := Controle;
  DModFin.QrDelLct.Params[04].AsInteger := Sub;
  DModFin.QrDelLct.Params[05].AsString  := Dta;
  DModFin.QrDelLct.Params[06].AsInteger := Tipo;
  DModFin.QrDelLct.Params[07].AsInteger := Carteira;
  DModFin.QrDelLct.Params[08].AsFloat   := Controle;
  DModFin.QrDelLct.Params[09].AsInteger := Sub;
  *)
  UMyMod.ExecutaQuery(DModFin.QrDelLct);
end;

function TUnFinanceiro.AlteracaoLancamento(QrCrt, QrLct: TmySQLQuery; Finalidade:
TLanctoFinalidade; OneAccount, OneCliInt: Integer; LockCliInt: Boolean): Boolean;
var
  Sit, Tipo, Cartao: Integer;
begin
  Result := False;
  if QrLct = nil then
  begin
    Geral.MensagemBox('Tabela de lan�amentos n�o definida!',
    'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  if QrLct.RecordCount = 0 then
  begin
    Geral.MensagemBox('Nenhum lan�amento foi selecionado!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if QrLct.FieldByName('Genero').Value = -1 then
  begin
    Geral.MensagemBox('Altera��o cancelada!' + #13#10 +
    'Lan�amento pertence a uma transfer�ncia entre carteiras!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if QrLct.FieldByName('FatID').Value = -1 then
  begin
    Geral.MensagemBox('Altera��o cancelada!' + #13#10 +
    'Lan�amento pertence a uma transfer�ncia entre contas do plano de contas!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Sit    := QrLct.FieldByName('Sit').AsInteger;
  Tipo   := QrLct.FieldByName('Tipo').AsInteger;
  Cartao := QrLct.FieldByName('Cartao').AsInteger;
  //
  if UmyMod.FormInsUpd_Cria(TFmLctEdita, FmLctEdita,
  afmoNegarComAviso, QrLct, stUpd) then
  begin
    with FmLctEdita do
    begin
      FQrCrt        := QrCrt;
      FQrLct        := QrLct;
      FFinalidade   := Finalidade;
      FOneAccount   := OneAccount;
      FOneCliInt    := OneCliInt;
      //
      if (Sit > 1) and (Tipo = 2) then
      begin
        if (Cartao <> 0) and not VAR_FATURANDO then
        begin
          Geral.MensagemBox('Esta emiss�o j� est� quitada!',
            'Aviso', MB_OK+MB_ICONWARNING);
          EdDeb.Enabled       := False;
          EdCred.Enabled      := False;
          EdDoc.Enabled       := False;
          EdDescricao.Enabled := False;
        end;
        EdCarteira.Enabled := False;
        CBCarteira.Enabled := False;
        VAR_BAIXADO := Sit;
      end else VAR_BAIXADO := -2;
      //
      LaCliInt.Enabled := not LockCliInt;
      EdCliInt.Enabled := not LockCliInt;
      CBCliInt.Enabled := not LockCliInt;
      //
      ShowModal;
      Destroy;
    end;
  end;
end;

procedure TUnFinanceiro.ExcluiItemCarteira(Controle: Int64; Data: TDateTime;
Sub, Genero, Cartao, Sit, Tipo, Chamada, ID_Pgto: Integer;
QrLct, QrCrt: TmySQLQuery; Pergunta: Boolean;
CartAtz, Carteira: Integer);
var
  Continua, Prox, CliInt: Integer;
begin
  CliInt := QrLct.FieldByName('CliInt').AsInteger;
  //if UMyMod.SelLockInt64Y(Lancto, Dmod.MyDB, VAR_LCT, 'Controle') then Exit;
  VAR_LANCTO2 := Controle;
  if (Cartao > 0) then
  begin
    ShowMessage('Esta emiss�o n�o pode ser exclu�da pois pertence a uma fatura!');
    Exit;
  end;
  if (Sit > 1) and (Tipo = 2) then
  begin
    ShowMessage('Esta emiss�o n�o pode ser exclu�da pois est� quitada!');
    Exit;
  end;
  if (Genero < 0) or (Sub > 0) then
  begin
    if Genero = -1 then
    begin
      CriarTransferCart(2, QrLct, QrCrt, True, CliInt, 0, 0, 0);
      Exit;
    end else if Chamada = 1 (*FmCartPagto*) then
      // Exclui normalmente
    else begin
      ShowMessage('Em constru��o. Para editar este item selecione o menu de op��es');
      Exit;
    end;
  end else if Genero = 0 then
  begin
    Application.MessageBox('Exclus�o cancelada. Lan�amento sem conta!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Pergunta then Continua := Application.MessageBox(FIN_MSG_ASKESCLUI,
    PChar(VAR_APPNAME), MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+MB_APPLMODAL)
  else Continua := ID_YES;
  if Continua = ID_YES then
  begin
    ExcluiLct_Unico(
      True, QrLct.Database, Data, Tipo, Carteira, Controle, Sub, False);
    if ID_Pgto > 0 then
      AtualizaEmissaoMasterExtra_Novo(ID_Pgto, QrLct, nil, nil, nil, nil, '', 'lanctos');
    if Pergunta then
    begin
      Prox := UMyMod.ProximoRegistro(QrLct, 'Controle',  Controle);
      RecalcSaldoCarteira(Carteira, QrCrt, True, true);
      QrLct.Locate('Controle', Prox, []);
    end;
  end;
end;

procedure TUnFinanceiro.RecalcSaldoCarteira(Carteira: Integer;
  QrCrt: TmySQLQuery; Reabre, Localiza: Boolean);
begin
  AtualizaVencimentos;
  //
  DmodFin.QrAtzCar.Close;
  DmodFin.QrAtzCar.Params[0].AsInteger := Carteira;
  DmodFin.QrAtzCar.Open;
  //
  DmodFin.QrUpd.SQL.Clear;
  DmodFin.QrUpd.SQL.Add('UPDATE carteiras SET Saldo=:P0, ');
  DmodFin.QrUpd.SQL.Add('FuturoC=:P1, FuturoD=:P2, FuturoS=:P3 ');
  DmodFin.QrUpd.SQL.Add('');
  DmodFin.QrUpd.SQL.Add('WHERE Codigo=:Pa ');
  DmodFin.QrUpd.Params[00].AsFloat   := DmodFin.QrAtzCarINICIAL.Value + DmodFin.QrAtzCarATU_S.Value;
  DmodFin.QrUpd.Params[01].AsFloat   := DmodFin.QrAtzCarFUT_C.Value;
  DmodFin.QrUpd.Params[02].AsFloat   := DmodFin.QrAtzCarFUT_D.Value;
  DmodFin.QrUpd.Params[03].AsFloat   := DmodFin.QrAtzCarFUT_S.Value;
  DmodFin.QrUpd.Params[04].AsInteger := Carteira;
  DmodFin.QrUpd.ExecSQL;
  //
  if Reabre and (QrCrt <> nil) then
  begin
    QrCrt.Close;
    QrCrt.Open;
  end;
  if Localiza and (QrCrt <> nil) then
    QrCrt.Locate('Codigo', Carteira, []);
end;

{
procedure TUnFinanceiro.AtualizaEmissaoMasterExtra_Antigo(ID_Pgto: Integer; FQuery: TmySQLQuery;
 EdDeb, EdCred, EdSaldoDeb, EdSaldoCred: T L M D Edit);
var
  SaldoCred, SaldoDeb, Pago: Double;
  Sit : Integer;
  QrSoma: TmySQLQuery;
  Data: String;
begin
  // Pagamentos emitidos
  QrSoma := TmySQLQuery.Create(Dmod);
  QrSoma.Close;
  QrSoma.Database := Dmod.MyDB;
  QrSoma.SQL.Clear;
  QrSoma.SQL.Add('SELECT SUM(Credito) Credito, SUM(Debito) Debito, ');
  QrSoma.SQL.Add('MAX(Data) Data ');
  QrSoma.SQL.Add('FROM ' + VAR_LCT + ' WHERE ID_Pgto=:P0');
  QrSoma.Params[0].AsInteger := ID_Pgto;
  QrSoma.Open;

  // Endosso
  DmodFin.QrVLA.Close;
  DmodFin.QrVLA.Params[0].AsInteger := ID_Pgto;
  DmodFin.QrVLA.Open;
  //

  SaldoDeb := QrSoma.FieldByName('Debito').AsFloat - FQuery.FieldByName('Debito').AsFloat + DmodFin.QrVLAEndossval.Value;
  SaldoCred := QrSoma.FieldByName('Credito').AsFloat - FQuery.FieldByName('Credito').AsFloat + DmodFin.QrVLAEndossval.Value;
  Pago := QrSoma.FieldByName('Credito').AsFloat - QrSoma.FieldByName('Debito').AsFloat + DmodFin.QrVLAEndossval.Value;
  //
  if EdDeb <> nil then
    EdDeb.Text := Geral.TFT(FloatToStr(QrSoma.FieldByName('Debito').AsFloat), 2, siNegativo);
  if EdCred <> nil then
    EdCred.Text := Geral.TFT(FloatToStr(QrSoma.FieldByName('Credito').AsFloat), 2, siNegativo);
  if EdSaldoDeb <> nil then
    EdSaldoDeb.Text := Geral.TFT(FloatToStr(SaldoDeb), 2, siNegativo);
  if EdSaldoCred <> nil then
    EdSaldoCred.Text := Geral.TFT(FloatToStr(QrSoma.FieldByName('Credito').AsFloat -
      FQuery.FieldByName('Credito').AsFloat), 2, siNegativo);
  Data := '';
  if (SaldoCred = 0) and (SaldoDeb = 0) then
  begin
    Sit := 2;
    Data := Geral.FDT(QrSoma.FieldByName('Data').AsDateTime, 1);
  end else if (QrSoma.FieldByName('Debito').AsFloat <> 0) or (QrSoma.FieldByName('Credito').AsFloat <> 0) then
  Sit := 1 else Sit := 0;
  QrSoma.Close;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE ' + VAR_LCT + ' SET Sit=:P0, Pago=:P1, Compensado=:P2 ');
  Dmod.QrUpdM.SQL.Add('WHERE Controle=:P3');
  Dmod.QrUpdM.Params[00].AsInteger := Sit;
  Dmod.QrUpdM.Params[01].AsFloat   := Pago;
  Dmod.QrUpdM.Params[02].AsString  := Data;
  //
  Dmod.QrUpdM.Params[03].AsInteger := ID_Pgto;
  Dmod.QrUpdM.ExecSQL;
  RecalcSaldoCarteira(FQuery.FieldByName('Carteira').AsInteger,
    nil, False, False);
end;
}

procedure TUnFinanceiro.AtualizaEmissaoMasterExtra_Novo(ID_Pgto: Integer; FQuery: TmySQLQuery;
 EdDeb, EdCred, EdSaldoDeb, EdSaldoCred: TdmkEdit; DataCompQuit, Tabela: String);
var
  Pago, MoraVal, MultaVal: Double;
  Controle, _Sit, Sit : Integer;
  Data: String;
begin
  _Sit := 0;     // Mudar ? J� trazer valor atual como no AtualizaEmissaoMasterRapida (_Sit) ?
  Controle := 0; // Mudar ? J� trazer valor atual como no AtualizaEmissaoMasterRapida (Controle) ?
  if not DmodFin.Reopen_APL_VLA(ID_Pgto, _Sit, Data,
    Pago, MoraVal, MultaVal, Sit, DataCompQuit) then
      Exit;
  //
  //
  { Fazer mostrar ??  Parei Aqui
  if EdDeb <> nil then
    EdDeb.Text := Geral.TFT(FloatToStr(Debito), 2, siNegativo);
  if EdCred <> nil then
    EdCred.Text := Geral.TFT(FloatToStr(Credito), 2, siNegativo);
  if EdSaldoDeb <> nil then
    EdSaldoDeb.Text := Geral.TFT(FloatToStr(SaldoDeb), 2, siNegativo);
  if EdSaldoCred <> nil then
    EdSaldoCred.Text := Geral.TFT(FloatToStr(Credito -
      FQuery.FieldByName('Credito').AsFloat), 2, siNegativo);
  }
  //
{
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET Sit=:P0, Pago=:P1, Compensado=:P2, ');
  Dmod.QrUpdM.SQL.Add('PagJur=:P3, PagMul=:P4, CtrlQuitPg=:P5');
  Dmod.QrUpdM.SQL.Add('WHERE Controle=:P6');
  Dmod.QrUpdM.Params[00].AsInteger := Sit;
  Dmod.QrUpdM.Params[01].AsFloat   := Pago;
  Dmod.QrUpdM.Params[02].AsString  := Data;
  Dmod.QrUpdM.Params[03].AsFloat   := MoraVal;
  Dmod.QrUpdM.Params[04].AsFloat   := MultaVal;
  Dmod.QrUpdM.Params[05].AsInteger := Controle;
  //
  Dmod.QrUpdM.Params[06].AsInteger := ID_Pgto;
  Dmod.QrUpdM.ExecSQL;
}
  SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
  'Sit', 'Pago', 'Compensado', 'PagJur', 'PagMul', 'CtrlQuitPg'], ['Controle'], [
  Sit, Pago, Data, MoraVal, MultaVal, Controle], [ID_Pgto], True, '');
  //
  RecalcSaldoCarteira(FQuery.FieldByName('Carteira').AsInteger,
    nil, False, False);
end;

procedure TUnFinanceiro.AtualizaEmissaoMasterRapida(ID_Pgto, _Sit, Controle: Integer; DataCompQuit: String);
var
  Pago, MoraVal, MultaVal: Double;
  Sit : Integer;
  Data: String;
begin
  if not DmodFin.Reopen_APL_VLA(ID_Pgto, _Sit, Data,
    Pago, MoraVal, MultaVal, Sit, DataCompQuit) then
      Exit;
  //
  {
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET Sit=:P0, Pago=:P1, Compensado=:P2, ');
  Dmod.QrUpdM.SQL.Add('PagJur=:P3, PagMul=:P4, CtrlQuitPg=:P5');
  Dmod.QrUpdM.SQL.Add('WHERE Controle=:P6');
  Dmod.QrUpdM.Params[00].AsInteger := Sit;
  Dmod.QrUpdM.Params[01].AsFloat   := Pago;
  Dmod.QrUpdM.Params[02].AsString  := Data;
  Dmod.QrUpdM.Params[03].AsFloat   := MoraVal;
  Dmod.QrUpdM.Params[04].AsFloat   := MultaVal;
  Dmod.QrUpdM.Params[05].AsInteger := Controle;
  //
  Dmod.QrUpdM.Params[06].AsInteger := ID_Pgto;
  Dmod.QrUpdM.ExecSQL;
  }
  SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
  'Sit', 'Pago', 'Compensado', 'PagJur', 'PagMul', 'CtrlQuitPg'], ['Controle'], [
  Sit, Pago, Data, MoraVal, MultaVal, Controle], [ID_Pgto], True, '');
end;

function TUnFinanceiro.InsereLancamento(): Boolean;
begin
  if FLAN_Tipo = 2 then
  begin
    //FLAN_ID_Pgto := 0;
  end else begin
    if (FLAN_Compensado = '') or (FLAN_Compensado = '0000-00-00') then
    begin
      if FLAN_Tipo = 1 then
      begin
        Dmod.QrLocY.Database := Dmod.MyDB;
        Dmod.QrLocY.Close;
        Dmod.QrLocY.SQL.Clear;
        Dmod.QrLocY.SQL.Add('SELECT Prazo Record');
        Dmod.QrLocY.SQL.Add('FROM carteiras');
        Dmod.QrLocY.SQL.Add('WHERE Codigo=' + IntToStr(FLAN_Carteira));
        Dmod.QrLocY.Open;
        //
        if Dmod.QrLocYRecord.Value = 0 then
          FLAN_Compensado := FLAN_Data;
      end else FLAN_Compensado := FLAN_Data;
    end;
  end;
  if FLAN_VERIFICA_CLIINT then
  begin
    if (FLAN_CliInt < 1) and (FLAN_CliInt > -11) then
    begin
      Dmod.QrLocY.Database := Dmod.MyDB;
      Dmod.QrLocY.Close;
      Dmod.QrLocY.SQL.Clear;
      Dmod.QrLocY.SQL.Add('SELECT ForneceI Record');
      Dmod.QrLocY.SQL.Add('FROM carteiras');
      Dmod.QrLocY.SQL.Add('WHERE Codigo=' + IntToStr(FLAN_Carteira));
      Dmod.QrLocY.Open;
      if Dmod.QrLocYRecord.Value <> FLAN_CliInt then
      begin
        if VAR_CliIntUnico <> 0 then
          FLAN_CliInt := VAR_CliIntUnico
        else begin
          if Dmod.QrLocYRecord.Value = 0 then
          begin
            Application.MessageBox(PChar('CUIDADO!! Cliente interno n�o definido ' +
            'para o lan�amento ' + FormatFloat('#,###,###,###', FLAN_Controle) +
            '. A carteira selecionada (n� ' + IntToStr(FLAN_Carteira) +
            ') tamb�m n�o possui cliente interno!'#13#10'AVISE A DERMATEK'),
            'AVISO', MB_OK+MB_ICONWARNING);
          end else begin
            FLAN_CliInt := Dmod.QrLocYRecord.Value;
            Application.MessageBox(PChar('CUIDADO!! Cliente interno n�o definido ' +
            'para o lan�amento ' + FormatFloat('#,###,###,###', FLAN_Controle) +
            '. Ser� considerado o cliente interno n� ' + IntToStr(FLAN_CliInt) +
            ' que � dono da carteira informada (n� ' + IntToStr(FLAN_Carteira) +
            ')!'#13#10'AVISE A DERMATEK'), 'AVISO', MB_OK+MB_ICONWARNING);
          end;
        end;
      end;
    end;
  end;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('INSERT INTO ' + VAR_LCT + ' SET Documento=:P0, Data=:P1, ');
  Dmod.QrUpdM.SQL.Add('Tipo=:P2, Carteira=:P3, Credito=:P4, Debito=:P5, ');
  Dmod.QrUpdM.SQL.Add('Genero=:P6, NotaFiscal=:P7, Vencimento=:P8, Mez=:P9, ');
  Dmod.QrUpdM.SQL.Add('Descricao=:P10, Sit=:P11, Controle=:P12, Cartao=:P13, ');
  Dmod.QrUpdM.SQL.Add('Compensado=:P14, Linha=:P15, Fornecedor=:P16, ');
  Dmod.QrUpdM.SQL.Add('Cliente=:P17, MoraDia=:P18, Multa=:P19, DataCad=:P20, ');
  Dmod.QrUpdM.SQL.Add('UserCad=:P21, DataDoc=:P22, Vendedor=:P23, ');
  Dmod.QrUpdM.SQL.Add('Account=:P24, FatID=:P25, FatID_Sub=:P26, ');
  Dmod.QrUpdM.SQL.Add('ICMS_P=:P27, ICMS_V=:P28, Duplicata=:P29, ');
  Dmod.QrUpdM.SQL.Add('CliInt=:P30, Depto=:P31, DescoPor=:P32, ');
  Dmod.QrUpdM.SQL.Add('ForneceI=:P33, DescoVal=:P34, NFVal=:P35, ');
  Dmod.QrUpdM.SQL.Add('Unidade=:P36, Qtde=:P37, FatNum=:P38, ');
  Dmod.QrUpdM.SQL.Add('FatParcela=:P39, Doc2=:P40, SerieCH=:P41, ');
  Dmod.QrUpdM.SQL.Add('MultaVal=:P42, MoraVal=:P43, CtrlIni=:P44, ');
  Dmod.QrUpdM.SQL.Add('Nivel=:P45, ID_Pgto=:P46, CNAB_Sit=:P47, ');
  Dmod.QrUpdM.SQL.Add('TipoCH=:P48, Atrelado=:P49, Sub=:P50, ');
  Dmod.QrUpdM.SQL.Add('Emitente=:P51, CNPJCPF=:P52, Banco=:P53, ');
  Dmod.QrUpdM.SQL.Add('Agencia=:P54, ContaCorrente=:P55, SubPgto1=:P56, ');
  Dmod.QrUpdM.SQL.Add('MultiPgto=:P57, SerieNF=:P58, Protocolo=:P59, ');
  Dmod.QrUpdM.SQL.Add('Pago=:P60, EventosCad=:P61, IndiPag=:P62 ');

  //
  Dmod.QrUpdM.Params[00].AsFloat   := FLAN_Documento;
  Dmod.QrUpdM.Params[01].AsString  := FLAN_Data;
  Dmod.QrUpdM.Params[02].AsInteger := FLAN_Tipo;
  Dmod.QrUpdM.Params[03].AsInteger := FLAN_Carteira;
  Dmod.QrUpdM.Params[04].AsFloat   := FLAN_Credito;
  Dmod.QrUpdM.Params[05].AsFloat   := FLAN_Debito;
  Dmod.QrUpdM.Params[06].AsInteger := FLAN_Genero;
  Dmod.QrUpdM.Params[07].AsInteger := FLAN_NotaFiscal;
  Dmod.QrUpdM.Params[08].AsString  := FLAN_Vencimento;
  Dmod.QrUpdM.Params[09].AsString  := FLAN_Mez;
  Dmod.QrUpdM.Params[10].AsString  := FLAN_Descricao;
  Dmod.QrUpdM.Params[11].AsInteger := FLAN_Sit;
  Dmod.QrUpdM.Params[12].AsFloat   := FLAN_Controle;
  Dmod.QrUpdM.Params[13].AsInteger := FLAN_Cartao;
  Dmod.QrUpdM.Params[14].AsString  := FLAN_Compensado;
  Dmod.QrUpdM.Params[15].AsInteger := FLAN_Linha;
  Dmod.QrUpdM.Params[16].AsInteger := FLAN_Fornecedor;
  Dmod.QrUpdM.Params[17].AsInteger := FLAN_Cliente;
  Dmod.QrUpdM.Params[18].AsFloat   := FLAN_MoraDia;
  Dmod.QrUpdM.Params[19].AsFloat   := FLAN_Multa;
  Dmod.QrUpdM.Params[20].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdM.Params[21].AsInteger := VAR_USUARIO;
  Dmod.QrUpdM.Params[22].AsString  := FLAN_DataDoc;
  Dmod.QrUpdM.Params[23].AsInteger := FLAN_Vendedor;
  Dmod.QrUpdM.Params[24].AsInteger := FLAN_Account;
  Dmod.QrUpdM.Params[25].AsInteger := FLAN_FatID;
  Dmod.QrUpdM.Params[26].AsInteger := FLAN_FatID_Sub;
  Dmod.QrUpdM.Params[27].AsFloat   := FLAN_ICMS_P;
  Dmod.QrUpdM.Params[28].AsFloat   := FLAN_ICMS_V;
  Dmod.QrUpdM.Params[29].AsString  := FLAN_Duplicata;
  Dmod.QrUpdM.Params[30].AsInteger := FLAN_CliInt;
  Dmod.QrUpdM.Params[31].AsInteger := FLAN_Depto;
  Dmod.QrUpdM.Params[32].AsInteger := FLAN_DescoPor;
  Dmod.QrUpdM.Params[33].AsInteger := FLAN_ForneceI;
  Dmod.QrUpdM.Params[34].AsFloat   := FLAN_DescoVal;
  Dmod.QrUpdM.Params[35].AsFloat   := FLAN_NFVal;
  Dmod.QrUpdM.Params[36].AsInteger := FLAN_Unidade;
  Dmod.QrUpdM.Params[37].AsFloat   := FLAN_Qtde;
  Dmod.QrUpdM.Params[38].AsFloat   := FLAN_FatNum;
  Dmod.QrUpdM.Params[39].AsInteger := FLAN_FatParcela;
  Dmod.QrUpdM.Params[40].AsString  := FLAN_Doc2;
  Dmod.QrUpdM.Params[41].AsString  := FLAN_SerieCH;
  Dmod.QrUpdM.Params[42].AsFloat   := FLAN_MultaVal;
  Dmod.QrUpdM.Params[43].AsFloat   := FLAN_MoraVal;
  Dmod.QrUpdM.Params[44].AsInteger := FLAN_CtrlIni;
  Dmod.QrUpdM.Params[45].AsInteger := FLAN_Nivel;
  Dmod.QrUpdM.Params[46].AsFloat   := FLAN_ID_Pgto;
  Dmod.QrUpdM.Params[47].AsInteger := FLAN_CNAB_Sit;
  Dmod.QrUpdM.Params[48].AsInteger := FLAN_TipoCH;
  Dmod.QrUpdM.Params[49].AsInteger := FLAN_Atrelado;
  Dmod.QrUpdM.Params[50].AsInteger := FLAN_Sub;
  Dmod.QrUpdM.Params[51].AsString  := FLAN_Emitente;
  Dmod.QrUpdM.Params[52].AsString  := FLAN_CNPJCPF;
  Dmod.QrUpdM.Params[53].AsInteger := FLAN_Banco;
  Dmod.QrUpdM.Params[54].AsInteger := FLAN_Agencia;
  Dmod.QrUpdM.Params[55].AsString  := FLAN_ContaCorrente;
  Dmod.QrUpdM.Params[56].AsInteger := FLAN_SubPgto1;
  Dmod.QrUpdM.Params[57].AsInteger := FLAN_MultiPgto;
  Dmod.QrUpdM.Params[58].AsString  := FLAN_SerieNF;
  Dmod.QrUpdM.Params[59].AsInteger := FLAN_Protocolo;
  Dmod.QrUpdM.Params[60].AsFloat   := FLAN_Pago;
  Dmod.QrUpdM.Params[61].AsInteger := FLAN_EventosCad;
  Dmod.QrUpdM.Params[62].AsInteger := FLAN_IndiPag;

  Dmod.QrUpdM.ExecSQL;
  Result := True;
  //
  if FLAN_ID_Pgto <> 0 then
    AtualizaEmissaoMasterRapida(FLAN_ID_Pgto, FLAN_Sit, FLAN_Controle, FLAN_Data);
  Application.ProcessMessages;
end;

procedure TUnFinanceiro.LancamentoDefaultVARS;
begin
  // N�o permite lan�amento para outro cliint que n�o seja o dono da carteira!
  FLAN_VERIFICA_CLIINT := True;
  //
  FLAN_Data          := '0000-00-00';
  FLAN_Vencimento    := Geral.FDT(Date, 1);
  FLAN_DataCad       := Geral.FDT(Date, 1);
  FLAN_Mez           := '0';
  FLAN_Descricao     := '';
  FLAN_Compensado    := '0000-00-00';
  FLAN_Duplicata     := '';
  FLAN_Doc2          := '';
  FLAN_SerieCH       := '';

  FLAN_Documento     := 0;
  FLAN_Tipo          := -1;
  FLAN_Carteira      := -1;
  FLAN_Credito       := 0;
  FLAN_Debito        := 0;
  FLAN_Genero        := -1000;
  FLAN_SerieNF       := '';
  FLAN_NotaFiscal    := 0;
  FLAN_Sit           := 0;
  FLAN_Controle      := 0;
  FLAN_Sub           := 0;
  FLAN_ID_Pgto       := 0;
  FLAN_Cartao        := 0;
  FLAN_Linha         := 0;
  FLAN_Fornecedor    := 0;
  FLAN_Cliente       := 0;
  FLAN_MoraDia       := 0;
  FLAN_Multa         := 0;
  FLAN_UserCad       := VAR_USUARIO;
  FLAN_DataDoc       := Geral.FDT(Date, 1);
  FLAN_Vendedor      := 0;
  FLAN_Account       := 0;
  FLAN_ICMS_P        := 0;
  FLAN_ICMS_V        := 0;
  FLAN_CliInt        := VAR_CliIntUnico;
  FLAN_Depto         := 0;
  FLAN_DescoPor      := 0;
  FLAN_ForneceI      := 0;
  FLAN_DescoVal      := 0;
  FLAN_NFVal         := 0;
  FLAN_Unidade       := 0;
  FLAN_Qtde          := 0;
  FLAN_FatID         := 0;
  FLAN_FatID_Sub     := 0;
  FLAN_FatNum        := 0;
  FLAN_FatParcela    := 0;
  FLAN_Pago          := 0;
  //
  FLAN_MultaVal      := 0;
  FLAN_MoraVal       := 0;
  FLAN_CtrlIni       := 0;
  FLAN_Nivel         := 0;
  FLAN_CNAB_Sit      := 0;
  FLAN_TipoCH        := 0;
  FLAN_Atrelado      := 0;
  FLAN_SubPgto1      := 0;
  FLAN_MultiPgto     := 0;
  FLAN_Protocolo     := 0;
  FLAN_EventosCad    := 0;
  FLAN_IndiPag       := 0;
  //
  FLAN_Emitente      := '';
  FLAN_CNPJCPF       := '';
  FLAN_Banco         := 0;
  FLAN_Agencia       := 0;
  FLAN_ContaCorrente := '';
end;

procedure TUnFinanceiro.LancamentoDuplica(QrAux: TmySQLQuery; Controle, Sub: Integer);
var
  Inteiro: Integer;
begin
  UFinanceiro.LancamentoDefaultVARS;
  QrAux.Close;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('SELECT * FROM ' + VAR_LCT + ' ');
  QrAux.SQL.Add('WHERE Controle=' + FormatFloat('0', Controle));
  QrAux.SQL.Add('AND SUB=' + FormatFloat('0', Sub));
  QrAux.Open;
  //
  FLAN_Data          := Geral.FDT(QrAux.FieldByName('Data'      ).AsDateTime, 1);
  FLAN_Vencimento    := Geral.FDT(QrAux.FieldByName('Vencimento').AsDateTime, 1);
  FLAN_DataCad       := Geral.FDT(QrAux.FieldByName('DataCad'   ).AsDateTime, 1);
  //
  Inteiro := QrAux.FieldByName('Mez').AsInteger;
  if Inteiro > 0 then
  FLAN_Mez           := IntToStr(Inteiro) else
  FLAN_Mez           := '';
  //
  FLAN_Descricao     := QrAux.FieldByName('Descricao' ).AsString;
  FLAN_Compensado    := Geral.FDT(QrAux.FieldByName('Compensado').AsDateTime, 1);
  FLAN_Duplicata     := QrAux.FieldByName('Duplicata' ).AsString;
  FLAN_Doc2          := QrAux.FieldByName('Doc2'      ).AsString;
  FLAN_SerieCH       := QrAux.FieldByName('SerieCH'   ).AsString;

  FLAN_Documento     := QrAux.FieldByName('Documento' ).AsInteger;
  FLAN_Tipo          := QrAux.FieldByName('Tipo'      ).AsInteger;
  FLAN_Carteira      := QrAux.FieldByName('Carteira'  ).AsInteger;
  FLAN_Credito       := QrAux.FieldByName('Credito'   ).AsFloat;
  FLAN_Debito        := QrAux.FieldByName('Debito'    ).AsFloat;
  FLAN_Genero        := QrAux.FieldByName('Genero'    ).AsInteger;
  FLAN_SerieNF       := QrAux.FieldByName('SerieNF'   ).AsString;
  FLAN_NotaFiscal    := QrAux.FieldByName('NotaFiscal').AsInteger;
  FLAN_Sit           := QrAux.FieldByName('Sit'       ).AsInteger;
  FLAN_Controle      := QrAux.FieldByName('Controle'  ).AsInteger;
  FLAN_Sub           := QrAux.FieldByName('Sub'       ).AsInteger;
  FLAN_ID_Pgto       := QrAux.FieldByName('ID_Pgto'   ).AsInteger;
  FLAN_Cartao        := QrAux.FieldByName('Cartao'    ).AsInteger;
  FLAN_Linha         := QrAux.FieldByName('Linha'     ).AsInteger;
  FLAN_Fornecedor    := QrAux.FieldByName('Fornecedor').AsInteger;
  FLAN_Cliente       := QrAux.FieldByName('Cliente'   ).AsInteger;
  FLAN_MoraDia       := QrAux.FieldByName('MoraDia'   ).AsFloat;
  FLAN_Multa         := QrAux.FieldByName('Multa'     ).AsFloat;
  FLAN_UserCad       := QrAux.FieldByName('UserCad'   ).AsInteger;
  FLAN_DataDoc       := Geral.FDT(QrAux.FieldByName('DataDoc'   ).AsDateTime, 1);
  FLAN_Vendedor      := QrAux.FieldByName('Vendedor'  ).AsInteger;
  FLAN_Account       := QrAux.FieldByName('Account'   ).AsInteger;
  FLAN_ICMS_P        := QrAux.FieldByName('ICMS_P'    ).AsFloat;
  FLAN_ICMS_V        := QrAux.FieldByName('ICMS_V'    ).AsFloat;
  FLAN_CliInt        := QrAux.FieldByName('CliInt'    ).AsInteger;
  FLAN_Depto         := QrAux.FieldByName('Depto'     ).AsInteger;
  FLAN_DescoPor      := QrAux.FieldByName('DescoPor'  ).AsInteger;
  FLAN_ForneceI      := QrAux.FieldByName('ForneceI'  ).AsInteger;
  FLAN_DescoVal      := QrAux.FieldByName('DescoVal'  ).AsFloat;
  FLAN_NFVal         := QrAux.FieldByName('NFVal'     ).AsFloat;
  FLAN_Unidade       := QrAux.FieldByName('Unidade'   ).AsInteger;
  FLAN_Qtde          := QrAux.FieldByName('Qtde'      ).AsFloat;
  FLAN_FatID         := QrAux.FieldByName('FatID'     ).AsInteger;
  FLAN_FatID_Sub     := QrAux.FieldByName('FatID_Sub' ).AsInteger;
  FLAN_FatNum        := QrAux.FieldByName('FatNum'    ).AsInteger;
  FLAN_FatParcela    := QrAux.FieldByName('FatParcela').AsInteger;
  //                    //
  FLAN_MultaVal      := QrAux.FieldByName('MultaVal'  ).AsFloat;
  FLAN_MoraVal       := QrAux.FieldByName('MoraVal'   ).AsFloat;
  FLAN_CtrlIni       := QrAux.FieldByName('CtrlIni'   ).AsInteger;
  FLAN_Nivel         := QrAux.FieldByName('Nivel'     ).AsInteger;
  FLAN_CNAB_Sit      := QrAux.FieldByName('CNAB_Sit'  ).AsInteger;
  FLAN_TipoCH        := QrAux.FieldByName('TipoCH'    ).AsInteger;
  FLAN_Atrelado      := QrAux.FieldByName('Atrelado'  ).AsInteger;
  //                    //
  FLAN_Emitente      := QrAux.FieldByName('Emitente'  ).AsString;
  FLAN_CNPJCPF       := QrAux.FieldByName('CNPJCPF'   ).AsString;
  FLAN_Banco         := QrAux.FieldByName('Banco'     ).AsInteger;
  FLAN_Agencia       := QrAux.FieldByName('Agencia'   ).AsInteger;
  FLAN_ContaCorrente := QrAux.FieldByName('ContaCorrente').AsString;
  FLAN_SubPgto1      := QrAux.FieldByName('SubPgto1'     ).AsInteger;
  FLAN_MultiPgto     := QrAux.FieldByName('MultiPgto'    ).AsInteger;
  FLAN_Protocolo     := 0;// n�o duplicar! QrAux.FieldByName('Protocolo'    ).AsInteger;
  //
end;

function TUnFinanceiro.LancamentosComProblemas(Tabela: String = 'lanctos'): Boolean;
var
  Erros: Integer;
begin
  // Tabela: apenas por compatibilidade com financeiro novo 2010-11-13
  if DBCheck.CriaFm(TFmLctAjustes, FmLctAjustes, afmoLiberado) then
  begin
    FmLctAjustes.ShowModal;
    Erros := FmLctAjustes.ErrosEncontrados;
    FmLctAjustes.Destroy;
    Result := Erros > 0;
    if Result then
      Geral.MensagemBox('Foram encontrados ' + IntToStr(Erros) +
      ' que devem ser corrigidos!', 'Aviso', MB_OK+MB_ICONERROR);
  end else Result := True;
end;

function TUnFinanceiro.ListaDeCRT: String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 16;
      Grade.ColWidths[01] := 44;
      Grade.ColWidths[02] := 715;
      Width := 800;
      FColSel := 1;
      Caption := 'CRT';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := CRT_Lista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;

function TUnFinanceiro.ListaDeCSOSN(): String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 4;
      Grade.ColWidths[00] := 16;
      Grade.ColWidths[01] := 16;
      Grade.ColWidths[02] := 44;
      Grade.ColWidths[03] := 800;
      Width := 1024;
      FColSel := 2;
      Caption := 'CSOSN do CRT=1';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[00,00] := 'CRT';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := CSOSN_Lista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
        Grade.Cells[3, j] := Lista[i][2];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;

function TUnFinanceiro.ListaDeCST_COFINS: String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 56;
      Grade.ColWidths[01] := 56;
      Grade.ColWidths[02] := 400;
      Width := 548;
      FColSel := 1;
      Caption := 'CST COFINS';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := CST_COFINS_Lista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;

function TUnFinanceiro.ListaDeCST_IPI(): String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 56;
      Grade.ColWidths[01] := 56;
      Grade.ColWidths[02] := 400;
      Width := 548;
      FColSel := 1;
      Caption := 'CST IPI';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := UFinanceiro.CST_IPI_Lista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;

function TUnFinanceiro.ListaDeCST_PIS(): String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 56;
      Grade.ColWidths[01] := 56;
      Grade.ColWidths[02] := 400;
      Width := 548;
      FColSel := 1;
      Caption := 'CST PIS';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := CST_PIS_Lista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;

function TUnFinanceiro.ListaDeIndProc(): String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 16;
      Grade.ColWidths[01] := 44;
      Grade.ColWidths[02] := 715;
      Width := 800;
      FColSel := 1;
      Caption := 'Origem de Processo';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := IndProc_Lista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;

function TUnFinanceiro.ListaDeSituacaoTributaria(): String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 56;
      Grade.ColWidths[01] := 56;
      Grade.ColWidths[02] := 400;
      Width := 548;
      FColSel := 1;
      Caption := 'Situa��o Tribut�ria';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := UFinanceiro.CST_A_Lista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;

function TUnFinanceiro.ListaDeSPED_EFD_IND_ATIV(): String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 56;
      Grade.ColWidths[01] := 56;
      Grade.ColWidths[02] := 400;
      Width := 548;
      FColSel := 1;
      Caption := 'Indicador de tipo de atividade';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := UFinanceiro.SPED_EFD_IND_ATIV_Lista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;

function TUnFinanceiro.ListaDeSPED_EFD_IND_PERFIL(): String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 56;
      Grade.ColWidths[01] := 56;
      Grade.ColWidths[02] := 400;
      Width := 548;
      FColSel := 1;
      Caption := 'Perfil de apresenta��o do arquivo fiscal';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := UFinanceiro.SPED_EFD_IND_PERFIL_Lista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;

function TUnFinanceiro.ListaDeSPED_Tipo_Item(): String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 56;
      Grade.ColWidths[01] := 56;
      Grade.ColWidths[02] := 400;
      Width := 548;
      FColSel := 1;
      Caption := 'Tipo de Item';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := UFinanceiro.SPED_Tipo_Item_Lista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;

function TUnFinanceiro.ListaDeTributacaoPeloICMS(): String;
var
  i,j: Integer;
  Lista: MyArrayLista;
begin
  if DBCheck.CriaFm(TFmSelOnStringGrid, FmSelOnStringGrid, afmoNegarComAviso) then
  begin
    with FmSelOnStringGrid do
    begin
      Grade.ColCount      := 3;
      Grade.ColWidths[00] := 56;
      Grade.ColWidths[01] := 56;
      Grade.ColWidths[02] := 400;
      Width := 548;
      FColSel := 1;
      Caption := 'Tributa��o pelo ICMS';
      Grade.Cells[00,00] := 'Seq';
      Grade.Cells[01,00] := 'C�digo';
      Grade.Cells[02,00] := 'Descri��o';
      Lista := CST_B_Lista();
      j := 0;
      for i := Low(Lista) to High(Lista) do
      begin
        j := j + 1;
        Grade.RowCount := j + 1;
        Grade.Cells[0, j] := IntToStr(j);
        Grade.Cells[1, j] := Lista[i][0];
        Grade.Cells[2, j] := Lista[i][1];
      end;
      ShowModal;
      if FResult <> '' then
        Result := FResult;
      Destroy;
    end;
  end;
end;

procedure TUnFinanceiro.ConsertaLctQuitados();
var
  Atz: Integer;
begin
  Screen.Cursor := crHourGlass;
  Atz := 0;
  DmodFin.QrErrQuit.Close;
  DmodFin.QrErrQuit.Open;
  if DmodFin.QrErrQuit.RecordCount > 0 then
  begin
    {
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET ID_Quit=:P0, Compensado=:P1 ');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa AND Sub=0');
    Dmod.QrUpd.SQL.Add('');
    }
    while not DmodFin.QrErrQuit.Eof do
    begin
      Atz := Atz + 1;
      {
      Dmod.QrUpd.Params[00].AsInteger := DmodFin.QrErrQuitControle2.Value;
      Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DmodFin.QrErrQuitData.Value, 1);
      //
      Dmod.QrUpd.Params[02].AsInteger := DmodFin.QrErrQuitControle1.Value;
      Dmod.QrUpd.ExecSQL;
      //
      DmodFin.QrErrQuit.Next;
      }
      SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
      'ID_Quit', 'Compensado'], ['Controle'], [DmodFin.QrErrQuitControle2.Value,
      Geral.FDT(DmodFin.QrErrQuitData.Value, 1)], [
      DmodFin.QrErrQuitControle1.Value], True, '');
    end;
  end;
  Screen.Cursor := crDefault;
  Application.MessageBox(PChar('Foram atualizados ' + IntToStr(Atz) +
  ' registros de ' + IntToStr(DmodFin.QrErrQuit.RecordCount)), 'Informa��o',
  MB_OK+MB_ICONINFORMATION);
end;

procedure TUnFinanceiro.AtualizaVencimentos;
begin
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE ' + VAR_LCT + ' SET Vencimento=Data');
  Dmod.QrUpdM.SQL.Add('WHERE (Vencimento+Data)=Data');
  Dmod.QrUpdM.ExecSQL;
end;

procedure TUnFinanceiro.ReverterPagtoEmissao(QrLct, QrCrt: TmySQLQuery;
 Pergunta, LocLancto, LocCarteira: Boolean);
var
  Reverte: Boolean;
  Controle: Double;
  Continua, Genero, Sub, CartLoc: Integer;
begin
  Controle := QrLct.FieldByName('Controle').AsInteger;
  Sub      := QrLct.FieldByName('Sub').AsInteger;
  Genero   := QrLct.FieldByName('Genero').AsInteger;
  CartLoc  := QrCrt.FieldByName('Codigo').AsInteger;
  Reverte  := False;
  if not Pergunta then Continua := ID_YES else Continua :=
    Application.MessageBox('Confirma a revers�o da compensa��o?', PChar(VAR_APPNAME),
  MB_ICONQUESTION+MB_YESNOCANCEL+MB_DEFBUTTON1+MB_APPLMODAL);
  if Continua = ID_YES then
  begin
    DmodFin.QrLocLct.Close;
    DmodFin.QrLocLct.SQL.Clear;
    DmodFin.QrLocLct.SQL.Add('SELECT * FROM ' + VAR_LCT + '');
    DmodFin.QrLocLct.SQL.Add('WHERE ID_Pgto=:P0 AND Sub=:P1');//para achar em qualquer carteira
    DmodFin.QrLocLct.Params[0].AsFloat := Controle;
    DmodFin.QrLocLct.Params[1].AsInteger := Sub;
    DmodFin.QrLocLct.Open;
    if DmodFin.QrLocLct.RecordCount = 0 then
      ShowMessage('AVISO. N�o foi localizado nenhum registro.');
    if DmodFin.QrLocLct.RecordCount in [0, 1] then Reverte := True;
    if DmodFin.QrLocLct.RecordCount > 1 then
    begin
      if Application.MessageBox('Foi localizada mais de uma compensa��o. Todas ser�o excluidas. Deseja continuar?',
      PChar(VAR_APPNAME), MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+MB_APPLMODAL) =
      ID_YES then Reverte := True else
      begin
        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add('UPDATE ' + VAR_LCT + ' SET Sit=2, Genero=:P0 ');
        Dmod.QrUpdM.SQL.Add('WHERE (ID_Pgto=:P1 OR Controle=:P2) AND Sub=:P3');
        Dmod.QrUpdM.Params[00].AsInteger := Genero;
        Dmod.QrUpdM.Params[01].AsFloat   := Controle;
        Dmod.QrUpdM.Params[02].AsFloat   := Controle;
        Dmod.QrUpdM.Params[03].AsInteger := Sub;
        Dmod.QrUpdM.ExecSQL;
        ShowMessage('Esta compensa��o foi convertida para quita��o. Assim � poss�vel edit�-la.');
      end;
    end;
    //
    if Reverte then
    begin
      DmodFin.QrLcts.Close;
      DmodFin.QrLcts.SQL.Clear;
      DmodFin.QrLcts.SQL.Add('SELECT Data, Tipo, Carteira, Controle, Sub');
      DmodFin.QrLcts.SQL.Add('FROM lanctos');
      DmodFin.QrLcts.SQL.Add('WHERE ID_Pgto=:P0');
      DmodFin.QrLcts.Params[0].AsFloat := Controle;
      DmodFin.QrLcts.Open;
      while not DmodFin.QrLcts.Eof do
      begin
        ExcluiLct_Unico(True, QrLct.Database, DmodFin.QrLctsData.Value,
          DmodFin.QrLctsTipo.Value, DmodFin.QrLctsCarteira.Value,
          DmodFin.QrLctsControle.Value, DmodFin.QrLctsSub.Value, False);
        //
        DmodFin.QrLcts.Next;
      end;
      DmodFin.QrLocLct.First;
      while not DmodFin.QrLocLct.Eof do
      begin
        UFinanceiro.RecalcSaldoCarteira(DmodFin.QrLocLctCarteira.Value,
          nil, False, False);
        DmodFin.QrLocLct.Next;
      end;
      SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
        'Compensado', 'Sit', 'Genero'], ['Controle', 'Sub'], [
        '', 0, Genero], [Controle, Sub], True, '');
      if Pergunta then
        UFinanceiro.RecalcSaldoCarteira(
          QrCrt.FieldByName('Codigo').AsInteger, nil, False, False)
      else
        UFinanceiro.RecalcSaldoCarteira(
          QrCrt.FieldByName('Codigo').AsInteger, nil, False, False);
    end;
  end;
  AtualizaEmissaoMasterExtra_Novo(QrLct.FieldByName('Controle').AsInteger,
    QrLct, nil, nil, nil, nil, '', 'lanctos');
  if LocCarteira then
    QrCrt.Locate('Codigo', CartLoc, []);
  if LocLancto then  
    QrLct.Locate('Controle', Controle, []);
end;

function TUnFinanceiro.SaldoAqui(Tipo: Integer; Atual: String;
QrLct, QrCrt: TmySQLQuery): String;
var
  Saldo: Double;
  Real: String;
begin
  case Tipo of
    0: Result := '';
    1:
    begin
      DmodFin.QrSaldo.Close;
      DmodFin.QrSaldo.SQL.Clear;
      DmodFin.QrSaldo.SQL.Add('SELECT SUM(Credito-Debito) Valor FROM ' + VAR_LCT + '');
      DmodFin.QrSaldo.SQL.Add('WHERE Tipo=:P0 AND Carteira=:P1');
      DmodFin.QrSaldo.SQL.Add('AND (Data<:P2 OR (Data=:P3 AND Controle<=:P4))');
      DmodFin.QrSaldo.Params[0].AsInteger := QrLct.FieldByName('Tipo').AsInteger;
      DmodFin.QrSaldo.Params[1].AsInteger := QrLct.FieldByName('Carteira').AsInteger;
      DmodFin.QrSaldo.Params[2].AsString :=
        FormatDateTime(VAR_FORMATDATE, QrLct.FieldByName('Data').AsDateTime);
      DmodFin.QrSaldo.Params[3].AsString :=
        FormatDateTime(VAR_FORMATDATE, QrLct.FieldByName('Data').AsDateTime);
      DmodFin.QrSaldo.Params[4].AsFloat := QrLct.FieldByName('Controle').AsInteger;
      DmodFin.QrSaldo.Open;
      Saldo := DmodFin.QrSaldoValor.Value + QrCrt.FieldByName('Inicial').AsFloat;
      DmodFin.QrSaldo.Close;
      Result := Geral.TFT(FloatToStr(Saldo), 2, siNegativo);
    end;
    2:
    begin
      Real := InputBox(Application.Title, 'Informe o saldo real:', CO_VAZIO);
      Saldo := Geral.DMV(Real);
      Saldo := Saldo - Geral.DMV(Atual);
      ShowMessage(Geral.TFT(Real, 2, siNegativo)+' - '+Atual+' = '+
        Geral.TFT(FloatToStr(Saldo), 2, siNegativo));
    end;
    else Application.MessageBox('Tipo de a��o n�o definido!', 'Erro',
    MB_OK+MB_ICONERROR);
  end;
end;

function TUnFinanceiro.SPED_EFD_IND_ATIV_Get(Codigo: Integer): String;
begin
// Indicador de tipo de atividade:
  case Codigo of
    00: Result := 'Industrial ou equiparado a industrial';
    01: Result := 'Outros';
    else Result := '?';
  end;
end;

function TUnFinanceiro.SPED_EFD_IND_ATIV_Lista(): MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const Codigo, Descricao: String);
  begin
    SetLength(Res, Linha + 1);
    SetLength(Res[Linha], 3);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
//TABELA IND_ATIV
  AddLinha(Result, Linha, '0', 'Industrial ou equiparado a industrial');
  AddLinha(Result, Linha, '1', 'Outros');
end;

function TUnFinanceiro.SPED_EFD_IND_PERFIL_Get(Codigo: String): String;
begin
// Indicador de tipo de atividade:
  if Uppercase(Codigo) = 'A' then Result := 'Perfil A' else
  if Uppercase(Codigo) = 'B' then Result := 'Perfil B' else
  if Uppercase(Codigo) = 'C' then Result := 'Perfil C' else
  Result := '?';
end;

function TUnFinanceiro.SPED_EFD_IND_PERFIL_Lista(): MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const Codigo, Descricao: String);
  begin
    SetLength(Res, Linha + 1);
    SetLength(Res[Linha], 3);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
//TABELA IND_PERFIL
  AddLinha(Result, Linha, 'A', 'Perfil A');
  AddLinha(Result, Linha, 'B', 'Perfil B');
  AddLinha(Result, Linha, 'C', 'Perfil C');
end;

function TUnFinanceiro.SPED_Tipo_Item_Get(Codigo: Integer): String;
begin
// Tipo de Item
  case Codigo of
    00: Result := 'Mercadoria para revenda';
    01: Result := 'Mat�ria-Prima';
    02: Result := 'Embalagem';
    03: Result := 'Produto em Processo';
    04: Result := 'Produto Acabado';
    05: Result := 'Subproduto';
    06: Result := 'Produto intermedi�rio';
    07: Result := 'Material de Uso e Consumo';
    08: Result := 'Ativo Imobilizado';
    09: Result := 'Servi�os';
    10: Result := 'Outros Insumos';
    99: Result := 'Outras';
    else Result := '';
  end;
end;

function TUnFinanceiro.SPED_Tipo_Item_Lista: MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const Codigo, Descricao: String);
  begin
    SetLength(Res, Linha + 1);
    SetLength(Res[Linha], 3);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
//TABELA IndProc
  AddLinha(Result, Linha, '00', 'Mercadoria para revenda');
  AddLinha(Result, Linha, '01', 'Mat�ria-Prima');
  AddLinha(Result, Linha, '02', 'Embalagem');
  AddLinha(Result, Linha, '03', 'Produto em Processo');
  AddLinha(Result, Linha, '04', 'Produto Acabado');
  AddLinha(Result, Linha, '05', 'Subproduto');
  AddLinha(Result, Linha, '06', 'Produto intermedi�rio');
  AddLinha(Result, Linha, '07', 'Material de Uso e Consumo');
  AddLinha(Result, Linha, '08', 'Ativo Imobilizado');
  AddLinha(Result, Linha, '09', 'Servi�os');
  AddLinha(Result, Linha, '10', 'Outros Insumos');
  AddLinha(Result, Linha, '99', 'Outras');
end;

procedure TUnFinanceiro.MudaValorEmCaixa(QrLct, QrCrt: TmySQLQuery);
var
//  Numero : String;
  Lancto: Int64;
  Valor : Double;
  OK: Boolean;
  //Carteira: Integer;
begin
  Lancto := QrLct.FieldByName('Controle').AsInteger;
  //Carteira := QrLct.FieldByName('Carteira').AsInteger;
  if QrCrt.FieldByName('Tipo').AsInteger > 0 then
  begin
    ShowMessage('Carteira inv�lida.');
    Exit;
  end;
  if QrCrt.FieldByName('Codigo').AsInteger = 0 then
  begin
    ShowMessage('Este caixa n�o pode ter saldo.');
    Exit;
  end;
  OK := False;
  //Numero := FloatToStr(QrCrtEmCaixa.Value);
  VAR_CALC := 1;
  VAR_QRCARTEIRAS := QrCrt;
  MyObjects.CriaForm_AcessoTotal(TFmSomaDinheiro, FmSomaDinheiro);
  FmSomaDinheiro.ShowModal;
  FmSomaDinheiro.Destroy;
  if VAR_CALC = 1 then Valor := VAR_VALOR else Exit;
  if Valor = 0 then begin
    if Application.MessageBox('Confirma o zeramento do caixa?', PChar(VAR_APPNAME),
    MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+
    MB_APPLMODAL) = ID_YES then  OK := True;
  end else OK := True;
  if OK then
  begin
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('UPDATE carteiras SET EmCaixa=:P0');
    Dmod.QrUpdM.SQL.Add('WHERE Codigo=:P1');
    Dmod.QrUpdM.Params[0].AsFloat := Valor;
    Dmod.QrUpdM.Params[1].AsInteger := QrCrt.FieldByName('Codigo').AsInteger;
    Dmod.QrUpdM.ExecSQL;
    VAR_LANCTO2 := Lancto;
    UFinanceiro.RecalcSaldoCarteira(QrCrt.FieldByName('Codigo').AsInteger,
      QrCrt, True, True);
  end;
  VAR_LANCTO2 := Lancto;
end;

procedure TUnFinanceiro.QuitacaoAutomaticaDmk(dmkDBGLct: TdmkDBGrid;
QrLct, QrCrt: TmySQLQuery);
var
  i, Controle: Integer;
begin
  //
  Controle := QrLct.FieldByName('Controle').AsInteger;
  //
  MyObjects.CriaForm_AcessoTotal(TFmGetData, FmGetData);
  FmGetData.TPData.Date := date;
  FmGetData.ShowModal;
  FmGetData.Destroy;
  if VAR_GETDATA = 0 then Exit;
  //
  if dmkDBGLct.SelectedRows.Count > 1 then
  begin
    if Application.MessageBox(PChar('Confirma a quita��o autom�tica '+
    'dos itens selecionados?'), 'Pergunta', MB_ICONQUESTION+MB_YESNOCANCEL) =
    ID_YES then
    begin
      with dmkDBGLct.DataSource.DataSet do
      for i:= 0 to dmkDBGLct.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(dmkDBGLct.SelectedRows.Items[i]));
        QuitaItemLanctoAutomatico(VAR_GETDATA, QrLct, QrCrt);
        Controle := QrLct.FieldByName('Controle').AsInteger;
      end;
    end;
  end else QuitaItemLanctoAutomatico(VAR_GETDATA, QrLct, QrCrt);
  UFinanceiro.RecalcSaldoCarteira(QrLct.FieldByName('Carteira').AsInteger,
    QrCrt, True, True);
  UFinanceiro.RecalcSaldoCarteira(QrLct.FieldByName('Banco').AsInteger,
    nil, False, False);
  QrLct.Locate('Controle', Controle, []);
end;

procedure TUnFinanceiro.QuitaItemLanctoAutomatico(Data: TDateTime;
QrLct, QrCrt: TmySQLQuery);
var
  Controle2: Integer;
  Txt: String;
begin
  if QrLct.FieldByName('Sit').AsInteger > 0 then
  begin
    case QrLct.FieldByName('Sit').AsInteger of
      1: Txt := 'j� tem pagamentos parciais atrelados a ele';
      2: Txt := 'j� est� quitado';
      3: Txt := 'j� est� compensado';
      else Txt := 'j� possui algum tipo de atrelamento desconhecido';
    end;
    Application.MessageBox(PChar('O lan�amento n� ' + IntToStr(
    QrLct.FieldByName('Controle').AsInteger) + ' n�o ser� compensada pois ' +
    Txt + '!'), 'Aviso', MB_OK + MB_ICONWARNING);
    Exit;
  end;
  Controle2 := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    VAR_LCT, VAR_LCT, 'Controle');
  //
  if QrLct.FieldByName('Sit').AsInteger > 0 then
  begin
    Application.MessageBox(PChar('O lan�amento n� ' +
    IntToStr(QrLct.FieldByName('Controle').AsInteger) +
    ' j� possui pagamento parcial ou total, portanto n�o ser� quitado!'),
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  {
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET Sit=3, Compensado=:P0,');
  Dmod.QrUpdM.SQL.Add('ID_Quit=:P1, DataAlt=:Pa, UserAlt=:Pb');
  Dmod.QrUpdM.SQL.Add('WHERE Controle=:Pc AND Sub=:Pd AND Tipo=2');
  Dmod.QrUpdM.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, Data);
  Dmod.QrUpdM.Params[01].AsFloat   := Controle2;
  //
  Dmod.QrUpdM.Params[02].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdM.Params[03].AsInteger := VAR_USUARIO;
  Dmod.QrUpdM.Params[04].AsFloat   := QrLct.FieldByName('Controle').AsInteger;
  Dmod.QrUpdM.Params[05].AsInteger := QrLct.FieldByName('Sub').AsInteger;
  Dmod.QrUpdM.ExecSQL;
  }
  SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
    'Sit', 'Compensado', 'ID_Quit'], ['Controle', 'Sub', 'Tipo'], [3,
    FormatDateTime(VAR_FORMATDATE, Date), Controle2], [
    QrLct.FieldByName('Controle').AsInteger, QrLct.FieldByName('Sub').AsInteger,
    2], True, '');
  //
  UFinanceiro.LancamentoDefaultVARS;
  FLAN_Data       := Geral.FDT(VAR_GETDATA, 1);
  FLAN_Vencimento := Geral.FDT(QrLct.FieldByName('Vencimento').AsDateTime, 1);
  FLAN_DataCad    := Geral.FDT(Date, 1);
  FLAN_Mez        := MLAGeral.ITS_Null(QrLct.FieldByName('Mez').AsInteger);
  FLAN_Descricao  := QrLct.FieldByName('Descricao').AsString;
  FLAN_Compensado := Geral.FDT(VAR_GETDATA, 1);
  FLAN_Duplicata  := QrLct.FieldByName('Duplicata').AsString;
  FLAN_Doc2       := QrLct.FieldByName('Doc2').AsString;
  FLAN_SerieCH    := QrLct.FieldByName('SerieCH').AsString;

  FLAN_Documento  := Trunc(QrLct.FieldByName('Documento').AsInteger);
  FLAN_Tipo       := 1;
  FLAN_Carteira   := QrCrt.FieldByName('Banco').AsInteger;
  FLAN_Credito    := QrLct.FieldByName('Credito').AsFloat;
  FLAN_Debito     := QrLct.FieldByName('Debito').AsFloat;
  FLAN_Genero     := QrLct.FieldByName('Genero').AsInteger;
  FLAN_SerieNF    := QrLct.FieldByName('SerieNF').AsString;
  FLAN_NotaFiscal := QrLct.FieldByName('NotaFiscal').AsInteger;
  FLAN_Sit        := 3;
  FLAN_Cartao     := 0;
  FLAN_Linha      := 0;
  FLAN_Fornecedor := QrLct.FieldByName('Fornecedor').AsInteger;
  FLAN_Cliente    := QrLct.FieldByName('Cliente').AsInteger;
  FLAN_MoraDia    := QrLct.FieldByName('MoraDia').AsFloat;
  FLAN_Multa      := QrLct.FieldByName('Multa').AsFloat;
  //FLAN_UserCad    := VAR_USUARIO;
  //FLAN_DataDoc    := Geral.FDT(Date, 1);
  FLAN_Vendedor   := QrLct.FieldByName('Vendedor').AsInteger;
  FLAN_Account    := QrLct.FieldByName('Account').AsInteger;
  FLAN_ICMS_P     := QrLct.FieldByName('ICMS_P').AsFloat;
  FLAN_ICMS_V     := QrLct.FieldByName('ICMS_V').AsFloat;
  FLAN_CliInt     := QrLct.FieldByName('CliInt').AsInteger;
  FLAN_Depto      := QrLct.FieldByName('Depto').AsInteger;
  FLAN_DescoPor   := QrLct.FieldByName('DescoPor').AsInteger;
  FLAN_ForneceI   := QrLct.FieldByName('ForneceI').AsInteger;
  FLAN_DescoVal   := QrLct.FieldByName('DescoVal').AsFloat;
  FLAN_NFVal      := QrLct.FieldByName('NFVal').AsFloat;
  FLAN_Unidade    := QrLct.FieldByName('Unidade').AsInteger;
  FLAN_Qtde       := QrLct.FieldByName('Qtde').AsFloat;
  FLAN_FatID      := QrLct.FieldByName('FatID').AsInteger;
  FLAN_FatID_Sub  := QrLct.FieldByName('FatID_Sub').AsInteger;
  FLAN_FatNum     := QrLct.FieldByName('FatNum').AsInteger;
  FLAN_FatParcela := QrLct.FieldByName('FatParcela').AsInteger;
  FLAN_MultaVal   := QrLct.FieldByName('MultaVal').AsFloat;
  FLAN_MoraVal    := QrLct.FieldByName('MoraVal').AsFloat;
  FLAN_ID_Pgto    := QrLct.FieldByName('Controle').AsInteger;
  FLAN_Controle   := Round(Controle2);
  //
  InsereLancamento();
end;

function TUnFinanceiro.LocalizarlanctoCliInt(Controle, Sub, CliInt: Integer;
  TPDataIni: TDateTimePicker; QrLctos, QrCarts, QrCartSum: TmySQLQuery;
  AvisaQuandoNaoEncontra: Boolean): Boolean;
var
  Lancto: Double;
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  DmodFin.QrLocLcto.Close;
  DmodFin.QrLocLcto.Params[00].AsInteger := Controle;
  DmodFin.QrLocLcto.Params[01].AsInteger := Sub;
  DmodFin.QrLocLcto.Open;
  if DmodFin.QrLocLctoCliInt.Value <> CliInt then
  begin
    if AvisaQuandoNaoEncontra then
    Application.MessageBox(
    'O lan�amento solicitado n�o pertence ao cliente interno atual!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Result := False;
    Exit;
  end;
  if TPDataIni <> nil then
  begin
    if DmodFin.QrLocLctoData.Value < TPDataIni.Date then
    TPDataIni.Date := DmodFin.QrLocLctoData.Value;
  end;
  //
  VAR_LANCTO2 := DmodFin.QrLocLctoControle.Value;
  Lancto      := DmodFin.QrLocLctoControle.Value;
  ReabreCarteirasCliInt(CliInt, DmodFin.QrLocLctoCarteira.Value, QrCarts,
  QrCartSum);
  if QrLctos.State = dsInactive then
    Result := False
  else
    Result :=  QrLctos.Locate('Controle', Lancto, []);
  if not Result then
  if AvisaQuandoNaoEncontra then
    Application.MessageBox(PChar('N�o foi poss�vel localizar o lan�amento n� ' +
    IntToStr(Controle) + '-' + IntToStr(Sub) + '!'), 'Aviso',
    MB_OK+MB_ICONWARNING);
  Screen.Cursor := MyCursor;
end;

procedure TUnFinanceiro.LocalizarLancamento(DTPicker: TDateTimePicker;
QrCrt, QrLct: TmySQLQuery; LocSohCliInt: Boolean;
QuemChamou: Integer);
var
  CliInt: Integer;
begin
  CliInt := 0;
  //if UMyMod.AcessoNegadoAoForm('Perfis', 'EditACxas', 0) then Exit;
  VAR_Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  try
    if MyObjects.CriaForm_AcessoTotal(TFmLocDefs, FmLocDefs) then
    begin
      FmLocDefs.FQuemChamou := QuemChamou;
      case VAR_LIB_ARRAY_EMPRESAS_CONTA of
        0:
        begin
          Application.MessageBox('Cliente(s) interno(s) n�o definido(s)!',
            'Aviso', MB_OK+MB_ICONWARNING);
          Exit;
        end;
        1:
        begin
          CliInt := VAR_LIB_ARRAY_EMPRESAS_LISTA[0];
          if CliInt = 0 then
          begin
            Application.MessageBox(PChar('Cliente interno inv�lido: ' +
            IntToStr(CliInt)), 'Aviso', MB_OK+MB_ICONWARNING);
            Exit;
          end else begin
            FmLocDefs.ReopenCliInt();
            FmLocDefs.EdCliInt.Text     := IntToStr(CliInt);
            FmLocDefs.CBCliInt.KeyValue := CliInt;
            FmLocDefs.CkCliInt.Checked  := True;
          end;
        // 2 ou mais
        end else FmLocDefs.ReopenCliInt();
      end;
      FmLocDefs.FQrLct        := QrLct;
      FmLocDefs.FQRCARTEIRAS  := QrCrt;
      FmLocDefs.FDTPDataIni   := DTPicker;
      FmLocDefs.F_CliInt      := CliInt;
      FmLocDefs.FLocSohCliInt := LocSohCliInt;
      FmLocDefs.FQuemChamou   := QuemChamou;
      //
      FmLocDefs.ShowModal;
      FmLocDefs.Destroy;
    end;
  finally
    Screen.Cursor := VAR_Cursor;
  end;
end;

procedure TUnFinanceiro.ImprimePlanoContas();
begin
  if DBCheck.CriaFm(TFmContasSdoImp, FmContasSdoImp, afmoNegarComAviso) then
  begin
    FmContasSdoImp.ShowModal;
    FmContasSdoImp.Destroy;
  end;
end;

procedure TUnFinanceiro.CriaFmQuitaDocs(Data: TDateTime; Documento, Controle,
 CtrlIni: Double; Sub, Carteira: Integer);
begin
  if DBCheck.CriaFm(TFmContasSdoImp, FmContasSdoImp, afmoNegarComAviso) then
  begin
    FmContasSdoImp.ShowModal;
    FmContasSdoImp.Destroy;
  end;
end;

{
procedure TUnFinanceiro.CriarTransfer(Tipo: Integer; QrLct, QrCrt:
TmySQLQuery; MostraForm: Boolean);
var
  Localiza: Double;
  Lock: Int64;
begin
  lock := QrLct.FieldByName('Controle').AsInteger;
  if not Tipo in [0,1,2,3] then
  begin
    ShowMessage('Tipo de transfer�ncia n�o definido.');
    Exit;
  end;
  //if UMyMod.AcessoNegadoAoForm('Perfis', 'MovContas', 0) and
  UMyMod.AcessoNegadoAoForm('Perfis', 'Transfer�ncia de dinheiro entre carteiras', 0) then Exit;
  if (QrLct.FieldByName('Genero').AsInteger = -1) and (Tipo in [1,2])
  and (TransferenciaCorrompida(QrLct.FieldByName('Controle').AsInteger,
    QrCrt)) then
  begin
    UMyMod.UpdUnlockInt64Y(lock, Dmod.MyDB, VAR_LCT, 'Controle');
    Exit;
  end;
  if DBCheck.CriaFm(TFmTransfer, FmTransfer, afmNegarComAviso) then
  begin
    with FmTransfer do
    begin
      if Tipo = 0 then
      begin
        LaTipo.Caption := DmkEnums.NomeTipoSQL(stIns);
        TPData.Date := MLAGeral.ObtemDataInserir;

      end else LaTipo.Caption := CO_ALTERACAO;
      if Tipo in [0,1,3] then BtConfirma.Enabled := True;
      if Tipo in [1,2,3] then BtExclui.Enabled := True;
      if Tipo in [1,2] then
      begin
        UMyMod.UpdLockInt64Y(lock, Dmod.MyDB, VAR_LCT, 'Controle');
        if QrLct.FieldByName('Genero').AsInteger <> -1 then
        begin
          ShowMessage('Este lan�amento n�o � uma transfer�ncia.');
          UMyMod.UpdUnlockInt64Y(lock, Dmod.MyDB, VAR_LCT, 'Controle');
          Exit;
        end else ConfiguraFmTransfer;
      end;
      if Tipo = 3 then
      begin
        Localiza := CuringaLoc.CriaForm2('Controle', 'Descricao', VAR_LCT,
        VAR_GOTOMySQLDBNAME, 'AND Genero=-1');
        if TransferenciaCorrompida(Localiza, QrCrt) then
        begin
          FmTransfer.Destroy;
          UMyMod.UpdUnlockInt64Y(lock, Dmod.MyDB, VAR_LCT, 'Controle');
          Exit;
        end;
        ConfiguraFmTransfer;
      end;
    end;
    if MostraForm then
    begin
      FmTransfer.ShowModal;
      FmTransfer.Destroy;
    end;
  end;
  UMyMod.UpdUnLockInt64Y(Lock, Dmod.MyDB, VAR_LCT, 'Controle');
end;
}

procedure TUnFinanceiro.CriarTransferCart(Tipo: Integer; QrLct, QrCrt:
TmySQLQuery; MostraForm: Boolean; CliInt, FatID, FatID_Sub, FatNum: Integer);
var
  Localiza: Double;
  Lock: Int64;
begin
  // Tipo
  // 0 - Inclui
  // 1 - Altera
  // 2 - Exclui
  // 3 - Localiza?
  //
  if QrLct = nil then lock := 0 else
    lock := QrLct.FieldByName('Controle').AsInteger;
  if not Tipo in [0,1,2,3] then
  begin
    ShowMessage('Tipo de transfer�ncia n�o definido.');
    Exit;
  end;
  if QrLct <> nil then
  begin
    if (QrLct.FieldByName('Genero').AsInteger = -1) and (Tipo in [1,2])
    and (TransferenciaCorrompidaCart(QrLct.FieldByName('Controle').AsInteger,
      QrCrt)) then
    begin
      UMyMod.UpdUnlockInt64Y(lock, Dmod.MyDB, VAR_LCT, 'Controle');
      Exit;
    end;
  end;
  if DBCheck.CriaFm(TFmTransfer2, FmTransfer2, afmoNegarComAviso) then
  begin
    with FmTransfer2 do
    begin
      F_CliInt   := CliInt;
      FFatID     := FatID;
      FFatNum    := FatNum;
      FFatId_Sub := FatID_Sub;
      QrOrigem.Close;
      QrOrigem.SQL.Clear;
      QrOrigem.SQL.Add('SELECT Codigo, Nome, Tipo');
      QrOrigem.SQL.Add('FROM carteiras');
      if CliInt <> 0 then
        QrOrigem.SQL.Add('WHERE ForneceI=' + FormatFloat('0', CliInt));
      QrOrigem.SQL.Add('ORDER BY Nome');
      QrDestino.SQL := QrOrigem.SQL;
      QrOrigem.Open;
      QrDestino.Open;
      if Tipo = 0 then
      begin
        LaTipo.Caption := DmkEnums.NomeTipoSQL(stIns);
        TPData.Date := MLAGeral.ObtemDataInserir;

      end else LaTipo.Caption := CO_ALTERACAO;
      //
      if Tipo in [0,1,3] then BtConfirma.Enabled := True;
      if Tipo in [1,2,3] then BtExclui.Enabled := True;
      if Tipo in [1,2] then
      begin
        if DmodFin.QrTransfData.Value < VAR_DATA_MINIMA then
        begin
          FmTransfer2.Destroy;
          Geral.MensagemBox(
          'A��o cancelada. Esta transfer�ncia pertence a um m�s encerrado!',
          'Aviso', MB_OK+MB_ICONWARNING);
          Exit;
        end;
        UMyMod.UpdLockInt64Y(lock, Dmod.MyDB, VAR_LCT, 'Controle');
        if QrLct <> nil then
        begin
          if QrLct.FieldByName('Genero').AsInteger <> -1 then
          begin
            FmTransfer2.Destroy;
            Geral.MensagemBox('Este lan�amento n�o � uma transfer�ncia.',
            'Aviso', MB_OK+MB_ICONWARNING);
            UMyMod.UpdUnlockInt64Y(lock, Dmod.MyDB, VAR_LCT, 'Controle');
            Exit;
          end else ConfiguraFmTransferCart;
        end else ConfiguraFmTransferCart;
      end;
      if Tipo = 3 then
      begin
        Localiza := CuringaLoc.CriaForm2('Controle', 'Descricao', VAR_LCT,
        VAR_GOTOMySQLDBNAME, 'AND Genero=-1');
        if TransferenciaCorrompidaCart(Localiza, QrCrt) then
        begin
          FmTransfer2.Destroy;
          UMyMod.UpdUnlockInt64Y(lock, Dmod.MyDB, VAR_LCT, 'Controle');
          Exit;
        end;
        ConfiguraFmTransferCart;
      end;
    end;
    if MostraForm then
    begin
      FmTransfer2.ShowModal;
      FmTransfer2.Destroy;
    end;
  end;
  UMyMod.UpdUnLockInt64Y(Lock, Dmod.MyDB, VAR_LCT, 'Controle');
end;

procedure TUnFinanceiro.CriarTransferCtas(Tipo: Integer; QrLct, QrCrt:
TmySQLQuery; MostraForm: Boolean; CliInt, FatID, FatID_Sub, FatNum: Integer);
var
  Localiza: Double;
  Lock: Int64;
begin
  // Tipo
  // 0 - Inclui
  // 1 - Altera
  // 2 - Exclui
  // 3 - Localiza?
  //
  if QrLct = nil then lock := 0 else
    lock := QrLct.FieldByName('Controle').AsInteger;
  if not Tipo in [0,1,2,3] then
  begin
    Application.MessageBox('Tipo de transfer�ncia entre contas n�o definido.',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if QrLct <> nil then
  begin
    if (QrLct.FieldByName('FatID').AsInteger = -1) //  and (Tipo in [1,2]) (?? 2008.12.19)
    and (TransferenciaCorrompidaCtas(QrLct.FieldByName('Controle').AsInteger,
      QrCrt)) then
    begin
      UMyMod.UpdUnlockInt64Y(lock, Dmod.MyDB, VAR_LCT, 'Controle');
      Exit;
    end;
  end;
  if DBCheck.CriaFm(TFmTransferCtas, FmTransferCtas, afmoNegarComAviso) then
  begin
    with FmTransferCtas do
    begin
      F_CliInt    := CliInt;
      QrOrigem.Close;
      QrOrigem.SQL.Clear;
      QrOrigem.SQL.Add('SELECT Codigo, Nome, Tipo');
      QrOrigem.SQL.Add('FROM carteiras');
      if CliInt <> 0 then
        QrOrigem.SQL.Add('WHERE ForneceI=' + FormatFloat('0', CliInt));
      QrOrigem.SQL.Add('ORDER BY Nome');
      QrDestino.SQL := QrOrigem.SQL;
      QrOrigem.Open;
      QrDestino.Open;
      if Tipo = 0 then
      begin
        LaTipo.Caption := DmkEnums.NomeTipoSQL(stIns);
        TPData.Date := MLAGeral.ObtemDataInserir;

      end else LaTipo.Caption := CO_ALTERACAO;
      if Tipo in [0,1,3] then BtConfirma.Enabled := True;
      if Tipo in [1,2,3] then BtExclui.Enabled := True;
      if Tipo in [1,2] then
      begin
        UMyMod.UpdLockInt64Y(lock, Dmod.MyDB, VAR_LCT, 'Controle');
        if QrLct <> nil then
        begin
          if QrLct.FieldByName('FatID').AsInteger <> -1 then
          begin
            Application.MessageBox(
              'Este lan�amento n�o � uma transfer�ncia entre contas!',
              'Aviso', MB_OK+MB_ICONWARNING);
            UMyMod.UpdUnlockInt64Y(lock, Dmod.MyDB, VAR_LCT, 'Controle');
            Exit;
          end else ConfiguraFmTransferCtas();
        end else ConfiguraFmTransferCtas();
      end;
      if Tipo = 3 then
      begin
        Localiza := CuringaLoc.CriaForm2('Controle', 'Descricao', VAR_LCT,
        VAR_GOTOMySQLDBNAME, 'AND FatID=-1');
        if TransferenciaCorrompidaCtas(Localiza, QrCrt) then
        begin
          FmTransferCtas.Destroy;
          UMyMod.UpdUnlockInt64Y(lock, Dmod.MyDB, VAR_LCT, 'Controle');
          Exit;
        end;
        ConfiguraFmTransferCtas();
      end;
    end;
    if MostraForm then
    begin
      FmTransferCtas.ShowModal;
      FmTransferCtas.Destroy;
    end;
  end;
  UMyMod.UpdUnLockInt64Y(Lock, Dmod.MyDB, VAR_LCT, 'Controle');
end;

function TUnFinanceiro.CRT_Get(Codigo: Integer): String;
begin
// C�digo do Regime Tribut�rio
  case Codigo of
    1: Result := 'Simples Nacional.';
    2: Result := 'Simples Nacional - excesso de sublimite de receita bruta';
    3: Result := 'Regime Normal.';
    else Result := '';
  end;
end;

function TUnFinanceiro.CRT_Lista: MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const Codigo, Descricao: String);
  begin
    SetLength(Res, Linha + 1);
    SetLength(Res[Linha], 3);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
//TABELA CRT
  AddLinha(Result, Linha, '1', 'Simples Nacional.');
  AddLinha(Result, Linha, '2', 'Simples Nacional - excesso de sublimite de receita bruta.');
  AddLinha(Result, Linha, '3', 'Regime Normal.');
end;

function TUnFinanceiro.TransferenciaCorrompidaCart(Lancto: Double; QrCrt:
TmySQLQuery): Boolean;
var
  Texto: PChar;
  Localiza: Boolean;
begin
  Result := False;
  DmodFin.QrTransf.Close;
  DmodFin.QrTransf.Params[0].AsFloat := Lancto;
  DmodFin.QrTransf.Open;
  DmodFin.QrTransf.Last;
  DmodFin.QrTransf.First;
  if DmodFin.QrTransf.RecordCount <> 2 then
  begin
    Result := True;
    if DmodFin.QrTransf.RecordCount <> 0 then
    begin
      Texto := PChar('Existem '+IntToStr(DmodFin.QrTransf.RecordCount)+
      'registros na transfer�ncia, quando deveria haver 2. Deseja exclu�los?');
      if Application.MessageBox(Texto, PChar(VAR_APPNAME), MB_ICONQUESTION+
      MB_YESNO+MB_DEFBUTTON1+MB_APPLMODAL) = ID_YES then
      begin
        while not DmodFin.QrTransf.Eof do
        begin
          ExcluiLct_Unico(True, DmodFin.QrTransf.Database,
            DmodFin.QrTransfData.Value, DmodFin.QrTransfTipo.Value,
            DmodFin.QrTransfCarteira.Value, DmodFin.QrTransfControle.Value,
            DmodFin.QrTransfSub.Value, False);
          if (DmodFin.QrTransfTipo.Value = QrCrt.FieldByName('Tipo').AsInteger)
          and (DmodFin.QrTransfCarteira.Value = QrCrt.FieldByName('Codigo').AsInteger)
          then Localiza := True else Localiza := False;
          UFinanceiro.RecalcSaldoCarteira(DmodFin.QrTransfCarteira.Value,
            QrCrt, Localiza, Localiza);
          DmodFin.QrTransf.Next;
        end;
      end;
    end;
    DmodFin.QrTransf.Close;
  end;
end;

function TUnFinanceiro.TransferenciaCorrompidaCtas(Lancto: Double; QrCrt:
TmySQLQuery): Boolean;
var
  Texto: PChar;
  Localiza: Boolean;
begin
  Result := False;
  DmodFin.QrTrfCtas.Close;
  DmodFin.QrTrfCtas.Params[0].AsFloat := Lancto;
  DmodFin.QrTrfCtas.Open;
  DmodFin.QrTrfCtas.Last;
  DmodFin.QrTrfCtas.First;
  if DmodFin.QrTrfCtas.RecordCount <> 2 then
  begin
    Result := True;
    if DmodFin.QrTrfCtas.RecordCount <> 0 then
    begin
      Texto := PChar('Existem '+IntToStr(DmodFin.QrTrfCtas.RecordCount)+
      'registros na transfer�ncia entre contas, quando deveria haver 2. Deseja exclu�los?');
      if Application.MessageBox(Texto, PChar(VAR_APPNAME), MB_ICONQUESTION+
      MB_YESNO+MB_DEFBUTTON1+MB_APPLMODAL) = ID_YES then
      begin
        while not DmodFin.QrTrfCtas.Eof do
        begin
          ExcluiLct_Unico(True, DmodFin.QrTrfCtas.Database,
            DmodFin.QrTrfCtasData.Value, DmodFin.QrTrfCtasTipo.Value,
            DmodFin.QrTrfCtasCarteira.Value, DmodFin.QrTrfCtasControle.Value,
            DmodFin.QrTrfCtasSub.Value, False);
          //  
          if (DmodFin.QrTrfCtasTipo.Value = QrCrt.FieldByName('Tipo').AsInteger)
          and (DmodFin.QrTrfCtasCarteira.Value = QrCrt.FieldByName('Codigo').AsInteger)
          then Localiza := True else Localiza := False;
          UFinanceiro.RecalcSaldoCarteira(DmodFin.QrTrfCtasCarteira.Value,
            QrCrt, Localiza, Localiza);
          DmodFin.QrTrfCtas.Next;
        end;
      end;
    end;
    DmodFin.QrTrfCtas.Close;
  end;
end;

procedure TUnFinanceiro.VerificaID_Pgto_x_Compensado(LaAviso: TLabel; PB1:
TProgressBar; QrNC: TmySQLQuery);
var
  Qry1, Qry2, Qry3(*, Qry4*): TmySQLQuery;
  Data: TDateTime;
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
{
  Qry1 := nil;
  Qry2 := nil;
  Qry3 := nil;
}
  //
  MyObjects. Informa(LaAviso, True, 'Verificando compensados');
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lanctos SET Compensado=Data');
  Dmod.QrUpd.SQL.Add('WHERE Tipo<2 AND Compensado < 2');
  Dmod.QrUpd.ExecSQL;
  //
  Qry1 := TmySQLQuery.Create(Dmod);
  Qry1.Close;
  Qry1.Database := Dmod.MyDB;
  Qry1.SQL.Clear;
  Qry1.SQL.Add('SELECT Data, Vencimento, Controle, CtrlQuitPg');
  Qry1.SQL.Add('FROM lanctos');
  Qry1.SQL.Add('WHERE Sit in (2,3)'); // 4 pode!
  Qry1.SQL.Add('AND Compensado < 2');
  Qry1.SQL.Add('AND Tipo=2');
  Qry1.SQL.Add('ORDER BY DATA');
  Qry1.Open;
  if PB1 <> nil then
  begin
    PB1.Max := Qry1.RecordCount;
    PB1.Position := 0;
  end;
  //
  if Qry1.RecordCount > 0 then
  begin
    //
    Qry2 := TmySQLQuery.Create(Dmod);
    Qry2.Close;
    Qry2.Database := Dmod.MyDB;
    Qry2.SQL.Clear;
    Qry2.SQL.Add('SELECT Data');
    Qry2.SQL.Add('FROM lanctos WHERE');
    Qry2.SQL.Add('Controle=:P0');
    //
    Qry3 := TmySQLQuery.Create(Dmod);
    Qry3.Close;
    Qry3.Database := Dmod.MyDB;
    Qry3.SQL.Clear;
    Qry3.SQL.Add('SELECT Data');
    Qry3.SQL.Add('FROM lanctos WHERE');
    Qry3.SQL.Add('ID_Pgto=:P0');
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lanctos SET Compensado=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Qry1.First;
    while not Qry1.Eof do
    begin
      if Qry1.FieldByName('CtrlQuitPg').AsInteger > 0 then
      begin
        Qry2.Close;
        Qry2.Params[00].AsInteger := Qry1.FieldByName('CtrlQuitPg').AsInteger;
        Qry2.Open;
        Data := Qry2.FieldByName('Data').AsDateTime;
        if Data > 1 then
        begin
          if PB1 <> nil then
          begin
            PB1.Position := PB1.Position + 1;
            PB1.Update;
            Application.ProcessMessages;
          end;
          Dmod.QrUpd.Params[00].AsString  := Geral.FDT(Data, 1);
          Dmod.QrUpd.Params[01].AsInteger := Qry1.FieldByName('Controle').AsInteger;
          Dmod.QrUpd.ExecSQL;
        end;
      end else
      begin
        Qry3.Close;
        Qry3.Params[00].AsInteger := Qry1.FieldByName('Controle').AsInteger;
        Qry3.Open;
        Data := Qry3.FieldByName('Data').AsDateTime;
        if Data > 1 then
        begin
          if PB1 <> nil then
          begin
            PB1.Position := PB1.Position + 1;
            PB1.Update;
            Application.ProcessMessages;
          end;
          Dmod.QrUpd.Params[00].AsString  := Geral.FDT(Data, 1);
          Dmod.QrUpd.Params[01].AsInteger := Qry1.FieldByName('Controle').AsInteger;
          Dmod.QrUpd.ExecSQL;
        end else begin
          // Atualizar quita��o pelo vencimento?
        end;
      end;
      //
      Qry1.Next;
    end;
    if Qry2 <> nil then
      Qry2.Free;
    if Qry3 <> nil then
      Qry3.Free;
  end;
  Qry1.Close;
  Qry1.Open;
  if Qry1.RecordCount > 0 then
  begin
    MyObjects. Informa(LaAviso, False,
      IntToStr(Qry1.RecordCount) + ' lan�amentos n�o foram corrigidos!');
    if QrNC <> nil then
    begin
      QrNC.Close;
      QrNC.Open;
    end;
  end
  else
    MyObjects. Informa(LaAviso, False, '...');
  //
  if Qry1 <> nil then
    Qry1.Free;
  //  
  Screen.Cursor := MyCursor;
end;

{
procedure TUnFinanceiro.ConfiguraFmTransfer;
var
  Debito, Credito: Double;
begin
  with FmTransfer do
  begin
    EdCodigo.Text := FloatToStr(DmodFin.QrTransfControle.Value);
    TPData.Date := DmodFin.QrTransfData.Value;
    Debito := 0;
    Credito := 0;
    while not DmodFin.QrTransf.Eof do
    begin
      if (DmodFin.QrTransfDebito.Value > 0) then begin
        if (Debito > 0) then
          Application.MessageBox(PChar('Erro. Mais de um d�bito'+Chr(13)+Chr(10)+
          'D�bito [1]: '+Geral.TFT(FloatToStr(Debito), 2, siNegativo)+Chr(13)+Chr(10)+
          'D�bito [2]: '+Geral.TFT(FloatToStr(DmodFin.QrTransfDebito.Value), 2, siNegativo)),
          'Erro', MB_OK+MB_ICONERROR);
        Debito := Debito + DmodFin.QrTransfDebito.Value;
        EdDocDeb.Text := FloatToStr(DmodFin.QrTransfDocumento.Value);
      end;
      if (DmodFin.QrTransfCredito.Value > 0) then begin
        if (Credito > 0) then
          Application.MessageBox(PChar('Erro. Mais de um d�bito'+Chr(13)+Chr(10)+
          'Cr�dito [1]: '+Geral.TFT(FloatToStr(Credito), 2, siNegativo)+Chr(13)+Chr(10)+
          'Cr�dito [2]: '+Geral.TFT(FloatToStr(DmodFin.QrTransfCredito.Value), 2, siNegativo)),
          'Erro', MB_OK+MB_ICONERROR);
        Credito := Credito + DmodFin.QrTransfCredito.Value;
        EdDocCred.Text := FloatToStr(DmodFin.QrTransfDocumento.Value);
      end;
      EdValor.Text := Geral.TFT(FloatToStr(
      DmodFin.QrTransfDebito.Value+DmodFin.QrTransfCredito.Value), 2, siPositivo);
      if DmodFin.QrTransfDebito.Value > 0 then
      begin
        RGOrigem.ItemIndex := DmodFin.QrTransfTipo.Value;
        CBOrigem.KeyValue := DmodFin.QrTransfCarteira.Value;
      end else begin
        RGDestino.ItemIndex := DmodFin.QrTransfTipo.Value;
        CBDestino.KeyValue := DmodFin.QrTransfCarteira.Value;
      end;
      DmodFin.QrTransf.Next;
    end;
    if Credito <> Debito then
      ShowMessage('Erro. Cr�dito diferente do d�bito'+Chr(13)+Chr(10)+
      'Cr�dito: '+Geral.TFT(FloatToStr(Credito), 2, siNegativo)+Chr(13)+Chr(10)+
      'D�bito : '+Geral.TFT(FloatToStr(Debito), 2, siNegativo));
  end;
end;
}
procedure TUnFinanceiro.CalculaPercMultaEJuros(ValOrig, MultaVal, JuroVal: Double;
  DataNew, DataVencto: TDateTime; var ValorNew, MultaPerc, JuroPerc: Double);
var
  Dias, MultPer, JuroPer: Double;
  DataUtil: TDateTime;
begin
  DataUtil := UMyMod.CalculaDataDeposito(DataVencto);
  Dias     := Int(DataNew) - Int(DataUtil);
  //
  if (Dias >= 1) and (ValOrig > 0) then
  begin
    MultPer := (100 * MultaVal) / ValOrig;
    JuroPer := (((JuroVal * 100) / ValOrig) / Dias) * 30;
  end else
  begin
    MultPer := 0;
    JuroPer := 0;
  end;
  //
  ValorNew  := ValOrig + MultaVal + JuroVal;
  MultaPerc := MultPer;
  JuroPerc  := JuroPer;
end;

procedure TUnFinanceiro.CalculaValMultaEJuros(ValOrig: Double; DataNew,
  DataVencto: TDateTime; var MultaPerc, JuroPerc, ValorNew, MultaVal,
  JuroVal: Double; const PermitePercZero: Boolean);
  procedure ObtemMultaEJuros(const MultaPer, JuroPer: Double; var MultaP,
    JuroP: Double);
  begin
    if MultaPer = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAux, Dmod.MyDB, [
      'SELECT Multa FROM controle ',
      '']);
      if DModG.QrAux.RecordCount > 0 then
        MultaP := DModG.QrAux.FieldByName('Multa').AsFloat
      else
        MultaP := 2;
    end else
      MultaP := MultaPer;
    if JuroPer = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAux, Dmod.MyDB, [
      'SELECT MoraDD FROM controle ',
      '']);
      if DModG.QrAux.RecordCount > 0 then
        JuroP := DModG.QrAux.FieldByName('MoraDD').AsFloat
      else
        JuroP := 1;
    end else
      JuroP := JuroPer;
  end;
var
  Dias, Multa, TaxJu, Juros: Double;
  DataUtil: TDateTime;
begin
  DataUtil := UMyMod.CalculaDataDeposito(DataVencto);
  Dias     := Int(DataNew) - Int(DataUtil);
  //
  if (Dias >= 1) and (ValOrig > 0) then
  begin
    if PermitePercZero then
    begin
      Multa := MultaPerc;
      TaxJu := JuroPerc;
    end else
      ObtemMultaEJuros(MultaPerc, JuroPerc, Multa, TaxJu);
    //
    Juros := TaxJu / 30 * Dias;
    //
    MultaVal  := Multa * ValOrig / 100;
    JuroVal   := Juros * ValOrig / 100;
    ValorNew  := ValOrig + MultaVal + JuroVal;
    MultaPerc := Multa;
    JuroPerc  := TaxJu; 
  end else
  begin
    ValorNew := ValOrig;
    MultaVal := 0;
    JuroVal  := 0;
  end;
end;

procedure TUnFinanceiro.ConfiguraFmTransferCart();
var
  Debito, Credito: Double;
begin
  with FmTransfer2 do
  begin
    EdCodigo.Text := FloatToStr(DmodFin.QrTransfControle.Value);
    TPData.Date := DmodFin.QrTransfData.Value;
    Debito := 0;
    Credito := 0;
    while not DmodFin.QrTransf.Eof do
    begin
      if (DmodFin.QrTransfDebito.Value > 0) then begin
        if (Debito > 0) then
          Application.MessageBox(PChar('Erro. Mais de um d�bito'+Chr(13)+Chr(10)+
          'D�bito [1]: '+Geral.TFT(FloatToStr(Debito), 2, siNegativo)+Chr(13)+Chr(10)+
          'D�bito [2]: '+Geral.TFT(FloatToStr(DmodFin.QrTransfDebito.Value), 2, siNegativo)),
          'Erro', MB_OK+MB_ICONERROR);
        Debito := Debito + DmodFin.QrTransfDebito.Value;
        EdDocDeb.Text := FloatToStr(DmodFin.QrTransfDocumento.Value);
      end;
      if (DmodFin.QrTransfCredito.Value > 0) then begin
        if (Credito > 0) then
          Application.MessageBox(PChar('Erro. Mais de um d�bito'+Chr(13)+Chr(10)+
          'Cr�dito [1]: '+Geral.TFT(FloatToStr(Credito), 2, siNegativo)+Chr(13)+Chr(10)+
          'Cr�dito [2]: '+Geral.TFT(FloatToStr(DmodFin.QrTransfCredito.Value), 2, siNegativo)),
          'Erro', MB_OK+MB_ICONERROR);
        Credito := Credito + DmodFin.QrTransfCredito.Value;
        EdDocCred.Text := FloatToStr(DmodFin.QrTransfDocumento.Value);
      end;
      EdValor.Text := Geral.TFT(FloatToStr(
      DmodFin.QrTransfDebito.Value+DmodFin.QrTransfCredito.Value), 2, siPositivo);
      if DmodFin.QrTransfDebito.Value > 0 then
      begin
        EdOrigem.ValueVariant  := DmodFin.QrTransfCarteira.Value;
        CBOrigem.KeyValue      := DmodFin.QrTransfCarteira.Value;
        EdSerieChDeb.Text      := DmodFin.QrTransfSerieCH.Value;
        TPOldData1.Date             := DmodFin.QrTransfData.Value;
        EdOldTipo1.ValueVariant     := DmodFin.QrTransfTipo.Value;
        EdOldCarteira1.ValueVariant := DmodFin.QrTransfCarteira.Value;
        EdOldControle1.ValueVariant := DmodFin.QrTransfControle.Value;
        EdOldSub1.ValueVariant      := DmodFin.QrTransfSub.Value;
      end else begin
        EdDestino.ValueVariant := DmodFin.QrTransfCarteira.Value;
        CBDestino.KeyValue     := DmodFin.QrTransfCarteira.Value;
        EdSerieChCred.Text     := DmodFin.QrTransfSerieCH.Value;
        TPOldData2.Date             := DmodFin.QrTransfData.Value;
        EdOldTipo2.ValueVariant     := DmodFin.QrTransfTipo.Value;
        EdOldCarteira2.ValueVariant := DmodFin.QrTransfCarteira.Value;
        EdOldControle2.ValueVariant := DmodFin.QrTransfControle.Value;
        EdOldSub2.ValueVariant      := DmodFin.QrTransfSub.Value;
      end;
      DmodFin.QrTransf.Next;
    end;
    if Credito <> Debito then
      ShowMessage('Erro. Cr�dito diferente do d�bito'+Chr(13)+Chr(10)+
      'Cr�dito: '+Geral.TFT(FloatToStr(Credito), 2, siNegativo)+Chr(13)+Chr(10)+
      'D�bito : '+Geral.TFT(FloatToStr(Debito), 2, siNegativo));
  end;
end;

procedure TUnFinanceiro.ConfiguraFmTransferCtas();
var
  Debito, Credito: Double;
begin
  with FmTransferCtas do
  begin
    EdCodigo.Text := FloatToStr(DmodFin.QrTrfCtasControle.Value);
    TPData.Date := DmodFin.QrTrfCtasData.Value;
    Debito := 0;
    Credito := 0;
    while not DmodFin.QrTrfCtas.Eof do
    begin
      if (DmodFin.QrTrfCtasDebito.Value > 0) then begin
        if (Debito > 0) then
          Application.MessageBox(PChar('Erro. Mais de um d�bito'+Chr(13)+Chr(10)+
          'D�bito [1]: '+Geral.TFT(FloatToStr(Debito), 2, siNegativo)+Chr(13)+Chr(10)+
          'D�bito [2]: '+Geral.TFT(FloatToStr(DmodFin.QrTrfCtasDebito.Value), 2, siNegativo)),
          'Erro', MB_OK+MB_ICONERROR);
        Debito := Debito + DmodFin.QrTrfCtasDebito.Value;
        EdDocDeb.Text := FloatToStr(DmodFin.QrTrfCtasDocumento.Value);
      end;
      if (DmodFin.QrTrfCtasCredito.Value > 0) then begin
        if (Credito > 0) then
          Application.MessageBox(PChar('Erro. Mais de um d�bito'+Chr(13)+Chr(10)+
          'Cr�dito [1]: '+Geral.TFT(FloatToStr(Credito), 2, siNegativo)+Chr(13)+Chr(10)+
          'Cr�dito [2]: '+Geral.TFT(FloatToStr(DmodFin.QrTrfCtasCredito.Value), 2, siNegativo)),
          'Erro', MB_OK+MB_ICONERROR);
        Credito := Credito + DmodFin.QrTrfCtasCredito.Value;
        EdDocCred.Text := FloatToStr(DmodFin.QrTrfCtasDocumento.Value);
      end;
      EdValor.Text := Geral.TFT(FloatToStr(
      DmodFin.QrTrfCtasDebito.Value+DmodFin.QrTrfCtasCredito.Value), 2, siPositivo);
      if DmodFin.QrTrfCtasDebito.Value > 0 then
      begin
        EdOrigem.ValueVariant  := DmodFin.QrTrfCtasCarteira.Value;
        CBOrigem.KeyValue      := DmodFin.QrTrfCtasCarteira.Value;
        EdCtaDebt.ValueVariant := DmodFin.QrTrfCtasGenero.Value;
        CBCtaDebt.KeyValue     := DmodFin.QrTrfCtasGenero.Value;
        EdSerieChDeb.Text      := DmodFin.QrTrfCtasSerieCH.Value;
        TPOldData1.Date             := DmodFin.QrTrfCtasData.Value;
        EdOldTipo1.ValueVariant     := DmodFin.QrTrfCtasTipo.Value;
        EdOldCarteira1.ValueVariant := DmodFin.QrTrfCtasCarteira.Value;
        EdOldControle1.ValueVariant := DmodFin.QrTrfCtasControle.Value;
        EdOldSub1.ValueVariant      := DmodFin.QrTrfCtasSub.Value;
      end else begin
        EdDestino.ValueVariant := DmodFin.QrTrfCtasCarteira.Value;
        CBDestino.KeyValue     := DmodFin.QrTrfCtasCarteira.Value;
        EdCtaCred.ValueVariant := DmodFin.QrTrfCtasGenero.Value;
        CBCtaCred.KeyValue     := DmodFin.QrTrfCtasGenero.Value;
        EdSerieChCred.Text     := DmodFin.QrTrfCtasSerieCH.Value;
        TPOldData2.Date             := DmodFin.QrTrfCtasData.Value;
        EdOldTipo2.ValueVariant     := DmodFin.QrTrfCtasTipo.Value;
        EdOldCarteira2.ValueVariant := DmodFin.QrTrfCtasCarteira.Value;
        EdOldControle2.ValueVariant := DmodFin.QrTrfCtasControle.Value;
        EdOldSub2.ValueVariant      := DmodFin.QrTrfCtasSub.Value;
      end;
      DmodFin.QrTrfCtas.Next;
    end;
    if Credito <> Debito then
      ShowMessage('Erro. Cr�dito diferente do d�bito'+Chr(13)+Chr(10)+
      'Cr�dito: '+Geral.TFT(FloatToStr(Credito), 2, siNegativo)+Chr(13)+Chr(10)+
      'D�bito : '+Geral.TFT(FloatToStr(Debito), 2, siNegativo));
  end;
end;

function TUnFinanceiro.TextoFinalidadeLancto(Finalidade: TLanctoFinalidade): String;
  function Frase(Numero: Integer; Texto: String): String;
  begin
    Result := FormatFloat('000', Numero) +
      ' - Inclus�o / altera��o de lan�amentos ' + Texto;
  end;
begin
  case Finalidade of
    lfProprio:    Result := Frase(1, 'pr�prios');
    lfCondominio: Result := Frase(2, 'de condom�nios');
    lfCicloCurso: Result := Frase(3, 'de cursos c�clicos');
    else          Result := Frase(0, 'de finalidade desconhecida');
  end;
end;

function TUnFinanceiro.CriaLanctoEditor(InstanceClass: TComponentClass; var Reference;
ModoAcesso: TAcessFmModo; QrSelect: TmySQLQuery; Controle, Sub: Integer;
Finalidade: TLanctoFinalidade): Boolean;
  procedure AvisaFalta(Nome: String);
  begin
    Application.MessageBox(PChar('O componente ' + Nome + ' n�o foi setado!'),
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
var
  MyLabel: TLabel;
begin
  Result := False;
  if not DBCheck.CriaFm(InstanceClass, Reference, ModoAcesso) then Exit;
  with TForm(Reference) do
  begin
    if (FindComponent('LaFinalidade') as TLabel) <> nil then
      MyLabel := FindComponent('LaFinalidade') as TLabel
    else MyLabel := nil;
    if MyLabel <> nil then
      MyLabel.Caption := TextoFinalidadeLancto(Finalidade);
    //
    Result := True;
  end;
end;

{function TUnFinanceiro.InsAltReopenLct(f : TInsAltReopenLct): Boolean;
begin
  Result := f;
end;
}

function TUnFinanceiro.InclusaoLancamento(
  InstanceClass: TComponentClass; var Reference;
  Finalidade: TLanctoFinalidade; ModoAcesso: TAcessFmModo;
  QrLct, QrCrt: TmySQLQuery; Acao: TGerencimantoDeRegistro;
  Controle, Sub, Genero: Integer; PercJuroM, PercMulta: Double;
  SetaVars: TInsAltReopenLct; FatID, FatID_Sub, FatNum, Carteira: Integer;
  Credito, Debito: Double; AlteraAtehFatID: Boolean;
  Cliente, Fornecedor, CliInt, ForneceI, Account, Vendedor: Integer;
  LockCliInt, LockForneceI, LockAccount, LockVendedor: Boolean;
  Data, Vencto, Compensado, DataDoc: TDateTime; IDFinalidade: Integer; Mes: TDateTime): Integer;
var
  Txt_LaTipo, Txt_Ctrl, Campo: String;
  MyCursor: TCursor;
  i, Sit, Tipo: Integer;
  PropInfo: PPropInfo;
begin
  Result := 0;
  VLAN_QRCARTEIRAS := QrCrt;
  VLAN_QRLCT       := QrLct;
  //if VLAN_QRCARTEIRAS = nil then
    //Application.MessageBox('"VLAN_QRCARTEIRAS" n�o definido em "InsAltLancto"!',
    //'Aviso', MB_OK+MB_ICONWARNING);
  if Acao = tgrAltera then
  begin
    if (Data > 2) and (Data < VAR_DATA_MINIMA) then
    begin
      Application.MessageBox(PChar('Lan�amento Encerrado. Para editar '+
      'este item desfa�a o encerramento do m�s correspondente!'), 'Erro', MB_OK+MB_ICONERROR);
      Exit;
    end;
    //
    if UMyMod.SelLockInt64Y(Controle, Dmod.MyDB, VAR_LCT, 'Controle') then
      Exit;
    if (AlteraAtehFatID = False) and (QrLct <> nil)
    and (QrLct.FieldByName('FatID').AsInteger <> 0 ) then
    begin
      Application.MessageBox(PChar('Lan�amento espec�fico. Para editar '+
      'este item selecione sua janela correta!'), 'Erro', MB_OK+MB_ICONERROR);
      Exit;
    end;
  end else
  if Acao <> tgrDuplica then
  begin
    Controle := 0;
    Sub := 0;
  end;
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    if Acao = tgrAltera then
    begin
      UMyMod.UpdLockInt64Y(Controle, Dmod.MyDB, VAR_LCT, 'Controle');
      if (Acao = tgrTeste) and (Genero < 0) then
      begin
        if Acao = tgrTeste then Result := -1
        else begin
          case Genero of
            -1: UFinanceiro.CriarTransferCart(1, QrLct, QrCrt, True,
              CliInt, FatID, FatID_Sub, FatNum);
            else
            begin
              Application.MessageBox(PChar('Lan�amento protegido. Para editar '+
              'este item selecione seu caminho correto!'), 'Erro', MB_OK+MB_ICONERROR);
              Exit;
            end;
          end;
        end;
        Exit;
      end;
      if (Acao = tgrTeste) and (Genero = 0) and (Controle = 0) then
      begin
        if Acao = tgrTeste then Result := -2 else
        Application.MessageBox('N�o h� dados para editar', 'Erro', MB_OK+MB_ICONERROR);
        Screen.Cursor := MyCursor;
        Exit;
      end;
      {
      if (Cartao > 0) and not VAR_FATURANDO then
      begin
        if SoTestar then Result := -3 else
        Application.MessageBox(PChar('Esta emiss�o n�o pode ser editada pois '+
        'pertence a uma fatura!'), 'Erro', MB_OK+MB_ICONERROR);
        Exit;
      end;
      }
      if (Genero > 0) and (Sub > 0) then
      begin
        Geral.MensagemBox('Esta emiss�o n�o pode ser editada pois '+
        'pertence a transfer�ncia entre contas!', 'Erro', MB_OK+MB_ICONERROR);
        Exit;
      end;
    end;
    if Acao = tgrTeste then
    begin
      Result := 1;
      Exit;
    end;
    if Finalidade = lfUnknow then
    begin
      Application.MessageBox(
      'N�o foi definida nenhuma finalidade para o lan�amento. AVISE A DERMATEK!',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    if not CriaLanctoEditor(InstanceClass, Reference, ModoAcesso, QrLct,
    Controle, Sub, Finalidade) then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    //
    try
    try
      if Controle <> 0 then
      begin
        LancamentoDuplica(DMod.QrAux, Controle, Sub);
        with TForm(Reference) do
        begin
          for i := 0 to ComponentCount -1 do
          begin
            PropInfo := GetPropInfo(TComponent(Components[i]).ClassInfo, 'QryCampo');
            if PropInfo <> nil then
            begin
              Campo := GetStrProp(TComponent(Components[i]), PropInfo);
              if Campo <> '' then
              begin
                if (Components[i] is TdmkEdit) then
                begin
                  if TdmkEdit(Components[i]).FormatType = dmktfMesAno then
                    TdmkEdit(Components[i]).Text :=
                    Geral.MesEAnoDoMez(
                    TmySQLQuery(QrLct).FieldByName(Campo).AsString)
                  else
                  if TdmkEdit(Components[i]).FormatType = dmktf_AAAAMM then
                    TdmkEdit(Components[i]).Text :=
                    Geral.AnoEMesDoMez(
                    TmySQLQuery(QrLct).FieldByName(Campo).AsString)
                  else
                    TdmkEdit(Components[i]).ValueVariant :=
                    TmySQLQuery(QrLct).FieldByName(Campo).AsVariant
                end
                //
                else if (Components[i] is TdmkDBLookupCombobox) then
                  TdmkDBLookupCombobox(Components[i]).KeyValue :=
                  TmySQLQuery(QrLct).FieldByName(Campo).AsVariant
                //
                else if (Components[i] is TdmkEditCB) then
                  TdmkEditCB(Components[i]).ValueVariant :=
                  TmySQLQuery(QrLct).FieldByName(Campo).AsVariant
                //
                else if (Components[i] is TdmkEditDateTimePicker) then
                  TdmkEditDateTimePicker(Components[i]).Date :=
                  TmySQLQuery(QrLct).FieldByName(Campo).AsDateTime;

              end;
            end;
          end;
        end;
      end;
      with TForm(Reference) do
      begin
        MyVCLref.SET_Component(TForm(Reference), 'LaFinalidade', 'Caption', FormatFloat('000', IDFinalidade), TLabel);
        MyVCLref.SET_Component(TForm(Reference), 'dmkEdOneCliente', 'ValueVariant', Cliente, TdmkEdit);
        MyVCLref.SET_Component(TForm(Reference), 'dmkEdOneFornecedor', 'ValueVariant', Fornecedor, TdmkEdit);
        MyVCLref.SET_Component(TForm(Reference), 'dmkEdOneCliInt', 'ValueVariant', CliInt, TdmkEdit);
        MyVCLref.SET_Component(TForm(Reference), 'dmkEdOneForneceI', 'ValueVariant', ForneceI, TdmkEdit);
        MyVCLref.SET_Component(TForm(Reference), 'dmkEdOneAccount', 'ValueVariant', Account, TdmkEdit);
        MyVCLref.SET_Component(TForm(Reference), 'dmkEdOneVendedor', 'ValueVariant', Vendedor, TdmkEdit);
        //
        MyVCLref.SET_Component(TForm(Reference), 'dmkEdCBCliInt', 'Enabled', not LockCliInt, TdmkEdit);
        MyVCLref.SET_Component(TForm(Reference), 'dmkCBCliInt', 'Enabled', not LockCliInt, TdmkDBLookupCombobox);
        MyVCLref.SET_Component(TForm(Reference), 'dmkEdCBForneceI', 'Enabled', not LockForneceI, TdmkEdit);
        MyVCLref.SET_Component(TForm(Reference), 'dmkEdCBAccount', 'Enabled', not LockAccount, TdmkEdit);
        MyVCLref.SET_Component(TForm(Reference), 'dmkEdCBVendedor', 'Enabled', not LockVendedor, TdmkEdit);
        //
        MyVCLref.SET_Component(TForm(Reference), 'LaCliInt', 'Enabled', not LockCliInt, TLabel);
        MyVCLref.SET_Component(TForm(Reference), 'LaForneceI', 'Enabled', not LockForneceI, TLabel);
        MyVCLref.SET_Component(TForm(Reference), 'LaAccount', 'Enabled', not LockAccount, TLabel);
        MyVCLref.SET_Component(TForm(Reference), 'LaVendedor', 'Enabled', not LockVendedor, TLabel);
        //
        MyVCLref.SET_Component(TForm(Reference), 'dmkCBCliInt', 'Enabled', not LockCliInt, TDBLookupComboBox);
        MyVCLref.SET_Component(TForm(Reference), 'dmkCBForneceI', 'Enabled', not LockForneceI, TDBLookupComboBox);
        MyVCLref.SET_Component(TForm(Reference), 'dmkCBAccount', 'Enabled', not LockAccount, TDBLookupComboBox);
        MyVCLref.SET_Component(TForm(Reference), 'dmkCBVendedor', 'Enabled', not LockVendedor, TDBLookupComboBox);
        //
        if Acao = tgrInclui then
        begin
          Txt_LaTipo := DmkEnums.NomeTipoSQL(stIns);

          MyVCLref.SET_dmkEditDateTimePicker(TForm(Reference),
            'dmkEdTPCompensado', 'Date', 0);
          MyVCLref.SET_dmkEditDateTimePicker(TForm(Reference),
            'dmkEdTPData', 'Date', Int(Date));
          MyVCLref.SET_dmkEdit(TForm(Reference), 'dmkEdFatID', 'ValueVariant', FatID);
          MyVCLref.SET_dmkEdit(TForm(Reference), 'dmkEdFatID_Sub', 'ValueVariant', FatID_Sub);
          MyVCLref.SET_dmkEdit(TForm(Reference), 'dmkEdFatNum', 'ValueVariant', FatNum);

          if Data > 1 then
            MyVCLref.SET_dmkEditDateTimePicker(TForm(Reference), 'dmkEdTPData', 'Date', Data);
          if Vencto > 1 then
            MyVCLref.SET_dmkEditDateTimePicker(TForm(Reference), 'dmkEdTPVencto', 'Date', Vencto);
          if DataDoc > 1 then
            MyVCLref.SET_dmkEditDateTimePicker(TForm(Reference), 'dmkEdTPDataDoc', 'Date', DataDoc);

          MyVCLref.SET_dmkEditCB(TForm(Reference), 'dmkEdCarteira', 'ValueVariant', Carteira);
          MyVCLref.SET_dmkDBLookupComboBox(TForm(Reference), 'dmkCBCarteira', 'KeyValue', Carteira);

          MyVCLref.SET_dmkEditCB(TForm(Reference), 'dmkEdCBGenero', 'ValueVariant', Genero);
          MyVCLref.SET_dmkDBLookupComboBox(TForm(Reference), 'dmkCBGenero', 'KeyValue', Genero);

          MyVCLref.SET_dmkEdit(TForm(Reference), 'dmkEdDeb', 'ValueVariant', Debito);
          MyVCLref.SET_dmkEdit(TForm(Reference), 'dmkEdCred', 'ValueVariant', Credito);
          if Mes > 2 then
            MyVCLref.SET_dmkEdit(TForm(Reference), 'dmkEdMes', 'ValueVariant', Mes);
          // N�o se sabe ainda?
          Sit  := -1;
          Tipo := -1;
        end else begin
          Sit  := TmySQLQuery(QrLct).FieldByName('Sit').AsInteger;
          Tipo := TmySQLQuery(QrLct).FieldByName('Tipo').AsInteger;
          //
          case Acao of
            tgrAltera:
            begin
              Txt_LaTipo := CO_ALTERACAO;
              MyVCLref.SET_dmkEditDateTimePicker(TForm(Reference), 'TPOldData', 'Date',
                TmySQLQuery(QrLct).FieldByName('Data').AsDateTime);
              MyVCLref.SET_dmkEdit(TForm(Reference), 'EdOldControle', 'ValueVariant',
                TmySQLQuery(QrLct).FieldByName('Controle').AsFloat);
              MyVCLref.SET_dmkEdit(TForm(Reference), 'EdOldSub', 'ValueVariant',
                TmySQLQuery(QrLct).FieldByName('Sub').AsInteger);
              MyVCLref.SET_dmkEdit(TForm(Reference), 'EdOldTipo', 'ValueVariant',
                TmySQLQuery(QrLct).FieldByName('Tipo').AsInteger);
              MyVCLref.SET_dmkEdit(TForm(Reference), 'EdOldCarteira', 'ValueVariant',
                TmySQLQuery(QrLct).FieldByName('Carteira').AsInteger);
            end;
            tgrDuplica:
            begin
              Txt_Latipo := DmkEnums.NomeTipoSQL(stIns);
              MyVCLref.SET_dmkEditDateTimePicker(TForm(Reference),
                'dmkEdTPCompensado', 'Date', 0);
              MyVCLref.SET_Component(TForm(Reference),
                'CkDuplicando', 'Checked', True, TCheckBox);
            end;
          end;
          if Acao = tgrDuplica then Txt_Ctrl := '0' else Txt_ctrl := IntToStr(
            TmySQLQuery(QrLct).FieldByName('Controle').AsInteger);
        end;
        MyVCLref.SET_Component(TForm(Reference), 'dmkEdControle', 'Texto',
          Txt_Ctrl, TdmkEdit);
        MyVCLref.SET_Label(TForm(Reference), 'LaTipo', 'Caption', Txt_LaTipo);
        //
        if ((Sit > 1) or (Compensado > 2)) and (Tipo = 2) and (Acao = tgrAltera) then
        begin
          Application.MessageBox('Esta emiss�o j� est� quitada!', 'Aviso',
            MB_OK+MB_ICONWARNING);
          TForm(Reference).Destroy;
          Exit;
        end;
        if (Sit > 1) and (Tipo = 2) and (Acao <> tgrDuplica) then
        begin
          Application.MessageBox('Esta emiss�o j� est� quitada!', 'Aviso',
            MB_OK+MB_ICONWARNING);
          MyVCLref.SET_Component(TForm(Reference), 'dmkEdDeb', 'Enabled', False, TdmkEdit);
          MyVCLref.SET_Component(TForm(Reference), 'dmkEdCred', 'Enabled', False, TdmkEdit);
          MyVCLref.SET_Component(TForm(Reference), 'dmkEdDoc', 'Enabled', False, TdmkEdit);
          MyVCLref.SET_Component(TForm(Reference), 'dmkEdDescricao', 'Enabled', False, TdmkEdit);
          MyVCLref.SET_Component(TForm(Reference), 'dmkEdCarteira', 'Enabled', False, TdmkEdit);
          MyVCLref.SET_Component(TForm(Reference), 'dmkEdCarteira', 'Enabled', False, TdmkEdit);
          VAR_BAIXADO := Sit;
        end else VAR_BAIXADO := -2;
        try
          //SetaVars();
        except
          Application.MessageBox(PChar('N�o foi poss�vel setar vari�veis ' +
          'secund�rias para a janela de lan�amentos!'),
          'Erro', MB_OK+MB_ICONERROR);
        end;
        //
      end;
      Screen.Cursor := MyCursor;
    finally
      Screen.Cursor := MyCursor;
    end;
    except
      raise;
    end;
    TForm(Reference).ShowModal;
    Result :=
      MyVCLref.GET_dmkEdit(TForm(Reference), 'dmkEdExecs', 'ValueVariant');
    TForm(Reference).Destroy;
    UMyMod.UpdUnLockInt64Y(Controle, Dmod.MyDB, VAR_LCT, 'Controle');
  finally
    ;
  end;
end;

function TUnFinanceiro.IndProc_Get(Codigo: Integer): String;
begin
// C�digo de Origem do Processo
  case Codigo of
    0: Result := 'SEFAZ';
    1: Result := 'Justi�a Federal';
    2: Result := 'Justi�a Estadual';
    3: Result := 'Secex/RFB';
    9: Result := 'Outros';
    else Result := '';
  end;
end;

function TUnFinanceiro.IndProc_Lista: MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const Codigo, Descricao: String);
  begin
    SetLength(Res, Linha + 1);
    SetLength(Res[Linha], 3);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
//TABELA IndProc
  AddLinha(Result, Linha, '0', 'SEFAZ');
  AddLinha(Result, Linha, '1', 'Justi�a Federal');
  AddLinha(Result, Linha, '2', 'Justi�a Estadual');
  AddLinha(Result, Linha, '3', 'Secex/RFB');
  AddLinha(Result, Linha, '9', 'Outros');
end;

function TUnFinanceiro.NaoDuplicarLancto(LaTipo: String; Cheque: Extended; PesqCH:
              Boolean; SerieCH: String; NF: Integer; PesqNF: Boolean;
              Credito, Debito: Double; Conta: Integer; Mes: String;
              PesqVal: Boolean; CliInt: Integer; EstahEmLoop: Boolean;
              Carteira, Cliente, Fornecedor: Integer;
              LaAviso: TLabel): Integer;
{
function TUnFinanceiro.NaoDuplicarLancto(LaTipo: String; Cheque: Integer; PesqCH:
  Boolean; SerieCH: String; NF: Integer; PesqNF: Boolean;
  Credito, Debito: Double; Conta: Integer; Mez: String; PesqVal: Boolean;
  CliInt: Integer): Boolean;
}
var
  //NotaF: Integer;
  SCH: String;
  Mostra: Boolean;
begin
  //Result := 0;
  if LaAviso <> nil then
    LaAviso.Caption := 'Iniciando pesquisas...';
  //NotaF := 0;
  Mostra := False;
  if LaTipo <> DmkEnums.NomeTipoSQL(stIns) then
  begin
    Result := 1;
    Exit;
  end;// else Result := 0;
  //
  //Cheque := Geral.IMV(EdDoc.Text);
  if PesqCH and (Cheque > 0) then
  begin
    if LaAviso <> nil then
      LaAviso.Caption := 'Pesquisando cheques...';
    SCH := Trim(SerieCh);
    DModFin.QrDuplCH.Close;
    DModFin.QrDuplCH.SQL.Clear;
    DModFin.QrDuplCH.SQL.Add('SELECT lan.Data, lan.Controle, lan.Descricao, lan.Credito,');
    DModFin.QrDuplCH.SQL.Add('lan.Debito, lan.NotaFiscal, lan.Compensado, lan.Mez,');
    DModFin.QrDuplCH.SQL.Add('lan.Fornecedor, lan.Cliente, car.Nome NOMECART,');
    DModFin.QrDuplCH.SQL.Add('lan.Documento, lan.SerieCH, lan.Carteira,');
    DModFin.QrDuplCH.SQL.Add('IF(lan.Cliente<>0, CASE WHEN cli.Tipo=0 THEN');
    DModFin.QrDuplCH.SQL.Add('cli.RazaoSocial ELSE cli.Nome END, "") NOMECLI,');
    DModFin.QrDuplCH.SQL.Add('IF(lan.Fornecedor<>0, CASE WHEN fnc.Tipo=0 THEN');
    DModFin.QrDuplCH.SQL.Add('fnc.RazaoSocial ELSE fnc.Nome END, "") NOMEFNC');
    DModFin.QrDuplCH.SQL.Add('FROM ' + VAR_LCT + ' lan');
    DModFin.QrDuplCH.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    DModFin.QrDuplCH.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=lan.Cliente');
    DModFin.QrDuplCH.SQL.Add('LEFT JOIN entidades fnc ON fnc.Codigo=lan.Fornecedor');
    DModFin.QrDuplCH.SQL.Add('WHERE ID_Pgto = 0');
    DModFin.QrDuplCH.SQL.Add('AND car.ForneceI=' + FormatFloat('0', CliInt));
    DModFin.QrDuplCH.SQL.Add('AND lan.Documento=:P0');
    // 2011-07-22
    DModFin.QrDuplCH.SQL.Add('AND car.Codigo=' + FormatFloat('0', Carteira));
    // Fim 2011-07-22

    DModFin.QrDuplCH.Params[0].AsFloat := Cheque;

    if SCH <> '' then
    begin
      DModFin.QrDuplCH.SQL.Add('AND lan.SerieCH=:P1');
      DModFin.QrDuplCH.Params[1].AsString := SCH;
    end;
    DModFin.QrDuplCH.Open;
    Mostra := DModFin.QrDuplCH.RecordCount > 0;
    if LaAviso <> nil then
      LaAviso.Caption := 'Pesquisa de cheques aberta...';
  end;

  if PesqNF and (NF > 0) then
  begin
    if LaAviso <> nil then
      LaAviso.Caption := 'Pesquisando notas fiscais...';
    DModFin.QrDuplNF.Close;
    DModFin.QrDuplNF.Params[00].AsInteger := NF;
    DModFin.QrDuplNF.Params[01].AsInteger := CliInt;
    DModFin.QrDuplNF.Open;
    Mostra := Mostra or (DModFin.QrDuplNF.RecordCount > 0);
    if LaAviso <> nil then
      LaAviso.Caption := 'Pesquisa de NFs aberta...';
  end;

  if PesqVal and ((Credito <> 0) or (Debito <> 0)) and
  (Conta <> 0) and (Mes <> '') then
  begin
    if LaAviso <> nil then
      LaAviso.Caption := 'Pesquisando valores...';
    DModFin.QrDuplVal.Close;
    DModFin.QrDuplVal.Params[00].AsFloat   := Credito;
    DModFin.QrDuplVal.Params[01].AsFloat   := Debito;
    DModFin.QrDuplVal.Params[02].AsInteger := Conta;
    DModFin.QrDuplVal.Params[03].AsString  := Mes;
    // 2011-07-22
    DModFin.QrDuplVal.Params[04].AsInteger := Cliente;
    DModFin.QrDuplVal.Params[05].AsInteger := Fornecedor;
    DModFin.QrDuplVal.Params[06].AsInteger := CliInt;
    // Fim 2011-07-22
    DModFin.QrDuplVal.Open;
    Mostra := Mostra or (DModFin.QrDuplVal.RecordCount > 0);
    if LaAviso <> nil then
      LaAviso.Caption := 'Pesquisa de valores aberta...';
  end;

  if Mostra then
  begin
    if LaAviso <> nil then
      LaAviso.Caption := 'Abrindo janela para mostrar valores...';
    MyObjects.CriaForm_AcessoTotal(TFmLctDuplic, FmLctDuplic);
    FmLctDuplic.FConfirma := 0;
    if Cheque = 0 then FmLctDuplic.STCheque.Caption := 'Cheque: N�o pesquisado' else
      FmLctDuplic.STCheque.Caption := 'Cheque: '+SCH + ' ' + FormatFloat('0', Cheque);
    if NF = 0 then FmLctDuplic.STNotaF.Caption := 'Nota Fiscal: N�o pesquisado' else
      FmLctDuplic.STNotaF.Caption := 'Nota Fiscal: '+IntToStr(NF);
    FmLctDuplic.BtEstahEmLoop.Visible := EstahEmLoop;
    FmLctDuplic.ShowModal;
    Result := FmLctDuplic.FConfirma;
    FmLctDuplic.Destroy;
  end else Result := 1;

  if Result = 1 then Screen.Cursor := crDefault;
  if LaAviso <> nil then
    LaAviso.Caption := 'Pesquisa de duplica��o finalizada...';
end;

function TUnFinanceiro.AtualizaPagamentosAVista(QrLctos: TmySQLQuery): Boolean;
var
  Controle: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ' + VAR_LCT + ' SET Compensado=Data ');
  Dmod.QrUpd.SQL.Add('WHERE Tipo<2 AND Compensado < 2');
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ' + VAR_LCT + ' SET Sit=3 ');
  Dmod.QrUpd.SQL.Add('WHERE Tipo<2 AND Sit < 2');
  Dmod.QrUpd.ExecSQL;
  Result := True;
  //
  if (QrLctos <> nil) and (QrLctos.State <> dsInactive) then
  begin
    Controle := QrLctos.FieldByName('Controle').AsInteger;
    QrLctos.Close;
    QrLctos.Open;
    if Controle > 0 then
      QrLctos.Locate('Controle', Controle, []);
  end;
  Screen.Cursor := crDefault;
end;

procedure TUnFinanceiro.AtualizaPagos();
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ' + VAR_LCT + ' SET Compensado=Vencimento');
  Dmod.QrUpd.SQL.Add('WHERE Tipo=2 AND Compensado<2 AND Sit > 1');
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ' + VAR_LCT + ' SET Sit=2');
  Dmod.QrUpd.SQL.Add('WHERE Tipo=2 AND Compensado>2');
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.ExecSQL;
end;

function TUnFinanceiro.AtualizaSaldosDeContas(Entidade, Periodo: Integer;
MostraVerif: Boolean): Boolean;
begin
  Result := False;
  if DBCheck.CriaFm(TFmContasHistAtz2, FmContasHistAtz2, afmoNegarComAviso) then
  begin
    FmContasHistAtz2.FEntiCod := Entidade;
    FmContasHistAtz2.ShowModal;
    Result := FmContasHistAtz2.FContinua;
    FmContasHistAtz2.Destroy;
    //
    if Result and MostraVerif then
    begin
      DmodFin.QrVerifHis.Close;
      DmodFin.QrVerifHis.Params[00].AsInteger := Entidade;
      DmodFin.QrVerifHis.Params[01].AsInteger := Periodo;
      DmodFin.QrVerifHis.Open;
      if DmodFin.QrVerifHis.RecordCount > 0 then
      begin
        if DBCheck.CriaFm(TFmContasHisVerif, FmContasHisVerif, afmoNegarComAviso) then
        begin
          FmContasHisVerif.ShowModal;
          FmContasHisVerif.Destroy;
        end;
      end;
    end;
  end;
end;

function TUnFinanceiro.SQLInsUpd_Lct(QrUpd: TmySQLQuery; Tipo: TSQLType;
  Auto_increment: Boolean; SQLCampos, SQLIndex: array of String; ValCampos,
  ValIndex: array of Variant; UserDataAlterweb: Boolean;
  ComplUpd: String;
  // Compatibilidade
  TabLct: String = ''): Boolean;
begin
  Result := UMyMod.SQLInsUpd(QrUpd, Tipo, VAR_LCT, Auto_increment,
    SQLCampos, SQLIndex, ValCampos, ValIndex, UserDataAlterweb, ComplUpd);
end;

function TUnFinanceiro.SQLLoc1(Query: TmySQLQuery; Tabela, Campo: String;
  Valor: Variant; MsgLoc, MsgNaoLoc: String): Integer;
var
  Txt: String;
begin
  Txt := Geral.VariavelToString(Valor);
  //
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT ' + Campo);
  Query.SQL.Add('FROM ' + Lowercase(Tabela));
  Query.SQL.Add('WHERE ' + Campo + '=' + Txt);
  Query.Open;
  //
  Result := Query.RecordCount;
  //
  if (Result = 0) and (Trim(MsgNaoLoc) <> '') then
    Application.MessageBox(PChar(MsgNaoLoc), 'Item n�o Localizado',
    MB_OK+MB_ICONWARNING);
  if (Result > 0) and (Trim(MsgLoc) <> '') then
    Application.MessageBox(PChar(MsgLoc + Chr(13) + Chr(10) + 'Itens = ' +
    IntToStr(Result)), 'Itenm Localizado', MB_OK+MB_ICONEXCLAMATION);
end;

function TUnFinanceiro.ProximoRegistro(Query: TmySQLQuery; Campo: String; Atual:
Integer): Integer;
begin
  Query.Next;
  if Query.FieldByName(Campo).AsInteger = Atual then Query.Prior;
  Result := Query.FieldByName(Campo).AsInteger;
end;

procedure TUnFinanceiro.AtzDataAtzSdoPlaCta(Entidade: Integer; Data: TDateTime);
begin
  DmodFin.QrUDAPC.Params[00].AsDate    := Data;
  DmodFin.QrUDAPC.Params[01].AsInteger := Entidade;
  DmodFin.QrUDAPC.ExecSQL;
end;

procedure TUnFinanceiro.CriaFmQuitaDoc2(Data: TDateTime; Documento, Controle,
 CtrlIni: Double; Sub: Integer; QrCrt: TmySQLQuery);
//var
  //Cursor : TCursor;
begin
  if DBCheck.CriaFm(TFmQuitaDoc2, FmQuitaDoc2, afmoNegarComAviso) then
  begin
    FmQuitaDoc2.FCtrlIni              := CtrlIni;
    FmQuitaDoc2.FControle             := Controle;
    //FmQuitaDoc2.LaQuitar.Caption    := IntToStr(Acao);
    FmQuitaDoc2.TPData1.Date          := Data;//Dmod.QrLctData.Value;
    FmQuitaDoc2.EdDoc.Text            := FloatToStr(Documento);
    FmQuitaDoc2.EdLancto.ValueVariant := FloatToStr(Controle);
    FmQuitaDoc2.EdSub.ValueVariant    := Geral.TFT(IntToStr(Sub), 0, siPositivo);
    FmQuitaDoc2.ShowModal;
    FmQuitaDoc2.Destroy;
    DmodFin.DefParams(QrCrt, nil, True, 'TUnFinanceiro.CriaFmQuitaDoc2()');
  end;
end;

function TUnFinanceiro.CSOSN_Get(CRT, Codigo: Integer): String;
begin
// C�digo de Situa��o da Opera��o no Simples Nacional
  if CRT = 1 then
  begin
    case Codigo of
      101: Result := 'Tributada pelo Simples Nacional com permiss�o de cr�dito.';
      102: Result := 'Tributada pelo Simples Nacional sem permiss�o de cr�dito.';
      103: Result := 'Isen��o do ICMS no Simples Nacional para faixa de receita bruta.';
      201: Result := 'Tributada pelo Simples Nacional com permiss�o de cr�dito e com cobran�a do ICMS por Substitui��o Tribut�ria';
      202: Result := 'Tributada pelo Simples Nacional sem permiss�o de cr�dito e com cobran�a do ICMS por Substitui��o Tribut�ria';
      203: Result := 'Isen��o do ICMS nos Simples Nacional para faixa de receita bruta e com cobran�a do ICMS por Substitui��o Tribut�ria';
      300: Result := 'Imune.';
      400: Result := 'N�o tributada pelo Simples Nacional';
      500: Result := 'ICMS cobrado anteriormente por substitui��o tribut�ria (substitu�do) ou por antecipa��o';
      900: Result := 'Outros';
      else Result := 'CSOSN inv�lido ou n�o implementado!';
    end;
  end else Result := 'CRT selecionado n�o prev� nenhum CSOSN!';
end;

function TUnFinanceiro.CSOSN_Lista: MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const CRT, Codigo, Descricao: String);
  begin
    SetLength(Res, Linha + 1);
    SetLength(Res[Linha], 3);
    Res[Linha][0] :=  CRT;
    Res[Linha][1] :=  Codigo;
    Res[Linha][2] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
//TABELA A ORIGEM DA MERCADORIA
  AddLinha(Result, Linha, '1', '101', 'Tributada pelo Simples Nacional com permiss�o de cr�dito.');
  AddLinha(Result, Linha, '1', '102', 'Tributada pelo Simples Nacional sem permiss�o de cr�dito.');
  AddLinha(Result, Linha, '1', '103', 'Isen��o do ICMS no Simples Nacional para faixa de receita bruta.');
  AddLinha(Result, Linha, '1', '201', 'Tributada pelo Simples Nacional com permiss�o de cr�dito e com cobran�a do ICMS por Substitui��o Tribut�ria');
  AddLinha(Result, Linha, '1', '202', 'Tributada pelo Simples Nacional sem permiss�o de cr�dito e com cobran�a do ICMS por Substitui��o Tribut�ria');
  AddLinha(Result, Linha, '1', '203', 'Isen��o do ICMS nos Simples Nacional para faixa de receita bruta e com cobran�a do ICMS por Substitui��o Tribut�ria');
  AddLinha(Result, Linha, '1', '300', 'Imune.');
  AddLinha(Result, Linha, '1', '400', 'N�o tributada pelo Simples Nacional');
  AddLinha(Result, Linha, '1', '500', 'ICMS cobrado anteriormente por substitui��o tribut�ria (substitu�do) ou por antecipa��o');
  AddLinha(Result, Linha, '1', '900', 'Outros');
end;

function TUnFinanceiro.CST_A_Get(Codigo: Integer): String;
begin
//TABELA A ORIGEM DA MERCADORIA
  case Codigo of
    0: Result := 'Nacional';
    1: Result := 'Estrangeira Importa��o direta';
    2: Result := 'Estrangeira Adquirida no mercado interno';
    else Result := '';
  end;
end;

function TUnFinanceiro.CST_A_Lista(): MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const Codigo, Descricao: String);
  begin
    SetLength(Res, Linha+1);
    SetLength(Res[Linha], 2);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
//TABELA A ORIGEM DA MERCADORIA
  AddLinha(Result, Linha, '0', 'Nacional');
  AddLinha(Result, Linha, '1', 'Estrangeira Importa��o direta');
  AddLinha(Result, Linha, '2', 'Estrangeira Adquirida no mercado interno');
end;

function TUnFinanceiro.CST_B_Get(Codigo: Integer): String;
begin
//TABELA B TRIBUTA��O PELO ICMS
  case Codigo of
    00: Result := 'Tributada integralmente';
    10: Result := 'Tributada e com cobran�a do ICMS por substitui��o tribut�ria';
    20: Result := 'Com redu��o de base de c�lculo';
    30: Result := 'Isenta ou n�o tributada e com cobran�a do ICMS por substitui��o tribut�ria';
    40: Result := 'Isenta';
    41: Result := 'N�o tributada';
    50: Result := 'Suspens�o';
    51: Result := 'Diferimento';
    60: Result := 'ICMS cobrado anteriormente por substitui��o tribut�ria';
    70: Result := 'Com redu��o de base de c�lculo e cobran�a do ICMS por substitui��o tribut�ria';
    90: Result := 'Outras';
    else Result := '';
  end;
end;

function TUnFinanceiro.CST_B_Lista(): MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const Codigo, Descricao: String);
  begin
    SetLength(Res, Linha+1);
    SetLength(Res[Linha], 2);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
//TABELA B TRIBUTA��O PELO ICMS
  AddLinha(Result, Linha, '00', 'Tributada integralmente');
  AddLinha(Result, Linha, '10', 'Tributada e com cobran�a do ICMS por substitui��o tribut�ria');
  AddLinha(Result, Linha, '20', 'Com redu��o de base de c�lculo');
  AddLinha(Result, Linha, '30', 'Isenta ou n�o tributada e com cobran�a do ICMS por substitui��o tribut�ria');
  AddLinha(Result, Linha, '40', 'Isenta');
  AddLinha(Result, Linha, '41', 'N�o tributada');
  AddLinha(Result, Linha, '50', 'Suspens�o');
  AddLinha(Result, Linha, '51', 'Diferimento');
  AddLinha(Result, Linha, '60', 'ICMS cobrado anteriormente por substitui��o tribut�ria');
  AddLinha(Result, Linha, '70', 'Com redu��o de base de c�lculo e cobran�a do ICMS por substitui��o tribut�ria');
  AddLinha(Result, Linha, '90', 'Outras');
end;

function TUnFinanceiro.CST_IPI_Get(Codigo: Integer): String;
begin
//TABELA TRIBUTA��O PELO IPI
  case Codigo of
    00: Result := 'Entrada com recupera��o de cr�dito';
    01: Result := 'Entrada tributada com aliquota zero';
    02: Result := 'Entrada isenta';
    03: Result := 'Entrada n�o tributada';
    04: Result := 'Entrada imune';
    05: Result := 'Entrada com suspens�o';
    49: Result := 'Outras entradas';
    50: Result := 'Sa�da tributada';
    51: Result := 'Sa�da tributada com aliquota zero';
    52: Result := 'Sa�da isenta';
    53: Result := 'Sa�da n�o tributada';
    54: Result := 'Sa�da imune';
    55: Result := 'Sa�da com suspens�o';
    99: Result := 'Outras sa�das';
    else Result := '? ? ? ? ?';
  end;
end;

function TUnFinanceiro.CST_IPI_Lista: MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const Codigo, Descricao: String);
  begin
    SetLength(Res, Linha+1);
    SetLength(Res[Linha], 2);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
//TABELA TRIBUTA��O PELO IPI
  AddLinha(Result, Linha, '00', 'Entrada com recupera��o de cr�dito');
  AddLinha(Result, Linha, '01', 'Entrada tributada com aliquota zero');
  AddLinha(Result, Linha, '02', 'Entrada isenta');
  AddLinha(Result, Linha, '03', 'Entrada n�o tributada');
  AddLinha(Result, Linha, '04', 'Entrada imune');
  AddLinha(Result, Linha, '05', 'Entrada com suspens�o');
  AddLinha(Result, Linha, '49', 'Outras entradas');
  AddLinha(Result, Linha, '50', 'Sa�da tributada');
  AddLinha(Result, Linha, '51', 'Sa�da tributada com aliquota zero');
  AddLinha(Result, Linha, '52', 'Sa�da isenta');
  AddLinha(Result, Linha, '53', 'Sa�da n�o tributada');
  AddLinha(Result, Linha, '54', 'Sa�da imune');
  AddLinha(Result, Linha, '55', 'Sa�da com suspens�o');
  AddLinha(Result, Linha, '99', 'Outras sa�das');
end;

function TUnFinanceiro.CST_PIS_Get(Codigo: Integer): String;
begin
//TABELA TRIBUTA��O PELO PIS
  case Codigo of
    01: Result := 'Opera��o tribut�vel (base de c�lculo = valor da opera��o x al�quota normal (cumulativo / n�o cumulativo))';
    02: Result := 'Opera��o tribut�vel (base de c�lculo = valor da opera��o x (al�quota diferenciada))';
    03: Result := 'Opera��o tribut�vel (base de c�lculo = quantidade vendida x al�quota por unidade de produto)';
    04: Result := 'Opera��o tribut�vel (tributa��o monof�sica (al�quota zero))';
    06: Result := 'Opera��o tribut�vel (al�quota zero)';
    07: Result := 'Opera��o isenta da contribui��o';
    08: Result := 'Opera��o sem incid�ncia da contribui��o';
    09: Result := 'Opera��o com suspens�o da contribui��o';
    99: Result := 'Outras opera��es';
    else Result := '? ? ? ? ?';
  end;
end;

function TUnFinanceiro.CST_PIS_Lista: MyArrayLista;
  procedure AddLinha(var Res: MyArrayLista; var Linha: Integer; const Codigo, Descricao: String);
  begin
    SetLength(Res, Linha+1);
    SetLength(Res[Linha], 2);
    Res[Linha][0] :=  Codigo;
    Res[Linha][1] :=  Descricao;
    inc(Linha, 1);
  end;
var
  Linha: Integer;
begin
  Linha := 0;
  SetLength(Result, 0);
//TABELA TRIBUTA��O PELO PIS
  AddLinha(Result, Linha, '01', 'Opera��o tribut�vel (base de c�lculo = valor da opera��o x al�quota normal (cumulativo / n�o cumulativo))');
  AddLinha(Result, Linha, '02', 'Opera��o tribut�vel (base de c�lculo = valor da opera��o x (al�quota diferenciada))');
  AddLinha(Result, Linha, '03', 'Opera��o tribut�vel (base de c�lculo = quantidade vendida x al�quota por unidade de produto)');
  AddLinha(Result, Linha, '04', 'Opera��o tribut�vel (tributa��o monof�sica (al�quota zero))');
  AddLinha(Result, Linha, '06', 'Opera��o tribut�vel (al�quota zero)');
  AddLinha(Result, Linha, '07', 'Opera��o isenta da contribui��o');
  AddLinha(Result, Linha, '08', 'Opera��o sem incid�ncia da contribui��o');
  AddLinha(Result, Linha, '09', 'Opera��o com suspens�o da contribui��o');
  AddLinha(Result, Linha, '99', 'Outras opera��es');
end;

function TUnFinanceiro.CST_COFINS_Get(Codigo: Integer): String;
begin
//TABELA TRIBUTA��O PELO COFINS (� igual ao PIS)
  Result := CST_PIS_Get(Codigo);
end;

function TUnFinanceiro.CST_COFINS_Lista: MyArrayLista;
begin
//TABELA TRIBUTA��O PELO COFINS (� igual ao PIS)
  Result := CST_PIS_Lista();
end;

{
procedure TUnFinanceiro.AtualizaEndossos(Controle, Sub, OriCtrl, OriSub: Integer);
  procedure Atualiza_Atual(C, S: Integer; Endossado: Boolean);
  var
    Status: Integer;
    Sit1, Sit2: Boolean;
    Valor: Double;
  begin
    if (C = 0) and (S = 0) then Exit;
    //
    Sit1 := False;
    Sit2 := False;
    Valor := 0;
    DModFin.QrEndosso.First;
    while not DModFin.QrEndosso.Eof do
    begin
      if (DModFin.QrEndossoControle.Value = C)
      and (DModFin.QrEndossoSub.Value = S) then
      begin
        Sit1 := True;
        Valor := Valor + DModFin.QrEndossoValor.Value;
      end;
      if (DModFin.QrEndossoOriCtrl.Value = C)
      and (DModFin.QrEndossoOriSub.Value = S) then
        Sit2 := True;
      //
      DModFin.QrEndosso.Next;
    end;
    if Sit1 and Sit2 then
      Status := 3 //  Ambos
    else
    if Sit1 then
      Status := 1 // Endossado
    else
    if Sit2 then
      Status := 2  // Endossante
    else Status := 0; // Nenhum
    //
    if Endossado then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET Endossado=:P0, EndossVal=:P1 ');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa AND Sub=:Pb');
      Dmod.QrUpd.Params[00].AsInteger := Status;
      Dmod.QrUpd.Params[01].AsFloat   := Valor;
      //
      Dmod.QrUpd.Params[02].AsInteger := C;
      Dmod.QrUpd.Params[03].AsInteger := S;
      Dmod.QrUpd.ExecSQL;
    end else begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET Endossado=:P0 ');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa AND Sub=:Pb');
      Dmod.QrUpd.Params[00].AsInteger := Status;
      //
      Dmod.QrUpd.Params[01].AsInteger := C;
      Dmod.QrUpd.Params[02].AsInteger := S;
      Dmod.QrUpd.ExecSQL;
    end;
    AtualizaEmissaoMasterRapida(C, 3, 0);
  end;
begin
  DModFin.QrEndosso.Close;
  DModFin.QrEndosso.Params[00].AsInteger := Controle;
  DModFin.QrEndosso.Params[01].AsInteger := Sub;
  DModFin.QrEndosso.Params[02].AsInteger := Controle;
  DModFin.QrEndosso.Params[03].AsInteger := Sub;
  DModFin.QrEndosso.Open;
  Atualiza_Atual(Controle, Sub, True);
  //
  DModFin.QrEndosso.Close;
  DModFin.QrEndosso.Params[00].AsInteger := OriCtrl;
  DModFin.QrEndosso.Params[01].AsInteger := OriSub;
  DModFin.QrEndosso.Params[02].AsInteger := OriCtrl;
  DModFin.QrEndosso.Params[03].AsInteger := OriSub;
  DModFin.QrEndosso.Open;
  Atualiza_Atual(OriCtrl, OriSub, False);
  //
end;
}

procedure TUnFinanceiro.ReabreCarteirasCliInt(CliInt, LocCart: Integer;
QrCrt, QrCartSum: TmySQLQuery);
begin
  QrCrt.Close;
  if QrCrt.Params.Count = 1 then
    QrCrt.Params[0].AsInteger := CliInt
  else Geral.MensagemBox('Query de carteiras sem par�mentro de Cliente Interno!',
  'Erro', MB_OK+MB_ICONERROR);
  QrCrt.Open;
  //
  QrCrt.Locate('Codigo', LocCart, []);
  //
  if QrCartSum <> nil then
  begin
    QrCartSum.Close;
    if QrCrt.Params.Count = 1 then
      QrCartSum.Params[0].AsInteger := CliInt
    else Geral.MensagemBox('Query de soma de carteiras sem par�mentro de Cliente Interno!',
    'Erro', MB_OK+MB_ICONERROR);
    QrCartSum.Open;
  end;
end;

function TUnFinanceiro.NomeSitLancto(LctSit, LanctoTipo, CarteiraPrazo:
         Integer; LanctoVencto: TDateTime; Reparcelamento: Integer;
         ConverteCompensadoEmQuitado: Boolean = False): String;
begin
//  if LctSit = -2 then (Editando em LocLancto - n�o � setado em -2, � usado VAR_BAIXADO setado a -2)
  if Reparcelamento <> 0 then
     //reparcelado nos bloquetos ?
     Result := CO_REPARCELADO
  else
  case LctSit of
    -1: Result := CO_IMPORTACAO;
    00:
    begin
      if Int(LanctoVencto) < int(Date) then
         Result := CO_VENCIDA
      else
         Result := CO_EMABERTO;
    end;
    01: Result := CO_PAGTOPARCIAL;
    02: Result := CO_QUITADA;
    03:
    begin
      // Caixa com prazo
      if (CarteiraPrazo = 0) and (LanctoTipo = 0) then
      begin
        if LanctoVencto < Date then
          Result := CO_QUIT_AUTOM
        else
          Result := CO_PREDATADO;
      end else
      begin
       if ConverteCompensadoEmQuitado then
         Result := CO_QUITADA
       else
         Result := CO_COMPENSADA;
      end;
    end;
    04: Result := CO_CANCELADO;
    05: Result := CO_BLOQUEADO;
    else Result := '? ? ? ? ?';
  end;
end;

end.

