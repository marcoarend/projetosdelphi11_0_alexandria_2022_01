object FmCarteiras: TFmCarteiras
  Left = 346
  Top = 171
  Caption = 'FIN-CARTE-001 :: Carteiras'
  ClientHeight = 694
  ClientWidth = 1019
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 1019
    Height = 646
    Align = alClient
    BevelOuter = bvNone
    Color = clAppWorkSpace
    TabOrder = 1
    Visible = False
    object PainelConfirma: TPanel
      Left = 0
      Top = 598
      Width = 1019
      Height = 48
      Align = alBottom
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel11: TPanel
        Left = 911
        Top = 1
        Width = 107
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 8
          Top = 1
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 422
      Width = 1019
      Height = 176
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object PnCaixEdit: TPanel
        Left = 0
        Top = 0
        Width = 1019
        Height = 176
        Align = alClient
        TabOrder = 1
        Visible = False
        object Label23: TLabel
          Left = 8
          Top = 4
          Width = 59
          Height = 13
          Caption = 'Saldo inicial:'
        end
        object CkPrazo: TCheckBox
          Left = 100
          Top = 24
          Width = 97
          Height = 17
          Caption = 'Permite prazo?'
          TabOrder = 1
        end
        object EdInicial_C: TdmkEdit
          Left = 8
          Top = 20
          Width = 85
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object CkRecebeBloq: TCheckBox
          Left = 204
          Top = 24
          Width = 189
          Height = 17
          Caption = 'Recebe pagamentos de bloquetos.'
          TabOrder = 2
          Visible = False
        end
      end
      object PnEmisEdit: TPanel
        Left = 0
        Top = 0
        Width = 1019
        Height = 176
        Align = alClient
        TabOrder = 2
        Visible = False
        object Label21: TLabel
          Left = 8
          Top = 4
          Width = 243
          Height = 13
          Caption = 'Carteira banc'#225'ria ao qual a emiss'#227'o est'#225' vinculada:'
        end
        object Label34: TLabel
          Left = 468
          Top = 4
          Width = 34
          Height = 13
          Caption = 'Banco:'
        end
        object Label35: TLabel
          Left = 512
          Top = 4
          Width = 42
          Height = 13
          Caption = 'Ag'#234'ncia:'
        end
        object Label37: TLabel
          Left = 564
          Top = 4
          Width = 31
          Height = 13
          Caption = 'Conta:'
        end
        object GBFatura2: TGroupBox
          Left = 8
          Top = 44
          Width = 669
          Height = 60
          Caption = '      '
          TabOrder = 6
          Visible = False
          object Label15: TLabel
            Left = 7
            Top = 16
            Width = 238
            Height = 13
            Caption = 'Identificador autom'#225'tico de fatura em importa'#231#245'es:'
          end
          object Label16: TLabel
            Left = 344
            Top = 16
            Width = 157
            Height = 13
            Caption = 'Fechamento da fatura (dd antes):'
          end
          object Label20: TLabel
            Left = 504
            Top = 16
            Width = 92
            Height = 13
            Caption = 'Dia do vencimento:'
          end
          object EdIDFat: TdmkEdit
            Left = 8
            Top = 32
            Width = 333
            Height = 21
            Color = clWhite
            MaxLength = 50
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdDdFechamento: TdmkEdit
            Left = 344
            Top = 32
            Width = 157
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdDiaMesVence: TdmkEdit
            Left = 504
            Top = 32
            Width = 157
            Height = 21
            Alignment = taRightJustify
            Color = clWhite
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
        end
        object CkFatura: TCheckBox
          Left = 20
          Top = 44
          Width = 129
          Height = 18
          Caption = 'Pagamento por fatura.'
          TabOrder = 5
        end
        object CBBanco: TdmkDBLookupComboBox
          Left = 72
          Top = 20
          Width = 393
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsBancos
          TabOrder = 1
          dmkEditCB = EdBanco
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CkExigeNumCheque: TCheckBox
          Left = 8
          Top = 108
          Width = 169
          Height = 17
          Caption = 'Exige n'#250'mero do cheque.'
          TabOrder = 7
        end
        object RGTipoDoc: TRadioGroup
          Left = 8
          Top = 128
          Width = 668
          Height = 35
          Caption = ' Tipo de Documento: '
          Columns = 9
          ItemIndex = 0
          Items.Strings = (
            'N/I'
            'Cheque'
            'DOC'
            'TED'
            'Esp'#233'cie'
            'Bloqueto'
            'Duplicata'
            'TEF'
            'D'#233'bito c/c')
          TabOrder = 8
        end
        object EdBanco: TdmkEditCB
          Left = 8
          Top = 20
          Width = 61
          Height = 21
          Alignment = taRightJustify
          Color = clWhite
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = CBBanco
          IgnoraDBLookupComboBox = False
        end
        object EdBanco1_1: TdmkEdit
          Left = 468
          Top = 20
          Width = 41
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 3
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object EdAgencia1_1: TdmkEdit
          Left = 512
          Top = 20
          Width = 49
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 4
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object EdConta1_1: TdmkEdit
          Left = 564
          Top = 20
          Width = 113
          Height = 21
          Alignment = taCenter
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object CheckBox1: TCheckBox
          Left = 156
          Top = 108
          Width = 153
          Height = 17
          Caption = 'Ocultar saldo no bloqueto.'
          TabOrder = 9
        end
        object CkIgnorSerie: TdmkCheckBox
          Left = 312
          Top = 108
          Width = 257
          Height = 17
          Caption = 'Ignorar s'#233'rie do cheque na concilia'#231#227'o banc'#225'ria.'
          TabOrder = 10
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
        object CkNotConcBco: TdmkCheckBox
          Left = 577
          Top = 108
          Width = 257
          Height = 17
          Caption = 'Ignorar carteira na concilia'#231#227'o banc'#225'ria'
          TabOrder = 11
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
      end
      object PnBancEdit: TPanel
        Left = 0
        Top = 0
        Width = 1019
        Height = 176
        Align = alClient
        TabOrder = 0
        Visible = False
        object Label24: TLabel
          Left = 8
          Top = 4
          Width = 59
          Height = 13
          Caption = 'Saldo inicial:'
        end
        object Label25: TLabel
          Left = 92
          Top = 4
          Width = 34
          Height = 13
          Caption = 'Banco:'
        end
        object Label26: TLabel
          Left = 160
          Top = 4
          Width = 42
          Height = 13
          Caption = 'Ag'#234'ncia:'
        end
        object Label27: TLabel
          Left = 228
          Top = 4
          Width = 73
          Height = 13
          Caption = 'Conta corrente:'
        end
        object Label36: TLabel
          Left = 326
          Top = 4
          Width = 104
          Height = 13
          Caption = 'C'#243'digo retorno CNAB:'
        end
        object EdInicial_B: TdmkEdit
          Left = 8
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object EdBanco1: TdmkEdit
          Left = 92
          Top = 20
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 3
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object EdAgencia1: TdmkEdit
          Left = 160
          Top = 20
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 4
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object EdConta1: TdmkEdit
          Left = 228
          Top = 20
          Width = 94
          Height = 21
          Alignment = taCenter
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdCodCedente: TdmkEdit
          Left = 326
          Top = 20
          Width = 160
          Height = 21
          Alignment = taCenter
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
      end
    end
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 1019
      Height = 269
      Align = alTop
      TabOrder = 2
      object Label9: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object LaForneceI: TLabel
        Left = 8
        Top = 124
        Width = 167
        Height = 13
        Caption = 'Cliente interno que utiliza a carteira:'
      end
      object Label10: TLabel
        Left = 72
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label14: TLabel
        Left = 440
        Top = 4
        Width = 105
        Height = 13
        Caption = 'Texto para Impress'#227'o:'
      end
      object LaID2: TLabel
        Left = 64
        Top = 184
        Width = 176
        Height = 13
        Caption = 'Identificador extrato banco (Importar):'
      end
      object Label28: TLabel
        Left = 256
        Top = 184
        Width = 40
        Height = 13
        Caption = 'Contato:'
        Visible = False
      end
      object Label46: TLabel
        Left = 476
        Top = 184
        Width = 204
        Height = 13
        Caption = 'Identificador conta contabilidade (exportar):'
      end
      object Label32: TLabel
        Left = 8
        Top = 184
        Width = 34
        Height = 13
        Caption = 'Ordem:'
      end
      object Label33: TLabel
        Left = 8
        Top = 224
        Width = 146
        Height = 13
        Caption = 'Entidade detentora da carteira:'
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 20
        Width = 61
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdForneceI: TdmkEditCB
        Left = 8
        Top = 140
        Width = 41
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBForneceI
        IgnoraDBLookupComboBox = False
      end
      object RGPagRec: TRadioGroup
        Left = 440
        Top = 124
        Width = 241
        Height = 37
        Caption = ' Caracter'#237'stica: '
        Columns = 3
        ItemIndex = 1
        Items.Strings = (
          'Pagar'
          'Ambos'
          'Receber')
        TabOrder = 11
      end
      object CBForneceI: TdmkDBLookupComboBox
        Left = 52
        Top = 140
        Width = 385
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsForneceI
        TabOrder = 4
        dmkEditCB = EdForneceI
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdNome: TdmkEdit
        Left = 72
        Top = 20
        Width = 365
        Height = 21
        Color = clWhite
        MaxLength = 50
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnExit = EdNomeExit
      end
      object EdNome2: TdmkEdit
        Left = 440
        Top = 20
        Width = 241
        Height = 21
        Color = clWhite
        MaxLength = 50
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object CkAtivo: TCheckBox
        Left = 636
        Top = 164
        Width = 45
        Height = 17
        Caption = 'Ativa.'
        Checked = True
        State = cbChecked
        TabOrder = 8
      end
      object EdID: TdmkEdit
        Left = 64
        Top = 200
        Width = 189
        Height = 21
        Color = clWhite
        MaxLength = 50
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdContato1: TdmkEdit
        Left = 256
        Top = 200
        Width = 217
        Height = 21
        Color = clWhite
        MaxLength = 50
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdContab: TdmkEdit
        Left = 476
        Top = 200
        Width = 205
        Height = 21
        Color = clWhite
        MaxLength = 50
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdOrdem: TdmkEdit
        Left = 8
        Top = 200
        Width = 53
        Height = 21
        Alignment = taRightJustify
        Color = clWhite
        MaxLength = 50
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object CkForneceN: TCheckBox
        Left = 8
        Top = 164
        Width = 265
        Height = 17
        Caption = 'A carteira n'#227'o pertence ao cliente interno indicado.'
        TabOrder = 7
      end
      object CkExclusivo: TCheckBox
        Left = 308
        Top = 164
        Width = 277
        Height = 17
        Caption = 'Omitir saldo de relat'#243'rio de saldos. (carteira exclusiva)'
        TabOrder = 12
      end
      object EdEntiDent: TdmkEditCB
        Left = 8
        Top = 240
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBEntiDent
        IgnoraDBLookupComboBox = False
      end
      object CBEntiDent: TdmkDBLookupComboBox
        Left = 68
        Top = 240
        Width = 613
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENT'
        ListSource = DsEntiDent
        TabOrder = 14
        dmkEditCB = EdEntiDent
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object RGTipo: TRadioGroup
        Left = 8
        Top = 44
        Width = 673
        Height = 77
        Caption = ' Tipo de carteira: '
        Items.Strings = (
          'Caixa: dinheiro em esp'#233'cie, cheques recebidos a vista, etc.'
          
            'Banco: conta corrente banc'#225'ria fiel ao extrato com d'#233'bitos diret' +
            'os em conta, compensa'#231#227'o de cheques, etc.'
          
            'Emiss'#227'o: emiss'#227'o de cheques, recebimento de cheques a prazo, dup' +
            'licatas, bloquetos, d'#233'bitos pr'#233'-agendados, e outros itens a praz' +
            'o.')
        TabOrder = 15
        OnClick = RGTipoClick
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 1019
    Height = 646
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
    ExplicitLeft = 368
    ExplicitTop = 164
    object Splitter1: TSplitter
      Left = 0
      Top = 493
      Width = 1019
      Height = 3
      Cursor = crVSplit
      Align = alBottom
      ExplicitLeft = 549
      ExplicitTop = 0
      ExplicitWidth = 548
    end
    object Splitter2: TSplitter
      Left = 308
      Top = 0
      Height = 493
      ExplicitLeft = 388
      ExplicitTop = 244
      ExplicitHeight = 100
    end
    object PainelControle: TPanel
      Left = 0
      Top = 598
      Width = 1019
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'ltimo registro'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Pr'#243'ximo registro'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Registro anterior'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Primeiro registro'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 459
        Top = 1
        Width = 559
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Exclui'
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera'
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui'
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtIncluiClick
        end
        object Panel9: TPanel
          Left = 448
          Top = 0
          Width = 111
          Height = 46
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 8
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtUsuario: TBitBtn
          Left = 280
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Usu'#225'rios'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtUsuarioClick
        end
      end
    end
    object PnFast: TPanel
      Left = 0
      Top = 0
      Width = 308
      Height = 493
      Align = alLeft
      TabOrder = 1
      object Panel7: TPanel
        Left = 1
        Top = 1
        Width = 306
        Height = 28
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object EdCliInt: TEdit
          Left = 4
          Top = 4
          Width = 241
          Height = 21
          TabOrder = 0
          OnChange = EdCliIntChange
        end
      end
      object dmkDBGrid1: TdmkDBGrid
        Left = 1
        Top = 29
        Width = 306
        Height = 435
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'CliInt'
            Title.Caption = 'Cli.int.'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Entid.'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECLI'
            Title.Caption = 'Raz'#227'o Social / Nome'
            Width = 224
            Visible = True
          end>
        Color = clWindow
        DataSource = DsCliInt
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CliInt'
            Title.Caption = 'Cli.int.'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Entid.'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECLI'
            Title.Caption = 'Raz'#227'o Social / Nome'
            Width = 224
            Visible = True
          end>
      end
      object Panel6: TPanel
        Left = 1
        Top = 464
        Width = 306
        Height = 28
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 306
          Height = 28
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object EdCliCart: TEdit
            Left = 4
            Top = 4
            Width = 197
            Height = 21
            TabOrder = 0
            OnChange = EdCliCartChange
          end
          object CkSohAtivos: TCheckBox
            Left = 208
            Top = 8
            Width = 97
            Height = 17
            Caption = 'Somente ativos.'
            Checked = True
            State = cbChecked
            TabOrder = 1
            OnClick = CkSohAtivosClick
          end
        end
      end
    end
    object PnCartAtu: TPanel
      Left = 336
      Top = 0
      Width = 928
      Height = 485
      BevelOuter = bvNone
      TabOrder = 2
      object PageControl1: TPageControl
        Left = 0
        Top = 0
        Width = 928
        Height = 485
        ActivePage = TabSheet1
        Align = alClient
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = 'Carteiras'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel10: TPanel
            Left = 0
            Top = 0
            Width = 920
            Height = 457
            Align = alClient
            ParentBackground = False
            TabOrder = 0
            object PainelData: TPanel
              Left = 1
              Top = 1
              Width = 918
              Height = 269
              Align = alTop
              Enabled = False
              TabOrder = 0
              object Label1: TLabel
                Left = 8
                Top = 4
                Width = 36
                Height = 13
                Caption = 'C'#243'digo:'
                FocusControl = DBEdCodigo
              end
              object Label2: TLabel
                Left = 76
                Top = 4
                Width = 51
                Height = 13
                Caption = 'Descri'#231#227'o:'
                FocusControl = DBEdNome
              end
              object Label3: TLabel
                Left = 584
                Top = 4
                Width = 24
                Height = 13
                Caption = 'Tipo:'
                FocusControl = DBEdit1
              end
              object Label12: TLabel
                Left = 8
                Top = 128
                Width = 317
                Height = 13
                Caption = 
                  'Nome do terceiro ao qual a carteira est'#225' vinculada (Cliente inte' +
                  'rno):'
                FocusControl = DBEdit9
              end
              object Label19: TLabel
                Left = 336
                Top = 4
                Width = 125
                Height = 13
                Caption = 'Descri'#231#227'o para impress'#227'o:'
                FocusControl = EdDBNome
              end
              object Label29: TLabel
                Left = 328
                Top = 128
                Width = 40
                Height = 13
                Caption = 'Contato:'
                FocusControl = EdDBContato
              end
              object Label30: TLabel
                Left = 472
                Top = 128
                Width = 204
                Height = 13
                Caption = 'Identificador conta contabilidade (exportar):'
              end
              object Label22: TLabel
                Left = 436
                Top = 168
                Width = 176
                Height = 13
                Caption = 'Identificador extrato banco (Importar):'
              end
              object Label31: TLabel
                Left = 380
                Top = 168
                Width = 34
                Height = 13
                Caption = 'Ordem:'
                FocusControl = DBEdit13
              end
              object Label38: TLabel
                Left = 8
                Top = 228
                Width = 146
                Height = 13
                Caption = 'Entidade detentora da carteira:'
              end
              object DBEdCodigo: TDBEdit
                Left = 8
                Top = 20
                Width = 64
                Height = 21
                Hint = 'N'#186' do banco'
                TabStop = False
                DataField = 'Codigo'
                DataSource = DsCarteiras
                Font.Charset = DEFAULT_CHARSET
                Font.Color = 8281908
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                MaxLength = 1
                ParentFont = False
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 0
              end
              object DBEdNome: TDBEdit
                Left = 76
                Top = 20
                Width = 257
                Height = 21
                Hint = 'Nome do banco'
                Color = clWhite
                DataField = 'Nome'
                DataSource = DsCarteiras
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                MaxLength = 13
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
              end
              object DBEdit1: TDBEdit
                Left = 604
                Top = 20
                Width = 74
                Height = 21
                DataField = 'NOMETIPO'
                DataSource = DsCarteiras
                TabOrder = 2
              end
              object DBCheckBox1: TDBCheckBox
                Left = 632
                Top = 184
                Width = 49
                Height = 25
                Caption = 'Ativa.'
                DataField = 'Ativo'
                DataSource = DsCarteiras
                TabOrder = 3
                ValueChecked = '1'
                ValueUnchecked = '0'
              end
              object DBRGPagRec: TDBRadioGroup
                Left = 8
                Top = 168
                Width = 365
                Height = 37
                Caption = ' Caracter'#237'stica: '
                Columns = 3
                DataField = 'PagRec'
                DataSource = DsCarteiras
                Items.Strings = (
                  'Pagar'
                  'Ambos'
                  'Receber')
                ParentBackground = True
                TabOrder = 4
                Values.Strings = (
                  '-1'
                  '0'
                  '1')
              end
              object DBEdit9: TDBEdit
                Left = 8
                Top = 144
                Width = 317
                Height = 21
                DataField = 'NOMEFORNECEI'
                DataSource = DsCarteiras
                TabOrder = 5
              end
              object EdDBNome: TDBEdit
                Left = 336
                Top = 20
                Width = 245
                Height = 21
                Hint = 'Nome do banco'
                Color = clWhite
                DataField = 'Nome2'
                DataSource = DsCarteiras
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 6
              end
              object EdDBID: TDBEdit
                Left = 436
                Top = 184
                Width = 189
                Height = 21
                Hint = 'Nome do banco'
                Color = clWhite
                DataField = 'ID'
                DataSource = DsCarteiras
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 7
              end
              object EdDBContato: TDBEdit
                Left = 328
                Top = 144
                Width = 141
                Height = 21
                Hint = 'Nome do banco'
                Color = clWhite
                DataField = 'Contato1'
                DataSource = DsCarteiras
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 8
              end
              object EdDBContab: TDBEdit
                Left = 472
                Top = 144
                Width = 205
                Height = 21
                Hint = 'Nome do banco'
                Color = clWhite
                DataField = 'Contab'
                DataSource = DsCarteiras
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 9
              end
              object DBEdit13: TDBEdit
                Left = 380
                Top = 184
                Width = 53
                Height = 21
                DataField = 'Ordem'
                DataSource = DsCarteiras
                TabOrder = 10
              end
              object DBCheckBox5: TDBCheckBox
                Left = 8
                Top = 208
                Width = 277
                Height = 17
                Caption = 'Omitir saldo de relat'#243'rio de saldos. (carteira exclusiva)'
                DataField = 'Exclusivo'
                DataSource = DsCarteiras
                TabOrder = 11
                ValueChecked = '1'
                ValueUnchecked = '0'
              end
              object DBEdit14: TDBEdit
                Left = 584
                Top = 20
                Width = 21
                Height = 21
                DataField = 'Tipo'
                DataSource = DsCarteiras
                TabOrder = 12
              end
              object DBEdit15: TDBEdit
                Left = 8
                Top = 244
                Width = 669
                Height = 21
                DataField = 'NOMEENTIDENT'
                DataSource = DsCarteiras
                TabOrder = 13
              end
              object DBRGTipo: TDBRadioGroup
                Left = 5
                Top = 44
                Width = 673
                Height = 77
                Caption = ' Tipo de carteira: '
                DataField = 'Tipo'
                DataSource = DsCarteiras
                Items.Strings = (
                  'Caixa: dinheiro em esp'#233'cie, cheques recebidos a vista, etc.'
                  
                    'Banco: conta corrente banc'#225'ria fiel ao extrato com d'#233'bitos diret' +
                    'os em conta, compensa'#231#227'o de cheques, etc.'
                  
                    'Emiss'#227'o: emiss'#227'o de cheques, recebimento de cheques a prazo, dup' +
                    'licatas, bloquetos, d'#233'bitos pr'#233'-agendados, e outros itens a praz' +
                    'o.')
                ParentBackground = True
                TabOrder = 14
                Values.Strings = (
                  '0'
                  '1'
                  '2')
                OnChange = DBRGTipoChange
                OnClick = DBRGTipoClick
              end
            end
            object PnTipoShow: TPanel
              Left = 1
              Top = 284
              Width = 918
              Height = 172
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 1
              object PnCaixShow: TPanel
                Left = 0
                Top = 0
                Width = 918
                Height = 172
                Align = alClient
                TabOrder = 0
                Visible = False
                object Label7: TLabel
                  Left = 8
                  Top = 4
                  Width = 59
                  Height = 13
                  Caption = 'Saldo inicial:'
                  FocusControl = DBEdit6
                end
                object Label8: TLabel
                  Left = 92
                  Top = 4
                  Width = 75
                  Height = 13
                  Caption = 'Saldo em caixa:'
                  FocusControl = DBEdit7
                end
                object DBEdit6: TDBEdit
                  Left = 8
                  Top = 20
                  Width = 80
                  Height = 21
                  DataField = 'Inicial'
                  DataSource = DsCarteiras
                  TabOrder = 0
                end
                object DBEdit7: TDBEdit
                  Left = 92
                  Top = 20
                  Width = 80
                  Height = 21
                  DataField = 'EmCaixa'
                  DataSource = DsCarteiras
                  TabOrder = 1
                end
                object DBCheckBox3: TDBCheckBox
                  Left = 180
                  Top = 24
                  Width = 97
                  Height = 17
                  Caption = 'Permite prazo.'
                  DataField = 'Prazo'
                  DataSource = DsCarteiras
                  TabOrder = 2
                  ValueChecked = '1'
                  ValueUnchecked = '0'
                end
                object DBCheckBox6: TDBCheckBox
                  Left = 284
                  Top = 24
                  Width = 193
                  Height = 17
                  Caption = 'Recebe pagamentos de bloquetos.'
                  DataField = 'RecebeBloq'
                  DataSource = DsCarteiras
                  TabOrder = 3
                  ValueChecked = '1'
                  ValueUnchecked = '0'
                  Visible = False
                end
              end
              object PnBancShow: TPanel
                Left = 0
                Top = 0
                Width = 918
                Height = 172
                Align = alClient
                TabOrder = 1
                Visible = False
                object Label4: TLabel
                  Left = 8
                  Top = 4
                  Width = 59
                  Height = 13
                  Caption = 'Saldo inicial:'
                  FocusControl = DBEdit2
                end
                object Label13: TLabel
                  Left = 92
                  Top = 4
                  Width = 34
                  Height = 13
                  Caption = 'Banco:'
                  FocusControl = DBEdit10
                end
                object Label17: TLabel
                  Left = 160
                  Top = 4
                  Width = 42
                  Height = 13
                  Caption = 'Ag'#234'ncia:'
                  FocusControl = DBEdit11
                end
                object Label18: TLabel
                  Left = 228
                  Top = 4
                  Width = 73
                  Height = 13
                  Caption = 'Conta corrente:'
                  FocusControl = DBEdit12
                end
                object Label39: TLabel
                  Left = 324
                  Top = 4
                  Width = 104
                  Height = 13
                  Caption = 'C'#243'digo retorno CNAB:'
                  FocusControl = DBEdit16
                end
                object DBEdit2: TDBEdit
                  Left = 8
                  Top = 20
                  Width = 80
                  Height = 21
                  DataField = 'Inicial'
                  DataSource = DsCarteiras
                  TabOrder = 0
                end
                object DBEdit10: TDBEdit
                  Left = 92
                  Top = 20
                  Width = 64
                  Height = 21
                  DataField = 'Banco1'
                  DataSource = DsCarteiras
                  TabOrder = 1
                end
                object DBEdit11: TDBEdit
                  Left = 160
                  Top = 20
                  Width = 64
                  Height = 21
                  DataField = 'Agencia1'
                  DataSource = DsCarteiras
                  TabOrder = 2
                end
                object DBEdit12: TDBEdit
                  Left = 228
                  Top = 20
                  Width = 94
                  Height = 21
                  DataField = 'Conta1'
                  DataSource = DsCarteiras
                  TabOrder = 3
                end
                object DBEdit16: TDBEdit
                  Left = 324
                  Top = 20
                  Width = 160
                  Height = 21
                  DataField = 'CodCedente'
                  DataSource = DsCarteiras
                  TabOrder = 4
                end
              end
              object PnEmisShow: TPanel
                Left = 0
                Top = 0
                Width = 918
                Height = 172
                Align = alClient
                Caption = #39#39#39#39
                TabOrder = 2
                Visible = False
                object Label5: TLabel
                  Left = 8
                  Top = 4
                  Width = 239
                  Height = 13
                  Caption = 'Nome do banco ao qual a emiss'#227'o est'#225' vinculada:'
                  FocusControl = DBEdit3
                end
                object DBEdit3: TDBEdit
                  Left = 8
                  Top = 20
                  Width = 669
                  Height = 21
                  DataField = 'NOMEDOBANCO'
                  DataSource = DsCarteiras
                  TabOrder = 0
                end
                object GroupBox1: TGroupBox
                  Left = 8
                  Top = 44
                  Width = 669
                  Height = 60
                  Caption = '     '
                  TabOrder = 1
                  object LaIDFat: TLabel
                    Left = 7
                    Top = 16
                    Width = 238
                    Height = 13
                    Caption = 'Identificador autom'#225'tico de fatura em importa'#231#245'es:'
                  end
                  object Label6: TLabel
                    Left = 344
                    Top = 16
                    Width = 157
                    Height = 13
                    Caption = 'Fechamento da fatura (dd antes):'
                  end
                  object Label11: TLabel
                    Left = 504
                    Top = 16
                    Width = 92
                    Height = 13
                    Caption = 'Dia do vencimento:'
                  end
                  object DBEdit4: TDBEdit
                    Left = 8
                    Top = 32
                    Width = 333
                    Height = 21
                    DataField = 'ID_Fat'
                    DataSource = DsCarteiras
                    TabOrder = 0
                  end
                  object DBEdit5: TDBEdit
                    Left = 344
                    Top = 32
                    Width = 157
                    Height = 21
                    DataField = 'Fechamento'
                    DataSource = DsCarteiras
                    TabOrder = 1
                  end
                  object DBEdit8: TDBEdit
                    Left = 504
                    Top = 32
                    Width = 157
                    Height = 21
                    DataField = 'DiaMesVence'
                    DataSource = DsCarteiras
                    TabOrder = 2
                  end
                end
                object DBCheckBox2: TDBCheckBox
                  Left = 20
                  Top = 44
                  Width = 97
                  Height = 17
                  Caption = 'Emite fatura: '
                  DataField = 'Fatura'
                  DataSource = DsCarteiras
                  TabOrder = 2
                  ValueChecked = '1'
                  ValueUnchecked = '0'
                end
                object DBCheckBox4: TDBCheckBox
                  Left = 8
                  Top = 108
                  Width = 225
                  Height = 17
                  Caption = 'Exige n'#250'mero de cheque em lan'#231'amentos.'
                  DataField = 'ExigeNumCheque'
                  DataSource = DsCarteiras
                  TabOrder = 3
                  ValueChecked = '1'
                  ValueUnchecked = '0'
                end
                object DBRGTipoDoc: TDBRadioGroup
                  Left = 8
                  Top = 128
                  Width = 668
                  Height = 35
                  Caption = ' Tipo de Documento: '
                  Columns = 9
                  DataField = 'TipoDoc'
                  DataSource = DsCarteiras
                  Items.Strings = (
                    'Outros'
                    'Cheque'
                    'DOC'
                    'TED'
                    'Esp'#233'cie'
                    'Bloqueto'
                    'Duplicata'
                    'TEF'
                    'D'#233'bito c/c')
                  ParentBackground = True
                  TabOrder = 4
                  Values.Strings = (
                    '0'
                    '1'
                    '2'
                    '3'
                    '4'
                    '5'
                    '6'
                    '7'
                    '8'
                    '9'
                    '10'
                    '11'
                    '12'
                    '13'
                    '14'
                    '15'
                    '16')
                end
                object DBCheckBox7: TDBCheckBox
                  Left = 236
                  Top = 108
                  Width = 265
                  Height = 17
                  Caption = 'Ignorar s'#233'rie do cheque na concilia'#231#227'o banc'#225'ria.'
                  DataField = 'IgnorSerie'
                  DataSource = DsCarteiras
                  TabOrder = 5
                  ValueChecked = '1'
                  ValueUnchecked = '0'
                end
                object DBCheckBox8: TDBCheckBox
                  Left = 500
                  Top = 108
                  Width = 265
                  Height = 17
                  Caption = 'Ignorar carteira na concilia'#231#227'o banc'#225'ria'
                  DataField = 'NotConcBco'
                  DataSource = DsCarteiras
                  TabOrder = 6
                  ValueChecked = '1'
                  ValueUnchecked = '0'
                end
              end
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Usu'#225'rios'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGFunci: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 920
            Height = 457
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Usuario'
                Title.Caption = 'Usu'#225'rio'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Login'
                Width = 105
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Funcionario'
                Title.Caption = 'Funcion'#225'rio'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEUSU'
                Title.Caption = 'Nome do Funcion'#225'rio'
                Width = 514
                Visible = True
              end>
            Color = clWindow
            DataSource = DsCarteirasU
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Usuario'
                Title.Caption = 'Usu'#225'rio'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Login'
                Width = 105
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Funcionario'
                Title.Caption = 'Funcion'#225'rio'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEUSU'
                Title.Caption = 'Nome do Funcion'#225'rio'
                Width = 514
                Visible = True
              end>
          end
        end
      end
    end
    object Panel2: TPanel
      Left = 0
      Top = 496
      Width = 1019
      Height = 102
      Align = alBottom
      TabOrder = 3
      object dmkDBGrid2: TdmkDBGrid
        Left = 1
        Top = 1
        Width = 1017
        Height = 100
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'NOME_TIPO'
            Title.Caption = 'Tipo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Banco1'
            Title.Caption = 'Bco'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Agencia1'
            Title.Caption = 'Ag'#234'nc.'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Conta1'
            Title.Caption = 'Conta corrente'
            Width = 77
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'C'#243'digo'
            Width = 47
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Descri'#231#227'o carteira'
            Width = 310
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEBCO'
            Title.Caption = 'Banco atrelado'
            Width = 251
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Inicial'
            Title.Caption = 'Saldo inicial'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Saldo'
            Title.Caption = 'Saldo atual'
            Width = 72
            Visible = True
          end>
        Color = clWindow
        DataSource = DsCliCart
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'NOME_TIPO'
            Title.Caption = 'Tipo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Banco1'
            Title.Caption = 'Bco'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Agencia1'
            Title.Caption = 'Ag'#234'nc.'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Conta1'
            Title.Caption = 'Conta corrente'
            Width = 77
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'C'#243'digo'
            Width = 47
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Descri'#231#227'o carteira'
            Width = 310
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEBCO'
            Title.Caption = 'Banco atrelado'
            Width = 251
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Inicial'
            Title.Caption = 'Saldo inicial'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Saldo'
            Title.Caption = 'Saldo atual'
            Width = 72
            Visible = True
          end>
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1019
    Height = 48
    Align = alTop
    Caption = '                              Carteiras'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object LaTipo: TLabel
      Left = 936
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 710
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 376
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 408
    Top = 5
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCarteirasBeforeOpen
    AfterOpen = QrCarteirasAfterOpen
    AfterScroll = QrCarteirasAfterScroll
    OnCalcFields = QrCarteirasCalcFields
    SQL.Strings = (
      'SELECT DISTINCT ca.*, ba.Nome NOMEDOBANCO,'
      'CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMEFORNECEI, eg.Nome NOMEENTIDENT '
      'FROM carteiras ca'
      'LEFT JOIN carteiras ba ON ba.Codigo=ca.Banco'
      'LEFT JOIN entidades en ON en.Codigo=ca.ForneceI'
      'LEFT JOIN equigru eg ON eg.Codigo=ca.EntiDent')
    Left = 380
    Top = 5
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'carteiras.Codigo'
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
      Origin = 'carteiras.Tipo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrCarteirasInicial: TFloatField
      FieldName = 'Inicial'
      Origin = 'carteiras.Inicial'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCarteirasBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'carteiras.Banco'
    end
    object QrCarteirasID: TWideStringField
      FieldName = 'ID'
      Origin = 'carteiras.ID'
      Size = 100
    end
    object QrCarteirasFatura: TWideStringField
      FieldName = 'Fatura'
      Origin = 'carteiras.Fatura'
      Size = 1
    end
    object QrCarteirasID_Fat: TWideStringField
      FieldName = 'ID_Fat'
      Origin = 'carteiras.ID_Fat'
      Size = 50
    end
    object QrCarteirasSaldo: TFloatField
      FieldName = 'Saldo'
      Origin = 'carteiras.Saldo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCarteirasEmCaixa: TFloatField
      FieldName = 'EmCaixa'
      Origin = 'carteiras.EmCaixa'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCarteirasFechamento: TIntegerField
      FieldName = 'Fechamento'
      Origin = 'carteiras.Fechamento'
    end
    object QrCarteirasPrazo: TSmallintField
      FieldName = 'Prazo'
      Origin = 'carteiras.Prazo'
    end
    object QrCarteirasPagRec: TIntegerField
      FieldName = 'PagRec'
      Origin = 'carteiras.PagRec'
    end
    object QrCarteirasDiaMesVence: TSmallintField
      FieldName = 'DiaMesVence'
      Origin = 'carteiras.DiaMesVence'
    end
    object QrCarteirasExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
      Origin = 'carteiras.ExigeNumCheque'
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
      Origin = 'carteiras.ForneceI'
    end
    object QrCarteirasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'carteiras.Lk'
    end
    object QrCarteirasDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'carteiras.DataCad'
    end
    object QrCarteirasDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'carteiras.DataAlt'
    end
    object QrCarteirasUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'carteiras.UserCad'
    end
    object QrCarteirasUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'carteiras.UserAlt'
    end
    object QrCarteirasNome2: TWideStringField
      FieldName = 'Nome2'
      Origin = 'carteiras.Nome2'
      Size = 100
    end
    object QrCarteirasTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
      Origin = 'carteiras.TipoDoc'
    end
    object QrCarteirasBanco1: TIntegerField
      FieldName = 'Banco1'
      Origin = 'carteiras.Banco1'
    end
    object QrCarteirasAgencia1: TIntegerField
      FieldName = 'Agencia1'
      Origin = 'carteiras.Agencia1'
    end
    object QrCarteirasConta1: TWideStringField
      FieldName = 'Conta1'
      Origin = 'carteiras.Conta1'
      Size = 15
    end
    object QrCarteirasCheque1: TIntegerField
      FieldName = 'Cheque1'
      Origin = 'carteiras.Cheque1'
    end
    object QrCarteirasContato1: TWideStringField
      FieldName = 'Contato1'
      Origin = 'carteiras.Contato1'
      Size = 100
    end
    object QrCarteirasNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 15
      Calculated = True
    end
    object QrCarteirasNOMEDOBANCO: TWideStringField
      FieldName = 'NOMEDOBANCO'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrCarteirasNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Origin = 'NOMEFORNECEI'
      Size = 100
    end
    object QrCarteirasAntigo: TWideStringField
      FieldName = 'Antigo'
      Origin = 'carteiras.Antigo'
    end
    object QrCarteirasContab: TWideStringField
      FieldName = 'Contab'
      Origin = 'carteiras.Contab'
    end
    object QrCarteirasOrdem: TIntegerField
      FieldName = 'Ordem'
      Origin = 'carteiras.Ordem'
      Required = True
    end
    object QrCarteirasForneceN: TSmallintField
      FieldName = 'ForneceN'
      Origin = 'carteiras.ForneceN'
      Required = True
    end
    object QrCarteirasFuturoC: TFloatField
      FieldName = 'FuturoC'
      Origin = 'carteiras.FuturoC'
    end
    object QrCarteirasFuturoD: TFloatField
      FieldName = 'FuturoD'
      Origin = 'carteiras.FuturoD'
    end
    object QrCarteirasFuturoS: TFloatField
      FieldName = 'FuturoS'
      Origin = 'carteiras.FuturoS'
    end
    object QrCarteirasExclusivo: TSmallintField
      FieldName = 'Exclusivo'
      Origin = 'carteiras.Exclusivo'
      Required = True
    end
    object QrCarteirasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'carteiras.AlterWeb'
      Required = True
    end
    object QrCarteirasAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'carteiras.Ativo'
      Required = True
    end
    object QrCarteirasRecebeBloq: TSmallintField
      FieldName = 'RecebeBloq'
      Origin = 'carteiras.RecebeBloq'
    end
    object QrCarteirasEntiDent: TIntegerField
      FieldName = 'EntiDent'
      Origin = 'carteiras.EntiDent'
    end
    object QrCarteirasNOMEENTIDENT: TWideStringField
      FieldName = 'NOMEENTIDENT'
      Size = 100
    end
    object QrCarteirasCodCedente: TWideStringField
      FieldName = 'CodCedente'
    end
    object QrCarteirasIgnorSerie: TSmallintField
      FieldName = 'IgnorSerie'
    end
    object QrCarteirasNotConcBco: TSmallintField
      FieldName = 'NotConcBco'
    end
  end
  object PMCarteira: TPopupMenu
    Left = 116
    Top = 220
    object Caixa1: TMenuItem
      Caption = '&Caixa (Dinheiro em esp'#233'cie)'
      OnClick = Caixa1Click
    end
    object Banco1: TMenuItem
      Caption = '&Banco (Conta banc'#225'ria)'
      OnClick = Banco1Click
    end
    object Emisso1: TMenuItem
      Caption = '&Emiss'#227'o (Cheque, duplicata, etc.)'
      OnClick = Emisso1Click
    end
  end
  object QrBancos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM carteiras'
      'WHERE Tipo=1'
      'ORDER BY Nome')
    Left = 192
    Top = 52
    object QrBancosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancosNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsBancos: TDataSource
    DataSet = QrBancos
    Left = 220
    Top = 52
  end
  object QrForneceI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NomeENTIDADE')
    Left = 88
    Top = 180
    object QrForneceICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceINOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsForneceI: TDataSource
    DataSet = QrForneceI
    Left = 116
    Top = 180
  end
  object QrEntiDent: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 88
    Top = 260
    object QrEntiDentCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiDentNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsEntiDent: TDataSource
    DataSet = QrEntiDent
    Left = 116
    Top = 260
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = DBCheckBox6
    CanUpd01 = DBCheckBox3
    CanDel01 = DBEdit7
    Left = 260
    Top = 12
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCliIntAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT ent.Codigo, ent.CliInt,'
      'IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMECLI'
      'FROM entidades ent'
      'LEFT JOIN carteiras car ON car.ForneceI=ent.Codigo'
      'WHERE ent.CliInt<>0'
      'OR car.ForneceI <> 0')
    Left = 88
    Top = 144
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Required = True
      Size = 100
    end
    object QrCliIntCliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 116
    Top = 144
  end
  object QrCliCart: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCliCartAfterScroll
    SQL.Strings = (
      'SELECT car.Codigo, car.Tipo, car.Nome,'
      'ELT(car.Tipo+1,"Caixa","C/C Banco",'
      '"Emiss'#227'o") NOME_TIPO, '
      'IF(car.Banco=0,"",bco.Nome) NOMEBCO,'
      'car.Banco1, car.Agencia1, car.Conta1,'
      'car.Inicial, car.Saldo'
      'FROM carteiras car'
      'LEFT JOIN carteiras bco ON bco.Codigo=car.Banco'
      'WHERE car.ForneceI=1')
    Left = 496
    Top = 4
    object QrCliCartCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCliCartTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrCliCartNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrCliCartNOME_TIPO: TWideStringField
      FieldName = 'NOME_TIPO'
      Size = 9
    end
    object QrCliCartNOMEBCO: TWideStringField
      FieldName = 'NOMEBCO'
      Size = 100
    end
    object QrCliCartBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrCliCartAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrCliCartConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrCliCartInicial: TFloatField
      FieldName = 'Inicial'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCliCartSaldo: TFloatField
      FieldName = 'Saldo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsCliCart: TDataSource
    DataSet = QrCliCart
    Left = 524
    Top = 4
  end
  object PMUsuario: TPopupMenu
    Left = 754
    Top = 606
    object Adiciona1: TMenuItem
      Caption = '&Adiciona'
      OnClick = Adiciona1Click
    end
    object Retira1: TMenuItem
      Caption = '&Retira'
      OnClick = Retira1Click
    end
  end
  object QrCarteirasU: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cpu.Controle, cpu.Usuario, '
      'sen.Login, sen.Funcionario, '
      'IF(ent.Tipo=0, RazaoSocial, Nome) NOMEUSU'
      'FROM carteirasu cpu'
      'LEFT JOIN senhas sen ON cpu.Usuario=sen.Numero'
      'LEFT JOIN entidades ent ON ent.Codigo=sen.Funcionario'
      'WHERE cpu.Codigo=:P0')
    Left = 436
    Top = 5
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCarteirasUControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCarteirasUUsuario: TIntegerField
      FieldName = 'Usuario'
      Required = True
    end
    object QrCarteirasULogin: TWideStringField
      FieldName = 'Login'
      Required = True
      Size = 30
    end
    object QrCarteirasUFuncionario: TIntegerField
      FieldName = 'Funcionario'
    end
    object QrCarteirasUNOMEUSU: TWideStringField
      FieldName = 'NOMEUSU'
      Size = 100
    end
  end
  object DsCarteirasU: TDataSource
    DataSet = QrCarteirasU
    Left = 464
    Top = 5
  end
  object QrLan: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT CliInt'
      'FROM lanctos'
      'WHERE CliInt <> 0'
      'AND Carteira=20')
    Left = 88
    Top = 220
    object QrLanCliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
end
