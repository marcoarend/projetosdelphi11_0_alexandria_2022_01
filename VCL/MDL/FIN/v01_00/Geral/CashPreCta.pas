unit CashPreCta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, frxClass, frxDBSet,
  DB, mySQLDbTables, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkCheckBox, ComCtrls, dmkDBGridDAC, Mask, UnDmkEnums;

type
  TFmCashPreCta = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrEmp: TmySQLQuery;
    QrEmpCodigo: TIntegerField;
    QrEmpNO_UF: TWideStringField;
    QrEmpNO_EMPRESA: TWideStringField;
    QrEmpCIDADE: TWideStringField;
    QrEmpCO_SHOW: TLargeintField;
    QrEmpCliInt: TIntegerField;
    DsEmp: TDataSource;
    frxDsEmp: TfrxDBDataset;
    Panel6: TPanel;
    CkNaoAgruparNada: TCheckBox;
    GroupBox4: TGroupBox;
    CkAcordos: TCheckBox;
    CkPeriodos: TCheckBox;
    CkTextos: TCheckBox;
    Panel3: TPanel;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    LaAno: TLabel;
    LaMes: TLabel;
    CBMesIni: TComboBox;
    CBAnoIni: TComboBox;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    CBMesFim: TComboBox;
    CBAnoFim: TComboBox;
    QrExclusivos: TmySQLQuery;
    QrExclusivosMovim: TFloatField;
    QrExclusivosCREDITO: TFloatField;
    QrExclusivosDEBITO: TFloatField;
    QrExclusivosANTERIOR: TFloatField;
    frxDsExclusivos: TfrxDBDataset;
    QrOrdinarios: TmySQLQuery;
    QrOrdinariosMovim: TFloatField;
    QrOrdinariosCREDITO: TFloatField;
    QrOrdinariosDEBITO: TFloatField;
    QrOrdinariosANTERIOR: TFloatField;
    frxDsOrdinarios: TfrxDBDataset;
    QrOrdiMov: TmySQLQuery;
    QrOrdiMovCredito: TFloatField;
    QrOrdiMovDebito: TFloatField;
    QrExclMov: TmySQLQuery;
    QrExclMovCredito: TFloatField;
    QrExclMovDebito: TFloatField;
    frxPreCta: TfrxReport;
    Panel5: TPanel;
    LaAviso: TLabel;
    QrCtasNiv: TmySQLQuery;
    DsCtasNiv: TDataSource;
    QrCtasNivNOMENIVEL: TWideStringField;
    QrCtasNivNOMEGENERO: TWideStringField;
    QrCtasNivNivel: TIntegerField;
    QrCtasNivGenero: TIntegerField;
    QrAnt: TmySQLQuery;
    QrMov: TmySQLQuery;
    QrAntSDOANT: TFloatField;
    QrMovCredito: TFloatField;
    QrMovDebito: TFloatField;
    QrSdoNivCtas: TmySQLQuery;
    frxDsSdoNivCtas: TfrxDBDataset;
    QrSdoNivCtasNomeNivel: TWideStringField;
    QrSdoNivCtasNivel: TIntegerField;
    QrSdoNivCtasNomeGenero: TWideStringField;
    QrSdoNivCtasGenero: TIntegerField;
    QrSdoNivCtasSdoIni: TFloatField;
    QrSdoNivCtasMovCre: TFloatField;
    QrSdoNivCtasMovDeb: TFloatField;
    QrSdoNivCtasSdoFim: TFloatField;
    QrSdoNivCtasAtivo: TIntegerField;
    QrSTCPa: TmySQLQuery;
    frxDsSTCPa: TfrxDBDataset;
    QrSTCPd: TmySQLQuery;
    frxDsSTCPd: TfrxDBDataset;
    QrSTCPf: TmySQLQuery;
    frxDsSTCPf: TfrxDBDataset;
    QrSTCPdSUMCRE: TFloatField;
    QrSTCPdSUMDEB: TFloatField;
    QrSTCPfSDOFIM: TFloatField;
    QrSTCPaSDOANT: TFloatField;
    QrSTCPdSUMMOV: TFloatField;
    QrInad: TmySQLQuery;
    QrInadCREDITO: TFloatField;
    frxDsInad: TfrxDBDataset;
    CkPaginar: TdmkCheckBox;
    Label4: TLabel;
    Label5: TLabel;
    EdReceVencer: TdmkEdit;
    QrCtasNivAtivo: TSmallintField;
    QrFutDeb: TmySQLQuery;
    QrCNS: TmySQLQuery;
    QrCNSNivel: TIntegerField;
    QrCNSGenero: TIntegerField;
    QrCNA: TmySQLQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    DsFutDeb: TDataSource;
    QrFutDebData: TDateField;
    QrFutDebControle: TIntegerField;
    QrFutDebGenero: TIntegerField;
    QrFutDebDescricao: TWideStringField;
    QrFutDebNotaFiscal: TIntegerField;
    QrFutDebDebito: TFloatField;
    QrFutDebVencimento: TDateField;
    QrFutDebCarteira: TIntegerField;
    QrFutDebDocumento: TFloatField;
    QrFutDebFornecedor: TIntegerField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label6: TLabel;
    DBGContasNiv: TdmkDBGridDAC;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    Panel4: TPanel;
    QrSumDeb: TmySQLQuery;
    DsSumDeb: TDataSource;
    QrSumDebDEBITO: TFloatField;
    QrArreBaI: TmySQLQuery;
    QrArreBaIValor: TFloatField;
    QrArreBaIPercent: TFloatField;
    QrArreBaIFator: TSmallintField;
    TabSheet3: TTabSheet;
    DBGrid2: TDBGrid;
    DsArreBaI: TDataSource;
    QrArreBaIParcelasRestantes: TLargeintField;
    QrArreBaIDoQue: TSmallintField;
    QrArreBaIValFracAsk: TSmallintField;
    QrArreBaITexto: TWideStringField;
    QrArreBaIValFracDef: TFloatField;
    QrArreBaINome: TWideStringField;
    QrArreBaIVAL_RESTANTE: TFloatField;
    EdDebiVencer: TdmkEdit;
    BtImprime: TBitBtn;
    QrPagantes: TmySQLQuery;
    QrPagantesITENS: TLargeintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrExclusivosCalcFields(DataSet: TDataSet);
    procedure QrOrdinariosCalcFields(DataSet: TDataSet);
    procedure frxPreCtaGetValue(const VarName: string; var Value: Variant);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdEmpresaExit(Sender: TObject);
    procedure QrArreBaICalcFields(DataSet: TDataSet);
    procedure CBMesIniChange(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
  private
    { Private declarations }
    FPeriodoIni, FPeriodoFim, F_CliInt(*, FGenero, FUH*): Integer;
    F_CliInt_TXT, FCtasNiv, FSdoNivCtas, FDataI1, FDataF1, FDataD1(*, FDataI2, FDataF2*): String;
    procedure DemostrativoDeReceitasEDespesas();
    procedure ReopenNiveisCtas();
    procedure SQLDebitosFuturos(Query: TmySQLQuery; SQL_Zero: String);
    function ValorArrecadacaoFutura(): Double;
    procedure FechaPesquisa();
  public
    { Public declarations }
  end;

  var
  FmCashPreCta: TFmCashPreCta;

implementation

uses UnMyObjects, ModuleGeral, Module, ModuleFin, UnInternalConsts, dmkGeral,
UCreate, UMySQLModule, GetValor;

{$R *.DFM}

procedure TFmCashPreCta.BtImprimeClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxPreCta, 'Presta��o de contas');
end;

procedure TFmCashPreCta.BtOKClick(Sender: TObject);
var
  MezIni, AnoIni, MesIni, MezFim, AnoFim, MesFim: Integer;
  Tab: String;
  //
  NomeNivel, NomeGenero, PeriF_TXT: String;
  Nivel, Genero: Integer;
  //DebiVencer,
  SdoIni, MovCre, MovDeb, SdoFim, ReceVencer: Double;
begin
  if EdEmpresa.ValueVariant = 0 then
  begin
    Application.MessageBox('Informe o cliente interno!',
      'Aviso', MB_OK+MB_ICONWARNING);
    EdEmpresa.SetFocus;
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    FPeriodoIni := ((Geral.IMV(CBAnoIni.Text)-2000) * 12) + CBMesIni.ItemIndex + 1;
    FPeriodoFim := ((Geral.IMV(CBAnoFim.Text)-2000) * 12) + CBMesFim.ItemIndex + 1;
    PeriF_TXT   := FormatFloat('0', FPeriodoFim);
    //
    F_CliInt := QrEmpCodigo.Value;
    F_CliInt_TXT := FormatFloat('0', F_CliInt);
    FDataI1 := Geral.FDT(MLAGeral.PrimeiroDiaDoPeriodo_Date(FPeriodoIni), 1);
    FDataF1 := Geral.FDT(MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoFim), 1);
    FDataD1 := Geral.FDT(MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoFim)+1, 1);
    AnoIni := Geral.IMV(CBAnoIni.Text);
    MesIni := CBMesIni.ItemIndex + 1;
    MezIni := ((AnoIni - 2000) * 100) + MesIni;
    //
    AnoFim := Geral.IMV(CBAnoFim.Text);
    MesFim := CBMesFim.ItemIndex + 1;
    MezFim := ((AnoFim - 2000) * 100) + MesFim;
    //
    DmodFin.AtualizaContasHistSdo3(QrEmpCodigo.Value, LaAviso);
    Screen.Cursor := crHourGlass;
    Application.ProcessMessages;
    //
    //
    // Colocar no DModFin ???
    FSdoNivCtas := UCriar.RecriaTempTable('SdoNivCtas', DModG.QrUpdPID1, False);
    {
    QrContasNiv.Close;
    QrContasNiv.Params[0].AsInteger := F_CliInt;
    QrContasNiv.Open;
    }
    QrCtasNiv.First;
    while not QrCtasNiv.Eof do
    begin
      case QrCtasNivNivel.Value of
        1: Tab := 'Genero';
        2: Tab := 'SubGrupo';
        3: Tab := 'Grupo';
        4: Tab := 'Conjunto';
        5: Tab := 'Plano';
        else Tab := '???';
      end;
      QrAnt.Close;
      QrAnt.SQL.Clear;
      QrAnt.SQL.Add('SELECT SUM(Movim) SDOANT');
      QrAnt.SQL.Add('FROM contasmov');
      QrAnt.SQL.Add('WHERE CliInt=:P0');
      QrAnt.SQL.Add('AND Mez<:P1');
      QrAnt.SQL.Add('AND '+Tab+'=:P2');
      QrAnt.SQL.Add('GROUP BY '+Tab);
  {
  SELECT Conjunto, SUM(Movim) SDOANT
  FROM contasmov
  WHERE CliInt=1751
  AND Mez<1004
  AND Conjunto=1
  GROUP BY Conjunto
  }
      QrAnt.Params[00].AsInteger := F_CliInt;
      QrAnt.Params[01].AsInteger := MezIni;
      QrAnt.Params[02].AsInteger := QrCtasNivGenero.Value;
      QrAnt.Open;
      //
      QrMov.Close;
      QrMov.SQL.Clear;
      QrMov.SQL.Add('SELECT SUM(Credito) Credito, SUM(Debito) Debito ');
      QrMov.SQL.Add('FROM contasmov');
      QrMov.SQL.Add('WHERE CliInt=:P0');
      QrMov.SQL.Add('AND Mez BETWEEN :P1 AND :P2');
      QrMov.SQL.Add('AND '+Tab+'=:P3');
      QrMov.SQL.Add('GROUP BY '+Tab);
      QrMov.Params[00].AsInteger := F_CliInt;
      QrMov.Params[01].AsInteger := MezIni;
      QrMov.Params[02].AsInteger := MezFim;
      QrMov.Params[03].AsInteger := QrCtasNivGenero.Value;
      QrMov.Open;
      //
      NomeNivel   := QrCtasNivNOMENIVEL.Value;
      Nivel       := QrCtasNivNivel.Value;
      NomeGenero  := QrCtasNivNOMEGENERO.Value;
      Genero      := QrCtasNivGenero.Value;
      SdoIni      := QrAntSDOANT.Value;
      MovCre      := QrMovCredito.Value;
      MovDeb      := QrMovDebito.Value;
      SdoFim      := QrAntSDOANT.Value + QrMovCredito.Value - QrMovDebito.Value;
      //
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, 'sdonivctas', False, [
      'NomeNivel', 'Nivel', 'NomeGenero',
      'Genero', 'SdoIni', 'MovCre',
      'MovDeb', 'SdoFim'], [
      ], [
      NomeNivel, Nivel, NomeGenero,
      Genero, SdoIni, MovCre,
      MovDeb, SdoFim], [
      ], False);
      //
      QrCtasNiv.Next;
    end;
    //
    QrSdoNivCtas.Close;
    QrSdoNivCtas.Database := DModG.MyPID_DB;
    QrSdoNivCtas.Open;
    // informar o saldo total mesmo que n�o informe os saldos dos n�veis
    DModFin.QrSTCP.Close;
    DModFin.QrSTCP.Params[0].AsInteger := F_CliInt;
    DModFin.QrSTCP.Params[1].AsInteger := FPeriodoIni;
    DModFin.QrSTCP.Open;
    //
    // SOMAS DE VALORES DE TODAS CONTAS
    // saldo anterior
    QrSTCPa.Close;
    QrSTCPa.Params[00].AsInteger := F_CliInt;
    QrSTCPa.Params[01].AsInteger := FPeriodoIni;
    QrSTCPa.Open;
    // Movimento durante o per�odo selecionado
    QrSTCPd.Close;
    QrSTCPd.Params[00].AsInteger := F_CliInt;
    QrSTCPd.Params[01].AsInteger := FPeriodoIni;
    QrSTCPd.Params[02].AsInteger := FPeriodoFim;
    QrSTCPd.Open;
    // saldo final
    QrSTCPf.Close;
    QrSTCPf.Params[00].AsInteger := F_CliInt;
    QrSTCPf.Params[01].AsInteger := FPeriodoFim;
    QrSTCPf.Open;
    // FIM SOMAS DE VALORES DE TODAS CONTAS
    //
    // Inadimpl�ncia
    QrInad.Close;
    QrInad.Params[00].AsInteger := F_CliInt;
    QrInad.Params[01].AsString  := FDataD1;
    QrInad.Params[02].AsString  := FDataD1;
    QrInad.Open;
    // Fim inadimpl�ncia
    //
    QrFutDeb.Close;
    QrFutDeb.SQL.Clear;
    QrFutDeb.SQL.Add('SELECT lan.Data, lan.Controle, lan.Genero,');
    QrFutDeb.SQL.Add('lan.Descricao, lan.NotaFiscal, lan.Debito,');
    QrFutDeb.SQL.Add('lan.Vencimento, lan.Carteira, lan.Documento,');
    QrFutDeb.SQL.Add('lan.Fornecedor');
    QrFutDeb.SQL.Add('FROM lanctos lan');
    QrFutDeb.SQL.Add('LEFT JOIN contas cta ON cta.Codigo=lan.Genero');
    QrFutDeb.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo');
    QrFutDeb.SQL.Add('LEFT JOIN grupos gru ON gru.Codigo=sgr.Grupo');
    QrFutDeb.SQL.Add('LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto');
    QrFutDeb.SQL.Add('LEFT JOIN plano pla ON pla.Codigo=cjt.Plano');
    QrFutDeb.SQL.Add('WHERE NOT (lan.FatID in (600,601,610))');
    QrFutDeb.SQL.Add('AND lan.Tipo=2');
    QrFutDeb.SQL.Add('AND lan.CliInt=' + F_CliInt_TXT);
    QrFutDeb.SQL.Add('AND (lan.Compensado=0');
    QrFutDeb.SQL.Add('  OR lan.Compensado>="'+FDataF1+'")');
    //
    // n�veis variados a serem pesquisados:
    SQLDebitosFuturos(QrFutDeb, 'AND lan.Controle=0');
    QrFutDeb.SQL.Add('ORDER BY lan.Vencimento, lan.Data');
    QrFutDeb.Open;
    //
    QrSumDeb.Close;
    QrSumDeb.SQL.Clear;
    QrSumDeb.SQL.Add('SELECT SUM(lan.Debito) DEBITO');
    QrSumDeb.SQL.Add('FROM lanctos lan');
    QrSumDeb.SQL.Add('LEFT JOIN contas cta ON cta.Codigo=lan.Genero');
    QrSumDeb.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo');
    QrSumDeb.SQL.Add('LEFT JOIN grupos gru ON gru.Codigo=sgr.Grupo');
    QrSumDeb.SQL.Add('LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto');
    QrSumDeb.SQL.Add('LEFT JOIN plano pla ON pla.Codigo=cjt.Plano');
    QrSumDeb.SQL.Add('WHERE NOT (lan.FatID in (600,601,610))');
    QrSumDeb.SQL.Add('AND lan.Tipo=2');
    QrSumDeb.SQL.Add('AND lan.CliInt=' + F_CliInt_TXT);
    QrSumDeb.SQL.Add('AND (lan.Compensado=0');
    QrSumDeb.SQL.Add('  OR lan.Compensado>="'+FDataF1+'")');
    SQLDebitosFuturos(QrSumDeb, 'AND lan.Controle=0');
    QrSumDeb.Open;
    EdDebiVencer.ValueVariant := QrSumDebDEBITO.Value;
    // Fim n�veis variados a serem pesquisados.
    //
    // Arrecada��es Futuras!
    QrArreBaI.Close;
    QrArreBaI.SQL.Clear;
    QrArreBaI.SQL.Add('SELECT ((bai.ParcPerF - '+PeriF_TXT+') + 1) ParcelasRestantes,');
    QrArreBaI.SQL.Add('bai.Valor, bai.Percent, bai.Fator, bai.DoQue, ');
    QrArreBaI.SQL.Add('bai.ValFracAsk, bai.ValFracDef, bai.Texto, bac.Nome');
    QrArreBaI.SQL.Add('FROM arrebai bai');
    QrArreBaI.SQL.Add('LEFT JOIN arrebac bac ON bac.Codigo=bai.Codigo');
    QrArreBaI.SQL.Add('LEFT JOIN contas cta ON cta.Codigo=bac.Conta');
    QrArreBaI.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo');
    QrArreBaI.SQL.Add('LEFT JOIN grupos gru ON gru.Codigo=sgr.Grupo');
    QrArreBaI.SQL.Add('LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto');
    QrArreBaI.SQL.Add('LEFT JOIN plano pla ON pla.Codigo=cjt.Plano');
    QrArreBaI.SQL.Add('WHERE bai.Cond=' + FormatFloat('0', EdEmpresa.ValueVariant));
    QrArreBaI.SQL.Add('AND (/*(SitCobr=1) OR*/ (SitCobr=2 AND '+PeriF_TXT);
    QrArreBaI.SQL.Add(' BETWEEN bai.ParcPerI AND bai.ParcPerF))');
    // n�o usar porque � mais complxa a situa��o
    //QrArreBaI.SQL.Add('AND bai.DoQue > 2'); // DoQue in (0,1,2) se desconhece o valor!
    SQLDebitosFuturos(QrArreBaI, 'AND bai.Controle=0');
    QrArreBaI.Open;
    ReceVencer := 0;
    while not QrArreBaI.Eof do
    begin
      ReceVencer := ReceVencer + QrArreBaIVAL_RESTANTE.Value;
      //
      QrArreBaI.Next;
    end;
    EdReceVencer.ValueVariant := ReceVencer;
    //
    // Fim do c�lculo (colocar  no DMod.Fin).
    //
    //
    QrExclMov.Close;
    QrExclMov.Params[00].AsInteger := QrEmpCodigo.Value;
    QrExclMov.Params[01].AsInteger := MezIni;
    QrExclMov.Params[02].AsInteger := MezFim;
    QrExclMov.Open;
    //
    QrExclusivos.Close;
    QrExclusivos.Params[00].AsInteger := QrEmpCodigo.Value;
    QrExclusivos.Params[01].AsInteger := MezFim;
    QrExclusivos.Open;
    //
    QrOrdiMov.Close;
    QrOrdiMov.Params[00].AsInteger := QrEmpCodigo.Value;
    QrOrdiMov.Params[01].AsInteger := MezIni;
    QrOrdiMov.Params[02].AsInteger := MezFim;
    QrOrdiMov.Open;
    //
    QrOrdinarios.Close;
    QrOrdinarios.Params[00].AsInteger := QrEmpCodigo.Value;
    QrOrdinarios.Params[01].AsInteger := MezFim;
    QrOrdinarios.Open;
    //
    DemostrativoDeReceitasEDespesas();
    //
    BtImprime.Enabled := True;
    //
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCashPreCta.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCashPreCta.CBMesIniChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmCashPreCta.DemostrativoDeReceitasEDespesas();
const
  CarteiraUnica = 0;
begin
  DModFin.ReopenCreditosEDebitos(F_CliInt, FDataI1, FDataF1, CkAcordos.Checked,
  CkPeriodos.Checked, CkTextos.Checked, CkNaoAgruparNada.Checked, CarteiraUnica);
  Screen.Cursor := crHourGlass;
  //
  DmodFin.ReopenSaldoAEResumo(F_CliInt, CarteiraUnica, FDataI1, FDataF1);
end;

procedure TFmCashPreCta.EdEmpresaChange(Sender: TObject);
begin
  if not EdEmpresa.Focused then
    ReopenNiveisCtas();
end;

procedure TFmCashPreCta.EdEmpresaExit(Sender: TObject);
begin
  ReopenNiveisCtas();
end;

procedure TFmCashPreCta.FechaPesquisa;
begin
  BtImprime.Enabled := False;
  QrFutDeb.Close;
  QrArreBai.Close;
end;

procedure TFmCashPreCta.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCashPreCta.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  QrSdoNivCtas.Close;
  QrSdoNivCtas.Database := DModG.MyPID_DB;
  //
  QrCtasNiv.Close;
  QrCtasNiv.Database := DModG.MyPID_DB;
  //
  QrCNS.Close;
  QrCNS.Database := DModG.MyPID_DB;
  //
  QrCNA.Close;
  QrCNA.Database := DModG.MyPID_DB;
  //
  QrEmp.Open;
  //
  MyObjects.PreencheCBAnoECBMes(CBAnoIni, CBMesIni, -7);
  MyObjects.PreencheCBAnoECBMes(CBAnoFim, CBMesFim, -1);
end;

procedure TFmCashPreCta.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCashPreCta.frxPreCtaGetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_PERIODO') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE6,
    MLAGeral.PrimeiroDiaDoPeriodo_Date(FPeriodoIni)) + ' at� ' +
    FormatDateTime(VAR_FORMATDATE6,
    MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoFim))
  else if VarName = 'VARF_PAGINAR' then
    Value := CkPaginar.Checked
  else if AnsiCompareText(VarName, 'VAR_DATA_PER_ANT') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE2,
    MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoIni-1))
  else if AnsiCompareText(VarName, 'VAR_DATA_PER_FIM') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE2,
    MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoFim))
  else if AnsiCompareText(VarName, 'VARF_DESP_VENCER') = 0 then Value :=
    - EdDebiVencer.ValueVariant
  else if AnsiCompareText(VarName, 'VARF_RECE_VENCER') = 0 then Value :=
    EdReceVencer.ValueVariant
  {
  else if VarName = 'VARF_PERIODO_TXT' then
    Value := 'FAZER PERIODO_TXT!'//Geral.Maiusculas(MyObjects.CBAnoECBMesToPeriodoTxt(CBAno, CBMes), False)
  else if AnsiCompareText(VarName, 'VARF_ULTIMODIA') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE3,
    MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoFim))
  else if AnsiCompareText(VarName, 'MES_ANO ') = 0 then Value :=
    MLAGeral.MesEAnoDoPeriodoLongo(FPeriodoAtu)
  else if AnsiCompareText(VarName, 'CIDADE  ') = 0 then Value :=
    QrEmpCIDADE.Value
  else if AnsiCompareText(VarName, 'SIGLA_UF') = 0 then Value :=
    QrEmpNO_UF.Value
  else if AnsiCompareText(VarName, 'DATA_ATU') = 0 then Value :=
    Geral.FDT(Date, 6)
  else if AnsiCompareText(VarName, 'DATA_FIM') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE6,
    MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoFim))
  else if Copy(VarName, 1, 5) = 'VMES_' then
    Value := Copy(VarName, 6) + ' / ' + CBAno.Text
  else if VarName = 'VARF_ANO1' then
    Value := 'Total ' + CBAno.Text
  else if VarName = 'VARF_ANO0' then
    Value := 'Total ' + FormatFloat('0', Geral.IMV(CBAno.Text) -1)
  else if VarName = 'VARF_MID1' then
    Value := 'M�dia ' + CBAno.Text
  else if VarName = 'VARF_MID0' then
    Value := 'M�dia ' + FormatFloat('0', Geral.IMV(CBAno.Text) -1)
  else if VarName = 'LogoBalanceteExiste' then
    Value := FileExists(QrEntiCfgRel_01LogoPath.Value)
  else if VarName = 'LogoBalancetePath' then
    Value := QrEntiCfgRel_01LogoPath.Value
  else if VarName = 'VARF_EMPRESA' then
    Value := CBEmpresa.Text
  else if VarName = 'VARF_RESUMIDO_01' then Value := FResumido_01
  else if VarName = 'VARF_RESUMIDO_02' then Value := FResumido_02
  else if VarName = 'VARF_RESUMIDO_03' then Value := FResumido_03
  else if VarName = 'VARF_RESUMIDO_04' then Value := FResumido_04
  else if VarName = 'VARF_RESUMIDO_05' then Value := FResumido_05
  else if VarName = 'VARF_RESUMIDO_06' then Value := FResumido_06
  }
end;

procedure TFmCashPreCta.QrArreBaICalcFields(DataSet: TDataSet);
begin
  QrArreBaIVAL_RESTANTE.Value := ValorArrecadacaoFutura();
end;

procedure TFmCashPreCta.QrExclusivosCalcFields(DataSet: TDataSet);
begin
  QrExclusivosCREDITO.Value := QrExclMovCredito.Value;
  QrExclusivosDEBITO.Value := QrExclMovDebito.Value;
  QrExclusivosANTERIOR.Value := QrExclusivosMovim.Value -
    QrExclMovDebito.Value + QrExclMovCredito.Value;
end;

procedure TFmCashPreCta.QrOrdinariosCalcFields(DataSet: TDataSet);
begin
  QrOrdinariosCREDITO.Value := QrOrdiMovCredito.Value;
  QrOrdinariosDEBITO.Value := QrOrdiMovDebito.Value;
  QrOrdinariosANTERIOR.Value := QrOrdinariosMovim.Value +
    QrOrdiMovDebito.Value - QrOrdiMovCredito.Value;
end;

procedure TFmCashPreCta.ReopenNiveisCtas();
begin
  FCtasNiv := UCriar.RecriaTempTable('CtasNiv', DModG.QrUpdPID1, False);
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FCtasNiv);
  DmodG.QrUpdPID1.SQL.Add('SELECT ELT(niv.Nivel, "Conta", "Sub-grupo", "Grupo",');
  DmodG.QrUpdPID1.SQL.Add('"Conjunto", "Plano") NOMENIVEL, ELT(niv.Nivel,');
  DmodG.QrUpdPID1.SQL.Add('cta.Nome, sgr.Nome, gru.Nome, cjt.Nome, pla.Nome)');
  DmodG.QrUpdPID1.SQL.Add('NOMEGENERO, niv.Nivel, niv.Genero, 0 Ativo');
  DmodG.QrUpdPID1.SQL.Add('FROM ' + TMeuDB + '.contasniv niv');
  DmodG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.contas    cta ON cta.Codigo=niv.Genero');
  DmodG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.subgrupos sgr ON sgr.Codigo=niv.Genero');
  DmodG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.grupos    gru ON gru.Codigo=niv.Genero');
  DmodG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.conjuntos cjt ON cjt.Codigo=niv.Genero');
  DmodG.QrUpdPID1.SQL.Add('LEFT JOIN '+TMeuDB+'.plano     pla ON pla.Codigo=niv.Genero');
  DmodG.QrUpdPID1.SQL.Add('WHERE niv.Entidade='+FormatFloat('0', QrEmpCodigo.Value)+';');
  DmodG.QrUpdPID1.ExecSQL;
  //
  QrCtasNiv.Close;
  QrCtasNiv.Open;
  //
  FechaPesquisa();
end;

procedure TFmCashPreCta.SQLDebitosFuturos(Query: TmySQLQuery;
SQL_Zero: String);
var
  Tab: String;
begin
  QrCNS.Close;
  QrCNS.Open;
  case QrCNS.RecordCount of
    0: Query.SQL.Add(SQL_Zero); // = nada
    1:
    begin
      case QrCNSNivel.Value of
        1: Tab := 'cta';
        2: Tab := 'sgr';
        3: Tab := 'gru';
        4: Tab := 'cjt';
        5: Tab := 'pla';
        else Tab := '???';
      end;
      Query.SQL.Add('AND '+Tab+'.Codigo='+FormatFloat('0', QrCNSGenero.Value));
    end;
    else
    begin
      QrCNS.First;
      Query.SQL.Add('AND (');
      while not QrCNS.Eof do
      begin
        case QrCNSNivel.Value of
          1: Tab := 'cta';
          2: Tab := 'sgr';
          3: Tab := 'gru';
          4: Tab := 'cjt';
          5: Tab := 'pla';
          else Tab := '???';
        end;
        if QrCNS.RecNo = 1 then
          Query.SQL.Add('    ('+Tab+'.Codigo='+FormatFloat('0', QrCNSGenero.Value)+ ')')
        else
          Query.SQL.Add(' OR ('+Tab+'.Codigo='+FormatFloat('0', QrCNSGenero.Value)+ ')');
        QrCNS.Next;
      end;
      Query.SQL.Add(')');
    end;
  end;
end;

function TFmCashPreCta.ValorArrecadacaoFutura(): Double;
var
  //ValCalc,
  ValB: Double;
  Titulo: String;
  ResVar: Variant;
begin
  ValB := 0;
  if Uppercase(Application.Title) = 'SYNDIC' then
  begin
    // UnAuxCondGer.IncluiintensbasedearrecadaoNovo(SoListaProvisoes: Boolean);
    // C�digo copiado de
    // ArreBaIFator para ValB
    case QrArreBaIFator.Value of
      // Fator = Valor
      0: ValB := QrArreBaIValor.Value;
      // 1: Fator = Percentual
      1:
      begin
        (*if DCond.QrAptos.RecordCount = 0 then ValB := 0 else*)
        begin
          //ValCalc := 0;
          case QrArreBaIDoQue.Value of

  // DoQue = (0,1,2) se desconhece o valor, ent�o ser� zero!
  (*
            //  Sobre Provis�es
            0: ValCalc := FmCondGer.QrPrevGastos.Value;
            //  Sobre Consumos
            1: ValCalc := 0;//QrSumCTVALOR.Value;
            //  Sobre ambos
            // N�o se sabe o valor do FmCondGer.QrPrevGastos.Value ainda!
            2: ValCalc := FmCondGer.QrPrevGastos.Value;// + QrSumCTVALOR.Value;
            //  Valor a definir / definido
  *)
            3:
            begin
              if QrArreBaIValFracAsk.Value = 1 then
              begin
                // Parei aqui
                if QrArreBaITexto.Value <> '' then
                  Titulo := '(' + QrArreBaITexto.Value + ')'
                else
                  Titulo := '';
                if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
                QrArreBaIValFracDef.Value, 2, 0, '', '', True,
                QrArreBaINome.Value, 'Informe o valor total: ' + Titulo, 0, ResVar) then
                  ValB := ResVar
                else
                  ValB := 0;
              end else ValB := QrArreBaIValFracDef.Value;
            end;
            4:
            begin
              // Parei aqui!!!
              // Precisa Fazer ArreBaI
            end;
            //else ValCalc := 0;
          end;
  // N�o se sabe ainda, ent�o ser� zero!
  (*
          if QrArreBaIDoQue.Value <> 3 then
          begin
            if QrArreBaIComoCobra.Value = 0 then
            //  Calcular o percentual sobre o valor cobrado.
            ValB := QrArreBaIPercent.Value * ValCalc / 100
            else
            begin
            //  Incluir o pr�prio valor gerado no c�lculo.
              if QrArreBaIPercent.Value = 0 then ValB := 0 else
              begin
                PercXtra := QrArreBaIPercent.Value;
                PercXtra := PercXtra + (PercXtra * PercXtra / 100);
                ValB := PercXtra * ValCalc / 100;
              end;
            end;
          end;
  *)
        end;
      end;
      else begin
        ValB := 0;
        Application.MessageBox(
          'Fator n�o definido no cadastro de arrecada��es base!',
          'Avise a DERMATEK!', MB_OK+MB_ICONWARNING);
      end;
    end;
    // FIM ArreBaIFator para ValB
    QrPagantes.Close;
    QrPagantes.SQL.Clear;
    QrPagantes.SQL.Add('SELECT COUNT(cdi.Conta) ITENS');
    QrPagantes.SQL.Add('FROM condimov cdi');
    QrPagantes.SQL.Add('WHERE cdi.SitImv=1');
    QrPagantes.SQL.Add('AND cdi.Codigo=' + FormatFloat('0', EdEmpresa.ValueVariant));
    QrPagantes.Open;
    Result := ValB * QrArreBaIParcelasRestantes.Value * QrPagantesITENS.Value;
  end else Result := 0;  
end;

end.
