object FmFinForm: TFmFinForm
  Left = 723
  Top = 345
  Width = 429
  Height = 244
  Caption = 'FmFinForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object frxCopyCH: TfrxReport
    Version = '4.6.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39094.0040391667
    ReportOptions.LastChange = 39094.0040391667
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxCopyCHGetValue
    Left = 12
    Top = 16
    Datasets = <
      item
        DataSet = DmodFin.frxDsAdress1
        DataSetName = 'frxDsAdress1'
      end
      item
        DataSet = DmodFin.frxDsCopyCH
        DataSetName = 'frxDsCopyCH'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000
      Width = 1000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210
      PaperHeight = 297
      PaperSize = 9
      object ReportTitle1: TfrxReportTitle
        Height = 56.69260827
        Top = 18.89765
        Width = 793.7013
        object Memo43: TfrxMemoView
          Left = 75.5906
          Top = 30.23624
          Width = 680.3154
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            'CÓPIA DE CHEQUE')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 504.56692913
        Top = 136.06308
        Width = 793.7013
        RowCount = 1
        object Shape1: TfrxShapeView
          Left = 75.5906
          Width = 680.3154
          Height = 222.99227
          ShowHint = False
          Frame.Width = 0.5
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          Left = 86.92919
          Top = 7.55905999999999
          Width = 49.13389
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            'Banco')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 136.06308
          Top = 7.55905999999999
          Width = 94.48825
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            'Agência')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 230.55133
          Top = 7.55905999999999
          Width = 158.74026
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            'Conta corrente')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 389.29159
          Top = 7.55905999999999
          Width = 49.13389
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            'Série')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 438.42548
          Top = 7.55905999999999
          Width = 94.48825
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            'Cheque nº')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 86.92919
          Top = 30.23624
          Width = 49.13389
          Height = 18.89765
          ShowHint = False
          DataField = 'Banco1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsCopyCH."Banco1"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 136.06308
          Top = 30.2362400000001
          Width = 94.48825
          Height = 18.89765
          ShowHint = False
          DataField = 'Agencia1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsCopyCH."Agencia1"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 230.55133
          Top = 30.2362400000001
          Width = 158.74026
          Height = 18.89765
          ShowHint = False
          DataField = 'Conta1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsCopyCH."Conta1"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 389.29159
          Top = 30.2362400000001
          Width = 49.13389
          Height = 18.89765
          ShowHint = False
          DataField = 'SerieCH'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsCopyCH."SerieCH"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 438.42548
          Top = 30.23624
          Width = 94.48825
          Height = 18.89765
          ShowHint = False
          DataField = 'Documento'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsCopyCH."Documento"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 566.9295
          Top = 7.55905999999999
          Height = 41.57483
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Memo11: TfrxMemoView
          Left = 574.48856
          Top = 7.55905999999999
          Width = 94.48825
          Height = 15.11812
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'R$')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 574.48856
          Top = 26.45671
          Width = 170.07885
          Height = 22.67718
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '$[frxDsCopyCH."Debito"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 86.92919
          Top = 52.91342
          Width = 657.63822
          Height = 37.79527559
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Pague por este cheque a quantia de [frxDsCopyCH."EXTENSO"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 86.92919
          Top = 90.70872
          Width = 657.63822
          Height = 37.79527559
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'a [frxDsCopyCH."NOMEFORNECE"] ou a sua ordem.')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 317.48052
          Top = 128.50402
          Width = 427.08689
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsAdress1."CIDADE"], [frxDsCopyCH."DATA_DE_HOJE"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 86.92919
          Top = 128.50402
          Width = 230.55133
          Height = 56.69295
          ShowHint = False
          DataField = 'NOMEBANCO'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsCopyCH."NOMEBANCO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 321.26005
          Top = 185.19697
          Width = 423.30736
          Height = 37.7953
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsAdress1."NOMEDONO"]')
          ParentFont = False
        end
        object Shape2: TfrxShapeView
          Left = 75.5906
          Top = 241.88992
          Width = 680.3154
          Height = 188.9765
          ShowHint = False
          Frame.Width = 0.5
        end
        object Shape3: TfrxShapeView
          Left = 521.57514
          Top = 260.78757
          Width = 234.33086
          Height = 26.45671
          ShowHint = False
          Frame.Width = 0.1
        end
        object Shape4: TfrxShapeView
          Left = 287.24428
          Top = 260.78757
          Width = 234.33086
          Height = 26.45671
          ShowHint = False
          Frame.Width = 0.1
        end
        object Shape5: TfrxShapeView
          Left = 75.5906
          Top = 260.78757
          Width = 211.65368
          Height = 26.45671
          ShowHint = False
          Frame.Width = 0.1
        end
        object Memo18: TfrxMemoView
          Left = 83.14966
          Top = 291.02381
          Width = 665.19728
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Do banco: [frxDsCopyCH."NOMEBANCO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 83.14966
          Top = 313.70099
          Width = 665.19728
          Height = 37.79527559
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8 = (
            'Utilizado para: [frxDsCopyCH."Descricao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 75.5906
          Top = 355.27582
          Width = 264.5671
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'VISTOS')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 75.5906
          Top = 374.17347
          Width = 173.85838
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 249.44898
          Top = 374.17347
          Width = 90.70872
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'CONTADOR')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 249.44898
          Top = 393.07112
          Width = 90.70872
          Height = 37.7953
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 75.5906
          Top = 393.07112
          Width = 86.92919
          Height = 37.7953
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 162.51979
          Top = 393.07112
          Width = 86.92919
          Height = 37.7953
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 340.1577
          Top = 355.27582
          Width = 90.70872
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          Memo.UTF8 = (
            '  CAIXA')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 340.1577
          Top = 374.17347
          Width = 90.70872
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          Memo.UTF8 = (
            '  C/C')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 340.1577
          Top = 393.07112
          Width = 90.70872
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          Memo.UTF8 = (
            '  TALÃO')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 340.1577
          Top = 411.96877
          Width = 90.70872
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 430.86642
          Top = 355.27582
          Width = 90.70872
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 430.86642
          Top = 374.17347
          Width = 90.70872
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 430.86642
          Top = 393.07112
          Width = 90.70872
          Height = 18.89765
          ShowHint = False
          DataField = 'Debito'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsCopyCH."Debito"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 430.86642
          Top = 411.96877
          Width = 90.70872
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 521.57514
          Top = 355.27582
          Width = 234.33086
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          HAlign = haCenter
          Memo.UTF8 = (
            'CHEQUE ASSINADO POR')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 521.57514
          Top = 374.17347
          Width = 234.33086
          Height = 56.69295
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 83.14966
          Top = 264.5671
          Width = 200.31509
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          Memo.UTF8 = (
            'LANÇAMENTO Nº [frxDsCopyCH."Controle"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 291.02381
          Top = 264.5671
          Width = 49.13389
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          Memo.UTF8 = (
            'VISADO')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox1: TfrxCheckBoxView
          Left = 370.39394
          Top = 264.5671
          Width = 18.89765
          Height = 18.89765
          ShowHint = False
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'VISADO_SIM'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
        end
        object Memo38: TfrxMemoView
          Left = 393.07112
          Top = 264.5671
          Width = 26.45671
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          Memo.UTF8 = (
            'Sim')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox2: TfrxCheckBoxView
          Left = 445.98454
          Top = 264.5671
          Width = 18.89765
          Height = 18.89765
          ShowHint = False
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'VISADO_NAO'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
        end
        object Memo39: TfrxMemoView
          Left = 468.66172
          Top = 264.5671
          Width = 26.45671
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          Memo.UTF8 = (
            'Não')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 525.35467
          Top = 264.5671
          Width = 60.47248
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          Memo.UTF8 = (
            'CRUZADO')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox3: TfrxCheckBoxView
          Left = 604.7248
          Top = 264.5671
          Width = 18.89765
          Height = 18.89765
          ShowHint = False
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'CRUZADO_SIM'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
        end
        object Memo41: TfrxMemoView
          Left = 627.40198
          Top = 264.5671
          Width = 26.45671
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          Memo.UTF8 = (
            'Sim')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox4: TfrxCheckBoxView
          Left = 680.3154
          Top = 264.5671
          Width = 18.89765
          Height = 18.89765
          ShowHint = False
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'CRUZADO_NAO'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.1
        end
        object Memo42: TfrxMemoView
          Left = 702.99258
          Top = 264.5671
          Width = 26.45671
          Height = 18.89765
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.1
          Memo.UTF8 = (
            'Não')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
end
