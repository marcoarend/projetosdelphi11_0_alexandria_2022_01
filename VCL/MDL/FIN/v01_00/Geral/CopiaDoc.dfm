object FmCopiaDoc: TFmCopiaDoc
  Left = 339
  Top = 185
  Caption = 'LAN-COPIA-001 :: C'#243'pia de Lan'#231'amento'
  ClientHeight = 455
  ClientWidth = 841
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 407
    Width = 841
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtPesquisa: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Pesquisa'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtPesquisaClick
    end
    object Panel2: TPanel
      Left = 729
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtImprime: TBitBtn
      Tag = 5
      Left = 112
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Imprime'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtImprimeClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 841
    Height = 48
    Align = alTop
    Caption = 'C'#243'pia de Lan'#231'amento'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 839
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 841
    Height = 359
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 333
      Width = 839
      Height = 25
      Align = alBottom
      TabOrder = 0
      object LaAviso: TLabel
        Left = 4
        Top = 4
        Width = 13
        Height = 13
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object DBGrid2: TDBGrid
      Left = 1
      Top = 51
      Width = 839
      Height = 282
      Align = alClient
      DataSource = DsCopiaCH2
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Data'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SerieCH'
          Title.Caption = 'S'#233'rie documento'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Documento'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D'#233'bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Banco1'
          Title.Caption = 'Banco'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Agencia1'
          Title.Caption = 'Ag'#234'ncia'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Conta1'
          Title.Caption = 'Conta corrente'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Tipo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TipoDoc'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ID_Copia'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ID_Pgto'
          Visible = True
        end>
    end
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 839
      Height = 50
      Align = alTop
      TabOrder = 2
      object CkDono: TCheckBox
        Left = 288
        Top = 4
        Width = 146
        Height = 17
        Caption = 'Imprime nome da empresa.'
        TabOrder = 2
      end
      object RGDataTipo: TRadioGroup
        Left = 114
        Top = 1
        Width = 170
        Height = 45
        Caption = 'Imprimir data:'
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Emiss'#227'o'
          'Vencimento')
        TabOrder = 1
      end
      object RGItensPagina: TRadioGroup
        Left = 4
        Top = 1
        Width = 104
        Height = 45
        Caption = ' Itens por p'#225'gina: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          '1'
          '2'
          '3')
        TabOrder = 0
      end
      object CkNaoAgrupar: TCheckBox
        Left = 288
        Top = 23
        Width = 146
        Height = 17
        Caption = 'N'#227'o agrupar lan'#231'amentos'
        TabOrder = 3
        OnClick = CkNaoAgruparClick
      end
    end
  end
  object QrCorrige: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT DISTINCT car.TipoDoc, cc1.ID_Pgto '
      'FROM copiach1 cc1'
      'LEFT JOIN ???.lanctos lan ON lan.Controle=cc1.ID_Pgto'
      'LEFT JOIN ???.carteiras car ON car.Codigo=lan.Carteira'
      'WHERE cc1.ID_Pgto <> 0')
    Left = 264
    Top = 164
    object QrCorrigeTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
    object QrCorrigeID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
    end
  end
  object QrCopiaCH1: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT COUNT(cc1.Debito) Itens, Banco1, Conta1, '
      'Agencia1, SerieCH, Data, Documento, NOMEBANCO, '
      'TipoDoc, Tipo, ID_Copia, SUM(Debito) DEBITO,'
      'NOMEEMPRESA, NotaFiscal, Duplicata, Genero,'
      'NOMEFORNECE, Controle, Descricao'
      'FROM copiach1 cc1'
      'GROUP BY Banco1, Conta1, '
      'Agencia1, SerieCH, Data, Documento, NOMEBANCO, '
      'TipoDoc, Tipo, ID_Copia')
    Left = 292
    Top = 164
    object QrCopiaCH1Itens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
    object QrCopiaCH1Banco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrCopiaCH1Conta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrCopiaCH1Agencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrCopiaCH1SerieCH: TWideStringField
      FieldName = 'SerieCH'
    end
    object QrCopiaCH1Data: TDateField
      FieldName = 'Data'
    end
    object QrCopiaCH1Documento: TFloatField
      FieldName = 'Documento'
    end
    object QrCopiaCH1NOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
    object QrCopiaCH1TipoDoc: TIntegerField
      FieldName = 'TipoDoc'
    end
    object QrCopiaCH1Tipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCopiaCH1ID_Copia: TIntegerField
      FieldName = 'ID_Copia'
    end
    object QrCopiaCH1DEBITO: TFloatField
      FieldName = 'DEBITO'
    end
    object QrCopiaCH1NOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrCopiaCH1NotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrCopiaCH1Duplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 50
    end
    object QrCopiaCH1Genero: TIntegerField
      FieldName = 'Genero'
    end
    object QrCopiaCH1NOMEFORNECE: TWideStringField
      FieldName = 'NOMEFORNECE'
      Size = 100
    end
    object QrCopiaCH1Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCopiaCH1Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
  end
  object QrItens: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT NOMEFORNECE, Descricao, Controle '
      'FROM copiach1 cc1'
      'WHERE Banco1=:P0'
      'AND Conta1=:P1'
      'AND Agencia1=:P2'
      'AND SerieCH=:P3'
      'AND Data=:P4'
      'AND Documento=:P5'
      'AND NOMEBANCO=:P6'
      'AND TipoDoc=:P7'
      'AND Tipo=:P8'
      'AND ID_Copia=:P9')
    Left = 320
    Top = 164
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P7'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P8'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P9'
        ParamType = ptUnknown
      end>
    object QrItensNOMEFORNECE: TWideStringField
      FieldName = 'NOMEFORNECE'
      Size = 100
    end
    object QrItensDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrItensControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrCopiaCH2: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterOpen = QrCopiaCH2AfterOpen
    BeforeClose = QrCopiaCH2BeforeClose
    SQL.Strings = (
      'SELECT * '
      'FROM copiach2'
      'ORDER BY SerieCH, Documento')
    Left = 348
    Top = 164
    object QrCopiaCH2Banco1: TIntegerField
      FieldName = 'Banco1'
      Origin = 'copiach2.Banco1'
    end
    object QrCopiaCH2Conta1: TWideStringField
      FieldName = 'Conta1'
      Origin = 'copiach2.Conta1'
      Size = 15
    end
    object QrCopiaCH2Agencia1: TIntegerField
      FieldName = 'Agencia1'
      Origin = 'copiach2.Agencia1'
    end
    object QrCopiaCH2SerieCH: TWideStringField
      FieldName = 'SerieCH'
      Origin = 'copiach2.SerieCH'
    end
    object QrCopiaCH2Data: TDateField
      FieldName = 'Data'
      Origin = 'copiach2.Data'
    end
    object QrCopiaCH2Documento: TFloatField
      FieldName = 'Documento'
      Origin = 'copiach2.Documento'
    end
    object QrCopiaCH2Debito: TFloatField
      FieldName = 'Debito'
      Origin = 'copiach2.Debito'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrCopiaCH2NOMEFORNECE: TWideMemoField
      FieldName = 'NOMEFORNECE'
      Origin = 'copiach2.NOMEFORNECE'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCopiaCH2Descricao: TWideMemoField
      FieldName = 'Descricao'
      Origin = 'copiach2.Descricao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCopiaCH2Controles: TWideMemoField
      FieldName = 'Controles'
      Origin = 'copiach2.Controles'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCopiaCH2ID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Origin = 'copiach2.ID_Pgto'
    end
    object QrCopiaCH2NOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Origin = 'copiach2.NOMEBANCO'
      Size = 100
    end
    object QrCopiaCH2TipoDoc: TIntegerField
      FieldName = 'TipoDoc'
      Origin = 'copiach2.TipoDoc'
    end
    object QrCopiaCH2Tipo: TIntegerField
      FieldName = 'Tipo'
      Origin = 'copiach2.Tipo'
    end
    object QrCopiaCH2ID_Copia: TIntegerField
      FieldName = 'ID_Copia'
      Origin = 'copiach2.ID_Copia'
    end
    object QrCopiaCH2QtdDocs: TIntegerField
      FieldName = 'QtdDocs'
      Origin = 'copiach2.QtdDocs'
    end
    object QrCopiaCH2NOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrCopiaCH2NotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrCopiaCH2Duplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 50
    end
    object QrCopiaCH2Genero: TIntegerField
      FieldName = 'Genero'
    end
  end
  object DsCopiaCH2: TDataSource
    DataSet = QrCopiaCH2
    Left = 376
    Top = 164
  end
  object frxCopiaDOC: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39094.004039166700000000
    ReportOptions.LastChange = 39094.004039166700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MeContratoOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MeContrato.Visible := <VARF_EHDEBITO>;                        ' +
        '                            '
      'end;'
      ''
      'begin'
      '  Memo20.Visible     := <VAR_IMPDONO>;    '
      
        '  MasterData1.Height := (13.80 / 2.539 * 96) * (2 / <VARF_ITENS>' +
        ');                                                              ' +
        '              '
      'end.')
    OnGetValue = frxCopiaDOCGetValue
    Left = 404
    Top = 164
    Datasets = <
      item
        DataSet = frxDsCopiaDOC
        DataSetName = 'frxDsCopiaDOC'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      object MasterData1: TfrxMasterData
        Height = 521.574803149606300000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        DataSet = frxDsCopiaDOC
        DataSetName = 'frxDsCopiaDOC'
        RowCount = 0
        object Shape1: TfrxShapeView
          Left = 181.417440000000000000
          Top = 64.252010000000000000
          Width = 536.693260000000000000
          Height = 279.685220000000000000
          ShowHint = False
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          Left = 200.315090000000000000
          Top = 68.031540000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Banco')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 245.669450000000000000
          Top = 68.031540000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Ag'#234'ncia')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 325.039580000000000000
          Top = 68.031540000000000000
          Width = 120.944960000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta corrente')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 449.764070000000000000
          Top = 68.031540000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 514.016080000000000000
          Top = 68.031540000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[TIT_DOC]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 200.315090000000000000
          Top = 90.708720000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."Banco1"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 245.669450000000000000
          Top = 90.708720000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."Agencia1"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 321.260050000000000000
          Top = 90.708720000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."Conta1"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 449.764070000000000000
          Top = 90.708720000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."SerieCH"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 514.016080000000000000
          Top = 90.708720000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."Documento"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 597.165740000000000000
          Top = 68.031540000000000000
          Height = 41.574830000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Memo11: TfrxMemoView
          Left = 600.945270000000000000
          Top = 68.031540000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'R$')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 600.945270000000000000
          Top = 86.929190000000000000
          Width = 113.385900000000000000
          Height = 22.677180000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '$[frxDsCopiaDOC."Debito"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 200.315090000000000000
          Top = 113.385900000000000000
          Width = 514.016080000000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[TXT_EXTENSO][VARF_EXTENSO]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 200.315090000000000000
          Top = 151.181200000000000000
          Width = 514.016080000000000000
          Height = 60.472455590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[TXT_PARA][frxDsCopiaDOC."NOMEFORNECE"][TXT_OUPARA]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 200.315090000000000000
          Top = 245.669450000000000000
          Width = 514.016080000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDono."CIDADE"], [frxDsCopiaDOC."Data"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 200.315090000000000000
          Top = 211.653680000000000000
          Width = 514.016080000000000000
          Height = 30.236240000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCopiaDOC."NOMEBANCO"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 200.315090000000000000
          Top = 306.141930000000000000
          Width = 514.016080000000000000
          Height = 37.795300000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."NOMEEMPRESA"]')
          ParentFont = False
        end
        object Shape2: TfrxShapeView
          Left = 41.574830000000000000
          Top = 351.496290000000000000
          Width = 676.535870000000000000
          Height = 170.078850000000000000
          ShowHint = False
          Frame.Width = 0.500000000000000000
        end
        object Shape3: TfrxShapeView
          Left = 487.559370000000000000
          Top = 351.496290000000000000
          Width = 230.551330000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Shape4: TfrxShapeView
          Left = 253.228510000000000000
          Top = 351.496290000000000000
          Width = 234.330860000000000000
          Height = 26.456710000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Shape5: TfrxShapeView
          Left = 41.574830000000000000
          Top = 351.496290000000000000
          Width = 211.653680000000000000
          Height = 64.252010000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Memo18: TfrxMemoView
          Left = 257.008040000000000000
          Top = 381.732530000000000000
          Width = 457.323130000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Do banco: [frxDsCopiaDOC."NOMEBANCO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 257.008040000000000000
          Top = 400.630180000000000000
          Width = 457.323130000000000000
          Height = 41.574805590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Utilizado para: [frxDsCopiaDOC."Descricao"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 41.574830000000000000
          Top = 445.984540000000000000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 306.141930000000000000
          Top = 445.984540000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  CAIXA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 306.141930000000000000
          Top = 464.882190000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  C/C')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 306.141930000000000000
          Top = 483.779840000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  TAL'#195'O')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 306.141930000000000000
          Top = 502.677490000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 396.850650000000000000
          Top = 445.984540000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 396.850650000000000000
          Top = 464.882190000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 396.850650000000000000
          Top = 502.677490000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 487.559370000000000000
          Top = 445.984540000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[TIT_RESPONSABILIDADE]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 487.559370000000000000
          Top = 464.882190000000000000
          Width = 230.551330000000000000
          Height = 56.692950000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 45.354360000000000000
          Top = 355.275820000000000000
          Width = 204.094620000000000000
          Height = 56.692950000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'LAN'#199'AMENTOS: [frxDsCopiaDOC."Controles"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 257.008040000000000000
          Top = 355.275820000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VISADO')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox1: TfrxCheckBoxView
          Left = 336.378170000000000000
          Top = 355.275820000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'VISADO_SIM'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo38: TfrxMemoView
          Left = 359.055350000000000000
          Top = 355.275820000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sim')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox2: TfrxCheckBoxView
          Left = 411.968770000000000000
          Top = 355.275820000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'VISADO_NAO'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 434.645950000000000000
          Top = 355.275820000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 491.338900000000000000
          Top = 355.275820000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CRUZADO')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox3: TfrxCheckBoxView
          Left = 570.709030000000000000
          Top = 355.275820000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'CRUZADO_SIM'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo41: TfrxMemoView
          Left = 593.386210000000000000
          Top = 355.275820000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sim')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox4: TfrxCheckBoxView
          Left = 646.299630000000000000
          Top = 355.275820000000000000
          Width = 18.897650000000000000
          Height = 18.897650000000000000
          ShowHint = False
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'CRUZADO_NAO'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo42: TfrxMemoView
          Left = 668.976810000000000000
          Top = 355.275820000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape6: TfrxShapeView
          Top = 351.496290000000000000
          Width = 41.574830000000000000
          Height = 170.078850000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Memo43: TfrxMemoView
          Top = 18.897650000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[TIT_COPIA]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 192.756030000000000000
          Top = 37.795300000000000000
          Width = 525.354670000000000000
          Height = 26.456685590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Utilizado para: [frxDsCopiaDOC."Descricao"]')
          ParentFont = False
          WordWrap = False
        end
        object Shape7: TfrxShapeView
          Top = 41.574830000000000000
          Width = 170.078740160000000000
          Height = 302.362204720000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Memo21: TfrxMemoView
          Left = 7.559060000000000000
          Top = 49.133890000000000000
          Width = 154.960730000000000000
          Height = 287.244280000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[CANHOTO]')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Shape8: TfrxShapeView
          Left = 41.574830000000000000
          Top = 415.748300000000000000
          Width = 211.653680000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Shape9: TfrxShapeView
          Left = 41.574830000000000000
          Top = 430.866420000000000000
          Width = 211.653680000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Memo22: TfrxMemoView
          Left = 49.133890000000000000
          Top = 415.748300000000000000
          Width = 200.315090000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N.F. N'#186' [frxDsCopiaDOC."NotaFiscal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 49.133890000000000000
          Top = 430.866420000000000000
          Width = 200.315090000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DUPLICATA N'#186' [frxDsCopiaDOC."Duplicata"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeContrato: TfrxMemoView
          Left = 253.228510000000000000
          Top = 351.496290000000000000
          Width = 464.882190000000000000
          Height = 26.456692910000000000
          OnBeforePrint = 'MeContratoOnBeforePrint'
          ShowHint = False
          Color = clWindow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CONTRATO: [VARF_CONTRATO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 41.574830000000000000
          Top = 445.984540000000000000
          Width = 264.567100000000000000
          Height = 75.590600000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
      end
    end
  end
  object frxCheckBoxObject1: TfrxCheckBoxObject
    Left = 376
    Top = 192
  end
  object frxRichObject1: TfrxRichObject
    Left = 404
    Top = 192
  end
  object frxDsCopiaDOC: TfrxDBDataset
    UserName = 'frxDsCopiaDOC'
    CloseDataSource = False
    DataSet = QrCopiaCH2
    BCDToCurrency = False
    Left = 348
    Top = 192
  end
  object QrContasEnt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM '
      'contasent'
      'WHERE Codigo=:P0'
      'AND Entidade=:P1')
    Left = 264
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrContasEntCntrDebCta: TWideStringField
      FieldName = 'CntrDebCta'
      Size = 50
    end
  end
  object frxCopiaDOC2: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39094.004039166700000000
    ReportOptions.LastChange = 40387.684103287050000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MeContratoOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MeContrato.Visible := <VARF_EHDEBITO>;                        ' +
        '                            '
      'end;'
      ''
      'begin'
      '  Memo20.Visible := <VAR_IMPDONO>;    '
      
        '  //MasterData1.Height := (13.80 / 2.539 * 96) * (2 / <VARF_ITEN' +
        'S>);                                                            ' +
        '                '
      'end.')
    OnGetValue = frxCopiaDOCGetValue
    Left = 432
    Top = 164
    Datasets = <
      item
        DataSet = frxDsCopiaDOC
        DataSetName = 'frxDsCopiaDOC'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object MasterData1: TfrxMasterData
        Height = 374.173228350000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        DataSet = frxDsCopiaDOC
        DataSetName = 'frxDsCopiaDOC'
        RowCount = 0
        object Line2: TfrxLineView
          Top = 374.173228350000000000
          Width = 793.700787400000000000
          ShowHint = False
          Frame.Style = fsDashDotDot
          Frame.Typ = [ftTop]
        end
        object Shape1: TfrxShapeView
          Left = 37.795275590000000000
          Top = 72.015770000000000000
          Width = 718.110700000000000000
          Height = 132.283550000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          Left = 56.897649999999990000
          Top = 75.795300000000000000
          Width = 41.574830000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Banco')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 113.590600000000000000
          Top = 75.795300000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ag'#234'ncia')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 204.299320000000000000
          Top = 75.795300000000000000
          Width = 124.724490000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta corrente')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 344.141930000000000000
          Top = 75.795300000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 419.732530000000000000
          Top = 75.795300000000000000
          Width = 79.370130000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[TIT_DOC]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 56.897649999999990000
          Top = 90.913420000000000000
          Width = 41.574830000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."Banco1"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 113.590600000000000000
          Top = 90.913420000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."Agencia1"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 204.299320000000000000
          Top = 90.913420000000000000
          Width = 124.724490000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."Conta1"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 344.141930000000000000
          Top = 90.913420000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."SerieCH"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 419.732530000000000000
          Top = 90.913420000000000000
          Width = 79.370130000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."Documento"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 620.047620000000000000
          Top = 75.795300000000000000
          Height = 30.236240000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
        end
        object Memo11: TfrxMemoView
          Left = 623.827150000000000000
          Top = 75.795300000000000000
          Width = 94.488250000000000000
          Height = 11.338580240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'R$')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 623.827150000000000000
          Top = 87.354360000000000000
          Width = 113.385900000000000000
          Height = 18.897640240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '$[frxDsCopiaDOC."Debito"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 56.692913390000000000
          Top = 105.811070000000000000
          Width = 680.314960630000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[TXT_EXTENSO][VARF_EXTENSO]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 56.692913390000000000
          Top = 121.149660000000000000
          Width = 680.314960630000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[TXT_PARA][frxDsCopiaDOC."NOMEFORNECE"][TXT_OUPARA]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 56.692913390000000000
          Top = 150.629921260000000000
          Width = 680.314960630000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDono."CIDADE"], [frxDsCopiaDOC."Data"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 56.692913385826770000
          Top = 136.645669290000000000
          Width = 680.314960629921300000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCopiaDOC."NOMEBANCO"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 132.283464566929100000
          Top = 189.181200000000000000
          Width = 529.133858270000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCopiaDOC."NOMEEMPRESA"]')
          ParentFont = False
        end
        object Shape2: TfrxShapeView
          Left = 79.370078740000000000
          Top = 211.653543310000000000
          Width = 676.535433070000000000
          Height = 120.944881890000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Shape3: TfrxShapeView
          Left = 525.354330708661400000
          Top = 211.653543310000000000
          Width = 230.551330000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Shape4: TfrxShapeView
          Left = 291.023622050000000000
          Top = 211.653543310000000000
          Width = 234.330708661417300000
          Height = 15.118110240000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Shape5: TfrxShapeView
          Left = 79.370078740000000000
          Top = 211.653543310000000000
          Width = 211.653680000000000000
          Height = 30.236230240000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Memo20: TfrxMemoView
          Left = 79.370078740000000000
          Top = 272.125984250000000000
          Width = 264.567100000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 343.937007870000000000
          Top = 272.125984250000000000
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  CAIXA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 343.937007870000000000
          Top = 287.244094488189000000
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  C/C')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 343.937007870000000000
          Top = 302.362204724409400000
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '  TAL'#195'O')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 343.937007870000000000
          Top = 317.480314960629900000
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 434.645669290000000000
          Top = 272.125984251968500000
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Left = 434.645669290000000000
          Top = 287.244094488189000000
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 434.645669290000000000
          Top = 302.362204724409400000
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 525.354330710000000000
          Top = 272.125984251968500000
          Width = 230.551181100000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[TIT_RESPONSABILIDADE]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 83.354360000000000000
          Top = 211.653543310000000000
          Width = 204.094620000000000000
          Height = 30.236220470000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'LAN'#199'AMENTOS: [frxDsCopiaDOC."Controles"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 294.803149610000000000
          Top = 211.653543307086600000
          Width = 49.133890000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VISADO')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox1: TfrxCheckBoxView
          Left = 374.378170000000000000
          Top = 211.653543307086600000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          ShowHint = False
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'VISADO_SIM'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo38: TfrxMemoView
          Left = 397.055350000000000000
          Top = 211.653543307086600000
          Width = 26.456710000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sim')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox2: TfrxCheckBoxView
          Left = 449.968770000000000000
          Top = 211.653543307086600000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          ShowHint = False
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'VISADO_NAO'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 472.645950000000000000
          Top = 211.653543307086600000
          Width = 26.456710000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 533.118430000000000000
          Top = 211.653543307086600000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CRUZADO')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox3: TfrxCheckBoxView
          Left = 601.149970000000000000
          Top = 211.653543307086600000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          ShowHint = False
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'CRUZADO_SIM'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo41: TfrxMemoView
          Left = 631.386210000000000000
          Top = 211.653543307086600000
          Width = 26.456710000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Sim')
          ParentFont = False
          VAlign = vaCenter
        end
        object CheckBox4: TfrxCheckBoxView
          Left = 684.299630000000000000
          Top = 211.653543307086600000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          ShowHint = False
          CheckColor = clBlack
          Checked = False
          CheckStyle = csCheck
          DataField = 'CRUZADO_NAO'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object Memo42: TfrxMemoView
          Left = 706.976810000000000000
          Top = 211.653543307086600000
          Width = 26.456710000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape6: TfrxShapeView
          Left = 37.795275590000000000
          Top = 211.653543307086600000
          Width = 41.574830000000000000
          Height = 120.944881890000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Memo43: TfrxMemoView
          Left = 38.000000000000000000
          Top = 37.795275590551180000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[TIT_COPIA]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 38.000000000000000000
          Top = 56.692913390000000000
          Width = 718.110700000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Utilizado para: [frxDsCopiaDOC."Descricao"]')
          ParentFont = False
          WordWrap = False
        end
        object Shape8: TfrxShapeView
          Left = 79.370078740000000000
          Top = 241.889763780000000000
          Width = 211.653680000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Shape9: TfrxShapeView
          Left = 79.370078740000000000
          Top = 257.007874015748000000
          Width = 211.653680000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
        end
        object Memo22: TfrxMemoView
          Left = 83.354360000000000000
          Top = 241.889763779527600000
          Width = 204.094620000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N.F. N'#186' [frxDsCopiaDOC."NotaFiscal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 83.354360000000000000
          Top = 257.007874015748000000
          Width = 200.315090000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DUPLICATA N'#186' [frxDsCopiaDOC."Duplicata"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeContrato: TfrxMemoView
          Left = 291.023622050000000000
          Top = 211.653543310000000000
          Width = 464.882190000000000000
          Height = 15.118110240000000000
          OnBeforePrint = 'MeContratoOnBeforePrint'
          ShowHint = False
          Color = clWindow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CONTRATO: [VARF_CONTRATO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 434.645950000000000000
          Top = 317.480314960629900000
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 291.023810000000000000
          Top = 226.771800000000000000
          Width = 464.882190000000000000
          Height = 45.354350240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 294.803340000000000000
          Top = 226.771800000000000000
          Width = 457.323130000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Do banco: [frxDsCopiaDOC."NOMEBANCO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 294.803149606299200000
          Top = 241.889910240000000000
          Width = 457.323130000000000000
          Height = 30.236220470000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Utilizado para: [frxDsCopiaDOC."Descricao"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 79.370130000000000000
          Top = 272.126160000000000000
          Width = 264.567100000000000000
          Height = 60.472480000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
      end
    end
  end
end
