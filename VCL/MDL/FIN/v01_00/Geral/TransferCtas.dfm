object FmTransferCtas: TFmTransferCtas
  Left = 371
  Top = 206
  Caption = 'FIN-PLCTA-010 :: Transfer'#234'ncia Entre Contas'
  ClientHeight = 514
  ClientWidth = 632
  Color = clBtnFace
  Constraints.MinHeight = 200
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelControle: TPanel
    Left = 0
    Top = 466
    Width = 632
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtExclui: TBitBtn
      Tag = 12
      Left = 276
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Exclui banco atual'
      Caption = '&Exclui'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtExcluiClick
      NumGlyphs = 2
    end
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 16
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Confirma'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtConfirmaClick
      NumGlyphs = 2
    end
    object BtDesiste: TBitBtn
      Tag = 13
      Left = 524
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtDesisteClick
      NumGlyphs = 2
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 172
    Width = 632
    Height = 294
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 630
      Height = 53
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 8
        Width = 62
        Height = 13
        Caption = 'Lan'#231'amento:'
      end
      object Label8: TLabel
        Left = 72
        Top = 8
        Width = 105
        Height = 13
        Caption = 'Data da transfer'#234'ncia:'
      end
      object Label7: TLabel
        Left = 184
        Top = 8
        Width = 27
        Height = 13
        Caption = 'Valor:'
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 24
        Width = 61
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object TPData: TdmkEditDateTimePicker
        Left = 72
        Top = 24
        Width = 109
        Height = 21
        Date = 39693.819140081000000000
        Time = 39693.819140081000000000
        TabOrder = 1
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object EdValor: TdmkEdit
        Left = 184
        Top = 24
        Width = 92
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
    end
    object GBDebt: TGroupBox
      Left = 1
      Top = 54
      Width = 630
      Height = 115
      Align = alTop
      Caption = ' Dados de d'#233'bito: '
      TabOrder = 1
      object Label5: TLabel
        Left = 532
        Top = 20
        Width = 87
        Height = 13
        Caption = 'Docum. de d'#233'bito:'
      end
      object Label2: TLabel
        Left = 452
        Top = 20
        Width = 75
        Height = 13
        Caption = 'S'#233'rie doc. d'#233'b.:'
      end
      object LaConjunto: TLabel
        Left = 8
        Top = 20
        Width = 83
        Height = 13
        Caption = 'Carteira a debitar:'
      end
      object Label9: TLabel
        Left = 8
        Top = 64
        Width = 75
        Height = 13
        Caption = 'Conta de d'#233'bito'
      end
      object EdOrigem: TdmkEditCB
        Left = 8
        Top = 36
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBOrigem
      end
      object EdDocDeb: TdmkEdit
        Left = 532
        Top = 36
        Width = 92
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdSerieChDeb: TdmkEdit
        Left = 452
        Top = 36
        Width = 77
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 10
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object CBOrigem: TdmkDBLookupComboBox
        Left = 64
        Top = 36
        Width = 385
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsOrigem
        TabOrder = 1
        dmkEditCB = EdOrigem
        UpdType = utYes
      end
      object EdCtaDebt: TdmkEditCB
        Left = 8
        Top = 80
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBCtaDebt
      end
      object CBCtaDebt: TdmkDBLookupComboBox
        Left = 64
        Top = 80
        Width = 559
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCtaDebt
        TabOrder = 5
        dmkEditCB = EdCtaDebt
        UpdType = utYes
      end
    end
    object GBCred: TGroupBox
      Left = 1
      Top = 169
      Width = 630
      Height = 108
      Align = alTop
      Caption = ' Dados de cr'#233'dito: '
      TabOrder = 2
      object Label4: TLabel
        Left = 8
        Top = 21
        Width = 86
        Height = 13
        Caption = 'Carteira a creditar:'
      end
      object Label3: TLabel
        Left = 452
        Top = 21
        Width = 75
        Height = 13
        Caption = 'S'#233'rie doc. cr'#233'd:'
      end
      object Label6: TLabel
        Left = 532
        Top = 21
        Width = 90
        Height = 13
        Caption = 'Docum. de cr'#233'dito:'
      end
      object Label10: TLabel
        Left = 8
        Top = 64
        Width = 78
        Height = 13
        Caption = 'Conta de cr'#233'dito'
      end
      object EdDestino: TdmkEditCB
        Left = 8
        Top = 37
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBDestino
      end
      object CBDestino: TdmkDBLookupComboBox
        Left = 64
        Top = 37
        Width = 385
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsDestino
        TabOrder = 1
        dmkEditCB = EdDestino
        UpdType = utYes
      end
      object EdSerieCHCred: TdmkEdit
        Left = 452
        Top = 37
        Width = 77
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 10
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdDocCred: TdmkEdit
        Left = 528
        Top = 37
        Width = 92
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdCtaCred: TdmkEditCB
        Left = 8
        Top = 80
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBCtaCred
      end
      object CBCtaCred: TdmkDBLookupComboBox
        Left = 64
        Top = 80
        Width = 559
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCtaCred
        TabOrder = 5
        dmkEditCB = EdCtaCred
        UpdType = utYes
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 632
    Height = 48
    Align = alTop
    Caption = 'Transfer'#234'ncia Entre Contas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 553
      Top = 1
      Width = 78
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 552
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 552
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = -4
      ExplicitTop = -1
      ExplicitWidth = 550
      ExplicitHeight = 44
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 48
    Width = 632
    Height = 62
    Align = alTop
    Enabled = False
    TabOrder = 3
    object GroupBox1: TGroupBox
      Left = 36
      Top = 1
      Width = 400
      Height = 60
      Caption = ' Dados para altera'#231#227'o lan'#231'amento 1: '
      TabOrder = 0
      object Label11: TLabel
        Left = 140
        Top = 16
        Width = 26
        Height = 13
        Caption = 'Data:'
      end
      object Label12: TLabel
        Left = 12
        Top = 16
        Width = 42
        Height = 13
        Caption = 'Controle:'
      end
      object Label13: TLabel
        Left = 76
        Top = 16
        Width = 22
        Height = 13
        Caption = 'Sub:'
      end
      object Label14: TLabel
        Left = 256
        Top = 16
        Width = 24
        Height = 13
        Caption = 'Tipo:'
      end
      object Label15: TLabel
        Left = 320
        Top = 16
        Width = 39
        Height = 13
        Caption = 'Carteira:'
      end
      object EdOldControle1: TdmkEdit
        Left = 12
        Top = 32
        Width = 60
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object TPOldData1: TdmkEditDateTimePicker
        Left = 140
        Top = 32
        Width = 112
        Height = 21
        Date = 40568.480364108800000000
        Time = 40568.480364108800000000
        Color = clWhite
        TabOrder = 1
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object EdOldSub1: TdmkEdit
        Left = 76
        Top = 32
        Width = 60
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdOldTipo1: TdmkEdit
        Left = 256
        Top = 32
        Width = 60
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdOldCarteira1: TdmkEdit
        Left = 320
        Top = 32
        Width = 60
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 110
    Width = 632
    Height = 62
    Align = alTop
    Enabled = False
    TabOrder = 4
    object GroupBox2: TGroupBox
      Left = 36
      Top = 1
      Width = 400
      Height = 60
      Caption = ' Dados para altera'#231#227'o lan'#231'amento 2:'
      TabOrder = 0
      object Label16: TLabel
        Left = 140
        Top = 16
        Width = 26
        Height = 13
        Caption = 'Data:'
      end
      object Label17: TLabel
        Left = 12
        Top = 16
        Width = 42
        Height = 13
        Caption = 'Controle:'
      end
      object Label18: TLabel
        Left = 76
        Top = 16
        Width = 22
        Height = 13
        Caption = 'Sub:'
      end
      object Label19: TLabel
        Left = 256
        Top = 16
        Width = 24
        Height = 13
        Caption = 'Tipo:'
      end
      object Label20: TLabel
        Left = 320
        Top = 16
        Width = 39
        Height = 13
        Caption = 'Carteira:'
      end
      object EdOldControle2: TdmkEdit
        Left = 12
        Top = 32
        Width = 60
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object TPOldData2: TdmkEditDateTimePicker
        Left = 140
        Top = 32
        Width = 112
        Height = 21
        Date = 40568.480364108800000000
        Time = 40568.480364108800000000
        Color = clWhite
        TabOrder = 1
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object EdOldSub2: TdmkEdit
        Left = 76
        Top = 32
        Width = 60
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdOldTipo2: TdmkEdit
        Left = 256
        Top = 32
        Width = 60
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdOldCarteira2: TdmkEdit
        Left = 320
        Top = 32
        Width = 60
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
    end
  end
  object QrOrigem: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Tipo '
      'FROM carteiras'
      'WHERE ForneceI=:P0'
      'ORDER BY Nome')
    Left = 8
    Top = 8
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOrigemCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOrigemNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrOrigemTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object DsOrigem: TDataSource
    DataSet = QrOrigem
    Left = 36
    Top = 8
  end
  object DsDestino: TDataSource
    DataSet = QrDestino
    Left = 92
    Top = 8
  end
  object QrDestino: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Tipo '
      'FROM carteiras'
      'WHERE ForneceI=:P0'
      'ORDER BY Nome')
    Left = 64
    Top = 8
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrDestinoTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object QrCtaDebt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM contas'
      'WHERE Codigo > 0'
      '/*AND Debito="V"*/'
      'ORDER BY Nome')
    Left = 544
    Top = 20
    object QrCtaDebtCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCtaDebtNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCtaDebt: TDataSource
    DataSet = QrCtaDebt
    Left = 572
    Top = 20
  end
  object QrCtaCred: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM contas'
      'WHERE Codigo > 0'
      '/*AND Credito="V"*/'
      'ORDER BY Nome')
    Left = 544
    Top = 48
    object QrCtaCredCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCtaCredNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCtaCred: TDataSource
    DataSet = QrCtaCred
    Left = 572
    Top = 48
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 260
    Top = 12
  end
end
