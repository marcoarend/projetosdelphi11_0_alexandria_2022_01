object DmodFin: TDmodFin
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 540
  Width = 925
  object frxDsCopyCH: TfrxDBDataset
    UserName = 'frxDsCopyCH'
    CloseDataSource = False
    DataSet = QrCopyCH
    BCDToCurrency = False
    Left = 92
    Top = 4
  end
  object QrCopyCH: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCopyCHCalcFields
    SQL.Strings = (
      'SELECT lan.Debito, LPAD(car.Banco1, 3, '#39'0'#39') Banco1, '
      'LPAD(car.Agencia1, 4, '#39'0'#39') Agencia1, car.Conta1, '
      'lan.SerieCH, IF(lan.Documento=0, "", '
      'LPAD(lan.Documento, 6, '#39'0'#39')) Documento, lan.Data, '
      'lan.Descricao, lan.Fornecedor, IF(lan.Fornecedor=0, "", '
      'CASE WHEN frn.Tipo=0 THEN frn.RazaoSocial'
      'ELSE frn.Nome END) NOMEFORNECE,'
      'ban.Nome NOMEBANCO, LPAD(lan.Controle, 6, '#39'0'#39') Controle,'
      'lan.TipoCH'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN entidades frn ON frn.Codigo=lan.Fornecedor'
      'LEFT JOIN bancos ban ON ban.Codigo=car.Banco1'
      ''
      'WHERE lan.Controle=:P0')
    Left = 32
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCopyCHDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'lanctos.Debito'
    end
    object QrCopyCHBanco1: TWideStringField
      FieldName = 'Banco1'
      Size = 8
    end
    object QrCopyCHAgencia1: TWideStringField
      FieldName = 'Agencia1'
      Size = 8
    end
    object QrCopyCHConta1: TWideStringField
      FieldName = 'Conta1'
      Origin = 'carteiras.Conta1'
      Size = 15
    end
    object QrCopyCHSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Origin = 'lanctos.SerieCH'
      Size = 10
    end
    object QrCopyCHDocumento: TWideStringField
      FieldName = 'Documento'
      Size = 53
    end
    object QrCopyCHData: TDateField
      FieldName = 'Data'
      Origin = 'lanctos.Data'
      Required = True
    end
    object QrCopyCHDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'lanctos.Descricao'
      Size = 100
    end
    object QrCopyCHFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Origin = 'lanctos.Fornecedor'
    end
    object QrCopyCHNOMEFORNECE: TWideStringField
      FieldName = 'NOMEFORNECE'
      Size = 100
    end
    object QrCopyCHNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Origin = 'bancos.Nome'
      Size = 100
    end
    object QrCopyCHControle: TWideStringField
      FieldName = 'Controle'
      Required = True
      Size = 11
    end
    object QrCopyCHCRUZADO_SIM: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'CRUZADO_SIM'
      Calculated = True
    end
    object QrCopyCHCRUZADO_NAO: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'CRUZADO_NAO'
      Calculated = True
    end
    object QrCopyCHVISADO_SIM: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'VISADO_SIM'
      Calculated = True
    end
    object QrCopyCHVISADO_NAO: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'VISADO_NAO'
      Calculated = True
    end
    object QrCopyCHTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Origin = 'lanctos.TipoCH'
      Required = True
    end
    object QrCopyCHDATA_DE_HOJE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA_DE_HOJE'
      Size = 255
      Calculated = True
    end
    object QrCopyCHEXTENSO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EXTENSO'
      Size = 2048
      Calculated = True
    end
  end
  object QrAdress1: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrAdress1CalcFields
    SQL.Strings = (
      'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, '
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMEDON' +
        'O, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF  END CNPJ_CP' +
        'F, '
      'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG   END IE_RG, '
      'CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""      END NIRE_, '
      'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua END RUA, '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero END NUME' +
        'RO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl END COMPL' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro END BAIR' +
        'RO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade END CIDA' +
        'DE, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome   END NOME' +
        'LOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome   END NOME' +
        'UF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais   END Pais' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd END Logr' +
        'ad, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP    END CEP,' +
        ' '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1    END TE1,' +
        ' '
      'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax    END FAX '
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 32
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAdress1Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrAdress1Cadastro: TDateField
      FieldName = 'Cadastro'
      Origin = 'entidades.Cadastro'
    end
    object QrAdress1NOMEDONO: TWideStringField
      FieldName = 'NOMEDONO'
      Size = 100
    end
    object QrAdress1CNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrAdress1IE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrAdress1NIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 53
    end
    object QrAdress1RUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrAdress1NUMERO: TLargeintField
      FieldName = 'NUMERO'
    end
    object QrAdress1COMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrAdress1BAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrAdress1CIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrAdress1NOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrAdress1NOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrAdress1Pais: TWideStringField
      FieldName = 'Pais'
    end
    object QrAdress1Lograd: TLargeintField
      FieldName = 'Lograd'
    end
    object QrAdress1Tipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrAdress1CEP: TLargeintField
      FieldName = 'CEP'
    end
    object QrAdress1TE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrAdress1FAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrAdress1ENatal: TDateField
      FieldName = 'ENatal'
      Origin = 'entidades.ENatal'
    end
    object QrAdress1PNatal: TDateField
      FieldName = 'PNatal'
      Origin = 'entidades.PNatal'
    end
    object QrAdress1ECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Calculated = True
    end
    object QrAdress1NUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrAdress1E_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 255
      Calculated = True
    end
    object QrAdress1CNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrAdress1FAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrAdress1TE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrAdress1NATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 30
      Calculated = True
    end
    object QrAdress1Respons1: TWideStringField
      FieldName = 'Respons1'
      Origin = 'entidades.Respons1'
      Required = True
      Size = 60
    end
  end
  object frxDsAdress1: TfrxDBDataset
    UserName = 'frxDsAdress1'
    CloseDataSource = False
    DataSet = QrAdress1
    BCDToCurrency = False
    Left = 92
    Top = 52
  end
  object QrAtzCar: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT'
      'IF(car.Tipo <> 2, car.Inicial, 0) INICIAL,'
      'IF(car.Tipo <> 2, SUM(lan.Credito-lan.Debito), 0) ATU_S,'
      'IF(car.Tipo <> 2, 0,'
      '  SUM(CASE WHEN (lan.Credito-lan.Debito+lan.Pago > 0)'
      '  AND (lan.Sit<2)'
      '  THEN lan.Credito-lan.Debito+lan.Pago ELSE 0 END)) FUT_C,'
      'IF(car.Tipo <> 2, 0,'
      '  SUM(CASE WHEN (lan.Credito-lan.Debito+lan.Pago < 0)'
      '  AND (lan.Sit<2)'
      '  THEN lan.Credito-lan.Debito+lan.Pago ELSE 0 END)) FUT_D,'
      'IF(car.Tipo <> 2, 0,'
      '  SUM(IF(lan.Sit=0, (lan.Credito-lan.Debito),'
      '  IF(lan.Sit=1, (lan.Credito-lan.Debito+lan.Pago), 0)))) FUT_S,'
      'car.Tipo'
      'FROM carteiras car'
      'LEFT JOIN lanctos lan   ON car.Codigo=lan.Carteira'
      'LEFT JOIN carteiras ca2 ON ca2.Codigo=car.Banco'
      'WHERE car.Codigo=:P0'
      'GROUP BY car.Codigo')
    Left = 32
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAtzCarINICIAL: TFloatField
      FieldName = 'INICIAL'
    end
    object QrAtzCarATU_S: TFloatField
      FieldName = 'ATU_S'
    end
    object QrAtzCarFUT_C: TFloatField
      FieldName = 'FUT_C'
    end
    object QrAtzCarFUT_D: TFloatField
      FieldName = 'FUT_D'
    end
    object QrAtzCarFUT_S: TFloatField
      FieldName = 'FUT_S'
    end
    object QrAtzCarTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
  end
  object QrUpd: TmySQLQuery
    Database = Dmod.MyDB
    Left = 92
    Top = 100
  end
  object QrLocLct: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM lanctos'
      'WHERE Tipo=1')
    Left = 32
    Top = 148
    object QrLocLctData: TDateField
      FieldName = 'Data'
      Origin = 'lanctos.Data'
    end
    object QrLocLctTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'lanctos.Tipo'
    end
    object QrLocLctCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'lanctos.Carteira'
    end
    object QrLocLctControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'lanctos.Controle'
    end
    object QrLocLctSub: TSmallintField
      FieldName = 'Sub'
      Origin = 'lanctos.Sub'
    end
    object QrLocLctAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      Origin = 'lanctos.Autorizacao'
    end
    object QrLocLctGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'lanctos.Genero'
    end
    object QrLocLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'lanctos.Descricao'
      Size = 100
    end
    object QrLocLctNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'lanctos.NotaFiscal'
    end
    object QrLocLctDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'lanctos.Debito'
    end
    object QrLocLctCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'lanctos.Credito'
    end
    object QrLocLctCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'lanctos.Compensado'
    end
    object QrLocLctDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'lanctos.Documento'
    end
    object QrLocLctSit: TIntegerField
      FieldName = 'Sit'
      Origin = 'lanctos.Sit'
    end
    object QrLocLctVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'lanctos.Vencimento'
    end
    object QrLocLctLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'lanctos.Lk'
    end
    object QrLocLctFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'lanctos.FatID'
    end
    object QrLocLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'lanctos.FatParcela'
    end
    object QrLocLctID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Origin = 'lanctos.ID_Pgto'
    end
    object QrLocLctID_Sub: TSmallintField
      FieldName = 'ID_Sub'
      Origin = 'lanctos.ID_Sub'
    end
    object QrLocLctFatura: TWideStringField
      FieldName = 'Fatura'
      Origin = 'lanctos.Fatura'
      Size = 1
    end
    object QrLocLctBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'lanctos.Banco'
    end
    object QrLocLctLocal: TIntegerField
      FieldName = 'Local'
      Origin = 'lanctos.Local'
    end
    object QrLocLctCartao: TIntegerField
      FieldName = 'Cartao'
      Origin = 'lanctos.Cartao'
    end
    object QrLocLctLinha: TIntegerField
      FieldName = 'Linha'
      Origin = 'lanctos.Linha'
    end
    object QrLocLctOperCount: TIntegerField
      FieldName = 'OperCount'
      Origin = 'lanctos.OperCount'
    end
    object QrLocLctLancto: TIntegerField
      FieldName = 'Lancto'
      Origin = 'lanctos.Lancto'
    end
    object QrLocLctPago: TFloatField
      FieldName = 'Pago'
      Origin = 'lanctos.Pago'
    end
    object QrLocLctMez: TIntegerField
      FieldName = 'Mez'
      Origin = 'lanctos.Mez'
    end
    object QrLocLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Origin = 'lanctos.Fornecedor'
    end
    object QrLocLctCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'lanctos.Cliente'
    end
    object QrLocLctMoraDia: TFloatField
      FieldName = 'MoraDia'
      Origin = 'lanctos.MoraDia'
    end
    object QrLocLctMulta: TFloatField
      FieldName = 'Multa'
      Origin = 'lanctos.Multa'
    end
    object QrLocLctProtesto: TDateField
      FieldName = 'Protesto'
      Origin = 'lanctos.Protesto'
    end
    object QrLocLctDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'lanctos.DataCad'
    end
    object QrLocLctDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'lanctos.DataAlt'
    end
    object QrLocLctUserCad: TSmallintField
      FieldName = 'UserCad'
      Origin = 'lanctos.UserCad'
    end
    object QrLocLctUserAlt: TSmallintField
      FieldName = 'UserAlt'
      Origin = 'lanctos.UserAlt'
    end
    object QrLocLctDataDoc: TDateField
      FieldName = 'DataDoc'
      Origin = 'lanctos.DataDoc'
    end
    object QrLocLctCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Origin = 'lanctos.CtrlIni'
    end
    object QrLocLctNivel: TIntegerField
      FieldName = 'Nivel'
      Origin = 'lanctos.Nivel'
    end
    object QrLocLctVendedor: TIntegerField
      FieldName = 'Vendedor'
      Origin = 'lanctos.Vendedor'
    end
    object QrLocLctAccount: TIntegerField
      FieldName = 'Account'
      Origin = 'lanctos.Account'
    end
    object QrLocLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object QrSaldo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(credito-Debito) Valor'
      'FROM lanctos')
    Left = 92
    Top = 148
    object QrSaldoValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsDuplNF: TDataSource
    DataSet = QrDuplNF
    Left = 92
    Top = 244
  end
  object QrDuplNF: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDuplNFCalcFields
    SQL.Strings = (
      'SELECT lan.Data, lan.Controle, lan.Descricao, lan.Credito, '
      'lan.Debito, lan.NotaFiscal, lan.Compensado, lan.Mez, SerieCH,'
      'lan.Fornecedor, lan.Cliente, car.Nome NOMECART, Documento, '
      'IF(lan.Cliente<>0, CASE WHEN cli.Tipo=0 THEN '
      'cli.RazaoSocial ELSE cli.Nome END, "") NOMECLI,'
      'IF(lan.Fornecedor<>0, CASE WHEN fnc.Tipo=0 THEN '
      'fnc.RazaoSocial ELSE fnc.Nome END, "") NOMEFNC,'
      'lan.Carteira'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN entidades cli ON cli.Codigo=lan.Cliente'
      'LEFT JOIN entidades fnc ON fnc.Codigo=lan.Fornecedor'
      'WHERE lan.ID_Pgto = 0'
      'AND lan.NotaFiscal=:P0'
      'AND car.ForneceI=:P1')
    Left = 32
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDuplNFData: TDateField
      FieldName = 'Data'
      Origin = 'lanctos.Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDuplNFControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'lanctos.Controle'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrDuplNFDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'lanctos.Descricao'
      Size = 100
    end
    object QrDuplNFCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'lanctos.Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDuplNFDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'lanctos.Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDuplNFNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'lanctos.NotaFiscal'
      DisplayFormat = '000000;-000000; '
    end
    object QrDuplNFCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'lanctos.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDuplNFMez: TIntegerField
      FieldName = 'Mez'
      Origin = 'lanctos.Mez'
      Required = True
    end
    object QrDuplNFFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Origin = 'lanctos.Fornecedor'
    end
    object QrDuplNFCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'lanctos.Cliente'
    end
    object QrDuplNFNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrDuplNFNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrDuplNFNOMEFNC: TWideStringField
      FieldName = 'NOMEFNC'
      Size = 100
    end
    object QrDuplNFDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'lanctos.Documento'
    end
    object QrDuplNFSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Origin = 'lanctos.SerieCH'
      Size = 10
    end
    object QrDuplNFCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'lanctos.Carteira'
      Required = True
    end
    object QrDuplNFTERCEIRO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TERCEIRO'
      Size = 255
      Calculated = True
    end
    object QrDuplNFMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 8
      Calculated = True
    end
    object QrDuplNFCHEQUE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CHEQUE'
      Calculated = True
    end
    object QrDuplNFCOMP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMP_TXT'
      Size = 10
      Calculated = True
    end
  end
  object DsDuplCH: TDataSource
    DataSet = QrDuplCH
    Left = 92
    Top = 196
  end
  object QrDuplCH: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDuplCHCalcFields
    SQL.Strings = (
      'SELECT lan.Data, lan.Controle, lan.Descricao, lan.Credito,'
      'lan.Debito, lan.NotaFiscal, lan.Compensado, lan.Mez,'
      'lan.Fornecedor, lan.Cliente, car.Nome NOMECART,'
      'lan.Documento, lan.SerieCH, lan.Carteira,'
      'IF(lan.Cliente<>0, CASE WHEN cli.Tipo=0 THEN '
      'cli.RazaoSocial ELSE cli.Nome END, "") NOMECLI,'
      'IF(lan.Fornecedor<>0, CASE WHEN fnc.Tipo=0 THEN '
      'fnc.RazaoSocial ELSE fnc.Nome END, "") NOMEFNC'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN entidades cli ON cli.Codigo=lan.Cliente'
      'LEFT JOIN entidades fnc ON fnc.Codigo=lan.Fornecedor'
      'WHERE ID_Pgto = 0')
    Left = 32
    Top = 196
    object QrDuplCHData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDuplCHControle: TIntegerField
      FieldName = 'Controle'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrDuplCHDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrDuplCHCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDuplCHDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDuplCHNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      DisplayFormat = '000000;-000000; '
    end
    object QrDuplCHCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDuplCHMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrDuplCHFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrDuplCHCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrDuplCHNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Size = 100
    end
    object QrDuplCHNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrDuplCHNOMEFNC: TWideStringField
      FieldName = 'NOMEFNC'
      Size = 100
    end
    object QrDuplCHDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrDuplCHSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrDuplCHCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrDuplCHTERCEIRO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TERCEIRO'
      Size = 255
      Calculated = True
    end
    object QrDuplCHMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 8
      Calculated = True
    end
    object QrDuplCHCHEQUE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CHEQUE'
      Calculated = True
    end
    object QrDuplCHCOMP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMP_TXT'
      Size = 10
      Calculated = True
    end
  end
  object QrVerifHis: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cta.Codigo, cta.Nome, his.SumCre, his.SumDeb,'
      'his.SdoFim, his.SumCre - his.SumDeb Valor'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto'
      ''
      'WHERE his.Entidade=:P0'
      'AND his.Periodo=:P1'
      'AND (SumCre < 0 OR'
      '     SumDeb<0)'
      'GROUP BY cta.Codigo')
    Left = 152
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrVerifHisCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrVerifHisNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrVerifHisSumCre: TFloatField
      FieldName = 'SumCre'
      Required = True
    end
    object QrVerifHisSumDeb: TFloatField
      FieldName = 'SumDeb'
      Required = True
    end
    object QrVerifHisSdoFim: TFloatField
      FieldName = 'SdoFim'
      Required = True
    end
    object QrVerifHisValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
  end
  object DsVerifHis: TDataSource
    DataSet = QrVerifHis
    Left = 152
    Top = 52
  end
  object QrSNC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pla.Codigo, pla.Nome NOME, SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE,'
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM,'
      'pla.Codigo CodPla, cjt.Codigo CodCjt,'
      'gru.Codigo CodGru, sgr.Codigo CodSgr'
      'FROM contashis his'
      'LEFT JOIN contas    cta ON cta.Codigo=his.Codigo'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto'
      'LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano'
      'LEFT JOIN contasniv niv ON pla.Codigo=niv.Genero'
      'WHERE his.Entidade=:P0'
      'AND his.Periodo=:P1'
      'AND niv.Nivel=5 AND niv.Entidade=:P2'
      ''
      'GROUP BY pla.Codigo'
      'ORDER BY pla.OrdemLista, pla.Nome')
    Left = 152
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSNCNOME: TWideStringField
      FieldName = 'NOME'
      Origin = 'plano.Nome'
      Size = 50
    end
    object QrSNCSUMMOV: TFloatField
      FieldName = 'SUMMOV'
    end
    object QrSNCSDOANT: TFloatField
      FieldName = 'SDOANT'
    end
    object QrSNCSUMCRE: TFloatField
      FieldName = 'SUMCRE'
    end
    object QrSNCSUMDEB: TFloatField
      FieldName = 'SUMDEB'
    end
    object QrSNCSDOFIM: TFloatField
      FieldName = 'SDOFIM'
    end
    object QrSNCCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'plano.Codigo'
    end
    object QrSNCCodPla: TIntegerField
      FieldName = 'CodPla'
      Origin = 'plano.Codigo'
      Required = True
    end
    object QrSNCCodCjt: TIntegerField
      FieldName = 'CodCjt'
      Origin = 'conjuntos.Codigo'
      Required = True
    end
    object QrSNCCodGru: TIntegerField
      FieldName = 'CodGru'
      Origin = 'grupos.Codigo'
      Required = True
    end
    object QrSNCCodSgr: TIntegerField
      FieldName = 'CodSgr'
      Origin = 'subgrupos.Codigo'
      Required = True
    end
  end
  object frxDsSNG: TfrxDBDataset
    UserName = 'frxDsSNG'
    CloseDataSource = False
    DataSet = QrSNG
    BCDToCurrency = False
    Left = 152
    Top = 196
  end
  object QrSNG: TmySQLQuery
    Database = Dmod.ZZDB
    OnCalcFields = QrSNGCalcFields
    SQL.Strings = (
      'SELECT 0 KGT, sn.*'
      'FROM sdoniveis sn'
      'ORDER BY sn.Ordena')
    Left = 152
    Top = 148
    object QrSNGNivel: TIntegerField
      FieldName = 'Nivel'
      Origin = 'sdoniveis.Nivel'
    end
    object QrSNGGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'sdoniveis.Genero'
    end
    object QrSNGNomeGe: TWideStringField
      FieldName = 'NomeGe'
      Origin = 'sdoniveis.NomeGe'
      Size = 100
    end
    object QrSNGNomeNi: TWideStringField
      FieldName = 'NomeNi'
      Origin = 'sdoniveis.NomeNi'
    end
    object QrSNGSumMov: TFloatField
      FieldName = 'SumMov'
      Origin = 'sdoniveis.SumMov'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSNGSdoAnt: TFloatField
      FieldName = 'SdoAnt'
      Origin = 'sdoniveis.SdoAnt'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSNGSumCre: TFloatField
      FieldName = 'SumCre'
      Origin = 'sdoniveis.SumCre'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSNGSumDeb: TFloatField
      FieldName = 'SumDeb'
      Origin = 'sdoniveis.SumDeb'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSNGSdoFim: TFloatField
      FieldName = 'SdoFim'
      Origin = 'sdoniveis.SdoFim'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSNGCtrla: TSmallintField
      FieldName = 'Ctrla'
      Origin = 'sdoniveis.Ctrla'
    end
    object QrSNGSeleci: TSmallintField
      FieldName = 'Seleci'
      Origin = 'sdoniveis.Seleci'
    end
    object QrSNGCodPla: TIntegerField
      FieldName = 'CodPla'
    end
    object QrSNGCodCjt: TIntegerField
      FieldName = 'CodCjt'
    end
    object QrSNGCodGru: TIntegerField
      FieldName = 'CodGru'
    end
    object QrSNGCodSgr: TIntegerField
      FieldName = 'CodSgr'
    end
    object QrSNGCODIGOS_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CODIGOS_TXT'
      Size = 50
      Calculated = True
    end
    object QrSNGOrdena: TWideStringField
      FieldName = 'Ordena'
      Size = 100
    end
    object QrSNGKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
  end
  object QrCTCN: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM contasniv'
      'WHERE Entidade=:P0')
    Left = 152
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrSTCP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(SumCre-SumDeb) SUMMOV,'
      'SUM(his.SdoAnt) SDOANT, SUM(SumCre) SUMCRE,'
      'SUM(his.SumDeb) SUMDEB, SUM(SdoFim) SDOFIM'
      'FROM contashis his'
      'WHERE his.Entidade=:P0'
      'AND his.Periodo=:P1')
    Left = 152
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSTCPSUMMOV: TFloatField
      FieldName = 'SUMMOV'
    end
    object QrSTCPSDOANT: TFloatField
      FieldName = 'SDOANT'
    end
    object QrSTCPSUMCRE: TFloatField
      FieldName = 'SUMCRE'
    end
    object QrSTCPSUMDEB: TFloatField
      FieldName = 'SUMDEB'
    end
    object QrSTCPSDOFIM: TFloatField
      FieldName = 'SDOFIM'
    end
  end
  object frxDsSTCP: TfrxDBDataset
    UserName = 'frxDsSTCP'
    CloseDataSource = False
    DataSet = QrSTCP
    BCDToCurrency = False
    Left = 152
    Top = 340
  end
  object QrUDAPC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'UPDATE entidades SET'
      'AltDtPlaCt=:P0'
      'WHERE Codigo=:P1'
      'AND AltDtPlaCt > :P2')
    Left = 32
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrTransf: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data, Tipo, Carteira, Controle, Sub, Genero, '
      'Debito, Credito, Documento, SerieCH '
      'FROM lanctos'
      'WHERE Controle=:P0'
      '')
    Left = 92
    Top = 292
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTransfData: TDateField
      FieldName = 'Data'
    end
    object QrTransfTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrTransfCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrTransfControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTransfGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrTransfDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrTransfCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrTransfDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrTransfSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrTransfSub: TSmallintField
      FieldName = 'Sub'
    end
  end
  object DsCartSum: TDataSource
    DataSet = QrCartSum
    Left = 92
    Top = 388
  end
  object QrCartSum: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Saldo) SALDO, SUM(FuturoC) FuturoC,'
      'SUM(FuturoD) FuturoD, SUM(FuturoS) FuturoS, '
      'SUM(EmCaixa) EmCaixa, SUM(EmCaixa-Saldo) Difere,'
      'SUM(Saldo+FuturoS) SDO_FUT'
      'FROM carteiras ca'
      'WHERE ca.ForneceI in (1,2,3,4)'
      '')
    Left = 32
    Top = 388
    object QrCartSumSALDO: TFloatField
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartSumFuturoC: TFloatField
      FieldName = 'FuturoC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartSumFuturoD: TFloatField
      FieldName = 'FuturoD'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartSumFuturoS: TFloatField
      FieldName = 'FuturoS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartSumEmCaixa: TFloatField
      FieldName = 'EmCaixa'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartSumDifere: TFloatField
      FieldName = 'Difere'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartSumSDO_FUT: TFloatField
      FieldName = 'SDO_FUT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object QrCarts: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrCartsBeforeClose
    AfterClose = QrCartsAfterClose
    AfterScroll = QrCartsAfterScroll
    OnCalcFields = QrCartsCalcFields
    SQL.Strings = (
      'SELECT DISTINCT ca.*, ba.Nome NOMEDOBANCO, '
      'CASE WHEN en.Tipo=0 THEN en.RazaoSocial '
      'ELSE en.Nome END NOMEFORNECEI '
      'FROM carteiras ca '
      'LEFT JOIN carteiras ba ON ba.Codigo=ca.Banco '
      'LEFT JOIN entidades en ON en.Codigo=ca.ForneceI '
      'WHERE ca.ForneceI in (-11)'
      'ORDER BY ca.Nome')
    Left = 32
    Top = 340
    object QrCartsDIFERENCA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DIFERENCA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrCartsTIPOPRAZO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOPRAZO'
      Size = 1
      Calculated = True
    end
    object QrCartsNOMEPAGREC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEPAGREC'
      Size = 50
      Calculated = True
    end
    object QrCartsNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrCartsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'carteiras.Codigo'
      DisplayFormat = '000'
    end
    object QrCartsTipo: TIntegerField
      FieldName = 'Tipo'
      Origin = 'carteiras.Tipo'
    end
    object QrCartsNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrCartsInicial: TFloatField
      FieldName = 'Inicial'
      Origin = 'carteiras.Inicial'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartsBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'carteiras.Banco'
    end
    object QrCartsID: TWideStringField
      FieldName = 'ID'
      Origin = 'carteiras.ID'
      Size = 100
    end
    object QrCartsFatura: TWideStringField
      FieldName = 'Fatura'
      Origin = 'carteiras.Fatura'
      FixedChar = True
      Size = 1
    end
    object QrCartsID_Fat: TWideStringField
      FieldName = 'ID_Fat'
      Origin = 'carteiras.ID_Fat'
      Size = 50
    end
    object QrCartsSaldo: TFloatField
      FieldName = 'Saldo'
      Origin = 'carteiras.Saldo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'carteiras.Lk'
    end
    object QrCartsEmCaixa: TFloatField
      FieldName = 'EmCaixa'
      Origin = 'carteiras.EmCaixa'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartsFechamento: TIntegerField
      FieldName = 'Fechamento'
      Origin = 'carteiras.Fechamento'
    end
    object QrCartsPrazo: TSmallintField
      FieldName = 'Prazo'
      Origin = 'carteiras.Prazo'
    end
    object QrCartsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'carteiras.DataCad'
    end
    object QrCartsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'carteiras.DataAlt'
    end
    object QrCartsUserCad: TSmallintField
      FieldName = 'UserCad'
      Origin = 'carteiras.UserCad'
    end
    object QrCartsUserAlt: TSmallintField
      FieldName = 'UserAlt'
      Origin = 'carteiras.UserAlt'
    end
    object QrCartsPagRec: TSmallintField
      FieldName = 'PagRec'
      Origin = 'carteiras.PagRec'
    end
    object QrCartsFuturoC: TFloatField
      FieldName = 'FuturoC'
      Origin = 'carteiras.FuturoC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartsFuturoD: TFloatField
      FieldName = 'FuturoD'
      Origin = 'carteiras.FuturoD'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartsFuturoS: TFloatField
      FieldName = 'FuturoS'
      Origin = 'carteiras.FuturoS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCartsNOMEDOBANCO: TWideStringField
      FieldName = 'NOMEDOBANCO'
      Size = 100
    end
    object QrCartsDiaMesVence: TSmallintField
      FieldName = 'DiaMesVence'
    end
    object QrCartsExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
    end
    object QrCartsForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrCartsNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 100
    end
    object QrCartsContato1: TWideStringField
      FieldName = 'Contato1'
      Size = 100
    end
    object QrCartsTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
    object QrCartsBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrCartsAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrCartsConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrCartsCheque1: TIntegerField
      FieldName = 'Cheque1'
    end
    object QrCartsAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrCartsContab: TWideStringField
      FieldName = 'Contab'
    end
    object QrCartsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrCartsForneceN: TSmallintField
      FieldName = 'ForneceN'
    end
    object QrCartsExclusivo: TSmallintField
      FieldName = 'Exclusivo'
    end
    object QrCartsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCartsRecebeBloq: TSmallintField
      FieldName = 'RecebeBloq'
    end
    object QrCartsEntiDent: TIntegerField
      FieldName = 'EntiDent'
    end
    object QrCartsCodCedente: TWideStringField
      FieldName = 'CodCedente'
    end
    object QrCartsValMorto: TFloatField
      FieldName = 'ValMorto'
    end
    object QrCartsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCartsIgnorSerie: TSmallintField
      FieldName = 'IgnorSerie'
    end
  end
  object DsCarts: TDataSource
    DataSet = QrCarts
    Left = 92
    Top = 340
  end
  object QrLctos: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrLctosBeforeClose
    OnCalcFields = QrLctosCalcFields
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2, '
      '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, '
      'la.*, ct.Codigo CONTA, ca.Prazo,'
      'ca.Banco1, ca.Agencia1, ca.Conta1, '
      'ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'ca.Nome NO_Carteira,'
      'CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial '
      'ELSE em.Nome END NOMEEMPRESA, '
      'CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial '
      'ELSE cl.Nome END NOMECLIENTE,'
      'CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial '
      'ELSE fo.Nome END NOMEFORNECEDOR,'
      'CASE 1 WHEN fi.Tipo=0 THEN fi.RazaoSocial '
      'ELSE fi.Nome END NOMEFORNECEI,'
      
        'ELT(la.Endossas, "Endossado", "Endossante", "Ambos", "? ? ? ?") ' +
        'NO_ENDOSSADO '
      'FROM lanctos la'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      
        'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = ' +
        '0'
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo'
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'LEFT JOIN entidades fi ON fi.Codigo=la.ForneceI'
      'ORDER BY la.Data, la.Controle')
    Left = 32
    Top = 436
    object QrLctosData: TDateField
      FieldName = 'Data'
      Origin = 'lanctos.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctosTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'lanctos.Tipo'
    end
    object QrLctosCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'lanctos.Carteira'
    end
    object QrLctosAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      Origin = 'lanctos.Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrLctosGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'lanctos.Genero'
    end
    object QrLctosDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'lanctos.Descricao'
      Size = 25
    end
    object QrLctosNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'lanctos.NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrLctosDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'lanctos.Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctosCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'lanctos.Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctosCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'lanctos.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctosDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'lanctos.Documento'
      DisplayFormat = '000000; ; '
    end
    object QrLctosSit: TIntegerField
      FieldName = 'Sit'
      Origin = 'lanctos.Sit'
    end
    object QrLctosVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'lanctos.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctosLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'lanctos.Lk'
    end
    object QrLctosFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'lanctos.FatID'
    end
    object QrLctosFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'lanctos.FatParcela'
    end
    object QrLctosCONTA: TIntegerField
      FieldName = 'CONTA'
      Origin = 'contas.Codigo'
    end
    object QrLctosNOMECONTA: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      FixedChar = True
      Size = 50
    end
    object QrLctosNOMEEMPRESA: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrLctosNOMESUBGRUPO: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMESUBGRUPO'
      Origin = 'subgrupos.Nome'
      Size = 50
    end
    object QrLctosNOMEGRUPO: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMEGRUPO'
      Origin = 'grupos.Nome'
      Size = 50
    end
    object QrLctosNOMECONJUNTO: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMECONJUNTO'
      Origin = 'conjuntos.Nome'
      Size = 50
    end
    object QrLctosNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrLctosAno: TFloatField
      FieldName = 'Ano'
    end
    object QrLctosMENSAL: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrLctosMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrLctosBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'lanctos.Banco'
    end
    object QrLctosLocal: TIntegerField
      FieldName = 'Local'
      Origin = 'lanctos.Local'
    end
    object QrLctosFatura: TWideStringField
      FieldName = 'Fatura'
      Origin = 'lanctos.Fatura'
      FixedChar = True
      Size = 1
    end
    object QrLctosSub: TSmallintField
      FieldName = 'Sub'
      Origin = 'lanctos.Sub'
      DisplayFormat = '00; ; '
    end
    object QrLctosCartao: TIntegerField
      FieldName = 'Cartao'
      Origin = 'lanctos.Cartao'
    end
    object QrLctosLinha: TIntegerField
      FieldName = 'Linha'
      Origin = 'lanctos.Linha'
    end
    object QrLctosPago: TFloatField
      FieldName = 'Pago'
      Origin = 'lanctos.Pago'
    end
    object QrLctosSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrLctosID_Sub: TSmallintField
      FieldName = 'ID_Sub'
      Origin = 'lanctos.ID_Sub'
    end
    object QrLctosMez: TIntegerField
      FieldName = 'Mez'
      Origin = 'lanctos.Mez'
    end
    object QrLctosFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Origin = 'lanctos.Fornecedor'
    end
    object QrLctoscliente: TIntegerField
      FieldName = 'cliente'
      Origin = 'lanctos.Cliente'
    end
    object QrLctosMoraDia: TFloatField
      FieldName = 'MoraDia'
      Origin = 'lanctos.MoraDia'
    end
    object QrLctosNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrLctosNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrLctosTIPOEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrLctosNOMERELACIONADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMERELACIONADO'
      Size = 50
      Calculated = True
    end
    object QrLctosOperCount: TIntegerField
      FieldName = 'OperCount'
      Origin = 'lanctos.OperCount'
    end
    object QrLctosLancto: TIntegerField
      FieldName = 'Lancto'
      Origin = 'lanctos.Lancto'
    end
    object QrLctosMulta: TFloatField
      FieldName = 'Multa'
      Origin = 'lanctos.Multa'
    end
    object QrLctosATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrLctosJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLctosDataDoc: TDateField
      FieldName = 'DataDoc'
      Origin = 'lanctos.DataDoc'
    end
    object QrLctosNivel: TIntegerField
      FieldName = 'Nivel'
      Origin = 'lanctos.Nivel'
    end
    object QrLctosVendedor: TIntegerField
      FieldName = 'Vendedor'
      Origin = 'lanctos.Vendedor'
    end
    object QrLctosAccount: TIntegerField
      FieldName = 'Account'
      Origin = 'lanctos.Account'
    end
    object QrLctosMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrLctosProtesto: TDateField
      FieldName = 'Protesto'
      Origin = 'lanctos.Protesto'
    end
    object QrLctosDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'lanctos.DataCad'
    end
    object QrLctosDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'lanctos.DataAlt'
    end
    object QrLctosUserCad: TSmallintField
      FieldName = 'UserCad'
      Origin = 'lanctos.UserCad'
    end
    object QrLctosUserAlt: TSmallintField
      FieldName = 'UserAlt'
      Origin = 'lanctos.UserAlt'
    end
    object QrLctosControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'lanctos.Controle'
      Required = True
    end
    object QrLctosID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Origin = 'lanctos.ID_Pgto'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrLctosCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Origin = 'lanctos.CtrlIni'
      Required = True
    end
    object QrLctosFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
      Origin = 'lanctos.FatID_Sub'
    end
    object QrLctosICMS_P: TFloatField
      FieldName = 'ICMS_P'
      Origin = 'lanctos.ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctosICMS_V: TFloatField
      FieldName = 'ICMS_V'
      Origin = 'lanctos.ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctosDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Origin = 'lanctos.Duplicata'
      Size = 10
    end
    object QrLctosCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrLctosCliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'lanctos.CliInt'
    end
    object QrLctosDepto: TIntegerField
      FieldName = 'Depto'
      Origin = 'lanctos.Depto'
    end
    object QrLctosDescoPor: TIntegerField
      FieldName = 'DescoPor'
      Origin = 'lanctos.DescoPor'
    end
    object QrLctosPrazo: TSmallintField
      FieldName = 'Prazo'
      Origin = 'carteiras.Prazo'
    end
    object QrLctosForneceI: TIntegerField
      FieldName = 'ForneceI'
      Origin = 'lanctos.ForneceI'
    end
    object QrLctosQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'lanctos.Qtde'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrLctosEmitente: TWideStringField
      FieldName = 'Emitente'
      Origin = 'lanctos.Emitente'
      Size = 30
    end
    object QrLctosContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Origin = 'lanctos.ContaCorrente'
      Size = 15
    end
    object QrLctosCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Origin = 'lanctos.CNPJCPF'
      Size = 15
    end
    object QrLctosDescoVal: TFloatField
      FieldName = 'DescoVal'
      Origin = 'lanctos.DescoVal'
    end
    object QrLctosDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Origin = 'lanctos.DescoControle'
      Required = True
    end
    object QrLctosUnidade: TIntegerField
      FieldName = 'Unidade'
      Origin = 'lanctos.Unidade'
      Required = True
    end
    object QrLctosNFVal: TFloatField
      FieldName = 'NFVal'
      Origin = 'lanctos.NFVal'
      Required = True
    end
    object QrLctosAntigo: TWideStringField
      FieldName = 'Antigo'
      Origin = 'lanctos.Antigo'
    end
    object QrLctosExcelGru: TIntegerField
      FieldName = 'ExcelGru'
      Origin = 'lanctos.ExcelGru'
    end
    object QrLctosSerieCH: TWideStringField
      DisplayWidth = 10
      FieldName = 'SerieCH'
      Origin = 'lanctos.SerieCH'
      Size = 10
    end
    object QrLctosSERIE_CHEQUE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SERIE_CHEQUE'
      Calculated = True
    end
    object QrLctosDoc2: TWideStringField
      FieldName = 'Doc2'
      Origin = 'lanctos.Doc2'
    end
    object QrLctosNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrLctosMoraVal: TFloatField
      FieldName = 'MoraVal'
      Origin = 'lanctos.MoraVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctosMultaVal: TFloatField
      FieldName = 'MultaVal'
      Origin = 'lanctos.MultaVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctosCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
      Origin = 'lanctos.CNAB_Sit'
    end
    object QrLctosBanco1: TIntegerField
      FieldName = 'Banco1'
      Origin = 'carteiras.Banco1'
    end
    object QrLctosAgencia1: TIntegerField
      FieldName = 'Agencia1'
      Origin = 'carteiras.Agencia1'
    end
    object QrLctosConta1: TWideStringField
      FieldName = 'Conta1'
      Origin = 'carteiras.Conta1'
      Size = 15
    end
    object QrLctosTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Origin = 'lanctos.TipoCH'
      Required = True
    end
    object QrLctosFatNum: TFloatField
      FieldName = 'FatNum'
      Origin = 'lanctos.FatNum'
    end
    object QrLctosSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Origin = 'lanctos.SerieNF'
      Size = 5
    end
    object QrLctosNO_ENDOSSADO: TWideStringField
      FieldName = 'NO_ENDOSSADO'
      Size = 10
    end
    object QrLctosNO_Carteira: TWideStringField
      FieldName = 'NO_Carteira'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrLctosUH: TWideStringField
      FieldName = 'UH'
      Size = 10
    end
    object QrLctosID_Quit: TIntegerField
      FieldName = 'ID_Quit'
    end
    object QrLctosReparcel: TIntegerField
      FieldName = 'Reparcel'
    end
    object QrLctosAtrelado: TIntegerField
      FieldName = 'Atrelado'
    end
    object QrLctosPagMul: TFloatField
      FieldName = 'PagMul'
    end
    object QrLctosPagJur: TFloatField
      FieldName = 'PagJur'
    end
    object QrLctosSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
    end
    object QrLctosMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
    end
    object QrLctosProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrLctosCtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
    end
    object QrLctosEndossas: TSmallintField
      FieldName = 'Endossas'
    end
    object QrLctosEndossan: TFloatField
      FieldName = 'Endossan'
    end
    object QrLctosEndossad: TFloatField
      FieldName = 'Endossad'
    end
    object QrLctosCancelado: TSmallintField
      FieldName = 'Cancelado'
    end
    object QrLctosEventosCad: TIntegerField
      FieldName = 'EventosCad'
    end
    object QrLctosEncerrado: TIntegerField
      FieldName = 'Encerrado'
    end
    object QrLctosErrCtrl: TIntegerField
      FieldName = 'ErrCtrl'
    end
    object QrLctosAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrLctosAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrLctosIndiPag: TIntegerField
      FieldName = 'IndiPag'
    end
    object QrLctosAgencia: TIntegerField
      FieldName = 'Agencia'
    end
  end
  object DsLctos: TDataSource
    DataSet = QrLctos
    Left = 92
    Top = 436
  end
  object QrSomaM: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT (SUM(Credito) - SUM(Debito)) Valor'
      'FROM lanctos')
    Left = 152
    Top = 388
    object QrSomaMValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrLocCta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Mensal'
      'FROM contas'
      'WHERE Codigo=:P0')
    Left = 152
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocCtaMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
  end
  object QrLPE: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.ForneceI CLIENTE_INTERNO, la2.Controle, la2.Data, '
      'la2.Descricao, la2.Credito, la2.Debito, la2.Carteira, la2.Tipo,'
      'IF(clii.Tipo=0, clii.RazaoSocial, clii.Nome) NOMECLIINT'
      'FROM lanctos lan'
      'LEFT JOIN lanctos la2 ON la2.ID_Pgto=lan.Controle'
      'LEFT JOIN carteiras car ON car.Codigo=la2.Carteira'
      'LEFT JOIN entidades clii ON clii.Codigo=car.ForneceI'
      'WHERE lan.Controle=:P0')
    Left = 212
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLPEControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLPEData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLPEDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLPECredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLPEDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLPECarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrLPETipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrLPECLIENTE_INTERNO: TIntegerField
      FieldName = 'CLIENTE_INTERNO'
    end
    object QrLPENOMECLIINT: TWideStringField
      FieldName = 'NOMECLIINT'
      Size = 100
    end
  end
  object DsLPE: TDataSource
    DataSet = QrLPE
    Left = 272
    Top = 4
  end
  object QrAPL: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Credito) Credito, SUM(Debito) Debito, '
      'SUM(MoraVal) MoraVal, SUM(MultaVal) MultaVal,'
      'MAX(Data) Data, COUNT(Controle) Itens '
      'FROM lanctos WHERE ID_Pgto=:P0')
    Left = 456
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAPLCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrAPLDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrAPLMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrAPLMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrAPLData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrAPLItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrVLA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Credito, Debito, Endossan, Endossad, Tipo'
      'FROM lanctos '
      'WHERE Sub=0 '
      'AND Controle=:P0')
    Left = 456
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVLACredito: TFloatField
      FieldName = 'Credito'
    end
    object QrVLADebito: TFloatField
      FieldName = 'Debito'
    end
    object QrVLAEndossan: TFloatField
      FieldName = 'Endossan'
    end
    object QrVLAEndossad: TFloatField
      FieldName = 'Endossad'
    end
    object QrVLATipo: TSmallintField
      FieldName = 'Tipo'
    end
  end
  object QrErrQuit: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lan.Controle Controle1, la2.Controle Controle2, '
      'la2.Data, lan.Sit, lan.Descricao'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN lanctos   la2 ON la2.ID_pgto=lan.Controle'
      ''
      'WHERE lan.Tipo=2'
      'AND lan.Sit=2'
      'AND lan.Credito-lan.Pago = 0'
      'AND lan.Controle<>la2.Controle'
      'AND lan.ID_Quit=0'
      '')
    Left = 272
    Top = 52
    object QrErrQuitControle1: TIntegerField
      FieldName = 'Controle1'
      Required = True
    end
    object QrErrQuitControle2: TIntegerField
      FieldName = 'Controle2'
      Required = True
    end
    object QrErrQuitData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrErrQuitSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrErrQuitDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
  end
  object QrPesqCart: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Tipo'
      'FROM carteiras'
      'WHERE Codigo=:P0')
    Left = 212
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqCartTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object QrCambioDia: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Valor'
      'FROM cambiocot cot'
      'WHERE cot.DataC=:P0'
      '')
    Left = 212
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCambioDiaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCambioDiaValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
  end
  object QrCambioUsu: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM cambiousu'
      'WHERE Codigo=:P0'
      ''
      '')
    Left = 212
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCambioUsuCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrTrfCtas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data, Tipo, Carteira, Controle, Sub, Genero, '
      'Debito, Credito, Documento, SerieCH '
      'FROM lanctos'
      'WHERE Controle=:P0')
    Left = 152
    Top = 484
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTrfCtasData: TDateField
      FieldName = 'Data'
    end
    object QrTrfCtasTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrTrfCtasCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrTrfCtasControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTrfCtasGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrTrfCtasDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrTrfCtasCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrTrfCtasDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrTrfCtasSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrTrfCtasSub: TSmallintField
      FieldName = 'Sub'
    end
  end
  object QrEndossan: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Valor) Valor'
      'FROM lctoendoss'
      'WHERE OriCtrl=:P0 AND OriSub=:P1')
    Left = 456
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEndossanValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrSomaLinhas: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSomaLinhasCalcFields
    SQL.Strings = (
      'SELECT SUM(Credito) CREDITO, SUM(Debito) DEBITO'
      'FROM lanctos la')
    Left = 32
    Top = 484
    object QrSomaLinhasCREDITO: TFloatField
      FieldName = 'CREDITO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSomaLinhasDEBITO: TFloatField
      FieldName = 'DEBITO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSomaLinhasSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object DsSomaLinhas: TDataSource
    DataSet = QrSomaLinhas
    Left = 92
    Top = 484
  end
  object QrSumBol: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Valor) VALOR'
      'FROM arreits ari'
      'LEFT JOIN condimov cdi ON cdi.Conta=ari.Apto'
      'LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet'
      'WHERE ari.Codigo=:P0'
      'AND ari.Boleto <> 0'
      ''
      'UNION'
      ''
      'SELECT SUM(Valor) VALOR'
      'FROM consits  cni '
      'LEFT JOIN condimov  cdi ON cdi.Conta=cni.Apto'
      'LEFT JOIN entidades ent ON ent.Codigo=cni.Propriet'
      'WHERE cni.Cond=:P1'
      'AND cni.Periodo=:P2'
      'AND cni.Boleto <> 0')
    Left = 272
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSumBolVALOR: TFloatField
      FieldName = 'VALOR'
    end
  end
  object frxDsExtratos: TfrxDBDataset
    UserName = 'frxDsExtratos'
    CloseDataSource = False
    DataSet = QrExtratos
    BCDToCurrency = False
    Left = 576
    Top = 4
  end
  object frxDsSdoCtas: TfrxDBDataset
    UserName = 'frxDsSdoCtas'
    CloseDataSource = False
    DataSet = QrSdoCtas
    BCDToCurrency = False
    Left = 636
    Top = 100
  end
  object QrPesqAgr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT con.ContasAgr, car.Nome NOMECART, car.Nome2 NOME2CART, '
      'car.Ordem OrdemCart, car.Tipo TipoCart,'
      'lan.Carteira, lan.Data, lan.Descricao, '
      'SUM(lan.Credito) CREDITO, '
      'SUM(lan.Debito) DEBITO, lan.SerieCH, lan.Documento, lan.Doc2, '
      'lan.Mez, lan.NotaFiscal,'
      'lan.Depto Apto, cag.Nome NOMEAGR, cag.InfoDescri, cag.Mensal,'
      'IF(cli.Tipo=0,cli.RazaoSocial,cli.Nome) NOMECLI'
      '/*,imv.Unidade UH,*/ '
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN contas    con ON con.Codigo=lan.Genero'
      'LEFT JOIN contasagr cag ON cag.Codigo=con.ContasAgr'
      '/*LEFT JOIN condimov  imv ON imv.Conta=lan.Depto*/'
      'LEFT JOIN entidades cli ON cli.Codigo=lan.Cliente'
      'WHERE car.Tipo <> 2'
      'AND con.ContasAgr>0'
      'AND car.ForneceI=:P0'
      'AND lan.Data BETWEEN :P1 AND :P2'
      'GROUP BY con.ContasAgr, lan.Data, lan.Cliente, lan.Fornecedor,'
      'lan.CliInt, lan.Depto, lan.Mez, lan.Carteira'
      'ORDER BY car.Nome, car.Codigo, lan.Data, lan.Controle, '
      'lan.Credito DESC, lan.Debito'
      '')
    Left = 212
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPesqAgrContasAgr: TIntegerField
      FieldName = 'ContasAgr'
    end
    object QrPesqAgrNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Size = 100
    end
    object QrPesqAgrNOME2CART: TWideStringField
      FieldName = 'NOME2CART'
      Size = 100
    end
    object QrPesqAgrCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrPesqAgrData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrPesqAgrDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPesqAgrCREDITO: TFloatField
      FieldName = 'CREDITO'
    end
    object QrPesqAgrDEBITO: TFloatField
      FieldName = 'DEBITO'
    end
    object QrPesqAgrSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrPesqAgrDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPesqAgrDoc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrPesqAgrMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrPesqAgrApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrPesqAgrNOMEAGR: TWideStringField
      FieldName = 'NOMEAGR'
      Size = 50
    end
    object QrPesqAgrInfoDescri: TSmallintField
      FieldName = 'InfoDescri'
    end
    object QrPesqAgrMensal: TSmallintField
      FieldName = 'Mensal'
    end
    object QrPesqAgrNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrPesqAgrOrdemCart: TIntegerField
      FieldName = 'OrdemCart'
    end
    object QrPesqAgrTipoCart: TIntegerField
      FieldName = 'TipoCart'
      Required = True
    end
    object QrPesqAgrNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
  end
  object QrPesqCar: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Codigo Carteira, car.Inicial, car.Nome, '
      'car.Nome2, car.Ordem OrdemCart, car.Tipo TipoCart,'
      '('
      '  SELECT SUM(lan.Credito-lan.Debito)'
      '  FROM lanctos lan'
      '  WHERE lan.Carteira=car.Codigo'
      '  AND lan.Data < :P0'
      ') SALDO'
      'FROM carteiras car'
      'WHERE car.ForneceI=:P1 '
      'AND car.Tipo <> 2'
      '')
    Left = 212
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPesqCarCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrPesqCarInicial: TFloatField
      FieldName = 'Inicial'
    end
    object QrPesqCarSALDO: TFloatField
      FieldName = 'SALDO'
    end
    object QrPesqCarNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrPesqCarNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 100
    end
    object QrPesqCarOrdemCart: TIntegerField
      FieldName = 'OrdemCart'
      Required = True
    end
    object QrPesqCarTipoCart: TIntegerField
      FieldName = 'TipoCart'
      Required = True
    end
  end
  object QrPesqRes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT car.Nome NOMECART, car.Nome2 NOME2CART, car.Tipo TipoCart' +
        ','
      
        'car.Ordem OrdemCart, lan.Carteira, lan.Data, lan.Descricao, lan.' +
        'Credito,'
      
        'lan.Debito, lan.Mez, lan.SerieCH, lan.Documento, lan.Depto, lan.' +
        'NotaFiscal'
      '/*, imv.Unidade*/'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN contas    con ON con.Codigo=lan.Genero'
      '/*LEFT JOIN condimov  imv ON imv.Conta=lan.Depto*/'
      ''
      'WHERE car.Tipo <> 2'
      'AND con.ContasAgr=0'
      'AND con.ContasSum=0'
      'AND car.ForneceI=:P0'
      'AND lan.Data BETWEEN :P1 AND :P2'
      ' ')
    Left = 272
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPesqResNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Size = 100
    end
    object QrPesqResNOME2CART: TWideStringField
      FieldName = 'NOME2CART'
      Size = 100
    end
    object QrPesqResCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrPesqResData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrPesqResDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPesqResCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrPesqResDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrPesqResMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrPesqResSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrPesqResDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPesqResOrdemCart: TIntegerField
      FieldName = 'OrdemCart'
    end
    object QrPesqResDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrPesqResTipoCart: TIntegerField
      FieldName = 'TipoCart'
      Required = True
    end
    object QrPesqResNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
  end
  object QrPesqSum: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT con.ContasAgr, car.Nome NOMECART, car.Nome2 NOME2CART, ca' +
        'r.Tipo TipoCart,'
      
        'car.Ordem OrdemCart, lan.Carteira, lan.Data, lan.Descricao, lan.' +
        'NotaFiscal,'
      'SUM(lan.Credito) CREDITO, SUM(lan.Debito) DEBITO, lan.Mez'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN contas    con ON con.Codigo=lan.Genero'
      '/*LEFT JOIN contassum csu ON csu.Codigo=con.ContasSum*/'
      'WHERE car.Tipo <> 2'
      'AND con.ContasAgr=0'
      'AND con.ContasSum>0'
      'AND car.ForneceI=:P0'
      'AND lan.Data BETWEEN :P1 AND :P2'
      'GROUP BY con.Codigo, lan.Data, lan.Mez, lan.Descricao'
      'ORDER BY car.Nome, car.Codigo, lan.Data, lan.Controle, '
      'lan.Credito DESC, lan.Debito')
    Left = 272
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPesqSumContasAgr: TIntegerField
      FieldName = 'ContasAgr'
    end
    object QrPesqSumNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Size = 100
    end
    object QrPesqSumNOME2CART: TWideStringField
      FieldName = 'NOME2CART'
      Size = 100
    end
    object QrPesqSumCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrPesqSumData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrPesqSumDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPesqSumCREDITO: TFloatField
      FieldName = 'CREDITO'
    end
    object QrPesqSumDEBITO: TFloatField
      FieldName = 'DEBITO'
    end
    object QrPesqSumMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrPesqSumOrdemCart: TIntegerField
      FieldName = 'OrdemCart'
    end
    object QrPesqSumTipoCart: TIntegerField
      FieldName = 'TipoCart'
      Required = True
    end
    object QrPesqSumNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
  end
  object QrSdoTrf: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(car.Tipo<>2,lan.Carteira, bco.Codigo) Carteira,'
      'SUM(lan.Credito) SDO_CRE, SUM(lan.Debito) SDO_DEB'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN carteiras bco ON bco.Codigo=car.Banco'
      'WHERE lan.Genero = -1'
      'AND car.ForneceI=:P0'
      'AND lan.Data BETWEEN :P1 AND :P2'
      'GROUP BY Carteira')
    Left = 516
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSdoTrfCarteira: TLargeintField
      FieldName = 'Carteira'
    end
    object QrSdoTrfSDO_CRE: TFloatField
      FieldName = 'SDO_CRE'
    end
    object QrSdoTrfSDO_DEB: TFloatField
      FieldName = 'SDO_DEB'
    end
  end
  object QrSdoMov: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lan.Carteira,'
      'SUM(lan.Credito) SDO_CRE, SUM(lan.Debito) SDO_DEB'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'WHERE (lan.Genero > 0'
      'OR lan.Genero=-10)'
      'AND car.Tipo <> 2'
      'AND car.ForneceI=:P0'
      'AND lan.Data BETWEEN :P1 AND :P2'
      'GROUP BY lan.Carteira')
    Left = 516
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSdoMovCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrSdoMovSDO_CRE: TFloatField
      FieldName = 'SDO_CRE'
    end
    object QrSdoMovSDO_DEB: TFloatField
      FieldName = 'SDO_DEB'
    end
  end
  object QrSdoCtas: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSdoCtasCalcFields
    SQL.Strings = (
      
        'SELECT 0 KGT, car.Codigo Carteira, car.Inicial SDO_CAD, car.Nome' +
        ', '
      'car.Nome2, car.Ordem OrdemCart, car.Tipo, '
      'ELT(car.Tipo+1,"CAIXA","EXTRATO","EMISS'#195'O","?") NO_TipoCart,'
      '('
      '  SELECT SUM(lan.Credito-lan.Debito)'
      '  FROM lanctos lan'
      '  WHERE lan.Carteira=car.Codigo'
      '  AND lan.Data < :P0'
      ') SDO_ANT'
      'FROM carteiras car'
      'WHERE car.ForneceI=:P1 '
      'AND car.Tipo <> 2'
      'AND'
      '( '
      '  car.Ativo=1'
      '  OR'
      '  car.Saldo <> 0'
      ')'
      'AND car.Exclusivo=0'
      '')
    Left = 576
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSdoCtasCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrSdoCtasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrSdoCtasNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 100
    end
    object QrSdoCtasOrdemCart: TIntegerField
      FieldName = 'OrdemCart'
      Required = True
    end
    object QrSdoCtasSDO_CAD: TFloatField
      FieldName = 'SDO_CAD'
    end
    object QrSdoCtasSDO_ANT: TFloatField
      FieldName = 'SDO_ANT'
    end
    object QrSdoCtasSDO_INI: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SDO_INI'
      Calculated = True
    end
    object QrSdoCtasMOV_CRE: TFloatField
      FieldKind = fkLookup
      FieldName = 'MOV_CRE'
      LookupDataSet = QrSdoMov
      LookupKeyFields = 'Carteira'
      LookupResultField = 'SDO_CRE'
      KeyFields = 'Carteira'
      Lookup = True
    end
    object QrSdoCtasMOV_DEB: TFloatField
      FieldKind = fkLookup
      FieldName = 'MOV_DEB'
      LookupDataSet = QrSdoMov
      LookupKeyFields = 'Carteira'
      LookupResultField = 'SDO_DEB'
      KeyFields = 'Carteira'
      Lookup = True
    end
    object QrSdoCtasTRF_CRE: TFloatField
      FieldKind = fkLookup
      FieldName = 'TRF_CRE'
      LookupDataSet = QrSdoTrf
      LookupKeyFields = 'Carteira'
      LookupResultField = 'SDO_CRE'
      KeyFields = 'Carteira'
      Lookup = True
    end
    object QrSdoCtasSDO_FIM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SDO_FIM'
      Calculated = True
    end
    object QrSdoCtasTRF_DEB: TFloatField
      FieldKind = fkLookup
      FieldName = 'TRF_DEB'
      LookupDataSet = QrSdoTrf
      LookupKeyFields = 'Carteira'
      LookupResultField = 'SDO_DEB'
      KeyFields = 'Carteira'
      Lookup = True
    end
    object QrSdoCtasKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
    object QrSdoCtasTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrSdoCtasNO_TipoCart: TWideStringField
      FieldName = 'NO_TipoCart'
      Size = 7
    end
  end
  object DsExtratos: TDataSource
    DataSet = QrExtratos
    Left = 636
    Top = 4
  end
  object QrSdoExcl: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSdoExclCalcFields
    SQL.Strings = (
      
        'SELECT 0 KGT, car.Codigo Carteira, car.Inicial SDO_CAD, car.Nome' +
        ', '
      'car.Nome2, car.Ordem OrdemCart, car.Tipo, '
      'ELT(car.Tipo+1,"CAIXA","EXTRATO","EMISS'#195'O","?") NO_TipoCart,'
      '('
      '  SELECT SUM(lan.Credito-lan.Debito)'
      '  FROM lanctos lan'
      '  WHERE lan.Carteira=car.Codigo'
      '  AND lan.Data < :P0'
      ') SDO_ANT'
      'FROM carteiras car'
      'WHERE car.ForneceI=:P1 '
      'AND car.Tipo <> 2'
      'AND'
      '( '
      '  car.Ativo=1'
      '  OR'
      '  car.Saldo <> 0'
      ')'
      'AND car.Exclusivo=1'
      '')
    Left = 576
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSdoExclCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrSdoExclNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrSdoExclNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 100
    end
    object QrSdoExclOrdemCart: TIntegerField
      FieldName = 'OrdemCart'
      Required = True
    end
    object QrSdoExclSDO_CAD: TFloatField
      FieldName = 'SDO_CAD'
    end
    object QrSdoExclSDO_ANT: TFloatField
      FieldName = 'SDO_ANT'
    end
    object QrSdoExclSDO_INI: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SDO_INI'
      Calculated = True
    end
    object QrSdoExclMOV_CRE: TFloatField
      FieldKind = fkLookup
      FieldName = 'MOV_CRE'
      LookupDataSet = QrSdoMov
      LookupKeyFields = 'Carteira'
      LookupResultField = 'SDO_CRE'
      KeyFields = 'Carteira'
      Lookup = True
    end
    object QrSdoExclMOV_DEB: TFloatField
      FieldKind = fkLookup
      FieldName = 'MOV_DEB'
      LookupDataSet = QrSdoMov
      LookupKeyFields = 'Carteira'
      LookupResultField = 'SDO_DEB'
      KeyFields = 'Carteira'
      Lookup = True
    end
    object QrSdoExclTRF_CRE: TFloatField
      FieldKind = fkLookup
      FieldName = 'TRF_CRE'
      LookupDataSet = QrSdoTrf
      LookupKeyFields = 'Carteira'
      LookupResultField = 'SDO_CRE'
      KeyFields = 'Carteira'
      Lookup = True
    end
    object QrSdoExclSDO_FIM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SDO_FIM'
      Calculated = True
    end
    object QrSdoExclTRF_DEB: TFloatField
      FieldKind = fkLookup
      FieldName = 'TRF_DEB'
      LookupDataSet = QrSdoTrf
      LookupKeyFields = 'Carteira'
      LookupResultField = 'SDO_DEB'
      KeyFields = 'Carteira'
      Lookup = True
    end
    object QrSdoExclKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
    object QrSdoExclTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrSdoExclNO_TipoCart: TWideStringField
      FieldName = 'NO_TipoCart'
      Size = 7
    end
  end
  object frxDsSdoExcl: TfrxDBDataset
    UserName = 'frxDsSdoExcl'
    CloseDataSource = False
    DataSet = QrSdoExcl
    BCDToCurrency = False
    Left = 636
    Top = 52
  end
  object QrELSCI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2,'
      '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,'
      '/*ci.Unidade UH,*/ la.*, ct.Codigo CONTA, ca.Prazo,'
      'ca.Banco1, ca.Agencia1, ca.Conta1,'
      'ca.ForneceI CART_DONO, ca.Nome NOMECARTEIRA,'
      'ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'IF(em.Tipo=0, em.RazaoSocial, em.Nome) NOMEEMPRESA,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE,'
      'IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEFORNECEDOR,'
      
        'IF(la.ForneceI=0, "", IF(fi.Tipo=0, fi.RazaoSocial, fi.Nome)) NO' +
        'MEFORNECEI,'
      'IF(la.Sit<2, la.Credito-la.Debito-(la.Pago*la.Sit), 0) SALDO,'
      'IF(la.Cliente>0,'
      '  IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome),'
      '  IF (la.Fornecedor>0,'
      '    IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome), "")) NOMERELACIONADO'
      'FROM lanctos la'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      
        'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = ' +
        '0'
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo'
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'LEFT JOIN entidades fi ON fi.Codigo=la.ForneceI'
      '/*LEFT JOIN condimov  ci ON ci.Conta=la.Depto*/'
      'WHERE la.CliInt =0'
      'AND la.Controle <> 0'
      'ORDER BY la.Data, la.Controle'
      ''
      '')
    Left = 332
    Top = 4
    object QrELSCIData: TDateField
      FieldName = 'Data'
      Origin = 'lanctos.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrELSCITipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'lanctos.Tipo'
    end
    object QrELSCICarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'lanctos.Carteira'
    end
    object QrELSCIAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      Origin = 'lanctos.Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrELSCIGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'lanctos.Genero'
    end
    object QrELSCIDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'lanctos.Descricao'
      Size = 25
    end
    object QrELSCINotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'lanctos.NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrELSCIDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'lanctos.Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrELSCICredito: TFloatField
      FieldName = 'Credito'
      Origin = 'lanctos.Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrELSCICompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'lanctos.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrELSCIDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'lanctos.Documento'
      DisplayFormat = '000000; ; '
    end
    object QrELSCISit: TIntegerField
      FieldName = 'Sit'
      Origin = 'lanctos.Sit'
    end
    object QrELSCIVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'lanctos.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrELSCILk: TIntegerField
      FieldName = 'Lk'
      Origin = 'lanctos.Lk'
    end
    object QrELSCIFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'lanctos.FatID'
      DisplayFormat = '000;-000; '
    end
    object QrELSCIFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'lanctos.FatParcela'
    end
    object QrELSCICONTA: TIntegerField
      FieldName = 'CONTA'
      Origin = 'contas.Codigo'
    end
    object QrELSCINOMECONTA: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      FixedChar = True
      Size = 128
    end
    object QrELSCINOMEEMPRESA: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMEEMPRESA'
      Size = 128
    end
    object QrELSCINOMESUBGRUPO: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMESUBGRUPO'
      Origin = 'subgrupos.Nome'
      Size = 128
    end
    object QrELSCINOMEGRUPO: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMEGRUPO'
      Origin = 'grupos.Nome'
      Size = 128
    end
    object QrELSCINOMECONJUNTO: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMECONJUNTO'
      Origin = 'conjuntos.Nome'
      Size = 128
    end
    object QrELSCINOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrELSCIAno: TFloatField
      FieldName = 'Ano'
    end
    object QrELSCIMENSAL: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrELSCIMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrELSCIBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'lanctos.Banco'
    end
    object QrELSCILocal: TIntegerField
      FieldName = 'Local'
      Origin = 'lanctos.Local'
    end
    object QrELSCIFatura: TWideStringField
      FieldName = 'Fatura'
      Origin = 'lanctos.Fatura'
      FixedChar = True
      Size = 128
    end
    object QrELSCISub: TSmallintField
      FieldName = 'Sub'
      Origin = 'lanctos.Sub'
      DisplayFormat = '00; ; '
    end
    object QrELSCICartao: TIntegerField
      FieldName = 'Cartao'
      Origin = 'lanctos.Cartao'
    end
    object QrELSCILinha: TIntegerField
      FieldName = 'Linha'
      Origin = 'lanctos.Linha'
    end
    object QrELSCIPago: TFloatField
      FieldName = 'Pago'
      Origin = 'lanctos.Pago'
    end
    object QrELSCISALDO: TFloatField
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrELSCIID_Sub: TSmallintField
      FieldName = 'ID_Sub'
      Origin = 'lanctos.ID_Sub'
    end
    object QrELSCIMez: TIntegerField
      FieldName = 'Mez'
      Origin = 'lanctos.Mez'
    end
    object QrELSCIFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Origin = 'lanctos.Fornecedor'
    end
    object QrELSCIcliente: TIntegerField
      FieldName = 'cliente'
      Origin = 'lanctos.Cliente'
    end
    object QrELSCIMoraDia: TFloatField
      FieldName = 'MoraDia'
      Origin = 'lanctos.MoraDia'
    end
    object QrELSCINOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrELSCINOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrELSCITIPOEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrELSCINOMERELACIONADO: TWideStringField
      FieldName = 'NOMERELACIONADO'
      Size = 50
    end
    object QrELSCIOperCount: TIntegerField
      FieldName = 'OperCount'
      Origin = 'lanctos.OperCount'
    end
    object QrELSCILancto: TIntegerField
      FieldName = 'Lancto'
      Origin = 'lanctos.Lancto'
    end
    object QrELSCIMulta: TFloatField
      FieldName = 'Multa'
      Origin = 'lanctos.Multa'
    end
    object QrELSCIATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrELSCIJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrELSCIDataDoc: TDateField
      FieldName = 'DataDoc'
      Origin = 'lanctos.DataDoc'
    end
    object QrELSCINivel: TIntegerField
      FieldName = 'Nivel'
      Origin = 'lanctos.Nivel'
    end
    object QrELSCIVendedor: TIntegerField
      FieldName = 'Vendedor'
      Origin = 'lanctos.Vendedor'
    end
    object QrELSCIAccount: TIntegerField
      FieldName = 'Account'
      Origin = 'lanctos.Account'
    end
    object QrELSCIMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrELSCIProtesto: TDateField
      FieldName = 'Protesto'
      Origin = 'lanctos.Protesto'
    end
    object QrELSCIDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'lanctos.DataCad'
    end
    object QrELSCIDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'lanctos.DataAlt'
    end
    object QrELSCIUserCad: TSmallintField
      FieldName = 'UserCad'
      Origin = 'lanctos.UserCad'
    end
    object QrELSCIUserAlt: TSmallintField
      FieldName = 'UserAlt'
      Origin = 'lanctos.UserAlt'
    end
    object QrELSCIControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'lanctos.Controle'
      Required = True
    end
    object QrELSCIID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Origin = 'lanctos.ID_Pgto'
      Required = True
    end
    object QrELSCICtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Origin = 'lanctos.CtrlIni'
      Required = True
    end
    object QrELSCIFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
      Origin = 'lanctos.FatID_Sub'
    end
    object QrELSCIICMS_P: TFloatField
      FieldName = 'ICMS_P'
      Origin = 'lanctos.ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrELSCIICMS_V: TFloatField
      FieldName = 'ICMS_V'
      Origin = 'lanctos.ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrELSCIDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Origin = 'lanctos.Duplicata'
      Size = 10
    end
    object QrELSCICOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrELSCICliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'lanctos.CliInt'
    end
    object QrELSCIDepto: TIntegerField
      FieldName = 'Depto'
      Origin = 'lanctos.Depto'
    end
    object QrELSCIDescoPor: TIntegerField
      FieldName = 'DescoPor'
      Origin = 'lanctos.DescoPor'
    end
    object QrELSCIPrazo: TSmallintField
      FieldName = 'Prazo'
      Origin = 'carteiras.Prazo'
    end
    object QrELSCIForneceI: TIntegerField
      FieldName = 'ForneceI'
      Origin = 'lanctos.ForneceI'
    end
    object QrELSCIQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'lanctos.Qtde'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrELSCIEmitente: TWideStringField
      FieldName = 'Emitente'
      Origin = 'lanctos.Emitente'
      Size = 30
    end
    object QrELSCIContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Origin = 'lanctos.ContaCorrente'
      Size = 15
    end
    object QrELSCICNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Origin = 'lanctos.CNPJCPF'
      Size = 15
    end
    object QrELSCIDescoVal: TFloatField
      FieldName = 'DescoVal'
      Origin = 'lanctos.DescoVal'
    end
    object QrELSCIDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Origin = 'lanctos.DescoControle'
      Required = True
    end
    object QrELSCIUnidade: TIntegerField
      FieldName = 'Unidade'
      Origin = 'lanctos.Unidade'
      Required = True
    end
    object QrELSCINFVal: TFloatField
      FieldName = 'NFVal'
      Origin = 'lanctos.NFVal'
      Required = True
    end
    object QrELSCIAntigo: TWideStringField
      FieldName = 'Antigo'
      Origin = 'lanctos.Antigo'
    end
    object QrELSCIExcelGru: TIntegerField
      FieldName = 'ExcelGru'
      Origin = 'lanctos.ExcelGru'
    end
    object QrELSCISerieCH: TWideStringField
      DisplayWidth = 10
      FieldName = 'SerieCH'
      Origin = 'lanctos.SerieCH'
      Size = 10
    end
    object QrELSCISERIE_CHEQUE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SERIE_CHEQUE'
      Calculated = True
    end
    object QrELSCIDoc2: TWideStringField
      FieldName = 'Doc2'
      Origin = 'lanctos.Doc2'
    end
    object QrELSCINOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrELSCIMoraVal: TFloatField
      FieldName = 'MoraVal'
      Origin = 'lanctos.MoraVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrELSCIMultaVal: TFloatField
      FieldName = 'MultaVal'
      Origin = 'lanctos.MultaVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrELSCICNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
      Origin = 'lanctos.CNAB_Sit'
    end
    object QrELSCIBanco1: TIntegerField
      FieldName = 'Banco1'
      Origin = 'carteiras.Banco1'
    end
    object QrELSCIAgencia1: TIntegerField
      FieldName = 'Agencia1'
      Origin = 'carteiras.Agencia1'
    end
    object QrELSCIConta1: TWideStringField
      FieldName = 'Conta1'
      Origin = 'carteiras.Conta1'
      Size = 15
    end
    object QrELSCITipoCH: TSmallintField
      FieldName = 'TipoCH'
      Origin = 'lanctos.TipoCH'
      Required = True
    end
    object QrELSCIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrELSCIReparcel: TIntegerField
      FieldName = 'Reparcel'
      Required = True
    end
    object QrELSCIID_Quit: TIntegerField
      FieldName = 'ID_Quit'
      Required = True
    end
    object QrELSCIAtrelado: TIntegerField
      FieldName = 'Atrelado'
      Required = True
    end
    object QrELSCIAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrELSCIFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrELSCIProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrELSCIPagMul: TFloatField
      FieldName = 'PagMul'
      Required = True
    end
    object QrELSCIPagJur: TFloatField
      FieldName = 'PagJur'
      Required = True
    end
    object QrELSCICART_DONO: TIntegerField
      FieldName = 'CART_DONO'
    end
    object QrELSCINOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrELSCIAgencia: TIntegerField
      FieldName = 'Agencia'
    end
  end
  object QrSdoCarts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Inicial) Inicial '
      'FROM carteiras'
      'WHERE ForneceI=:P0')
    Left = 332
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSdoCartsInicial: TFloatField
      FieldName = 'Inicial'
    end
  end
  object QrSdoCtSdo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(SdoIni) SdoIni'
      'FROM contassdo'
      'WHERE Entidade=:P0'
      '')
    Left = 332
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSdoCtSdoSdoIni: TFloatField
      FieldName = 'SdoIni'
    end
  end
  object QrContasNiv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nivel, Genero '
      'FROM contasniv'
      'WHERE Entidade=:P0'
      'ORDER BY Nivel'
      '')
    Left = 392
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContasNivNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrContasNivGenero: TIntegerField
      FieldName = 'Genero'
    end
  end
  object QrNiv1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mov.Genero, mov.Subgrupo, mov.Grupo, '
      'mov.Conjunto, mov.Plano, SUM(mov.Movim) Movim,'
      'cta.Nome NO_Cta, cta.OrdemLista OR_Cta,'
      'sgr.Nome NO_Sgr, sgr.OrdemLista OR_SGr,'
      'gru.Nome NO_Gru, gru.OrdemLista OR_Gru,'
      'cjt.Nome NO_Cjt, cjt.OrdemLista OR_Cjt,'
      'pla.Nome NO_Pla, pla.OrdemLista OR_Pla'
      'FROM contasmov mov'
      'LEFT JOIN contas cta ON cta.Codigo=mov.Genero'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=mov.Subgrupo'
      'LEFT JOIN grupos gru ON gru.Codigo=mov.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=mov.Conjunto'
      'LEFT JOIN plano pla ON pla.Codigo=mov.Plano'
      'WHERE mov.CliInt=:P0'
      'AND mov.Genero IN '
      '('
      '  SELECT Genero   '
      '  FROM contasniv'
      '  WHERE Entidade=:P1'
      '  AND Nivel=1'
      ')'
      'AND mov.Mez <=:P2'
      'GROUP BY mov.Genero')
    Left = 392
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNiv1Genero: TIntegerField
      FieldName = 'Genero'
      Required = True
    end
    object QrNiv1Subgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrNiv1Grupo: TIntegerField
      FieldName = 'Grupo'
      Required = True
    end
    object QrNiv1Conjunto: TIntegerField
      FieldName = 'Conjunto'
      Required = True
    end
    object QrNiv1Plano: TIntegerField
      FieldName = 'Plano'
      Required = True
    end
    object QrNiv1Movim: TFloatField
      FieldName = 'Movim'
    end
    object QrNiv1NO_Cta: TWideStringField
      FieldName = 'NO_Cta'
      Size = 50
    end
    object QrNiv1OR_Cta: TIntegerField
      FieldName = 'OR_Cta'
    end
    object QrNiv1NO_Sgr: TWideStringField
      FieldName = 'NO_Sgr'
      Size = 50
    end
    object QrNiv1OR_SGr: TIntegerField
      FieldName = 'OR_SGr'
    end
    object QrNiv1NO_Gru: TWideStringField
      FieldName = 'NO_Gru'
      Size = 50
    end
    object QrNiv1OR_Gru: TIntegerField
      FieldName = 'OR_Gru'
    end
    object QrNiv1NO_Cjt: TWideStringField
      FieldName = 'NO_Cjt'
      Size = 50
    end
    object QrNiv1OR_Cjt: TIntegerField
      FieldName = 'OR_Cjt'
    end
    object QrNiv1NO_Pla: TWideStringField
      FieldName = 'NO_Pla'
      Size = 50
    end
    object QrNiv1OR_Pla: TIntegerField
      FieldName = 'OR_Pla'
    end
  end
  object QrNivMov: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mov.Credito, mov.Debito'
      'FROM contasmov mov'
      'WHERE mov.CliInt=-11'
      'AND mov.Mez=905')
    Left = 332
    Top = 196
    object QrNivMovCredito: TFloatField
      FieldName = 'Credito'
      Required = True
    end
    object QrNivMovDebito: TFloatField
      FieldName = 'Debito'
      Required = True
    end
  end
  object DsSaldosNiv: TDataSource
    DataSet = QrSaldosNiv
    Left = 272
    Top = 244
  end
  object QrNiv2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT 0 Genero, mov.Subgrupo, mov.Grupo, '
      'mov.Conjunto, mov.Plano, SUM(mov.Movim) Movim,'
      
        'CONCAT("CONTAS N'#195'O CONTROLADAS DO SUBGRUPO ",sgr.Nome) NO_Cta, 9' +
        '99999999 OR_Cta,'
      'sgr.Nome NO_Sgr, sgr.OrdemLista OR_SGr,'
      'gru.Nome NO_Gru, gru.OrdemLista OR_Gru,'
      'cjt.Nome NO_Cjt, cjt.OrdemLista OR_Cjt,'
      'pla.Nome NO_Pla, pla.OrdemLista OR_Pla'
      'FROM contasmov mov'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=mov.Subgrupo'
      'LEFT JOIN grupos gru ON gru.Codigo=mov.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=mov.Conjunto'
      'LEFT JOIN plano pla ON pla.Codigo=mov.Plano'
      'WHERE mov.CliInt=:P0'
      'AND mov.Subgrupo IN '
      '('
      '  SELECT Genero   '
      '  FROM contasniv'
      '  WHERE Entidade=:P1'
      '  AND Nivel=2'
      ')'
      'AND mov.Mez <=:P2'
      'GROUP BY mov.Subgrupo')
    Left = 392
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNiv2Genero: TLargeintField
      FieldName = 'Genero'
      Required = True
    end
    object QrNiv2Subgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrNiv2Grupo: TIntegerField
      FieldName = 'Grupo'
      Required = True
    end
    object QrNiv2Conjunto: TIntegerField
      FieldName = 'Conjunto'
      Required = True
    end
    object QrNiv2Plano: TIntegerField
      FieldName = 'Plano'
      Required = True
    end
    object QrNiv2Movim: TFloatField
      FieldName = 'Movim'
    end
    object QrNiv2NO_Cta: TWideStringField
      FieldName = 'NO_Cta'
      Size = 85
    end
    object QrNiv2OR_Cta: TLargeintField
      FieldName = 'OR_Cta'
      Required = True
    end
    object QrNiv2NO_Sgr: TWideStringField
      FieldName = 'NO_Sgr'
      Size = 50
    end
    object QrNiv2OR_SGr: TIntegerField
      FieldName = 'OR_SGr'
    end
    object QrNiv2NO_Gru: TWideStringField
      FieldName = 'NO_Gru'
      Size = 50
    end
    object QrNiv2OR_Gru: TIntegerField
      FieldName = 'OR_Gru'
    end
    object QrNiv2NO_Cjt: TWideStringField
      FieldName = 'NO_Cjt'
      Size = 50
    end
    object QrNiv2OR_Cjt: TIntegerField
      FieldName = 'OR_Cjt'
    end
    object QrNiv2NO_Pla: TWideStringField
      FieldName = 'NO_Pla'
      Size = 50
    end
    object QrNiv2OR_Pla: TIntegerField
      FieldName = 'OR_Pla'
    end
  end
  object QrNiv3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT 0 Genero, 0 Subgrupo, mov.Grupo, '
      'mov.Conjunto, mov.Plano, SUM(mov.Movim) Movim,'
      
        'CONCAT("CONTAS N'#195'O CONTROLADAS DO GRUPO ", gru.Nome) NO_Cta, 999' +
        '999999 OR_Cta,'
      
        'CONCAT("SUB-GRUPOS N'#195'O CONTROLADOS DO GRUPO ", gru.Nome) NO_Sgr,' +
        ' 999999999 OR_SGr,'
      'gru.Nome NO_Gru, gru.OrdemLista OR_Gru,'
      'cjt.Nome NO_Cjt, cjt.OrdemLista OR_Cjt,'
      'pla.Nome NO_Pla, pla.OrdemLista OR_Pla'
      'FROM contasmov mov'
      'LEFT JOIN grupos gru ON gru.Codigo=mov.Grupo'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=mov.Conjunto'
      'LEFT JOIN plano pla ON pla.Codigo=mov.Plano'
      'WHERE mov.CliInt=:P0'
      'AND mov.Grupo IN '
      '('
      '  SELECT Genero   '
      '  FROM contasniv'
      '  WHERE Entidade=:P1'
      '  AND Nivel=3'
      ')'
      'AND mov.Mez <=:P2'
      'GROUP BY mov.Grupo')
    Left = 392
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNiv3Genero: TLargeintField
      FieldName = 'Genero'
      Required = True
    end
    object QrNiv3Subgrupo: TLargeintField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrNiv3Grupo: TIntegerField
      FieldName = 'Grupo'
      Required = True
    end
    object QrNiv3Conjunto: TIntegerField
      FieldName = 'Conjunto'
      Required = True
    end
    object QrNiv3Plano: TIntegerField
      FieldName = 'Plano'
      Required = True
    end
    object QrNiv3Movim: TFloatField
      FieldName = 'Movim'
    end
    object QrNiv3NO_Cta: TWideStringField
      FieldName = 'NO_Cta'
      Size = 82
    end
    object QrNiv3OR_Cta: TLargeintField
      FieldName = 'OR_Cta'
      Required = True
    end
    object QrNiv3NO_Sgr: TWideStringField
      FieldName = 'NO_Sgr'
      Size = 86
    end
    object QrNiv3OR_SGr: TLargeintField
      FieldName = 'OR_SGr'
      Required = True
    end
    object QrNiv3NO_Gru: TWideStringField
      FieldName = 'NO_Gru'
      Size = 50
    end
    object QrNiv3OR_Gru: TIntegerField
      FieldName = 'OR_Gru'
    end
    object QrNiv3NO_Cjt: TWideStringField
      FieldName = 'NO_Cjt'
      Size = 50
    end
    object QrNiv3OR_Cjt: TIntegerField
      FieldName = 'OR_Cjt'
    end
    object QrNiv3NO_Pla: TWideStringField
      FieldName = 'NO_Pla'
      Size = 50
    end
    object QrNiv3OR_Pla: TIntegerField
      FieldName = 'OR_Pla'
    end
  end
  object QrNiv4: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT 0 Genero, 0 Subgrupo,0 Grupo, '
      'mov.Conjunto, mov.Plano, SUM(mov.Movim) Movim,'
      
        'CONCAT("CONTAS N'#195'O CONTROLADAS DO CONJUTO ", cjt.Nome) NO_Cta, 9' +
        '99999999 OR_Cta,'
      
        'CONCAT("SUB-GRUPOS N'#195'O CONTROLADOS DO CONJUNTO ", cjt.Nome) NO_S' +
        'gr, 999999999 OR_SGr,'
      
        'CONCAT("GRUPOS N'#195'O CONTROLADOS", cjt.Nome) NO_Gru, 999999999 OR_' +
        'Gru,'
      'cjt.Nome NO_Cjt, cjt.OrdemLista OR_Cjt,'
      'pla.Nome NO_Pla, pla.OrdemLista OR_Pla'
      'FROM contasmov mov'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=mov.Conjunto'
      'LEFT JOIN plano pla ON pla.Codigo=mov.Plano'
      'WHERE mov.CliInt=:P0'
      'AND mov.Conjunto IN '
      '('
      '  SELECT Genero   '
      '  FROM contasniv'
      '  WHERE Entidade=:P1'
      '  AND Nivel=4'
      ')'
      'AND mov.Mez <=:P2'
      'GROUP BY mov.Conjunto')
    Left = 392
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNiv4Genero: TLargeintField
      FieldName = 'Genero'
      Required = True
    end
    object QrNiv4Subgrupo: TLargeintField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrNiv4Grupo: TLargeintField
      FieldName = 'Grupo'
      Required = True
    end
    object QrNiv4Conjunto: TIntegerField
      FieldName = 'Conjunto'
      Required = True
    end
    object QrNiv4Plano: TIntegerField
      FieldName = 'Plano'
      Required = True
    end
    object QrNiv4Movim: TFloatField
      FieldName = 'Movim'
    end
    object QrNiv4NO_Cta: TWideStringField
      FieldName = 'NO_Cta'
      Size = 84
    end
    object QrNiv4OR_Cta: TLargeintField
      FieldName = 'OR_Cta'
      Required = True
    end
    object QrNiv4NO_Sgr: TWideStringField
      FieldName = 'NO_Sgr'
      Size = 89
    end
    object QrNiv4OR_SGr: TLargeintField
      FieldName = 'OR_SGr'
      Required = True
    end
    object QrNiv4NO_Gru: TWideStringField
      FieldName = 'NO_Gru'
      Size = 72
    end
    object QrNiv4OR_Gru: TLargeintField
      FieldName = 'OR_Gru'
      Required = True
    end
    object QrNiv4NO_Cjt: TWideStringField
      FieldName = 'NO_Cjt'
      Size = 50
    end
    object QrNiv4OR_Cjt: TIntegerField
      FieldName = 'OR_Cjt'
    end
    object QrNiv4NO_Pla: TWideStringField
      FieldName = 'NO_Pla'
      Size = 50
    end
    object QrNiv4OR_Pla: TIntegerField
      FieldName = 'OR_Pla'
    end
  end
  object QrNiv5: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT 0 Genero, 0 Subgrupo, 0 Grupo, '
      '0 Conjunto, mov.Plano, SUM(mov.Movim) Movim,'
      
        'CONCAT("CONTAS N'#195'O CONTROLADAS DO PLANO", pla.Nome) NO_Cta, 9999' +
        '99999 OR_Cta,'
      
        'CONCAT("SUB-GRUPOS N'#195'O CONTROLADOS DO PLANO", pla.Nome) NO_Sgr, ' +
        '999999999 OR_SGr,'
      
        'CONCAT("GRUPOS N'#195'O CONTROLADOS DO PLANO", pla.Nome) NO_Gru, 9999' +
        '99999 OR_Gru,'
      
        'CONCAT("CONJUNTOS N'#195'O CONTROLADOS DO PLANO", pla.Nome) NO_Cjt, 9' +
        '99999999 OR_Cjt,'
      'pla.Nome NO_Pla, pla.OrdemLista OR_Pla'
      'FROM contasmov mov'
      'LEFT JOIN plano pla ON pla.Codigo=mov.Plano'
      'WHERE mov.CliInt=:P0'
      'AND mov.Plano IN '
      '('
      '  SELECT Genero   '
      '  FROM contasniv'
      '  WHERE Entidade=:P1'
      '  AND Nivel=5'
      ')'
      'AND mov.Mez <=:P2'
      'GROUP BY mov.Plano')
    Left = 392
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNiv5Genero: TLargeintField
      FieldName = 'Genero'
      Required = True
    end
    object QrNiv5Subgrupo: TLargeintField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrNiv5Grupo: TLargeintField
      FieldName = 'Grupo'
      Required = True
    end
    object QrNiv5Conjunto: TLargeintField
      FieldName = 'Conjunto'
      Required = True
    end
    object QrNiv5Plano: TIntegerField
      FieldName = 'Plano'
      Required = True
    end
    object QrNiv5Movim: TFloatField
      FieldName = 'Movim'
    end
    object QrNiv5NO_Cta: TWideStringField
      FieldName = 'NO_Cta'
      Size = 81
    end
    object QrNiv5OR_Cta: TLargeintField
      FieldName = 'OR_Cta'
      Required = True
    end
    object QrNiv5NO_Sgr: TWideStringField
      FieldName = 'NO_Sgr'
      Size = 85
    end
    object QrNiv5OR_SGr: TLargeintField
      FieldName = 'OR_SGr'
      Required = True
    end
    object QrNiv5NO_Gru: TWideStringField
      FieldName = 'NO_Gru'
      Size = 81
    end
    object QrNiv5OR_Gru: TLargeintField
      FieldName = 'OR_Gru'
      Required = True
    end
    object QrNiv5NO_Cjt: TWideStringField
      FieldName = 'NO_Cjt'
      Size = 84
    end
    object QrNiv5OR_Cjt: TLargeintField
      FieldName = 'OR_Cjt'
      Required = True
    end
    object QrNiv5NO_Pla: TWideStringField
      FieldName = 'NO_Pla'
      Size = 50
    end
    object QrNiv5OR_Pla: TIntegerField
      FieldName = 'OR_Pla'
    end
  end
  object DsSdoPar: TDataSource
    DataSet = QrSdoPar
    Left = 452
    Top = 244
  end
  object frxDsSaldosNiv: TfrxDBDataset
    UserName = 'frxDsSaldosNiv'
    CloseDataSource = False
    DataSet = QrSaldosNiv
    BCDToCurrency = False
    Left = 332
    Top = 244
  end
  object frxDsCreditos: TfrxDBDataset
    UserName = 'frxDsCreditos'
    CloseDataSource = False
    DataSet = QrCreditos
    BCDToCurrency = False
    Left = 452
    Top = 340
  end
  object DsCreditos: TDataSource
    DataSet = QrCreditos
    Left = 392
    Top = 340
  end
  object QrCreditos: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCreditosCalcFields
    SQL.Strings = (
      'SELECT lan.Mez, SUM(lan.Credito) Credito, '
      'lan.Controle, lan.Sub, lan.Carteira, lan.Cartao, lan.Tipo,'
      'lan.Vencimento, lan.Compensado, lan.Sit, lan.Genero, '
      'lan.SubPgto1,'
      'con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN contas    con ON con.Codigo=lan.Genero'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'WHERE lan.Tipo <> 2'
      'AND lan.Credito > 0'
      'AND lan.Genero>0'
      'AND car.ForneceI = :P0'
      'AND lan.Data BETWEEN :P1 AND :P2'
      'GROUP BY lan.Genero, lan.Mez'
      '/*, lan.SubPgto1*/'
      'ORDER BY gru.OrdemLista, NOMEGRU, sgr.OrdemLista, '
      'NOMESGR, con.OrdemLista, NOMECON, Mez, Data'
      '')
    Left = 332
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCreditosMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrCreditosCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrCreditosNOMECON: TWideStringField
      FieldName = 'NOMECON'
      Size = 50
    end
    object QrCreditosNOMESGR: TWideStringField
      FieldName = 'NOMESGR'
      Size = 50
    end
    object QrCreditosNOMEGRU: TWideStringField
      FieldName = 'NOMEGRU'
      Size = 50
    end
    object QrCreditosMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 7
      Calculated = True
    end
    object QrCreditosNOMECON_2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECON_2'
      Size = 255
      Calculated = True
    end
    object QrCreditosSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
      Required = True
    end
    object QrCreditosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCreditosSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrCreditosCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrCreditosCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrCreditosVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrCreditosCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrCreditosSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrCreditosGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrCreditosTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
  end
  object QrSaldoA: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSaldoACalcFields
    SQL.Strings = (
      'SELECT SUM(car.Inicial) Inicial,'
      '('
      '  SELECT SUM(lan.Credito-lan.Debito) '
      '  FROM lanctos lan'
      '  LEFT JOIN carteiras crt ON crt.Codigo=lan.Carteira'
      '  WHERE crt.Tipo <> 2'
      '  AND crt.ForneceI=:P0'
      '  AND lan.Data < :P1'
      ') SALDO'
      'FROM carteiras car'
      'WHERE car.ForneceI=:P2 '
      'AND car.Tipo <> 2'
      '')
    Left = 332
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSaldoAInicial: TFloatField
      FieldName = 'Inicial'
    end
    object QrSaldoASALDO: TFloatField
      FieldName = 'SALDO'
    end
    object QrSaldoATOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTAL'
      Calculated = True
    end
  end
  object frxDsSaldoA: TfrxDBDataset
    UserName = 'frxDsSaldoA'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Inicial=Inicial'
      'SALDO=SALDO'
      'TOTAL=TOTAL')
    DataSet = QrSaldoA
    BCDToCurrency = False
    Left = 392
    Top = 388
  end
  object frxDsResumo: TfrxDBDataset
    UserName = 'frxDsResumo'
    CloseDataSource = False
    DataSet = QrResumo
    BCDToCurrency = False
    Left = 272
    Top = 388
  end
  object QrResumo: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrResumoCalcFields
    SQL.Strings = (
      'SELECT SUM(lan.Credito) Credito, -SUM(lan.Debito) Debito'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'WHERE lan.Tipo <> 2'
      'AND lan.Genero > 0'
      'AND car.ForneceI = :P0'
      'AND lan.Data BETWEEN :P1 AND :P2')
    Left = 212
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrResumoCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrResumoDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrResumoSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      Calculated = True
    end
    object QrResumoFINAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'FINAL'
      Calculated = True
    end
  end
  object DsDebitos: TDataSource
    DataSet = QrDebitos
    Left = 392
    Top = 292
  end
  object frxDsDebitos: TfrxDBDataset
    UserName = 'frxDsDebitos'
    CloseDataSource = False
    DataSet = QrDebitos
    BCDToCurrency = False
    Left = 452
    Top = 292
  end
  object QrDebitos: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDebitosCalcFields
    SQL.Strings = (
      'SELECT lan.Controle, lan.Sub, lan.Carteira, lan.Cartao,'
      'lan.Vencimento, lan.Sit, lan.Genero, lan.Tipo,'
      'IF(COUNT(lan.Compensado) > 1, "V'#225'rias", IF(lan.Compensado=0,"", '
      'DATE_FORMAT(lan.Compensado, "%d/%m/%y"))) COMPENSADO_TXT,'
      
        'IF(COUNT(lan.Data)>1, "V'#225'rias", DATE_FORMAT(lan.Data, "%d/%m/%y"' +
        ')) DATA, '
      'lan.Descricao, SUM(lan.Debito) DEBITO, '
      'IF(COUNT(lan.Data)>1, 0, lan.NotaFiscal) NOTAFISCAL, '
      'IF(COUNT(lan.Data)>1, "", lan.SerieCH) SERIECH, '
      'IF(COUNT(lan.Data)>1, 0, lan.Documento) DOCUMENTO, '
      'IF(COUNT(lan.Data)>1, 0, lan.Mez) MEZ, lan.Compensado,'
      'con.Nome NOMECON, sgr.Nome NOMESGR, gru.Nome NOMEGRU,'#10
      ''
      ''
      ''
      ''
      ''
      ''
      'COUNT(lan.Data) ITENS'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN contas    con ON con.Codigo=lan.Genero'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo'
      'LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo'
      'WHERE lan.Tipo <> 2'
      'AND lan.Debito > 0'
      'AND lan.Genero>0 '
      'AND car.ForneceI = :P0'
      'AND lan.Data BETWEEN :P1 AND :P2'
      'GROUP BY lan.Descricao'
      'ORDER BY gru.OrdemLista, NOMEGRU, sgr.OrdemLista, '
      'NOMESGR, con.OrdemLista, NOMECON, Data'
      '')
    Left = 332
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrDebitosCOMPENSADO_TXT: TWideStringField
      FieldName = 'COMPENSADO_TXT'
      Size = 8
    end
    object QrDebitosDATA: TWideStringField
      FieldName = 'DATA'
      Size = 8
    end
    object QrDebitosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrDebitosDEBITO: TFloatField
      FieldName = 'DEBITO'
    end
    object QrDebitosNOTAFISCAL: TLargeintField
      FieldName = 'NOTAFISCAL'
    end
    object QrDebitosSERIECH: TWideStringField
      FieldName = 'SERIECH'
      Size = 10
    end
    object QrDebitosDOCUMENTO: TFloatField
      FieldName = 'DOCUMENTO'
    end
    object QrDebitosMEZ: TLargeintField
      FieldName = 'MEZ'
      Required = True
    end
    object QrDebitosCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrDebitosNOMECON: TWideStringField
      FieldName = 'NOMECON'
      Size = 50
    end
    object QrDebitosNOMESGR: TWideStringField
      FieldName = 'NOMESGR'
      Size = 50
    end
    object QrDebitosNOMEGRU: TWideStringField
      FieldName = 'NOMEGRU'
      Size = 50
    end
    object QrDebitosITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrDebitosMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 7
      Calculated = True
    end
    object QrDebitosSERIE_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SERIE_DOC'
      Size = 30
      Calculated = True
    end
    object QrDebitosNF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NF_TXT'
      Size = 30
      Calculated = True
    end
    object QrDebitosMES2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES2'
      Size = 5
      Calculated = True
    end
    object QrDebitosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDebitosSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrDebitosCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrDebitosCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrDebitosVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrDebitosSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrDebitosGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrDebitosTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrDebitosNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
  end
  object QrRealizado: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(la.Credito-la.Debito) Valor '
      'FROM lanctos la'
      'WHERE la.Genero=:P0'
      'AND la.Tipo <> 2'
      'AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data))=:P1')
    Left = 836
    Top = 124
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrRealizadoValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrCIni: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT co.Codigo, MIN(ci.Periodo) Minimo, co.Nome,'
      'MAX(ci.Periodo) Maximo, co.PendenMesSeg, co.CalculMesSeg  '
      'FROM contas co'
      'LEFT JOIN contasits ci ON ci.Codigo=co.Codigo'
      'WHERE ci.Codigo IS NOT NULL'
      'GROUP BY co.Codigo')
    Left = 808
    Top = 124
    object QrCIniCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCIniMinimo: TIntegerField
      FieldName = 'Minimo'
    end
    object QrCIniMaximo: TIntegerField
      FieldName = 'Maximo'
    end
    object QrCIniPendenMesSeg: TSmallintField
      FieldName = 'PendenMesSeg'
      Required = True
    end
    object QrCIniCalculMesSeg: TSmallintField
      FieldName = 'CalculMesSeg'
      Required = True
    end
    object QrCIniNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object QrCIts1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT co.Codigo, co.Nome,'
      'ci.Periodo, ci.Tipo, ci.Fator'
      'FROM contas co'
      'LEFT JOIN contasits ci ON ci.Codigo=co.Codigo'
      'WHERE co.Codigo = :P0'
      'AND ci.Periodo = :P1')
    Left = 808
    Top = 172
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCIts1Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCIts1Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrCIts1Periodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrCIts1Tipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrCIts1Fator: TFloatField
      FieldName = 'Fator'
      Required = True
    end
  end
  object QrCIts2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT co.Codigo, co.Nome,'
      'ci.Periodo, ci.Tipo, ci.Fator '
      'FROM contas co'
      'LEFT JOIN contasits ci ON ci.Codigo=co.Codigo'
      'WHERE co.Codigo = :P0'
      'AND ci.Periodo = :P1')
    Left = 836
    Top = 172
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCIts2Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCIts2Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrCIts2Periodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrCIts2Tipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCIts2Fator: TFloatField
      FieldName = 'Fator'
    end
  end
  object QrItsCtas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Conta '
      'FROM contasitsctas'
      'WHERE Codigo=:P0'
      'AND Periodo=:P1')
    Left = 516
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrItsCtasConta: TIntegerField
      FieldName = 'Conta'
    end
  end
  object QrValFator: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(la.Credito-la.Debito) Valor '
      'FROM lanctos la'
      'WHERE la.Genero in (:?)'
      'AND la.Tipo <> 2'
      'AND (((YEAR(la.Data)-2000)*12)+MONTH(la.Data)) = :P0')
    Left = 576
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = '?'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrValFatorValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrCtasResMes2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ctasresmes')
    Left = 756
    Top = 4
    object QrCtasResMes2SeqImp: TIntegerField
      FieldName = 'SeqImp'
    end
    object QrCtasResMes2Conta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCtasResMes2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCtasResMes2Periodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrCtasResMes2Tipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCtasResMes2Fator: TFloatField
      FieldName = 'Fator'
    end
    object QrCtasResMes2ValFator: TFloatField
      FieldName = 'ValFator'
    end
    object QrCtasResMes2Devido: TFloatField
      FieldName = 'Devido'
    end
    object QrCtasResMes2Pago: TFloatField
      FieldName = 'Pago'
    end
    object QrCtasResMes2Diferenca: TFloatField
      FieldName = 'Diferenca'
    end
    object QrCtasResMes2Acumulado: TFloatField
      FieldName = 'Acumulado'
    end
  end
  object QrCtasAnt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Conta, Nome, SUM(Devido+Pago) Acumulado'
      'FROM ctasresmes'
      'WHERE Periodo<:P0'
      'GROUP BY Conta')
    Left = 816
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCtasAntConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCtasAntAcumulado: TFloatField
      FieldName = 'Acumulado'
    end
    object QrCtasAntNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrLocLcto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data, Controle, Tipo, Carteira, CliInt'
      'FROM lanctos'
      'WHERE Controle=:P0'
      'AND Sub=:P1')
    Left = 456
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocLctoData: TDateField
      FieldName = 'Data'
    end
    object QrLocLctoControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLocLctoTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLocLctoCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLocLctoCliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object QrConsigna: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Descricao Nome, (Debito-Credito) Saldo'
      'FROM Consignacoes'
      'WHERE CliInt=:P0')
    Left = 704
    Top = 308
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConsignaNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.consignacoes.Descricao'
      Size = 128
    end
    object QrConsignaSaldo: TFloatField
      FieldName = 'Saldo'
      Origin = 'DBMMONEY.consignacoes.Credito'
    end
  end
  object QrTotalSaldo: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTotalSaldoCalcFields
    SQL.Strings = (
      'SELECT * FROM Saldos')
    Left = 704
    Top = 260
    object QrTotalSaldoNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMLOCAL.saldos.Nome'
      Size = 128
    end
    object QrTotalSaldoSaldo: TFloatField
      FieldName = 'Saldo'
      Origin = 'DBMLOCAL.saldos.Saldo'
    end
    object QrTotalSaldoTipo: TIntegerField
      FieldName = 'Tipo'
      Origin = 'DBMLOCAL.saldos.Tipo'
    end
    object QrTotalSaldoNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 30
      Calculated = True
    end
  end
  object frxSaldos: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.439335567100000000
    ReportOptions.LastChange = 39720.439335567100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxSaldosGetValue
    Left = 708
    Top = 404
    Datasets = <
      item
      end
      item
      end
      item
        DataSet = frxDsTotalSaldo
        DataSetName = 'frxDsTotalSaldo'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 215.900000000000000000
      PaperHeight = 279.400000000000000000
      PaperSize = 1
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 219.212740000000000000
        Width = 702.614627000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsTotalSaldo
        DataSetName = 'frxDsTotalSaldo'
        RowCount = 0
        object Memo14: TfrxMemoView
          Left = 120.000000000000000000
          Width = 416.756030000000000000
          Height = 18.000000000000000000
          DataField = 'Nome'
          DataSet = frxDsTotalSaldo
          DataSetName = 'frxDsTotalSaldo'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsTotalSaldo."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 536.692918270000000000
          Width = 105.826771650000000000
          Height = 18.000000000000000000
          DataSet = frxDsTotalSaldo
          DataSetName = 'frxDsTotalSaldo'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsTotalSaldo."Saldo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 98.267716540000000000
        Top = 18.897650000000000000
        Width = 702.614627000000000000
        object Memo28: TfrxMemoView
          Left = 120.000000000000000000
          Top = 79.370078740157480000
          Width = 416.756030000000000000
          Height = 18.897637795275590000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 536.692913390000000000
          Top = 79.370078740157480000
          Width = 105.826771650000000000
          Height = 18.897637795275590000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Top = 79.370078740157480000
          Width = 116.000000000000000000
          Height = 18.897637795275590000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Status')
          ParentFont = False
        end
        object Memo211: TfrxMemoView
          Top = 60.472480000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'SALDOS FUTUROS DAS CARTEIRAS EXTRATO E CAIXA SEM AS EMISS'#213'ES')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 38.000000000000000000
        Top = 355.275820000000000000
        Width = 702.614627000000000000
        object Memo11: TfrxMemoView
          Left = 423.307086610000000000
          Top = 7.559057559999985000
          Width = 104.000000000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Total Geral:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 536.692913389999900000
          Top = 7.559057559999985000
          Width = 105.826771650000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsTotalSaldo."Saldo">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 5.385590000000000000
          Width = 716.440940000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 177.637910000000000000
        Width = 702.614627000000000000
        Condition = 'frxDsTotalSaldo."Tipo"'
        object Memo13: TfrxMemoView
          Left = 4.000000000000000000
          Width = 332.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsTotalSaldo."NOMETIPO"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 34.000000000000000000
        Top = 260.787570000000000000
        Width = 702.614627000000000000
        object Memo1: TfrxMemoView
          Left = 536.692913389999900000
          Top = 3.779527560000020000
          Width = 105.826771650000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsTotalSaldo."Saldo">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 423.307086614173300000
          Top = 3.779527560000020000
          Width = 104.000000000000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter3: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 415.748300000000000000
        Width = 702.614627000000000000
        object Memo120: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DataSet = frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object QrSaldos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Saldo, Tipo'
      'FROM Carteiras'
      'WHERE Tipo <> 2'
      'AND ForneceI=:P0')
    Left = 704
    Top = 356
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSaldosNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 8
    end
    object QrSaldosSaldo: TFloatField
      FieldName = 'Saldo'
      Origin = 'DBMMONEY.carteiras.Saldo'
    end
    object QrSaldosTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrSaldosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object frxDsTotalSaldo: TfrxDBDataset
    UserName = 'frxDsTotalSaldo'
    CloseDataSource = False
    DataSet = QrTotalSaldo
    BCDToCurrency = False
    Left = 708
    Top = 452
  end
  object QrEndossad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Valor) Valor '
      'FROM lctoendoss'
      'WHERE Controle=:P0 AND Sub=:P1')
    Left = 456
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEndossadValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrDuplVal: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDuplValCalcFields
    SQL.Strings = (
      'SELECT lan.Data, lan.Controle, lan.Descricao, lan.Credito, '
      'lan.Debito, lan.NotaFiscal, lan.Compensado, lan.Mez, SerieCH,'
      'lan.Fornecedor, lan.Cliente, car.Nome NOMECART, Documento, '
      'IF(lan.Cliente<>0, CASE WHEN cli.Tipo=0 THEN '
      'cli.RazaoSocial ELSE cli.Nome END, "") NOMECLI,'
      'IF(lan.Fornecedor<>0, CASE WHEN fnc.Tipo=0 THEN '
      'fnc.RazaoSocial ELSE fnc.Nome END, "") NOMEFNC,'
      'lan.Carteira'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN entidades cli ON cli.Codigo=lan.Cliente'
      'LEFT JOIN entidades fnc ON fnc.Codigo=lan.Fornecedor'
      'WHERE ID_Pgto = 0'
      'AND Credito=:P0'
      'AND Debito=:P1'
      'AND Genero=:P2'
      'AND Mez=:P3'
      'AND lan.Cliente=:P4'
      'AND lan.Fornecedor=:P5'
      'AND car.ForneceI=:P6'
      '')
    Left = 212
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end>
    object QrDuplValData: TDateField
      FieldName = 'Data'
      Origin = 'lanctos.Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDuplValControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'lanctos.Controle'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrDuplValDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'lanctos.Descricao'
      Size = 100
    end
    object QrDuplValCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'lanctos.Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDuplValDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'lanctos.Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDuplValNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'lanctos.NotaFiscal'
      DisplayFormat = '000000;-000000; '
    end
    object QrDuplValCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'lanctos.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDuplValMez: TIntegerField
      FieldName = 'Mez'
      Origin = 'lanctos.Mez'
      Required = True
    end
    object QrDuplValFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Origin = 'lanctos.Fornecedor'
    end
    object QrDuplValCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'lanctos.Cliente'
    end
    object QrDuplValNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrDuplValNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrDuplValNOMEFNC: TWideStringField
      FieldName = 'NOMEFNC'
      Size = 100
    end
    object QrDuplValDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'lanctos.Documento'
    end
    object QrDuplValSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Origin = 'lanctos.SerieCH'
      Size = 10
    end
    object QrDuplValCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'lanctos.Carteira'
      Required = True
    end
    object QrDuplValTERCEIRO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TERCEIRO'
      Size = 255
      Calculated = True
    end
    object QrDuplValMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 8
      Calculated = True
    end
    object QrDuplValCHEQUE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CHEQUE'
      Calculated = True
    end
    object QrDuplValCOMP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMP_TXT'
      Size = 10
      Calculated = True
    end
  end
  object DsDuplVal: TDataSource
    DataSet = QrDuplVal
    Left = 272
    Top = 196
  end
  object QrSaldosNiv: TmySQLQuery
    Database = Dmod.MyDB
    Left = 212
    Top = 244
  end
  object QrSdoPar: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(SdoAnt) SdoAnt, SUM(Credito) Credito, '
      'SUM(Debito) Debito, SUM(Movim) Movim '
      'FROM saldosniv;')
    Left = 392
    Top = 244
    object QrSdoParSdoAnt: TFloatField
      FieldName = 'SdoAnt'
    end
    object QrSdoParCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrSdoParDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrSdoParMovim: TFloatField
      FieldName = 'Movim'
    end
  end
  object QrExtratos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM extratocc'
      
        'ORDER BY CartO, CartN, CartC, DataM, SdIni, DebCr, Unida,  Docum' +
        ', TipoI')
    Left = 516
    Top = 4
    object QrExtratosLinha: TAutoIncField
      FieldName = 'Linha'
    end
    object QrExtratosDataM: TDateField
      FieldName = 'DataM'
    end
    object QrExtratosTexto: TWideStringField
      FieldName = 'Texto'
      Size = 100
    end
    object QrExtratosDocum: TWideStringField
      FieldName = 'Docum'
      Size = 30
    end
    object QrExtratosNotaF: TIntegerField
      FieldName = 'NotaF'
    end
    object QrExtratosCredi: TFloatField
      FieldName = 'Credi'
    end
    object QrExtratosDebit: TFloatField
      FieldName = 'Debit'
    end
    object QrExtratosSaldo: TFloatField
      FieldName = 'Saldo'
    end
    object QrExtratosCartO: TIntegerField
      FieldName = 'CartO'
    end
    object QrExtratosCartC: TIntegerField
      FieldName = 'CartC'
    end
    object QrExtratosCartN: TWideStringField
      FieldName = 'CartN'
      Size = 100
    end
    object QrExtratosSdIni: TIntegerField
      FieldName = 'SdIni'
    end
    object QrExtratosTipoI: TIntegerField
      FieldName = 'TipoI'
    end
    object QrExtratosUnida: TWideStringField
      FieldName = 'Unida'
    end
    object QrExtratosCTipN: TWideStringField
      FieldName = 'CTipN'
      Size = 30
    end
    object QrExtratosDebCr: TWideStringField
      FieldName = 'DebCr'
      Size = 1
    end
  end
  object QrZer: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CartC, SUM(Credi) Credi, SUM(Debit) Debit'
      'FROM extratocc'
      'GROUP BY CartC')
    Left = 696
    Top = 4
    object QrZerCartC: TIntegerField
      FieldName = 'CartC'
    end
    object QrZerCredi: TFloatField
      FieldName = 'Credi'
    end
    object QrZerDebit: TFloatField
      FieldName = 'Debit'
    end
  end
  object QrLocPerX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM prev'
      'WHERE Cond=:P0'
      'AND Periodo=:P1')
    Left = 212
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocPerXCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object frxDsPrevItO: TfrxDBDataset
    UserName = 'frxDsPrevItO'
    CloseDataSource = False
    DataSet = QrPrevItO
    BCDToCurrency = False
    Left = 332
    Top = 436
  end
  object QrPrevItO: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pit.*, con.SubGrupo, '
      'con.Nome NOMECONTA, sgo.Nome NOMESUBGRUPO '
      'FROM previts pit'
      'LEFT JOIN contas con ON con.Codigo=pit.Conta'
      'LEFT JOIN subgrupos sgo ON sgo.Codigo=con.SubGrupo'
      'WHERE pit.Codigo=:P0'
      'ORDER BY NOMESUBGRUPO, NOMECONTA'
      '')
    Left = 272
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrevItOCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPrevItOControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPrevItOConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrPrevItOValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrPrevItOPrevBaI: TIntegerField
      FieldName = 'PrevBaI'
      Required = True
    end
    object QrPrevItOTexto: TWideStringField
      FieldName = 'Texto'
      Required = True
      Size = 50
    end
    object QrPrevItOLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPrevItODataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPrevItODataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPrevItOUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPrevItOUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPrevItOPrevBaC: TIntegerField
      FieldName = 'PrevBaC'
      Required = True
    end
    object QrPrevItOEmitVal: TFloatField
      FieldName = 'EmitVal'
      Required = True
    end
    object QrPrevItOEmitSit: TIntegerField
      FieldName = 'EmitSit'
      Required = True
    end
    object QrPrevItOSubGrupo: TIntegerField
      FieldName = 'SubGrupo'
    end
    object QrPrevItONOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrPrevItONOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 50
    end
  end
  object QrPgBloq: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPgBloqCalcFields
    SQL.Strings = (
      'SELECT IF(lan.Compensado=0, 0, IF(car.Tipo=2, '
      'lan.Credito + lan.PagMul + lan.PagJur, lan.Credito)) Credito, '
      
        'IF(lan.Compensado=0, 0, IF(car.Tipo=2, lan.PagMul, lan.MultaVal)' +
        ') MultaVal,'
      
        'IF(lan.Compensado=0, 0, IF(car.Tipo=2, lan.PagJur, lan.MoraVal))' +
        ' MoraVal, '
      'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE '
      'ent.Nome END NOMEPROPRIET, imv.Unidade UH, '
      'con.Nome NOMECONTA, lan.Mez, lan.Vencimento, '
      'lan.Documento, lan.Credito ORIGINAL, lan.Compensado Data,'
      
        'IF(lan.Compensado=0, "", DATE_FORMAT(lan.Compensado, "%d/%m/%Y")' +
        ') DATA_TXT'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN condimov imv ON imv.Conta=lan.Depto'
      'LEFT JOIN entidades ent ON ent.Codigo=lan.ForneceI'
      'LEFT JOIN contas con ON con.Codigo=lan.Genero'
      'WHERE car.Tipo in (0,2)'
      'AND lan.FatID in (600,601)'
      'AND car.ForneceI=:P0'
      'AND lan.Mez=:P1'
      ''
      'ORDER BY UH, lan.Documento, con.OrdemLista, NOMECONTA')
    Left = 212
    Top = 484
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPgBloqCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrPgBloqMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrPgBloqMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrPgBloqNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
    object QrPgBloqUH: TWideStringField
      FieldName = 'UH'
      Size = 10
    end
    object QrPgBloqNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrPgBloqMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrPgBloqVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrPgBloqDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPgBloqORIGINAL: TFloatField
      FieldName = 'ORIGINAL'
    end
    object QrPgBloqData: TDateField
      FieldName = 'Data'
    end
    object QrPgBloqMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 8
      Calculated = True
    end
    object QrPgBloqDATA_TXT: TWideStringField
      FieldName = 'DATA_TXT'
      Size = 10
    end
  end
  object frxDsPgBloq: TfrxDBDataset
    UserName = 'frxDsPgBloq'
    CloseDataSource = False
    DataSet = QrPgBloq
    BCDToCurrency = False
    Left = 272
    Top = 484
  end
  object frxDsSuBloq: TfrxDBDataset
    UserName = 'frxDsSuBloq'
    CloseDataSource = False
    DataSet = QrSuBloq
    BCDToCurrency = False
    Left = 392
    Top = 484
  end
  object QrSuBloq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(IF(lan.Compensado=0, 0, IF(car.Tipo=2, '
      'lan.Credito + lan.PagMul + lan.PagJur, lan.Credito))) PAGO, '
      
        'SUM(IF(lan.Compensado=0, 0, IF(car.Tipo=2, lan.PagMul, lan.Multa' +
        'Val))) MultaVal,'
      
        'SUM(IF(lan.Compensado=0, 0, IF(car.Tipo=2, lan.PagJur, lan.MoraV' +
        'al))) MoraVal, '
      'con.Nome NOMECONTA, SUM(lan.Credito) ORIGINAL, 0 KGT'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN condimov imv ON imv.Conta=lan.Depto'
      'LEFT JOIN entidades ent ON ent.Codigo=lan.ForneceI'
      'LEFT JOIN contas con ON con.Codigo=lan.Genero'
      'WHERE car.Tipo in (0,2)'
      'AND lan.FatID in (600,601)'
      'AND car.ForneceI=:P0'
      'AND lan.Mez=:P1'#13
      'GROUP BY NOMECONTA')
    Left = 332
    Top = 484
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSuBloqMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrSuBloqMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrSuBloqNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrSuBloqORIGINAL: TFloatField
      FieldName = 'ORIGINAL'
    end
    object QrSuBloqKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
    object QrSuBloqPAGO: TFloatField
      FieldName = 'PAGO'
    end
  end
  object QrPendG: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPendGCalcFields
    SQL.Strings = (
      'SELECT lan.Cliente, lan.Fornecedor, lan.FatNum,'
      'lan.Mez, SUM(lan.Credito) Credito, lan.Vencimento,'
      'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial '
      'ELSE ent.Nome END NOMEPROPRIET, imv.Unidade'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN entidades ent ON ent.Codigo=lan.ForneceI'
      'LEFT JOIN condimov  imv ON imv.Conta=lan.Depto'
      ''
      'WHERE car.Tipo=2'
      'AND lan.Reparcel=0'
      'AND car.ForneceI=:P0'
      'AND imv.Codigo=:P1'
      'AND lan.Depto>0'
      'AND lan.Sit<2'
      'AND lan.Vencimento < SYSDATE()'
      
        'GROUP BY imv.Controle, imv.Unidade, lan.Mez, lan.Vencimento, lan' +
        '.FatNum'
      
        'ORDER BY imv.Controle, imv.Unidade, lan.Mez, lan.Vencimento, lan' +
        '.FatNum')
    Left = 272
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPendGCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPendGFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrPendGMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrPendGCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrPendGNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
    object QrPendGUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrPendGNOMEMEZ: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEMEZ'
      Size = 30
      Calculated = True
    end
    object QrPendGVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrPendGFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object frxDsPendG: TfrxDBDataset
    UserName = 'frxDsPendG'
    CloseDataSource = False
    DataSet = QrPendG
    BCDToCurrency = False
    Left = 332
    Top = 100
  end
  object frxDsRep: TfrxDBDataset
    UserName = 'frxDsRep'
    CloseDataSource = False
    DataSet = QrRep
    BCDToCurrency = False
    Left = 572
    Top = 340
  end
  object QrRepBPP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM bloqparcpar'
      'WHERE Codigo=:P0'
      'ORDER BY Vencimento')
    Left = 512
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrRepBPPCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'bloqparcpar.Codigo'
    end
    object QrRepBPPControle: TAutoIncField
      FieldName = 'Controle'
      Origin = 'bloqparcpar.Controle'
    end
    object QrRepBPPParcela: TIntegerField
      FieldName = 'Parcela'
      Origin = 'bloqparcpar.Parcela'
    end
    object QrRepBPPVlrOrigi: TFloatField
      FieldName = 'VlrOrigi'
      Origin = 'bloqparcpar.VlrOrigi'
      DisplayFormat = '#,###,##0.00'
    end
    object QrRepBPPVlrMulta: TFloatField
      FieldName = 'VlrMulta'
      Origin = 'bloqparcpar.VlrMulta'
      DisplayFormat = '#,###,##0.00'
    end
    object QrRepBPPVlrJuros: TFloatField
      FieldName = 'VlrJuros'
      Origin = 'bloqparcpar.VlrJuros'
      DisplayFormat = '#,###,##0.00'
    end
    object QrRepBPPLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'bloqparcpar.Lk'
    end
    object QrRepBPPDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'bloqparcpar.DataCad'
    end
    object QrRepBPPDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'bloqparcpar.DataAlt'
    end
    object QrRepBPPUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'bloqparcpar.UserCad'
    end
    object QrRepBPPUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'bloqparcpar.UserAlt'
    end
    object QrRepBPPAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'bloqparcpar.AlterWeb'
    end
    object QrRepBPPLancto: TIntegerField
      FieldName = 'Lancto'
      Origin = 'bloqparcpar.Lancto'
    end
    object QrRepBPPValBol: TFloatField
      FieldName = 'ValBol'
      Origin = 'bloqparcpar.ValBol'
      DisplayFormat = '#,###,##0.00'
    end
    object QrRepBPPVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'bloqparcpar.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrRepBPPVlrTotal: TFloatField
      FieldName = 'VlrTotal'
      Origin = 'bloqparcpar.VlrTotal'
    end
    object QrRepBPPAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'bloqparcpar.Ativo'
    end
    object QrRepBPPFatNum: TFloatField
      FieldName = 'FatNum'
      Origin = 'bloqparcpar.FatNum'
    end
  end
  object frxDsRepBPP: TfrxDBDataset
    UserName = 'frxDsRepBPP'
    CloseDataSource = False
    DataSet = QrRepBPP
    BCDToCurrency = False
    Left = 572
    Top = 388
  end
  object QrRep: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrRepAfterScroll
    SQL.Strings = (
      'SELECT bpa.*, cim.Unidade,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP '
      'FROM bloqparc bpa'
      'LEFT JOIN entidades ent ON ent.Codigo=bpa.CodigoEnt'
      'LEFT JOIN condimov  cim ON cim.Conta=bpa.CodigoEsp'
      'WHERE bpa.CodCliEnt=:P0'
      'AND bpa.DataP BETWEEN :P1 AND :P2 '
      'ORDER BY bpa.DataP, cim.Unidade')
    Left = 512
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrRepCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrReploginID: TWideStringField
      FieldName = 'loginID'
      Required = True
      Size = 32
    end
    object QrRepautentica: TWideMemoField
      FieldName = 'autentica'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrRepCodCliEsp: TIntegerField
      FieldName = 'CodCliEsp'
      Required = True
    end
    object QrRepCodCliEnt: TIntegerField
      FieldName = 'CodCliEnt'
      Required = True
    end
    object QrRepCodigoEnt: TIntegerField
      FieldName = 'CodigoEnt'
      Required = True
    end
    object QrRepCodigoEsp: TIntegerField
      FieldName = 'CodigoEsp'
      Required = True
    end
    object QrRepVlrOrigi: TFloatField
      FieldName = 'VlrOrigi'
      Required = True
    end
    object QrRepVlrMulta: TFloatField
      FieldName = 'VlrMulta'
      Required = True
    end
    object QrRepVlrJuros: TFloatField
      FieldName = 'VlrJuros'
      Required = True
    end
    object QrRepVlrTotal: TFloatField
      FieldName = 'VlrTotal'
      Required = True
    end
    object QrRepDataP: TDateTimeField
      FieldName = 'DataP'
      Required = True
    end
    object QrRepNovo: TSmallintField
      FieldName = 'Novo'
      Required = True
    end
    object QrRepTxaJur: TFloatField
      FieldName = 'TxaJur'
      Required = True
    end
    object QrRepTxaMul: TFloatField
      FieldName = 'TxaMul'
      Required = True
    end
    object QrRepLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrRepDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrRepDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrRepUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrRepUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrRepAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrRepAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrRepUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrRepNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Size = 100
    end
  end
  object QrRepori: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT BloqOrigi, SUM(VlrOrigi) VlrOrigi, '
      'SUM(VlrMulta) VlrMulta, SUM(VlrJuros+VlrAjust) VlrJuros,'
      'SUM(VlrTotal) VlrTotal'
      'FROM bloqparcits'
      'WHERE Codigo=:P0'
      ''
      'GROUP BY BloqOrigi')
    Left = 452
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrReporiBloqOrigi: TIntegerField
      FieldName = 'BloqOrigi'
    end
    object QrReporiVlrOrigi: TFloatField
      FieldName = 'VlrOrigi'
    end
    object QrReporiVlrMulta: TFloatField
      FieldName = 'VlrMulta'
    end
    object QrReporiVlrJuros: TFloatField
      FieldName = 'VlrJuros'
    end
    object QrReporiVlrTotal: TFloatField
      FieldName = 'VlrTotal'
    end
  end
  object QrReplan: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Reparcel, FatNum, SUM(Credito) Credito, Vencimento '
      'FROM lanctos'
      'WHERE Reparcel=:P0'
      'GROUP BY FatNum'
      'ORDER BY Vencimento')
    Left = 512
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrReplanCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrReplanVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrReplanVLRORIG: TFloatField
      FieldKind = fkLookup
      FieldName = 'VLRORIG'
      LookupDataSet = QrRepori
      LookupKeyFields = 'BloqOrigi'
      LookupResultField = 'VlrOrigi'
      KeyFields = 'FatNum'
      Lookup = True
    end
    object QrReplanVLRMULTA: TFloatField
      FieldKind = fkLookup
      FieldName = 'VLRMULTA'
      LookupDataSet = QrRepori
      LookupKeyFields = 'BloqOrigi'
      LookupResultField = 'VlrMulta'
      KeyFields = 'FatNum'
      Lookup = True
    end
    object QrReplanVLRJUROS: TFloatField
      FieldKind = fkLookup
      FieldName = 'VLRJUROS'
      LookupDataSet = QrRepori
      LookupKeyFields = 'BloqOrigi'
      LookupResultField = 'VlrJuros'
      KeyFields = 'FatNum'
      Lookup = True
    end
    object QrReplanVLRTOTAL: TFloatField
      FieldKind = fkLookup
      FieldName = 'VLRTOTAL'
      LookupDataSet = QrRepori
      LookupKeyFields = 'BloqOrigi'
      LookupResultField = 'VlrTotal'
      KeyFields = 'FatNum'
      Lookup = True
    end
    object QrReplanReparcel: TIntegerField
      FieldName = 'Reparcel'
    end
    object QrReplanFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object QrRepAbe: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrRepAbeCalcFields
    SQL.Strings = (
      'SELECT imv.Unidade, lan.Data, lan.Vencimento, lan.Controle, '
      'lan.Descricao, lan.Credito, lan.Compensado, lan.FatNum, '
      'lan.MoraDia, lan.Multa, lan.Depto, lan.Sit,'
      'lan.Pago, lan.PagMul, lan.PagJur,  lan.Atrelado,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN entidades ent ON ent.Codigo=lan.ForneceI'
      'LEFT JOIN condimov  imv ON imv.Conta=lan.Depto'
      'WHERE lan.Atrelado>0'
      'AND lan.Genero=-10'
      'AND car.Tipo=2'
      'AND car.ForneceI=:P0'
      'AND lan.Sit < 2'
      'ORDER BY NOMEPROP, lan.Atrelado, lan.Vencimento')
    Left = 512
    Top = 484
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrRepAbeUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrRepAbeData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrRepAbeVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrRepAbeControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrRepAbeDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrRepAbeCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrRepAbeCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrRepAbeMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrRepAbeMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrRepAbeDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrRepAbeSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrRepAbePago: TFloatField
      FieldName = 'Pago'
    end
    object QrRepAbePagMul: TFloatField
      FieldName = 'PagMul'
      Required = True
    end
    object QrRepAbePagJur: TFloatField
      FieldName = 'PagJur'
      Required = True
    end
    object QrRepAbeNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Size = 100
    end
    object QrRepAbeAtrelado: TIntegerField
      FieldName = 'Atrelado'
      Required = True
    end
    object QrRepAbeCompensado_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Compensado_TXT'
      Size = 8
      Calculated = True
    end
    object QrRepAbeFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object frxDsRepLan: TfrxDBDataset
    UserName = 'frxDsRepLan'
    CloseDataSource = False
    DataSet = QrReplan
    BCDToCurrency = False
    Left = 572
    Top = 436
  end
  object frxDsRepAbe: TfrxDBDataset
    UserName = 'frxDsRepAbe'
    CloseDataSource = False
    DataSet = QrRepAbe
    BCDToCurrency = False
    Left = 572
    Top = 484
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT PBB'
      'FROM cond'
      'WHERE Codigo=1')
    Left = 452
    Top = 388
    object QrCliIntPBB: TSmallintField
      FieldName = 'PBB'
    end
  end
  object frxDsPendAll: TfrxDBDataset
    UserName = 'frxDsPendAll'
    CloseDataSource = False
    DataSet = QrPendAll
    BCDToCurrency = False
    Left = 572
    Top = 244
  end
  object QrPendAll: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT "   " Unidade, SUM(lan.Credito) Credito,'
      '"    " TIPO, 0 KGT, '
      'COUNT(DISTINCT lan.FatNum) BLOQUETOS'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      '/*LEFT JOIN condimov  imv ON imv.Conta=lan.Depto*/'
      'WHERE car.Tipo=2'
      'AND lan.Reparcel=0'
      'AND car.ForneceI=:P0'
      '/*AND imv.Codigo=:P1*/'
      'AND lan.Depto>0'
      'AND lan.Sit<2'
      'AND lan.Vencimento <= :P2'
      'GROUP BY lan.Depto'
      ''
      'UNION'
      ''
      'SELECT "   " Unidade, SUM(lan.Credito) Credito,'
      '"      " TIPO, 0 KGT, '
      'COUNT(DISTINCT lan.FatNum) BLOQUETOS'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      '/*LEFT JOIN condimov  imv ON imv.Conta=lan.Depto*/'
      'WHERE lan.Atrelado>0'
      'AND lan.Genero=-10'
      'AND car.Tipo=2'
      'AND lan.Sit < 2'
      'AND car.ForneceI=:P3'
      'AND lan.Vencimento <= :P4'
      'GROUP BY lan.Depto'
      ''
      'ORDER BY Unidade')
    Left = 512
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1*/'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrPendAllUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrPendAllCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrPendAllTIPO: TWideStringField
      FieldName = 'TIPO'
      Required = True
      Size = 17
    end
    object QrPendAllKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
    object QrPendAllBLOQUETOS: TLargeintField
      FieldName = 'BLOQUETOS'
      Required = True
    end
  end
  object QrPendSum: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(lan.Credito) Credito'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      '/*LEFT JOIN condimov  imv ON imv.Conta=lan.Depto*/'
      'WHERE (car.Tipo=2'
      'AND lan.Reparcel=0'
      'AND car.ForneceI=:P0'
      '/*AND imv.Codigo=:P1*/'
      'AND lan.Depto>0'
      'AND lan.Sit<2'
      'AND lan.Vencimento <=:P2)'
      ''
      'OR'
      ''
      '(lan.Atrelado>0'
      'AND lan.Genero=-10'
      'AND car.Tipo=2'
      'AND car.ForneceI=:P3'
      'AND lan.Vencimento <=:P4'
      'AND lan.Sit < 2'#13
      ')'#10)
    Left = 512
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1*/'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrPendSumCredito: TFloatField
      FieldName = 'Credito'
    end
  end
  object frxDsPendSum: TfrxDBDataset
    UserName = 'frxDsPendSum'
    CloseDataSource = False
    DataSet = QrPendSum
    BCDToCurrency = False
    Left = 572
    Top = 292
  end
  object QrLctTipCart: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lct.Tipo TipoA, crt.Tipo TipoB, lct.Data, '
      'lct.Carteira, lct.Controle, lct.Sub, '
      'lct.Credito, lct.Debito '
      'FROM lanctos lct'
      'LEFT JOIN carteiras crt ON crt.Codigo=lct.Carteira'
      'WHERE crt.Tipo<>lct.Tipo')
    Left = 812
    Top = 252
    object QrLctTipCartTipoA: TSmallintField
      FieldName = 'TipoA'
    end
    object QrLctTipCartTipoB: TIntegerField
      FieldName = 'TipoB'
      Required = True
    end
    object QrLctTipCartData: TDateField
      FieldName = 'Data'
    end
    object QrLctTipCartCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctTipCartControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLctTipCartSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrLctTipCartCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrLctTipCartDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object DsLctTipCart: TDataSource
    DataSet = QrLctTipCart
    Left = 872
    Top = 252
  end
  object QrDelLct: TmySQLQuery
    Database = Dmod.MyDB
    Left = 780
    Top = 304
  end
  object QrFats3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data, Tipo, Carteira, Controle, Sub'
      ' FROM lanctos'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND FatParcela=:P2')
    Left = 780
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrFats3Data: TDateField
      FieldName = 'Data'
    end
    object QrFats3Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrFats3Carteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrFats3Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFats3Sub: TSmallintField
      FieldName = 'Sub'
    end
  end
  object QrFats2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data, Tipo, Carteira, Controle, Sub'
      ' FROM lanctos'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND CliInt=:P2')
    Left = 780
    Top = 400
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrFats2Data: TDateField
      FieldName = 'Data'
    end
    object QrFats2Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrFats2Carteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrFats2Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFats2Sub: TSmallintField
      FieldName = 'Sub'
    end
  end
  object QrFats1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data, Tipo, Carteira, Controle, Sub'
      'FROM lanctos'
      'WHERE FatID=:P0')
    Left = 780
    Top = 448
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFats1Data: TDateField
      FieldName = 'Data'
    end
    object QrFats1Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrFats1Carteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrFats1Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFats1Sub: TSmallintField
      FieldName = 'Sub'
    end
  end
  object QrLcts: TmySQLQuery
    Database = Dmod.MyDB
    Left = 836
    Top = 304
    object QrLctsData: TDateField
      FieldName = 'Data'
    end
    object QrLctsTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLctsCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLctsSub: TSmallintField
      FieldName = 'Sub'
    end
  end
end
