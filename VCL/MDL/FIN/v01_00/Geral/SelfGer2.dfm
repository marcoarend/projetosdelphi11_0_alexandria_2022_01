object FmSelfGer2: TFmSelfGer2
  Left = 339
  Top = 169
  Caption = 'FIN-SELFG-001 :: Minhas Finan'#231'as'
  ClientHeight = 555
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Minhas Finan'#231'as'
    Font.Charset = ANSI_CHARSET
    Font.Color = 8404992
    Font.Height = -32
    Font.Name = 'Calibri'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 928
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 507
    Align = alClient
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 1006
      Height = 505
      ActivePage = TabSheet3
      Align = alClient
      TabOrder = 0
      OnChange = PageControl1Change
      object TabSheet3: TTabSheet
        Caption = ' Lan'#231'amentos '
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PainelLct: TPanel
          Left = 0
          Top = 0
          Width = 998
          Height = 477
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object PainelDados2: TPanel
            Left = 0
            Top = 0
            Width = 998
            Height = 233
            Align = alTop
            BevelOuter = bvNone
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 998
              Height = 233
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel7: TPanel
                Left = 0
                Top = 0
                Width = 998
                Height = 45
                Align = alTop
                TabOrder = 0
                object Label20: TLabel
                  Left = 648
                  Top = 6
                  Width = 80
                  Height = 13
                  Caption = 'Soma das linhas:'
                end
                object Label5: TLabel
                  Left = 792
                  Top = 4
                  Width = 36
                  Height = 13
                  Caption = 'Cr'#233'dito:'
                  FocusControl = DBEdit13
                end
                object Label19: TLabel
                  Left = 860
                  Top = 4
                  Width = 34
                  Height = 13
                  Caption = 'D'#233'bito:'
                  FocusControl = DBEdit14
                end
                object Label21: TLabel
                  Left = 928
                  Top = 4
                  Width = 30
                  Height = 13
                  Caption = 'Saldo:'
                  FocusControl = DBEdit15
                end
                object BtSaldoAqui: TBitBtn
                  Tag = 238
                  Left = 464
                  Top = 3
                  Width = 100
                  Height = 40
                  Caption = 'Saldo A&qui'
                  Enabled = False
                  TabOrder = 0
                  OnClick = BtSaldoAquiClick
                end
                object EdSdoAqui: TdmkEdit
                  Left = 568
                  Top = 20
                  Width = 77
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                end
                object BtRefresh: TBitBtn
                  Tag = 18
                  Left = 733
                  Top = 3
                  Width = 40
                  Height = 40
                  Cursor = crHandPoint
                  Enabled = False
                  NumGlyphs = 2
                  TabOrder = 2
                  OnClick = BtRefreshClick
                end
                object EdSoma2: TdmkEdit
                  Left = 648
                  Top = 20
                  Width = 81
                  Height = 21
                  TabStop = False
                  Alignment = taRightJustify
                  Color = clBtnFace
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                end
                object DBEdit13: TDBEdit
                  Left = 792
                  Top = 20
                  Width = 64
                  Height = 21
                  DataField = 'CREDITO'
                  TabOrder = 4
                end
                object DBEdit14: TDBEdit
                  Left = 860
                  Top = 20
                  Width = 64
                  Height = 21
                  DataField = 'DEBITO'
                  TabOrder = 5
                end
                object DBEdit15: TDBEdit
                  Left = 928
                  Top = 20
                  Width = 64
                  Height = 21
                  DataField = 'SALDO'
                  TabOrder = 6
                end
              end
              object Panel6: TPanel
                Left = 0
                Top = 45
                Width = 998
                Height = 188
                Align = alClient
                TabOrder = 1
                object Panel3: TPanel
                  Left = 1
                  Top = 1
                  Width = 996
                  Height = 45
                  Align = alTop
                  TabOrder = 0
                  object BtMenu: TBitBtn
                    Tag = 237
                    Left = 8
                    Top = 2
                    Width = 90
                    Height = 40
                    Caption = '&Menu'
                    NumGlyphs = 2
                    TabOrder = 0
                    OnClick = BtMenuClick
                  end
                  object BtInclui: TBitBtn
                    Tag = 10
                    Left = 100
                    Top = 2
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 1
                    OnClick = BtIncluiClick
                  end
                  object BtAltera: TBitBtn
                    Tag = 11
                    Left = 142
                    Top = 2
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    Enabled = False
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 2
                    OnClick = BtAlteraClick
                  end
                  object BtExclui: TBitBtn
                    Tag = 12
                    Left = 184
                    Top = 2
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    Enabled = False
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 3
                    OnClick = BtExcluiClick
                  end
                  object BtEspecificos: TBitBtn
                    Left = 226
                    Top = 2
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 4
                    Visible = False
                    OnClick = BtEspecificosClick
                  end
                  object BtConcilia: TBitBtn
                    Tag = 10011
                    Left = 268
                    Top = 2
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 5
                    OnClick = BtConciliaClick
                  end
                  object BtQuita: TBitBtn
                    Tag = 10024
                    Left = 310
                    Top = 2
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 6
                    OnClick = BtQuitaClick
                  end
                  object BtCNAB: TBitBtn
                    Tag = 10018
                    Left = 352
                    Top = 2
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 7
                    OnClick = BtCNABClick
                  end
                  object BtCopiaCH: TBitBtn
                    Tag = 10019
                    Left = 394
                    Top = 2
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 8
                    OnClick = BtCopiaCHClick
                  end
                  object BtContarDinheiro: TBitBtn
                    Tag = 239
                    Left = 436
                    Top = 2
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    Enabled = False
                    NumGlyphs = 2
                    TabOrder = 9
                    OnClick = BtContarDinheiroClick
                  end
                  object BtFluxoCxa: TBitBtn
                    Tag = 10025
                    Left = 478
                    Top = 2
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    TabOrder = 10
                    OnClick = BtFluxoCxaClick
                  end
                  object BtAutom: TBitBtn
                    Tag = 174
                    Left = 520
                    Top = 2
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    TabOrder = 11
                    OnClick = BtAutomClick
                  end
                  object BtTrfCta: TBitBtn
                    Left = 562
                    Top = 2
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    Caption = 'trf cta'
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 12
                    OnClick = BtTrfCtaClick
                  end
                  object BtLctoEndoss: TBitBtn
                    Left = 604
                    Top = 2
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    Caption = 'endos'
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 13
                    OnClick = BtLctoEndossClick
                  end
                  object BtRelat: TBitBtn
                    Left = 646
                    Top = 2
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    Caption = 'Relat.'
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 14
                    OnClick = BtRelatClick
                  end
                end
                object Panel4: TPanel
                  Left = 1
                  Top = 46
                  Width = 996
                  Height = 141
                  Align = alClient
                  TabOrder = 1
                  object Label14: TLabel
                    Left = 196
                    Top = 8
                    Width = 73
                    Height = 13
                    Alignment = taRightJustify
                    AutoSize = False
                    Caption = 'Dono carteira:'
                  end
                  object Label8: TLabel
                    Left = 196
                    Top = 32
                    Width = 73
                    Height = 13
                    Alignment = taRightJustify
                    AutoSize = False
                    Caption = 'Dono lan'#231'am.:'
                  end
                  object Label15: TLabel
                    Left = 196
                    Top = 56
                    Width = 73
                    Height = 13
                    Alignment = taRightJustify
                    AutoSize = False
                    Caption = 'Cliente:'
                  end
                  object Label17: TLabel
                    Left = 196
                    Top = 80
                    Width = 73
                    Height = 13
                    Alignment = taRightJustify
                    AutoSize = False
                    Caption = 'Fornecedor:'
                  end
                  object Label16: TLabel
                    Left = 580
                    Top = 8
                    Width = 73
                    Height = 13
                    Alignment = taRightJustify
                    AutoSize = False
                    Caption = 'Carteira:'
                  end
                  object Label18: TLabel
                    Left = 580
                    Top = 32
                    Width = 73
                    Height = 13
                    Alignment = taRightJustify
                    AutoSize = False
                    Caption = 'Conta (Plano):'
                  end
                  object Label1: TLabel
                    Left = 763
                    Top = 112
                    Width = 25
                    Height = 13
                    Caption = 'U.H.:'
                  end
                  object CkDataI: TCheckBox
                    Left = 4
                    Top = 6
                    Width = 72
                    Height = 17
                    Caption = 'Data inicial:'
                    Checked = True
                    State = cbChecked
                    TabOrder = 0
                    OnClick = CkDataIClick
                  end
                  object TPDataI: TdmkEditDateTimePicker
                    Left = 81
                    Top = 4
                    Width = 112
                    Height = 21
                    Date = 39328.122587314800000000
                    Time = 39328.122587314800000000
                    TabOrder = 1
                    OnChange = TPDataIChange
                    ReadOnly = False
                    DefaultEditMask = '!99/99/99;1;_'
                    AutoApplyEditMask = True
                    UpdType = utYes
                  end
                  object CkDataF: TCheckBox
                    Left = 4
                    Top = 30
                    Width = 73
                    Height = 17
                    Caption = 'Data final:'
                    Checked = True
                    State = cbChecked
                    TabOrder = 2
                    OnClick = CkDataFClick
                  end
                  object TPDataF: TdmkEditDateTimePicker
                    Left = 80
                    Top = 28
                    Width = 112
                    Height = 21
                    Date = 39328.122587314800000000
                    Time = 39328.122587314800000000
                    TabOrder = 3
                    OnChange = TPDataFChange
                    ReadOnly = False
                    DefaultEditMask = '!99/99/99;1;_'
                    AutoApplyEditMask = True
                    UpdType = utYes
                  end
                  object CkVctoI: TCheckBox
                    Left = 4
                    Top = 54
                    Width = 73
                    Height = 17
                    Caption = 'Vcto inicial:'
                    TabOrder = 4
                    OnClick = CkVctoIClick
                  end
                  object TPVctoI: TdmkEditDateTimePicker
                    Left = 80
                    Top = 52
                    Width = 112
                    Height = 21
                    Date = 39328.122587314800000000
                    Time = 39328.122587314800000000
                    TabOrder = 5
                    OnChange = TPDataIChange
                    ReadOnly = False
                    DefaultEditMask = '!99/99/99;1;_'
                    AutoApplyEditMask = True
                    UpdType = utYes
                  end
                  object CkVctoF: TCheckBox
                    Left = 4
                    Top = 78
                    Width = 77
                    Height = 17
                    Caption = 'Vcto final:'
                    TabOrder = 6
                    OnClick = CkVctoFClick
                  end
                  object TPVctoF: TdmkEditDateTimePicker
                    Left = 80
                    Top = 76
                    Width = 112
                    Height = 21
                    Date = 39328.122587314800000000
                    Time = 39328.122587314800000000
                    TabOrder = 7
                    OnChange = TPDataFChange
                    ReadOnly = False
                    DefaultEditMask = '!99/99/99;1;_'
                    AutoApplyEditMask = True
                    UpdType = utYes
                  end
                  object EdCliIntCart: TdmkEditCB
                    Left = 272
                    Top = 4
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 8
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    OnChange = EdCliIntCartChange
                    DBLookupComboBox = CBCliIntCart
                    IgnoraDBLookupComboBox = False
                  end
                  object CBCliIntCart: TdmkDBLookupComboBox
                    Left = 328
                    Top = 4
                    Width = 248
                    Height = 21
                    KeyField = 'Filial'
                    ListField = 'NO_ENT'
                    ListSource = DsDonosCart
                    TabOrder = 9
                    dmkEditCB = EdCliIntCart
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                  object EdCliIntLancto: TdmkEditCB
                    Left = 272
                    Top = 28
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 10
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    OnChange = EdCliIntLanctoChange
                    DBLookupComboBox = CBCliIntLancto
                    IgnoraDBLookupComboBox = False
                  end
                  object CBCliIntLancto: TdmkDBLookupComboBox
                    Left = 328
                    Top = 28
                    Width = 248
                    Height = 21
                    KeyField = 'Filial'
                    ListField = 'NO_ENT'
                    ListSource = DsDonosLcto
                    TabOrder = 11
                    dmkEditCB = EdCliIntLancto
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                  object EdCliente: TdmkEditCB
                    Left = 272
                    Top = 52
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 12
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    OnChange = EdClienteChange
                    DBLookupComboBox = CBCliente
                    IgnoraDBLookupComboBox = False
                  end
                  object CBCliente: TdmkDBLookupComboBox
                    Left = 328
                    Top = 52
                    Width = 248
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'NO_ENT'
                    ListSource = DsClientes
                    TabOrder = 13
                    dmkEditCB = EdCliente
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                  object EdFornecedor: TdmkEditCB
                    Left = 272
                    Top = 76
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 14
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    OnChange = EdFornecedorChange
                    DBLookupComboBox = CBFornecedor
                    IgnoraDBLookupComboBox = False
                  end
                  object CBFornecedor: TdmkDBLookupComboBox
                    Left = 328
                    Top = 76
                    Width = 248
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'NO_ENT'
                    ListSource = DsFornecedores
                    TabOrder = 15
                    dmkEditCB = EdFornecedor
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                  object EdCarteira: TdmkEditCB
                    Left = 656
                    Top = 4
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 16
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    OnChange = EdCarteiraChange
                    DBLookupComboBox = CBCarteira
                    IgnoraDBLookupComboBox = False
                  end
                  object CBCarteira: TdmkDBLookupComboBox
                    Left = 712
                    Top = 4
                    Width = 276
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'Nome'
                    ListSource = DsCarteiras
                    TabOrder = 17
                    dmkEditCB = EdCarteira
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                  object EdConta: TdmkEditCB
                    Left = 656
                    Top = 28
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 18
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    OnChange = EdContaChange
                    DBLookupComboBox = CBConta
                    IgnoraDBLookupComboBox = False
                  end
                  object CBConta: TdmkDBLookupComboBox
                    Left = 712
                    Top = 28
                    Width = 276
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'Nome'
                    ListSource = DsContas
                    TabOrder = 19
                    dmkEditCB = EdConta
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                  object GroupBox1: TGroupBox
                    Left = 584
                    Top = 52
                    Width = 305
                    Height = 45
                    Caption = ' M'#234's de compet'#234'ncia: '
                    TabOrder = 20
                    object EdMezIni: TdmkEdit
                      Left = 88
                      Top = 16
                      Width = 54
                      Height = 21
                      Alignment = taCenter
                      TabOrder = 1
                      FormatType = dmktfMesAno
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfLong
                      HoraFormat = dmkhfShort
                      QryCampo = 'Mez'
                      UpdType = utYes
                      Obrigatorio = True
                      PermiteNulo = False
                      ValueVariant = Null
                      OnChange = CkDataIClick
                    end
                    object CkMezIni: TCheckBox
                      Left = 12
                      Top = 18
                      Width = 73
                      Height = 17
                      Caption = 'M'#234's inicial:'
                      TabOrder = 0
                      OnClick = CkDataIClick
                    end
                    object EdMezFim: TdmkEdit
                      Left = 236
                      Top = 16
                      Width = 54
                      Height = 21
                      Alignment = taCenter
                      TabOrder = 3
                      FormatType = dmktfMesAno
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfLong
                      HoraFormat = dmkhfShort
                      QryCampo = 'Mez'
                      UpdType = utYes
                      Obrigatorio = True
                      PermiteNulo = False
                      ValueVariant = Null
                      OnChange = CkDataIClick
                    end
                    object CkMezFim: TCheckBox
                      Left = 168
                      Top = 18
                      Width = 65
                      Height = 17
                      Caption = 'M'#234's final:'
                      TabOrder = 2
                      OnClick = CkDataIClick
                    end
                  end
                  object BitBtn1: TBitBtn
                    Left = 896
                    Top = 56
                    Width = 90
                    Height = 40
                    Caption = '&Reabre'
                    TabOrder = 21
                    OnClick = BitBtn1Click
                  end
                  object RGValores: TRadioGroup
                    Left = 584
                    Top = 97
                    Width = 175
                    Height = 40
                    Caption = ' Valores: '
                    Columns = 3
                    ItemIndex = 2
                    Items.Strings = (
                      'Cr'#233'dito'
                      'D'#233'bito'
                      'Ambos')
                    TabOrder = 22
                    OnClick = RGValoresClick
                  end
                  object CBUH: TDBLookupComboBox
                    Left = 791
                    Top = 108
                    Width = 65
                    Height = 21
                    KeyField = 'Conta'
                    ListField = 'Unidade'
                    TabOrder = 23
                    OnClick = CBUHClick
                  end
                end
              end
            end
          end
          object Panel8: TPanel
            Left = 0
            Top = 450
            Width = 998
            Height = 27
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            object Label6: TLabel
              Left = 4
              Top = 7
              Width = 39
              Height = 13
              Caption = 'Carteira:'
            end
            object EdCodigo: TDBEdit
              Left = 48
              Top = 3
              Width = 45
              Height = 21
              TabStop = False
              DataField = 'Carteira'
              MaxLength = 1
              ReadOnly = True
              TabOrder = 0
            end
            object EdNome: TDBEdit
              Left = 96
              Top = 3
              Width = 400
              Height = 21
              DataField = 'NO_Carteira'
              TabOrder = 1
            end
          end
          object StatusBar: TStatusBar
            Left = 0
            Top = 431
            Width = 998
            Height = 19
            Panels = <>
          end
          object dmkDBGLct: TdmkDBGrid
            Left = 0
            Top = 233
            Width = 998
            Height = 161
            Align = alClient
            Columns = <
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'Data'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FatNum'
                Title.Caption = 'Bloqueto'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FatID'
                Title.Caption = 'Tipo'
                Width = 29
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ID_Pgto'
                Title.Caption = 'ID Pagto'
                Width = 50
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SERIE_CHEQUE'
                Title.Caption = 'S'#233'rie/Docum.'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Duplicata'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descricao'
                Title.Caption = 'Descri'#231#227'o'
                Width = 164
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Debito'
                Title.Caption = 'D'#233'bito'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Credito'
                Title.Caption = 'Cr'#233'dito'
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'Vencimento'
                Title.Caption = 'Vencim.'
                Width = 59
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'COMPENSADO_TXT'
                Title.Caption = 'Compen.'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_ENDOSSADO'
                Title.Caption = 'Endosso'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MENSAL'
                Title.Caption = 'M'#234's'
                Width = 38
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMESIT'
                Title.Caption = 'Situa'#231#227'o'
                Width = 70
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'Lan'#231'to'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Sub'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SALDO'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NotaFiscal'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMERELACIONADO'
                Title.Caption = 'Cliente / Fornecedor'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtde'
                Title.Caption = 'Quantidade'
                Width = 62
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEFORNECEI'
                Title.Caption = 'Fonte pagadora'
                Width = 180
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MultaVal'
                Title.Caption = 'Multa paga'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MoraVal'
                Title.Caption = 'Juros pagos'
                Width = 56
                Visible = True
              end>
            Color = clWindow
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 3
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Visible = False
            OnCellClick = dmkDBGLctCellClick
            OnDrawColumnCell = dmkDBGLctDrawColumnCell
            OnKeyDown = dmkDBGLctKeyDown
            FieldsCalcToOrder.Strings = (
              'NOMESIT=Sit,Vencimento'
              'SERIE_CHEQUE=SerieCH,Documento'
              'COMPENSADO_TXT=Compensado'
              'MENSAL=Mez')
            Columns = <
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'Data'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FatNum'
                Title.Caption = 'Bloqueto'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FatID'
                Title.Caption = 'Tipo'
                Width = 29
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ID_Pgto'
                Title.Caption = 'ID Pagto'
                Width = 50
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SERIE_CHEQUE'
                Title.Caption = 'S'#233'rie/Docum.'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Duplicata'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descricao'
                Title.Caption = 'Descri'#231#227'o'
                Width = 164
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Debito'
                Title.Caption = 'D'#233'bito'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Credito'
                Title.Caption = 'Cr'#233'dito'
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'Vencimento'
                Title.Caption = 'Vencim.'
                Width = 59
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'COMPENSADO_TXT'
                Title.Caption = 'Compen.'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_ENDOSSADO'
                Title.Caption = 'Endosso'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MENSAL'
                Title.Caption = 'M'#234's'
                Width = 38
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMESIT'
                Title.Caption = 'Situa'#231#227'o'
                Width = 70
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'Lan'#231'to'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Sub'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SALDO'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NotaFiscal'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMERELACIONADO'
                Title.Caption = 'Cliente / Fornecedor'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtde'
                Title.Caption = 'Quantidade'
                Width = 62
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEFORNECEI'
                Title.Caption = 'Fonte pagadora'
                Width = 180
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MultaVal'
                Title.Caption = 'Multa paga'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MoraVal'
                Title.Caption = 'Juros pagos'
                Width = 56
                Visible = True
              end>
          end
          object ProgressBar1: TProgressBar
            Left = 0
            Top = 414
            Width = 998
            Height = 17
            Align = alBottom
            TabOrder = 4
          end
          object Panel2: TPanel
            Left = 0
            Top = 394
            Width = 998
            Height = 20
            Align = alBottom
            TabOrder = 5
            object Label2: TLabel
              Left = 8
              Top = 2
              Width = 31
              Height = 13
              Caption = 'Conta:'
            end
            object DBText1: TDBText
              Left = 40
              Top = 2
              Width = 341
              Height = 13
              DataField = 'NOMECONTA'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clHotLight
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label3: TLabel
              Left = 384
              Top = 2
              Width = 52
              Height = 13
              Caption = 'Sub-grupo:'
            end
            object DBText2: TDBText
              Left = 436
              Top = 2
              Width = 185
              Height = 13
              DataField = 'NOMESUBGRUPO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clHotLight
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label4: TLabel
              Left = 624
              Top = 2
              Width = 32
              Height = 13
              Caption = 'Grupo:'
            end
            object DBText3: TDBText
              Left = 660
              Top = 2
              Width = 141
              Height = 13
              DataField = 'NOMEGRUPO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clHotLight
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label7: TLabel
              Left = 804
              Top = 2
              Width = 45
              Height = 13
              Caption = 'Conjunto:'
            end
            object DBText4: TDBText
              Left = 852
              Top = 2
              Width = 141
              Height = 13
              DataField = 'NOMECONJUNTO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clHotLight
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
          end
        end
      end
      object TabSheet1: TTabSheet
        Caption = ' SQL '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Memo1: TMemo
          Left = 0
          Top = 0
          Width = 998
          Height = 477
          Align = alClient
          TabOrder = 0
        end
      end
    end
  end
  object Timer2: TTimer
    Interval = 100
    OnTimer = Timer2Timer
    Left = 96
    Top = 8
  end
  object PMAtzCarts: TPopupMenu
    Left = 152
    Top = 8
    object Acarteiraselecionada1: TMenuItem
      Caption = 'A carteira &selecionada'
      OnClick = Acarteiraselecionada1Click
    end
    object Todascarteiras1: TMenuItem
      Caption = '&Todas carteiras'
      OnClick = Todascarteiras1Click
    end
  end
  object PMMenu: TPopupMenu
    OnPopup = PMMenuPopup
    Left = 124
    Top = 8
    object Transferir1: TMenuItem
      Caption = '&Transfer'#234'ncia entre carteiras'
      object Nova1: TMenuItem
        Caption = '&Nova'
        OnClick = Nova1Click
      end
      object Editar1: TMenuItem
        Caption = '&Editar'
        Enabled = False
        OnClick = Editar1Click
      end
      object Excluir1: TMenuItem
        Caption = 'E&xcluir'
        Enabled = False
        OnClick = Excluir1Click
      end
      object Localizar1: TMenuItem
        Caption = '&Localizar'
        OnClick = Localizar1Click
      end
    end
    object ransfernciaentrecontas1: TMenuItem
      Caption = 'Transfer'#234'ncia entre &Contas'
      object Inclui1: TMenuItem
        Caption = '&Nova'
      end
    end
    object Quitar1: TMenuItem
      Caption = '&Quita'#231#227'o'
      object Compensar1: TMenuItem
        Caption = '&Compensar na conta corrente (Banco)'
        Enabled = False
        OnClick = Compensar1Click
      end
      object Reverter1: TMenuItem
        Caption = 'D&esfazer compensa'#231#227'o'
        Enabled = False
        OnClick = Reverter1Click
      end
      object N10: TMenuItem
        Caption = '-'
      end
      object Pagar1: TMenuItem
        Caption = '&Pagar / Rolar com emiss'#227'o  (cheque, etc)'
        Enabled = False
        OnClick = Pagar1Click
      end
      object N11: TMenuItem
        Caption = '-'
      end
      object PagarAVista1: TMenuItem
        Caption = 'Pagar '#224' vista para um caixa'
        OnClick = PagarAVista1Click
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object Localizarpagamentosdestaemisso1: TMenuItem
        Caption = 'Localizar pagamentos desta emiss'#227'o'
        OnClick = Localizarpagamentosdestaemisso1Click
      end
    end
    object Lanamento1: TMenuItem
      Caption = '&Lan'#231'amento'
      object Localizar2: TMenuItem
        Caption = '&Localizar'
        OnClick = Localizar2Click
      end
      object Copiar1: TMenuItem
        Caption = '&Copiar'
        OnClick = Copiar1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Exclusoincondicional1: TMenuItem
        Caption = '&Exclus'#227'o incondicional'
        OnClick = Exclusoincondicional1Click
      end
    end
    object Mudacarteiradelanamentosselecionados1: TMenuItem
      Caption = '&Muda lan'#231'amentos selecionados'
      Enabled = False
      object Carteiras1: TMenuItem
        Caption = '&Carteira'
        OnClick = Carteiras1Click
      end
      object Data1: TMenuItem
        Caption = '&Data'
        OnClick = Data1Click
      end
      object Compensao1: TMenuItem
        Caption = '&Compensa'#231#227'o'
        OnClick = Compensao1Click
      end
      object Msdecompetncia1: TMenuItem
        Caption = '&M'#234's de compet'#234'ncia'
        OnClick = Msdecompetncia1Click
      end
      object TextoParcial1: TMenuItem
        Caption = 'Texto (Parcial)'
        OnClick = TextoParcial1Click
      end
    end
    object Recibo1: TMenuItem
      Caption = '&Recibo'
      OnClick = Recibo1Click
    end
    object RetornoCNAB1: TMenuItem
      Caption = 'Retorno CNA&B'
      OnClick = RetornoCNAB1Click
    end
    object Colocarmsdecompetnciaondenotem1: TMenuItem
      Caption = 'Colocar m'#234's de compet'#234'ncia onde n'#227'o tem'
      object Datalancto1: TMenuItem
        Caption = '&Data lancto'
        OnClick = Datalancto1Click
      end
      object Vencimento1: TMenuItem
        Caption = '&Vencimento'
        OnClick = Vencimento1Click
      end
    end
  end
  object PMQuita: TPopupMenu
    OnPopup = PMQuitaPopup
    Left = 12
    Top = 8
    object Compensar2: TMenuItem
      Caption = '&Compensar na conta corrente (Banco)'
      OnClick = Compensar2Click
    end
    object Reverter2: TMenuItem
      Caption = 'D&esfazer compensa'#231#227'o'
      OnClick = Reverter2Click
    end
    object MenuItem3: TMenuItem
      Caption = '-'
    end
    object Pagar2: TMenuItem
      Caption = '&Pagar / Rolar com emiss'#227'o  (cheque, etc)'
      OnClick = Pagar2Click
    end
    object MenuItem5: TMenuItem
      Caption = '-'
    end
    object PagarAVista2: TMenuItem
      Caption = 'Pagar em carteira para um caixa'
      OnClick = PagarAVista2Click
    end
  end
  object PMRefresh: TPopupMenu
    Left = 68
    Top = 8
    object Carteira1: TMenuItem
      Caption = 'Carteira (Caixa, banco, emiss'#227'o)'
      object Atual5: TMenuItem
        Caption = 'Carteira &Atual'
        OnClick = Atual5Click
      end
      object Todas1: TMenuItem
        Caption = '&Todas carteiras'
        OnClick = Todas1Click
      end
    end
    object ContaPlanodecontas1: TMenuItem
      Caption = 'C&onta (Plano de contas)'
    end
  end
  object PMSaldoAqui: TPopupMenu
    Left = 40
    Top = 8
    object Calcula1: TMenuItem
      Caption = '&Calcula'
      OnClick = Calcula1Click
    end
    object Limpa1: TMenuItem
      Caption = '&Limpa'
      OnClick = Limpa1Click
    end
    object Diferena1: TMenuItem
      Caption = '&Diferen'#231'a'
      OnClick = Diferena1Click
    end
  end
  object PMEspecificos: TPopupMenu
    Left = 180
    Top = 8
  end
  object PMTrfCta: TPopupMenu
    OnPopup = PMTrfCtaPopup
    Left = 572
    Top = 164
    object Novatransferncia1: TMenuItem
      Caption = '&Nova transfer'#234'ncia'
      OnClick = Novatransferncia1Click
    end
    object Alteratransferncia1: TMenuItem
      Caption = '&Altera transfer'#234'ncia'
      OnClick = Alteratransferncia1Click
    end
    object Excluitransferncia1: TMenuItem
      Caption = '&Exclui transfer'#234'ncia'
      OnClick = Excluitransferncia1Click
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 208
    Top = 8
  end
  object QrDonosCart: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT car.ForneceI , ent.Filial,'
      'IF(ent.Tipo=0, ent.RazaoSocial,ent.Nome) NO_ENT'
      'FROM carteiras car'
      'LEFT JOIN entidades ent ON ent.Codigo=car.ForneceI')
    Left = 620
    Top = 8
    object QrDonosCartForneceI: TIntegerField
      FieldName = 'ForneceI'
      Required = True
    end
    object QrDonosCartNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrDonosCartFilial: TIntegerField
      FieldName = 'Filial'
    end
  end
  object DsDonosCart: TDataSource
    DataSet = QrDonosCart
    Left = 648
    Top = 8
  end
  object DsDonosLcto: TDataSource
    DataSet = QrDonosLcto
    Left = 704
    Top = 8
  end
  object QrDonosLcto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Filial, ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial,ent.Nome) NO_ENT'
      'FROM entidades ent'
      'WHERE CliInt<>0'
      'ORDER BY NO_ENT')
    Left = 676
    Top = 8
    object QrDonosLctoFilial: TIntegerField
      FieldName = 'Filial'
      Required = True
    end
    object QrDonosLctoNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
    object QrDonosLctoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial,ent.Nome) NO_ENT'
      'FROM entidades ent'
      'ORDER BY NO_ENT')
    Left = 732
    Top = 8
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrClientesNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 760
    Top = 8
  end
  object QrFornecedores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial,ent.Nome) NO_ENT'
      'FROM entidades ent'
      'ORDER BY NO_ENT')
    Left = 788
    Top = 8
    object QrFornecedoresCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFornecedoresNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
  end
  object DsFornecedores: TDataSource
    DataSet = QrFornecedores
    Left = 816
    Top = 8
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, ForneceI'
      'FROM carteiras'
      'ORDER BY Nome')
    Left = 844
    Top = 8
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 872
    Top = 8
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'ORDER BY Nome')
    Left = 900
    Top = 8
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 928
    Top = 8
  end
  object QrCarteira: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM carteiras '
      'WHERE Codigo=:P0')
    Left = 148
    Top = 492
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCarteiraCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteiraNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsCarteira: TDataSource
    DataSet = QrCarteira
    Left = 176
    Top = 492
  end
  object PMRelat: TPopupMenu
    Left = 660
    Top = 152
    object Pesquisas1: TMenuItem
      Caption = '&Pesquisas'
      OnClick = Pesquisas1Click
    end
    object Result1: TMenuItem
      Caption = '&Resultados'
      object Mensais1: TMenuItem
        Caption = '&Mensais'
        OnClick = Mensais1Click
      end
      object porPerodo1: TMenuItem
        Caption = 'por &Per'#237'odo'
        object Emitidos1: TMenuItem
          Caption = 'Emitidos'
          OnClick = Emitidos1Click
        end
        object Quitados1: TMenuItem
          Caption = 'Quitados'
          OnClick = Quitados1Click
        end
      end
    end
    object Saldos1: TMenuItem
      Caption = '&Saldos'
      OnClick = Saldos1Click
    end
    object SaldosEm1: TMenuItem
      Caption = 'Saldos &Em...'
      OnClick = SaldosEm1Click
    end
    object Descontodeduplicatas1: TMenuItem
      Caption = '&Desconto de duplicatas'
      OnClick = Descontodeduplicatas1Click
    end
    object Extrato1: TMenuItem
      Caption = '&Extrato'
      object PagarReceber1: TMenuItem
        Caption = '&Pagar/Receber'
        OnClick = PagarReceber1Click
      end
      object Movimento1: TMenuItem
        Caption = '&Movimento'
        OnClick = Movimento1Click
      end
    end
  end
end
