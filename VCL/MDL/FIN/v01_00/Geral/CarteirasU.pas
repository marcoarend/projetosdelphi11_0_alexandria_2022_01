unit CarteirasU;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Variants, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkGeral,
  UnDmkEnums;

type
  TFmCarteirasU = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdUsuario: TdmkEditCB;
    CBUsuario: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrSenhas: TmySQLQuery;
    DsSenhas: TDataSource;
    QrSenhasLogin: TWideStringField;
    QrSenhasNumero: TIntegerField;
    CkContinuar: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenSenhas();
  public
    { Public declarations }
  end;

  var
  FmCarteirasU: TFmCarteirasU;

implementation

uses UnMyObjects, Module, Carteiras, UMySQLModule;

{$R *.DFM}

procedure TFmCarteirasU.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Usuario: Integer;
begin
  Usuario := Geral.IMV(EdUsuario.Text);
  if Usuario = 0 then
  begin
    Application.MessageBox('Informe o usu�rio!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdUsuario.SetFocus;
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT *');
  Dmod.QrAux.SQL.Add('FROM carteirasu');
  Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
  Dmod.QrAux.SQL.Add('AND Usuario=:P1');
  Dmod.QrAux.Open;
  if Dmod.QrAux.RecordCount > 0 then
  begin
    Application.MessageBox(PChar('O usu�rio "' + CBUsuario.Text + '" j� est� ' +
    'cadastrado para a carteira "' + FmCarteiras.QrCarteirasNome.Value +
    '".'), 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  Codigo   := FmCarteiras.QrCarteirasCodigo.Value;
  Controle := UMyMod.BuscaEmLivreY_Def('carteirasu', 'Controle', stIns, 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'carteirasu', False, [
    'Codigo', 'Usuario'], ['Controle'], [
    Codigo, Usuario], [Controle], True) then
  begin
    FmCarteiras.ReopenCarteirasU(Controle);
    if CkContinuar.Checked then
    begin
      EdUsuario.ValueVariant := 0;
      CBUsuario.KeyValue     := Null;
      //
      EdUsuario.SetFocus;
      ReopenSenhas();
    end else Close;
  end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCarteirasU.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCarteirasU.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCarteirasU.FormCreate(Sender: TObject);
begin
  ReopenSenhas();
end;

procedure TFmCarteirasU.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCarteirasU.ReopenSenhas();
begin
  QrSenhas.Close;
  QrSenhas.Params[0].AsInteger := FmCarteiras.QrCarteirasCodigo.Value;
  QrSenhas.Open;
end;

end.

