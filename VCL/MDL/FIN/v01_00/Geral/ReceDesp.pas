unit ReceDesp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkEdit, dmkEditCB, Db,
  mySQLDbTables, DBCtrls, dmkDBLookupComboBox, ComCtrls, dmkEditDateTimePicker,
  frxClass, frxDBSet, Variants, dmkCheckGroup, Grids, DBGrids, dmkDBGrid, Menus,
  dmkGeral, MyDBCheck, UmySQLModule, DmkDAC_PF;

type
  TFmReceDesp = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    QrEmpresas: TmySQLQuery;
    DsEmpresas: TDataSource;
    frxReceDesp: TfrxReport;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DataSource1: TDataSource;
    QrDC: TmySQLQuery;
    frxDC_Mes: TfrxReport;
    frxDsDC: TfrxDBDataset;
    QrDCNOMEMES: TWideStringField;
    QrDCGenero: TIntegerField;
    QrDCMez: TIntegerField;
    QrDCDepto: TIntegerField;
    QrDCValor: TFloatField;
    QrDCNOMECON: TWideStringField;
    QrDCUNIDADE: TWideStringField;
    QrDCControle: TIntegerField;
    frxDC_Uni: TfrxReport;
    QrUHs: TmySQLQuery;
    QrUHsUnidade: TWideStringField;
    QrUHsConta: TIntegerField;
    DsUHs: TDataSource;
    TabSheet3: TTabSheet;
    QrRet: TmySQLQuery;
    DsRet: TDataSource;
    Panel4: TPanel;
    EdEmpresa: TdmkEditCB;
    Label1: TLabel;
    CBEmpresa: TdmkDBLookupComboBox;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    TPDataIni1: TdmkEditDateTimePicker;
    TPDataFim1: TdmkEditDateTimePicker;
    GroupBox3: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    TPDataIni2: TdmkEditDateTimePicker;
    TPDataFim2: TdmkEditDateTimePicker;
    Panel5: TPanel;
    GroupBox4: TGroupBox;
    Label7: TLabel;
    EdValPagMin: TdmkEdit;
    EdValPagMax: TdmkEdit;
    Label8: TLabel;
    GroupBox5: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    EdValTitMin: TdmkEdit;
    EdValTitMax: TdmkEdit;
    DBGrid1: TdmkDBGrid;
    QrRetCodigo: TIntegerField;
    QrRetBanco: TIntegerField;
    QrRetID_Link: TIntegerField;
    QrRetNossoNum: TWideStringField;
    QrRetSeuNum: TIntegerField;
    QrRetIDNum: TIntegerField;
    QrRetOcorrCodi: TWideStringField;
    QrRetOcorrData: TDateField;
    QrRetValTitul: TFloatField;
    QrRetValAbati: TFloatField;
    QrRetValDesco: TFloatField;
    QrRetValPago: TFloatField;
    QrRetValJuros: TFloatField;
    QrRetValMulta: TFloatField;
    QrRetValJuMul: TFloatField;
    QrRetMotivo1: TWideStringField;
    QrRetMotivo2: TWideStringField;
    QrRetMotivo3: TWideStringField;
    QrRetMotivo4: TWideStringField;
    QrRetMotivo5: TWideStringField;
    QrRetQuitaData: TDateField;
    QrRetDiretorio: TIntegerField;
    QrRetArquivo: TWideStringField;
    QrRetItemArq: TIntegerField;
    QrRetStep: TSmallintField;
    QrRetEntidade: TIntegerField;
    QrRetCarteira: TIntegerField;
    QrRetDevJuros: TFloatField;
    QrRetDevMulta: TFloatField;
    QrRetValOutro: TFloatField;
    QrRetValTarif: TFloatField;
    QrRetLk: TIntegerField;
    QrRetDataCad: TDateField;
    QrRetDataAlt: TDateField;
    QrRetUserCad: TIntegerField;
    QrRetUserAlt: TIntegerField;
    QrRetDtaTarif: TDateField;
    QrRetAlterWeb: TSmallintField;
    QrRetAtivo: TSmallintField;
    QrRetTamReg: TIntegerField;
    GroupBox6: TGroupBox;
    Label9: TLabel;
    EdBloqIni: TdmkEdit;
    EdBloqFim: TdmkEdit;
    Label12: TLabel;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    Panel6: TPanel;
    DBGDebitos: TdmkDBGrid;
    DBGCreditos: TdmkDBGrid;
    CkNaoAgruparNada: TCheckBox;
    Panel7: TPanel;
    BtCreditos: TBitBtn;
    PMCreditos: TPopupMenu;
    Alteracontadoslanamentosselecionados_Cred: TMenuItem;
    Panel8: TPanel;
    BtDebitos: TBitBtn;
    PMDebitos: TPopupMenu;
    Alteracontadoslanamentosselecionados_Debi: TMenuItem;
    Alteradescriodolanamentoselecionado1: TMenuItem;
    Alteralanamentoselecionado1: TMenuItem;
    QrLct: TmySQLQuery;
    QrLctData: TDateField;
    QrLctTipo: TSmallintField;
    QrLctCarteira: TIntegerField;
    QrLctControle: TIntegerField;
    QrLctSub: TSmallintField;
    QrLctAutorizacao: TIntegerField;
    QrLctGenero: TIntegerField;
    QrLctQtde: TFloatField;
    QrLctDescricao: TWideStringField;
    QrLctNotaFiscal: TIntegerField;
    QrLctDebito: TFloatField;
    QrLctCredito: TFloatField;
    QrLctCompensado: TDateField;
    QrLctDocumento: TFloatField;
    QrLctSit: TIntegerField;
    QrLctVencimento: TDateField;
    QrLctFatID: TIntegerField;
    QrLctFatID_Sub: TIntegerField;
    QrLctFatNum: TFloatField;
    QrLctFatParcela: TIntegerField;
    QrLctID_Pgto: TIntegerField;
    QrLctID_Sub: TSmallintField;
    QrLctFatura: TWideStringField;
    QrLctEmitente: TWideStringField;
    QrLctBanco: TIntegerField;
    QrLctContaCorrente: TWideStringField;
    QrLctCNPJCPF: TWideStringField;
    QrLctLocal: TIntegerField;
    QrLctCartao: TIntegerField;
    QrLctLinha: TIntegerField;
    QrLctOperCount: TIntegerField;
    QrLctLancto: TIntegerField;
    QrLctPago: TFloatField;
    QrLctMez: TIntegerField;
    QrLctFornecedor: TIntegerField;
    QrLctCliente: TIntegerField;
    QrLctCliInt: TIntegerField;
    QrLctForneceI: TIntegerField;
    QrLctMoraDia: TFloatField;
    QrLctMulta: TFloatField;
    QrLctProtesto: TDateField;
    QrLctDataDoc: TDateField;
    QrLctCtrlIni: TIntegerField;
    QrLctNivel: TIntegerField;
    QrLctVendedor: TIntegerField;
    QrLctAccount: TIntegerField;
    QrLctICMS_P: TFloatField;
    QrLctICMS_V: TFloatField;
    QrLctDuplicata: TWideStringField;
    QrLctDepto: TIntegerField;
    QrLctDescoPor: TIntegerField;
    QrLctDescoVal: TFloatField;
    QrLctDescoControle: TIntegerField;
    QrLctUnidade: TIntegerField;
    QrLctNFVal: TFloatField;
    QrLctAntigo: TWideStringField;
    QrLctExcelGru: TIntegerField;
    QrLctLk: TIntegerField;
    QrLctDataCad: TDateField;
    QrLctDataAlt: TDateField;
    QrLctUserCad: TIntegerField;
    QrLctUserAlt: TIntegerField;
    QrLctSerieCH: TWideStringField;
    QrLctDoc2: TWideStringField;
    QrLctMoraVal: TFloatField;
    QrLctMultaVal: TFloatField;
    QrLctCNAB_Sit: TSmallintField;
    QrLctTipoCH: TSmallintField;
    QrLctAlterWeb: TSmallintField;
    QrLctReparcel: TIntegerField;
    QrLctID_Quit: TIntegerField;
    QrLctAtrelado: TIntegerField;
    QrLctAtivo: TSmallintField;
    QrLctPagMul: TFloatField;
    QrLctPagJur: TFloatField;
    QrLctSubPgto1: TIntegerField;
    QrLctMultiPgto: TIntegerField;
    QrLctProtocolo: TIntegerField;
    QrLctCtrlQuitPg: TIntegerField;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasTipo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasNome2: TWideStringField;
    QrCarteirasContato1: TWideStringField;
    QrCarteirasInicial: TFloatField;
    QrCarteirasBanco: TIntegerField;
    QrCarteirasID: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasID_Fat: TWideStringField;
    QrCarteirasSaldo: TFloatField;
    QrCarteirasEmCaixa: TFloatField;
    QrCarteirasFechamento: TIntegerField;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasPagRec: TIntegerField;
    QrCarteirasDiaMesVence: TSmallintField;
    QrCarteirasExigeNumCheque: TSmallintField;
    QrCarteirasForneceI: TIntegerField;
    QrCarteirasTipoDoc: TSmallintField;
    QrCarteirasBanco1: TIntegerField;
    QrCarteirasAgencia1: TIntegerField;
    QrCarteirasConta1: TWideStringField;
    QrCarteirasCheque1: TIntegerField;
    QrCarteirasAntigo: TWideStringField;
    QrCarteirasContab: TWideStringField;
    QrCarteirasLk: TIntegerField;
    QrCarteirasDataCad: TDateField;
    QrCarteirasDataAlt: TDateField;
    QrCarteirasUserCad: TIntegerField;
    QrCarteirasUserAlt: TIntegerField;
    QrCarteirasOrdem: TIntegerField;
    QrCarteirasForneceN: TSmallintField;
    QrCarteirasFuturoC: TFloatField;
    QrCarteirasFuturoD: TFloatField;
    QrCarteirasFuturoS: TFloatField;
    QrCarteirasExclusivo: TSmallintField;
    QrCarteirasAlterWeb: TSmallintField;
    QrCarteirasAtivo: TSmallintField;
    QrCarteirasRecebeBloq: TSmallintField;
    QrCarteirasEntiDent: TIntegerField;
    Alteralanamentoselecionado2: TMenuItem;
    Panel9: TPanel;
    Label4: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    Label41: TLabel;
    CBUH: TDBLookupComboBox;
    CGValores: TdmkCheckGroup;
    RGOrdem: TRadioGroup;
    GroupBox2: TGroupBox;
    CkAcordos: TCheckBox;
    CkPeriodos: TCheckBox;
    CkTextos: TCheckBox;
    QrEmpresasCodCliInt: TIntegerField;
    QrEmpresasCodEnti: TIntegerField;
    QrEmpresasNOMECLI: TWideStringField;
    BitBtn1: TBitBtn;
    EdCarteira: TdmkEditCB;
    Label13: TLabel;
    CBCarteira: TdmkDBLookupComboBox;
    QrCarts: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    StringField2: TWideStringField;
    IntegerField2: TIntegerField;
    SmallintField1: TSmallintField;
    IntegerField3: TIntegerField;
    SmallintField2: TSmallintField;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    QrCarteirasUsaTalao: TSmallintField;
    DsCarts: TDataSource;
    QrLctAgencia: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxReceDespGetValue(const VarName: String;
      var Value: Variant);
    procedure QrDCCalcFields(DataSet: TDataSet);
    procedure EdEmpresaChange(Sender: TObject);
    procedure CBUHKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdValPagMinChange(Sender: TObject);
    procedure EdValTitMinChange(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure CkNaoAgruparNadaClick(Sender: TObject);
    procedure Alteracontadoslanamentosselecionados_CredClick(Sender: TObject);
    procedure BtCreditosClick(Sender: TObject);
    procedure Alteracontadoslanamentosselecionados_DebiClick(Sender: TObject);
    procedure BtDebitosClick(Sender: TObject);
    procedure Alteradescriodolanamentoselecionado1Click(Sender: TObject);
    procedure Alteralanamentoselecionado1Click(Sender: TObject);
    procedure Alteralanamentoselecionado2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    F_CliInt, FGenero, FUH: Integer;
    FDataI1, FDataF1, FDataI2, FDataF2: String;
    procedure DemostrativoDeReceitasEDespesas();
    procedure HistoricoConta();
    procedure RetornosCNAB();
    procedure ReopenCarteiras();
  public
    { Public declarations }
  end;

  var
  FmReceDesp: TFmReceDesp;

implementation

uses UnMyObjects, UnInternalConsts, Module, UnFinanceiro, LctEdit, Principal, ModuleFin;

{$R *.DFM}

procedure TFmReceDesp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmReceDesp.CBUHKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_DELETE then
    CBUH.KeyValue := Null;
end;

procedure TFmReceDesp.CkNaoAgruparNadaClick(Sender: TObject);
begin
  Pagecontrol2.Visible := CkNaoAgruparNada.Checked;
(***
  DCond.QrCreditos.Close;
  DCond.QrDebitos.Close;
*)
end;

procedure TFmReceDesp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  CGValores.Value := 3;
end;

procedure TFmReceDesp.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmReceDesp.FormCreate(Sender: TObject);
begin
  QrCarteiras.Open;
  QrEmpresas.Open;
  QrContas.Open;
  TPDataIni1.Date := Date - 30;
  TPDataFim1.Date := Date;
  TPDataIni2.Date := Date - 30;
  TPDataFim2.Date := Date;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmReceDesp.Alteracontadoslanamentosselecionados_CredClick(
  Sender: TObject);
{
var
  i, Genero, Controle: Integer;
}
begin
(***
  Application.CreateForm(TFmLctMudaConta, FmLctMudaConta);
  FmLctMudaConta.ShowModal;
  Genero := FmLctMudaConta.FContaSel;
  FmLctMudaConta.Destroy;
  if Genero = DCond.QrCreditosGenero.Value then Exit;
  if Genero <> -1000 then
  begin
    Screen.Cursor := crHourGlass;
    with DBGCreditos.DataSource.DataSet do
    for i:= 0 to DBGCreditos.SelectedRows.Count-1 do
    begin
      if DCond.QrCreditosGenero.Value < 1 then
      begin
        Application.MessageBox(PChar('O lan�amento '+IntToStr(
        DCond.QrCreditosControle.Value)+' � protegido. Para editar '+
        'este item selecione seu caminho correto!'), 'Erro', MB_OK+MB_ICONERROR);
        Exit;
      end;
      if DCond.QrCreditosCartao.Value > 0 then
      begin
        Application.MessageBox(PChar('O lan�amento '+IntToStr(
        DCond.QrCreditosControle.Value)+' n�o pode ser editado pois '+
        'pertence a uma fatura!'), 'Erro', MB_OK+MB_ICONERROR);
        Exit;
      end;
      GotoBookmark(pointer(DBGCreditos.SelectedRows.Items[i]));
      {
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Genero=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa AND Sub=:Pb');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.Params[00].AsInteger := Genero;
      //
      Dmod.QrUpd.Params[01].AsInteger := DCond.QrCreditosControle.Value;
      Dmod.QrUpd.Params[02].AsInteger := DCond.QrCreditosSub.Value;
      Dmod.QrUpd.ExecSQL;
      }
      UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
      'Genero'], ['Controle', 'Sub'], [Genero], [DCond.QrCreditosControle.Value,
      DCond.QrCreditosSub.Value], True, '');
    end;
    //
    Controle := DCond.QrCreditosControle.Value;
    DCond.QrCreditos.Close;
    DCond.QrCreditos.Open;
    DCond.QrCreditos.Locate('Controle', Controle, []);
    Screen.Cursor := crDefault;
  end;
*)
end;

procedure TFmReceDesp.Alteracontadoslanamentosselecionados_DebiClick(
  Sender: TObject);
{
var
  i, Genero, Controle: Integer;
}
begin
(***
  Application.CreateForm(TFmLctMudaConta, FmLctMudaConta);
  FmLctMudaConta.ShowModal;
  Genero := FmLctMudaConta.FContaSel;
  FmLctMudaConta.Destroy;
  if Genero = DCond.QrDebitosGenero.Value then Exit;
  if Genero <> -1000 then
  begin
    Screen.Cursor := crHourGlass;
    with DBGDebitos.DataSource.DataSet do
    for i:= 0 to DBGDebitos.SelectedRows.Count-1 do
    begin
      if DCond.QrDebitosGenero.Value < 1 then
      begin
        Application.MessageBox(PChar('O lan�amento '+IntToStr(
        DCond.QrDebitosControle.Value)+' � protegido. Para editar '+
        'este item selecione seu caminho correto!'), 'Erro', MB_OK+MB_ICONERROR);
        Exit;
      end;
      if DCond.QrDebitosCartao.Value > 0 then
      begin
        Application.MessageBox(PChar('O lan�amento '+IntToStr(
        DCond.QrDebitosControle.Value)+' n�o pode ser editado pois '+
        'pertence a uma fatura!'), 'Erro', MB_OK+MB_ICONERROR);
        Exit;
      end;
      GotoBookmark(pointer(DBGDebitos.SelectedRows.Items[i]));
      {
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Genero=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa AND Sub=:Pb');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.Params[00].AsInteger := Genero;
      //
      Dmod.QrUpd.Params[01].AsInteger := DCond.QrDebitosControle.Value;
      Dmod.QrUpd.Params[02].AsInteger := DCond.QrDebitosSub.Value;
      Dmod.QrUpd.ExecSQL;
      }
      UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
      'Genero'], ['Controle', 'Sub'], [Genero], [DCond.QrDebitosControle.Value,
      DCond.QrDebitosSub.Value], True, '');
    end;
    //
    Controle := DCond.QrDebitosControle.Value;
    DCond.QrDebitos.Close;
    DCond.QrDebitos.Open;
    DCond.QrDebitos.Locate('Controle', Controle, []);
    Screen.Cursor := crDefault;
  end;
*)
end;

procedure TFmReceDesp.Alteradescriodolanamentoselecionado1Click(
  Sender: TObject);
{
var
  Descricao: String;
  Controle: Integer;
}
begin
(***
  Descricao := DCond.QrDebitosDescricao.Value;
  if InputQuery('Altera��o de Lan�amento', 'Informa o novo hist�rico', Descricao) then
  begin
    Screen.Cursor := crHourGlass;
    UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
    'Descricao'], ['Controle', 'Sub'], [Descricao], [
    DCond.QrDebitosControle.Value, DCond.QrDebitosSub.Value], True, '');
    //
    Controle := DCond.QrDebitosControle.Value;
    DCond.QrDebitos.Close;
    DCond.QrDebitos.Open;
    DCond.QrDebitos.Locate('Controle', Controle, []);
    Screen.Cursor := crDefault;
  end;
*)
end;

procedure TFmReceDesp.Alteralanamentoselecionado1Click(Sender: TObject);
{
var
  Controle, EntInt: Integer;
}
begin
(***
  EntInt := FmPrincipal.FEntInt;
  try
    FmPrincipal.FEntInt := QrCondCliente.Value;
    QrLct.Close;
    QrLct.Params[0].AsInteger  := DCond.QrCreditosControle.Value;
    QrLct.Params[01].AsInteger := DCond.QrCreditosSub.Value;
    QrLct.Open;
    //
    if UFinanceiro.AlteracaoLancamento(QrCarteiras, QrLct, lfCondominio, 0,
    QrCondCliente.Value, True) then
    begin
      Controle := DCond.QrCreditosControle.Value;
      DCond.QrCreditos.Close;
      DCond.QrCreditos.Open;
      DCond.QrCreditos.Locate('Controle', Controle, []);
    end;
  finally
    FmPrincipal.FEntInt := EntInt;
  end;
*)
end;

procedure TFmReceDesp.Alteralanamentoselecionado2Click(Sender: TObject);
{
var
  Controle, EntInt: Integer;
}
begin
(***
  EntInt := FmPrincipal.FEntInt;
  try
    FmPrincipal.FEntInt := QrCondCliente.Value;
    QrLct.Close;
    QrLct.Params[0].AsInteger  := DCond.QrDebitosControle.Value;
    QrLct.Params[01].AsInteger := DCond.QrDebitosSub.Value;
    QrLct.Open;
    //
    if UFinanceiro.AlteracaoLancamento(QrCarteiras, QrLct, lfCondominio, 0,
    QrCondCliente.Value, True) then
    begin
      Controle := DCond.QrDebitosControle.Value;
      DCond.QrDebitos.Close;
      DCond.QrDebitos.Open;
      DCond.QrDebitos.Locate('Controle', Controle, []);
    end;
  finally
    FmPrincipal.FEntInt := EntInt;
  end;
*)
end;

procedure TFmReceDesp.BtDebitosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDebitos, BtDebitos);
end;

procedure TFmReceDesp.BitBtn1Click(Sender: TObject);
begin
  TPDataIni1.Date := Int(Date - 1);
  TPDataFim1.Date := TPDataIni1.Date;
end;

procedure TFmReceDesp.BtCreditosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCreditos, BtCreditos);
end;

procedure TFmReceDesp.BtOKClick(Sender: TObject);
begin
  if EdEmpresa.ValueVariant = 0 then
  begin
    Application.MessageBox('Informe a Empresa!',
    'Aviso', MB_OK+MB_ICONWARNING);
    EdEmpresa.SetFocus;
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    F_CliInt := QrEmpresasCodEnti.Value;
    FGenero := Geral.IMV(EdConta.Text);
    FDataI1 := Geral.FDT(TPDataIni1.Date, 1);
    FDataF1 := Geral.FDT(TPDataFim1.Date, 1);
    FDataI2 := Geral.FDT(TPDataIni2.Date, 1);
    FDataF2 := Geral.FDT(TPDataFim2.Date, 1);
    if CBUH.KeyValue <> Null then
      FUH := CBUH.KeyValue
    else FUH := 0;
    //
    case PageControl1.ActivePageIndex of
      0: DemostrativoDeReceitasEDespesas();
      1: HistoricoConta();
      2: RetornosCNAB();
      else Application.MessageBox('Relat�rio (aba) n�o definido', 'Aviso',
      MB_OK+MB_ICONWARNING);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmReceDesp.DBGrid1CellClick(Column: TColumn);
var
  Codigo, Status: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  Codigo := QrRetCodigo.Value;
  if QrRetStep.Value = 0 then
    Status := 1
  else
    Status := 0;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE cnab_lei SET ');
  Dmod.QrUpd.SQL.Add('Step=:P0');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1');
  Dmod.QrUpd.Params[00].AsInteger := Status;
  Dmod.QrUpd.Params[01].AsInteger := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
  QrRet.Close;
  QrRet.Open;
  QrRet.Locate('Codigo', Codigo, []);
  Screen.Cursor := crDefault;
end;

procedure TFmReceDesp.DemostrativoDeReceitasEDespesas();
var
  Imprime: Boolean;
  CarteiraUnica: Integer;
begin
  CarteiraUnica := EdCarteira.ValueVariant;
  DModFin.ReopenCreditosEDebitos(F_CliInt, FDataI1, FDataF1, CkAcordos.Checked,
  CkPeriodos.Checked, CkTextos.Checked, CkNaoAgruparNada.Checked, CarteiraUnica);
  //
  DmodFin.ReopenSaldoAEResumo(F_CliInt, CarteiraUnica, FDataI1, FDataF1);
  //
  Screen.Cursor := crDefault;
  if not CkNaoAgruparNada.Checked then
    Imprime := True
  else
    Imprime := False;
  if Imprime then
    MyObjects.frxMostra(frxReceDesp, 'Demonstrativo de receitas e despesas');
  //
end;

procedure TFmReceDesp.EdEmpresaChange(Sender: TObject);
{
var
  Cond: Integer;
}
begin
  ReopenCarteiras();
(***
  CBUH.KeyValue := Null;
  QrUHs.Close;
  Cond := Geral.IMV(EdCond.Text);
  if Cond > 0 then
  begin
    QrUHs.Params[0].AsInteger := Cond;
    QrUHs.Open;
  end;
*)
end;

procedure TFmReceDesp.EdValPagMinChange(Sender: TObject);
begin
  EdValPagMax.ValueVariant := EdValPagMin.ValueVariant;
end;

procedure TFmReceDesp.EdValTitMinChange(Sender: TObject);
begin
  EdValTitMax.ValueVariant := EdValTitMin.ValueVariant;
end;

procedure TFmReceDesp.frxReceDespGetValue(const VarName: String;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_PERIODO') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE2, TPDataIni1.Date) + ' at� ' +
    FormatDateTime(VAR_FORMATDATE2, TPDataFim1.Date)
  else if AnsiCompareText(VarName, 'VARF_NOMEEMPRESA') = 0 then
    Value := CBEmpresa.Text
  else if AnsiCompareText(VarName, 'VARF_CONTA_SEL') = 0 then
    Value := CBConta.Text
  else if AnsiCompareText(VarName, 'VARF_UH') = 0 then
  begin
    if CBUH.KeyValue = Null then
      Value := 'TODAS UHs'
    else
      Value := CBUH.Text;
  end
  else
end;

procedure TFmReceDesp.HistoricoConta;
begin
  QrDC.Close;
  QrDC.SQL.Clear;
  QrDC.SQL.Add('SELECT lan.Genero, lan.Mez, lan.Depto, lan.Credito-lan.Debito Valor,');
  QrDC.SQL.Add('lan.Controle, con.Nome NOMECON, cim.Unidade UNIDADE');
  QrDC.SQL.Add('FROM ' + VAR_LCT + ' lan');
  QrDC.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrDC.SQL.Add('LEFT JOIN contas    con ON con.Codigo=lan.Genero');
  QrDC.SQL.Add('LEFT JOIN condimov  cim ON cim.Conta=lan.Depto');
  QrDC.SQL.Add('WHERE lan.Tipo <> 2');
  case CGValores.Value of
    1: QrDC.SQL.Add('AND lan.Credito > lan.Debito');
    2: QrDC.SQL.Add('AND lan.Debito > lan.Credito');
  end;
  QrDC.SQL.Add('AND lan.Genero>0');
  QrDC.SQL.Add('AND car.ForneceI = ' + FormatFloat('0', F_CliInt));
  if FGenero > 0 then
    QrDC.SQL.Add('AND Genero=' + FormatFloat('0', FGenero));
  if FUH > 0 then
    QrDC.SQL.Add('AND Depto=' + FormatFloat('0', FUH));
  QrDC.SQL.Add('AND lan.Data BETWEEN "' + FDataI1 + '" AND "' + FDataF2 + '"');
  case RGOrdem.ItemIndex of
    0: QrDC.SQL.Add('ORDER BY con.OrdemLista, NOMECON, Mez, Depto');
    1: QrDC.SQL.Add('ORDER BY con.OrdemLista, NOMECON, Depto, Mez');
  end;
  QrDC.Open;
  //
  case RGOrdem.ItemIndex of
    0: MyObjects.frxMostra(frxDC_Mes, 'Hist�rico de Conta (Plano de contas) por M�s');
    1: MyObjects.frxMostra(frxDC_Uni, 'Hist�rico de Conta (Plano de contas) por Unidade');
  end;

end;

procedure TFmReceDesp.QrDCCalcFields(DataSet: TDataSet);
begin
   QrDCNOMEMES.Value := MLAGeral.MezToFDT(QrDCMez.Value, 0, 14);
end;

procedure TFmReceDesp.ReopenCarteiras();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCarts, Dmod.MyDB, [
  'SELECT car.*',
  'FROM carteiras car',
  'WHERE car.ForneceI IN (' + FormatFloat('0', QrEmpresasCodEnti.Value) + ')',
  'ORDER BY Nome',
  '']);
  EdCarteira.ValueVariant := 0;
  CBCarteira.KeyValue := 0;
end;

procedure TFmReceDesp.RetornosCNAB;
{
var
  Cond, Enti: Integer;
}
begin
(***
  Cond := Geral.IMV(EdCond.Text);
  //
  if Cond > 0 then
  begin
    Enti := QrCondCliente.Value;
    QrRet.Close;
    QrRet.SQL.Clear;
    QrRet.SQL.Add('SELECT *');
    QrRet.SQL.Add('FROM cnab_lei');
    QrRet.SQL.Add('WHERE Entidade=' + dmkPF.FFP(Enti, 0));
    QrRet.SQL.Add(dmkPF.SQL_Periodo('AND QuitaData ', TPDataIni2.Date,
      TPDataFim2.Date, True, True));
    QrRet.SQL.Add(dmkPF.SQL_Periodo('AND OcorrData ', TPDataIni2.Date,
      TPDataFim2.Date, True, True));
    QrRet.SQL.Add('AND ValTitul BETWEEN ' +
      dmkPF.FFP(EdValTitMin.ValueVariant, 2) + ' AND '  +
      dmkPF.FFP(EdValTitMax.ValueVariant, 2));
    QrRet.SQL.Add('AND ValPago BETWEEN ' +
      dmkPF.FFP(EdValPagMin.ValueVariant, 2) + ' AND '  +
      dmkPF.FFP(EdValPagMax.ValueVariant, 2));
    if EdBloqIni.ValueVariant > 0 then
      QrRet.SQL.Add('AND IDNum >= ' + dmkPF.FFP(EdBloqIni.ValueVariant, 0));
    if EdBloqFim.ValueVariant > 0 then
      QrRet.SQL.Add('AND IDNum <= ' + dmkPF.FFP(EdBloqIni.ValueVariant, 0));
    QrRet.Open;
  end else begin
    Application.MessageBox('Informe o condom�nio!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdCond.SetFocus;
  end;
*)
end;

end.

