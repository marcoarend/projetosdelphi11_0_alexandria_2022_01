object FmCorrigeCompensados: TFmCorrigeCompensados
  Left = 339
  Top = 185
  Caption = 'Corrige Compensados'
  ClientHeight = 494
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object Label1: TLabel
      Left = 316
      Top = 4
      Width = 82
      Height = 13
      Caption = 'Tempo Restante:'
    end
    object Label2: TLabel
      Left = 440
      Top = 4
      Width = 26
      Height = 13
      Caption = 'Itens:'
    end
    object Panel2: TPanel
      Left = 672
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 15
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sair'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Edit1: TEdit
      Left = 316
      Top = 20
      Width = 121
      Height = 21
      ReadOnly = True
      TabOrder = 1
    end
    object Edit2: TEdit
      Left = 440
      Top = 20
      Width = 221
      Height = 21
      ReadOnly = True
      TabOrder = 2
    end
    object BitBtn1: TBitBtn
      Tag = 14
      Left = 12
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Compensar'
      TabOrder = 3
      OnClick = BitBtn1Click
      NumGlyphs = 2
    end
    object BitBtn2: TBitBtn
      Tag = 13
      Left = 104
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Abortar'
      TabOrder = 4
      OnClick = BitBtn2Click
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    
    
    
    
    Caption = 'Corrige Compensados'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2

    object Image1: TImage
      Left = 2
      Top = 2
      Width = 780
      Height = 44
      Align = alClient
      Transparent = True
      ExplicitWidth = 788
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 398
    Align = alClient
    TabOrder = 0
    object PB1: TProgressBar
      Left = 1
      Top = 380
      Width = 782
      Height = 17
      Align = alBottom
      TabOrder = 0
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 1
      Width = 782
      Height = 379
      Align = alClient
      DataSource = DsErr
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
  end
  object QrUpd: TmySQLDirectQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'UPDATE lanctos SET CtrlQuitPg=:P0'
      'WHERE Controle=:P1')
    Left = 92
    Top = 8
  end
  object QrErrQ: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Sub, CtrlQuitPg, Vencimento, Data'
      'FROM syndic.lanctos'
      'WHERE Sit>1'
      'AND Compensado<2'
      'AND Tipo = 2'
      'AND CtrlQuitPg>0')
    Left = 8
    Top = 8
    object QrErrQControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrErrQData: TDateField
      FieldName = 'Data'
    end
    object QrErrQVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrErrQCtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
    end
  end
  object DsErr: TDataSource
    DataSet = QrErrQ
    Left = 36
    Top = 8
  end
  object QrPagto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data'
      'FROM lanctos'
      'WHERE Controle=:P0')
    Left = 64
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPagtoData: TDateField
      FieldName = 'Data'
    end
  end
end
