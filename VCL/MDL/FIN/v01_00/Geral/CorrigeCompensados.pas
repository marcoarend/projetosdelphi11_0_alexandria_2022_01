unit CorrigeCompensados;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     ComCtrls, DB,
  mySQLDbTables, mySQLDirectQuery, UMySQLModule, dmkGeral, Grids, DBGrids;

type
  TFmCorrigeCompensados = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    PB1: TProgressBar;
    Edit1: TEdit;
    Label1: TLabel;
    Edit2: TEdit;
    Label2: TLabel;
    QrUpd: TmySQLDirectQuery;
    BitBtn1: TBitBtn;
    QrErrQ: TmySQLQuery;
    DBGrid1: TDBGrid;
    DsErr: TDataSource;
    QrPagto: TmySQLQuery;
    QrErrQControle: TIntegerField;
    QrErrQCtrlQuitPg: TIntegerField;
    QrPagtoData: TDateField;
    BitBtn2: TBitBtn;
    QrErrQVencimento: TDateField;
    QrErrQData: TDateField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
    FAbortar: Boolean;
  public
    { Public declarations }
  end;

  var
  FmCorrigeCompensados: TFmCorrigeCompensados;

implementation

uses UnMyObjects, Module, UnFinanceiro;

{$R *.DFM}

procedure TFmCorrigeCompensados.BitBtn1Click(Sender: TObject);
const
  Step = 1;
var
  Conta, Atual,
  Total, Falta: Integer;
  T1, T2, T3: TDateTime;
//var
  Compensado: String;
  Controle: Integer;
begin
  FAbortar := False;
  QrErrQ.First;
  QrErrQ.DisableControls;
  Screen.Cursor := crHourGlass;
  T1 := Now();
  Total := QrErrQ.RecordCount;
  PB1.Max := Total;
  Conta := 0;
  Atual := 0;
   //
  while not QrErrQ.Eof do
  begin
    Application.ProcessMessages;
    if FAbortar then
    begin
      QrErrQ.EnableControls;
      Screen.Cursor := crDefault;
      Geral.MensagemBox(
        'Atualização abortada!', 'Aviso', MB_OK+MB_ICONWARNING);
      QrErrQ.Close;
      QrErrQ.Open;
      Edit1.Text := '';
      Edit2.Text := '';
      Exit;
    end;
    Atual := Atual + 1;
    Conta := Conta + 1;
    if (Conta div step = Conta / step) then
    begin
      { @3
      Dmod.MyDB.Execute(Texto);
      Texto := '';
      }
      PB1.Position := PB1.Position + step;
      Conta := 0;
      T2 := Now();
      Falta := Total - Atual;
      T3 := (T2 - T1) /  Atual * Falta;
      //
      Edit1.Text := FormatDateTime('hh:nn:ss', T3);
      Edit2.Text := IntToStr(Atual) + ' de ' +
                    IntToStr(Total) + '. Faltam ' + IntToStr(Falta);
      //
      Update;
      Application.ProcessMessages;
    end;
    QrPagto.Close;
    QrPagto.Params[0].AsInteger := QrErrQCtrlQuitPg.Value;
    QrPagto.Open;
    //
    if QrPagto.RecordCount > 0 then
    begin
      if QrPagtoData.Value > 2 then
        Compensado := Geral.FDT(QrPagtoData.Value, 1)
      else
      if QrErrQVencimento.Value > 2 then
        Compensado := Geral.FDT(QrErrQVencimento.Value, 1)
      else
        Compensado := Geral.FDT(QrErrQData.Value, 1);
      Controle := QrErrQControle.Value;
      UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
      'Compensado'], ['Controle'], [Compensado], [Controle], True, '');
      if QrPagtoData.Value <= 2 then
        UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
        'Data'], ['Controle'], [Compensado], [QrErrQCtrlQuitPg.Value], True, '');
    end;
    //
    QrErrQ.Next;
  end;
  PB1.Position := PB1.Position + Conta;
  QrErrQ.Close;
  QrErrQ.Open;
  Screen.Cursor := crDefault;
  Application.MessageBox(
    'Atualização de compensado finalizado!', 'Aviso', MB_OK+MB_ICONINFORMATION);
  Screen.Cursor := crDefault;
  QrErrQ.EnableControls;
end;

procedure TFmCorrigeCompensados.BitBtn2Click(Sender: TObject);
begin
  FAbortar := True;
end;

procedure TFmCorrigeCompensados.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCorrigeCompensados.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCorrigeCompensados.FormCreate(Sender: TObject);
begin
  FAbortar := False;
  QrErrQ.Close;
  QrErrQ.Open;
end;

procedure TFmCorrigeCompensados.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.

