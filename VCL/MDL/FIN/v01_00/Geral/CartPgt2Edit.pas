unit CartPgt2Edit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, UnMLAGeral, ComCtrls, UnGOTOy, Buttons, Db, (*DBTables,*)
  UnInternalConsts, (*DBIProcs,*) ExtCtrls, ZCF2, UMySQLModule, mySQLDbTables,
  UnMyLinguas, Variants, dmkGeral, dmkEdit, dmkDBLookupComboBox, dmkEditCB,
  dmkEditDateTimePicker, UnDmkEnums;

type
  TFmCartPgt2Edit = class(TForm)
    QrContas: TMySQLQuery;
    DsContas: TDataSource;
    QrCarteiras: TMySQLQuery;
    DsCarteiras: TDataSource;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    Panel1: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    Label1: TLabel;
    LaDeb: TLabel;
    LaVencimento: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    TPData: TdmkEditDateTimePicker;
    EdValor: TdmkEdit;
    TPVencimento: TdmkEditDateTimePicker;
    EdDescricao: TdmkEdit;
    EdCodigo: TdmkEdit;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    RGCarteira: TRadioGroup;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasNome3: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrContasDataCad: TDateField;
    QrContasDataAlt: TDateField;
    QrContasUserCad: TSmallintField;
    QrContasUserAlt: TSmallintField;
    QrContasNOMESUBGRUPO: TWideStringField;
    QrContasNOMEGRUPO: TWideStringField;
    QrContasNOMECONJUNTO: TWideStringField;
    QrContasNOMEEMPRESA: TWideStringField;
    LaDescoPor: TLabel;
    CBDescoPor: TdmkDBLookupComboBox;
    EdDescoPor: TdmkEditCB;
    EdDescoVal: TdmkEdit;
    LaDescoVal: TLabel;
    QrDescoPor: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    IntegerField4: TIntegerField;
    DsDescoPor: TDataSource;
    QrCarteirasTipo: TIntegerField;
    QrCarteirasInicial: TFloatField;
    QrCarteirasBanco: TIntegerField;
    QrCarteirasID: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasID_Fat: TWideStringField;
    QrCarteirasSaldo: TFloatField;
    QrCarteirasLk: TIntegerField;
    QrCarteirasEmCaixa: TFloatField;
    QrCarteirasFechamento: TIntegerField;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasDataCad: TDateField;
    QrCarteirasDataAlt: TDateField;
    QrCarteirasUserCad: TIntegerField;
    QrCarteirasUserAlt: TIntegerField;
    QrCarteirasPagRec: TIntegerField;
    QrCarteirasDiaMesVence: TSmallintField;
    QrCarteirasExigeNumCheque: TSmallintField;
    QrCarteirasForneceI: TIntegerField;
    QrCarteirasNome2: TWideStringField;
    QrCarteirasContato1: TWideStringField;
    QrCarteirasTipoDoc: TSmallintField;
    QrCarteirasBanco1: TIntegerField;
    QrCarteirasAgencia1: TIntegerField;
    QrCarteirasConta1: TWideStringField;
    QrCarteirasCheque1: TIntegerField;
    QrCarteirasAntigo: TWideStringField;
    QrCarteirasContab: TWideStringField;
    QrCarteirasFuturoC: TFloatField;
    QrCarteirasFuturoD: TFloatField;
    QrCarteirasFuturoS: TFloatField;
    QrCarteirasOrdem: TIntegerField;
    QrCarteirasForneceN: TSmallintField;
    QrCarteirasExclusivo: TSmallintField;
    QrCarteirasAlterWeb: TSmallintField;
    QrCarteirasRecebeBloq: TSmallintField;
    QrCarteirasEntiDent: TIntegerField;
    QrCarteirasCodCedente: TWideStringField;
    QrCarteirasValMorto: TFloatField;
    QrCarteirasAtivo: TSmallintField;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    EdOldControle: TdmkEdit;
    TPOldData: TdmkEditDateTimePicker;
    Label2: TLabel;
    Label3: TLabel;
    EdOldSub: TdmkEdit;
    Label4: TLabel;
    EdOldTipo: TdmkEdit;
    Label5: TLabel;
    EdOldCarteira: TdmkEdit;
    Label6: TLabel;
    EdSerieCH: TdmkEdit;
    EdDoc: TdmkEdit;
    LaDoc: TLabel;
    Label7: TLabel;
    EdMoraDia: TdmkEdit;
    EdMulta: TdmkEdit;
    Label9: TLabel;
    Label8: TLabel;
    EdMoraVal: TdmkEdit;
    EdMultaVal: TdmkEdit;
    Label10: TLabel;
    Label11: TLabel;
    EdOldSaldo: TdmkEdit;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure RGCarteiraClick(Sender: TObject);
    procedure BtContasClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdValorExit(Sender: TObject);
    procedure TPDataChange(Sender: TObject);
    procedure TPDataClick(Sender: TObject);
    procedure EdMoraDiaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdMultaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdMultaExit(Sender: TObject);
    procedure EdMoraDiaExit(Sender: TObject);
    procedure EdMoraValExit(Sender: TObject);
    procedure EdMultaValExit(Sender: TObject);
  private
    { Private declarations }
    procedure CarteirasReopen(Tipo: Integer);
    procedure CalculaMultaJuroVal;
    procedure CalculaMultaJuroPer;
  public
    { Public declarations }
  end;

var
  FmCartPgt2Edit: TFmCartPgt2Edit;

implementation

uses UnMyObjects, Module, Contas, Principal, CartPgt2, UnFinanceiro, MyDBCheck;

{$R *.DFM}

procedure TFmCartPgt2Edit.CalculaMultaJuroPer;
var
  ValorNew, MultaPerc, JuroPerc: Double;
begin
  UFinanceiro.CalculaPercMultaEJuros(EdOldSaldo.ValueVariant,
    EdMultaVal.ValueVariant, EdMoraVal.ValueVariant, TPData.Date,
    TPOldData.Date, ValorNew, MultaPerc, JuroPerc);
  //
  EdMoraDia.ValueVariant := JuroPerc;
  EdMulta.ValueVariant   := MultaPerc;
  EdValor.ValueVariant   := ValorNew;
end;

procedure TFmCartPgt2Edit.CalculaMultaJuroVal;
var
  MultaPer, JuroPer, ValAtualiz, ValMulta, ValJuro: Double;
begin
  MultaPer := EdMulta.ValueVariant;
  JuroPer  := EdMoraDia.ValueVariant;
  //   
  UFinanceiro.CalculaValMultaEJuros(EdOldSaldo.ValueVariant, TPData.Date,
    TPOldData.Date, MultaPer, JuroPer, ValAtualiz, ValMulta, ValJuro, True);
  //
  EdMoraVal.ValueVariant  := ValJuro;
  EdMultaVal.ValueVariant := ValMulta;
  EdValor.ValueVariant    := ValAtualiz;
end;

procedure TFmCartPgt2Edit.CarteirasReopen(Tipo: Integer);
begin
  QrCarteiras.Close;
  QrCarteiras.Params[0].AsInteger := Tipo;
  QrCarteiras.Params[1].AsInteger := FmCartPgt2.FQrCarteiras.FieldByName('ForneceI').AsInteger;
  QrCarteiras.Open;
end;

procedure TFmCartPgt2Edit.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCartPgt2Edit.BtConfirmaClick(Sender: TObject);
var
  OldTipo, OldCarteira, OldControle, OldSub,
  Carteira, Sit, Nivel: Integer;
  Controle: Int64;
  Valor, Credito, Debito, Doc, CtrlIni: Double;
  OldData: TDate;
  Vencimento: TDateTime;
  Compensado, Mes: String;
begin
  Nivel := FmCartPgt2.FQrLct.FieldByName('Nivel').AsInteger + 1;
  if FmCartPgt2.FQrLct.FieldByName('CtrlIni').AsInteger > 0 then
    CtrlIni := FmCartPgt2.FQrLct.FieldByName('CtrlIni').AsInteger
  else CtrlIni := FmCartPgt2.FQrLct.FieldByName('Controle').AsInteger;
  //
  Valor := Geral.DMV(EdValor.Text);
  if FmCartPgt2.FQrLct.FieldByName('Credito').AsFloat > 0 then Credito := Valor else Credito := 0;
  if FmCartPgt2.FQrLct.FieldByName('Debito').AsFloat > 0 then Debito := Valor else Debito := 0;
  if RGCarteira.ItemIndex < 0 then
  begin
    ShowMessage('Defina uma carteira!');
    RGCarteira.SetFocus;
    Exit;
  end;
  if CBCarteira.KeyValue <> NULL then Carteira := CBCarteira.KeyValue else
  begin
    ShowMessage('Defina um item de carteira!');
    EdCarteira.SetFocus;
    Exit;
  end;
  if Carteira < 1 then
  begin
    ShowMessage('Defina um item de carteira maior que zero!');
    EdCarteira.SetFocus;
    Exit;
  end;
  if (Valor = 0)  then
  begin
    ShowMessage('Defina um valor.');
    EdValor.SetFocus;
    Exit;
  end;
  if FmCartPgt2.FQrLct.FieldByName('MENSAL2').AsString = '' then Mes := '' else
  Mes := IntToStr(FmCartPgt2.FQrLct.FieldByName('Mez').AsInteger);
  if (EdDoc.Enabled = False) then Doc := 0
  else Doc := Geral.DMV(EdDoc.Text);
  {
  //Estava colocando no vencimento a data de compensação 
  if (TPVencimento.Enabled = False) then Vencimento :=
    FormatDateTime(VAR_FORMATDATE, TPData.Date)
  else Vencimento := FormatDateTime(VAR_FORMATDATE, TPVencimento.Date);
  }
  //Vencimento := FormatDateTime(VAR_FORMATDATE, TPVencimento.Date);
  if (TPVencimento.Visible = False) then
    Vencimento := TPOldData.Date
  else
    Vencimento := TPVencimento.Date;
  //
  if RGCarteira.ItemIndex = 2 then
  begin
    Compensado := '';
    Sit := 0;
  end else begin
    Compensado := FormatDateTime(VAR_FORMATDATE, TPData.Date);
    Sit := 3;
  end;
  OldCarteira := EdOldCarteira.ValueVariant;
  //
  UFinanceiro.LancamentoDefaultVARS;
  FLAN_SerieCH        := EdSerieCH.Text;
  FLAN_Documento      := Trunc(Doc + 0.01);
  FLAN_Data           := FormatDateTime(VAR_FORMATDATE, TPData.Date);
  FLAN_Tipo           := RGCarteira.ItemIndex;
  FLAN_Carteira       := Carteira;
  FLAN_Credito        := Credito;
  FLAN_Debito         := Debito;
  FLAN_Genero         := FmCartPgt2.FQrLct.FieldByName('Genero').AsInteger;
  FLAN_NotaFiscal     := 0;
  FLAN_Vencimento     := FormatDateTime(VAR_FORMATDATE, Vencimento);
  FLAN_Mez            := Mes;
  FLAN_Descricao      := EdDescricao.Text;
  FLAN_Sit            := Sit;
  FLAN_Cliente        := FmCartPgt2.FQrLct.FieldByName('Cliente').AsInteger;
  FLAN_Fornecedor     := FmCartPgt2.FQrLct.FieldByName('Fornecedor').AsInteger;
  FLAN_ID_Pgto        := StrToInt(FmCartPgt2.LaControle.Caption);
  FLAN_DataCad        := FormatDateTime(VAR_FORMATDATE, Date);
  FLAN_UserCad        := VAR_USUARIO;
  FLAN_DataDoc        := FormatDateTime(VAR_FORMATDATE, FmCartPgt2.FQrLct.FieldByName('DataDoc').AsDateTime);
  //
  //FLAN_MoraDia        := Geral.DMV(EdMoraDia.Text);
  //FLAN_Multa          := Geral.DMV(EdMulta.Text);
  FLAN_MoraVal        := Geral.DMV(EdMoraDia.Text);
  FLAN_MultaVal       := Geral.DMV(EdMulta.Text);
  //
  FLAN_CtrlIni        := Trunc(CtrlIni + 0.01);
  FLAN_Nivel          := Nivel;
  FLAN_Vendedor       := FmCartPgt2.FQrLct.FieldByName('Vendedor').AsInteger;
  FLAN_Account        := FmCartPgt2.FQrLct.FieldByName('Account').AsInteger;
  FLAN_DescoPor       := Geral.IMV(EdDescoPor.Text);
  FLAN_DescoVal       := Geral.DMV(EdDescoVal.Text);
  // Synker
  FLAN_CliInt         := FmCartPgt2.FQrLct.FieldByName('CliInt').AsInteger;
  FLAN_ForneceI       := FmCartPgt2.FQrLct.FieldByName('ForneceI').AsInteger;
  FLAN_Depto          := FmCartPgt2.FQrLct.FieldByName('Depto').AsInteger;
  FLAN_Compensado     := Compensado;
  FLAN_FatID          := FmCartPgt2.FQrLct.FieldByName('FatID').AsInteger;
  FLAN_FatID_Sub      := FmCartPgt2.FQrLct.FieldByName('FatID_Sub').AsInteger;
  FLAN_FatNum         := FmCartPgt2.FQrLct.FieldByName('FatNum').AsInteger;
  FLAN_FatParcela     := FmCartPgt2.FQrLct.FieldByName('FatParcela').AsInteger;
  //
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Controle := UMyMod.BuscaEmLivreInt64Y(Dmod.MyDB, 'Livres', 'Controle', VAR_LCT, VAR_LCT, 'Controle');
    FLAN_Controle       := Controle;
    VAR_DATAINSERIR := TPData.Date;
    VAR_LANCTO2 := Controle;
    //
    if UFinanceiro.InsereLancamento() then
    begin
      UFinanceiro.RecalcSaldoCarteira(CBCarteira.KeyValue, nil, False, False);
      UFinanceiro.RecalcSaldoCarteira(OldCarteira, nil, False, False);
    end;
  end else begin
    OldData     := TPOldData.Date;
    OldTipo     := EdOldTipo.ValueVariant;
    OldControle := EdOldControle.ValueVariant;
    OldSub      := EdOldSub.ValueVariant;
    UMyMod.LctUpd(Dmod.QrUpd, VAR_LCT, [
    'Descricao', 'Debito', 'Credito',
    'SerieCH', 'Documento', 'Vencimento',
    //'MoraDia', 'Multa',
    'MoraVal', 'MultaVal',
    'DescoPor', 'DescoVal', 'Data'], [
    FLAN_Descricao, FLAN_Debito, FLAN_Credito,
    FLAN_SerieCH, FLAN_Documento, FLAN_Vencimento,
    //FLAN_MoraDia, FLAN_Multa,
    FLAN_MoraVal, FLAN_MultaVal,    
    FLAN_DescoPor, FLAN_DescoVal, FLAN_Data], OldData,
    OldTipo, OldCarteira, OldControle, OldSub);
    VAR_LANCTO2 := FmCartPgt2.FQrLct.FieldByName('Controle').AsInteger;
    UFinanceiro.RecalcSaldoCarteira(OldCarteira, nil, False, False);
  end;
  //
  Close;
end;

procedure TFmCartPgt2Edit.FormCreate(Sender: TObject);
begin
  QrContas.Open;
  //
  LaVencimento.Visible := False;
  TPVencimento.Visible := False;
end;

procedure TFmCartPgt2Edit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  CarteirasReopen(RGCarteira.ItemIndex);
end;

procedure TFmCartPgt2Edit.RGCarteiraClick(Sender: TObject);
begin
  EdCarteira.Text := '0';
  CBCarteira.KeyValue := 0;
  CarteirasReopen(RGCarteira.ItemIndex);
  case RGCarteira.ItemIndex of
    0:
    begin
      EdDoc.Enabled := False;
      LaDoc.Enabled := False;
      TPVencimento.Visible := False;
      LaVencimento.Visible := False;
      //
      TPVencimento.Date := Date; 
    end;
    1:
    begin
      EdDoc.Enabled := True;
      LaDoc.Enabled := True;
      TPVencimento.Visible := False;
      LaVencimento.Visible := False;
      //
      TPVencimento.Date := FmCartPgt2.FQrLct.FieldByName('Vencimento').AsDateTime; 
    end;
    2:
    begin
      EdDoc.Enabled := True;
      LaDoc.Enabled := True;
      TPVencimento.Visible := True;
      LaVencimento.Visible := True;
      //
      TPVencimento.Date := FmCartPgt2.FQrLct.FieldByName('Vencimento').AsDateTime; 
    end;
  end;
end;

procedure TFmCartPgt2Edit.TPDataChange(Sender: TObject);
begin
  CalculaMultaJuroVal;
end;

procedure TFmCartPgt2Edit.TPDataClick(Sender: TObject);
begin
  CalculaMultaJuroVal;
end;

procedure TFmCartPgt2Edit.BtContasClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    FmContas.LocCod(QrContasCodigo.Value, QrContasCodigo.Value);
    FmContas.ShowModal;
    FmContas.Destroy;
    QrContas.Close;
    QrContas.Open;
    FmContas.LocCod(0, QrContasCodigo.Value);
  end;
end;

procedure TFmCartPgt2Edit.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCartPgt2Edit.EdMoraDiaExit(Sender: TObject);
begin
  CalculaMultaJuroVal;
end;

procedure TFmCartPgt2Edit.EdMoraDiaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {
  if Key=VK_F4 then
  begin
    FmPrincipal.CriaCalcPercent(EdValor.Text, '1,0000', cpJurosMes);
    EdMoraDia.Text := Geral.TFT(FloatToStr(VAR_VALOR), 2, siPositivo);
  end;
  }
end;

procedure TFmCartPgt2Edit.EdMoraValExit(Sender: TObject);
begin
  CalculaMultaJuroPer;
end;

procedure TFmCartPgt2Edit.EdMultaExit(Sender: TObject);
begin
  CalculaMultaJuroVal;
end;

procedure TFmCartPgt2Edit.EdMultaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {
  if Key=VK_F4 then
  begin
    FmPrincipal.CriaCalcPercent(EdValor.Text, '1,0000', cpJurosMes);
    EdMoraDia.Text := Geral.TFT(FloatToStr(VAR_VALOR), 2, siPositivo);
  end;
  }
end;

procedure TFmCartPgt2Edit.EdMultaValExit(Sender: TObject);
begin
  CalculaMultaJuroPer;
end;

procedure TFmCartPgt2Edit.EdValorExit(Sender: TObject);
  function SubstituirString(Str, Texto: String): String;
  var
    Posicao: Integer;
  begin
    Posicao := Pos(Str, Texto);
    if Posicao > 0 then
      Result := StringReplace(Texto, Str, '', [rfReplaceAll, rfIgnoreCase])
    else
      Result := Texto;
  end;
  function ImplementaStr(Str, Texto: String): String;
  var
    Posicao: Integer;
  begin
    Posicao := Pos(Str, Texto);
    if Posicao = 0 then
      Result := Texto + Str
    else
      Result := Texto;
  end;
var
  Credito, Debito, Valor: Double;
begin
  Valor   := EdValor.ValueVariant;
  Credito := FmCartPgt2.FQrLct.FieldByName('Credito').AsFloat;
  Debito  := FmCartPgt2.FQrLct.FieldByName('Debito').AsFloat;
  //
  if (Credito > 0) and (Debito = 0) then
  begin
    if Valor < Credito then
      EdDescricao.ValueVariant := ImplementaStr(' (parcial)', EdDescricao.ValueVariant)
    else
      EdDescricao.ValueVariant := SubstituirString(' (parcial)', EdDescricao.ValueVariant);
  end else
  if (Credito = 0) and (Debito > 0) then
  begin
    if Valor < Debito then
      EdDescricao.ValueVariant := ImplementaStr(' (parcial)', EdDescricao.ValueVariant)
    else
      EdDescricao.ValueVariant := SubstituirString(' (parcial)', EdDescricao.ValueVariant);
  end;
end;

end.

