object FmCarteirasU: TFmCarteirasU
  Left = 339
  Top = 185
  Caption = 'FIN-CARTE-002 :: Carteiras - Habilita'#231#227'o de Usu'#225'rio'
  ClientHeight = 182
  ClientWidth = 547
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 134
    Width = 547
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 435
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 547
    Height = 48
    Align = alTop
    Caption = 'Carteiras - Habilita'#231#227'o de Usu'#225'rio'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 545
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitTop = 2
      ExplicitWidth = 543
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 547
    Height = 86
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 20
      Top = 4
      Width = 39
      Height = 13
      Caption = 'Usu'#225'rio:'
    end
    object EdUsuario: TdmkEditCB
      Left = 20
      Top = 20
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBUsuario
      IgnoraDBLookupComboBox = False
    end
    object CBUsuario: TdmkDBLookupComboBox
      Left = 76
      Top = 20
      Width = 453
      Height = 21
      KeyField = 'Numero'
      ListField = 'Login'
      ListSource = DsSenhas
      TabOrder = 1
      dmkEditCB = EdUsuario
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object CkContinuar: TCheckBox
      Left = 20
      Top = 48
      Width = 113
      Height = 17
      Caption = 'Continuar inserindo.'
      TabOrder = 2
    end
  end
  object QrSenhas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Login, Numero'
      'FROM senhas'
      'WHERE Numero > 0'
      'AND NOT (Numero IN ('
      '  SELECT Usuario'
      '  FROM carteirasu'
      '  WHERE Codigo=:P0)'#13
      ')'#10
      ''
      'ORDER BY Login')
    Left = 244
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSenhasLogin: TWideStringField
      FieldName = 'Login'
      Size = 30
    end
    object QrSenhasNumero: TIntegerField
      FieldName = 'Numero'
    end
  end
  object DsSenhas: TDataSource
    DataSet = QrSenhas
    Left = 272
    Top = 56
  end
end
