unit CashBalIni;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, DB, mySQLDbTables,
  Mask, DBCtrls, dmkEdit, Menus, ComCtrls, Variants, dmkGeral, UnDmkEnums;

type
  TFmCashBalIni = class(TForm)
    PainelConfirma: TPanel;
    BtAcao: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrCrt: TmySQLQuery;
    PnDivergencias: TPanel;
    DsEmpresa: TDataSource;
    DsCrt: TDataSource;
    QrCrtCodigo: TIntegerField;
    QrCrtNome: TWideStringField;
    QrCrtInicial: TFloatField;
    QrSdo: TmySQLQuery;
    DsSdo: TDataSource;
    Splitter2: TSplitter;
    QrSCrt: TmySQLQuery;
    DsSCrt: TDataSource;
    QrSSdo: TmySQLQuery;
    DsSSdo: TDataSource;
    QrSCrtInicial: TFloatField;
    QrSSdoSdoIni: TFloatField;
    QrSdoCodigo: TIntegerField;
    QrSdoNOMECTA: TWideStringField;
    QrSdoSdoIni: TFloatField;
    Panel10: TPanel;
    Splitter3: TSplitter;
    Panel11: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Panel12: TPanel;
    Label7: TLabel;
    DBGrid3: TDBGrid;
    Panel13: TPanel;
    DBEdit9: TDBEdit;
    Panel14: TPanel;
    Label8: TLabel;
    DBGrid4: TDBGrid;
    Panel15: TPanel;
    DBEdit10: TDBEdit;
    LaDiverge: TLabel;
    QrDivCart: TmySQLQuery;
    DsDivCart: TDataSource;
    QrDivLan: TmySQLQuery;
    DsDivLan: TDataSource;
    QrDivCartInicial: TFloatField;
    QrDivCartNome: TWideStringField;
    QrDivCartCarteira: TIntegerField;
    QrDivCartSdoMov: TFloatField;
    QrDivCartSaldo: TFloatField;
    QrDivLanInicial: TFloatField;
    QrDivLanNome: TWideStringField;
    QrDivLanCarteira: TIntegerField;
    QrDivLanSdoMov: TFloatField;
    QrDivLanSaldo: TFloatField;
    EdPeriodo_TXT: TdmkEdit;
    Label3: TLabel;
    PMAcao1: TPopupMenu;
    Colocarcarteiracorretanoslanamentos1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel3: TPanel;
    Splitter1: TSplitter;
    Panel6: TPanel;
    Label2: TLabel;
    Panel9: TPanel;
    EdDivLan: TdmkEdit;
    DBGrid1: TDBGrid;
    Panel7: TPanel;
    Label4: TLabel;
    DBGrid2: TDBGrid;
    Panel8: TPanel;
    EdDivCart: TdmkEdit;
    TabSheet2: TTabSheet;
    QrLctDvr: TmySQLQuery;
    DsLctDvr: TDataSource;
    DBGLctDvr: TDBGrid;
    QrLctDvrControle: TIntegerField;
    QrLctDvrSub: TSmallintField;
    QrLctDvrGenero: TIntegerField;
    QrLctDvrCredito: TFloatField;
    QrLctDvrDebito: TFloatField;
    QrLctDvrCliInt: TIntegerField;
    QrLctDvrCAR_TIPO: TIntegerField;
    QrLctDvrCAR_FORNECEI: TIntegerField;
    DsSNG: TDataSource;
    TabSheet3: TTabSheet;
    DBGrid5: TDBGrid;
    QrSNG: TmySQLQuery;
    QrSNGNivel: TIntegerField;
    QrSNGGenero: TIntegerField;
    QrSNGNomeGe: TWideStringField;
    QrSNGNomeNi: TWideStringField;
    QrSNGSumMov: TFloatField;
    QrSNGSdoAnt: TFloatField;
    QrSNGSumCre: TFloatField;
    QrSNGSumDeb: TFloatField;
    QrSNGSdoFim: TFloatField;
    QrSNGCtrla: TSmallintField;
    QrSNGSeleci: TSmallintField;
    QrSNGCodPla: TIntegerField;
    QrSNGCodCjt: TIntegerField;
    QrSNGCodGru: TIntegerField;
    QrSNGCodSgr: TIntegerField;
    QrSNGCODIGOS_TXT: TWideStringField;
    QrSNGOrdena: TWideStringField;
    QrSNGKGT: TLargeintField;
    QrPesq: TmySQLQuery;
    QrPesqSdoIni: TFloatField;
    QrSNGIniOld: TFloatField;
    QrQLN: TmySQLQuery;
    DsQLN: TDataSource;
    QrQLNCodigo: TIntegerField;
    QrQLNNome: TWideStringField;
    QrQLNLanctos: TLargeintField;
    Panel4: TPanel;
    Panel5: TPanel;
    DBGrid6: TDBGrid;
    Panel16: TPanel;
    Label1: TLabel;
    RGNiveis1: TRadioGroup;
    Panel17: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    DBGPrevLct1: TDBGrid;
    QrPrevLct1: TmySQLQuery;
    DsPrevLct1: TDataSource;
    QrPrevLct1Controle: TAutoIncField;
    QrPrevLct1Data: TDateField;
    QrPrevLct1Tipo: TSmallintField;
    QrPrevLct1Carteira: TIntegerField;
    QrPrevLct1Genero: TIntegerField;
    QrPrevLct1Descricao: TWideStringField;
    QrPrevLct1Debito: TFloatField;
    QrPrevLct1Credito: TFloatField;
    QrPrevLct1Ativo: TSmallintField;
    QrCrtDIFERENCA: TFloatField;
    QrOCF_Cart: TmySQLQuery;
    QrOCF_CartVALOR: TFloatField;
    QrSNGDIFERENCA: TFloatField;
    QrOCF_Ctas: TmySQLQuery;
    QrOCF_CtasVALOR: TFloatField;
    PMAcao2: TPopupMenu;
    Confirmaracriacaodospre_lanamentos1: TMenuItem;
    QrPrevLct1Entidade: TIntegerField;
    Revertersaldosiniciais1: TMenuItem;
    PMInclui: TPopupMenu;
    Peladiferencadacarteiradeprecada1: TMenuItem;
    Pelovalordacontadeprecada1: TMenuItem;
    TabSheet4: TTabSheet;
    QrSdoNew: TmySQLQuery;
    DsSdoNew: TDataSource;
    DBGrid7: TDBGrid;
    QrSdoNewData: TDateField;
    QrSdoNewCarteira: TIntegerField;
    QrSdoNewControle: TIntegerField;
    QrSdoNewGenero: TIntegerField;
    QrSdoNewDescricao: TWideStringField;
    QrSdoNewCredito: TFloatField;
    QrSdoNewDebito: TFloatField;
    QrSdoNewNOMECART: TWideStringField;
    QrSdoNewNOMECONTA: TWideStringField;
    QrSdoNewSubGrupo: TIntegerField;
    QrSdoNewNOMESUBGRUPO: TWideStringField;
    QrSdoNewGrupo: TIntegerField;
    QrSdoNewNOMEGRUPO: TWideStringField;
    QrSdoNewConjunto: TIntegerField;
    QrSdoNewNOMECONJUNTO: TWideStringField;
    QrSdoNewPlano: TIntegerField;
    QrSdoNewNOMEPLANO: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrCrtBeforeClose(DataSet: TDataSet);
    procedure QrCrtAfterOpen(DataSet: TDataSet);
    procedure QrSdoAfterOpen(DataSet: TDataSet);
    procedure PMAcao1Popup(Sender: TObject);
    procedure Colocarcarteiracorretanoslanamentos1Click(Sender: TObject);
    procedure BtAcaoClick(Sender: TObject);
    procedure RGNiveis1Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure QrCrtCalcFields(DataSet: TDataSet);
    procedure QrSNGCalcFields(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure QrQLNAfterScroll(DataSet: TDataSet);
    procedure BtAlteraClick(Sender: TObject);
    procedure Confirmaracriacaodospre_lanamentos1Click(Sender: TObject);
    procedure Revertersaldosiniciais1Click(Sender: TObject);
    procedure Peladiferencadacarteiradeprecada1Click(Sender: TObject);
    procedure Pelovalordacontadeprecada1Click(Sender: TObject);
    procedure QrSdoBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    FEntidade: Integer;
    FPrevLct1, FDataSeguinte: String;
    procedure ReopenDivergencias();
    procedure ReopenQLN();
    procedure ReopenPrevLct1();
    procedure ReopenSdoNew();
    procedure MostraEdicaoLctPrevia(SQLType: TSQLType; Valor: Double);
    procedure ReopenTudo();
  public
    { Public declarations }
    procedure AtualizaIniOld();
  end;

  var
  FmCashBalIni: TFmCashBalIni;

implementation

{$R *.DFM}

uses UnMyObjects, CashBal, Module, UMySQLModule, UnInternalConsts, ModuleFin, ModuleGeral,
UCreate, LctPrevia, MyDBCheck, UnFinanceiro;

procedure TFmCashBalIni.AtualizaIniOld();
var
  IniOld: Double;
  Nivel, Genero: Integer;
  Tab: String;
begin
  QrSNG.First;
  while not QrSNG.Eof do
  begin
    case QrSNGNivel.Value of
      1: Tab := 'Cta';
      2: Tab := 'Sgr';
      3: Tab := 'Gru';
      4: Tab := 'Cjt';
      5: Tab := 'Pla';
      else Tab := '???';
    end;
    QrPesq.Close;
    QrPesq.DataBase := Dmod.MyDB;
    QrPesq.SQL.Clear;
    QrPesq.SQL.Add('SELECT SUM(sdo.SdoIni) SdoIni');
    QrPesq.SQL.Add('FROM contas cta');
    QrPesq.SQL.Add('LEFT JOIN contassdo sdo ON cta.Codigo=sdo.Codigo');
    QrPesq.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo');
    QrPesq.SQL.Add('LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo');
    QrPesq.SQL.Add('LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto');
    QrPesq.SQL.Add('LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano');
    QrPesq.SQL.Add('WHERE ' + Tab + '.Codigo=:P0');
    QrPesq.SQL.Add('AND sdo.Entidade=:P1');
    QrPesq.SQL.Add('AND sdo.SdoIni <> 0');
    QrPesq.SQL.Add('ORDER BY cta.Nome');
    QrPesq.Params[00].AsInteger := QrSNGGenero.Value;
    QrPesq.Params[01].AsInteger := FEntidade;
    QrPesq.Open;
    //
    IniOld := QrPesqSdoIni.Value;
    Nivel  := QrSNGNivel.Value;
    Genero := QrSNGGenero.Value;
    //if
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stUpd, 'sdoniveis', False, [
    'iniold'], ['nivel', 'genero'], [IniOld], [Nivel, Genero], False);
    //
    QrSNG.Next;
  end;
  QrSNG.Close;
  QrSNG.DataBase := DModG.MyPID_DB;
  QrSNG.Open;
end;

procedure TFmCashBalIni.BtAcaoClick(Sender: TObject);
begin
  case PageControl1.ActivePageIndex of
    1: MyObjects.MostraPopUpDeBotao(PMAcao1, BtAcao);
    2: MyObjects.MostraPopUpDeBotao(PMAcao2, BtAcao);
    else  Geral.MensagemBox('N�o a��o implementada para a guia selecionada "' +
    TTabSheet(PageControl1.ActivePage).Caption +  '"',
    'Mensagem', MB_OK+MB_ICONINFORMATION);
  end;
end;

procedure TFmCashBalIni.BtAlteraClick(Sender: TObject);
begin
  MostraEdicaoLctPrevia(stUpd, 0);
end;

procedure TFmCashBalIni.BtExcluiClick(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(DModG.QrUpdPID1, QrPrevLct1, DBGPrevLct1,
  FPrevLct1, ['controle'], ['controle'], istPergunta, '');
  //
  Screen.Cursor := crHourGlass;
  try
    ReopenPrevLct1();
    //
    QrCrt.Close;
    QrCrt.DataBase := Dmod.MyDB;
    QrCrt.Open;
    //QrCrt.Locate('Codigo', Carteira, []);
    //
    QrSNG.Close;
    QrSNG.Database := DModG.MyPID_DB;
    QrSNG.Open;
    //QrSNG.Locate('Nivel;Genero', VarArrayOf([Nivel, Genero]),[]);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCashBalIni.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmCashBalIni.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCashBalIni.Colocarcarteiracorretanoslanamentos1Click(
  Sender: TObject);
  procedure CorrigeLinha();
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, VAR_LCT, False, [
    'Tipo', 'CliInt'], ['Controle', 'Sub'], [
    QrLctDvrCAR_TIPO.Value, QrLctDvrCAR_FORNECEI.Value], [
    QrLctDvrControle.Value, QrLctDvrSub.Value], True);
  end;
var
  I: Integer;  
begin
  Screen.Cursor := crHourGlass;
  if DBGLctDvr.SelectedRows.Count > 0 then
  begin
    with DBGLctDvr.DataSource.DataSet do
    for i:= 0 to DBGLctDvr.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(DBGLctDvr.SelectedRows.Items[i]));
      CorrigeLinha();
    end;
  end else CorrigeLinha();
  ReopenDivergencias();
  Screen.Cursor := crDefault;
end;

procedure TFmCashBalIni.Confirmaracriacaodospre_lanamentos1Click(Sender: TObject);
begin
  if RGNiveis1.ItemIndex = 0 then
  begin
    Screen.Cursor := crHourGlass;
    QrPrevLct1.First;
    while not QrPrevLct1.Eof do
    begin
      UFinanceiro.LancamentoDefaultVARS();
      //
      FLAN_Data          := Geral.FDT(QrPrevLct1Data.Value, 1);
      FLAN_Vencimento    := Geral.FDT(QrPrevLct1Data.Value, 1);
      FLAN_DataCad       := Geral.FDT(Date, 1);
      FLAN_Descricao     := QrPrevLct1Descricao.Value;
      FLAN_Compensado    := Geral.FDT(QrPrevLct1Data.Value, 1);
      FLAN_Tipo          := QrPrevLct1Tipo.Value;;
      FLAN_Carteira      := QrPrevLct1Carteira.Value;
      FLAN_Credito       := QrPrevLct1Credito.Value;
      FLAN_Debito        := QrPrevLct1Debito.Value;
      FLAN_Genero        := QrPrevLct1Genero.Value;
      FLAN_Sit           := 2;
      FLAN_Sub           := 0;
      FLAN_UserCad       := VAR_USUARIO;
      FLAN_DataDoc       := Geral.FDT(Date, 1);
      FLAN_CliInt        := QrPrevLct1Entidade.Value;
      FLAN_FatID         := -999999999;
      FLAN_FatID_Sub     := 0;
      FLAN_FatNum        := 0;
      FLAN_FatParcela    := 0;
      FLAN_Controle      := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                            VAR_LCT, VAR_LCT, 'Controle');
      UFinanceiro.InsereLancamento;
      QrPrevLct1.Next;
    end;
    //
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('UPDATE carteiras SET ValIniOld=Inicial, Inicial=0');
    DMod.QrUpd.SQL.Add('WHERE ForneceI=' + FormatFloat('0', FEntidade));
    DMod.QrUpd.SQL.Add('AND Inicial<>0');
    DMod.QrUpd.ExecSQL;
    //
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('UPDATE contassdo SET ValIniOld=SdoIni, SdoIni=0');
    DMod.QrUpd.SQL.Add('WHERE Entidade=' + FormatFloat('0', FEntidade));
    DMod.QrUpd.SQL.Add('AND SdoIni<>0');
    DMod.QrUpd.ExecSQL;
    //
    ReopenTudo();
    Screen.Cursor := crDefault;
  end else Geral.MensagemBox(
  'A inclus�o dos pr�-lan�amentos s� ser� realizada ' + #13#10 +
  'quando for selecionado o N�vel "TODOS"!', 'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmCashBalIni.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCashBalIni.FormCreate(Sender: TObject);
var
  Ano, Mes: Integer;
begin
  Panel10.Height := 222;
  FPrevLct1 := UCriar.GerenciaTempTable('PrevLct1', acCreate, DModG, DModG.MyPID_DB,
    DModG.QrUpdPID1, False);
  //
  PageControl1.ActivePageIndex := 0;
  FEntidade := FmCashBal.QrEmpCodigo.Value;
  Ano := Geral.IMV(FmCashBal.CBAno.Text);
  Mes := FmCashBal.CBMes.ItemIndex + 1;
  EdPeriodo_Txt.Text := FormatFloat('00', Mes) + FormatFloat('0000', Ano);
  {
  Mez := ((Ano - 2000) * 100) + Mes;
  FDataAnt := Geral.FDT(EncodeDate(Ano-1,1,1), 1);
  FDataIni := Geral.FDT(EncodeDate(Ano  ,1,1), 1);
  FPeriodoIni := (Ano - 2000) * 100 + 1;
  FPeriodoFim := (Ano - 2000) * 100 + 12;
  FPeriodoAnt := FPeriodoIni - 100;
  }
  if Mes = 12 then
  begin
    Ano := Ano + 1;
    Mes := 1;
  end else Mes      := Mes + 1;
  FDataSeguinte     := Geral.FDT(EncodeDate(Ano, Mes, 1), 1);
  DsEmpresa.DataSet := FmCashBal.QrEmp;
  QrSNG.DataBase    := DModG.MyPID_DB;
  //
  ReopenTudo();
end;

procedure TFmCashBalIni.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCashBalIni.MostraEdicaoLctPrevia(SQLType: TSQLType; Valor: Double);
var
  Tab: String;
  Carteira, Nivel, Genero: Integer;
begin
  if DBCheck.CriaFm(TFmLctPrevia, FmLctPrevia, afmoSoBoss) then
  begin
    Carteira  := QrCrtCodigo.Value;
    Nivel     := QrSNGNivel.Value;
    Genero    := QrSNGGenero.Value;
    //
    with FmLctPrevia do
    begin
      LaTipo.SQLType := SQLType;
      FEntCod := FEntidade;
      QrCrts.Close;
      QrCrts.Database := Dmod.MyDB;
      QrCrts.Params[0].AsInteger := FEntidade;
      QrCrts.Open;
      //
      case RGNiveis1.ItemIndex of
        1: Tab := 'co';
        2: Tab := 'sg';
        3: Tab := 'gr';
        4: Tab := 'cj';
        5: Tab := 'pl';
        else Tab := '??';
      end;
      QrCtas.Close;
      QrCtas.Database := Dmod.MyDB;
      QrCtas.SQL.Clear;
      QrCtas.SQL.Add('SELECT co.*, pl.Nome NOMEPLANO, sg.Grupo,');
      QrCtas.SQL.Add('sg.Nome NOMESUBGRUPO, gr.Conjunto,');
      QrCtas.SQL.Add('gr.Nome NOMEGRUPO, cj.Plano, cj.Nome NOMECONJUNTO,');
      QrCtas.SQL.Add('IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMEEMPRESA');
      QrCtas.SQL.Add('FROM contas co');
      QrCtas.SQL.Add('LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo');
      QrCtas.SQL.Add('LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo');
      QrCtas.SQL.Add('LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto');
      QrCtas.SQL.Add('LEFT JOIN plano     pl ON pl.Codigo=cj.Plano');
      QrCtas.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=co.Empresa');
      QrCtas.SQL.Add('WHERE co.Terceiro=0');
      QrCtas.SQL.Add('AND co.Codigo>0');
      QrCtas.SQL.Add('AND ' + Tab + '.Codigo=' + FormatFloat('0', QrQLNCodigo.Value));
      QrCtas.SQL.Add('ORDER BY co.Nome');
      QrCtas.Open;
      //
      if SQLType = stIns then
      begin
        //EdControle.ValueVariant  := QrPrevLct1Controle.Value;
        EdCarteira.ValueVariant  := QrCrtCodigo.Value;
        CBCarteira.KeyValue      := QrCrtCodigo.Value;
        EdGenero.ValueVariant    := QrSdoCodigo.Value;
        CBGenero.KeyValue        := QrSdoCodigo.Value;
        //TPData.Date              := QrPrevLct1Data.Value;
        if Valor > 0 then
          EdCredito.ValueVariant   := Valor
        else
          EdDebito.ValueVariant    := - Valor;
        //EdDescricao.ValueVariant := QrPrevLct1Descricao.Value;
      end else
      if SQLType = stUpd then
      begin
        EdControle.ValueVariant  := QrPrevLct1Controle.Value;
        EdCarteira.ValueVariant  := QrPrevLct1Carteira.Value;
        CBCarteira.KeyValue      := QrPrevLct1Carteira.Value;
        EdGenero.ValueVariant    := QrPrevLct1Genero.Value;
        CBGenero.KeyValue        := QrPrevLct1Genero.Value;
        TPData.Date              := QrPrevLct1Data.Value;
        EdCredito.ValueVariant   := QrPrevLct1Credito.Value;
        EdDebito.ValueVariant    := QrPrevLct1Debito.Value;
        EdDescricao.ValueVariant := QrPrevLct1Descricao.Value;
      end;
      //
      ShowModal;
      Destroy;
      //
      ReopenPrevLct1();
    end;
    //
    QrCrt.Close;
    QrCrt.Database := Dmod.MyDB;
    QrCrt.Open;
    QrCrt.Locate('Codigo', Carteira, []);
    //
    QrSNG.Close;
    QrSNG.Database := DModG.MyPID_DB;
    QrSNG.Open;
    QrSNG.Locate('Nivel;Genero', VarArrayOf([Nivel, Genero]),[]);
  end;
end;

procedure TFmCashBalIni.Peladiferencadacarteiradeprecada1Click(Sender: TObject);
begin
  MostraEdicaoLctPrevia(stIns, QrCrtDIFERENCA.Value);
end;

procedure TFmCashBalIni.Pelovalordacontadeprecada1Click(Sender: TObject);
begin
  MostraEdicaoLctPrevia(stIns, QrSdoSdoIni.Value);
end;

procedure TFmCashBalIni.PMAcao1Popup(Sender: TObject);
begin
  Colocarcarteiracorretanoslanamentos1.Enabled := QrLctDvr.RecordCount > 0;
end;

procedure TFmCashBalIni.QrCrtAfterOpen(DataSet: TDataSet);
begin
  QrSCrt.Close;
  QrSCrt.Database := Dmod.MyDB;
  QrSCrt.Params[0] := QrCrt.Params[0];
  QrSCrt.Open;
end;

procedure TFmCashBalIni.QrCrtBeforeClose(DataSet: TDataSet);
begin
  QrSCrt.Close;
end;

procedure TFmCashBalIni.QrCrtCalcFields(DataSet: TDataSet);
begin
  QrOCF_Cart.Close;
  QrOCF_Cart.Database := DModG.MyPID_DB;
  QrOCF_Cart.Params[0].AsInteger := QrCrtCodigo.Value;
  QrOCF_Cart.Open;
  //
  QrCrtDIFERENCA.Value := QrCrtInicial.Value - QrOCF_CartVALOR.Value;
end;

procedure TFmCashBalIni.QrQLNAfterScroll(DataSet: TDataSet);
begin
  ReopenPrevLct1();
end;

procedure TFmCashBalIni.QrSdoAfterOpen(DataSet: TDataSet);
begin
  QrSSdo.Close;
  QrSSdo.Database := Dmod.MyDB;
  QrSSdo.Params[0] := QrSdo.Params[0];
  QrSSdo.Open;
end;

procedure TFmCashBalIni.QrSdoBeforeClose(DataSet: TDataSet);
begin
  QrSSdo.Close;
end;

procedure TFmCashBalIni.QrSNGCalcFields(DataSet: TDataSet);
var
  Tab: String;
begin
  case QrSNGNivel.Value of
    1: Tab := 'Genero';
    2: Tab := 'SubGrupo';
    3: Tab := 'Grupo';
    4: Tab := 'Conjunto';
    5: Tab := 'Plano';
    else Tab := '???';
  end;
  QrOCF_Ctas.Close;
  QrOCF_Ctas.Database := DModG.MyPID_DB;
  QrOCF_Ctas.SQL.Clear;
  QrOCF_Ctas.SQL.Add('SELECT SUM(Credito-Debito) VALOR');
  QrOCF_Ctas.SQL.Add('FROM prevlct1');
  QrOCF_Ctas.SQL.Add('WHERE '+Tab+'=:P0');
  QrOCF_Ctas.Params[00].AsInteger := QrSNGGenero.Value;
  QrOCF_Ctas.Open;
  //
  QrSNGDIFERENCA.Value := QrSNGIniOld.Value - QrOCF_CtasVALOR.Value;
end;

procedure TFmCashBalIni.ReopenTudo;
begin
  QrCrt.Close;
  QrCrt.Database := Dmod.MyDB;
  QrCrt.Params[0].AsInteger := FEntidade;
  QrCrt.Open;
  //
  QrSdo.Close;
  QrSdo.Database := Dmod.MyDB;
  QrSdo.Params[0].AsInteger := FEntidade;
  QrSdo.Open;
  //
  ReopenDivergencias();
  //
  ReopenQLN();
  //
  ReopenPrevLct1();
  //
  ReopenSdoNew();
end;

procedure TFmCashBalIni.ReopenDivergencias;
var
  Valor: Double;
begin
  QrDivCart.Close;
  QrDivCart.Database := Dmod.MyDB;
  QrDivCart.Params[00].AsInteger := FEntidade;
  QrDivCart.Params[01].AsString  := FDataSeguinte;
  QrDivCart.Open;
  Valor := 0;
  QrDivCart.First;
  while not QrDivCart.Eof do
  begin
    Valor := Valor + QrDivCartSaldo.Value;
    QrDivCart.Next;
  end;
  EdDivCart.ValueVariant := Valor;
  //
  QrDivLan.Close;
  QrDivLan.Database := Dmod.MyDB;
  QrDivLan.Params[00].AsInteger := FEntidade;
  QrDivLan.Params[01].AsString  := FDataSeguinte;
  QrDivLan.Open;
  Valor := 0;
  QrDivLan.First;
  while not QrDivLan.Eof do
  begin
    Valor := Valor + QrDivLanSaldo.Value;
    QrDivLan.Next;
  end;
  EdDivLan.ValueVariant := Valor;
  //
  QrLctDvr.Close;
  QrLctDvr.Database := Dmod.MyDB;
  QrLctDvr.Params[00].AsInteger := FEntidade;
  QrLctDvr.Open;
  //
  LaDiverge.Visible := (QrLctDvr.RecordCount > 0) or 
    (EdDivCart.ValueVariant <> EdDivLan.ValueVariant);
  QrDivCart.First;
  QrDivLan.First;
end;

procedure TFmCashBalIni.ReopenPrevLct1();
var
  Tab: String;
begin
  case RGNiveis1.ItemIndex of
    1: Tab := 'Genero';
    2: Tab := 'SubGrupo';
    3: Tab := 'Grupo';
    4: Tab := 'Conjunto';
    5: Tab := 'Plano';
    else Tab := '???';
  end;
  QrPrevLct1.Close;
  QrPrevLct1.DataBase := DModG.MyPID_DB;
  QrPrevLct1.SQL.Clear;
  QrPrevLct1.SQL.Add('SELECT * FROM prevlct1');
  QrPrevLct1.SQL.Add('WHERE Entidade=' + FormatFloat('0', FEntidade));
  if RGNiveis1.ItemIndex > 0 then
  begin
    QrPrevLct1.SQL.Add('AND ' + Tab + '=:P0');
    QrPrevLct1.Params[00].AsInteger := QrQLNCodigo.Value;
  end;
  QrPrevLct1.Open;
end;

procedure TFmCashBalIni.ReopenQLN();
var
  Tab: String;
begin
  QrQLN.Close;
  QrQLN.Database := Dmod.MyDB;
  if RGNiveis1.ItemIndex > 0 then
  begin
    case RGNiveis1.ItemIndex of
      1: Tab := 'cta';
      2: Tab := 'sgr';
      3: Tab := 'gru';
      4: Tab := 'cjt';
      5: Tab := 'pla';
      else Tab := '???';
    end;
    QrQLN.SQL.Clear;
    QrQLN.SQL.Add('SELECT '+Tab+'.Codigo, '+Tab+'.Nome,');
    QrQLN.SQL.Add('COUNT(lan.Genero) Lanctos');
    QrQLN.SQL.Add('FROM lanctos lan');
    QrQLN.SQL.Add('LEFT JOIN contas    cta ON cta.Codigo=lan.Genero');
    QrQLN.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=cta.Subgrupo');
    QrQLN.SQL.Add('LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo');
    QrQLN.SQL.Add('LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto');
    QrQLN.SQL.Add('LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano');
    QrQLN.SQL.Add('WHERE lan.Tipo <> 2');
    QrQLN.SQL.Add('AND lan.Genero > 0');
    QrQLN.SQL.Add('AND lan.CliInt=:P0');
    QrQLN.SQL.Add('GROUP BY '+Tab+'.Codigo');
    QrQLN.SQL.Add('ORDER BY '+Tab+'.Codigo');
    QrQLN.Params[0].AsInteger := FEntidade;
    QrQLN.Open;
  end;    
end;

procedure TFmCashBalIni.ReopenSdoNew();
begin
  QrSdoNew.Close;
  QrSdoNew.Database := Dmod.MyDB;
  QrSdoNew.Params[0].AsInteger := FEntidade;
  QrSdoNew.Open;
end;

procedure TFmCashBalIni.Revertersaldosiniciais1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add(DELETE_FROM + LAN_CTOS);
  Dmod.QrUpd.SQL.Add('WHERE FatID = -999999999');
  Dmod.QrUpd.SQL.Add('AND CliInt=:P0;');
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.SQL.Add('UPDATE carteiras SET Inicial=ValIniOld, ValIniOld=0');
  Dmod.QrUpd.SQL.Add('WHERE ForneceI=:P0 AND ValIniOld <> 0;');
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.SQL.Add('UPDATE contassdo SET SdoIni=ValIniOld, ValIniOld=0');
  Dmod.QrUpd.SQL.Add('WHERE Entidade=:P0 AND ValIniOld <> 0;');
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.Params[00].AsInteger := FmCashBal.QrEmpCodigo.Value;
  Dmod.QrUpd.Params[01].AsInteger := FmCashBal.QrEmpCodigo.Value;
  Dmod.QrUpd.Params[02].AsInteger := FmCashBal.QrEmpCodigo.Value;
  Dmod.QrUpd.ExecSQL;
  //
  ReopenTudo();
  //
  Screen.Cursor := crDefault;
end;

procedure TFmCashBalIni.RGNiveis1Click(Sender: TObject);
begin
  BtInclui.Enabled := RGNiveis1.ItemIndex > 0;
  BtAltera.Enabled := RGNiveis1.ItemIndex > 0;
  ReopenQLN();
  if RGNiveis1.ItemIndex = 0 then
    ReopenPrevLct1();
end;

(*
SELECT Entidade, COUNT(DISTINCT(Nivel)) QtdNiveis, COUNT(Genero) QtdCtas
FROM contasniv
GROUP BY Entidade
ORDER BY QtdNiveis DESC
*)

(*
Excluir prevlct1 quando gerado lancto
*)

end.
