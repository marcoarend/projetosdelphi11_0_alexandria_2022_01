unit EmiChPertoChek;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, dmkGeral, dmkEdit,
  DBCtrls, DB, mySQLDbTables, dmkDBLookupComboBox, dmkEditCB, Grids, DBGrids,
  UnDmkEnums;

type
  TFmEmiChPertoChek = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Label3: TLabel;
    ST_Banco: TStaticText;
    Label4: TLabel;
    ST_Agencia: TStaticText;
    ST_Conta: TStaticText;
    Label6: TLabel;
    ST_CMC7: TStaticText;
    Label7: TLabel;
    ST_Cheque: TStaticText;
    ST_Praca: TStaticText;
    Label8: TLabel;
    Label9: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    LaValor: TLabel;
    Label5: TLabel;
    Label2: TLabel;
    TPData: TDateTimePicker;
    EdValor: TdmkEdit;
    EdBenef: TdmkEdit;
    EdCidade: TdmkEdit;
    RGForma: TRadioGroup;
    Edit1: TEdit;
    Edit2: TEdit;
    QrBancos: TmySQLQuery;
    QrBancosCodigo: TIntegerField;
    QrBancosNome: TWideStringField;
    QrBancosSite: TWideStringField;
    QrBancosLk: TIntegerField;
    QrBancosDataCad: TDateField;
    QrBancosDataAlt: TDateField;
    QrBancosUserCad: TIntegerField;
    QrBancosUserAlt: TIntegerField;
    QrBancosDVCC: TSmallintField;
    DsBancos: TDataSource;
    Label11: TLabel;
    EdBanco: TdmkEditCB;
    CBBanco: TdmkDBLookupComboBox;
    Timer1: TTimer;
    QrImpCHMsg: TmySQLQuery;
    DBGrid1: TDBGrid;
    DsImpCHMsg: TDataSource;
    QrImpCHMsgAgora: TDateTimeField;
    QrImpCHMsgImpressora: TSmallintField;
    QrImpCHMsgCodErr: TWideStringField;
    QrImpCHMsgCodMsg: TWideStringField;
    QrImpCHMsgTipMsg: TSmallintField;
    QrImpCHMsgMENSAGEM_ERRO: TWideStringField;
    Label10: TLabel;
    QrImpCHMsgValor: TFloatField;
    QrImpCHMsgBenefic: TWideStringField;
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdValorExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure QrImpCHMsgCalcFields(DataSet: TDataSet);
    procedure QrImpCHMsgAfterScroll(DataSet: TDataSet);
    procedure QrImpCHMsgAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FTabela: String;
    FControle: Double;
  end;

  var
  FmEmiChPertoChek: TFmEmiChPertoChek;

implementation

uses UnMyObjects, PertoChek, ModuleGeral, UMySQLModule, Module;

{$R *.DFM}

procedure TFmEmiChPertoChek.BtOKClick(Sender: TObject);
var
  Agora, Data: String;
  Valor: Double;
  Banco: Integer;
begin
  Valor := EdValor.ValueVariant;
  if Valor <= 0 then
  begin
    Application.MessageBox('Informe um valor v�lido!', 'Erro', MB_OK+MB_ICONERROR);
    EdValor.SetFocus;
    Exit;
  end;
  if EdCidade.Text = '' then
  begin
    Application.MessageBox('Informe a cidade!', 'Erro', MB_OK+MB_ICONERROR);
    EdCidade.SetFocus;
    Exit;
  end;
  Banco := Geral.IMV(EdBanco.Text);
  if Banco = 0 then
  begin
    Application.MessageBox('Informe o c�digo do banco!', 'Erro', MB_OK+MB_ICONERROR);
    EdBanco.SetFocus;
    Exit;
  end;
  //
  Agora := Geral.FDT(DModG.ObtemAgora(), 105);
  Data  := Geral.FDT(TPData.Date, 1);
  if UMyMod.SQLInsUpd(DmodG.QrUpdL, stIns, 'impch', False, [
  'Agora', 'Data', 'Cidade',
  'Nome', 'Valor', 'Banco',
  'Forma'], [
  'Tabela', 'Controle'], [
  Agora, Data, EdCidade.Text,
  EdBenef.Text, Valor, EdBanco.ValueVariant,
  RGForma.ItemIndex], [
  FTabela, FControle], True) then
  begin
    Label10.Caption := 'Dados enviados. Aguardando retorno...';
    Timer1.Enabled := True;
  end;  
  {
  Panel1.Visible := False;
  ST_Banco.Caption   := '';
  ST_Agencia.Caption := '';
  ST_Conta.Caption   := '';
  ST_Cheque.Caption  := '';
  ST_Praca.Caption   := '';
  ST_CMC7.Caption    := '';
  Valor := Geral.CompletaString(Geral.SoNumero_TT(EdValor.Text),
    '0', 12, taRightJustify, False);
  Valor := RGForma.Items[RGForma.ItemIndex][1]+Valor;
  Cidade := Geral.SemAcento(Geral.Maiusculas(EdCidade.Text, False));
  Nome := Geral.SemAcento(Geral.Maiusculas(EdBenef.Text, False));
  Data := FormatDateTime('ddmmyy', TPData.Date);
  //
  if Nome = '' then Nome := '     ';
  if Geral.DMV(EdValor.Text) <= 0 then
  begin
    Application.MessageBox('Informe um valor v�lido!', 'Erro', MB_OK+MB_ICONERROR);
    EdValor.SetFocus;
    Exit;
  end;
  if EdCidade.Text = '' then
  begin
    Application.MessageBox('Informe a cidade!', 'Erro', MB_OK+MB_ICONERROR);
    EdCidade.SetFocus;
    Exit;
  end;
  //PertoChekP.HabilitaPertoCheck;
  PertoChekP.EnviaPertoChek2('!' + Data, EdMens);
  PertoChekP.EnviaPertoChek2('#' + Cidade, EdMens);
  PertoChekP.EnviaPertoChek2('%' + Nome, EdMens);
  if RGForma.ItemIndex in ([2,3,6,7,10,11,14,15]) then
  begin
    Edit2.Text := PertoChekP.EnviaPertoChek2('=', EdMens);
    Edit1.Text := PertoChekP.EnviaPertoChek2('.', EdMens);
    Panel1.Visible := True;
    PertoChekP.EnviaPertoChek2(';' + Valor, EdMens);
  end else begin
    Num := PertoChekP.EnviaPertoChek2(Char(039), EdMens);
    Num := Copy(Num, 8, 3);
    Num := InputBox('Impress�o de cheque - Pertochek',
    'Informe os tr�s digitos do banco.', Num);
    PertoChekP.EnviaPertoChek2('$' + Valor+Num, EdMens);
    //PertoChekP.DesabilitaPertoCheck;
  end;
  //
  }
end;

procedure TFmEmiChPertoChek.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEmiChPertoChek.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEmiChPertoChek.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmEmiChPertoChek.QrImpCHMsgAfterOpen(DataSet: TDataSet);
begin
  if QrImpCHMsg.RecordCount > 0 then
  begin
    Label10.Caption := 'Retorno recebido!!';
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmEmiChPertoChek.QrImpCHMsgAfterScroll(DataSet: TDataSet);
var
  i: Integer;
  CMC7: String;
  Erro: Integer;
begin
  ST_Banco.Caption   := '';
  ST_Agencia.Caption := '';
  ST_Conta.Caption   := '';
  ST_Cheque.Caption  := '';
  ST_Praca.Caption   := '';
  ST_CMC7.Caption    := '';
  i := Length(QrImpCHMsgCodMsg.Value);
  if i >= 34 then
  begin
    CMC7 := Char(123);
    for i := 34 downto 3 do
    begin
      case i of
        26: CMC7 := CMC7 + Char(123) + ' ';
        15: CMC7 := CMC7 + Char(125) + ' ';
        else CMC7 := CMC7 + QrImpCHMsgCodMsg.Value[i];
      end;
    end;
    CMC7 := CMC7 + Char(91);
    ST_CMC7.Caption := CMC7;
  end;
  i := Length(QrImpCHMsgCodMsg.Value);
  if i = 30 then
  begin
    Erro := StrToInt(Copy(QrImpCHMsgCodMsg.Value, 2,3));
    if Erro = 0 then
    begin
      ST_Banco.Caption   := Copy(QrImpCHMsgCodMsg.Value, 05,03);
      ST_Agencia.Caption := Copy(QrImpCHMsgCodMsg.Value, 08,04);
      ST_Conta.Caption   := Copy(QrImpCHMsgCodMsg.Value, 12,10);
      ST_Cheque.Caption  := Copy(QrImpCHMsgCodMsg.Value, 22,06);
      ST_Praca.Caption   := Copy(QrImpCHMsgCodMsg.Value, 28,03);
    end else begin
      ST_Banco.Caption   := '';
      ST_Agencia.Caption := '';
      ST_Conta.Caption   := '';
      ST_Cheque.Caption  := '';
      ST_Praca.Caption   := '';
    end;
  end;
end;

procedure TFmEmiChPertoChek.QrImpCHMsgCalcFields(DataSet: TDataSet);
begin
  QrImpCHMsgMENSAGEM_ERRO.Value := PertoChekP.ErroPertoChek(
    QrImpCHMsgCodErr.Value, False, False);
end;

procedure TFmEmiChPertoChek.Timer1Timer(Sender: TObject);
begin
  QrImpCHMsg.Close;
  QrImpCHMsg.Params[00].AsString  := FTabela;
  QrImpCHMsg.Params[01].AsFloat   := FControle;
  QrImpCHMsg.Open;
  //
end;

procedure TFmEmiChPertoChek.FormCreate(Sender: TObject);
begin
  QrBancos.Open;
  TPData.Date := Date;
  ST_CMC7.Caption := '';
  ST_Banco.Caption := '';
  ST_Agencia.Caption := '';
  ST_Conta.Caption := '';
  ST_Cheque.Caption := '';
  ST_Praca.Caption := '';
  Edit1.Text := '';
  RGForma.ItemIndex := Geral.ReadAppKey('Cheque\Pertochek\', Application.Title,
    ktInteger, 0, HKEY_LOCAL_MACHINE);
end;

procedure TFmEmiChPertoChek.EdValorExit(Sender: TObject);
begin
  EdValor.Text := Geral.TFT(EdValor.Text, 2, siPositivo);
end;

procedure TFmEmiChPertoChek.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Geral.WriteAppKey('Cheque\Pertochek\', Application.Title,
    RGForma.ItemIndex, ktInteger, HKEY_LOCAL_MACHINE);
end;

end.
