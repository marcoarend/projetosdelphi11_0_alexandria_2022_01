object FmCartPgt2: TFmCartPgt2
  Left = 340
  Top = 178
  Caption = 'FIN-PGTOS-002 :: Pagamentos de Lan'#231'amentos'
  ClientHeight = 617
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 161
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label2: TLabel
      Left = 8
      Top = 42
      Width = 31
      Height = 13
      Caption = 'Conta:'
    end
    object Label3: TLabel
      Left = 297
      Top = 42
      Width = 52
      Height = 13
      Caption = 'Sub-grupo:'
    end
    object Label4: TLabel
      Left = 8
      Top = 82
      Width = 32
      Height = 13
      Caption = 'Grupo:'
    end
    object Label7: TLabel
      Left = 296
      Top = 82
      Width = 45
      Height = 13
      Caption = 'Conjunto:'
    end
    object Label1: TLabel
      Left = 8
      Top = 6
      Width = 62
      Height = 13
      Caption = 'Lan'#231'amento:'
    end
    object Label5: TLabel
      Left = 104
      Top = 6
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object LaControle: TLabel
      Left = 0
      Top = 24
      Width = 6
      Height = 13
      Caption = '0'
      Visible = False
    end
    object Label6: TLabel
      Left = 580
      Top = 82
      Width = 44
      Height = 13
      Caption = 'Empresa:'
    end
    object LaCred: TLabel
      Left = 684
      Top = 6
      Width = 92
      Height = 13
      Caption = 'Cr'#233'dito documento:'
    end
    object LaDeb: TLabel
      Left = 580
      Top = 6
      Width = 90
      Height = 13
      Caption = 'D'#233'bito documento:'
    end
    object Label8: TLabel
      Left = 580
      Top = 42
      Width = 95
      Height = 13
      Caption = 'D'#233'bito pagamentos:'
    end
    object Label9: TLabel
      Left = 684
      Top = 42
      Width = 97
      Height = 13
      Caption = 'Cr'#233'dito pagamentos:'
    end
    object Label12: TLabel
      Left = 8
      Top = 122
      Width = 42
      Height = 13
      Caption = 'Entidade'
    end
    object Label13: TLabel
      Left = 400
      Top = 122
      Width = 59
      Height = 13
      Caption = 'Vencimento:'
    end
    object Label14: TLabel
      Left = 316
      Top = 122
      Width = 42
      Height = 13
      Caption = 'Emiss'#227'o:'
    end
    object Label15: TLabel
      Left = 484
      Top = 122
      Width = 47
      Height = 13
      Caption = 'dd atraso:'
    end
    object Label16: TLabel
      Left = 540
      Top = 122
      Width = 42
      Height = 13
      Caption = 'Mora dd:'
      Visible = False
    end
    object Label17: TLabel
      Left = 623
      Top = 122
      Width = 52
      Height = 13
      Caption = 'Valor juros:'
      Visible = False
    end
    object Label18: TLabel
      Left = 706
      Top = 122
      Width = 29
      Height = 13
      Caption = 'Multa:'
      Visible = False
    end
    object Label21: TLabel
      Left = 76
      Top = 6
      Width = 22
      Height = 13
      Caption = 'Sub:'
    end
    object DBEdit1: TDBEdit
      Left = 8
      Top = 56
      Width = 284
      Height = 21
      TabStop = False
      Color = clInactiveCaption
      DataField = 'NOMECONTA'
      DataSource = DataSource1
      ReadOnly = True
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 297
      Top = 56
      Width = 280
      Height = 21
      TabStop = False
      Color = clInactiveCaption
      DataField = 'NOMESUBGRUPO'
      DataSource = DataSource1
      ReadOnly = True
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Left = 8
      Top = 96
      Width = 284
      Height = 21
      TabStop = False
      Color = clInactiveCaption
      DataField = 'NOMEGRUPO'
      DataSource = DataSource1
      ReadOnly = True
      TabOrder = 2
    end
    object DBEdit5: TDBEdit
      Left = 580
      Top = 96
      Width = 207
      Height = 21
      TabStop = False
      Color = clInactiveCaption
      DataField = 'NOMEEMPRESA'
      DataSource = DataSource1
      ReadOnly = True
      TabOrder = 3
    end
    object DBEdit4: TDBEdit
      Left = 296
      Top = 96
      Width = 280
      Height = 21
      TabStop = False
      Color = clInactiveCaption
      DataField = 'NOMECONJUNTO'
      DataSource = DataSource1
      ReadOnly = True
      TabOrder = 4
    end
    object EdControle: TDBEdit
      Left = 8
      Top = 20
      Width = 65
      Height = 21
      TabStop = False
      Color = clInactiveCaption
      DataField = 'Controle'
      DataSource = DataSource1
      ReadOnly = True
      TabOrder = 5
    end
    object DBEdit7: TDBEdit
      Left = 104
      Top = 20
      Width = 473
      Height = 21
      TabStop = False
      Color = clInactiveCaption
      DataField = 'Descricao'
      DataSource = DataSource1
      ReadOnly = True
      TabOrder = 6
    end
    object EdDeb: TdmkEdit
      Left = 580
      Top = 56
      Width = 101
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Color = clInactiveCaption
      ReadOnly = True
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object EdCred: TdmkEdit
      Left = 684
      Top = 56
      Width = 101
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Color = clInactiveCaption
      ReadOnly = True
      TabOrder = 8
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object DBEdit8: TDBEdit
      Left = 580
      Top = 20
      Width = 101
      Height = 21
      TabStop = False
      Color = clInactiveCaption
      DataField = 'Debito'
      DataSource = DataSource1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 9
    end
    object DBEdit9: TDBEdit
      Left = 684
      Top = 20
      Width = 101
      Height = 21
      TabStop = False
      Color = clInactiveCaption
      DataField = 'Credito'
      DataSource = DataSource1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 10
    end
    object DBEdit10: TDBEdit
      Left = 8
      Top = 136
      Width = 305
      Height = 21
      TabStop = False
      Color = clInactiveCaption
      DataField = 'NOMERELACIONADO'
      DataSource = DataSource1
      ReadOnly = True
      TabOrder = 11
    end
    object DBEdit11: TDBEdit
      Left = 400
      Top = 136
      Width = 81
      Height = 21
      TabStop = False
      Color = clInactiveCaption
      DataField = 'Vencimento'
      DataSource = DataSource1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 12
    end
    object DBEdit12: TDBEdit
      Left = 316
      Top = 136
      Width = 81
      Height = 21
      TabStop = False
      Color = clInactiveCaption
      DataField = 'Data'
      DataSource = DataSource1
      ReadOnly = True
      TabOrder = 13
    end
    object DBEdit13: TDBEdit
      Left = 484
      Top = 136
      Width = 53
      Height = 21
      TabStop = False
      Color = clInactiveCaption
      DataField = 'ATRASO'
      DataSource = DataSource1
      ReadOnly = True
      TabOrder = 14
    end
    object DBEdit14: TDBEdit
      Left = 540
      Top = 136
      Width = 80
      Height = 21
      TabStop = False
      Color = clInactiveCaption
      DataField = 'MoraDia'
      DataSource = DataSource1
      ReadOnly = True
      TabOrder = 15
      Visible = False
    end
    object DBEdit15: TDBEdit
      Left = 623
      Top = 136
      Width = 80
      Height = 21
      TabStop = False
      Color = clInactiveCaption
      DataField = 'JUROS'
      DataSource = DataSource1
      ReadOnly = True
      TabOrder = 16
      Visible = False
    end
    object DBEdit16: TDBEdit
      Left = 706
      Top = 136
      Width = 80
      Height = 21
      TabStop = False
      Color = clInactiveCaption
      DataField = 'Multa'
      DataSource = DataSource1
      ReadOnly = True
      TabOrder = 17
      Visible = False
    end
    object DBEdit6: TDBEdit
      Left = 76
      Top = 20
      Width = 25
      Height = 21
      TabStop = False
      Color = clInactiveCaption
      DataField = 'Sub'
      DataSource = DataSource1
      ReadOnly = True
      TabOrder = 18
    end
  end
  object DBGPagtos: TDBGrid
    Left = 0
    Top = 306
    Width = 792
    Height = 203
    Align = alClient
    DataSource = DsPagtos
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDrawColumnCell = DBGPagtosDrawColumnCell
    Columns = <
      item
        Expanded = False
        FieldName = 'Data'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Documento'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Descricao'
        Title.Caption = 'Descri'#231#227'o'
        Width = 178
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Debito'
        Title.Caption = 'D'#233'bito'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Credito'
        Title.Caption = 'Cr'#233'dito'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Vencimento'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Compensado'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Mes'
        Title.Caption = 'M'#234's'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMESIT'
        Title.Caption = 'Situa'#231#227'o'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Controle'
        Title.Caption = 'Lan'#231'amento'
        Visible = True
      end>
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 696
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 360
        Height = 32
        Caption = 'Pagamentos de Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 360
        Height = 32
        Caption = 'Pagamentos de Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 360
        Height = 32
        Caption = 'Pagamentos de Lan'#231'amentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 509
    Width = 792
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 553
    Width = 792
    Height = 64
    Align = alBottom
    TabOrder = 4
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label10: TLabel
        Left = 308
        Top = 6
        Width = 65
        Height = 13
        Caption = 'Saldo cr'#233'dito:'
      end
      object Label11: TLabel
        Left = 420
        Top = 6
        Width = 62
        Height = 13
        Caption = 'Saldo d'#233'bito:'
      end
      object PnSaiDesis: TPanel
        Left = 644
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 54
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 8
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Inclui novo banco'
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtIncluiClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 104
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Exclui banco atual'
        Caption = 'Excl&ui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtExcluiClick
      end
      object EdSaldoCred: TdmkEdit
        Left = 308
        Top = 21
        Width = 101
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdSaldoDeb: TdmkEdit
        Left = 420
        Top = 21
        Width = 101
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 200
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Altera banco atual'
        Caption = '&Altera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        Visible = False
        OnClick = BtAlteraClick
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 209
    Width = 792
    Height = 97
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 5
    Visible = False
    object LaDescoVal: TLabel
      Left = 264
      Top = 7
      Width = 52
      Height = 13
      Caption = 'Valor juros:'
    end
    object LaDescoPor: TLabel
      Left = 8
      Top = 7
      Width = 110
      Height = 13
      Caption = 'Fatura descontada por:'
    end
    object Label19: TLabel
      Left = 8
      Top = 47
      Width = 124
      Height = 13
      Caption = 'Criar desconto na carteira:'
    end
    object Label20: TLabel
      Left = 264
      Top = 46
      Width = 26
      Height = 13
      Caption = 'Data:'
    end
    object BtPagtosAltera: TBitBtn
      Tag = 240
      Left = 394
      Top = 26
      Width = 120
      Height = 40
      Cursor = crHandPoint
      Hint = 'Inclui novo banco'
      Caption = '&Desconta'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtPagtosAlteraClick
    end
    object EdDescoVal: TdmkEdit
      Left = 264
      Top = 23
      Width = 112
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object CBDescoPor: TdmkDBLookupComboBox
      Left = 56
      Top = 23
      Width = 204
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'NOMEENTIDADE'
      ListSource = DsDescoPor
      TabOrder = 2
      dmkEditCB = EdDescoPor
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdDescoPor: TdmkEditCB
      Left = 8
      Top = 23
      Width = 45
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBDescoPor
      IgnoraDBLookupComboBox = False
    end
    object EdCarteiraDesco: TdmkEditCB
      Left = 8
      Top = 63
      Width = 45
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCarteiraDesco
      IgnoraDBLookupComboBox = False
    end
    object CBCarteiraDesco: TdmkDBLookupComboBox
      Left = 56
      Top = 63
      Width = 204
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCarteiras
      TabOrder = 5
      dmkEditCB = EdCarteiraDesco
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object TPData: TdmkEditDateTimePicker
      Left = 264
      Top = 62
      Width = 112
      Height = 21
      Date = 38680.426625787000000000
      Time = 38680.426625787000000000
      TabOrder = 6
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
  end
  object QrPagtos: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPagtosAfterOpen
    AfterScroll = QrPagtosAfterScroll
    OnCalcFields = QrPagtosCalcFields
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2, '
      '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, '
      ' la.*'
      'FROM lanctos la'
      'WHERE la.ID_Pgto=:P0'
      'ORDER BY la.Data, la.Controle')
    Left = 216
    Top = 269
    ParamData = <
      item
        DataType = ftFloat
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPagtosData: TDateField
      FieldName = 'Data'
      Origin = 'DBMMONEY.lanctos.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPagtosTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'DBMMONEY.lanctos.Tipo'
    end
    object QrPagtosCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'DBMMONEY.lanctos.Carteira'
    end
    object QrPagtosAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      Origin = 'DBMMONEY.lanctos.Autorizacao'
    end
    object QrPagtosGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'DBMMONEY.lanctos.Genero'
    end
    object QrPagtosDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'DBMMONEY.lanctos.Descricao'
      Size = 128
    end
    object QrPagtosNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'DBMMONEY.lanctos.NotaFiscal'
    end
    object QrPagtosDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'DBMMONEY.lanctos.Debito'
      DisplayFormat = '###,###,##0.00;-###,###,##0.00; '
    end
    object QrPagtosCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'DBMMONEY.lanctos.Credito'
      DisplayFormat = '###,###,##0.00;-###,###,##0.00; '
    end
    object QrPagtosCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'DBMMONEY.lanctos.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPagtosDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'DBMMONEY.lanctos.Documento'
      DisplayFormat = '000000; ; '
    end
    object QrPagtosSit: TIntegerField
      FieldName = 'Sit'
      Origin = 'DBMMONEY.lanctos.Sit'
    end
    object QrPagtosVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'DBMMONEY.lanctos.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPagtosLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMMONEY.lanctos.Lk'
    end
    object QrPagtosFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'DBMMONEY.lanctos.FatID'
    end
    object QrPagtosFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'DBMMONEY.lanctos.FatParcela'
    end
    object QrPagtosNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrPagtosAno: TFloatField
      FieldName = 'Ano'
    end
    object QrPagtosMENSAL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 15
      Calculated = True
    end
    object QrPagtosMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Size = 15
      Calculated = True
    end
    object QrPagtosMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrPagtosMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrPagtosSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrPagtosCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrPagtosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPagtosID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrPagtosMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrPagtosSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrPagtosMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrPagtosMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
  end
  object DsPagtos: TDataSource
    DataSet = QrPagtos
    Left = 252
    Top = 269
  end
  object QrSoma: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Credito) Credito, SUM(Debito) Debito'
      'FROM lanctos')
    Left = 288
    Top = 269
    object QrSomaCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'DBMMONEY.lanctos.Credito'
    end
    object QrSomaDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'DBMMONEY.lanctos.Debito'
    end
    object QrSomaMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrSomaJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrSomaDesconto: TFloatField
      FieldName = 'Desconto'
    end
  end
  object DataSource1: TDataSource
    Left = 12
    Top = 5
  end
  object QrDescoPor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Account,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece1="V"'
      'OR Fornece2="V"'
      'OR Fornece3="V"'
      'OR Fornece2="V"'
      'ORDER BY NomeENTIDADE')
    Left = 168
    Top = 148
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object StringField3: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object IntegerField4: TIntegerField
      FieldName = 'Account'
      Required = True
    end
  end
  object DsDescoPor: TDataSource
    DataSet = QrDescoPor
    Left = 196
    Top = 148
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Tipo, ForneceI'
      'FROM carteiras ')
    Left = 168
    Top = 212
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.carteiras.Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.carteiras.Nome'
      FixedChar = True
      Size = 18
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 196
    Top = 212
  end
end
