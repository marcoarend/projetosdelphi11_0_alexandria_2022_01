unit CartNiv2;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, frxClass,
  frxDBSet, UnDmkProcFunc, UnDmkEnums;

type
  TFmCartNiv2 = class(TForm)
    PnDados: TPanel;
    DsCartNiv2: TDataSource;
    QrCartNiv2: TmySQLQuery;
    PnEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label8: TLabel;
    EdCodUsu: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtSeleciona: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrCartNiv2Codigo: TIntegerField;
    QrCartNiv2CodUsu: TIntegerField;
    QrCartNiv2Nome: TWideStringField;
    QrLcts: TmySQLQuery;
    QrLctsCarteira: TIntegerField;
    QrLctsControle: TIntegerField;
    QrLctsGenero: TIntegerField;
    QrLctsDescricao: TWideStringField;
    QrLctsNotaFiscal: TIntegerField;
    QrLctsDebito: TFloatField;
    QrLctsCredito: TFloatField;
    QrLctsVencimento: TDateField;
    QrLctsFornecedor: TIntegerField;
    QrLctsCliente: TIntegerField;
    QrLctsCliInt: TIntegerField;
    QrLctsVendedor: TIntegerField;
    QrLctsDuplicata: TWideStringField;
    frxFIN_CARTE_004_A: TfrxReport;
    frxDsLcts: TfrxDBDataset;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCartNiv2AfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCartNiv2BeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxFIN_CARTE_004_AGetValue(const VarName: string;
      var Value: Variant);
  private
    FDataRel: TDateTime;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmCartNiv2: TFmCartNiv2;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, ModuleFin, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCartNiv2.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCartNiv2.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCartNiv2Codigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCartNiv2.DefParams;
begin
  VAR_GOTOTABELA := 'cartniv2';
  VAR_GOTOMYSQLTABLE := QrCartNiv2;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT Codigo, CodUsu, Nome');
  VAR_SQLx.Add('FROM cartniv2');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmCartNiv2.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'cartniv2', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmCartNiv2.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      GBCntrl.Visible := True;
      GBDados.Visible := True;
      PnEdita.Visible := False;
    end;
    1:
    begin
      PnEdita.Visible := True;
      GBDados.Visible := False;
      GBCntrl.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCartNiv2.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCartNiv2.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCartNiv2.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCartNiv2.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCartNiv2.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCartNiv2.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCartNiv2.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCartNiv2.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCartNiv2.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrCartNiv2, [PnDados],
  [PnEdita], EdCodUsu, ImgTipo, 'cartniv2');
end;

procedure TFmCartNiv2.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCartNiv2Codigo.Value;
  Close;
end;

procedure TFmCartNiv2.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if MyObjects.FIC(Trim(EdNome.Text) = '', EdNome, 'Defina uma descri��o!') then
    Exit;
  //
  Codigo := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'cartniv2', 'Codigo', [], [],
    ImgTipo.SQLType, EdCodigo.ValueVariant, siPositivo, EdCodigo);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmCartNiv2, GBEdita,
  'cartniv2', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmCartNiv2.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'cartniv2', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'cartniv2', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'cartniv2', 'Codigo');
end;

procedure TFmCartNiv2.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, GBEdita, QrCartNiv2, [PnDados],
  [PnEdita], EdCodUsu, ImgTipo, 'cartniv2');
end;

procedure TFmCartNiv2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmCartNiv2.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCartNiv2Codigo.Value, LaRegistro.Caption);
end;

procedure TFmCartNiv2.SbImprimeClick(Sender: TObject);
{
var
  DtTxt, TabLctA: String;
}
begin
{ Ver como fazer TabLctA!
  if DBCheck.ObtemData(Date - 1, FDataRel, 0) then
  begin
    TabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, CliInt);
    //
    DtTxt := Geral.FDT(FDataRel, 1);
    UnDmkDAC_PF.AbreMySQLQuery1(QrLcts, [
    'SELECT Carteira, Controle, Genero, Descricao, ',
    'NotaFiscal, Debito, Credito, Vencimento, Fornecedor, ',
    'Cliente, CliInt, Vendedor, Duplicata, IF(Debito >= 0.01, ',
    'Fornecedor, Cliente) TERCEIRO',
    'FROM ' + TabLctA,
    'WHERE Data="' + DtTxt + '"',
    '']);
    //
    DmodFin.GeraDadosSaldos(FDataRel, True, True);
    //
    MyObjects.frxMostra(frxFIN_CARTE_004_A, 'Lan�amentos do dia');
  end;
}
end;

procedure TFmCartNiv2.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCartNiv2.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCartNiv2CodUsu.Value, LaRegistro.Caption);
end;

procedure TFmCartNiv2.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCartNiv2.QrCartNiv2AfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCartNiv2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCartNiv2.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCartNiv2Codigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cartniv2', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCartNiv2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCartNiv2.frxFIN_CARTE_004_AGetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_DATA_LCT') = 0 then
    Value := Geral.FDT(FDataRel, 3)
end;

procedure TFmCartNiv2.QrCartNiv2BeforeOpen(DataSet: TDataSet);
begin
  QrCartNiv2Codigo.DisplayFormat := FFormatFloat;
end;

end.

