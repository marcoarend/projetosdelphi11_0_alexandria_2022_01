object FmEmiChPertoChek: TFmEmiChPertoChek
  Left = 367
  Top = 177
  Caption = 'Emite Cheque'
  ClientHeight = 593
  ClientWidth = 778
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 778
    Height = 497
    Align = alClient
    TabOrder = 0
    object Panel1: TPanel
      Left = 1
      Top = 384
      Width = 776
      Height = 112
      Align = alBottom
      TabOrder = 0
      object Label3: TLabel
        Left = 14
        Top = 60
        Width = 34
        Height = 13
        Caption = 'Banco:'
      end
      object Label4: TLabel
        Left = 98
        Top = 60
        Width = 42
        Height = 13
        Caption = 'Ag'#234'ncia:'
      end
      object Label6: TLabel
        Left = 204
        Top = 60
        Width = 31
        Height = 13
        Caption = 'Conta:'
      end
      object Label7: TLabel
        Left = 434
        Top = 60
        Width = 40
        Height = 13
        Caption = 'Cheque:'
      end
      object Label8: TLabel
        Left = 580
        Top = 60
        Width = 33
        Height = 13
        Caption = 'Comp.:'
      end
      object Label9: TLabel
        Left = 16
        Top = 8
        Width = 35
        Height = 13
        Caption = 'CMC-7:'
      end
      object ST_Banco: TStaticText
        Left = 14
        Top = 76
        Width = 67
        Height = 30
        Caption = '399'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'CMC7'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object ST_Agencia: TStaticText
        Left = 98
        Top = 76
        Width = 88
        Height = 30
        Caption = '0036'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'CMC7'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
      object ST_Conta: TStaticText
        Left = 204
        Top = 76
        Width = 214
        Height = 30
        Caption = '0036288290'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'CMC7'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
      object ST_CMC7: TStaticText
        Left = 14
        Top = 24
        Width = 634
        Height = 30
        Caption = '399 0036 0036288290 908398 009'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'CMC7'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
      end
      object ST_Cheque: TStaticText
        Left = 434
        Top = 76
        Width = 130
        Height = 30
        Caption = '908398'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'CMC7'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
      end
      object ST_Praca: TStaticText
        Left = 580
        Top = 76
        Width = 67
        Height = 30
        Caption = '009'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'CMC7'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
      end
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 776
      Height = 304
      Align = alTop
      TabOrder = 1
      object Label1: TLabel
        Left = 12
        Top = 8
        Width = 26
        Height = 13
        Caption = 'Data:'
      end
      object LaValor: TLabel
        Left = 112
        Top = 8
        Width = 27
        Height = 13
        Caption = 'Valor:'
      end
      object Label5: TLabel
        Left = 208
        Top = 8
        Width = 58
        Height = 13
        Caption = 'Benefici'#225'rio:'
      end
      object Label2: TLabel
        Left = 568
        Top = 48
        Width = 36
        Height = 13
        Caption = 'Cidade:'
      end
      object Label11: TLabel
        Left = 12
        Top = 48
        Width = 34
        Height = 13
        Caption = 'Banco:'
      end
      object Label10: TLabel
        Left = 16
        Top = 280
        Width = 640
        Height = 16
        Caption = 
          'Clique no bot'#227'o "Envia" e aguarde o retorno ou feche a janela ca' +
          'so n'#227'o queira aguardar o retorno!!!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TPData: TDateTimePicker
        Left = 12
        Top = 24
        Width = 97
        Height = 21
        Date = 38741.724412615700000000
        Time = 38741.724412615700000000
        TabOrder = 0
      end
      object EdValor: TdmkEdit
        Left = 112
        Top = 24
        Width = 93
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnExit = EdValorExit
      end
      object EdBenef: TdmkEdit
        Left = 208
        Top = 24
        Width = 553
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdCidade: TdmkEdit
        Left = 568
        Top = 64
        Width = 193
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object RGForma: TRadioGroup
        Left = 16
        Top = 100
        Width = 757
        Height = 177
        Caption = ' Forma de preenchimento: '
        Columns = 2
        ItemIndex = 2
        Items.Strings = (
          '0 - Somente preenchimento'
          '1 - Preenchimento e chancela'
          '2 - Preenchimento e leitura de caracteres magnetiz'#225'veis CMC7'
          '3 - Preenchimento, chancela e leitura'
          '4 - Preenchimento com ano de 4 d'#237'gitos'
          '5 - Preenchimento, chancela e ano com 4 d'#237'gitos'
          '6 - Preenchimento, leitura e ano com 4 d'#237'gitos'
          '7 - Preenchimento, chancela, leitura e ano com 4 d'#237'gitos'
          '8 - Preenchimento e cruzamento'
          '9 - Preenchimento, cruzamento e chancela'
          'A - Preenchimento, cruzamento e leitura'
          'B - Preenchimento, cruzamento, chancela e leitura'
          'C - Preenchimento, cruzamento e ano com 4 d'#237'gitos'
          'D - Preenchimento, cruzamento, chancela e ano com 4 d'#237'gitos'
          'E - Preenchimento, cruzamento, leitura e ano com 4 d'#237'gitos'
          
            'F - Preenchimento, cruzamento, chancela, leitura e ano com 4 d'#237'g' +
            'itos')
        TabOrder = 4
      end
      object EdBanco: TdmkEditCB
        Left = 12
        Top = 64
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBBanco
        IgnoraDBLookupComboBox = False
      end
      object CBBanco: TdmkDBLookupComboBox
        Left = 60
        Top = 64
        Width = 505
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsBancos
        TabOrder = 6
        dmkEditCB = EdBanco
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 305
      Width = 776
      Height = 79
      Align = alClient
      DataSource = DsImpCHMsg
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Agora'
          Title.Caption = 'Data / hora'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Benefic'
          Title.Caption = 'Benefici'#225'rio'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CodErr'
          Title.Caption = 'Retorno'
          Width = 45
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MENSAGEM_ERRO'
          Title.Caption = 'Significado do retorno'
          Width = 580
          Visible = True
        end>
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 545
    Width = 778
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Envia'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 682
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSaidaClick
    end
    object Edit1: TEdit
      Left = 216
      Top = 16
      Width = 229
      Height = 21
      ReadOnly = True
      TabOrder = 2
      Visible = False
    end
    object Edit2: TEdit
      Left = 448
      Top = 16
      Width = 129
      Height = 21
      ReadOnly = True
      TabOrder = 3
      Visible = False
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 778
    Height = 48
    Align = alTop
    Caption = 'Emite Cheque'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 2
      Top = 2
      Width = 774
      Height = 44
      Align = alClient
      Transparent = True
      ExplicitWidth = 782
    end
  end
  object QrBancos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM bancos'
      'ORDER BY Nome')
    Left = 9
    Top = 9
    object QrBancosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrBancosSite: TWideStringField
      FieldName = 'Site'
      Size = 255
    end
    object QrBancosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrBancosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBancosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrBancosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrBancosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrBancosDVCC: TSmallintField
      FieldName = 'DVCC'
    end
  end
  object DsBancos: TDataSource
    DataSet = QrBancos
    Left = 37
    Top = 9
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 296
    Top = 124
  end
  object QrImpCHMsg: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrImpCHMsgAfterOpen
    AfterScroll = QrImpCHMsgAfterScroll
    OnCalcFields = QrImpCHMsgCalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM impchmsg'
      'WHERE Tabela=:P0'
      'AND Controle=:P1'
      '')
    Left = 68
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrImpCHMsgAgora: TDateTimeField
      FieldName = 'Agora'
    end
    object QrImpCHMsgImpressora: TSmallintField
      FieldName = 'Impressora'
    end
    object QrImpCHMsgCodErr: TWideStringField
      FieldName = 'CodErr'
      Size = 3
    end
    object QrImpCHMsgCodMsg: TWideStringField
      FieldName = 'CodMsg'
      Size = 40
    end
    object QrImpCHMsgTipMsg: TSmallintField
      FieldName = 'TipMsg'
    end
    object QrImpCHMsgMENSAGEM_ERRO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAGEM_ERRO'
      Size = 255
      Calculated = True
    end
    object QrImpCHMsgValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrImpCHMsgBenefic: TWideStringField
      FieldName = 'Benefic'
    end
  end
  object DsImpCHMsg: TDataSource
    DataSet = QrImpCHMsg
    Left = 96
    Top = 8
  end
end
