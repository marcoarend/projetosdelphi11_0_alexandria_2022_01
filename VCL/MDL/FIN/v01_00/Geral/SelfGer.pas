unit SelfGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkDBGrid, ComCtrls, DBCtrls, Grids,
  DBGrids, Menus, Db, mySQLDbTables, dmkPermissoes, dmkGeral, dmkEdit,
  dmkEditCB, dmkDBLookupComboBox, UMySQLModule, dmkEditDateTimePicker, Mask,
  UnDmkEnums;

type
  TFmSelfGer = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel4: TPanel;
    Panel24: TPanel;
    BtAtzCarts: TBitBtn;
    Panel25: TPanel;
    DBGrid18: TDBGrid;
    Panel21: TPanel;
    DBGrid17: TDBGrid;
    StaticText1: TStaticText;
    Panel22: TPanel;
    DBGCarteiras: TDBGrid;
    StaticText2: TStaticText;
    Panel23: TPanel;
    DBGCarts1: TDBGrid;
    StaticText3: TStaticText;
    TabSheet3: TTabSheet;
    PnLct: TPanel;
    PainelDados2: TPanel;
    Panel5: TPanel;
    Panel7: TPanel;
    Label6: TLabel;
    LaSaldo: TLabel;
    LaDiferenca: TLabel;
    LaCaixa: TLabel;
    Label20: TLabel;
    EdCodigo: TDBEdit;
    EdNome: TDBEdit;
    EdSaldo: TDBEdit;
    EdDiferenca: TDBEdit;
    EdCaixa: TDBEdit;
    BtSaldoAqui: TBitBtn;
    EdSdoAqui: TdmkEdit;
    BtRefresh: TBitBtn;
    EdSoma2: TdmkEdit;
    Panel6: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label5: TLabel;
    Label1: TLabel;
    BtMenu: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    TPDataIni: TdmkEditDateTimePicker;
    TPDataFim: TdmkEditDateTimePicker;
    BtExclui: TBitBtn;
    BtContarDinheiro: TBitBtn;
    BtConcilia: TBitBtn;
    BtCNAB: TBitBtn;
    BtCopiaCH: TBitBtn;
    BtQuita: TBitBtn;
    BtFluxoCxa: TBitBtn;
    BtAutom: TBitBtn;
    CBUH: TDBLookupComboBox;
    DBGCarts2: TDBGrid;
    Panel8: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    StatusBar: TStatusBar;
    dmkDBGLct: TdmkDBGrid;
    Timer2: TTimer;
    PMAtzCarts: TPopupMenu;
    Acarteiraselecionada1: TMenuItem;
    Todascarteiras1: TMenuItem;
    PMMenu: TPopupMenu;
    Transferir1: TMenuItem;
    Nova1: TMenuItem;
    Editar1: TMenuItem;
    Excluir1: TMenuItem;
    Localizar1: TMenuItem;
    Quitar1: TMenuItem;
    Compensar1: TMenuItem;
    Reverter1: TMenuItem;
    N10: TMenuItem;
    Pagar1: TMenuItem;
    N11: TMenuItem;
    PagarAVista1: TMenuItem;
    N2: TMenuItem;
    Localizarpagamentosdestaemisso1: TMenuItem;
    Lanamento1: TMenuItem;
    Localizar2: TMenuItem;
    Copiar1: TMenuItem;
    Mudacarteiradelanamentosselecionados1: TMenuItem;
    Carteiras1: TMenuItem;
    Data1: TMenuItem;
    Compensao1: TMenuItem;
    Msdecompetncia1: TMenuItem;
    TextoParcial1: TMenuItem;
    Recibo1: TMenuItem;
    RetornoCNAB1: TMenuItem;
    Colocarmsdecompetnciaondenotem1: TMenuItem;
    Datalancto1: TMenuItem;
    Vencimento1: TMenuItem;
    PMQuita: TPopupMenu;
    Compensar2: TMenuItem;
    Reverter2: TMenuItem;
    MenuItem3: TMenuItem;
    Pagar2: TMenuItem;
    MenuItem5: TMenuItem;
    PagarAVista2: TMenuItem;
    BtEspecificos: TBitBtn;
    PMRefresh: TPopupMenu;
    Carteira1: TMenuItem;
    Atual5: TMenuItem;
    Todas1: TMenuItem;
    ContaPlanodecontas1: TMenuItem;
    ProgressBar1: TProgressBar;
    TabSheet4: TTabSheet;
    MeAtz: TMemo;
    PMSaldoAqui: TPopupMenu;
    Calcula1: TMenuItem;
    Limpa1: TMenuItem;
    Diferena1: TMenuItem;
    PMEspecificos: TPopupMenu;
    N1: TMenuItem;
    Exclusoincondicional1: TMenuItem;
    ransfernciaentrecontas1: TMenuItem;
    IncluiTrfCta1: TMenuItem;
    BtTrfCta: TBitBtn;
    PMTrfCta: TPopupMenu;
    Novatransferncia1: TMenuItem;
    Alteratransferncia1: TMenuItem;
    Excluitransferncia1: TMenuItem;
    dmkPermissoes1: TdmkPermissoes;
    BtLctoEndoss: TBitBtn;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    BtBalancete: TBitBtn;
    BtRelat: TBitBtn;
    PMRelat: TPopupMenu;
    Pesquisas1: TMenuItem;
    Result1: TMenuItem;
    porPerodo1: TMenuItem;
    Mensais1: TMenuItem;
    Saldos1: TMenuItem;
    SaldosEm1: TMenuItem;
    Descontodeduplicatas1: TMenuItem;
    Extrato1: TMenuItem;
    PagarReceber1: TMenuItem;
    Movimento1: TMenuItem;
    BtAtzSdoLcto: TBitBtn;
    Emitidos1: TMenuItem;
    Quitados1: TMenuItem;
    RGTipoData: TRadioGroup;
    AlteraTrfCta1: TMenuItem;
    ExcluiTrfCta1: TMenuItem;
    ImgSelfGer: TImage;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure DBGCarts1DblClick(Sender: TObject);
    procedure dmkDBGLctCellClick(Column: TColumn);
    procedure dmkDBGLctDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure dmkDBGLctKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PageControl1Change(Sender: TObject);
    procedure PMQuitaPopup(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure BtAtzCartsClick(Sender: TObject);
    procedure Acarteiraselecionada1Click(Sender: TObject);
    procedure Todascarteiras1Click(Sender: TObject);
    procedure Nova1Click(Sender: TObject);
    procedure Editar1Click(Sender: TObject);
    procedure Excluir1Click(Sender: TObject);
    procedure Localizar1Click(Sender: TObject);
    procedure BtMenuClick(Sender: TObject);
    procedure Compensar1Click(Sender: TObject);
    procedure Reverter1Click(Sender: TObject);
    procedure Pagar1Click(Sender: TObject);
    procedure PagarAVista1Click(Sender: TObject);
    procedure Localizarpagamentosdestaemisso1Click(Sender: TObject);
    procedure Localizar2Click(Sender: TObject);
    procedure Copiar1Click(Sender: TObject);
    procedure Carteiras1Click(Sender: TObject);
    procedure Data1Click(Sender: TObject);
    procedure Compensao1Click(Sender: TObject);
    procedure Msdecompetncia1Click(Sender: TObject);
    procedure TextoParcial1Click(Sender: TObject);
    procedure Recibo1Click(Sender: TObject);
    procedure RetornoCNAB1Click(Sender: TObject);
    procedure Datalancto1Click(Sender: TObject);
    procedure Vencimento1Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtConciliaClick(Sender: TObject);
    procedure BtQuitaClick(Sender: TObject);
    procedure Compensar2Click(Sender: TObject);
    procedure Reverter2Click(Sender: TObject);
    procedure Pagar2Click(Sender: TObject);
    procedure PagarAVista2Click(Sender: TObject);
    procedure BtCNABClick(Sender: TObject);
    procedure BtCopiaCHClick(Sender: TObject);
    procedure TPDataIniChange(Sender: TObject);
    procedure CBUHClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure BtEspecificosClick(Sender: TObject);
    procedure PMMenuPopup(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure Atual5Click(Sender: TObject);
    procedure Todas1Click(Sender: TObject);
    procedure ContaPlanodecontas1Click(Sender: TObject);
    procedure BtSaldoAquiClick(Sender: TObject);
    procedure Calcula1Click(Sender: TObject);
    procedure Limpa1Click(Sender: TObject);
    procedure Diferena1Click(Sender: TObject);
    procedure BtContarDinheiroClick(Sender: TObject);
    procedure BtFluxoCxaClick(Sender: TObject);
    procedure BtAutomClick(Sender: TObject);
    procedure TPDataFimChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Exclusoincondicional1Click(Sender: TObject);
    procedure BtTrfCtaClick(Sender: TObject);
    procedure Novatransferncia1Click(Sender: TObject);
    procedure Alteratransferncia1Click(Sender: TObject);
    procedure Excluitransferncia1Click(Sender: TObject);
    procedure PMTrfCtaPopup(Sender: TObject);
    procedure BtLctoEndossClick(Sender: TObject);
    procedure BtBalanceteClick(Sender: TObject);
    procedure Pesquisas1Click(Sender: TObject);
    procedure BtRelatClick(Sender: TObject);
    procedure Mensais1Click(Sender: TObject);
    procedure Saldos1Click(Sender: TObject);
    procedure SaldosEm1Click(Sender: TObject);
    procedure Descontodeduplicatas1Click(Sender: TObject);
    procedure PagarReceber1Click(Sender: TObject);
    procedure Movimento1Click(Sender: TObject);
    procedure BtAtzSdoLctoClick(Sender: TObject);
    procedure Emitidos1Click(Sender: TObject);
    procedure Quitados1Click(Sender: TObject);
    procedure RGTipoDataClick(Sender: TObject);
    procedure IncluiTrfCta1Click(Sender: TObject);
    procedure AlteraTrfCta1Click(Sender: TObject);
    procedure ExcluiTrfCta1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ShowHint(Sender: TObject);
    procedure SomaLinhas_2;
    procedure VerificaBotoes();
    procedure VerificaCarteirasCliente();
    procedure MudaDataLctSel(CampoData: String);
    procedure ReverterCompensacao;
    procedure CompensarContaCorrenteBanco;
    procedure PagarAVistaEmCaixa;
    procedure Colocarmsdecompetnciaondenotem(Tipo: Integer);
    //function ReabreLct(): Boolean;
  public
    { Public declarations }
    FFinalidade: TIDFinalidadeLct;
    FGeradorTxt: String;
    procedure AtualizaCarteiraAtual();
    procedure AtualizaTodasCarteiras();
    //procedure CompensarContaCorrenteBanco;
  end;

  var
  FmSelfGer: TFmSelfGer;

implementation

uses UnMyObjects, Principal, UnInternalConsts, Module, LctMudaCart, GetData,
LctMudaText, UnFinanceiro, LctPgEmCxa, UnGOTOy, Concilia, MyDBCheck, ModuleFin,
FinForm, ModuleGeral, FluxoCxa, LctEdit, CashBal, Resultados1, Resultados2, Resmes,
Saldos, DescDupli, PrincipalImp, Extratos, Pesquisas;

{$R *.DFM}

procedure TFmSelfGer.FormActivate(Sender: TObject);
begin
  MyObjects.DefineTituloDBGrid(TDBGrid(dmkDBGLct), 'FatNum', VAR_TITULO_FATNUM);
  MyObjects.CorIniComponente();
end;

procedure TFmSelfGer.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmSelfGer.IncluiTrfCta1Click(Sender: TObject);
var
  CliInt: Integer;
begin
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  UFinanceiro.CriarTransferCtas(0, DmodFin.QrLctos, DmodFin.QrCarts,
    True, CliInt, 0, 0, 0);
  UFinanceiro.RecalcSaldoCarteira(DmodFin.QrCartsCodigo.Value, DmodFin.QrCarts,
    True, True);
end;

procedure TFmSelfGer.DBGCarts1DblClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 2;
  PageControl1Change(PageControl1);
end;

procedure TFmSelfGer.Descontodeduplicatas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmDescDupli, FmDescDupli, afmoNegarComAviso) then
  begin
    FmDescDupli.ShowModal;
    FmDescDupli.Destroy;
  end;
end;

procedure TFmSelfGer.dmkDBGLctCellClick(Column: TColumn);
begin
  Somalinhas_2;
end;

procedure TFmSelfGer.dmkDBGLctDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  MyObjects.DefineCorTextoSitLancto(TDBGrid(Sender), Rect, Column.FieldName,
    DmodFin.QrLctosNOMESIT.Value);
end;

procedure TFmSelfGer.dmkDBGLctKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key=VK_UP) and (Shift=[ssShift]) then Somalinhas_2
  else if (key=VK_DOWN) and (Shift=[ssShift]) then Somalinhas_2
  //
  else if key=13 then

{
  UFinanceiro.InsAltLancamento(TFmLctEdit, FmLctEdit,
    lfProprio, afmoNegarComAviso, DmodFin.QrLctos, DmodFin.QrCarts,
    tgrAltera, DmodFin.QrLctosControle.Value, DmodFin.QrLctosSub.Value,
    DmodFin.QrLctosGenero.Value, Dmod.QrControleMyPerJuros.Value,
    Dmod.QrControleMyPerMulta.Value, nil, 0, 0, 0, 0, 0, 0, False,
    0(*Cliente*), 0(*Fornecedor*), DmodFin.QrCartsForneceI.Value(*cliInt*),
    0(*ForneceI*), 0(*Account*), 0(*Vendedor*), True(*LockCliInt*),
    False(*LockForneceI*), False(*LockAccount*), False(*LockVendedor*),
    0, 0, DmodFin.QrLctosCompensado.Value, 0, 1, 0)
}  UFinanceiro.AlteracaoLancamento(DmodFin.QrCarts, DmodFin.QrLctos, lfProprio,
    0(*OneAccount*), DmodFin.QrCartsForneceI.Value(*OneCliInt*), True)
    
  else if (key=VK_DELETE) and (Shift=[ssCtrl]) then
  UFinanceiro.ExcluiItemCarteira(DmodFin.QrLctosControle.Value,
    DmodFin.QrLctosData.Value,
    DmodFin.QrLctosSub.Value, DmodFin.QrLctosGenero.Value,
    DmodFin.QrLctosCartao.Value, DmodFin.QrLctosSit.Value,
    DmodFin.QrLctosTipo.Value, 0, DmodFin.QrLctosID_Pgto.Value,
    DmodFin.QrLctos, DmodFin.QrCarts, True,
    DmodFin.QrLctosCarteira.Value, DmodFin.QrLctosCarteira.Value)
  else if (key=VK_F4) and (Shift=[ssCtrl]) then
    MyObjects.MostraPopUpDeBotaoObject(PMQuita, dmkDBGLct, 0, 0)
  //else if (key=VK_F5) and (Shift=[ssCtrl]) then ExibirLista1Click(Self)
  //else if (key=VK_F6) and (Shift=[ssCtrl]) then Promissria1Click(Self)
end;

procedure TFmSelfGer.PageControl1Change(Sender: TObject);
begin
  //!BtSaida.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    1: VerificaCarteirasCliente();
    2: VerificaBotoes;
  end;
  //!PainelConfirma.Visible := PageControl1.ActivePageIndex in ([0,1,2]);
end;

procedure TFmSelfGer.Pesquisas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPesquisas, FmPesquisas, afmoNegarComAviso) then
  begin
    FmPesquisas.ShowModal;
    FmPesquisas.Destroy;
  end;
end;

procedure TFmSelfGer.SomaLinhas_2;
begin
  Timer2.Enabled := False;
  Timer2.Enabled := True;
end;

procedure TFmSelfGer.PMQuitaPopup(Sender: TObject);
const
  k = 3;
var
  i, n: Integer;
  c: array [0..k] of Integer;
begin
  for i := 0 to k do c[i] := 0;
  if dmkDBGLct.SelectedRows.Count > 1 then
  begin
    with dmkDBGLct.DataSource.DataSet do
    for i:= 0 to dmkDBGLct.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(dmkDBGLct.SelectedRows.Items[i]));
      Inc(c[DmodFin.QrLctosSit.Value], 1);
    end;
    n := dmkDBGLct.SelectedRows.Count;
  end else
  begin
    c[DmodFin.QrLctosSit.Value] := 1;
    n := 1;
  end;
  Compensar2.Enabled   := False;
  Pagar2.Enabled       := False;
  Reverter2.Enabled    := False;
  PagarAVista2.Enabled := False;
  if DmodFin.QrCartsTipo.Value = 2 then
  begin
    //if DmodFin.QrLctosSit.Value in [0] then   Compensar2.Enabled := True;
    Compensar2.Enabled := c[0] = n;
    //if DmodFin.QrLctosSit.Value in [0,1,2] then   Pagar2.Enabled := True;
    Pagar2.Enabled := (n=1) and (c[3] = 0);
    //if DmodFin.QrLctosSit.Value in [3] then    Reverter2.Enabled := True;
    Reverter2.Enabled := c[3] = n;
    //if DmodFin.QrLctosSit.Value in [0] then PagarAvista2.Enabled := True;
    PagarAvista2.Enabled := c[0] = n;
  end;
end;

procedure TFmSelfGer.PMTrfCtaPopup(Sender: TObject);
begin
  Alteratransferncia1.Enabled := DmodFin.QrLctosFatID.Value = -1;
  Excluitransferncia1.Enabled := DmodFin.QrLctosFatID.Value = -1;
end;

procedure TFmSelfGer.Quitados1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmResultados2, FmResultados2, afmoNegarComAviso) then
  begin
    FmResultados2.ShowModal;
    FmResultados2.Destroy;
  end;
end;

procedure TFmSelfGer.VerificaCarteirasCliente();
var
  Carteira: Integer;
begin
  if DmodFin.QrCarts.State <> dsBrowse then Carteira := 0
  else Carteira := DmodFin.QrCartsCodigo.Value;
  //
  if (DmodFin.QrCarts.State = dsInactive)
  or not DmodG.EmpresaLogada(DmodFin.QrCartsForneceI.Value) then
    DmodFin.ReabreCarteiras(Carteira, DmodFin.QrCarts, DmodFin.QrCartSum,
    ' > FmSelfGer.VerificaCarteirasCliente()');
end;

procedure TFmSelfGer.VerificaBotoes();
var
  Ativo: Boolean;
begin
  if DmodFin.QrCarts.State = dsBrowse then
  begin
    if DmodFin.QrCarts.RecordCount > 0 then Ativo := True
    else Ativo := False;
  end else Ativo := False;
  //BtMenu.Enabled := Ativo;
  //BtInclui.Enabled := Ativo;
  BtAltera.Enabled := Ativo;
  BtExclui.Enabled := Ativo;
  BtSaldoAqui.Enabled := Ativo;
  BtContarDinheiro.Enabled := Ativo;
  BtRefresh.Enabled := Ativo;
  //
  (*if DmodFin.QrLctos.State = dsBrowse then
  begin
    if DmodFin.QrLctos.RecordCount > 0 then Ativo := True
    else Ativo := False;
  end else Ativo := False;*)
end;

procedure TFmSelfGer.Timer2Timer(Sender: TObject);
var
  Debito, Credito: Double;
  i: Integer;
begin
  Timer2.Enabled := False;
  Debito := 0;
  Credito := 0;
  with dmkDBGLct.DataSource.DataSet do
  for i:= 0 to dmkDBGLct.SelectedRows.Count-1 do
  begin
    GotoBookmark(pointer(dmkDBGLct.SelectedRows.Items[i]));
    Debito := Debito + DmodFin.QrLctosDebito.Value;
    Credito := Credito + DmodFin.QrLctosCredito.Value;
  end;
  EdSoma2.Text := Geral.FFT(Credito-Debito, 2, siNegativo);
end;

procedure TFmSelfGer.BtAtzCartsClick(Sender: TObject);
begin
  if (DmodFin.QrCarts.State = dsBrowse)
  or (DmodFin.QrCarts.RecordCount = 0) then
    MyObjects.MostraPopUpDeBotao(PMAtzCarts, BtAtzCarts)
  else Application.MessageBox('N�o h� carteira selecionada!', 'Aviso',
  MB_OK+MB_ICONWARNING);
end;

procedure TFmSelfGer.BtAtzSdoLctoClick(Sender: TObject);
var
  _Sit, Controle: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    _Sit     := DmodFin.QrLctosSit.Value;
    Controle := DmodFin.QrLctosControle.Value;
    //
    UFinanceiro.AtualizaEmissaoMasterRapida(Controle, _Sit, 0, '');
    //
    DmodFin.QrLctos.Close;
    DmodFin.QrLctos.Open;
    DmodFin.QrLctos.Locate('Controle', Controle, []);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSelfGer.Acarteiraselecionada1Click(Sender: TObject);
begin
  AtualizaCarteiraAtual();
end;

procedure TFmSelfGer.Todascarteiras1Click(Sender: TObject);
begin
  AtualizaTodasCarteiras();
end;

procedure TFmSelfGer.AtualizaCarteiraAtual();
begin
  UFinanceiro.AtualizaVencimentos;
  UFinanceiro.RecalcSaldoCarteira(DmodFin.QrCartsCodigo.Value,
    DmodFin.QrCarts, True, True);
end;

procedure TFmSelfGer.AtualizaTodasCarteiras();
var
  Carteira: Integer;
begin
  Screen.Cursor := crHourGlass;
  Carteira := DmodFin.QrCartsCodigo.Value;
  UFinanceiro.AtualizaVencimentos;
  DmodFin.QrCarts.DisableControls;
  DmodFin.QrCarts.First;
  while not DmodFin.QrCarts.Eof do
  begin
    UFinanceiro.RecalcSaldoCarteira(DmodFin.QrCartsCodigo.Value,
      DmodFin.QrCarts, True, True);
    //
    DmodFin.QrCarts.Next;
  end;
  DmodFin.LocCod(Carteira, Carteira, DmodFin.QrCarts, 'TFmSelfGer.AtualizaTodasCarteiras()');
  DmodFin.QrCarts.EnableControls;
  Screen.Cursor := crDefault;
end;

procedure TFmSelfGer.Nova1Click(Sender: TObject);
var
  CliInt: Integer;
begin
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  UFinanceiro.CriarTransferCart(0, DmodFin.QrLctos, DmodFin.QrCarts,
    True, CliInt, 0, 0, 0);
  UFinanceiro.RecalcSaldoCarteira(DmodFin.QrCartsCodigo.Value,
   DmodFin.QrCarts, True, True);
  //if QrCarts.State = dsBrowse then ReopenResumo;
end;

procedure TFmSelfGer.Novatransferncia1Click(Sender: TObject);
var
  CliInt: Integer;
begin
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  UFinanceiro.CriarTransferCtas(0, DmodFin.QrLctos, DmodFin.QrCarts,
    True, CliInt, 0, 0, 0);
  UFinanceiro.RecalcSaldoCarteira(DmodFin.QrCartsCodigo.Value, DmodFin.QrCarts,
    True, True);
end;

procedure TFmSelfGer.Editar1Click(Sender: TObject);
var
  CliInt: Integer;
begin
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  UFinanceiro.CriarTransferCart(1, DmodFin.QrLctos, DmodFin.QrCarts,
    True, CliInt, 0, 0, 0);
  UFinanceiro.RecalcSaldoCarteira(DmodFin.QrCartsCodigo.Value,
    DmodFin.QrCarts, True, True);
end;

procedure TFmSelfGer.Emitidos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmResultados1, FmResultados1, afmoNegarComAviso) then
  begin
    FmResultados1.FTabLctA := VAR_LCT;
    FmResultados1.ShowModal;
    FmResultados1.Destroy;
  end;
end;

procedure TFmSelfGer.ExcluiTrfCta1Click(Sender: TObject);
var
  CliInt: Integer;
begin
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  UFinanceiro.CriarTransferCtas(2, DmodFin.QrLctos, DmodFin.QrCarts,
    True, CliInt, 0, 0, 0);
  UFinanceiro.RecalcSaldoCarteira(DmodFin.QrCartsCodigo.Value,
    DmodFin.QrCarts, True, True);
end;

procedure TFmSelfGer.Excluir1Click(Sender: TObject);
var
  CliInt: Integer;
begin
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  UFinanceiro.CriarTransferCart(2, DmodFin.QrLctos, DmodFin.QrCarts,
    True, CliInt, 0, 0, 0);
  UFinanceiro.RecalcSaldoCarteira(DmodFin.QrCartsCodigo.Value,
    DmodFin.QrCarts, True, True);
end;

procedure TFmSelfGer.Excluitransferncia1Click(Sender: TObject);
var
  CliInt: Integer;
begin
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  UFinanceiro.CriarTransferCtas(2, DmodFin.QrLctos, DmodFin.QrCarts,
    True, CliInt, 0, 0, 0);
  UFinanceiro.RecalcSaldoCarteira(DmodFin.QrCartsCodigo.Value,
    DmodFin.QrCarts, True, True);
end;

procedure TFmSelfGer.Exclusoincondicional1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if Application.MessageBox('Confirma a exclus�o INCONDICIONAL deste lan�amento?',
  PChar(VAR_APPNAME),
  MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+MB_APPLMODAL) = ID_YES then
  begin
    UFinanceiro.ExcluiLct_Unico(True, DmodFin.QrLctos.Database,
            DmodFin.QrLctosData.Value, DmodFin.QrLctosTipo.Value,
            DmodFin.QrLctosCarteira.Value, DmodFin.QrLctosControle.Value,
            DmodFin.QrLctosSub.Value, True);
    {
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add(Delete lct WHERE Controle=:P0 AND Sub=:P1 ');
    Dmod.QrUpdM.SQL.Add('AND Carteira=:P2 AND Tipo=:P3 AND Credito=:P4 AND Debito=:P5 ');
    Dmod.QrUpdM.Params[00].AsFloat := DmodFin.QrLctosControle.Value;
    Dmod.QrUpdM.Params[01].AsInteger := DmodFin.QrLctosSub.Value;
    Dmod.QrUpdM.Params[02].AsInteger := DmodFin.QrLctosCarteira.Value;
    Dmod.QrUpdM.Params[03].AsInteger := DmodFin.QrLctosTipo.Value;
    Dmod.QrUpdM.Params[04].AsFloat := DmodFin.QrLctosCredito.Value;
    Dmod.QrUpdM.Params[05].AsFloat := DmodFin.QrLctosDebito.Value;
    Dmod.QrUpdM.ExecSQL;
    }
    //
    DmodFin.QrLctos.Next;
    Controle := DmodFin.QrLctosControle.Value;
    {
    Parei Aqui
    n�o esta recalculando!
    }
    UFinanceiro.RecalcSaldoCarteira(DmodFin.QrCartsCodigo.Value,
      DmodFin.QrCarts, True, True);
    DmodFin.QrLctos.Close;
    DmodFin.QrLctos.Open;
    DmodFin.QrLctos.Locate('Controle', Controle, []);
  end;
end;

procedure TFmSelfGer.Localizar1Click(Sender: TObject);
var
  CliInt: Integer;
begin
  if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
  UFinanceiro.CriarTransferCart(3, DmodFin.QrLctos, DmodFin.QrCarts,
    True, CliInt, 0, 0, 0);
  UFinanceiro.RecalcSaldoCarteira(DmodFin.QrCartsCodigo.Value,
    DmodFin.QrCarts, True, True);
end;

procedure TFmSelfGer.BtMenuClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMenu, BtMenu);
end;

procedure TFmSelfGer.Compensar1Click(Sender: TObject);
begin
  CompensarContaCorrenteBanco;
end;

procedure TFmSelfGer.CompensarContaCorrenteBanco;
var
  Codigo, Controle: Integer;
begin
  Codigo   := DmodFin.QrLctosCarteira.Value;
  Controle := DmodFin.QrLctosControle.Value;
  UFinanceiro.CriaFmQuitaDoc2(DmodFin.QrLctosData.Value,
    DmodFin.QrLctosSub.Value, DmodFin.QrLctosControle.Value,
    DmodFin.QrLctosCtrlIni.Value, DmodFin.QrLctosSub.Value,
    DmodFin.QrCarts);
  DmodFin.LocCod(Codigo, Codigo, DmodFin.QrCarts, 'TFmSelfGer.CompensarContaCorrenteBanco');
  DmodFin.QrLctos.Locate('Controle', Controle, []);
end;

procedure TFmSelfGer.Reverter1Click(Sender: TObject);
begin
  ReverterCompensacao;
end;

procedure TFmSelfGer.ReverterCompensacao;
var
  i, k: Integer;
begin
  if dmkDBGLct.SelectedRows.Count > 1 then
  begin
    if Application.MessageBox(PChar('Confirma a revers�o da compensa��o '+
    'dos itens selecionados?'), 'Pergunta', MB_ICONQUESTION+MB_YESNOCANCEL) =
    ID_YES then
    begin
      with dmkDBGLct.DataSource.DataSet do
      for i:= 0 to dmkDBGLct.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(dmkDBGLct.SelectedRows.Items[i]));
        if DmodFin.QrLctosSit.Value = 3 then
          UFinanceiro.ReverterPagtoEmissao(DmodFin.QrLctos,
            DmodFin.QrCarts, False, False, False);
      end;
      k := DmodFin.QrLctosControle.Value;
      UFinanceiro.RecalcSaldoCarteira(DmodFin.QrCartsCodigo.Value,
        DmodFin.QrCarts, True, True);
      DmodFin.QrLctos.Locate('Controle', k, []);
    end;
  end else UFinanceiro.ReverterPagtoEmissao(DmodFin.QrLctos,
    DmodFin.QrCarts, True, True, True);
end;

procedure TFmSelfGer.RGTipoDataClick(Sender: TObject);
begin
  DmodFin.FTipoData := RGTipoData.ItemIndex;
  //
  DModFin.VeSeReabreLct(TPDataIni, TPDataFim, CBUH.KeyValue,
  DmodFin.QrLctosControle.Value, DmodFin.QrLctosSub.Value,
  DmodFin.QrCarts, DmodFin.QrLctos);
end;

procedure TFmSelfGer.Pagar1Click(Sender: TObject);
begin
  DmodFin.PagarRolarEmissao(DmodFin.QrLctos, DmodFin.QrCarts);
end;

procedure TFmSelfGer.PagarAVista1Click(Sender: TObject);
begin
  PagarAVistaEmCaixa;
end;

procedure TFmSelfGer.Localizarpagamentosdestaemisso1Click(Sender: TObject);
begin
  UFinanceiro.LocalizaPagamentosDeEmissao(DmodFin.QrLctosControle.Value,
    TPDataIni, TPDataFim, DmodFin.QrLctos);
end;

procedure TFmSelfGer.Localizar2Click(Sender: TObject);
begin
  UFinanceiro.LocalizarLancamento(TPDataIni,
  DmodFin.QrCarts, DmodFin.QrLctos, True, 0);
end;

procedure TFmSelfGer.Copiar1Click(Sender: TObject);
begin
  DmodFin.AlteraLanctoEsp(DmodFin.QrLctos, 3,
    Dmod.QrControleMyPerJuros.Value, Dmod.QrControleMyPerMulta.Value,
    TPDataIni, TPDataFim, CBUH.KeyValue, FFinalidade);
end;

procedure TFmSelfGer.Carteiras1Click(Sender: TObject);
var
  i, Carteira, Tipo, Sit: Integer;
  Compensado: String;
begin
  if DBCheck.CriaFm(TFmLctMudaCart, FmLctMudaCart, afmoNegarComAviso) then
  begin
    FmLctMudaCart.ShowModal;
    Carteira := FmLctMudaCart.FCarteiraSel;
    Tipo     := FmLctMudaCart.FCarteiraTip;
    FmLctMudaCart.Destroy;
    if Carteira = DmodFin.QrLctosCarteira.Value then Exit;
    if Carteira <> -1000 then
    begin
      Screen.Cursor := crHourGlass;
      {
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET Carteira=:P0, Tipo=:P1, ');
      Dmod.QrUpd.SQL.Add('Sit=:P2, Compensado=:P3 ');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa AND Sub=:Pb');
      Dmod.QrUpd.SQL.Add('');
      }
      with dmkDBGLct.DataSource.DataSet do
      for i:= 0 to dmkDBGLct.SelectedRows.Count-1 do
      begin
        if Tipo <> 2 then
        begin
          Sit := 3;
          Compensado := Geral.FDT(DmodFin.QrLctosVencimento.Value, 1)
        end else begin
          Sit := DmodFin.QrLctosControle.Value;
          Compensado := Geral.FDT(DmodFin.QrLctosCompensado.Value, 1)
        end;
        if DmodFin.QrLctosGenero.Value < 1 then
        begin
          Application.MessageBox(PChar('O lan�amento '+IntToStr(
          DmodFin.QrLctosControle.Value)+' � protegido. Para editar '+
          'este item selecione seu caminho correto!'), 'Erro', MB_OK+MB_ICONERROR);
          Exit;
        end;
        if DmodFin.QrLctosCartao.Value > 0 then
        begin
          Application.MessageBox(PChar('O lan�amento '+IntToStr(
          DmodFin.QrLctosControle.Value)+' n�o pode ser editado pois '+
          'pertence a uma fatura!'), 'Erro', MB_OK+MB_ICONERROR);
          Exit;
        end;
        GotoBookmark(pointer(dmkDBGLct.SelectedRows.Items[i]));
        UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
        'Carteira', 'Tipo', 'Sit', 'Compensado'], ['Controle'], [
        Carteira, Tipo, Sit, Compensado], [DmodFin.QrLctosControle.Value,
        DmodFin.QrLctosSub.Value], True, '');
        {
        Dmod.QrUpd.Params[00].AsInteger := Carteira;
        Dmod.QrUpd.Params[01].AsInteger := Tipo;
        Dmod.QrUpd.Params[02].AsInteger := Sit;
        Dmod.QrUpd.Params[03].AsString  := Compensado;
        //
        Dmod.QrUpd.Params[04].AsInteger := DmodFin.QrLctosControle.Value;
        Dmod.QrUpd.Params[05].AsInteger := DmodFin.QrLctosSub.Value;
        Dmod.QrUpd.ExecSQL;
        }
      end;
      UFinanceiro.RecalcSaldoCarteira(Carteira, nil, False, False);
      UFinanceiro.RecalcSaldoCarteira(DmodFin.QrLctosCarteira.Value,
        DmodFin.QrLctos, True, True);
    end;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSelfGer.MudaDataLctSel(CampoData: String);
var
  //, Tipo
  i, Carteira, Controle, Sub: Integer;
  Data: String;
begin
  //Tipo     := -1000;
  Carteira := -1000;
  MyObjects.CriaForm_AcessoTotal(TFmGetData, FmGetData);
  FmGetData.TPData.Date := Date;
  FmGetData.ShowModal;
  FmGetData.Destroy;
  if VAR_GETDATA = 0 then
    Exit
  else
  begin
    with dmkDBGLct.DataSource.DataSet do
    for i:= 0 to dmkDBGLct.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(dmkDBGLct.SelectedRows.Items[i]));
      //
      if (DmodFin.QrLctosSit.Value < 2) and
      (Uppercase(CampoData) = Uppercase('Compensado')) then
      begin
        Application.MessageBox(PChar('O lan�amento '+IntToStr(
        DmodFin.QrLctosControle.Value)+' n�o foi compensado ainda!'),
        'Erro', MB_OK+MB_ICONERROR);
        Exit;
      end;
      if (Uppercase(CampoData) = Uppercase('Mez')) then
      begin
        if VAR_GETDATA = 0 then
          Data := ''
        else
          Data := Geral.FDT(VAR_GETDATA, 13);
      end else Data := Geral.FDT(VAR_GETDATA, 1);
{
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET '+CampoData+'=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:P1 AND Sub=:P2');
      Dmod.QrUpd.SQL.Add('');
      //
      Dmod.QrUpd.Params[00].AsString  := Data;
      Dmod.QrUpd.Params[01].AsInteger := DmodFin.QrLctosControle.Value;
      Dmod.QrUpd.Params[02].AsInteger := DmodFin.QrLctosSub.Value;
      Dmod.QrUpd.ExecSQL;
}
      Controle := DmodFin.QrLctosControle.Value;
      Sub      := DmodFin.QrLctosSub.Value;
      UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
      CampoData], ['Controle', 'Sub'], [Data], [Controle, Sub], True, '');
      //
      if (Uppercase(CampoData) = Uppercase('Compensado')) then
      begin
        if Controle > 0 then
        begin
          {
          Dmod.QrUpdM.SQL.Clear;
          Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET Data=:P0');
          Dmod.QrUpdM.SQL.Add('WHERE ID_Pgto=:P1 AND ID_Pgto>0');
          Dmod.QrUpdM.SQL.Add('');
          Dmod.QrUpdM.Params[00].AsString  := Data;
          Dmod.QrUpdM.Params[01].AsInteger := DmodFin.QrLctosControle.Value;
          Dmod.QrUpdM.ExecSQL;
          // Igualar  Vencimento e compensado dos pagamentos � vista
          Dmod.QrUpdM.SQL.Clear;
          Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET Compensado=:P0, Vencimento=:P1');
          Dmod.QrUpdM.SQL.Add('WHERE ID_Pgto=:P2 AND Tipo<>2 AND ID_Pgto > 0');
          Dmod.QrUpdM.Params[00].AsString  := Data;
          Dmod.QrUpdM.Params[01].AsString  := Data;
          Dmod.QrUpdM.Params[02].AsInteger := DmodFin.QrLctosControle.Value;
          Dmod.QrUpdM.ExecSQL;
          }
          UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
          'Data'], ['ID_Pgto'], [Data], [Controle], True, '');
          // Igualar  Vencimento e compensado dos pagamentos � vista
          UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
          'Compensado', 'Vencimento'], ['ID_Pgto'], [
          Data, Data], [Controle], True, ' AND Tipo<>2');
        end;
      end;
    end;
    UFinanceiro.RecalcSaldoCarteira(Carteira, nil, False, False);
    UFinanceiro.RecalcSaldoCarteira(DmodFin.QrLctosCarteira.Value,
      DmodFin.QrCarts, True, True);
  end;
end;

procedure TFmSelfGer.Data1Click(Sender: TObject);
begin
  MudaDataLctSel('Data');
end;

procedure TFmSelfGer.Compensao1Click(Sender: TObject);
begin
  MudaDataLctSel('Compensado');
end;

procedure TFmSelfGer.Mensais1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmResMes, FmResMes, afmoNegarComAviso) then
  begin
    FmResMes.ShowModal;
    FmResMes.Destroy;
  end;
end;

procedure TFmSelfGer.Movimento1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPrincipalImp, FmPrincipalImp, afmoNegarComAviso) then
  begin
    FmPrincipalImp.ShowModal;
    FmPrincipalImp.Destroy;
  end;
end;

procedure TFmSelfGer.Msdecompetncia1Click(Sender: TObject);
begin
  MudaDataLctSel('Mez');
end;

procedure TFmSelfGer.TextoParcial1Click(Sender: TObject);
var
  i: Integer;
  Atual, Novo, TodoTexto: String;
  Muda: Boolean;
begin
  if DBCheck.CriaFm(TFmLctMudaText, FmLctMudaText, afmoNegarComAviso) then
  begin
    FmLctMudaText.ShowModal;
    Muda  := FmLctMudaText.FMuda;
    Atual := FmLctMudaText.FAtual;
    Novo  := FmLctMudaText.FNovo;
    FmLctMudaText.Destroy;
    if Muda then
    begin
      Screen.Cursor := crHourGlass;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET Descricao=:P0 ');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa AND Sub=:Pb');
      Dmod.QrUpd.SQL.Add('');
      with dmkDBGLct.DataSource.DataSet do
      for i:= 0 to dmkDBGLct.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(dmkDBGLct.SelectedRows.Items[i]));
        TodoTexto := DmodFin.QrLctosDescricao.Value;
        if pos(Atual, TodoTexto) > 0 then
        begin
          TodoTexto := Geral.Substitui(TodoTexto, Atual, Novo);
          Dmod.QrUpd.Params[00].AsString  := TodoTexto;
          //
          Dmod.QrUpd.Params[01].AsInteger := DmodFin.QrLctosControle.Value;
          Dmod.QrUpd.Params[02].AsInteger := DmodFin.QrLctosSub.Value;
          Dmod.QrUpd.ExecSQL;
        end;
      end;
    end;
    DModFin.VeSeReabreLct(TPDataIni, TPDataFim, CBUH.KeyValue,
    FLAN_Controle, FLAN_Sub, DmodFin.QrCarts, DmodFin.QrLctos);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSelfGer.PagarAVistaEmCaixa;
  procedure InsereEmLctoEdit();
  begin
    DModG.QrUpdPID1.Params[00].AsInteger := DmodFin.QrLctosControle.Value;
    DModG.QrUpdPID1.Params[01].AsInteger := DmodFin.QrLctosSub.Value;
    DModG.QrUpdPID1.Params[02].AsString  := DmodFin.QrLctosDescricao.Value;
    DModG.QrUpdPID1.Params[03].AsString  := Geral.FDT(DmodFin.QrLctosData.Value, 1);
    DModG.QrUpdPID1.Params[04].AsString  := Geral.FDT(DmodFin.QrLctosVencimento.Value, 1);
    DModG.QrUpdPID1.Params[05].AsFloat   := DmodFin.QrLctosCredito.Value - DmodFin.QrLctosDebito.Value;
    //
    DModG.QrUpdPID1.ExecSQL;
  end;
  var
    i: Integer;
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('DELETE FROM lctoedit ');
  DModG.QrUpdPID1.ExecSQL;
  //
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO lctoedit SET ');
  DModG.QrUpdPID1.SQL.Add('Controle=:P0, Sub=:P1, Descricao=:P2, Data=:P3, ');
  DModG.QrUpdPID1.SQL.Add('Vencimento=:P4, ValorOri=:P5');
  DModG.QrUpdPID1.SQL.Add('');
  if dmkDBGLct.SelectedRows.Count > 1 then
  begin
    with dmkDBGLct.DataSource.DataSet do
    for i:= 0 to dmkDBGLct.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(dmkDBGLct.SelectedRows.Items[i]));
      InsereEmLctoEdit();
    end;

  end else InsereEmLctoEdit();
  if DBCheck.CriaFm(TFmLctPgEmCxa, FmLctPgEmCxa, afmoNegarComAviso) then
  begin
    FmLctPgEmCxa.EdMulta.Text := Geral.FFT(DmodFin.QrLctosMulta.Value, 6, siPositivo);
    FmLctPgEmCxa.EdTaxaM.Text := Geral.FFT(DmodFin.QrLctosMoraDia.Value, 6, siPositivo);
    FmLctPgEmCxa.ShowModal;
    FmLctPgEmCxa.Destroy;
  end;
end;

procedure TFmSelfGer.PagarReceber1Click(Sender: TObject);
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  if UFinanceiro.AtualizaPagamentosAVista(Dmod.QrLct) then
  begin
    if DBCheck.CriaFm(TFmExtratos, FmExtratos, afmoNegarComAviso) then
    begin
      FmExtratos.ShowModal;
      FmExtratos.Destroy;
    end;
  end;
end;

procedure TFmSelfGer.Recibo1Click(Sender: TObject);
begin
  if DmodFin.QrLctosCredito.Value > 0 then
    GOTOy.EmiteRecibo(DmodFin.QrLctosCliente.Value, DmodFin.QrLctosCliInt.Value,
    DmodFin.QrLctosCredito.Value, 0, 0, IntToStr(DmodFin.QrLctosControle.Value) + '-' +
    IntToStr(DmodFin.QrLctosSub.Value), DmodFin.QrLctosDescricao.Value, '', '',
    DmodFin.QrLctosData.Value, DmodFin.QrLctosSit.Value);
end;

procedure TFmSelfGer.RetornoCNAB1Click(Sender: TObject);
begin
  FmPrincipal.RetornoCNAB;
end;

procedure TFmSelfGer.Datalancto1Click(Sender: TObject);
begin
  Colocarmsdecompetnciaondenotem(0);
end;

procedure TFmSelfGer.Vencimento1Click(Sender: TObject);
begin
  Colocarmsdecompetnciaondenotem(1);
end;

procedure TFmSelfGer.Colocarmsdecompetnciaondenotem(Tipo: Integer);
var
  Data: TDateTime;
  Ano, Mes, Dia: Word;
begin
  if Application.MessageBox(PChar('Somente os lan�amentos presentes na grade ' +
  ' (conforme sele��a de carteira e per�odo) ser�o analisados! Deseja ' +
  'continuar assim mesmo?'), 'Pergunta', MB_YESNOCANCEL + MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Mez=:P0 ');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1 ');
    Dmod.QrUpd.SQL.Add('');
    DmodFin.QrLctos.First;
    while not DmodFin.QrLctos.Eof do
    begin
      Application.ProcessMessages;
      DmodFin.QrLocCta.Close;
      DmodFin.QrLocCta.Params[0].AsInteger := DmodFin.QrLctosGenero.Value;
      DmodFin.QrLocCta.Open;
      if (Uppercase(DmodFin.QrLocCtaMensal.Value) = 'V')
      and (DmodFin.QrLctosMez.Value = 0) then
      begin
        case Tipo of
          1: {Vencimento} Data := DmodFin.QrLctosVencimento.Value;
          else {Data}     Data := DmodFin.QrLctosData.Value;
        end;
        DecodeDate(Data, Ano, Mes, Dia);
        while ano > 100 do Ano := Ano - 100;
        Dmod.QrUpd.Params[00].AsInteger := Ano * 100 + Mes;
        Dmod.QrUpd.Params[01].AsInteger := DmodFin.QrLctosControle.Value;
        Dmod.QrUpd.ExecSQL;
      end;
      DmodFin.QrLctos.Next;
    end;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmSelfGer.BtIncluiClick(Sender: TObject);
begin
  VerificaCarteirasCliente();
  VerificaBotoes();
  // Ver juros e multa da construtora
  if UFinanceiro.InclusaoLancamento(TFmLctEdit, FmLctEdit,
  lfProprio, afmoNegarComAviso, DmodFin.QrLctos, DmodFin.QrCarts,
  tgrInclui, 0, 0, DmodFin.QrLctosGenero.Value,
  Dmod.QrControleMyPerJuros.Value, Dmod.QrControleMyPerMulta.Value, nil,
  0, 0, 0, 0, 0, 0, False,
  0(*Cliente*), 0(*Fornecedor*), DmodFin.QrCartsForneceI.Value(*cliInt*),
  0(*ForneceI*), 0(*Account*), 0(*Vendedor*), True(*LockCliInt*),
  False(*LockForneceI*), False(*LockAccount*), False(*LockVendedor*),
  0, 0, DmodFin.QrLctosCompensado.Value, 0, 1, 0) > 0 then
    DModFin.VeSeReabreLct(TPDataIni, TPDataFim, CBUH.KeyValue,
    FLAN_Controle, FLAN_Sub, DmodFin.QrCarts, DmodFin.QrLctos);
  {FmPrincipal.IncluiNovoLancto(
    Dmod.QrControleMyPerJuros.Value, Dmod.QrControleMyPerMulta.Value,
    TPDataIni, TPDataFim, CBUH.KeyValue, FmPrincipal.F_EntInt,
    FFinalidade);
   }
end;

procedure TFmSelfGer.BtLctoEndossClick(Sender: TObject);
begin
  (*
  if DBCheck.CriaFm(TFmLctoEndoss, FmLctoEndoss, afmoNegarComAviso) then
  begin
    FmLctoEndoss.FControle  := DmodFin.QrLctosControle.Value;
    FmLctoEndoss.FSub       := DmodFin.QrLctosSub.Value;
    FmLctoEndoss.FTPDataIni := TPDataIni;
    FmLctoEndoss.FCarteiras := DmodFin.QrCarts;
    FmLctoEndoss.FLct       := DmodFin.QrLctos;
    //
    FmLctoEndoss.ReopenLancto(0, 0, True);
    //
    FmLctoEndoss.ShowModal;
    FmLctoEndoss.Destroy;
  end;
  *)
end;

procedure TFmSelfGer.BtRelatClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRelat, BtRelat);
end;

procedure TFmSelfGer.BtAlteraClick(Sender: TObject);
begin
{
  if UFinanceiro.InsAltLancamento(TFmLctEdit, FmLctEdit,
  lfProprio, afmoNegarComAviso, DmodFin.QrLctos, DmodFin.QrCarts,
  tgrAltera, DmodFin.QrLctosControle.Value, DmodFin.QrLctosSub.Value,
  DmodFin.QrLctosGenero.Value, Dmod.QrControleMyPerJuros.Value,
  Dmod.QrControleMyPerMulta.Value, nil,
  0, 0, 0, 0, 0, 0, False,
  0(*Cliente*), 0(*Fornecedor*), DmodFin.QrCartsForneceI.Value(*cliInt*),
  0(*ForneceI*), 0(*Account*), 0(*Vendedor*), True(*LockCliInt*),
  False(*LockForneceI*), False(*LockAccount*), False(*LockVendedor*),
  0, 0, DmodFin.QrLctosCompensado.Value, 0, 1, 0) > 0 then
    DModFin.VeSeReabreLct(TPDataIni, TPDataFim, CBUH.KeyValue,
    FLAN_Controle, FLAN_Sub, DmodFin.QrCarts, DmodFin.QrLctos);
}
  UFinanceiro.AlteracaoLancamento(DmodFin.QrCarts, DmodFin.QrLctos, lfProprio,
    0(*OneAccount*), DmodFin.QrCartsForneceI.Value(*OneCliInt*), True)
end;

procedure TFmSelfGer.BtExcluiClick(Sender: TObject);
begin 
  UFinanceiro.ExcluiLanctoGrade(TDBGrid(dmkDBGLct),
    DmodFin.QrLctos, DmodFin.QrCarts, False);
end;

procedure TFmSelfGer.BtConciliaClick(Sender: TObject);
begin
  // Criar carteira de concilia��o
  if DBCheck.CriaFm(TFmConcilia, FmConcilia, afmoNegarComAviso) then
  begin
    FmConcilia.FQrCarteiras  := DmodFin.QrCarts;
    FmConcilia.FQrLct    := DmodFin.QrLctos;
    FmConcilia.ReopenConcilia(0);
    //FmConcilia.FCartConcil := QrCondCartConcil.Value;
    FmConcilia.FCartConcilia := 0;
    FmConcilia.FMostra       := FmConcilia.QrConcilia0.RecordCount > 0;
    FmConcilia.F_CliInt      := DmodFin.QrCartsForneceI.Value;
    FmConcilia.FDTPicker     := TPDataIni;
    FmConcilia.ShowModal;
    FmConcilia.Destroy;
  end;
end;

procedure TFmSelfGer.BtQuitaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMQuita, BtQuita);
end;

procedure TFmSelfGer.Compensar2Click(Sender: TObject);
begin
  CompensarContaCorrenteBanco;
end;

procedure TFmSelfGer.Reverter2Click(Sender: TObject);
begin
  ReverterCompensacao;
end;

procedure TFmSelfGer.Pagar2Click(Sender: TObject);
begin
  DmodFin.PagarRolarEmissao(DmodFin.QrLctos, DmodFin.QrCarts);
end;

procedure TFmSelfGer.PagarAVista2Click(Sender: TObject);
begin
  PagarAVistaEmCaixa;
end;

procedure TFmSelfGer.BtCNABClick(Sender: TObject);
begin
  FmPrincipal.RetornoCNAB;
end;

procedure TFmSelfGer.BtCopiaCHClick(Sender: TObject);
begin
  FmFinForm.ImprimeCopiaDeCh(DmodFin.QrLctosControle.Value,
    DmodFin.QrLctosCliInt.Value);
end;

procedure TFmSelfGer.TPDataIniChange(Sender: TObject);
begin
  DmodFin.FTipoData := RGTipoData.ItemIndex;
  //
  DModFin.VeSeReabreLct(TPDataIni, TPDataFim, CBUH.KeyValue,
  DmodFin.QrLctosControle.Value, DmodFin.QrLctosSub.Value,
  DmodFin.QrCarts, DmodFin.QrLctos);
end;

procedure TFmSelfGer.CBUHClick(Sender: TObject);
begin
  DModFin.VeSeReabreLct(TPDataIni, TPDataFim, CBUH.KeyValue,
  DmodFin.QrLctosControle.Value, DmodFin.QrLctosSub.Value,
  DmodFin.QrCarts, DmodFin.QrLctos);
end;

procedure TFmSelfGer.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Geral.WriteAppKeyLM2('Dias', Application.Title, Date - TPDataIni.Date, ktInteger);
  //
  DmodFin.FTPDataIni := nil;
  DmodFin.FTPDataFim := nil;
end;

procedure TFmSelfGer.FormCreate(Sender: TObject);
var
  Imagem: String;
begin
  DmodFin.FTPDataIni           := TPDataIni;
  DmodFin.FTPDataFim           := TPDataFim;
  FFinalidade                  := idflIndefinido;
  Application.OnHint           := ShowHint;
  PageControl1.ActivePageIndex := 0;
  DBGCarteiras.DataSource      := DmodFin.DsCarts;
  DBGCarts1.DataSource         := DmodFin.DsCarts;
  DBGCarts2.DataSource         := DmodFin.DsCarts;
  EdCodigo.DataSource          := DmodFin.DsCarts;
  EdNome.DataSource            := DmodFin.DsCarts;
  EdSaldo.DataSource           := DmodFin.DsCarts;
  EdDiferenca.DataSource       := DmodFin.DsCarts;
  EdCaixa.DataSource           := DmodFin.DsCarts;
  //
  dmkDBGLct.DataSource         := DmodFin.DsLctos;
  DBText1.DataSource           := DmodFin.DsLctos;
  DBText2.DataSource           := DmodFin.DsLctos;
  DBText3.DataSource           := DmodFin.DsLctos;
  DBText4.DataSource           := DmodFin.DsLctos;
  //
  DBGrid17.DataSource          := DmodFin.DsCarts;
  DBGrid18.DataSource          := DmodFin.DsCartSum;
  //
  //////////////////////////////////////////////////////////////////////////////
  TPDataIni.Date := Date - Geral.ReadAppKeyLM('Dias', Application.Title, ktInteger, 60);
  TPDataFim.Date := Date;
  //////////////////////////////////////////////////////////////////////////////
  //
  Imagem := Geral.ReadAppKeyLM('ImagemFundo', Application.Title, ktString, VAR_APP+'Fundo.jpg');
  //
  if FileExists(Imagem) then
  begin
    ImgSelfGer.Picture.LoadFromFile(Imagem);
    //FmPrincipal.AjustaImagem(ImgSelfGer);
  end;
  BtLctoEndoss.Visible := False;
end;

procedure TFmSelfGer.Saldos1Click(Sender: TObject);
begin
  DmodFin.ImprimeSaldos(DModFin.QrCartsForneceI.Value);
end;

procedure TFmSelfGer.SaldosEm1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSaldos, FmSaldos, afmoNegarComAviso) then
  begin
    FmSaldos.ShowModal;
    FmSaldos.Destroy;
  end;
end;

procedure TFmSelfGer.ShowHint(Sender: TObject);
begin
  if Length(Application.Hint) > 0 then
  begin
    StatusBar.SimplePanel := True;
    StatusBar.SimpleText := Application.Hint;
  end
  else StatusBar.SimplePanel := False;
end;

procedure TFmSelfGer.BtEspecificosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEspecificos, BtEspecificos);
end;

procedure TFmSelfGer.PMMenuPopup(Sender: TObject);
begin
  if DmodFin.QrLctosGenero.Value = -1 then begin
    Editar1.Enabled := True;
    Excluir1.Enabled := True;
  end else begin
    Editar1.Enabled  := False;
    Excluir1.Enabled := False;
  end;
  Compensar1.Enabled := False;
  Pagar1.Enabled     := False;
  Reverter1.Enabled  := False;
  if DmodFin.QrCartsTipo.Value = 2 then
  begin
    if DmodFin.QrLctosSit.Value in [0] then Compensar1.Enabled := True;
    if DmodFin.QrLctosSit.Value in [0,1,2] then Pagar1.Enabled := True;
    if DmodFin.QrLctosSit.Value in [3] then  Reverter1.Enabled := True;
  end;
  if dmkDBGLct.SelectedRows.Count > 0 then
  begin
    Mudacarteiradelanamentosselecionados1.Enabled := True;
    PagarAVista1.Enabled := True;
  end else begin
    Mudacarteiradelanamentosselecionados1.Enabled := False;
    PagarAVista1.Enabled := False;
  end;
  AlteraTrfCta1.Enabled := DmodFin.QrLctosFatID.Value = -1;
  ExcluiTrfCta1.Enabled := DmodFin.QrLctosFatID.Value = -1;
end;

procedure TFmSelfGer.BtRefreshClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRefresh, BtRefresh);
end;

procedure TFmSelfGer.AlteraTrfCta1Click(Sender: TObject);
begin
  UFinanceiro.CriarTransferCtas(1, DmodFin.QrLctos, DmodFin.QrCarts,
    True, DmodFin.QrCartsForneceI.Value, 0, 0, 0);
  UFinanceiro.RecalcSaldoCarteira(DmodFin.QrCartsCodigo.Value,
    DmodFin.QrCarts, True, True);
end;

procedure TFmSelfGer.Alteratransferncia1Click(Sender: TObject);
begin
  UFinanceiro.CriarTransferCtas(1, DmodFin.QrLctos, DmodFin.QrCarts,
    True, DmodFin.QrCartsForneceI.Value, 0, 0, 0);
  UFinanceiro.RecalcSaldoCarteira(DmodFin.QrCartsCodigo.Value,
    DmodFin.QrCarts, True, True);
end;

procedure TFmSelfGer.Atual5Click(Sender: TObject);
begin
  UFinanceiro.RecalcSaldoCarteira(DmodFin.QrCartsCodigo.Value,
    DmodFin.QrCarts, True, True);
end;

procedure TFmSelfGer.Todas1Click(Sender: TObject);
begin
  AtualizaTodasCarteiras();
end;

procedure TFmSelfGer.ContaPlanodecontas1Click(Sender: TObject);
begin
  //DModG.AtualizaContasEntidade(DmodFin.QrCartsForneceI.Value, 0, ProgressBar1, MeAtz);
end;

procedure TFmSelfGer.BtSaldoAquiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSaldoAqui, BtSaldoAqui);
end;

procedure TFmSelfGer.BtTrfCtaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTrfCta, BtTrfCta);
end;

procedure TFmSelfGer.Calcula1Click(Sender: TObject);
begin
  EdSdoAqui.Text := UFinanceiro.SaldoAqui(1, EdSdoAqui.Text,
    DmodFin.QrLctos, DmodFin.QrCarts);
end;

procedure TFmSelfGer.Limpa1Click(Sender: TObject);
begin
  EdSdoAqui.Text := UFinanceiro.SaldoAqui(0, EdSdoAqui.Text,
    DmodFin.QrLctos, DmodFin.QrCarts);
end;

procedure TFmSelfGer.Diferena1Click(Sender: TObject);
begin
  EdSdoAqui.Text := UFinanceiro.SaldoAqui(2, EdSdoAqui.Text,
    DmodFin.QrLctos, DmodFin.QrCarts);
end;

procedure TFmSelfGer.BtContarDinheiroClick(Sender: TObject);
begin
  UFinanceiro.MudaValorEmCaixa(
    DmodFin.QrLctos, DmodFin.QrCarts);
end;

procedure TFmSelfGer.BtFluxoCxaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFluxoCxa, FmFluxoCxa, afmoNegarComAviso) then
  begin
    FmFluxoCxa.FEntCod := DmodFin.QrCartsForneceI.Value;
    FmFluxoCxa.FConCod := 0;
    FmFluxoCxa.ShowModal;
    FmFluxoCxa.Destroy;
  end;
end;

procedure TFmSelfGer.BtAutomClick(Sender: TObject);
begin
  UFinanceiro.QuitacaoAutomaticaDmk(dmkDBGLct,
    DmodFin.QrLctos, DmodFin.QrCarts);
end;

procedure TFmSelfGer.BtBalanceteClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCashBal, FmCashBal, afmoNegarComAviso) then
  begin
    FmCashBal.ShowModal;
    FmCashBal.Destroy;
  end;
end;

{
function TFmSelfGer.ReabreLct(): Boolean;
begin
  DModFin.VeSeReabreLct(TPDataIni, TPDataFim, CBUH.KeyValue,
  FLAN_Controle, FLAN_Sub, DmodFin.QrCarts);
  Result := True;
end;
}

procedure TFmSelfGer.TPDataFimChange(Sender: TObject);
begin
  DmodFin.FTipoData := RGTipoData.ItemIndex;
  //
  DModFin.VeSeReabreLct(TPDataIni, TPDataFim, CBUH.KeyValue,
  DmodFin.QrLctosControle.Value, DmodFin.QrLctosSub.Value,
  DmodFin.QrCarts, DmodFin.QrLctos);
end;

procedure TFmSelfGer.FormDestroy(Sender: TObject);
begin
  Application.OnHint := FmPrincipal.ShowHint;
end;

end.

