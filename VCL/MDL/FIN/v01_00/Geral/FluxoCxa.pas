unit FluxoCxa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, Grids, dmkGeral,
  DBGrids, Db, mySQLDbTables, Mask, DBCtrls, frxClass, frxDBSet, Variants,
  UnDmkProcFunc, UnDmkEnums;

type
  TFmFluxoCxa = class(TForm)
    PainelConfirma: TPanel;
    BtPesquisa: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    QrFluxo: TmySQLQuery;
    DsFluxo: TDataSource;
    QrInicial: TmySQLQuery;
    QrInicialValor: TFloatField;
    Panel5: TPanel;
    Panel6: TPanel;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    QrMovant: TmySQLQuery;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    QrMovantMovim: TFloatField;
    QrMovantVALOR: TFloatField;
    Label7: TLabel;
    DBEdit3: TDBEdit;
    QrFluxoQUITACAO: TWideStringField;
    QrFluxoData: TDateField;
    QrFluxoTipo: TSmallintField;
    QrFluxoCarteira: TIntegerField;
    QrFluxoControle: TIntegerField;
    QrFluxoSub: TSmallintField;
    QrFluxoAutorizacao: TIntegerField;
    QrFluxoGenero: TIntegerField;
    QrFluxoQtde: TFloatField;
    QrFluxoDescricao: TWideStringField;
    QrFluxoNotaFiscal: TIntegerField;
    QrFluxoDebito: TFloatField;
    QrFluxoCredito: TFloatField;
    QrFluxoCompensado: TDateField;
    QrFluxoSerieCH: TWideStringField;
    QrFluxoDocumento: TFloatField;
    QrFluxoSit: TIntegerField;
    QrFluxoVencimento: TDateField;
    QrFluxoFatID: TIntegerField;
    QrFluxoFatID_Sub: TIntegerField;
    QrFluxoFatParcela: TIntegerField;
    QrFluxoID_Pgto: TIntegerField;
    QrFluxoID_Sub: TSmallintField;
    QrFluxoFatura: TWideStringField;
    QrFluxoEmitente: TWideStringField;
    QrFluxoBanco: TIntegerField;
    QrFluxoContaCorrente: TWideStringField;
    QrFluxoCNPJCPF: TWideStringField;
    QrFluxoLocal: TIntegerField;
    QrFluxoCartao: TIntegerField;
    QrFluxoLinha: TIntegerField;
    QrFluxoOperCount: TIntegerField;
    QrFluxoLancto: TIntegerField;
    QrFluxoPago: TFloatField;
    QrFluxoMez: TIntegerField;
    QrFluxoFornecedor: TIntegerField;
    QrFluxoCliente: TIntegerField;
    QrFluxoCliInt: TIntegerField;
    QrFluxoForneceI: TIntegerField;
    QrFluxoMoraDia: TFloatField;
    QrFluxoMulta: TFloatField;
    QrFluxoMoraVal: TFloatField;
    QrFluxoMultaVal: TFloatField;
    QrFluxoProtesto: TDateField;
    QrFluxoDataDoc: TDateField;
    QrFluxoCtrlIni: TIntegerField;
    QrFluxoNivel: TIntegerField;
    QrFluxoVendedor: TIntegerField;
    QrFluxoAccount: TIntegerField;
    QrFluxoICMS_P: TFloatField;
    QrFluxoICMS_V: TFloatField;
    QrFluxoDuplicata: TWideStringField;
    QrFluxoDepto: TIntegerField;
    QrFluxoDescoPor: TIntegerField;
    QrFluxoDescoVal: TFloatField;
    QrFluxoDescoControle: TIntegerField;
    QrFluxoUnidade: TIntegerField;
    QrFluxoNFVal: TFloatField;
    QrFluxoAntigo: TWideStringField;
    QrFluxoExcelGru: TIntegerField;
    QrFluxoDoc2: TWideStringField;
    QrFluxoCNAB_Sit: TSmallintField;
    QrFluxoTipoCH: TSmallintField;
    QrFluxoLk: TIntegerField;
    QrFluxoDataCad: TDateField;
    QrFluxoDataAlt: TDateField;
    QrFluxoUserCad: TIntegerField;
    QrFluxoUserAlt: TIntegerField;
    PB1: TProgressBar;
    QrFluxoNOMECART: TWideStringField;
    QrLct: TmySQLQuery;
    QrExtrato: TmySQLQuery;
    DsExtrato: TDataSource;
    QrExtratoDataE: TDateField;
    QrExtratoDataV: TDateField;
    QrExtratoDataQ: TDateField;
    QrExtratoTexto: TWideStringField;
    QrExtratoDocum: TWideStringField;
    QrExtratoNotaF: TWideStringField;
    QrExtratoCredi: TFloatField;
    QrExtratoDebit: TFloatField;
    QrExtratoSaldo: TFloatField;
    QrExtratoCartC: TIntegerField;
    QrExtratoCartN: TWideStringField;
    QrExtratoCodig: TIntegerField;
    QrExtratoCtrle: TIntegerField;
    QrExtratoCtSub: TIntegerField;
    QrExtratoCART_ORIG: TWideStringField;
    QrExtratoDataX: TDateField;
    QrExtratoID_Pg: TIntegerField;
    QrExtratoTipoI: TIntegerField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGPesq: TDBGrid;
    DBGFluxo: TDBGrid;
    frxExtrato: TfrxReport;
    frxDsExtrato: TfrxDBDataset;
    GroupBox1: TGroupBox;
    CkIni: TCheckBox;
    TPIni: TDateTimePicker;
    CkFim: TCheckBox;
    TPFim: TDateTimePicker;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    TPCre: TDateTimePicker;
    TPDeb: TDateTimePicker;
    Label3: TLabel;
    Label4: TLabel;
    BitBtn1: TBitBtn;
    TabSheet3: TTabSheet;
    DBGrid1: TDBGrid;
    QrPrevIts: TmySQLQuery;
    BitBtn2: TBitBtn;
    DsPrevIts: TDataSource;
    QrPrevItsPeriodo: TIntegerField;
    QrPrevItsControle: TIntegerField;
    QrPrevItsConta: TIntegerField;
    QrPrevItsValor: TFloatField;
    QrPrevItsPrevBaI: TIntegerField;
    QrPrevItsTexto: TWideStringField;
    QrPrevItsPrevBaC: TIntegerField;
    QrPrevItsEmitVal: TFloatField;
    QrPrevItsDIFER: TFloatField;
    QrPrevItsPERIODO_TXT: TWideStringField;
    QrRealiz: TmySQLQuery;
    QrRealizValor: TFloatField;
    QrFluxoAgencia: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure QrMovantCalcFields(DataSet: TDataSet);
    procedure QrExtratoBeforeOpen(DataSet: TDataSet);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrPrevItsCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FEntCod: Integer;
    FConCod: Integer;
  end;

  var
  FmFluxoCxa: TFmFluxoCxa;

implementation

uses UnMyObjects, Module, ModuleGeral, UCreate, UMySQLModule, UnInternalConsts;

{$R *.DFM}

procedure TFmFluxoCxa.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFluxoCxa.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFluxoCxa.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmFluxoCxa.FormCreate(Sender: TObject);
begin
  QrExtrato.Close;
  QrExtrato.Database := DModG.MyPID_DB;
  TPIni.Date := Int(Date);
  TPFim.Date := Int(Date);
  TPCre.Date := Int(Date);
  TPDeb.Date := Int(Date);
  //
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmFluxoCxa.BtPesquisaClick(Sender: TObject);
  function Fmt(Data: TDateTime): String;
  begin
    Result := '"' + FormatDateTime('YYYY-MM-DD', Data) + '"';
  end;
var
  Quitacao, Texto, Docum, NotaF: String;
  DataE, DataV, DataQ, DataX: Variant;
  Codig: Integer;
  Saldo: Double;
  DataI: TDateTime;
begin
  Screen.Cursor := crHourGlass;
  PB1.Position := 0;
  //   S A L D O    I N I C I A L
  QrInicial.Close;
  QrInicial.Params[0].AsInteger := FEntCod;
  QrInicial.Open;

  //   M O V I M E N T O
  if CkIni.Checked then DataI := TPIni.Date else DataI := 0;
  QrMovant.Close;
  QrMovant.Params[0].AsInteger := FEntCod;
  QrMovant.Params[1].AsString  := Geral.FDT(DataI, 1);
  QrMovant.Open;

  //   L A N � A M E N T O S
   Quitacao := 'Date(IF(car.Tipo <> 2, lan.Data,IF(lan.Sit > 1, lan.Compensado, ' +
  'IF(lan.Vencimento<SYSDATE(), IF(lan.Debito>0, ' + Fmt(TPDeb.Date) +
  ', ' + Fmt(TPCre.Date) + '), lan.Vencimento))))';
  //
  QrFluxo.Close;
  QrFluxo.SQL.Clear;
  QrFluxo.SQL.Add('SELECT ' + Quitacao + ' QUITACAO, ');
  QrFluxo.SQL.Add('lan.*, car.Nome NOMECART');
  QrFluxo.SQL.Add('FROM ' + VAR_LCT + ' lan');
  QrFluxo.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  //
  QrFluxo.SQL.Add('WHERE car.Fornecei=' + IntToStr(FEntCod));
  QrFluxo.SQL.Add(dmkPF.SQL_Periodo('AND ' + Quitacao,
    TPIni.Date, TPFim.Date, CkIni.Checked, CkFim.Checked));
  QrFluxo.SQL.Add('AND lan.Genero > 0');
  QrFluxo.SQL.Add('AND ((car.Tipo<>2) OR (lan.Sit < 2))');
  //
  QrFluxo.SQL.Add('ORDER BY QUITACAO, Credito DESC, Debito');
  QrFluxo.Open;

  // INSERE DADOS TABELA LOCAL
  UCriar.RecriaTempTable('extratocc2', DModG.QrUpdPID1, False);
  {
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('DELETE FROM extratocc2');
  DModG.QrUpdPID1.ExecSQL;
  }
  //
  Codig := 1;
  PB1.Position := Codig;
  PB1.Update;
  Application.ProcessMessages;
  //
  Saldo := QrMovantVALOR.Value;
  UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'ExtratoCC2', false,
  [
    'DataE', 'DataV', 'DataQ',
    'DataX', 'Texto', 'Docum',
    'NotaF', 'Credi', 'Debit',
    'Saldo', 'CartC', 'CartN',
    'TipoI', 'Ctrle', 'ID_Pg'
  ], ['Codig'], [
    Null, Null, Null,
    MLAGeral.FDT_NULL(DataI, 1), 'SALDO ANTERIOR', '',
    '', 0, 0,
    Saldo, 0, '*** T O D A S ***', 1, 0, 0
  ], [Codig], True);
  //
  QrFluxo.First;
  while not QrFluxo.Eof do
  begin
    Codig := Codig + 1;
    PB1.Position := Codig;
    PB1.Update;
    Application.ProcessMessages;
    Saldo := Saldo + QrFluxoCredito.Value - QrFluxoDebito.Value;
    DataE := MLAGeral.FDT_NULL(QrFluxoData.Value, 1);
    DataV := MLAGeral.FDT_NULL(QrFluxoVencimento.Value, 1);
    DataQ := MLAGeral.FDT_NULL(QrFluxoCompensado.Value, 1);
    DataX := QrFluxoQUITACAO.Value;
    //
    Texto := QrFluxoDescricao.Value;
    if QrFluxoQtde.Value > 0 then
      Texto := FloatToStr(QrFluxoQtde.Value) + ' ' + Texto;
    if QrFluxoDocumento.Value > 0 then
      Docum := FormatFloat('000000', QrFluxoDocumento.Value) else Docum := '';
    if QrFluxoSerieCH.Value <> '' then Docum := QrFluxoSerieCH.Value + Docum;
    if QrFluxoNotaFiscal.Value > 0 then
      NotaF := FormatFloat('000000', QrFluxoNotaFiscal.Value) else NotaF := '';
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'ExtratoCC2', false,
    [
      'DataE', 'DataV', 'DataQ',
      'DataX', 'Texto', 'Docum',
      'NotaF', 'Credi', 'Debit',
      'Saldo', 'CartC', 'CartN',
      'TipoI', 'Ctrle', 'ID_Pg', 'CtSub'
    ], ['Codig'], [
      DataE, DataV, DataQ,
      DataX, Texto, Docum,
      NotaF, QrFluxoCredito.Value, QrFluxoDebito.Value,
      Saldo, QrFluxoCarteira.Value, QrFluxoNOMECART.Value,
      2, QrFluxoControle.Value, QrFluxoID_Pgto.Value, QrFluxoSub.Value
    ], [Codig], True);
    //
    QrFluxo.Next;
  end;
  PB1.Max := QrFluxo.RecordCount + 1;
  PageControl1.ActivepageIndex := 1;
  QrExtrato.Close;
  QrExtrato.Open;
  Screen.Cursor := crDefault;
end;

procedure TFmFluxoCxa.QrMovantCalcFields(DataSet: TDataSet);
begin
  QrMovantVALOR.Value := QrMovantMovim.Value + QrInicialValor.Value;
end;

procedure TFmFluxoCxa.QrExtratoBeforeOpen(DataSet: TDataSet);
begin
  QrLct.Close;
  QrLct.Params[0].AsInteger := FEntCod;
  QrLct.Open;
end;

procedure TFmFluxoCxa.QrPrevItsCalcFields(DataSet: TDataSet);
begin
  QrPrevItsPERIODO_TXT.Value := dmkPF.PeriodoToMensal(QrprevItsPeriodo.Value);
end;

procedure TFmFluxoCxa.BitBtn1Click(Sender: TObject);
var
  DataI, DataF: String;
  Sit: Integer;
  Valor: Double;
begin
  Screen.Cursor := crHourGlass;
  //
  PageControl1.ActivePageIndex := 2;
  Update;
  Application.ProcessMessages;
  //
  QrPrevIts.Close;
  QrPrevIts.Params[0].AsInteger := FConCod;
  QrPrevIts.Open;
  PB1.Position := 0;
  PB1.Max := QrPrevIts.RecordCount;
  Update;
  Application.ProcessMessages;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE previts SET EmitVal=:P0, EmitSit=:P1 ');
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa');
  while not QrPrevIts.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    Update;
    Application.ProcessMessages;
    DataI := Geral.FDT(MLAGeral.PrimeiroDiaDoPeriodo_Date(QrPrevItsPeriodo.Value), 1);
    DataF := Geral.FDT(MLAGeral.UltimoDiaDoPeriodo_Date(QrPrevItsPeriodo.Value), 1);
    QrRealiz.Close;
    QrRealiz.Params[00].AsInteger := FEntCod;
    QrRealiz.Params[01].AsInteger := QrPrevItsConta.Value;
    QrRealiz.Params[02].AsInteger := MLAGeral.PeriodoToMez(QrPrevItsPeriodo.Value);
    QrRealiz.Params[03].AsString  := DataI;
    QrRealiz.Params[04].AsString  := DataF;
    QrRealiz.Open;
    //
    Valor := QrRealizValor.Value;
    if Valor < 0 then Valor := - Valor;
    if Valor >= QrPrevItsValor.Value then Sit := 1 else Sit := 0;
    //
    Dmod.QrUpd.Params[00].AsFloat   := Valor;
    Dmod.QrUpd.Params[01].AsInteger := Sit;
    Dmod.QrUpd.Params[02].AsInteger := QrPrevItsControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    QrPrevIts.Next;
  end;
  //
  QrPrevIts.Close;
  QrPrevIts.Params[0].AsInteger := FConCod;
  QrPrevIts.Open;
  //
  PB1.Position := 0;
  Screen.Cursor := crDefault;
end;

end.

