unit CartPgt2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Grids, DBGrids, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, ZCF2, UnInternalConsts, UnMsgInt, UnInternalConsts2, UnMyLinguas,
  mySQLDbTables, ComCtrls, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkEditDateTimePicker, UMySQLModule, dmkImage, UnDmkEnums;

type
  TFmCartPgt2 = class(TForm)
    Panel1: TPanel;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    DBEdit5: TDBEdit;
    Label7: TLabel;
    DBEdit4: TDBEdit;
    Label1: TLabel;
    EdControle: TDBEdit;
    DBEdit7: TDBEdit;
    Label5: TLabel;
    QrPagtos: TMySQLQuery;
    QrPagtosData: TDateField;
    QrPagtosTipo: TSmallintField;
    QrPagtosCarteira: TIntegerField;
    QrPagtosAutorizacao: TIntegerField;
    QrPagtosGenero: TIntegerField;
    QrPagtosDescricao: TWideStringField;
    QrPagtosNotaFiscal: TIntegerField;
    QrPagtosDebito: TFloatField;
    QrPagtosCredito: TFloatField;
    QrPagtosCompensado: TDateField;
    QrPagtosDocumento: TFloatField;
    QrPagtosSit: TIntegerField;
    QrPagtosVencimento: TDateField;
    QrPagtosLk: TIntegerField;
    QrPagtosFatID: TIntegerField;
    QrPagtosFatParcela: TIntegerField;
    LaControle: TLabel;
    Label6: TLabel;
    DsPagtos: TDataSource;
    QrSoma: TMySQLQuery;
    LaCred: TLabel;
    EdDeb: TdmkEdit;
    LaDeb: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCred: TdmkEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    QrSomaCredito: TFloatField;
    QrSomaDebito: TFloatField;
    QrPagtosNOMESIT: TWideStringField;
    QrPagtosAno: TFloatField;
    QrPagtosMENSAL: TWideStringField;
    QrPagtosMENSAL2: TWideStringField;
    Label12: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    Label13: TLabel;
    Label14: TLabel;
    DBEdit12: TDBEdit;
    Label15: TLabel;
    DBEdit13: TDBEdit;
    Label16: TLabel;
    DBEdit14: TDBEdit;
    Label17: TLabel;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    Label18: TLabel;
    QrPagtosMoraDia: TFloatField;
    QrPagtosMulta: TFloatField;
    QrPagtosSub: TSmallintField;
    QrPagtosCartao: TIntegerField;
    QrPagtosControle: TIntegerField;
    QrPagtosID_Pgto: TIntegerField;
    QrPagtosMes2: TLargeintField;
    DataSource1: TDataSource;
    QrDescoPor: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    IntegerField4: TIntegerField;
    DsDescoPor: TDataSource;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    DsCarteiras: TDataSource;
    QrCarteirasTipo: TIntegerField;
    QrCarteirasForneceI: TIntegerField;
    DBEdit6: TDBEdit;
    Label21: TLabel;
    QrPagtosSerieCH: TWideStringField;
    QrPagtosMoraVal: TFloatField;
    QrPagtosMultaVal: TFloatField;
    DBGPagtos: TDBGrid;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    PnSaiDesis: TPanel;
    BtInclui: TBitBtn;
    BtExclui: TBitBtn;
    Label10: TLabel;
    EdSaldoCred: TdmkEdit;
    Label11: TLabel;
    EdSaldoDeb: TdmkEdit;
    BtSaida: TBitBtn;
    BtAltera: TBitBtn;
    Panel3: TPanel;
    LaDescoVal: TLabel;
    LaDescoPor: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    BtPagtosAltera: TBitBtn;
    EdDescoVal: TdmkEdit;
    CBDescoPor: TdmkDBLookupComboBox;
    EdDescoPor: TdmkEditCB;
    EdCarteiraDesco: TdmkEditCB;
    CBCarteiraDesco: TdmkDBLookupComboBox;
    TPData: TdmkEditDateTimePicker;
    QrSomaMulta: TFloatField;
    QrSomaJuros: TFloatField;
    QrSomaDesconto: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure QrPagtosAfterOpen(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure QrPagtosCalcFields(DataSet: TDataSet);
    procedure QrPagtosAfterScroll(DataSet: TDataSet);
    procedure DBGPagtosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormResize(Sender: TObject);
    procedure BtPagtosAlteraClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    procedure EditaPagtos(Estado: Integer);
    procedure ConfiguraDBGCarteira;
    procedure AtualizaEmissaoMaster;
  public
    { Public declarations }
    FQrLct, FQrCarteiras: TmySQLQuery;
    procedure PagtosReopen(Lancto: Double);
  end;

var
  FmCartPgt2: TFmCartPgt2;

implementation

uses UnMyObjects, Module, CartPgt2Edit, Principal, UnFinanceiro, MyDBCheck,
  ModuleFin;

{$R *.DFM}

procedure TFmCartPgt2.ConfiguraDBGCarteira;
var
  i: Integer;
begin
  for i := 0 to DBGPagtos.Columns.Count -1 do
  begin
    if DBGPagtos.Columns[i].FieldName = 'Documento' then
    begin
      if QrPagtosTipo.Value = 0 then DBGPagtos.Columns[i].Visible := False
      else DBGPagtos.Columns[i].Visible := True;
    end;
    if DBGPagtos.Columns[i].FieldName = 'NotaFiscal' then
    begin
      if QrPagtosTipo.Value = 1 then DBGPagtos.Columns[i].Visible := False
      else DBGPagtos.Columns[i].Visible := True;
    end;
    if DBGPagtos.Columns[i].FieldName = 'Vencimento' then
    begin
      if QrPagtosTipo.Value = 2 then DBGPagtos.Columns[i].Visible := True
      else DBGPagtos.Columns[i].Visible := False;
    end;
    if DBGPagtos.Columns[i].FieldName = 'Compensado' then
    begin
      if QrPagtosTipo.Value = 2 then DBGPagtos.Columns[i].Visible := True
      else DBGPagtos.Columns[i].Visible := False;
    end;
    if DBGPagtos.Columns[i].FieldName = 'NOMESIT' then
    begin
      if QrPagtosTipo.Value = 2 then DBGPagtos.Columns[i].Visible := True
      else DBGPagtos.Columns[i].Visible := False;
    end;
  end;
end;

procedure TFmCartPgt2.EditaPagtos(Estado: Integer);
var
  Lancto: Int64;
  Sub, Genero, Cartao, Sit, Tipo, ID_Pgto, Carteira: Integer;
  Data: TDateTime;
  Valor, PerMulta, PerJuro, ValAtualiz, ValMulta, ValJuro: Double;
begin
  //if UMyMod.AcessoNegadoAoForm(ivTabPerfis, 'QuitaDocs', 0) then Exit;
  Lancto := QrPagtosControle.Value;
  Data   := QrPagtosData.Value;
  Sub    := QrPagtosSub.Value;
  Genero := QrPagtosGenero.Value;
  Cartao := QrPagtosCartao.Value;
  Sit    := QrPagtosSit.Value;
  Tipo   := QrPagtosTipo.Value;
  ID_Pgto := QrPagtosID_Pgto.Value;
  Carteira := QrPagtosCarteira.Value;
  //
  if UMyMod.SelLockInt64Y(Lancto, Dmod.MyDB, VAR_LCT, 'Controle') then Exit;
  UMyMod.UpdLockInt64Y(Lancto, Dmod.MyDB, VAR_LCT, 'Controle');
  Refresh;
  if Estado < 3 then
  begin
    if DBCheck.CriaFm(TFmCartPgt2Edit, FmCartPgt2Edit, afmoNegarComAviso) then
    begin
      with FmCartPgt2Edit do begin
        if Estado = 1 then
        begin
          Valor := Geral.DMV(EdSaldoDeb.ValueVariant);
          if Valor < 0 then
            Valor := Valor * -1
          else begin
            Valor := Geral.DMV(EdSaldoCred.ValueVariant);
            if Valor < 0 then
              Valor := Valor * -1;
          end;
          //
          PerMulta := FQrLct.FieldByName('Multa').AsFloat;
          PerJuro  := FQrLct.FieldByName('MoraDia').AsFloat;
          //
          UFinanceiro.CalculaValMultaEJuros(Valor, Date,
            FQrLct.FieldByName('Vencimento').AsDateTime, PerMulta, PerJuro,
            ValAtualiz, ValMulta, ValJuro, False);
          //
          LaTipo.Caption             := CO_INCLUSAO;
          EdOldControle.ValueVariant := FQrLct.FieldByName('Controle').AsInteger;
          EdOldSub.ValueVariant      := FQrLct.FieldByName('Sub').AsInteger;
          TPOldData.Date             := FQrLct.FieldByName('Vencimento').AsFloat;;
          EdOldTipo.ValueVariant     := FQrLct.FieldByName('Tipo').AsInteger;
          EdOldCarteira.ValueVariant := FQrLct.FieldByName('Carteira').AsInteger;
          //
          EdOldSaldo.ValueVariant    := Valor;
          TPData.Date                := MLAGeral.ObtemDataInserir;
          TPVencimento.Date          := Date;
          EdDescricao.Text           := FQrLct.FieldByName('Descricao').AsString;
          EdValor.ValueVariant       := ValAtualiz;
          EdMoraVal.ValueVariant     := ValJuro;
          EdMultaVal.ValueVariant    := ValMulta;
          EdMulta.ValueVariant       := PerMulta;
          EdMoraDia.ValueVariant     := PerJuro;
        end else begin
          {
          TPOldData.Date             := FQrLct.FieldByName('Data').AsDateTime;
          EdOldTipo.ValueVariant     := IntToStr(FQrLct.FieldByName('Tipo').AsInteger);
          EdOldCarteira.ValueVariant := IntToStr(FQrLct.FieldByName('Carteira').AsInteger);
          EdOldControle.ValueVariant := IntToStr(FQrLct.FieldByName('Controle').AsInteger);
          EdOldSub.ValueVariant      := IntToStr(FQrLct.FieldByName('Sub').AsInteger);
          }//
          TPOldData.Date             := QrPagtosData.Value;
          TPOldData.Time             := 0;
          EdOldTipo.ValueVariant     := QrPagtosTipo.Value;
          EdOldCarteira.ValueVariant := QrPagtosCarteira.Value;
          EdOldControle.ValueVariant := QrPagtosControle.Value;
          EdOldSub.ValueVariant      := QrPagtosSub.Value;
          //
          LaTipo.Caption       := CO_ALTERACAO;
          EdCodigo.Text        := Geral.TFT(FloatToStr(QrPagtosControle.Value), 0, siPositivo);
          TPData.Date          := QrPagtosData.Value;
          EdValor.Text         := Geral.TFT(FloatToStr(QrPagtosDebito.Value+QrPagtosCredito.Value), 2, siPositivo);
          //
          //EdMoraDia.Text       := Geral.TFT(FloatToStr(QrPagtosMoraDia.Value), 2, siPositivo);
          //EdMulta.Text         := Geral.TFT(FloatToStr(QrPagtosMulta.Value), 2, siPositivo);
          EdMoraDia.Text       := Geral.TFT(FloatToStr(QrPagtosMoraVal.Value), 2, siPositivo);
          EdMulta.Text         := Geral.TFT(FloatToStr(QrPagtosMultaVal.Value), 2, siPositivo);
          //
          EdDoc.Text           := Geral.TFT(FloatToStr(QrPagtosDocumento.Value), 0, siPositivo);
          EdSerieCH.Text       := QrPagtosSerieCH.Value;
          //EdDoc.Enabled        := True;
          //LaDoc.Enabled        := True;
          TPVencimento.Date    := QrPagtosVencimento.Value;
          EdDescricao.Text     := QrPagtosDescricao.Value;
          RGCarteira.ItemIndex := QrPagtosTipo.Value;
          EDCarteira.Text      := IntToStr(QrPagtosCarteira.Value);
          CBCarteira.KeyValue  := QrPagtosCarteira.Value;
        end;
      end;
      FmCartPgt2Edit.ShowModal;
      FmCartPgt2Edit.Destroy;
    end;
  end else begin
    if Geral.MensagemBox('Confirma exclus�o deste registro?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES
    then
      Exit;
    //
    UMyMod.UpdUnLockInt64Y(Lancto, Dmod.MyDB, VAR_LCT, 'Controle');
    UFinanceiro.ExcluiItemCarteira(Lancto, Data, Sub, Genero, Cartao, Sit,
    Tipo, (*Chamada*) 1, ID_Pgto, FQrLct, FQrCarteiras, False,
    FQrLct.FieldByName('Carteira').AsInteger, Carteira);
  end;
  PagtosReopen(Lancto);
  AtualizaEmissaoMaster;
  UMyMod.UpdUnLockInt64Y(Lancto, Dmod.MyDB, VAR_LCT, 'Controle');
end;

procedure TFmCartPgt2.PagtosReopen(Lancto: Double);
var
  ID_Pgto: Double;
begin
  ID_Pgto := Geral.DMV(Geral.TFT(LaControle.Caption, 0, siPositivo));
  QrPagtos.Close;
  QrPagtos.Params[0].AsFloat := ID_Pgto;
  QrPagtos.Open;
  QrPagtos.Locate('Controle', Lancto, []);
end;

procedure TFmCartPgt2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCartPgt2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  AtualizaEmissaoMaster;
end;

procedure TFmCartPgt2.BtIncluiClick(Sender: TObject);
begin
  EditaPagtos(1);
end;

procedure TFmCartPgt2.BtAlteraClick(Sender: TObject);
begin
  EditaPagtos(2);
end;

procedure TFmCartPgt2.BtExcluiClick(Sender: TObject);
begin
  EditaPagtos(3);
end;

procedure TFmCartPgt2.QrPagtosAfterOpen(DataSet: TDataSet);
begin
  if QrPagtos.RecordCount > 0 then
  begin
    BtAltera.Visible := False;
    BtExclui.Enabled := True;
  end else begin
    BtAltera.Visible := False;
    BtExclui.Enabled := False;
  end;
end;

procedure TFmCartPgt2.QrPagtosCalcFields(DataSet: TDataSet);
begin
  if QrPagtosMes2.Value > 0 then
    QrPagtosMENSAL.Value := FormatFloat('00', QrPagtosMes2.Value)+'/'
    +Copy(FormatFloat('0000', QrPagtosAno.Value), 3, 2)
   else QrPagtosMENSAL.Value := CO_VAZIO;
  if QrPagtosMes2.Value > 0 then
    QrPagtosMENSAL2.Value := FormatFloat('0000', QrPagtosAno.Value)+'/'+
    FormatFloat('00', QrPagtosMes2.Value)+'/01'
   else QrPagtosMENSAL2.Value := CO_VAZIO;

  QrPagtosNOMESIT.Value := UFinanceiro.NomeSitLancto(QrPagtosSit.Value,
    QrPagtosTipo.Value, 0(*QrPagtosPrazo.Value*), QrPagtosVencimento.Value,
    0(*QrPagtosReparcel.Value*));
end;

procedure TFmCartPgt2.QrPagtosAfterScroll(DataSet: TDataSet);
begin
  ConfiguraDBGCarteira;
end;

procedure TFmCartPgt2.DBGPagtosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if (Column.FieldName = 'NOMESIT') then
  begin
    if Column.Visible = True then
      MyObjects.DefineCorTextoSitLancto(TDBGrid(Sender), Rect, 'NOMESIT',
        QrPagtosNOMESIT.Value);
  end;
end;

procedure TFmCartPgt2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCartPgt2.BtPagtosAlteraClick(Sender: TObject);
var
  CarteiraDesco, Sit, Controle, DescoPor: Integer;
  Valor, DescoVal: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    {
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET DescoPor=:P0, DescoVal=:P1');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P2');
    Dmod.QrUpd.Params[00].AsInteger := Geral.IMV(EdDescoPor.Text);
    Dmod.QrUpd.Params[01].AsFloat   := Geral.DMV(EdDescoVal.Text);
    Dmod.QrUpd.Params[02].AsInteger := Geral.IMV(EdControle.Text);
    //Dmod.QrUpd.Params[03].AsInteger := ;
    Dmod.QrUpd.ExecSQL;
    }
    //
    DescoPor := Geral.IMV(EdDescoPor.Text);
    DescoVal := Geral.DMV(EdDescoVal.Text);
    Controle := Geral.IMV(EdControle.Text);
    UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
    'DescoPor', 'DescoVal'], ['Controle'], [
    DescoPor, DescoVal], [Controle], True, '');
    //
    CarteiraDesco := Geral.IMV(EdCarteiraDesco.Text);
    if CarteiraDesco > 0 then
    begin
      Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', VAR_LCT, VAR_LCT, 'Controle');
      if QrCarteirasTipo.Value = 2 then Sit := 0 else Sit := 3;
      //
{
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add(delete lct WHERE DescoControle=:P0');
      Dmod.QrUpd.Params[0].AsInteger := Geral.IMV(EdControle.Text);
      Dmod.QrUpd.ExecSQL;
}
      DmodFin.QrLcts.Close;
      DmodFin.QrLcts.SQL.Clear;
      DmodFin.QrLcts.SQL.Add('SELECT Data, Tipo, Carteira, Controle, Sub');
      DmodFin.QrLcts.SQL.Add('FROM lanctos');
      DmodFin.QrLcts.SQL.Add('WHERE DescoControle=:P0');
      DmodFin.QrLcts.Params[0].AsFloat := Geral.IMV(EdControle.Text);
      DmodFin.QrLcts.Open;
      while not DmodFin.QrLcts.Eof do
      begin
        UFinanceiro.ExcluiLct_Unico(True, Dmod.MyDB, DmodFin.QrLctsData.Value,
          DmodFin.QrLctsTipo.Value, DmodFin.QrLctsCarteira.Value,
          DmodFin.QrLctsControle.Value, DmodFin.QrLctsSub.Value, False);
        //
        DmodFin.QrLcts.Next;
      end;
      //
      Valor := Geral.DMV(EdDescoVal.Text);
      if Valor <> 0 then
      begin
        Dmod.QrUpdU.SQL.Clear;
        Dmod.QrUpdU.SQL.Add('INSERT INTO ' + VAR_LCT + ' SET ');
        Dmod.QrUpdU.SQL.Add('Data=:P0');
        Dmod.QrUpdU.SQL.Add(', Tipo=:P1');
        Dmod.QrUpdU.SQL.Add(', Carteira=:P2');
        Dmod.QrUpdU.SQL.Add(', Documento=:P3');
        Dmod.QrUpdU.SQL.Add(', Genero=:P4');
        Dmod.QrUpdU.SQL.Add(', Descricao=:P5');
        Dmod.QrUpdU.SQL.Add(', NotaFiscal=:P6');
        Dmod.QrUpdU.SQL.Add(', Debito=:P7');
        Dmod.QrUpdU.SQL.Add(', Credito=:P8');
        Dmod.QrUpdU.SQL.Add(', Compensado=:P9');
        Dmod.QrUpdU.SQL.Add(', Vencimento=:P10');
        Dmod.QrUpdU.SQL.Add(', Sit=:P11');
        Dmod.QrUpdU.SQL.Add(', Fornecedor=:P12');
        Dmod.QrUpdU.SQL.Add(', Cliente=:P13');
        Dmod.QrUpdU.SQL.Add(', MoraDia=:P14');
        Dmod.QrUpdU.SQL.Add(', Multa=:P15');
        Dmod.QrUpdU.SQL.Add(', Vendedor=:P16');
        Dmod.QrUpdU.SQL.Add(', Account=:P17');
        Dmod.QrUpdU.SQL.Add(', FatID_Sub=:P18');
        Dmod.QrUpdU.SQL.Add(', CliInt=:P19');
        //
        Dmod.QrUpdU.SQL.Add(', DescoControle=:P20');
        Dmod.QrUpdU.SQL.Add(', UserCad=:Pa');
        Dmod.QrUpdU.SQL.Add(', Controle=:Pb');
        //
        Dmod.QrUpdU.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TPData.Date);
        Dmod.QrUpdU.Params[01].AsInteger := QrCarteirasTipo.Value;
        Dmod.QrUpdU.Params[02].AsInteger := QrCarteirasCodigo.Value;
        Dmod.QrUpdU.Params[03].AsFloat   := 0;
        Dmod.QrUpdU.Params[04].AsInteger := -9; // Desconto duplicata
        Dmod.QrUpdU.Params[05].AsString  := 'Desconto de duplicata';
        Dmod.QrUpdU.Params[06].AsInteger := 0;
        Dmod.QrUpdU.Params[07].AsFloat   := Valor;
        Dmod.QrUpdU.Params[08].AsFloat   := 0;
        Dmod.QrUpdU.Params[09].AsString  := CO_VAZIO;
        Dmod.QrUpdU.Params[10].AsString  := FormatDateTime(VAR_FORMATDATE, TPData.Date);
        Dmod.QrUpdU.Params[11].AsInteger := Sit;
        Dmod.QrUpdU.Params[12].AsInteger := Geral.IMV(EdDescoPor.Text);
        Dmod.QrUpdU.Params[13].AsInteger := 0;
        Dmod.QrUpdU.Params[14].AsFloat   := 0;
        Dmod.QrUpdU.Params[15].AsFloat   := 0;
        Dmod.QrUpdU.Params[16].AsInteger := 0;
        Dmod.QrUpdU.Params[17].AsInteger := 0;
        Dmod.QrUpdU.Params[18].AsInteger := 0;
        Dmod.QrUpdU.Params[19].AsInteger := 0;
        //
        Dmod.QrUpdU.Params[20].AsInteger := Geral.IMV(EdControle.Text);
        //
        Dmod.QrUpdU.Params[21].AsInteger := VAR_USUARIO;
        Dmod.QrUpdU.Params[22].AsFloat   := Controle;
        Dmod.QrUpdU.ExecSQL;
      end;
    end;
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
  //
end;

procedure TFmCartPgt2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrDescoPor.Open;
  QrCarteiras.Open;
  TPData.Date := Date;
  (*if Dmod.QrControleContaDesco.Value <> 0 then
  begin
    Label19.Enabled := True;
    Label20.Enabled := True;
    Label21.Visible := False;
    //
    EdCarteiraDesco.Enabled := True;
    CBCarteiraDesco.Enabled := True;
    TPData.Enabled := True;
  end; *)
end;

procedure TFmCartPgt2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  AtualizaEmissaoMaster;
end;

procedure TFmCartPgt2.AtualizaEmissaoMaster;
var
  SaldoCred, SaldoDeb, Pago: Double;
  Sit : Integer;
begin
  QrSoma.Close;
  QrSoma.SQL.Clear;
  QrSoma.SQL.Add('SELECT SUM(Credito) Credito, SUM(Debito) Debito, ');
  QrSoma.SQL.Add('SUM(MultaVal) Multa, SUM(MoraVal) Juros, SUM(DescoVal) Desconto ');
  QrSoma.SQL.Add('FROM ' + VAR_LCT + ' WHERE ID_Pgto=:P0');
  QrSoma.Params[0].AsFloat := StrToFloat(LaControle.Caption);
  QrSoma.Open;
  //
  if FQrLct.FieldByName('Debito').AsFloat = 0 then
    SaldoDeb := 0
  else
    SaldoDeb := ((QrSoma.FieldByName('Debito').AsFloat -
      QrSoma.FieldByName('Multa').AsFloat - QrSoma.FieldByName('Juros').AsFloat) +
      QrSoma.FieldByName('Desconto').AsFloat) - FQrLct.FieldByName('Debito').AsFloat;
  //
  if FQrLct.FieldByName('Credito').AsFloat = 0 then
    SaldoCred := 0
  else
    SaldoCred := ((QrSoma.FieldByName('Credito').AsFloat -
      QrSoma.FieldByName('Multa').AsFloat - QrSoma.FieldByName('Juros').AsFloat) +
      QrSoma.FieldByName('Desconto').AsFloat) - FQrLct.FieldByName('Credito').AsFloat;
  //
  Pago        := QrSoma.FieldByName('Credito').AsFloat - QrSoma.FieldByName('Debito').AsFloat;
  EdDeb.Text  := Geral.TFT(FloatToStr(QrSoma.FieldByName('Debito').AsFloat), 2, siNegativo);
  EdCred.Text := Geral.TFT(FloatToStr(QrSoma.FieldByName('Credito').AsFloat), 2, siNegativo);
  //
  EdSaldoDeb.Text  := Geral.TFT(FloatToStr(SaldoDeb), 2, siNegativo);
  EdSaldoCred.Text := Geral.TFT(FloatToStr(QrSoma.FieldByName('Credito').AsFloat -
    FQrLct.FieldByName('Credito').AsFloat), 2, siNegativo);
  //
  if (SaldoCred >= 0) and (SaldoDeb >= 0) then
    Sit := 2
  else if (QrSoma.FieldByName('Debito').AsFloat <> 0) or (QrSoma.FieldByName('Credito').AsFloat <> 0) then
    Sit := 1
  else
    Sit := 0;
  //
  QrSoma.Close;

  UFinanceiro.AtualizaEmissaoMasterExtra_Novo(StrToInt(LaControle.Caption), FQrLct,
    EdDeb, EdCred, EdSaldoDeb, EdSaldoCred, '', VAR_LCT);
end;

end.

