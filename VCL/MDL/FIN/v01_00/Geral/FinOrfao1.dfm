object FmFinOrfao1: TFmFinOrfao1
  Left = 339
  Top = 185
  Caption = 'FIN-ORFAO-001 :: Lan'#231'amentos Sem Cliente Interno'
  ClientHeight = 494
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtAcao: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&A'#231#227'o'
      TabOrder = 0
      OnClick = BtAcaoClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Lan'#231'amentos Sem Cliente Interno'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 398
    Align = alClient
    TabOrder = 0
    object DBGLct: TdmkDBGrid
      Left = 1
      Top = 1
      Width = 1006
      Height = 396
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Carteira'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECARTEIRA'
          Title.Caption = 'Descri'#231#227'o da carteira'
          Width = 216
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatNum'
          Title.Caption = 'Bloqueto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatID'
          Title.Caption = 'Tipo'
          Width = 29
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ID_Pgto'
          Title.Caption = 'ID Pagto'
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Protocolo'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SERIE_CHEQUE'
          Title.Caption = 'S'#233'rie/Docum.'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 164
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D'#233'bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Cr'#233'dito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Title.Caption = 'Vencim.'
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'COMPENSADO_TXT'
          Title.Caption = 'Compen.'
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MENSAL'
          Title.Caption = 'M'#234's'
          Width = 38
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMESIT'
          Title.Caption = 'Situa'#231#227'o'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'Lan'#231'to'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Sub'
          Width = 24
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SALDO'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NotaFiscal'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMERELACIONADO'
          Title.Caption = 'Cliente / Fornecedor'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtde'
          Title.Caption = 'Quantidade'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEFORNECEI'
          Title.Caption = 'Propriet'#225'rio'
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MultaVal'
          Title.Caption = 'Multa paga'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MoraVal'
          Title.Caption = 'Juros pagos'
          Width = 56
          Visible = True
        end>
      Color = clWindow
      DataSource = DsELSCI
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      FieldsCalcToOrder.Strings = (
        'NOMESIT=Sit,Vencimento'
        'SERIE_CHEQUE=SerieCH,Documento'
        'COMPENSADO_TXT=Compensado'
        'MENSAL=Mez')
      Columns = <
        item
          Expanded = False
          FieldName = 'Carteira'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECARTEIRA'
          Title.Caption = 'Descri'#231#227'o da carteira'
          Width = 216
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatNum'
          Title.Caption = 'Bloqueto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatID'
          Title.Caption = 'Tipo'
          Width = 29
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ID_Pgto'
          Title.Caption = 'ID Pagto'
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Protocolo'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SERIE_CHEQUE'
          Title.Caption = 'S'#233'rie/Docum.'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 164
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D'#233'bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Cr'#233'dito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Title.Caption = 'Vencim.'
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'COMPENSADO_TXT'
          Title.Caption = 'Compen.'
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MENSAL'
          Title.Caption = 'M'#234's'
          Width = 38
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMESIT'
          Title.Caption = 'Situa'#231#227'o'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'Lan'#231'to'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Sub'
          Width = 24
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SALDO'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NotaFiscal'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMERELACIONADO'
          Title.Caption = 'Cliente / Fornecedor'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Qtde'
          Title.Caption = 'Quantidade'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEFORNECEI'
          Title.Caption = 'Propriet'#225'rio'
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MultaVal'
          Title.Caption = 'Multa paga'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MoraVal'
          Title.Caption = 'Juros pagos'
          Width = 56
          Visible = True
        end>
    end
  end
  object DsELSCI: TDataSource
    DataSet = DmodFin.QrELSCI
    Left = 136
    Top = 124
  end
  object PMAcao: TPopupMenu
    Left = 52
    Top = 416
    object Atribuirlanamentoaodonodacarteira1: TMenuItem
      Caption = 'Atribuir lan'#231'amento(s) ao dono da carteira'
      OnClick = Atribuirlanamentoaodonodacarteira1Click
    end
    object Excluirlanamentoincondicionalmente1: TMenuItem
      Caption = 'Excluir lan'#231'amento(s) incondicionalmente'
      OnClick = Excluirlanamentoincondicionalmente1Click
    end
  end
end
