unit UCashier;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, UnMLAGeral,
  UnInternalConsts2, UnInternalConsts3, ComCtrls, Registry, mySQLDbTables,
  (*DBTables,*) UMySQLModule, UnGOTOy, dmkGeral, UnDmkEnums;

type
  //TMinMax    = (mmNenhum, mmMinimo, mmMaximo, mmAmbos);
  //TAquemPag  = (apFornece, apCliente, apTranspo, apNinguem);
  TUCashier = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Pagto(QrEmiss: TMySQLQuery; tpPagto:TTipoPagto;
              LocCod, Terceiro, FatID, Genero: Integer; SQLType: TSQLType; Titulo: String;
              Valor: Double; Usuario, FatID_Sub, CliInt: Integer; UsaMinMax:
              TMinMax; ValMin, ValMax: Double; Show, Reconfig: Boolean; Carteira,
              Parcelas, Periodic, DiasParc: Integer);
    procedure Pagto2(QrEmiss: TMySQLQuery; tpPagto: TTipoPagto;
              LocCod, Terceiro, FatID, Genero: Integer;
              SQLType: TSQLType; Titulo: String;
              Valor: Double; Usuario, FatID_Sub, CliInt: Integer;
              UsaMinMax: TMinMax; ValMin, ValMax: Double;
              Show, Reconfig: Boolean; Carteira,
              Parcelas, Periodic, DiasParc: Integer;
              //
              Descricao: String);
    procedure CriaFmQuitaDocs(Data: TDateTime; Documento, Controle, CtrlIni:
              Double; Sub, Carteira: Integer);
    //procedure CriarTransfer(Tipo: Integer; QrLct, QrCarteiras: TmySQLQuery;
    //          MostraForm: Boolean);
    //procedure ConfiguraFmTransfer;

    //
    //function ExcluiTodosLct(FatID: Integer; FatNum: Double): Boolean;
    // N�o usar mais, usar UFinanceiro.ExcluiLct_FatNum(
    //

    //function TransferenciaCorrompida(Lancto: Double; QrCarteiras:
    //         TmySQLQuery): Boolean;
{    function CriaFmCartEdit(Altera, Tipo, Genero, Sit, Cartao, NF,
              Carteira, Fornecedor, Cliente, Vendedor, Account, ClienteInterno,
              Depto, DescoPor, ForneceI: Integer; Controle: Int64; Credito,
              Debito, Documento, MoraDia, Multa, ICMS_P, ICMS_V, DescoVal, NFVal: Double;
              Data, Vencimento, DataDoc: TDateTime; Mensal, Descricao,
              Duplicata: String; QrLct, QrCarteiras: TmySQLQuery;
              SoTestar: Boolean; Qtde: Double; Unidade: Integer): Integer;
}
  end;

var
  UCash: TUCashier;

implementation

uses UnMyObjects, Module, MyPagtos, (*MyPagtos2,*) (*QuitaDocs,*) UnMySQLCuringa,
UnFinanceiro, MyDBCheck, ModuleFin;

procedure TUCashier.CriaFmQuitaDocs(Data: TDateTime; Documento, Controle,
 CtrlIni: Double; Sub, Carteira: Integer);
begin
(* --BD2
  if DBCheck.CriaFm(TFmQuitaDocs, FmQuitaDocs, afmoNegarComAviso) then
  begin
    FmQuitaDocs.FCtrlIni  := CtrlIni;
    FmQuitaDocs.FControle := Controle;
    FmQuitaDocs.TPData1.Date        := Data;//Dmod.QrLctData.Value;
    FmQuitaDocs.EdDoc.Text          := FloatToStr(Documento);
    FmQuitaDocs.EdLancto.Caption    := FloatToStr(Controle);
    FmQuitaDocs.EdSub.Caption       := Geral.TFT(IntToStr(Sub), 0, siPositivo);
    FmQuitaDocs.EdCarteira.Text     := IntToStr(Carteira);
    FmQuitaDocs.CBCarteira.KeyValue := IntToStr(Carteira);
    FmQuitaDocs.ShowModal;
    FmQuitaDocs.Destroy;
  end;
--BD2 *)
end;

procedure TUCashier.Pagto(QrEmiss: TMySQLQuery; tpPagto:TTipoPagto;
              LocCod, Terceiro, FatID, Genero: Integer; SQLType: TSQLType; Titulo: String;
              Valor: Double; Usuario, FatID_Sub, CliInt: Integer; UsaMinMax: TMinMax;
              ValMin, ValMax: Double; Show, Reconfig: Boolean; Carteira,
              Parcelas, Periodic, DiasParc: Integer);
var
  Cursor : TCursor;
  Parcela: Integer;
begin
  Parcela := 0;
  if (SQLType = stUpd) and (QrEmiss <> nil) then
  begin
    if GOTOy.Registros(QrEmiss) = 0 then
    begin
      Application.MessageBox('N�o h� dados para editar!', 'Informa��o',
      MB_OK+MB_ICONINFORMATION);
      Exit;
    end;
    if (QrEmiss.FieldByName('Tipo').AsInteger = 2)
    and (QrEmiss.FieldByName('Sit').AsInteger in ([2,3])) then
    begin
      Application.MessageBox(PChar('Lan�amentos do tipo "emiss�o" n�o podem '+
      'ser modificados ap�s a sua baixa/quita��o/pagamento. Para modific�-lo '+
      'desfa�a a baixa/quita��o/pagamento!'), 'Erro', MB_OK+MB_ICONERROR);
      Exit;
    end;
  end;
  VAR_FATIDTXT := FatID;
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Screen.ActiveForm.Refresh;
  if DBCheck.CriaFm(TFmMyPagtos, FmMyPagtos, afmoNegarComAviso) then
  begin
    try
      with FmMyPagtos do
      begin
        //
        if Uppercase(Application.Title) = 'LESEW' then
        begin
          QrGeneros.SQL.Clear;
          QrGeneros.SQL.Add('SELECT DISTINCT con.Codigo, con.Nome');
          QrGeneros.SQL.Add('FROM contas con');
          QrGeneros.SQL.Add('LEFT JOIN contasu cau ON cau.Codigo = con.Codigo ');
          QrGeneros.SQL.Add('WHERE (con.Codigo > 0 OR con.Codigo < -99)');
          if VAR_USUARIO > 0 then
            QrGeneros.SQL.Add('AND cau.Usuario =' + IntToStr(VAR_USUARIO));
        end else
        begin
          QrGeneros.Close;
          QrGeneros.SQL.Clear;
          QrGeneros.SQL.Add('SELECT con.Codigo, con.Nome');
          QrGeneros.SQL.Add('FROM contas con');
          QrGeneros.SQL.Add('WHERE (con.Codigo > 0 OR con.Codigo < -99)');
        end;
        if tpPagto = tpCred then
        begin
          LaValor.Caption := CO_CREDITO;
          LaDevedor.Enabled := True;
          EdDevedor.Enabled := True;
          CBDevedor.Enabled := True;
          EdDevedor.Text := IntToStr(Terceiro);
          CBDevedor.KeyValue := Terceiro;
          QrGeneros.SQL.Add('AND Credito="V"');
        end else begin
          LaValor.Caption := CO_DEBITO;
          LaCredor.Enabled := True;
          EdCredor.Enabled := True;
          CBCredor.Enabled := True;
          EdCredor.Text := IntToStr(Terceiro);
          CBCredor.KeyValue := Terceiro;
          QrGeneros.SQL.Add('AND Debito="V"');
        end;
        QrGeneros.SQL.Add('ORDER BY Nome');
        QrGeneros.Open;
        //
        EdNotaFiscal.Text := IntToStr(IC3_ED_NF);
        if (SQLType = stUpd) and (QrEmiss <> nil) then
        begin
          EdCodigo.Enabled    := False;
          //RGTipo.ItemIndex    := QrEmiss.FieldByName('Tipo').AsInteger;
          EdCarteira.Text     := IntToStr(QrEmiss.FieldByName('Carteira').AsInteger);
          CBCarteira.KeyVAlue := QrEmiss.FieldByName('Carteira').AsInteger;
          EdDocumento.Text    := FloatToStr(QrEmiss.FieldByName('Documento').AsFloat);
          TPVencimento.Date   := QrEmiss.FieldByName('Vencimento').AsDateTime;
          //
          EdBanco.Text        := FormatFloat('000', QrEmiss.FieldByName('Banco').AsInteger);
          EdNome.Text         := QrEmiss.FieldByName('Emitente').AsString;
          EdAgencia.Text      := QrEmiss.FieldByName('Agencia').AsString;
          EdConta.Text        := QrEmiss.FieldByName('ContaCorrente').AsString;
          EdCNPJCPF.Text      := Geral.FormataCNPJ_TT(QrEmiss.FieldByName('CNPJCPF').AsString);
          EdGenero.Text       := IntToStr(QrEmiss.FieldByName('Genero').AsInteger);
          CBGenero.KeyValue   := QrEmiss.FieldByName('Genero').AsInteger;
        end else begin
          TPVencimento.Date   := Date;
          EdGenero.Text       := IntToStr(Genero);
          CBGenero.KeyValue   := Genero;
          if Reconfig then
          begin
            EdCarteira.Text        := IntToStr(Carteira);
            CBCarteira.KeyValue    := Carteira;
            CkParcelamento.Checked := Parcelas > 0;
            EdParcelas.Text        := IntToStr(Parcelas);
            RGPeriodo.ItemIndex    := Periodic;
            EdDias.Text            := IntToStr(DiasParc);
          end else
          if VAR_MYPAGTOSCONFIG = 1 then
          begin
            if Dmod.QrControleVendaParcPg.Value > 1 then
            begin
              CkParcelamento.Checked := True;
              EdParcelas.Text        := IntToStr(Dmod.QrControleVendaParcPg.Value);
              RGPeriodo.ItemIndex    := Dmod.QrControleVendaPeriPg.Value;
              EdDias.Text            := IntToStr(Dmod.QrControleVendaDiasPg.Value);
              //RGTipo.ItemIndex       := GOTOy.VerificaTipoDaCarteira(
                                        //Dmod.QrControleVendaCartPg.Value);
              EdCarteira.Text        := IntToStr(Dmod.QrControleVendaCartPg.Value);
              CBCarteira.KeyValue    := Dmod.QrControleVendaCartPg.Value;
              FmMyPagtos.CalculaParcelas;
            end;
          end else begin
            CkParcelamento.Checked := Dmod.QrControleMyPgParc.Value > 0;
            EdParcelas.Text := IntToStr(Dmod.QrControleMyPgQtdP.Value);
            RGPeriodo.ItemIndex := Dmod.QrControleMyPgPeri.Value;
            EdDias.Text := IntToStr(Dmod.QrControleMyPgDias.Value);
            CalculaParcelas;
          end;
        end;
        FUsaMinMax := UsaMinMax;
        FValMin := ValMin;
        FValMax := ValMax;
        //
        // MUDAR !!!!!!
        LaUser.Caption := FormatFloat('0', Usuario);
        LaTipo.Caption := DmkEnums.NomeTipoSQL(SQLType);
        LaDefDuplicata.Caption := Titulo;
        FFatIDSub := FatID_Sub;
        (*
        FFatID_Sub := FatID_Sub;
        FUsuario := Usuario;
        ImgTipo.SQLType := SQLType;
        FDefDuplicata := Titulo;
        *)
        if QrEmiss <> nil then
        begin
          if (SQLType = stIns) then
            Parcela := QrEmiss.FieldByName('FatParcela').AsInteger + 1
          else Parcela := QrEmiss.FieldByName('FatParcela').AsInteger;
          FMaxCod := GOTOy.Registros(QrEmiss) + 1;
        end else begin
          Parcela := 1;
          FMaxCod := 1;
        end;
        EdCodigo.Text := IntToStr(Parcela);
        EdCodigo.Enabled := True;
        EdCliInt.Text := IntToStr(CliInt);
        CBCliInt.KeyValue := CliInt;
        if Valor < 0 then Valor := Valor * -1;
        EdValor.Text := Geral.TFT(FloatToStr(Valor), 2, siPositivo);
      end;
    finally
      Screen.Cursor := Cursor;
    end;
    if Show then FmMyPagtos.ShowModal else FmMyPagtos.BtConfirmaClick(Self);
    FmMyPagtos.Destroy;
  end;
  if QrEmiss <> nil then
  begin
    QrEmiss.Close;
    QrEmiss.Params[0].AsInteger := LocCod;
    QrEmiss.Open;
    QrEmiss.Locate('FatParcela', Parcela, []);
  end;
end;

procedure TUCashier.Pagto2(QrEmiss: TMySQLQuery; tpPagto: TTipoPagto;
              LocCod, Terceiro, FatID, Genero: Integer;
              SQLType: TSQLType; Titulo: String;
              Valor: Double; Usuario, FatID_Sub, CliInt: Integer;
              UsaMinMax: TMinMax; ValMin, ValMax: Double;
              Show, Reconfig: Boolean; Carteira,
              Parcelas, Periodic, DiasParc: Integer;
              //
              Descricao: String);
var
  Cursor : TCursor;
  Parcela: Integer;
begin
(* --BD2
  Parcela := 0;
  if (SQLType = stUpd) and (QrEmiss <> nil) then
  begin
    if GOTOy.Registros(QrEmiss) = 0 then
    begin
      Application.MessageBox('N�o h� dados para editar!', 'Informa��o',
      MB_OK+MB_ICONINFORMATION);
      Exit;
    end;
    if (QrEmiss.FieldByName('Tipo').AsInteger = 2)
    and (QrEmiss.FieldByName('Sit').AsInteger in ([2,3])) then
    begin
      Application.MessageBox(PChar('Lan�amentos do tipo "emiss�o" n�o podem '+
      'ser modificados ap�s a sua baixa/quita��o/pagamento. Para modific�-lo '+
      'desfa�a a baixa/quita��o/pagamento!'), 'Erro', MB_OK+MB_ICONERROR);
      Exit;
    end;
  end;
  VAR_FATIDTXT := FatID;
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Screen.ActiveForm.Refresh;
  if DBCheck.CriaFm(TFmMyPagtos2, FmMyPagtos2, afmoNegarComAviso) then
  begin
    try
      with FmMyPagtos2 do
      begin
        //
        if Uppercase(Application.Title) = 'LESEW' then
        begin
          QrGeneros.SQL.Clear;
          QrGeneros.SQL.Add('SELECT DISTINCT con.Codigo, con.Nome');
          QrGeneros.SQL.Add('FROM contas con');
          QrGeneros.SQL.Add('LEFT JOIN contasu cau ON cau.Codigo = con.Codigo ');
          QrGeneros.SQL.Add('WHERE (con.Codigo > 0 OR con.Codigo < -99)');
          if VAR_USUARIO > 0 then
            QrGeneros.SQL.Add('AND cau.Usuario =' + IntToStr(VAR_USUARIO));
        end else
        begin
          QrGeneros.Close;
          QrGeneros.SQL.Clear;
          QrGeneros.SQL.Add('SELECT con.Codigo, con.Nome');
          QrGeneros.SQL.Add('FROM contas con');
          QrGeneros.SQL.Add('WHERE (con.Codigo > 0 OR con.Codigo < -99)');
        end;
        if tpPagto = tpCred then
        begin
          LaValor.Caption := CO_CREDITO;
          LaDevedor.Enabled := True;
          EdDevedor.Enabled := True;
          CBDevedor.Enabled := True;
          EdDevedor.Text := IntToStr(Terceiro);
          CBDevedor.KeyValue := Terceiro;
          QrGeneros.SQL.Add('AND Credito="V"');
        end else begin
          LaValor.Caption := CO_DEBITO;
          LaCredor.Enabled := True;
          EdCredor.Enabled := True;
          CBCredor.Enabled := True;
          EdCredor.Text := IntToStr(Terceiro);
          CBCredor.KeyValue := Terceiro;
          QrGeneros.SQL.Add('AND Debito="V"');
        end;
        QrGeneros.SQL.Add('ORDER BY Nome');
        QrGeneros.Open;
        //
        EdNotaFiscal.Text := IntToStr(IC3_ED_NF);
        if (SQLType = stUpd) and (QrEmiss <> nil) then
        begin
          EdDescricao.Text    := QrEmiss.FieldByName('Descricao').AsString;
          EdCodigo.Enabled    := False;
          //RGTipo.ItemIndex    := QrEmiss.FieldByName('Tipo').AsInteger;
          EdCarteira.Text     := IntToStr(QrEmiss.FieldByName('Carteira').AsInteger);
          CBCarteira.KeyVAlue := QrEmiss.FieldByName('Carteira').AsInteger;
          EdDocumento.Text    := FloatToStr(QrEmiss.FieldByName('Documento').AsFloat);
          TPVencimento.Date   := QrEmiss.FieldByName('Vencimento').AsDateTime;
          //
          EdBanco.Text        := FormatFloat('000', QrEmiss.FieldByName('Banco').AsInteger);
          EdNome.Text         := QrEmiss.FieldByName('Emitente').AsString;
          EdAgencia.Text      := QrEmiss.FieldByName('Agencia').AsString;
          EdConta.Text        := QrEmiss.FieldByName('ContaCorrente').AsString;
          EdCNPJCPF.Text      := Geral.FormataCNPJ_TT(QrEmiss.FieldByName('CNPJCPF').AsString);
          EdGenero.Text       := IntToStr(QrEmiss.FieldByName('Genero').AsInteger);
          CBGenero.KeyValue   := QrEmiss.FieldByName('Genero').AsInteger;
        end else begin
          EdDescricao.Text    := Descricao;
          TPVencimento.Date   := Date;
          EdGenero.Text       := IntToStr(Genero);
          CBGenero.KeyValue   := Genero;
          if Reconfig then
          begin
            EdCarteira.Text        := IntToStr(Carteira);
            CBCarteira.KeyValue    := Carteira;
            CkParcelamento.Checked := Parcelas > 0;
            EdParcelas.Text        := IntToStr(Parcelas);
            RGPeriodo.ItemIndex    := Periodic;
            EdDias.Text            := IntToStr(DiasParc);
          end else
          if VAR_MYPAGTOSCONFIG = 1 then
          begin
            if Dmod.QrControleVendaParcPg.Value > 1 then
            begin
              CkParcelamento.Checked := True;
              EdParcelas.Text        := IntToStr(Dmod.QrControleVendaParcPg.Value);
              RGPeriodo.ItemIndex    := Dmod.QrControleVendaPeriPg.Value;
              EdDias.Text            := IntToStr(Dmod.QrControleVendaDiasPg.Value);
              //RGTipo.ItemIndex       := GOTOy.VerificaTipoDaCarteira(
                                        //Dmod.QrControleVendaCartPg.Value);
              EdCarteira.Text        := IntToStr(Dmod.QrControleVendaCartPg.Value);
              CBCarteira.KeyValue    := Dmod.QrControleVendaCartPg.Value;
              FmMyPagtos2.CalculaParcelas;
            end;
          end else begin
            CkParcelamento.Checked := Dmod.QrControleMyPgParc.Value > 0;
            EdParcelas.Text := IntToStr(Dmod.QrControleMyPgQtdP.Value);
            RGPeriodo.ItemIndex := Dmod.QrControleMyPgPeri.Value;
            EdDias.Text := IntToStr(Dmod.QrControleMyPgDias.Value);
            CalculaParcelas;
          end;
        end;
        FFatIDSub := FatID_Sub;
        FUsaMinMax := UsaMinMax;
        FValMin := ValMin;
        FValMax := ValMax;
        //
        //LaUser.Caption := FormatFloat('0', Usuario);
        LaTipo.SQLType := SQLType;
        FmMyPagtos2.FTitulo := Titulo;
        if QrEmiss <> nil then
        begin
          if (SQLType = stIns) then
            Parcela := QrEmiss.FieldByName('FatParcela').AsInteger + 1
          else Parcela := QrEmiss.FieldByName('FatParcela').AsInteger;
          EdCodigo.ValMax := IntToStr(GOTOy.Registros(QrEmiss) + 1);
        end else begin
          Parcela := 1;
          EdCodigo.ValMax := '1';
        end;
        EdCodigo.Text := IntToStr(Parcela);
        EdCodigo.Enabled := True;
        EdCliInt.Text := IntToStr(CliInt);
        CBCliInt.KeyValue := CliInt;
        if Valor < 0 then Valor := Valor * -1;
        EdValor.Text := Geral.TFT(FloatToStr(Valor), 2, siPositivo);
      end;
    finally
      Screen.Cursor := Cursor;
    end;
    if Show then FmMyPagtos2.ShowModal else FmMyPagtos2.BtConfirmaClick(Self);
    FmMyPagtos2.Destroy;
  end;
  if QrEmiss <> nil then
  begin
    QrEmiss.Close;
    QrEmiss.Params[0].AsInteger := LocCod;
    QrEmiss.Open;
    QrEmiss.Locate('FatParcela', Parcela, []);
  end;
--BD2 *)
end;

{
function TUCashier.CriaFmCartEdit(Altera, Tipo, Genero, Sit, Cartao, NF,
          Carteira, Fornecedor, Cliente, Vendedor, Account, ClienteInterno,
          Depto, DescoPor, ForneceI: Integer; Controle: Int64; Credito, Debito,
          Documento, MoraDia, Multa, ICMS_P, ICMS_V, DescoVal, NFVal: Double;
          Data, Vencimento, DataDoc: TDateTime; Mensal, Descricao, Duplicata:
          String; QrLct, QrCarteiras: TmySQLQuery; SoTestar: Boolean; Qtde: Double;
          Unidade: Integer): Integer;
var
  Cursor: TCursor;
  Mes: String;
  Ano: Integer;
begin
//  Controle := Dmod.QrLctControle.Value;
  Result := 0;
  if Altera>1 then
  if not SoTestar then
    if UMyMod.SelLockInt64Y(Controle, Dmod.MyDB, VAR_LCT, 'Controle') then Exit;
  try
    if Altera > 1 then
    begin
      if Mensal <> CO_VAZIO then
      begin
        Ano := Geral.IMV(Mensal[4]+Mensal[5]);
        if Ano > 90 then Ano := Ano + 1900 else Ano := Ano + 2000;
        Mes := FormatDateTime('yyyy/mm/dd', EncodeDate(Ano,
        Geral.IMV(Mensal[1]+Mensal[2]), 1));
      end else Mes := CO_VAZIO;
      if not SoTestar then
        UMyMod.UpdLockInt64Y(Controle, Dmod.MyDB, VAR_LCT, 'Controle');
    end;
    if (Altera=2) and (Genero < 0) then
    begin
      if SoTestar then Result := -1
      else begin
        case Genero of
          -1: UFinanceiro.CriarTransfer2(1, QrLct, Qrcarteiras, True,
            ClienteInterno, 0,0);
  //        -7: CriaFmAjCustos;
          else
          begin
            Application.MessageBox(PChar('Lan�amento protegido. Para editar '+
            'este item selecione seu caminho correto!'), 'Erro', MB_OK+MB_ICONERROR);
            Exit;
          end;
        end;
      end;
      Exit;
    end;
    if (Altera=2) and (Genero=0) then
    begin
      if SoTestar then Result := -2 else
      Application.MessageBox('N�o h� dados para editar', 'Erro', MB_OK+MB_ICONERROR);
      Exit;
    end;
    if Altera = 2 then
    begin
//      if (Dmod.QrLctCartao.Value > 0) then
      if (Cartao > 0) and not VAR_FATURANDO then
      begin
        if SoTestar then Result := -3 else
        Application.MessageBox(PChar('Esta emiss�o n�o pode ser editada pois '+
        'pertence a uma fatura!'), 'Erro', MB_OK+MB_ICONERROR);
        Exit;
      end;
    end;
    if SoTestar then
    begin
      Result := 1;
      Exit;
    end;
    //if UMyMod.AcessoNegadoAoForm('Perfis', 'EditACxas', 0) and
    UMyMod.AcessoNegadoAoForm('Perfis', 'Inclus�o de Lan�amento Financeiro', 0) then Exit;
    Cursor := Screen.Cursor;
    Screen.Cursor := crHourglass;
    if DBCheck.CriaFm(TFmCaixaEdit, FmCaixaEdit, afmoNegarComAviso) then
    begin
      try
        with FmCaixaEdit do
        begin
          FNFVal := NFVal;
          if Altera = 1 then
          begin
            LaTipo.Caption        := CO_INCLUSAO;
            CBConta.KeyValue      := 0;
            TPData.Date           := MLAGeral.ObtemDataInserir;
            TPVencimento.Date     := Date;
            TPDataDoc.Date        := Date;
          end else begin
            EdCartAnt.Caption     := IntToStr(Carteira);
            EdTipoAnt.Caption     := IntToStr(Tipo);
            case Altera of
              2: LaTipo.Caption   := CO_ALTERACAO;
              3: LaTipo.Caption   := CO_INCLUSAO;
            end;
            EdCodigo.Text         := Geral.TFT(FloatToStr(Controle), 0, siPositivo);
            TPData.Date           := Data;
            TPDataDoc.Date        := DataDoc;
            EdConta.Text          := IntToStr(Genero);
            CBConta.KeyValue      := Genero;
            EdFornecedor.Text     := IntToStr(Fornecedor);
            CBFornecedor.KeyValue := Fornecedor;
            EdCliente.Text        := IntToStr(Cliente);
            CBCliente.KeyValue    := Cliente;
            EdCliInt.Text         := IntToStr(ClienteInterno);
            CBCliInt.KeyValue     := ClienteInterno;
            EdVendedor.Text       := IntToStr(Vendedor);
            CBVendedor.KeyValue   := Vendedor;
            EdAccount.Text        := IntToStr(Account);
            CBAccount.KeyValue    := Account;
            EdDepto.Text          := IntToStr(Depto);
            CBDepto.KeyValue      := Depto;
            EdDescoPor.Text       := IntToStr(DescoPor);
            CBDescoPor.KeyValue   := DescoPor;
            EdForneceI.Text       := IntToStr(ForneceI);
            CBForneceI.KeyValue   := ForneceI;
            EdUnidade.Text        := IntToStr(Unidade);
            CBUnidade.KeyValue    := Unidade;
            EdQtde.Text           := Geral.FFT(Qtde, 3, siPositivo);
            EdMes.Text            := MLAGeral.TST(Mensal, False);

            EdCred.Text           := Geral.TFT(FloatToStr(Credito), 2, siPositivo);
            EdDeb.Text            := Geral.TFT(FloatToStr(Debito), 2, siPositivo);
            EdMoraDia.Text        := Geral.TFT(FloatToStr(MoraDia), 2, siPositivo);
            EdMulta.Text          := Geral.TFT(FloatToStr(Multa), 2, siPositivo);
            EdICMS_P.Text         := Geral.FFT(ICMS_P, 2, siPositivo);
            EdICMS_V.Text         := Geral.FFT(ICMS_V, 2, siPositivo);
            EdDescoVal.Text       := Geral.FFT(DescoVal, 2, siPositivo);
            EdNF.Text             := IntToStr(NF);
            EdDoc.Text            := Geral.TFT(FloatToStr(Documento), 0, siPositivo);
            TPVencimento.Date     := Vencimento;
            EdDescricao.Text      := Descricao;
            EdDuplicata.Text      := Duplicata;
          end;
          RGCarteira.ItemIndex    := Tipo;//VAR_CARTEIRA;
          EDCarteira.Text         := IntToStr(Carteira);
          CBCarteira.KeyValue     := Carteira;
          if (Sit > 1) and (Tipo = 2) then
          begin
            if (Cartao <> 0) and not VAR_FATURANDO then
            begin
              ShowMessage('Esta emiss�o j� est� quitada!');
              EdDeb.Enabled       := False;
              EdCred.Enabled      := False;
              EdDoc.Enabled       := False;
              EdDescricao.Enabled := False;
            end;
            RGCarteira.Enabled    := False;
            EDCarteira.Enabled    := False;
            CBCarteira.Enabled    := False;
            VAR_BAIXADO           := Sit;
          end else VAR_BAIXADO    := -2;
        end;
      finally
        Screen.Cursor             := Cursor;
      end;
      FmCaixaEdit.ShowModal;
      FmCaixaEdit.Destroy;
      UMyMod.UpdUnLockInt64Y(Controle, Dmod.MyDB, VAR_LCT, 'Controle');
    end;
  finally
    ;
  end;
end;
}
{
procedure TUCashier.ConfiguraFmTransfer;
var
  Debito, Credito: Double;
begin
  with FmTransfer do
  begin
    EdCodigo.Text := FloatToStr(DmodFin.QrTransfControle.Value);
    TPData.Date := DmodFin.QrTransfData.Value;
    Debito := 0;
    Credito := 0;
    while not DmodFin.QrTransf.Eof do
    begin
      if (DmodFin.QrTransfDebito.Value > 0) then begin
        if (Debito > 0) then
          Application.MessageBox(PChar('Erro. Mais de um d�bito'+Chr(13)+Chr(10)+
          'D�bito [1]: '+Geral.TFT(FloatToStr(Debito), 2, siNegativo)+Chr(13)+Chr(10)+
          'D�bito [2]: '+Geral.TFT(FloatToStr(DmodFin.QrTransfDebito.Value), 2, siNegativo)),
          'Erro', MB_OK+MB_ICONERROR);
        Debito := Debito + DmodFin.QrTransfDebito.Value;
        EdDocDeb.Text := FloatToStr(DmodFin.QrTransfDocumento.Value);
      end;
      if (DmodFin.QrTransfCredito.Value > 0) then begin
        if (Credito > 0) then
          Application.MessageBox(PChar('Erro. Mais de um d�bito'+Chr(13)+Chr(10)+
          'Cr�dito [1]: '+Geral.TFT(FloatToStr(Credito), 2, siNegativo)+Chr(13)+Chr(10)+
          'Cr�dito [2]: '+Geral.TFT(FloatToStr(DmodFin.QrTransfCredito.Value), 2, siNegativo)),
          'Erro', MB_OK+MB_ICONERROR);
        Credito := Credito + DmodFin.QrTransfCredito.Value;
        EdDocCred.Text := FloatToStr(DmodFin.QrTransfDocumento.Value);
      end;
      EdValor.Text := Geral.TFT(FloatToStr(
      DmodFin.QrTransfDebito.Value+DmodFin.QrTransfCredito.Value), 2, siPositivo);
      if DmodFin.QrTransfDebito.Value > 0 then
      begin
        RGOrigem.ItemIndex := DmodFin.QrTransfTipo.Value;
        CBOrigem.KeyValue := DmodFin.QrTransfCarteira.Value;
      end else begin
        RGDestino.ItemIndex := DmodFin.QrTransfTipo.Value;
        CBDestino.KeyValue := DmodFin.QrTransfCarteira.Value;
      end;
      DmodFin.QrTransf.Next;
    end;
    if Credito <> Debito then
      ShowMessage('Erro. Cr�dito diferente do d�bito'+Chr(13)+Chr(10)+
      'Cr�dito: '+Geral.TFT(FloatToStr(Credito), 2, siNegativo)+Chr(13)+Chr(10)+
      'D�bito : '+Geral.TFT(FloatToStr(Debito), 2, siNegativo));
  end;
end;
}
end.

