unit SomaDinheiroCH;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, UnMLAGeral, UnGOTOy, StdCtrls, Buttons, UnInternalConsts,
  ComCtrls, dmkEdit, dmkEditDateTimePicker, dmkGeral;

type
  TFmSomaDinheiroCH = class(TForm)
    PainelDados: TPanel;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    EdDescricao: TdmkEdit;
    Label4: TLabel;
    Label3: TLabel;
    EdValor: TdmkEdit;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Label1: TLabel;
    EdControle: TdmkEdit;
    TPVencimento: TdmkEditDateTimePicker;
    Label2: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmSomaDinheiroCH: TFmSomaDinheiroCH;

implementation

uses UnMyObjects, Module, SomaDinheiro, UMySQLModule;

{$R *.DFM}

procedure TFmSomaDinheiroCH.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  EdValor.SetFocus;
end;

procedure TFmSomaDinheiroCH.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSomaDinheiroCH.BtConfirmaClick(Sender: TObject);
var
  Controle: Integer;
begin
  Dmod.QrUpdM.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'SomaIts', 'SomaIts', 'Controle');
    Dmod.QrUpdM.SQL.Add('INSERT INTO somaits SET ');
  end else begin
    Controle := EdControle.ValueVariant;
    Dmod.QrUpdM.SQL.Add('UPDATE somaits SET ');
  end;
  Dmod.QrUpdM.SQL.Add('Valor=:P0, Descricao=:P1, Vencimento=:P2 ');
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdM.SQL.Add(', Caixa=:Pa, Controle=:Pb')
  else
    Dmod.QrUpdM.SQL.Add('WHERE Caixa=:Pa AND Controle=:Pb');
  //
  Dmod.QrUpdM.Params[00].AsFloat  := EdValor.ValueVariant;
  Dmod.QrUpdM.Params[01].AsString := EdDescricao.Text;
  Dmod.QrUpdM.Params[02].AsString := Geral.FDT(TPVencimento.Date, 1);
  //
  Dmod.QrUpdM.Params[03].AsInteger := VAR_QRCARTEIRAS.FieldByName('Codigo').AsInteger;
  Dmod.QrUpdM.Params[04].AsInteger := Controle;
  Dmod.QrUpdM.ExecSQL;
  Close;
  FmSomaDinheiro.ReopenSomaIts(Controle);
end;

procedure TFmSomaDinheiroCH.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmSomaDinheiroCH.FormCreate(Sender: TObject);
begin
  TPVencimento.Date := Date;
end;

end.
