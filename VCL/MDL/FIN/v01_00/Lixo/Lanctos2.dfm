object FmLanctos2: TFmLanctos2
  Left = 339
  Top = 169
  Caption = 'Edi'#231#227'o de lan'#231'amentos'
  ClientHeight = 456
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelControle: TPanel
    Left = 0
    Top = 408
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    ExplicitTop = 410
    ExplicitWidth = 792
    object Label20: TLabel
      Left = 376
      Top = 4
      Width = 81
      Height = 13
      Caption = '* do condom'#237'nio.'
    end
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 148
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtConfirmaClick
      NumGlyphs = 2
    end
    object CkContinuar: TCheckBox
      Left = 12
      Top = 7
      Width = 113
      Height = 17
      Caption = 'Continuar inserindo.'
      TabOrder = 0
      Visible = False
    end
    object CkCopiaCH: TCheckBox
      Left = 12
      Top = 24
      Width = 129
      Height = 17
      Caption = 'Fazer c'#243'pia de cheque.'
      TabOrder = 2
    end
    object BitBtn1: TBitBtn
      Tag = 10042
      Left = 260
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = 'C&heque'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = BitBtn1Click
      NumGlyphs = 2
    end
    object Panel4: TPanel
      Left = 672
      Top = 1
      Width = 119
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      Caption = 'Panel4'
      TabOrder = 4
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 15
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 40
    Align = alTop
    
    
    
    
    Caption = 'Edi'#231#227'o de lan'#231'amentos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2

    ExplicitWidth = 792
    object LaTipo: TLabel
      Left = 712
      Top = 2
      Width = 78
      Height = 36
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Image1: TImage
      Left = 2
      Top = 2
      Width = 710
      Height = 36
      Align = alClient
      Transparent = True
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 40
    Width = 784
    Height = 368
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    OnChange = PageControl1Change
    ExplicitWidth = 792
    ExplicitHeight = 370
    object TabSheet1: TTabSheet
      Caption = 'Edi'#231#227'o'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object PainelDados: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 340
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 784
        ExplicitHeight = 342
        object EdCartAnt: TLabel
          Left = 456
          Top = 8
          Width = 6
          Height = 13
          Caption = '0'
          Visible = False
        end
        object EdTipoAnt: TLabel
          Left = 472
          Top = 8
          Width = 6
          Height = 13
          Caption = '0'
          Visible = False
        end
        object LaFunci: TLabel
          Left = 496
          Top = 4
          Width = 6
          Height = 13
          Caption = '0'
          Visible = False
        end
        object Panel2: TPanel
          Left = 1
          Top = 1
          Width = 572
          Height = 338
          Align = alLeft
          TabOrder = 0
          ExplicitHeight = 340
          object Label15: TLabel
            Left = 88
            Top = 4
            Width = 39
            Height = 13
            Caption = 'Carteira:'
          end
          object Label14: TLabel
            Left = 8
            Top = 4
            Width = 62
            Height = 13
            Caption = 'Lan'#231'amento:'
          end
          object Label1: TLabel
            Left = 8
            Top = 44
            Width = 26
            Height = 13
            Caption = 'Data:'
          end
          object Label2: TLabel
            Left = 124
            Top = 44
            Width = 97
            Height = 13
            Caption = 'Conta [F7] pesquisa:'
          end
          object SpeedButton1: TSpeedButton
            Left = 536
            Top = 20
            Width = 23
            Height = 22
            Hint = 'Inclui item de carteira'
            Caption = '...'
            OnClick = SpeedButton1Click
          end
          object BtContas: TSpeedButton
            Left = 536
            Top = 60
            Width = 23
            Height = 22
            Hint = 'Inclui conta'
            Caption = '...'
            OnClick = BtContasClick
          end
          object LaVencimento: TLabel
            Left = 472
            Top = 88
            Width = 80
            Height = 13
            Caption = 'Vencimento [F6]:'
          end
          object Label17: TLabel
            Left = 444
            Top = 90
            Width = 22
            Height = 12
            Caption = 'pq'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Wingdings 3'
            Font.Style = []
            ParentFont = False
          end
          object Label11: TLabel
            Left = 392
            Top = 88
            Width = 48
            Height = 13
            Caption = 'Duplicata:'
          end
          object Label12: TLabel
            Left = 368
            Top = 90
            Width = 22
            Height = 12
            Caption = 'pq'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Wingdings 3'
            Font.Style = []
            ParentFont = False
          end
          object LaDoc: TLabel
            Left = 328
            Top = 88
            Width = 40
            Height = 13
            Caption = 'Cheque:'
          end
          object Label16: TLabel
            Left = 260
            Top = 90
            Width = 22
            Height = 12
            Caption = 'pq'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Wingdings 3'
            Font.Style = []
            ParentFont = False
          end
          object LaNF: TLabel
            Left = 220
            Top = 88
            Width = 23
            Height = 13
            Caption = 'N.F.:'
          end
          object LaCred: TLabel
            Left = 144
            Top = 88
            Width = 36
            Height = 13
            Caption = 'Cr'#233'dito:'
            Enabled = False
          end
          object LaDeb: TLabel
            Left = 68
            Top = 88
            Width = 34
            Height = 13
            Caption = 'D'#233'bito:'
            Enabled = False
          end
          object LaMes: TLabel
            Left = 8
            Top = 88
            Width = 23
            Height = 13
            Caption = 'M'#234's:'
            Enabled = False
          end
          object Label13: TLabel
            Left = 68
            Top = 252
            Width = 192
            Height = 13
            Caption = 'Descri'#231#227'o [F4 : F5 : F6] (+ cli/forn. [F8]) :'
          end
          object LaCliente: TLabel
            Left = 8
            Top = 168
            Width = 109
            Height = 13
            Caption = 'Cliente do condom'#237'nio:'
          end
          object LaFornecedor: TLabel
            Left = 292
            Top = 168
            Width = 61
            Height = 13
            Caption = 'Fornecedor*:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label3: TLabel
            Left = 8
            Top = 252
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object Label4: TLabel
            Left = 43
            Top = 90
            Width = 22
            Height = 12
            Caption = 'pq'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Wingdings 3'
            Font.Style = []
            ParentFont = False
          end
          object LaCliInt: TLabel
            Left = 8
            Top = 128
            Width = 58
            Height = 13
            Caption = 'Condominio:'
            Visible = False
          end
          object Label5: TLabel
            Left = 284
            Top = 88
            Width = 45
            Height = 13
            Caption = 'S'#233'rie CH:'
          end
          object LaForneceRN: TLabel
            Left = 352
            Top = 168
            Width = 85
            Height = 13
            Caption = '[F5] Raz'#227'o/Nome'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object LaForneceFA: TLabel
            Left = 444
            Top = 168
            Width = 101
            Height = 13
            Caption = '[F6] Fantasia/Apelido'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label6: TLabel
            Left = 8
            Top = 212
            Width = 177
            Height = 13
            Caption = 'Unidade habitacional do condom'#237'nio:'
          end
          object LaForneceI: TLabel
            Left = 192
            Top = 212
            Width = 172
            Height = 13
            Caption = 'Propriet'#225'rio da unidade habitacional:'
          end
          object SpeedButton2: TSpeedButton
            Left = 536
            Top = 184
            Width = 23
            Height = 22
            Hint = 'Inclui conta'
            Caption = '...'
            OnClick = SpeedButton2Click
          end
          object EdCarteira: TLMDEdit
            Left = 88
            Top = 20
            Width = 77
            Height = 21
            
            Caret.BlinkRate = 530
            CtlXP = False
            TabOrder = 1
            OnChange = EdCarteiraChange
            OnExit = EdCarteiraExit
            AutoSelect = True
            Alignment = taRightJustify
            CustomButtons = <>
            PasswordChar = #0
          end
          object CBCarteira: TDBLookupComboBox
            Left = 168
            Top = 20
            Width = 365
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCarteiras
            TabOrder = 2
            OnClick = CBCarteiraClick
            OnDropDown = CBCarteiraDropDown
          end
          object EdCodigo: TLMDEdit
            Left = 8
            Top = 20
            Width = 77
            Height = 21
            
            Caret.BlinkRate = 530
            Color = clBtnFace
            CtlXP = False
            TabOrder = 0
            TabStop = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8281908
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            AutoSelect = True
            ParentFont = False
            Alignment = taRightJustify
            CustomButtons = <>
            PasswordChar = #0
            ReadOnly = True
            Text = '0'
          end
          object TPData: TDateTimePicker
            Left = 8
            Top = 60
            Width = 113
            Height = 21
            Date = 37617.480364108800000000
            Time = 37617.480364108800000000
            Color = clWhite
            TabOrder = 3
            OnChange = TPDataChange
          end
          object EdConta: TLMDEdit
            Left = 124
            Top = 60
            Width = 77
            Height = 21
            
            Caret.BlinkRate = 530
            CtlXP = False
            TabOrder = 4
            OnChange = EdContaChange
            OnExit = EdContaExit
            OnKeyDown = EdContaKeyDown
            AutoSelect = True
            Alignment = taRightJustify
            CustomButtons = <>
            PasswordChar = #0
          end
          object CBConta: TDBLookupComboBox
            Left = 204
            Top = 60
            Width = 329
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsContas
            TabOrder = 5
            OnClick = CBContaClick
            OnDropDown = CBContaDropDown
            OnKeyDown = CBContaKeyDown
          end
          object EdMes: TLMDEdit
            Left = 8
            Top = 104
            Width = 57
            Height = 21
            
            Caret.BlinkRate = 530
            CtlXP = False
            Enabled = False
            TabOrder = 6
            OnExit = EdMesExit
            OnKeyDown = EdMesKeyDown
            AutoSelect = True
            Alignment = taCenter
            CustomButtons = <>
            PasswordChar = #0
            Text = '07/2007'
          end
          object EdDeb: TLMDEdit
            Left = 68
            Top = 104
            Width = 72
            Height = 21
            
            Caret.BlinkRate = 530
            CtlXP = False
            Enabled = False
            TabOrder = 7
            OnExit = EdDebExit
            AutoSelect = True
            Alignment = taRightJustify
            CustomButtons = <>
            PasswordChar = #0
            Text = '0,00'
          end
          object EdCred: TLMDEdit
            Left = 144
            Top = 104
            Width = 72
            Height = 21
            
            Caret.BlinkRate = 530
            CtlXP = False
            Enabled = False
            TabOrder = 8
            OnExit = EdCredExit
            AutoSelect = True
            Alignment = taRightJustify
            CustomButtons = <>
            PasswordChar = #0
            Text = '0,00'
          end
          object EdNF: TLMDEdit
            Left = 220
            Top = 104
            Width = 60
            Height = 21
            
            Caret.BlinkRate = 530
            CtlXP = False
            TabOrder = 9
            OnExit = EdNFExit
            OnKeyDown = EdNFKeyDown
            MaxLength = 11
            AutoSelect = True
            Alignment = taRightJustify
            CustomButtons = <>
            PasswordChar = #0
            Text = '0'
          end
          object EdDoc: TLMDEdit
            Left = 328
            Top = 104
            Width = 60
            Height = 21
            
            Caret.BlinkRate = 530
            CtlXP = False
            TabOrder = 11
            OnChange = EdDocChange
            OnExit = EdDocExit
            OnKeyDown = EdDocKeyDown
            MaxLength = 11
            AutoSelect = True
            Alignment = taRightJustify
            CustomButtons = <>
            PasswordChar = #0
            Text = '0'
          end
          object EdDuplicata: TLMDEdit
            Left = 392
            Top = 104
            Width = 76
            Height = 21
            
            Caret.BlinkRate = 530
            CtlXP = False
            TabOrder = 12
            OnKeyDown = EdDuplicataKeyDown
            AutoSelect = True
            Alignment = taCenter
            CustomButtons = <>
            PasswordChar = #0
          end
          object TPVencimento: TDateTimePicker
            Left = 472
            Top = 104
            Width = 89
            Height = 21
            Date = 37617.480364108800000000
            Time = 37617.480364108800000000
            Color = clWhite
            TabOrder = 13
            OnKeyDown = TPVencimentoKeyDown
          end
          object EdDescricao: TLMDEdit
            Left = 68
            Top = 268
            Width = 493
            Height = 21
            
            Caret.BlinkRate = 530
            CtlXP = False
            TabOrder = 25
            OnKeyDown = EdDescricaoKeyDown
            AutoSelect = True
            CustomButtons = <>
            PasswordChar = #0
          end
          object EdCliente: TLMDEdit
            Left = 8
            Top = 184
            Width = 57
            Height = 21
            
            Caret.BlinkRate = 530
            CtlXP = False
            TabOrder = 16
            OnChange = EdClienteChange
            OnExit = EdClienteExit
            AutoSelect = True
            Alignment = taRightJustify
            CustomButtons = <>
            PasswordChar = #0
          end
          object CBCliente: TDBLookupComboBox
            Left = 64
            Top = 184
            Width = 225
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsClientes
            TabOrder = 17
            OnClick = CBClienteClick
            OnDropDown = CBClienteDropDown
          end
          object EdFornecedor: TLMDEdit
            Left = 292
            Top = 184
            Width = 57
            Height = 21
            
            Caret.BlinkRate = 530
            CtlXP = False
            TabOrder = 18
            OnChange = EdFornecedorChange
            OnExit = EdFornecedorExit
            OnKeyDown = EdFornecedorKeyDown
            AutoSelect = True
            Alignment = taRightJustify
            CustomButtons = <>
            PasswordChar = #0
          end
          object CBFornecedor: TDBLookupComboBox
            Left = 348
            Top = 184
            Width = 185
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsFornecedores
            TabOrder = 19
            OnClick = CBFornecedorClick
            OnDropDown = CBFornecedorDropDown
            OnKeyDown = CBFornecedorKeyDown
          end
          object EdQtde: TLMDEdit
            Left = 8
            Top = 268
            Width = 57
            Height = 21
            
            Caret.BlinkRate = 530
            CtlXP = False
            TabOrder = 24
            OnExit = EdQtdeExit
            AutoSelect = True
            Alignment = taRightJustify
            CustomButtons = <>
            PasswordChar = #0
          end
          object EdCliInt: TLMDEdit
            Left = 8
            Top = 144
            Width = 57
            Height = 21
            
            Caret.BlinkRate = 530
            CtlXP = False
            TabOrder = 14
            Visible = False
            OnChange = EdCliIntChange
            OnExit = EdCliIntExit
            AutoSelect = True
            Alignment = taRightJustify
            CustomButtons = <>
            PasswordChar = #0
          end
          object CBCliInt: TDBLookupComboBox
            Left = 68
            Top = 144
            Width = 493
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsCliInt
            TabOrder = 15
            Visible = False
            OnClick = CBCliIntClick
            OnDropDown = CBCliIntDropDown
          end
          object EdSerieCh: TLMDEdit
            Left = 284
            Top = 104
            Width = 41
            Height = 21
            
            Caret.BlinkRate = 530
            CtlXP = False
            TabOrder = 10
            CharCase = ecUpperCase
            MaxLength = 3
            AutoSelect = True
            CustomButtons = <>
            PasswordChar = #0
          end
          object EdDepto: TLMDEdit
            Left = 8
            Top = 228
            Width = 57
            Height = 21
            
            Caret.BlinkRate = 530
            CtlXP = False
            Enabled = False
            TabOrder = 20
            OnChange = EdDeptoChange
            OnExit = EdFunciExit
            AutoSelect = True
            Alignment = taRightJustify
            CustomButtons = <>
            PasswordChar = #0
          end
          object CBDepto: TDBLookupComboBox
            Left = 68
            Top = 228
            Width = 121
            Height = 21
            Color = clWhite
            KeyField = 'Conta'
            ListField = 'Unidade'
            ListSource = DsAptos
            TabOrder = 21
            OnClick = CBFunciClick
            OnDropDown = CBFunciDropDown
            OnKeyDown = CBDeptoKeyDown
          end
          object EdCliCli: TLMDEdit
            Left = 192
            Top = 228
            Width = 57
            Height = 21
            
            Caret.BlinkRate = 530
            CtlXP = False
            TabOrder = 22
            OnChange = EdFunciChange
            OnExit = EdFunciExit
            AutoSelect = True
            Alignment = taRightJustify
            CustomButtons = <>
            PasswordChar = #0
          end
          object CBCliCli: TDBLookupComboBox
            Left = 252
            Top = 228
            Width = 309
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsCliCli
            TabOrder = 23
            OnClick = CBFunciClick
            OnDropDown = CBFunciDropDown
          end
          object CkPesqCH: TCheckBox
            Left = 8
            Top = 296
            Width = 553
            Height = 17
            Caption = 'Pesquisar duplica'#231#227'o de cheque.'
            Checked = True
            State = cbChecked
            TabOrder = 26
          end
          object CkPesqNF: TCheckBox
            Left = 8
            Top = 316
            Width = 553
            Height = 17
            Caption = 'Pesquisar duplica'#231#227'o de nota fiscal.'
            Checked = True
            State = cbChecked
            TabOrder = 27
          end
        end
        object PnMaskPesq: TPanel
          Left = 573
          Top = 1
          Width = 202
          Height = 338
          Align = alClient
          Caption = 'PnMaskPesq'
          TabOrder = 1
          Visible = False
          ExplicitWidth = 210
          ExplicitHeight = 340
          object Panel5: TPanel
            Left = 1
            Top = 21
            Width = 200
            Height = 44
            Align = alTop
            BevelOuter = bvLowered
            TabOrder = 0
            ExplicitWidth = 208
            object Label18: TLabel
              Left = 4
              Top = 4
              Width = 186
              Height = 13
              Caption = 'Digite parte da descri'#231#227'o (min. 4 letras):'
            end
            object EdLinkMask: TEdit
              Left = 4
              Top = 20
              Width = 201
              Height = 21
              TabOrder = 0
              OnChange = EdLinkMaskChange
              OnKeyDown = EdLinkMaskKeyDown
            end
          end
          object LLBPesq: TDBLookupListBox
            Left = 1
            Top = 65
            Width = 200
            Height = 264
            Align = alClient
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPesq
            TabOrder = 1
            OnDblClick = LLBPesqDblClick
            OnKeyDown = LLBPesqKeyDown
            ExplicitWidth = 208
          end
          object PnLink: TPanel
            Left = 1
            Top = 1
            Width = 200
            Height = 20
            Align = alTop
            BevelOuter = bvLowered
            Caption = 'Sem link'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
            ExplicitWidth = 208
            object Panel6: TPanel
              Left = 189
              Top = 1
              Width = 18
              Height = 18
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 0
              object SpeedButton3: TSpeedButton
                Left = 0
                Top = 0
                Width = 18
                Height = 18
                Caption = 'x'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Layout = blGlyphBottom
                ParentFont = False
                OnClick = SpeedButton3Click
              end
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Outros dados'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 145
        Align = alTop
        TabOrder = 0
        ExplicitWidth = 784
        object Label9: TLabel
          Left = 8
          Top = 4
          Width = 82
          Height = 13
          Caption = 'Data documento:'
        end
        object Label22: TLabel
          Left = 8
          Top = 44
          Width = 65
          Height = 13
          Caption = 'Compensado:'
        end
        object TPDataDoc: TDateTimePicker
          Left = 8
          Top = 20
          Width = 113
          Height = 21
          Date = 37617.480364108800000000
          Time = 37617.480364108800000000
          Color = clWhite
          TabOrder = 0
        end
        object TPCompensado: TDateTimePicker
          Left = 8
          Top = 60
          Width = 113
          Height = 21
          Date = 1.000000000000000000
          Time = 1.000000000000000000
          Color = clWhite
          TabOrder = 1
        end
        object CkTipoCH: TLMDCheckGroup
          Left = 684
          Top = 4
          Width = 85
          Height = 125
          
          BtnAlignment.Alignment = agCenterLeft
          Caption = ' Cheque: '
          CaptionFont.Charset = DEFAULT_CHARSET
          CaptionFont.Color = clWindowText
          CaptionFont.Height = -11
          CaptionFont.Name = 'MS Sans Serif'
          CaptionFont.Style = []
          Items.Strings = (
            'Visado'
            'Cruzado')
          TabOrder = 2
          Value = -1
        end
        object GroupBox1: TGroupBox
          Left = 128
          Top = 4
          Width = 461
          Height = 137
          TabOrder = 3
          object GroupBox2: TGroupBox
            Left = 8
            Top = 8
            Width = 233
            Height = 61
            Caption = 'Multa e juros a cobrar:'
            TabOrder = 0
            object Label8: TLabel
              Left = 8
              Top = 16
              Width = 83
              Height = 13
              Caption = 'Juros ao M'#234's (%):'
            end
            object Label7: TLabel
              Left = 120
              Top = 16
              Width = 46
              Height = 13
              Caption = 'Multa (%):'
            end
            object EdMoraDia: TLMDEdit
              Left = 8
              Top = 32
              Width = 105
              Height = 21
              
              Caret.BlinkRate = 530
              CtlXP = False
              TabOrder = 0
              OnExit = EdMoraDiaExit
              OnKeyDown = EdMoraDiaKeyDown
              AutoSelect = True
              Alignment = taRightJustify
              CustomButtons = <>
              PasswordChar = #0
              Text = '0,0000'
            end
            object EdMulta: TLMDEdit
              Left = 120
              Top = 32
              Width = 105
              Height = 21
              
              Caret.BlinkRate = 530
              CtlXP = False
              TabOrder = 1
              OnExit = EdMultaExit
              OnKeyDown = EdMultaKeyDown
              AutoSelect = True
              Alignment = taRightJustify
              CustomButtons = <>
              PasswordChar = #0
              Text = '0,0000'
            end
          end
          object GroupBox3: TGroupBox
            Left = 8
            Top = 72
            Width = 233
            Height = 61
            Caption = 'Multa e juros pagos:'
            TabOrder = 1
            object Label19: TLabel
              Left = 8
              Top = 16
              Width = 38
              Height = 13
              Caption = 'Multa $:'
            end
            object Label21: TLabel
              Left = 120
              Top = 16
              Width = 37
              Height = 13
              Caption = 'Juros $:'
            end
            object EdMultaVal: TLMDEdit
              Left = 8
              Top = 32
              Width = 105
              Height = 21
              
              Caret.BlinkRate = 530
              CtlXP = False
              TabOrder = 0
              OnExit = EdMultaValExit
              AutoSelect = True
              Alignment = taRightJustify
              CustomButtons = <>
              PasswordChar = #0
              Text = '0,00'
            end
            object EdMoraVal: TLMDEdit
              Left = 120
              Top = 32
              Width = 105
              Height = 21
              
              Caret.BlinkRate = 530
              CtlXP = False
              TabOrder = 1
              OnExit = EdMoraValExit
              AutoSelect = True
              Alignment = taRightJustify
              CustomButtons = <>
              PasswordChar = #0
              Text = '0,00'
            end
          end
          object GroupBox4: TGroupBox
            Left = 244
            Top = 8
            Width = 209
            Height = 125
            Caption = 'Rec'#225'lculo: '
            TabOrder = 2
            object Label28: TLabel
              Left = 8
              Top = 16
              Width = 63
              Height = 13
              Caption = 'Valor original:'
            end
            object Label29: TLabel
              Left = 116
              Top = 16
              Width = 46
              Height = 13
              Caption = 'Multa (%):'
            end
            object EdValNovo: TLMDEdit
              Left = 8
              Top = 32
              Width = 105
              Height = 21
              
              Caret.BlinkRate = 530
              CtlXP = False
              TabOrder = 0
              OnExit = EdMultaValExit
              AutoSelect = True
              Alignment = taRightJustify
              CustomButtons = <>
              PasswordChar = #0
              Text = '0,00'
            end
            object EdPerMult: TLMDEdit
              Left = 116
              Top = 32
              Width = 85
              Height = 21
              
              Caret.BlinkRate = 530
              CtlXP = False
              TabOrder = 1
              OnExit = EdMultaExit
              OnKeyDown = EdMultaKeyDown
              AutoSelect = True
              Alignment = taRightJustify
              CustomButtons = <>
              PasswordChar = #0
              Text = '2,0000'
            end
            object BitBtn2: TBitBtn
              Tag = 180
              Left = 63
              Top = 68
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
              Caption = '&Recalcular'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              OnClick = BitBtn2Click
              NumGlyphs = 2
            end
          end
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 145
        Width = 776
        Height = 195
        Align = alClient
        TabOrder = 1
        Visible = False
        ExplicitWidth = 784
        ExplicitHeight = 197
        object Label10: TLabel
          Left = 8
          Top = 4
          Width = 58
          Height = 13
          Caption = 'Funcion'#225'rio:'
          Visible = False
        end
        object LaVendedor: TLabel
          Left = 8
          Top = 48
          Width = 49
          Height = 13
          Caption = 'Vendedor:'
          Visible = False
        end
        object LaAccount: TLabel
          Left = 288
          Top = 48
          Width = 185
          Height = 13
          Caption = 'Representante ([F4 ] para preferencial):'
          Visible = False
        end
        object LaICMS_P: TLabel
          Left = 288
          Top = 4
          Width = 37
          Height = 13
          Caption = '% ICMS'
          Visible = False
        end
        object LaICMS_V: TLabel
          Left = 392
          Top = 4
          Width = 56
          Height = 13
          Caption = 'Valor ICMS:'
          Visible = False
        end
        object EdFunci: TLMDEdit
          Left = 8
          Top = 20
          Width = 57
          Height = 21
          
          Caret.BlinkRate = 530
          CtlXP = False
          TabOrder = 0
          Visible = False
          OnChange = EdFunciChange
          OnExit = EdFunciExit
          AutoSelect = True
          Alignment = taRightJustify
          CustomButtons = <>
          PasswordChar = #0
        end
        object CBFunci: TDBLookupComboBox
          Left = 68
          Top = 20
          Width = 217
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsFunci
          TabOrder = 1
          Visible = False
          OnClick = CBFunciClick
          OnDropDown = CBFunciDropDown
        end
        object EdVendedor: TLMDEdit
          Left = 8
          Top = 64
          Width = 57
          Height = 21
          
          Caret.BlinkRate = 530
          CtlXP = False
          TabOrder = 2
          Visible = False
          OnChange = EdFornecedorChange
          OnExit = EdFornecedorExit
          AutoSelect = True
          Alignment = taRightJustify
          CustomButtons = <>
          PasswordChar = #0
        end
        object CBVendedor: TDBLookupComboBox
          Left = 64
          Top = 64
          Width = 221
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsVendedores
          TabOrder = 3
          Visible = False
          OnClick = CBFornecedorClick
        end
        object EdAccount: TLMDEdit
          Left = 288
          Top = 64
          Width = 57
          Height = 21
          
          Caret.BlinkRate = 530
          CtlXP = False
          TabOrder = 4
          Visible = False
          OnChange = EdClienteChange
          OnExit = EdClienteExit
          OnKeyDown = EdAccountKeyDown
          AutoSelect = True
          Alignment = taRightJustify
          CustomButtons = <>
          PasswordChar = #0
        end
        object CBAccount: TDBLookupComboBox
          Left = 342
          Top = 64
          Width = 219
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsAccounts
          TabOrder = 5
          Visible = False
          OnClick = CBClienteClick
          OnDropDown = CBClienteDropDown
          OnKeyDown = CBAccountKeyDown
        end
        object EdICMS_P: TLMDEdit
          Left = 288
          Top = 20
          Width = 101
          Height = 21
          
          Caret.BlinkRate = 530
          CtlXP = False
          TabOrder = 6
          Visible = False
          OnExit = EdICMS_PExit
          OnKeyDown = EdICMS_PKeyDown
          AutoSelect = True
          Alignment = taRightJustify
          CustomButtons = <>
          PasswordChar = #0
          Text = '0,00'
        end
        object EdICMS_V: TLMDEdit
          Left = 392
          Top = 20
          Width = 105
          Height = 21
          
          Caret.BlinkRate = 530
          CtlXP = False
          TabOrder = 7
          Visible = False
          OnExit = EdICMS_VExit
          OnKeyDown = EdICMS_VKeyDown
          AutoSelect = True
          Alignment = taRightJustify
          CustomButtons = <>
          PasswordChar = #0
          Text = '0,00'
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Parcelamento'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 156
        Align = alTop
        TabOrder = 0
        ExplicitWidth = 784
        object GBParcelamento: TGroupBox
          Left = 1
          Top = 1
          Width = 774
          Height = 154
          Align = alClient
          Caption = ' Parcelamento autom'#225'tico: '
          TabOrder = 1
          Visible = False
          ExplicitWidth = 782
          object Label23: TLabel
            Left = 8
            Top = 20
            Width = 44
            Height = 13
            Caption = 'Parcelas:'
          end
          object Label24: TLabel
            Left = 508
            Top = 24
            Width = 78
            Height = 13
            Caption = 'Primeira parcela:'
          end
          object Label25: TLabel
            Left = 588
            Top = 24
            Width = 70
            Height = 13
            Caption = #218'ltima parcela:'
          end
          object Label26: TLabel
            Left = 660
            Top = 24
            Width = 97
            Height = 13
            Caption = 'Total parcelamento: '
          end
          object RGArredondar: TRadioGroup
            Left = 308
            Top = 16
            Width = 193
            Height = 45
            Caption = '   Arredondar '
            Columns = 2
            ItemIndex = 1
            Items.Strings = (
              '1'#170' parcela'
              #218'ltima parcela')
            TabOrder = 7
            OnClick = RGArredondarClick
          end
          object EdParcelas: TLMDEdit
            Left = 8
            Top = 36
            Width = 45
            Height = 21
            
            Caret.BlinkRate = 530
            TabOrder = 0
            OnExit = EdParcelasExit
            AutoSelect = True
            Alignment = taRightJustify
            CustomButtons = <>
            PasswordChar = #0
            Text = '2'
          end
          object RGPeriodo: TRadioGroup
            Left = 60
            Top = 16
            Width = 141
            Height = 45
            Caption = ' Periodo: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Mensal'
              '         dias')
            TabOrder = 1
            OnClick = RGPeriodoClick
          end
          object EdDias: TLMDEdit
            Left = 151
            Top = 32
            Width = 24
            Height = 21
            
            Caret.BlinkRate = 530
            Color = clBtnFace
            CtlXP = False
            Enabled = False
            TabOrder = 2
            OnExit = EdDiasExit
            AutoSelect = True
            Alignment = taRightJustify
            CustomButtons = <>
            PasswordChar = #0
            Text = '7'
          end
          object RGIncremCH: TRadioGroup
            Left = 204
            Top = 16
            Width = 101
            Height = 45
            Caption = ' Increm. cheque.: '
            Columns = 2
            Enabled = False
            ItemIndex = 1
            Items.Strings = (
              'N'#227'o'
              'Sim')
            TabOrder = 3
            OnClick = RGIncremCHClick
          end
          object EdParcela1: TLMDEdit
            Left = 508
            Top = 40
            Width = 77
            Height = 21
            
            Caret.BlinkRate = 530
            Color = clBtnFace
            TabOrder = 4
            AutoSelect = True
            Alignment = taRightJustify
            CustomButtons = <>
            PasswordChar = #0
            ReadOnly = True
            Text = '0,00'
          end
          object EdParcelaX: TLMDEdit
            Left = 588
            Top = 40
            Width = 69
            Height = 21
            
            Caret.BlinkRate = 530
            Color = clBtnFace
            TabOrder = 5
            AutoSelect = True
            Alignment = taRightJustify
            CustomButtons = <>
            PasswordChar = #0
            ReadOnly = True
            Text = '0,00'
          end
          object CkArredondar: TCheckBox
            Left = 320
            Top = 14
            Width = 77
            Height = 17
            Caption = 'Arredondar: '
            TabOrder = 6
            OnClick = CkArredondarClick
          end
          object EdSoma: TLMDEdit
            Left = 660
            Top = 39
            Width = 97
            Height = 21
            
            Caret.BlinkRate = 530
            Color = clBtnFace
            TabOrder = 8
            TabStop = False
            AutoSelect = True
            Alignment = taRightJustify
            CustomButtons = <>
            PasswordChar = #0
            ReadOnly = True
            Text = '0,00'
          end
          object RGIncremDupl: TGroupBox
            Left = 8
            Top = 66
            Width = 381
            Height = 73
            Caption = '   Incremento de duplicata: '
            TabOrder = 10
            Visible = False
            object Label27: TLabel
              Left = 4
              Top = 36
              Width = 52
              Height = 13
              Caption = 'Separador:'
            end
            object EdDuplSep: TEdit
              Left = 60
              Top = 32
              Width = 21
              Height = 21
              TabOrder = 0
              Text = '/'
              OnExit = EdDuplSepExit
            end
            object RGDuplSeq: TRadioGroup
              Left = 84
              Top = 16
              Width = 293
              Height = 53
              Caption = ' Sequenciador: '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'Num'#233'rico: 1, 2, 3, etc.'
                'Alfab'#233'tico: A, B, C, etc.')
              TabOrder = 1
              OnClick = RGDuplSeqClick
            end
          end
          object GBIncremTxt: TGroupBox
            Left = 392
            Top = 66
            Width = 381
            Height = 73
            Caption = '       '
            TabOrder = 11
            Visible = False
            object Label30: TLabel
              Left = 4
              Top = 36
              Width = 52
              Height = 13
              Caption = 'Separador:'
            end
            object EdSepTxt: TEdit
              Left = 60
              Top = 32
              Width = 21
              Height = 21
              TabOrder = 0
              Text = '/'
              OnExit = EdDuplSepExit
            end
            object RGSepTxt: TRadioGroup
              Left = 84
              Top = 20
              Width = 293
              Height = 49
              Caption = ' Sequenciador: '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'Num'#233'rico: 1, 2, 3, etc.'
                'Alfab'#233'tico: A, B, C, etc.')
              TabOrder = 1
              OnClick = RGDuplSeqClick
            end
          end
          object CkIncremDU: TCheckBox
            Left = 20
            Top = 63
            Width = 137
            Height = 17
            Caption = 'Incremento da duplicata: '
            TabOrder = 9
            OnClick = CkIncremDUClick
          end
          object CkIncremTxt: TCheckBox
            Left = 408
            Top = 64
            Width = 121
            Height = 17
            Caption = 'Incremento do texto: '
            TabOrder = 12
            OnClick = CkIncremTxtClick
          end
        end
        object CkParcelamento: TCheckBox
          Left = 12
          Top = 1
          Width = 153
          Height = 17
          Caption = ' Parcelamento autom'#225'tico: '
          TabOrder = 0
          Visible = False
          OnClick = CkParcelamentoClick
        end
      end
      object DBGParcelas: TDBGrid
        Left = 0
        Top = 156
        Width = 776
        Height = 184
        TabStop = False
        Align = alClient
        DataSource = DsParcPagtos
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnKeyDown = DBGParcelasKeyDown
        Columns = <
          item
            Expanded = False
            FieldName = 'Parcela'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Data'
            Title.Caption = 'Vencimento'
            Width = 65
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Debito'
            Title.Caption = 'D'#233'bito'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Credito'
            Title.Caption = 'Cr'#233'dito'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Doc'
            Title.Caption = 'Docum.'
            Width = 49
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Mora'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Multa'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ICMS_V'
            Title.Caption = '$ ICMS'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Duplicata'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descricao'
            Visible = True
          end>
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Hist'#243'rico'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object TabControl1: TTabControl
        Left = 0
        Top = 0
        Width = 776
        Height = 340
        Align = alClient
        TabOrder = 0
        Tabs.Strings = (
          'Carteira'
          'Cliente interno'
          'Cliente'
          'Fornecedor'
          'Conta')
        TabIndex = 0
        OnChange = TabControl1Change
        ExplicitWidth = 784
        ExplicitHeight = 342
        object DBGrid1: TDBGrid
          Left = 4
          Top = 24
          Width = 768
          Height = 312
          Align = alClient
          DataSource = DsLanctos
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Data'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Documento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMERELACIONADO'
              Title.Caption = 'Empresa'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NotaFiscal'
              Title.Caption = 'N.F.'
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'Duplicata'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Caption = 'D'#233'bito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Caption = 'Cr'#233'dito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencim.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COMPENSADO_TXT'
              Title.Caption = 'Compen.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MENSAL'
              Title.Caption = 'M'#234's'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESIT'
              Title.Caption = 'Situa'#231#227'o'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Lan'#231'amento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sub'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO'
              Title.Caption = 'Saldo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ICMS_P'
              Title.Caption = '% ICMS'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ICMS_V'
              Title.Caption = '$ ICMS'
              Visible = True
            end>
        end
      end
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 468
    Top = 32
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 468
    Top = 4
  end
  object DsFornecedores: TDataSource
    DataSet = QrFornecedores
    Left = 260
    Top = 324
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 516
    Top = 376
  end
  object DsNF: TDataSource
    DataSet = QrNF
    Left = 160
    Top = 5
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrContasAfterScroll
    SQL.Strings = (
      'SELECT co.*, sg.Nome NOMESUBGRUPO ,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'CASE 1'
      'WHEN cl.Tipo=0 THEN cl.RazaoSocial'
      'ELSE cl.Nome END NOMEEMPRESA'
      'FROM contas co, Subgrupos sg,'
      'Grupos gr, Conjuntos cj, Entidades cl'
      'WHERE sg.Codigo=co.Subgrupo'
      'AND gr.Codigo=sg.Grupo'
      'AND cj.Codigo=gr.Conjunto'
      'AND cl.Codigo=co.Empresa'
      'AND co.Terceiro=0'
      'AND co.Codigo>0'
      'ORDER BY co.Nome')
    Left = 440
    Top = 32
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrContasNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object QrContasNome3: TWideStringField
      FieldName = 'Nome3'
      Required = True
      Size = 50
    end
    object QrContasID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrContasSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrContasEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrContasCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContasDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContasMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContasExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrContasMensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrContasMensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrContasMensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrContasMenscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrContasMensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrContasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContasTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrContasExcel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrContasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContasUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrContasUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrContasNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Required = True
      Size = 50
    end
    object QrContasNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Required = True
      Size = 50
    end
    object QrContasNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Required = True
      Size = 50
    end
    object QrContasNOMEEMPRESA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCarteirasAfterScroll
    SQL.Strings = (
      
        'SELECT Codigo, Nome, Fatura, Fechamento, Prazo, Tipo, ExigeNumCh' +
        'eque'
      'FROM carteiras '
      'WHERE ForneceI=:P0'
      'ORDER BY Nome')
    Left = 440
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrCarteirasFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrCarteirasFechamento: TIntegerField
      FieldName = 'Fechamento'
    end
    object QrCarteirasPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCarteirasExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
    end
  end
  object QrFatura: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(Data) Data '
      'FROM faturas'
      'WHERE Emissao=:P0')
    Left = 504
    Top = 28
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFaturaData: TDateField
      FieldKind = fkInternalCalc
      FieldName = 'Data'
      Required = True
    end
  end
  object QrNF: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT la.*, co.Nome CONTA,'
      'CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMECLIENTE '
      'FROM lanctos la, Contas co, Entidades en'
      'WHERE co.Codigo=la.Genero'
      'AND la.Credito>0 '
      'AND en.Codigo=la.Cliente'
      'AND la.NotaFiscal=:P0')
    Left = 132
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFCONTA: TWideStringField
      FieldName = 'CONTA'
      Required = True
      Size = 50
    end
    object QrNFNOMECLIENTE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrNFData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrNFTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrNFCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrNFControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrNFSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrNFAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrNFGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrNFDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrNFNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrNFDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrNFCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrNFCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrNFDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrNFSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrNFVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrNFLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrNFID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrNFID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrNFFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrNFBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrNFLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrNFCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrNFLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrNFOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrNFLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrNFPago: TFloatField
      FieldName = 'Pago'
    end
    object QrNFMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrNFFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrNFCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrNFMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrNFMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrNFProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrNFDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrNFUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrNFDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrNFCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrNFNivel: TIntegerField
      FieldName = 'Nivel'
      Required = True
    end
    object QrNFVendedor: TIntegerField
      FieldName = 'Vendedor'
      Required = True
    end
    object QrNFAccount: TIntegerField
      FieldName = 'Account'
      Required = True
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Account,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'OR Cliente2="V"'
      'OR Cliente3="V"'
      'OR Cliente4="V"'
      'OR Terceiro="V"'
      'ORDER BY NomeENTIDADE')
    Left = 488
    Top = 376
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrClientesAccount: TIntegerField
      FieldName = 'Account'
      Required = True
    end
  end
  object QrFornecedores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece1="V"'
      'OR Fornece2="V"'
      'OR Fornece3="V"'
      'OR Fornece4="V"'
      'OR Fornece5="V"'
      'OR Fornece6="V"'
      'OR Terceiro="V"'
      'ORDER BY NomeENTIDADE')
    Left = 232
    Top = 324
    object QrFornecedoresCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFornecedoresNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object QrVendedores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE 1'
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NomeENTIDADE')
    Left = 188
    Top = 456
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object StringField1: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsVendedores: TDataSource
    DataSet = QrVendedores
    Left = 216
    Top = 456
  end
  object DsAccounts: TDataSource
    DataSet = QrAccounts
    Left = 496
    Top = 456
  end
  object QrAccounts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,  '
      'CASE 1'
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NomeENTIDADE')
    Left = 468
    Top = 456
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object StringField2: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object QrFunci: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NomeENTIDADE')
    Left = 104
    Top = 416
    object QrFunciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFunciNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsFunci: TDataSource
    DataSet = QrFunci
    Left = 132
    Top = 416
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCliIntAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Account,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'OR Cliente2="V"'
      'ORDER BY NomeENTIDADE')
    Left = 488
    Top = 332
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrCliIntNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 516
    Top = 332
  end
  object QrLanctos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2, '
      '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, '
      'la.*, ct.Codigo CONTA, '
      'ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial '
      'ELSE em.Nome END NOMEEMPRESA, '
      'CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial '
      'ELSE cl.Nome END NOMECLIENTE,'
      'CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial '
      'ELSE fo.Nome END NOMEFORNECEDOR,'
      'ca.Nome NOMECARTEIRA, ca.Saldo SALDOCARTEIRA'
      'FROM lanctos la'
      
        'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = ' +
        '0'
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo'
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      'ORDER BY la.Data, la.Controle')
    Left = 544
    Top = 8
    object QrLanctosData: TDateField
      FieldName = 'Data'
      Origin = 'lanctos.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLanctosTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'lanctos.Tipo'
    end
    object QrLanctosCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'lanctos.Carteira'
    end
    object QrLanctosAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      Origin = 'lanctos.Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrLanctosGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'lanctos.Genero'
    end
    object QrLanctosDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'lanctos.Descricao'
      Size = 25
    end
    object QrLanctosNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'lanctos.NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrLanctosDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'lanctos.Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLanctosCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'lanctos.Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLanctosCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'lanctos.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLanctosDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'lanctos.Documento'
      DisplayFormat = '000000; ; '
    end
    object QrLanctosSit: TIntegerField
      FieldName = 'Sit'
      Origin = 'lanctos.Sit'
    end
    object QrLanctosVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'lanctos.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLanctosLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'lanctos.Lk'
    end
    object QrLanctosFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'lanctos.FatID'
    end
    object QrLanctosFatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'lanctos.FatNum'
    end
    object QrLanctosFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'lanctos.FatParcela'
    end
    object QrLanctosCONTA: TIntegerField
      FieldName = 'CONTA'
      Origin = 'contas.Codigo'
    end
    object QrLanctosNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      FixedChar = True
      Size = 128
    end
    object QrLanctosNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Origin = 'contas.Nome'
      Size = 128
    end
    object QrLanctosNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Origin = 'subgrupos.Nome'
      Size = 4
    end
    object QrLanctosNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Origin = 'grupos.Nome'
      Size = 4
    end
    object QrLanctosNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Origin = 'conjuntos.Nome'
      Size = 4
    end
    object QrLanctosNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrLanctosAno: TFloatField
      FieldName = 'Ano'
    end
    object QrLanctosMENSAL: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrLanctosMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrLanctosBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLanctosLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrLanctosFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrLanctosSub: TSmallintField
      FieldName = 'Sub'
      DisplayFormat = '00; ; '
    end
    object QrLanctosCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrLanctosLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrLanctosPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLanctosSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrLanctosID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrLanctosMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLanctosFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLanctoscliente: TIntegerField
      FieldName = 'cliente'
    end
    object QrLanctosMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLanctosNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrLanctosNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrLanctosTIPOEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrLanctosNOMERELACIONADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMERELACIONADO'
      Size = 50
      Calculated = True
    end
    object QrLanctosOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrLanctosLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrLanctosMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLanctosATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrLanctosJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLanctosDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLanctosNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrLanctosVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLanctosAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrLanctosMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrLanctosProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrLanctosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLanctosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLanctosUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrLanctosUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrLanctosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLanctosID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrLanctosCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrLanctosFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLanctosICMS_P: TFloatField
      FieldName = 'ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLanctosICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLanctosDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrLanctosCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrLanctosCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLanctosNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrLanctosSALDOCARTEIRA: TFloatField
      FieldName = 'SALDOCARTEIRA'
    end
  end
  object DsLanctos: TDataSource
    DataSet = QrLanctos
    Left = 572
    Top = 8
  end
  object DsAptos: TDataSource
    DataSet = QrAptos
    Left = 472
    Top = 252
  end
  object QrAptos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Conta, Unidade, Propriet'
      'FROM condimov'
      'WHERE Codigo=:P0'
      'ORDER BY Unidade')
    Left = 444
    Top = 252
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAptosConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrAptosUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrAptosPropriet: TIntegerField
      FieldName = 'Propriet'
    end
  end
  object QrCliCli: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Account,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY NomeENTIDADE')
    Left = 328
    Top = 284
    object QrCliCliCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliCliAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrCliCliNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsCliCli: TDataSource
    DataSet = QrCliCli
    Left = 356
    Top = 284
  end
  object QrDuplCH: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDuplCHCalcFields
    SQL.Strings = (
      'SELECT lan.Data, lan.Controle, lan.Descricao, lan.Credito,'
      'lan.Debito, lan.NotaFiscal, lan.Compensado, lan.Mez,'
      'lan.Fornecedor, lan.Cliente, car.Nome NOMECART,'
      'lan.Documento, lan.SerieCH, lan.Carteira,'
      'IF(lan.Cliente<>0, CASE WHEN cli.Tipo=0 THEN '
      'cli.RazaoSocial ELSE cli.Nome END, "") NOMECLI,'
      'IF(lan.Fornecedor<>0, CASE WHEN fnc.Tipo=0 THEN '
      'fnc.RazaoSocial ELSE fnc.Nome END, "") NOMEFNC'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN entidades cli ON cli.Codigo=lan.Cliente'
      'LEFT JOIN entidades fnc ON fnc.Codigo=lan.Fornecedor'
      'WHERE ID_Pgto = 0')
    Left = 12
    Top = 4
    object QrDuplCHData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDuplCHControle: TIntegerField
      FieldName = 'Controle'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrDuplCHDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrDuplCHCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDuplCHDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDuplCHNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      DisplayFormat = '000000;-000000; '
    end
    object QrDuplCHCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDuplCHMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrDuplCHFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrDuplCHCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrDuplCHNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Size = 100
    end
    object QrDuplCHNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrDuplCHNOMEFNC: TWideStringField
      FieldName = 'NOMEFNC'
      Size = 100
    end
    object QrDuplCHDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrDuplCHSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrDuplCHCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrDuplCHTERCEIRO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TERCEIRO'
      Size = 255
      Calculated = True
    end
    object QrDuplCHMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 8
      Calculated = True
    end
    object QrDuplCHCHEQUE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CHEQUE'
      Calculated = True
    end
    object QrDuplCHCOMP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMP_TXT'
      Size = 10
      Calculated = True
    end
  end
  object DsDuplCH: TDataSource
    DataSet = QrDuplCH
    Left = 40
    Top = 4
  end
  object DsDuplNF: TDataSource
    DataSet = QrDuplNF
    Left = 96
    Top = 4
  end
  object QrDuplNF: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDuplNFCalcFields
    SQL.Strings = (
      'SELECT lan.Data, lan.Controle, lan.Descricao, lan.Credito, '
      'lan.Debito, lan.NotaFiscal, lan.Compensado, lan.Mez, SerieCH,'
      'lan.Fornecedor, lan.Cliente, car.Nome NOMECART, Documento, '
      'IF(lan.Cliente<>0, CASE WHEN cli.Tipo=0 THEN '
      'cli.RazaoSocial ELSE cli.Nome END, "") NOMECLI,'
      'IF(lan.Fornecedor<>0, CASE WHEN fnc.Tipo=0 THEN '
      'fnc.RazaoSocial ELSE fnc.Nome END, "") NOMEFNC,'
      'lan.Carteira'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN entidades cli ON cli.Codigo=lan.Cliente'
      'LEFT JOIN entidades fnc ON fnc.Codigo=lan.Fornecedor'
      'WHERE ID_Pgto = 0'
      'AND NotaFiscal=:P0')
    Left = 68
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDuplNFData: TDateField
      FieldName = 'Data'
      Origin = 'lanctos.Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDuplNFControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'lanctos.Controle'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrDuplNFDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'lanctos.Descricao'
      Size = 100
    end
    object QrDuplNFCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'lanctos.Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDuplNFDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'lanctos.Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDuplNFNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'lanctos.NotaFiscal'
      DisplayFormat = '000000;-000000; '
    end
    object QrDuplNFCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'lanctos.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDuplNFMez: TIntegerField
      FieldName = 'Mez'
      Origin = 'lanctos.Mez'
      Required = True
    end
    object QrDuplNFFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Origin = 'lanctos.Fornecedor'
    end
    object QrDuplNFCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'lanctos.Cliente'
    end
    object QrDuplNFNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrDuplNFNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrDuplNFNOMEFNC: TWideStringField
      FieldName = 'NOMEFNC'
      Size = 100
    end
    object QrDuplNFDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'lanctos.Documento'
    end
    object QrDuplNFSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Origin = 'lanctos.SerieCH'
      Size = 10
    end
    object QrDuplNFCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'lanctos.Carteira'
      Required = True
    end
    object QrDuplNFTERCEIRO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TERCEIRO'
      Size = 255
      Calculated = True
    end
    object QrDuplNFMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 8
      Calculated = True
    end
    object QrDuplNFCHEQUE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CHEQUE'
      Calculated = True
    end
    object QrDuplNFCOMP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMP_TXT'
      Size = 10
      Calculated = True
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Nome LIKE :P0'
      'ORDER BY Nome')
    Left = 624
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'contas.Codigo'
    end
    object QrPesqNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'contas.Nome'
      Size = 50
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 652
    Top = 144
  end
  object TbParcpagtos: TmySQLTable
    Database = Dmod.MyLocDatabase
    TableName = 'parcpagtos'
    Left = 621
    Top = 69
    object TbParcpagtosParcela: TIntegerField
      FieldName = 'Parcela'
    end
    object TbParcpagtosData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object TbParcpagtosCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object TbParcpagtosDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object TbParcpagtosDoc: TLargeintField
      FieldName = 'Doc'
    end
    object TbParcpagtosMora: TFloatField
      FieldName = 'Mora'
      DisplayFormat = '#,###,##0.00'
    end
    object TbParcpagtosMulta: TFloatField
      FieldName = 'Multa'
      DisplayFormat = '#,###,##0.00'
    end
    object TbParcpagtosICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '#,###,##0.00'
    end
    object TbParcpagtosDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 30
    end
    object TbParcpagtosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
  end
  object DsParcPagtos: TDataSource
    DataSet = TbParcpagtos
    Left = 649
    Top = 69
  end
  object QrSoma: TmySQLQuery
    Database = Dmod.MyLocDatabase
    SQL.Strings = (
      'SELECT SUM(Credito+Debito) VALOR'
      'FROM parcpagtos')
    Left = 372
    Top = 373
    object QrSomaVALOR: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'VALOR'
    end
  end
end
