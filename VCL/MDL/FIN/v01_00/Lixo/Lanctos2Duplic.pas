unit Lanctos2Duplic;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
     
    Grids, DBGrids;

type
  TFmLanctos2Duplic = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    STNotaF: TStaticText;
    STCheque: TStaticText;
    DBGrid2: TDBGrid;
    DBGrid1: TDBGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FConfirma: Boolean;
  end;

  var
  FmLanctos2Duplic: TFmLanctos2Duplic;

implementation

//uses Lanctos2;

{$R *.DFM}

procedure TFmLanctos2Duplic.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLanctos2Duplic.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLanctos2Duplic.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLanctos2Duplic.FormCreate(Sender: TObject);
begin
  FConfirma := False;
end;

procedure TFmLanctos2Duplic.BtOKClick(Sender: TObject);
begin
  FConfirma := True;
  Close;
end;

end.
