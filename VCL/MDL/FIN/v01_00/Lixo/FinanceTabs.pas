unit FinanceTabs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms;

type
  TFinanceTabs = class(TObject)
  private
    { Private declarations }
    procedure CarregaTabela(Tabela: String);
  public
    { Public declarations }
    procedure TabelasFinance;
  end;

//const

var
  FinTabs: TFinanceTabs;

implementation

uses MyListas;

procedure TFinanceTabs.TabelasFinance;
begin
  CarregaTabela('Entidades');
  CarregaTabela('EntiGrupos');
  CarregaTabela('Lanctos');
  CarregaTabela('MotivosE');
  CarregaTabela('Senhas');
end;

procedure TFinanceTabs.CarregaTabela(Tabela: String);
begin
  if Uppercase(Tabela) = Uppercase('Entidades') then
  begin
    WriteLn(FmlTxt, '');
    WriteLn(FmlTxt, '* Defini��o da tabela master "Entidades"');
    WriteLn(FmlTxt, 'DefineFile(''Entidades'')');
    WriteLn(FmlTxt, '  Description(''Tabela master Entidades'')');
    //
    WriteLn(FmlTxt, '  DefineTag(''TAG_CODIGO'', ''Codigo''    ,      '''', True, False)');
    //WriteLn(FmlTxt, '  DefineTag(''TAG_NOME'',   ''IIF(Tipo=0, RazaoSocia, Nome)'' ,'''', False, False)');
    //
    WriteLn(FmlTxt, '  DefineField(''Codigo''    , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''RazaoSocia'', ''C'',  100,  0)');
    WriteLn(FmlTxt, '  DefineField(''Fantasia''  , ''C'',   60,  0)');
    WriteLn(FmlTxt, '  DefineField(''Respons1''  , ''C'',   60,  0)');
    WriteLn(FmlTxt, '  DefineField(''Respons2''  , ''C'',   60,  0)');
    WriteLn(FmlTxt, '  DefineField(''Pai''       , ''C'',   60,  0)');
    WriteLn(FmlTxt, '  DefineField(''Mae''       , ''C'',   60,  0)');
    WriteLn(FmlTxt, '  DefineField(''CNPJ''      , ''C'',   18,  0)');
    WriteLn(FmlTxt, '  DefineField(''IE''        , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''Nome''      , ''C'',  100,  0)');
    WriteLn(FmlTxt, '  DefineField(''Apelido''   , ''C'',   60,  0)');
    WriteLn(FmlTxt, '  DefineField(''CPF''       , ''C'',   18,  0)');
    WriteLn(FmlTxt, '  DefineField(''RG''        , ''C'',   15,  0)');
    WriteLn(FmlTxt, '  DefineField(''ELograd''   , ''N'',    2,  0)');
    WriteLn(FmlTxt, '  DefineField(''ERua''      , ''C'',   30,  0)');
    WriteLn(FmlTxt, '  DefineField(''ENumero''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''ECompl''    , ''C'',   30,  0)');
    WriteLn(FmlTxt, '  DefineField(''EBairro''   , ''C'',   30,  0)');
    WriteLn(FmlTxt, '  DefineField(''ECidade''   , ''C'',   25,  0)');
    WriteLn(FmlTxt, '  DefineField(''EUF''       , ''N'',    3,  0)');
    WriteLn(FmlTxt, '  DefineField(''ECEP''      , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''EPais''     , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''ETe1''      , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''Ete2''      , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''Ete3''      , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''ECel''      , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''EFax''      , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''EEmail''    , ''C'',  100,  0)');
    WriteLn(FmlTxt, '  DefineField(''EContato''  , ''C'',   60,  0)');
    WriteLn(FmlTxt, '  DefineField(''ENatal''    , ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''PLograd''   , ''N'',    2,  0)');
    WriteLn(FmlTxt, '  DefineField(''PRua''      , ''C'',   30,  0)');
    WriteLn(FmlTxt, '  DefineField(''PNumero''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''PCompl''    , ''C'',   30,  0)');
    WriteLn(FmlTxt, '  DefineField(''PBairro''   , ''C'',   30,  0)');
    WriteLn(FmlTxt, '  DefineField(''PCidade''   , ''C'',   25,  0)');
    WriteLn(FmlTxt, '  DefineField(''PUF''       , ''N'',    3,  0)');
    WriteLn(FmlTxt, '  DefineField(''PCEP''      , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''PPais''     , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''PTe1''      , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''Pte2''      , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''Pte3''      , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''PCel''      , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''PFax''      , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''PEmail''    , ''C'',  100,  0)');
    WriteLn(FmlTxt, '  DefineField(''PContato''  , ''C'',   60,  0)');
    WriteLn(FmlTxt, '  DefineField(''PNatal''    , ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''Sexo''      , ''C'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''Responsave'', ''C'',   60,  0)');
    WriteLn(FmlTxt, '  DefineField(''Profissao'' , ''C'',   60,  0)');
    WriteLn(FmlTxt, '  DefineField(''Cargo''     , ''C'',   60,  0)');
    WriteLn(FmlTxt, '  DefineField(''Recibo''    , ''N'',    3,  0)');
    WriteLn(FmlTxt, '  DefineField(''DiaRecibo'' , ''N'',    2,  0)');
    WriteLn(FmlTxt, '  DefineField(''AjudaEmpV'' , ''F'',   15,  2)');
    WriteLn(FmlTxt, '  DefineField(''AjudaEmpP'' , ''F'',   15,  2)');
    WriteLn(FmlTxt, '  DefineField(''Cliente1''  , ''C'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''Cliente2''  , ''C'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''Fornece1''  , ''C'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''Fornece2''  , ''C'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''Fornece3''  , ''C'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''Fornece4''  , ''C'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''Terceiro''  , ''C'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''Cadastro''  , ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''Informacoe'', ''C'',  255,  0)');
//    WriteLn(FmlTxt, '  DefineField(''Logo''      , ''?'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''Veiculo''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Mensal''    , ''C'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''Observacoe'', ''M'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''Tipo''      , ''N'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''CLograd''   , ''N'',    2,  0)');
    WriteLn(FmlTxt, '  DefineField(''CRua''      , ''C'',   30,  0)');
    WriteLn(FmlTxt, '  DefineField(''CNumero''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''CCompl''    , ''C'',   30,  0)');
    WriteLn(FmlTxt, '  DefineField(''CBairro''   , ''C'',   30,  0)');
    WriteLn(FmlTxt, '  DefineField(''CCidade''   , ''C'',   25,  0)');
    WriteLn(FmlTxt, '  DefineField(''CUF''       , ''N'',    3,  0)');
    WriteLn(FmlTxt, '  DefineField(''CCEP''      , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''CPais''     , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''CTel''      , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''CCel''      , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''CFax''      , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''CContato''  , ''C'',   60,  0)');
    WriteLn(FmlTxt, '  DefineField(''LLograd''   , ''N'',    2,  0)');
    WriteLn(FmlTxt, '  DefineField(''LRua''      , ''C'',   30,  0)');
    WriteLn(FmlTxt, '  DefineField(''LNumero''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''LCompl''    , ''C'',   30,  0)');
    WriteLn(FmlTxt, '  DefineField(''LBairro''   , ''C'',   30,  0)');
    WriteLn(FmlTxt, '  DefineField(''LCidade''   , ''C'',   25,  0)');
    WriteLn(FmlTxt, '  DefineField(''LUF''       , ''N'',    3,  0)');
    WriteLn(FmlTxt, '  DefineField(''LCEP''      , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''LPais''     , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''LTel''      , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''LCel''      , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''LFax''      , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''LContato''  , ''C'',   60,  0)');
    WriteLn(FmlTxt, '  DefineField(''Comissao''  , ''F'',   15,  6)');
    WriteLn(FmlTxt, '  DefineField(''Situacao''  , ''N'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''Nivel''     , ''C'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''Grupo''     , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Account''   , ''N'',    8,  0)');
//    WriteLn(FmlTxt, '  DefineField(''Logo2''     , ''?'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''ConjugeNom'', ''C'',   35,  0)');
    WriteLn(FmlTxt, '  DefineField(''ConjugeNat'', ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''Nome1''     , ''C'',   30,  0)');
    WriteLn(FmlTxt, '  DefineField(''Natal1''    , ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''Nome2''     , ''C'',   30,  0)');
    WriteLn(FmlTxt, '  DefineField(''Natal2''    , ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''Nome3''     , ''C'',   30,  0)');
    WriteLn(FmlTxt, '  DefineField(''Natal3''    , ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''Nome4''     , ''C'',   30,  0)');
    WriteLn(FmlTxt, '  DefineField(''Natal4''    , ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''CreditosI'' , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''CreditosL'' , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''CreditosF2'', ''F'',   15,  2)');
    WriteLn(FmlTxt, '  DefineField(''CreditosD'' , ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''CreditosU'' , ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''CreditosV'' , ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''Motivo''    , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''QuantI1''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''QuantI2''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''QuantI3''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''QuantI4''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''QuantN1''   , ''F'',   15,  2)');
    WriteLn(FmlTxt, '  DefineField(''QuantN2''   , ''F'',   15,  2)');
    WriteLn(FmlTxt, '  DefineField(''Agenda''    , ''C'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''SenhaQuer'' , ''C'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''Senha1''    , ''C'',    6,  0)');
    WriteLn(FmlTxt, '  DefineField(''Lk''        , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''DataCad''   , ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''DataAlt''   , ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''UserCad''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''UserAlt''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''LimiCred''  , ''F'',   15,  2)');
    WriteLn(FmlTxt, '  DefineField(''IEST''      , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''Desco''     , ''F'',   15,  6)');
    WriteLn(FmlTxt, '  DefineField(''CasasApliD'', ''N'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''TempD''     , ''F'',   15,  2)');
    WriteLn(FmlTxt, '  DefineField(''NIRE''      , ''F'',   11,  0)');
    WriteLn(FmlTxt, '  DefineField(''FormaSocie'', ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''Simples''   , ''N'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''Atividade'' , ''C'',   50,  0)');
    WriteLn(FmlTxt, '  DefineField(''CPF_Pai''   , ''C'',   18,  0)');
    WriteLn(FmlTxt, '  DefineField(''CPF_Conjug'', ''C'',   18,  0)');
    WriteLn(FmlTxt, '  DefineField(''SSP''       , ''C'',   10,  0)');
    WriteLn(FmlTxt, '  DefineField(''DataRG''    , ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''CidadeNata'', ''C'',   30,  0)');
    WriteLn(FmlTxt, '  DefineField(''EstCivil''  , ''N'',    3,  0)');
    WriteLn(FmlTxt, '  DefineField(''UFNatal''   , ''N'',    3,  0)');
    WriteLn(FmlTxt, '  DefineField(''Nacionalid'', ''C'',   15,  0)');
    WriteLn(FmlTxt, '  DefineField(''Fornece5''  , ''C'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''Fornece6''  , ''C'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''QuantN3''   , ''F'',   15,  2)');
    WriteLn(FmlTxt, '  DefineField(''TempA''     , ''F'',   15,  2)');
    WriteLn(FmlTxt, '  DefineField(''Banco''     , ''N'',    4,  0)');
    WriteLn(FmlTxt, '  DefineField(''Agencia''   , ''C'',   11,  0)');
    WriteLn(FmlTxt, '  DefineField(''ContaCorre'', ''C'',   15,  0)');
    WriteLn(FmlTxt, '  DefineField(''FatorCompr'', ''F'',   15,  4)');
    WriteLn(FmlTxt, '  DefineField(''AdValorem'' , ''F'',   15,  4)');
    WriteLn(FmlTxt, '  DefineField(''DMaisC''    , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''DMaisD''    , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Empresa''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''CBE''       , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''SCB''       , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''PAtividad'' , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''EAtividad'' , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''PCidadeCod'', ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''ECidadeCod'', ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''PPaisCod''  , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''EPaisCod''  , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Antigo''    , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''CUF2''      , ''C'',    2,  0)');
    WriteLn(FmlTxt, '  DefineField(''Contab''    , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''MSN1''      , ''C'',  255,  0)');
    WriteLn(FmlTxt, '  DefineField(''PastaTxtFT'', ''C'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''PastaPwdFT'', ''C'',  100,  0)');
    WriteLn(FmlTxt, '  DefineField(''Protestar'' , ''N'',    2,  0)');
    WriteLn(FmlTxt, '  DefineField(''MultaCodi'' , ''N'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''MultaDias'' , ''N'',    2,  0)');
    WriteLn(FmlTxt, '  DefineField(''MultaValr'' , ''F'',   15,  2)');
    WriteLn(FmlTxt, '  DefineField(''MultaPerc'' , ''F'',   15,  2)');
    WriteLn(FmlTxt, '  DefineField(''MultaTiVe'' , ''N'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''JuroSacado'', ''F'',   15,  2)');
    WriteLn(FmlTxt, '  DefineField(''CPMF''      , ''F'',   15,  4)');
    WriteLn(FmlTxt, '  DefineField(''Corrido''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''CPF_Resp1'' , ''C'',   18,  0)');
    WriteLn(FmlTxt, '');
  end else
  if Uppercase(Tabela) = Uppercase('EntiGrupos') then
  begin
    WriteLn(FmlTxt, '');
    WriteLn(FmlTxt, '* Defini��o da tabela master "entigrupos"');
    WriteLn(FmlTxt, 'DefineFile(''entigrupos'')');
    WriteLn(FmlTxt, '  Description(''Tabela master entigrupos'')');
    WriteLn(FmlTxt, '  DefineField(''Codigo''    , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Nome''      , ''C'',  100,  0)');
    WriteLn(FmlTxt, '  DefineField(''Desconto''  , ''F'',   15,  6)');
    WriteLn(FmlTxt, '  DefineField(''Lk''        , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''DataCad''   , ''T'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''DataAlt''   , ''T'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''UserCad''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''UserAlt''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '');
  end else
  if Uppercase(Tabela) = Uppercase('Lanctos') then
  begin
    WriteLn(FmlTxt, '');
    WriteLn(FmlTxt, '* Defini��o da tabela master "lanctos"');
    WriteLn(FmlTxt, 'DefineFile(''lanctos'')');
    WriteLn(FmlTxt, '  Description(''Tabela master lanctos'')');
    WriteLn(FmlTxt, '  DefineTag(''TAG_FATNUMa'', ''FatNum''     ,''FatID=35'', False, False)');
    WriteLn(FmlTxt, '  DefineField(''Data''      , ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''Tipo''      , ''N'',    4,  0)');
    WriteLn(FmlTxt, '  DefineField(''Carteira''  , ''N'',    9,  0)');
    WriteLn(FmlTxt, '  DefineField(''Controle''  , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Sub''       , ''N'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''Autorizaca'', ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Genero''    , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Descricao'' , ''C'',  100,  0)');
    WriteLn(FmlTxt, '  DefineField(''NotaFiscal'', ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Debito''    , ''F'',   15,  2)');
    WriteLn(FmlTxt, '  DefineField(''Credito''   , ''F'',   15,  2)');
    WriteLn(FmlTxt, '  DefineField(''Compensado'', ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''Documento'' , ''F'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''Sit''       , ''N'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''Vencimento'', ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''FatID''     , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''FatNum''    , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''FatParcela'', ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''ID_Pgto''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''ID_Sub''    , ''N'',    4,  0)');
    WriteLn(FmlTxt, '  DefineField(''Fatura''    , ''C'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''Banco''     , ''N'',    4,  0)');
    WriteLn(FmlTxt, '  DefineField(''Local''     , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Cartao''    , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Linha''     , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''OperCount'' , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Lancto''    , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Pago''      , ''F'',   15,  2)');
    WriteLn(FmlTxt, '  DefineField(''Mez''       , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Fornecedor'', ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Cliente''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''MoraDia''   , ''F'',   13,  2)');
    WriteLn(FmlTxt, '  DefineField(''Multa''     , ''F'',   13,  2)');
    WriteLn(FmlTxt, '  DefineField(''Protesto''  , ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''DataDoc''   , ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''CtrlIni''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Nivel''     , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Vendedor''  , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Account''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Lk''        , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''DataCad''   , ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''DataAlt''   , ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''UserCad''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''UserAlt''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''FatID_Sub'' , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''CliInt''    , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''ICMS_P''    , ''F'',    4,  2)');
    WriteLn(FmlTxt, '  DefineField(''ICMS_V''    , ''F'',   13,  2)');
    WriteLn(FmlTxt, '  DefineField(''Duplicata'' , ''C'',   13,  0)');
    WriteLn(FmlTxt, '  DefineField(''Qtde''      , ''F'',   15,  3)');
    WriteLn(FmlTxt, '  DefineField(''Emitente''  , ''C'',   30,  0)');
    WriteLn(FmlTxt, '  DefineField(''Agencia''   , ''C'',   11,  0)');
    WriteLn(FmlTxt, '  DefineField(''ContaCorre'', ''C'',   15,  0)');
    WriteLn(FmlTxt, '  DefineField(''CNPJCPF''   , ''C'',   15,  0)');
    WriteLn(FmlTxt, '  DefineField(''ForneceI''  , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Depto''     , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''DescoPor''  , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''DescoVal''  , ''F'',   15,  2)');
    WriteLn(FmlTxt, '  DefineField(''DescoContr'', ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''NFVal''     , ''F'',   15,  2)');
    WriteLn(FmlTxt, '  DefineField(''Antigo''    , ''C'',   20,  0)');
    WriteLn(FmlTxt, '  DefineField(''Unidade''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '');
  end else
  if Uppercase(Tabela) = Uppercase('MotivosE') then
  begin
    WriteLn(FmlTxt, '');
    WriteLn(FmlTxt, '* Defini��o da tabela master "motivose"');
    WriteLn(FmlTxt, 'DefineFile(''motivose'')');
    WriteLn(FmlTxt, '  Description(''Tabela master motivose'')');
    WriteLn(FmlTxt, '  DefineField(''Codigo''    , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Descricao'' , ''C'',   50,  0)');
    WriteLn(FmlTxt, '  DefineField(''Lk''        , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''DataCad''   , ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''DataAlt''   , ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''UserCad''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''UserAlt''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '');
  end else
  if Uppercase(Tabela) = Uppercase('Senhas') then
  begin
    WriteLn(FmlTxt, '');
    WriteLn(FmlTxt, '* Defini��o da tabela master "senhas"');
    WriteLn(FmlTxt, 'DefineFile(''senhas'')');
    WriteLn(FmlTxt, '  Description(''Tabela master senhas'')');
    WriteLn(FmlTxt, '  DefineField(''Login''     , ''C'',   30,  0)');
    WriteLn(FmlTxt, '  DefineField(''Numero''    , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Senha''     , ''C'',   30,  0)');
    WriteLn(FmlTxt, '  DefineField(''Perfil''    , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Funcionari'', ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''DataSenha'' , ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''SenhaDia''  , ''C'',   30,  0)');
    WriteLn(FmlTxt, '  DefineField(''Lk''        , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''DataCad''   , ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''DataAlt''   , ''D'',    0,  0)');
    WriteLn(FmlTxt, '  DefineField(''UserCad''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''UserAlt''   , ''N'',    8,  0)');
    WriteLn(FmlTxt, '  DefineField(''Ativo''     , ''C'',    1,  0)');
    WriteLn(FmlTxt, '  DefineField(''SitSenha''  , ''N'',    1,  0)');
    WriteLn(FmlTxt, '');
  end else
  Application.MessageBox(PChar('O texto da estrutura da tabela "'+Tabela+
  '" n�o foi definido!'), 'Aviso', MB_OK+MB_ICONWARNING);
end;

end.
