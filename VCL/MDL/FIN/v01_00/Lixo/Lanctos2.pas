unit Lanctos2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls,   UnMLAGeral, UnGOTOy,
      ComCtrls, UnMyLinguas,
  Db, mySQLDbTables, ExtCtrls,  
   Buttons, UnInternalConsts, UMySQLModule, Grids, DBGrids,
     
   LMDDBLabel, LMDCustomGroupBox, LMDCustomButtonGroup,
  LMDCustomCheckGroup, LMDCheckGroup, frxChBox, frxClass, UnFinanceiro;

type
  TTipoValor = (tvNil, tvCred, tvDeb);
  TFmLanctos2 = class(TForm)
    DsContas: TDataSource;
    DsCarteiras: TDataSource;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    DsFornecedores: TDataSource;
    DsClientes: TDataSource;
    DsNF: TDataSource;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    QrContas: TmySQLQuery;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasFechamento: TIntegerField;
    QrFatura: TmySQLQuery;
    QrFaturaData: TDateField;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasNome3: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrContasDataCad: TDateField;
    QrContasDataAlt: TDateField;
    QrContasUserCad: TSmallintField;
    QrContasUserAlt: TSmallintField;
    QrContasNOMESUBGRUPO: TWideStringField;
    QrContasNOMEGRUPO: TWideStringField;
    QrContasNOMECONJUNTO: TWideStringField;
    QrContasNOMEEMPRESA: TWideStringField;
    QrNF: TmySQLQuery;
    QrNFCONTA: TWideStringField;
    QrNFNOMECLIENTE: TWideStringField;
    QrClientes: TmySQLQuery;
    QrFornecedores: TmySQLQuery;
    QrFornecedoresCodigo: TIntegerField;
    QrFornecedoresNOMEENTIDADE: TWideStringField;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    QrVendedores: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsVendedores: TDataSource;
    DsAccounts: TDataSource;
    QrAccounts: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    QrClientesAccount: TIntegerField;
    QrNFData: TDateField;
    QrNFTipo: TSmallintField;
    QrNFCarteira: TIntegerField;
    QrNFControle: TIntegerField;
    QrNFSub: TSmallintField;
    QrNFAutorizacao: TIntegerField;
    QrNFGenero: TIntegerField;
    QrNFDescricao: TWideStringField;
    QrNFNotaFiscal: TIntegerField;
    QrNFDebito: TFloatField;
    QrNFCredito: TFloatField;
    QrNFCompensado: TDateField;
    QrNFDocumento: TFloatField;
    QrNFSit: TIntegerField;
    QrNFVencimento: TDateField;
    QrNFLk: TIntegerField;
    QrNFFatID: TIntegerField;
    QrNFFatNum: TIntegerField;
    QrNFFatParcela: TIntegerField;
    QrNFID_Pgto: TIntegerField;
    QrNFID_Sub: TSmallintField;
    QrNFFatura: TWideStringField;
    QrNFBanco: TIntegerField;
    QrNFLocal: TIntegerField;
    QrNFCartao: TIntegerField;
    QrNFLinha: TIntegerField;
    QrNFOperCount: TIntegerField;
    QrNFLancto: TIntegerField;
    QrNFPago: TFloatField;
    QrNFMez: TIntegerField;
    QrNFFornecedor: TIntegerField;
    QrNFCliente: TIntegerField;
    QrNFMoraDia: TFloatField;
    QrNFMulta: TFloatField;
    QrNFProtesto: TDateField;
    QrNFDataCad: TDateField;
    QrNFDataAlt: TDateField;
    QrNFUserCad: TSmallintField;
    QrNFUserAlt: TSmallintField;
    QrNFDataDoc: TDateField;
    QrNFCtrlIni: TIntegerField;
    QrNFNivel: TIntegerField;
    QrNFVendedor: TIntegerField;
    QrNFAccount: TIntegerField;
    QrFunci: TmySQLQuery;
    DsFunci: TDataSource;
    QrFunciCodigo: TIntegerField;
    QrFunciNOMEENTIDADE: TWideStringField;
    CkContinuar: TCheckBox;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrCliIntCodigo: TIntegerField;
    QrCliIntAccount: TIntegerField;
    QrCliIntNOMEENTIDADE: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PainelDados: TPanel;
    EdCartAnt: TLabel;
    EdTipoAnt: TLabel;
    LaFunci: TLabel;
    QrLanctos: TmySQLQuery;
    QrLanctosData: TDateField;
    QrLanctosTipo: TSmallintField;
    QrLanctosCarteira: TIntegerField;
    QrLanctosAutorizacao: TIntegerField;
    QrLanctosGenero: TIntegerField;
    QrLanctosDescricao: TWideStringField;
    QrLanctosNotaFiscal: TIntegerField;
    QrLanctosDebito: TFloatField;
    QrLanctosCredito: TFloatField;
    QrLanctosCompensado: TDateField;
    QrLanctosDocumento: TFloatField;
    QrLanctosSit: TIntegerField;
    QrLanctosVencimento: TDateField;
    QrLanctosLk: TIntegerField;
    QrLanctosFatID: TIntegerField;
    QrLanctosFatNum: TIntegerField;
    QrLanctosFatParcela: TIntegerField;
    QrLanctosCONTA: TIntegerField;
    QrLanctosNOMECONTA: TWideStringField;
    QrLanctosNOMEEMPRESA: TWideStringField;
    QrLanctosNOMESUBGRUPO: TWideStringField;
    QrLanctosNOMEGRUPO: TWideStringField;
    QrLanctosNOMECONJUNTO: TWideStringField;
    QrLanctosNOMESIT: TWideStringField;
    QrLanctosAno: TFloatField;
    QrLanctosMENSAL: TWideStringField;
    QrLanctosMENSAL2: TWideStringField;
    QrLanctosBanco: TIntegerField;
    QrLanctosLocal: TIntegerField;
    QrLanctosFatura: TWideStringField;
    QrLanctosSub: TSmallintField;
    QrLanctosCartao: TIntegerField;
    QrLanctosLinha: TIntegerField;
    QrLanctosPago: TFloatField;
    QrLanctosSALDO: TFloatField;
    QrLanctosID_Sub: TSmallintField;
    QrLanctosMez: TIntegerField;
    QrLanctosFornecedor: TIntegerField;
    QrLanctoscliente: TIntegerField;
    QrLanctosMoraDia: TFloatField;
    QrLanctosNOMECLIENTE: TWideStringField;
    QrLanctosNOMEFORNECEDOR: TWideStringField;
    QrLanctosTIPOEM: TWideStringField;
    QrLanctosNOMERELACIONADO: TWideStringField;
    QrLanctosOperCount: TIntegerField;
    QrLanctosLancto: TIntegerField;
    QrLanctosMulta: TFloatField;
    QrLanctosATRASO: TFloatField;
    QrLanctosJUROS: TFloatField;
    QrLanctosDataDoc: TDateField;
    QrLanctosNivel: TIntegerField;
    QrLanctosVendedor: TIntegerField;
    QrLanctosAccount: TIntegerField;
    QrLanctosMes2: TLargeintField;
    QrLanctosProtesto: TDateField;
    QrLanctosDataCad: TDateField;
    QrLanctosDataAlt: TDateField;
    QrLanctosUserCad: TSmallintField;
    QrLanctosUserAlt: TSmallintField;
    QrLanctosControle: TIntegerField;
    QrLanctosID_Pgto: TIntegerField;
    QrLanctosCtrlIni: TIntegerField;
    QrLanctosFatID_Sub: TIntegerField;
    QrLanctosICMS_P: TFloatField;
    QrLanctosICMS_V: TFloatField;
    QrLanctosDuplicata: TWideStringField;
    QrLanctosCOMPENSADO_TXT: TWideStringField;
    QrLanctosCliInt: TIntegerField;
    QrLanctosNOMECARTEIRA: TWideStringField;
    QrLanctosSALDOCARTEIRA: TFloatField;
    DsLanctos: TDataSource;
    TabSheet2: TTabSheet;
    DsAptos: TDataSource;
    QrAptos: TmySQLQuery;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasTipo: TIntegerField;
    QrAptosConta: TIntegerField;
    QrAptosUnidade: TWideStringField;
    QrAptosPropriet: TIntegerField;
    Label20: TLabel;
    QrCliCli: TmySQLQuery;
    DsCliCli: TDataSource;
    QrCliCliCodigo: TIntegerField;
    QrCliCliAccount: TIntegerField;
    QrCliCliNOMEENTIDADE: TWideStringField;
    TabSheet3: TTabSheet;
    QrCarteirasExigeNumCheque: TSmallintField;
    CkCopiaCH: TCheckBox;
    BitBtn1: TBitBtn;
    QrDuplCH: TmySQLQuery;
    DsDuplCH: TDataSource;
    DsDuplNF: TDataSource;
    QrDuplNF: TmySQLQuery;
    QrDuplCHData: TDateField;
    QrDuplCHControle: TIntegerField;
    QrDuplCHDescricao: TWideStringField;
    QrDuplCHCredito: TFloatField;
    QrDuplCHDebito: TFloatField;
    QrDuplCHNotaFiscal: TIntegerField;
    QrDuplCHCompensado: TDateField;
    QrDuplCHMez: TIntegerField;
    QrDuplCHFornecedor: TIntegerField;
    QrDuplCHCliente: TIntegerField;
    QrDuplCHNOMECART: TWideStringField;
    QrDuplCHNOMECLI: TWideStringField;
    QrDuplCHNOMEFNC: TWideStringField;
    QrDuplCHDocumento: TFloatField;
    QrDuplCHSerieCH: TWideStringField;
    QrDuplCHCarteira: TIntegerField;
    QrDuplCHTERCEIRO: TWideStringField;
    QrDuplCHMES: TWideStringField;
    QrDuplCHCHEQUE: TWideStringField;
    QrDuplCHCOMP_TXT: TWideStringField;
    QrDuplNFData: TDateField;
    QrDuplNFControle: TIntegerField;
    QrDuplNFDescricao: TWideStringField;
    QrDuplNFCredito: TFloatField;
    QrDuplNFDebito: TFloatField;
    QrDuplNFNotaFiscal: TIntegerField;
    QrDuplNFCompensado: TDateField;
    QrDuplNFMez: TIntegerField;
    QrDuplNFFornecedor: TIntegerField;
    QrDuplNFCliente: TIntegerField;
    QrDuplNFNOMECART: TWideStringField;
    QrDuplNFNOMECLI: TWideStringField;
    QrDuplNFNOMEFNC: TWideStringField;
    QrDuplNFDocumento: TFloatField;
    QrDuplNFSerieCH: TWideStringField;
    QrDuplNFCarteira: TIntegerField;
    QrDuplNFTERCEIRO: TWideStringField;
    QrDuplNFMES: TWideStringField;
    QrDuplNFCHEQUE: TWideStringField;
    QrDuplNFCOMP_TXT: TWideStringField;
    Panel2: TPanel;
    Label15: TLabel;
    Label14: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    SpeedButton1: TSpeedButton;
    BtContas: TSpeedButton;
    LaVencimento: TLabel;
    Label17: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    LaDoc: TLabel;
    Label16: TLabel;
    LaNF: TLabel;
    LaCred: TLabel;
    LaDeb: TLabel;
    LaMes: TLabel;
    Label13: TLabel;
    LaCliente: TLabel;
    LaFornecedor: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    LaCliInt: TLabel;
    Label5: TLabel;
    LaForneceRN: TLabel;
    LaForneceFA: TLabel;
    Label6: TLabel;
    LaForneceI: TLabel;
    SpeedButton2: TSpeedButton;
    EdCarteira: TLMDEdit;
    CBCarteira: TDBLookupComboBox;
    EdCodigo: TLMDEdit;
    TPData: TDateTimePicker;
    EdConta: TLMDEdit;
    CBConta: TDBLookupComboBox;
    EdMes: TLMDEdit;
    EdDeb: TLMDEdit;
    EdCred: TLMDEdit;
    EdNF: TLMDEdit;
    EdDoc: TLMDEdit;
    EdDuplicata: TLMDEdit;
    TPVencimento: TDateTimePicker;
    EdDescricao: TLMDEdit;
    EdCliente: TLMDEdit;
    CBCliente: TDBLookupComboBox;
    EdFornecedor: TLMDEdit;
    CBFornecedor: TDBLookupComboBox;
    EdQtde: TLMDEdit;
    EdCliInt: TLMDEdit;
    CBCliInt: TDBLookupComboBox;
    EdSerieCh: TLMDEdit;
    EdDepto: TLMDEdit;
    CBDepto: TDBLookupComboBox;
    EdCliCli: TLMDEdit;
    CBCliCli: TDBLookupComboBox;
    CkPesqCH: TCheckBox;
    CkPesqNF: TCheckBox;
    Panel4: TPanel;
    BtDesiste: TBitBtn;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    PnMaskPesq: TPanel;
    Panel5: TPanel;
    EdLinkMask: TEdit;
    Label18: TLabel;
    QrPesqCodigo: TIntegerField;
    QrPesqNome: TWideStringField;
    LLBPesq: TDBLookupListBox;
    PnLink: TPanel;
    Panel6: TPanel;
    SpeedButton3: TSpeedButton;
    TabSheet4: TTabSheet;
    TabControl1: TTabControl;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    Label9: TLabel;
    Label22: TLabel;
    TPDataDoc: TDateTimePicker;
    TPCompensado: TDateTimePicker;
    CkTipoCH: TLMDCheckGroup;
    Panel3: TPanel;
    Label10: TLabel;
    LaVendedor: TLabel;
    LaAccount: TLabel;
    LaICMS_P: TLabel;
    LaICMS_V: TLabel;
    EdFunci: TLMDEdit;
    CBFunci: TDBLookupComboBox;
    EdVendedor: TLMDEdit;
    CBVendedor: TDBLookupComboBox;
    EdAccount: TLMDEdit;
    CBAccount: TDBLookupComboBox;
    EdICMS_P: TLMDEdit;
    EdICMS_V: TLMDEdit;
    Panel7: TPanel;
    GBParcelamento: TGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    RGArredondar: TRadioGroup;
    EdParcelas: TLMDEdit;
    RGPeriodo: TRadioGroup;
    EdDias: TLMDEdit;
    RGIncremCH: TRadioGroup;
    EdParcela1: TLMDEdit;
    EdParcelaX: TLMDEdit;
    CkArredondar: TCheckBox;
    EdSoma: TLMDEdit;
    CkParcelamento: TCheckBox;
    DBGParcelas: TDBGrid;
    TbParcpagtos: TmySQLTable;
    TbParcpagtosParcela: TIntegerField;
    TbParcpagtosData: TDateField;
    TbParcpagtosCredito: TFloatField;
    TbParcpagtosDebito: TFloatField;
    TbParcpagtosDoc: TLargeintField;
    TbParcpagtosMora: TFloatField;
    TbParcpagtosMulta: TFloatField;
    TbParcpagtosICMS_V: TFloatField;
    TbParcpagtosDuplicata: TWideStringField;
    TbParcpagtosDescricao: TWideStringField;
    DsParcPagtos: TDataSource;
    QrSoma: TmySQLQuery;
    QrSomaVALOR: TFloatField;
    CkIncremDU: TCheckBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label8: TLabel;
    Label7: TLabel;
    EdMoraDia: TLMDEdit;
    EdMulta: TLMDEdit;
    GroupBox3: TGroupBox;
    Label19: TLabel;
    Label21: TLabel;
    EdMultaVal: TLMDEdit;
    EdMoraVal: TLMDEdit;
    GroupBox4: TGroupBox;
    Label28: TLabel;
    EdValNovo: TLMDEdit;
    Label29: TLabel;
    EdPerMult: TLMDEdit;
    BitBtn2: TBitBtn;
    RGIncremDupl: TGroupBox;
    Label27: TLabel;
    EdDuplSep: TEdit;
    RGDuplSeq: TRadioGroup;
    GBIncremTxt: TGroupBox;
    Label30: TLabel;
    EdSepTxt: TEdit;
    RGSepTxt: TRadioGroup;
    CkIncremTxt: TCheckBox;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdContaChange(Sender: TObject);
    procedure EdContaExit(Sender: TObject);
    procedure CBContaClick(Sender: TObject);
    procedure CBContaDropDown(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdCarteiraChange(Sender: TObject);
    procedure EdCarteiraExit(Sender: TObject);
    procedure CBCarteiraClick(Sender: TObject);
    procedure CBCarteiraDropDown(Sender: TObject);
    procedure EdMesExit(Sender: TObject);
    procedure BtContasClick(Sender: TObject);
    procedure EdDebExit(Sender: TObject);
    procedure EdCredExit(Sender: TObject);
    procedure EdNFExit(Sender: TObject);
    procedure EdDocExit(Sender: TObject);
    procedure EdMesKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdFornecedorChange(Sender: TObject);
    procedure EdFornecedorExit(Sender: TObject);
    procedure CBFornecedorClick(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure EdClienteExit(Sender: TObject);
    procedure CBClienteClick(Sender: TObject);
    procedure CBClienteDropDown(Sender: TObject);
    procedure EdDescricaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormResize(Sender: TObject);
    procedure EdMoraDiaExit(Sender: TObject);
    procedure EdMultaExit(Sender: TObject);
    procedure EdMoraDiaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdMultaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton1Click(Sender: TObject);
    procedure QrContasAfterScroll(DataSet: TDataSet);
    procedure EdAccountKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBAccountKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBFornecedorDropDown(Sender: TObject);
    procedure EdFunciChange(Sender: TObject);
    procedure EdFunciExit(Sender: TObject);
    procedure CBFunciClick(Sender: TObject);
    procedure CBFunciDropDown(Sender: TObject);
    procedure EdICMS_PExit(Sender: TObject);
    procedure EdICMS_VExit(Sender: TObject);
    procedure EdICMS_PKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdICMS_VKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdDocKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdDuplicataKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TabSheet2Resize(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure TPDataChange(Sender: TObject);
    procedure EdQtdeExit(Sender: TObject);
    procedure QrCarteirasAfterScroll(DataSet: TDataSet);
    procedure TPVencimentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdFornecedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBFornecedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCliIntChange(Sender: TObject);
    procedure EdCliIntExit(Sender: TObject);
    procedure CBCliIntClick(Sender: TObject);
    procedure CBCliIntDropDown(Sender: TObject);
    procedure EdDeptoChange(Sender: TObject);
    procedure QrCliIntAfterScroll(DataSet: TDataSet);
    procedure EdMultaValExit(Sender: TObject);
    procedure EdMoraValExit(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrDuplCHCalcFields(DataSet: TDataSet);
    procedure QrDuplNFCalcFields(DataSet: TDataSet);
    procedure EdContaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBContaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdLinkMaskChange(Sender: TObject);
    procedure LLBPesqDblClick(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure LLBPesqKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdLinkMaskKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBDeptoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CkParcelamentoClick(Sender: TObject);
    procedure EdParcelasExit(Sender: TObject);
    procedure RGPeriodoClick(Sender: TObject);
    procedure DBGParcelasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CkArredondarClick(Sender: TObject);
    procedure RGArredondarClick(Sender: TObject);
    procedure EdDiasExit(Sender: TObject);
    procedure RGIncremCHClick(Sender: TObject);
    procedure EdDocChange(Sender: TObject);
    procedure CkIncremDUClick(Sender: TObject);
    procedure EdDuplSepExit(Sender: TObject);
    procedure RGDuplSeqClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure CkIncremTxtClick(Sender: TObject);
  private
    { Private declarations }
    FCriandoForm: Boolean;
    FValorAParcelar: Double;
    procedure VerificaEdits;
    procedure CarteirasReopen;
    procedure SetaAccount(var Key: Word; Shift: TShiftState);
    procedure ExigeFuncionario;
    procedure CalculaICMS;
    function ReopenLanctos: Boolean;

    function VerificaVencimento: Boolean;
    procedure ConfiguraVencimento;
    procedure ConfiguraComponentesCarteira;
    procedure ReopenFornecedores(Tipo: Integer);
    //function NaoDuplicarLancto(): Boolean;
    procedure MostraPnMaskPesq(Link: String);
    procedure SelecionaItemDePesquisa;
    // Parcelamento
    procedure CalculaParcelas;
    function GetTipoValor: TTipoValor;

  public
    { Public declarations }
    FCNAB_Sit, FFatID, FFatNum, FFAtPArcela, FFatID_Sub, FID_Pgto, FICMS,
    FNivel, FDescoPor, FCtrlIni, FUnidade: Integer;
    FDescoVal, FNFVal: Double;
    FDoc2: String;
  end;

const
  FLargMaior = 800;
  FLargMenor = 584;

var
  FmLanctos2: TFmLanctos2;
  Pagto_Doc: Double;

implementation

uses UnMyObjects, Module, Contas, Principal, CondGer, Entidades, ModuleCond, EmiteCheque;

{$R *.DFM}

procedure TFmLanctos2.CalculaParcelas;
var
  i, Parce, DiasP, Casas: Integer;
  Valor, Valor1, ValorX, ValorA, ValorC, ValorD, Total, Mora, Multa, Fator: Double;
  Data: TDate;
  TipoValor: TTipoValor;
  Duplicata, Descricao, DuplSeq, DuplSep, Fmt, TxtSeq: String;
begin
  TbParcpagtos.Close;
  TbParcpagtos.Open;
  TbParcpagtos.DisableControls;
  while not TbParcpagtos.Eof do TbParcpagtos.Delete;
  TbParcpagtos.EnableControls;
  TbParcpagtos.Close;
  if GBParcelamento.Visible then
  begin
    TipoValor := GetTipoValor;
    Pagto_Doc := Geral.DMV(EdDoc.Text);
    DiasP := Geral.IMV(EdDias.Text);
    Parce := Geral.IMV(EdParcelas.Text);
    Mora  := Geral.DMV(EdMoraDia.Text);
    Multa := Geral.DMV(EdMulta.Text);
    Total := FValorAParcelar;
    if Total > 0 then Fator := Geral.DMV(EdICMS_V.Text) / Total else Fator := 0;
    if Total <= 0 then Valor := 0
    else
    begin
      Valor := (Total / Parce)*100;
      Valor := (Trunc(Valor))/100;
    end;
    if CkArredondar.Checked then Valor := int(Valor);
    Valor1 := Valor;
    ValorX := Valor;
    if RGArredondar.ItemIndex = 0 then
    begin
      EdParcela1.Text := Geral.TFT(FloatToStr(Valor), 2, siPositivo);
      ValorX := Total - ((Parce - 1) * Valor);
      EdParcelaX.Text := Geral.TFT(FloatToStr(ValorX), 2, siPositivo);
    end else begin
      EdParcelaX.Text := Geral.TFT(FloatToStr(Valor), 2, siPositivo);
      Valor1 := Total - ((Parce - 1) * Valor);
      EdParcela1.Text := Geral.TFT(FloatToStr(Valor1), 2, siPositivo);
    end;
    //Duplicata := MLAGeral.IncrementaDuplicata(EdDuplicata.Text, -1);
    Duplicata := EdDuplicata.Text;
    Casas := Length(IntTostr(Parce));
    fmt := '';
    for i := 1 to Casas do fmt := fmt + '0';
    DuplSep := Trim(EdDuplSep.Text);
    if DuplSep = '' then DuplSep := ' ';
    for i := 1 to Parce do
    begin
      if i= 1 then ValorA := Valor1
      else if i = Parce then ValorA := ValorX
      else ValorA := Valor;
      if TipoValor = tvCred then ValorC := ValorA else ValorC := 0;
      if TipoValor = tvDeb  then ValorD := ValorA else ValorD := 0;
      //
      if RGPeriodo.ItemIndex = 0 then
        Data := MLAGeral.IncrementaMeses(TPVencimento.Date, i-1, True)
      else
        Data := TPVencimento.Date + (DiasP * (i-1));
      //
      if EdMes.Enabled then
      begin
        if CkIncremTxt.Checked then
        begin
          case RGSepTxt.ItemIndex of
            0: TxtSeq := FormatFloat(fmt, i);
            1: TxtSeq := MLAGeral.IntToColTxt(i);
            else TxtSeq := '?';
          end;
          case RGSepTxt.ItemIndex of
            0: Descricao := FormatFloat(fmt, Parce);
            1: Descricao := MLAGeral.IntToColTxt(Parce);
            else TxtSeq := '?';
          end;
          TxtSeq := TxtSeq + EdSepTxt.Text;
          Descricao := TxtSeq + Descricao + ' ' +EdDescricao.Text;
        end else
          Descricao := EdDescricao.Text
      end else
        Descricao := IntToStr(i) + '�/'+ IntToStr(Parce) + ' ' +EdDescricao.Text;
      //Duplicata := MLAGeral.IncrementaDuplicata(Duplicata, 1);
      //
      if CkIncremDU.Checked then
      begin
        case RGDuplSeq.ItemIndex of
          0: DuplSeq := FormatFloat(fmt, i);
          1: DuplSeq := MLAGeral.IntToColTxt(i);
          else DuplSeq := '?';
        end;
        DuplSeq := DuplSep + DuplSeq;
      end else DuplSeq := '';
      Dmod.QrUpdL.SQL.Clear;
      Dmod.QrUpdL.SQL.Add('INSERT INTO parcpagtos SET Parcela=:P0, ');
      Dmod.QrUpdL.SQL.Add('Data=:P1, Credito=:P2, Debito=:P3, Doc=:P4, ');
      Dmod.QrUpdL.SQL.Add('Mora=:P5, Multa=:P6, ICMS_V=:P7, Duplicata=:P8, ');
      Dmod.QrUpdL.SQL.Add('Descricao=:P9 ');
      Dmod.QrUpdL.Params[0].AsInteger := i;
      Dmod.QrUpdL.Params[1].AsString := FormatDateTime(VAR_FORMATDATE, Data);
      Dmod.QrUpdL.Params[2].AsFloat := ValorC;
      Dmod.QrUpdL.Params[3].AsFloat := ValorD;
      Dmod.QrUpdL.Params[4].AsFloat := Pagto_Doc;
      Dmod.QrUpdL.Params[5].AsFloat := Mora;
      Dmod.QrUpdL.Params[6].AsFloat := Multa;
      Dmod.QrUpdL.Params[7].AsFloat := Fator * (ValorC+ValorD);
      Dmod.QrUpdL.Params[8].AsString := Duplicata + DuplSeq;
      Dmod.QrUpdL.Params[9].AsString := Descricao;
      Dmod.QrUpdL.ExecSQL;
      if not RGIncremCH.Enabled then Pagto_Doc := Pagto_Doc
      else Pagto_Doc := Pagto_Doc + RGIncremCH.ItemIndex;
      //
    end;
    TbParcpagtos.Open;
    TbParcpagtos.EnableControls;
  end;
  QrSoma.Close;
  QrSoma.Open;
  EdSoma.Text := FormatFloat('#,###,##0.00', QrSomaVALOR.Value);
end;

procedure TFmLanctos2.CarteirasReopen;
begin
  QrCarteiras.Close;
  QrCarteiras.Params[0].AsInteger := FmPrincipal.FEntInt;
  QrCarteiras.Open;
end;

procedure TFmLanctos2.VerificaEdits;
begin
  if (QrContasMensal.Value = 'V') or (Dmod.QrControleMensalSempre.Value=1) then
  begin
    EdMes.Enabled := True;
    LaMes.Enabled := True;
  end else begin
    EdMes.Enabled := False;
    LaMes.Enabled := False;
  end;
  //
  if (VAR_BAIXADO <> -2) and not VAR_FATURANDO then Exit;
  if QrContasCredito.Value = 'V' then
  begin
    EdCred.Enabled := True;
    LaCred.Enabled := True;
    EdCliente.Enabled := True;
    LaCliente.Enabled := True;
    CBCliente.Enabled := True;
  end else begin
    EdCred.Enabled := False;
    LaCred.Enabled := False;
    EdCliente.Enabled := False;
    LaCliente.Enabled := False;
    CBCliente.Enabled := False;
  end;

  if QrContasDebito.Value = 'V' then
  begin
    EdDeb.Enabled := True;
    LaDeb.Enabled := True;
    EdFornecedor.Enabled := True;
    LaFornecedor.Enabled := True;
    CBFornecedor.Enabled := True;
  end else begin
    EdDeb.Enabled := False;
    LaDeb.Enabled := False;
    EdFornecedor.Enabled := False;
    LaFornecedor.Enabled := False;
    CBFornecedor.Enabled := False;
  end;

end;

procedure TFmLanctos2.BtDesisteClick(Sender: TObject);
begin
  Close;
  VAR_VALOREMITIRIMP := 0;
end;

procedure TFmLanctos2.BtConfirmaClick(Sender: TObject);
var
  Cartao, Carteira, Genero, TipoAnt, CartAnt, NF, Sit, Linha, Depto, Fornecedor,
  Cliente, CliInt, ForneceI, Vendedor, Account, Funci, Parcelas,
  DifMes, Me1, Me2: Integer;
  Controle: Int64;
  Credito, Debito, Doc, MoraDia, Multa, ICMS_V, Difer, MultaVal, MoraVal: Double;
  Mes, Vencimento, Compensado: String;
  Inseriu: Boolean;
  MesX, AnoX: Word;
  DataX: TDateTime;
begin
  Me1 := 0;
  //
  if CkParcelamento.Checked and (LaTipo.Caption = CO_ALTERACAO) then
  begin
    Application.MessageBox('N�o pode haver parcelamento em altera��o!',
    'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  if EdFornecedor.Enabled = False then Fornecedor := 0 else
    if CBFornecedor.KeyValue <> NULL then
      Fornecedor := CBFornecedor.KeyValue else Fornecedor := 0;
  if EdCliente.Enabled = False then Cliente := 0 else
    if CBCliente.KeyValue <> NULL then
      Cliente := CBCliente.KeyValue else Cliente := 0;
  if EdVendedor.Enabled = False then Vendedor := 0 else
    if CBVendedor.KeyValue <> NULL then
      Vendedor := CBVendedor.KeyValue else Vendedor := 0;
  if EdAccount.Enabled = False then Account := 0 else
    if CBAccount.KeyValue <> NULL then
      Account := CBAccount.KeyValue else Account := 0;
  Depto := Geral.IMV(EdDepto.Text);
  ForneceI := Geral.IMV(EdCliCli.Text);
  CliInt := Geral.IMV(EdCliInt.Text);
  if (CliInt = 0) or (CliInt <> FmPrincipal.FEntInt) then
  begin
    Application.MessageBox('ERRO! Cliente interno inv�lido!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    LaCliInt.Enabled := True;
    EdCliInt.Enabled := True;
    CBCliInt.Enabled := True;
    if EdCliInt.Enabled then EdCliInt.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (VAR_AVISOSCXAEDIT in ([1,3,5,7])) and (Vendedor < 1) and (CBVendedor.Visible) then
  begin
    if Application.MessageBox('Deseja continuar mesmo sem definir o vendedor?',
    'Aviso de escolha de vendedor', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      if CBVendedor.Enabled then CBVendedor.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  if (VAR_AVISOSCXAEDIT in ([2,3,6,7])) and (Account < 1) and (CBAccount.Visible) then
  begin
    if Application.MessageBox('Deseja continuar mesmo sem definir o Representante?',
    'Aviso de escolha de Representante', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      if CBAccount.Enabled then CBAccount.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  VAR_CANCELA := False;
  if not VerificaVencimento then
  begin
   Screen.Cursor := crDefault;
   Exit;
  end;
  Cartao := 0;
  Compensado := CO_VAZIO;
  if VAR_FATURANDO then
  begin
    Cartao := FmPrincipal.CartaoDeFatura;
    Compensado := FmPrincipal.CompensacaoDeFatura;
  end else if TPCompensado.Date > 1 then
    Compensado := Geral.FDT(TPCompensado.Date, 1);
  if (EdNF.Enabled = False) then NF := 0
  else NF := Geral.IMV(EdNF.Text);
  Credito := Geral.DMV(EdCred.Text);
  if VAR_CANCELA then
  begin
    Application.MessageBox('Inclus�o cancelada pelo usu�rio!',
    'Cancelaqmento de Inclus�o', MB_OK + MB_ICONEXCLAMATION);
    if EdNF.Enabled then EdNF.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  Debito  := Geral.DMV(EdDeb.Text);
  MoraDia := Geral.DMV(EdMoraDia.Text);
  Multa   := Geral.DMV(EdMulta.Text);
  ICMS_V  := Geral.DMV(EdICMS_V.Text);
  Difer   := Credito-Debito;
  if ((Difer<0) and (ICMS_V>0)) or ((Difer>0) and (ICMS_V<0)) then
  begin
    Application.MessageBox('ICMS incorreto! Para d�bitos o ICMS deve ser negativo!',
    'Cancelaqmento de Inclus�o', MB_OK + MB_ICONEXCLAMATION);
    if EdICMS_P.Enabled then EdICMS_P.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (EdCred.Enabled = False) and (Credito <> 0) and (VAR_BAIXADO = -2) then
  begin
    EdCred.Text := '0';
    Credito := 0;
  end;
  if (EdDeb.Enabled = False) and (Debito <> 0) and (VAR_BAIXADO = -2) then
  begin
    EdDeb.Text := '0';
    Debito := 0;
  end;
  if CBCarteira.KeyValue <> NULL then Carteira := CBCarteira.KeyValue else Carteira := 0;
  if Carteira = 0 then
  begin
    Application.MessageBox('ERRO. Defina uma carteira!', 'Erro', MB_OK+MB_ICONERROR);
    if EdCarteira.Enabled then EdCarteira.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (CBFunci.Visible=True) and (CBFunci.KeyValue=NULL)then
  begin
    Application.MessageBox('ERRO. Defina um funcion�rio!', 'Erro', MB_OK+MB_ICONERROR);
    if EdFunci.Enabled then EdFunci.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end else Funci := Geral.IMV(EdFunci.Text);
  if Funci = 0 then Funci := VAR_USUARIO;
  Genero := Geral.IMV(EdConta.Text);
  if Genero = 0 then
  begin
    Application.MessageBox('ERRO. Defina uma conta!', 'Erro', MB_OK+MB_ICONERROR);
    if EdcOnta.Enabled then EdConta.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (Credito > 0) and (QrContasCredito.Value = 'F') then
  begin
    Application.MessageBox('ERRO. Valor n�o pode ser cr�dito', 'Erro', MB_OK+MB_ICONERROR);
    EdCred.Text;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (Debito > 0) and (QrContasDebito.Value = 'F') then
  begin
    Application.MessageBox('ERRO. Valor n�o pode ser d�bito', 'Erro', MB_OK+MB_ICONERROR);
    EdDeb.Text;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (EdCred.Enabled = False) and (EdDeb.Enabled = False) and
  (VAR_BAIXADO = -2) then
  begin
    Application.MessageBox('ERRO. Opera��o imposs�vel. Conta sem permiss�o de d�bito ou cr�dito.', 'Erro', MB_OK+MB_ICONERROR);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (Credito = 0) and (Debito = 0) then
  begin
    Application.MessageBox('ERRO. Defina um d�bito ou um cr�dito.', 'Erro', MB_OK+MB_ICONERROR);
    if EdDeb.Enabled = True then EdDeb.SetFocus
    else if EdCred.Enabled = True then EdCred.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if EdMes.Text = CO_VAZIO then Mes := CO_VAZIO else
  if (EdMes.Enabled = False) then Mes := CO_VAZIO else
  begin
    Mes := (*EdMes.Text[4]+EdMes.Text[5]+*)EdMes.Text[6]+EdMes.Text[7]+(*'/'+*)
    EdMes.Text[1]+EdMes.Text[2](*+'/01'*);
    Me1 := Geral.Periodo2000(TPVencimento.Date);
    AnoX := Geral.IMV(Copy(Mes, 1, 2)) + 2000;
    MesX := Geral.IMV(Copy(Mes, 3, 2));
    DataX := EncodeDate(AnoX, MesX, 1);
    Me2 := Geral.Periodo2000(DataX);
    DifMes := Me2 - Me1;
    if Mes = CO_VAZIO then
    begin
      Application.MessageBox('ERRO. Defina o m�s do vencimento.', 'Erro', MB_OK+MB_ICONERROR);
      if EdMes.Enabled then EdMes.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  if (Mes = CO_VAZIO) and (EdMes.Enabled = True) then
  begin
    Application.MessageBox('ERRO. Defina o m�s do vencimento.', 'Erro', MB_OK+MB_ICONERROR);
    if EdMes.Enabled then EdMes.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (EdDoc.Enabled = False) and (VAR_BAIXADO = -2) then Doc := 0
  else begin
    Doc := Geral.DMV(EdDoc.Text);
    if (QrCarteirasExigeNumCheque.Value = 1) and (Doc=0) then
    begin
      Application.MessageBox('Esta carteira exige o n�mero do cheque (documento)!',
      'Aviso', MB_OK+MB_ICONWARNING);
      EdDoc.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  // n�o mexer no vencimento aqui!
  {if (TPVencimento.Enabled = False) then Vencimento :=
    FormatDateTime(VAR_FORMATDATE, TPData.Date)
  else}
  Vencimento := FormatDateTime(VAR_FORMATDATE, TPVencimento.Date);
  if VAR_IMPORTANDO then Linha := VAR_IMPLINHA else Linha := 0;
  if VAR_FATURANDO then Sit := 3
  else if VAR_IMPORTANDO then Sit := -1
    else if VAR_BAIXADO <> -2 then Sit := VAR_BAIXADO
      else if QrCarteirasTipo.Value = 2 then Sit := 0
        else Sit := 3;
  TipoAnt := StrToInt(EdTipoAnt.Caption);
  CartAnt := StrToInt(EdCartAnt.Caption);
  MultaVal := Geral.DMV(EdMultaVal.Text);
  MoraVal := Geral.DMV(EdMoraVal.Text);
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'Lanctos', 'Lanctos', 'Controle');
    VAR_DATAINSERIR := TPdata.Date;

  end else begin
    Controle := Geral.IMV(EdCodigo.Text);
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('DELETE FROM lanctos WHERE Controle=:P0');
    Dmod.QrUpdM.Params[0].AsFloat := Controle;
    Dmod.QrUpdM.ExecSQL;
    Dmod.RecalcSaldoCarteira(TipoAnt, CartAnt, 0);
    //dbiSaveChanges(Dmod.QrUpdU.Handle);
    VAR_DATAINSERIR := TPData.Date;
  end;

  if UFinanceiro.NaoDuplicarLancto(LaTipo.Caption, Geral.IMV(EdDoc.Text),
  CkPesqCH.Checked, EdSerieCH.Text, Geral.IMV(EdNF.Text),
  CkPesqNF.Checked) then Exit;

  VAR_LANCTO2 := Controle;







  {Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERTINTO Lanctos SET Documento=:P0, Data=:P1, ');
  Dmod.QrUpdU.SQL.Add('Tipo=:P2, Carteira=:P3, Credito=:P4, Debito=:P5, ');
  Dmod.QrUpdU.SQL.Add('Genero=:P6, NotaFiscal=:P7, Vencimento=:P8, Mez=:P9, ');
  Dmod.QrUpdU.SQL.Add('Descricao=:P10, Sit=:P11, Controle=:P12, Cartao=:P13, ');
  Dmod.QrUpdU.SQL.Add('Compensado=:P14, Linha=:P15, Fornecedor=:P16, ');
  Dmod.QrUpdU.SQL.Add('Cliente=:P17, MoraDia=:P18, Multa=:P19, DataCad=:P20, ');
  Dmod.QrUpdU.SQL.Add('UserCad=:P21, DataDoc=:P22, Vendedor=:P23, ');
  Dmod.QrUpdU.SQL.Add('Account=:P24, FatID=:P25, FatID_Sub=:P26, ');
  Dmod.QrUpdU.SQL.Add('ICMS_P=:P27, ICMS_V=:P28, Duplicata=:P29, ');
  if EdQtde.Text <> '' then
    Dmod.QrUpdU.SQL.Add('Qtde='+MLAGeral.TFT_Dot(EdQtde.Text, 3, siPositivo)+',');
  Dmod.QrUpdU.SQL.Add('CliInt=:P30, Depto=:P31, SerieCH=:P32, ForneceI=:P33, ');
  Dmod.QrUpdU.SQL.Add('MultaVal=:P34, MoraVal=:P35, CNAB_Sit=:P36, ');
  Dmod.QrUpdU.SQL.Add('ID_Pgto=:P37, TipoCH=:P38 ');}
  //



  UFinanceiro.LancamentoDefaultVARS;
  {Dmod.QrUpdU.Params[00].AsFloat   :=} FLAN_Documento  := Trunc(Doc);
  {Dmod.QrUpdU.Params[01].AsString  :=} FLAN_Data       := FormatDateTime(VAR_FORMATDATE, TPData.Date);
  {Dmod.QrUpdU.Params[02].AsInteger :=} FLAN_Tipo       := QrCarteirasTipo.Value;
  {Dmod.QrUpdU.Params[03].AsInteger :=} FLAN_Carteira   := Carteira;
  {Dmod.QrUpdU.Params[04].AsFloat   :=} FLAN_Credito    := Credito;
  {Dmod.QrUpdU.Params[05].AsFloat   :=} FLAN_Debito     := Debito;
  {Dmod.QrUpdU.Params[06].AsInteger :=} FLAN_Genero     := Genero;
  {Dmod.QrUpdU.Params[07].AsInteger :=} FLAN_NotaFiscal := NF;
  {Dmod.QrUpdU.Params[08].AsString  :=} FLAN_Vencimento := Vencimento;
  {Dmod.QrUpdU.Params[09].AsString  :=} FLAN_Mez        := Mes;
  {Dmod.QrUpdU.Params[10].AsString  :=} FLAN_Descricao  := EdDescricao.Text;
  {Dmod.QrUpdU.Params[11].AsInteger :=} FLAN_Sit        := Sit;
  {Dmod.QrUpdU.Params[12].AsFloat   :=} FLAN_Controle   := Controle;
  {Dmod.QrUpdU.Params[13].AsInteger :=} FLAN_Cartao     := Cartao;
  {Dmod.QrUpdU.Params[14].AsString  :=} FLAN_Compensado := Compensado;
  {Dmod.QrUpdU.Params[15].AsInteger :=} FLAN_Linha      := Linha;
  {Dmod.QrUpdU.Params[16].AsInteger :=} FLAN_Fornecedor := Fornecedor;
  {Dmod.QrUpdU.Params[17].AsInteger :=} FLAN_Cliente    := Cliente;
  {Dmod.QrUpdU.Params[18].AsFloat   :=} FLAN_MoraDia    := MoraDia;
  {Dmod.QrUpdU.Params[19].AsFloat   :=} FLAN_Multa      := Multa;
  {Dmod.QrUpdU.Params[20].AsString  :=} FLAN_DataCad    := FormatDateTime(VAR_FORMATDATE, Date);
  {Dmod.QrUpdU.Params[21].AsInteger :=} FLAN_UserCad    := Funci;
  {Dmod.QrUpdU.Params[22].AsString  :=} FLAN_DataDoc    := FormatDateTime(VAR_FORMATDATE, TPDataDoc.Date);
  {Dmod.QrUpdU.Params[23].AsInteger :=} FLAN_Vendedor   := Vendedor;
  {Dmod.QrUpdU.Params[24].AsInteger :=} FLAN_Account    := Account;
  {Dmod.QrUpdU.Params[25].AsInteger :=} FLAN_FatID      := FFatID;
  {Dmod.QrUpdU.Params[26].AsInteger :=} FLAN_FatID_Sub  := FFatID_Sub;
  {Dmod.QrUpdU.Params[27].AsFloat   :=} FLAN_ICMS_P     := Geral.DMV(EdICMS_P.Text);
  {Dmod.QrUpdU.Params[28].AsFloat   :=} FLAN_ICMS_V     := Geral.DMV(EdICMS_V.Text);
  {Dmod.QrUpdU.Params[29].AsString  :=} FLAN_Duplicata  := EdDuplicata.Text;
  {Dmod.QrUpdU.Params[30].AsInteger :=} FLAN_CliInt     := CliInt;
  {Dmod.QrUpdU.Params[31].AsInteger :=} FLAN_Depto      := Depto;
  {Dmod.QrUpdM.Params[32].AsInteger :=} FLAN_DescoPor   := FDescoPor;
  {Dmod.QrUpdU.Params[33].AsInteger :=} FLAN_ForneceI   := ForneceI;
  {Dmod.QrUpdM.Params[34].AsFloat   :=} FLAN_DescoVal   := FDescoVal;
  {Dmod.QrUpdM.Params[35].AsFloat   :=} FLAN_NFVal      := FNFVAl;
  {Dmod.QrUpdM.Params[36].AsInteger :=} FLAN_Unidade    := FUnidade;
  {Dmod.QrUpdM.Params[37].AsFloat   :=} //FLAN_Qtde       := Qtde;  ABAIXO
  {Dmod.QrUpdM.Params[38].AsInteger :=} FLAN_FatNum     := FFatNum;
  {Dmod.QrUpdM.Params[39].AsInteger :=} FLAN_FatParcela := FFatParcela;
  {Dmod.QrUpdM.Params[40].AsString  :=} FLAN_Doc2       := FDoc2;
  {Dmod.QrUpdU.Params[41].AsString  :=} FLAN_SerieCH    := EdSerieCh.Text;
  {Dmod.QrUpdM.Params[42].AsFloat   :=} FLAN_MultaVal   := MultaVal;
  {Dmod.QrUpdM.Params[43].AsFloat   :=} FLAN_MoraVal    := MoraVal;
  {Dmod.QrUpdM.Params[44].AsInteger :=} FLAN_CtrlIni    := FCtrlIni;
  {Dmod.QrUpdM.Params[45].AsInteger :=} FLAN_Nivel      := FNivel;
  {Dmod.QrUpdU.Params[46].AsInteger :=} FLAN_ID_Pgto    := FID_Pgto;
  {Dmod.QrUpdU.Params[47].AsInteger :=} FLAN_CNAB_Sit   := FCNAB_Sit;
  {Dmod.QrUpdU.Params[48].AsInteger :=} FLAN_TipoCH     := CkTipoCH.Value;
  {Dmod.QrUpdU.ExecSQL; }
  if EdQtde.Text <> '' then
    FLAN_Qtde := Geral.DMV(EdQtde.Text);
    //Dmod.QrUpdU.SQL.Add('Qtde='+MLAGeral.TFT_Dot(EdQtde.Text, 3, siPositivo)+',');









  {Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERTINTO Lanctos SET Documento=:P0, Data=:P1, ');
  Dmod.QrUpdU.SQL.Add('Tipo=:P2, Carteira=:P3, Credito=:P4, Debito=:P5, ');
  Dmod.QrUpdU.SQL.Add('Genero=:P6, NotaFiscal=:P7, Vencimento=:P8, Mez=:P9, ');
  Dmod.QrUpdU.SQL.Add('Descricao=:P10, Sit=:P11, Controle=:P12, Cartao=:P13, ');
  Dmod.QrUpdU.SQL.Add('Compensado=:P14, Linha=:P15, Fornecedor=:P16, ');
  Dmod.QrUpdU.SQL.Add('Cliente=:P17, MoraDia=:P18, Multa=:P19,DataCad=:P20, ');
  Dmod.QrUpdU.SQL.Add('UserCad=:P21, DataDoc=:P22, Vendedor=:P23, ');
  Dmod.QrUpdU.SQL.Add('Account=:P24, FatID=:P25, FatID_Sub=:P26, ');
  Dmod.QrUpdU.SQL.Add('ICMS_P=:P27, ICMS_V=:P28, Duplicata=:P29, ');
  if EdQtde.Text <> '' then
    Dmod.QrUpdU.SQL.Add('Qtde='+MLAGeral.TFT_Dot(EdQtde.Text, 3, siPositivo)+',');
  Dmod.QrUpdU.SQL.Add('CliInt=:P30, Depto=:P31, SerieCH=:P32, ForneceI=:P33, ');
  Dmod.QrUpdU.SQL.Add('MultaVal=:P34, MoraVal=:P35, CNAB_Sit=:P36, ');
  Dmod.QrUpdU.SQL.Add('ID_Pgto=:P37, TipoCH=:P38 ');
  //
  Dmod.QrUpdU.Params[00].AsFloat   := Doc;
  Dmod.QrUpdU.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPData.Date);
  Dmod.QrUpdU.Params[02].AsInteger := QrCarteirasTipo.Value;
  Dmod.QrUpdU.Params[03].AsInteger := Carteira;
  Dmod.QrUpdU.Params[04].AsFloat   := Credito;
  Dmod.QrUpdU.Params[05].AsFloat   := Debito;
  Dmod.QrUpdU.Params[06].AsInteger := Genero;
  Dmod.QrUpdU.Params[07].AsInteger := NF;
  Dmod.QrUpdU.Params[08].AsString  := Vencimento;
  Dmod.QrUpdU.Params[09].AsString  := Mes;
  Dmod.QrUpdU.Params[10].AsString  := EdDescricao.Text;
  Dmod.QrUpdU.Params[11].AsInteger := Sit;
  Dmod.QrUpdU.Params[12].AsFloat   := Controle;
  Dmod.QrUpdU.Params[13].AsInteger := Cartao;
  Dmod.QrUpdU.Params[14].AsString  := Compensado;
  Dmod.QrUpdU.Params[15].AsInteger := Linha;
  Dmod.QrUpdU.Params[16].AsInteger := Fornecedor;
  Dmod.QrUpdU.Params[17].AsInteger := Cliente;
  Dmod.QrUpdU.Params[18].AsFloat   := MoraDia;
  Dmod.QrUpdU.Params[19].AsFloat   := Multa;
  Dmod.QrUpdU.Params[20].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[21].AsInteger := Funci;
  Dmod.QrUpdU.Params[22].AsString  := FormatDateTime(VAR_FORMATDATE, TPDataDoc.Date);
  Dmod.QrUpdU.Params[23].AsInteger := Vendedor;
  Dmod.QrUpdU.Params[24].AsInteger := Account;
  Dmod.QrUpdU.Params[25].AsInteger := FatID;
  Dmod.QrUpdU.Params[26].AsInteger := FatID_Sub;
  Dmod.QrUpdU.Params[27].AsFloat   := Geral.DMV(EdICMS_P.Text);
  Dmod.QrUpdU.Params[28].AsFloat   := Geral.DMV(EdICMS_V.Text);
  Dmod.QrUpdU.Params[29].AsString  := EdDuplicata.Text;
  Dmod.QrUpdU.Params[30].AsInteger := CliInt;
  Dmod.QrUpdU.Params[31].AsInteger := Depto;
  Dmod.QrUpdU.Params[32].AsString  := EdSerieCh.Text;
  Dmod.QrUpdU.Params[33].AsInteger := ForneceI;
  Dmod.QrUpdU.Params[34].AsFloat   := MultaVal;
  Dmod.QrUpdU.Params[35].AsFloat   := MoraVal;
  Dmod.QrUpdU.Params[36].AsInteger := FCNAB_Sit;
  Dmod.QrUpdU.Params[37].AsInteger := FID_Pgto;
  Dmod.QrUpdU.Params[38].AsInteger := CkTipoCH.Value;
  Dmod.QrUpdU.ExecSQL;}






    Parcelas := 0;
    if CkParcelamento.Checked then
    begin
      TbParcpagtos.First;
      while not TbParcpagtos.Eof do
      begin
        if (Me1 > 0) then
        begin
          Me2 := Geral.Periodo2000(TbParcpagtosData.Value) + DifMes;
          FLAN_Mez := IntToStr(MLAGeral.PeriodoToAnoMes(Me2));
          FLAN_Descricao  := TbParcPagtosDescricao.Value;
          if (CkIncremTxt.Checked = False) then
            FLAN_Descricao  := FLAN_Descricao +
              ' - ' + MLAGeral.MesEAnoDoPeriodo(Me2)
        end else
        begin
          FLAN_Mez := '';
          FLAN_Descricao  := TbParcPagtosDescricao.Value;
        end;
        FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
          'Controle', 'Lanctos', 'Lanctos', 'Controle');
        FLAN_Duplicata  := TbParcPagtosDuplicata.Value;
        FLAN_Credito    := TbParcpagtosCredito.Value;
        FLAN_Debito     := TbParcpagtosDebito.Value;
        FLAN_MoraDia    := FmCondGer.QrCondPercJuros.Value;
        FLAN_Multa      := FmCondGer.QrCondPercMulta.Value;
        FLAN_ICMS_V     := TbParcpagtosICMS_V.Value;
        FLAN_Vencimento := Geral.FDT(TbParcpagtosData.Value, 1);
        FLAN_Sit        := Sit;
        //
        if UFinanceiro.InsereLancamento then Inc(Parcelas, 1);
        TbParcPagtos.Next;
      end;
      Inseriu := TbParcPagtos.RecordCount = Parcelas;
      if not Inseriu then
        Application.MessageBox(PChar('ATEN��O! Foram inseridas ' +
        IntToStr(Parcelas) + ' de ' + IntToStr(TbParcPagtos.RecordCount) + '!'),
        'Aviso de diverg�ncias', MB_OK+MB_ICONWARNING);
    end else Inseriu := UFinanceiro.InsereLancamento;














  if Inseriu then
  begin
    if LaTipo.Caption = CO_INCLUSAO then
      Dmod.RecalcSaldoCarteira(QrCarteirasTipo.Value, CBCarteira.KeyValue, 1)
    else// begin if CBNovo.Checked = False then
      begin
        Dmod.RecalcSaldoCarteira(QrCarteirasTipo.Value, CBCarteira.KeyValue, 0);
        Dmod.RecalcSaldoCarteira(TipoAnt, CartAnt, 1);
    end;
    //
    VAR_VALOREMITIRIMP := VAR_VALOREMITIRIMP - Credito - Debito;
    FmPrincipal.ReabreCarteirasELanctos(FmPrincipal.QrcarteirasTipo.Value,
    FmPrincipal.QrCarteirasCodigo.Value, Controle, 0, FmCondGer.TPDataIni,
      FmCondGer.TPDataFim, FmCondGer.CBUH.KeyValue); // Sindic
      //FmCondGer.TPDataFim, FmCondGer.CBUH.KeyValue); // Synker
    if CkCopiaCH.Checked then
      FmCondGer.BtCopiaCHClick(Self);
    if CkContinuar.Checked then
    begin
      Application.MessageBox('Inclus�o concluida.', 'Aviso', MB_OK+MB_ICONINFORMATION);
      if Pagecontrol1.ActivePageIndex = 0 then
      if TPData.Enabled then TPData.SetFocus else EdConta.SetFocus;
    end else Close;
  end;  
  Screen.Cursor := crDefault;
end;

procedure TFmLanctos2.EdContaChange(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 0, siPositivo);
  VerificaEdits;
end;

procedure TFmLanctos2.EdContaExit(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 1, siPositivo);
end;

procedure TFmLanctos2.CBContaClick(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 2, siPositivo);
end;

procedure TFmLanctos2.CBContaDropDown(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 3, siPositivo);
end;

procedure TFmLanctos2.FormCreate(Sender: TObject);
var
  CliInt: Integer;
begin
  Width := FLargMenor;
  FCriandoForm := True;
  QrContas.Open;
  QrAptos.Close;
  QrAptos.Params[0].AsInteger := FmCondGer.QrCondCodigo.Value;
  QrAptos.Open;
  if VAR_CXEDIT_ICM then
  begin
    LaICMS_P.Visible := True;
    LaICMS_V.Visible := True;
    EdICMS_P.Visible := True;
    EdICMS_V.Visible := True;
  end;
  //if Trim(VAR_CLIENTEC) <> '' then
  //begin
    QrClientes.SQL.Clear;
    QrClientes.SQL.Add('SELECT Codigo, Account, ');
    QrClientes.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrClientes.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrClientes.SQL.Add('FROM entidades');
    QrClientes.SQL.Add('WHERE Cliente1="V"');
    QrClientes.SQL.Add('OR    Cliente2="V"');
    QrClientes.SQL.Add('OR    Cliente3="V"');
    QrClientes.SQL.Add('OR    Cliente4="V"');
    QrClientes.SQL.Add('OR    Terceiro="V"');
    QrClientes.SQL.Add('ORDER BY NOMEENTIDADE');
    QrClientes.Open;
    //////
    LaCliente.Visible := True;
    EdCliente.Visible := True;
    CBCliente.Visible := True;
    //////
    EdCliInt.Enabled := False;
    CBCliInt.Enabled := False;
    EdCliInt.Text := IntToStr(FmPrincipal.FEntInt);
    CBCliInt.KeyValue := FmPrincipal.FEntInt;
    CliInt := Geral.IMV(EdCliInt.Text);
    if (CliInt = 0) or (CliInt <> FmPrincipal.FEntInt) then
    Application.MessageBox('CUIDADO! Cliente interno n�o definido!', 'Aviso',
      MB_OK+MB_ICONWARNING);
  //end;
  if Trim(VAR_CLIENTEI) <> '' then
  begin
    QrCliInt.SQL.Clear;
    QrCliInt.SQL.Add('SELECT Codigo, Account, ');
    QrCliInt.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrCliInt.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrCliInt.SQL.Add('FROM entidades');
    QrCliInt.SQL.Add('WHERE Cliente'+VAR_CLIENTEI+'="V"');
    QrCliInt.SQL.Add('ORDER BY NOMEENTIDADE');
    QrCliInt.Open;
    //////
    LaCliInt.Visible := True;
    EdCliInt.Visible := True;
    CBCliInt.Visible := True;
  end;
  ReopenFornecedores(0);
  if Trim(VAR_FORNECEF) <> '' then
  begin
    //
    LaFornecedor.Visible := True;
    EdFornecedor.Visible := True;
    CBFornecedor.Visible := True;
  end;
  if Trim(VAR_FORNECEV) <> '' then
  begin
    QrVendedores.SQL.Clear;
    QrVendedores.SQL.Add('SELECT Codigo, ');
    QrVendedores.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrVendedores.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrVendedores.SQL.Add('FROM entidades');
    QrVendedores.SQL.Add('WHERE Fornece'+VAR_FORNECEV+'="V"');
    QrVendedores.SQL.Add('ORDER BY NOMEENTIDADE');
    QrVendedores.Open;
    //////
    QrAccounts.SQL.Clear;
    QrAccounts.SQL.Add('SELECT Codigo, ');
    QrAccounts.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrAccounts.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrAccounts.SQL.Add('FROM entidades');
    QrAccounts.SQL.Add('WHERE Fornece'+VAR_FORNECEV+'="V"');
    QrAccounts.SQL.Add('ORDER BY NOMEENTIDADE');
    QrAccounts.Open;
    //////
    LaVendedor.Visible := True;
    EdVendedor.Visible := True;
    CBVendedor.Visible := True;
    LaAccount.Visible := True;
    EdAccount.Visible := True;
    CBAccount.Visible := True;
  end;
  if VAR_LANCTOCONDICIONAL1 then
  begin
    VAR_DATAINSERIR := Date;
    TPDataDoc.Date := Date;
    //
    EdCarteira.Enabled := False;
    CBCarteira.Enabled := False;
    TPDAta.Enabled := False;
  end;
  if VAR_TIPOVARCLI <> '' then ExigeFuncionario;
  PageControl1.ActivePageIndex := 0;
  TabControl1.TabIndex := 0;
end;

procedure TFmLanctos2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  CarteirasReopen;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    CkContinuar.Visible := True;
    CkParcelamento.Visible := True;
  end;  
  if EdCarteira.Enabled then EdCarteira.SetFocus;
  VerificaEdits;
  FCriandoForm := False;
end;

procedure TFmLanctos2.ConfiguraComponentesCarteira;
begin
  case QrCarteirasTipo.Value of
    0:
    begin
      if QrCarteirasPrazo.Value = 1 then
      begin
        EdDoc.Enabled := True;
        LaDoc.Enabled := True;
        EdNF.Enabled := True;
        LaNF.Enabled := True;
      end else begin
        // N�mero bloqueto no cheque
        EdDoc.Enabled := True;
        LaDoc.Enabled := True;
        //
        EdNF.Enabled := True;
        LaNF.Enabled := True;
      end;
    end;
    1:
    begin
      EdDoc.Enabled := True;
      LaDoc.Enabled := True;
    end;
    2:
    begin
      EdDoc.Enabled := True;
      LaDoc.Enabled := True;
      EdNF.Enabled := True;
      LaNF.Enabled := True;
    end;
  end;
  ConfiguraVencimento;
end;

procedure TFmLanctos2.EdCarteiraChange(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 0, siPositivo);
//  if LaTipo.Caption = CO_ALTERACAO then CBNovo.Visible := True;
end;

procedure TFmLanctos2.EdCarteiraExit(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 1, siPositivo);
end;

procedure TFmLanctos2.CBCarteiraClick(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 2, siPositivo);
end;

procedure TFmLanctos2.CBCarteiraDropDown(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 3, siPositivo);
end;

procedure TFmLanctos2.EdMesExit(Sender: TObject);
begin
  EdMes.Text := MLAGeral.TST(EdMes.Text, True);
  EdDescricao.Text := QrContasNome2.Value+' '+EdMes.Text;
  if EdMes.Text = CO_VAZIO then EdMes.SetFocus;
end;

procedure TFmLanctos2.BtContasClick(Sender: TObject);
var
  Cursor : TCursor;
begin
  if UMyMod.AcessoNegadoAoForm(ivTabPerfis, 'Contas', 0) then Exit;
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Application.CreateForm(TFmContas, FmContas);
  finally
    FmContas.LocCod(QrContasCodigo.Value, QrContasCodigo.Value);
    Screen.Cursor := Cursor;
  end;
  FmContas.ShowModal;
  FmContas.Destroy;
  QrContas.Close;
  QrContas.Open;
  EdConta.Text := IntToStr(VAR_CONTA);
  CBConta.KeyValue := VAR_CONTA;
  //Dmod.DefParams;
end;

procedure TFmLanctos2.EdDebExit(Sender: TObject);
begin
   EdDeb.Text := Geral.TFT(EdDeb.Text, 2, siPositivo);
   CalculaICMS;
end;

procedure TFmLanctos2.EdCredExit(Sender: TObject);
begin
   EdCred.Text := Geral.TFT(EdCred.Text, 2, siPositivo);
   CalculaICMS;
end;

procedure TFmLanctos2.EdNFExit(Sender: TObject);
begin
   EdNF.Text := Geral.TFD(EdNF.Text, 6, siPositivo);
end;

procedure TFmLanctos2.EdDocExit(Sender: TObject);
begin
   EdDoc.Text := Geral.TFD(EdDoc.Text, 6, siPositivo);
end;

procedure TFmLanctos2.EdMesKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Periodo: Integer;
begin
  if key in ([VK_DOWN, VK_UP, VK_F5, VK_F6]) then
  begin
    if (EdMes.Text = '') or (key=VK_F5) then
      Periodo := Geral.Periodo2000(Date)
    else begin
      Periodo := MLAGeral.MensalToPeriodo(EdMes.Text);
      if key=VK_DOWN then Periodo := Periodo -1;
      if key=VK_UP   then Periodo := Periodo +1;
      if key=VK_F4   then Periodo := Periodo -1;
      if key=VK_F5   then Periodo := Periodo   ;
    end;
    EdMes.Text := dmkPF.PeriodoToMensal(Periodo);
  end;
end;

function TFmLanctos2.VerificaVencimento: Boolean;
var
  Carteira: Integer;
  Data: TDateTime;
begin
  Result := True;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    if QrCarteirasFatura.Value = 'V' then
    begin
      if CBCarteira.KeyValue <> NULL then Carteira := CBCarteira.KeyValue
      else Carteira := 0;
      QrFatura.Close;
      QrFatura.Params[0].AsInteger := Carteira;
      QrFatura.Open;
      if (QrFatura.RecordCount > 0) and (CBCarteira.KeyValue <> NULL) then
      begin
        Data := QrFaturaData.Value;
        while (Data - QrCarteirasFechamento.Value) < TPData.Date do
          Data := IncMonth(Data, 1);
        if int(TPVencimento.Date) <> Int(Data) then
        begin
          case Application.MessageBox(PChar(
          'A configura��o do sistema sugere a data de vencimento '+
          FormatDateTime(VAR_FORMATDATE3, Data)+' ao inv�s de '+
          FormatDateTime(VAR_FORMATDATE3, TPVencimento.Date)+
          '. Confirma a altera��o?'), PChar(VAR_APPNAME),
          MB_ICONQUESTION+MB_YESNOCANCEL+MB_DEFBUTTON1+MB_APPLMODAL) of
            ID_YES    : TPVencimento.Date := Data;
            ID_CANCEL : Result := False;
            ID_NO     : ;
          end;
        end;
      end;
      QrFatura.Close;
    end;
  end;
end;

procedure TFmLanctos2.EdFornecedorChange(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 0, siPositivo);
end;

procedure TFmLanctos2.EdFornecedorExit(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 1, siPositivo);
end;

procedure TFmLanctos2.CBFornecedorClick(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 2, siPositivo);
end;

procedure TFmLanctos2.EdClienteChange(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 0, siPositivo);
end;

procedure TFmLanctos2.EdClienteExit(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 1, siPositivo);
end;

procedure TFmLanctos2.CBClienteClick(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 2, siPositivo);
end;

procedure TFmLanctos2.CBClienteDropDown(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 3, siPositivo);
end;

procedure TFmLanctos2.EdDescricaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Mes: String;
begin
  if key in ([VK_F4, VK_F5, VK_F6]) then
  begin
    if EdMes.Enabled then Mes := EdMes.Text else Mes := '';
    if key=VK_F4 then EdDescricao.Text := QrContasNome.Value+' '+Mes;
    if key=VK_F5 then EdDescricao.Text := QrContasNome2.Value+' '+Mes;
    if key=VK_F6 then EdDescricao.Text := QrContasNome3.Value+' '+Mes;
    if EdDescricao.Enabled then EdDescricao.SetFocus;
    EdDescricao.SelStart := Length(EdDescricao.Text);
    EdDescricao.SelLength := 0;
  end else if key = VK_F8 then
  begin
    if CBFornecedor.Enabled then
      EdDescricao.Text := EdDescricao.Text + ' - ' + CBFornecedor.Text
    else if CBCliente.Enabled then
      EdDescricao.Text := EdDescricao.Text + ' - ' + CBCliente.Text;
  end;
end;

procedure TFmLanctos2.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLanctos2.EdMoraDiaExit(Sender: TObject);
begin
  EdMoraDia.Text := Geral.TFT(EdMoraDia.Text, 2, siPositivo);
end;

procedure TFmLanctos2.EdMultaExit(Sender: TObject);
begin
  EdMulta.Text := Geral.TFT(EdMulta.Text, 2, siPositivo);
end;

procedure TFmLanctos2.EdMoraDiaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //Erro
  (*if Key=VK_F4 then
  begin
    if QrContasCredito.Value = 'V' then
      FmPrincipal.CriaCalcPercent(EdCred.Text, '1,0000', cpJurosMes)
    else
      FmPrincipal.CriaCalcPercent(EdDeb.Text, '1,0000', cpJurosMes);
    EdMoraDia.Text := Geral.TFT(FloatToStr(VAR_VALOR), 2, siPositivo);
  end;*)
end;

procedure TFmLanctos2.EdMultaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //Erro
  (*if Key=VK_F4 then
  begin
    if QrContasCredito.Value = 'V' then
      FmPrincipal.CriaCalcPercent(EdCred.Text, '2,0000', cpMulta)
    else
      FmPrincipal.CriaCalcPercent(EdDeb.Text, '2,0000', cpMulta);
    EdMulta.Text := Geral.TFT(FloatToStr(VAR_VALOR), 2, siPositivo);
  end;*)
end;

procedure TFmLanctos2.SpeedButton1Click(Sender: TObject);
begin
  if UMyMod.AcessoNegadoAoForm(ivTabPerfis, 'Carteiras', 0) then Exit;
  VAR_CARTNUM := Geral.IMV(EdCarteira.Text);
  FmPrincipal.CadastroDeCarteiras(QrCarteirasTipo.Value,
    Geral.IMV(EdCarteira.Text));
  QrCarteiras.Close;
  QrCarteiras.Open;
  EdCarteira.Text := IntToStr(VAR_CARTNUM);
  CBCarteira.KeyValue := VAR_CARTNUM;
  Dmod.DefParams;
end;

procedure TFmLanctos2.QrContasAfterScroll(DataSet: TDataSet);
begin
  VerificaEdits;
end;

procedure TFmLanctos2.SetaAccount(var Key: Word; Shift: TShiftState);
begin
  if key=VK_F4 then
  begin
    EdAccount.Text := IntToStr(QrClientesAccount.Value);
    CBAccount.KeyValue := QrClientesAccount.Value;
  end;
end;

procedure TFmLanctos2.EdAccountKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  SetaAccount(Key, Shift);
end;

procedure TFmLanctos2.CBAccountKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  SetaAccount(Key, Shift);
end;

procedure TFmLanctos2.CBFornecedorDropDown(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 3, siPositivo);
end;

procedure TFmLanctos2.EdFunciChange(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 0, siPositivo);
end;

procedure TFmLanctos2.EdFunciExit(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 1, siPositivo);
end;

procedure TFmLanctos2.CBFunciClick(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 2, siPositivo);
end;
                                                                              
procedure TFmLanctos2.CBFunciDropDown(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 3, siPositivo);
end;

procedure TFmLanctos2.ExigeFuncionario;
begin
  QrFunci.Close;
  QrFunci.SQL.Clear;
  QrFunci.SQL.Add('SELECT Codigo,');
  QrFunci.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
  QrFunci.SQL.Add('ELSE Nome END NOMEENTIDADE');
  QrFunci.SQL.Add('FROM entidades');
  QrFunci.SQL.Add('WHERE '+VAR_TIPOVARCLI+'="V"');
  QrFunci.SQL.Add('ORDER BY NomeENTIDADE');
  QrFunci.Open;
  Label10.Visible := True;
  EdFunci.Visible := True;
  CBFunci.Visible := True;
end;

procedure TFmLanctos2.EdICMS_PExit(Sender: TObject);
begin
  CalculaICMS;
  EdICMS_P.Text := Geral.TFT(EdICMS_P.Text, 2, siPositivo);
end;

procedure TFmLanctos2.EdICMS_VExit(Sender: TObject);
begin
  CalculaICMS;
  EdICMS_V.Text := Geral.TFT(EdICMS_V.Text, 2, siPositivo);
end;

procedure TFmLanctos2.EdICMS_PKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if MLAGeral.EhNumeroOuSinalOuSeparador(Key) then FICMS := 0;
end;

procedure TFmLanctos2.EdICMS_VKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if MLAGeral.EhNumeroOuSinalOuSeparador(Key) then FICMS := 1;
end;

procedure TFmLanctos2.CalculaICMS;
var
  C, D, B, V, P: Double;
begin
  V := Geral.DMV(EdICMS_V.Text);
  P := Geral.DMV(EdICMS_P.Text);
  C := Geral.DMV(EdCred.Text);
  D := Geral.DMV(EdDeb.Text);
  //if C > D then B := C - D else B := D - C;
  B := C-D;
  if FICMS = 0 then V := B * P / 100
  else if B <> 0 then P := V / B * 100 else P := 0;
  EdICMS_V.Text := Geral.FFT(V, 2, siPositivo);
  EdICMS_P.Text := Geral.FFT(P, 2, siPositivo);
end;

procedure TFmLanctos2.EdNFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  NF: Integer;
begin
  NF := Geral.IMV(EdNF.Text);
  if key=VK_DOWN then if NF > 0 then EdNF.Text := IntToStr(NF-1);
  if key=VK_UP                  then EdNF.Text := IntToStr(NF+1);
end;

procedure TFmLanctos2.EdDocKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Doc: Integer;
begin
  Doc := Geral.IMV(EdDoc.Text);
  if key=VK_DOWN then if Doc > 0 then EdDoc.Text := IntToStr(Doc-1);
  if key=VK_UP                   then EdDoc.Text := IntToStr(Doc+1);
end;

procedure TFmLanctos2.EdDuplicataKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key in ([VK_DOWN, VK_UP]) then
  begin
    if (key=VK_DOWN) then
      EdDuplicata.Text := MLAGeral.DuplicataIncrementa(EdDuplicata.Text, -1)
    else
      EdDuplicata.Text := MLAGeral.DuplicataIncrementa(EdDuplicata.Text,  1);
  end;
end;

function TFmLanctos2.ReopenLanctos: Boolean;
var
  Ini, Fim: String;
begin
  Ini := FormatDateTime(VAR_FORMATDATE, 1);
  Fim := FormatDateTime(VAR_FORMATDATE, Date);
  QrLanctos.Close;
  QrLanctos.SQL.Clear;
  QrLanctos.SQL.Add('SELECT MOD(la.Mez, 100) Mes2,');
  QrLanctos.SQL.Add('((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,');
  QrLanctos.SQL.Add('la.*, ct.Codigo CONTA,');
  QrLanctos.SQL.Add('ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,');
  QrLanctos.SQL.Add('gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,');
  QrLanctos.SQL.Add('CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial');
  QrLanctos.SQL.Add('ELSE em.Nome END NOMEEMPRESA,');
  QrLanctos.SQL.Add('CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial');
  QrLanctos.SQL.Add('ELSE cl.Nome END NOMECLIENTE,');
  QrLanctos.SQL.Add('CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial');
  QrLanctos.SQL.Add('ELSE fo.Nome END NOMEFORNECEDOR,');
  QrLanctos.SQL.Add('ca.Nome NOMECARTEIRA, ca.Saldo SALDOCARTEIRA');
  QrLanctos.SQL.Add('FROM lanctos la');
  QrLanctos.SQL.Add('LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = 0');
  QrLanctos.SQL.Add('LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo');
  QrLanctos.SQL.Add('LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo');
  QrLanctos.SQL.Add('LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto');
  QrLanctos.SQL.Add('LEFT JOIN entidades em ON em.Codigo=ct.Empresa');
  QrLanctos.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
  QrLanctos.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor');
  QrLanctos.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
  //
  QrLanctos.SQL.Add('WHERE la.Data BETWEEN "'+Ini+'" AND "'+Fim+'"');
  //
  case TabControl1.TabIndex of
    0: QrLanctos.SQL.Add('AND la.Carteira='  +IntToStr(Geral.IMV(EdCarteira.Text)));
    1: QrLanctos.SQL.Add('AND la.CliInt='    +IntToStr(Geral.IMV(EdCliInt.Text)));
    2: QrLanctos.SQL.Add('AND la.Cliente='   +IntToStr(Geral.IMV(EdCliente.Text)));
    3: QrLanctos.SQL.Add('AND la.Fornecedor='+IntToStr(Geral.IMV(EdFornecedor.Text)));
    4: QrLanctos.SQL.Add('AND la.Genero='    +IntToStr(Geral.IMV(EdConta.Text)));
  end;
  QrLanctos.SQL.Add('ORDER BY la.Data DESC, la.Controle DESC');
  QrLanctos.Open;
  Result := True;
end;

procedure TFmLanctos2.TabSheet2Resize(Sender: TObject);
begin
  (*DBGrid1.Align := alClient;
  DBGrid1.Align := alNone;
  DBGrid1.Top := 24;
  DBGrid1.Height := DBGrid1.Height - 24;*)
end;

procedure TFmLanctos2.PageControl1Change(Sender: TObject);
begin
  ReopenLanctos;
  if PageControl1.ActivePageIndex in ([2,3]) then
    WindowState := wsMaximized
  else
    WindowState := wsNormal;
end;

procedure TFmLanctos2.TabControl1Change(Sender: TObject);
begin
  ReopenLanctos;
end;

procedure TFmLanctos2.TPDataChange(Sender: TObject);
begin
  ConfiguraVencimento;
end;

procedure TFmLanctos2.ConfiguraVencimento;
begin
  if LaTipo.Caption = CO_INCLUSAO then
    TPVencimento.Date := TPData.Date;
end;

procedure TFmLanctos2.EdQtdeExit(Sender: TObject);
begin
  EdQtde.Text := Geral.TFT_NULL(EdQtde.Text, 3, siPositivo);
end;

procedure TFmLanctos2.QrCarteirasAfterScroll(DataSet: TDataSet);
begin
  ConfiguraComponentesCarteira;
end;

procedure TFmLanctos2.TPVencimentoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F6 then
    TPVencimento.Date := TPData.Date;
end;

procedure TFmLanctos2.EdFornecedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then ReopenFornecedores(0) else
  if Key = VK_F6 then ReopenFornecedores(1);
end;

procedure TFmLanctos2.CBFornecedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then ReopenFornecedores(0) else
  if Key = VK_F6 then ReopenFornecedores(1);
end;

procedure TFmLanctos2.ReopenFornecedores(Tipo: Integer);
begin
  QrFornecedores.Close;
  QrFornecedores.SQL.Clear;
  QrFornecedores.SQL.Add('SELECT Codigo, ');
  if Tipo = 0 then
  begin
    QrFornecedores.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrFornecedores.SQL.Add('ELSE Nome END NOMEENTIDADE');
    //
    LaFornecedor.Font.Color := LaForneceRN.Font.Color;
  end else begin
    QrFornecedores.SQL.Add('CASE WHEN Tipo=0 THEN Fantasia');
    QrFornecedores.SQL.Add('ELSE Apelido END NOMEENTIDADE');
    //
    LaFornecedor.Font.Color := LaForneceFA.Font.Color;
  end;
  QrFornecedores.SQL.Add('FROM entidades');
  QrFornecedores.SQL.Add('WHERE Fornece1="V"');
  QrFornecedores.SQL.Add('OR    Fornece2="V"');
  QrFornecedores.SQL.Add('OR    Fornece3="V"');
  QrFornecedores.SQL.Add('OR    Fornece4="V"');
  QrFornecedores.SQL.Add('OR    Fornece5="V"');
  QrFornecedores.SQL.Add('OR    Fornece6="V"');
  QrFornecedores.SQL.Add('OR    Terceiro="V"');
  QrFornecedores.SQL.Add('ORDER BY NOMEENTIDADE');
  QrFornecedores.Open;
  //
end;

procedure TFmLanctos2.EdCliIntChange(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 0, siPositivo);
end;

procedure TFmLanctos2.EdCliIntExit(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 1, siPositivo);
end;

procedure TFmLanctos2.CBCliIntClick(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 2, siPositivo);
end;

procedure TFmLanctos2.CBCliIntDropDown(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 3, siPositivo);
end;

procedure TFmLanctos2.EdDeptoChange(Sender: TObject);
begin
  MLAGeral.SincroI(FmLanctos2, Sender, 0, siPositivo);
  if not FCriandoForm then
  begin
    EdCliCli.Text := InttoStr(QrAptosPropriet.Value);
    CBCliCli.KeyValue := QrAptosPropriet.Value;
  end;  
end;

procedure TFmLanctos2.QrCliIntAfterScroll(DataSet: TDataSet);
begin
  QrCliCli.SQL.Clear;
  QrCliCli.SQL.Add('SELECT Codigo, Account,');
  QrCliCli.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
  QrCliCli.SQL.Add('ELSE Nome END NOMEENTIDADE');
  QrCliCli.SQL.Add('FROM entidades');
  QrCliCli.SQL.Add('WHERE Cliente2="V"');
  QrCliCli.SQL.Add('AND Codigo in (');
  QrCliCli.SQL.Add('  SELECT ci2.Propriet');
  QrCliCli.SQL.Add('  FROM condimov ci2');
  QrCliCli.SQL.Add('  LEFT JOIN cond co2 ON co2.Codigo=ci2.Codigo');
  QrCliCli.SQL.Add('  WHERE co2.Cliente=:P0 ');
  QrCliCli.SQL.Add(') ORDER BY NOMEENTIDADE');
  QrCliCli.SQL.Add('');
  QrCliCli.Params[0].AsInteger := FmPrincipal.FEntInt;
  QrCliCli.Open;
end;

procedure TFmLanctos2.EdMultaValExit(Sender: TObject);
begin
  EdMultaVal.Text := Geral.TFT(EdMultaVal.Text, 2, siPositivo);
end;

procedure TFmLanctos2.EdMoraValExit(Sender: TObject);
begin
  EdMoraVal.Text := Geral.TFT(EdMoraVal.Text, 2, siPositivo);
end;

procedure TFmLanctos2.SpeedButton2Click(Sender: TObject);
var
  Cursor : TCursor;
begin
  if UMyMod.AcessoNegadoAoForm(ivTabPerfis, 'Entidades', 0) then Exit;
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Application.CreateForm(TFmEntidades, FmEntidades);
  finally
    FmEntidades.LocCod(QrFornecedoresCodigo.Value, QrFornecedoresCodigo.Value);
    Screen.Cursor := Cursor;
  end;
  FmEntidades.ShowModal;
  FmEntidades.Destroy;
  QrFornecedores.Close;
  QrFornecedores.Open;
  EdFornecedor.Text := IntToStr(VAR_ENTIDADE);
  CBFornecedor.KeyValue := VAR_ENTIDADE;
  //Dmod.DefParams;
end;

procedure TFmLanctos2.BitBtn1Click(Sender: TObject);
var
  Valor, Benef, Cidade: String;
begin
  if EdDeb.Enabled then
  begin
    Valor := EdDeb.Text;
    Benef := CBFornecedor.Text;
  end else begin
    Valor := EdCred.Text;
    Benef := CBCliente.Text;
  end;
  //
  Benef := Geral.SemAcento(MLAGeral.Maiusculas(Benef, False));
  Cidade := Geral.SemAcento(MLAGeral.Maiusculas(Dmod.QrDonoCIDADE.Value, False));
  //
  Application.CreateForm(TFmEmiteCheque, FmEmiteCheque);
  FmEmiteCheque.TPData.Date   := TPData.Date;
  FmEmiteCheque.EdValor.Text  := Valor;
  FmEmiteCheque.EdBenef.Text  := Benef;
  FmEmiteCheque.EdCidade.Text := Cidade;
  //
  FmEmiteCheque.ShowModal;
  (*if FmEmiteCheque.ST_CMC7.Caption <> '' then
    EdBanda1.Text := MLAGeral.SoNumeroECMC7(FmEmiteCheque.ST_CMC7.Caption);*)
  FmEmiteCheque.Destroy;
end;

procedure TFmLanctos2.QrDuplCHCalcFields(DataSet: TDataSet);
begin
  QrDuplCHTERCEIRO.Value := QrDuplCHNOMEFNC.Value + QrDuplCHNOMECLI.Value;
  QrDuplCHMES.Value := MLAGeral.MezToFDT(QrDuplCHMez.Value, 0, 104);
  QrDuplCHCHEQUE.Value := Trim(QrDuplCHSerieCH.Value) +
    FormatFloat('000000', QrDuplCHDocumento.Value);
  if QrDuplCHCompensado.Value = 0 then QrDuplCHCOMP_TXT.Value := '' else
    QrDuplCHCOMP_TXT.Value := Geral.FDT(QrDuplCHCompensado.Value, 2);
end;

procedure TFmLanctos2.QrDuplNFCalcFields(DataSet: TDataSet);
begin
  QrDuplNFTERCEIRO.Value := QrDuplNFNOMEFNC.Value + QrDuplNFNOMECLI.Value;
  QrDuplNFMES.Value := MLAGeral.MezToFDT(QrDuplNFMez.Value, 0, 104);
  QrDuplNFCHEQUE.Value := Trim(QrDuplNFSerieCH.Value) +
    FormatFloat('000000', QrDuplNFDocumento.Value);
  if QrDuplNFCompensado.Value = 0 then QrDuplNFCOMP_TXT.Value := '' else
    QrDuplNFCOMP_TXT.Value := Geral.FDT(QrDuplNFCompensado.Value, 2);
end;

procedure TFmLanctos2.EdContaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F7 then
  begin
    MostraPnMaskPesq('Conta (Plano de contas)');
    ShowMessage('Key Down');
  end;
end;

procedure TFmLanctos2.CBContaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F7 then
  begin
    MostraPnMaskPesq('Conta (Plano de contas)');
    ShowMessage('Key Down');
  end;
end;

procedure TFmLanctos2.MostraPnMaskPesq(Link: String);
begin
  PnLink.Caption     := Link;
  Width := FLargMaior;
  PnMaskPesq.Visible := True;
  EdLinkMask.Text    := '';
  EdLinkMask.SetFocus;
end;

procedure TFmLanctos2.SelecionaItemDePesquisa;
begin
  if QrPesq.RecordCount > 0 then
  begin
    Width := FLargMenor;
    PnMaskPesq.Visible := False;
    if PnLink.Caption = 'Conta (Plano de contas)' then
    begin
      EdConta.Text     := IntToStr(QrPesqCodigo.Value);
      CBConta.KeyValue := QrPesqCodigo.Value;
      if CBConta.Visible then CBConta.SetFocus;
    end
  end;
end;

procedure TFmLanctos2.LLBPesqDblClick(Sender: TObject);
begin
  SelecionaItemDePesquisa;
end;

procedure TFmLanctos2.EdLinkMaskChange(Sender: TObject);
begin
  QrPesq.Close;
  if Length(EdLinkMask.Text) >= 4 then
  begin
    if PnLink.Caption = 'Conta (Plano de contas)' then
    begin
      QrPesq.Close;
      QrPesq.SQL.Clear;
      QrPesq.SQL.Add('SELECT Codigo, Nome ');
      QrPesq.SQL.Add('FROM contas ');
      QrPesq.SQL.Add('WHERE Nome LIKE "%'+EdLinkMask.Text+'%"');
      QrPesq.SQL.Add('ORDER BY Nome');
      //QrPesq.SQL.Add('');
      QrPesq.Open;
    end
  end;
end;

procedure TFmLanctos2.SpeedButton3Click(Sender: TObject);
begin
  Width := FLargMenor;
  PnMaskPesq.Visible := False;
end;

procedure TFmLanctos2.LLBPesqKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = 13 then SelecionaItemdePesquisa;
end;

procedure TFmLanctos2.EdLinkMaskKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_DOWN then LLBPesq.SetFocus;
end;

procedure TFmLanctos2.CBDeptoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_DELETE then
  begin
    EdDepto.Text := '';
    CBDepto.KeyValue := NULL;
  end;
end;

procedure TFmLanctos2.CkParcelamentoClick(Sender: TObject);
begin
  if CkParcelamento.Checked then
  begin
    GBParcelamento.Visible := True;
    DBGParcelas.Visible := True;
    CalculaParcelas;
  end else begin
    GBParcelamento.Visible := False;
    DBGParcelas.Visible := True;
  end;
end;

procedure TFmLanctos2.EdParcelasExit(Sender: TObject);
begin
  EdParcelas.Text := MLAGeral.TFT_MinMax(EdParcelas.Text, 2, 1000, 0, siPositivo);
  CalculaParcelas;
end;

procedure TFmLanctos2.RGPeriodoClick(Sender: TObject);
begin
  if RGPeriodo.ItemIndex = 1 then
  begin
    EdDias.Enabled := True;
    EdDias.SetFocus;
  end else begin
    EdDias.Enabled := False;
  end;
  CalculaParcelas;
end;

procedure TFmLanctos2.EdDiasExit(Sender: TObject);
begin
  EdDias.Text := Geral.TFT(EdDias.Text, 0, siPositivo);
  CalculaParcelas;
end;

procedure TFmLanctos2.RGIncremCHClick(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLanctos2.CkArredondarClick(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLanctos2.RGArredondarClick(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLanctos2.DBGParcelasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then if TbParcPagtos.State in ([dsEdit, dsInsert])
  then TbParcpagtos.Post;
end;

function TFmLanctos2.GetTipoValor: TTipoValor;
var
  D,C: Double;
begin
  if EdDeb.Enabled  then D := Geral.DMV(EdDeb.Text)  else D := 0;
  if EdCred.Enabled then C := Geral.DMV(EdCred.Text) else C := 0;
  FValorAParcelar := D + C;
  if (D>0) and (C>0) then
  begin
    Application.MessageBox('Valor n�o pode ser cr�dito e d�bito ao mesmo tempo!',
    'Erro', MB_OK+MB_ICONERROR);
    Result := tvNil;
    Exit;
  end else if D>0 then Result := tvDeb
  else if C>0 then Result := tvCred
  else Result := tvNil;
end;

procedure TFmLanctos2.EdDocChange(Sender: TObject);
begin
  if Geral.DMV(EdDoc.Text) = 0 then
  begin
    RGIncremCH.Enabled := False;
    //GBCheque.Visible := False;
  end else begin
    RGIncremCH.Enabled := True;
    //GBCheque.Visible := True;
  end;
end;

procedure TFmLanctos2.CkIncremDUClick(Sender: TObject);
begin
  RGIncremDupl.Visible := CkIncremDU.Checked;
  CalculaParcelas;
end;

procedure TFmLanctos2.EdDuplSepExit(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLanctos2.RGDuplSeqClick(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLanctos2.BitBtn2Click(Sender: TObject);
var
  ValOrig, ValMult, ValJuro, ValNovo, PerMult, ValDife: Double;
begin
  ValOrig := Geral.DMV(EdCred.Text) - Geral.DMV(EdDeb.Text);
  if ValOrig < 0 then ValOrig := ValOrig * -1;
  ValNovo := Geral.DMV(EdValNovo.Text);
  PerMult := Geral.DMV(EdPerMult.Text);
  //
  ValDife := ValOrig - ValNovo;
  ValMult := Int(ValOrig * PerMult ) / 100;
  if ValMult > ValDife then ValMult := ValDife;
  ValJuro := ValDife - ValMult;
  //
  EdMultaVal.Text := Geral.FFT(ValMult, 2, siNegativo);
  EdMoraVal.Text  := Geral.FFT(ValJuro, 2, siNegativo);
end;

procedure TFmLanctos2.CkIncremTxtClick(Sender: TObject);
begin
  GBIncremTxt.Visible := CkIncremTxt.Checked;
  CalculaParcelas;
end;

end.


