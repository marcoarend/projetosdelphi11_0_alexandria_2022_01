object FmCNAB_Lot: TFmCNAB_Lot
  Left = 256
  Top = 161
  Caption = 'Remessa CNAB'
  ClientHeight = 610
  ClientWidth = 1250
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnTitulos: TPanel
    Left = 0
    Top = 59
    Width = 1250
    Height = 551
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Visible = False
    object Panel2: TPanel
      Left = 1
      Top = 491
      Width = 1248
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object BitBtn1: TBitBtn
        Tag = 14
        Left = 10
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Adiciona'
        Default = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BitBtn1Click
      end
      object Panel4: TPanel
        Left = 1113
        Top = 1
        Width = 134
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        Caption = 'Panel4'
        TabOrder = 1
        object BitBtn2: TBitBtn
          Tag = 13
          Left = 10
          Top = 2
          Width = 111
          Height = 50
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sair'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn2Click
        end
      end
    end
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 1248
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label4: TLabel
        Left = 20
        Top = 10
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit01
      end
      object Label5: TLabel
        Left = 148
        Top = 10
        Width = 32
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data:'
        FocusControl = DBEdit2
      end
      object Label6: TLabel
        Left = 276
        Top = 34
        Width = 287
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Escolha um ou mais t'#237'tulos e clique em adiciona.'
      end
      object DBEdit01: TDBEdit
        Left = 20
        Top = 30
        Width = 123
        Height = 24
        Hint = 'N'#186' do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsCNAB_Lot
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 148
        Top = 30
        Width = 119
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'MyDATAG'
        DataSource = DsCNAB_Lot
        TabOrder = 1
      end
    end
    object DBGrid2: TdmkDBGrid
      Left = 1
      Top = 65
      Width = 1248
      Height = 148
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Columns = <
        item
          Expanded = False
          FieldName = 'Duplicata'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Title.Caption = 'Cliente'
          Width = 189
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Lote'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Emitente'
          Width = 181
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CPF'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bruto'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Desco'
          Title.Caption = 'Desconto'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DCompra'
          Title.Caption = 'D. Compra'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencto'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DDeposito'
          Title.Caption = 'D. Pagto'
          Width = 56
          Visible = True
        end>
      Color = clWindow
      DataSource = DsTitulos
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -15
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Duplicata'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Title.Caption = 'Cliente'
          Width = 189
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Lote'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Emitente'
          Width = 181
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CPF'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bruto'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Desco'
          Title.Caption = 'Desconto'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DCompra'
          Title.Caption = 'D. Compra'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencto'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DDeposito'
          Title.Caption = 'D. Pagto'
          Width = 56
          Visible = True
        end>
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 59
    Width = 1250
    Height = 551
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 491
      Width = 1248
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 10
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel6: TPanel
        Left = 1113
        Top = 1
        Width = 134
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        Caption = 'Panel6'
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 10
          Top = 2
          Width = 111
          Height = 50
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 1248
      Height = 158
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 0
      object Label9: TLabel
        Left = 10
        Top = 5
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
      end
      object Label3: TLabel
        Left = 103
        Top = 5
        Width = 32
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data:'
      end
      object Label7: TLabel
        Left = 433
        Top = 5
        Width = 162
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Configura'#231#227'o de cobran'#231'a:'
      end
      object Label11: TLabel
        Left = 251
        Top = 5
        Width = 33
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Hora:'
      end
      object Label13: TLabel
        Left = 10
        Top = 54
        Width = 81
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Mensagem 1:'
      end
      object Label14: TLabel
        Left = 409
        Top = 54
        Width = 81
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Mensagem 2:'
      end
      object Label15: TLabel
        Left = 418
        Top = 128
        Width = 438
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'As mensagens n'#227'o podem conter caracteres especiais, acentuados, ' +
          'etc. '
      end
      object Label20: TLabel
        Left = 340
        Top = 5
        Width = 91
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Lote Remessa:'
      end
      object Label23: TLabel
        Left = 10
        Top = 103
        Width = 265
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'CNAB 240 Registro 1 posi'#231#245'es de 208 a 240:'
        FocusControl = DBEdit10
      end
      object EdCodigo: TdmkEdit
        Left = 10
        Top = 25
        Width = 90
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object TPDataG: TdmkEditDateTimePicker
        Left = 103
        Top = 25
        Width = 142
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Date = 39067.403862476900000000
        Time = 39067.403862476900000000
        TabOrder = 1
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdCNAB_Cfg: TdmkEditCB
        Left = 433
        Top = 25
        Width = 80
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCNAB_Cfg
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCNAB_Cfg: TdmkDBLookupComboBox
        Left = 517
        Top = 25
        Width = 385
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCNAB_Cfg
        TabOrder = 4
        dmkEditCB = EdCNAB_Cfg
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPHoraG: TDateTimePicker
        Left = 251
        Top = 25
        Width = 85
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Date = 39198.000000000000000000
        Time = 39198.000000000000000000
        Kind = dtkTime
        TabOrder = 2
      end
      object EdMensagem1: TdmkEdit
        Left = 10
        Top = 74
        Width = 394
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        CharCase = ecUpperCase
        MaxLength = 40
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdMensagem2: TdmkEdit
        Left = 409
        Top = 74
        Width = 393
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        CharCase = ecUpperCase
        MaxLength = 40
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 340
        Top = 25
        Width = 88
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        MaxLength = 8
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Ed_2401_208: TdmkEdit
        Left = 10
        Top = 123
        Width = 344
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        CharCase = ecUpperCase
        MaxLength = 40
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 59
    Width = 1250
    Height = 551
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object Splitter1: TSplitter
      Left = 1
      Top = 361
      Width = 1248
      Height = 3
      Cursor = crVSplit
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
    end
    object PainelControle: TPanel
      Left = 1
      Top = 491
      Width = 1248
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object LaRegistro: TLabel
        Left = 213
        Top = 1
        Width = 457
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = '[N]: 0'
        ExplicitWidth = 31
        ExplicitHeight = 16
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 212
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Hint = 'ltimo registro'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Hint = 'Pr'#243'ximo registro'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Hint = 'Registro anterior'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Hint = 'Primeiro registro'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 670
        Top = 1
        Width = 577
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object Label10: TLabel
          Left = 350
          Top = 5
          Width = 39
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Linhas'
        end
        object BtSaida: TBitBtn
          Tag = 13
          Left = 458
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
        object BtGera: TBitBtn
          Tag = 266
          Left = 231
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&CNAB'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtGeraClick
        end
        object BtTitulos: TBitBtn
          Tag = 11
          Left = 118
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&T'#237'tulos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtTitulosClick
        end
        object BtLotes: TBitBtn
          Tag = 265
          Left = 5
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Lote'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtLotesClick
        end
        object Edit1: TEdit
          Left = 350
          Top = 25
          Width = 84
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 4
          Text = '0'
        end
      end
    end
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 1248
      Height = 153
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 809
        Height = 153
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label16: TLabel
          Left = 10
          Top = 54
          Width = 81
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Mensagem 1:'
          FocusControl = DBEdit5
        end
        object Label1: TLabel
          Left = 10
          Top = 5
          Width = 47
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'C'#243'digo:'
          FocusControl = DBEdCodigo
        end
        object Label8: TLabel
          Left = 138
          Top = 5
          Width = 162
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Configura'#231#227'o de cobran'#231'a:'
          FocusControl = DBEdit3
        end
        object Label17: TLabel
          Left = 409
          Top = 54
          Width = 81
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Mensagem 2:'
          FocusControl = DBEdit6
        end
        object Label21: TLabel
          Left = 724
          Top = 5
          Width = 29
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Lote:'
          FocusControl = DBEdit9
        end
        object Label22: TLabel
          Left = 10
          Top = 103
          Width = 265
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'CNAB 240 Registro 1 posi'#231#245'es de 208 a 240:'
          FocusControl = DBEdit10
        end
        object DBEdit5: TDBEdit
          Left = 10
          Top = 74
          Width = 394
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Mensagem1'
          DataSource = DsCNAB_Lot
          TabOrder = 0
        end
        object DBEdCodigo: TDBEdit
          Left = 10
          Top = 25
          Width = 123
          Height = 24
          Hint = 'N'#186' do banco'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsCNAB_Lot
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 138
          Top = 25
          Width = 582
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NOMECONFIG'
          DataSource = DsCNAB_Lot
          TabOrder = 2
        end
        object DBEdit6: TDBEdit
          Left = 409
          Top = 74
          Width = 393
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Mensagem2'
          DataSource = DsCNAB_Lot
          TabOrder = 3
        end
        object DBEdit9: TDBEdit
          Left = 724
          Top = 25
          Width = 78
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'CodUsu'
          DataSource = DsCNAB_Lot
          TabOrder = 4
        end
        object DBEdit10: TDBEdit
          Left = 10
          Top = 123
          Width = 344
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = '_2401_208'
          DataSource = DsCNAB_Lot
          TabOrder = 5
        end
      end
      object Panel8: TPanel
        Left = 809
        Top = 0
        Width = 439
        Height = 153
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 1
        object Panel9: TPanel
          Left = 1
          Top = 1
          Width = 437
          Height = 84
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          TabOrder = 0
          object GroupBox2: TGroupBox
            Left = 1
            Top = 1
            Width = 182
            Height = 82
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            Caption = ' Cria'#231#227'o: '
            TabOrder = 0
            object Label18: TLabel
              Left = 10
              Top = 20
              Width = 32
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Data:'
              FocusControl = DBEdit7
            end
            object Label19: TLabel
              Left = 103
              Top = 20
              Width = 33
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Hora:'
              FocusControl = DBEdit8
            end
            object DBEdit7: TDBEdit
              Left = 10
              Top = 39
              Width = 88
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'MyDATAG'
              DataSource = DsCNAB_Lot
              TabOrder = 0
            end
            object DBEdit8: TDBEdit
              Left = 103
              Top = 39
              Width = 69
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'HoraG'
              DataSource = DsCNAB_Lot
              TabOrder = 1
            end
          end
          object GroupBox1: TGroupBox
            Left = 183
            Top = 1
            Width = 183
            Height = 82
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            Caption = ' '#218'ltimo envio: '
            TabOrder = 1
            object Label2: TLabel
              Left = 10
              Top = 20
              Width = 32
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Data:'
              FocusControl = DBEdit1
            end
            object Label12: TLabel
              Left = 103
              Top = 20
              Width = 33
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Hora:'
              FocusControl = DBEdit4
            end
            object DBEdit1: TDBEdit
              Left = 10
              Top = 39
              Width = 88
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'MyDATAS'
              DataSource = DsCNAB_Lot
              TabOrder = 0
            end
            object DBEdit4: TDBEdit
              Left = 103
              Top = 39
              Width = 69
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'HoraS'
              DataSource = DsCNAB_Lot
              TabOrder = 1
            end
          end
        end
      end
    end
    object Memo1: TMemo
      Left = 1
      Top = 364
      Width = 1248
      Height = 127
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      WordWrap = False
      OnChange = Memo1Change
    end
    object DBGrid1: TdmkDBGrid
      Left = 1
      Top = 154
      Width = 1248
      Height = 143
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Title.Caption = 'Cliente'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Lote'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Emitente'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CPF'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bruto'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Desco'
          Title.Caption = 'Desconto'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DCompra'
          Title.Caption = 'D. Compra'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencto'
          Title.Caption = 'Vencto.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DDeposito'
          Title.Caption = 'Deposito'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Visible = True
        end>
      Color = clWindow
      DataSource = DsCNABLotIts
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -15
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Title.Caption = 'Cliente'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Lote'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Emitente'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CPF'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bruto'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Desco'
          Title.Caption = 'Desconto'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DCompra'
          Title.Caption = 'D. Compra'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencto'
          Title.Caption = 'Vencto.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DDeposito'
          Title.Caption = 'Deposito'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Visible = True
        end>
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1250
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = '                              Remessa CNAB'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TdmkLabel
      Left = 1148
      Top = 1
      Width = 101
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stNil
    end
    object Image1: TImage
      Left = 278
      Top = 1
      Width = 870
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 277
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 4
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsCNAB_Lot: TDataSource
    DataSet = QrCNAB_Lot
    Left = 444
    Top = 8
  end
  object QrCNAB_Lot: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCNAB_LotBeforeOpen
    AfterOpen = QrCNAB_LotAfterOpen
    AfterScroll = QrCNAB_LotAfterScroll
    OnCalcFields = QrCNAB_LotCalcFields
    SQL.Strings = (
      'SELECT con.Nome NOMECONFIG, cob.* '
      'FROM cnab_lot cob'
      'LEFT JOIN cnab_cfg con ON con.Codigo=cob.CNAB_Cfg'
      'WHERE cob.Codigo > 0')
    Left = 416
    Top = 8
    object QrCNAB_LotMyDATAG: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MyDATAG'
      Size = 10
      Calculated = True
    end
    object QrCNAB_LotMyDATAS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MyDATAS'
      Size = 10
      Calculated = True
    end
    object QrCNAB_LotHoraS: TTimeField
      FieldName = 'HoraS'
      Required = True
    end
    object QrCNAB_LotHoraG: TTimeField
      FieldName = 'HoraG'
      Required = True
    end
    object QrCNAB_LotNOMECONFIG: TWideStringField
      FieldName = 'NOMECONFIG'
      Size = 50
    end
    object QrCNAB_LotCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCNAB_LotDataG: TDateField
      FieldName = 'DataG'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCNAB_LotCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
    object QrCNAB_LotMensagem1: TWideStringField
      FieldName = 'Mensagem1'
      Size = 40
    end
    object QrCNAB_LotMensagem2: TWideStringField
      FieldName = 'Mensagem2'
      Size = 40
    end
    object QrCNAB_LotDataS: TDateField
      FieldName = 'DataS'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCNAB_LotSeqArq: TIntegerField
      FieldName = 'SeqArq'
    end
    object QrCNAB_LotCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrCNAB_Lot_2401_208: TWideStringField
      FieldName = '_2401_208'
      Size = 33
    end
  end
  object QrCNAB_LotIts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCNAB_LotItsCalcFields
    SQL.Strings = (
      'SELECT lot.Cliente, lot.Lote, ent.MultaCodi, ent.MultaDias, '
      'ent.MultaValr, ent.MultaPerc, ent.MultaTiVe, ent.Protestar, '
      
        'ent.JuroSacado, ent.Tipo TipoCLI, PUF, EUF, uf0.Nome UFE, uf1.No' +
        'me UFP,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOME' +
        'CLI, '
      'CASE WHEN ent.Tipo=0 THEN ent.CNPJ ELSE ent.CPF END DOCUMCLI, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.CNPJ    ELSE ent.CPF     END CNPJC' +
        'LI, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.ERua    ELSE ent.PRua    END RuaCL' +
        'I, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.ENumero+0.000 ELSE ent.PNumero+0.0' +
        '00 END NumCLI, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.ECompl  ELSE ent.PCompl  END CplCL' +
        'I, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.EBairro ELSE ent.PBairro END BrrCL' +
        'I, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.ECEP    ELSE ent.PCEP    END + 0.0' +
        '00 CEPCLI, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.ECidade ELSE ent.PCidade END CidCL' +
        'I, '
      'ent.Corrido, sac.Numero+0.000 Numero, '
      'sac.CNPJ SACADO_CNPJ, sac.Nome SACADO_NOME, sac.Rua SACADO_RUA,'
      'sac.Numero SACADO_NUMERO, sac.Compl SACADO_COMPL, '
      'sac.Bairro SACADO_BAIRRO, sac.Bairro SACADO_BAIRRO,'
      'sac.Cidade SACADO_CIDADE, sac.UF SACADO_xUF, '
      'sac.CEP SACADO_CEP, IF(LENGTH(sac.CNPJ) >=14, 0, 1) SACADO_TIPO,'
      'sac.*, loi.*'
      'FROM lotesits loi'
      'LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente'
      'LEFT JOIN sacados   sac ON sac.CNPJ=loi.CPF'
      'LEFT JOIN ufs       uf0 ON uf0.Codigo=ent.EUF'
      'LEFT JOIN ufs       uf1 ON uf1.Codigo=ent.PUF'
      'WHERE loi.CNAB_Lot=:P0'
      'ORDER BY Duplicata')
    Left = 476
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNAB_LotItsCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrCNAB_LotItsLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrCNAB_LotItsNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrCNAB_LotItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCNAB_LotItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCNAB_LotItsComp: TIntegerField
      FieldName = 'Comp'
      Required = True
    end
    object QrCNAB_LotItsBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
    end
    object QrCNAB_LotItsAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
    end
    object QrCNAB_LotItsConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrCNAB_LotItsCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
    end
    object QrCNAB_LotItsCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrCNAB_LotItsEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrCNAB_LotItsBruto: TFloatField
      FieldName = 'Bruto'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCNAB_LotItsDesco: TFloatField
      FieldName = 'Desco'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCNAB_LotItsValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCNAB_LotItsEmissao: TDateField
      FieldName = 'Emissao'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCNAB_LotItsDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCNAB_LotItsDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCNAB_LotItsVencto: TDateField
      FieldName = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCNAB_LotItsTxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Required = True
    end
    object QrCNAB_LotItsTxaJuros: TFloatField
      FieldName = 'TxaJuros'
      Required = True
    end
    object QrCNAB_LotItsTxaAdValorem: TFloatField
      FieldName = 'TxaAdValorem'
      Required = True
    end
    object QrCNAB_LotItsVlrCompra: TFloatField
      FieldName = 'VlrCompra'
      Required = True
    end
    object QrCNAB_LotItsVlrAdValorem: TFloatField
      FieldName = 'VlrAdValorem'
      Required = True
    end
    object QrCNAB_LotItsDMais: TIntegerField
      FieldName = 'DMais'
      Required = True
    end
    object QrCNAB_LotItsDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrCNAB_LotItsDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrCNAB_LotItsDevolucao: TIntegerField
      FieldName = 'Devolucao'
      Required = True
    end
    object QrCNAB_LotItsQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrCNAB_LotItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCNAB_LotItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCNAB_LotItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCNAB_LotItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCNAB_LotItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCNAB_LotItsPraca: TIntegerField
      FieldName = 'Praca'
      Required = True
    end
    object QrCNAB_LotItsBcoCobra: TIntegerField
      FieldName = 'BcoCobra'
      Required = True
    end
    object QrCNAB_LotItsAgeCobra: TIntegerField
      FieldName = 'AgeCobra'
      Required = True
    end
    object QrCNAB_LotItsTotalJr: TFloatField
      FieldName = 'TotalJr'
      Required = True
    end
    object QrCNAB_LotItsTotalDs: TFloatField
      FieldName = 'TotalDs'
      Required = True
    end
    object QrCNAB_LotItsTotalPg: TFloatField
      FieldName = 'TotalPg'
      Required = True
    end
    object QrCNAB_LotItsData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrCNAB_LotItsProrrVz: TIntegerField
      FieldName = 'ProrrVz'
      Required = True
    end
    object QrCNAB_LotItsProrrDd: TIntegerField
      FieldName = 'ProrrDd'
      Required = True
    end
    object QrCNAB_LotItsRepassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
    object QrCNAB_LotItsDepositado: TSmallintField
      FieldName = 'Depositado'
      Required = True
    end
    object QrCNAB_LotItsValQuit: TFloatField
      FieldName = 'ValQuit'
      Required = True
    end
    object QrCNAB_LotItsValDeposito: TFloatField
      FieldName = 'ValDeposito'
      Required = True
    end
    object QrCNAB_LotItsTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrCNAB_LotItsAliIts: TIntegerField
      FieldName = 'AliIts'
      Required = True
    end
    object QrCNAB_LotItsAlinPgs: TIntegerField
      FieldName = 'AlinPgs'
      Required = True
    end
    object QrCNAB_LotItsNaoDeposita: TSmallintField
      FieldName = 'NaoDeposita'
      Required = True
    end
    object QrCNAB_LotItsReforcoCxa: TSmallintField
      FieldName = 'ReforcoCxa'
      Required = True
    end
    object QrCNAB_LotItsCartDep: TIntegerField
      FieldName = 'CartDep'
      Required = True
    end
    object QrCNAB_LotItsMultaCodi: TSmallintField
      FieldName = 'MultaCodi'
    end
    object QrCNAB_LotItsMultaDias: TSmallintField
      FieldName = 'MultaDias'
    end
    object QrCNAB_LotItsMultaValr: TFloatField
      FieldName = 'MultaValr'
    end
    object QrCNAB_LotItsMultaPerc: TFloatField
      FieldName = 'MultaPerc'
    end
    object QrCNAB_LotItsCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Required = True
      Size = 15
    end
    object QrCNAB_LotItsIE: TWideStringField
      FieldName = 'IE'
      Size = 25
    end
    object QrCNAB_LotItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCNAB_LotItsRua: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
    object QrCNAB_LotItsCompl: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object QrCNAB_LotItsBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 30
    end
    object QrCNAB_LotItsCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 25
    end
    object QrCNAB_LotItsUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrCNAB_LotItsCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrCNAB_LotItsTel1: TWideStringField
      FieldName = 'Tel1'
    end
    object QrCNAB_LotItsRisco: TFloatField
      FieldName = 'Risco'
    end
    object QrCNAB_LotItsMultaTiVe: TSmallintField
      FieldName = 'MultaTiVe'
    end
    object QrCNAB_LotItsProtestar: TSmallintField
      FieldName = 'Protestar'
    end
    object QrCNAB_LotItsJuroSacado: TFloatField
      FieldName = 'JuroSacado'
    end
    object QrCNAB_LotItsPUF: TSmallintField
      FieldName = 'PUF'
    end
    object QrCNAB_LotItsEUF: TSmallintField
      FieldName = 'EUF'
    end
    object QrCNAB_LotItsUFE: TWideStringField
      FieldName = 'UFE'
      Required = True
      Size = 2
    end
    object QrCNAB_LotItsUFP: TWideStringField
      FieldName = 'UFP'
      Required = True
      Size = 2
    end
    object QrCNAB_LotItsCNPJCLI: TWideStringField
      FieldName = 'CNPJCLI'
      Size = 18
    end
    object QrCNAB_LotItsRuaCLI: TWideStringField
      FieldName = 'RuaCLI'
      Size = 30
    end
    object QrCNAB_LotItsCplCLI: TWideStringField
      FieldName = 'CplCLI'
      Size = 30
    end
    object QrCNAB_LotItsBrrCLI: TWideStringField
      FieldName = 'BrrCLI'
      Size = 30
    end
    object QrCNAB_LotItsCidCLI: TWideStringField
      FieldName = 'CidCLI'
      Size = 25
    end
    object QrCNAB_LotItsTipoCLI: TSmallintField
      FieldName = 'TipoCLI'
    end
    object QrCNAB_LotItsCorrido: TIntegerField
      FieldName = 'Corrido'
    end
    object QrCNAB_LotItsNumero: TFloatField
      FieldName = 'Numero'
    end
    object QrCNAB_LotItsNumCLI: TFloatField
      FieldName = 'NumCLI'
    end
    object QrCNAB_LotItsENDERECO_EMI: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENDERECO_EMI'
      Size = 255
      Calculated = True
    end
    object QrCNAB_LotItsCNAB_Lot: TIntegerField
      FieldName = 'CNAB_Lot'
    end
    object QrCNAB_LotItsSACADO_CNPJ: TWideStringField
      FieldName = 'SACADO_CNPJ'
      Required = True
      Size = 15
    end
    object QrCNAB_LotItsSACADO_NOME: TWideStringField
      FieldName = 'SACADO_NOME'
      Size = 50
    end
    object QrCNAB_LotItsSACADO_RUA: TWideStringField
      FieldName = 'SACADO_RUA'
      Size = 30
    end
    object QrCNAB_LotItsSACADO_NUMERO: TLargeintField
      FieldName = 'SACADO_NUMERO'
    end
    object QrCNAB_LotItsSACADO_COMPL: TWideStringField
      FieldName = 'SACADO_COMPL'
      Size = 30
    end
    object QrCNAB_LotItsSACADO_BAIRRO: TWideStringField
      FieldName = 'SACADO_BAIRRO'
      Size = 30
    end
    object QrCNAB_LotItsSACADO_CIDADE: TWideStringField
      FieldName = 'SACADO_CIDADE'
      Size = 25
    end
    object QrCNAB_LotItsSACADO_xUF: TWideStringField
      FieldName = 'SACADO_xUF'
      Size = 2
    end
    object QrCNAB_LotItsSACADO_CEP: TIntegerField
      FieldName = 'SACADO_CEP'
    end
    object QrCNAB_LotItsSACADO_TIPO: TLargeintField
      FieldName = 'SACADO_TIPO'
      Required = True
    end
    object QrCNAB_LotItsSACADO_CEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SACADO_CEP_TXT'
      Size = 10
      Calculated = True
    end
    object QrCNAB_LotItsSACADO_NUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SACADO_NUMERO_TXT'
      Size = 15
      Calculated = True
    end
    object QrCNAB_LotItsDOCUMCLI: TWideStringField
      FieldName = 'DOCUMCLI'
      Size = 18
    end
    object QrCNAB_LotItsCEPCLI: TFloatField
      FieldName = 'CEPCLI'
    end
  end
  object DsCNABLotIts: TDataSource
    DataSet = QrCNAB_LotIts
    Left = 504
    Top = 8
  end
  object PMLotes: TPopupMenu
    OnPopup = PMLotesPopup
    Left = 576
    Top = 424
    object Crianovolote1: TMenuItem
      Caption = '&Cria novo lote'
      OnClick = Crianovolote1Click
    end
    object Alteraloteatual1: TMenuItem
      Caption = '&Altera lote atual'
      OnClick = Alteraloteatual1Click
    end
    object Excluiloteatual1: TMenuItem
      Caption = '&Exclui lote atual'
      OnClick = Excluiloteatual1Click
    end
  end
  object PMTitulos: TPopupMenu
    OnPopup = PMTitulosPopup
    Left = 660
    Top = 428
    object Inclui1: TMenuItem
      Caption = '&Entrada no Banco'
      OnClick = Inclui1Click
    end
    object Instruesparabanco1: TMenuItem
      Caption = '&Instru'#231#245'es para banco'
      Enabled = False
      OnClick = Instruesparabanco1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Retira1: TMenuItem
      Caption = '&Remove do lote'
      OnClick = Retira1Click
    end
  end
  object QrTitulos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT lot.Cliente, lot.Lote, CASE WHEN ent.Tipo=0 THEN ent.Raza' +
        'oSocial'
      'ELSE ent.Nome END NOMECLI, loi.*'
      'FROM lotesits loi'
      'LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente'
      'WHERE lot.Tipo=1'
      'AND loi.Cobranca=0'
      'AND loi.CNAB_Lot=0')
    Left = 540
    Top = 8
    object QrTitulosCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrTitulosLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrTitulosNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrTitulosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTitulosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrTitulosComp: TIntegerField
      FieldName = 'Comp'
      Required = True
    end
    object QrTitulosBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
    end
    object QrTitulosAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
    end
    object QrTitulosConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrTitulosCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
    end
    object QrTitulosCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrTitulosEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrTitulosBruto: TFloatField
      FieldName = 'Bruto'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTitulosDesco: TFloatField
      FieldName = 'Desco'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTitulosValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTitulosEmissao: TDateField
      FieldName = 'Emissao'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTitulosDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTitulosDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTitulosVencto: TDateField
      FieldName = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTitulosTxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Required = True
    end
    object QrTitulosTxaJuros: TFloatField
      FieldName = 'TxaJuros'
      Required = True
    end
    object QrTitulosTxaAdValorem: TFloatField
      FieldName = 'TxaAdValorem'
      Required = True
    end
    object QrTitulosVlrCompra: TFloatField
      FieldName = 'VlrCompra'
      Required = True
    end
    object QrTitulosVlrAdValorem: TFloatField
      FieldName = 'VlrAdValorem'
      Required = True
    end
    object QrTitulosDMais: TIntegerField
      FieldName = 'DMais'
      Required = True
    end
    object QrTitulosDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrTitulosDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrTitulosDevolucao: TIntegerField
      FieldName = 'Devolucao'
      Required = True
    end
    object QrTitulosQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrTitulosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTitulosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTitulosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTitulosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTitulosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTitulosPraca: TIntegerField
      FieldName = 'Praca'
      Required = True
    end
    object QrTitulosBcoCobra: TIntegerField
      FieldName = 'BcoCobra'
      Required = True
    end
    object QrTitulosAgeCobra: TIntegerField
      FieldName = 'AgeCobra'
      Required = True
    end
    object QrTitulosTotalJr: TFloatField
      FieldName = 'TotalJr'
      Required = True
    end
    object QrTitulosTotalDs: TFloatField
      FieldName = 'TotalDs'
      Required = True
    end
    object QrTitulosTotalPg: TFloatField
      FieldName = 'TotalPg'
      Required = True
    end
    object QrTitulosData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrTitulosProrrVz: TIntegerField
      FieldName = 'ProrrVz'
      Required = True
    end
    object QrTitulosProrrDd: TIntegerField
      FieldName = 'ProrrDd'
      Required = True
    end
    object QrTitulosRepassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
    object QrTitulosDepositado: TSmallintField
      FieldName = 'Depositado'
      Required = True
    end
    object QrTitulosValQuit: TFloatField
      FieldName = 'ValQuit'
      Required = True
    end
    object QrTitulosValDeposito: TFloatField
      FieldName = 'ValDeposito'
      Required = True
    end
    object QrTitulosTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrTitulosAliIts: TIntegerField
      FieldName = 'AliIts'
      Required = True
    end
    object QrTitulosAlinPgs: TIntegerField
      FieldName = 'AlinPgs'
      Required = True
    end
    object QrTitulosNaoDeposita: TSmallintField
      FieldName = 'NaoDeposita'
      Required = True
    end
    object QrTitulosReforcoCxa: TSmallintField
      FieldName = 'ReforcoCxa'
      Required = True
    end
    object QrTitulosCartDep: TIntegerField
      FieldName = 'CartDep'
      Required = True
    end
    object QrTitulosCNAB_Lot: TIntegerField
      FieldName = 'CNAB_Lot'
    end
  end
  object DsTitulos: TDataSource
    DataSet = QrTitulos
    Left = 568
    Top = 8
  end
  object QrCNAB_Cfg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cbb.Nome, cbb.Codigo'
      'FROM cnab_cfg cbb'
      'ORDER BY Nome')
    Left = 388
    Top = 120
    object QrCNAB_CfgNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCNAB_CfgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsCNAB_Cfg: TDataSource
    DataSet = QrCNAB_Cfg
    Left = 416
    Top = 120
  end
  object mySQLQuery1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ban.Nome NOMEBANCO, cbb.*'
      'FROM cnab_cfg cbb'
      'LEFT JOIN bancos ban ON ban.Codigo=cbb.Banco '
      'WHERE cbb.Codigo=:P0')
    Left = 596
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object IntegerField2: TIntegerField
      FieldName = 'Convenio'
    end
    object StringField2: TWideStringField
      FieldName = 'Carteira'
      Size = 2
    end
    object StringField3: TWideStringField
      FieldName = 'Variacao'
      Size = 3
    end
    object StringField4: TWideStringField
      FieldName = 'SiglaEspecie'
      Size = 5
    end
    object StringField5: TWideStringField
      FieldName = 'Moeda'
      Size = 5
    end
    object SmallintField1: TSmallintField
      FieldName = 'Aceite'
    end
    object SmallintField2: TSmallintField
      FieldName = 'Protestar'
    end
    object StringField6: TWideStringField
      FieldName = 'MsgLinha1'
      Size = 40
    end
    object SmallintField3: TSmallintField
      FieldName = 'PgAntes'
    end
    object SmallintField4: TSmallintField
      FieldName = 'MultaCodi'
    end
    object SmallintField5: TSmallintField
      FieldName = 'MultaDias'
    end
    object FloatField1: TFloatField
      FieldName = 'MultaValr'
    end
    object FloatField2: TFloatField
      FieldName = 'MultaPerc'
    end
    object SmallintField6: TSmallintField
      FieldName = 'MultaTiVe'
    end
    object SmallintField7: TSmallintField
      FieldName = 'ImpreLoc'
    end
    object IntegerField3: TIntegerField
      FieldName = 'Modalidade'
    end
    object StringField7: TWideStringField
      FieldName = 'clcAgencNr'
      Size = 4
    end
    object StringField8: TWideStringField
      FieldName = 'clcAgencDV'
      Size = 1
    end
    object StringField9: TWideStringField
      FieldName = 'clcContaNr'
      Size = 8
    end
    object StringField10: TWideStringField
      FieldName = 'clcContaDV'
      Size = 1
    end
    object StringField11: TWideStringField
      FieldName = 'cedAgencNr'
      Size = 4
    end
    object StringField12: TWideStringField
      FieldName = 'cedAgencDV'
      Size = 1
    end
    object StringField13: TWideStringField
      FieldName = 'cedContaNr'
      Size = 8
    end
    object StringField14: TWideStringField
      FieldName = 'cedContaDV'
      Size = 1
    end
    object SmallintField8: TSmallintField
      FieldName = 'Especie'
    end
    object SmallintField9: TSmallintField
      FieldName = 'Corrido'
    end
    object IntegerField4: TIntegerField
      FieldName = 'Banco'
    end
    object StringField15: TWideStringField
      FieldName = 'IDEmpresa'
    end
    object StringField16: TWideStringField
      FieldName = 'Produto'
      Size = 4
    end
    object StringField17: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
    object SmallintField10: TSmallintField
      FieldName = 'InfoCovH'
      Required = True
    end
    object StringField18: TWideStringField
      FieldName = 'Carteira240'
      Size = 1
    end
    object StringField19: TWideStringField
      FieldName = 'Cadastramento'
      Size = 1
    end
    object StringField20: TWideStringField
      FieldName = 'TradiEscrit'
      Size = 1
    end
    object StringField21: TWideStringField
      FieldName = 'Distribuicao'
      Size = 1
    end
    object StringField22: TWideStringField
      FieldName = 'Aceite240'
      Size = 1
    end
    object StringField23: TWideStringField
      FieldName = 'Protesto'
      Size = 1
    end
    object IntegerField5: TIntegerField
      FieldName = 'Protestodd'
      Required = True
    end
    object StringField24: TWideStringField
      FieldName = 'BaixaDevol'
      Size = 1
    end
    object IntegerField6: TIntegerField
      FieldName = 'BaixaDevoldd'
      Required = True
    end
    object IntegerField7: TIntegerField
      FieldName = 'Lk'
    end
    object DateField1: TDateField
      FieldName = 'DataCad'
    end
    object DateField2: TDateField
      FieldName = 'DataAlt'
    end
    object IntegerField8: TIntegerField
      FieldName = 'UserCad'
    end
    object IntegerField9: TIntegerField
      FieldName = 'UserAlt'
    end
    object StringField25: TWideStringField
      FieldName = 'EmisBloqueto'
      Size = 1
    end
    object StringField26: TWideStringField
      FieldName = 'Especie240'
      Size = 2
    end
    object StringField27: TWideStringField
      FieldName = 'Juros240Cod'
      Size = 1
    end
    object FloatField3: TFloatField
      FieldName = 'Juros240Qtd'
      Required = True
    end
    object IntegerField10: TIntegerField
      FieldName = 'ContrOperCred'
      Required = True
    end
    object StringField28: TWideStringField
      FieldName = 'ReservBanco'
    end
    object StringField29: TWideStringField
      FieldName = 'ReservEmprs'
    end
    object StringField30: TWideStringField
      FieldName = 'LH_208_33'
      Size = 33
    end
    object StringField31: TWideStringField
      FieldName = 'SQ_233_008'
      Size = 8
    end
    object StringField32: TWideStringField
      FieldName = 'TL_124_117'
      Size = 117
    end
    object StringField33: TWideStringField
      FieldName = 'SR_208_033'
      Size = 33
    end
    object StringField34: TWideStringField
      FieldName = 'Diretorio'
      Size = 255
    end
  end
  object QrLot: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(CodUsu) CodUsu'
      'FROM cnab_lot')
    Left = 396
    Top = 56
    object QrLotCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
  end
end
