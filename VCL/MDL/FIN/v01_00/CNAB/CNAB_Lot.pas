unit CNAB_Lot;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  ComCtrls, Grids, DBGrids, Menus, Variants, dmkEdit, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkLabel, dmkDBGrid, UnDmkProcFunc,
  UnDmkEnums;

type
  TTipoGera = (tgEnvio, tgTeste);
  dceAlinha = (posEsquerda, posCentro, posDireita);
  TFmCNAB_Lot = class(TForm)
    PainelDados: TPanel;
    DsCNAB_Lot: TDataSource;
    QrCNAB_Lot: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtGera: TBitBtn;
    BtTitulos: TBitBtn;
    BtLotes: TBitBtn;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    TPDataG: TdmkEditDateTimePicker;
    Label3: TLabel;
    QrCNAB_LotIts: TmySQLQuery;
    DsCNABLotIts: TDataSource;
    PnTitulos: TPanel;
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    PMLotes: TPopupMenu;
    Crianovolote1: TMenuItem;
    Alteraloteatual1: TMenuItem;
    Excluiloteatual1: TMenuItem;
    PMTitulos: TPopupMenu;
    Inclui1: TMenuItem;
    Retira1: TMenuItem;
    QrCNAB_LotMyDATAS: TWideStringField;
    Panel1: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    DBEdit01: TDBEdit;
    DBEdit2: TDBEdit;
    QrTitulos: TmySQLQuery;
    DsTitulos: TDataSource;
    QrTitulosCliente: TIntegerField;
    QrTitulosLote: TIntegerField;
    QrTitulosNOMECLI: TWideStringField;
    QrTitulosCodigo: TIntegerField;
    QrTitulosControle: TIntegerField;
    QrTitulosComp: TIntegerField;
    QrTitulosBanco: TIntegerField;
    QrTitulosAgencia: TIntegerField;
    QrTitulosConta: TWideStringField;
    QrTitulosCheque: TIntegerField;
    QrTitulosCPF: TWideStringField;
    QrTitulosEmitente: TWideStringField;
    QrTitulosBruto: TFloatField;
    QrTitulosDesco: TFloatField;
    QrTitulosValor: TFloatField;
    QrTitulosEmissao: TDateField;
    QrTitulosDCompra: TDateField;
    QrTitulosDDeposito: TDateField;
    QrTitulosVencto: TDateField;
    QrTitulosTxaCompra: TFloatField;
    QrTitulosTxaJuros: TFloatField;
    QrTitulosTxaAdValorem: TFloatField;
    QrTitulosVlrCompra: TFloatField;
    QrTitulosVlrAdValorem: TFloatField;
    QrTitulosDMais: TIntegerField;
    QrTitulosDias: TIntegerField;
    QrTitulosDuplicata: TWideStringField;
    QrTitulosDevolucao: TIntegerField;
    QrTitulosQuitado: TIntegerField;
    QrTitulosLk: TIntegerField;
    QrTitulosDataCad: TDateField;
    QrTitulosDataAlt: TDateField;
    QrTitulosUserCad: TIntegerField;
    QrTitulosUserAlt: TIntegerField;
    QrTitulosPraca: TIntegerField;
    QrTitulosBcoCobra: TIntegerField;
    QrTitulosAgeCobra: TIntegerField;
    QrTitulosTotalJr: TFloatField;
    QrTitulosTotalDs: TFloatField;
    QrTitulosTotalPg: TFloatField;
    QrTitulosData3: TDateField;
    QrTitulosProrrVz: TIntegerField;
    QrTitulosProrrDd: TIntegerField;
    QrTitulosRepassado: TSmallintField;
    QrTitulosDepositado: TSmallintField;
    QrTitulosValQuit: TFloatField;
    QrTitulosValDeposito: TFloatField;
    QrTitulosTipo: TIntegerField;
    QrTitulosAliIts: TIntegerField;
    QrTitulosAlinPgs: TIntegerField;
    QrTitulosNaoDeposita: TSmallintField;
    QrTitulosReforcoCxa: TSmallintField;
    QrTitulosCartDep: TIntegerField;
    Panel4: TPanel;
    BitBtn2: TBitBtn;
    Panel6: TPanel;
    BtDesiste: TBitBtn;
    QrCNAB_LotItsCliente: TIntegerField;
    QrCNAB_LotItsLote: TIntegerField;
    QrCNAB_LotItsNOMECLI: TWideStringField;
    QrCNAB_LotItsCodigo: TIntegerField;
    QrCNAB_LotItsControle: TIntegerField;
    QrCNAB_LotItsComp: TIntegerField;
    QrCNAB_LotItsBanco: TIntegerField;
    QrCNAB_LotItsAgencia: TIntegerField;
    QrCNAB_LotItsConta: TWideStringField;
    QrCNAB_LotItsCheque: TIntegerField;
    QrCNAB_LotItsCPF: TWideStringField;
    QrCNAB_LotItsEmitente: TWideStringField;
    QrCNAB_LotItsBruto: TFloatField;
    QrCNAB_LotItsDesco: TFloatField;
    QrCNAB_LotItsValor: TFloatField;
    QrCNAB_LotItsEmissao: TDateField;
    QrCNAB_LotItsDCompra: TDateField;
    QrCNAB_LotItsDDeposito: TDateField;
    QrCNAB_LotItsVencto: TDateField;
    QrCNAB_LotItsTxaCompra: TFloatField;
    QrCNAB_LotItsTxaJuros: TFloatField;
    QrCNAB_LotItsTxaAdValorem: TFloatField;
    QrCNAB_LotItsVlrCompra: TFloatField;
    QrCNAB_LotItsVlrAdValorem: TFloatField;
    QrCNAB_LotItsDMais: TIntegerField;
    QrCNAB_LotItsDias: TIntegerField;
    QrCNAB_LotItsDuplicata: TWideStringField;
    QrCNAB_LotItsDevolucao: TIntegerField;
    QrCNAB_LotItsQuitado: TIntegerField;
    QrCNAB_LotItsLk: TIntegerField;
    QrCNAB_LotItsDataCad: TDateField;
    QrCNAB_LotItsDataAlt: TDateField;
    QrCNAB_LotItsUserCad: TIntegerField;
    QrCNAB_LotItsUserAlt: TIntegerField;
    QrCNAB_LotItsPraca: TIntegerField;
    QrCNAB_LotItsBcoCobra: TIntegerField;
    QrCNAB_LotItsAgeCobra: TIntegerField;
    QrCNAB_LotItsTotalJr: TFloatField;
    QrCNAB_LotItsTotalDs: TFloatField;
    QrCNAB_LotItsTotalPg: TFloatField;
    QrCNAB_LotItsData3: TDateField;
    QrCNAB_LotItsProrrVz: TIntegerField;
    QrCNAB_LotItsProrrDd: TIntegerField;
    QrCNAB_LotItsRepassado: TSmallintField;
    QrCNAB_LotItsDepositado: TSmallintField;
    QrCNAB_LotItsValQuit: TFloatField;
    QrCNAB_LotItsValDeposito: TFloatField;
    QrCNAB_LotItsTipo: TIntegerField;
    QrCNAB_LotItsAliIts: TIntegerField;
    QrCNAB_LotItsAlinPgs: TIntegerField;
    QrCNAB_LotItsNaoDeposita: TSmallintField;
    QrCNAB_LotItsReforcoCxa: TSmallintField;
    QrCNAB_LotItsCartDep: TIntegerField;
    Label6: TLabel;
    Memo1: TMemo;
    QrCNAB_Cfg: TmySQLQuery;
    QrCNAB_LotItsMultaCodi: TSmallintField;
    QrCNAB_LotItsMultaDias: TSmallintField;
    QrCNAB_LotItsMultaValr: TFloatField;
    QrCNAB_LotItsMultaPerc: TFloatField;
    QrCNAB_LotItsCNPJ: TWideStringField;
    QrCNAB_LotItsIE: TWideStringField;
    QrCNAB_LotItsNome: TWideStringField;
    QrCNAB_LotItsRua: TWideStringField;
    QrCNAB_LotItsCompl: TWideStringField;
    QrCNAB_LotItsBairro: TWideStringField;
    QrCNAB_LotItsCidade: TWideStringField;
    QrCNAB_LotItsUF: TWideStringField;
    QrCNAB_LotItsCEP: TIntegerField;
    QrCNAB_LotItsTel1: TWideStringField;
    QrCNAB_LotItsRisco: TFloatField;
    QrCNAB_LotItsMultaTiVe: TSmallintField;
    QrCNAB_LotItsProtestar: TSmallintField;
    QrCNAB_LotItsJuroSacado: TFloatField;
    QrCNAB_LotItsPUF: TSmallintField;
    QrCNAB_LotItsEUF: TSmallintField;
    QrCNAB_LotItsUFE: TWideStringField;
    QrCNAB_LotItsUFP: TWideStringField;
    QrCNAB_LotItsCNPJCLI: TWideStringField;
    QrCNAB_LotItsRuaCLI: TWideStringField;
    QrCNAB_LotItsCplCLI: TWideStringField;
    QrCNAB_LotItsBrrCLI: TWideStringField;
    QrCNAB_LotItsCidCLI: TWideStringField;
    QrCNAB_LotItsTipoCLI: TSmallintField;
    Splitter1: TSplitter;
    QrCNAB_LotItsCorrido: TIntegerField;
    QrCNAB_LotItsNumero: TFloatField;
    QrCNAB_LotItsNumCLI: TFloatField;
    Label7: TLabel;
    EdCNAB_Cfg: TdmkEditCB;
    CBCNAB_Cfg: TdmkDBLookupComboBox;
    Edit1: TEdit;
    Label10: TLabel;
    Label11: TLabel;
    TPHoraG: TDateTimePicker;
    QrCNAB_LotHoraG: TTimeField;
    EdMensagem1: TdmkEdit;
    Label13: TLabel;
    EdMensagem2: TdmkEdit;
    Label14: TLabel;
    Label15: TLabel;
    QrCNAB_LotItsENDERECO_EMI: TWideStringField;
    N1: TMenuItem;
    Instruesparabanco1: TMenuItem;
    Panel7: TPanel;
    DBEdit5: TDBEdit;
    Label16: TLabel;
    DBEdCodigo: TDBEdit;
    Label1: TLabel;
    Label8: TLabel;
    DBEdit3: TDBEdit;
    Label17: TLabel;
    DBEdit6: TDBEdit;
    QrCNAB_LotHoraS: TTimeField;
    QrCNAB_LotMyDATAG: TWideStringField;
    mySQLQuery1: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    StringField3: TWideStringField;
    StringField4: TWideStringField;
    StringField5: TWideStringField;
    SmallintField1: TSmallintField;
    SmallintField2: TSmallintField;
    StringField6: TWideStringField;
    SmallintField3: TSmallintField;
    SmallintField4: TSmallintField;
    SmallintField5: TSmallintField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    SmallintField6: TSmallintField;
    SmallintField7: TSmallintField;
    IntegerField3: TIntegerField;
    StringField7: TWideStringField;
    StringField8: TWideStringField;
    StringField9: TWideStringField;
    StringField10: TWideStringField;
    StringField11: TWideStringField;
    StringField12: TWideStringField;
    StringField13: TWideStringField;
    StringField14: TWideStringField;
    SmallintField8: TSmallintField;
    SmallintField9: TSmallintField;
    IntegerField4: TIntegerField;
    StringField15: TWideStringField;
    StringField16: TWideStringField;
    StringField17: TWideStringField;
    SmallintField10: TSmallintField;
    StringField18: TWideStringField;
    StringField19: TWideStringField;
    StringField20: TWideStringField;
    StringField21: TWideStringField;
    StringField22: TWideStringField;
    StringField23: TWideStringField;
    IntegerField5: TIntegerField;
    StringField24: TWideStringField;
    IntegerField6: TIntegerField;
    IntegerField7: TIntegerField;
    DateField1: TDateField;
    DateField2: TDateField;
    IntegerField8: TIntegerField;
    IntegerField9: TIntegerField;
    StringField25: TWideStringField;
    StringField26: TWideStringField;
    StringField27: TWideStringField;
    FloatField3: TFloatField;
    IntegerField10: TIntegerField;
    StringField28: TWideStringField;
    StringField29: TWideStringField;
    StringField30: TWideStringField;
    StringField31: TWideStringField;
    StringField32: TWideStringField;
    StringField33: TWideStringField;
    StringField34: TWideStringField;
    QrCNAB_LotNOMECONFIG: TWideStringField;
    QrCNAB_LotCodigo: TIntegerField;
    QrCNAB_LotDataG: TDateField;
    QrCNAB_LotCNAB_Cfg: TIntegerField;
    QrCNAB_LotMensagem1: TWideStringField;
    QrCNAB_LotMensagem2: TWideStringField;
    QrCNAB_LotDataS: TDateField;
    QrCNAB_CfgNome: TWideStringField;
    QrCNAB_CfgCodigo: TIntegerField;
    QrTitulosCNAB_Lot: TIntegerField;
    QrCNAB_LotItsCNAB_Lot: TIntegerField;
    QrCNAB_LotSeqArq: TIntegerField;
    EdCodUsu: TdmkEdit;
    Label20: TLabel;
    QrLot: TmySQLQuery;
    QrLotCodUsu: TIntegerField;
    QrCNAB_LotCodUsu: TIntegerField;
    Label21: TLabel;
    DBEdit9: TDBEdit;
    Panel8: TPanel;
    Panel9: TPanel;
    GroupBox2: TGroupBox;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label12: TLabel;
    DBEdit1: TDBEdit;
    DBEdit4: TDBEdit;
    Label22: TLabel;
    DBEdit10: TDBEdit;
    Label23: TLabel;
    Ed_2401_208: TdmkEdit;
    QrCNAB_Lot_2401_208: TWideStringField;
    QrCNAB_LotItsSACADO_CNPJ: TWideStringField;
    QrCNAB_LotItsSACADO_NOME: TWideStringField;
    QrCNAB_LotItsSACADO_RUA: TWideStringField;
    QrCNAB_LotItsSACADO_NUMERO: TLargeintField;
    QrCNAB_LotItsSACADO_COMPL: TWideStringField;
    QrCNAB_LotItsSACADO_BAIRRO: TWideStringField;
    QrCNAB_LotItsSACADO_CIDADE: TWideStringField;
    QrCNAB_LotItsSACADO_xUF: TWideStringField;
    QrCNAB_LotItsSACADO_CEP: TIntegerField;
    QrCNAB_LotItsSACADO_TIPO: TLargeintField;
    QrCNAB_LotItsSACADO_CEP_TXT: TWideStringField;
    QrCNAB_LotItsSACADO_NUMERO_TXT: TWideStringField;
    QrCNAB_LotItsDOCUMCLI: TWideStringField;
    DBGrid1: TdmkDBGrid;
    DBGrid2: TdmkDBGrid;
    QrCNAB_LotItsCEPCLI: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtLotesClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCNAB_LotAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrCNAB_LotAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCNAB_LotBeforeOpen(DataSet: TDataSet);
    procedure Crianovolote1Click(Sender: TObject);
    procedure Alteraloteatual1Click(Sender: TObject);
    procedure Excluiloteatual1Click(Sender: TObject);
    procedure PMLotesPopup(Sender: TObject);
    procedure BtTitulosClick(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure QrCNAB_LotCalcFields(DataSet: TDataSet);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Retira1Click(Sender: TObject);
    procedure Memo1Change(Sender: TObject);
    procedure QrCNAB_LotItsCalcFields(DataSet: TDataSet);
    procedure Instruesparabanco1Click(Sender: TObject);
    procedure BtGeraClick(Sender: TObject);
    procedure PMTitulosPopup(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenTitulos(Controle: Integer);
    procedure AdicionaItem;
    procedure CalculaLote(Lote: Integer);
    {procedure AdicionaAoMemo(Memo: TMemo; Texto: String;
              MesmoSeTextoVazio: Boolean; Tamanho: Integer);}
    {procedure VerificaSomaArray(MaxS: Integer; Tam: array of Integer);}
    {function CompletaString(Texto, Compl: String; Tamanho: Integer;
             Alinhamento: dceAlinha): String;}
    {function AjustaString(Texto, Compl: String; Tamanho: Integer;
             Alinhamento: dceAlinha): String;}
    //  FIM gera arquivo remessa
    {procedure VerificaTamanhoTxts(Txt: array of String; Tam: array of Integer;
              MaxS: Integer);}
    function ObtemProximoLote(): Integer;
    function ObtemCaminhoArquivo(): String;
  public
    { Public declarations }
    FSeq: Integer;
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenCNAB_LotIts(Controle: Integer);
  end;

var
  FmCNAB_Lot: TFmCNAB_Lot;

const
  F_CR = #13#10;
  FFormatFloat = '00000';
  //FArquivoSalvaDir = 'C:\Dermatek\Titulos\';
  FArquivoSalvaArq = 'CNAB240.txt';
  FTamCNAB = 240;

implementation

uses UnMyObjects, Module, CNAB240RemSel, ModuleBco, UCreate, MyDBCheck, CNAB_Rem2, CNAB_Rem;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCNAB_Lot.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCNAB_Lot.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCNAB_LotCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCNAB_Lot.DefParams;
begin
  VAR_GOTOTABELA := 'CNAB_Lot';
  VAR_GOTOMYSQLTABLE := QrCNAB_Lot;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT con.Nome NOMECONFIG, cob.*');
  VAR_SQLx.Add('FROM cnab_lot cob');
  VAR_SQLx.Add('LEFT JOIN cnab_cfg con ON con.Codigo=cob.CNAB_Cfg');
  VAR_SQLx.Add('WHERE cob.Codigo > -1000');
  //
  VAR_SQL1.Add('AND cob.Codigo=:P0');
  //
  VAR_SQLa.Add(''); //AND Nome Like :P0
  //
end;

procedure TFmCNAB_Lot.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible := True;
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
      PnTitulos.Visible      := False;
      // Corrigir
      Memo1.Align            := alTop;
      Memo1.Align            := alBottom;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.Text       := '';
        TPDataG.Date        := Date;
        TPHoraG.Time        := Time;
        EdCNAB_Cfg.Text     := '0';
        CBCNAB_Cfg.KeyValue := NULL;
        EdMensagem1.Text    := '';
        EdMensagem2.Text    := '';
        Ed_2401_208.Text    := '';
        EdCodUsu.ValueVariant := ObtemProximoLote();
      end else begin
        EdCodigo.Text       := Geral.FF0(QrCNAB_LotCodigo.Value);
        TPDataG.Date        := QrCNAB_LotDataG.Value;
        TPHoraG.Time        := QrCNAB_LotHoraG.Value;
        EdCNAB_Cfg.Text     := Geral.FF0(QrCNAB_LotCNAB_Cfg.Value);
        CBCNAB_Cfg.KeyValue := QrCNAB_LotCNAB_Cfg.Value;
        EdMensagem1.Text    := QrCNAB_LotMensagem1.Value;
        EdMensagem2.Text    := QrCNAB_LotMensagem2.Value;
        Ed_2401_208.Text    := QrCNAB_Lot_2401_208.Value;
        EdCodUsu.ValueVariant := QrCNAB_LotCodUsu.Value;
      end;
      TPDataG.SetFocus;
    end;
    2:
    begin
      PnTitulos.Visible      := True;
      PainelDados.Visible    := False;
      PainelControle.Visible :=False;
      ReopenTitulos(0);
    end;
  end;
  LaTipo.SQLType := SQLType;
  GOTOy.BotoesSb(LaTipo.Caption);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

function TFmCNAB_Lot.ObtemProximoLote(): Integer;
begin
  QrLot.Close;
  QrLot.Open;
  Result := QrLotCodUsu.Value + 1;
end;

procedure TFmCNAB_Lot.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCNAB_Lot.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCNAB_Lot.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCNAB_Lot.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCNAB_Lot.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCNAB_Lot.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCNAB_Lot.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCNAB_Lot.BtLotesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLotes, BtLotes);
end;

procedure TFmCNAB_Lot.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCNAB_LotCodigo.Value;
  Close;
end;

procedure TFmCNAB_Lot.BtConfirmaClick(Sender: TObject);
var
  Codigo, CNAB_Cfg, CodUsu: Integer;
  DataG, HoraG, Mensagem1, Mensagem2, _2401_208: String;
begin
  DataG     := Geral.FDT(TPDataG.Date, 1);
  HoraG     := Geral.FDT(TPHoraG.Time, 100);
  CodUsu    := EdCodUsu.ValueVariant;
  CNAB_Cfg  := Geral.IMV(EdCNAB_Cfg.Text);
  _2401_208 := Ed_2401_208.Text;
  //
  if MyObjects.FIC(CNAB_Cfg = 0, EdCNAB_Cfg, 'Defina a conta de cobran�a!') then Exit;
  //
  Mensagem1 := Geral.Maiusculas(Geral.SemAcento(EdMensagem1.Text), False);
  Mensagem2 := Geral.Maiusculas(Geral.SemAcento(EdMensagem2.Text), False);
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('cnab_lot', 'Codigo', LaTipo.SQLType, QrCNAB_LotCodigo.Value);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'cnab_lot', False, [
    'DataG', 'HoraG', 'CNAB_Cfg', 'Mensagem1', 'Mensagem2',
    'CodUsu', '_2401_208'
  ], ['Codigo'], [
    DataG, HoraG, CNAB_Cfg, Mensagem1, Mensagem2,
    CodUsu, _2401_208
  ], [Codigo], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CNAB_Lot', 'Codigo');
    MostraEdicao(0, stLok, 0);
    LocCod(Codigo,Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmCNAB_Lot.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'CNAB_Lot', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CNAB_Lot', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CNAB_Lot', 'Codigo');
end;

procedure TFmCNAB_Lot.BtGeraClick(Sender: TObject);
  function EnderecoSacado(): String;
  begin
    Result := '';
    if Result <> '' then Result := Result + ' ';

    Result := Result + QrCNAB_LotItsSACADO_RUA.Value;
    if QrCNAB_LotItsSACADO_RUA.Value <> '' then Result := Result + ', ';

    Result := Result + QrCNAB_LotItsSACADO_NUMERO_TXT.Value;
    if QrCNAB_LotItsSACADO_NUMERO_TXT.Value <> '' then Result := Result + ' ';

    if QrCNAB_LotItsSACADO_COMPL.Value <> '' then Result := Result + '- ';
    Result := Result + QrCNAB_LotItsSACADO_COMPL.Value;
  end;
  function Endereco(): String;
  begin
    Result := DmBco.QrAddr3NOMELOGRAD.Value;
    if Result <> '' then Result := Result + ' ';

    Result := Result + DmBco.QrAddr3RUA.Value;
    if DmBco.QrAddr3RUA.Value <> '' then Result := Result + ', ';

    Result := Result + DmBco.QrAddr3NUMERO_TXT.Value;
    if DmBco.QrAddr3NUMERO_TXT.Value <> '' then Result := Result + ' ';

    if DmBco.QrAddr3COMPL.Value <> '' then Result := Result + '- ';
    Result := Result + DmBco.QrAddr3COMPL.Value;
  end;
const
  // PAREI AQUI Ver!!!
  Def_Client = -11;
var
  CedenteTipo, SacadorTipo, Item, Addr3_Tipo, n, NossoNumero: Integer;
  SQL, CedenteNome, CedenteCNPJ, SacadorNome, SacadorCNPJ, NomeCedente,
  NomeSacador, CEPSacado_, CEPSacador, Addr3_CNPJ, Addr3_NOMEENT,
  Addr3_NOMELOGRAD, Addr3_RUA, Addr3_NUMERO_TXT, Addr3_COMPL,
  Addr3_BAIRRO, Addr3_CIDADE, Addr3_NOMEUF: String;
  JurosPerc, MultaPerc, JurosValr, MultaValr: Double;
  {HoraI,} DataMora: TDateTime;
  g: TStringGrid;
  {
  Vezes, i: Integer;
  HoraF: TDateTime;
  }
  CNAB_Cfg, SeqArq, Lote: Integer;
  SQL_HEADE_ARQUI, SQL_HEADE_LOTES, SQL_ITENS_LOTES, AVALISTA: String;
begin
  CNAB_Cfg := QrCNAB_LotCNAB_Cfg.Value;
  SeqArq   := QrCNAB_LotSeqArq.Value;
  Lote     := QrCNAB_LotCodigo.Value;
  //
  SQL_HEADE_ARQUI := MLAGeral.Linhas(
  ['SELECT con.Nome NOMECONFIG, cob.CodUsu NUM_REM, cob.* ',
  'FROM cnab_lot cob',
  'LEFT JOIN cnab_cfg con ON con.Codigo=cob.CNAB_Cfg',
  'WHERE cob.Codigo = ' + FormatFloat('0', Lote)]);
  //
  SQL_HEADE_LOTES := MLAGeral.Linhas(
  ['SELECT con.Nome NOMECONFIG, cob.Codigo NUM_LOT, ',
  'cob.*, con.*',
  'FROM cnab_lot cob',
  'LEFT JOIN cnab_cfg con ON con.Codigo=cob.CNAB_Cfg',
  'WHERE cob.Codigo = ' + FormatFloat('0', Lote)]);
  //
  SQL_ITENS_LOTES := MLAGeral.Linhas(
  ['SELECT loi.Duplicata, loi.Emissao, loi.Vencto, ',
  'loi.Bruto, loi.Desco Abatimento, ',
  'loi.Controle + 0.000000000000000000 SeuNumero, ',
  'loi.Controle + 0.000000000000000000 Sequencial, ',
  'ent.Tipo AVALISTA_TIPO, ',
  'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END AVALISTA_NOME, ',
  'CASE WHEN ent.Tipo=0 THEN ent.CNPJ    ELSE ent.CPF     END AVALISTA_CNPJ, ',
  'CASE WHEN ent.Tipo=0 THEN ent.ERua    ELSE ent.PRua    END AVALISTA_RUA, ',
  'CASE WHEN ent.Tipo=0 THEN ent.ENumero+0.000 ELSE ent.PNumero+0.000 END AVALISTA_NUMERO, ',
  'CASE WHEN ent.Tipo=0 THEN ent.ECompl  ELSE ent.PCompl  END AVALISTA_COMPL, ',
  'CASE WHEN ent.Tipo=0 THEN ent.EBairro ELSE ent.PBairro END AVALISTA_BAIRRO, ',
  'CASE WHEN ent.Tipo=0 THEN ent.ECEP    ELSE ent.PCEP    END + 0.000 AVALISTA_CEP, ',
  'CASE WHEN ent.Tipo=0 THEN ent.ECidade ELSE ent.PCidade END AVALISTA_CIDADE, ',
  'CASE WHEN ent.Tipo=0 THEN ufe.Nome    ELSE ufp.Nome    END AVALISTA_xUF, ',
  'sac.CNPJ SACADO_CNPJ, sac.Nome SACADO_NOME, sac.Rua SACADO_RUA,',
  'sac.Numero SACADO_NUMERO, sac.Compl SACADO_COMPL, ',
  'sac.Bairro SACADO_BAIRRO, sac.Bairro SACADO_BAIRRO,',
  'sac.Cidade SACADO_CIDADE, sac.UF SACADO_xUF, ',
  'sac.CEP SACADO_CEP, IF(LENGTH(sac.CNPJ) >=14, 0, 1) SACADO_TIPO',
  'FROM lotesits loi ',
  'LEFT JOIN lotes     lot ON lot.Codigo=loi.Codigo',
  'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente',
  'LEFT JOIN sacados   sac ON sac.CNPJ=loi.CPF ',
  'LEFT JOIN ufs       ufe ON ent.EUF=ufe.Codigo ',
  'LEFT JOIN ufs       ufp ON ent.PUF=ufp.Codigo ',
  'WHERE loi.CNAB_Lot=:P0 ',
  'ORDER BY Duplicata']);
  //
  SQL := 'SELECT 0 Itens';
  if not DmBco.ReopenCNAB_Cfg(QrCNAB_LotCNAB_Cfg.Value,
    Def_Client, SQL,
    CedenteTipo, CedenteNome, CedenteCNPJ,
    SacadorTipo, SacadorNome, SacadorCNPJ) then
  begin
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  case DmBco.QrCNAB_CfgCNAB.Value of
    240:
    begin
      if DBCheck.CriaFm(TFmCNAB_Rem2, FmCNAB_Rem2, afmoNegarComAviso) then
      begin
        if FmCNAB_Rem2.CarregaDadosNoForm(CNAB_Cfg, SeqArq, Lote, SQL_HEADE_ARQUI,
        SQL_HEADE_LOTES, SQL_ITENS_LOTES, [ftInteger], ['NUM_LOT']) then
          FmCNAB_Rem2.ShowModal;
        FmCNAB_Rem2.Destroy;
      end;
    end;
    400:
    begin
      if QrCNAB_LotCNAB_Cfg.Value = 0 then Exit;
      //HoraI := Now();

      Screen.Cursor := crHourGlass;
      //
      // N�o precisa de confer�ncia de cliente interno ?
      //
      if DmBco.QrCNAB_CfgCedNome.Value <> '' then
        NomeCedente := DmBco.QrCNAB_CfgCedNome.Value
      else NomeCedente := CedenteNome;
      //
      if QrCNAB_LotItsNOMECLI.Value <> '' then
        NomeSacador := QrCNAB_LotItsNOMECLI.Value
      else
        NomeSacador := SacadorNome;
      //
      if QrCNAB_LotSeqArq.Value > 0 then
      begin
        if Geral.MB_Pergunta('Este lote j� possui um n�mero sequencial ' +
        'de remessa de arquivo! Ser� utilizado o mesmo n�mero.' + sLineBreak +
        'Alguns bancos podem n�o aceitar o mesmo n�mero sequencial mais de uma vez!.' + sLineBreak +
        '- Caso voc� ainda n�o enviou este arquivo, desconsidere este aviso.' + sLineBreak +
        '- Caso queira usar um n�mero espec�fico informe o n�mero anterior a ele na configura��o de remessa correspondente.'+ sLineBreak +
        '- Caso queira zerar o n�mero cancele e use o menu do bot�o de lotes deste formul�rio'+ sLineBreak +
        '- Caso queira continuar assim mesmo confirme esta a��o') <> ID_YES then Exit;
      end;
      UCriar.RecriaTabelaLocal('CNAB_Rem', 1);
      //
      Item := 0;
      if DBCheck.CriaFm(TFmCNAB_Rem, FmCNAB_Rem, afmoNegarComAviso) then
      begin
        with FmCNAB_Rem do
        begin
          // Dados para gera��o do arquivo
          // Banco
          FBanco := DmBco.QrCNAB_CfgCedBanco.Value;
          // Lote
          FLote := QrCNAB_LotCodigo.Value;
          // N�mero de remessa do arquivo (Bradesco)
          FSeqArqRem := QrCNAB_LotSeqArq.Value;
          ////////////////// -0- R E G I S T R O    H E A D E R ////////////////////
          //dmkEd_0_010.ValueVariant := 0;
          dmkEd_0_001.ValueVariant := DmBco.QrCNAB_CfgCedBanco.Value;
          dmkEd_0_003.ValueVariant := 1;
          dmkEd_0_004.ValueVariant := 'COBRANCA';
          dmkEd_0_005.ValueVariant := 1;
          dmkEd_0_006.ValueVariant := 'REMESSA';
          dmkEd_0_020.ValueVariant := DmBco.QrCNAB_CfgCedAgencia.Value;
          dmkEd_0_021.ValueVariant := DmBco.QrCNAB_CfgCedConta.Value;
          dmkEd_0_022.ValueVariant := DmBco.QrCNAB_CfgCedDAC_A.Value;
          dmkEd_0_023.ValueVariant := DmBco.QrCNAB_CfgCedDAC_C.Value;
          dmkEd_0_024.ValueVariant := DmBco.QrCNAB_CfgCedDAC_AC.Value;
          dmkEd_0_402.ValueVariant := NomeCedente;
          dmkEd_0_410.ValueVariant := DmBco.QrCNAB_CfgCodEmprBco.Value;
          dmkEd_0_699.ValueVariant := DmBco.QrCNAB_CfgCodOculto.Value;
          dmkEd_0_990.ValueVariant := (*QrProtocoPak*)QrCNAB_LotDataG.Value;
          //
          ///////////////// -1- R E G I S T R O    D E T A L H E ///////////////////

          JurosPerc := DmBco.QrCNAB_CfgJurosPerc.Value;
          MultaPerc := DmBco.QrCNAB_CfgMultaPerc.Value;

          QrCNAB_LotIts.First;
          while not QrCNAB_LotIts.Eof do
          begin
            inc(Item, 1);
            g := Grade_1;
            n := AddLinha(FLinG1, Grade_1);
            //
            JurosValr := Round(QrCNAB_LotItsValor.Value * JurosPerc / 30) / 100;
            (*  Parei Aqui Fazer!!!
            if QrCNAB_LotItsMoraDiaVal.Value > JurosValr then
            JurosValr := QrCNAB_LotItsMoraDiaVal.Value;
            *)
            //
            MultaValr := Round(QrCNAB_LotItsValor.Value * MultaPerc) / 100;
            //
            DataMora  := QrCNAB_LotItsVencto.Value + DmBco.QrCNAB_CfgJurosDias.Value;
            //
            if (DmBco.QrCNAB_CfgCedBanco.Value = 237) and
              (DmBco.QrCNAB_CfgQuemPrint.Value = '1') //Bradesco / Banco imprime
            then
              NossoNumero := 0
            else
              NossoNumero := QrCNAB_LotItsControle.Value;
            //
            AddCampo(g, n, F1_000, '0000'    , Item);
            AddCampo(g, n, F1_011, '0'       , 1);
            AddCampo(g, n, F1_400, '0'       , CedenteTipo);
            AddCampo(g, n, F1_401, 'CNPJ'    , CedenteCNPJ);
            AddCampo(g, n, F1_020, '0000'    , DmBco.QrCNAB_CfgCedAgencia.Value);
            AddCampo(g, n, F1_021, ''        , DmBco.QrCNAB_CfgCedConta.Value);
            AddCampo(g, n, F1_022, ''        , DmBco.QrCNAB_CfgCedDAC_A.Value);
            AddCampo(g, n, F1_023, ''        , DmBco.QrCNAB_CfgCedDAC_C.Value);
            AddCampo(g, n, F1_024, ''        , DmBco.QrCNAB_CfgCedDAC_AC.Value);
            AddCampo(g, n, F1_506, '0'       , QrCNAB_LotItsControle.Value);
            AddCampo(g, n, F1_501, '0'       , NossoNumero);  
            AddCampo(g, n, F1_509, ''        , DmBco.QrCNAB_CfgCartNum.Value);
            AddCampo(g, n, F1_508, ''        , DmBco.QrCNAB_CfgCartCod.Value);
            AddCampo(g, n, F1_504, ''        , '001'); // Minha ocorr�ncia para Remessa (Registro do t�tulo)
            AddCampo(g, n, F1_502, ''        , QrCNAB_LotItsDuplicata.Value);
            AddCampo(g, n, F1_580, 'dd/mm/yy', QrCNAB_LotItsVencto.Value);
            AddCampo(g, n, F1_550, '0.00'    , QrCNAB_LotItsValor.Value);
            AddCampo(g, n, F1_001, '000'     , DmBco.QrCNAB_CfgCedBanco.Value);
            AddCampo(g, n, F1_507, ''        , DmBco.QrCNAB_CfgEspecieTit.Value);
            AddCampo(g, n, F1_520, '0'       , DmBco.QrCNAB_CfgAceiteTit.Value);
            AddCampo(g, n, F1_583, 'dd/mm/yy', QrCNAB_LotItsEmissao.Value);

            //Protesto - C�digo e dias para o banco 237
            //Pois � junto na Instru��o de cobran�a 1 e 2
            if DmBco.QrCNAB_CfgCedBanco.Value = 237 then
            begin
              if (Length(DmBco.QrCNAB_CfgInstrCobr1.Value) = 0) and
                (Length(DmBco.QrCNAB_CfgInstrCobr2.Value) = 0) then
              begin
                AddCampo(g, n, F1_701, ''        , DmBco.QrCNAB_CfgProtesCod.Value);
                AddCampo(g, n, F1_702, ''        , DmBco.QrCNAB_CfgProtesDds.Value);
              end else
              begin
                AddCampo(g, n, F1_701, ''        , DmBco.QrCNAB_CfgInstrCobr1.Value);
                AddCampo(g, n, F1_702, ''        , DmBco.QrCNAB_CfgInstrCobr2.Value);
              end;
            end;
            AddCampo(g, n, F1_950, '00'      , DmBco.QrCNAB_CfgInstrDias.Value);
            AddCampo(g, n, F1_572, '0.00'    , JurosValr);
            AddCampo(g, n, F1_573, '0.00'    , MultaValr);
            AddCampo(g, n, F1_574, '0.00'    , JurosPerc);
            AddCampo(g, n, F1_575, '0.00'    , MultaPerc);
            AddCampo(g, n, F1_576, '0'       , DmBco.QrCNAB_CfgJurosTipo.Value);
            AddCampo(g, n, F1_577, '0'       , DmBco.QrCNAB_CfgMultaTipo.Value);
            //
{
            DmBco.QrAddr3.Close;
            DmBco.QrAddr3.Params[0].AsInteger := QrCNAB_LotItsCliente.Value;
            DmBco.QrAddr3.Open;
}            
            CEPSacado_ := Geral.SoNumero_TT(QrCNAB_LotItsSACADO_CEP_TXT.Value);
            //
            AddCampo(g, n, F1_801, '0'       , QrCNAB_LotItsSACADO_TIPO.Value);    
            AddCampo(g, n, F1_802, 'CNPJ'    , QrCNAB_LotItsSACADO_CNPJ.Value);
            AddCampo(g, n, F1_803, ''        , QrCNAB_LotItsSACADO_NOME.Value);
            AddCampo(g, n, F1_811, ''        , {DmBco.QrAddr3NOMELOGRAD.Value)}'');
            AddCampo(g, n, F1_812, ''        , QrCNAB_LotItsSACADO_RUA.Value);
            AddCampo(g, n, F1_813, ''        , QrCNAB_LotItsSACADO_NUMERO_TXT.Value);
            AddCampo(g, n, F1_814, ''        , QrCNAB_LotItsSACADO_COMPL.Value);
            AddCampo(g, n, F1_805, ''        , QrCNAB_LotItsSACADO_BAIRRO.Value);
            AddCampo(g, n, F1_806, ''        , CEPSacado_);
            AddCampo(g, n, F1_807, ''        , QrCNAB_LotItsSACADO_CIDADE.Value);
            AddCampo(g, n, F1_808, ''        , QrCNAB_LotItsSACADO_xUF.Value);
            AddCampo(g, n, F1_853, ''        , NomeSacador);
            AddCampo(g, n, F1_584, 'dd/mm/yy', DataMora);
            AddCampo(g, n, F1_621, '0'       , DmBco.QrCNAB_CfgQuemPrint.Value);
            AddCampo(g, n, F1_639, '0'       , DmBco.QrCNAB_Cfg_237Mens1.Value);
            //
            //Obter os dados do sacador avalista para o banco 237
            //Pois � junto na mensagem 2
            if DmBco.QrCNAB_CfgCedBanco.Value = 237 then
            begin
              if Length(DmBco.QrCNAB_Cfg_237Mens2.Value) = 0 then
              begin
                if Length(QrCNAB_LotItsDOCUMCLI.Value) = 14 then
                begin
                  AVALISTA := Geral.SoNumero_TT(QrCNAB_LotItsDOCUMCLI.Value);
                  AVALISTA := FormatFloat('000000000000000', Geral.DMV(AVALISTA));
                end else
                if Length(QrCNAB_LotItsDOCUMCLI.Value) = 11 then
                begin
                  AVALISTA := Copy(QrCNAB_LotItsDOCUMCLI.Value, 10, 2);
                  AVALISTA := AVALISTA + '0000';
                  AVALISTA := AVALISTA + Copy(QrCNAB_LotItsDOCUMCLI.Value, 1, 9);
                end else
                  AVALISTA := '';
                //
                if Length(AVALISTA) > 0 then
                begin
                  AVALISTA := AVALISTA + '  ';
                  if Length(QrCNAB_LotItsNOMECLI.Value) > 43 then
                    AVALISTA := AVALISTA + Copy(QrCNAB_LotItsNOMECLI.Value, 1, 43)
                  else
                    AVALISTA := AVALISTA + QrCNAB_LotItsNOMECLI.Value;
                end;
                AddCampo(g, n, F1_640, '0'       , AVALISTA);
              end else
                AddCampo(g, n, F1_640, '0'       , DmBco.QrCNAB_Cfg_237Mens2.Value);
            end else
              AddCampo(g, n, F1_640, '0'       , DmBco.QrCNAB_Cfg_237Mens2.Value);

            //  I N F O R M A T I V O S
            // Endere�o completo do logradouro

            //O banco 237 coloca a cidade e UF junto no endere�o
            if DmBco.QrCNAB_CfgCedBanco.Value = 237 then
              AddCampo(g, n, F1_804, ''        , EnderecoSacado() + ' ' +
                QrCNAB_LotItsSACADO_CIDADE.Value + ' ' + QrCNAB_LotItsSACADO_xUF.Value)
            else
              AddCampo(g, n, F1_804, ''        , EnderecoSacado());

            //  N � O   I M P L E M E N T A D O S
            // Data limite para desconto (n�o tem)
            AddCampo(g, n, F1_586, ''        , '00/00/00');
            // Valor do desconto
            AddCampo(g, n, F1_552, ''        , 0);
            // Valor do IOF
            AddCampo(g, n, F1_569, ''        , 0);
            // Valor do abatimento
            AddCampo(g, n, F1_551, ''        , 0);
            // Valor do deconto bonifica��o
            AddCampo(g, n, F1_558, ''        , 0);


        //
        ///////////////// -5- R E G I S T R O    D E T A L H E ///////////////////
            if ((DmBco.QrCNAB_CfgEnvEmeio.Value = 1) and
            (DmBco.QrAddr3EMail.Value <> '')) or
            (QrCNAB_LotItsCliente.Value <> 0) then
            begin
              // � o mesmo do registro 1
              //inc(Item, 1);
              g := Grade_5;
              n := AddLinha(FLinG5, Grade_5);
              AddCampo(g, n, F5_000, '0000' , Item);
              AddCampo(g, n, F5_011, '0'      , 5);
              //AddCampo(g, n, F5_000, '000000' , Linha);
              //AddCampo(g, n, F5_011, '0'      , 5);
              AddCampo(g, n, F5_815, ''       , DmBco.QrAddr3EMail.Value);
              //
              DmBco.QrAddr3.Close;
              if QrCNAB_LotItsCliente.Value <> 0 then
              begin
                DmBco.QrAddr3.Params[0].AsInteger := QrCNAB_LotItsCliente.Value;
                DmBco.QrAddr3.Open;

                CEPSacador       := Geral.SoNumero_TT(DmBco.QrAddr3ECEP_TXT.Value);
                Addr3_Tipo       := DmBco.QrAddr3Tipo.Value;
                Addr3_CNPJ       := DmBco.QrAddr3CNPJ_CPF.Value;
                Addr3_NOMEENT    := DmBco.QrAddr3NOMEENT.Value;
                Addr3_NOMELOGRAD := DmBco.QrAddr3NOMELOGRAD.Value;
                Addr3_RUA        := DmBco.QrAddr3RUA.Value;
                Addr3_NUMERO_TXT := DmBco.QrAddr3NUMERO_TXT.Value;
                Addr3_COMPL      := DmBco.QrAddr3COMPL.Value;
                Addr3_BAIRRO     := DmBco.QrAddr3BAIRRO.Value;
                Addr3_CIDADE     := DmBco.QrAddr3CIDADE.Value;
                Addr3_NOMEUF     := DmBco.QrAddr3NOMEUF.Value;
              end else begin
                CEPSacador       := '';
                Addr3_Tipo       := -1;
                Addr3_CNPJ       := '';
                Addr3_NOMEENT    := '';
                Addr3_NOMELOGRAD := '';
                Addr3_RUA        := '';
                Addr3_NUMERO_TXT := '';
                Addr3_COMPL      := '';
                Addr3_BAIRRO     := '';
                Addr3_CIDADE     := '';
                Addr3_NOMEUF     := '';
              end;
              //
              AddCampo(g, n, F5_851, '0'      , Addr3_Tipo);
              AddCampo(g, n, F5_852, 'CNPJ'   , Addr3_CNPJ);
              AddCampo(g, n, F5_861, ''       , Addr3_NOMELOGRAD);
              AddCampo(g, n, F5_862, ''       , Addr3_RUA);
              AddCampo(g, n, F5_863, ''       , Addr3_NUMERO_TXT);
              AddCampo(g, n, F5_864, ''       , Addr3_COMPL);
              AddCampo(g, n, F5_855, ''       , Addr3_BAIRRO);
              AddCampo(g, n, F5_856, ''       , CEPSacador);
              AddCampo(g, n, F5_857, ''       , Addr3_CIDADE);
              AddCampo(g, n, F5_858, ''       , Addr3_NOMEUF);
              //
              //  I N F O R M A T I V O S
              // Endere�o completo do logradouro
              AddCampo(g, n, F5_854, ''        , Endereco());
            end;
            (*QrProtPakIts*)QrCNAB_LotIts.Next;
          end;
          (*
          end;
          *)
          (* Parei Aqui
          HoraF := Now();
          dmkEdHoraF.ValueVariant := HoraF;
          dmkEdTempo.ValueVariant := (HoraF - HoraI) * 24 * 60 * 60;
          *)
          Screen.Cursor := crDefault;
          ShowModal;
          Destroy;
          // Parei Aqui Fazer!!!
          //ReopenProtocoPak((*QrProtocoPak*)QrCNAB_LotControle.Value);
        end;
      end;
    end;
  end;
  Screen.Cursor := crDefault;
end;

function TFmCNAB_Lot.ObtemCaminhoArquivo(): String;
begin
  case DmBco.QrCNAB_CfgCNAB.Value of
    240:
    begin
      (*
      Result := dmkPF.CaminhoArquivo(DmBco.QrCNAB_CfgDiretorio.Value,
                  FormatFloat('0000000', Geral.IMV(SeqArq)), 'Rem');
      *)
    end;
    400:
    begin

    end;
    else
      Result := '';
  end;
end;

procedure TFmCNAB_Lot.FormCreate(Sender: TObject);
begin
  FSeq := 0;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  DBGrid1.Align     := alClient;
  DBGrid2.Align     := alClient;
  CriaOForm;
  QrCNAB_Cfg.Open;
end;

procedure TFmCNAB_Lot.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCNAB_LotCodigo.Value,LaRegistro.Caption);
end;

procedure TFmCNAB_Lot.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCNAB_Lot.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  VAR_MARCA := QrCNAB_LotCodigo.Value;
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmCNAB_Lot.QrCNAB_LotAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCNAB_Lot.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'CNAB_Lot', 'Livres', 99) then
  //BtInclui.Enabled := False;
  if FSeq = 1 then MostraEdicao(1, stIns, 0);
end;

procedure TFmCNAB_Lot.QrCNAB_LotAfterScroll(DataSet: TDataSet);
begin
  ReopenCNAB_LotIts(0);
  if QrCNAB_LotCodigo.Value < 1 then
    BtGera.Enabled := False
  else
    BtGera.Enabled := True;
end;

procedure TFmCNAB_Lot.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCNAB_LotCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'CNAB_Lot', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCNAB_Lot.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmCNAB_Lot.QrCNAB_LotBeforeOpen(DataSet: TDataSet);
begin
  QrCNAB_LotCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCNAB_Lot.ReopenCNAB_LotIts(Controle: Integer);
begin
  QrCNAB_LotIts.Close;
  QrCNAB_LotIts.Params[0].AsInteger := QrCNAB_LotCodigo.Value;
  QrCNAB_LotIts.Open;
  //
  if Controle > 0 then QrCNAB_LotIts.Locate('Controle', Controle, []);
end;

procedure TFmCNAB_Lot.PMLotesPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab  := (QrCNAB_Lot.State <> dsInactive) and (QrCNAB_Lot.RecordCount > 0);
  //
  Alteraloteatual1.Enabled := Enab;
  Excluiloteatual1.Enabled := Enab;
end;

procedure TFmCNAB_Lot.PMTitulosPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrCNAB_Lot.State <> dsInactive) and (QrCNAB_Lot.RecordCount > 0);
  Enab2 := (QrCNAB_LotIts.State <> dsInactive) and (QrCNAB_LotIts.RecordCount > 0);
  //
  Inclui1.Enabled            := Enab;
  Instruesparabanco1.Enabled := False;
  Retira1.Enabled            := Enab and Enab2;
end;

procedure TFmCNAB_Lot.Crianovolote1Click(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmCNAB_Lot.Alteraloteatual1Click(Sender: TObject);
var
  CNAB_Lot : Integer;
begin
  CNAB_Lot := QrCNAB_LotCodigo.Value;
  if not UMyMod.SelLockY(CNAB_Lot, Dmod.MyDB, 'CNAB_Lot', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(CNAB_Lot, Dmod.MyDB, 'CNAB_Lot', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmCNAB_Lot.Excluiloteatual1Click(Sender: TObject);
var
  Atual, Proximo: Integer;
begin
  if(QrCNAB_LotIts.State = dsInactive) or (QrCNAB_LotIts.RecordCount = 0) then
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o deste lote?') = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      try
        Atual   := QrCNAB_LotCodigo.Value;
        Proximo := UMyMod.ProximoRegistro(QrCNAB_Lot, 'Codigo', Atual);
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM cnab_lot WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := Atual;
        Dmod.QrUpd.ExecSQL;
        //
        LocCod(Atual, Proximo);
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end else
    Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak +
      'Motivo: Este lote possui itens!');
end;

procedure TFmCNAB_Lot.BtTitulosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTitulos, BtTitulos);
end;

procedure TFmCNAB_Lot.Inclui1Click(Sender: TObject);
begin
  MostraEdicao(2, stIns, 0);
end;

procedure TFmCNAB_Lot.ReopenTitulos(Controle: Integer);
begin
  QrTitulos.Close;
  QrTitulos.Open;
  //
  if Controle > 0 then QrTitulos.Locate('Controle', Controle, []);
end;

procedure TFmCNAB_Lot.QrCNAB_LotCalcFields(DataSet: TDataSet);
begin
  if QrCNAB_LotDataG.Value = 0 then
    QrCNAB_LotMyDataG.Value := '00/00/0000'
  else QrCNAB_LotMyDataG.Value := Geral.FDT(QrCNAB_LotDataG.Value, 2);
  if QrCNAB_LotDataS.Value = 0 then
    QrCNAB_LotMyDataS.Value := '00/00/0000'
  else QrCNAB_LotMyDataS.Value := Geral.FDT(QrCNAB_LotDataS.Value, 2);
end;

procedure TFmCNAB_Lot.BitBtn2Click(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmCNAB_Lot.BitBtn1Click(Sender: TObject);
var
  i: integer;
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1, CNAB_Lot=:P0 WHERE Controle=:P1');
  if DBGrid2.SelectedRows.Count > 0 then
  begin
    if Geral.MB_Pergunta('Confirma a adi��o dos itens selecionados?') = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      try
        with DBGrid2.DataSource.DataSet do
        for i:= 0 to DBGrid2.SelectedRows.Count-1 do
        begin
          GotoBookmark(pointer(DBGrid2.SelectedRows.Items[i]));
          AdicionaItem;
        end;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end else
    if Geral.MB_Pergunta('Confirma a adi��o do item selecionado?') = ID_YES then
      AdicionaItem;
  CalculaLote(QrCNAB_LotCodigo.Value);
  LocCod(QrCNAB_LotCodigo.Value, QrCNAB_LotCodigo.Value);
  ReopenTitulos(0);
end;

procedure TFmCNAB_Lot.AdicionaItem;
begin
  Dmod.QrUpd.Params[0].AsInteger := QrCNAB_LotCodigo.Value;
  Dmod.QrUpd.Params[1].AsInteger := QrTitulosControle.Value;
  Dmod.QrUpd.ExecSQL;
end;

procedure TFmCNAB_Lot.CalculaLote;
begin
  (*Dmod.QrUpd.Params[0].AsInteger := QrCNAB_LotCodigo.Value;
  Dmod.QrUpd.Params[1].AsInteger := QrTitulosControle.Value;
  Dmod.QrUpd.ExecSQL;*)
end;

{function TFmCNAB_Lot.AjustaString(Texto, Compl: String; Tamanho: Integer;
  Alinhamento: dceAlinha): String;
var
  Txt: String;
  Direita: Boolean;
begin
  Direita := True;
  Texto := Geral.SemAcento(Texto);
  Texto := Geral.Maiusculas(Texto, False);
  Txt := CompletaString(Texto, Compl, Tamanho, Alinhamento);
  while Length(Txt) > Tamanho do
  begin
    case Alinhamento of
      posEsquerda: Txt := Copy(Txt, 1, Length(Txt)-1);
      posCentro  :
      begin
        if Direita then
          Txt := Copy(Txt, 2, Length(Txt)-1)
        else
          Txt := Copy(Txt, 1, Length(Txt)-1);
        Direita := not Direita;
      end;
      posDireita: Txt := Copy(Txt, 2, Length(Txt)-1);
    end;
  end;
  Result := Txt;
end;}

{function TFmCNAB_Lot.CompletaString(Texto, Compl: String; Tamanho: Integer;
  Alinhamento: dceAlinha): String;
var
  Txt: String;
  Direita: Boolean;
begin
  Direita := True;
  Txt := Texto;
  while Length(Txt) < Tamanho do
  begin
    case Alinhamento of
      posEsquerda: Txt := Txt + Compl;
      posCentro  :
      begin
        if Direita then
          Txt := Txt + Compl
        else
          Txt := Compl + Txt;
        Direita := not Direita;
      end;
      posDireita:  Txt := Compl + Txt;
    end;
  end;
  Result := Txt;
end;}

procedure TFmCNAB_Lot.Retira1Click(Sender: TObject);
var
  Atual, Proximo: Integer;
begin
  Atual := QrCNAB_LotItsControle.Value;
  Proximo := UMyMod.ProximoRegistro(QrCNAB_LotIts, 'Controle', Atual);
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1, CNAB_Lot=0 WHERE Controle=:P0');
  Dmod.QrUpd.Params[0].AsInteger := Atual;
  Dmod.QrUpd.ExecSQL;
  //
  ReopenCNAB_LotIts(Proximo);
end;

procedure TFmCNAB_Lot.Memo1Change(Sender: TObject);
begin
  Edit1.Text := Geral.FF0(Memo1.Lines.Count);
end;

{procedure TFmCNAB_Lot.AdicionaAoMemo(Memo: TMemo; Texto: String;
  MesmoSeTextoVazio: Boolean; Tamanho: Integer);
var
  Leng: Integer;
begin
  if (MesmoSeTextoVazio = False) and (Trim(Texto) = '') then Exit;
  Memo.Lines.Add(Texto);
  Leng := Length(Texto);
  if Leng <> Tamanho then
    Geral.MB_Aviso('Erro. A linha ' + Geral.FF0(Memo.Lines.Count)+
    ' tem ' + Geral.FF0(Length(Texto))+' caracteres, quando deveria ter '+
    Geral.FF0(Tamanho)+'!');
end;}

{procedure TFmCNAB_Lot.VerificaSomaArray(MaxS: Integer; Tam: array of Integer);
var
  c, i: Integer;
begin
  c := 0;
  for i := 0 to MaxS - 1 do
    c := c + Tam[i];
  if c <> FTamCNAB then
    Geral.MB_Erro('Configura��o de array de segmentos inv�lida!');
end;}

{procedure TFmCNAB_Lot.VerificaTamanhoTxts(Txt: array of String;
  Tam: array of Integer; MaxS: Integer);
var
  i: Integer;
begin
  for i := 0 to MaxS - 1 do
  begin
    if Length(Txt[i]) <> Tam[i] then
      Geral.MB_Erro('O segmento '+Geral.FF0(i+1)+' deveria ter '+
      Geral.FF0(Tam[i])+ ' caracteres, mas possui '+Geral.FF0(Length(Txt[i]))+'!'));
  end;
end;}

procedure TFmCNAB_Lot.QrCNAB_LotItsCalcFields(DataSet: TDataSet);
var
  Txt: String;
begin
  Txt := QrCNAB_LotItsRua.Value + ', ';
  if QrCNAB_LotItsNumero.Value = 0 then Txt := Txt + 's/n' else
  Txt := Txt + Geral.SoNumero_TT(FloatToStr(QrCNAB_LotItsNumero.Value));
  Txt := Txt + ' ' + QrCNAB_LotItsCompl.Value;
  //
  QrCNAB_LotItsENDERECO_EMI.Value := Txt;
  //
  if QrCNAB_LotItsSACADO_NUMERO.Value < 1 then
    QrCNAB_LotItsSACADO_NUMERO_TXT.Value := 'S/N'
  else
    QrCNAB_LotItsSACADO_NUMERO_TXT.Value := Geral.SoNumero_TT(FloatToStr(QrCNAB_LotItsSACADO_NUMERO.Value));
  QrCNAB_LotItsSACADO_CEP_TXT.Value :=Geral.FormataCEP_NT(QrCNAB_LotItsSACADO_CEP.Value);
end;

procedure TFmCNAB_Lot.Instruesparabanco1Click(Sender: TObject);
begin
  {
  Application.CreateForm(TFmCNAB_Lot_Sel, FmCNAB_Lot_Sel);
  FmCNAB_Lot_Sel.FCNAB_Cfg := QrCNAB_LotCNAB_Cfg.Value;
  FmCNAB_Lot_Sel.ReabrirTabelas;
  FmCNAB_Lot_Sel.ShowModal;
  FmCNAB_Lot_Sel.Destroy;
  }
end;

end.

