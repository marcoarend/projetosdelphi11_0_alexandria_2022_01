object FmContasConfCad: TFmContasConfCad
  Left = 339
  Top = 185
  Caption = 'FIN-PLCTA-029 :: Contas Mensais - Itens de Confer'#234'ncia'
  ClientHeight = 494
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Contas Mensais - Itens de Confer'#234'ncia'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 700
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 701
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 708
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 446
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 444
      Align = alClient
      TabOrder = 0
      object PnEdita: TPanel
        Left = 1
        Top = 328
        Width = 780
        Height = 115
        Align = alBottom
        TabOrder = 0
        Visible = False
        object Label1: TLabel
          Left = 8
          Top = 4
          Width = 117
          Height = 13
          Caption = 'Conta (Plano de contas):'
        end
        object Label2: TLabel
          Left = 436
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Qtde. min.:'
        end
        object Label3: TLabel
          Left = 520
          Top = 4
          Width = 54
          Height = 13
          Caption = 'Qtde. max.:'
        end
        object Label4: TLabel
          Left = 688
          Top = 4
          Width = 52
          Height = 13
          Caption = 'Valor max.:'
        end
        object Label5: TLabel
          Left = 604
          Top = 4
          Width = 49
          Height = 13
          Caption = 'Valor min.:'
        end
        object PnConfirma: TPanel
          Left = 1
          Top = 66
          Width = 778
          Height = 48
          Align = alBottom
          TabOrder = 7
          object BitBtn1: TBitBtn
            Tag = 14
            Left = 20
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Confirma'
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BitBtn1Click
          end
          object Panel6: TPanel
            Left = 666
            Top = 1
            Width = 111
            Height = 46
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
            object BitBtn2: TBitBtn
              Tag = 15
              Left = 2
              Top = 3
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Hint = 'Sai da janela atual'
              Caption = '&Desiste'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BitBtn2Click
            end
          end
        end
        object EdConta: TdmkEditCB
          Left = 8
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = CBConta
          IgnoraDBLookupComboBox = False
        end
        object CBConta: TdmkDBLookupComboBox
          Left = 64
          Top = 20
          Width = 369
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsContas
          TabOrder = 1
          dmkEditCB = EdConta
          QryCampo = 'Codigo'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdQtdeMin: TdmkEdit
          Left = 436
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'QtdeMin'
          UpdCampo = 'QtdeMin'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object EdQtdeMax: TdmkEdit
          Left = 520
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'QtdeMax'
          UpdCampo = 'QtdeMax'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object EdValrMin: TdmkEdit
          Left = 604
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ValrMin'
          UpdCampo = 'ValrMin'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object EdValrMax: TdmkEdit
          Left = 688
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ValrMax'
          UpdCampo = 'ValrMax'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object CkContinuar: TCheckBox
          Left = 8
          Top = 44
          Width = 117
          Height = 17
          Caption = 'Continuar inserindo.'
          Checked = True
          State = cbChecked
          TabOrder = 6
        end
      end
      object PnNomeCond: TPanel
        Left = 1
        Top = 1
        Width = 780
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        Caption = 'Nome do Cliente Interno'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 4227327
        Font.Height = -19
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
      object dmkDBGrid1: TdmkDBGrid
        Left = 1
        Top = 42
        Width = 780
        Height = 238
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Conta'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECONTA'
            Title.Caption = 'Descri'#231#227'o da conta (Plano de contas)'
            Width = 418
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QtdeMin'
            Title.Caption = 'Qtde. min.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QtdeMax'
            Title.Caption = 'Qtde. max.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValrMin'
            Title.Caption = 'Valor min.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValrMax'
            Title.Caption = 'Valor max.'
            Visible = True
          end>
        Color = clWindow
        DataSource = DsConfPgtos
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Conta'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECONTA'
            Title.Caption = 'Descri'#231#227'o da conta (Plano de contas)'
            Width = 418
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QtdeMin'
            Title.Caption = 'Qtde. min.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QtdeMax'
            Title.Caption = 'Qtde. max.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValrMin'
            Title.Caption = 'Valor min.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValrMax'
            Title.Caption = 'Valor max.'
            Visible = True
          end>
      end
      object PnControle: TPanel
        Left = 1
        Top = 280
        Width = 780
        Height = 48
        Align = alBottom
        TabOrder = 3
        object BtInclui: TBitBtn
          Tag = 10
          Left = 20
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Inclui'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 668
          Top = 1
          Width = 111
          Height = 46
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtSaida: TBitBtn
            Tag = 13
            Left = 2
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 112
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Altera'
          NumGlyphs = 2
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 204
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Exclui'
          NumGlyphs = 2
          TabOrder = 3
        end
      end
    end
  end
  object QrConfPgtos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cp.*, co.Nome NOMECONTA '
      'FROM confpgtos cp'#13
      'LEFT JOIN contas co ON co.Codigo=cp.Codigo'#10
      'WHERE CliInt=:P0'
      '')
    Left = 452
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConfPgtosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrConfPgtosNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrConfPgtosQtdeMin: TIntegerField
      FieldName = 'QtdeMin'
      Required = True
    end
    object QrConfPgtosQtdeMax: TIntegerField
      FieldName = 'QtdeMax'
      Required = True
    end
    object QrConfPgtosValrMin: TFloatField
      FieldName = 'ValrMin'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrConfPgtosValrMax: TFloatField
      FieldName = 'ValrMax'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsConfPgtos: TDataSource
    DataSet = QrConfPgtos
    Left = 480
    Top = 128
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE NOT Codigo IN ('
      '  SELECT Codigo FROM confpgtos'
      '  WHERE CliInt=:P0'
      ')'
      'ORDER BY Nome')
    Left = 152
    Top = 212
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 180
    Top = 212
  end
end
