unit Plano;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, dmkPermissoes, dmkGeral, dmkEdit, UnDmkProcFunc;

type
  TFmPlano = class(TForm)
    PainelDados: TPanel;
    DsPlano: TDataSource;
    QrPlano: TmySQLQuery;
    PainelTitulo: Tpanel;
    LaTipo: TLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TEdit;
    Label10: TLabel;
    DBGrid1: TDBGrid;
    TbConjuntos: TmySQLTable;
    DsConjuntos: TDataSource;
    Label4: TLabel;
    EdOrdemLista: TdmkEdit;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrPlanoCodigo: TIntegerField;
    QrPlanoNome: TWideStringField;
    QrPlanoLk: TIntegerField;
    QrPlanoDataCad: TDateField;
    QrPlanoDataAlt: TDateField;
    QrPlanoUserCad: TIntegerField;
    QrPlanoUserAlt: TIntegerField;
    QrPlanoCtrlaSdo: TSmallintField;
    QrPlanoOrdemLista: TIntegerField;
    TbConjuntosCodigo: TIntegerField;
    TbConjuntosNome: TWideStringField;
    TbConjuntosPlano: TIntegerField;
    TbConjuntosLk: TIntegerField;
    TbConjuntosDataCad: TDateField;
    TbConjuntosDataAlt: TDateField;
    TbConjuntosUserCad: TIntegerField;
    TbConjuntosUserAlt: TIntegerField;
    TbConjuntosOrdemLista: TIntegerField;
    TbConjuntosCtrlaSdo: TSmallintField;
    PainelData: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit1: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    CkCtrlaSdo: TCheckBox;
    dmkPermissoes1: TdmkPermissoes;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPlanoAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrPlanoAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPlanoBeforeOpen(DataSet: TDataSet);
    procedure TbConjuntosDeleting(Sender: TObject; var Allow: Boolean);
  private
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
//    procedure IncluiSubRegistro;
//    procedure ExcluiSubRegistro;
//    procedure AlteraSubRegistro;
//    procedure TravaOForm;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmPlano: TFmPlano;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPlano.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPlano.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPlanoCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPlano.DefParams;
begin
  VAR_GOTOTABELA := 'Plano';
  VAR_GOTOMYSQLTABLE := QrPlano;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;



  VAR_SQLx.Add('SELECT pl.*');
  VAR_SQLx.Add('FROM plano pl');
  //
  VAR_SQL1.Add('WHERE pl.Codigo=:P0');
  //
  VAR_SQLa.Add('WHERE pl.Nome Like :P0');
  //
end;

procedure TFmPlano.MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    PainelControle.Visible:=False;
    if Status = CO_INCLUSAO then
    begin
    EdNome.Text := CO_VAZIO;
      EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
      EdOrdemLista.Text := '';
      CkCtrlaSdo.Checked := True;
    end else begin
      EdCodigo.Text := DBEdCodigo.Text;
      EdNome.Text := DBEdNome.Text;
      EdOrdemLista.Text := IntToStr(QrPlanoOrdemLista.Value);;
      CkCtrlaSdo.Checked := Geral.IntToBool_0(QrPlanoCtrlaSdo.Value);
    end;
    EdNome.SetFocus;
  end else begin
    PainelControle.Visible:=True;
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmPlano.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPlano.AlteraRegistro;
var
  Plano : Integer;
begin
  Plano := QrPlanoCodigo.Value;
  if QrPlanoCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(Plano, Dmod.MyDB, 'Plano', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Plano, Dmod.MyDB, 'Plano', 'Codigo');
      MostraEdicao(True, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmPlano.IncluiRegistro;
var
  Cursor : TCursor;
  Plano : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Plano := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Plano', 'Plano', 'Codigo');
    if Length(FormatFloat(FFormatFloat, Plano))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(True, CO_INCLUSAO, Plano);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmPlano.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPlano.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPlano.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPlano.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPlano.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPlano.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPlano.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmPlano.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmPlano.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPlanoCodigo.Value;
  Close;
end;

procedure TFmPlano.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := Geral.IMV(EdCodigo.Text);
  Dmod.QrUpdU.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('INSERT INTO plano SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE plano SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0, OrdemLista=:P1, CtrlaSdo=:P2, ');
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := Geral.IMV(EdOrdemLista.Text);
  Dmod.QrUpdU.Params[02].AsInteger := MLAGeral.BoolToInt(CkCtrlaSdo.Checked);
  //
  Dmod.QrUpdU.Params[03].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[04].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[05].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Plano', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmPlano.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Plano', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Plano', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Plano', 'Codigo');
end;

procedure TFmPlano.FormCreate(Sender: TObject);
begin
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  DBGrid1.Align     := alClient;
  CriaOForm;
  TbConjuntos.Open;
end;

procedure TFmPlano.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPlanoCodigo.Value,LaRegistro.Caption);
end;

procedure TFmPlano.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPlano.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmPlano.QrPlanoAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPlano.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Plano', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmPlano.QrPlanoAfterScroll(DataSet: TDataSet);
begin
  TbConjuntos.Filter := 'Plano='+IntToStr(QrPlanoCodigo.Value);
end;

procedure TFmPlano.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPlanoCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Plano', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPlano.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmPlano.QrPlanoBeforeOpen(DataSet: TDataSet);
begin
  QrPlanoCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPlano.TbConjuntosDeleting(Sender: TObject;
  var Allow: Boolean);
begin
  Allow := False;
  Application.MessageBox('Para excluir sub-grupo de conta acesse o cadastro de sub-Plano!',
  'Informa��o', MB_OK+MB_ICONINFORMATION);
end;

end.

