object FmContasLnk: TFmContasLnk
  Left = 368
  Top = 175
  VertScrollBar.Visible = False
  Caption = 'FIN-PLCTA-023 :: Links de Consolida'#231#227'o Banc'#225'ria'
  ClientHeight = 548
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 500
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 196
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 8
        Width = 42
        Height = 13
        Caption = 'Controle:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 8
        Width = 108
        Height = 13
        Caption = 'Hist'#243'rico (M'#225'scara: %):'
      end
      object Label3: TLabel
        Left = 276
        Top = 8
        Width = 97
        Height = 13
        Caption = 'Texto  (M'#225'scara: %):'
      end
      object Label4: TLabel
        Left = 8
        Top = 88
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
        FocusControl = DBEdit3
      end
      object Label5: TLabel
        Left = 504
        Top = 8
        Width = 34
        Height = 13
        Caption = 'Banco:'
        FocusControl = DBEdit4
      end
      object Label6: TLabel
        Left = 8
        Top = 48
        Width = 309
        Height = 13
        Caption = 
          'Conta '#250'nica ou referencial para v'#225'rias contas:   (Plano de conta' +
          's)'
        FocusControl = DBEdit8
      end
      object Label7: TLabel
        Left = 404
        Top = 8
        Width = 24
        Height = 13
        Caption = 'Tipo:'
      end
      object Label8: TLabel
        Left = 504
        Top = 48
        Width = 57
        Height = 13
        Caption = 'Fornecedor:'
        FocusControl = DBEdit11
      end
      object Label9: TLabel
        Left = 504
        Top = 88
        Width = 35
        Height = 13
        Caption = 'Cliente:'
        FocusControl = DBEdit13
      end
      object Label10: TLabel
        Left = 452
        Top = 174
        Width = 462
        Height = 13
        Caption = 
          'DICA: Copie diretamente de arquivos '#39'excel'#39' o "hist'#243'rico" e o "d' +
          'ocumento" quando for necess'#225'rio!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object DBEdCodigo: TDBEdit
        Left = 8
        Top = 24
        Width = 64
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Controle'
        DataSource = DsContasLnk
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdit3: TDBEdit
        Left = 76
        Top = 104
        Width = 425
        Height = 21
        DataField = 'NOMECLIINT'
        DataSource = DsContasLnk
        TabOrder = 1
      end
      object DBEdit4: TDBEdit
        Left = 572
        Top = 24
        Width = 421
        Height = 21
        DataField = 'NOMEBANCO'
        DataSource = DsContasLnk
        TabOrder = 2
      end
      object DBEdit5: TDBEdit
        Left = 8
        Top = 104
        Width = 64
        Height = 21
        DataField = 'CliInt'
        DataSource = DsContasLnk
        TabOrder = 3
      end
      object DBEdit6: TDBEdit
        Left = 504
        Top = 24
        Width = 64
        Height = 21
        DataField = 'Banco'
        DataSource = DsContasLnk
        TabOrder = 4
      end
      object DBEdit7: TDBEdit
        Left = 8
        Top = 64
        Width = 64
        Height = 21
        DataField = 'Codigo'
        DataSource = DsContasLnk
        TabOrder = 5
      end
      object DBEdit8: TDBEdit
        Left = 76
        Top = 64
        Width = 425
        Height = 21
        DataField = 'NOMECONTA'
        DataSource = DsContasLnk
        TabOrder = 6
      end
      object DBEdit1: TDBEdit
        Left = 76
        Top = 24
        Width = 197
        Height = 21
        DataField = 'Texto'
        DataSource = DsContasLnk
        TabOrder = 7
      end
      object DBEdit2: TDBEdit
        Left = 276
        Top = 24
        Width = 124
        Height = 21
        DataField = 'Doc'
        DataSource = DsContasLnk
        TabOrder = 8
      end
      object DBEdit9: TDBEdit
        Left = 404
        Top = 24
        Width = 97
        Height = 21
        DataField = 'NOMETIPO'
        DataSource = DsContasLnk
        TabOrder = 9
      end
      object DBEdit10: TDBEdit
        Left = 504
        Top = 64
        Width = 64
        Height = 21
        DataField = 'Fornece'
        DataSource = DsContasLnk
        TabOrder = 10
      end
      object DBEdit11: TDBEdit
        Left = 572
        Top = 64
        Width = 421
        Height = 21
        DataField = 'NOMEFORNECE'
        DataSource = DsContasLnk
        TabOrder = 11
      end
      object DBEdit12: TDBEdit
        Left = 504
        Top = 104
        Width = 64
        Height = 21
        DataField = 'Cliente'
        DataSource = DsContasLnk
        TabOrder = 12
      end
      object DBEdit13: TDBEdit
        Left = 572
        Top = 104
        Width = 421
        Height = 21
        DataField = 'NOMECLIENTE'
        DataSource = DsContasLnk
        TabOrder = 13
      end
      object DBCheckBox1: TDBCheckBox
        Left = 8
        Top = 172
        Width = 441
        Height = 17
        Caption = 
          'Usar como fornecedor e/ou cliente, a entidade banc'#225'ria informada' +
          ' no cadastro do banco.'
        DataField = 'UsaEntBank'
        DataSource = DsContasLnk
        TabOrder = 14
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object dmkDBCheckGroup1: TdmkDBCheckGroup
        Left = 8
        Top = 128
        Width = 985
        Height = 41
        Caption = 
          ' Composi'#231#227'o da descri'#231#227'o (Hist'#243'rico) para gera'#231#227'o do lan'#231'amento:' +
          ' '
        Columns = 3
        DataField = 'ComposHist'
        DataSource = DsContasLnk
        Items.Strings = (
          'Descri'#231#227'o do extrato'
          'Descri'#231#227'o da conta (Plano de contas)'
          'Nome do Terceiro (Forneceor / Cliente)')
        ParentBackground = False
        TabOrder = 15
      end
    end
    object PainelControle: TPanel
      Left = 1
      Top = 451
      Width = 1006
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'ltimo registro'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Pr'#243'ximo registro'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Registro anterior'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Primeiro registro'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 320
        Top = 1
        Width = 685
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtAtiva: TBitBtn
          Tag = 10008
          Left = 188
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Exclui banco atual'
          Caption = 'Ati&var'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Visible = False
          OnClick = BtAtivaClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtIncluiClick
        end
        object Panel1: TPanel
          Left = 584
          Top = 0
          Width = 101
          Height = 46
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtMulti: TBitBtn
          Tag = 10056
          Left = 310
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Multi'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtMultiClick
        end
      end
    end
    object DBGItens: TDBGrid
      Left = 1
      Top = 197
      Width = 1006
      Height = 254
      Align = alClient
      DataSource = DsContasLnkIts
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMECONTA'
          Title.Caption = 'Conta'
          Width = 217
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Texto'
          Title.Caption = 'Texto complementar'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMETIPOCALC'
          Title.Caption = 'Forma'
          Width = 78
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Fator'
          Title.Caption = '$ / %'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMERELACIONADO'
          Title.Caption = 'Fornecedor / Cliente'
          Width = 209
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ComposHist'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Considera'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UsaEntBank'
          Visible = True
        end>
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = '                  Links de Consolida'#231#227'o Banc'#225'ria'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 925
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 555
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 136
      Top = 1
      Width = 789
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 137
      ExplicitTop = 2
      ExplicitWidth = 418
      ExplicitHeight = 44
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 135
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbNumero: TBitBtn
        Tag = 7
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        Visible = False
        OnClick = SbQueryClick
      end
    end
  end
  object DsContasLnk: TDataSource
    DataSet = QrContasLnk
    Left = 224
    Top = 13
  end
  object QrContasLnk: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrContasLnkBeforeOpen
    AfterOpen = QrContasLnkAfterOpen
    BeforeClose = QrContasLnkBeforeClose
    AfterScroll = QrContasLnkAfterScroll
    OnCalcFields = QrContasLnkCalcFields
    SQL.Strings = (
      'SELECT IF(ent.Codigo=0, '#39'** TODOS **'#39', CASE WHEN ent.Tipo=0 '
      'THEN ent.RazaoSocial ELSE ent.Nome END) NOMECLIINT,'
      'IF(cli.Codigo=0, '#39'** V'#193'RIOS **'#39', CASE WHEN cli.Tipo=0 '
      'THEN cli.RazaoSocial ELSE cli.Nome END) NOMECLIENTE,'
      'IF(fnc.Codigo=0, '#39'** V'#193'RIOS **'#39', CASE WHEN fnc.Tipo=0 '
      'THEN fnc.RazaoSocial ELSE fnc.Nome END) NOMEFORNECE,'
      'CASE WHEN con.Codigo=0 THEN '#39'** NENHUMA **'#39' '
      'ELSE Con.Nome END NOMECONTA, '
      'CASE WHEN ban.Codigo=0 THEN '#39'** TODOS **'#39' '
      'ELSE ban.Nome END NOMEBANCO, clk.* '
      'FROM contaslnk clk'
      'LEFT JOIN contas con ON con.Codigo=clk.Codigo'
      'LEFT JOIN bancos ban ON ban.Codigo=clk.Banco'
      'LEFT JOIN entidades ent ON ent.Codigo=clk.CliInt'
      'LEFT JOIN entidades cli ON cli.Codigo=clk.Cliente'
      'LEFT JOIN entidades fnc ON fnc.Codigo=clk.Fornece'
      'WHERE clk.Controle > 0')
    Left = 196
    Top = 13
    object QrContasLnkNOMECLIINT: TWideStringField
      FieldName = 'NOMECLIINT'
      Size = 100
    end
    object QrContasLnkNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrContasLnkNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
    object QrContasLnkCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContasLnkControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrContasLnkTexto: TWideStringField
      FieldName = 'Texto'
      Required = True
      Size = 50
    end
    object QrContasLnkDoc: TWideStringField
      FieldName = 'Doc'
      Required = True
    end
    object QrContasLnkLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContasLnkDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContasLnkDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContasLnkUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrContasLnkUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrContasLnkCliInt: TIntegerField
      FieldName = 'CliInt'
      Required = True
    end
    object QrContasLnkBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
    end
    object QrContasLnkConsidera: TSmallintField
      FieldName = 'Considera'
      Required = True
    end
    object QrContasLnkNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 30
      Calculated = True
    end
    object QrContasLnkCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrContasLnkFornece: TIntegerField
      FieldName = 'Fornece'
      Required = True
    end
    object QrContasLnkNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrContasLnkNOMEFORNECE: TWideStringField
      FieldName = 'NOMEFORNECE'
      Size = 100
    end
    object QrContasLnkUsaEntBank: TSmallintField
      FieldName = 'UsaEntBank'
      Required = True
    end
    object QrContasLnkComposHist: TSmallintField
      FieldName = 'ComposHist'
      Required = True
    end
    object QrContasLnkMultiCtas: TIntegerField
      FieldName = 'MultiCtas'
    end
  end
  object PMInclui: TPopupMenu
    Left = 233
    Top = 445
    object CadastrarnovoLinkdeconta1: TMenuItem
      Caption = '&Cadastrar novo &Link de conta'
      OnClick = CadastrarnovoLinkdeconta1Click
    end
    object Cadastrarnovoitemdesconsidervel1: TMenuItem
      Caption = '&Cadastrar novo item desconsider'#225'vel'
      OnClick = Cadastrarnovoitemdesconsidervel1Click
    end
  end
  object PMAtiva: TPopupMenu
    Left = 433
    Top = 441
    object Alteradados1: TMenuItem
      Caption = '&Link de conta'
      OnClick = Alteradados1Click
    end
    object Alteratipo1: TMenuItem
      Caption = '&Item desconsider'#225'vel'
      OnClick = Alteratipo1Click
    end
  end
  object QrContasLnkIts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrContasLnkItsCalcFields
    SQL.Strings = (
      'SELECT '
      '/*'
      'IF(ent.Codigo=0, '#39'** TODOS **'#39', CASE WHEN ent.Tipo=0 '
      'THEN ent.RazaoSocial ELSE ent.Nome END) NOMECLIINT,'
      '*/'
      'IF(cli.Codigo=0, '#39#39', CASE WHEN cli.Tipo=0 '
      'THEN cli.RazaoSocial ELSE cli.Nome END) NOMECLIENTE,'
      'IF(fnc.Codigo=0, '#39#39', CASE WHEN fnc.Tipo=0 '
      'THEN fnc.RazaoSocial ELSE fnc.Nome END) NOMEFORNECE,'
      'CASE WHEN con.Codigo=0 THEN '#39'** NENHUMA **'#39' '
      'ELSE Con.Nome END NOMECONTA, '
      '/*'
      'CASE WHEN ban.Codigo=0 THEN '#39'** TODOS **'#39' '
      'ELSE ban.Nome END NOMEBANCO, '
      '*/'
      'clk.* '
      'FROM contaslnkits clk'
      'LEFT JOIN contas con ON con.Codigo=clk.Genero'
      '/*'
      'LEFT JOIN bancos ban ON ban.Codigo=clk.Banco'
      'LEFT JOIN entidades ent ON ent.Codigo=clk.CliInt'
      '*/'
      'LEFT JOIN entidades cli ON cli.Codigo=clk.Cliente'
      'LEFT JOIN entidades fnc ON fnc.Codigo=clk.Fornece'
      'WHERE clk.Controle =:P0')
    Left = 196
    Top = 41
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContasLnkItsNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrContasLnkItsNOMEFORNECE: TWideStringField
      FieldName = 'NOMEFORNECE'
      Size = 100
    end
    object QrContasLnkItsNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrContasLnkItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrContasLnkItsSubCtrl: TIntegerField
      FieldName = 'SubCtrl'
    end
    object QrContasLnkItsGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrContasLnkItsTexto: TWideStringField
      FieldName = 'Texto'
      Size = 50
    end
    object QrContasLnkItsTipoCalc: TSmallintField
      FieldName = 'TipoCalc'
    end
    object QrContasLnkItsFator: TFloatField
      FieldName = 'Fator'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrContasLnkItsConsidera: TSmallintField
      FieldName = 'Considera'
    end
    object QrContasLnkItsCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrContasLnkItsFornece: TIntegerField
      FieldName = 'Fornece'
    end
    object QrContasLnkItsUsaEntBank: TSmallintField
      FieldName = 'UsaEntBank'
    end
    object QrContasLnkItsComposHist: TSmallintField
      FieldName = 'ComposHist'
    end
    object QrContasLnkItsNOMERELACIONADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMERELACIONADO'
      Size = 255
      Calculated = True
    end
    object QrContasLnkItsNOMETIPOCALC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPOCALC'
      Calculated = True
    end
  end
  object DsContasLnkIts: TDataSource
    DataSet = QrContasLnkIts
    Left = 224
    Top = 41
  end
  object PMMulti: TPopupMenu
    Left = 692
    Top = 444
    object Incluiitemdelanamento1: TMenuItem
      Caption = '&Inclui novo item de lan'#231'amento'
      OnClick = Incluiitemdelanamento1Click
    end
    object Alteraitemdelanamentoatual1: TMenuItem
      Caption = '&Altera item de lan'#231'amento atual'
      OnClick = Alteraitemdelanamentoatual1Click
    end
    object Excluiitemdelanamentoatual1: TMenuItem
      Caption = '&Exclui item de lan'#231'amento atual'
      OnClick = Excluiitemdelanamentoatual1Click
    end
  end
end
