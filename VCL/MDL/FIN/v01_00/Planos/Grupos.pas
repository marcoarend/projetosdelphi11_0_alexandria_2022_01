unit Grupos;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, Variants, dmkPermissoes, dmkGeral, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, UnDmkProcFunc;

type
  TFmGrupos = class(TForm)
    PainelDados: TPanel;
    DsGrupos: TDataSource;
    QrGrupos: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    QrGruposLk: TIntegerField;
    QrGruposDataCad: TDateField;
    QrGruposDataAlt: TDateField;
    QrGruposUserCad: TIntegerField;
    QrGruposUserAlt: TIntegerField;
    QrGruposCodigo: TSmallintField;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    LaConjunto: TLabel;
    DBEdConjunto: TDBEdit;
    DBGrid1: TDBGrid;
    TbSubGrupos: TmySQLTable;
    DsSubGrupos: TDataSource;
    TbSubGruposCodigo: TIntegerField;
    TbSubGruposOrdemLista: TIntegerField;
    TbSubGruposNome: TWideStringField;
    Label3: TLabel;
    EdConjunto: TdmkEditCB;
    CBConjunto: TdmkDBLookupComboBox;
    QrConjuntos: TmySQLQuery;
    QrConjuntosCodigo: TIntegerField;
    QrConjuntosNome: TWideStringField;
    DsConjuntos: TDataSource;
    QrGruposConjunto: TIntegerField;
    QrGruposNOMECONJUNTO: TWideStringField;
    TbSubGruposGrupo: TIntegerField;
    TbSubGruposTipoAgrupa: TIntegerField;
    TbSubGruposLk: TIntegerField;
    TbSubGruposDataCad: TDateField;
    TbSubGruposDataAlt: TDateField;
    TbSubGruposUserCad: TIntegerField;
    TbSubGruposUserAlt: TIntegerField;
    Label4: TLabel;
    EdOrdemLista: TdmkEdit;
    QrGruposOrdemLista: TIntegerField;
    DBEdit1: TDBEdit;
    Label5: TLabel;
    QrGruposNome: TWideStringField;
    DBCheckBox1: TDBCheckBox;
    CkCtrlaSdo: TCheckBox;
    QrGruposCtrlaSdo: TSmallintField;
    dmkPermissoes1: TdmkPermissoes;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGruposAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrGruposAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGruposBeforeOpen(DataSet: TDataSet);
    procedure TbSubGruposDeleting(Sender: TObject; var Allow: Boolean);
  private
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
//    procedure IncluiSubRegistro;
//    procedure ExcluiSubRegistro;
//    procedure AlteraSubRegistro;
//    procedure TravaOForm;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmGrupos: TFmGrupos;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGrupos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGrupos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGruposCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGrupos.DefParams;
begin
  VAR_GOTOTABELA := 'Grupos';
  VAR_GOTOMYSQLTABLE := QrGrupos;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT gr.*, cj.Nome NOMECONJUNTO');
  VAR_SQLx.Add('FROM grupos gr');
  VAR_SQLx.Add('LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto');
  //
  VAR_SQL1.Add('WHERE gr.Codigo=:P0');
  //
  VAR_SQLa.Add('WHERE gr.Nome Like :P0');
  //
end;

procedure TFmGrupos.MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    PainelControle.Visible:=False;
    if Status = CO_INCLUSAO then
    begin
    EdNome.Text := CO_VAZIO;
      EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
      EdConjunto.Text := '';
      CBConjunto.KeyValue := Null;
      EdOrdemLista.Text := '';
      CkCtrlaSdo.Checked := True;
    end else begin
      EdCodigo.Text := DBEdCodigo.Text;
      EdNome.Text := DBEdNome.Text;
      EdConjunto.Text := IntToStr(QrGruposConjunto.Value);
      CBConjunto.KeyValue := QrGruposConjunto.Value;
      EdOrdemLista.Text := IntToStr(QrGruposOrdemLista.Value);;
      CkCtrlaSdo.Checked := Geral.IntToBool_0(QrGruposCtrlaSdo.Value);
    end;
    EdNome.SetFocus;
  end else begin
    PainelControle.Visible:=True;
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmGrupos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmGrupos.AlteraRegistro;
var
  Grupos : Integer;
begin
  Grupos := QrGruposCodigo.Value;
  if QrGruposCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(Grupos, Dmod.MyDB, 'Grupos', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Grupos, Dmod.MyDB, 'Grupos', 'Codigo');
      MostraEdicao(True, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmGrupos.IncluiRegistro;
var
  Cursor : TCursor;
  Grupos : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Grupos := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Grupos', 'Grupo', 'Codigo');
    if Length(FormatFloat(FFormatFloat, Grupos))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(True, CO_INCLUSAO, Grupos);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmGrupos.QueryPrincipalAfterOpen;
begin
end;

procedure TFmGrupos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmGrupos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGrupos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGrupos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGrupos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGrupos.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmGrupos.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmGrupos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrGruposCodigo.Value;
  Close;
end;

procedure TFmGrupos.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := Geral.IMV(EdCodigo.Text);
  Dmod.QrUpdU.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('INSERT INTO grupos SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE grupos SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0, Conjunto=:P1, OrdemLista=:P2, CtrlaSdo=:P3, ');
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := Geral.IMV(EdConjunto.Text);
  Dmod.QrUpdU.Params[02].AsInteger := Geral.IMV(EdOrdemLista.Text);
  Dmod.QrUpdU.Params[03].AsInteger := MLAGeral.BoolToInt(CkCtrlaSdo.Checked);
  //
  Dmod.QrUpdU.Params[04].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[05].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[06].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Grupos', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmGrupos.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Grupos', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Grupos', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Grupos', 'Codigo');
end;

procedure TFmGrupos.FormCreate(Sender: TObject);
begin
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  DBGrid1.Align     := alClient;
  CriaOForm;
  TbSubGrupos.Open;
  QrConjuntos.Open;
end;

procedure TFmGrupos.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGruposCodigo.Value,LaRegistro.Caption);
end;

procedure TFmGrupos.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmGrupos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmGrupos.QrGruposAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmGrupos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Grupos', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmGrupos.QrGruposAfterScroll(DataSet: TDataSet);
begin
  TbSubGrupos.Filter := 'Grupo='+IntToStr(QrGruposCodigo.Value);
end;

procedure TFmGrupos.SbQueryClick(Sender: TObject);
begin
  LocCod(QrGruposCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Grupos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmGrupos.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmGrupos.QrGruposBeforeOpen(DataSet: TDataSet);
begin
  QrGruposCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmGrupos.TbSubGruposDeleting(Sender: TObject;
  var Allow: Boolean);
begin
  Allow := False;
  Application.MessageBox('Para excluir sub-grupo de conta acesse o cadastro de sub-grupos!',
  'Informa��o', MB_OK+MB_ICONINFORMATION);
end;

end.

