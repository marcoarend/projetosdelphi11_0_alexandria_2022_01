unit ContasLnk;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Menus,
  Variants, dmkCheckGroup, dmkLabel, Grids, DBGrids, UnDmkProcFunc,
  UnDmkEnums;

type
  TFmContasLnk = class(TForm)
    PainelDados: TPanel;
    DsContasLnk: TDataSource;
    QrContasLnk: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtAtiva: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    Label2: TLabel;
    Label3: TLabel;
    PMInclui: TPopupMenu;
    CadastrarnovoLinkdeconta1: TMenuItem;
    Cadastrarnovoitemdesconsidervel1: TMenuItem;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    Label6: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label7: TLabel;
    DBEdit9: TDBEdit;
    QrContasLnkNOMECLIINT: TWideStringField;
    QrContasLnkNOMECONTA: TWideStringField;
    QrContasLnkNOMEBANCO: TWideStringField;
    QrContasLnkCodigo: TIntegerField;
    QrContasLnkControle: TIntegerField;
    QrContasLnkTexto: TWideStringField;
    QrContasLnkDoc: TWideStringField;
    QrContasLnkLk: TIntegerField;
    QrContasLnkDataCad: TDateField;
    QrContasLnkDataAlt: TDateField;
    QrContasLnkUserCad: TIntegerField;
    QrContasLnkUserAlt: TIntegerField;
    QrContasLnkCliInt: TIntegerField;
    QrContasLnkBanco: TIntegerField;
    QrContasLnkConsidera: TSmallintField;
    Panel2: TPanel;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    QrContasLnkNOMETIPO: TWideStringField;
    PMAtiva: TPopupMenu;
    Alteradados1: TMenuItem;
    Alteratipo1: TMenuItem;
    QrContasLnkCliente: TIntegerField;
    QrContasLnkFornece: TIntegerField;
    QrContasLnkNOMECLIENTE: TWideStringField;
    QrContasLnkNOMEFORNECE: TWideStringField;
    Label8: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    Label9: TLabel;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    QrContasLnkUsaEntBank: TSmallintField;
    DBCheckBox1: TDBCheckBox;
    QrContasLnkComposHist: TSmallintField;
    Label10: TLabel;
    dmkDBCheckGroup1: TdmkDBCheckGroup;
    QrContasLnkIts: TmySQLQuery;
    DsContasLnkIts: TDataSource;
    DBGItens: TDBGrid;
    QrContasLnkMultiCtas: TIntegerField;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtMulti: TBitBtn;
    PMMulti: TPopupMenu;
    Incluiitemdelanamento1: TMenuItem;
    Alteraitemdelanamentoatual1: TMenuItem;
    Excluiitemdelanamentoatual1: TMenuItem;
    QrContasLnkItsNOMECLIENTE: TWideStringField;
    QrContasLnkItsNOMEFORNECE: TWideStringField;
    QrContasLnkItsNOMECONTA: TWideStringField;
    QrContasLnkItsControle: TIntegerField;
    QrContasLnkItsSubCtrl: TIntegerField;
    QrContasLnkItsGenero: TIntegerField;
    QrContasLnkItsTexto: TWideStringField;
    QrContasLnkItsTipoCalc: TSmallintField;
    QrContasLnkItsFator: TFloatField;
    QrContasLnkItsConsidera: TSmallintField;
    QrContasLnkItsCliente: TIntegerField;
    QrContasLnkItsFornece: TIntegerField;
    QrContasLnkItsUsaEntBank: TSmallintField;
    QrContasLnkItsComposHist: TSmallintField;
    QrContasLnkItsNOMERELACIONADO: TWideStringField;
    QrContasLnkItsNOMETIPOCALC: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrContasLnkBeforeOpen(DataSet: TDataSet);
    procedure CadastrarnovoLinkdeconta1Click(Sender: TObject);
    procedure Cadastrarnovoitemdesconsidervel1Click(Sender: TObject);
    procedure QrContasLnkCalcFields(DataSet: TDataSet);
    procedure BtAtivaClick(Sender: TObject);
    procedure QrContasLnkBeforeClose(DataSet: TDataSet);
    procedure QrContasLnkAfterOpen(DataSet: TDataSet);
    procedure QrContasLnkAfterScroll(DataSet: TDataSet);
    procedure Alteradados1Click(Sender: TObject);
    procedure Alteratipo1Click(Sender: TObject);
    procedure BtMultiClick(Sender: TObject);
    procedure Incluiitemdelanamento1Click(Sender: TObject);
    procedure Alteraitemdelanamentoatual1Click(Sender: TObject);
    procedure Excluiitemdelanamentoatual1Click(Sender: TObject);
    procedure QrContasLnkItsCalcFields(DataSet: TDataSet);
  private
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure DefineStatusBtnAtiva;
  public
    { Public declarations }
    procedure ReopenContasLnkIts(SubCtrl: Integer);
    procedure AtualizaMultiCtas();
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmContasLnk: TFmContasLnk;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ContasLnkEdit, ContasLnkIts, MyGlyfs, dmkGeral, MyDBCheck;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmContasLnk.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmContasLnk.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrContasLnkControle.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmContasLnk.DefParams;
begin
  VAR_GOTOTABELA := 'ContasLnk';
  VAR_GOTOMYSQLTABLE := QrContasLnk;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := 'Controle';
  VAR_GOTONOME := '(clk.Texto OR clk.Doc)';
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT IF(ent.Codigo=0, "** TODOS **", CASE WHEN ent.Tipo=0');
  VAR_SQLx.Add('THEN ent.RazaoSocial ELSE ent.Nome END) NOMECLIINT,');
  VAR_SQLx.Add('IF(cli.Codigo=0, "** V�RIOS **", CASE WHEN cli.Tipo=0');
  VAR_SQLx.Add('THEN cli.RazaoSocial ELSE cli.Nome END) NOMECLIENTE,');
  VAR_SQLx.Add('IF(fnc.Codigo=0, "** V�RIOS **", CASE WHEN fnc.Tipo=0');
  VAR_SQLx.Add('THEN fnc.RazaoSocial ELSE fnc.Nome END) NOMEFORNECE,');
  VAR_SQLx.Add('CASE WHEN con.Codigo=0 THEN "** NENHUMA **"');
  VAR_SQLx.Add('ELSE Con.Nome END NOMECONTA,');
  VAR_SQLx.Add('CASE WHEN ban.Codigo=0 THEN "** TODOS **"');
  VAR_SQLx.Add('ELSE ban.Nome END NOMEBANCO, clk.*');
  VAR_SQLx.Add('FROM contaslnk clk');
  VAR_SQLx.Add('LEFT JOIN contas con ON con.Codigo=clk.Codigo');
  VAR_SQLx.Add('LEFT JOIN bancos ban ON ban.Codigo=clk.Banco');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=clk.CliInt');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=clk.Cliente');
  VAR_SQLx.Add('LEFT JOIN entidades fnc ON fnc.Codigo=clk.Fornece');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE clk.Controle > 0');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND clk.Controle=:P0');
  //
  VAR_SQLa.Add('AND (clk.Texto OR clk.Doc) Like :P0 ');
  //
end;

procedure TFmContasLnk.Excluiitemdelanamentoatual1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrContasLnkIts, DBGItens,
  'contaslnkits', ['SubCtrl'], ['SubCtrl'], istPergunta, '');
end;

procedure TFmContasLnk.CriaOForm;
begin
  DefParams;
  Va(vpLast);
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmContasLnk.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmContasLnk.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmContasLnk.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmContasLnk.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmContasLnk.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmContasLnk.BtMultiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMulti, BtMulti);
end;

procedure TFmContasLnk.BtSaidaClick(Sender: TObject);
begin
  VAR_CONTASLNK := QrContasLnkCodigo.Value;
  Close;
end;

procedure TFmContasLnk.FormCreate(Sender: TObject);
begin
  PainelDados.Align := alClient;
  CriaOForm;
end;

procedure TFmContasLnk.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrContasLnkControle.Value,LaRegistro.Caption);
end;

procedure TFmContasLnk.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmContasLnk.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.SQLType);
end;

procedure TFmContasLnk.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if QrContasLnk.State = dsBrowse then DefineStatusBtnAtiva;
end;

procedure TFmContasLnk.SbQueryClick(Sender: TObject);
begin
  LocCod(QrContasLnkControle.Value,
  CuringaLoc.CriaForm('Controle', '(Texto OR Doc)' , 'ContasLnk', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmContasLnk.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmContasLnk.Incluiitemdelanamento1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmContasLnkIts, FmContasLnkIts, afmoNegarComAviso,
    QrContasLnkIts, stIns);
end;

procedure TFmContasLnk.QrContasLnkBeforeOpen(DataSet: TDataSet);
begin
  QrContasLnkControle.DisplayFormat := FFormatFloat;
end;

procedure TFmContasLnk.CadastrarnovoLinkdeconta1Click(Sender: TObject);
begin
  VAR_CONTA := 0;
  VAR_CONTASLNK := 0;
  Application.CreateForm(TFmContasLnkEdit, FmContasLnkEdit);
  FmContasLnkEdit.LaTipo.SQLType := stIns;
  FmContasLnkEdit.FConta    := 0;
  FmContasLnkEdit.FControle := 0;
  FmContasLnkEdit.EdTexto.Text := '';
  FmContasLnkEdit.EdDoc.Text := '';
  FmContasLnkEdit.EdCliInt.Text := '';
  FmContasLnkEdit.CBCliInt.KeyValue := 0;
  FmContasLnkEdit.EdBanco.Text := '';
  FmContasLnkEdit.CBBanco.KeyValue := 0;
  FmContasLnkEdit.EdCliente.Text := '';
  FmContasLnkEdit.CBCliente.KeyValue := 0;
  FmContasLnkEdit.EdFornece.Text := '';
  FmContasLnkEdit.CBFornece.KeyValue := 0;
  FmContasLnkEdit.CkUsaEntBank.Checked := False;
  FmContasLnkEdit.CGComposHist.Value := 1;
  //
  FmContasLnkEdit.ShowModal;
  FmContasLnkEdit.Destroy;
  //
  if VAR_CONTASLNK > 0 then LocCod(QrContasLnkControle.Value, VAR_CONTASLNK)
end;

procedure TFmContasLnk.Cadastrarnovoitemdesconsidervel1Click(
  Sender: TObject);
begin
  VAR_CONTA := 0;
  VAR_CONTASLNK := 0;
  Application.CreateForm(TFmContasLnkEdit, FmContasLnkEdit);
  FmContasLnkEdit.LaTipo.SQLType := stIns;
  FmContasLnkEdit.FConta    := 0;
  FmContasLnkEdit.FControle := 0;
  FmContasLnkEdit.EdTexto.Text := '';
  FmContasLnkEdit.EdDoc.Text := '';
  FmContasLnkEdit.EdCliInt.Text := '';
  FmContasLnkEdit.CBCliInt.KeyValue := 0;
  FmContasLnkEdit.EdCliente.Text := '';
  FmContasLnkEdit.CBCliente.KeyValue := 0;
  FmContasLnkEdit.EdFornece.Text := '';
  FmContasLnkEdit.CBFornece.KeyValue := 0;
  //
  FmContasLnkEdit.EdConta.Text := '';
  FmContasLnkEdit.CBConta.KeyValue := Null;
  FmContasLnkEdit.EdConta.Enabled := False;
  FmContasLnkEdit.CBConta.Enabled := False;
  FmContasLnkEdit.LaConta.Enabled := False;
  //
  FmContasLnkEdit.EdBanco.Text := '';
  FmContasLnkEdit.CBBanco.KeyValue := 0;
  FmContasLnkEdit.CkUsaEntBank.Checked := False;
  FmContasLnkEdit.CGComposHist.Value := 1;
  //
  FmContasLnkEdit.ShowModal;
  FmContasLnkEdit.Destroy;
  if VAR_CONTASLNK > 0 then LocCod(QrContasLnkControle.Value, VAR_CONTASLNK)
end;

procedure TFmContasLnk.BtAlteraClick(Sender: TObject);
begin
  VAR_CONTA := 0;
  VAR_CONTASLNK := 0;
  Application.CreateForm(TFmContasLnkEdit, FmContasLnkEdit);
  FmContasLnkEdit.LaTipo.SQLType := stUpd;
  FmContasLnkEdit.FConta    := QrContasLnkCodigo.Value;
  FmContasLnkEdit.FControle := QrContasLnkControle.Value;
  FmContasLnkEdit.EdTexto.Text := QrContasLnkTexto.Value;
  FmContasLnkEdit.EdDoc.Text := QrContasLnkDoc.Value;
  FmContasLnkEdit.EdCliInt.Text := IntToStr(QrContasLnkCliInt.Value);
  FmContasLnkEdit.CBCliInt.KeyValue := QrContasLnkCliInt.Value;
  FmContasLnkEdit.EdCliente.Text := IntToStr(QrContasLnkCliente.Value);
  FmContasLnkEdit.CBCliente.KeyValue := QrContasLnkCliente.Value;
  FmContasLnkEdit.EdFornece.Text := IntToStr(QrContasLnkFornece.Value);
  FmContasLnkEdit.CBFornece.KeyValue := QrContasLnkFornece.Value;
  //
  if QrContasLnkConsidera.Value = 0 then
  begin
    FmContasLnkEdit.EdConta.Text := '';
    FmContasLnkEdit.CBConta.KeyValue := 0;
    FmContasLnkEdit.EdConta.Enabled := False;
    FmContasLnkEdit.CBConta.Enabled := False;
    FmContasLnkEdit.LaConta.Enabled := False;
  end else begin
    FmContasLnkEdit.EdConta.Text := IntToStr(QrContasLnkCodigo.Value);
    FmContasLnkEdit.CBConta.KeyValue := QrContasLnkCodigo.Value;
  end;
  //
  FmContasLnkEdit.EdBanco.Text := IntToStr(QrContasLnkBanco.Value);
  FmContasLnkEdit.CBBanco.KeyValue := QrContasLnkBanco.Value;
  FmContasLnkEdit.CkUsaEntBank.Checked := MLAGeral.IntToBool(QrContasLnkUsaEntBank.Value);
  FmContasLnkEdit.CGComposHist.Value := QrContasLnkComposHist.Value;
  //
  FmContasLnkEdit.ShowModal;
  FmContasLnkEdit.Destroy;
  LocCod(QrContasLnkControle.Value, QrContasLnkControle.Value);
end;

procedure TFmContasLnk.QrContasLnkCalcFields(DataSet: TDataSet);
begin
  case QrContasLnkConsidera.Value of
    0: QrContasLnkNOMETIPO.Value := 'Desconsidera��o';
    1: QrContasLnkNOMETIPO.Value := 'Lan�amento';
    9: QrContasLnkNOMETIPO.Value := 'ITEM INATIVO';
    else QrContasLnkNOMETIPO.Value := 'Desconhecido: '+IntToStr(QrContasLnkConsidera.Value);
  end;
end;

procedure TFmContasLnk.QrContasLnkItsCalcFields(DataSet: TDataSet);
begin
  QrContasLnkItsNOMERELACIONADO.Value :=
  QrContasLnkItsNOMEFORNECE.Value +
  QrContasLnkItsNOMECLIENTE.Value;
  //
  case QrContasLnkItsTipoCalc.Value of
    0: QrContasLnkItsNOMETIPOCALC.Value := 'Nenhuma';
    1: QrContasLnkItsNOMETIPOCALC.Value := 'Valor fixo $';
    2: QrContasLnkItsNOMETIPOCALC.Value := '% do total';
    3: QrContasLnkItsNOMETIPOCALC.Value := '% Saldo restante';
    else QrContasLnkItsNOMETIPOCALC.Value := '? ? ? ? ? ';
  end;
end;

procedure TFmContasLnk.ReopenContasLnkIts(SubCtrl: Integer);
begin
  QrContasLnkIts.Close;
  QrContasLnkIts.Params[0].AsInteger := QrContasLnkControle.Value;
  QrContasLnkIts.Open;
  //
  if SubCtrl > 0 then
    QrContasLnkIts.Locate('SubCtrl', SubCtrl, []);
  if ((QrContasLnkIts.RecordCount <> QrContasLnkMultiCtas.Value)) then
    AtualizaMultiCtas();
end;

procedure TFmContasLnk.BtAtivaClick(Sender: TObject);
//var
  //i: Integer;
begin
  // 0, ou 1 ou 9
  Screen.Cursor := crHourGlass;
  (*if QrContasLnkConsidera.Value <> 9  then i := 9
  else if QrContasLnkCodigo.Value = 0 then i := 0 else i := 1;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE contaslnk SET Considera=:P0 ');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
  Dmod.QrUpd.Params[00].AsInteger := i;
  Dmod.QrUpd.Params[01].AsInteger := QrcontaslnkControle.Value;
  Dmod.QrUpd.ExecSQL;
  LocCod(QrcontaslnkControle.Value, QrcontaslnkControle.Value);
  Screen.Cursor := crDefault;*)
  if QrcontaslnkConsidera.Value <> 9  then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE contaslnk SET Considera=9 ');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Dmod.QrUpd.Params[0].AsInteger := QrcontaslnkControle.Value;
    Dmod.QrUpd.ExecSQL;
    LocCod(QrcontaslnkControle.Value, QrcontaslnkControle.Value);
  end else MyObjects.MostraPopUpDeBotao(PMAtiva, BtAtiva);
  Screen.Cursor := crDefault;
end;

procedure TFmcontaslnk.QrcontaslnkBeforeClose(DataSet: TDataSet);
begin
  BtAtiva.Visible := False;
end;

procedure TFmcontaslnk.QrcontaslnkAfterOpen(DataSet: TDataSet);
begin
  DefineStatusBtnAtiva;
  BtAtiva.Visible := Qrcontaslnk.RecordCount > 0;
end;

procedure TFmcontaslnk.QrcontaslnkAfterScroll(DataSet: TDataSet);
begin
  DefineStatusBtnAtiva;
  ReopenContasLnkIts(0);
end;

procedure TFmcontaslnk.DefineStatusBtnAtiva;
begin
  case QrcontaslnkConsidera.Value of
    0..1:
    begin
      BtAtiva.Caption := 'Desa&tivar';
      //BtAtiva.Glyph := FmMyGlyfs.Lista_48x24.Items[224].Bitmap;
      FmMyGlyfs.ObtemBitmapDeGlyph(224, BtAtiva);
    end;
    9:
    begin
      BtAtiva.Caption := 'Ati&var';
      //BtAtiva.Glyph := FmMyGlyfs.Lista_48x24.Items[223].Bitmap;
      FmMyGlyfs.ObtemBitmapDeGlyph(223, BtAtiva);
    end;
  end;
end;

procedure TFmcontaslnk.Alteradados1Click(Sender: TObject);
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE contaslnk SET Considera=1 ');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
  Dmod.QrUpd.Params[0].AsInteger := QrcontaslnkControle.Value;
  Dmod.QrUpd.ExecSQL;
  LocCod(QrcontaslnkControle.Value, QrcontaslnkControle.Value);
  BtAlteraClick(Self);
end;

procedure TFmContasLnk.Alteraitemdelanamentoatual1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmContasLnkIts, FmContasLnkIts, afmoNegarComAviso,
    QrContasLnkIts, stUpd);
end;

procedure TFmcontaslnk.Alteratipo1Click(Sender: TObject);
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE contaslnk SET Considera=0 ');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
  Dmod.QrUpd.Params[0].AsInteger := QrcontaslnkControle.Value;
  Dmod.QrUpd.ExecSQL;
  LocCod(QrcontaslnkControle.Value, QrcontaslnkControle.Value);
  BtAlteraClick(Self);
end;

procedure TFmContasLnk.AtualizaMultiCtas();
begin
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT COUNT(*) Itens ');
  Dmod.QrAux.SQL.Add('FROM contaslnkits ');
  Dmod.QrAux.SQL.Add('WHERE Controle=:P0');
  Dmod.QrAux.Params[00].AsInteger := QrContasLnkControle.Value;
  Dmod.QrAux.Open;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE contaslnk SET MultiCtas=:P0 WHERE Controle=:P1');
  Dmod.QrUpd.Params[00].AsInteger := Dmod.QrAux.FieldByName('Itens').AsInteger;
  Dmod.QrUpd.Params[01].AsInteger := QrContasLnkControle.Value;
  Dmod.QrUpd.ExecSQL;
  // N�o precisa reabrir (?)
end;

end.

