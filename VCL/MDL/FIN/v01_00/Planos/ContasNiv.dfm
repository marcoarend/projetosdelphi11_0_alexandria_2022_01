object FmContasNiv: TFmContasNiv
  Left = 339
  Top = 185
  Caption = 'FIN-PLCTA-025 :: Configura'#231#227'o de Controle de Saldos'
  ClientHeight = 496
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 448
    Width = 792
    Height = 48
    Align = alBottom
    TabOrder = 1
    object LaAviso: TLabel
      Left = 116
      Top = 8
      Width = 9
      Height = 13
      Caption = '...'
    end
    object Panel2: TPanel
      Left = 680
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtAcao: TBitBtn
      Tag = 10010
      Left = 21
      Top = 4
      Width = 90
      Height = 40
      Caption = '&A'#231#227'o'
      Enabled = False
      TabOrder = 1
      OnClick = BtAcaoClick
    end
    object PB1: TProgressBar
      Left = 116
      Top = 24
      Width = 557
      Height = 17
      TabOrder = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Caption = 'Configura'#231#227'o de Controle de Saldos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 790
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 400
    Align = alClient
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 331
      Top = 1
      Width = 5
      Height = 398
    end
    object dmkDBGrid2: TdmkDBGrid
      Left = 336
      Top = 1
      Width = 455
      Height = 398
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Nivel'
          Title.Caption = 'N'#237'vel'
          Width = 33
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMENIVEL'
          Title.Caption = 'Descri'#231#227'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Genero'
          Title.Caption = 'G'#234'nero'
          Width = 42
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEGENERO'
          Title.Caption = 'Descri'#231#227'o'
          Width = 299
          Visible = True
        end>
      Color = clWindow
      DataSource = DsContasNiv
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Nivel'
          Title.Caption = 'N'#237'vel'
          Width = 33
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMENIVEL'
          Title.Caption = 'Descri'#231#227'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Genero'
          Title.Caption = 'G'#234'nero'
          Width = 42
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEGENERO'
          Title.Caption = 'Descri'#231#227'o'
          Width = 299
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 330
      Height = 398
      Align = alLeft
      BevelOuter = bvNone
      Caption = 'Panel3'
      TabOrder = 1
      object dmkDBGrid1: TdmkDBGrid
        Left = 0
        Top = 0
        Width = 330
        Height = 398
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'CliInt'
            Title.Caption = 'Cliente'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Entidade'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECLI'
            Title.Caption = 'Nome / Raz'#227'o Social'
            Visible = True
          end>
        Color = clWindow
        DataSource = DsEmpresas
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CliInt'
            Title.Caption = 'Cliente'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Entidade'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECLI'
            Title.Caption = 'Nome / Raz'#227'o Social'
            Visible = True
          end>
      end
    end
  end
  object QrEmpresas: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrEmpresasAfterScroll
    SQL.Strings = (
      'SELECT Codigo, CliInt, '
      'IF(Tipo=0, RazaoSocial, Nome) NOMECLI '
      'FROM entidades'
      'WHERE CliInt <>0'
      '')
    Left = 228
    Top = 96
    object QrEmpresasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmpresasCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrEmpresasNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Required = True
      Size = 100
    end
  end
  object DsEmpresas: TDataSource
    DataSet = QrEmpresas
    Left = 256
    Top = 96
  end
  object QrContasNiv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ELT(niv.Nivel, '#39'Conta'#39', '#39'Sub-grupo'#39', '#39'Grupo'#39', '
      #39'Conjunto'#39', '#39'Plano'#39') NOMENIVEL, ELT(niv.Nivel,'
      'cta.Nome, sgr.Nome, gru.Nome, cjt.Nome, pla.Nome) '
      'NOMEGENERO, niv.Nivel, niv.Genero, niv.Entidade'
      'FROM contasniv niv'
      'LEFT JOIN contas    cta ON cta.Codigo=niv.Genero'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=niv.Genero'
      'LEFT JOIN grupos    gru ON gru.Codigo=niv.Genero'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=niv.Genero'
      'LEFT JOIN plano     pla ON pla.Codigo=niv.Genero'
      'WHERE niv.Entidade=:P0'
      '')
    Left = 228
    Top = 124
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContasNivNOMENIVEL: TWideStringField
      FieldName = 'NOMENIVEL'
      Size = 9
    end
    object QrContasNivNOMEGENERO: TWideStringField
      FieldName = 'NOMEGENERO'
      Size = 50
    end
    object QrContasNivNivel: TIntegerField
      FieldName = 'Nivel'
      Required = True
    end
    object QrContasNivGenero: TIntegerField
      FieldName = 'Genero'
      Required = True
    end
    object QrContasNivEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
  end
  object DsContasNiv: TDataSource
    DataSet = QrContasNiv
    Left = 256
    Top = 124
  end
  object PMAcao: TPopupMenu
    Left = 68
    Top = 376
    object Incluiitemacontrolar1: TMenuItem
      Caption = '&Inclui item a controlar'
      OnClick = Incluiitemacontrolar1Click
    end
    object Selecionavriositens1: TMenuItem
      Caption = 'Inclui &V'#225'rios itens'
      OnClick = Selecionavriositens1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Excluiitemacontrolar1: TMenuItem
      Caption = '&Exclui item cadastrado'
      OnClick = Excluiitemacontrolar1Click
    end
  end
  object QrNiveis: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, CtrlaSdo, 1 Nivel'
      'FROM contas'
      'WHERE Codigo > 0'
      ''
      'UNION'
      ''
      'SELECT Codigo, Nome, CtrlaSdo, 2 Nivel'
      'FROM subgrupos'
      'WHERE Codigo > 0'
      ''
      'UNION'
      ''
      'SELECT Codigo, Nome, CtrlaSdo, 3 Nivel'
      'FROM grupos'
      'WHERE Codigo > 0'
      ''
      'UNION'
      ''
      'SELECT Codigo, Nome, CtrlaSdo, 4 Nivel'
      'FROM conjuntos'
      'WHERE Codigo > 0'
      ''
      'UNION'
      ''
      'SELECT Codigo, Nome, CtrlaSdo, 5 Nivel'
      'FROM plano'
      'WHERE Codigo > 0')
    Left = 205
    Top = 225
    object QrNiveisCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrNiveisNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrNiveisCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
      Required = True
    end
    object QrNiveisNivel: TLargeintField
      FieldName = 'Nivel'
      Required = True
    end
  end
end
