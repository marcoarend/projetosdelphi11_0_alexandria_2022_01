object FmContasSdo: TFmContasSdo
  Left = 339
  Top = 185
  Caption = 'FIN-PLCTA-019 :: Saldo de Conta do Plano de Contas'
  ClientHeight = 226
  ClientWidth = 554
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 178
    Width = 554
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 442
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 554
    Height = 48
    Align = alTop
    Caption = 'Saldo de Conta do Plano de Contas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 552
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 558
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 554
    Height = 130
    Align = alClient
    TabOrder = 0
    object Label3: TLabel
      Left = 16
      Top = 8
      Width = 279
      Height = 13
      Caption = 'Entidade (que possui pelo menos uma carteira cadastrada):'
    end
    object Label1: TLabel
      Left = 16
      Top = 48
      Width = 31
      Height = 13
      Caption = 'Conta:'
    end
    object Label4: TLabel
      Left = 452
      Top = 48
      Width = 86
      Height = 13
      Caption = 'Novo saldo inicial:'
    end
    object Label2: TLabel
      Left = 452
      Top = 8
      Width = 85
      Height = 13
      Caption = 'Saldo inicial atual:'
      FocusControl = DBEdit1
    end
    object EdEntidade: TdmkEditCB
      Left = 16
      Top = 24
      Width = 45
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdEntidadeChange
      DBLookupComboBox = CBEntidade
    end
    object CBEntidade: TdmkDBLookupComboBox
      Left = 64
      Top = 24
      Width = 381
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENT'
      ListSource = DsEntidades
      TabOrder = 1
      dmkEditCB = EdEntidade
      UpdType = utYes
    end
    object EdCodigo: TdmkEditCB
      Left = 16
      Top = 64
      Width = 45
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdEntidadeChange
      DBLookupComboBox = CBCodigo
    end
    object CBCodigo: TdmkDBLookupComboBox
      Left = 64
      Top = 64
      Width = 381
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContas
      TabOrder = 3
      dmkEditCB = EdCodigo
      UpdType = utYes
    end
    object EdSdoIni: TdmkEdit
      Left = 452
      Top = 64
      Width = 90
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object DBEdit1: TDBEdit
      Left = 452
      Top = 24
      Width = 89
      Height = 21
      DataField = 'SdoIni'
      DataSource = DataSource1
      TabOrder = 5
    end
    object CkContinuar: TCheckBox
      Left = 16
      Top = 100
      Width = 425
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 6
    end
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'WHERE Codigo IN ('
      '  SELECT ForneceI '
      '  FROM carteiras'
      '  WHERE ForneceI <> 0)'
      'ORDER BY NOMEENT')
    Left = 128
    Top = 188
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 156
    Top = 188
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo>0'
      'ORDER BY Nome')
    Left = 192
    Top = 188
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 220
    Top = 188
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SdoIni'
      'FROM contassdo'
      'WHERE Codigo=:P0'
      'AND Entidade=:P1')
    Left = 256
    Top = 188
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPesqSdoIni: TFloatField
      FieldName = 'SdoIni'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DataSource1: TDataSource
    DataSet = QrPesq
    Left = 268
    Top = 100
  end
end
