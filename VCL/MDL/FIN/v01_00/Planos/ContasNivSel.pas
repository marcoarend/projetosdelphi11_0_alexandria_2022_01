unit ContasNivSel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkDBGrid, Db,
  mySQLDbTables, Variants, UnDmkEnums;

type
  TFmContasNivSel = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    dmkDBGrid1: TdmkDBGrid;
    QrSdoNiveis: TmySQLQuery;
    DsSdoNiveis: TDataSource;
    CkControla: TCheckBox;
    QrSdoNiveisNivel: TIntegerField;
    QrSdoNiveisGenero: TIntegerField;
    QrSdoNiveisNomeGe: TWideStringField;
    QrSdoNiveisNomeNi: TWideStringField;
    QrSdoNiveisSumMov: TFloatField;
    QrSdoNiveisSdoAnt: TFloatField;
    QrSdoNiveisSumCre: TFloatField;
    QrSdoNiveisSumDeb: TFloatField;
    QrSdoNiveisSdoFim: TFloatField;
    QrSdoNiveisSeleci: TSmallintField;
    CkSeleci: TCheckBox;
    QrSdoNiveisCtrla: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dmkDBGrid1CellClick(Column: TColumn);
    procedure CkControlaClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenSdoNiveis(Nivel, Genero: Integer);
  public
    { Public declarations }
  end;

  var
  FmContasNivSel: TFmContasNivSel;

implementation

uses UnMyObjects, Module, UnFinanceiro, ContasNiv, dmkGeral, UMySQLModule, ModuleGeral;

{$R *.DFM}

procedure TFmContasNivSel.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasNivSel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContasNivSel.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmContasNivSel.ReopenSdoNiveis(Nivel, Genero: Integer);
begin
  QrSdoNiveis.Close;
  QrSdoNiveis.Params[0].AsInteger :=
    MLAGeral.BoolToInt2(CkSeleci.Checked, 0, -1);
  QrSdoNiveis.Params[1].AsInteger :=
    MLAGeral.BoolToInt2(CkControla.Checked, 0, -1);
  QrSdoNiveis.Open;
  //
  QrSdoNiveis.Locate('Nivel;Genero', VarArrayOf([Nivel,Genero]), [])
end;

procedure TFmContasNivSel.FormCreate(Sender: TObject);
begin
  ReopenSdoNiveis(0,0);
end;

procedure TFmContasNivSel.dmkDBGrid1CellClick(Column: TColumn);
var
  Seleci: Integer;
begin
  if (dmkDBGrid1.SelectedField.Name = 'QrSdoNiveisSeleci') then
  begin
    if QrSdoNiveisSeleci.Value = 0 then Seleci := 1 else Seleci := 0;
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('UPDATE sdoniveis SET Seleci=:P0');
    DModG.QrUpdPID1.SQL.Add('WHERE Nivel=:P1');
    DModG.QrUpdPID1.SQL.Add('AND Genero=:P2');
    //
    DModG.QrUpdPID1.Params[00].AsInteger := Seleci;
    DModG.QrUpdPID1.Params[01].AsInteger := QrSdoNiveisNivel.Value;
    DModG.QrUpdPID1.Params[02].AsInteger := QrSdoNiveisGenero.Value;
    DModG.QrUpdPID1.ExecSQL;
    //
    ReopenSdoNiveis(QrSdoNiveisNivel.Value, QrSdoNiveisGenero.Value);
  end;
end;

procedure TFmContasNivSel.CkControlaClick(Sender: TObject);
begin
  ReopenSdoNiveis(QrSdoNiveisNivel.Value, QrSdoNiveisGenero.Value);
end;

procedure TFmContasNivSel.BtOKClick(Sender: TObject);
begin
  QrSdoNiveis.First;
  while not QrSdoNiveis.Eof do
  begin
    if QrSdoNiveisSeleci.Value = 1 then
    begin
      UMyMod.SQLInsUpd_IGNORE(Dmod.QrUpd, stIns, 'contasniv', False, [],
      ['Entidade', 'Nivel', 'Genero'], [], [
        FmContasNiv.QrEmpresasCodigo.Value,
        QrSdoNiveisNivel.Value, QrSdoNiveisGenero.Value], True);
    end;
    QrSdoNiveis.Next;
  end;
  FmContasNiv.ReopenContasNiv(QrSdoNiveisNivel.Value, QrSdoNiveisGenero.Value);
  Close;
end;

end.
