object FmContasLnkEdit: TFmContasLnkEdit
  Left = 396
  Top = 181
  Caption = 'FIN-PLCTA-024 :: Link de Conta (Concilia'#231#227'o'
  ClientHeight = 386
  ClientWidth = 688
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 40
    Width = 688
    Height = 298
    Align = alClient
    TabOrder = 0
    object LaConta: TLabel
      Left = 12
      Top = 4
      Width = 131
      Height = 13
      Caption = 'Conta (do plano de contas):'
    end
    object Label2: TLabel
      Left = 348
      Top = 4
      Width = 44
      Height = 13
      Caption = 'Hist'#243'rico:'
    end
    object Label3: TLabel
      Left = 576
      Top = 4
      Width = 58
      Height = 13
      Caption = 'Documento:'
    end
    object Label4: TLabel
      Left = 12
      Top = 48
      Width = 127
      Height = 13
      Caption = 'Entidade: (zero para todas)'
    end
    object Label5: TLabel
      Left = 348
      Top = 48
      Width = 116
      Height = 13
      Caption = 'Banco: (zero para todos)'
    end
    object Label1: TLabel
      Left = 12
      Top = 92
      Width = 141
      Height = 13
      Caption = 'Fornecedor: (zero para v'#225'rios)'
    end
    object Label6: TLabel
      Left = 348
      Top = 92
      Width = 119
      Height = 13
      Caption = 'Cliente: (zero para v'#225'rios)'
    end
    object CBConta: TdmkDBLookupComboBox
      Left = 80
      Top = 20
      Width = 264
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContas
      TabOrder = 1
      dmkEditCB = EdConta
      UpdType = utYes
    end
    object EdConta: TdmkEditCB
      Left = 12
      Top = 20
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBConta
    end
    object EdTexto: TdmkEdit
      Left = 348
      Top = 20
      Width = 225
      Height = 21
      MaxLength = 50
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdDoc: TdmkEdit
      Left = 576
      Top = 20
      Width = 101
      Height = 21
      MaxLength = 20
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdCliInt: TdmkEditCB
      Left = 12
      Top = 64
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCliInt
    end
    object CBCliInt: TdmkDBLookupComboBox
      Left = 80
      Top = 64
      Width = 264
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENTI'
      ListSource = DsCliInt
      TabOrder = 5
      dmkEditCB = EdCliInt
      UpdType = utYes
    end
    object EdBanco: TdmkEditCB
      Left = 348
      Top = 64
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBBanco
    end
    object CBBanco: TdmkDBLookupComboBox
      Left = 416
      Top = 64
      Width = 265
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsBancos
      TabOrder = 7
      dmkEditCB = EdBanco
      UpdType = utYes
    end
    object EdFornece: TdmkEditCB
      Left = 12
      Top = 108
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBFornece
    end
    object CBFornece: TdmkDBLookupComboBox
      Left = 80
      Top = 108
      Width = 264
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENTI'
      ListSource = DsFornece
      TabOrder = 9
      dmkEditCB = EdFornece
      UpdType = utYes
    end
    object EdCliente: TdmkEditCB
      Left = 348
      Top = 108
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 10
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCliente
    end
    object CBCliente: TdmkDBLookupComboBox
      Left = 416
      Top = 108
      Width = 265
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENTI'
      ListSource = DsCliente
      TabOrder = 11
      dmkEditCB = EdCliente
      UpdType = utYes
    end
    object CkUsaEntBank: TdmkCheckBox
      Left = 12
      Top = 136
      Width = 669
      Height = 17
      Caption = 
        'Usar como fornecedor e/ou cliente, a entidade banc'#225'ria informada' +
        ' no cadastro do banco.'
      TabOrder = 12
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
    object CGComposHist: TdmkCheckGroup
      Left = 12
      Top = 156
      Width = 669
      Height = 77
      Caption = 
        ' Composi'#231#227'o da descri'#231#227'o (Hist'#243'rico) para gera'#231#227'o do lan'#231'amento:' +
        ' '
      Items.Strings = (
        'Descri'#231#227'o do extrato'
        'Descri'#231#227'o da conta (Plano de contas)'
        'Nome do Terceiro (Forneceor / Cliente)')
      TabOrder = 13
      UpdType = utYes
      Value = 0
      OldValor = 0
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 338
    Width = 688
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 14
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
      NumGlyphs = 2
    end
    object BtDesiste: TBitBtn
      Tag = 15
      Left = 582
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 688
    Height = 40
    Align = alTop
    Caption = 'Link de Conta (Concilia'#231#227'o)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TdmkLabel
      Left = 605
      Top = 1
      Width = 82
      Height = 38
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 604
      ExplicitTop = 2
      ExplicitHeight = 36
    end
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 604
      Height = 38
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 602
      ExplicitHeight = 36
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 236
    Top = 58
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'ORDER BY Nome')
    Left = 208
    Top = 58
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMEENTI'
      'FROM entidades'
      'WHERE Codigo<10'
      'OR CliInt<>0'
      'ORDER BY NOMEENTI')
    Left = 208
    Top = 98
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 236
    Top = 98
  end
  object QrBancos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM bancos'
      'ORDER BY Nome')
    Left = 476
    Top = 98
    object QrBancosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsBancos: TDataSource
    DataSet = QrBancos
    Left = 504
    Top = 98
  end
  object QrFornece: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMEENTI'
      'FROM entidades'
      'WHERE Fornece1="V"'
      'OR Fornece2="V"'
      'OR Fornece3="V"'
      'OR Fornece4="V"'
      'OR Fornece5="V"'
      'OR Fornece6="V"'
      'ORDER BY NOMEENTI')
    Left = 208
    Top = 146
    object QrForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object DsFornece: TDataSource
    DataSet = QrFornece
    Left = 236
    Top = 146
  end
  object QrCliente: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMEENTI'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'OR Cliente2="V"'
      'OR Cliente3="V"'
      'OR Cliente4="V"'
      'ORDER BY NOMEENTI')
    Left = 476
    Top = 142
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object DsCliente: TDataSource
    DataSet = QrCliente
    Left = 504
    Top = 142
  end
end
