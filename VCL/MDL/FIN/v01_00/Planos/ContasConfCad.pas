unit ContasConfCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, DB, mySQLDbTables,
  dmkGeral, dmkDBGrid, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkImage, dmkLabel, UnDmkEnums;

type
  TFmContasConfCad = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel3: TPanel;
    PnEdita: TPanel;
    PnNomeCond: TPanel;
    QrConfPgtos: TmySQLQuery;
    DsConfPgtos: TDataSource;
    dmkDBGrid1: TdmkDBGrid;
    QrConfPgtosCodigo: TIntegerField;
    QrConfPgtosQtdeMin: TIntegerField;
    QrConfPgtosQtdeMax: TIntegerField;
    QrConfPgtosValrMin: TFloatField;
    QrConfPgtosValrMax: TFloatField;
    QrConfPgtosNOMECONTA: TWideStringField;
    PnControle: TPanel;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    PnConfirma: TPanel;
    BitBtn1: TBitBtn;
    Panel6: TPanel;
    BitBtn2: TBitBtn;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    EdQtdeMin: TdmkEdit;
    EdQtdeMax: TdmkEdit;
    EdValrMin: TdmkEdit;
    EdValrMax: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DsContas: TDataSource;
    LaTipo: TdmkLabel;
    CkContinuar: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenContas(EntInt: Integer);
    //procedure TravaForm();
  public
    { Public declarations }
    procedure ReopenConfPgtos(EntInt: Integer);
  end;

  var
  FmContasConfCad: TFmContasConfCad;

implementation

uses UnMyObjects, Module, Principal, UMySQLModule;

{$R *.DFM}

procedure TFmContasConfCad.BitBtn1Click(Sender: TObject);
var
  Codigo, CliInt, QtdeMin, QtdeMax: Integer;
  ValrMin, ValrMax: Double;
begin
  Codigo  := EdConta.ValueVariant;
  if Codigo < 1 then
  begin
    Application.MessageBox('Informe a conta do plano de contas!',
    'Aviso', MB_OK+MB_ICONWARNING);
    EdConta.SetFocus;
    Exit;
  end;
  CliInt  := FmPrincipal.FEntInt;
  if CliInt = 0 then
  begin
    Application.MessageBox('Nenhum cliente interno foi informado!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  QtdeMin := EdQtdeMin.ValueVariant;
  QtdeMax := EdQtdeMax.ValueVariant;
  ValrMin := EdValrMin.ValueVariant;
  ValrMax := EdValrMax.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'confpgtos', False, [
    'QtdeMin', 'QtdeMax', 'ValrMin', 'ValrMax'
  ], ['Codigo', 'CliInt'], [
    QtdeMin, QtdeMax, ValrMin, ValrMax
  ], [Codigo, CliInt], True) then
  begin
    EdConta.ValueVariant   := 0;
    CBConta.KeyValue       := 0;
    EdQtdeMin.ValueVariant := 0;
    EdQtdeMax.ValueVariant := 0;
    EdValrMin.ValueVariant := 0;
    EdValrMax.ValueVariant := 0;
    UMyMod.TravaFmEmPanelInsUpd([PnEdita],[PnControle], TdmkImage(LaTipo));
    ReopenConfPgtos(FmPrincipal.FEntInt);
    if CkContinuar.Checked then
      BtIncluiClick(Self);
  end;
end;

procedure TFmContasConfCad.BitBtn2Click(Sender: TObject);
begin
  UMyMod.TravaFmEmPanelInsUpd([PnEdita],[PnControle], TdmkImage(LaTipo));
end;

procedure TFmContasConfCad.BtAlteraClick(Sender: TObject);
begin
  EdConta.Enabled := False;
  CBConta.Enabled := False;
  ReopenContas(0);
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrConfPgtos, [PnControle],
    [PnEdita], EdQtdeMin, LaTipo, 'confpgtos');
end;

procedure TFmContasConfCad.BtIncluiClick(Sender: TObject);
begin
  // Deve ser antes para evitar erro
  EdConta.Enabled := True;
  CBConta.Enabled := True;
  ReopenContas(FmPrincipal.FEntInt);
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrConfPgtos, [PnControle],
    [PnEdita], EdConta, LaTipo, 'confpgtos');
end;

procedure TFmContasConfCad.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasConfCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContasConfCad.FormCreate(Sender: TObject);
begin
  ReopenConfPgtos(FmPrincipal.FEntInt);
end;

procedure TFmContasConfCad.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmContasConfCad.ReopenConfPgtos(EntInt: Integer);
begin
  QrConfPgtos.Close;
  QrConfPgtos.Params[0].AsInteger := EntInt;
  QrConfPgtos.Open;
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT IF(Tipo=0,RazaoSocial,Nome) NOMECLI');
  Dmod.QrAux.SQL.Add('FROM entidades WHERE Codigo=' + FormatFloat('0', EntInt));
  Dmod.QrAux.Open;
  PnNomeCond.Caption := Dmod.QrAux.FieldByName('NOMECLI').AsString;
  //
end;

procedure TFmContasConfCad.ReopenContas(EntInt: Integer);
begin
  QrContas.Close;
  QrContas.SQL.Clear;
  QrContas.SQL.Add('SELECT Codigo, Nome');
  QrContas.SQL.Add('FROM contas');
  QrContas.SQL.Add('WHERE Debito="V"');
  if EntInt <> 0 then
  begin
    QrContas.SQL.Add('AND NOT (Codigo IN (');
    QrContas.SQL.Add('  SELECT Codigo FROM confpgtos');
    QrContas.SQL.Add('  WHERE CliInt=' + FormatFloat('0', EntInt));
    QrContas.SQL.Add('))');
  end;
  QrContas.SQL.Add('ORDER BY Nome');
  QrContas.SQL.Add('');
  QrContas.Open;
end;

{
procedure TFmContasConfCad.TravaForm;
begin
  PnControle.Visible := True;
  PnEdita.Visible    := False;
  LaTipo.SQLType     := stLok;
end;
}

end.

