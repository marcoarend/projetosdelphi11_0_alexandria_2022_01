unit ContasSdoAll;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, Mask,
  Grids, DBGrids, frxClass, frxDBSet, MySQLMacroQuery, Menus, ComCtrls, dmkGeral,
  dmkDBGridDAC, Variants, dmkPermissoes, dmkDBLookupComboBox, dmkEdit, dmkEditCB;

type
  THackDBGrid = class(TDBGrid);
  TFmContasSdoAll = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENT: TWideStringField;
    DsContasSdo: TDataSource;
    frxLista: TfrxReport;
    frxDsListaCtaSdo: TfrxDBDataset;
    PainelPesquisa: TPanel;
    SbImprime: TBitBtn;
    QrPesq: TmySQLQuery;
    QrPesqSdoIni: TFloatField;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    PMImprime: TPopupMenu;
    Listadapesquisa1: TMenuItem;
    Outrasimpresses1: TMenuItem;
    QrSCart: TmySQLQuery;
    QrSCartInicial: TFloatField;
    DsSCart: TDataSource;
    QrMCart: TmySQLQuery;
    DsMCart: TDataSource;
    QrMCartMOVTO: TFloatField;
    QrMCartATUAL: TFloatField;
    QrFCart: TmySQLQuery;
    QrFCartMOVTO: TFloatField;
    DsFCart: TDataSource;
    QrFCartSALDO: TFloatField;
    QrSCtas: TmySQLQuery;
    QrSCtasSdoIni: TFloatField;
    QrSCtasSdoAtu: TFloatField;
    QrSCtasSdoFut: TFloatField;
    DsSCtas: TDataSource;
    GroupBox1: TGroupBox;
    DBEdit6: TDBEdit;
    Label9: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label8: TLabel;
    Panel3: TPanel;
    Label3: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdCodigo: TdmkEditCB;
    Label4: TLabel;
    EdxxxSdoIni: TdmkEdit;
    CBCodigo: TdmkDBLookupComboBox;
    Label10: TLabel;
    DBEdit7: TDBEdit;
    Label11: TLabel;
    DBEdit8: TDBEdit;
    BtAtzSdoEnt: TBitBtn;
    ProgressBar1: TProgressBar;
    BtInclui: TBitBtn;
    QrContasSdo: TmySQLQuery;
    QrContasSdoCodigo: TIntegerField;
    QrContasSdoNOMECTA: TWideStringField;
    QrContasSdoNOMEPLA: TWideStringField;
    QrContasSdoNOMECNJ: TWideStringField;
    QrContasSdoNOMEGRU: TWideStringField;
    QrContasSdoNOMESGR: TWideStringField;
    QrContasSdoSdoIni: TFloatField;
    QrContasSdoSdoAtu: TFloatField;
    QrContasSdoSdoFut: TFloatField;
    QrContasSdoEntidade: TIntegerField;
    QrContasSdoNOMEENTI: TWideStringField;
    Button1: TButton;
    dmkDBGridDAC1: TdmkDBGridDAC;
    Memo1: TMemo;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdEntidadeChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TbContasSdoBeforeEdit(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxListaGetValue(const VarName: String; var Value: Variant);
    procedure Listadapesquisa1Click(Sender: TObject);
    procedure Outrasimpresses1Click(Sender: TObject);
    procedure QrMCartCalcFields(DataSet: TDataSet);
    procedure QrFCartCalcFields(DataSet: TDataSet);
    procedure BtAtzSdoEntClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure dmkDBGridDAC1AfterSQLExec(Sender: TObject);
  private
    { Private declarations }
    FOrd_idx, FOrd_seq: String;

    procedure Refiltra(Reopen: Boolean; LocCodigo, LocEntidade: Integer);
    procedure ReabreComparacoes(Entidade: Integer);
    procedure AtualizaSaldosAtualEFuturo();

  public
    { Public declarations }
  end;

  var
  FmContasSdoAll: TFmContasSdoAll;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyGlyfs, Principal, ModuleGeral,
UnFinanceiro;

{$R *.DFM}

procedure TFmContasSdoAll.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasSdoAll.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  Refiltra(True, -1000, -1000);
end;

procedure TFmContasSdoAll.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmContasSdoAll.EdEntidadeChange(Sender: TObject);
var
  Entidade: Integer;
begin
  Entidade := Geral.IMV(EdEntidade.Text);
  //
  BtAtzSdoEnt.Enabled := Entidade <> 0;
  ReabreComparacoes(Entidade);
  Refiltra(False, -1000, -1000);
end;

procedure TFmContasSdoAll.FormCreate(Sender: TObject);
begin
  FOrd_idx := 'NOMECTA';
  FOrd_seq := CO_ASC;
  QrEntidades.Open;
  QrContas.Open;
end;

procedure TFmContasSdoAll.Refiltra(Reopen: Boolean; LocCodigo, LocEntidade: Integer);
var
  Filtro: String;
  Entidade, Codigo: Integer;
  LocCod, LocEnt: Integer;
begin
  if LocCodigo <> -1000 then
    LocCod := LocCodigo
  else
    if QrContasSdo.State = dsBrowse then
    LocCod := QrContasSdoCodigo.Value
  else LocCod := 0;
  //
  if LocEntidade <> -1000 then
    LocEnt := LocEntidade
  else
    if QrContasSdo.State = dsBrowse then
  LocEnt := QrContasSdoEntidade.Value
    else LocEnt := 0;
  //
  Filtro   := '';
  Entidade := Geral.IMV(EdEntidade.Text);
  Codigo   := Geral.IMV(EdCodigo.Text);
  if (Entidade <> 0) and (Codigo <> 0) then Filtro :=
    '(Entidade=' + IntToStr(Entidade) + ') AND (' + IntToStr(Codigo) + ')'
  else if Entidade <> 0 then Filtro := 'Entidade=' + IntToStr(Entidade)
  else if Codigo <> 0 then Filtro := '(Codigo=' + IntToStr(Codigo);
  if QrContasSdo.Filter <> Filtro then
    QrContasSdo.Filter := Filtro;
  //
  if Reopen then QrContasSdo.Close;
  //
  if QrContasSdo.State = dsInactive then
    QrContasSdo.Open;
  QrContasSdo.Locate('Codigo;Entidade', VarArrayOf([LocCod, LocEnt]), []);
end;

procedure TFmContasSdoAll.TbContasSdoBeforeEdit(DataSet: TDataSet);
begin
  //FSdoIni := TbContasSdoSdoIni.Value;
end;

procedure TFmContasSdoAll.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmContasSdoAll.frxListaGetValue(const VarName: String;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_ENTIDADE') = 0 then
    Value := CBEntidade.Text;
  if AnsiCompareText(VarName, 'VARF_CONTA') = 0 then
    Value := CBCodigo.Text;

end;

procedure TFmContasSdoAll.Listadapesquisa1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxLista, 'Lista de saldos de contas');
end;

procedure TFmContasSdoAll.Outrasimpresses1Click(Sender: TObject);
begin
  UFinanceiro.ImprimePlanoContas();
end;

procedure TFmContasSdoAll.ReabreComparacoes(Entidade: Integer);
begin
  QrSCart.Close;
  QrSCart.Params[0].AsInteger := Entidade;
  QrSCart.Open;
  //
  QrMCart.Close;
  QrMCart.SQL.Clear;
  QrMCart.SQL.Add('SELECT SUM(lan.Credito-lan.Debito) MOVTO');
  QrMCart.SQL.Add('FROM ' + VAR_LCT + ' lan');
  QrMCart.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrMCart.SQL.Add('WHERE car.Tipo <> 2');
  QrMCart.SQL.Add('AND lan.Genero > 0');
  QrMCart.SQL.Add('AND car.ForneceI=:P0');
  QrMCart.Params[0].AsInteger := Entidade;
  QrMCart.Open;
  //
  QrFCart.Close;
  QrFCart.SQL.Clear;
  QrFCart.SQL.Add('SELECT SUM(lan.Credito-lan.Debito-lan.Pago) MOVTO');
  QrFCart.SQL.Add('FROM ' + VAR_LCT + ' lan');
  QrFCart.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrFCart.SQL.Add('WHERE car.Tipo = 2');
  QrFCart.SQL.Add('AND lan.Sit < 2');
  QrFCart.SQL.Add('AND lan.Genero > 0');
  QrFCart.SQL.Add('AND car.ForneceI=:P0');
  QrFCart.Params[0].AsInteger := Entidade;
  QrFCart.Open;
  //
  QrSCtas.Close;
  QrSCtas.Params[0].AsInteger := Entidade;
  QrSCtas.Open;
  //
end;

procedure TFmContasSdoAll.QrMCartCalcFields(DataSet: TDataSet);
begin
  QrMCartATUAL.Value := QrSCartInicial.Value + QrMCartMOVTO.Value;
end;

procedure TFmContasSdoAll.QrFCartCalcFields(DataSet: TDataSet);
begin
  QrFCartSALDO.Value := QrMCartATUAL.Value + QrFCartMOVTO.Value;
end;

procedure TFmContasSdoAll.BtAtzSdoEntClick(Sender: TObject);
{var
  Entidade: Integer;
begin
  Entidade := Geral.IMV(EdEntidade.Text);
  DModG.AtualizaContasEntidade(Entidade, 0, ProgressBar1, nil);
}
begin
  Refiltra(True, QrContasSdoCodigo.Value, QrContasSdoEntidade.Value);
end;

procedure TFmContasSdoAll.BtIncluiClick(Sender: TObject);
var
  Entidade, Conta: Integer;
begin
  Entidade := Geral.IMV(EdEntidade.Text);
  Conta    := Geral.IMV(EdCodigo.Text);
  if FmPrincipal.CadastroDeContasSdoSimples(Entidade, Conta) then
    Refiltra(True, Entidade, Conta);
end;

procedure TFmContasSdoAll.Button1Click(Sender: TObject);
begin
  Memo1.Text := dmkDBGridDAC1.SQL.Text;
end;

procedure TFmContasSdoAll.AtualizaSaldosAtualEFuturo();
var
  SdoIni, SdoAtu, SdoFut: Double;
begin
  Screen.Cursor := crHourGlass;
  //
  SdoIni := Geral.DMV(dmkDBGridDAC1.EditText);
  SdoAtu := QrContasSdoSdoAtu.Value - QrContasSdoSdoIni.Value + SdoIni;
  SdoFut := QrContasSdoSdoFut.Value - QrContasSdoSdoIni.Value + SdoIni;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE contassdo SET ');
  Dmod.QrUpd.SQL.Add('SdoIni=:P0, SdoAtu=:P1, SdoFut=:P2 ');
  //
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa AND Entidade=:Pb');
  Dmod.QrUpd.SQL.Add('');
  //
  Dmod.QrUpd.Params[00].AsFloat   := SdoIni;
  Dmod.QrUpd.Params[01].AsFloat   := SdoAtu;
  Dmod.QrUpd.Params[02].AsFloat   := SdoFut;
  //
  Dmod.QrUpd.Params[03].AsInteger := QrContasSdoCodigo.Value;
  Dmod.QrUpd.Params[04].AsInteger := QrContasSdoEntidade.Value;
  //
  Dmod.QrUpd.ExecSQL;
  //
  if QrContasSdoEntidade.Value = Geral.IMV(EdEntidade.Text) then
    ReabreComparacoes(QrContasSdoEntidade.Value);
  //
  Refiltra(True, QrContasSdoCodigo.Value, QrContasSdoEntidade.Value);
  //
  Screen.Cursor := crDefault;
end;

procedure TFmContasSdoAll.dmkDBGridDAC1AfterSQLExec(Sender: TObject);
begin
  AtualizaSaldosAtualEFuturo;
end;

end.
