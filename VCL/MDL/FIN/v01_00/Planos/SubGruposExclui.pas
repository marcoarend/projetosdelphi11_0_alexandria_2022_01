unit SubGruposExclui;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, Grids, DBGrids,
  Variants, dmkPermissoes, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral;

type
  TFmSubGruposExclui = class(TForm)
    PainelDados: TPanel;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    BtSaida: TBitBtn;
    DsSubgrupos: TDataSource;
    QrSubGrupos: TmySQLQuery;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Label1: TLabel;
    EdSubgrupo: TdmkEditCB;
    CBSubGrupo: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdNova: TdmkEditCB;
    CBNova: TdmkDBLookupComboBox;
    Label3: TLabel;
    QrSubGruposCodigo: TIntegerField;
    QrSubGruposNome: TWideStringField;
    DsNova: TDataSource;
    QrNova: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsContas: TDataSource;
    QrContas: TmySQLQuery;
    DBGLct: TDBGrid;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdSubgrupoChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure QrSubGruposAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmSubGruposExclui: TFmSubGruposExclui;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmSubGruposExclui.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSubGruposExclui.BtConfirmaClick(Sender: TObject);
var
  Nova, Subgrupo: Integer;
begin
  if CBSubgrupo.KeyValue = Null then
  begin
    Application.MessageBox(PChar('Informe um sub-grupo a excluir!'), 'Erro', MB_OK+MB_ICONERROR);
    EdSubgrupo.SetFocus;
    Exit;
  end;
  Subgrupo := Geral.IMV(EdSubgrupo.Text);
  if (CBNova.KeyValue = Null) and (QrContas.RecordCount > 0) then
  begin
    Application.MessageBox(PChar('Informe um sub-grupo para transferência!'),
    'Erro', MB_OK+MB_ICONERROR);
    EdNova.SetFocus;
    Exit;
  end;
  Nova := Geral.IMV(EdNova.Text);
  if Application.MessageBox(PChar('Confirma a exclusão do sub-grupo "'+
  CBSubgrupo.Text+'"?'), 'Erro', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then Exit;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE contas SET Subgrupo=:P0 WHERE Subgrupo=:P1');
  Dmod.QrUpd.Params[0].AsInteger := Nova;
  Dmod.QrUpd.Params[1].AsInteger := Subgrupo;
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM subgrupos WHERE Codigo=:P0');
  Dmod.QrUpd.Params[0].AsInteger := Subgrupo;
  Dmod.QrUpd.ExecSQL;
  //
  UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Subgrupos', Subgrupo);
  Close;
end;

procedure TFmSubGruposExclui.EdSubgrupoChange(Sender: TObject);
begin
  if Length(Trim(EdSubgrupo.Text)) = 0 then QrContas.Close;
end;

procedure TFmSubGruposExclui.FormCreate(Sender: TObject);
begin
  QrSubgrupos.Open;
end;

procedure TFmSubGruposExclui.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1,PainelTitulo, True,0);
end;

procedure TFmSubGruposExclui.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSubGruposExclui.QrSubGruposAfterScroll(DataSet: TDataSet);
begin
  if CBSubgrupo.KeyValue <> Null then
  begin
    EdNova.Text := '';
    CBNova.KeyValue := Null;
    QrNova.Close;
    QrNova.Params[0].AsInteger := QrSubgruposCodigo.Value;
    QrNova.Open;
    //
    QrContas.Close;
    QrContas.Params[0].AsInteger := QrSubgruposCodigo.Value;
    QrContas.Open;
  end;
end;

end.

