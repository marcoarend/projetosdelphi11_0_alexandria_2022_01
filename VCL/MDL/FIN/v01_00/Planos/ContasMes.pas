unit ContasMes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, dmkEdit,
  dmkDBLookupComboBox, dmkEditCB, dmkLabel, dmkGeral, dmkValUsu, UnDmkEnums;

type
  TFmContasMes = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    Label1: TLabel;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdDescricao: TdmkEdit;
    Edit1: TdmkEdit;
    EdPeriodoIni: TdmkEdit;
    Label5: TLabel;
    Label16: TLabel;
    Edit2: TdmkEdit;
    EdPeriodoFim: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    EdCodigo: TdmkEditCB;
    CBCodigo: TdmkDBLookupComboBox;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    GroupBox1: TGroupBox;
    EdValorMin: TdmkEdit;
    Label7: TLabel;
    EdValorMax: TdmkEdit;
    Label8: TLabel;
    LaTipo: TdmkLabel;
    QrCliIntCodigo: TIntegerField;
    QrCliIntNO_UF: TWideStringField;
    QrCliIntCO_SHOW: TLargeintField;
    QrCliIntNO_EMPRESA: TWideStringField;
    QrCliIntCIDADE: TWideStringField;
    VUCliInt: TdmkValUsu;
    GroupBox2: TGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    EdQtdeMin: TdmkEdit;
    EdQtdeMax: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdPeriodoIniChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdPeriodoFimChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdDescricaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    //procedure MudaPeriodo(Key: Word);
  public
    { Public declarations }
    FControle: Integer;
    FQuery: TmySQLQuery;
    procedure ReopenQueryOutroForm(Controle: Integer);
  end;

  var
  FmContasMes: TFmContasMes;

implementation

uses UnMyObjects, UMySQLModule, Module, Contas;

{$R *.DFM}

procedure TFmContasMes.BtOKClick(Sender: TObject);
var
  Codigo, CliInt, PeriodoIni, PeriodoFim, Controle, QtdeMin, QtdeMax: Integer;
  Descricao: String;
  ValorMin, ValorMax: Double;
begin
  Codigo     := EdCodigo.ValueVariant;
  CliInt     := QrCliIntCodigo.Value;//EdCliInt.ValueVariant;
  PeriodoIni := EdPeriodoIni.ValueVariant;
  PeriodoFim := EdPeriodoFim.ValueVariant;
  Descricao  := EdDescricao.ValueVariant;
  ValorMin   := EdValorMin.ValueVariant;
  ValorMax   := EdValorMax.ValueVariant;
  QtdeMin    := EdQtdeMin.ValueVariant;
  QtdeMax    := EdQtdeMax.ValueVariant;
  Controle   := UMyMod.BuscaEmLivreY_Def('contasmes', 'Controle', LaTipo.SQLType, FControle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'contasmes', False, [
  'Codigo', 'CliInt', 'Descricao',
  'PeriodoIni', 'PeriodoFim', 'ValorMin',
  'ValorMax', 'QtdeMin', 'QtdeMax'], [
  'Controle'], [
  Codigo, CliInt, Descricao,
  PeriodoIni, PeriodoFim, ValorMin,
  ValorMax, QtdeMin, QtdeMax], [
  Controle], True) then
  begin
    ReopenQueryOutroForm(Controle);
    {
    if FForm = 'FmContas' then
      FmContas.ReopenContasMes(Controle)
    else
    if FForm = 'FmCond' then
      FmCond.ReopenContasMes(Controle)
    else;
    }
    Geral.MensagemBox('Item inserido com sucesso!', 'Aviso', MB_OK+MB_ICONWARNING);
    LaTipo.SQLType := stIns;
    EdCliInt.SetFocus;
  end;
end;

procedure TFmContasMes.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasMes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContasMes.FormCreate(Sender: TObject);
begin
  FControle := 0;
  QrContas.Open;
  QrCliInt.Open;
end;

procedure TFmContasMes.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmContasMes.EdDescricaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdDescricao.Text := CBCliInt.Text + ' - ' + CBCodigo.Text;
end;

procedure TFmContasMes.EdPeriodoFimChange(Sender: TObject);
begin
  Edit2.Text := MLAGeral.MesEAnoDoPeriodoLongo(EdPeriodoFim.ValueVariant);
end;

procedure TFmContasMes.EdPeriodoIniChange(Sender: TObject);
begin
  Edit1.Text := MLAGeral.MesEAnoDoPeriodoLongo(EdPeriodoIni.ValueVariant);
end;

{
procedure TFmContasMes.MudaPeriodo(Key: Word);
begin
  case key of
    VK_DOWN: dmkEdPeriodo.ValueVariant := dmkEdPeriodo.ValueVariant - 1;
    VK_UP: dmkEdPeriodo.ValueVariant := dmkEdPeriodo.ValueVariant + 1;
    VK_PRIOR: dmkEdPeriodo.ValueVariant := dmkEdPeriodo.ValueVariant - 12;
    VK_NEXT: dmkEdPeriodo.ValueVariant := dmkEdPeriodo.ValueVariant + 12;
  end;
end;
}

procedure TFmContasMes.ReopenQueryOutroForm(Controle: Integer);
begin
  if FQuery = nil then Exit;
  FQuery.Close;
  FQuery.Open;
  FQuery.Locate('Controle', Controle, []);
end;

end.
