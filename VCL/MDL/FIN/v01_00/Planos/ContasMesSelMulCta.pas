unit ContasMesSelMulCta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkDBGridDAC, DB,
  mySQLDbTables, dmkGeral, UnDmkEnums;

type
  TFmContasMesSelMulCta = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GradeContasMes: TdmkDBGridDAC;
    QrContasMes: TmySQLQuery;
    QrContasMesCodigo: TIntegerField;
    QrContasMesCodUsu: TIntegerField;
    QrContasMesNome: TWideStringField;
    QrContasMesAtivo: TSmallintField;
    DsContasMes: TDataSource;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    StaticText1: TStaticText;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure AtivarTodos(Ativo: Integer);
    procedure ReopenContasMes(Codigo: Integer);
  public
    { Public declarations }
    FCliIntCodi: Integer;
    FCliIntNome, FContasMes: String;
  end;

  var
  FmContasMesSelMulCta: TFmContasMesSelMulCta;

implementation

uses UnMyObjects, ModuleGeral, UnInternalConsts, UMySQLModule, Module, UCreate;

{$R *.DFM}

procedure TFmContasMesSelMulCta.AtivarTodos(Ativo: Integer);
var
  Codigo: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Codigo := QrContasMesCodigo.Value;
    DmodG.QrUpdPID1.Close;
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FContasMes + ' SET ');
    DmodG.QrUpdPID1.SQL.Add('Ativo=:P0');
    DmodG.QrUpdPID1.Params[00].AsInteger := Ativo;
    DmodG.QrUpdPID1.ExecSQL;
    //
    ReopenContasMes(Codigo);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmContasMesSelMulCta.BtNenhumClick(Sender: TObject);
begin
  AtivarTodos(0);
end;

procedure TFmContasMesSelMulCta.BtOKClick(Sender: TObject);
const
  PeriodoIni  = 0;
  PeriodoFim  = 0;
  QtdeMin     = 1;
  QtdeMax     = 1;
  ValorMin = 0.01;
  ValorMax = 0.01;
var
  Controle, Codigo, CliInt: Integer;
  Descricao: String;
begin
  Screen.Cursor := crHourGlass;
  try
    QrContasMes.First;
    while not QrContasMes.Eof do
    begin
      if QrContasMesAtivo.Value = 1 then
      begin
        Codigo := QrContasMesCodigo.Value;
        CliInt := FCliIntCodi;
        Descricao := FCliIntNome + ' - ' + QrContasMesNome.Value;
        Controle := UMyMod.BuscaEmLivreY_Def('contasmes', 'Controle', stIns, 0);
        UMyMod.SQLReplace(DMod.QrUpd, 'contasmes',
        ['Codigo', 'CliInt', 'Descricao',
        'PeriodoIni', 'PeriodoFim', 'ValorMin',
        'ValorMax', 'QtdeMin', 'QtdeMax'], [
        'Controle'], [
        Codigo, CliInt, Descricao,
        PeriodoIni, PeriodoFim, ValorMin,
        ValorMax, QtdeMin, QtdeMax], [
        Controle], True);
      end;
      QrContasMes.Next;
    end;
    Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmContasMesSelMulCta.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasMesSelMulCta.BtTodosClick(Sender: TObject);
begin
  AtivarTodos(1);
end;

procedure TFmContasMesSelMulCta.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContasMesSelMulCta.FormCreate(Sender: TObject);
begin
  FContasMes := UCriar.RecriaTempTable('Cad_0', DModG.QrUpdPID1, False);
  QrContasMes.Database := DmodG.MyPID_DB;
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO cad_0');
  DmodG.QrUpdPID1.SQL.Add('SELECT Codigo, Codigo CodUsu, Nome, 0 Ativo');
  DmodG.QrUpdPID1.SQL.Add('FROM ' + TMeuDB + '.contas');
  DmodG.QrUpdPID1.SQL.Add('WHERE Mensal = "V"');
  DmodG.QrUpdPID1.ExecSQL;
  //
  ReopenContasMes(0);
  //
end;

procedure TFmContasMesSelMulCta.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmContasMesSelMulCta.ReopenContasMes(Codigo: Integer);
begin
  QrContasMes.Close;
  UmyMod.AbreQuery(QrContasMes, DModG.MyPID_DB, 'TFmContasMesSelMulCta.AtivarTodos()');
  QrContasMes.Locate('Codigo', Codigo, []);
end;

end.
