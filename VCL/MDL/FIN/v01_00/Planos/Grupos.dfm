object FmGrupos: TFmGrupos
  Left = 369
  Top = 181
  Caption = 'FIN-PLCTA-003 :: Cadastro de Grupos de Contas'
  ClientHeight = 348
  ClientWidth = 704
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 704
    Height = 300
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelControle: TPanel
      Left = 1
      Top = 251
      Width = 702
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'ltimo registro'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
          NumGlyphs = 2
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Pr'#243'ximo registro'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
          NumGlyphs = 2
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Registro anterior'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
          NumGlyphs = 2
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Primeiro registro'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
          NumGlyphs = 2
        end
      end
      object Panel3: TPanel
        Left = 232
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 372
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
          NumGlyphs = 2
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Exclui banco atual'
          Caption = '&Exclui'
          Enabled = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          NumGlyphs = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Caption = '&Altera'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
          NumGlyphs = 2
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Inclui'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtIncluiClick
          NumGlyphs = 2
        end
      end
    end
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 702
      Height = 84
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 64
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object LaConjunto: TLabel
        Left = 428
        Top = 8
        Width = 45
        Height = 13
        Caption = 'Conjunto:'
      end
      object Label5: TLabel
        Left = 388
        Top = 8
        Width = 34
        Height = 13
        Caption = 'Ordem:'
      end
      object DBEdCodigo: TDBEdit
        Left = 16
        Top = 24
        Width = 45
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsGrupos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 64
        Top = 24
        Width = 321
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsGrupos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdConjunto: TDBEdit
        Left = 428
        Top = 24
        Width = 265
        Height = 21
        Color = clWhite
        DataField = 'NOMECONJUNTO'
        DataSource = DsGrupos
        TabOrder = 2
      end
      object DBEdit1: TDBEdit
        Left = 388
        Top = 24
        Width = 33
        Height = 21
        Color = clWhite
        DataField = 'OrdemLista'
        DataSource = DsGrupos
        TabOrder = 3
      end
      object DBCheckBox1: TDBCheckBox
        Left = 16
        Top = 52
        Width = 97
        Height = 17
        Caption = 'Controla saldo.'
        DataField = 'CtrlaSdo'
        DataSource = DsGrupos
        TabOrder = 4
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
    end
    object DBGrid1: TDBGrid
      Left = 28
      Top = 120
      Width = 537
      Height = 120
      DataSource = DsSubGrupos
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          ReadOnly = True
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OrdemLista'
          Title.Caption = 'Ordem lista'
          Visible = True
        end>
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 704
    Height = 300
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 251
      Width = 702
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Default = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
        NumGlyphs = 2
      end
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 584
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtDesisteClick
        NumGlyphs = 2
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 820
      Height = 196
      TabOrder = 0
      object Label9: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 64
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label3: TLabel
        Left = 16
        Top = 48
        Width = 45
        Height = 13
        Caption = 'Conjunto:'
      end
      object Label4: TLabel
        Left = 360
        Top = 48
        Width = 55
        Height = 13
        Caption = 'Ordem lista:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 24
        Width = 45
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNome: TEdit
        Left = 64
        Top = 24
        Width = 353
        Height = 21
        MaxLength = 255
        TabOrder = 1
      end
      object EdConjunto: TdmkEditCB
        Left = 16
        Top = 64
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBConjunto
      end
      object CBConjunto: TdmkDBLookupComboBox
        Left = 64
        Top = 64
        Width = 293
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsConjuntos
        TabOrder = 3
        dmkEditCB = EdConjunto
        UpdType = utYes
      end
      object EdOrdemLista: TdmkEdit
        Left = 360
        Top = 64
        Width = 57
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object CkCtrlaSdo: TCheckBox
        Left = 16
        Top = 92
        Width = 97
        Height = 17
        Caption = 'Controla saldo.'
        TabOrder = 5
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 704
    Height = 48
    Align = alTop
    Alignment = taLeftJustify
    Caption = '                             Cadastro de Grupos de Contas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TLabel
      Left = 621
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 620
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 395
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 393
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 0
        NumGlyphs = 2
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 1
        NumGlyphs = 2
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 2
        OnClick = SbNumeroClick
        NumGlyphs = 2
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 3
        OnClick = SbNomeClick
        NumGlyphs = 2
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 4
        OnClick = SbQueryClick
        NumGlyphs = 2
      end
    end
  end
  object DsGrupos: TDataSource
    DataSet = QrGrupos
    Left = 488
    Top = 161
  end
  object QrGrupos: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrGruposBeforeOpen
    AfterOpen = QrGruposAfterOpen
    AfterScroll = QrGruposAfterScroll
    SQL.Strings = (
      'SELECT gr.*, cj.Nome NOMECONJUNTO'
      'FROM grupos gr'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'WHERE gr.Codigo > 0')
    Left = 460
    Top = 161
    object QrGruposLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'grupos.Lk'
    end
    object QrGruposDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'grupos.DataCad'
    end
    object QrGruposDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'grupos.DataAlt'
    end
    object QrGruposUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'grupos.UserCad'
    end
    object QrGruposUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'grupos.UserAlt'
    end
    object QrGruposCodigo: TSmallintField
      FieldName = 'Codigo'
      Origin = 'grupos.Codigo'
    end
    object QrGruposConjunto: TIntegerField
      FieldName = 'Conjunto'
      Origin = 'grupos.Conjunto'
      Required = True
    end
    object QrGruposNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Origin = 'conjuntos.Nome'
      Size = 50
    end
    object QrGruposOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
      Origin = 'grupos.OrdemLista'
      Required = True
    end
    object QrGruposNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'grupos.Nome'
      Required = True
      Size = 50
    end
    object QrGruposCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
      Origin = 'grupos.CtrlaSdo'
      Required = True
    end
  end
  object TbSubGrupos: TmySQLTable
    Database = Dmod.MyDB
    Filtered = True
    SortFieldNames = 'OrdemLista,Nome'
    TableName = 'subgrupos'
    Left = 460
    Top = 189
    object TbSubGruposCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object TbSubGruposNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object TbSubGruposOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
    end
    object TbSubGruposGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object TbSubGruposTipoAgrupa: TIntegerField
      FieldName = 'TipoAgrupa'
    end
    object TbSubGruposLk: TIntegerField
      FieldName = 'Lk'
    end
    object TbSubGruposDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbSubGruposDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbSubGruposUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbSubGruposUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsSubGrupos: TDataSource
    DataSet = TbSubGrupos
    Left = 488
    Top = 188
  end
  object QrConjuntos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM conjuntos'
      'ORDER BY Nome'
      '')
    Left = 480
    Top = 64
    object QrConjuntosCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.conjuntos.Codigo'
    end
    object QrConjuntosNome: TWideStringField
      DisplayWidth = 50
      FieldName = 'Nome'
      Origin = 'DBMMONEY.conjuntos.Nome'
      Size = 50
    end
  end
  object DsConjuntos: TDataSource
    DataSet = QrConjuntos
    Left = 508
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtExclui
    CanDel01 = BtExclui
    Left = 260
    Top = 12
  end
end
