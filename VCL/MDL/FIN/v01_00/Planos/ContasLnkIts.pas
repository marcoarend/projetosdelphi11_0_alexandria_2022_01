unit ContasLnkIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkCheckGroup,
  dmkLabel, dmkEdit, dmkEditCB, dmkDBLookupComboBox, dmkCheckBox,
  dmkRadioGroup, dmkGeral;

type
  TFmContasLnkIts = class(TForm)
    PainelDados: TPanel;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    LaConta: TLabel;
    CBConta: TdmkDBLookupComboBox;
    DsContas: TDataSource;
    EdConta: TdmkEditCB;
    QrContas: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    EdTexto: TdmkEdit;
    Label2: TLabel;
    Label4: TLabel;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    EdBanco: TdmkEditCB;
    CBBanco: TdmkDBLookupComboBox;
    Label5: TLabel;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrCliIntCodigo: TIntegerField;
    QrCliIntNOMEENTI: TWideStringField;
    QrBancos: TmySQLQuery;
    DsBancos: TDataSource;
    QrBancosCodigo: TIntegerField;
    QrBancosNome: TWideStringField;
    Label1: TLabel;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    Label6: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    QrFornece: TmySQLQuery;
    DsFornece: TDataSource;
    QrCliente: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsCliente: TDataSource;
    QrForneceCodigo: TIntegerField;
    QrForneceNOMEENTI: TWideStringField;
    CkUsaEntBank: TdmkCheckBox;
    CGComposHist: TdmkCheckGroup;
    RGTipoCalc: TdmkRadioGroup;
    EdFator: TdmkEdit;
    Label3: TLabel;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FConta, FControle: Integer;
  end;

var
  FmContasLnkIts: TFmContasLnkIts;

implementation

uses UnMyObjects, Module, ContasLnk;

{$R *.DFM}

procedure TFmContasLnkIts.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasLnkIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if RGTipoCalc.ItemIndex < 0 then
    RGTipoCalc.ItemIndex := 0;
end;

procedure TFmContasLnkIts.FormCreate(Sender: TObject);
begin
  QrContas.Open;
  QrBancos.Open;
  QrCliInt.Open;
  QrCliente.Open;
  QrFornece.Open;
end;

procedure TFmContasLnkIts.BtConfirmaClick(Sender: TObject);
var
  Texto(*, Doc*): String;
  (*Codigo, *)Considera, Cliente, Fornece, UsaEntBank, ComposHist, Controle,
  Genero, SubCtrl, TipoCalc: Integer;
  Fator: Double;
begin
  Genero := Geral.IMV(EdConta.Text);
  if (Genero = 0) and (EdConta.Enabled) then
  begin
    Application.MessageBox(PChar('Informe uma Conta!'), 'Erro', MB_OK+MB_ICONERROR);
    EdConta.SetFocus;
    Exit;
  end;
  Texto      := EdTexto.Text;
  {
  Doc        := EdDoc.Text;
  CliInt     := Geral.IMV(EdCliInt.Text);
  Banco      := Geral.IMV(EdBanco.Text);
  }
  Considera  := MLAGeral.BoolToInt(EdConta.Enabled);
  Cliente    := Geral.IMV(EdCliente.Text);
  Fornece    := Geral.IMV(EdFornece.Text);
  UsaEntBank := MLAGeral.BoolToInt(CkUsaEntBank.Checked);
  ComposHist := CGComposHist.Value;
  TipoCalc   := RGTipoCalc.ItemIndex;
  Controle   := FmContasLnk.QrContasLnkControle.Value;
  SubCtrl    := FmContasLnk.QrContasLnkItsSubCtrl.Value;
  Fator      := EdFator.ValueVariant;
  //
  SubCtrl := UMyMod.BuscaEmLivreY_Def('contaslnkits', 'SubCtrl', LaTipo.SQLType, SubCtrl);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'contaslnkits', False, [
  'Controle', 'Genero', 'Texto',
  'TipoCalc', 'Fator', 'Considera',
  'Cliente', 'Fornece', 'UsaEntBank',
  'ComposHist'], [
  'SubCtrl'], [
  Controle, Genero, Texto,
  TipoCalc, Fator, Considera,
  Cliente, Fornece, UsaEntBank,
  ComposHist], [
  SubCtrl], True) then
  begin
    FmContasLnk.AtualizaMultiCtas();
    FmContasLnk.ReopenContasLnkIts(0);
    Close;
  end;
end;

end.
