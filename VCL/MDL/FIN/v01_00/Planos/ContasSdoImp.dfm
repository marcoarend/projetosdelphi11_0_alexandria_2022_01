object FmContasSdoImp: TFmContasSdoImp
  Left = 480
  Top = 194
  Caption = 'FIN-PLCTA-007 :: Saldos de contas'
  ClientHeight = 318
  ClientWidth = 479
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelControle: TPanel
    Left = 0
    Top = 270
    Width = 479
    Height = 48
    Align = alBottom
    TabOrder = 0
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 14
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object BtDesiste: TBitBtn
      Tag = 15
      Left = 374
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 479
    Height = 40
    Align = alTop
    Caption = 'Saldos de contas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TLabel
      Left = 396
      Top = 1
      Width = 82
      Height = 38
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 395
      ExplicitTop = 2
      ExplicitHeight = 36
    end
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 395
      Height = 38
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 393
      ExplicitHeight = 36
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 40
    Width = 479
    Height = 230
    Align = alClient
    TabOrder = 2
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 477
      Height = 72
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object CkCtasPosi: TCheckBox
        Left = 12
        Top = 48
        Width = 149
        Height = 17
        Caption = 'Somente contas positivas.'
        TabOrder = 0
      end
      object CkJoin: TCheckBox
        Left = 12
        Top = 28
        Width = 281
        Height = 17
        Caption = 'Somente conjuntos, grupos e sub-grupos com contas.'
        TabOrder = 1
      end
      object CkAtivos: TCheckBox
        Left = 12
        Top = 8
        Width = 229
        Height = 17
        Caption = 'Somente contas cadastradas como ativas. '
        TabOrder = 2
      end
    end
    object GBSaldos: TGroupBox
      Left = 1
      Top = 73
      Width = 477
      Height = 152
      Align = alTop
      Caption = '       '
      TabOrder = 1
      Visible = False
      object GroupBox2: TGroupBox
        Left = 2
        Top = 69
        Width = 473
        Height = 81
        Align = alClient
        TabOrder = 0
        object CkContasUser: TCheckBox
          Left = 12
          Top = 16
          Width = 153
          Height = 17
          Caption = 'Somente contas do usu'#225'rio.'
          Checked = True
          Enabled = False
          State = cbChecked
          TabOrder = 0
        end
        object CkControla: TCheckBox
          Left = 12
          Top = 36
          Width = 185
          Height = 17
          Caption = 'Somente itens que controla saldo.'
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
        object CkOculta: TCheckBox
          Left = 12
          Top = 56
          Width = 217
          Height = 17
          Caption = 'Ocultar saldos de itens que n'#227'o controla.'
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
      end
      object PainelDados: TPanel
        Left = 2
        Top = 15
        Width = 473
        Height = 54
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label1: TLabel
          Left = 8
          Top = 8
          Width = 70
          Height = 13
          Caption = 'Cliente interno:'
        end
        object EdCliInt: TdmkEditCB
          Left = 8
          Top = 24
          Width = 65
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = CBCliInt
          IgnoraDBLookupComboBox = False
        end
        object CBCliInt: TdmkDBLookupComboBox
          Left = 76
          Top = 24
          Width = 389
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsCliInt
          TabOrder = 1
          dmkEditCB = EdCliInt
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
    end
    object CkMostraSaldos: TCheckBox
      Left = 13
      Top = 71
      Width = 97
      Height = 17
      Caption = ' Mostra saldos: '
      TabOrder = 2
      OnClick = CkMostraSaldosClick
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 36
    Top = 6
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CASE 1 WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE, Codigo'
      'FROM entidades '
      'WHERE CliInt'
      'ORDER BY NOMEENTIDADE')
    Left = 8
    Top = 6
    object QrCliIntNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object QrLCS: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLCSCalcFields
    SQL.Strings = (
      'SELECT co.Codigo CODIGOCONTA, co.Nome NOMECONTA,'
      'sg.Codigo CODIGOSUBGRUPO, sg.Nome NOMESUBGRUPO,'
      'gr.Codigo CODIGOGRUPO, gr.Nome NOMEGRUPO,'
      'cj.Codigo CODIGOCONJUNTO, cj.Nome NOMECONJUNTO,'
      'pl.Codigo CODIGOPLANO, pl.Nome NOMEPLANO,'
      ''
      'pls.SdoAtu PL_ATU, pls.SdoFut PL_FUT, '
      'cjs.SdoAtu CJ_ATU, cjs.SdoFut CJ_FUT, '
      'grs.SdoAtu GR_ATU, grs.SdoFut GR_FUT, '
      'sgs.SdoAtu SG_ATU, sgs.SdoFut SG_FUT,'
      'cos.SdoAtu CO_ATU, cos.SdoFut CO_FUT,'
      ''
      'pl.CtrlaSdo PL_CS, cj.CtrlaSdo CJ_CS, '
      'gr.CtrlaSdo GR_CS, sg.CtrlaSdo SG_CS, '
      'co.CtrlaSdo CO_CS'
      ''
      'FROM plano pl'
      'LEFT JOIN conjuntos cj ON pl.Codigo=cj.Plano'
      'LEFT JOIN grupos    gr ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN subgrupos sg ON gr.Codigo=sg.Grupo'
      'LEFT JOIN contas    co ON sg.Codigo=co.SubGrupo'
      ''
      
        'LEFT JOIN planosdo  pls ON pls.Codigo=pl.Codigo AND pls.Entidade' +
        '=1'
      
        'LEFT JOIN conjunsdo cjs ON cjs.Codigo=cj.Codigo AND cjs.Entidade' +
        '=1'
      
        'LEFT JOIN grupossdo grs ON grs.Codigo=gr.Codigo AND grs.Entidade' +
        '=1'
      
        'LEFT JOIN subgrusdo sgs ON sgs.Codigo=sg.Codigo AND sgs.Entidade' +
        '=1'
      
        'LEFT JOIN contassdo cos ON cos.Codigo=co.Codigo AND cos.Entidade' +
        '=1'
      ''
      'WHERE co.Ativo = 1'
      'AND (co.Codigo>0 OR co.Codigo IS NULL)'
      'ORDER BY cj.Codigo, gr.Codigo, sg.Codigo, co.Codigo, pl.Codigo')
    Left = 124
    Top = 8
    object QrLCSCODIGOCONTA: TIntegerField
      FieldName = 'CODIGOCONTA'
    end
    object QrLCSNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrLCSCODIGOSUBGRUPO: TIntegerField
      FieldName = 'CODIGOSUBGRUPO'
    end
    object QrLCSNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 50
    end
    object QrLCSCODIGOGRUPO: TIntegerField
      FieldName = 'CODIGOGRUPO'
    end
    object QrLCSNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 50
    end
    object QrLCSCODIGOCONJUNTO: TIntegerField
      FieldName = 'CODIGOCONJUNTO'
    end
    object QrLCSNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Size = 50
    end
    object QrLCSCODIGOPLANO: TIntegerField
      FieldName = 'CODIGOPLANO'
      Required = True
    end
    object QrLCSNOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Required = True
      Size = 50
    end
    object QrLCSPL_ATU: TFloatField
      FieldName = 'PL_ATU'
    end
    object QrLCSPL_FUT: TFloatField
      FieldName = 'PL_FUT'
    end
    object QrLCSCJ_ATU: TFloatField
      FieldName = 'CJ_ATU'
    end
    object QrLCSCJ_FUT: TFloatField
      FieldName = 'CJ_FUT'
    end
    object QrLCSGR_ATU: TFloatField
      FieldName = 'GR_ATU'
    end
    object QrLCSGR_FUT: TFloatField
      FieldName = 'GR_FUT'
    end
    object QrLCSSG_ATU: TFloatField
      FieldName = 'SG_ATU'
    end
    object QrLCSSG_FUT: TFloatField
      FieldName = 'SG_FUT'
    end
    object QrLCSCO_ATU: TFloatField
      FieldName = 'CO_ATU'
    end
    object QrLCSCO_FUT: TFloatField
      FieldName = 'CO_FUT'
    end
    object QrLCSPL_CS: TSmallintField
      FieldName = 'PL_CS'
      Required = True
    end
    object QrLCSCJ_CS: TSmallintField
      FieldName = 'CJ_CS'
    end
    object QrLCSGR_CS: TSmallintField
      FieldName = 'GR_CS'
    end
    object QrLCSSG_CS: TSmallintField
      FieldName = 'SG_CS'
    end
    object QrLCSCO_CS: TSmallintField
      FieldName = 'CO_CS'
    end
    object QrLCSPL_ATU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PL_ATU_TXT'
      Size = 50
      Calculated = True
    end
    object QrLCSPL_FUT_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PL_FUT_TXT'
      Size = 50
      Calculated = True
    end
    object QrLCSCJ_ATU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CJ_ATU_TXT'
      Size = 50
      Calculated = True
    end
    object QrLCSCJ_FUT_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CJ_FUT_TXT'
      Size = 50
      Calculated = True
    end
    object QrLCSGR_ATU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'GR_ATU_TXT'
      Size = 50
      Calculated = True
    end
    object QrLCSGR_FUT_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'GR_FUT_TXT'
      Size = 50
      Calculated = True
    end
    object QrLCSSG_ATU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SG_ATU_TXT'
      Size = 50
      Calculated = True
    end
    object QrLCSSG_FUT_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SG_FUT_TXT'
      Size = 50
      Calculated = True
    end
    object QrLCSCO_ATU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CO_ATU_TXT'
      Size = 50
      Calculated = True
    end
    object QrLCSCO_FUT_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CO_FUT_TXT'
      Size = 50
      Calculated = True
    end
  end
  object frxDsLCS: TfrxDBDataset
    UserName = 'frxDsLCS'
    CloseDataSource = False
    DataSet = QrLCS
    BCDToCurrency = False
    Left = 96
    Top = 8
  end
  object frxLCS: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39436.881021770800000000
    ReportOptions.LastChange = 39436.881021770800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Alt: Integer;'
      ''
      'procedure GroupHeader1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  {'
      '  if <VARF_NAO_OCULTA> then Alt := 23'
      '    else if <frxDsLCS."PL_CS"> = 1 then Alt := 23'
      '      else Alt := 0;'
      '  GroupHeader1.Height := Alt;'
      '  //'
      '  Memo4.Height := Alt;'
      '  Memo5.Height := Alt;'
      '  Memo6.Height := Alt;'
      '  Memo7.Height := Alt;'
      '  }'
      'end;'
      ''
      'procedure GroupHeader2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  {'
      '  if <VARF_NAO_OCULTA> then Alt := 19'
      '    else if <frxDsLCS."CJ_CS"> = 1 then Alt := 19'
      '      else Alt := 0;'
      '  GroupHeader2.Height := Alt;'
      '  //'
      '  Memo8.Height := Alt;'
      '  Memo9.Height := Alt;'
      '  Memo10.Height := Alt;'
      '  Memo11.Height := Alt;'
      '  }'
      'end;'
      ''
      'procedure GroupHeader3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  {'
      '  if <VARF_NAO_OCULTA> then Alt := 15'
      '    else if <frxDsLCS."GR_CS"> = 1 then Alt := 15'
      '      else Alt := 0;'
      '  GroupHeader3.Height := Alt;'
      '  //'
      '  Memo12.Height := Alt;'
      '  Memo13.Height := Alt;'
      '  Memo14.Height := Alt;'
      '  Memo15.Height := Alt;'
      '  }'
      'end;'
      ''
      'procedure GroupHeader4OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  {'
      '  if <VARF_NAO_OCULTA> then Alt := 15'
      '    else if <frxDsLCS."SG_CS"> = 1 then Alt := 15'
      '      else Alt := 0;'
      '  GroupHeader4.Height := Alt;'
      '  //'
      '  Memo16.Height := Alt;'
      '  Memo17.Height := Alt;'
      '  Memo18.Height := Alt;'
      '  Memo19.Height := Alt;'
      '  }'
      'end;'
      ''
      'procedure MasterData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <VARF_NAO_OCULTA> then Alt := 13'
      '    else if <frxDsLCS."CO_CS"> = 1 then Alt := 13'
      '      else Alt := 0;'
      '  MasterData1.Height := Alt;'
      '  //'
      '  Memo20.Height := Alt;'
      '  Memo21.Height := Alt;'
      '  Memo22.Height := Alt;'
      '  Memo23.Height := Alt;'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxLCSGetValue
    Left = 68
    Top = 8
    Datasets = <
      item
        DataSet = frxDsLCS
        DataSetName = 'frxDsLCS'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object GroupHeader1: TfrxGroupHeader
        Height = 26.456710000000000000
        Top = 173.858380000000000000
        Width = 793.701300000000000000
        OnBeforePrint = 'GroupHeader1OnBeforePrint'
        Condition = 'frxDsLCS."CODIGOPLANO"'
        object Memo4: TfrxMemoView
          Left = 37.795300000000000000
          Top = 3.779529999999994000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."CODIGOPLANO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 75.590600000000000000
          Top = 3.779529999999994000
          Width = 529.134200000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMEPLANO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 604.724800000000000000
          Top = 3.779529999999965000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLCS."PL_ATU_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 680.315400000000000000
          Top = 3.779529999999965000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLCS."PL_FUT_TXT"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        Height = 18.897650000000000000
        Top = 222.992270000000000000
        Width = 793.701300000000000000
        OnBeforePrint = 'GroupHeader2OnBeforePrint'
        Condition = 'frxDsLCS."CODIGOCONJUNTO"'
        object Memo8: TfrxMemoView
          Left = 75.590600000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."CODIGOCONJUNTO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 113.385900000000000000
          Width = 491.338900000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMECONJUNTO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLCS."CJ_ATU_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 680.315400000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLCS."CJ_FUT_TXT"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        Height = 15.118120000000000000
        Top = 264.567100000000000000
        Width = 793.701300000000000000
        OnBeforePrint = 'GroupHeader3OnBeforePrint'
        Condition = 'frxDsLCS."CODIGOGRUPO"'
        object Memo12: TfrxMemoView
          Left = 113.385900000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."CODIGOGRUPO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 151.181200000000000000
          Width = 453.543600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMEGRUPO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLCS."GR_ATU_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Left = 680.315400000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLCS."GR_FUT_TXT"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader4: TfrxGroupHeader
        Height = 15.118120000000000000
        Top = 302.362400000000000000
        Width = 793.701300000000000000
        OnBeforePrint = 'GroupHeader4OnBeforePrint'
        Condition = 'frxDsLCS."CODIGOSUBGRUPO"'
        object Memo16: TfrxMemoView
          Left = 151.181200000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."CODIGOSUBGRUPO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 188.976500000000000000
          Width = 415.748300000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMESUBGRUPO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLCS."SG_ATU_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 680.315400000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLCS."SG_FUT_TXT"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 13.228346460000000000
        Top = 340.157700000000000000
        Width = 793.701300000000000000
        OnBeforePrint = 'MasterData1OnBeforePrint'
        DataSet = frxDsLCS
        DataSetName = 'frxDsLCS'
        RowCount = 0
        object Memo20: TfrxMemoView
          Left = 188.976500000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'CODIGOCONTA'
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCS."CODIGOCONTA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 226.771800000000000000
          Width = 377.953000000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'NOMECONTA'
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCS."NOMECONTA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'CO_ATU_TXT'
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLCS."CO_ATU_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 680.315400000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLCS."CO_FUT_TXT"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 68.031540000000000000
        Top = 472.441250000000000000
        Width = 793.701300000000000000
      end
      object PageHeader1: TfrxPageHeader
        Height = 92.598476460000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo1: TfrxMemoView
          Left = 37.795300000000000000
          Top = 37.795300000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'PLANO DE CONTAS COM SALDOS')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 37.795300000000000000
          Top = 56.692949999999990000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Entidade:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 98.267780000000000000
          Top = 56.692949999999990000
          Width = 657.638220000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_ENTICONTA]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 37.795300000000000000
          Top = 79.370130000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Plano')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 75.590600000000000000
          Top = 79.370130000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conjunto')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 113.385900000000000000
          Top = 79.370130000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Grupo')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          Left = 151.181200000000000000
          Top = 79.370130000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Sub-grupo')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          Left = 188.976500000000000000
          Top = 79.370130000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          Left = 226.771800000000000000
          Top = 79.370130000000000000
          Width = 377.953000000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          Left = 604.724800000000000000
          Top = 79.370130000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo atual')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          Left = 680.315400000000000000
          Top = 79.370130000000000000
          Width = 75.590600000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo futuro')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 34.015770000000000000
        Top = 415.748300000000000000
        Width = 793.701300000000000000
        object Memo32: TfrxMemoView
          Left = 517.795610000000000000
          Top = 7.559059999999988000
          Width = 90.708720000000000000
          Height = 15.118110236220500000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'SALDO TOTAL:     ')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          Left = 608.504330000000000000
          Top = 7.559059999999988000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLCS."CO_ATU">,MasterData1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          Left = 684.094930000000000000
          Top = 7.559059999999988000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsLCS
          DataSetName = 'frxDsLCS'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLCS."CO_FUT">,MasterData1)]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object frxLCL: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39436.881021770800000000
    ReportOptions.LastChange = 39436.881021770800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxLCSGetValue
    Left = 336
    Top = 56
    Datasets = <
      item
        DataSet = frxDsLCL
        DataSetName = 'frxDsLCL'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object GroupHeader1: TfrxGroupHeader
        Height = 26.456710000000000000
        Top = 154.960730000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsLCL."CODIGOPLANO"'
        object Memo4: TfrxMemoView
          Left = 37.795300000000000000
          Top = 3.779529999999994000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCL."CODIGOPLANO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 75.590600000000000000
          Top = 3.779529999999994000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCL."NOMEPLANO"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        Height = 18.897650000000000000
        Top = 204.094620000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsLCL."CODIGOCONJUNTO"'
        object Memo8: TfrxMemoView
          Left = 75.590600000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCL."CODIGOCONJUNTO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 113.385900000000000000
          Width = 642.520100000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCL."NOMECONJUNTO"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        Height = 15.118120000000000000
        Top = 245.669450000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsLCL."CODIGOGRUPO"'
        object Memo12: TfrxMemoView
          Left = 113.385900000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCL."CODIGOGRUPO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 151.181200000000000000
          Width = 604.724800000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCL."NOMEGRUPO"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader4: TfrxGroupHeader
        Height = 15.118120000000000000
        Top = 283.464750000000000000
        Width = 793.701300000000000000
        Condition = 'frxDsLCL."CODIGOSUBGRUPO"'
        object Memo16: TfrxMemoView
          Left = 151.181200000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCL."CODIGOSUBGRUPO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 188.976500000000000000
          Width = 566.929500000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCL."NOMESUBGRUPO"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 13.228346460000000000
        Top = 321.260050000000000000
        Width = 793.701300000000000000
        DataSet = frxDsLCL
        DataSetName = 'frxDsLCL'
        RowCount = 0
        object Memo20: TfrxMemoView
          Left = 188.976500000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'CODIGOCONTA'
          DataSet = frxDsLCL
          DataSetName = 'frxDsLCL'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLCL."CODIGOCONTA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 226.771800000000000000
          Width = 529.134200000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'NOMECONTA'
          DataSet = frxDsLCL
          DataSetName = 'frxDsLCL'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLCL."NOMECONTA"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 68.031540000000000000
        Top = 396.850650000000000000
        Width = 793.701300000000000000
        object Memo32: TfrxMemoView
          Left = 566.929500000000000000
          Top = 3.779530000000022000
          Width = 188.976500000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsLCL
          DataSetName = 'frxDsLCL'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#].')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 73.700826460000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo1: TfrxMemoView
          Left = 37.795300000000000000
          Top = 37.795300000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'LISTA DE PLANOS DE CONTAS')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 37.795300000000000000
          Top = 60.472480000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCL
          DataSetName = 'frxDsLCL'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Plano')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 75.590600000000000000
          Top = 60.472480000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCL
          DataSetName = 'frxDsLCL'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conjunto')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 113.385900000000000000
          Top = 60.472480000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCL
          DataSetName = 'frxDsLCL'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Grupo')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          Left = 151.181200000000000000
          Top = 60.472480000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCL
          DataSetName = 'frxDsLCL'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Sub-grupo')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          Left = 188.976500000000000000
          Top = 60.472480000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCL
          DataSetName = 'frxDsLCL'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          Left = 226.771800000000000000
          Top = 60.472480000000000000
          Width = 529.134200000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsLCL
          DataSetName = 'frxDsLCL'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object QrLCL: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT co.Codigo CODIGOCONTA, co.Nome NOMECONTA,'
      'sg.Codigo CODIGOSUBGRUPO, sg.Nome NOMESUBGRUPO,'
      'gr.Codigo CODIGOGRUPO, gr.Nome NOMEGRUPO,'
      'cj.Codigo CODIGOCONJUNTO, cj.Nome NOMECONJUNTO,'
      'pl.Codigo CODIGOPLANO, pl.Nome NOMEPLANO'
      ''
      'FROM plano pl'
      'LEFT JOIN conjuntos cj ON pl.Codigo=cj.Plano'
      'LEFT JOIN grupos    gr ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN subgrupos sg ON gr.Codigo=sg.Grupo'
      'LEFT JOIN contas    co ON sg.Codigo=co.SubGrupo'
      'WHERE (co.Ativo = 1 OR (co.Ativo IS NULL))'
      'AND (co.Codigo>0 OR co.Codigo IS NULL)'
      'ORDER BY cj.Codigo, gr.Codigo, sg.Codigo, co.Codigo, pl.Codigo')
    Left = 365
    Top = 57
    object QrLCLCODIGOCONTA: TIntegerField
      FieldName = 'CODIGOCONTA'
    end
    object QrLCLNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrLCLCODIGOSUBGRUPO: TIntegerField
      FieldName = 'CODIGOSUBGRUPO'
    end
    object QrLCLNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 50
    end
    object QrLCLCODIGOGRUPO: TIntegerField
      FieldName = 'CODIGOGRUPO'
    end
    object QrLCLNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 50
    end
    object QrLCLCODIGOCONJUNTO: TIntegerField
      FieldName = 'CODIGOCONJUNTO'
    end
    object QrLCLNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Size = 50
    end
    object QrLCLCODIGOPLANO: TIntegerField
      FieldName = 'CODIGOPLANO'
      Required = True
    end
    object QrLCLNOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Required = True
      Size = 50
    end
  end
  object frxDsLCL: TfrxDBDataset
    UserName = 'frxDsLCL'
    CloseDataSource = False
    DataSet = QrLCL
    BCDToCurrency = False
    Left = 393
    Top = 57
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 244
    Top = 4
  end
end
