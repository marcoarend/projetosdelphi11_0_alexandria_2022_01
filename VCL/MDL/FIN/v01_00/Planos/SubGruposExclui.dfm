object FmSubGruposExclui: TFmSubGruposExclui
  Left = 406
  Top = 221
  Caption = 'Exclus'#227'o de sub-grupos'
  ClientHeight = 393
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 40
    Width = 792
    Height = 305
    Align = alClient
    TabOrder = 0
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 60
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 143
        Height = 13
        Caption = 'Sub-grupo que ser'#225'  exclu'#237'do:'
      end
      object Label2: TLabel
        Left = 400
        Top = 4
        Width = 306
        Height = 13
        Caption = 
          'Sub-grupo para a qual ser'#227'o transferidos as contas (se existirem' +
          ').'
      end
      object Label3: TLabel
        Left = 16
        Top = 44
        Width = 168
        Height = 13
        Caption = 'Contas do sub-grupo a ser exclu'#237'da'
      end
      object EdSubgrupo: TdmkEditCB
        Left = 12
        Top = 20
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdSubgrupoChange
        DBLookupComboBox = CBSubGrupo
      end
      object CBSubGrupo: TdmkDBLookupComboBox
        Left = 80
        Top = 20
        Width = 313
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSubgrupos
        TabOrder = 1
        dmkEditCB = EdSubgrupo
        UpdType = utYes
      end
      object EdNova: TdmkEditCB
        Left = 400
        Top = 20
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBNova
      end
      object CBNova: TdmkDBLookupComboBox
        Left = 468
        Top = 20
        Width = 313
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsNova
        TabOrder = 3
        dmkEditCB = EdNova
        UpdType = utYes
      end
    end
    object DBGLct: TDBGrid
      Left = 1
      Top = 61
      Width = 790
      Height = 243
      Align = alClient
      DataSource = DsContas
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Nome'
          Width = 317
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end>
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 345
    Width = 792
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 14
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
      NumGlyphs = 2
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 686
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
      Caption = '&Sair'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSaidaClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 40
    Align = alTop
    Caption = 'Exclus'#227'o de sub-grupos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 790
      Height = 38
      Align = alClient
      Transparent = True
      ExplicitLeft = 3
      ExplicitTop = 3
      ExplicitWidth = 788
      ExplicitHeight = 36
    end
  end
  object DsSubgrupos: TDataSource
    DataSet = QrSubGrupos
    Left = 320
    Top = 50
  end
  object QrSubGrupos: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrSubGruposAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM subgrupos'
      'WHERE Codigo>0'
      'ORDER BY Nome')
    Left = 292
    Top = 50
    object QrSubGruposCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSubGruposNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsNova: TDataSource
    DataSet = QrNova
    Left = 688
    Top = 62
  end
  object QrNova: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM subgrupos'
      'WHERE Codigo>0'
      'AND Codigo<>:P0'
      'ORDER BY Nome')
    Left = 660
    Top = 62
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 12
    Top = 144
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM contas'
      'WHERE SubGrupo=:P0')
    Left = 40
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 36
    Top = 4
  end
end
