unit ContasExclui;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, Grids, DBGrids,
  Variants, dmkPermissoes, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UnDmkEnums;

type
  TFmContasExclui = class(TForm)
    PainelDados: TPanel;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    BtSaida: TBitBtn;
    DsContas: TDataSource;
    QrContas: TmySQLQuery;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Label1: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdNova: TdmkEditCB;
    CBNova: TdmkDBLookupComboBox;
    Label3: TLabel;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DsNova: TDataSource;
    QrNova: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsLct: TDataSource;
    QrLct: TmySQLQuery;
    QrLctData: TDateField;
    QrLctTipo: TSmallintField;
    QrLctCarteira: TIntegerField;
    QrLctAutorizacao: TIntegerField;
    QrLctGenero: TIntegerField;
    QrLctDescricao: TWideStringField;
    QrLctNotaFiscal: TIntegerField;
    QrLctDebito: TFloatField;
    QrLctCredito: TFloatField;
    QrLctCompensado: TDateField;
    QrLctDocumento: TFloatField;
    QrLctSit: TIntegerField;
    QrLctVencimento: TDateField;
    QrLctLk: TIntegerField;
    QrLctFatID: TIntegerField;
    QrLctFatParcela: TIntegerField;
    QrLctCONTA: TIntegerField;
    QrLctNOMECONTA: TWideStringField;
    QrLctNOMEEMPRESA: TWideStringField;
    QrLctNOMESUBGRUPO: TWideStringField;
    QrLctNOMEGRUPO: TWideStringField;
    QrLctNOMECONJUNTO: TWideStringField;
    QrLctNOMESIT: TWideStringField;
    QrLctAno: TFloatField;
    QrLctMENSAL: TWideStringField;
    QrLctMENSAL2: TWideStringField;
    QrLctBanco: TIntegerField;
    QrLctLocal: TIntegerField;
    QrLctFatura: TWideStringField;
    QrLctSub: TSmallintField;
    QrLctCartao: TIntegerField;
    QrLctLinha: TIntegerField;
    QrLctPago: TFloatField;
    QrLctSALDO: TFloatField;
    QrLctID_Sub: TSmallintField;
    QrLctMez: TIntegerField;
    QrLctFornecedor: TIntegerField;
    QrLctcliente: TIntegerField;
    QrLctMoraDia: TFloatField;
    QrLctNOMECLIENTE: TWideStringField;
    QrLctNOMEFORNECEDOR: TWideStringField;
    QrLctTIPOEM: TWideStringField;
    QrLctNOMERELACIONADO: TWideStringField;
    QrLctOperCount: TIntegerField;
    QrLctLancto: TIntegerField;
    QrLctMulta: TFloatField;
    QrLctATRASO: TFloatField;
    QrLctJUROS: TFloatField;
    QrLctDataDoc: TDateField;
    QrLctNivel: TIntegerField;
    QrLctVendedor: TIntegerField;
    QrLctAccount: TIntegerField;
    QrLctMes2: TLargeintField;
    QrLctProtesto: TDateField;
    QrLctDataCad: TDateField;
    QrLctDataAlt: TDateField;
    QrLctUserCad: TSmallintField;
    QrLctUserAlt: TSmallintField;
    QrLctControle: TIntegerField;
    QrLctID_Pgto: TIntegerField;
    QrLctCtrlIni: TIntegerField;
    QrLctFatID_Sub: TIntegerField;
    QrLctICMS_P: TFloatField;
    QrLctICMS_V: TFloatField;
    QrLctDuplicata: TWideStringField;
    QrLctCOMPENSADO_TXT: TWideStringField;
    QrLctCliInt: TIntegerField;
    QrLctDepto: TIntegerField;
    DBGLct: TDBGrid;
    QrLctDescoPor: TIntegerField;
    QrLctPrazo: TSmallintField;
    QrLctFatNum: TFloatField;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdContaChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure QrContasAfterScroll(DataSet: TDataSet);
    procedure QrLctCalcFields(DataSet: TDataSet);
    procedure DBGLctDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmContasExclui: TFmContasExclui;

implementation

uses UnMyObjects, Module, UnFinanceiro;

{$R *.DFM}

procedure TFmContasExclui.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasExclui.BtConfirmaClick(Sender: TObject);
var
  Nova, Conta: Integer;
begin
  if CBConta.KeyValue = Null then
  begin
    Application.MessageBox(PChar('Informe uma conta a excluir!'), 'Erro', MB_OK+MB_ICONERROR);
    EdConta.SetFocus;
    Exit;
  end;
  Conta := Geral.IMV(EdConta.Text);
  if (CBNova.KeyValue = Null) and (QrLct.RecordCount > 0) then
  begin
    Application.MessageBox(PChar('Informe uma conta para transfer�ncia!'),
    'Erro', MB_OK+MB_ICONERROR);
    EdNova.SetFocus;
    Exit;
  end;
  Nova := Geral.IMV(EdNova.Text);
  if Application.MessageBox(PChar('Confirma a exclus�o da conta "'+
  CBConta.Text+'"?'), 'Erro', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then Exit;
  if Nova > 0 then
  begin
    {
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET Genero=:P0 WHERE Genero=:P1');
    Dmod.QrUpd.Params[0].AsInteger := Nova;
    Dmod.QrUpd.Params[1].AsInteger := Conta;
    Dmod.QrUpd.ExecSQL;
    }
    UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
    'Genero'], ['Genero'], [Nova], [Conta], True, '');
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM contas WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := Conta;
    Dmod.QrUpd.ExecSQL;
    //
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Contas', Conta);
    Close;
  end else begin
    Application.MessageBox('Conta de transfer�ncia n�o definida', 'Erro',
    MB_OK+MB_ICONERROR);
    Exit;
  end;
end;

procedure TFmContasExclui.EdContaChange(Sender: TObject);
begin
  if Length(Trim(EdConta.Text)) = 0 then QrLct.Close;
end;

procedure TFmContasExclui.FormCreate(Sender: TObject);
begin
  QrContas.Open;
end;

procedure TFmContasExclui.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1,PainelTitulo, True,0);
end;

procedure TFmContasExclui.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContasExclui.QrContasAfterScroll(DataSet: TDataSet);
begin
  if CBConta.KeyValue <> Null then
  begin
    EdNova.Text := '';
    CBNova.KeyValue := Null;
    QrNova.Close;
    QrNova.Params[0].AsInteger := QrContasCodigo.Value;
    QrNova.Open;
    //
    QrLct.Close;
    QrLct.SQL.Clear;
    QrLct.SQL.Add('SELECT MOD(la.Mez, 100) Mes2,');
    QrLct.SQL.Add('((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,');
    QrLct.SQL.Add('la.*, ct.Codigo CONTA, ca.Prazo,');
    QrLct.SQL.Add('ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,');
    QrLct.SQL.Add('gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,');
    QrLct.SQL.Add('CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial');
    QrLct.SQL.Add('ELSE em.Nome END NOMEEMPRESA,');
    QrLct.SQL.Add('CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial');
    QrLct.SQL.Add('ELSE cl.Nome END NOMECLIENTE,');
    QrLct.SQL.Add('CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial');
    QrLct.SQL.Add('ELSE fo.Nome END NOMEFORNECEDOR');
    QrLct.SQL.Add('FROM ' + VAR_LCT + ' la');
    QrLct.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
    QrLct.SQL.Add('LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = 0');
    QrLct.SQL.Add('LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo');
    QrLct.SQL.Add('LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo');
    QrLct.SQL.Add('LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto');
    QrLct.SQL.Add('LEFT JOIN entidades em ON em.Codigo=ct.Empresa');
    QrLct.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
    QrLct.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor');
    QrLct.SQL.Add('WHERE Genero=:P0');
    QrLct.SQL.Add('ORDER BY la.Data, la.Controle');
    QrLct.Params[0].AsInteger := QrContasCodigo.Value;
    QrLct.Open;
  end;  
end;

procedure TFmContasExclui.QrLctCalcFields(DataSet: TDataSet);
begin
  if QrLctMes2.Value > 0 then
    QrLctMENSAL.Value := FormatFloat('00', QrLctMes2.Value)+'/'
    +Copy(FormatFloat('0000', QrLctAno.Value), 3, 2)
   else QrLctMENSAL.Value := CO_VAZIO;
  if QrLctMes2.Value > 0 then
    QrLctMENSAL2.Value := FormatFloat('0000', QrLctAno.Value)+'/'+
    FormatFloat('00', QrLctMes2.Value)+'/01'
   else QrLctMENSAL2.Value := CO_VAZIO;

  QrLctNOMESIT.Value := UFinanceiro.NomeSitLancto(QrLctSit.Value,
    QrLctTipo.Value, QrLctPrazo.Value, QrLctVencimento.Value,
    0(*QrLctReparcel.Value*));
  case QrLctSit.Value of
    0: QrLctSALDO.Value := QrLctCredito.Value - QrLctDebito.Value;
    1: QrLctSALDO.Value := (QrLctCredito.Value - QrLctDebito.Value) - QrLctPago.Value;
    else QrLctSALDO.Value := 0;
  end;
  QrLctNOMERELACIONADO.Value := CO_VAZIO;
  if QrLctcliente.Value <> 0 then
  QrLctNOMERELACIONADO.Value := QrLctNOMECLIENTE.Value;
  if QrLctFornecedor.Value <> 0 then
  QrLctNOMERELACIONADO.Value := QrLctNOMERELACIONADO.Value +
  QrLctNOMEFORNECEDOR.Value;
  //
  if QrLctVencimento.Value > Date then QrLctATRASO.Value := 0
    else QrLctATRASO.Value := Date - QrLctVencimento.Value;
  //
  QrLctJUROS.Value :=
    Trunc(QrLctATRASO.Value * QrLctMoraDia.Value * 100)/100;
  //
  if QrLctCompensado.Value = 0 then
     QrLctCOMPENSADO_TXT.Value := '' else
     QrLctCOMPENSADO_TXT.Value :=
     FormatDateTime(VAR_FORMATDATE2, QrLctCompensado.Value);
end;

procedure TFmContasExclui.DBGLctDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  MyObjects.DefineCorTextoSitLancto(TDBGrid(Sender), Rect, 'NOMESIT',
    QrLctNOMESIT.Value);
end;

end.

