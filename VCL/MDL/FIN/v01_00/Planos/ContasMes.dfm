object FmContasMes: TFmContasMes
  Left = 339
  Top = 185
  Caption = 'FIN-PLCTA-021 :: Previs'#245'es Mensais'
  ClientHeight = 335
  ClientWidth = 447
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 287
    Width = 447
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 335
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 447
    Height = 48
    Align = alTop
    Caption = 'Previs'#245'es Mensais'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 363
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 451
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 364
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 620
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 447
    Height = 239
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 4
      Width = 70
      Height = 13
      Caption = 'Cliente interno:'
    end
    object Label2: TLabel
      Left = 12
      Top = 84
      Width = 72
      Height = 13
      Caption = 'Descri'#231#227'o [F4]:'
    end
    object Label5: TLabel
      Left = 12
      Top = 124
      Width = 94
      Height = 13
      Caption = 'Compet'#234'ncia inicial:'
    end
    object Label16: TLabel
      Left = 112
      Top = 126
      Width = 22
      Height = 12
      Caption = 'pq'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Wingdings 3'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 228
      Top = 124
      Width = 87
      Height = 13
      Caption = 'Compet'#234'ncia final:'
    end
    object Label4: TLabel
      Left = 320
      Top = 126
      Width = 22
      Height = 12
      Caption = 'pq'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Wingdings 3'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 12
      Top = 44
      Width = 326
      Height = 13
      Caption = 'Conta (Plano de contas): (somente contas MENSAIS s'#227'o mostradas!)'
    end
    object EdCliInt: TdmkEditCB
      Left = 12
      Top = 20
      Width = 45
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInt64
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCliInt
      IgnoraDBLookupComboBox = False
    end
    object CBCliInt: TdmkDBLookupComboBox
      Left = 60
      Top = 20
      Width = 381
      Height = 21
      KeyField = 'CO_SHOW'
      ListField = 'NO_EMPRESA'
      ListSource = DsCliInt
      TabOrder = 1
      dmkEditCB = EdCliInt
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdDescricao: TdmkEdit
      Left = 12
      Top = 100
      Width = 429
      Height = 21
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Descricao'
      UpdCampo = 'Descricao'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnKeyDown = EdDescricaoKeyDown
    end
    object Edit1: TdmkEdit
      Left = 48
      Top = 140
      Width = 177
      Height = 21
      ReadOnly = True
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdPeriodoIni: TdmkEdit
      Left = 12
      Top = 140
      Width = 36
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'PeriodoIni'
      UpdCampo = 'PeriodoIni'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdPeriodoIniChange
    end
    object Edit2: TdmkEdit
      Left = 264
      Top = 140
      Width = 177
      Height = 21
      ReadOnly = True
      TabOrder = 8
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdPeriodoFim: TdmkEdit
      Left = 228
      Top = 140
      Width = 36
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'PeriodoFim'
      UpdCampo = 'PeriodoFim'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdPeriodoFimChange
    end
    object EdCodigo: TdmkEditCB
      Left = 12
      Top = 60
      Width = 45
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCodigo
      IgnoraDBLookupComboBox = False
    end
    object CBCodigo: TdmkDBLookupComboBox
      Left = 60
      Top = 60
      Width = 381
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContas
      TabOrder = 3
      dmkEditCB = EdCodigo
      QryCampo = 'Codigo'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object GroupBox1: TGroupBox
      Left = 12
      Top = 164
      Width = 213
      Height = 65
      Caption = ' Valor para ser considerado quitado: '
      TabOrder = 9
      object Label7: TLabel
        Left = 12
        Top = 20
        Width = 64
        Height = 13
        Caption = 'Valor m'#237'nimo:'
      end
      object Label8: TLabel
        Left = 96
        Top = 20
        Width = 65
        Height = 13
        Caption = 'Valor m'#225'ximo:'
      end
      object EdValorMin: TdmkEdit
        Left = 12
        Top = 36
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0,01'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,01'
        QryCampo = 'ValorMin'
        UpdCampo = 'ValorMin'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.010000000000000000
      end
      object EdValorMax: TdmkEdit
        Left = 96
        Top = 36
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0,01'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,01'
        QryCampo = 'ValorMax'
        UpdCampo = 'ValorMax'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.010000000000000000
      end
    end
    object GroupBox2: TGroupBox
      Left = 228
      Top = 164
      Width = 213
      Height = 65
      Caption = ' Qtde de lan'#231'am. p/ ser consid. quitado: '
      TabOrder = 10
      object Label9: TLabel
        Left = 12
        Top = 20
        Width = 63
        Height = 13
        Caption = 'Qtde m'#237'nima:'
      end
      object Label10: TLabel
        Left = 96
        Top = 20
        Width = 64
        Height = 13
        Caption = 'Qtde m'#225'xima:'
      end
      object EdQtdeMin: TdmkEdit
        Left = 12
        Top = 36
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '1'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1'
        QryCampo = 'QtdeMin'
        UpdCampo = 'QtdeMin'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1
      end
      object EdQtdeMax: TdmkEdit
        Left = 96
        Top = 36
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '1'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1'
        QryCampo = 'QtdeMax'
        UpdCampo = 'QtdeMax'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1
      end
    end
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, uf.Nome NO_UF,'
      'FLOOR(IF(en.Codigo<-10,en.Filial,en.CliInt)) CO_SHOW,'
      'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_EMPRESA,'
      'IF(en.Tipo=0,en.ECidade,en.PCidade) CIDADE'
      'FROM entidades en'
      'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF)'
      'WHERE en.Codigo < -10'
      'OR en.Codigo=-1'
      'OR en.CliInt <> 0')
    Left = 356
    Top = 58
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntNO_UF: TWideStringField
      FieldName = 'NO_UF'
      Required = True
      Size = 2
    end
    object QrCliIntCO_SHOW: TLargeintField
      FieldName = 'CO_SHOW'
      Required = True
    end
    object QrCliIntNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Required = True
      Size = 100
    end
    object QrCliIntCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 384
    Top = 58
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo>0'
      'AND Mensal="V"'
      'ORDER BY Nome')
    Left = 356
    Top = 86
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 384
    Top = 86
  end
  object VUCliInt: TdmkValUsu
    dmkEditCB = EdCliInt
    Panel = Panel1
    QryCampo = 'CliInt'
    UpdCampo = 'CliInt'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 12
    Top = 8
  end
end
