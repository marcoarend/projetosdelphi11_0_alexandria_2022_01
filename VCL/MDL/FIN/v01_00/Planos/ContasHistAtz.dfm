object FmContasHistAtz: TFmContasHistAtz
  Left = 339
  Top = 185
  BorderStyle = bsDialog
  Caption = 'Atualiza'#231#227'o de Saldos de Contas'
  ClientHeight = 500
  ClientWidth = 794
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 452
    Width = 794
    Height = 48
    Align = alBottom
    TabOrder = 1
    Visible = False
    object BtLocaliza: TBitBtn
      Tag = 18
      Left = 20
      Top = 4
      Width = 180
      Height = 40
      Caption = '&Localiza lancto de origem'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtLocalizaClick
    end
    object Panel2: TPanel
      Left = 682
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtLinkar: TBitBtn
      Tag = 311
      Left = 354
      Top = 3
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Linkar'
      Enabled = False
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtLinkarClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 794
    Height = 48
    Align = alTop
    Caption = 'Atualiza'#231#227'o de Saldos de Contas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 792
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 794
    Height = 404
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 792
      Height = 48
      Align = alTop
      TabOrder = 0
      object LaAviso: TLabel
        Left = 8
        Top = 4
        Width = 66
        Height = 16
        Caption = 'Aguarde...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object PB1: TProgressBar
        Left = 8
        Top = 24
        Width = 777
        Height = 17
        TabOrder = 0
      end
    end
    object DBGPesq: TDBGrid
      Left = 1
      Top = 284
      Width = 792
      Height = 119
      Align = alBottom
      DataSource = DsPesq
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Visible = False
      Columns = <
        item
          Expanded = False
          FieldName = 'Data'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Sub'
          Width = 24
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Genero'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Cr'#233'dito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D'#233'bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Documento'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Carteira'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Visible = True
        end>
    end
    object DBGOrfaos: TDBGrid
      Left = 1
      Top = 49
      Width = 792
      Height = 235
      Align = alClient
      DataSource = DsOrfaos
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Data'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Sub'
          Width = 24
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ID_Pgto'
          Title.Caption = 'ID Pgto'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ID_Sub'
          Title.Caption = 'Sub'
          Width = 24
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Cr'#233'dito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D'#233'bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Documento'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Carteira'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Visible = True
        end>
    end
  end
  object QrOrfaos: TmySQLQuery
    Database = Dmod.MyDB
    Left = 316
    Top = 136
    object QrOrfaosData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOrfaosControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOrfaosSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrOrfaosID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
    end
    object QrOrfaosID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrOrfaosCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOrfaosDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOrfaosDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrOrfaosCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrOrfaosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
  end
  object DsOrfaos: TDataSource
    DataSet = QrOrfaos
    Left = 344
    Top = 136
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 152
    Top = 124
    object QrLocGenero: TIntegerField
      FieldName = 'Genero'
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPesqAfterOpen
    BeforeClose = QrPesqBeforeClose
    Left = 316
    Top = 172
    object QrPesqData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPesqSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrPesqCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPesqDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPesqDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPesqCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrPesqDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPesqGenero: TIntegerField
      FieldName = 'Genero'
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 344
    Top = 172
  end
end
