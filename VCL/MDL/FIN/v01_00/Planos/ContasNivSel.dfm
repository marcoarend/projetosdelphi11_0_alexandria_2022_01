object FmContasNivSel: TFmContasNivSel
  Left = 339
  Top = 185
  Caption = 'FIN-PLCTA-026 :: Sele'#231#227'o de G'#234'neros de Diversos N'#237'veis'
  ClientHeight = 496
  ClientWidth = 629
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 448
    Width = 629
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 517
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object CkControla: TCheckBox
      Left = 120
      Top = 6
      Width = 209
      Height = 17
      Caption = 'Mostrar somente g'#234'neros que controla.'
      Checked = True
      State = cbChecked
      TabOrder = 2
      OnClick = CkControlaClick
    end
    object CkSeleci: TCheckBox
      Left = 120
      Top = 26
      Width = 209
      Height = 17
      Caption = 'Mostrar somente g'#234'neros selecionados.'
      TabOrder = 3
      OnClick = CkControlaClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 629
    Height = 48
    Align = alTop
    Caption = 'Sele'#231#227'o de G'#234'neros de Diversos N'#237'veis'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 627
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 625
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 629
    Height = 400
    Align = alClient
    TabOrder = 0
    object dmkDBGrid1: TdmkDBGrid
      Left = 1
      Top = 1
      Width = 627
      Height = 398
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Seleci'
          Title.Caption = 'ok'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nivel'
          Title.Caption = 'N'#237'vel'
          Width = 33
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NomeNi'
          Title.Caption = 'Descri'#231#227'o'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Genero'
          Title.Caption = 'G'#234'nero'
          Width = 47
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NomeGe'
          Title.Caption = 'Descri'#231#227'o'
          Width = 420
          Visible = True
        end>
      Color = clWindow
      DataSource = DsSdoNiveis
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = dmkDBGrid1CellClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Seleci'
          Title.Caption = 'ok'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nivel'
          Title.Caption = 'N'#237'vel'
          Width = 33
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NomeNi'
          Title.Caption = 'Descri'#231#227'o'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Genero'
          Title.Caption = 'G'#234'nero'
          Width = 47
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NomeGe'
          Title.Caption = 'Descri'#231#227'o'
          Width = 420
          Visible = True
        end>
    end
  end
  object QrSdoNiveis: TmySQLQuery
    Database = Dmod.MyLocDatabase
    SQL.Strings = (
      'SELECT * '
      'FROM sdoniveis'
      'WHERE Seleci>:P0'
      'AND Ctrla>:P1')
    Left = 156
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSdoNiveisNivel: TIntegerField
      FieldName = 'Nivel'
      Origin = 'sdoniveis.Nivel'
    end
    object QrSdoNiveisGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'sdoniveis.Genero'
    end
    object QrSdoNiveisNomeGe: TWideStringField
      FieldName = 'NomeGe'
      Origin = 'sdoniveis.NomeGe'
      Size = 100
    end
    object QrSdoNiveisNomeNi: TWideStringField
      FieldName = 'NomeNi'
      Origin = 'sdoniveis.NomeNi'
    end
    object QrSdoNiveisSumMov: TFloatField
      FieldName = 'SumMov'
      Origin = 'sdoniveis.SumMov'
    end
    object QrSdoNiveisSdoAnt: TFloatField
      FieldName = 'SdoAnt'
      Origin = 'sdoniveis.SdoAnt'
    end
    object QrSdoNiveisSumCre: TFloatField
      FieldName = 'SumCre'
      Origin = 'sdoniveis.SumCre'
    end
    object QrSdoNiveisSumDeb: TFloatField
      FieldName = 'SumDeb'
      Origin = 'sdoniveis.SumDeb'
    end
    object QrSdoNiveisSdoFim: TFloatField
      FieldName = 'SdoFim'
      Origin = 'sdoniveis.SdoFim'
    end
    object QrSdoNiveisSeleci: TSmallintField
      FieldName = 'Seleci'
      Origin = 'sdoniveis.Seleci'
      MaxValue = 1
    end
    object QrSdoNiveisCtrla: TSmallintField
      FieldName = 'Ctrla'
      Origin = 'sdoniveis.Ctrla'
    end
  end
  object DsSdoNiveis: TDataSource
    DataSet = QrSdoNiveis
    Left = 184
    Top = 128
  end
end
