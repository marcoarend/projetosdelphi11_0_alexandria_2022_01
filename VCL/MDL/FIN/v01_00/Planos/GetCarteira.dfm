object FmGetCarteira: TFmGetCarteira
  Left = 339
  Top = 185
  Caption = 'FIN-CONCI-007 :: Transfer'#234'ncia (Carteira)'
  ClientHeight = 150
  ClientWidth = 467
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 102
    Width = 467
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 355
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 467
    Height = 48
    Align = alTop
    Caption = 'Transfer'#234'ncia (Carteira)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 465
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 6
      ExplicitTop = 2
      ExplicitWidth = 463
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 467
    Height = 54
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 4
      Width = 97
      Height = 13
      Caption = 'Carteira relacionada:'
    end
    object EdCarteira: TdmkEditCB
      Left = 12
      Top = 20
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCarteira
    end
    object CBCarteira: TdmkDBLookupComboBox
      Left = 80
      Top = 20
      Width = 385
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsSelCareteiras
      TabOrder = 1
      dmkEditCB = EdCarteira
      UpdType = utYes
    end
  end
  object QrSelCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM carteiras'
      'ORDER BY Nome'
      '')
    Left = 148
    Top = 104
    object QrSelCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSelCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsSelCareteiras: TDataSource
    DataSet = QrSelCarteiras
    Left = 176
    Top = 104
  end
end
