unit ContasHistAtz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, DB, mySQLDbTables, Grids,
  DBGrids, UMySQLModule, dmkGeral, UnDmkEnums;

type
  TFmContasHistAtz = class(TForm)
    PainelConfirma: TPanel;
    BtLocaliza: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    LaAviso: TLabel;
    PB1: TProgressBar;
    QrOrfaos: TmySQLQuery;
    DsOrfaos: TDataSource;
    DBGPesq: TDBGrid;
    QrOrfaosData: TDateField;
    QrOrfaosControle: TIntegerField;
    QrOrfaosSub: TSmallintField;
    QrOrfaosID_Pgto: TIntegerField;
    QrOrfaosID_Sub: TSmallintField;
    QrOrfaosCredito: TFloatField;
    QrOrfaosDebito: TFloatField;
    QrOrfaosDocumento: TFloatField;
    QrOrfaosCarteira: TIntegerField;
    QrOrfaosDescricao: TWideStringField;
    QrLoc: TmySQLQuery;
    QrLocGenero: TIntegerField;
    DBGOrfaos: TDBGrid;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    BtLinkar: TBitBtn;
    QrPesqData: TDateField;
    QrPesqControle: TIntegerField;
    QrPesqSub: TSmallintField;
    QrPesqCredito: TFloatField;
    QrPesqDebito: TFloatField;
    QrPesqDocumento: TFloatField;
    QrPesqCarteira: TIntegerField;
    QrPesqDescricao: TWideStringField;
    QrPesqGenero: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtLocalizaClick(Sender: TObject);
    procedure QrPesqBeforeClose(DataSet: TDataSet);
    procedure QrPesqAfterOpen(DataSet: TDataSet);
    procedure BtLinkarClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenOrfaos();
  public
    { Public declarations }
    procedure ConsertaLancamento();
  end;

  var
  FmContasHistAtz: TFmContasHistAtz;

implementation

uses UnMyObjects, Module, UnInternalConsts, UnFinanceiro;

{$R *.DFM}

procedure TFmContasHistAtz.BtLinkarClick(Sender: TObject);
begin
  BtLinkar.Enabled := False;
  //
  {
  DMod.MyDB.Execute('UPDATE lan ctos SET Genero=' + FormatFloat('0',
  QrPesqGenero.Value) + ' WHERE Controle=' + FormatFloat('0',
  QrOrfaosControle.Value) + ' AND Sub='  + FormatFloat('0',
  QrOrfaosSub.Value));
  }
  UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
  'Genero'], ['Controle', 'Sub'], [QrPesqGenero.Value], [
  QrOrfaosControle.Value, QrOrfaosSub.Value], True, '');
  //
  QrPesq.Close;
  DBGPesq.Visible := False;
  //
  ReopenOrfaos();
  LaAviso.Caption := FormatFloat('0', QrOrfaos.RecordCount) +
  ' registros n�o puderam ser corrigidos.';
end;

procedure TFmContasHistAtz.BtLocalizaClick(Sender: TObject);
begin
  QrPesq.Close;
  QrPesq.SQL.Clear;
  QrPesq.SQL.Add('SELECT Data, Controle, Sub, Genero, Credito,');
  QrPesq.SQL.Add('Debito, Documento, Carteira, Descricao');
  QrPesq.SQL.Add('FROM ' + VAR_LCT);
  QrPesq.SQL.Add('WHERE Credito=:P0');
  QrPesq.SQL.Add('AND Debito=:P1');
  QrPesq.SQL.Add('AND Documento=:P2');
  QrPesq.SQL.Add('AND Controle <> :P3');
  QrPesq.SQL.Add('AND Tipo=2');
  QrPesq.Params[00].AsFloat   := QrOrfaosCredito.Value;
  QrPesq.Params[01].AsFloat   := QrOrfaosDebito.Value;
  QrPesq.Params[02].AsFloat   := QrOrfaosDocumento.Value;
  QrPesq.Params[03].AsInteger := QrOrfaosControle.Value;
  //
  QrPesq.Open;
  DBGPesq.Visible := True;
end;

procedure TFmContasHistAtz.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasHistAtz.ConsertaLancamento();
begin
  Screen.Cursor := crHourGlass;
  try
    LaAviso.Caption := 'Aguarde... Consertando lan�amentos de g�nero -5 para o do lan�amento origem...';
    PB1.Position := 0;
    PB1.Max := QrOrfaos.RecordCount;
    QrOrfaos.First;
    while not QrOrfaos.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      QrLoc.Close;
      QrLoc.SQL.Clear;
      QrLoc.SQL.Add('SELECT Genero');
      QrLoc.SQL.Add('FROM ' + VAR_LCT);
      QrLoc.SQL.Add('WHERE Controle=:P0');
      QrLoc.SQL.Add('AND Sub=:P1');
      QrLoc.Params[00].AsInteger := QrOrfaosID_Pgto.Value;
      QrLoc.Params[01].AsInteger := QrOrfaosID_Sub.Value;
      QrLoc.Open;
      if QrLocGenero.Value > 0 then
      begin
        {
        DMod.MyDB.Execute('UPDATE lan ctos SET Genero=' + FormatFloat('0',
        QrLocGenero.Value) + ' WHERE Controle=' + FormatFloat('0',
        QrOrfaosControle.Value) + ' AND Sub='  + FormatFloat('0',
        QrOrfaosSub.Value));
        }
        UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
        'Genero'], ['Controle', 'Sub'], [QrLocGenero.Value], [
        QrOrfaosControle.Value, QrOrfaosSub.Value], True, '');
      end;
      //
      QrOrfaos.Next;
    end;
    //PB1.Position := PB1.Max;
    LaAviso.Caption := 'Aguarde... Reabrindo tabela...';
    ReopenOrfaos();
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmContasHistAtz.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContasHistAtz.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmContasHistAtz.QrPesqAfterOpen(DataSet: TDataSet);
begin
  BtLinkar.Enabled := QrPesq.RecordCount > 0;
end;

procedure TFmContasHistAtz.QrPesqBeforeClose(DataSet: TDataSet);
begin
  BtLinkar.Enabled := False;
end;

procedure TFmContasHistAtz.ReopenOrfaos();
begin
  QrOrfaos.Close;
  QrOrfaos.SQL.Clear;
  QrOrfaos.SQL.Add('SELECT Data, Controle, Sub,');
  QrOrfaos.SQL.Add('ID_Pgto, ID_Sub, Credito, Debito,');
  QrOrfaos.SQL.Add('Documento, Carteira, Descricao');
  QrOrfaos.SQL.Add('FROM ' + VAR_LCT);
  QrOrfaos.SQL.Add('WHERE Genero=-5');
  QrOrfaos.Open;
end;

end.
