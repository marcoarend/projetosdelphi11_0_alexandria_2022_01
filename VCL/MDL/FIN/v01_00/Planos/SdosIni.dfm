object FmSdosIni: TFmSdosIni
  Left = 339
  Top = 185
  Caption = 'FIN-GERAL-000 :: Saldos Iniciais'
  ClientHeight = 245
  ClientWidth = 555
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 197
    Width = 555
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Confirma'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 443
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 555
    Height = 48
    Align = alTop
    Caption = 'Saldos Iniciais'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 471
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 472
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 555
    Height = 149
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 88
      Width = 27
      Height = 13
      Caption = 'Valor:'
    end
    object Label2: TLabel
      Left = 100
      Top = 88
      Width = 26
      Height = 13
      Caption = 'Data:'
    end
    object Label3: TLabel
      Left = 16
      Top = 8
      Width = 125
      Height = 13
      Caption = 'Conta do plano de contas:'
    end
    object Label4: TLabel
      Left = 16
      Top = 48
      Width = 160
      Height = 13
      Caption = 'Carteira (caixa ou conta corrente):'
    end
    object Label5: TLabel
      Left = 224
      Top = 88
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object EdValor: TdmkEdit
      Left = 16
      Top = 104
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object TPData: TdmkEditDateTimePicker
      Left = 100
      Top = 104
      Width = 121
      Height = 21
      Date = 39968.767882187500000000
      Time = 39968.767882187500000000
      TabOrder = 5
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object EdGenero: TdmkEditCB
      Left = 16
      Top = 24
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBGenero
    end
    object CBGenero: TdmkDBLookupComboBox
      Left = 72
      Top = 24
      Width = 465
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContas
      TabOrder = 1
      dmkEditCB = EdGenero
      UpdType = utYes
    end
    object EdCarteira: TdmkEditCB
      Left = 16
      Top = 64
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdCarteiraChange
      DBLookupComboBox = CBCarteira
    end
    object CBCarteira: TdmkDBLookupComboBox
      Left = 72
      Top = 64
      Width = 465
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCarteiras
      TabOrder = 3
      dmkEditCB = EdCarteira
      UpdType = utYes
    end
    object EdDescri: TdmkEdit
      Left = 224
      Top = 104
      Width = 313
      Height = 21
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = 'S A L D O   I N I C I A L'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 'S A L D O   I N I C I A L'
    end
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 8
    Top = 8
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, ForneceI'
      'FROM carteiras'
      'WHERE Tipo < 2 '
      'AND ForneceI=:P0'
      'ORDER BY Nome'
      '')
    Left = 412
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 36
    Top = 8
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 440
    Top = 100
  end
  object QrLcto: TmySQLQuery
    Database = Dmod.MyDB
    Left = 400
    Top = 8
    object QrLctoData: TDateField
      FieldName = 'Data'
      Required = True
    end
  end
end
