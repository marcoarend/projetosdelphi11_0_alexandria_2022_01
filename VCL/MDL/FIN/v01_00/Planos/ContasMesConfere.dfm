object FmContasMesConfere: TFmContasMesConfere
  Left = 339
  Top = 185
  Caption = 'FIN-PLCTA-031 :: Previs'#245'es Mensais - Confer'#234'ncia de Contas'
  ClientHeight = 494
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtInclui: TBitBtn
      Tag = 10
      Left = 116
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Inclui'
      Enabled = False
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtIncluiClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Previs'#245'es Mensais - Confer'#234'ncia de Contas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 398
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 48
      Align = alTop
      TabOrder = 0
      object LaMes: TLabel
        Left = 496
        Top = 2
        Width = 23
        Height = 13
        Caption = 'M'#234's:'
      end
      object LaAno: TLabel
        Left = 680
        Top = 2
        Width = 22
        Height = 13
        Caption = 'Ano:'
      end
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object CBMes: TComboBox
        Left = 496
        Top = 19
        Width = 182
        Height = 21
        Color = clWhite
        DropDownCount = 12
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 7622183
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        Text = 'CBMes'
      end
      object CBAno: TComboBox
        Left = 680
        Top = 19
        Width = 90
        Height = 21
        Color = clWhite
        DropDownCount = 3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 7622183
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        Text = 'CBAno'
      end
      object EdCliInt: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBCliInt
        IgnoraDBLookupComboBox = False
      end
      object CBCliInt: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 425
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECLI'
        ListSource = DsCliInt
        TabOrder = 1
        dmkEditCB = EdCliInt
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GradeDados: TDBGrid
      Left = 1
      Top = 66
      Width = 1006
      Height = 331
      Align = alClient
      DataSource = DataSource1
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDrawColumnCell = GradeDadosDrawColumnCell
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Conta'
          Width = 42
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NomeCta'
          Title.Caption = 'Descri'#231#227'o da conta (do plano de contas)'
          Width = 274
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdeMin'
          Title.Caption = 'Qtde. m'#237'n.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdeMax'
          Title.Caption = 'Qtde. m'#225'x.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdeExe'
          Title.Caption = 'Qtde. real.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValrMin'
          Title.Caption = 'Valor m'#237'n.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValrMax'
          Title.Caption = 'Valor m'#225'x.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValrExe'
          Title.Caption = 'Valor real.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMESTATUS'
          Title.Caption = 'Avalia'#231#227'o'
          Width = 285
          Visible = True
        end>
    end
    object PB1: TProgressBar
      Left = 1
      Top = 49
      Width = 1006
      Height = 17
      Align = alTop
      TabOrder = 2
    end
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0,RazaoSocial,Nome) NOMECLI'
      'FROM entidades'
      'WHERE CliInt>0'
      'ORDER BY NOMECLI')
    Left = 572
    Top = 12
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Required = True
      Size = 100
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 600
    Top = 12
  end
  object QrContasMes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cm.*, co.Nome NOMECONTA '
      'FROM contasmes cm'
      'LEFT JOIN contas co ON co.Codigo=cm.Codigo'
      'WHERE cm.CliInt=:P0')
    Left = 632
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContasMesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasMesControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrContasMesCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrContasMesDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrContasMesPeriodoIni: TIntegerField
      FieldName = 'PeriodoIni'
    end
    object QrContasMesPeriodoFim: TIntegerField
      FieldName = 'PeriodoFim'
    end
    object QrContasMesValorMin: TFloatField
      FieldName = 'ValorMin'
    end
    object QrContasMesValorMax: TFloatField
      FieldName = 'ValorMax'
    end
    object QrContasMesQtdeMin: TIntegerField
      FieldName = 'QtdeMin'
    end
    object QrContasMesQtdeMax: TIntegerField
      FieldName = 'QtdeMax'
    end
    object QrContasMesNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = Query
    Left = 40
    Top = 12
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    Left = 664
    Top = 12
    object QrPesqQtde: TLargeintField
      FieldName = 'Qtde'
      Required = True
    end
    object QrPesqDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object Query: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterOpen = QueryAfterOpen
    BeforeClose = QueryBeforeClose
    OnCalcFields = QueryCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM confpgtos'
      'ORDER BY Status')
    Left = 12
    Top = 16
    object QueryCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QueryNomeCta: TWideStringField
      FieldName = 'NomeCta'
      Size = 100
    end
    object QueryQtdeMin: TIntegerField
      FieldName = 'QtdeMin'
      DisplayFormat = '#,###,###,##0;-#,###,###,##0; '
    end
    object QueryQtdeMax: TIntegerField
      FieldName = 'QtdeMax'
      DisplayFormat = '#,###,###,##0;-#,###,###,##0; '
    end
    object QueryQtdeExe: TIntegerField
      FieldName = 'QtdeExe'
      DisplayFormat = '#,###,###,##0;-#,###,###,##0; '
    end
    object QueryValrMin: TFloatField
      FieldName = 'ValrMin'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QueryValrMax: TFloatField
      FieldName = 'ValrMax'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QueryValrExe: TFloatField
      FieldName = 'ValrExe'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QueryStatus: TIntegerField
      FieldName = 'Status'
    end
    object QueryNOMESTATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 255
      Calculated = True
    end
  end
end
