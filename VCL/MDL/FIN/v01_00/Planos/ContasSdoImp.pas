unit ContasSdoImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, frxClass, frxDBSet,
  dmkPermissoes, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral, UnDmkEnums;

type
  TFmContasSdoImp = class(TForm)
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    DsCliInt: TDataSource;
    QrCliInt: TmySQLQuery;
    QrCliIntNOMEENTIDADE: TWideStringField;
    QrCliIntCodigo: TIntegerField;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    QrLCS: TmySQLQuery;
    QrLCSCODIGOCONTA: TIntegerField;
    QrLCSNOMECONTA: TWideStringField;
    QrLCSCODIGOSUBGRUPO: TIntegerField;
    QrLCSNOMESUBGRUPO: TWideStringField;
    QrLCSCODIGOGRUPO: TIntegerField;
    QrLCSNOMEGRUPO: TWideStringField;
    QrLCSCODIGOCONJUNTO: TIntegerField;
    QrLCSNOMECONJUNTO: TWideStringField;
    QrLCSCODIGOPLANO: TIntegerField;
    QrLCSNOMEPLANO: TWideStringField;
    QrLCSPL_ATU: TFloatField;
    QrLCSPL_FUT: TFloatField;
    QrLCSCJ_ATU: TFloatField;
    QrLCSCJ_FUT: TFloatField;
    QrLCSGR_ATU: TFloatField;
    QrLCSGR_FUT: TFloatField;
    QrLCSSG_ATU: TFloatField;
    QrLCSSG_FUT: TFloatField;
    QrLCSCO_ATU: TFloatField;
    QrLCSCO_FUT: TFloatField;
    QrLCSPL_CS: TSmallintField;
    QrLCSCJ_CS: TSmallintField;
    QrLCSGR_CS: TSmallintField;
    QrLCSSG_CS: TSmallintField;
    QrLCSCO_CS: TSmallintField;
    QrLCSPL_ATU_TXT: TWideStringField;
    QrLCSPL_FUT_TXT: TWideStringField;
    QrLCSCJ_ATU_TXT: TWideStringField;
    QrLCSCJ_FUT_TXT: TWideStringField;
    QrLCSGR_ATU_TXT: TWideStringField;
    QrLCSGR_FUT_TXT: TWideStringField;
    QrLCSSG_ATU_TXT: TWideStringField;
    QrLCSSG_FUT_TXT: TWideStringField;
    QrLCSCO_ATU_TXT: TWideStringField;
    QrLCSCO_FUT_TXT: TWideStringField;
    frxDsLCS: TfrxDBDataset;
    frxLCS: TfrxReport;
    Panel1: TPanel;
    Panel2: TPanel;
    CkCtasPosi: TCheckBox;
    CkJoin: TCheckBox;
    CkAtivos: TCheckBox;
    GBSaldos: TGroupBox;
    GroupBox2: TGroupBox;
    CkContasUser: TCheckBox;
    CkControla: TCheckBox;
    CkOculta: TCheckBox;
    PainelDados: TPanel;
    Label1: TLabel;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    CkMostraSaldos: TCheckBox;
    frxLCL: TfrxReport;
    QrLCL: TmySQLQuery;
    frxDsLCL: TfrxDBDataset;
    QrLCLCODIGOCONTA: TIntegerField;
    QrLCLNOMECONTA: TWideStringField;
    QrLCLCODIGOSUBGRUPO: TIntegerField;
    QrLCLNOMESUBGRUPO: TWideStringField;
    QrLCLCODIGOGRUPO: TIntegerField;
    QrLCLNOMEGRUPO: TWideStringField;
    QrLCLCODIGOCONJUNTO: TIntegerField;
    QrLCLNOMECONJUNTO: TWideStringField;
    QrLCLCODIGOPLANO: TIntegerField;
    QrLCLNOMEPLANO: TWideStringField;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure QrLCSCalcFields(DataSet: TDataSet);
    procedure frxLCSGetValue(const VarName: String; var Value: Variant);
    procedure CkMostraSaldosClick(Sender: TObject);
  private
    { Private declarations }
    procedure ListaContasSaldos;
    procedure ListaContasLista;
  public
    { Public declarations }
  end;

var
  FmContasSdoImp: TFmContasSdoImp;

implementation

uses UnMyObjects, Module, Entidades;

{$R *.DFM}

procedure TFmContasSdoImp.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasSdoImp.FormCreate(Sender: TObject);
begin
  QrCliInt.Open;
end;

procedure TFmContasSdoImp.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1,PainelTitulo, True,0);
end;

procedure TFmContasSdoImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContasSdoImp.BtConfirmaClick(Sender: TObject);
begin
  if CkMostraSaldos.Checked then
    ListaContasSaldos
  else ListaContasLista;
end;

procedure TFmContasSdoImp.ListaContasSaldos;
var
  Ativo: String;
  Entidade: Integer;
begin
  Entidade := Geral.IMV(EdCliInt.Text);
  if Entidade = 0 then
  begin
    Application.MessageBox('Defina o cliente interno!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;

  //fazer saldos de contas
  //so mostrar saldos se no cadastro de contas, sbgru, grupos etc. pedir
  if CkAtivos.Checked then Ativo := ' 1 ' else Ativo := '0, 1';
  if CkJoin.Checked then  Ativo := Ativo + ')'
  else Ativo := Ativo + ') OR (co.Ativo IS NULL)';
  QrLCS.Close;
  QrLCS.SQL.Clear;

  QrLCS.SQL.Add('SELECT co.Codigo CODIGOCONTA, co.Nome NOMECONTA,');
  QrLCS.SQL.Add('sg.Codigo CODIGOSUBGRUPO, sg.Nome NOMESUBGRUPO,');
  QrLCS.SQL.Add('gr.Codigo CODIGOGRUPO, gr.Nome NOMEGRUPO,');
  QrLCS.SQL.Add('cj.Codigo CODIGOCONJUNTO, cj.Nome NOMECONJUNTO,');
  QrLCS.SQL.Add('pl.Codigo CODIGOPLANO, pl.Nome NOMEPLANO,');
  QrLCS.SQL.Add('');


  QrLCS.SQL.Add('pls.SdoAtu PL_ATU, pls.SdoFut PL_FUT,');
  QrLCS.SQL.Add('cjs.SdoAtu CJ_ATU, cjs.SdoFut CJ_FUT,');
  QrLCS.SQL.Add('grs.SdoAtu GR_ATU, grs.SdoFut GR_FUT,');
  QrLCS.SQL.Add('sgs.SdoAtu SG_ATU, sgs.SdoFut SG_FUT,');
  QrLCS.SQL.Add('cos.SdoAtu CO_ATU, cos.SdoFut CO_FUT,');

  QrLCS.SQL.Add('pl.CtrlaSdo PL_CS, cj.CtrlaSdo CJ_CS,');
  QrLCS.SQL.Add('gr.CtrlaSdo GR_CS, sg.CtrlaSdo SG_CS,');
  QrLCS.SQL.Add('co.CtrlaSdo CO_CS');

  QrLCS.SQL.Add('FROM plano pl');
  QrLCS.SQL.Add('LEFT JOIN conjuntos cj ON pl.Codigo=cj.Plano');
  QrLCS.SQL.Add('LEFT JOIN grupos    gr ON cj.Codigo=gr.Conjunto');
  QrLCS.SQL.Add('LEFT JOIN subgrupos sg ON gr.Codigo=sg.Grupo');
  QrLCS.SQL.Add('LEFT JOIN contas    co ON sg.Codigo=co.SubGrupo');

  QrLCS.SQL.Add('LEFT JOIN planosdo  pls ON pls.Codigo=pl.Codigo AND pls.Entidade=' + EdCliInt.Text);
  QrLCS.SQL.Add('LEFT JOIN conjunsdo cjs ON cjs.Codigo=cj.Codigo AND cjs.Entidade=' + EdCliInt.Text);
  QrLCS.SQL.Add('LEFT JOIN grupossdo grs ON grs.Codigo=gr.Codigo AND grs.Entidade=' + EdCliInt.Text);
  QrLCS.SQL.Add('LEFT JOIN subgrusdo sgs ON sgs.Codigo=sg.Codigo AND sgs.Entidade=' + EdCliInt.Text);
  QrLCS.SQL.Add('LEFT JOIN contassdo cos ON cos.Codigo=co.Codigo AND cos.Entidade=' + EdCliInt.Text);

  QrLCS.SQL.Add('WHERE (co.Ativo in ('+Ativo + ')');
  //if CBSubgrupos.KeyValue <> NULL then
    //QrLCS.SQL.Add('AND co.Subgrupo='''+IntToStr(CBSubgrupos.KeyValue)+'''');
  if CkContasUser.Checked then //and CkSaldos.Checked then
  begin
    if CkJoin.Checked then
      QrLCS.SQL.Add('AND co.Codigo>0')
    else
      QrLCS.SQL.Add('AND (co.Codigo>0 OR co.Codigo IS NULL)');
  end;
  if CkCtasPosi.Checked then
    QrLCS.SQL.Add('AND co.Codigo>0');

  QrLCS.SQL.Add('ORDER BY cj.Codigo, gr.Codigo, sg.Codigo, co.Codigo, pl.Codigo');
  //
  QrLCS.Open;
  MyObjects.frxMostra(frxLCS, 'Plano de Contas com Saldos');
end;

procedure TFmContasSdoImp.QrLCSCalcFields(DataSet: TDataSet);
  function DFS(Status: Integer; ValVerdade: Double): String;
  var
    Verdade: Boolean;
  begin
    Verdade :=  (Status = 0) and (CkOculta.Checked (*or not CkSaldos.Checked*));
    Result := MLAGeral.EscolhaDe2Str(Verdade, 'n/i',
      Geral.FFT(ValVerdade, 2, siNegativo));
  end;
begin
  QrLCSPL_ATU_TXT.Value := DFS(QrLCSPL_CS.Value, QrLCSPL_ATU.Value);
  QrLCSPL_FUT_TXT.Value := DFS(QrLCSPL_CS.Value, QrLCSPL_FUT.Value);
  //
  QrLCSCJ_ATU_TXT.Value := DFS(QrLCSCJ_CS.Value, QrLCSCJ_ATU.Value);
  QrLCSCJ_FUT_TXT.Value := DFS(QrLCSCJ_CS.Value, QrLCSCJ_FUT.Value);
  //
  QrLCSGR_ATU_TXT.Value := DFS(QrLCSGR_CS.Value, QrLCSGR_ATU.Value);
  QrLCSGR_FUT_TXT.Value := DFS(QrLCSGR_CS.Value, QrLCSGR_FUT.Value);
  //
  QrLCSSG_ATU_TXT.Value := DFS(QrLCSSG_CS.Value, QrLCSSG_ATU.Value);
  QrLCSSG_FUT_TXT.Value := DFS(QrLCSSG_CS.Value, QrLCSSG_FUT.Value);
  //
  QrLCSCO_ATU_TXT.Value := DFS(QrLCSCO_CS.Value, QrLCSCO_ATU.Value);
  QrLCSCO_FUT_TXT.Value := DFS(QrLCSCO_CS.Value, QrLCSCO_FUT.Value);
  //
end;

procedure TFmContasSdoImp.frxLCSGetValue(const VarName: String;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_ENTICONTA') = 0 then
    Value := CBCliInt.Text
  else if AnsiCompareText(VarName, 'VARF_NAO_OCULTA') = 0 then
    Value := not CkControla.Checked (*or not CkSaldos.Checked*)
end;

procedure TFmContasSdoImp.CkMostraSaldosClick(Sender: TObject);
begin
  GBSaldos.Visible := CkMostraSaldos.Checked;
end;

procedure TFmContasSdoImp.ListaContasLista;
var
  Ativo: String;
begin
  //fazer saldos de contas
  //so mostrar saldos se no cadastro de contas, sbgru, grupos etc. pedir
  if CkAtivos.Checked then Ativo := ' 1 ' else Ativo := '0, 1';
  if CkJoin.Checked then  Ativo := Ativo + ')'
  else Ativo := Ativo + ') OR (co.Ativo IS NULL)';
  QrLCL.Close;
  QrLCL.SQL.Clear;

  QrLCL.SQL.Add('SELECT co.Codigo CODIGOCONTA, co.Nome NOMECONTA,');
  QrLCL.SQL.Add('sg.Codigo CODIGOSUBGRUPO, sg.Nome NOMESUBGRUPO,');
  QrLCL.SQL.Add('gr.Codigo CODIGOGRUPO, gr.Nome NOMEGRUPO,');
  QrLCL.SQL.Add('cj.Codigo CODIGOCONJUNTO, cj.Nome NOMECONJUNTO,');
  QrLCL.SQL.Add('pl.Codigo CODIGOPLANO, pl.Nome NOMEPLANO');
  QrLCL.SQL.Add('');

  QrLCL.SQL.Add('FROM plano pl');
  QrLCL.SQL.Add('LEFT JOIN conjuntos cj ON pl.Codigo=cj.Plano');
  QrLCL.SQL.Add('LEFT JOIN grupos    gr ON cj.Codigo=gr.Conjunto');
  QrLCL.SQL.Add('LEFT JOIN subgrupos sg ON gr.Codigo=sg.Grupo');
  QrLCL.SQL.Add('LEFT JOIN contas    co ON sg.Codigo=co.SubGrupo');

  QrLCL.SQL.Add('WHERE (co.Ativo in ('+Ativo + ')');

  if CkContasUser.Checked then //and CkSaldos.Checked then
  begin
    if CkJoin.Checked then
      QrLCL.SQL.Add('AND co.Codigo>0')
    else
      QrLCL.SQL.Add('AND (co.Codigo>0 OR co.Codigo IS NULL)');
  end;
  if CkCtasPosi.Checked then
    QrLCL.SQL.Add('AND co.Codigo>0');

  QrLCL.SQL.Add('ORDER BY cj.Codigo, gr.Codigo, sg.Codigo, co.Codigo, pl.Codigo');
  //
  QrLCL.Open;
  MyObjects.frxMostra(frxLCL, 'Lista de Planos de Contas');
end;

end.

