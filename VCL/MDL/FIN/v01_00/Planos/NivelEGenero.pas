unit NivelEGenero;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral;

type
  TFmNivelEGenero = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrCodiNivel: TmySQLQuery;
    QrCodiNivelCodigo: TIntegerField;
    QrCodiNivelNome: TWideStringField;
    DsCodiNivel: TDataSource;
    Panel4: TPanel;
    Label12: TLabel;
    RGNivel: TRadioGroup;
    dmkEditItem: TdmkEditCB;
    dmkDBItem: TdmkDBLookupComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure RGNivelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCodiNivel(Codigo: Integer);
  public
    { Public declarations }
    FNivel, FGenero: Integer;
  end;

  var
  FmNivelEGenero: TFmNivelEGenero;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmNivelEGenero.BtSaidaClick(Sender: TObject);
begin
  FNivel := 0;
  FGenero := 0;
  Close;
end;

procedure TFmNivelEGenero.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNivelEGenero.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmNivelEGenero.ReopenCodiNivel(Codigo: Integer);
begin
  QrCodiNivel.Close;
  if RGNivel.ItemIndex > 0 then
  begin
    QrCodiNivel.SQL[1] := 'FROM ' +
      Lowercase(MLAGeral.NivelSelecionado(RgNivel.ItemIndex, 2, '', ''));
    QrCodiNivel.Open;
    //
    if Codigo <> 0 then
      QrCodiNivel.Locate('Codigo', Codigo, []);
  end;
end;

procedure TFmNivelEGenero.RGNivelClick(Sender: TObject);
begin
  ReopenCodiNivel(0);
  Label12.Caption :=
    MLAGeral.NivelSelecionado(RGNivel.ItemIndex, 1, 'Item ', ':');
end;

procedure TFmNivelEGenero.FormCreate(Sender: TObject);
begin
  FNivel := 0;
  FGenero := 0;
end;

procedure TFmNivelEGenero.BtOKClick(Sender: TObject);
begin
  if RGNivel.ItemIndex = 0 then
  begin
    Application.MessageBox('Defina o n�vel e o item do n�vel (g�nero)!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    RGNivel.SetFocus;
    Exit;
  end;
  FNivel  := RGNivel.ItemIndex;
  FGenero := Geral.IMV(dmkEditItem.Text);
  if FGenero = 0 then
  begin
    Application.MessageBox('Defina item do n�vel (g�nero)!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    dmkEditItem.SetFocus;
    Exit;
  end;
  Close;
end;

end.

