object FmContasLnkIts: TFmContasLnkIts
  Left = 396
  Top = 181
  Caption = 'FIN-PLCTA-027 :: Sub-itens de Links de Consolida'#231#227'o Banc'#225'ria'
  ClientHeight = 386
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 40
    Width = 784
    Height = 298
    Align = alClient
    TabOrder = 0
    object LaConta: TLabel
      Left = 12
      Top = 4
      Width = 131
      Height = 13
      Caption = 'Conta (do plano de contas):'
    end
    object Label2: TLabel
      Left = 360
      Top = 156
      Width = 124
      Height = 13
      Caption = 'Complemento de hist'#243'rico:'
    end
    object Label4: TLabel
      Left = 12
      Top = 48
      Width = 127
      Height = 13
      Caption = 'Entidade: (zero para todas)'
      Enabled = False
    end
    object Label5: TLabel
      Left = 392
      Top = 48
      Width = 116
      Height = 13
      Caption = 'Banco: (zero para todos)'
      Enabled = False
    end
    object Label1: TLabel
      Left = 12
      Top = 92
      Width = 60
      Height = 13
      Caption = 'Fornecedor: '
    end
    object Label6: TLabel
      Left = 392
      Top = 92
      Width = 35
      Height = 13
      Caption = 'Cliente:'
    end
    object Label3: TLabel
      Left = 520
      Top = 196
      Width = 100
      Height = 13
      Caption = '('#185') Valor / percentual:'
    end
    object CBConta: TdmkDBLookupComboBox
      Left = 80
      Top = 20
      Width = 689
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContas
      TabOrder = 1
      dmkEditCB = EdConta
      QryCampo = 'Genero'
      UpdType = utYes
    end
    object EdConta: TdmkEditCB
      Left = 12
      Top = 20
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Genero'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBConta
    end
    object EdTexto: TdmkEdit
      Left = 360
      Top = 172
      Width = 409
      Height = 21
      MaxLength = 50
      TabOrder = 12
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Texto'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdCliInt: TdmkEditCB
      Left = 12
      Top = 64
      Width = 65
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCliInt
    end
    object CBCliInt: TdmkDBLookupComboBox
      Left = 80
      Top = 64
      Width = 308
      Height = 21
      Enabled = False
      KeyField = 'Codigo'
      ListField = 'NOMEENTI'
      ListSource = DsCliInt
      TabOrder = 3
      dmkEditCB = EdCliInt
      UpdType = utYes
    end
    object EdBanco: TdmkEditCB
      Left = 392
      Top = 64
      Width = 65
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBBanco
    end
    object CBBanco: TdmkDBLookupComboBox
      Left = 460
      Top = 64
      Width = 308
      Height = 21
      Enabled = False
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsBancos
      TabOrder = 5
      dmkEditCB = EdBanco
      UpdType = utYes
    end
    object EdFornece: TdmkEditCB
      Left = 12
      Top = 108
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Fornece'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBFornece
    end
    object CBFornece: TdmkDBLookupComboBox
      Left = 80
      Top = 108
      Width = 308
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENTI'
      ListSource = DsFornece
      TabOrder = 7
      dmkEditCB = EdFornece
      QryCampo = 'Fornece'
      UpdType = utYes
    end
    object EdCliente: TdmkEditCB
      Left = 392
      Top = 108
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cliente'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCliente
    end
    object CBCliente: TdmkDBLookupComboBox
      Left = 460
      Top = 108
      Width = 308
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENTI'
      ListSource = DsCliente
      TabOrder = 9
      dmkEditCB = EdCliente
      QryCampo = 'Cliente'
      UpdType = utYes
    end
    object CkUsaEntBank: TdmkCheckBox
      Left = 12
      Top = 136
      Width = 669
      Height = 17
      Caption = 
        'Usar como fornecedor e/ou cliente, a entidade banc'#225'ria informada' +
        ' no cadastro do banco.'
      TabOrder = 10
      QryCampo = 'UsaEntBank'
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
    object CGComposHist: TdmkCheckGroup
      Left = 12
      Top = 156
      Width = 341
      Height = 129
      Caption = 
        ' Composi'#231#227'o da descri'#231#227'o (Hist'#243'rico) para gera'#231#227'o do lan'#231'amento:' +
        ' '
      Items.Strings = (
        'Descri'#231#227'o do extrato'
        'Descri'#231#227'o da conta (Plano de contas)'
        'Nome do Terceiro (Forneceor / Cliente)'
        'Complemento de hist'#243'rico')
      TabOrder = 11
      QryCampo = 'ComposHist'
      UpdType = utYes
      Value = 0
      OldValor = 0
    end
    object RGTipoCalc: TdmkRadioGroup
      Left = 360
      Top = 196
      Width = 153
      Height = 89
      Caption = ' Forma de debitar / creditar: '
      ItemIndex = 0
      Items.Strings = (
        'Ser'#225' informado'
        'Valor fixo $ ('#185')'
        '% do total ('#185')'
        '% Saldo restante ('#185')')
      TabOrder = 13
      QryCampo = 'TipoCalc'
      UpdType = utYes
      OldValor = 0
    end
    object EdFator: TdmkEdit
      Left = 520
      Top = 212
      Width = 101
      Height = 21
      Alignment = taRightJustify
      TabOrder = 14
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'Fator'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 338
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 14
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
      NumGlyphs = 2
    end
    object BtDesiste: TBitBtn
      Tag = 15
      Left = 682
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 40
    Align = alTop
    Caption = 'Sub-itens de Links de Consolida'#231#227'o Banc'#225'ria'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TdmkLabel
      Left = 701
      Top = 1
      Width = 82
      Height = 38
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 604
      ExplicitTop = 2
      ExplicitHeight = 36
    end
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 700
      Height = 38
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 602
      ExplicitHeight = 36
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 236
    Top = 58
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'ORDER BY Nome')
    Left = 208
    Top = 58
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMEENTI'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NOMEENTI')
    Left = 208
    Top = 98
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 236
    Top = 98
  end
  object QrBancos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM bancos'
      'ORDER BY Nome')
    Left = 476
    Top = 98
    object QrBancosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsBancos: TDataSource
    DataSet = QrBancos
    Left = 504
    Top = 98
  end
  object QrFornece: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMEENTI'
      'FROM entidades'
      'WHERE Fornece1="V"'
      'OR Fornece2="V"'
      'OR Fornece3="V"'
      'OR Fornece4="V"'
      'OR Fornece5="V"'
      'OR Fornece6="V"'
      'ORDER BY NOMEENTI')
    Left = 208
    Top = 146
    object QrForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object DsFornece: TDataSource
    DataSet = QrFornece
    Left = 236
    Top = 146
  end
  object QrCliente: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMEENTI'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'OR Cliente2="V"'
      'OR Cliente3="V"'
      'OR Cliente4="V"'
      'ORDER BY NOMEENTI')
    Left = 476
    Top = 142
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object DsCliente: TDataSource
    DataSet = QrCliente
    Left = 504
    Top = 142
  end
end
