object FmContas: TFmContas
  Left = 370
  Top = 194
  Caption = 'FIN-PLCTA-005 :: Cadastro de Contas'
  ClientHeight = 582
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 486
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 735
      Height = 436
      BevelOuter = bvNone
      TabOrder = 1
      object PageControl1: TPageControl
        Left = 0
        Top = 0
        Width = 735
        Height = 436
        ActivePage = TabSheet8
        Align = alClient
        TabOrder = 0
        OnChange = PageControl1Change
        object TabSheet1: TTabSheet
          Caption = 'Dados'
          object Panel1: TPanel
            Left = 0
            Top = 0
            Width = 727
            Height = 408
            Align = alClient
            ParentBackground = False
            TabOrder = 0
            object Label1: TLabel
              Left = 60
              Top = 8
              Width = 36
              Height = 13
              Caption = 'C'#243'digo:'
              FocusControl = DBEdCodigo
            end
            object Label2: TLabel
              Left = 124
              Top = 8
              Width = 51
              Height = 13
              Caption = 'Descri'#231#227'o:'
              FocusControl = DBEdNome
            end
            object Label3: TLabel
              Left = 60
              Top = 50
              Width = 99
              Height = 13
              Caption = 'Descri'#231#227'o substituta:'
              FocusControl = DBEdNome2
            end
            object Label13: TLabel
              Left = 372
              Top = 50
              Width = 153
              Height = 13
              Caption = 'Descri'#231#227'o padr'#227'o para hist'#243'rico:'
            end
            object Label4: TLabel
              Left = 372
              Top = 92
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object Label31: TLabel
              Left = 372
              Top = 132
              Width = 45
              Height = 13
              Caption = 'Entidade:'
            end
            object LaSubGrupo: TLabel
              Left = 60
              Top = 92
              Width = 52
              Height = 13
              Caption = 'Sub-grupo:'
            end
            object Label28: TLabel
              Left = 60
              Top = 132
              Width = 78
              Height = 13
              Caption = 'Centro de custo:'
            end
            object Label9: TLabel
              Left = 432
              Top = 172
              Width = 107
              Height = 13
              Caption = 'Espec'#237'fico de terceiro:'
              Enabled = False
            end
            object Label10: TLabel
              Left = 368
              Top = 172
              Width = 60
              Height = 13
              Caption = 'C'#233'lula excel:'
              FocusControl = DBEdExcel
            end
            object LaIDFat: TLabel
              Left = 60
              Top = 172
              Width = 193
              Height = 13
              Caption = 'Identificador autom'#225'tico em importa'#231#245'es:'
            end
            object GroupBox4: TGroupBox
              Left = 60
              Top = 260
              Width = 617
              Height = 129
              Caption = '    '
              TabOrder = 17
              object LaSaldo: TLabel
                Left = 8
                Top = 20
                Width = 96
                Height = 13
                Caption = 'Dia da mensalidade:'
                FocusControl = DBEdMensDia
              end
              object Label5: TLabel
                Left = 112
                Top = 20
                Width = 117
                Height = 13
                Caption = 'D'#233'bito mensal esperado:'
                FocusControl = DBEdMensdeb
              end
              object Label6: TLabel
                Left = 236
                Top = 20
                Width = 107
                Height = 13
                Caption = 'D'#233'bito mensal m'#237'nimo:'
                FocusControl = DBEdmensmind
              end
              object Label7: TLabel
                Left = 360
                Top = 20
                Width = 119
                Height = 13
                Caption = 'Cr'#233'dito mensal esperado:'
              end
              object Label8: TLabel
                Left = 484
                Top = 20
                Width = 109
                Height = 13
                Caption = 'Cr'#233'dito mensal m'#237'nimo:'
                FocusControl = DBEdMensminc
              end
              object DBEdMensDia: TDBEdit
                Left = 8
                Top = 36
                Width = 101
                Height = 21
                TabStop = False
                Color = clWhite
                DataField = 'Mensdia'
                DataSource = DsContas
                TabOrder = 0
              end
              object DBEdMensdeb: TDBEdit
                Left = 112
                Top = 36
                Width = 121
                Height = 21
                TabStop = False
                Color = clWhite
                DataField = 'Mensdeb'
                DataSource = DsContas
                TabOrder = 1
              end
              object DBEdmensmind: TDBEdit
                Left = 236
                Top = 36
                Width = 121
                Height = 21
                TabStop = False
                Color = clWhite
                DataField = 'Mensmind'
                DataSource = DsContas
                TabOrder = 2
              end
              object DBEdMenscred: TDBEdit
                Left = 360
                Top = 36
                Width = 121
                Height = 21
                TabStop = False
                Color = clWhite
                DataField = 'Menscred'
                DataSource = DsContas
                TabOrder = 3
              end
              object DBEdMensminc: TDBEdit
                Left = 484
                Top = 36
                Width = 121
                Height = 21
                TabStop = False
                Color = clWhite
                DataField = 'Mensminc'
                DataSource = DsContas
                TabOrder = 4
              end
              object DBRadioGroup1: TDBRadioGroup
                Left = 8
                Top = 60
                Width = 597
                Height = 41
                Caption = ' C'#225'lculo da pend'#234'ncia: '
                Columns = 3
                DataField = 'PendenMesSeg'
                DataSource = DsContas
                Items.Strings = (
                  'Meses futuros n'#227'o considerar'
                  'Considerar sempre'
                  'Proporcional na data atual.')
                ParentBackground = True
                TabOrder = 5
                Values.Strings = (
                  '0'
                  '1'
                  '2')
              end
            end
            object DBEdCodigo: TDBEdit
              Left = 60
              Top = 24
              Width = 61
              Height = 21
              Hint = 'N'#186' do banco'
              TabStop = False
              Color = clWhite
              DataField = 'Codigo'
              DataSource = DsContas
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 8281908
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              ParentShowHint = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 0
            end
            object DBEdNome: TDBEdit
              Left = 124
              Top = 24
              Width = 245
              Height = 21
              Hint = 'Nome do banco'
              TabStop = False
              Color = clWhite
              DataField = 'Nome'
              DataSource = DsContas
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
            end
            object DBCBDebito: TDBCheckBox
              Left = 376
              Top = 4
              Width = 53
              Height = 17
              TabStop = False
              Caption = 'D'#233'bito.'
              DataField = 'Debito'
              DataSource = DsContas
              TabOrder = 2
              ValueChecked = 'V'
              ValueUnchecked = 'F'
            end
            object DBCBCredito: TDBCheckBox
              Left = 376
              Top = 24
              Width = 61
              Height = 17
              TabStop = False
              Caption = 'Cr'#233'dito.'
              DataField = 'Credito'
              DataSource = DsContas
              TabOrder = 3
              ValueChecked = 'V'
              ValueUnchecked = 'F'
            end
            object DBCBMensal: TDBCheckBox
              Left = 72
              Top = 260
              Width = 61
              Height = 17
              TabStop = False
              Caption = 'Mensal: '
              DataField = 'Mensal'
              DataSource = DsContas
              TabOrder = 18
              ValueChecked = 'V'
              ValueUnchecked = 'F'
              OnClick = DBCBMensalClick
            end
            object DBCBExclusivo: TDBCheckBox
              Left = 444
              Top = 4
              Width = 67
              Height = 17
              TabStop = False
              Caption = 'Exclusivo.'
              DataField = 'Exclusivo'
              DataSource = DsContas
              TabOrder = 4
              ValueChecked = 'V'
              ValueUnchecked = 'F'
            end
            object DBCBAtivo: TDBCheckBox
              Left = 444
              Top = 24
              Width = 67
              Height = 17
              TabStop = False
              Caption = 'Ativo.'
              DataField = 'Ativo'
              DataSource = DsContas
              TabOrder = 5
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object DBEdNome2: TDBEdit
              Left = 60
              Top = 66
              Width = 309
              Height = 21
              Hint = 'Nome do banco'
              TabStop = False
              DataField = 'Nome2'
              DataSource = DsContas
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 6
            end
            object DBEdNome3: TDBEdit
              Left = 372
              Top = 66
              Width = 305
              Height = 21
              TabStop = False
              Color = clWhite
              DataField = 'Nome3'
              DataSource = DsContas
              TabOrder = 7
            end
            object DBEdEmpresa: TDBEdit
              Left = 372
              Top = 108
              Width = 305
              Height = 21
              TabStop = False
              Color = clWhite
              DataField = 'NOMEEMPRESA'
              DataSource = DsContas
              TabOrder = 8
            end
            object EdDBEntidade: TDBEdit
              Left = 372
              Top = 148
              Width = 305
              Height = 21
              TabStop = False
              Color = clWhite
              DataField = 'NOMEENTIDADE'
              DataSource = DsContas
              TabOrder = 9
            end
            object DBEdit1: TDBEdit
              Left = 60
              Top = 148
              Width = 309
              Height = 21
              TabStop = False
              Color = clWhite
              DataField = 'NOMECENTROCUSTO'
              DataSource = DsContas
              TabOrder = 10
            end
            object DBEdSubgrupo: TDBEdit
              Left = 60
              Top = 108
              Width = 309
              Height = 21
              TabStop = False
              Color = clWhite
              DataField = 'NOMESUBGRUPO'
              DataSource = DsContas
              TabOrder = 11
            end
            object DbEdTerceiro: TDBEdit
              Left = 432
              Top = 188
              Width = 245
              Height = 21
              TabStop = False
              Color = clWhite
              DataField = 'NOMETERCEIRO'
              DataSource = DsContas
              Enabled = False
              TabOrder = 12
            end
            object DBEdExcel: TDBEdit
              Left = 368
              Top = 188
              Width = 60
              Height = 21
              Hint = 'Nome do banco'
              TabStop = False
              CharCase = ecUpperCase
              Color = clWhite
              DataField = 'Excel'
              DataSource = DsContas
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 13
            end
            object DBRGRateio: TDBRadioGroup
              Left = 60
              Top = 213
              Width = 617
              Height = 41
              Caption = ' Rateio: '
              DataField = 'Rateio'
              DataSource = DsContas
              ParentBackground = True
              TabOrder = 14
            end
            object DBEdID: TDBEdit
              Left = 60
              Top = 188
              Width = 305
              Height = 21
              TabStop = False
              Color = clWhite
              DataField = 'ID'
              DataSource = DsContas
              TabOrder = 15
            end
            object DBCkCalculMesSeg: TDBCheckBox
              Left = 68
              Top = 364
              Width = 245
              Height = 17
              TabStop = False
              Caption = 'Pendente somente ap'#243's encerramento do m'#234's.'
              DataField = 'CalculMesSeg'
              DataSource = DsContas
              TabOrder = 16
              ValueChecked = '1'
              ValueUnchecked = '0'
              OnClick = DBCBMensalClick
            end
            object DBCheckBox1: TDBCheckBox
              Left = 516
              Top = 4
              Width = 97
              Height = 17
              Caption = 'Controla saldo.'
              DataField = 'CtrlaSdo'
              DataSource = DsContas
              TabOrder = 19
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object DBCheckBox2: TDBCheckBox
              Left = 516
              Top = 24
              Width = 97
              Height = 17
              Caption = 'Rateia provis'#227'o.'
              DataField = 'ProvRat'
              DataSource = DsContas
              TabOrder = 20
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Localiza'
          ImageIndex = 1
          object Panel13: TPanel
            Left = 0
            Top = 0
            Width = 727
            Height = 49
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label40: TLabel
              Left = 4
              Top = 4
              Width = 51
              Height = 13
              Caption = 'Descri'#231#227'o:'
            end
            object EdLocate: TdmkEdit
              Left = 3
              Top = 20
              Width = 366
              Height = 21
              MaxLength = 30
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              OnChange = EdLocateChange
              OnKeyDown = EdLocateKeyDown
            end
          end
          object DBGrid4: TDBGrid
            Left = 0
            Top = 49
            Width = 727
            Height = 359
            Align = alClient
            DataSource = DsLocate
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = DBGrid4DblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 180
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMESUBGRUPO'
                Title.Caption = 'Sub-grupo'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEEMPRESA'
                Title.Caption = 'Empresa'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETERCEIRO'
                Title.Caption = 'Terceiro'
                Width = 140
                Visible = True
              end>
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'Mensalidades pr'#233'-estipuladas'
          ImageIndex = 2
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 727
            Height = 101
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object DBGrid1: TDBGrid
              Left = 236
              Top = 48
              Width = 491
              Height = 53
              Align = alClient
              DataSource = DsContasItsCtas
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NOMECONTA'
                  Title.Caption = 'Contas dependentes'
                  Width = 449
                  Visible = True
                end>
            end
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 727
              Height = 48
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              object Label37: TLabel
                Left = 296
                Top = 6
                Width = 36
                Height = 13
                Caption = 'C'#243'digo:'
                FocusControl = EdDBCodigo
              end
              object Label38: TLabel
                Left = 360
                Top = 6
                Width = 51
                Height = 13
                Caption = 'Descri'#231#227'o:'
                FocusControl = EdDBDescricao
              end
              object BtInsere: TBitBtn
                Tag = 10
                Left = 4
                Top = 4
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Inclui novo registro'
                Caption = 'I&nsere'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtInsereClick
              end
              object BtExcluiIts: TBitBtn
                Tag = 12
                Left = 100
                Top = 4
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Exclui registro atual'
                Caption = 'E&xclui'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BtExcluiItsClick
              end
              object EdDBCodigo: TDBEdit
                Left = 296
                Top = 22
                Width = 61
                Height = 21
                Hint = 'N'#186' do banco'
                TabStop = False
                Color = clWhite
                DataField = 'Codigo'
                DataSource = DsContas
                Font.Charset = DEFAULT_CHARSET
                Font.Color = 8281908
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 2
              end
              object EdDBDescricao: TDBEdit
                Left = 360
                Top = 22
                Width = 357
                Height = 21
                Hint = 'Nome do banco'
                TabStop = False
                Color = clWhite
                DataField = 'Nome'
                DataSource = DsContas
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 3
              end
              object BtAlteraIts: TBitBtn
                Tag = 11
                Left = 196
                Top = 4
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Exclui registro atual'
                Caption = '&Altera'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 4
                OnClick = BtAlteraItsClick
              end
            end
            object DBGrid3: TDBGrid
              Left = 0
              Top = 48
              Width = 236
              Height = 53
              Align = alLeft
              DataSource = DsContasIts
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 2
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NOMEPERIODO'
                  Title.Caption = 'M'#234's'
                  Width = 91
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'ESTIPULADO'
                  Title.Alignment = taRightJustify
                  Title.Caption = 'Estipulado'
                  Width = 105
                  Visible = True
                end>
            end
          end
          object Panel8: TPanel
            Left = 0
            Top = 101
            Width = 727
            Height = 307
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            Visible = False
            object Panel10: TPanel
              Left = 0
              Top = 0
              Width = 272
              Height = 307
              Align = alLeft
              TabOrder = 0
              object Label36: TLabel
                Left = 105
                Top = 156
                Width = 86
                Height = 13
                Caption = 'Fator (Valor ou %):'
              end
              object GroupBox1: TGroupBox
                Left = 1
                Top = 1
                Width = 264
                Height = 64
                Caption = ' Per'#237'odo Inicial: '
                TabOrder = 0
                object Label32: TLabel
                  Left = 4
                  Top = 15
                  Width = 23
                  Height = 13
                  Caption = 'M'#234's:'
                end
                object Label33: TLabel
                  Left = 180
                  Top = 15
                  Width = 22
                  Height = 13
                  Caption = 'Ano:'
                end
                object CBMesI: TComboBox
                  Left = 5
                  Top = 32
                  Width = 172
                  Height = 21
                  Style = csDropDownList
                  Color = clWhite
                  DropDownCount = 12
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 7622183
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 0
                end
                object CBAnoI: TComboBox
                  Left = 179
                  Top = 32
                  Width = 78
                  Height = 21
                  Style = csDropDownList
                  Color = clWhite
                  DropDownCount = 3
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 7622183
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 1
                end
              end
              object GroupBox2: TGroupBox
                Left = 1
                Top = 65
                Width = 264
                Height = 64
                Caption = ' Per'#237'odo final: '
                TabOrder = 1
                object Label34: TLabel
                  Left = 4
                  Top = 15
                  Width = 23
                  Height = 13
                  Caption = 'M'#234's:'
                end
                object Label35: TLabel
                  Left = 180
                  Top = 15
                  Width = 22
                  Height = 13
                  Caption = 'Ano:'
                end
                object CBMesF: TComboBox
                  Left = 5
                  Top = 32
                  Width = 172
                  Height = 21
                  Style = csDropDownList
                  Color = clWhite
                  DropDownCount = 12
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 7622183
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 0
                end
                object CBAnoF: TComboBox
                  Left = 179
                  Top = 32
                  Width = 78
                  Height = 21
                  Style = csDropDownList
                  Color = clWhite
                  DropDownCount = 3
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 7622183
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 1
                end
              end
              object RGTipo: TRadioGroup
                Left = 1
                Top = 129
                Width = 100
                Height = 64
                Caption = ' Tipo de C'#225'culo: '
                ItemIndex = 0
                Items.Strings = (
                  'Valor'
                  'Porcentagem')
                TabOrder = 2
              end
              object EdFator: TdmkEdit
                Left = 105
                Top = 172
                Width = 88
                Height = 20
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 6
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,000000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object GroupBox5: TGroupBox
                Left = 1
                Top = 243
                Width = 270
                Height = 63
                Align = alBottom
                TabOrder = 4
                object BitBtn9: TBitBtn
                  Tag = 14
                  Left = 12
                  Top = 17
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Confirma'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  NumGlyphs = 2
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtConfirmaClick
                end
                object Panel9: TPanel
                  Left = 160
                  Top = 15
                  Width = 108
                  Height = 46
                  Align = alRight
                  BevelOuter = bvNone
                  TabOrder = 1
                  object BitBtn10: TBitBtn
                    Tag = 15
                    Left = 7
                    Top = 2
                    Width = 90
                    Height = 40
                    Cursor = crHandPoint
                    Caption = '&Desiste'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    NumGlyphs = 2
                    ParentFont = False
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    OnClick = BtDesisteClick
                  end
                end
              end
            end
            object Panel11: TPanel
              Left = 272
              Top = 0
              Width = 455
              Height = 307
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object Panel12: TPanel
                Left = 0
                Top = 0
                Width = 455
                Height = 48
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object BtIncluiIts: TBitBtn
                  Left = 4
                  Top = 4
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Hint = 'Inclui novo registro'
                  Caption = '&1. Insere'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtIncluiItsClick
                end
                object BtExclui2: TBitBtn
                  Left = 188
                  Top = 4
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Hint = 'Exclui registro atual'
                  Caption = '&2. Exclui'
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                  OnClick = BtExclui2Click
                end
              end
              object DBGrid2: TDBGrid
                Left = 0
                Top = 48
                Width = 455
                Height = 259
                Align = alClient
                DataSource = DsCtas
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Conta'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Nome'
                    Width = 350
                    Visible = True
                  end>
              end
            end
          end
        end
        object TabSheet4: TTabSheet
          Caption = 'Lan'#231'amentos'
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel14: TPanel
            Left = 0
            Top = 0
            Width = 727
            Height = 48
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label39: TLabel
              Left = 240
              Top = 4
              Width = 250
              Height = 13
              Caption = 'Conta para a qual ser'#227'o transferidos os lan'#231'amentos.'
            end
            object BtReciclo: TBitBtn
              Left = 628
              Top = 4
              Width = 90
              Height = 40
              Hint = 'Sub-grupos|Cadastro de sub-grupos de contas'
              Caption = '&Tranferir'
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtRecicloClick
            end
            object EdNova: TdmkEditCB
              Left = 240
              Top = 20
              Width = 65
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              OnChange = EdNovaChange
              DBLookupComboBox = CBNova
              IgnoraDBLookupComboBox = False
            end
            object CBNova: TdmkDBLookupComboBox
              Left = 308
              Top = 20
              Width = 313
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsNova
              TabOrder = 2
              dmkEditCB = EdNova
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
          object DBGLct: TDBGrid
            Left = 0
            Top = 48
            Width = 727
            Height = 360
            Align = alClient
            DataSource = DsLct
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Data'
                Width = 51
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Documento'
                Title.Caption = 'Cheque'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descricao'
                Title.Caption = 'Descri'#231#227'o'
                Width = 164
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Debito'
                Title.Caption = 'D'#233'bito'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Credito'
                Title.Caption = 'Cr'#233'dito'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Vencimento'
                Title.Caption = 'Vencim.'
                Width = 51
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'COMPENSADO_TXT'
                Title.Caption = 'Compen.'
                Width = 51
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MENSAL'
                Title.Caption = 'M'#234's'
                Width = 38
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMESIT'
                Title.Caption = 'Situa'#231#227'o'
                Width = 70
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'Lan'#231'to'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Sub'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SALDO'
                Title.Caption = 'Saldo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECLIENTE'
                Title.Caption = 'Membro'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEFORNECEDOR'
                Title.Caption = 'Fornecedor'
                Visible = True
              end>
          end
        end
        object TabSheet8: TTabSheet
          Caption = 'Dados espec'#237'ficos de entidades'
          ImageIndex = 4
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel18: TPanel
            Left = 0
            Top = 311
            Width = 727
            Height = 49
            Align = alBottom
            ParentBackground = False
            TabOrder = 0
            Visible = False
            object Label43: TLabel
              Left = 4
              Top = 4
              Width = 279
              Height = 13
              Caption = 'Entidade (que possui pelo menos uma carteira cadastrada):'
            end
            object Label42: TLabel
              Left = 392
              Top = 4
              Width = 137
              Height = 13
              Caption = 'Contrato de d'#233'bito em conta:'
            end
            object EdEnti2: TdmkEditCB
              Left = 4
              Top = 20
              Width = 45
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBEnti2
              IgnoraDBLookupComboBox = False
            end
            object CBEnti2: TdmkDBLookupComboBox
              Left = 52
              Top = 20
              Width = 337
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENT'
              ListSource = DsEnti2
              TabOrder = 1
              dmkEditCB = EdEnti2
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdCntrDebCta: TdmkEdit
              Left = 392
              Top = 20
              Width = 305
              Height = 21
              Color = clWhite
              MaxLength = 50
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
          end
          object Panel17: TPanel
            Left = 0
            Top = 0
            Width = 727
            Height = 48
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object Label44: TLabel
              Left = 296
              Top = 6
              Width = 36
              Height = 13
              Caption = 'C'#243'digo:'
              FocusControl = EdDBCodigo2
            end
            object Label45: TLabel
              Left = 360
              Top = 6
              Width = 51
              Height = 13
              Caption = 'Descri'#231#227'o:'
              FocusControl = EdDBDescricao2
            end
            object EdDBCodigo2: TDBEdit
              Left = 296
              Top = 22
              Width = 61
              Height = 21
              Hint = 'N'#186' do banco'
              TabStop = False
              Color = clWhite
              DataField = 'Codigo'
              DataSource = DsContas
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 8281908
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              ParentShowHint = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 0
            end
            object EdDBDescricao2: TDBEdit
              Left = 360
              Top = 22
              Width = 357
              Height = 21
              Hint = 'Nome do banco'
              TabStop = False
              Color = clWhite
              DataField = 'Nome'
              DataSource = DsContas
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
            end
            object Panel21: TPanel
              Left = 0
              Top = 0
              Width = 292
              Height = 48
              Align = alLeft
              BevelOuter = bvNone
              Caption = 'Panel21'
              TabOrder = 2
              object BitBtn3: TBitBtn
                Tag = 12
                Left = 100
                Top = 4
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Exclui registro atual'
                Caption = 'E&xclui'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
              end
              object BitBtn4: TBitBtn
                Tag = 11
                Left = 196
                Top = 4
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Exclui registro atual'
                Caption = '&Altera'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BitBtn4Click
              end
              object BitBtn2: TBitBtn
                Tag = 10
                Left = 4
                Top = 4
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Inclui novo registro'
                Caption = 'I&nsere'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
                OnClick = BitBtn2Click
              end
            end
          end
          object Panel20: TPanel
            Left = 0
            Top = 360
            Width = 727
            Height = 48
            Align = alBottom
            ParentBackground = False
            TabOrder = 2
            Visible = False
            object BitBtn1: TBitBtn
              Tag = 14
              Left = 8
              Top = 4
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
              Caption = '&Confirma'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BitBtn1Click
            end
            object BitBtn5: TBitBtn
              Tag = 15
              Left = 624
              Top = 4
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
              Caption = '&Desiste'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BitBtn5Click
            end
          end
          object DBGrid5: TDBGrid
            Left = 0
            Top = 48
            Width = 727
            Height = 263
            Align = alClient
            DataSource = DsContasEnt
            TabOrder = 3
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Entidade'
                Width = 49
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEENT'
                Title.Caption = 'Nome'
                Width = 366
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CntrDebCta'
                Title.Caption = 'Contrato d'#233'b. conta'
                Visible = True
              end>
          end
        end
        object TabSheet9: TTabSheet
          Caption = 'Mensalidades previstas'
          ImageIndex = 5
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel22: TPanel
            Left = 0
            Top = 0
            Width = 727
            Height = 48
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label46: TLabel
              Left = 296
              Top = 6
              Width = 36
              Height = 13
              Caption = 'C'#243'digo:'
              FocusControl = EdDBCodigo3
            end
            object Label47: TLabel
              Left = 360
              Top = 6
              Width = 51
              Height = 13
              Caption = 'Descri'#231#227'o:'
              FocusControl = EdDBDescricao3
            end
            object EdDBCodigo3: TDBEdit
              Left = 296
              Top = 22
              Width = 61
              Height = 21
              Hint = 'N'#186' do banco'
              TabStop = False
              Color = clWhite
              DataField = 'Codigo'
              DataSource = DsContas
              Font.Charset = DEFAULT_CHARSET
              Font.Color = 8281908
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              ParentShowHint = False
              ReadOnly = True
              ShowHint = True
              TabOrder = 0
            end
            object EdDBDescricao3: TDBEdit
              Left = 360
              Top = 22
              Width = 357
              Height = 21
              Hint = 'Nome do banco'
              TabStop = False
              Color = clWhite
              DataField = 'Nome'
              DataSource = DsContas
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
            end
            object Panel23: TPanel
              Left = 0
              Top = 0
              Width = 292
              Height = 48
              Align = alLeft
              BevelOuter = bvNone
              Caption = 'Panel21'
              TabOrder = 2
              object BitBtn6: TBitBtn
                Tag = 12
                Left = 100
                Top = 4
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Exclui registro atual'
                Caption = 'E&xclui'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BitBtn6Click
              end
              object BitBtn7: TBitBtn
                Tag = 11
                Left = 196
                Top = 4
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Exclui registro atual'
                Caption = '&Altera'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BitBtn7Click
              end
              object BitBtn8: TBitBtn
                Tag = 10
                Left = 4
                Top = 4
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Inclui novo registro'
                Caption = 'I&nsere'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
                OnClick = BitBtn8Click
              end
            end
          end
          object GradeContasMes: TdmkDBGrid
            Left = 0
            Top = 48
            Width = 727
            Height = 360
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descricao'
                Width = 300
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PERIODOINI_TXT'
                Title.Caption = 'In'#237'cio'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PERIODOFIM_TXT'
                Title.Caption = 'Final'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorMin'
                Title.Caption = 'Valor m'#237'n.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorMax'
                Title.Caption = 'Valor m'#225'x.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_CliInt'
                Title.Caption = 'Cliente interno'
                Visible = True
              end>
            Color = clWindow
            DataSource = DsContasMes
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Descricao'
                Width = 300
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PERIODOINI_TXT'
                Title.Caption = 'In'#237'cio'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PERIODOFIM_TXT'
                Title.Caption = 'Final'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorMin'
                Title.Caption = 'Valor m'#237'n.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorMax'
                Title.Caption = 'Valor m'#225'x.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_CliInt'
                Title.Caption = 'Cliente interno'
                Visible = True
              end>
          end
        end
      end
    end
    object PainelControle: TPanel
      Left = 0
      Top = 438
      Width = 792
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'ltimo registro'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Pr'#243'ximo registro'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Registro anterior'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Primeiro registro'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 322
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        Caption = 'Panel3'
        ParentColor = True
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 372
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
        object BtNivelAcima: TBitBtn
          Tag = 23
          Left = 280
          Top = 4
          Width = 90
          Height = 40
          Hint = 'Sub-grupos|Cadastro de sub-grupos de contas'
          Caption = '&Sub-grup'
          NumGlyphs = 2
          TabOrder = 1
          OnClick = BtNivelAcimaClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Exclui registro atual'
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtExcluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera registro atual'
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo registro'
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtIncluiClick
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 792
    Height = 486
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitLeft = 288
    ExplicitTop = 60
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 400
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label14: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label15: TLabel
        Left = 80
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label19: TLabel
        Left = 16
        Top = 50
        Width = 99
        Height = 13
        Caption = 'Descri'#231#227'o substituta:'
      end
      object Label27: TLabel
        Left = 332
        Top = 50
        Width = 153
        Height = 13
        Caption = 'Descri'#231#227'o padr'#227'o para hist'#243'rico:'
      end
      object Label17: TLabel
        Left = 16
        Top = 92
        Width = 52
        Height = 13
        Caption = 'Sub-grupo:'
      end
      object Label11: TLabel
        Left = 332
        Top = 92
        Width = 26
        Height = 13
        Caption = 'Sigla:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 24
        Width = 61
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNome: TdmkEdit
        Left = 80
        Top = 24
        Width = 245
        Height = 21
        Color = clWhite
        MaxLength = 50
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnExit = EdNomeExit
      end
      object CBDebito: TCheckBox
        Left = 340
        Top = 24
        Width = 53
        Height = 17
        Caption = 'D'#233'bito.'
        TabOrder = 2
      end
      object CBCredito: TCheckBox
        Left = 396
        Top = 24
        Width = 61
        Height = 17
        Caption = 'Cr'#233'dito.'
        TabOrder = 3
      end
      object CBExclusivo: TCheckBox
        Left = 460
        Top = 24
        Width = 67
        Height = 17
        Caption = 'Exclusivo.'
        TabOrder = 4
      end
      object CBAtivo: TCheckBox
        Left = 532
        Top = 24
        Width = 67
        Height = 17
        Caption = 'Ativo.'
        Checked = True
        State = cbChecked
        TabOrder = 5
      end
      object EdNome2: TdmkEdit
        Left = 16
        Top = 66
        Width = 309
        Height = 21
        Color = clWhite
        MaxLength = 50
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdNome3: TdmkEdit
        Left = 331
        Top = 66
        Width = 305
        Height = 21
        Color = clWhite
        MaxLength = 50
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object CBSubgrupo: TdmkDBLookupComboBox
        Left = 80
        Top = 108
        Width = 245
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSubGrupos
        TabOrder = 9
        dmkEditCB = EdSubgrupo
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdSubgrupo: TdmkEditCB
        Left = 16
        Top = 108
        Width = 61
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBSubgrupo
        IgnoraDBLookupComboBox = False
      end
      object PageControl2: TPageControl
        Left = 0
        Top = 145
        Width = 792
        Height = 255
        ActivePage = TabSheet7
        Align = alBottom
        TabOrder = 12
        object TabSheet5: TTabSheet
          Caption = 'Mensal'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 782
          ExplicitHeight = 0
          object Panel15: TPanel
            Left = 0
            Top = 0
            Width = 784
            Height = 281
            Align = alTop
            ParentBackground = False
            TabOrder = 0
            ExplicitLeft = 152
            ExplicitTop = -8
            ExplicitWidth = 782
            object GroupBox3: TGroupBox
              Left = 8
              Top = 12
              Width = 617
              Height = 129
              Caption = '    '
              TabOrder = 0
              object Label16: TLabel
                Left = 8
                Top = 20
                Width = 96
                Height = 13
                Caption = 'Dia da mensalidade:'
              end
              object Label21: TLabel
                Left = 116
                Top = 20
                Width = 117
                Height = 13
                Caption = 'D'#233'bito mensal esperado:'
              end
              object Label22: TLabel
                Left = 240
                Top = 20
                Width = 107
                Height = 13
                Caption = 'D'#233'bito mensal m'#237'nimo:'
              end
              object Label23: TLabel
                Left = 364
                Top = 20
                Width = 119
                Height = 13
                Caption = 'Cr'#233'dito mensal esperado:'
              end
              object Label24: TLabel
                Left = 488
                Top = 20
                Width = 109
                Height = 13
                Caption = 'Cr'#233'dito mensal m'#237'nimo:'
              end
              object EdMensDia: TdmkEdit
                Left = 8
                Top = 36
                Width = 105
                Height = 21
                Alignment = taRightJustify
                MaxLength = 2
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                OnExit = EdMensDiaExit
              end
              object EdMensdeb: TdmkEdit
                Left = 116
                Top = 36
                Width = 121
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object Edmensmind: TdmkEdit
                Left = 240
                Top = 36
                Width = 121
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdMenscred: TdmkEdit
                Left = 364
                Top = 36
                Width = 121
                Height = 21
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdMensminc: TdmkEdit
                Left = 488
                Top = 36
                Width = 121
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object CkCalculMesSeg: TCheckBox
                Left = 8
                Top = 104
                Width = 245
                Height = 17
                Caption = 'Pendente somente ap'#243's encerramento do m'#234's.'
                Enabled = False
                TabOrder = 6
              end
              object RGPendenMesSeg: TRadioGroup
                Left = 8
                Top = 60
                Width = 601
                Height = 41
                Caption = ' C'#225'lculo da pend'#234'ncia: '
                Columns = 3
                ItemIndex = 0
                Items.Strings = (
                  'Meses futuros n'#227'o considerar'
                  'Considerar sempre'
                  'Proporcional na data atual.')
                TabOrder = 5
              end
            end
            object CBMensal: TCheckBox
              Left = 20
              Top = 10
              Width = 61
              Height = 17
              Caption = 'Mensal: '
              TabOrder = 1
              OnClick = CBMensalClick
            end
          end
        end
        object TabSheet6: TTabSheet
          Caption = 'Extrato'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel16: TPanel
            Left = 0
            Top = 0
            Width = 784
            Height = 227
            Align = alClient
            ParentBackground = False
            TabOrder = 0
            object Label41: TLabel
              Left = 8
              Top = 8
              Width = 168
              Height = 13
              Caption = 'Agrupamento de contas em extrato:'
            end
            object Label18: TLabel
              Left = 8
              Top = 92
              Width = 193
              Height = 13
              Caption = 'Identificador autom'#225'tico em importa'#231#245'es:'
              Enabled = False
            end
            object EdContasAgr: TdmkEditCB
              Left = 8
              Top = 24
              Width = 61
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBContasAgr
              IgnoraDBLookupComboBox = False
            end
            object CBContasAgr: TdmkDBLookupComboBox
              Left = 72
              Top = 24
              Width = 340
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsContasAgr
              TabOrder = 1
              dmkEditCB = EdContasAgr
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdID: TdmkEdit
              Left = 8
              Top = 108
              Width = 305
              Height = 21
              Color = clWhite
              Enabled = False
              MaxLength = 50
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object CkContasSum: TCheckBox
              Left = 8
              Top = 64
              Width = 265
              Height = 17
              Caption = 'Soma os itens iguais no extrato.'
              TabOrder = 3
            end
          end
        end
        object TabSheet7: TTabSheet
          Caption = 'Espec'#237'ficos'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel24: TPanel
            Left = 0
            Top = 0
            Width = 784
            Height = 227
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label20: TLabel
              Left = 12
              Top = 8
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object Label29: TLabel
              Left = 12
              Top = 48
              Width = 78
              Height = 13
              Caption = 'Centro de custo:'
            end
            object Label30: TLabel
              Left = 324
              Top = 8
              Width = 45
              Height = 13
              Caption = 'Entidade:'
            end
            object Label25: TLabel
              Left = 388
              Top = 48
              Width = 107
              Height = 13
              Caption = 'Espec'#237'fico de terceiro:'
              Enabled = False
            end
            object Label26: TLabel
              Left = 324
              Top = 48
              Width = 60
              Height = 13
              Caption = 'C'#233'lula excel:'
            end
            object EdEmpresa: TdmkEditCB
              Left = 12
              Top = 24
              Width = 61
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBEmpresa
              IgnoraDBLookupComboBox = False
            end
            object EdCentroCusto: TdmkEditCB
              Left = 12
              Top = 64
              Width = 61
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBCentroCusto
              IgnoraDBLookupComboBox = False
            end
            object CBCentroCusto: TdmkDBLookupComboBox
              Left = 76
              Top = 64
              Width = 245
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DscentroCusto
              TabOrder = 2
              dmkEditCB = EdCentroCusto
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object CBEmpresa: TdmkDBLookupComboBox
              Left = 76
              Top = 24
              Width = 245
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsEpresas
              TabOrder = 3
              dmkEditCB = EdEmpresa
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdEntidade: TdmkEditCB
              Left = 324
              Top = 24
              Width = 61
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBEntidade
              IgnoraDBLookupComboBox = False
            end
            object CBEntidade: TdmkDBLookupComboBox
              Left = 388
              Top = 24
              Width = 245
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsEntidades
              TabOrder = 5
              dmkEditCB = EdEntidade
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdExcel: TdmkEdit
              Left = 324
              Top = 64
              Width = 56
              Height = 21
              CharCase = ecUpperCase
              Color = clWhite
              MaxLength = 6
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              OnExit = EdNomeExit
            end
            object CBTerceiro: TDBLookupComboBox
              Left = 384
              Top = 64
              Width = 245
              Height = 21
              Color = clWhite
              Enabled = False
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsTerceiros
              TabOrder = 7
            end
            object RGRateio: TRadioGroup
              Left = 12
              Top = 88
              Width = 617
              Height = 41
              Caption = ' Rateio: '
              Columns = 3
              Items.Strings = (
                'Pessoas'
                #193'rea (m'#178')'
                'Consumo kw')
              TabOrder = 8
            end
            object CGNotPrntBal: TdmkCheckGroup
              Left = 12
              Top = 132
              Width = 617
              Height = 89
              Caption = ' N'#227'o imprimir nos seguintes modelos de balancete: '
              Items.Strings = (
                'Balancete de resultado industrial modelo 01')
              TabOrder = 9
              UpdType = utYes
              Value = 0
              OldValor = 0
            end
          end
        end
      end
      object CkCtrlaSdo: TCheckBox
        Left = 528
        Top = 112
        Width = 93
        Height = 17
        Caption = 'Controla saldo.'
        TabOrder = 11
      end
      object EdSigla: TdmkEdit
        Left = 332
        Top = 108
        Width = 185
        Height = 21
        Color = clWhite
        MaxLength = 15
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnExit = EdNomeExit
      end
      object CkProvRat: TCheckBox
        Left = 628
        Top = 112
        Width = 93
        Height = 17
        Caption = 'Rateia provis'#227'o.'
        TabOrder = 13
      end
    end
    object Grade: TStringGrid
      Left = 684
      Top = 8
      Width = 89
      Height = 69
      ColCount = 1
      DefaultRowHeight = 18
      FixedCols = 0
      RowCount = 3
      FixedRows = 0
      TabOrder = 1
      Visible = False
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 423
      Width = 792
      Height = 63
      Align = alBottom
      TabOrder = 2
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel19: TPanel
        Left = 682
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 528
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 242
        Height = 32
        Caption = 'Cadastro de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 242
        Height = 32
        Caption = 'Cadastro de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 242
        Height = 32
        Caption = 'Cadastro de Contas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 792
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel25: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrContasBeforeClose
    AfterScroll = QrContasAfterScroll
    SQL.Strings = (
      'SELECT co.*, sg.Nome NOMESUBGRUPO, cc.Nome NOMECENTROCUSTO,'
      
        'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial ELSE cl.Nome END NOMEEMP' +
        'RESA,'
      
        'CASE WHEN te.Tipo=0 THEN te.RazaoSocial ELSE te.Nome END NOMETER' +
        'CEIRO,'
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMEENT' +
        'IDADE'
      'FROM contas co'
      'LEFT JOIN subgrupos   sg ON sg.Codigo=co.Subgrupo'
      'LEFT JOIN centrocusto cc ON cc.Codigo=co.CentroCusto'
      'LEFT JOIN entidades   cl ON cl.Codigo=co.Empresa'
      'LEFT JOIN entidades   te ON te.Codigo=co.Terceiro'
      'LEFT JOIN entidades   en ON en.Codigo=co.Entidade')
    Left = 728
    Top = 140
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'contas.Codigo'
      Required = True
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'contas.Nome'
      Required = True
      Size = 50
    end
    object QrContasNome2: TWideStringField
      FieldName = 'Nome2'
      Origin = 'contas.Nome2'
      Required = True
      Size = 50
    end
    object QrContasNome3: TWideStringField
      FieldName = 'Nome3'
      Origin = 'contas.Nome3'
      Required = True
      Size = 50
    end
    object QrContasID: TWideStringField
      FieldName = 'ID'
      Origin = 'contas.ID'
      Size = 50
    end
    object QrContasSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Origin = 'contas.Subgrupo'
      Required = True
    end
    object QrContasEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'contas.Empresa'
      Required = True
    end
    object QrContasCredito: TWideStringField
      FieldName = 'Credito'
      Origin = 'contas.Credito'
      Size = 1
    end
    object QrContasDebito: TWideStringField
      FieldName = 'Debito'
      Origin = 'contas.Debito'
      Size = 1
    end
    object QrContasMensal: TWideStringField
      FieldName = 'Mensal'
      Origin = 'contas.Mensal'
      Size = 1
    end
    object QrContasExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Origin = 'contas.Exclusivo'
      Size = 1
    end
    object QrContasMensdia: TSmallintField
      FieldName = 'Mensdia'
      Origin = 'contas.Mensdia'
    end
    object QrContasMensdeb: TFloatField
      FieldName = 'Mensdeb'
      Origin = 'contas.Mensdeb'
    end
    object QrContasMensmind: TFloatField
      FieldName = 'Mensmind'
      Origin = 'contas.Mensmind'
    end
    object QrContasMenscred: TFloatField
      FieldName = 'Menscred'
      Origin = 'contas.Menscred'
    end
    object QrContasMensminc: TFloatField
      FieldName = 'Mensminc'
      Origin = 'contas.Mensminc'
    end
    object QrContasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'contas.Lk'
    end
    object QrContasTerceiro: TIntegerField
      FieldName = 'Terceiro'
      Origin = 'contas.Terceiro'
    end
    object QrContasExcel: TWideStringField
      FieldName = 'Excel'
      Origin = 'contas.Excel'
      Size = 6
    end
    object QrContasDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'contas.DataCad'
    end
    object QrContasDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'contas.DataAlt'
    end
    object QrContasUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'contas.UserCad'
    end
    object QrContasUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'contas.UserAlt'
    end
    object QrContasCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
      Origin = 'contas.CentroCusto'
      Required = True
    end
    object QrContasNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Origin = 'subgrupos.Nome'
      Size = 50
    end
    object QrContasNOMECENTROCUSTO: TWideStringField
      FieldName = 'NOMECENTROCUSTO'
      Origin = 'centrocusto.Nome'
      Size = 50
    end
    object QrContasNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrContasNOMETERCEIRO: TWideStringField
      FieldName = 'NOMETERCEIRO'
      Size = 100
    end
    object QrContasRateio: TIntegerField
      FieldName = 'Rateio'
      Origin = 'contas.Rateio'
      Required = True
    end
    object QrContasEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'contas.Entidade'
      Required = True
    end
    object QrContasNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrContasAntigo: TWideStringField
      FieldName = 'Antigo'
      Origin = 'contas.Antigo'
    end
    object QrContasPendenMesSeg: TSmallintField
      FieldName = 'PendenMesSeg'
      Origin = 'contas.PendenMesSeg'
      Required = True
    end
    object QrContasCalculMesSeg: TSmallintField
      FieldName = 'CalculMesSeg'
      Origin = 'contas.CalculMesSeg'
      Required = True
    end
    object QrContasOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
      Origin = 'contas.OrdemLista'
      Required = True
    end
    object QrContasContasAgr: TIntegerField
      FieldName = 'ContasAgr'
    end
    object QrContasContasSum: TIntegerField
      FieldName = 'ContasSum'
      Required = True
    end
    object QrContasCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
      Required = True
    end
    object QrContasAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrContasNotPrntBal: TIntegerField
      FieldName = 'NotPrntBal'
      Required = True
    end
    object QrContasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrContasSigla: TWideStringField
      DisplayWidth = 15
      FieldName = 'Sigla'
      Required = True
    end
    object QrContasProvRat: TSmallintField
      FieldName = 'ProvRat'
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 756
    Top = 140
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Genero'
      'FROM :p0'
      'WHERE Genero=:P1')
    Left = 424
    Top = 305
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocGenero: TIntegerField
      FieldName = 'Genero'
    end
  end
  object QrSubGrupos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome FROM subgrupos'
      'ORDER BY Nome')
    Left = 728
    Top = 168
    object QrSubGruposCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.subgrupos.Codigo'
    end
    object QrSubGruposNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.subgrupos.Nome'
      Size = 14
    end
  end
  object DsSubGrupos: TDataSource
    DataSet = QrSubGrupos
    Left = 756
    Top = 168
  end
  object QrEmpresas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE 1'
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades')
    Left = 204
    Top = 80
    object QrEmpresasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmpresasNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 45
    end
  end
  object DsEpresas: TDataSource
    DataSet = QrEmpresas
    Left = 232
    Top = 80
  end
  object QrTerceiros: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE 1'
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades')
    Left = 204
    Top = 52
    object QrTerceirosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTerceirosNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 45
    end
  end
  object DsTerceiros: TDataSource
    DataSet = QrTerceiros
    Left = 232
    Top = 52
  end
  object QrCentroCusto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM centrocusto'
      'ORDER BY Nome')
    Left = 728
    Top = 284
    object QrCentroCustoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCentroCustoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DscentroCusto: TDataSource
    DataSet = QrCentroCusto
    Left = 756
    Top = 284
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE '
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 728
    Top = 312
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 756
    Top = 312
  end
  object QrContasIts: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrContasItsBeforeClose
    AfterScroll = QrContasItsAfterScroll
    OnCalcFields = QrContasItsCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM contasits'
      'WHERE Codigo=:P0'
      'ORDER BY Periodo DESC')
    Left = 728
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContasItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasItsPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrContasItsTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrContasItsFator: TFloatField
      FieldName = 'Fator'
    end
    object QrContasItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContasItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContasItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContasItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrContasItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrContasItsNOMEPERIODO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEPERIODO'
      Size = 30
      Calculated = True
    end
    object QrContasItsESTIPULADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ESTIPULADO'
      Calculated = True
    end
  end
  object DsContasIts: TDataSource
    DataSet = QrContasIts
    Left = 756
    Top = 196
  end
  object QrCtas: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM ctasitsctas')
    Left = 204
    Top = 108
    object QrCtasConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCtasNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsCtas: TDataSource
    DataSet = QrCtas
    Left = 232
    Top = 108
  end
  object QrLoc2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo=:P0'
      '')
    Left = 452
    Top = 305
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLoc2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLoc2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrContasItsCtas: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrContasItsCalcFields
    SQL.Strings = (
      'SELECT cc.*, co.Nome NOMECONTA '
      'FROM contasitsctas cc'
      'LEFT JOIN contas co ON co.Codigo=cc.Conta'
      'WHERE cc.Codigo=:P0'
      'AND cc.Periodo=:P1'
      'ORDER BY NOMECONTA')
    Left = 728
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrContasItsCtasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContasItsCtasPeriodo: TIntegerField
      FieldName = 'Periodo'
      Required = True
    end
    object QrContasItsCtasConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrContasItsCtasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContasItsCtasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContasItsCtasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContasItsCtasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrContasItsCtasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrContasItsCtasNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
  end
  object DsContasItsCtas: TDataSource
    DataSet = QrContasItsCtas
    Left = 756
    Top = 224
  end
  object QrLocate: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT co.*, sg.Nome NOMESUBGRUPO, cc.Nome NOMECENTROCUSTO,'
      
        'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial ELSE cl.Nome END NOMEEMP' +
        'RESA,'
      
        'CASE WHEN te.Tipo=0 THEN te.RazaoSocial ELSE te.Nome END NOMETER' +
        'CEIRO,'
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMEENT' +
        'IDADE'
      'FROM contas co'
      'LEFT JOIN subgrupos   sg ON sg.Codigo=co.Subgrupo'
      'LEFT JOIN centrocusto cc ON cc.Codigo=co.CentroCusto'
      'LEFT JOIN entidades   cl ON cl.Codigo=co.Empresa'
      'LEFT JOIN entidades   te ON te.Codigo=co.Terceiro'
      'LEFT JOIN entidades   en ON en.Codigo=co.Entidade'
      'ORDER BY Nome')
    Left = 204
    Top = 472
    object QrLocateCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLocateNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrLocateNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object QrLocateNome3: TWideStringField
      FieldName = 'Nome3'
      Required = True
      Size = 50
    end
    object QrLocateID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrLocateSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrLocateEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrLocateCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrLocateDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrLocateMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrLocateExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrLocateMensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrLocateMensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrLocateMensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrLocateMenscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrLocateMensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrLocateLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocateTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrLocateExcel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrLocateDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocateDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocateUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLocateUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLocateCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
      Required = True
    end
    object QrLocateRateio: TIntegerField
      FieldName = 'Rateio'
      Required = True
    end
    object QrLocateEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object QrLocateNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 50
    end
    object QrLocateNOMECENTROCUSTO: TWideStringField
      FieldName = 'NOMECENTROCUSTO'
      Size = 50
    end
    object QrLocateNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrLocateNOMETERCEIRO: TWideStringField
      FieldName = 'NOMETERCEIRO'
      Size = 100
    end
    object QrLocateNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsLocate: TDataSource
    DataSet = QrLocate
    Left = 232
    Top = 472
  end
  object QrNova: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo>0'
      'AND Codigo<>:P0'
      'ORDER BY Nome')
    Left = 204
    Top = 446
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsNova: TDataSource
    DataSet = QrNova
    Left = 232
    Top = 446
  end
  object DsLct: TDataSource
    DataSet = QrLct
    Left = 592
    Top = 84
  end
  object QrLct: TmySQLQuery
    Database = Dmod.MyDB
    Left = 620
    Top = 84
    object QrLctData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLctCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrLctGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 25
    end
    object QrLctNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrLctDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctDocumento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000; ; '
    end
    object QrLctSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLctVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLctFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLctCONTA: TIntegerField
      FieldName = 'CONTA'
    end
    object QrLctNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      FixedChar = True
      Size = 128
    end
    object QrLctNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Size = 128
    end
    object QrLctNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 4
    end
    object QrLctNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 4
    end
    object QrLctNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Size = 4
    end
    object QrLctNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrLctAno: TFloatField
      FieldName = 'Ano'
    end
    object QrLctMENSAL: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrLctMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrLctBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLctLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrLctFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrLctSub: TSmallintField
      FieldName = 'Sub'
      DisplayFormat = '00; ; '
    end
    object QrLctCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrLctLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrLctPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLctSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrLctID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrLctMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLctcliente: TIntegerField
      FieldName = 'cliente'
    end
    object QrLctMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLctNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrLctNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrLctTIPOEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrLctNOMERELACIONADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMERELACIONADO'
      Size = 50
      Calculated = True
    end
    object QrLctOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrLctLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrLctMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLctATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrLctJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLctDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLctNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrLctVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLctAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrLctMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrLctProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrLctDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLctDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLctUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrLctUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLctID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrLctCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrLctFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLctICMS_P: TFloatField
      FieldName = 'ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrLctCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrLctCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLctDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLctDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrLctPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object QrContasAgr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contasagr'
      'ORDER BY Nome')
    Left = 449
    Top = 181
    object QrContasAgrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasAgrNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContasAgr: TDataSource
    DataSet = QrContasAgr
    Left = 477
    Top = 181
  end
  object DsEnti2: TDataSource
    DataSet = QrEnti2
    Left = 236
    Top = 408
  end
  object QrEnti2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 208
    Top = 408
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object QrContasEnt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMEENT, ce.*'
      'FROM contasent ce'
      'LEFT JOIN entidades en ON en.Codigo=ce.Codigo'
      'WHERE ce.Codigo=:P0'
      'ORDER BY NOMEENT'
      '')
    Left = 450
    Top = 210
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContasEntNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrContasEntCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContasEntEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object QrContasEntCntrDebCta: TWideStringField
      FieldName = 'CntrDebCta'
      Size = 50
    end
    object QrContasEntLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContasEntDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContasEntDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContasEntUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrContasEntUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrContasEntAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrContasEntAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsContasEnt: TDataSource
    DataSet = QrContasEnt
    Left = 478
    Top = 210
  end
  object QrContasMes: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrContasMesCalcFields
    SQL.Strings = (
      'SELECT IF(en.Tipo=0,RazaoSocial,en.Nome) NO_CliInt, cm.* '
      'FROM contasmes cm'
      'LEFT JOIN entidades en ON en.Codigo=cm.CliInt'
      'WHERE cm.Codigo=:P0')
    Left = 728
    Top = 252
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContasMesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasMesControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrContasMesCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrContasMesDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrContasMesPeriodoIni: TIntegerField
      FieldName = 'PeriodoIni'
    end
    object QrContasMesPeriodoFim: TIntegerField
      FieldName = 'PeriodoFim'
    end
    object QrContasMesValorMin: TFloatField
      FieldName = 'ValorMin'
    end
    object QrContasMesValorMax: TFloatField
      FieldName = 'ValorMax'
    end
    object QrContasMesPERIODOINI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PERIODOINI_TXT'
      Size = 10
      Calculated = True
    end
    object QrContasMesPERIODOFIM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PERIODOFIM_TXT'
      Size = 10
      Calculated = True
    end
    object QrContasMesNO_CliInt: TWideStringField
      FieldName = 'NO_CliInt'
      Size = 100
    end
  end
  object DsContasMes: TDataSource
    DataSet = QrContasMes
    Left = 756
    Top = 252
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtExclui
    CanDel01 = BtExclui
    Left = 260
    Top = 12
  end
end
