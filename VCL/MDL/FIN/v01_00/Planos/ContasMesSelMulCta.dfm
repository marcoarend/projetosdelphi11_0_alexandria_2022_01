object FmContasMesSelMulCta: TFmContasMesSelMulCta
  Left = 339
  Top = 185
  Caption = 'FIN-PLCTA-028 :: Previs'#245'es Mensais - M'#250'ltipla Sele'#231#227'o de Contas'
  ClientHeight = 494
  ClientWidth = 646
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 646
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 534
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtTodos: TBitBtn
      Tag = 127
      Left = 221
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Todos'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtTodosClick
    end
    object BtNenhum: TBitBtn
      Tag = 128
      Left = 333
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Nenhum'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = BtNenhumClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 646
    Height = 48
    Align = alTop
    Caption = 'Previs'#245'es Mensais - M'#250'ltipla Sele'#231#227'o de Contas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 644
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 646
    Height = 398
    Align = alClient
    TabOrder = 0
    object GradeContasMes: TdmkDBGridDAC
      Left = 1
      Top = 18
      Width = 644
      Height = 379
      SQLFieldsToChange.Strings = (
        'Ativo')
      SQLIndexesOnUpdate.Strings = (
        'Codigo')
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = '?'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CodUsu'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Motivo da sa'#237'da'
          Width = 519
          Visible = True
        end>
      Color = clWindow
      DataSource = DsContasMes
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      SQLTable = 'cad_0'
      EditForceNextYear = False
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = '?'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CodUsu'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Motivo da sa'#237'da'
          Width = 519
          Visible = True
        end>
    end
    object StaticText1: TStaticText
      Left = 1
      Top = 1
      Width = 222
      Height = 17
      Align = alTop
      Alignment = taCenter
      Caption = 'Somente contas mensais s'#227'o exibidas!'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
  object QrContasMes: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * '
      'FROM cad_0'
      'ORDER BY Nome')
    Left = 36
    Top = 120
    object QrContasMesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasMesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrContasMesNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrContasMesAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsContasMes: TDataSource
    DataSet = QrContasMes
    Left = 64
    Top = 120
  end
end
