unit ContasSdoTrf;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, Mask,
  ComCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral;

type
  TFmContasSdoTrf = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Label3: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdCtaOrig: TdmkEditCB;
    CBCtaOrig: TdmkDBLookupComboBox;
    EdValor: TdmkEdit;
    Label4: TLabel;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENT: TWideStringField;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    CkContinuar: TCheckBox;
    Label5: TLabel;
    EdCtaDest: TdmkEditCB;
    CBCtaDest: TdmkDBLookupComboBox;
    TPDataT: TDateTimePicker;
    Label6: TLabel;
    LaTipo: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FControle: Integer;
  end;

  var
  FmContasSdoTrf: TFmContasSdoTrf;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts;

{$R *.DFM}

procedure TFmContasSdoTrf.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasSdoTrf.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContasSdoTrf.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmContasSdoTrf.FormCreate(Sender: TObject);
begin
  QrEntidades.Open;
  QrContas.Open;
  TPDataT.Date := Date;
end;

procedure TFmContasSdoTrf.BtOKClick(Sender: TObject);
var
  Controle, CtaOrig, CtaDest, Entidade: Integer;
  Valor: Double;
begin
  CtaOrig  := Geral.IMV(EdCtaOrig.Text);
  CtaDest  := Geral.IMV(EdCtaDest.Text);
  Entidade := Geral.IMV(EdEntidade.Text);
  Valor    := Geral.DMV(EdValor.Text);
  if (CtaOrig = 0) or (CtaDest = 0) or (Entidade = 0) or (Valor < 0.01) then
  begin
    Application.MessageBox('A entidade o valor e a contas s�o obrigat�rios!',
    'Aviso', MB_OK + MB_ICONWARNING);
    if Entidade = 0 then
      EdEntidade.SetFocus
    else if CtaOrig = 0 then
      EdCtaOrig.SetFocus
    else
      EdCtaDest.SetFocus;
    Exit;
  end;
  if CtaOrig = CtaDest then
  begin
    Application.MessageBox('As contas de origem e destino devem ser diferentes!',
    'Aviso', MB_OK + MB_ICONWARNING);
    EdCtaOrig.SetFocus;
    Exit;
  end;

  Controle := UMyMod.BuscaEmLivreY_Def_Old('ContasTrf', 'Controle', LaTipo.Caption, FControle);
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, LaTipo.Caption, 'ContasTrf', False,
  [
    'DataT', 'CtaOrig', 'CtaDest', 'Entidade', 'Valor'
  ], ['Controle'],
  [
    Geral.FDT(TPDataT.Date, 1), CtaOrig, CtaDest, Entidade, Valor
  ], [Controle]) then
  begin
   if CkContinuar.Checked then
   begin
     LaTipo.Caption := CO_INCLUSAO;
     Application.MessageBox(PChar(LaTipo.Caption + ' realizada com sucesso!'), 'Mensagem',
     MB_OK+MB_ICONINFORMATION);
   end else
     Close;
  end;
end;

end.
