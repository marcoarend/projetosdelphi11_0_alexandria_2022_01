object FmContasExclui: TFmContasExclui
  Left = 406
  Top = 221
  Caption = 'FIN-PLCTA-017 :: Exclus'#227'o de Conta'
  ClientHeight = 393
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 40
    Width = 792
    Height = 305
    Align = alClient
    TabOrder = 0
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 60
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 117
        Height = 13
        Caption = 'Conta que ser'#225' excluida:'
      end
      object Label2: TLabel
        Left = 400
        Top = 4
        Width = 320
        Height = 13
        Caption = 
          'Conta para a qual ser'#227'o transferidos os lan'#231'amentos (se houverem' +
          ').'
      end
      object Label3: TLabel
        Left = 16
        Top = 44
        Width = 179
        Height = 13
        Caption = 'Lan'#231'amentos da conta a ser exclu'#237'da'
      end
      object EdConta: TdmkEditCB
        Left = 12
        Top = 20
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdContaChange
        DBLookupComboBox = CBConta
        IgnoraDBLookupComboBox = False
      end
      object CBConta: TdmkDBLookupComboBox
        Left = 80
        Top = 20
        Width = 313
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsContas
        TabOrder = 1
        dmkEditCB = EdConta
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdNova: TdmkEditCB
        Left = 400
        Top = 20
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBNova
        IgnoraDBLookupComboBox = False
      end
      object CBNova: TdmkDBLookupComboBox
        Left = 468
        Top = 20
        Width = 313
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsNova
        TabOrder = 3
        dmkEditCB = EdNova
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object DBGLct: TDBGrid
      Left = 1
      Top = 61
      Width = 790
      Height = 243
      Align = alClient
      DataSource = DsLct
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDrawColumnCell = DBGLctDrawColumnCell
      Columns = <
        item
          Expanded = False
          FieldName = 'Data'
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Documento'
          Title.Caption = 'Cheque'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 164
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D'#233'bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Cr'#233'dito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Title.Caption = 'Vencim.'
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'COMPENSADO_TXT'
          Title.Caption = 'Compen.'
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MENSAL'
          Title.Caption = 'M'#234's'
          Width = 38
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMESIT'
          Title.Caption = 'Situa'#231#227'o'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'Lan'#231'to'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Sub'
          Width = 24
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SALDO'
          Title.Caption = 'Saldo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLIENTE'
          Title.Caption = 'Membro'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEFORNECEDOR'
          Title.Caption = 'Fornecedor'
          Visible = True
        end>
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 345
    Width = 792
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 14
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 686
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
      Caption = '&Sair'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSaidaClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 40
    Align = alTop
    Caption = 'Exclus'#227'o de Conta'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 790
      Height = 38
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 36
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 320
    Top = 50
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrContasAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo>0'
      'ORDER BY Nome')
    Left = 292
    Top = 50
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsNova: TDataSource
    DataSet = QrNova
    Left = 688
    Top = 62
  end
  object QrNova: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Codigo>0'
      'AND Codigo<>:P0'
      'ORDER BY Nome')
    Left = 660
    Top = 62
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsLct: TDataSource
    DataSet = QrLct
    Left = 12
    Top = 144
  end
  object QrLct: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLctCalcFields
    Left = 40
    Top = 144
    object QrLctData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLctCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrLctGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 25
    end
    object QrLctNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrLctDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctDocumento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000; ; '
    end
    object QrLctSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLctVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLctFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLctCONTA: TIntegerField
      FieldName = 'CONTA'
    end
    object QrLctNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      FixedChar = True
      Size = 128
    end
    object QrLctNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Size = 128
    end
    object QrLctNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 4
    end
    object QrLctNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 4
    end
    object QrLctNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Size = 4
    end
    object QrLctNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrLctAno: TFloatField
      FieldName = 'Ano'
    end
    object QrLctMENSAL: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrLctMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrLctBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLctLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrLctFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrLctSub: TSmallintField
      FieldName = 'Sub'
      DisplayFormat = '00; ; '
    end
    object QrLctCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrLctLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrLctPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLctSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrLctID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrLctMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLctcliente: TIntegerField
      FieldName = 'cliente'
    end
    object QrLctMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLctNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrLctNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrLctTIPOEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrLctNOMERELACIONADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMERELACIONADO'
      Size = 50
      Calculated = True
    end
    object QrLctOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrLctLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrLctMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLctATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrLctJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLctDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLctNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrLctVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLctAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrLctMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrLctProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrLctDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLctDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLctUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrLctUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLctID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrLctCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrLctFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLctICMS_P: TFloatField
      FieldName = 'ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrLctCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrLctCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLctDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLctDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrLctPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 28
    Top = 4
  end
end
