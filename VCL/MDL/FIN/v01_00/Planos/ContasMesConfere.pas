unit ContasMesConfere;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkDBGrid, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, ComCtrls, ABSMain,
  UnFinanceiro, dmkGeral, MyDBCheck, UnDmkEnums;

type
  TFmContasMesConfere = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    LaMes: TLabel;
    CBMes: TComboBox;
    CBAno: TComboBox;
    LaAno: TLabel;
    GradeDados: TDBGrid;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrCliIntCodigo: TIntegerField;
    QrCliIntNOMECLI: TWideStringField;
    QrContasMes: TmySQLQuery;
    PB1: TProgressBar;
    DataSource1: TDataSource;
    QrPesq: TmySQLQuery;
    QrPesqDebito: TFloatField;
    QrPesqQtde: TLargeintField;
    Query: TmySQLQuery;
    QueryCodigo: TIntegerField;
    QueryNomeCta: TWideStringField;
    QueryQtdeMin: TIntegerField;
    QueryQtdeMax: TIntegerField;
    QueryQtdeExe: TIntegerField;
    QueryValrMin: TFloatField;
    QueryValrMax: TFloatField;
    QueryValrExe: TFloatField;
    QueryStatus: TIntegerField;
    QueryNOMESTATUS: TWideStringField;
    QrContasMesCodigo: TIntegerField;
    QrContasMesControle: TIntegerField;
    QrContasMesCliInt: TIntegerField;
    QrContasMesDescricao: TWideStringField;
    QrContasMesPeriodoIni: TIntegerField;
    QrContasMesPeriodoFim: TIntegerField;
    QrContasMesValorMin: TFloatField;
    QrContasMesValorMax: TFloatField;
    QrContasMesQtdeMin: TIntegerField;
    QrContasMesQtdeMax: TIntegerField;
    QrContasMesNOMECONTA: TWideStringField;
    BtInclui: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QueryCalcFields(DataSet: TDataSet);
    procedure GradeDadosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtIncluiClick(Sender: TObject);
    procedure QueryBeforeClose(DataSet: TDataSet);
    procedure QueryAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    FConfPgtos: String;
    procedure ReopenQuery(Codigo: Integer);
  public
    { Public declarations }
    FFinalidade: TLanctoFinalidade;
    FQrLct, FQrCrt: TmySQLQuery;
    FPercJuroM, FPercMulta: Double;
    FSetaVars: TInsAltReopenLct;
    FAlteraAtehFatID, FLockCliInt, FLockForneceI, FLockAccount, FLockVendedor:
    Boolean;
    FCliente, FFornecedor, FForneceI, FAccount, FVendedor, FIDFinalidade: Integer;
  end;

  var
  FmContasMesConfere: TFmContasMesConfere;

implementation

uses UnMyObjects, Module, Principal, ModuleGeral, UCreate, UMySQLModule, LctEdit,
UnInternalConsts;

{$R *.DFM}

procedure TFmContasMesConfere.BtIncluiClick(Sender: TObject);
const
  Controle   = 0;
  Sub        = 0;
  FatID      = 0;
  FatID_Sub  = 0;
  FatNum     = 0;
  Carteira   = 0;
  Compensado = 0;
  //
  Credito    = 0.00;
  Debito     = 0.00;
  //
var
  Genero, CliInt: Integer;
  Data, Vencto, DataDoc: TDateTime;
  Mes: TDateTime;
begin
  Genero  := QueryCodigo.Value;
  CliInt  := EdCliInt.ValueVariant;
  Data    := Date();
  Vencto  := Date();
  DataDoc := Date();
  Mes     := EncodeDate(Geral.IMV(CBAno.Text), CBMes.ItemIndex + 1, 1);
  if UFinanceiro.InclusaoLancamento(TFmLctEdit, FmLctEdit, FFinalidade,
  afmoNegarComAviso, FQrLct, FQrCrt, tgrInclui, Controle, Sub, Genero,
  FPercJuroM, FPercMulta, FSetaVars, FatID, FatID_Sub, FatNum, Carteira,
  Credito, Debito, FAlteraAtehFatID, FCliente, FFornecedor, CliInt, FForneceI,
  FAccount, FVendedor, FLockCliInt, FLockForneceI, FLockAccount, FLockVendedor,
  Data, Vencto, Compensado, DataDoc, FIDFinalidade, Mes) > 0 then
  begin
    BtOKClick(Self);
  end;
end;

procedure TFmContasMesConfere.BtOKClick(Sender: TObject);
  procedure ReabrePesq(EntInt, Genero, Mez: Integer);
  begin
    QrPesq.Close;
    QrPesq.SQL.Clear;
    QrPesq.SQL.Add('SELECT COUNT(Controle) Qtde, SUM(lan.Debito) Debito');
    QrPesq.SQL.Add('FROM ' + VAR_LCT + ' lan');
    QrPesq.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    QrPesq.SQL.Add('WHERE car.Tipo <> 1');
    QrPesq.SQL.Add('AND car.ForneceI=:P0');
    QrPesq.SQL.Add('AND lan.Genero=:P1');
    QrPesq.SQL.Add('AND lan.Mez=:P2');
    QrPesq.Params[00].AsInteger := EntInt;
    QrPesq.Params[01].AsInteger := Genero;
    QrPesq.Params[02].AsInteger := Mez;
    QrPesq.Open;
    //
  end;
  procedure InsereItemQuery();
  var
    x, Codigo, Status: Integer;
    NomeCta: String;
    QtdeMin, QtdeMax, QtdeExe, ValrMin, ValrMax, ValrExe: Double;
  begin
    if (QrPesqQtde.Value < QrContasMesQtdeMin.Value)
    or (QrPesqDebito.Value < QrContasMesValorMin.Value) then
      x := 0 else
    if (QrPesqQtde.Value > QrContasMesQtdeMax.Value)
    and (QrPesqDebito.Value < QrContasMesValorMin.Value) then
      x := 1 else
    if (QrPesqQtde.Value > QrContasMesQtdeMax.Value)
    and (QrPesqDebito.Value < QrContasMesValorMax.Value) then
      x := 2 else
    if (QrPesqQtde.Value > QrContasMesQtdeMax.Value)
    and (QrPesqDebito.Value > QrContasMesValorMin.Value) then
      x := 4 else
    begin
      if (QrContasMesValorMin.Value <= 0 ) then
        x := -1
      else
        x := 3;
    end;
    //
    Codigo  := QrContasMesCodigo.Value;
    Status  := x;
    NomeCta := QrContasMesNOMECONTA.Value;
    QtdeMin := QrContasMesQtdeMin.Value;
    QtdeMax := QrContasMesQtdeMax.Value;
    QtdeExe := QrPesqQtde.Value;
    ValrMin := QrContasMesValorMin.Value;
    ValrMax := QrContasMesValorMax.Value;
    ValrExe := QrPesqDebito.Value;
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'ConfPgtos', False, [
    'Codigo', 'NomeCta', 'QtdeMin',
    'QtdeMax', 'QtdeExe', 'ValrMin',
    'ValrMax', 'ValrExe', 'Status'], [
    ], [
    Codigo, NomeCta, QtdeMin,
    QtdeMax, QtdeExe, ValrMin,
    ValrMax, ValrExe, Status], [
    ], False);
  end;
var
  Mez, Perio, EntInt, Genero: Integer;
  DataI, DataF: String;
begin
  EntInt := EdCliInt.ValueVariant;
  if EntInt = 0 then
  begin
    Application.MessageBox('Informe o cliente interno!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    FConfPgtos := UCriar.RecriaTempTable('ConfPgtos', DModG.QrUpdPID1, False);
    Query.Database := DmodG.MyPID_DB;
    //
    Mez := ((Geral.IMV(CBAno.Text)-2000) * 100) + CBMes.ItemIndex + 1;
    Perio := ((Geral.IMV(CBAno.Text)-2000) * 12) + CBMes.ItemIndex + 1;
    DataI := MLAGeral.PrimeiroDiaDoPeriodo(Perio, dtSystem);
    DataF := MLAGeral.UltimoDiaDoPeriodo(Perio, dtSystem);
    //
    QrContasMes.Close;
    QrContasMes.Params[0].AsInteger := EntInt;
    QrContasMes.Open;
    //
    PB1.Position := 0;
    PB1.Max := QrContasMes.RecordCount;
    while not QrContasMes.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      PB1.Update;
      Application.ProcessMessages;
      //
      Genero := QrContasMesCodigo.Value;
      ReabrePesq(EntInt, Genero, Mez);
      InsereItemQuery();
      QrContasMes.Next;
    end;
    ReopenQuery(0);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmContasMesConfere.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasMesConfere.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContasMesConfere.FormCreate(Sender: TObject);
begin
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, -1);
  QrCliInt.Open;
  EdCliInt.ValueVariant := FmPrincipal.FEntInt;
  CBCliInt.KeyValue     := FmPrincipal.FEntInt;
end;

procedure TFmContasMesConfere.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmContasMesConfere.GradeDadosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
  if (Column.FieldName = 'NOMESTATUS') then
  begin
    case QueryStatus.Value of
      0: Cor := clRed;
      1: Cor := clPurple;
      2: Cor := clNavy;
      3: Cor := clBlue;
      4: Cor := clGreen;
      else Cor := clFuchsia;
    end;
    with GradeDados.Canvas do
    begin
      if Cor = clBlack then Font.Style := [] else Font.Style := [fsBold];
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
end;

procedure TFmContasMesConfere.QueryAfterOpen(DataSet: TDataSet);
begin
  BtInclui.Enabled := Query.RecordCount > 0;
end;

procedure TFmContasMesConfere.QueryBeforeClose(DataSet: TDataSet);
begin
  BtInclui.Enabled := False;
end;

procedure TFmContasMesConfere.QueryCalcFields(DataSet: TDataSet);
var
  p1, p2, p3: String;
begin
  if QueryQtdeMin.Value - QueryQtdeExe.Value > 1 then p1 := 's' else p1 := '';
  if QueryQtdeExe.Value > 1 then p2 := 's' else p2 := '';
  if QueryQtdeExe.Value > 1 then p3 := 'm' else p3 := '';
  //
  case QueryStatus.Value of
    -1: QueryNOMESTATUS.Value := 'Valor m�nimo inv�lido';
    0: QueryNOMESTATUS.Value := 'Lan�amento' + p1 + ' pendente' + p1;
    1: QueryNOMESTATUS.Value := 'Lan�amento' + p2 + ' excede' + p3 + ', e o valor total � insuficiente';
    2: QueryNOMESTATUS.Value := 'Lan�amento' + p2 + ' excede' + p3 + ', mas o valor total � suficiente';
    3: QueryNOMESTATUS.Value := 'Lan�amento' + p2 + ' e valor total conferem';
    4: QueryNOMESTATUS.Value := 'Lan�amento' + p2 + ' e valor total excedem';
    else QueryNOMESTATUS.Value := '### STATUS DESCONHECIDO ###';
  end;
end;

procedure TFmContasMesConfere.ReopenQuery(Codigo: Integer);
begin
  Query.Close;
  UmyMod.AbreQuery(Query, DModG.MyPID_DB, 'TFmContasMesSelMulCta.AtivarTodos()');
  Query.Locate('Codigo', Codigo, []);
end;

end.

