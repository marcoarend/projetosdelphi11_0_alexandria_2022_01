unit ContasLnkEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkCheckGroup,
  dmkLabel, dmkEdit, dmkEditCB, dmkDBLookupComboBox, dmkCheckBox, dmkGeral;

type
  TFmContasLnkEdit = class(TForm)
    PainelDados: TPanel;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    LaConta: TLabel;
    CBConta: TdmkDBLookupComboBox;
    DsContas: TDataSource;
    EdConta: TdmkEditCB;
    QrContas: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    EdTexto: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdDoc: TdmkEdit;
    Label4: TLabel;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    EdBanco: TdmkEditCB;
    CBBanco: TdmkDBLookupComboBox;
    Label5: TLabel;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrCliIntCodigo: TIntegerField;
    QrCliIntNOMEENTI: TWideStringField;
    QrBancos: TmySQLQuery;
    DsBancos: TDataSource;
    QrBancosCodigo: TIntegerField;
    QrBancosNome: TWideStringField;
    Label1: TLabel;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    Label6: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    QrFornece: TmySQLQuery;
    DsFornece: TDataSource;
    QrCliente: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsCliente: TDataSource;
    QrForneceCodigo: TIntegerField;
    QrForneceNOMEENTI: TWideStringField;
    CkUsaEntBank: TdmkCheckBox;
    CGComposHist: TdmkCheckGroup;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FConta, FControle: Integer;
  end;

var
  FmContasLnkEdit: TFmContasLnkEdit;

implementation

uses UnMyObjects, Module, ContasLnk;

{$R *.DFM}

procedure TFmContasLnkEdit.BtDesisteClick(Sender: TObject);
begin
  VAR_CONTA := 0;
  Close;
end;

procedure TFmContasLnkEdit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContasLnkEdit.FormCreate(Sender: TObject);
begin
  QrContas.Open;
  QrBancos.Open;
  QrCliInt.Open;
  QrCliente.Open;
  QrFornece.Open;
end;

procedure TFmContasLnkEdit.BtConfirmaClick(Sender: TObject);
var
  Texto, Doc: String;
  CliInt, Banco, Codigo, Considera, Cliente, Fornece, UsaEntBank, ComposHist,
  Controle: Integer;
begin
  VAR_CONTA := Geral.IMV(EdConta.Text);
  if (VAR_CONTA = 0) and (EdConta.Enabled) then
  begin
    Application.MessageBox(PChar('Informe uma Conta!'), 'Erro', MB_OK+MB_ICONERROR);
    EdConta.SetFocus;
    Exit;
  end;
  {
  Dmod.QrUpd.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Dmod.QrUpd.SQL.Add('INSERT INTO contaslnk SET ');
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'ContasLnk', 'ContasLnk', 'Controle');
  end else begin
    Dmod.QrUpd.SQL.Add('UPDATE contaslnk SET ');
    Controle := FControle;
  end;
  Dmod.QrUpd.SQL.Add('Texto=:P0, Doc=:P1, CliInt=:P2, Banco=:P3, ');
  Dmod.QrUpd.SQL.Add('Codigo=:P4, Considera=:P5, Cliente=:P6, Fornece=:P7, ');
  Dmod.QrUpd.SQL.Add('UsaEntBank=:P8, ComposHist=:P9, ');
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpd.SQL.Add('DataCad=:Pa, UserCad=:Pb, Controle=:Pc')
  else begin
    Dmod.QrUpd.SQL.Add('DataAlt=:Pa, UserAlt=:Pb WHERE Controle=:Pc ');
    Dmod.QrUpd.SQL.Add('AND Codigo=:Pd');
  end;
  Dmod.QrUpd.Params[00].AsString  := EdTexto.Text;
  Dmod.QrUpd.Params[01].AsString  := EdDoc.Text;
  Dmod.QrUpd.Params[02].AsInteger := Geral.IMV(EdCliInt.Text);
  Dmod.QrUpd.Params[03].AsInteger := Geral.IMV(EdBanco.Text);
  Dmod.QrUpd.Params[04].AsInteger := VAR_CONTA;
  Dmod.QrUpd.Params[05].AsInteger := MLAGeral.BoolToInt(EdConta.Enabled);
  Dmod.QrUpd.Params[06].AsInteger := Geral.IMV(EdCliente.Text);
  Dmod.QrUpd.Params[07].AsInteger := Geral.IMV(EdFornece.Text);
  Dmod.QrUpd.Params[08].AsInteger := MLAGeral.BoolToInt(CkUsaEntBank.Checked);
  Dmod.QrUpd.Params[09].AsInteger := CGComposHist.Value;
  //
  Dmod.QrUpd.Params[10].AsString  := Geral.FDT(Date, 1);
  Dmod.QrUpd.Params[11].AsInteger := VAR_USUARIO;
  Dmod.QrUpd.Params[12].AsInteger := Controle;
  if LaTipo.Caption = CO_ALTERACAO then
    Dmod.QrUpd.Params[13].AsInteger := FConta;
  Dmod.QrUpd.ExecSQL;
  }
  Texto      := EdTexto.Text;
  Doc        := EdDoc.Text;
  CliInt     := Geral.IMV(EdCliInt.Text);
  Banco      := Geral.IMV(EdBanco.Text);
  Codigo     := VAR_CONTA;
  Considera  := MLAGeral.BoolToInt(EdConta.Enabled);
  Cliente    := Geral.IMV(EdCliente.Text);
  Fornece    := Geral.IMV(EdFornece.Text);
  UsaEntBank := MLAGeral.BoolToInt(CkUsaEntBank.Checked);
  ComposHist := CGComposHist.Value;
  //
  Controle := UMyMod.BuscaEmLivreY_Def('contaslnk', 'Controle', LaTipo.SQLType, FControle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'contaslnk', False, [
    'Codigo', 'CliInt', 'Banco',
    'Texto', 'Doc', 'Considera',
    'Cliente', 'Fornece', 'UsaEntBank',
    'ComposHist'
  ], ['Controle'], [
    Codigo, CliInt, Banco,
    Texto, Doc, Considera,
    Cliente, Fornece, UsaEntBank,
    ComposHist
  ], [Controle], True) then
  begin
    try
      if FindWindow('TFmContasLnk', nil) > 0 then
        FmContasLnk.LocCod(FmContasLnk.QrContasLnkControle.Value, Controle);
    finally
      Close;
    end;
  end;
end;

end.
