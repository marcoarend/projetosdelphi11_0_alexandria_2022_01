object FmSubGrupos: TFmSubGrupos
  Left = 386
  Top = 245
  Caption = 'FIN-PLCTA-004 :: Cadastro de Sub-grupos'
  ClientHeight = 342
  ClientWidth = 726
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 726
    Height = 246
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Visible = False
    object Label9: TLabel
      Left = 16
      Top = 8
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label10: TLabel
      Left = 124
      Top = 8
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object Label5: TLabel
      Left = 412
      Top = 8
      Width = 32
      Height = 13
      Caption = 'Grupo:'
    end
    object EdCodigo: TdmkEdit
      Left = 16
      Top = 24
      Width = 100
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8281908
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object PainelConfirma: TPanel
      Left = 1
      Top = 197
      Width = 724
      Height = 48
      Align = alBottom
      TabOrder = 5
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
        NumGlyphs = 2
      end
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 624
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtDesisteClick
        NumGlyphs = 2
      end
    end
    object EdNome: TdmkEdit
      Left = 124
      Top = 24
      Width = 285
      Height = 21
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdGrupo: TdmkEditCB
      Left = 412
      Top = 24
      Width = 45
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBGrupo
    end
    object CBGrupo: TdmkDBLookupComboBox
      Left = 460
      Top = 24
      Width = 257
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsGrupos
      TabOrder = 3
      dmkEditCB = EdGrupo
      UpdType = utYes
    end
    object GroupBox1: TGroupBox
      Left = 16
      Top = 52
      Width = 325
      Height = 85
      Caption = ' Configura'#231#227'o de Resultados mensais: '
      TabOrder = 4
      object Label4: TLabel
        Left = 13
        Top = 20
        Width = 74
        Height = 13
        Caption = 'Ordem na Lista:'
      end
      object EdOrdemLista: TdmkEdit
        Left = 12
        Top = 36
        Width = 77
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object RGTipoAgrupa: TRadioGroup
        Left = 96
        Top = 16
        Width = 225
        Height = 61
        Caption = ' Tipo de Agrupamento: '
        Columns = 3
        ItemIndex = 2
        Items.Strings = (
          'D'#233'bito'
          'Cr'#233'dito'
          'Ambos')
        TabOrder = 1
      end
    end
    object CkCtrlaSdo: TCheckBox
      Left = 16
      Top = 144
      Width = 97
      Height = 17
      Caption = 'Controla saldo.'
      TabOrder = 6
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 726
    Height = 246
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 724
      Height = 216
      ActivePage = TabSheet1
      TabOrder = 0
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = 'Dados'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel1: TPanel
          Left = 0
          Top = 0
          Width = 716
          Height = 188
          Align = alClient
          BevelOuter = bvNone
          Enabled = False
          ParentBackground = False
          TabOrder = 0
          object Label1: TLabel
            Left = 16
            Top = 8
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
          end
          object Label2: TLabel
            Left = 120
            Top = 8
            Width = 51
            Height = 13
            Caption = 'Descri'#231#227'o:'
          end
          object Label3: TLabel
            Left = 416
            Top = 8
            Width = 32
            Height = 13
            Caption = 'Grupo:'
          end
          object DBEdCodigo: TDBEdit
            Left = 16
            Top = 23
            Width = 100
            Height = 21
            BiDiMode = bdLeftToRight
            DataField = 'Codigo'
            DataSource = DsSubGrupos
            ParentBiDiMode = False
            TabOrder = 0
          end
          object DBEdNome: TDBEdit
            Left = 120
            Top = 23
            Width = 293
            Height = 21
            BiDiMode = bdLeftToRight
            DataField = 'Nome'
            DataSource = DsSubGrupos
            ParentBiDiMode = False
            TabOrder = 1
          end
          object GroupBox2: TGroupBox
            Left = 16
            Top = 52
            Width = 325
            Height = 85
            Caption = ' Configura'#231#227'o de Resultados mensais: '
            TabOrder = 2
            object Label6: TLabel
              Left = 13
              Top = 20
              Width = 74
              Height = 13
              Caption = 'Ordem na Lista:'
            end
            object EdDBOrdem: TDBEdit
              Left = 12
              Top = 36
              Width = 77
              Height = 21
              DataField = 'OrdemLista'
              DataSource = DsSubGrupos
              TabOrder = 0
            end
            object RadioGroup1: TDBRadioGroup
              Left = 96
              Top = 16
              Width = 225
              Height = 61
              Caption = ' Tipo de Agrupamento: '
              Columns = 3
              DataField = 'TipoAgrupa'
              DataSource = DsSubGrupos
              Items.Strings = (
                'D'#233'bito'
                'Cr'#233'dito'
                'Ambos')
              ParentBackground = True
              TabOrder = 1
              Values.Strings = (
                '0'
                '1'
                '2')
            end
          end
          object DBEdGrupo: TDBEdit
            Left = 416
            Top = 23
            Width = 293
            Height = 21
            BiDiMode = bdLeftToRight
            DataField = 'NOMEGRUPO'
            DataSource = DsSubGrupos
            ParentBiDiMode = False
            TabOrder = 3
          end
          object DBCheckBox1: TDBCheckBox
            Left = 16
            Top = 148
            Width = 97
            Height = 17
            Caption = 'Controla saldo.'
            DataField = 'CtrlaSdo'
            DataSource = DsSubGrupos
            TabOrder = 4
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Localiza'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel13: TPanel
          Left = 0
          Top = 0
          Width = 716
          Height = 49
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          object Label40: TLabel
            Left = 4
            Top = 4
            Width = 51
            Height = 13
            Caption = 'Descri'#231#227'o:'
          end
          object EdLocate: TdmkEdit
            Left = 7
            Top = 20
            Width = 310
            Height = 21
            MaxLength = 30
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnChange = EdLocateChange
            OnKeyDown = EdLocateKeyDown
          end
          object RGOrdem2: TRadioGroup
            Left = 612
            Top = 1
            Width = 103
            Height = 47
            Align = alRight
            Caption = ' Ordem 2: '
            ItemIndex = 0
            Items.Strings = (
              'Crescente'
              'Decrescente')
            TabOrder = 1
            OnClick = RGOrdem2Click
          end
          object RGOrdem1: TRadioGroup
            Left = 324
            Top = 1
            Width = 288
            Height = 47
            Align = alRight
            Caption = ' Ordem 1: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Descri'#231#227'o'
              'C'#243'digo'
              'Tipo agrupamento'
              'Ordem lista')
            TabOrder = 2
            OnClick = RGOrdem1Click
          end
        end
        object DBGrid1: TDBGrid
          Left = 0
          Top = 49
          Width = 716
          Height = 139
          Align = alClient
          DataSource = DsLocate
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGrid1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Nome'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TipoAgrupa'
              Title.Caption = 'Tipo Agrupam.'
              Width = 77
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OrdemLista'
              Title.Caption = 'Ordem Lista'
              Visible = True
            end>
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Contas'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid2: TDBGrid
          Left = 0
          Top = 49
          Width = 716
          Height = 139
          Align = alClient
          DataSource = DsContas
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              ReadOnly = True
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 220
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OrdemLista'
              Title.Caption = 'Ordem lista'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome2'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome3'
              Width = 177
              Visible = True
            end>
        end
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 716
          Height = 49
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object Label7: TLabel
            Left = 4
            Top = 4
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
          end
          object Label8: TLabel
            Left = 108
            Top = 4
            Width = 51
            Height = 13
            Caption = 'Descri'#231#227'o:'
          end
          object Label11: TLabel
            Left = 404
            Top = 4
            Width = 32
            Height = 13
            Caption = 'Grupo:'
          end
          object EdDBCodigo: TDBEdit
            Left = 4
            Top = 19
            Width = 100
            Height = 21
            BiDiMode = bdLeftToRight
            DataField = 'Codigo'
            DataSource = DsSubGrupos
            ParentBiDiMode = False
            TabOrder = 0
          end
          object EdDBDescricao: TDBEdit
            Left = 108
            Top = 19
            Width = 293
            Height = 21
            BiDiMode = bdLeftToRight
            DataField = 'Nome'
            DataSource = DsSubGrupos
            ParentBiDiMode = False
            TabOrder = 1
          end
          object EdDBGrupo: TDBEdit
            Left = 404
            Top = 19
            Width = 293
            Height = 21
            BiDiMode = bdLeftToRight
            DataField = 'NOMEGRUPO'
            DataSource = DsSubGrupos
            ParentBiDiMode = False
            TabOrder = 2
          end
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 726
    Height = 48
    Align = alTop
    Alignment = taLeftJustify
    Caption = '                              Cadastro de Sub-grupos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TLabel
      Left = 643
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 642
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 417
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 415
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 0
        NumGlyphs = 2
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 1
        NumGlyphs = 2
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 2
        OnClick = SbNumeroClick
        NumGlyphs = 2
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 3
        OnClick = SbNomeClick
        NumGlyphs = 2
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 4
        OnClick = SbQueryClick
        NumGlyphs = 2
      end
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 294
    Width = 726
    Height = 48
    Align = alBottom
    TabOrder = 2
    object LaRegistro: TLabel
      Left = 173
      Top = 1
      Width = 26
      Height = 13
      Align = alClient
      Caption = '[N]: 0'
    end
    object Panel5: TPanel
      Left = 1
      Top = 1
      Width = 172
      Height = 46
      Align = alLeft
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object SpeedButton4: TBitBtn
        Tag = 4
        Left = 128
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'ltimo registro'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = SpeedButton4Click
        NumGlyphs = 2
      end
      object SpeedButton3: TBitBtn
        Tag = 3
        Left = 88
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'Pr'#243'ximo registro'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = SpeedButton3Click
        NumGlyphs = 2
      end
      object SpeedButton2: TBitBtn
        Tag = 2
        Left = 48
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'Registro anterior'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = SpeedButton2Click
        NumGlyphs = 2
      end
      object SpeedButton1: TBitBtn
        Tag = 1
        Left = 8
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'Primeiro registro'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = SpeedButton1Click
        NumGlyphs = 2
      end
    end
    object Panel3: TPanel
      Left = 256
      Top = 1
      Width = 469
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 372
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 188
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Exclui banco atual'
        Caption = '&Exclui'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtExcluiClick
        NumGlyphs = 2
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 96
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Altera banco atual'
        Caption = '&Altera'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtAlteraClick
        NumGlyphs = 2
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 4
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Inclui novo banco'
        Caption = '&Inclui'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtIncluiClick
        NumGlyphs = 2
      end
    end
  end
  object DsSubGrupos: TDataSource
    DataSet = QrSubGrupos
    Left = 488
    Top = 161
  end
  object QrSubGrupos: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSubGruposAfterOpen
    AfterScroll = QrSubGruposAfterScroll
    SQL.Strings = (
      'SELECT sg.*, gr.Nome NOMEGRUPO'
      'FROM subgrupos sg'
      'LEFT JOIN grupos gr ON gr.Codigo=sg.Grupo'
      'WHERE sg.Codigo > 0')
    Left = 460
    Top = 161
    object QrSubGruposCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'subgrupos.Codigo'
      Required = True
    end
    object QrSubGruposGrupo: TIntegerField
      FieldName = 'Grupo'
      Origin = 'subgrupos.Grupo'
      Required = True
    end
    object QrSubGruposNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'subgrupos.Nome'
      Required = True
      Size = 50
    end
    object QrSubGruposLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'subgrupos.Lk'
    end
    object QrSubGruposDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'subgrupos.DataCad'
    end
    object QrSubGruposDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'subgrupos.DataAlt'
    end
    object QrSubGruposUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'subgrupos.UserCad'
    end
    object QrSubGruposUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'subgrupos.UserAlt'
    end
    object QrSubGruposNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Origin = 'grupos.Nome'
      Required = True
      Size = 50
    end
    object QrSubGruposOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
      Origin = 'subgrupos.OrdemLista'
      Required = True
    end
    object QrSubGruposTipoAgrupa: TIntegerField
      FieldName = 'TipoAgrupa'
      Origin = 'subgrupos.TipoAgrupa'
      Required = True
    end
    object QrSubGruposCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
      Origin = 'subgrupos.CtrlaSdo'
      Required = True
    end
  end
  object QrGrupos: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrSubGruposBeforeOpen
    AfterOpen = QrSubGruposAfterOpen
    AfterScroll = QrSubGruposAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM grupos'
      'ORDER BY Nome')
    Left = 628
    Top = 17
    object QrGruposCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'grupos.Codigo'
    end
    object QrGruposNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'grupos.Nome'
      Size = 50
    end
  end
  object DsGrupos: TDataSource
    DataSet = QrGrupos
    Left = 656
    Top = 17
  end
  object QrLocate: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM subgrupos'
      'ORDER BY TipoAgrupa, OrdemLista, Nome')
    Left = 240
    Top = 113
    object QrLocateCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'subgrupos.Codigo'
    end
    object QrLocateNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'subgrupos.Nome'
      Size = 50
    end
    object QrLocateGrupo: TIntegerField
      FieldName = 'Grupo'
      Origin = 'subgrupos.Grupo'
    end
    object QrLocateLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'subgrupos.Lk'
    end
    object QrLocateDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'subgrupos.DataCad'
    end
    object QrLocateDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'subgrupos.DataAlt'
    end
    object QrLocateUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'subgrupos.UserCad'
    end
    object QrLocateUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'subgrupos.UserAlt'
    end
    object QrLocateOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
      Origin = 'subgrupos.OrdemLista'
    end
    object QrLocateTipoAgrupa: TIntegerField
      FieldName = 'TipoAgrupa'
      Origin = 'subgrupos.TipoAgrupa'
    end
  end
  object DsLocate: TDataSource
    DataSet = QrLocate
    Left = 268
    Top = 113
  end
  object TbContas: TmySQLTable
    Database = Dmod.MyDB
    Filtered = True
    SortFieldNames = 'OrdemLista,Nome'
    TableName = 'contas'
    Left = 169
    Top = 117
    object TbContasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'contas.Codigo'
    end
    object TbContasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'contas.Nome'
      Size = 50
    end
    object TbContasNome2: TWideStringField
      FieldName = 'Nome2'
      Origin = 'contas.Nome2'
      Size = 50
    end
    object TbContasNome3: TWideStringField
      FieldName = 'Nome3'
      Origin = 'contas.Nome3'
      Size = 50
    end
    object TbContasOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
      Origin = 'contas.OrdemLista'
    end
    object TbContasID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object TbContasSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
    end
    object TbContasCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
    object TbContasEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object TbContasCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object TbContasDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object TbContasMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object TbContasExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object TbContasMensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object TbContasMensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object TbContasMensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object TbContasMenscred: TFloatField
      FieldName = 'Menscred'
    end
    object TbContasMensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object TbContasTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object TbContasExcel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object TbContasRateio: TIntegerField
      FieldName = 'Rateio'
    end
    object TbContasEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object TbContasAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object TbContasLk: TIntegerField
      FieldName = 'Lk'
    end
    object TbContasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbContasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbContasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbContasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object TbContasPendenMesSeg: TSmallintField
      FieldName = 'PendenMesSeg'
    end
    object TbContasCalculMesSeg: TSmallintField
      FieldName = 'CalculMesSeg'
    end
  end
  object DsContas: TDataSource
    DataSet = TbContas
    Left = 197
    Top = 117
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtExclui
    CanDel01 = BtExclui
    Left = 260
    Top = 12
  end
end
