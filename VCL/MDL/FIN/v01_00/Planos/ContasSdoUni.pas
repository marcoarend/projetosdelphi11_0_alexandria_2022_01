unit ContasSdoUni;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, Mask,
  dmkPermissoes, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral;

type
  TFmContasSdoUni = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Label3: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdCodigo: TdmkEditCB;
    CBCodigo: TdmkDBLookupComboBox;
    EdSdoIni: TdmkEdit;
    Label4: TLabel;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENT: TWideStringField;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrPesq: TmySQLQuery;
    QrPesqSdoIni: TFloatField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    CkContinuar: TCheckBox;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdEntidadeChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenPesq();
  public
    { Public declarations }
    FExecutou: Boolean;
  end;

  var
  FmContasSdoUni: TFmContasSdoUni;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts;

{$R *.DFM}

procedure TFmContasSdoUni.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasSdoUni.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ReopenPesq();
end;

procedure TFmContasSdoUni.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmContasSdoUni.EdEntidadeChange(Sender: TObject);
begin
  ReopenPesq();
end;

procedure TFmContasSdoUni.FormCreate(Sender: TObject);
begin
  FExecutou := False;
  QrEntidades.Open;
  QrContas.Open;
end;

procedure TFmContasSdoUni.BtOKClick(Sender: TObject);
var
  Codigo, Entidade: Integer;
  Status: String;
begin
  if QrPesq.State <> dsBrowse then ReopenPesq();
  Codigo := Geral.IMV(EdCodigo.Text);
  Entidade := Geral.IMV(EdEntidade.Text);
  if (Codigo = 0) or (Entidade = 0) then
  begin
    Application.MessageBox('A entidade e a conta s�o obrigat�rias!',
    'Aviso', MB_OK + MB_ICONWARNING);
    if Codigo = 0 then
      EdCodigo.SetFocus
    else
      EdEntidade.SetFocus;
    Exit;
  end;
  if QrPesq.RecordCount = 0 then
    Status := CO_INCLUSAO
  else
    Status := CO_ALTERACAO;
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, Status, 'ContasSdo', False, 
  [
    'SdoIni'
  ], ['Codigo', 'Entidade'],
  [
    Geral.DMV(EdSdoIni.Text)
  ], [Codigo, Entidade]) then
  begin
    FExecutou := true;
    if CkContinuar.Checked then
    begin
      ReopenPesq();
      Application.MessageBox(PChar(Status + ' realizada com sucesso!'), 'Mensagem',
      MB_OK+MB_ICONINFORMATION);
    end else
      Close;
  end;   
end;

procedure TFmContasSdoUni.ReopenPesq();
begin
  QrPesq.Close;
  QrPesq.Params[00].AsInteger := Geral.IMV(EdCodigo.Text);
  QrPesq.Params[01].AsInteger := Geral.IMV(EdEntidade.Text);
  QrPesq.Open;
end;

end.
