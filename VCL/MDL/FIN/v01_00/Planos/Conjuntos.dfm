object FmConjuntos: TFmConjuntos
  Left = 369
  Top = 181
  Caption = 'FIN-PLCTA-002 :: Cadastro de Conjuntos de Contas'
  ClientHeight = 348
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 300
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object PainelControle: TPanel
      Left = 1
      Top = 251
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'ltimo registro'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
          NumGlyphs = 2
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Pr'#243'ximo registro'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
          NumGlyphs = 2
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Registro anterior'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
          NumGlyphs = 2
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Primeiro registro'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
          NumGlyphs = 2
        end
      end
      object Panel3: TPanel
        Left = 320
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 372
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
          NumGlyphs = 2
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Exclui banco atual'
          Caption = '&Exclui'
          Enabled = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          NumGlyphs = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Caption = '&Altera'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
          NumGlyphs = 2
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Inclui'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtIncluiClick
          NumGlyphs = 2
        end
      end
    end
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 92
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 64
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object LaConjunto: TLabel
        Left = 428
        Top = 8
        Width = 80
        Height = 13
        Caption = 'Palno de contas:'
      end
      object Label5: TLabel
        Left = 388
        Top = 8
        Width = 34
        Height = 13
        Caption = 'Ordem:'
      end
      object DBEdCodigo: TDBEdit
        Left = 16
        Top = 24
        Width = 45
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsConjuntos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 64
        Top = 24
        Width = 321
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsConjuntos
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdConjunto: TDBEdit
        Left = 428
        Top = 24
        Width = 353
        Height = 21
        Color = clWhite
        DataField = 'NOMEPLANO'
        DataSource = DsConjuntos
        TabOrder = 2
      end
      object DBEdOrdem: TDBEdit
        Left = 388
        Top = 24
        Width = 33
        Height = 21
        Color = clWhite
        DataField = 'OrdemLista'
        DataSource = DsConjuntos
        TabOrder = 3
      end
      object DBCheckBox1: TDBCheckBox
        Left = 16
        Top = 56
        Width = 97
        Height = 17
        Caption = 'Controla saldo.'
        DataField = 'CtrlaSdo'
        DataSource = DsConjuntos
        TabOrder = 4
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
    end
    object PageControl1: TPageControl
      Left = 4
      Top = 144
      Width = 777
      Height = 101
      ActivePage = TabSheet2
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = 'Grupos deste conjunto'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGGrupos: TDBGrid
          Left = 0
          Top = 0
          Width = 769
          Height = 73
          Align = alClient
          DataSource = DsGrupos
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              ReadOnly = True
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OrdemLista'
              Title.Caption = 'Ordem lista'
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Clientes internos com informa'#231#227'o de saldo (em constru'#231#227'o)'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object dmkDBGSdo: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 769
          Height = 73
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Info'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEENT'
              Title.Caption = 'Entidade'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsConjunSdo
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = dmkDBGSdoCellClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Info'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEENT'
              Title.Caption = 'Entidade'
              Visible = True
            end>
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 300
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 251
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Default = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
        NumGlyphs = 2
      end
      object Panel1: TPanel
        Left = 672
        Top = 1
        Width = 117
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 12
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
          NumGlyphs = 2
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 820
      Height = 196
      TabOrder = 0
      object Label9: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 64
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label3: TLabel
        Left = 16
        Top = 48
        Width = 30
        Height = 13
        Caption = 'Plano:'
      end
      object Label4: TLabel
        Left = 360
        Top = 48
        Width = 55
        Height = 13
        Caption = 'Ordem lista:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 24
        Width = 45
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNome: TEdit
        Left = 64
        Top = 24
        Width = 353
        Height = 21
        MaxLength = 255
        TabOrder = 1
      end
      object CBPlano: TdmkDBLookupComboBox
        Left = 64
        Top = 64
        Width = 293
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsPlano
        TabOrder = 2
        dmkEditCB = EdPlano
        UpdType = utYes
      end
      object EdOrdemLista: TdmkEdit
        Left = 360
        Top = 64
        Width = 57
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object CkCtrlaSdo: TCheckBox
        Left = 16
        Top = 92
        Width = 97
        Height = 17
        Caption = 'Controla saldo.'
        TabOrder = 4
      end
      object EdPlano: TdmkEditCB
        Left = 16
        Top = 64
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBPlano
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Alignment = taLeftJustify
    Caption = '                             Cadastro de Conjuntos de Contas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object LaTipo: TLabel
      Left = 709
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 708
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 483
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 221
      ExplicitWidth = 481
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 0
        NumGlyphs = 2
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 1
        NumGlyphs = 2
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 2
        OnClick = SbNumeroClick
        NumGlyphs = 2
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 3
        OnClick = SbNomeClick
        NumGlyphs = 2
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 4
        OnClick = SbQueryClick
        NumGlyphs = 2
      end
    end
  end
  object DsConjuntos: TDataSource
    DataSet = QrConjuntos
    Left = 488
    Top = 161
  end
  object QrConjuntos: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrConjuntosBeforeOpen
    AfterOpen = QrConjuntosAfterOpen
    AfterScroll = QrConjuntosAfterScroll
    SQL.Strings = (
      'SELECT cj.*, pl.Nome NOMEPLANO'
      'FROM conjuntos cj'
      'LEFT JOIN plano pl ON pl.Codigo=cj.Plano'
      'WHERE cj.Codigo>0')
    Left = 460
    Top = 161
    object QrConjuntosCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'conjuntos.Codigo'
      Required = True
    end
    object QrConjuntosNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'conjuntos.Nome'
      Required = True
      Size = 50
    end
    object QrConjuntosPlano: TIntegerField
      FieldName = 'Plano'
      Origin = 'conjuntos.Plano'
      Required = True
    end
    object QrConjuntosLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'conjuntos.Lk'
    end
    object QrConjuntosDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'conjuntos.DataCad'
    end
    object QrConjuntosDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'conjuntos.DataAlt'
    end
    object QrConjuntosUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'conjuntos.UserCad'
    end
    object QrConjuntosUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'conjuntos.UserAlt'
    end
    object QrConjuntosNOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Origin = 'plano.Nome'
      Size = 50
    end
    object QrConjuntosOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
      Origin = 'conjuntos.OrdemLista'
    end
    object QrConjuntosCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
      Origin = 'conjuntos.CtrlaSdo'
      Required = True
    end
  end
  object QrPlano: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM plano'
      'ORDER BY Nome'
      '')
    Left = 480
    Top = 68
    object QrPlanoCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.conjuntos.Codigo'
    end
    object QrPlanoNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.conjuntos.Nome'
      Size = 14
    end
  end
  object DsPlano: TDataSource
    DataSet = QrPlano
    Left = 508
    Top = 68
  end
  object TbGrupos: TmySQLTable
    Database = Dmod.MyDB
    Filtered = True
    SortFieldNames = 'OrdemLista,Nome'
    TableName = 'grupos'
    Left = 460
    Top = 189
    object TbGruposCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'grupos.Codigo'
    end
    object TbGruposNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'grupos.Nome'
      Size = 50
    end
    object TbGruposConjunto: TIntegerField
      FieldName = 'Conjunto'
      Origin = 'grupos.Conjunto'
    end
    object TbGruposOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
      Origin = 'grupos.OrdemLista'
    end
    object TbGruposLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'grupos.Lk'
    end
    object TbGruposDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'grupos.DataCad'
    end
    object TbGruposDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'grupos.DataAlt'
    end
    object TbGruposUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'grupos.UserCad'
    end
    object TbGruposUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'grupos.UserAlt'
    end
  end
  object DsGrupos: TDataSource
    DataSet = TbGrupos
    Left = 488
    Top = 188
  end
  object QrConjunSdo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) '
      'NOMEENT, scj.* '
      'FROM conjunsdo scj'
      'LEFT JOIN entidades ent ON ent.Codigo=scj.Entidade'
      'WHERE scj.Codigo=:P0'
      'ORDER BY NOMEENT')
    Left = 556
    Top = 156
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConjunSdoNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrConjunSdoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrConjunSdoEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object QrConjunSdoSdoAtu: TFloatField
      FieldName = 'SdoAtu'
      Required = True
    end
    object QrConjunSdoSdoFut: TFloatField
      FieldName = 'SdoFut'
      Required = True
    end
    object QrConjunSdoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrConjunSdoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrConjunSdoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrConjunSdoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrConjunSdoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrConjunSdoPerAnt: TFloatField
      FieldName = 'PerAnt'
      Required = True
    end
    object QrConjunSdoPerAtu: TFloatField
      FieldName = 'PerAtu'
      Required = True
    end
    object QrConjunSdoInfo: TSmallintField
      FieldName = 'Info'
      Required = True
      MaxValue = 1
    end
  end
  object DsConjunSdo: TDataSource
    DataSet = QrConjunSdo
    Left = 584
    Top = 156
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtExclui
    CanDel01 = BtExclui
    Left = 260
    Top = 12
  end
end
