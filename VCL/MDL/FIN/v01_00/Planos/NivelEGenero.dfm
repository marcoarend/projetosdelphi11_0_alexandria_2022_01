object FmNivelEGenero: TFmNivelEGenero
  Left = 339
  Top = 185
  Caption = 'Nivel e G'#234'nero'
  ClientHeight = 191
  ClientWidth = 539
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 143
    Width = 539
    Height = 48
    Align = alBottom
    TabOrder = 0
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 427
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 539
    Height = 48
    Align = alTop
    Caption = 'Form Base'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 537
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 543
      ExplicitHeight = 44
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 48
    Width = 539
    Height = 95
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Label12: TLabel
      Left = 8
      Top = 52
      Width = 59
      Height = 13
      Caption = 'Item do ???:'
    end
    object RGNivel: TRadioGroup
      Left = 0
      Top = 0
      Width = 539
      Height = 45
      Align = alTop
      Caption = ' N'#237'vel do plano: '
      Columns = 6
      ItemIndex = 0
      Items.Strings = (
        'Nenhum'
        'Conta'
        'Sub-grupo'
        'Grupo'
        'Conjunto'
        'Plano')
      TabOrder = 0
      OnClick = RGNivelClick
    end
    object dmkEditItem: TdmkEditCB
      Left = 8
      Top = 68
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = dmkDBItem
    end
    object dmkDBItem: TdmkDBLookupComboBox
      Left = 64
      Top = 68
      Width = 477
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCodiNivel
      TabOrder = 2
      dmkEditCB = dmkEditItem
      UpdType = utYes
    end
  end
  object QrCodiNivel: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'ORDER BY Nome'
      '')
    Left = 20
    Top = 16
    object QrCodiNivelCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCodiNivelNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCodiNivel: TDataSource
    DataSet = QrCodiNivel
    Left = 48
    Top = 16
  end
end
