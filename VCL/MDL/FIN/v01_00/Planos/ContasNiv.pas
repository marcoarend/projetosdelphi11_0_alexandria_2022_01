unit ContasNiv;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, UnDmkEnums,
  Db, mySQLDbTables, dmkDBGrid, DBCtrls, Menus, ComCtrls, Variants;

type
  TFmContasNiv = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrEmpresas: TmySQLQuery;
    DsEmpresas: TDataSource;
    QrEmpresasCodigo: TIntegerField;
    QrEmpresasCliInt: TIntegerField;
    QrEmpresasNOMECLI: TWideStringField;
    QrContasNiv: TmySQLQuery;
    DsContasNiv: TDataSource;
    dmkDBGrid2: TdmkDBGrid;
    QrContasNivNOMENIVEL: TWideStringField;
    QrContasNivNOMEGENERO: TWideStringField;
    QrContasNivNivel: TIntegerField;
    QrContasNivGenero: TIntegerField;
    Panel3: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    Splitter1: TSplitter;
    BtAcao: TBitBtn;
    PMAcao: TPopupMenu;
    Incluiitemacontrolar1: TMenuItem;
    Excluiitemacontrolar1: TMenuItem;
    Selecionavriositens1: TMenuItem;
    N1: TMenuItem;
    QrNiveis: TmySQLQuery;
    QrNiveisCodigo: TIntegerField;
    QrNiveisNome: TWideStringField;
    QrNiveisCtrlaSdo: TSmallintField;
    QrNiveisNivel: TLargeintField;
    PB1: TProgressBar;
    LaAviso: TLabel;
    QrContasNivEntidade: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrEmpresasAfterScroll(DataSet: TDataSet);
    procedure BtAcaoClick(Sender: TObject);
    procedure Incluiitemacontrolar1Click(Sender: TObject);
    procedure Selecionavriositens1Click(Sender: TObject);
    procedure Excluiitemacontrolar1Click(Sender: TObject);
  private
    { Private declarations }
    FCarregou: Boolean;
  public
    { Public declarations }
    procedure ReopenContasNiv(Nivel, Genero: Integer);
  end;

  var
  FmContasNiv: TFmContasNiv;

implementation

uses UnMyObjects, Module, NivelEGenero, MyDBCheck, UMySQLModule, dmkGeral, ContasNivSel,
UCreate, ModuleGeral;

{$R *.DFM}

procedure TFmContasNiv.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasNiv.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContasNiv.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmContasNiv.FormCreate(Sender: TObject);
begin
  FCarregou := False;
  LaAviso.Caption := '';
  QrEmpresas.Open;
end;

procedure TFmContasNiv.QrEmpresasAfterScroll(DataSet: TDataSet);
begin
  ReopenContasNiv(0,0);
  BtAcao.Enabled := QrEmpresasCodigo.Value <> 0;
end;

procedure TFmContasNiv.ReopenContasNiv(Nivel, Genero: Integer);
begin
  QrContasNiv.Close;
  QrContasNiv.Params[0].AsInteger := QrEmpresasCodigo.Value;
  QrContasNiv.Open;
  //
  QrContasNiv.Locate('Nivel;Genero', VarArrayOf([Nivel, Genero]), []);
end;

procedure TFmContasNiv.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmContasNiv.Incluiitemacontrolar1Click(Sender: TObject);
var
  Entidade, Nivel, Genero: Integer;
begin
  Nivel    := 0;
  Genero   := 0;
  Entidade := QrEmpresasCodigo.Value;
  if DBCheck.CriaFm(TFmNivelEGenero, FmNivelEGenero, afmoNegarComAviso) then
  begin
    FmNivelEGenero.ShowModal;
    Nivel := FmNivelEGenero.FNivel;
    Genero := FmNivelEGenero.FGenero;
    FmNivelEGenero.Destroy;
  end;
  if (Nivel <> 0) and (Genero <> 0) then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'contasniv', False, [],
    ['Entidade', 'Nivel', 'Genero'], [], [Entidade, Nivel, Genero], True) then
      ReopenContasNiv(Nivel, Genero);
  end;
end;

procedure TFmContasNiv.Selecionavriositens1Click(Sender: TObject);
var
  NomeNi: String;
begin
  if not FCarregou then
  begin
    UCriar.RecriaTempTable('sdoniveis', DModG.QrUpdPID1, False);
    //
    Screen.Cursor := crHourGlass;
    LaAviso.Caption := 'AGUARDE... Listando itens...';
    PB1.Position := 0;
    Application.ProcessMessages;
    //
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO sdoniveis SET');
    DModG.QrUpdPID1.SQL.Add('Nivel=:P0, Genero=:P1, NomeNi=:P2, ');
    DModG.QrUpdPID1.SQL.Add('NomeGe=:P3, Ctrla=:P4, Seleci=:P5');
    //
    QrNiveis.Close;
    QrNiveis.Open;
    PB1.Max := QrNiveis.RecordCount;
    //
    while not QrNiveis.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      case QrNiveisNivel.Value of
        1: NomeNi := 'Conta';
        2: NomeNi := 'Sub-grupo';
        3: NomeNi := 'Grupo';
        4: NomeNi := 'Conjunto';
        5: NomeNi := 'Plano';
        else NomeNi := '? ? ?';
      end;
      DModG.QrUpdPID1.Params[00].AsInteger := QrNiveisNivel.Value;
      DModG.QrUpdPID1.Params[01].AsInteger := QrNiveisCodigo.Value;
      DModG.QrUpdPID1.Params[02].AsString  := NomeNi;
      DModG.QrUpdPID1.Params[03].AsString  := QrNiveisNome.Value;
      DModG.QrUpdPID1.Params[05].AsInteger := QrNiveisCtrlaSdo.Value;
      DModG.QrUpdPID1.Params[04].AsInteger := QrNiveisCtrlaSdo.Value;
      DModG.QrUpdPID1.ExecSQL;
      //
      QrNiveis.Next;
    end;
    LaAviso.Caption := '';
    FCarregou := True;
    Screen.Cursor := crDefault;
    //
  end;
  if DBCheck.CriaFm(TFmContasNivSel, FmContasNivSel, afmoNegarComAviso) then
  begin
    FmContasNivSel.ShowModal;
    FmContasNivSel.Destroy;
  end;
end;

procedure TFmContasNiv.Excluiitemacontrolar1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrContasNiv, TDBGrid(dmkDBGrid2),
    'contasniv', ['Entidade', 'Nivel', 'Genero'],
    ['Entidade', 'Nivel', 'Genero'], istPergunta, '');
  ReopenContasNiv(0,0);
end;

end.

