object FmPlaCtas: TFmPlaCtas
  Left = 234
  Top = 170
  Caption = 'FIN-PLCTA-006 :: Plano de Contas'
  ClientHeight = 496
  ClientWidth = 1016
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel
    Left = 0
    Top = 401
    Width = 1016
    Height = 47
    Align = alBottom
    TabOrder = 3
    object Label1: TLabel
      Left = 8
      Top = 28
      Width = 87
      Height = 13
      Caption = '*S: Controla saldo.'
    end
    object Label2: TLabel
      Left = 108
      Top = 28
      Width = 56
      Height = 13
      Caption = '*M: Mensal.'
    end
    object Label3: TLabel
      Left = 180
      Top = 28
      Width = 52
      Height = 13
      Caption = '*D: D'#233'bito.'
    end
    object Label4: TLabel
      Left = 240
      Top = 28
      Width = 53
      Height = 13
      Caption = '*C: Cr'#233'dito.'
    end
    object Label5: TLabel
      Left = 304
      Top = 28
      Width = 110
      Height = 13
      Caption = '*R: Rateio de provis'#227'o.'
    end
    object CkOcultaCadSistema: TCheckBox
      Left = 8
      Top = 8
      Width = 161
      Height = 17
      Caption = 'Ocultar cadastros do sistema.'
      Checked = True
      State = cbChecked
      TabOrder = 0
      OnClick = CkOcultaCadSistemaClick
    end
    object CkDragDrop: TCheckBox
      Left = 172
      Top = 8
      Width = 233
      Height = 17
      Caption = 'Ativar arraste entre n'#237'veis (mudar n'#237'vel pai).'
      TabOrder = 1
      OnClick = CkDragDropClick
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 448
    Width = 1016
    Height = 48
    Align = alBottom
    TabOrder = 0
    Visible = False
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      Enabled = False
      TabOrder = 0
      NumGlyphs = 2
    end
    object BtCancela: TBitBtn
      Tag = 15
      Left = 198
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Cancela'
      Enabled = False
      TabOrder = 1
      NumGlyphs = 2
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 362
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtSaidaClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1016
    Height = 48
    Align = alTop
    Caption = 'Plano de Contas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 52
      Top = 1
      Width = 963
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitWidth = 961
      ExplicitHeight = 44
    end
    object PainelPesquisa: TPanel
      Left = 1
      Top = 1
      Width = 51
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -27
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 0
        OnClick = SbImprimeClick
        NumGlyphs = 2
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1016
    Height = 353
    Align = alClient
    TabOrder = 2
    object Splitter1: TSplitter
      Left = 1
      Top = 157
      Width = 1014
      Height = 3
      Cursor = crVSplit
      Align = alTop
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 1014
      Height = 156
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object DBGPlano: TDBGrid
        Left = 0
        Top = 0
        Width = 239
        Height = 156
        Align = alLeft
        DataSource = DsPlano
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBGPlanoCellClick
        OnColEnter = DBGContaColEnter
        OnColExit = DBGContaColExit
        OnDrawColumnCell = DBGPlanoDrawColumnCell
        OnDragDrop = DBGPlanoDragDrop
        OnDragOver = DBGPlanoDragOver
        OnEnter = DBGPlanoEnter
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Cod.'
            Width = 24
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Title.Caption = 'Descri'#231#227'o do PLANO'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OrdemLista'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'Ordem'
            Width = 21
            Visible = True
          end
          item
            Expanded = False
            FieldName = '_S'
            Title.Caption = 'S*'
            Width = 18
            Visible = True
          end>
      end
      object DBGConju: TDBGrid
        Left = 239
        Top = 0
        Width = 380
        Height = 156
        Align = alLeft
        DataSource = DsConju
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBGConjuCellClick
        OnColEnter = DBGContaColEnter
        OnColExit = DBGContaColExit
        OnDrawColumnCell = DBGConjuDrawColumnCell
        OnDragDrop = DBGConjuDragDrop
        OnDragOver = DBGConjuDragOver
        OnEnter = DBGConjuEnter
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Cod.'
            Width = 24
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Title.Caption = 'Descri'#231#227'o do CONJUNTO'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OrdemLista'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'Ordem'
            Width = 21
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME_PAI'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGray
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Title.Caption = 'Plano'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = '_S'
            Title.Caption = 'S*'
            Width = 18
            Visible = True
          end>
      end
      object DBGGrupo: TDBGrid
        Left = 619
        Top = 0
        Width = 380
        Height = 156
        Align = alLeft
        DataSource = DsGrupo
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBGGrupoCellClick
        OnColEnter = DBGContaColEnter
        OnColExit = DBGContaColExit
        OnDrawColumnCell = DBGGrupoDrawColumnCell
        OnDragDrop = DBGGrupoDragDrop
        OnDragOver = DBGGrupoDragOver
        OnEnter = DBGGrupoEnter
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Cod.'
            Width = 24
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Title.Caption = 'Descri'#231#227'o do GRUPO'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OrdemLista'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'Ordem'
            Width = 21
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME_PAI'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGray
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Title.Caption = 'Conjunto'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = '_S'
            Title.Caption = 'S*'
            Width = 18
            Visible = True
          end>
      end
      object DBGSubgr: TDBGrid
        Left = 999
        Top = 0
        Width = 15
        Height = 156
        Align = alClient
        DataSource = DsSubgr
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
        TabOrder = 3
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBGSubgrCellClick
        OnColEnter = DBGContaColEnter
        OnColExit = DBGContaColExit
        OnDrawColumnCell = DBGSubgrDrawColumnCell
        OnDragDrop = DBGSubgrDragDrop
        OnDragOver = DBGSubgrDragOver
        OnEnter = DBGSubgrEnter
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Cod.'
            Width = 24
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Title.Caption = 'Descri'#231#227'o do SUB-GRUPO'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OrdemLista'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'Ordem'
            Width = 21
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME_PAI'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGray
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Title.Caption = 'Grupo'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = '_S'
            Title.Caption = 'S*'
            Width = 18
            Visible = True
          end>
      end
    end
    object DBGConta: TDBGrid
      Left = 1
      Top = 160
      Width = 746
      Height = 192
      Align = alClient
      DataSource = DsConta
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBGContaCellClick
      OnColEnter = DBGContaColEnter
      OnColExit = DBGContaColExit
      OnDrawColumnCell = DBGContaDrawColumnCell
      OnEnter = DBGContaEnter
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Cod.'
          Width = 24
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Title.Caption = 'Descri'#231#227'o da CONTA'
          Width = 280
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OrdemLista'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Width = 21
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOME_PAI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Title.Caption = 'Sub-grupo'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = '_S'
          ReadOnly = True
          Title.Caption = 'S*'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = '_M'
          ReadOnly = True
          Title.Caption = 'M*'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = '_C'
          ReadOnly = True
          Title.Caption = 'C*'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = '_D'
          ReadOnly = True
          Title.Caption = 'D*'
          Width = 18
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ProvRat'
          Title.Caption = 'R*'
          Width = 18
          Visible = True
        end>
    end
    object PnExtra: TPanel
      Left = 747
      Top = 160
      Width = 268
      Height = 192
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 2
      object DBGExtra: TDBGrid
        Left = 0
        Top = 17
        Width = 268
        Height = 175
        Align = alClient
        DataSource = DsExtra
        Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBGExtraCellClick
        OnColEnter = DBGContaColEnter
        OnColExit = DBGContaColExit
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Cod.'
            Width = 24
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Descri'#231#227'o CONJUNTO'
            Width = 209
            Visible = True
          end>
      end
      object StaticText1: TStaticText
        Left = 0
        Top = 0
        Width = 268
        Height = 17
        Align = alTop
        Alignment = taCenter
        Caption = 'Itens n'#227'o pertencentes '
        TabOrder = 1
      end
    end
  end
  object DsPlano: TDataSource
    DataSet = TbPlano
    Left = 45
    Top = 89
  end
  object DsConju: TDataSource
    DataSet = TbConju
    Left = 313
    Top = 93
  end
  object DsGrupo: TDataSource
    DataSet = TbGrupo
    Left = 541
    Top = 89
  end
  object DsSubgr: TDataSource
    DataSet = TbSubgr
    Left = 821
    Top = 93
  end
  object DsConta: TDataSource
    DataSet = TbConta
    Left = 88
    Top = 256
  end
  object TbPlano: TmySQLTable
    Database = Dmod.MyDB
    Filter = 'Codigo>0'
    Filtered = True
    BeforeInsert = TbPlanoBeforeInsert
    BeforePost = TbPlanoBeforePost
    AfterPost = TbPlanoAfterPost
    AfterCancel = TbPlanoAfterCancel
    BeforeDelete = TbPlanoBeforeDelete
    AfterScroll = TbPlanoAfterScroll
    OnCalcFields = TbPlanoCalcFields
    TableName = 'plano'
    Left = 17
    Top = 89
    object TbPlanoCodigo: TIntegerField
      FieldName = 'Codigo'
      ReadOnly = True
    end
    object TbPlanoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object TbPlanoLk: TIntegerField
      FieldName = 'Lk'
    end
    object TbPlanoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbPlanoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbPlanoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbPlanoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object TbPlanoOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
    end
    object TbPlanoCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
    end
    object TbPlano_S: TSmallintField
      FieldKind = fkCalculated
      FieldName = '_S'
      Calculated = True
    end
  end
  object TbConju: TmySQLTable
    Database = Dmod.MyDB
    Filter = 'Plano=-1000'
    Filtered = True
    BeforeInsert = TbConjuBeforeInsert
    AfterInsert = TbConjuAfterInsert
    AfterEdit = TbConjuAfterEdit
    BeforePost = TbConjuBeforePost
    AfterPost = TbConjuAfterPost
    AfterCancel = TbConjuAfterCancel
    BeforeDelete = TbConjuBeforeDelete
    AfterScroll = TbConjuAfterScroll
    OnCalcFields = TbConjuCalcFields
    TableName = 'conjuntos'
    Left = 285
    Top = 93
    object TbConjuCodigo: TIntegerField
      FieldName = 'Codigo'
      ReadOnly = True
    end
    object TbConjuNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object TbConjuLk: TIntegerField
      FieldName = 'Lk'
    end
    object TbConjuDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbConjuDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbConjuUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbConjuUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object TbConjuPlano: TIntegerField
      FieldName = 'Plano'
    end
    object TbConjuOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
    end
    object TbConjuCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
    end
    object TbConjuNOME_PAI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOME_PAI'
      LookupDataSet = QrTPla
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'Plano'
      Size = 50
      Lookup = True
    end
    object TbConju_S: TSmallintField
      FieldKind = fkCalculated
      FieldName = '_S'
      Calculated = True
    end
  end
  object TbGrupo: TmySQLTable
    Database = Dmod.MyDB
    Filter = 'Conjunto=-1000'
    Filtered = True
    BeforeInsert = TbGrupoBeforeInsert
    BeforePost = TbGrupoBeforePost
    AfterPost = TbGrupoAfterPost
    AfterCancel = TbGrupoAfterCancel
    BeforeDelete = TbGrupoBeforeDelete
    AfterScroll = TbGrupoAfterScroll
    OnCalcFields = TbGrupoCalcFields
    TableName = 'grupos'
    Left = 513
    Top = 89
    object TbGrupoCodigo: TIntegerField
      FieldName = 'Codigo'
      ReadOnly = True
    end
    object TbGrupoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object TbGrupoConjunto: TIntegerField
      FieldName = 'Conjunto'
    end
    object TbGrupoLk: TIntegerField
      FieldName = 'Lk'
    end
    object TbGrupoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbGrupoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbGrupoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbGrupoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object TbGrupoOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
    end
    object TbGrupoCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
    end
    object TbGrupoNOME_PAI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOME_PAI'
      LookupDataSet = QrTCjn
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'Conjunto'
      Size = 50
      Lookup = True
    end
    object TbGrupo_S: TSmallintField
      FieldKind = fkCalculated
      FieldName = '_S'
      Calculated = True
    end
  end
  object TbSubgr: TmySQLTable
    Database = Dmod.MyDB
    Filter = 'Grupo=-1000'
    Filtered = True
    BeforeInsert = TbSubgrBeforeInsert
    BeforePost = TbSubgrBeforePost
    AfterPost = TbSubgrAfterPost
    AfterCancel = TbSubgrAfterCancel
    BeforeDelete = TbSubgrBeforeDelete
    AfterScroll = TbSubgrAfterScroll
    OnCalcFields = TbSubgrCalcFields
    TableName = 'subgrupos'
    Left = 793
    Top = 93
    object TbSubgrCodigo: TIntegerField
      FieldName = 'Codigo'
      ReadOnly = True
    end
    object TbSubgrNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object TbSubgrGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object TbSubgrOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
    end
    object TbSubgrTipoAgrupa: TIntegerField
      FieldName = 'TipoAgrupa'
    end
    object TbSubgrLk: TIntegerField
      FieldName = 'Lk'
    end
    object TbSubgrDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbSubgrDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbSubgrUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbSubgrUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object TbSubgrCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
    end
    object TbSubgrNOME_PAI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOME_PAI'
      LookupDataSet = QrTGru
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'Grupo'
      Size = 50
      Lookup = True
    end
    object TbSubgr_S: TSmallintField
      FieldKind = fkCalculated
      FieldName = '_S'
      Calculated = True
    end
  end
  object TbConta: TmySQLTable
    Database = Dmod.MyDB
    Filter = 'SubGrupo=-1000'
    Filtered = True
    BeforePost = TbContaBeforePost
    BeforeDelete = TbContaBeforeDelete
    OnCalcFields = TbContaCalcFields
    TableName = 'contas'
    Left = 60
    Top = 256
    object TbContaCodigo: TIntegerField
      FieldName = 'Codigo'
      ReadOnly = True
    end
    object TbContaNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object TbContaNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 50
    end
    object TbContaNome3: TWideStringField
      FieldName = 'Nome3'
      Size = 50
    end
    object TbContaID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object TbContaSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
    end
    object TbContaCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
    object TbContaEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object TbContaCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object TbContaDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object TbContaMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object TbContaExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object TbContaMensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object TbContaMensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object TbContaMensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object TbContaMenscred: TFloatField
      FieldName = 'Menscred'
    end
    object TbContaMensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object TbContaTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object TbContaExcel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object TbContaRateio: TIntegerField
      FieldName = 'Rateio'
    end
    object TbContaEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object TbContaAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object TbContaLk: TIntegerField
      FieldName = 'Lk'
    end
    object TbContaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object TbContaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object TbContaUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object TbContaUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object TbContaPendenMesSeg: TSmallintField
      FieldName = 'PendenMesSeg'
    end
    object TbContaCalculMesSeg: TSmallintField
      FieldName = 'CalculMesSeg'
    end
    object TbContaOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
    end
    object TbContaContasAgr: TIntegerField
      FieldName = 'ContasAgr'
    end
    object TbContaContasSum: TIntegerField
      FieldName = 'ContasSum'
    end
    object TbContaCtrlaSdo: TSmallintField
      FieldName = 'CtrlaSdo'
    end
    object TbContaNOME_PAI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOME_PAI'
      LookupDataSet = QrTSgr
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'Subgrupo'
      Size = 50
      Lookup = True
    end
    object TbContaAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object TbConta_S: TSmallintField
      FieldKind = fkCalculated
      FieldName = '_S'
      Calculated = True
    end
    object TbConta_M: TSmallintField
      FieldKind = fkCalculated
      FieldName = '_M'
      Calculated = True
    end
    object TbConta_C: TIntegerField
      FieldKind = fkCalculated
      FieldName = '_C'
      Calculated = True
    end
    object TbConta_D: TIntegerField
      FieldKind = fkCalculated
      FieldName = '_D'
      Calculated = True
    end
    object TbContaProvRat: TSmallintField
      FieldName = 'ProvRat'
      MaxValue = 1
    end
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 204
    Top = 252
  end
  object QrNCjt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tb1.Codigo, tb1.Nome, tb2.Nome NOME_PAI'
      'FROM conjuntos tb1'
      'LEFT JOIN plano tb2 ON tb2.Codigo=tb1.Plano'
      'WHERE tb1.Codigo>0'
      'AND tb1.Plano NOT IN (0, :P0)'
      'ORDER BY Nome')
    Left = 808
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNCjtCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNCjtNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrNCjtNOME_PAI: TWideStringField
      FieldName = 'NOME_PAI'
      Size = 50
    end
  end
  object DsExtra: TDataSource
    Left = 836
    Top = 292
  end
  object QrNGru: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tb1.Codigo, tb1.Nome, tb2.Nome NOME_PAI'
      'FROM grupos tb1'
      'LEFT JOIN conjuntos tb2 ON tb2.Codigo=tb1.Conjunto'
      'WHERE tb1.Codigo>0'
      'AND tb1.Conjunto NOT IN (0, :P0)'
      'ORDER BY Nome')
    Left = 808
    Top = 320
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrNGruNOME_PAI: TWideStringField
      FieldName = 'NOME_PAI'
      Size = 50
    end
  end
  object QrNSgr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tb1.Codigo, tb1.Nome, tb2.Nome NOME_PAI'
      'FROM subgrupos tb1'
      'LEFT JOIN grupos tb2 ON tb2.Codigo=tb1.Grupo'
      'WHERE tb1.Codigo>0'
      'AND tb1.Grupo NOT IN (0, :P0)'
      'ORDER BY Nome')
    Left = 808
    Top = 348
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrNSgrNOME_PAI: TWideStringField
      FieldName = 'NOME_PAI'
      Size = 50
    end
  end
  object QrNCta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tb1.Codigo, tb1.Nome, tb2.Nome NOME_PAI'
      'FROM contas tb1'
      'LEFT JOIN subgrupos tb2 ON tb2.Codigo=tb1.Subgrupo'
      'WHERE tb1.Codigo>0'
      'AND tb1.Subgrupo NOT IN (0, :P0)'
      'ORDER BY Nome')
    Left = 808
    Top = 376
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrNCtaNOME_PAI: TWideStringField
      FieldName = 'NOME_PAI'
      Size = 50
    end
  end
  object mySQLUpdateSQL1: TmySQLUpdateSQL
    Left = 412
    Top = 260
  end
  object QrTPla: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM plano'
      'ORDER BY Nome'
      '')
    Left = 68
    Top = 12
    object QrTPlaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTPlaNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrTCjn: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM conjuntos'
      'ORDER BY Nome'
      '')
    Left = 96
    Top = 12
    object QrTCjnCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTCjnNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrTGru: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM grupos'
      'ORDER BY Nome'
      '')
    Left = 124
    Top = 12
    object QrTGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrTSgr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM subgrupos'
      'ORDER BY Nome'
      '')
    Left = 152
    Top = 12
    object QrTSgrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTSgrNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 260
    Top = 12
  end
end
