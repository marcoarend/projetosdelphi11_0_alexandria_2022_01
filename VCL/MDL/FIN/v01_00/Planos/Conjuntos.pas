unit Conjuntos;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, ComCtrls, dmkDBGridDAC, dmkDBGrid, Variants, dmkPermissoes, dmkGeral,
  dmkEdit, dmkEditCB, dmkDBLookupComboBox, UnDmkProcFunc;

type
  TFmConjuntos = class(TForm)
    DsConjuntos: TDataSource;
    QrConjuntos: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    QrPlano: TmySQLQuery;
    QrPlanoCodigo: TIntegerField;
    QrPlanoNome: TWideStringField;
    DsPlano: TDataSource;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelEdit: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TEdit;
    CBPlano: TdmkDBLookupComboBox;
    EdOrdemLista: TdmkEdit;
    PainelDados: TPanel;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelData: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    LaConjunto: TLabel;
    Label5: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdConjunto: TDBEdit;
    DBEdOrdem: TDBEdit;
    TbGrupos: TmySQLTable;
    DsGrupos: TDataSource;
    QrConjuntosCodigo: TIntegerField;
    QrConjuntosNome: TWideStringField;
    QrConjuntosPlano: TIntegerField;
    QrConjuntosLk: TIntegerField;
    QrConjuntosDataCad: TDateField;
    QrConjuntosDataAlt: TDateField;
    QrConjuntosUserCad: TIntegerField;
    QrConjuntosUserAlt: TIntegerField;
    QrConjuntosNOMEPLANO: TWideStringField;
    QrConjuntosOrdemLista: TIntegerField;
    TbGruposCodigo: TIntegerField;
    TbGruposNome: TWideStringField;
    TbGruposConjunto: TIntegerField;
    TbGruposOrdemLista: TIntegerField;
    TbGruposLk: TIntegerField;
    TbGruposDataCad: TDateField;
    TbGruposDataAlt: TDateField;
    TbGruposUserCad: TIntegerField;
    TbGruposUserAlt: TIntegerField;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    CkCtrlaSdo: TCheckBox;
    DBCheckBox1: TDBCheckBox;
    QrConjuntosCtrlaSdo: TSmallintField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGGrupos: TDBGrid;
    QrConjunSdo: TmySQLQuery;
    DsConjunSdo: TDataSource;
    QrConjunSdoNOMEENT: TWideStringField;
    QrConjunSdoCodigo: TIntegerField;
    QrConjunSdoEntidade: TIntegerField;
    QrConjunSdoSdoAtu: TFloatField;
    QrConjunSdoSdoFut: TFloatField;
    QrConjunSdoLk: TIntegerField;
    QrConjunSdoDataCad: TDateField;
    QrConjunSdoDataAlt: TDateField;
    QrConjunSdoUserCad: TIntegerField;
    QrConjunSdoUserAlt: TIntegerField;
    QrConjunSdoPerAnt: TFloatField;
    QrConjunSdoPerAtu: TFloatField;
    QrConjunSdoInfo: TSmallintField;
    dmkDBGSdo: TdmkDBGrid;
    dmkPermissoes1: TdmkPermissoes;
    EdPlano: TdmkEditCB;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrConjuntosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrConjuntosAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrConjuntosBeforeOpen(DataSet: TDataSet);
    procedure TbGruposDeleting(Sender: TObject; var Allow: Boolean);
    procedure dmkDBGSdoCellClick(Column: TColumn);
  private
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
//    procedure IncluiSubRegistro;
//    procedure ExcluiSubRegistro;
//    procedure AlteraSubRegistro;
//    procedure TravaOForm;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure ReopenConjunSdo(CliInt: Integer);

  public
    { Public declarations }
  end;

var
  FmConjuntos: TFmConjuntos;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmConjuntos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmConjuntos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrConjuntosCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmConjuntos.DefParams;
begin
  VAR_GOTOTABELA := 'Conjuntos';
  VAR_GOTOMYSQLTABLE := QrConjuntos;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT cj.*, pl.Nome NOMEPLANO');
  VAR_SQLx.Add('FROM conjuntos cj');
  VAR_SQLx.Add('LEFT JOIN plano pl ON pl.Codigo=cj.Plano');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('WHERE cj.Codigo=:P0');
  //
  VAR_SQLa.Add('WHERE cj.Nome Like :P0');
  //
end;

procedure TFmConjuntos.MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    PainelControle.Visible:=False;
    if Status = CO_INCLUSAO then
    begin
    EdNome.Text         := CO_VAZIO;
      EdCodigo.Text     := FormatFloat(FFormatFloat, Codigo);
      EdPlano.Text      := '';
      CBPlano.KeyValue  := Null;
      EdOrdemLista.Text := '';
      CkCtrlaSdo.Checked := True;
    end else begin
      EdCodigo.Text     := DBEdCodigo.Text;
      EdNome.Text       := DBEdNome.Text;
      EdPlano.Text      := IntToStr(QrConjuntosPlano.Value);
      CBPlano.KeyValue  := QrConjuntosPlano.Value;
      EdOrdemLista.Text := IntToStr(QrConjuntosOrdemLista.Value);
      CkCtrlaSdo.Checked := Geral.IntToBool_0(QrConjuntosCtrlaSdo.Value);
    end;
    EdNome.SetFocus;
  end else begin
    PainelControle.Visible:=True;
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmConjuntos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmConjuntos.AlteraRegistro;
var
  Conjuntos : Integer;
begin
  Conjuntos := QrConjuntosCodigo.Value;
  if QrConjuntosCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(Conjuntos, Dmod.MyDB, 'Conjuntos', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Conjuntos, Dmod.MyDB, 'Conjuntos', 'Codigo');
      MostraEdicao(True, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmConjuntos.IncluiRegistro;
var
  Cursor : TCursor;
  Conjuntos : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Conjuntos := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Conjuntos', 'Conjunto', 'Codigo');
    if Length(FormatFloat(FFormatFloat, Conjuntos))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(True, CO_INCLUSAO, Conjuntos);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmConjuntos.QueryPrincipalAfterOpen;
begin
end;

procedure TFmConjuntos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmConjuntos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmConjuntos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmConjuntos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmConjuntos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmConjuntos.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmConjuntos.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmConjuntos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrConjuntosCodigo.Value;
  Close;
end;

procedure TFmConjuntos.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := Geral.IMV(EdCodigo.Text);
  Dmod.QrUpdU.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('INSERT INTO conjuntos SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE conjuntos SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0, Plano=:P1, OrdemLista=:P2, CtrlaSdo=:P3, ');
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := Geral.IMV(EdPlano.Text);
  Dmod.QrUpdU.Params[02].AsInteger := Geral.IMV(EdOrdemLista.Text);
  Dmod.QrUpdU.Params[03].AsInteger := MLAGeral.BoolToInt(CkCtrlaSdo.Checked);
  //
  Dmod.QrUpdU.Params[04].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[05].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[06].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Conjuntos', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmConjuntos.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Conjuntos', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Conjuntos', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Conjuntos', 'Codigo');
end;

procedure TFmConjuntos.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  PainelEdita.Align  := alClient;
  PainelDados.Align  := alClient;
  PainelEdit.Align   := alClient;
  PageControl1.Align := alClient;
  CriaOForm;
  TbGrupos.Open;
  QrPlano.Open;
end;

procedure TFmConjuntos.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrConjuntosCodigo.Value,LaRegistro.Caption);
end;

procedure TFmConjuntos.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmConjuntos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmConjuntos.QrConjuntosAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmConjuntos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Conjuntos', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmConjuntos.QrConjuntosAfterScroll(DataSet: TDataSet);
begin
  TbGrupos.Filter := 'Conjunto='+IntToStr(QrConjuntosCodigo.Value);
  ReopenConjunSdo(0);
end;

procedure TFmConjuntos.SbQueryClick(Sender: TObject);
begin
  LocCod(QrConjuntosCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Conjuntos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmConjuntos.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmConjuntos.QrConjuntosBeforeOpen(DataSet: TDataSet);
begin
  QrConjuntosCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmConjuntos.TbGruposDeleting(Sender: TObject;
  var Allow: Boolean);
begin
  Allow := False;
  Application.MessageBox('Para excluir grupo de conta acesse o cadastro de grupos!',
  'Informa��o', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmConjuntos.ReopenConjunSdo(CliInt: Integer);
begin
  QrConjunSdo.Close;
  QrConjunSdo.Params[0].AsInteger := QrConjuntosCodigo.Value;
  QrConjunSdo.Open;
  //
  QrConjunSdo.Locate('Entidade', CliInt, []);
end;

procedure TFmConjuntos.dmkDBGSdoCellClick(Column: TColumn);
var
  Status, Entidade: Integer;
begin
  if Column.FieldName = 'Info' then
  begin
    Status := QrConjunSdoInfo.Value;
    if Status = 0 then Status := 1 else Status := 0;
    Entidade := QrConjunSdoEntidade.Value;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE conjunsdo SET Info=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Entidade=:P1 AND Codigo=:P2');
    Dmod.QrUpd.Params[00].AsInteger := Status;
    Dmod.QrUpd.Params[01].AsInteger := Entidade;
    Dmod.QrUpd.Params[02].AsInteger := QrConjuntosCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenConjunSdo(Entidade);
  end;
end;

end.

