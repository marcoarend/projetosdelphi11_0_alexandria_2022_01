object FmMyPagtos2: TFmMyPagtos2
  Left = 334
  Top = 173
  Caption = 'FIN-MYPAG-002 :: Pagamentos e Recebimentos'
  ClientHeight = 440
  ClientWidth = 888
  Color = clBtnFace
  Constraints.MinWidth = 800
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 40
    Width = 888
    Height = 400
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PainelDados: TPanel
      Left = 0
      Top = 0
      Width = 465
      Height = 400
      Align = alLeft
      TabOrder = 0
      object LaParcela: TLabel
        Left = 8
        Top = 4
        Width = 39
        Height = 13
        Caption = 'Parcela:'
      end
      object Label1: TLabel
        Left = 52
        Top = 4
        Width = 39
        Height = 13
        Caption = 'Carteira:'
      end
      object LaValor: TLabel
        Left = 8
        Top = 84
        Width = 34
        Height = 13
        Caption = 'D'#233'bito:'
      end
      object LaDocumento: TLabel
        Left = 128
        Top = 84
        Width = 40
        Height = 13
        Caption = 'Cheque:'
      end
      object LaVencimento: TLabel
        Left = 320
        Top = 84
        Width = 59
        Height = 13
        Caption = 'Vencimento:'
      end
      object LaCredor: TLabel
        Left = 108
        Top = 124
        Width = 34
        Height = 13
        Caption = 'Credor:'
        Enabled = False
      end
      object LaDevedor: TLabel
        Left = 108
        Top = 164
        Width = 44
        Height = 13
        Caption = 'Devedor:'
        Enabled = False
      end
      object LaMoraDia: TLabel
        Left = 108
        Top = 204
        Width = 163
        Height = 13
        Caption = 'Valor mora/dia ( F4 para  %/30dd):'
      end
      object LaMulta: TLabel
        Left = 304
        Top = 204
        Width = 112
        Height = 13
        Caption = 'Valor Multa (F4 para %):'
      end
      object Label10: TLabel
        Left = 8
        Top = 228
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object Label2: TLabel
        Left = 188
        Top = 84
        Width = 23
        Height = 13
        Caption = 'N.F.:'
      end
      object Label20: TLabel
        Left = 248
        Top = 84
        Width = 48
        Height = 13
        Caption = 'Duplicata:'
      end
      object Label21: TLabel
        Left = 8
        Top = 44
        Width = 31
        Height = 13
        Caption = 'Conta:'
      end
      object SpeedButton1: TSpeedButton
        Left = 436
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton1Click
      end
      object SpeedButton2: TSpeedButton
        Left = 436
        Top = 100
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton2Click
      end
      object SpeedButton3: TSpeedButton
        Left = 436
        Top = 60
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton3Click
      end
      object Label22: TLabel
        Left = 76
        Top = 84
        Width = 27
        Height = 13
        Caption = 'S'#233'rie:'
      end
      object Label23: TLabel
        Left = 8
        Top = 268
        Width = 101
        Height = 13
        Caption = 'Descri'#231#227'o (Hist'#243'rico):'
      end
      object EdValor: TdmkEdit
        Left = 8
        Top = 100
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdValorChange
        OnExit = EdValorExit
      end
      object EdDocumento: TdmkEdit
        Left = 128
        Top = 100
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdDocumentoChange
        OnExit = EdDocumentoExit
      end
      object TPVencimento: TdmkEditDateTimePicker
        Left = 320
        Top = 100
        Width = 112
        Height = 21
        Date = 37478.000000000000000000
        Time = 0.679422685199824600
        Color = clWhite
        TabOrder = 10
        OnExit = TPVencimentoExit
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdCredor: TdmkEditCB
        Left = 108
        Top = 140
        Width = 49
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCredorChange
        DBLookupComboBox = CBCredor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCredor: TdmkDBLookupComboBox
        Left = 160
        Top = 140
        Width = 297
        Height = 21
        Color = clWhite
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsCredor
        TabOrder = 13
        dmkEditCB = EdCredor
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdDevedor: TdmkEditCB
        Left = 108
        Top = 180
        Width = 49
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 14
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdDevedorChange
        DBLookupComboBox = CBDevedor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBDevedor: TdmkDBLookupComboBox
        Left = 160
        Top = 180
        Width = 297
        Height = 21
        Color = clWhite
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsDevedor
        TabOrder = 15
        dmkEditCB = EdDevedor
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdMoraDia: TdmkEdit
        Left = 108
        Top = 220
        Width = 169
        Height = 21
        Alignment = taRightJustify
        TabOrder = 16
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnExit = EdMoraDiaExit
        OnKeyDown = EdMoraDiaKeyDown
      end
      object EdMulta: TdmkEdit
        Left = 304
        Top = 220
        Width = 153
        Height = 21
        Alignment = taRightJustify
        TabOrder = 17
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnExit = EdMultaExit
        OnKeyDown = EdMultaKeyDown
      end
      object Panel3: TPanel
        Left = 8
        Top = 124
        Width = 97
        Height = 97
        BevelOuter = bvLowered
        TabOrder = 11
        object Label12: TLabel
          Left = 4
          Top = 50
          Width = 54
          Height = 13
          Caption = 'Valor troco:'
        end
        object Label13: TLabel
          Left = 4
          Top = 2
          Width = 55
          Height = 13
          Caption = '&Troco para:'
        end
        object EdTrocoVal: TdmkEdit
          Left = 4
          Top = 68
          Width = 77
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdTrocoPara: TdmkEdit
          Left = 4
          Top = 20
          Width = 77
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdTrocoParaChange
        end
      end
      object EdCliInt: TdmkEditCB
        Left = 8
        Top = 244
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 18
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliInt
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCliInt: TdmkDBLookupComboBox
        Left = 72
        Top = 244
        Width = 385
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsCliInt
        TabOrder = 19
        dmkEditCB = EdCliInt
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdNotaFiscal: TdmkEdit
        Left = 188
        Top = 100
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnExit = EdNotaFiscalExit
      end
      object EdDuplicata: TdmkEdit
        Left = 248
        Top = 100
        Width = 68
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnExit = EdNotaFiscalExit
      end
      object EdGenero: TdmkEditCB
        Left = 8
        Top = 60
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGenero: TdmkDBLookupComboBox
        Left = 64
        Top = 60
        Width = 369
        Height = 21
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsGeneros
        TabOrder = 4
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdSerieCH: TdmkEdit
        Left = 76
        Top = 100
        Width = 48
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdDocumentoChange
        OnExit = EdDocumentoExit
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 8
        Top = 308
        Width = 449
        Height = 37
        Caption = ' Tipo de carteira: '
        Columns = 3
        DataField = 'Tipo'
        DataSource = DsCarteiras
        Items.Strings = (
          'Caixa (Esp'#233'cie)'
          'Banco (Conta corr.)'
          'Emiss'#227'o (Ch., dupl., fatura)')
        TabOrder = 20
        Values.Strings = (
          '0'
          '1'
          '2')
      end
      object EdCarteira: TdmkEditCB
        Left = 52
        Top = 20
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCarteira
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCarteira: TdmkDBLookupComboBox
        Left = 96
        Top = 20
        Width = 337
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCarteiras
        TabOrder = 2
        dmkEditCB = EdCarteira
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 20
        Width = 40
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '1'
        ValWarn = False
        OnChange = EdCodigoChange
      end
      object EdDescricao: TdmkEdit
        Left = 8
        Top = 284
        Width = 449
        Height = 21
        TabOrder = 21
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object PainelControle: TPanel
        Left = 1
        Top = 351
        Width = 463
        Height = 48
        Align = alBottom
        TabOrder = 22
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 116
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
        end
        object CkContinuar: TCheckBox
          Left = 8
          Top = 19
          Width = 93
          Height = 17
          Caption = 'Cont. inserindo.'
          TabOrder = 0
          Visible = False
        end
        object Panel2: TPanel
          Left = 357
          Top = 1
          Width = 105
          Height = 46
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 2
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
      end
    end
    object PainelDados2: TPanel
      Left = 465
      Top = 0
      Width = 423
      Height = 400
      Align = alClient
      TabOrder = 1
      object PageControl1: TPageControl
        Left = 1
        Top = 1
        Width = 421
        Height = 398
        ActivePage = TabSheet2
        Align = alClient
        TabOrder = 0
        object TabSheet2: TTabSheet
          Caption = 'Parcelamento'
          ImageIndex = 1
          object DBGParcelas: TDBGrid
            Left = 0
            Top = 113
            Width = 413
            Height = 257
            TabStop = False
            Align = alClient
            DataSource = DsParcPagtos
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnKeyDown = DBGParcelasKeyDown
            Columns = <
              item
                Expanded = False
                FieldName = 'Parcela'
                Width = 44
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Data'
                Width = 65
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Debito'
                Title.Caption = 'D'#233'bito'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Credito'
                Title.Caption = 'Cr'#233'dito'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'S'#233'rie'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Doc'
                Title.Caption = 'Docum.'
                Width = 49
                Visible = True
              end>
          end
          object Panel4: TPanel
            Left = 0
            Top = 0
            Width = 413
            Height = 113
            Align = alTop
            ParentBackground = False
            TabOrder = 0
            object GBParcelamento: TGroupBox
              Left = 1
              Top = 1
              Width = 411
              Height = 111
              Align = alClient
              Caption = ' Parcelamento autom'#225'tico: '
              TabOrder = 1
              Visible = False
              object Label7: TLabel
                Left = 8
                Top = 20
                Width = 44
                Height = 13
                Caption = 'Parcelas:'
              end
              object Label8: TLabel
                Left = 8
                Top = 68
                Width = 78
                Height = 13
                Caption = 'Primeira parcela:'
              end
              object Label9: TLabel
                Left = 100
                Top = 68
                Width = 70
                Height = 13
                Caption = #218'ltima parcela:'
              end
              object Label11: TLabel
                Left = 192
                Top = 68
                Width = 97
                Height = 13
                Caption = 'Total parcelamento: '
              end
              object RGArredondar: TRadioGroup
                Left = 308
                Top = 16
                Width = 97
                Height = 89
                Caption = ' Increm. do doc.: '
                ItemIndex = 1
                Items.Strings = (
                  '1'#170' parcela'
                  #218'ltima parcela')
                TabOrder = 7
                OnClick = RGArredondarClick
              end
              object EdParcelas: TdmkEdit
                Left = 8
                Top = 36
                Width = 45
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '2'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 2
                ValWarn = False
                OnExit = EdParcelasExit
              end
              object RGPeriodo: TRadioGroup
                Left = 60
                Top = 16
                Width = 141
                Height = 45
                Caption = ' Periodo: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'Mensal'
                  '         dias')
                TabOrder = 1
                OnClick = RGPeriodoClick
              end
              object EdDias: TdmkEdit
                Left = 149
                Top = 32
                Width = 24
                Height = 21
                Alignment = taRightJustify
                Color = clBtnFace
                Enabled = False
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '7'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 7
                ValWarn = False
                OnExit = EdDiasExit
              end
              object RGIncremento: TRadioGroup
                Left = 204
                Top = 16
                Width = 101
                Height = 45
                Caption = ' Increm. cheque.: '
                Columns = 2
                Enabled = False
                ItemIndex = 1
                Items.Strings = (
                  'N'#227'o'
                  'Sim')
                TabOrder = 3
                OnClick = RGIncrementoClick
              end
              object EdParcela1: TdmkEdit
                Left = 8
                Top = 84
                Width = 90
                Height = 21
                Alignment = taRightJustify
                Color = clBtnFace
                ReadOnly = True
                TabOrder = 4
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdParcelaX: TdmkEdit
                Left = 100
                Top = 84
                Width = 90
                Height = 21
                Alignment = taRightJustify
                Color = clBtnFace
                ReadOnly = True
                TabOrder = 5
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object CkArredondar: TCheckBox
                Left = 320
                Top = 14
                Width = 77
                Height = 17
                Caption = 'Arredondar: '
                TabOrder = 6
                OnClick = CkArredondarClick
              end
              object EdSoma: TdmkEdit
                Left = 192
                Top = 84
                Width = 105
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Color = clBtnFace
                ReadOnly = True
                TabOrder = 8
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnExit = EdValorExit
              end
            end
            object CkParcelamento: TCheckBox
              Left = 12
              Top = 1
              Width = 153
              Height = 17
              Caption = ' Parcelamento autom'#225'tico: '
              TabOrder = 0
              Visible = False
              OnClick = CkParcelamentoClick
            end
          end
        end
        object TabSheet1: TTabSheet
          Caption = 'Outros'
          ImageIndex = 1
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 413
            Height = 370
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label14: TLabel
              Left = 10
              Top = 4
              Width = 26
              Height = 13
              Caption = 'Data:'
            end
            object TPData: TdmkEditDateTimePicker
              Left = 10
              Top = 20
              Width = 116
              Height = 21
              Date = 37478.000000000000000000
              Time = 0.679422685199824600
              Color = clWhite
              TabOrder = 0
              OnExit = TPVencimentoExit
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object GBCheque: TGroupBox
              Left = 1
              Top = 44
              Width = 396
              Height = 105
              Caption = ' Dados do cheque: '
              TabOrder = 1
              Visible = False
              object Label15: TLabel
                Left = 12
                Top = 18
                Width = 167
                Height = 13
                Caption = 'Emitente (F4 -> Copia do devedor): '
              end
              object Label16: TLabel
                Left = 132
                Top = 58
                Width = 34
                Height = 13
                Caption = 'Banco:'
              end
              object Label17: TLabel
                Left = 184
                Top = 58
                Width = 42
                Height = 13
                Caption = 'Ag'#234'ncia:'
              end
              object Label18: TLabel
                Left = 272
                Top = 58
                Width = 31
                Height = 13
                Caption = 'Conta:'
              end
              object Label19: TLabel
                Left = 12
                Top = 58
                Width = 61
                Height = 13
                Caption = 'CNPJ / CPF:'
              end
              object EdNome: TdmkEdit
                Left = 12
                Top = 34
                Width = 376
                Height = 21
                MaxLength = 30
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnKeyDown = EdNomeKeyDown
              end
              object EdBanco: TdmkEdit
                Left = 132
                Top = 74
                Width = 49
                Height = 21
                Alignment = taCenter
                MaxLength = 3
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnExit = EdBancoExit
              end
              object EdAgencia: TdmkEdit
                Left = 184
                Top = 74
                Width = 84
                Height = 21
                Alignment = taCenter
                MaxLength = 11
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdConta: TdmkEdit
                Left = 272
                Top = 74
                Width = 116
                Height = 21
                Alignment = taCenter
                MaxLength = 15
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdCNPJCPF: TdmkEdit
                Left = 12
                Top = 74
                Width = 115
                Height = 21
                Alignment = taCenter
                MaxLength = 15
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnExit = EdCNPJCPFExit
              end
            end
          end
        end
        object TabSheet3: TTabSheet
          Caption = ' Recibo '
          ImageIndex = 2
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 413
            Height = 370
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object GBRecibo: TGroupBox
              Left = 4
              Top = 5
              Width = 237
              Height = 240
              Caption = ' Recibos: '
              TabOrder = 0
              Visible = False
              object Label3: TLabel
                Left = 4
                Top = 56
                Width = 49
                Height = 13
                Caption = '$ Pessoal:'
              end
              object Label4: TLabel
                Left = 80
                Top = 56
                Width = 53
                Height = 13
                Caption = '$ Empresa:'
              end
              object Label5: TLabel
                Left = 156
                Top = 56
                Width = 55
                Height = 13
                Caption = '% Empresa:'
              end
              object Label6: TLabel
                Left = 4
                Top = 16
                Width = 15
                Height = 13
                Caption = 'N'#186':'
              end
              object EdPessoal: TdmkEdit
                Left = 4
                Top = 72
                Width = 72
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnEnter = EdPessoalEnter
                OnExit = EdPessoalExit
              end
              object EdEmpresa: TdmkEdit
                Left = 80
                Top = 72
                Width = 72
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnEnter = EdEmpresaEnter
                OnExit = EdEmpresaExit
              end
              object EdPorcento: TdmkEdit
                Left = 156
                Top = 72
                Width = 72
                Height = 21
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 4
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,0000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdPorcentoChange
                OnEnter = EdPorcentoEnter
                OnExit = EdPorcentoExit
              end
              object EdRecibo: TdmkEdit
                Left = 4
                Top = 32
                Width = 225
                Height = 21
                CharCase = ecUpperCase
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'XXX-000000.0-U'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'XXX-000000.0-U'
                ValWarn = False
                OnEnter = EdPessoalEnter
                OnExit = EdPessoalExit
              end
              object EdBaseRecibo: TdmkEdit
                Left = 40
                Top = 12
                Width = 121
                Height = 21
                CharCase = ecUpperCase
                TabOrder = 6
                Visible = False
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'XXX-000000.0-U'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'XXX-000000.0-U'
                ValWarn = False
              end
              object RGRecibo: TRadioGroup
                Left = 4
                Top = 96
                Width = 225
                Height = 109
                Caption = ' Op'#231#227'o: '
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o necessita de recibo'
                  'Emitir um recibo no total'
                  'Emitir um recibo para cada parcela'
                  'Emitir recibos mensais. dia')
                TabOrder = 4
                OnClick = RGReciboClick
              end
              object EdDiaRecibo: TdmkEdit
                Left = 157
                Top = 176
                Width = 36
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                MaxLength = 2
                TabOrder = 5
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnExit = EdDiaReciboExit
              end
            end
            object CkRecibo: TCheckBox
              Left = 16
              Top = 2
              Width = 81
              Height = 17
              Caption = 'Emitir recibo:'
              TabOrder = 1
              OnClick = CkReciboClick
            end
          end
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 888
    Height = 40
    Align = alTop
    Caption = 'Pagamentos e Recebimentos '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 809
      Top = 1
      Width = 78
      Height = 38
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 712
      ExplicitTop = 2
      ExplicitHeight = 36
    end
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 808
      Height = 38
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 710
      ExplicitHeight = 36
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 376
    Top = 49
  end
  object DsCredor: TDataSource
    DataSet = QrCredor
    Left = 412
    Top = 164
  end
  object DsDevedor: TDataSource
    DataSet = QrDevedor
    Left = 404
    Top = 204
  end
  object TbParcpagtos: TMySQLTable
    Database = Dmod.ZZDB
    BeforeOpen = TbParcpagtosBeforeOpen
    AfterPost = TbParcpagtosAfterPost
    TableName = 'parcpagtos'
    Left = 489
    Top = 269
    object TbParcpagtosParcela: TIntegerField
      FieldName = 'Parcela'
    end
    object TbParcpagtosData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object TbParcpagtosCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object TbParcpagtosDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object TbParcpagtosDoc: TLargeintField
      FieldName = 'Doc'
    end
    object TbParcpagtosSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
  end
  object DsParcPagtos: TDataSource
    DataSet = TbParcpagtos
    Left = 517
    Top = 269
  end
  object QrCarteiras: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCarteirasAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Nome, Tipo, ForneceI'
      'FROM carteiras'
      'ORDER BY Nome')
    Left = 348
    Top = 49
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
  end
  object QrTerceiro: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.*, '
      'ufp.Nome NOMEUFP,'
      'ufe.Nome NOMEUFE'
      'FROM entidades en, UFs ufe, UFs ufp'
      'WHERE en.Codigo=:P0'
      'AND ufe.Codigo=en.EUF'
      'AND ufp.Codigo=en.PUF'
      '')
    Left = 336
    Top = 13
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerceiroCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTerceiroRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Required = True
      Size = 100
    end
    object QrTerceiroFantasia: TWideStringField
      FieldName = 'Fantasia'
      Required = True
      Size = 60
    end
    object QrTerceiroRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrTerceiroRespons2: TWideStringField
      FieldName = 'Respons2'
      Required = True
      Size = 60
    end
    object QrTerceiroPai: TWideStringField
      FieldName = 'Pai'
      Required = True
      Size = 60
    end
    object QrTerceiroMae: TWideStringField
      FieldName = 'Mae'
      Required = True
      Size = 60
    end
    object QrTerceiroCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrTerceiroIE: TWideStringField
      FieldName = 'IE'
    end
    object QrTerceiroNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrTerceiroApelido: TWideStringField
      FieldName = 'Apelido'
      Required = True
      Size = 60
    end
    object QrTerceiroCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrTerceiroRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrTerceiroERua: TWideStringField
      FieldName = 'ERua'
      Size = 30
    end
    object QrTerceiroECompl: TWideStringField
      FieldName = 'ECompl'
      Size = 30
    end
    object QrTerceiroEBairro: TWideStringField
      FieldName = 'EBairro'
      Size = 30
    end
    object QrTerceiroECIDADE: TWideStringField
      FieldName = 'ECIDADE'
      Size = 25
    end
    object QrTerceiroEUF: TSmallintField
      FieldName = 'EUF'
      Required = True
    end
    object QrTerceiroECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrTerceiroEPais: TWideStringField
      FieldName = 'EPais'
    end
    object QrTerceiroETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrTerceiroETe2: TWideStringField
      FieldName = 'ETe2'
    end
    object QrTerceiroETe3: TWideStringField
      FieldName = 'ETe3'
    end
    object QrTerceiroECel: TWideStringField
      FieldName = 'ECel'
    end
    object QrTerceiroEFax: TWideStringField
      FieldName = 'EFax'
    end
    object QrTerceiroEEMail: TWideStringField
      FieldName = 'EEMail'
      Size = 100
    end
    object QrTerceiroEContato: TWideStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrTerceiroENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrTerceiroPRua: TWideStringField
      FieldName = 'PRua'
      Size = 30
    end
    object QrTerceiroPCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 30
    end
    object QrTerceiroPBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 30
    end
    object QrTerceiroPCIDADE: TWideStringField
      FieldName = 'PCIDADE'
      Size = 25
    end
    object QrTerceiroPUF: TSmallintField
      FieldName = 'PUF'
      Required = True
    end
    object QrTerceiroPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrTerceiroPPais: TWideStringField
      FieldName = 'PPais'
    end
    object QrTerceiroPTe1: TWideStringField
      FieldName = 'PTe1'
    end
    object QrTerceiroPTe2: TWideStringField
      FieldName = 'PTe2'
    end
    object QrTerceiroPTe3: TWideStringField
      FieldName = 'PTe3'
    end
    object QrTerceiroPCel: TWideStringField
      FieldName = 'PCel'
    end
    object QrTerceiroPFax: TWideStringField
      FieldName = 'PFax'
    end
    object QrTerceiroPEMail: TWideStringField
      FieldName = 'PEMail'
      Size = 100
    end
    object QrTerceiroPContato: TWideStringField
      FieldName = 'PContato'
      Size = 60
    end
    object QrTerceiroPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrTerceiroSexo: TWideStringField
      FieldName = 'Sexo'
      Required = True
      Size = 1
    end
    object QrTerceiroResponsavel: TWideStringField
      FieldName = 'Responsavel'
      Size = 60
    end
    object QrTerceiroProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrTerceiroCargo: TWideStringField
      FieldName = 'Cargo'
      Size = 60
    end
    object QrTerceiroRecibo: TSmallintField
      FieldName = 'Recibo'
      Required = True
    end
    object QrTerceiroDiaRecibo: TSmallintField
      FieldName = 'DiaRecibo'
      Required = True
    end
    object QrTerceiroAjudaEmpV: TFloatField
      FieldName = 'AjudaEmpV'
      Required = True
    end
    object QrTerceiroAjudaEmpP: TFloatField
      FieldName = 'AjudaEmpP'
      Required = True
    end
    object QrTerceiroCliente1: TWideStringField
      FieldName = 'Cliente1'
      Size = 1
    end
    object QrTerceiroCliente2: TWideStringField
      FieldName = 'Cliente2'
      Size = 1
    end
    object QrTerceiroFornece1: TWideStringField
      FieldName = 'Fornece1'
      Size = 1
    end
    object QrTerceiroFornece2: TWideStringField
      FieldName = 'Fornece2'
      Size = 1
    end
    object QrTerceiroFornece3: TWideStringField
      FieldName = 'Fornece3'
      Size = 1
    end
    object QrTerceiroFornece4: TWideStringField
      FieldName = 'Fornece4'
      Size = 1
    end
    object QrTerceiroTerceiro: TWideStringField
      FieldName = 'Terceiro'
      Size = 1
    end
    object QrTerceiroCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrTerceiroInformacoes: TWideStringField
      FieldName = 'Informacoes'
      Size = 255
    end
    object QrTerceiroLogo: TBlobField
      FieldName = 'Logo'
      Size = 4
    end
    object QrTerceiroVeiculo: TIntegerField
      FieldName = 'Veiculo'
    end
    object QrTerceiroMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrTerceiroObservacoes: TWideMemoField
      FieldName = 'Observacoes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrTerceiroTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrTerceiroLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTerceiroGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrTerceiroDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTerceiroDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTerceiroUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrTerceiroUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrTerceiroCRua: TWideStringField
      FieldName = 'CRua'
      Size = 30
    end
    object QrTerceiroCCompl: TWideStringField
      FieldName = 'CCompl'
      Size = 30
    end
    object QrTerceiroCBairro: TWideStringField
      FieldName = 'CBairro'
      Size = 30
    end
    object QrTerceiroCCIDADE: TWideStringField
      FieldName = 'CCIDADE'
      Size = 25
    end
    object QrTerceiroCUF: TSmallintField
      FieldName = 'CUF'
      Required = True
    end
    object QrTerceiroCCEP: TIntegerField
      FieldName = 'CCEP'
    end
    object QrTerceiroCPais: TWideStringField
      FieldName = 'CPais'
    end
    object QrTerceiroCTel: TWideStringField
      FieldName = 'CTel'
    end
    object QrTerceiroCFax: TWideStringField
      FieldName = 'CFax'
    end
    object QrTerceiroCCel: TWideStringField
      FieldName = 'CCel'
    end
    object QrTerceiroCContato: TWideStringField
      FieldName = 'CContato'
      Size = 60
    end
    object QrTerceiroLRua: TWideStringField
      FieldName = 'LRua'
      Size = 30
    end
    object QrTerceiroLCompl: TWideStringField
      FieldName = 'LCompl'
      Size = 30
    end
    object QrTerceiroLBairro: TWideStringField
      FieldName = 'LBairro'
      Size = 30
    end
    object QrTerceiroLCIDADE: TWideStringField
      FieldName = 'LCIDADE'
      Size = 25
    end
    object QrTerceiroLUF: TSmallintField
      FieldName = 'LUF'
      Required = True
    end
    object QrTerceiroLCEP: TIntegerField
      FieldName = 'LCEP'
    end
    object QrTerceiroLPais: TWideStringField
      FieldName = 'LPais'
    end
    object QrTerceiroLTel: TWideStringField
      FieldName = 'LTel'
    end
    object QrTerceiroLFax: TWideStringField
      FieldName = 'LFax'
    end
    object QrTerceiroLCel: TWideStringField
      FieldName = 'LCel'
    end
    object QrTerceiroLContato: TWideStringField
      FieldName = 'LContato'
      Size = 60
    end
    object QrTerceiroComissao: TFloatField
      FieldName = 'Comissao'
    end
    object QrTerceiroSituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object QrTerceiroNivel: TWideStringField
      FieldName = 'Nivel'
      Size = 1
    end
    object QrTerceiroNOMEUFP: TWideStringField
      FieldName = 'NOMEUFP'
      Required = True
      Size = 2
    end
    object QrTerceiroNOMEUFE: TWideStringField
      FieldName = 'NOMEUFE'
      Required = True
      Size = 2
    end
    object QrTerceiroENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrTerceiroPNumero: TIntegerField
      FieldName = 'PNumero'
    end
  end
  object QrSoma: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT SUM(Credito+Debito) VALOR'
      'FROM parcpagtos')
    Left = 548
    Top = 269
    object QrSomaVALOR: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'VALOR'
    end
  end
  object QrCredor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE 1 '
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 384
    Top = 165
    object QrCredorCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCredorNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object QrDevedor: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDevedorCalcFields
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE,'
      'CASE WHEN Tipo=0 THEN CNPJ'
      'ELSE CPF END CNPJ_CPF, '
      'Banco, Agencia, ContaCorrente'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 376
    Top = 205
    object QrDevedorCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrDevedorNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrDevedorCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrDevedorBanco: TSmallintField
      FieldName = 'Banco'
    end
    object QrDevedorAgencia: TWideStringField
      FieldName = 'Agencia'
      Size = 11
    end
    object QrDevedorContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrDevedorCNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_CPF_TXT'
      Size = 50
      Calculated = True
    end
  end
  object QrCliInt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente1="V" OR Cliente2="V" OR Terceiro="V" OR Codigo=-1'
      'ORDER BY NOMEENTIDADE')
    Left = 380
    Top = 245
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 408
    Top = 244
  end
  object QrGeneros: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'ORDER BY Nome'
      'WHERE (Codigo > 0 OR Codigo < -99)'
      'AND Credito=:P0'
      'AND Debito=:P1')
    Left = 350
    Top = 89
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGenerosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGenerosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsGeneros: TDataSource
    DataSet = QrGeneros
    Left = 378
    Top = 89
  end
end
