object FmMyPagtosCh: TFmMyPagtosCh
  Left = 352
  Top = 173
  Caption = 'Controle de Vencimento de Cheques'
  ClientHeight = 447
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 351
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 49
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label15: TLabel
        Left = 200
        Top = 8
        Width = 57
        Height = 13
        Caption = 'Valor Total: '
      end
      object Label14: TLabel
        Left = 288
        Top = 8
        Width = 52
        Height = 13
        Caption = 'Total dias: '
      end
      object Label16: TLabel
        Left = 356
        Top = 8
        Width = 48
        Height = 13
        Caption = 'Cheques: '
      end
      object Label17: TLabel
        Left = 424
        Top = 8
        Width = 79
        Height = 13
        Caption = 'Prazo m'#233'dio dd: '
      end
      object Label1: TLabel
        Left = 508
        Top = 8
        Width = 93
        Height = 13
        Caption = 'Vencimento m'#233'dio: '
      end
      object Label2: TLabel
        Left = 8
        Top = 8
        Width = 62
        Height = 13
        Caption = 'Lan'#231'amento:'
      end
      object Label3: TLabel
        Left = 100
        Top = 8
        Width = 55
        Height = 13
        Caption = 'Data inicial:'
      end
      object EdLancamento: TdmkEdit
        Left = 8
        Top = 24
        Width = 89
        Height = 21
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnChange = EdLancamentoChange
      end
      object TPIni: TDateTimePicker
        Left = 100
        Top = 24
        Width = 97
        Height = 21
        Date = 38242.441209502300000000
        Time = 38242.441209502300000000
        TabOrder = 1
        OnClick = TPIniClick
        OnChange = TPIniChange
      end
      object DBEdit1: TDBEdit
        Left = 200
        Top = 24
        Width = 85
        Height = 21
        DataField = 'Valor'
        DataSource = DsSoma
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 288
        Top = 24
        Width = 64
        Height = 21
        DataField = 'DIAS'
        DataSource = DsSoma
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 356
        Top = 24
        Width = 64
        Height = 21
        DataField = 'ITENS'
        DataSource = DsSoma
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 424
        Top = 24
        Width = 77
        Height = 21
        DataField = 'DDMEDIO'
        DataSource = DsSoma
        TabOrder = 5
      end
      object DBEdit5: TDBEdit
        Left = 508
        Top = 24
        Width = 89
        Height = 21
        DataField = 'VENCTOMEDIO'
        DataSource = DsSoma
        TabOrder = 6
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 90
      Width = 784
      Height = 261
      Align = alClient
      DataSource = DsMediaCh
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'ORDEM'
          Title.Caption = 'Ordem'
          Width = 46
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Width = 73
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DIAS'
          Title.Caption = 'Dias'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Cheque'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Conta'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Banco'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Emitente'
          Width = 248
          Visible = True
        end>
    end
    object RGOrdem: TRadioGroup
      Left = 0
      Top = 49
      Width = 784
      Height = 41
      Align = alTop
      Caption = ' Ordem: '
      Columns = 4
      ItemIndex = 0
      Items.Strings = (
        'Inclus'#227'o'
        'Valor Ascendente'
        'Valor Descendente'
        'Vencimento')
      TabOrder = 1
      OnClick = RGOrdemClick
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 399
    Width = 784
    Height = 48
    Align = alBottom
    ParentColor = True
    TabOrder = 1
    object BtSaida: TBitBtn
      Tag = 13
      Left = 684
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtSaidaClick
      NumGlyphs = 2
    end
    object BtExclui: TBitBtn
      Tag = 12
      Left = 372
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Exclui banco atual'
      Caption = '&Exclui'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtExcluiClick
      NumGlyphs = 2
    end
    object BtAltera: TBitBtn
      Tag = 11
      Left = 280
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Altera banco atual'
      Caption = '&Altera'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtAlteraClick
      NumGlyphs = 2
    end
    object BtInclui: TBitBtn
      Tag = 10
      Left = 188
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Inclui novo banco'
      Caption = '&Inclui'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = BtIncluiClick
      NumGlyphs = 2
    end
    object BtImprime: TBitBtn
      Tag = 5
      Left = 12
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Inclui novo banco'
      Caption = '&Preview'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = BtImprimeClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'M'#233'dia de Cheques'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 782
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object QrMediaCh: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrMediaChAfterOpen
    OnCalcFields = QrMediaChCalcFields
    SQL.Strings = (
      'SELECT * FROM mediach')
    Left = 152
    Top = 172
    object QrMediaChValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMediaChCheque: TWideStringField
      FieldName = 'Cheque'
      Size = 10
    end
    object QrMediaChBanco: TWideStringField
      FieldName = 'Banco'
      Size = 5
    end
    object QrMediaChConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrMediaChNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrMediaChVencto: TDateField
      FieldName = 'Vencto'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrMediaChORDEM: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'ORDEM'
      Calculated = True
    end
    object QrMediaChDIAS: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'DIAS'
      Calculated = True
    end
    object QrMediaChITENS: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'ITENS'
      Calculated = True
    end
    object QrMediaChCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrMediaChControle: TLargeintField
      FieldName = 'Controle'
    end
  end
  object DsMediaCh: TDataSource
    DataSet = QrMediaCh
    Left = 180
    Top = 172
  end
  object QrSoma: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSomaCalcFields
    SQL.Strings = (
      'SELECT SUM(Valor) Valor,'
      'SUM(Vencto- :P0) DIAS,'
      '(SUM((Vencto- :P1)*Valor) / SUM(Valor)) DDMEDIO,'
      'COUNT(*) ITENS'
      'FROM mediach'
      'WHERE Codigo=:P2')
    Left = 152
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSomaValor: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSomaDIAS: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'DIAS'
      DisplayFormat = '#,###,##0'
    end
    object QrSomaVENCTOMEDIO: TDateField
      FieldKind = fkCalculated
      FieldName = 'VENCTOMEDIO'
      DisplayFormat = 'dd/mm/yyyy'
      Calculated = True
    end
    object QrSomaDDMEDIO: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'DDMEDIO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSomaITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
  object DsSoma: TDataSource
    DataSet = QrSoma
    Left = 180
    Top = 200
  end
  object PMExclui: TPopupMenu
    Left = 348
    Top = 300
    object RegistroAtual1: TMenuItem
      Caption = 'Registro &Atual'
      OnClick = RegistroAtual1Click
    end
    object TodosRegistros1: TMenuItem
      Caption = '&Todos Registros'
      OnClick = TodosRegistros1Click
    end
  end
  object frxMediaCh: TfrxReport
    Version = '4.10.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.453874594900000000
    ReportOptions.LastChange = 39720.453874594900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxMediaChGetValue
    Left = 152
    Top = 228
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsMediaCH
        DataSetName = 'frxDsMediaCH'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 28.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          Left = 543.102350000000000000
          Top = 8.220470000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[DATE], [TIME]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 24.000000000000000000
        Top = 396.850650000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          Left = 461.559060000000000000
          Top = 8.472170000000010000
          Width = 244.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 5.118120000000000000
          Top = 4.472170000000010000
          Width = 705.543290000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 90.850340000000000000
        Top = 68.031540000000000000
        Width = 718.110700000000000000
        object Memo35: TfrxMemoView
          Left = 130.000000000000000000
          Top = 72.850340000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            'Vencto')
          ParentFont = False
        end
        object Picture2: TfrxPictureView
          Left = 22.000000000000000000
          Top = 4.850340000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo26: TfrxMemoView
          Left = 194.000000000000000000
          Top = 8.850340000000000000
          Width = 510.425170000000000000
          Height = 22.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 194.000000000000000000
          Top = 36.850340000000000000
          Width = 514.204700000000000000
          Height = 30.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'RELA'#195#8225#195#402'O DE CHEQUES E PRAZO M'#195#8240'DIO')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 22.000000000000000000
          Top = 72.850340000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            'Ordem')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 178.000000000000000000
          Top = 72.850340000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            'Dias')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 218.000000000000000000
          Top = 72.850340000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'Cheque')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 282.000000000000000000
          Top = 72.850340000000000000
          Width = 92.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'Conta')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 374.000000000000000000
          Top = 72.850340000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8 = (
            'Banco')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 70.000000000000000000
          Top = 72.850340000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8 = (
            'Valor')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 414.000000000000000000
          Top = 72.850340000000000000
          Width = 296.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8 = (
            'Emitente')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 15.118110240000000000
        Top = 219.212740000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsMediaCH
        DataSetName = 'frxDsMediaCH'
        RowCount = 0
        object Memo1: TfrxMemoView
          Left = 22.000000000000000000
          Width = 48.000000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[FormatFloat('#39'000;-000; '#39', <frxDsMediaCh."ORDEM">)]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 70.000000000000000000
          Width = 60.000000000000000000
          Height = 15.118110236220500000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsMediaCh."Valor"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 130.000000000000000000
          Width = 48.000000000000000000
          Height = 15.118110236220500000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsMediaCh."Vencto"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 178.000000000000000000
          Width = 40.000000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsMediaCh."DIAS"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 218.000000000000000000
          Width = 64.000000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[FormatFloat('#39'000000;-000000; '#39', <frxDsMediaCh."Cheque">)]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 282.000000000000000000
          Width = 92.000000000000000000
          Height = 15.118110236220500000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsMediaCh."Conta"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 374.000000000000000000
          Width = 40.000000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[FormatFloat('#39'000;-000; '#39', <frxDsMediaCh."Banco">)]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 414.000000000000000000
          Width = 296.000000000000000000
          Height = 15.118110236220500000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsMediaCh."Nome"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 80.000000000000000000
        Top = 294.803340000000000000
        Width = 718.110700000000000000
        object Line2: TfrxLineView
          Left = 5.559060000000000000
          Top = 3.653370000000000000
          Width = 704.661410000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
        object Memo13: TfrxMemoView
          Left = 22.000000000000000000
          Top = 11.873840000000000000
          Width = 80.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Valor total:')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 242.000000000000000000
          Top = 11.873840000000000000
          Width = 80.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Cheques:')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 22.000000000000000000
          Top = 35.873840000000000000
          Width = 80.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Data doc.:')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 102.000000000000000000
          Top = 11.873840000000000000
          Width = 120.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[VALOR_TOTAL]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 322.000000000000000000
          Top = 11.873840000000000000
          Width = 120.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[TOTAL_CHEQUES]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 102.000000000000000000
          Top = 35.873840000000000000
          Width = 120.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[DATA_DOC]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 242.000000000000000000
          Top = 35.873840000000000000
          Width = 80.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Total dias :')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 322.000000000000000000
          Top = 35.873840000000000000
          Width = 120.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[TOTAL_DIAS]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 462.000000000000000000
          Top = 11.873840000000000000
          Width = 112.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Prazo m'#195#169'dio:')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 574.000000000000000000
          Top = 11.873840000000000000
          Width = 120.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[PRAZO_MEDIO]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 462.000000000000000000
          Top = 35.873840000000000000
          Width = 112.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Vencto m'#195#169'dio:')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 574.000000000000000000
          Top = 35.873840000000000000
          Width = 120.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[VENCTO_MEDIO]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsMediaCH: TfrxDBDataset
    UserName = 'frxDsMediaCH'
    CloseDataSource = False
    DataSet = QrMediaCh
    BCDToCurrency = False
    Left = 180
    Top = 228
  end
end
