object FmMyPagtosChEdit: TFmMyPagtosChEdit
  Left = 404
  Top = 279
  Caption = 'M'#233'dia de Cheques'
  ClientHeight = 263
  ClientWidth = 470
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 215
    Width = 470
    Height = 48
    Align = alBottom
    TabOrder = 2
    object LaIncluido: TLabel
      Left = 168
      Top = 16
      Width = 134
      Height = 20
      Caption = 'Cheque incluido!'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 100
      Height = 46
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Caption = '&Confirma'
        TabOrder = 0
        OnClick = BtConfirmaClick
        NumGlyphs = 2
      end
    end
    object Panel2: TPanel
      Left = 370
      Top = 1
      Width = 99
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 0
        Top = 3
        Width = 90
        Height = 40
        Caption = '&Sair'
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 81
    Width = 470
    Height = 134
    Align = alClient
    TabOrder = 1
    object LaValor: TLabel
      Left = 20
      Top = 52
      Width = 27
      Height = 13
      Caption = 'Valor:'
    end
    object Label1: TLabel
      Left = 120
      Top = 52
      Width = 86
      Height = 13
      Caption = 'Vencto [ddmmaa]:'
    end
    object Label2: TLabel
      Left = 216
      Top = 52
      Width = 53
      Height = 13
      Caption = 'Cheque n'#186':'
    end
    object Label3: TLabel
      Left = 284
      Top = 52
      Width = 47
      Height = 13
      Caption = 'Banco n'#186':'
    end
    object Label4: TLabel
      Left = 348
      Top = 52
      Width = 31
      Height = 13
      Caption = 'Conta:'
    end
    object Label5: TLabel
      Left = 20
      Top = 92
      Width = 44
      Height = 13
      Caption = 'Emitente:'
    end
    object Label6: TLabel
      Left = 20
      Top = 12
      Width = 62
      Height = 13
      Caption = 'Lan'#231'amento:'
    end
    object Label7: TLabel
      Left = 116
      Top = 12
      Width = 42
      Height = 13
      Caption = 'Controle:'
    end
    object EdValor: TdmkEdit
      Left = 20
      Top = 68
      Width = 93
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object EdVencto: TdmkEdit
      Left = 120
      Top = 68
      Width = 93
      Height = 21
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnExit = EdVenctoExit
    end
    object EdCheque: TdmkEdit
      Left = 216
      Top = 68
      Width = 61
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 6
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdBanco: TdmkEdit
      Left = 284
      Top = 68
      Width = 61
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 3
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnExit = EdBancoExit
    end
    object EdConta: TdmkEdit
      Left = 348
      Top = 68
      Width = 113
      Height = 21
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdEmitente: TdmkEdit
      Left = 20
      Top = 108
      Width = 441
      Height = 21
      TabOrder = 5
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdLancto: TdmkEdit
      Left = 20
      Top = 28
      Width = 93
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Color = clBtnFace
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdControle: TdmkEdit
      Left = 116
      Top = 28
      Width = 93
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Color = clBtnFace
      TabOrder = 7
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 470
    Height = 48
    Align = alTop
    Caption = 'M'#233'dia de Cheques'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 3
    object LaTipo: TLabel
      Left = 391
      Top = 1
      Width = 78
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 398
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 390
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 396
      ExplicitHeight = 44
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 48
    Width = 470
    Height = 33
    Align = alTop
    TabOrder = 0
    object CkContinua: TCheckBox
      Left = 8
      Top = 7
      Width = 121
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object CkLimparDados: TCheckBox
      Left = 140
      Top = 7
      Width = 121
      Height = 17
      Caption = 'Limpar dados.'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 2000
    OnTimer = Timer1Timer
    Left = 200
    Top = 88
  end
end
