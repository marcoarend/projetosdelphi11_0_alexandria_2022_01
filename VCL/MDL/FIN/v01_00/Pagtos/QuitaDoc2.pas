unit QuitaDoc2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Buttons, Db, (*DBTables,*) UnInternalConsts, UnMLAGeral,
  UnDmkProcFunc, UnGOTOy, UMySQLModule, ExtCtrls, Mask, DBCtrls, mySQLDbTables,
  dmkGeral, dmkEdit, dmkEditDateTimePicker, UnDmkEnums;

type
  TFmQuitaDoc2 = class(TForm)
    QrLancto1: TMySQLQuery;
    PainelDados: TPanel;
    Label1: TLabel;
    TPData1: TdmkEditDateTimePicker;
    Label2: TLabel;
    EdDoc: TDmkEdit;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    DsLancto1: TDataSource;
    QrLancto2: TMySQLQuery;
    DsLancto2: TDataSource;
    EdDescricao: TdmkEdit;
    QrLancto1Sub: TSmallintField;
    QrLancto1Descricao: TWideStringField;
    QrLancto1Credito: TFloatField;
    QrLancto1Debito: TFloatField;
    QrLancto1NotaFiscal: TIntegerField;
    QrLancto1Documento: TFloatField;
    QrLancto1Vencimento: TDateField;
    QrLancto1Carteira: TIntegerField;
    QrLancto1Data: TDateField;
    QrLancto1NOMECARTEIRA: TWideStringField;
    QrLancto1TIPOCARTEIRA: TIntegerField;
    QrLancto1CARTEIRABANCO: TIntegerField;
    QrLancto2Tipo: TSmallintField;
    QrLancto2Credito: TFloatField;
    QrLancto2Debito: TFloatField;
    QrLancto2Sub: TSmallintField;
    QrLancto2Carteira: TIntegerField;
    PainelTitulo: TPanel;
    Image1: TImage;
    QrLancto1Cliente: TIntegerField;
    QrLancto1Fornecedor: TIntegerField;
    QrLancto1DataDoc: TDateField;
    QrLancto1Nivel: TIntegerField;
    QrLancto1Vendedor: TIntegerField;
    QrLancto1Account: TIntegerField;
    QrLancto1Controle: TIntegerField;
    QrLancto1CtrlIni: TIntegerField;
    QrLancto2Controle: TIntegerField;
    QrLancto1Genero: TIntegerField;
    QrLancto1Mez: TIntegerField;
    QrLancto1Duplicata: TWideStringField;
    QrLancto1Doc2: TWideStringField;
    QrLancto1SerieCH: TWideStringField;
    QrLancto1MoraDia: TFloatField;
    QrLancto1Multa: TFloatField;
    QrLancto1ICMS_P: TFloatField;
    QrLancto1ICMS_V: TFloatField;
    QrLancto1CliInt: TIntegerField;
    QrLancto1Depto: TIntegerField;
    QrLancto1DescoPor: TIntegerField;
    QrLancto1ForneceI: TIntegerField;
    QrLancto1Unidade: TIntegerField;
    QrLancto1Qtde: TFloatField;
    QrLancto1FatID: TIntegerField;
    QrLancto1FatID_Sub: TIntegerField;
    QrLancto1DescoVal: TFloatField;
    QrLancto1NFVal: TFloatField;
    QrLancto1FatParcela: TIntegerField;
    Label8: TLabel;
    EdSerieCH: TDmkEdit;
    EdValor: TDmkEdit;
    Label10: TLabel;
    EdMultaVal: TDmkEdit;
    Label11: TLabel;
    EdMoraVal: TDmkEdit;
    Label12: TLabel;
    EdMulta: TDmkEdit;
    Label14: TLabel;
    Label15: TLabel;
    EdMoraDia: TDmkEdit;
    QrLancto1FatNum: TFloatField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    EdLancto: TdmkEdit;
    EdSub: TdmkEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TPData1Change(Sender: TObject);
    procedure TPData1Click(Sender: TObject);
    procedure EdMultaExit(Sender: TObject);
    procedure EdMoraDiaExit(Sender: TObject);
    procedure EdMultaValExit(Sender: TObject);
    procedure EdMoraValExit(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaMultaJuroVal;
    procedure CalculaMultaJuroPer;
  public
    { Public declarations }
    FCtrlIni, FControle: Double;
    //
    procedure AcoesIniciais();
  end;

var
  FmQuitaDoc2: TFmQuitaDoc2;

implementation

uses UnMyObjects, Module, Principal, UnFinanceiro;

{$R *.DFM}

procedure TFmQuitaDoc2.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmQuitaDoc2.CalculaMultaJuroPer;
var
  ValOrig, ValorNew, MultaPerc, JuroPerc: Double;
begin
  ValOrig := QrLancto1Credito.Value - QrLancto1Debito.Value;
  //
  if ValOrig < 0 then
    ValOrig := ValOrig * -1;
  //
  UFinanceiro.CalculaPercMultaEJuros(ValOrig, EdMultaVal.ValueVariant,
    EdMoraVal.ValueVariant, TPData1.Date, QrLancto1Vencimento.Value,
    ValorNew, MultaPerc, JuroPerc);
  //
  EdMoraDia.ValueVariant := JuroPerc;
  EdMulta.ValueVariant   := MultaPerc;
  EdValor.ValueVariant   := ValorNew;
end;

procedure TFmQuitaDoc2.CalculaMultaJuroVal;
var
  ValOrig, MultaPer, JuroPer, ValAtualiz, ValMulta, ValJuro: Double;
begin
  MultaPer := EdMulta.ValueVariant;
  JuroPer  := EdMoraDia.ValueVariant;
  ValOrig  := QrLancto1Credito.Value - QrLancto1Debito.Value;
  //
  if ValOrig < 0 then
    ValOrig := ValOrig * -1;
  //
  UFinanceiro.CalculaValMultaEJuros(ValOrig, TPData1.Date,
    QrLancto1Vencimento.Value, MultaPer, JuroPer, ValAtualiz, ValMulta,
    ValJuro, True);
  //
  EdMoraVal.ValueVariant  := ValJuro;
  EdMultaVal.ValueVariant := ValMulta;
  EdValor.ValueVariant    := ValAtualiz;
end;

procedure TFmQuitaDoc2.EdMoraDiaExit(Sender: TObject);
begin
  CalculaMultaJuroVal;
end;

procedure TFmQuitaDoc2.EdMoraValExit(Sender: TObject);
begin
  CalculaMultaJuroPer;
end;

procedure TFmQuitaDoc2.EdMultaExit(Sender: TObject);
begin
  CalculaMultaJuroVal;
end;

procedure TFmQuitaDoc2.EdMultaValExit(Sender: TObject);
begin
  CalculaMultaJuroPer;
end;

procedure TFmQuitaDoc2.FormActivate(Sender: TObject);
begin
  EdDoc.SetFocus;
  MyObjects.CorIniComponente();
  //
  AcoesIniciais();
  //
  EdDescricao.setFocus;
end;

procedure TFmQuitaDoc2.AcoesIniciais();
var
  Controle, PerMulta, PerJuro, Valor, ValAtualiz, ValMulta, ValJuro: Double;
  Sub: Integer;
begin
  Controle :=   EdLancto.ValueVariant;
  Sub :=        EdSub.ValueVariant;
  QrLancto1.Params[0].AsFloat := Controle;
  QrLancto1.Params[1].AsInteger := Sub;
  QrLancto1.Open;
  if GOTOy.Registros(QrLancto1) = 0 then
  begin
    ShowMessage('Registro n�o encontrado');
    BtConfirma.Enabled := False;
  end;
  //TPData1.Date     := QrLancto1Vencimento.Value;
  TPData1.Date     := Date;
  EdDescricao.Text := QrLancto1Descricao.Value;
  EdDoc.ValueVariant := QrLancto1Documento.Value;
  EdSerieCH.Text   := QrLancto1SerieCH.Value;
  if QrLancto1Debito.Value > 0 then
  begin
    Label10.Caption := Dmod.QrControleMoeda.Value + ' D�bito:';
    Valor           := Qrlancto1Debito.Value;
  end else begin
    Label10.Caption := Dmod.QrControleMoeda.Value + ' Cr�dito:';
    Valor           := Qrlancto1Credito.Value;
  end;
  //EdMoraDia.Text := Geral.FFT(Qrlancto1MoraDia.Value, 6, siPositivo);
  //EdMulta.Text   := Geral.FFT(Qrlancto1Multa.Value, 6, siPositivo);
  //
  PerMulta := QrLancto1Multa.Value;
  PerJuro  := QrLancto1MoraDia.Value;
  //
  UFinanceiro.CalculaValMultaEJuros(Valor, Date, QrLancto1Vencimento.Value,
    PerMulta, PerJuro, ValAtualiz, ValMulta, ValJuro, False);
  //
  EdMulta.ValueVariant    := PerMulta;
  EdMoraDia.ValueVariant  := PerJuro;
  EdMultaVal.ValueVariant := ValMulta;
  EdMoraVal.ValueVariant  := ValJuro;
  EdValor.ValueVariant    := ValAtualiz;
  //
  QrLancto2.Close;
  QrLancto2.Params[0].AsFloat := Controle;
  QrLancto2.Params[1].AsInteger := QrLancto1Sub.Value;
  QrLancto2.Open;
end;

procedure TFmQuitaDoc2.BtConfirmaClick(Sender: TObject);
var
  Texto: PChar;
  Doc, Controle, Controle2, Credito, Debito: Double;
  Sub: Integer;
begin
  if QrLancto1Debito.Value > 0 then
  begin
    Debito := Geral.DMV(EdValor.Text);
    Credito := 0;
  end else begin
    Debito := 0;
    Credito := Geral.DMV(EdValor.Text);
  end;
  FLAN_Nivel := QrLancto1Nivel.Value + 1;
  if FCtrlIni = -1000 then ShowMessage('"FCtrlIni" n�o definido');
  if FCtrlIni > 0 then
    FLAN_CtrlIni := Trunc(FCtrlIni)
  else
    FLAN_CtrlIni := Trunc(FControle);
  //
  Controle := QrLancto1Controle.Value;
  Sub := QrLancto1Sub.Value;
  Doc := Geral.DMV(EdDoc.Text);
  //
  if QrLancto2.RecordCount <> 0 then
  begin
    if ((QrLancto2Credito.Value + QrLancto2Debito.Value) =
      (QrLancto1Credito.Value + QrLancto1Debito.Value))
    and (GOTOy.Registros(QrLancto2) = 1) and (QrLancto2Tipo.Value = 1) then
    begin
      Texto := PChar('J� existe uma quita��o, e ela possui o mesmo valor,'+
      ' deseja difini-la como quita��o desta emiss�o ?');
      if Application.MessageBox(Texto, PChar(VAR_APPNAME),
      MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+MB_APPLMODAL) = ID_YES then
      begin
        {
        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Sit=3, Compensado=:P0,');
        Dmod.QrUpdM.SQL.Add('DataAlt=:P1, UserAlt=:P2');
        Dmod.QrUpdM.SQL.Add('WHERE Controle=:P3 AND Tipo=2');
        Dmod.QrUpdM.Params[0].AsString := FormatDateTime(VAR_FORMATDATE, TPData1.Date);
        Dmod.QrUpdM.Params[1].AsString := FormatDateTime(VAR_FORMATDATE, Date);
        Dmod.QrUpdM.Params[2].AsInteger := VAR_USUARIO;
        Dmod.QrUpdM.Params[3].AsFloat := Controle;
//        Dmod.QrUpdM.Params[2].AsInteger := Sub; precisa??
        Dmod.QrUpdM.ExecSQL;
        }
        UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
        'Sit', 'Compensado'], ['Controle', 'Tipo'], [3,
        FormatDateTime(VAR_FORMATDATE, TPData1.Date)], [Controle, 2], True, '');
        //
        UFinanceiro.RecalcSaldoCarteira(QrLancto1Carteira.Value,
          nil, False, False);
        //
        {
        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Data=:P0,');
        Dmod.QrUpdM.SQL.Add('DataAlt=:P1, UserAlt=:P2');
        Dmod.QrUpdM.SQL.Add('WHERE ID_Pgto=:P3 AND Tipo=1 AND ID_Pgto>0');
        Dmod.QrUpdM.Params[0].AsString := FormatDateTime(VAR_FORMATDATE, TPData1.Date);
        Dmod.QrUpdM.Params[1].AsString := FormatDateTime(VAR_FORMATDATE, Date);
        Dmod.QrUpdM.Params[2].AsInteger := VAR_USUARIO;
        Dmod.QrUpdM.Params[3].AsFloat := Controle;
        Dmod.QrUpdM.ExecSQL;
        }
        if Controle > 0 then
        begin
          UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
          'Data'], ['ID_Pgto', 'Tipo'], [FormatDateTime(VAR_FORMATDATE,
          TPData1.Date)], [Controle, 1], True, '');
        end;
        UFinanceiro.RecalcSaldoCarteira(QrLancto1Carteira.Value, nil, False, False);
      end;
    end else ShowMessage('J� existe pagamentos/rolagem para esta emiss�o!');
  end else begin
    Controle2 := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      VAR_LCT, VAR_LCT, 'Controle');
    //
    {
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Sit=3, ');
    Dmod.QrUpdM.SQL.Add('Compensado=:P0, Documento=:P1, Descricao=:P2, ');
    Dmod.QrUpdM.SQL.Add('ID_Quit=:P3, DataAlt=:Pa, UserAlt=:Pb');
    Dmod.QrUpdM.SQL.Add('WHERE Controle=:Pc AND Sub=:Pd AND Tipo=2');
    Dmod.QrUpdM.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TPData1.Date);
    Dmod.QrUpdM.Params[01].AsFloat   := Doc;
    Dmod.QrUpdM.Params[02].AsString  := EdDescricao.Text;
    Dmod.QrUpdM.Params[03].AsFloat   := Controle2;
    //
    Dmod.QrUpdM.Params[04].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpdM.Params[05].AsInteger := VAR_USUARIO;
    Dmod.QrUpdM.Params[06].AsFloat   := Controle;
    Dmod.QrUpdM.Params[07].AsInteger := Sub;
    Dmod.QrUpdM.ExecSQL;
    }
    UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
    'Sit', 'Compensado', 'Documento', 'Descricao', 'ID_Quit'], [
    'Controle', 'Sub', 'Tipo'], [3, FormatDateTime(VAR_FORMATDATE, TPData1.Date),
    Doc, EdDescricao.Text, Controle2], [Controle, Sub, 2], True, '');
    //
    UFinanceiro.RecalcSaldoCarteira(QrLancto1Carteira.Value,
      nil, False, False);

    //
    UFinanceiro.LancamentoDefaultVARS;
    FLAN_Data       := Geral.FDT(TPData1.Date, 1);
    FLAN_Vencimento := Geral.FDT(QrLancto1Vencimento.Value, 1);
    FLAN_DataCad    := Geral.FDT(Date, 1);
    FLAN_Mez        := dmkPF.ITS_NULL(QrLancto1Mez.Value);
    FLAN_Descricao  := EdDescricao.Text;
    FLAN_Compensado := Geral.FDT(TPData1.Date, 1);
    FLAN_Duplicata  := QrLancto1Duplicata.Value;
    FLAN_Doc2       := QrLancto1Doc2.Value;
    FLAN_SerieCH    := QrLancto1SerieCH.Value;

    FLAN_Documento  := Trunc(QrLancto1Documento.Value);
    FLAN_Tipo       := 1;
    FLAN_Carteira   := QrLancto1CARTEIRABANCO.Value;
    FLAN_Credito    := Credito;
    FLAN_Debito     := Debito;
    FLAN_Genero     := QrLancto1Genero.Value;
    FLAN_NotaFiscal := QrLancto1NotaFiscal.Value;
    FLAN_Sit        := 3;
    FLAN_Cartao     := 0;
    FLAN_Linha      := 0;
    FLAN_Fornecedor := QrLancto1Fornecedor.Value;
    FLAN_Cliente    := QrLancto1Cliente.Value;
    FLAN_MoraDia    := QrLancto1MoraDia.Value;
    FLAN_Multa      := QrLancto1Multa.Value;
    //FLAN_UserCad    := VAR_USUARIO;
    //FLAN_DataDoc    := Geral.FDT(Date, 1);
    FLAN_Vendedor   := QrLancto1Vendedor.Value;
    FLAN_Account    := QrLancto1Account.Value;
    FLAN_ICMS_P     := QrLancto1ICMS_P.Value;
    FLAN_ICMS_V     := QrLancto1ICMS_V.Value;
    FLAN_CliInt     := QrLancto1CliInt.Value;
    FLAN_Depto      := QrLancto1Depto.Value;
    FLAN_DescoPor   := QrLancto1DescoPor.Value;
    FLAN_ForneceI   := QrLancto1ForneceI.Value;
    FLAN_DescoVal   := QrLancto1DescoVal.Value;
    FLAN_NFVal      := QrLancto1NFVal.Value;
    FLAN_Unidade    := QrLancto1Unidade.Value;
    FLAN_Qtde       := QrLancto1Qtde.Value;
    FLAN_FatID      := QrLancto1FatID.Value;
    FLAN_FatID_Sub  := QrLancto1FatID_Sub.Value;
    FLAN_FatNum     := Trunc(QrLancto1FatNum.Value);
    FLAN_FatParcela := QrLancto1FatParcela.Value;
    FLAN_MultaVal   := Geral.DMV(EdMultaVal.Text);
    FLAN_MoraVal    := Geral.DMV(EdMoraVal.Text);
    FLAN_ID_Pgto    := Round(Controle);
    FLAN_Controle   := Round(Controle2);
    //
    UFinanceiro.InsereLancamento();
    UFinanceiro.RecalcSaldoCarteira(QrLancto1CARTEIRABANCO.Value, nil, False, False);
  end;
  Close;
end;

procedure TFmQuitaDoc2.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmQuitaDoc2.TPData1Change(Sender: TObject);
begin
  CalculaMultaJuroVal;
end;

procedure TFmQuitaDoc2.TPData1Click(Sender: TObject);
begin
  CalculaMultaJuroVal;
end;

procedure TFmQuitaDoc2.FormCreate(Sender: TObject);
begin
  FCtrlIni := -1000;
end;

end.
