unit LocPgto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkDBGrid, ComCtrls,
  Db, mySQLDbTables;

type
  TFmLocPgto = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    dmkDBGrid1: TdmkDBGrid;
    BtLocaliza1: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtLocaliza1Click(Sender: TObject);
    procedure dmkDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FDTPDataIni, FDTPDataFim: TDateTimePicker;
    FQrCarteiras, FQuery: TmySQLQuery;
  end;

  var
  FmLocPgto: TFmLocPgto;

implementation

uses UnMyObjects, ModuleFin, Module, Principal, LocDefs, UnInternalConsts;

{$R *.DFM}

procedure TFmLocPgto.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocPgto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLocPgto.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLocPgto.BtLocaliza1Click(Sender: TObject);
var
  Lancto: Double;
begin
  Screen.Cursor := crHourGlass;
  if FDTPDataIni <> nil then
  begin
    if DmodFin.QrLPEData.Value < FDTPDataIni.Date then
    FDTPDataIni.Date := DmodFin.QrLPEData.Value;
  end;
  if FDTPDataFim <> nil then
  begin
    if DmodFin.QrLPEData.Value > FDTPDataFim.Date then
    FDTPDataFim.Date := DmodFin.QrLPEData.Value;
  end;
  //
  VAR_LANCTO2 := DmodFin.QrLPEControle.Value;
  Lancto      := DmodFin.QrLPEControle.Value;
  DmodFin.DefParams(FQrCarteiras, nil, True, 'TFmLocPgto.BtLocaliza1Click()');
  DmodFin.LocCod(DmodFin.QrLPECarteira.Value, DmodFin.QrLPECarteira.Value,
    DmodFin.QrCarts, 'TFmLocPgto.BtLocaliza1Click()');
  if FQuery = nil then
    Application.MessageBox('N�o h� tabela de lan�amentos definida!', 'Aviso',
    MB_OK+MB_ICONWARNING)
  else begin
    if not FQuery.Locate('Controle', Lancto, []) then
    begin
      if not FQuery.Locate('Controle', Lancto, []) then
        Application.MessageBox(PChar('N�o foi poss�vel localizar o lan�amento ' +
        FormatFloat('0', Lancto) + '! Verifique o p�riodo pesquisado!'),
        'Aviso', MB_OK+MB_ICONWARNING);
    end else Close;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmLocPgto.dmkDBGrid1DblClick(Sender: TObject);
begin
  BtLocaliza1Click(Self);
end;

end.
