unit MyPagtosCh;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Grids, DBGrids, Buttons, Db, mySQLDbTables, ComCtrls,
  Mask, DBCtrls, Menus, frxClass, frxDBSet, dmkGeral, dmkEdit;

type
  TFmMyPagtosCh = class(TForm)
    Panel2: TPanel;
    Panel4: TPanel;
    Label15: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    DBGrid1: TDBGrid;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Label1: TLabel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Label2: TLabel;
    EdLancamento: TdmkEdit;
    QrMediaCh: TmySQLQuery;
    QrMediaChValor: TFloatField;
    QrMediaChConta: TWideStringField;
    QrMediaChNome: TWideStringField;
    QrMediaChVencto: TDateField;
    DsMediaCh: TDataSource;
    QrMediaChORDEM: TIntegerField;
    QrMediaChDIAS: TIntegerField;
    TPIni: TDateTimePicker;
    Label3: TLabel;
    RGOrdem: TRadioGroup;
    QrSoma: TmySQLQuery;
    QrSomaValor: TFloatField;
    QrSomaDIAS: TFloatField;
    DBEdit1: TDBEdit;
    DsSoma: TDataSource;
    DBEdit2: TDBEdit;
    QrMediaChITENS: TIntegerField;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    QrSomaVENCTOMEDIO: TDateField;
    DBEdit5: TDBEdit;
    QrMediaChCheque: TWideStringField;
    QrMediaChBanco: TWideStringField;
    QrMediaChCodigo: TLargeintField;
    QrMediaChControle: TLargeintField;
    PMExclui: TPopupMenu;
    RegistroAtual1: TMenuItem;
    TodosRegistros1: TMenuItem;
    QrSomaDDMEDIO: TFloatField;
    BtImprime: TBitBtn;
    frxMediaCh: TfrxReport;
    frxDsMediaCH: TfrxDBDataset;
    QrSomaITENS: TLargeintField;
    procedure BtIncluiClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure QrMediaChCalcFields(DataSet: TDataSet);
    procedure FormResize(Sender: TObject);
    procedure RGOrdemClick(Sender: TObject);
    procedure TPIniChange(Sender: TObject);
    procedure TPIniClick(Sender: TObject);
    procedure EdLancamentoChange(Sender: TObject);
    procedure QrSomaCalcFields(DataSet: TDataSet);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure QrMediaChAfterOpen(DataSet: TDataSet);
    procedure RegistroAtual1Click(Sender: TObject);
    procedure TodosRegistros1Click(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure frxMediaChGetValue(const VarName: String;
      var Value: Variant);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FDataIni: String;
    FLancamento: Integer;
    procedure ReopenIts;
    procedure CalculaPrazoMedio;
  public
    { Public declarations }
  end;

var
  FmMyPagtosCh: TFmMyPagtosCh;

implementation

uses UnMyObjects, MyPagtosChEdit, UnInternalConsts, UnMLAGeral, Module;

{$R *.DFM}

procedure TFmMyPagtosCh.BtIncluiClick(Sender: TObject);
begin
  Application.CreateForm(TFmMyPagtosChEdit, FmMyPagtosChEdit);
  with FmMyPagtosChEdit do
  begin
    LaTipo.Caption := CO_INCLUSAO;
    EdLancto.Text := EdLancamento.Text;
    ShowModal;
    Destroy;
  end;
  CalculaPrazoMedio;
  ReopenIts;
end;

procedure TFmMyPagtosCh.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMyPagtosCh.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  FDataIni := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  FLancamento := Geral.IMV(EdLancamento.Text);
  ReopenIts;
  TPIni.SetFocus;
end;

procedure TFmMyPagtosCh.QrMediaChCalcFields(DataSet: TDataSet);
begin
  QrMediaChORDEM.Value := QrMediaCh.RecNo;
  QrMediaChITENS.Value := QrMediaCh.RecordCount;
  QrMediaChDIAS.Value := Trunc(QrMediaChVencto.Value) - Trunc(TPIni.Date);
end;

procedure TFmMyPagtosCh.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 35);
end;

procedure TFmMyPagtosCh.RGOrdemClick(Sender: TObject);
begin
  ReopenIts;
end;

procedure TFmMyPagtosCh.ReopenIts;
begin
  QrMediaCh.Close;
  QrMediaCh.SQL.Clear;
  QrMediaCh.SQL.Add('SELECT * FROM mediach');
  QrMediaCh.SQL.Add('WHERE Codigo='+EdLancamento.Text);
  case RGOrdem.ItemIndex of
    0: ;
    1: QrMediaCh.SQL.Add('ORDER BY Valor ASC');
    2: QrMediaCh.SQL.Add('ORDER BY Valor DESC');
    3: QrMediaCh.SQL.Add('ORDER BY Vencto ASC');
  end;
  QrMediaCh.Open;
  CalculaPrazoMedio;
end;

procedure TFmMyPagtosCh.CalculaPrazoMedio;
begin
  QrSoma.Close;
  QrSoma.SQL.Clear;
  QrSoma.SQL.Add('SELECT SUM(Valor) Valor,');
  QrSoma.SQL.Add('SUM(TO_DAYS(Vencto)-TO_DAYS("'+Geral.SoNumero_TT(FDataIni)+'")) DIAS,');
  QrSoma.SQL.Add('(SUM((TO_DAYS(Vencto)-TO_DAYS("'+Geral.SoNumero_TT(FDataIni)+
  '")) *Valor) / SUM(Valor)) DDMEDIO,');
  QrSoma.SQL.Add('COUNT(*) ITENS');
  QrSoma.SQL.Add('FROM mediach');
  QrSoma.SQL.Add('WHERE Codigo='+IntToStr(FLancamento));
  QrSoma.Open;
end;

procedure TFmMyPagtosCh.TPIniChange(Sender: TObject);
begin
  FDataIni := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  CalculaPrazoMedio;
end;

procedure TFmMyPagtosCh.TPIniClick(Sender: TObject);
begin
  FDataIni := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  CalculaPrazoMedio;
end;

procedure TFmMyPagtosCh.EdLancamentoChange(Sender: TObject);
begin
  FLancamento := Geral.IMV(EdLancamento.Text);
end;

procedure TFmMyPagtosCh.QrSomaCalcFields(DataSet: TDataSet);
begin
  QrSomaVENCTOMEDIO.Value := TPIni.Date + QrSomaDDMEDIO.Value;
end;

procedure TFmMyPagtosCh.BtAlteraClick(Sender: TObject);
begin
  Application.CreateForm(TFmMyPagtosChEdit, FmMyPagtosChEdit);
  with FmMyPagtosChEdit do
  begin
    LaTipo.Caption  := CO_ALTERACAO;
    EdLancto.Text   := EdLancamento.Text;
    EdControle.Text := FormatFloat('0', QrMediaChControle.Value);
    EdValor.ValueVariant := QrMediaChValor.Value;
    EdVencto.Text   := FormatDateTime(VAR_FORMATDATE2, QrMediaChVencto.Value);
    EdCheque.Text   := QrMediaChCheque.Value;
    EdConta.Text    := QrMediaChConta.Value;
    EdBanco.Text    := QrMediaChBanco.Value;
    EdEmitente.Text := QrMediaChNome.Value;
    ShowModal;
    Destroy;
  end;
  CalculaPrazoMedio;
  ReopenIts;
end;

procedure TFmMyPagtosCh.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmMyPagtosCh.QrMediaChAfterOpen(DataSet: TDataSet);
begin
  if QrMediaCh.RecordCount > 0 then
  begin
    BtAltera.Enabled := True;
    BtExclui.Enabled := True;
  end else begin
    BtAltera.Enabled := False;
    BtExclui.Enabled := False;
  end;
end;

procedure TFmMyPagtosCh.RegistroAtual1Click(Sender: TObject);
begin
  if Application.MessageBox('Confirma a exclus�o deste cheque?',
  'Exclus�o de Cheque', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM mediach WHERE Codigo=:P0 AND Controle=:P1');
    Dmod.QrUpd.Params[0].AsFloat := QrMediaChCodigo.Value;
    Dmod.QrUpd.Params[1].AsFloat := QrMediaChControle.Value;
    Dmod.QrUpd.ExecSQL;
    ReopenIts;
  end;
end;

procedure TFmMyPagtosCh.TodosRegistros1Click(Sender: TObject);
begin
  if Application.MessageBox('Confirma a exclus�o de TODOS cheques deste lan�amento?',
  'Exclus�o de TODOS Cheques do Lan�amento Atual',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM mediach WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsFloat := QrMediaChCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    ReopenIts;
  end;
end;

procedure TFmMyPagtosCh.BtImprimeClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxMediaCH, 'M�dia de cheques');
end;

procedure TFmMyPagtosCh.frxMediaChGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VALOR_TOTAL' then Value := DBEdit1.Text
  else if VarName = 'DATA_DOC' then Value := FormatDateTime(VAR_FORMATDATE2, TPIni.Date)
  else if VarName = 'TOTAL_CHEQUES' then Value := DBEdit3.Text
  else if VarName = 'TOTAL_DIAS' then Value := DBEdit2.Text
  else if VarName = 'PRAZO_MEDIO' then Value := DBEdit4.Text
  else if VarName = 'VENCTO_MEDIO' then Value := DBEdit5.Text
end;

procedure TFmMyPagtosCh.FormCreate(Sender: TObject);
begin
  TPIni.Date := Date;
end;

end.

