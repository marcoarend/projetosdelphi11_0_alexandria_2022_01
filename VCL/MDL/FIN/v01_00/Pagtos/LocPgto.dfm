object FmLocPgto: TFmLocPgto
  Left = 339
  Top = 185
  Caption = 'Pagamentos de Lan'#231'amento'
  ClientHeight = 496
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 448
    Width = 792
    Height = 48
    Align = alBottom
    TabOrder = 0
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 680
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object BtLocaliza1: TBitBtn
      Tag = 22
      Left = 116
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Localizar'
      TabOrder = 2
      OnClick = BtLocaliza1Click
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Caption = 'Pagamentos de Lan'#231'amento'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 790
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object dmkDBGrid1: TdmkDBGrid
    Left = 0
    Top = 48
    Width = 792
    Height = 400
    Align = alClient
    Columns = <
      item
        Expanded = False
        FieldName = 'Data'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Controle'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Descricao'
        Title.Caption = 'Descri'#231#227'o'
        Width = 308
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Credito'
        Title.Caption = 'Cr'#233'dito'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Debito'
        Title.Caption = 'D'#233'bito'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMECLIINT'
        Title.Caption = 'Entidade competente'
        Visible = True
      end>
    Color = clWindow
    DataSource = DmodFin.DsLPE
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = dmkDBGrid1DblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'Data'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Controle'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Descricao'
        Title.Caption = 'Descri'#231#227'o'
        Width = 308
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Credito'
        Title.Caption = 'Cr'#233'dito'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Debito'
        Title.Caption = 'D'#233'bito'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMECLIINT'
        Title.Caption = 'Entidade competente'
        Visible = True
      end>
  end
end
