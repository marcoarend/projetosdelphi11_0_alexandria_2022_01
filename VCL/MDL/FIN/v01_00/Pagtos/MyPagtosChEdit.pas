unit MyPagtosChEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DBCtrls, UnInternalConsts, Db, UMySQLModule,
  UnMLAGeral, dmkGeral, dmkEdit;

type
  TFmMyPagtosChEdit = class(TForm)
    PainelConfirma: TPanel;
    Panel1: TPanel;
    BtConfirma: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    LaValor: TLabel;
    EdValor: TdmkEdit;
    Label1: TLabel;
    EdVencto: TdmkEdit;
    Label2: TLabel;
    EdCheque: TdmkEdit;
    Label3: TLabel;
    EdBanco: TdmkEdit;
    Label4: TLabel;
    EdConta: TdmkEdit;
    Label5: TLabel;
    EdEmitente: TdmkEdit;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    Label6: TLabel;
    EdLancto: TdmkEdit;
    Panel4: TPanel;
    CkContinua: TCheckBox;
    CkLimparDados: TCheckBox;
    LaIncluido: TLabel;
    Timer1: TTimer;
    Label7: TLabel;
    EdControle: TdmkEdit;
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure EdVenctoExit(Sender: TObject);
    procedure EdBancoExit(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmMyPagtosChEdit: TFmMyPagtosChEdit;

implementation

{$R *.DFM}

uses UnMyObjects, Module;

procedure TFmMyPagtosChEdit.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption,
  Image1, PainelTitulo, True, 31);
end;

procedure TFmMyPagtosChEdit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMyPagtosChEdit.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMyPagtosChEdit.EdVenctoExit(Sender: TObject);
var
  Ano, Mes, Dia: Word;
  Texto: String;
  Data: TDateTime;
begin
  if Length(EdVencto.Text) <> 0 then
  begin
    try
      DecodeDate(Date, Ano, Mes, Dia);
      Texto := Geral.SoNumero_TT(EdVencto.Text);
      if Length(Texto) in ([4,6]) then
      begin
        Dia := StrToInt(Texto[1]+Texto[2]);
        Mes := StrToInt(Texto[3]+Texto[4]);
      end;
      if Length(Texto) in ([6]) then
      begin
        Ano := StrToInt(Texto[5]+Texto[6]);
      end;
      Data := EncodeDate(Ano, Mes, Dia);
      if (Data < (Date)) and (Length(Texto)=4) then
      begin
        if ((Ano/4) = int(Ano/4)) and (Mes=2) and (Dia=29) then
        begin
          Mes := 3;
          Dia := 1;
        end;
        Ano := Ano + 1;
        Data := EncodeDate(Ano, Mes, Dia);
      end;
      DecodeDate(Data, Ano, Mes, Dia);
      EdVencto.Text := FormatFloat('00', Dia) + '/' +FormatFloat('00', Mes) +
      '/' +FormatFloat('00', Ano);
    except
      EdVencto.Text := '';
      EdVencto.SetFocus;
      Application.MessageBox('Data inv�lida!', 'Erro', MB_OK+MB_ICONERROR);
    end;
  end;
end;

procedure TFmMyPagtosChEdit.EdBancoExit(Sender: TObject);
begin
  EdBanco.Text := FormatFloat('000', Geral.IMV(EdBanco.Text));
end;

procedure TFmMyPagtosChEdit.BtConfirmaClick(Sender: TObject);
var
  Valor: Double;
  Lancto, Controle: Int64;
  Data: String;
begin
  Valor := Geral.DMV(EdValor.Text);
  if Valor <= 0 then
  begin
    Application.MessageBox('Defina o valor!', 'Erro', MB_OK+MB_ICONERROR);
    EdValor.SetFocus;
    Exit;
  end;
  try
    Data := FormatDateTime(VAR_FORMATDATE, StrToDate(EdVencto.Text));
  except
    Application.MessageBox('Data inv�lida!', 'Erro', MB_OK+MB_ICONERROR);
    EdVencto.SetFocus;
    Exit;
  end;
  Lancto := Geral.IMV(EdLancto.Text);
  //
  Dmod.QrUpd.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpd.SQL.Add('INSERT INTO mediach SET ')
  else Dmod.QrUpd.SQL.Add('UPDATE mediach SET ');
  Dmod.QrUpd.SQL.Add('Valor      =:P00');
  Dmod.QrUpd.SQL.Add(',Cheque    =:P01');
  Dmod.QrUpd.SQL.Add(',Banco     =:P02');
  Dmod.QrUpd.SQL.Add(',Conta     =:P03');
  Dmod.QrUpd.SQL.Add(',Nome      =:P04');
  Dmod.QrUpd.SQL.Add(',Vencto    =:P05');
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Controle := UMyMod.BuscaEmLivreInt64Y(Dmod.MyDB, 'Livres', 'Controle',
    'MediaCh', 'MediaCh', 'Codigo');
    Dmod.QrUpd.SQL.Add(',Codigo  =:Px, Controle=:Py')
  end else begin
    Controle := Geral.IMV(EdControle.Text);
    Dmod.QrUpd.SQL.Add('WHERE Codigo =:Px AND Controle=:Py');
  end;
  Dmod.QrUpd.Params[00].AsFloat   := Valor;
  Dmod.QrUpd.Params[01].AsString  := EdCheque.Text;
  Dmod.QrUpd.Params[02].AsString  := EdBanco.Text;
  Dmod.QrUpd.Params[03].AsString  := EdConta.Text;
  Dmod.QrUpd.Params[04].AsString  := EdEmitente.Text;
  Dmod.QrUpd.Params[05].AsString  := Data;
  Dmod.QrUpd.Params[06].AsFloat   := Lancto;
  Dmod.QrUpd.Params[07].AsFloat   := Controle;
  Dmod.QrUpd.ExecSQL;
  if CkContinua.Checked then
  begin
    if CkLimparDados.Checked then
    begin
      LaTipo.Caption := CO_INCLUSAO;
      EdValor.Text    := '';
      EdVencto.Text   := '';
      EdCheque.Text   := '';
      EdBanco.Text    := '';
      EdConta.Text    := '';
      EdEmitente.Text := '';
      EdControle.Text := '0';
    end;
    LaIncluido.Visible := True;
    Timer1.Enabled := True;
    EdValor.SetFocus;
  end else Close;
end;

procedure TFmMyPagtosChEdit.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  LaIncluido.Visible := False;
end;

end.
