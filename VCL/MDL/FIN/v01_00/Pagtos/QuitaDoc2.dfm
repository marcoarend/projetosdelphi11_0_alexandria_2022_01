object FmQuitaDoc2: TFmQuitaDoc2
  Left = 472
  Top = 219
  Caption = 'FIN-PGTOS-001 :: Quita'#231#227'o de Documento 2'
  ClientHeight = 335
  ClientWidth = 514
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 514
    Height = 239
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Label1: TLabel
      Left = 200
      Top = 144
      Width = 85
      Height = 13
      Caption = 'Data da quita'#231#227'o:'
    end
    object Label2: TLabel
      Left = 112
      Top = 144
      Width = 58
      Height = 13
      Caption = 'Documento:'
    end
    object Label3: TLabel
      Left = 48
      Top = 12
      Width = 62
      Height = 13
      Caption = 'Lan'#231'amento:'
    end
    object Label4: TLabel
      Left = 48
      Top = 56
      Width = 39
      Height = 13
      Caption = 'Carteira:'
    end
    object Label5: TLabel
      Left = 201
      Top = 12
      Width = 36
      Height = 13
      Caption = 'Cr'#233'dito:'
    end
    object Label6: TLabel
      Left = 301
      Top = 12
      Width = 34
      Height = 13
      Caption = 'D'#233'bito:'
    end
    object Label7: TLabel
      Left = 48
      Top = 100
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object Label9: TLabel
      Left = 397
      Top = 12
      Width = 53
      Height = 13
      Caption = 'Nota fiscal:'
    end
    object Label8: TLabel
      Left = 48
      Top = 144
      Width = 27
      Height = 13
      Caption = 'S'#233'rie:'
    end
    object Label10: TLabel
      Left = 255
      Top = 188
      Width = 36
      Height = 13
      Caption = '$ Valor:'
    end
    object Label11: TLabel
      Left = 48
      Top = 188
      Width = 38
      Height = 13
      Caption = '$ Multa:'
    end
    object Label12: TLabel
      Left = 151
      Top = 188
      Width = 37
      Height = 13
      Caption = '$ Juros:'
    end
    object Label14: TLabel
      Left = 298
      Top = 144
      Width = 40
      Height = 13
      Caption = '% Multa:'
    end
    object Label15: TLabel
      Left = 382
      Top = 144
      Width = 39
      Height = 13
      Caption = '% Juros:'
    end
    object TPData1: TdmkEditDateTimePicker
      Left = 200
      Top = 160
      Width = 93
      Height = 21
      Date = 37619.114129247700000000
      Time = 37619.114129247700000000
      TabOrder = 10
      OnClick = TPData1Click
      OnChange = TPData1Change
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object EdDoc: TdmkEdit
      Left = 112
      Top = 160
      Width = 81
      Height = 21
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 6
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdDescricao: TdmkEdit
      Left = 48
      Top = 116
      Width = 414
      Height = 21
      TabOrder = 7
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdSerieCH: TdmkEdit
      Left = 48
      Top = 160
      Width = 59
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 8
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdValor: TdmkEdit
      Left = 255
      Top = 204
      Width = 105
      Height = 21
      Alignment = taRightJustify
      TabOrder = 15
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdMultaVal: TdmkEdit
      Left = 48
      Top = 204
      Width = 100
      Height = 21
      Alignment = taRightJustify
      TabOrder = 13
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdMultaValExit
    end
    object EdMoraVal: TdmkEdit
      Left = 151
      Top = 204
      Width = 100
      Height = 21
      Alignment = taRightJustify
      TabOrder = 14
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdMoraValExit
    end
    object EdMulta: TdmkEdit
      Left = 298
      Top = 160
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 11
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdMultaExit
    end
    object EdMoraDia: TdmkEdit
      Left = 382
      Top = 160
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 12
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnExit = EdMoraDiaExit
    end
    object DBEdit1: TDBEdit
      Left = 201
      Top = 28
      Width = 96
      Height = 21
      Color = clInactiveCaption
      DataField = 'Credito'
      DataSource = DsLancto1
      TabOrder = 2
    end
    object DBEdit2: TDBEdit
      Left = 301
      Top = 28
      Width = 92
      Height = 21
      Color = clInactiveCaption
      DataField = 'Debito'
      DataSource = DsLancto1
      TabOrder = 3
    end
    object EdLancto: TdmkEdit
      Left = 48
      Top = 28
      Width = 121
      Height = 21
      Alignment = taRightJustify
      Color = clInactiveCaption
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdSub: TdmkEdit
      Left = 169
      Top = 28
      Width = 28
      Height = 21
      Alignment = taRightJustify
      Color = clInactiveCaption
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object DBEdit3: TDBEdit
      Left = 48
      Top = 72
      Width = 60
      Height = 21
      Color = clInactiveCaption
      DataField = 'Carteira'
      DataSource = DsLancto1
      TabOrder = 5
    end
    object DBEdit4: TDBEdit
      Left = 108
      Top = 72
      Width = 354
      Height = 21
      Color = clInactiveCaption
      DataField = 'NOMECARTEIRA'
      DataSource = DsLancto1
      TabOrder = 6
    end
    object DBEdit5: TDBEdit
      Left = 397
      Top = 28
      Width = 65
      Height = 21
      Color = clInactiveCaption
      DataField = 'NotaFiscal'
      DataSource = DsLancto1
      TabOrder = 4
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 287
    Width = 514
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 48
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object BtDesiste: TBitBtn
      Tag = 15
      Left = 372
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 514
    Height = 48
    Align = alTop
    Caption = 'Quita'#231#227'o de Documento 2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 512
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 330
      ExplicitHeight = 44
    end
  end
  object QrLancto1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT la.Controle, la.Sub, la.Descricao, la.Credito, la.Debito,'
      'la.NotaFiscal, la.Documento, la.Vencimento, la.Carteira, Data,'
      'ca.Nome NOMECARTEIRA, ca.Tipo TIPOCARTEIRA ,'
      'ca.Banco CARTEIRABANCO, la.Cliente, la.Fornecedor, '
      'la.DataDoc, la.Nivel, la.CtrlIni, la.Vendedor, la.Account,'
      
        'la.Genero, la.Mez, la.Duplicata, la.Doc2, la.SerieCH, la.MoraDia' +
        ','
      
        'la.Multa, la.ICMS_P, la.ICMS_V, la.CliInt, la.Depto, la.DescoPor' +
        ','
      'la.ForneceI, la.Unidade, la.Qtde, la.FatID, la.FatID_Sub, '
      'la.FatNum, la.DescoVal, la.NFVal, la.FatParcela'
      'FROM lanctos la, Carteiras ca'
      'WHERE la.Controle=:P0'
      'AND la.Sub=:P1'
      'AND ca.Codigo=la.Carteira')
    Left = 328
    Top = 80
    ParamData = <
      item
        DataType = ftFloat
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLancto1Sub: TSmallintField
      FieldName = 'Sub'
      Origin = 'DBMMONEY.lanctos.Sub'
    end
    object QrLancto1Descricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'DBMMONEY.lanctos.Descricao'
      Size = 128
    end
    object QrLancto1Credito: TFloatField
      FieldName = 'Credito'
      Origin = 'DBMMONEY.lanctos.Credito'
      DisplayFormat = '#,###,##0.00  '
    end
    object QrLancto1Debito: TFloatField
      FieldName = 'Debito'
      Origin = 'DBMMONEY.lanctos.Debito'
      DisplayFormat = '#,###,##0.00  '
    end
    object QrLancto1NotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'DBMMONEY.lanctos.NotaFiscal'
    end
    object QrLancto1Documento: TFloatField
      FieldName = 'Documento'
      Origin = 'DBMMONEY.lanctos.Documento'
    end
    object QrLancto1Vencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'DBMMONEY.lanctos.Vencimento'
    end
    object QrLancto1Carteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'DBMMONEY.lanctos.Carteira'
    end
    object QrLancto1Data: TDateField
      FieldName = 'Data'
      Origin = 'DBMMONEY.lanctos.Data'
    end
    object QrLancto1NOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
    object QrLancto1TIPOCARTEIRA: TIntegerField
      FieldName = 'TIPOCARTEIRA'
      Origin = 'DBMMONEY.carteiras.Tipo'
    end
    object QrLancto1CARTEIRABANCO: TIntegerField
      FieldName = 'CARTEIRABANCO'
      Origin = 'DBMMONEY.carteiras.Banco'
    end
    object QrLancto1Cliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'DBMMONEY.lanctos.Cliente'
    end
    object QrLancto1Fornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Origin = 'DBMMONEY.lanctos.Fornecedor'
    end
    object QrLancto1DataDoc: TDateField
      FieldName = 'DataDoc'
      Origin = 'DBMMONEY.lanctos.DataDoc'
    end
    object QrLancto1Nivel: TIntegerField
      FieldName = 'Nivel'
      Origin = 'DBMMONEY.lanctos.Nivel'
    end
    object QrLancto1Vendedor: TIntegerField
      FieldName = 'Vendedor'
      Origin = 'DBMMONEY.lanctos.Vendedor'
    end
    object QrLancto1Account: TIntegerField
      FieldName = 'Account'
      Origin = 'DBMMONEY.lanctos.Account'
    end
    object QrLancto1Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLancto1CtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrLancto1Genero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLancto1Mez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrLancto1Duplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrLancto1Doc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrLancto1SerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrLancto1MoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLancto1Multa: TFloatField
      FieldName = 'Multa'
    end
    object QrLancto1ICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrLancto1ICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrLancto1CliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLancto1Depto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLancto1DescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrLancto1ForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLancto1Unidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrLancto1Qtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrLancto1FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLancto1FatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLancto1DescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrLancto1NFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrLancto1FatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLancto1FatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object DsLancto1: TDataSource
    DataSet = QrLancto1
    Left = 356
    Top = 80
  end
  object QrLancto2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Tipo, Credito, Debito, Controle, Sub, Carteira'
      'FROM lanctos '
      'WHERE ID_Pgto=:P0 '
      'AND ID_Sub=:P1')
    Left = 328
    Top = 108
    ParamData = <
      item
        DataType = ftFloat
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLancto2Tipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'DBMMONEY.lanctos.Tipo'
    end
    object QrLancto2Credito: TFloatField
      FieldName = 'Credito'
      Origin = 'DBMMONEY.lanctos.Credito'
    end
    object QrLancto2Debito: TFloatField
      FieldName = 'Debito'
      Origin = 'DBMMONEY.lanctos.Debito'
    end
    object QrLancto2Sub: TSmallintField
      FieldName = 'Sub'
      Origin = 'DBMMONEY.lanctos.Sub'
    end
    object QrLancto2Carteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'DBMMONEY.lanctos.Carteira'
    end
    object QrLancto2Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsLancto2: TDataSource
    DataSet = QrLancto2
    Left = 356
    Top = 108
  end
end
