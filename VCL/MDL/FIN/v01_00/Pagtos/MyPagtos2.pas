unit MyPagtos2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, DBCtrls, Db, (*DBTables,*) UnMsgInt, ComCtrls, Buttons, Mask,
  UCreate, UMySQLModule, UnInternalConsts, UnInternalConsts2, UnInternalConsts3,
  UnMLAGeral, Grids, DBGrids, mySQLDbTables, Variants, dmkGeral,
  //UCashier,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkEditDateTimePicker, dmkLabel,
  dmkMemo, UnDmkEnums;

type
  TFmMyPagtos2 = class(TForm)
    DsCarteiras: TDataSource;
    DsCredor: TDataSource;
    DsDevedor: TDataSource;
    Panel1: TPanel;
    PainelDados: TPanel;
    LaParcela: TLabel;
    Label1: TLabel;
    LaValor: TLabel;
    LaDocumento: TLabel;
    LaVencimento: TLabel;
    LaCredor: TLabel;
    LaDevedor: TLabel;
    LaMoraDia: TLabel;
    LaMulta: TLabel;
    EdValor: TdmkEdit;
    EdDocumento: TdmkEdit;
    TPVencimento: TdmkEditDateTimePicker;
    EdCredor: TdmkEditCB;
    CBCredor: TdmkDBLookupComboBox;
    EdDevedor: TdmkEditCB;
    CBDevedor: TdmkDBLookupComboBox;
    EdMoraDia: TdmkEdit;
    EdMulta: TdmkEdit;
    PainelDados2: TPanel;
    TbParcpagtos: TmySQLTable;
    DsParcPagtos: TDataSource;
    TbParcpagtosParcela: TIntegerField;
    TbParcpagtosData: TDateField;
    TbParcpagtosCredito: TFloatField;
    TbParcpagtosDebito: TFloatField;
    TbParcpagtosDoc: TLargeintField;
    QrCarteiras: TmySQLQuery;
    QrTerceiro: TmySQLQuery;
    QrSoma: TmySQLQuery;
    QrTerceiroCodigo: TIntegerField;
    QrTerceiroRazaoSocial: TWideStringField;
    QrTerceiroFantasia: TWideStringField;
    QrTerceiroRespons1: TWideStringField;
    QrTerceiroRespons2: TWideStringField;
    QrTerceiroPai: TWideStringField;
    QrTerceiroMae: TWideStringField;
    QrTerceiroCNPJ: TWideStringField;
    QrTerceiroIE: TWideStringField;
    QrTerceiroNome: TWideStringField;
    QrTerceiroApelido: TWideStringField;
    QrTerceiroCPF: TWideStringField;
    QrTerceiroRG: TWideStringField;
    QrTerceiroERua: TWideStringField;
    QrTerceiroECompl: TWideStringField;
    QrTerceiroEBairro: TWideStringField;
    QrTerceiroECIDADE: TWideStringField;
    QrTerceiroEUF: TSmallintField;
    QrTerceiroECEP: TIntegerField;
    QrTerceiroEPais: TWideStringField;
    QrTerceiroETe1: TWideStringField;
    QrTerceiroETe2: TWideStringField;
    QrTerceiroETe3: TWideStringField;
    QrTerceiroECel: TWideStringField;
    QrTerceiroEFax: TWideStringField;
    QrTerceiroEEMail: TWideStringField;
    QrTerceiroEContato: TWideStringField;
    QrTerceiroENatal: TDateField;
    QrTerceiroPRua: TWideStringField;
    QrTerceiroPCompl: TWideStringField;
    QrTerceiroPBairro: TWideStringField;
    QrTerceiroPCIDADE: TWideStringField;
    QrTerceiroPUF: TSmallintField;
    QrTerceiroPCEP: TIntegerField;
    QrTerceiroPPais: TWideStringField;
    QrTerceiroPTe1: TWideStringField;
    QrTerceiroPTe2: TWideStringField;
    QrTerceiroPTe3: TWideStringField;
    QrTerceiroPCel: TWideStringField;
    QrTerceiroPFax: TWideStringField;
    QrTerceiroPEMail: TWideStringField;
    QrTerceiroPContato: TWideStringField;
    QrTerceiroPNatal: TDateField;
    QrTerceiroSexo: TWideStringField;
    QrTerceiroResponsavel: TWideStringField;
    QrTerceiroProfissao: TWideStringField;
    QrTerceiroCargo: TWideStringField;
    QrTerceiroRecibo: TSmallintField;
    QrTerceiroDiaRecibo: TSmallintField;
    QrTerceiroAjudaEmpV: TFloatField;
    QrTerceiroAjudaEmpP: TFloatField;
    QrTerceiroCliente1: TWideStringField;
    QrTerceiroCliente2: TWideStringField;
    QrTerceiroFornece1: TWideStringField;
    QrTerceiroFornece2: TWideStringField;
    QrTerceiroFornece3: TWideStringField;
    QrTerceiroFornece4: TWideStringField;
    QrTerceiroTerceiro: TWideStringField;
    QrTerceiroCadastro: TDateField;
    QrTerceiroInformacoes: TWideStringField;
    QrTerceiroLogo: TBlobField;
    QrTerceiroVeiculo: TIntegerField;
    QrTerceiroMensal: TWideStringField;
    QrTerceiroObservacoes: TWideMemoField;
    QrTerceiroTipo: TSmallintField;
    QrTerceiroLk: TIntegerField;
    QrTerceiroGrupo: TIntegerField;
    QrTerceiroDataCad: TDateField;
    QrTerceiroDataAlt: TDateField;
    QrTerceiroUserCad: TSmallintField;
    QrTerceiroUserAlt: TSmallintField;
    QrTerceiroCRua: TWideStringField;
    QrTerceiroCCompl: TWideStringField;
    QrTerceiroCBairro: TWideStringField;
    QrTerceiroCCIDADE: TWideStringField;
    QrTerceiroCUF: TSmallintField;
    QrTerceiroCCEP: TIntegerField;
    QrTerceiroCPais: TWideStringField;
    QrTerceiroCTel: TWideStringField;
    QrTerceiroCFax: TWideStringField;
    QrTerceiroCCel: TWideStringField;
    QrTerceiroCContato: TWideStringField;
    QrTerceiroLRua: TWideStringField;
    QrTerceiroLCompl: TWideStringField;
    QrTerceiroLBairro: TWideStringField;
    QrTerceiroLCIDADE: TWideStringField;
    QrTerceiroLUF: TSmallintField;
    QrTerceiroLCEP: TIntegerField;
    QrTerceiroLPais: TWideStringField;
    QrTerceiroLTel: TWideStringField;
    QrTerceiroLFax: TWideStringField;
    QrTerceiroLCel: TWideStringField;
    QrTerceiroLContato: TWideStringField;
    QrTerceiroComissao: TFloatField;
    QrTerceiroSituacao: TSmallintField;
    QrTerceiroNivel: TWideStringField;
    QrTerceiroNOMEUFP: TWideStringField;
    QrTerceiroNOMEUFE: TWideStringField;
    QrSomaVALOR: TFloatField;
    QrCredor: TmySQLQuery;
    QrDevedor: TmySQLQuery;
    QrCredorCodigo: TIntegerField;
    QrCredorNOMEENTIDADE: TWideStringField;
    QrDevedorCodigo: TIntegerField;
    QrDevedorNOMEENTIDADE: TWideStringField;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    Panel3: TPanel;
    Label12: TLabel;
    EdTrocoVal: TdmkEdit;
    Label13: TLabel;
    EdTrocoPara: TdmkEdit;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    DBGParcelas: TDBGrid;
    Panel4: TPanel;
    GBParcelamento: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    RGArredondar: TRadioGroup;
    EdParcelas: TdmkEdit;
    RGPeriodo: TRadioGroup;
    EdDias: TdmkEdit;
    RGIncremento: TRadioGroup;
    EdParcela1: TdmkEdit;
    EdParcelaX: TdmkEdit;
    CkArredondar: TCheckBox;
    Label10: TLabel;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrCliIntCodigo: TIntegerField;
    QrCliIntNOMEENTIDADE: TWideStringField;
    CkParcelamento: TCheckBox;
    EdSoma: TdmkEdit;
    Label11: TLabel;
    TabSheet1: TTabSheet;
    QrDevedorCNPJ_CPF: TWideStringField;
    QrDevedorBanco: TSmallintField;
    QrDevedorAgencia: TWideStringField;
    QrDevedorContaCorrente: TWideStringField;
    QrDevedorCNPJ_CPF_TXT: TWideStringField;
    EdNotaFiscal: TdmkEdit;
    Label2: TLabel;
    EdDuplicata: TdmkEdit;
    Label20: TLabel;
    QrTerceiroENumero: TIntegerField;
    QrTerceiroPNumero: TIntegerField;
    Label21: TLabel;
    EdGenero: TdmkEditCB;
    CBGenero: TdmkDBLookupComboBox;
    QrGeneros: TmySQLQuery;
    DsGeneros: TDataSource;
    QrGenerosCodigo: TIntegerField;
    QrGenerosNome: TWideStringField;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasTipo: TIntegerField;
    EdSerieCH: TdmkEdit;
    Label22: TLabel;
    DBRadioGroup1: TDBRadioGroup;
    QrCarteirasForneceI: TIntegerField;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    EdCodigo: TdmkEdit;
    Panel5: TPanel;
    Label14: TLabel;
    TPData: TdmkEditDateTimePicker;
    GBCheque: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    EdNome: TdmkEdit;
    EdBanco: TdmkEdit;
    EdAgencia: TdmkEdit;
    EdConta: TdmkEdit;
    EdCNPJCPF: TdmkEdit;
    TabSheet3: TTabSheet;
    Panel6: TPanel;
    GBRecibo: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    EdPessoal: TdmkEdit;
    EdEmpresa: TdmkEdit;
    EdPorcento: TdmkEdit;
    EdRecibo: TdmkEdit;
    EdBaseRecibo: TdmkEdit;
    RGRecibo: TRadioGroup;
    EdDiaRecibo: TdmkEdit;
    CkRecibo: TCheckBox;
    TbParcpagtosSerieCH: TWideStringField;
    EdDescricao: TdmkEdit;
    Label23: TLabel;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    CkContinuar: TCheckBox;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    procedure RGTipo_Click(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdValorExit(Sender: TObject);
    procedure EdDocumentoExit(Sender: TObject);
    procedure EdCredorChange(Sender: TObject);
    procedure CkReciboClick(Sender: TObject);
    procedure EdPorcentoExit(Sender: TObject);
    procedure EdEmpresaExit(Sender: TObject);
    procedure EdPessoalExit(Sender: TObject);
    procedure EdPessoalEnter(Sender: TObject);
    procedure EdEmpresaEnter(Sender: TObject);
    procedure EdPorcentoEnter(Sender: TObject);
    procedure EdPorcentoChange(Sender: TObject);
    procedure EdDiaReciboExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdMoraDiaExit(Sender: TObject);
    procedure EdMultaExit(Sender: TObject);
    procedure EdMultaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdMoraDiaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RGPeriodoClick(Sender: TObject);
    procedure EdDiasExit(Sender: TObject);
    procedure EdParcelasExit(Sender: TObject);
    procedure CkParcelamentoClick(Sender: TObject);
    procedure EdDocumentoChange(Sender: TObject);
    procedure CkArredondarClick(Sender: TObject);
    procedure RGArredondarClick(Sender: TObject);
    procedure TbParcpagtosBeforeOpen(DataSet: TDataSet);
    procedure TPVencimentoExit(Sender: TObject);
    procedure TbParcpagtosAfterPost(DataSet: TDataSet);
    procedure RGIncrementoClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdTrocoParaChange(Sender: TObject);
    procedure EdValorChange(Sender: TObject);
    procedure RGReciboClick(Sender: TObject);
    procedure EdCNPJCPFExit(Sender: TObject);
    procedure EdBancoExit(Sender: TObject);
    procedure EdDevedorChange(Sender: TObject);
    procedure EdNomeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrDevedorCalcFields(DataSet: TDataSet);
    procedure DBGParcelasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNotaFiscalExit(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure QrCarteirasAfterScroll(DataSet: TDataSet);
    procedure SpeedButton3Click(Sender: TObject);
    procedure EdCodigoChange(Sender: TObject);
  private
    { Private declarations }
    FParcPagtos, FNome, FCNPJCPF, FConta: String;
    FBanco, FAgencia: Integer;
    function CalculaRecibos(AvisaErro: Boolean): Boolean;

    procedure InsereParcela(Parcela: Integer; Vencimento: String);
    procedure CalculaTroco;
    procedure ReopenCarteiras();
  public
    FAtivou: Boolean;
    FFatIDSub,
    FFatIDIts: Integer;
    FUsaMinMax: TMinMax;
    FValMin: Double;
    FValMax: Double;
    //
    FTitulo: String;
    { Public declarations }
    procedure CalculaParcelas;
  end;

var
  FmMyPagtos2: TFmMyPagtos2;

implementation

uses UnMyObjects, Module, Recibos, Principal, UnGOTOy, UnFinanceiro, MyListas,
ModuleGeral;

var
  EdGetFocus: Integer;
  PorcentoFocused: Boolean;
  CriandoForm: Boolean;
  Pagto_Doc: Double;

{$R *.DFM}

procedure TFmMyPagtos2.RGTipo_Click(Sender: TObject);
begin
  //QrCarteiras.Close;
  //QrCarteiras.Params[0].AsInteger := RGTipo.ItemIndex;
  //QrCarteiras.Open;
  ReopenCarteiras;
  //CBCarteira.KeyValue := null;
  //EdCarteira.Text := '';
end;

procedure TFmMyPagtos2.BtDesisteClick(Sender: TObject);
begin
  VAR_FATPARCELA := Geral.IMV(EdCodigo.Text);
  VAR_PGTOVALOR := 0;
  Close;
end;

procedure TFmMyPagtos2.BtConfirmaClick(Sender: TObject);
var
  Parcela: Integer;
  Vencimento: String;
begin
  //acertar recibo
  if ((CBCarteira.KeyValue = 0) and (CBCarteira.Text = ''))
  or (CBCarteira.KeyValue = Null) then
  begin
    Application.MessageBox(PChar(VAR_MSG_DEFCARTEIRA), 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  if (CBCarteira.KeyValue = 0) or (CBCarteira.KeyValue = Null) then
  begin
    Application.MessageBox(PChar(VAR_MSG_DEFCARTEIRA), 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  if CBCredor.KeyValue  = Null then CBCredor.KeyValue  := 0;
  if CBDevedor.KeyValue = Null then CBDevedor.KeyValue := 0;
  if (CBCliInt.KeyValue = 0) or (CBCliInt.KeyValue = Null) then
  begin
    //Application.MessageBox('Defina o cliente interno', 'Erro', MB_OK+MB_ICONERROR);
    //Exit;
    //CliInt := Dmod.QrMasterDono.Value;
  end else ;//CliInt := CBCliInt.KeyValue;
  Parcela := Geral.IMV(EdCodigo.Text);
  Vencimento := FormatDateTime(VAR_FORMATDATE, TPVencimento.Date);
  if GBCheque.Visible then
  begin
    FNome    := EdNome.Text;
    FCNPJCPF := MLAGeral.FormataCNPJ_TFT(EdCNPJCPF.Text);
    FAgencia := Geral.IMV(EdAgencia.Text);
    FConta   := EdConta.Text;
    FBanco   := Geral.IMV(EdBanco.Text);
  end else begin
    FNome    := '';
    FCNPJCPF := '';
    FAgencia := 0;
    FConta   := '';
    FBanco   := 0;
  end;

  if CkParcelamento.Checked then
  begin
    if Geral.IMV(EdParcelas.Text) < 2 then
    begin
      Application.MessageBox('N�mero de parcelas deve ser no m�nimo duas.',
      'Parcelamento Autom�tico', MB_OK+MB_ICONERROR);
      EdParcelas.SetFocus;
      Exit;
    end;
    if Geral.DMV(EdSoma.Text) <> Geral.DMV(EdValor.Text) then
    begin
      Application.MessageBox('Valor das parcelas n�o confere com o total!.',
      'Parcelamento Autom�tico', MB_OK+MB_ICONERROR);
      EdValor.SetFocus;
      Exit;
    end;
    TbParcpagtos.DisableControls;
    TbParcpagtos.First;
    while not TbParcpagtos.Eof do
    begin
      InsereParcela(TbParcpagtosParcela.Value,
      FormatDateTime(VAR_FORMATDATE, TbParcpagtosData.Value));
      TbParcpagtos.Next;
    end;
    TbParcpagtos.EnableControls;
  end else InsereParcela(Parcela, Vencimento);
  VAR_FATPARCELA := Geral.IMV(EdCodigo.Text);
  VAR_PGTOVALOR := Geral.DMV(EdValor.Text);
  if GBCheque.Visible then
  begin
    if (QrDevedorBanco.Value <> Geral.IMV(EdBanco.Text)) or
    (QrDevedorAgencia.Value <> EdAgencia.Text) or
    (QrDevedorContaCorrente.Value <> EdConta.text) then
    begin
      if Application.MessageBox(PChar('Deseja atualizar os dados de banco, '+
      'ag�ncia e conta no cadastro do devedor?'), 'Pergunta', MB_YESNOCANCEL+
      MB_ICONQUESTION) = ID_YES then
      begin
        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add('UPDATE entidades SET AlterWeb=1, Banco=:P0, Agencia=:P1,');
        Dmod.QrUpdM.SQL.Add('ContaCorrente=:P2 WHERE Codigo=:P3');
        Dmod.QrUpdM.Params[0].AsInteger := Geral.IMV(EdBanco.Text);
        Dmod.QrUpdM.Params[1].AsString  := EdAgencia.Text;
        Dmod.QrUpdM.Params[2].AsString  := EdConta.Text;
        Dmod.QrUpdM.Params[3].AsInteger := Geral.IMV(EdDevedor.Text);
        Dmod.QrUpdM.ExecSQL;
        QrDevedor.Close;
        QrDevedor.Open;
      end;  
    end;
  end;
  if not CkContinuar.Checked then Close;
end;

procedure TFmMyPagtos2.FormActivate(Sender: TObject);
begin
  {
  if (not FAtivou) and (LaTipo.Caption = CO_INCLUSAO) then
  begin
    if VAR_MYPAGTOSCONFIG = 1 then
    begin
      if Dmod.QrControleVendaParcPg.Value > 1 then
      begin
        CkParcelamento.Checked := True;
        EdParcelas.Text        := IntToStr(Dmod.QrControleVendaParcPg.Value);
        RGPeriodo.ItemIndex    := Dmod.QrControleVendaPeriPg.Value;
        EdDias.Text            := IntToStr(Dmod.QrControleVendaDiasPg.Value);
        //RGTipo.ItemIndex       := GOTOy.VerificaTipoDaCarteira(
                                  //Dmod.QrControleVendaCartPg.Value);
        EdCarteira.Text        := IntToStr(Dmod.QrControleVendaCartPg.Value);
        CBCarteira.KeyValue    := Dmod.QrControleVendaCartPg.Value;
        CalculaParcelas;
      end;
    end else begin
      CkParcelamento.Checked := Dmod.QrControleMyPgParc.Value > 0;
      EdParcelas.Text := IntToStr(Dmod.QrControleMyPgQtdP.Value);
      RGPeriodo.ItemIndex := Dmod.QrControleMyPgPeri.Value;
      EdDias.Text := IntToStr(Dmod.QrControleMyPgDias.Value);
      CalculaParcelas;
    end;
  end;
  }
  if RGRecibo.ItemIndex = 2 then
   CkRecibo.Visible := True;
  MyObjects.CorIniComponente();
  EdMoraDia.SetFocus;
  EdCarteira.SetFocus;
  //QrCarteiras.Close;
  //QrCarteiras.Params[0].AsInteger := RGTipo.ItemIndex;
  //QrCarteiras.Open;
  ReopenCarteiras;
  PorcentoFocused := False;
  EdGetFocus := 0;
  Refresh;
  CriandoForm := False;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    CkParcelamento.Visible := True;
    CkContinuar.Visible := True;
  end;
  if VAR_CARTEIRADESPESAS <> 0 then  EdValor.SetFocus;
  FAtivou := True;
end;

procedure TFmMyPagtos2.FormCreate(Sender: TObject);
begin
  FTitulo := 'Pagamentos Diversos';
  QrCarteiras.Database  := VAR_MyBDFINANCAS;
  QrCredor.Database     := VAR_MyBDFINANCAS;
  QrDevedor.Database    := VAR_MyBDFINANCAS;
  QrTerceiro.Database   := VAR_MyBDFINANCAS;
  QrGeneros.Database    := VAR_MyBDFINANCAS;
  QrSoma.Database       := DModG.MyPID_DB;
  TbParcpagtos.Database := DModG.MyPID_DB;
  //
  CriandoForm := True;
  TPVencimento.Date := IC3_ED_Vencto;
  EdDocumento.Text := FloatToStr(IC3_ED_Doc);
  if LaValor.Caption <> CO_CREDITO then LaValor.Caption := CO_DEBITO;
  QrCredor.Open;
  QrDevedor.Open;
  QrCliInt.Open;
  //
  {
  LaDevedor.Top := 112;
  EdDevedor.Top := 128;
  CBDevedor.Top := 128;
  LaMoraDia.Top := 152;
  LaMulta.Top   := 152;
  EdMoraDia.Top := 168;
  EdMulta.Top   := 168;
  }
  //
  RGRecibo.ItemIndex := IC3_ED_RECIBO;
  EdDiaRecibo.Text   := IntToStr(IC3_ED_DIARECIBO);
  EdEmpresa.Text := Geral.TFT(FloatToStr(IC3_ED_VALEMPRESAV), 2, siPositivo);
  EdPorcento.Text := Geral.TFT(FloatToStr(IC3_ED_VALEMPRESAP), 4, siPositivo);
  //
  FParcPagtos := UCriar.RecriaTempTable('parcpagtos', DmodG.QrUpdPID1, False);
  //
  TbParcpagtos.Open;
  //
  if VAR_CARTEIRADESPESAS <> 0 then
  begin
    //RGTipo.ItemIndex    := VAR_CARTTIPODESPESAS;
    EdCarteira.Text     := IntToStr(VAR_CARTEIRADESPESAS);
    CBCarteira.KeyValue := VAR_CARTEIRADESPESAS;
  end else begin
    //RGTipo.ItemIndex    := Dmod.QrControleMyPagTip.Value;
    EdCarteira.Text     := IntToStr(Dmod.QrControleMyPagCar.Value);
    CBCarteira.KeyValue := Dmod.QrControleMyPagCar.Value;
  end;
  //
  TPData.Date := IC3_ED_Data;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmMyPagtos2.EdValorExit(Sender: TObject);
var
  Valor: Double;
begin
  if FUsaMinMax = mmAmbos then
     EdValor.Text := MLAGeral.TFT_MinMax(EdValor.Text, FValMin, FValMax, 2, siPositivo)
  else EdValor.Text := Geral.TFT(EdValor.Text, 2, siPositivo);
  if FUsaMinMax = mmMinimo then
  begin
    Valor := Geral.DMV(EdValor.Text);
    if Valor < FValMin then
    begin
      Application.MessageBox('Valor abaixo do m�nimo permitido!', 'Erro', MB_OK+MB_ICONERROR);
      EdValor.Text := FormatFloat('#,###,##0.00', FValMin);
    end;
  end;
  if FUsaMinMax = mmMaximo then
  begin
    Valor := Geral.DMV(EdValor.Text);
    if Valor > FValMax then
    begin
      Application.MessageBox('Valor acima do m�ximo permitido!', 'Erro', MB_OK+MB_ICONERROR);
      EdValor.Text := FormatFloat('#,###,##0.00', FValMax);
    end;
  end;
  CalculaRecibos(True);
  CalculaParcelas;
end;

procedure TFmMyPagtos2.EdDocumentoExit(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmMyPagtos2.EdCredorChange(Sender: TObject);
begin
  if (EdNome.Text = '') and (CBCredor.Text <> '')
  then EdNome.Text := CBCredor.Text;
end;

procedure TFmMyPagtos2.CkReciboClick(Sender: TObject);
begin
  if CkRecibo.Checked then
  //begin
    GBRecibo.Visible := True
    //CkParcelamento.Checked := False;
    //CkParcelamento.Enabled := False;
  //end else begin
  else
    GBRecibo.Visible := False;
    //CkParcelamento.Enabled := True;
  //end;
  if CriandoForm then CalculaRecibos(False)
  else CalculaRecibos(True);
end;

procedure TFmMyPagtos2.EdPorcentoExit(Sender: TObject);
begin
  EdPorcento.Text := Geral.TFT(EdPorcento.Text, 4, siPositivo);
  CalculaRecibos(True);
  EdGetFocus := 0;
end;

procedure TFmMyPagtos2.EdEmpresaExit(Sender: TObject);
begin
  EdEmpresa.Text := Geral.TFT(EdEmpresa.Text, 2, siPositivo);
  CalculaRecibos(True);
  EdGetFocus := 0;
end;

procedure TFmMyPagtos2.EdPessoalExit(Sender: TObject);
begin
  EdPessoal.Text := Geral.TFT(EdPessoal.Text, 2, siPositivo);
  CalculaRecibos(True);
  EdGetFocus := 0;
end;

procedure TFmMyPagtos2.EdPessoalEnter(Sender: TObject);
begin
  EdGetFocus := 1;
end;

procedure TFmMyPagtos2.EdEmpresaEnter(Sender: TObject);
begin
  EdGetFocus := 2;
end;

procedure TFmMyPagtos2.EdPorcentoEnter(Sender: TObject);
begin
  EdGetFocus := 3;
end;

function TFmMyPagtos2.CalculaRecibos(AvisaErro: Boolean): Boolean;
var
  Total, Pessoal, Empresa, Porcento: Double;
begin
  Result := True;
  if CkRecibo.Checked = False then Exit;
  Total    := Geral.DMV(EdValor.Text);
  Pessoal  := Geral.DMV(EdPessoal.Text);
  Empresa  := Geral.DMV(EdEmpresa.Text);
  Porcento := Geral.DMV(EdPorcento.Text);
  case EdGetFocus of
    0:
    begin
      if PorcentoFocused then Empresa := Total * Porcento / 100
      else if Total > 0 then Porcento := (Empresa / Total) * 100
      else Porcento := 0;
      Pessoal := Total - Empresa;
    end;
    1:
    begin
      Empresa := Total - Pessoal;
      if Total > 0 then Porcento := (Empresa / Total) * 100
      else Porcento := 0;
    end;
    2:
    begin
      Pessoal := Total - Empresa;
      if Total > 0 then Porcento := (Empresa / Total) * 100
      else Porcento := 0;
    end;
    3:
    begin
      if Porcento > 100 then Porcento := 100;
      Empresa := (Total * Porcento) / 100;
      Pessoal := Total - Empresa;
    end;
  end;
  if (Pessoal < 0) or (Empresa<0) or (Total<0) or (Porcento<0) then
  begin
    Result := False;
    if AvisaErro then Application.MessageBox('Erro no c�lculo dos recibos',
    'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  EdValor.Text    := Geral.TFT(FloatToStr(Total), 2, siPositivo);
  EdPessoal.Text  := Geral.TFT(FloatToStr(Pessoal), 2, siPositivo);
  EdEmpresa.Text  := Geral.TFT(FloatToStr(Empresa), 2, siPositivo);
  EdPorcento.Text := Geral.TFT(FloatToStr(Porcento), 4, siPositivo);
end;

procedure TFmMyPagtos2.EdPorcentoChange(Sender: TObject);
begin
  if EdGetFocus = 3 then PorcentoFocused := True;
end;

procedure TFmMyPagtos2.EdCodigoChange(Sender: TObject);
begin
  EdRecibo.Text := EdBaseRecibo.Text + FormatFloat('000', StrToInt(EdCodigo.Text));
end;

procedure TFmMyPagtos2.EdDiaReciboExit(Sender: TObject);
begin
  EdDiaRecibo.Text := MLAGeral.TFT_MinMax(EdDiaRecibo.Text, 0, 31, 0, siPositivo);
end;

procedure TFmMyPagtos2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  IC3_ED_RECIBO      := 0;
  IC3_ED_DIARECIBO   := 0;
  IC3_ED_VALEMPRESAP := 0;
  IC3_ED_VALEMPRESAV := 0;
  VAR_MYPAGTOSCONFIG := 0;
end;

procedure TFmMyPagtos2.EdMoraDiaExit(Sender: TObject);
begin
  EdMoraDia.Text := Geral.TFT(EdMoraDia.Text, 2, siPositivo);
  //CalculaRecibos(True);
end;

procedure TFmMyPagtos2.EdMultaExit(Sender: TObject);
begin
  EdMulta.Text := Geral.TFT(EdMulta.Text, 2, siPositivo);
  //CalculaRecibos(True);
end;

procedure TFmMyPagtos2.EdMultaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Porcent_Txt: String;
  Porcentagem, Valor: Double;
begin
  if key=VK_F4 then
  begin
    Porcent_Txt := '2,00';
    if InputQuery('Pagamentos e recebimentos', 'Defina a porcentagem de multa:',
    Porcent_Txt) then
    begin
      Porcentagem := Geral.DMV(Porcent_Txt);
      Valor := Geral.DMV(EdValor.Text);
      EdMulta.Text := Geral.TFT(FloatToStr((Porcentagem/100)*Valor), 2,
      siPositivo);
    end;
  end;
end;

procedure TFmMyPagtos2.EdMoraDiaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Porcent_Txt: String;
  Porcentagem, Valor: Double;
begin
  if key=VK_F4 then
  begin
    Porcent_Txt := '2,00';
    if InputQuery('Pagamentos e recebimentos', 'Defina a porcentagem de juros para 30 dias:',
    Porcent_Txt) then
    begin
      Porcentagem := Geral.DMV(Porcent_Txt);
      Valor := Geral.DMV(EdValor.Text);
      EdMoraDia.Text := Geral.TFT(FloatToStr((Porcentagem/3000)*Valor), 2,
      siPositivo);
    end;
  end;
end;

procedure TFmMyPagtos2.RGPeriodoClick(Sender: TObject);
begin
  if RGPeriodo.ItemIndex = 1 then
  begin
    EdDias.Enabled := True;
    if FAtivou then EdDias.SetFocus;
  end else begin
    EdDias.Enabled := False;
  end;
  CalculaParcelas;
end;

procedure TFmMyPagtos2.EdDiasExit(Sender: TObject);
begin
  EdDias.Text := Geral.TFT(EdDias.Text, 0, siPositivo);
  CalculaParcelas;
end;

procedure TFmMyPagtos2.EdParcelasExit(Sender: TObject);
begin
  EdParcelas.Text := MLAGeral.TFT_MinMax(EdParcelas.Text, 2, 1000, 0, siPositivo);
  CalculaParcelas;
end;

procedure TFmMyPagtos2.CkParcelamentoClick(Sender: TObject);
begin
  if CkParcelamento.Checked then
  begin
    GBParcelamento.Visible := True;
    LaParcela.Visible := False;
    EdCodigo.Visible := False;
  end else begin
    GBParcelamento.Visible := False;
    LaParcela.Visible := True;
    EdCodigo.Visible := True;
  end;
  // Calcular mesmo sem parcelamento,
  //porque da� limpa as parcelas calculadas
  CalculaParcelas;
end;

procedure TFmMyPagtos2.InsereParcela(Parcela: Integer; Vencimento: String);
var
  Documento, Controle, Debito, Credito, MoraDia, Multa : Double;
  Sit, Genero, Devedor, Credor, Terceiro, CliInt: Integer;
  Descricao, (*Responsavel, *)Duplicata: String;
begin
  Duplicata := EdDuplicata.Text;
  if CkParcelamento.Checked then Duplicata := Duplicata+'/'+IntToStr(Parcela);
  if CBCliInt.KeyValue = Null then CliInt := DmodG.QrMasterDono.Value
  else CliInt := CBCliInt.KeyValue;
  Credor  := CBCredor.KeyValue;
  Devedor := CBDevedor.KeyValue;
  if not CalculaRecibos(True) then exit;
  Screen.Cursor := crHourglass;
  Descricao := EdDescricao.Text;
  //Genero  := -99 + (VAR_FATID*(-1));
  Genero    := Geral.IMV(EdGenero.Text);
  Documento := Geral.DMV(EdDocumento.Text);
  if CkParcelamento.Checked then
  begin
    Documento := TbParcpagtosDoc.Value;
    Debito    := TbParcpagtosDebito.Value;
    Credito   := TbParcpagtosCredito.Value;
  end else begin
    if LaValor.Caption = CO_DEBITO then
    begin
      Debito := Geral.DMV(EdValor.Text);
      Credito := 0;
    end else begin
      Debito := 0;
      Credito := Geral.DMV(EdValor.Text);
    end;
  end;
  //
  if QrCarteirasTipo.Value = 2 then Sit := 0 else Sit := 3;
  Dmod.QrUpdU.SQL.Clear;
  MoraDia := Geral.DMV(EdMoraDia.Text);
  Multa   := Geral.DMV(EdMulta.Text);
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Dmod.QrUpdU.SQL.Add('UPDATE ' + VAR_LCT + ' SET FatParcela=FatParcela+1');
    Dmod.QrUpdU.SQL.Add('WHERE FatParcela>=:P0 AND FatID=:P1');
    Dmod.QrUpdU.SQL.Add('AND FatNum=:P2');
    Dmod.QrUpdU.Params[0].AsInteger := Parcela;
    Dmod.QrUpdU.Params[1].AsInteger := VAR_FATIDTXT;
    Dmod.QrUpdU.Params[2].AsInteger := IC3_ED_FatNum;
    Dmod.QrUpdU.ExecSQL;

    Controle := UMyMod.BuscaEmLivreY_Double(VAR_MyBDFINANCAS,
      'Livres', 'Controle', VAR_LCT, VAR_LCT, 'Controle');
  end else
  begin
    Controle := IC3_ED_Controle;
  end;
  UFinanceiro.LancamentoDefaultVARS;
  FLAN_Data          := FormatDateTime(VAR_FORMATDATE, TPData.Date);
  FLAN_Tipo          := QrCarteirasTipo.Value;
  FLAN_Carteira      := CBCarteira.KeyValue;
  FLAN_Documento     := Trunc(Documento + 0.1);
  FLAN_Genero        := Genero;
  FLAN_Descricao     := Descricao;
  FLAN_NotaFiscal    := Geral.IMV(EdNotaFiscal.Text);
  FLAN_Debito        := Debito;
  FLAN_Credito       := Credito;
  FLAN_Compensado    := CO_VAZIO;
  FLAN_Vencimento    := Vencimento;
  FLAN_Sit           := Sit;
  FLAN_Fornecedor    := CBCredor.KeyValue;
  FLAN_Cliente       := CBDevedor.KeyValue;
  FLAN_MoraDia       := MoraDia;
  FLAN_Multa         := Multa;
  FLAN_Vendedor      := 0; // vendedor
  FLAN_Account       := 0; // account
  FLAN_FatID_Sub     := FFatIDSub;
  FLAN_CliInt        := CliInt;
  //
  FLAN_Emitente      := FNome;
  FLAN_CNPJCPF       := FCNPJCPF;
  FLAN_Banco         := FBanco;
  FLAN_Agencia       := FAgencia;
  FLAN_ContaCorrente := FConta;
  FLAN_Duplicata     := Duplicata;
  FLAN_Controle      := Trunc(Controle + 0.01);
  FLAN_FatID         := VAR_FATIDTXT;
  FLAN_FatNum        := IC3_ED_FatNum;
  FLAN_FatParcela    := Parcela;
  FLAN_SerieCH       := EdSerieCH.Text;
  //
  if UFinanceiro.InsereLancamento() then
  begin
    if CkRecibo.Checked then
    begin
      if CBDevedor.Enabled then Terceiro := Devedor else Terceiro := Credor;
      //
      GOTOy.EmiteRecibo(Terceiro, EdCliInt.ValueVariant, EdPessoal.ValueVariant,
      0, 0, EdRecibo.ValueVariant, EdDescricao.ValueVariant, '', '',
      TPVencimento.Date, 0);
      {if Trim(Dmod.QrDonoRespons1.Value) <> CO_VAZIO then
      Responsavel := Dmod.QrDonoRespons1.Value else Responsavel := CO_VAZIO;
      if Trim(Dmod.QrDonoRespons2.Value) <> CO_VAZIO then
      begin
        if Responsavel <> CO_VAZIO then
        Responsavel := Responsavel + ' ou '+ Dmod.QrDonoRespons1.Value
        else Responsavel := Dmod.QrDonoRespons1.Value;
      end;
      if Responsavel = CO_VAZIO then Responsavel := 'Respons�vel';
      if CBDevedor.Enabled then Terceiro := Devedor else Terceiro := Credor;
      QrTerceiro.Close;
      QrTerceiro.Params[0].AsInteger := Terceiro;
      QrTerceiro.Open;
      MyObjects.CriaForm_AcessoTotal(TFmRecibos, FmRecibos);
      with FmRecibos do
      begin
       ///////
        // PARTE ALUNO
        //////
        if EdPessoal.Text <> Geral.FFT(0, 2, siPositivo) then
        begin
          EdNumeroP.Text   := EdRecibo.Text + '-P';
          EdValorP.Text    := EdPessoal.Text;
          EdPPagador.Text  := QrTerceiroNome.Value;
          EdPCNPJC.Text    := Geral.FormataCNPJ_TT(QrTerceiroCPF.Value);
          EdPRua.Text      := QrTerceiroPRua.Value;
          EdPNumero.Text   := IntToStr(QrTerceiroPNumero.Value);
          EdPCompl.Text    := QrTerceiroPCompl.Value;
          EdPBairro.Text   := QrTerceiroPBairro.Value;
          EdPCidade.Text   := QrTerceiroPCidade.Value;
          EdPUF.Text       := QrTerceiroNOMEUFP.Value;
          EdPCEP.Text      :=Geral.FormataCEP_NT(QrTerceiroPCEP.Value);
          EdPPais.Text     := QrTerceiroPPais.Value;
          EdPTe1.Text      := Geral.FormataTelefone_TT(QrTerceiroPTe1.Value);
          TextoP.Text      := '';
          // EMitente
          EdEmitenteP.Text := Dmod.QrDonoNOMEDONO.Value;
          EdCNPJP.Text     := Dmod.QrDonoCNPJ_TXT.Value;
          EdPRuaE.Text     := Dmod.QrDonoRUA.Value;
          EdPNumeroE.Text  := Dmod.QrDonoNUMERO_TXT.Value;
          EdPComplE.Text   := Dmod.QrDonoCOMPL.Value;
          EdPBairroE.Text  := Dmod.QrDonoBAIRRO.Value;
          EdPCidadeE.Text  := Dmod.QrDonoCIDADE.Value;
          EdPUFE.Text      := QrTerceiroNOMEUFP.Value;
          EdPCEPE.Text     :=Geral.FormataCEP_NT(Dmod.QrDonoCEP.Value);
          EdPPaisE.Text    := Dmod.QrDonoPais.Value;
          EdPTe1E.Text     := Dmod.QrDonoTE1_TXT.Value;
        end;
        ///////
        //  PARTE EMPRESA PATROCINADORA
        //////
        if EdEmpresa.Text <> Geral.FFT(0, 2, siPositivo) then
        begin
          EdNumeroE.Text   := EdRecibo.Text + '-E';
          EdValorE.Text    := EdEmpresa.Text;
          EdEPagador.Text  := QrTerceiroRazaoSocial.Value;
          EdECNPJC.Text    := Geral.FormataCNPJ_TT(QrTerceiroCNPJ.Value);
          EdERuaC.Text     := QrTerceiroERua.Value;
          EdENumeroC.Text  := IntToStr(QrTerceiroENumero.Value);
          EdEComplC.Text   := QrTerceiroECompl.Value;
          EdEBairroC.Text  := QrTerceiroEBairro.Value;
          EdECidadeC.Text  := QrTerceiroECidade.Value;
          EdeUFC.Text      := QrTerceiroNOMEUFE.Value;
          EdeCEPC.Text     :=Geral.FormataCEP_NT(QrTerceiroECEP.Value);
          EdePaisC.Text    := QrTerceiroEPais.Value;
          EdeTe1C.Text     := Geral.FormataTelefone_TT(QrTerceiroETe1.Value);
          TextoE.Text      := '"Parte da Empresa"';
          // EMitente
          EdEmitenteE.Text := Dmod.QrDonoNOMEDONO.Value;
          EdECNPJE.Text    := Dmod.QrDonoCNPJ_TXT.Value;
          EdERuaE.Text     := Dmod.QrDonoRUA.Value;
          EdENumeroE.Text  := Dmod.QrDonoNUMERO_TXT.Value;
          EdEComplE.Text   := Dmod.QrDonoCOMPL.Value;
          EdEBairroE.Text  := Dmod.QrDonoBAIRRO.Value;
          EdECidadeE.Text  := Dmod.QrDonoCIDADE.Value;
          EdEUFE.Text      := QrTerceiroNOMEUFP.Value;
          EdECEPE.Text     :=Geral.FormataCEP_NT(Dmod.QrDonoCEP.Value);
          EdEPaisE.Text    := Dmod.QrDonoPais.Value;
          EdETe1E.Text     := Dmod.QrDonoTE1_TXT.Value;
        end;
        EdLocalData.Text := Dmod.QrDonoCIDADE.Value;
        if Trim(Dmod.QrDonoCIDADE.Value) <> CO_VAZIO then
        EdLocalData.Text := EdLocalData.Text + ', ';
        EdLocalData.Text := EdLocalData.Text +
          FormatDateTime('dddd, dd" de "mmmm" de "yyyy', Now);
        LaResponsavelP.Caption := Responsavel;
        LaResponsavelE.Caption := Responsavel;
        ShowModal;
        Destroy;
      end;
      QrTerceiro.Close;}
    end;
  end;
end;

procedure TFmMyPagtos2.EdDocumentoChange(Sender: TObject);
begin
  if Geral.DMV(EdDocumento.Text) = 0 then
  begin
    RGIncremento.Enabled := False;
    GBCheque.Visible := False;
  end else begin
    RGIncremento.Enabled := True;
    GBCheque.Visible := True;
  end;
end;

procedure TFmMyPagtos2.CalculaParcelas;
var
  i, Parce, DiasP: Integer;
  Valor, Valor1, ValorX, ValorA, ValorC, ValorD, Total: Double;
  Data: TDate;
  //Arred, First: Boolean;
begin
  TbParcpagtos.DisableControls;
  while not TbParcpagtos.Eof do TbParcpagtos.Delete;
  TbParcpagtos.EnableControls;
  TbParcpagtos.Close;
  if GBParcelamento.Visible then
  begin
    Pagto_Doc := Geral.DMV(EdDocumento.Text);
    DiasP := Geral.IMV(EdDias.Text);
    Parce := Geral.IMV(EdParcelas.Text);
    Total := Geral.DMV(EdValor.Text);
    if Total <= 0 then Valor := 0
    else
    begin
      if Parce = 0 then Valor := Total else
      begin
        Valor := (Total / Parce)*100;
        Valor := (Trunc(Valor))/100;
      end;  
    end;
    if CkArredondar.Checked then Valor := int(Valor);
    Valor1 := Valor;
    ValorX := Valor;
    if RGArredondar.ItemIndex = 0 then
    begin
      EdParcela1.Text := Geral.TFT(FloatToStr(Valor), 2, siPositivo);
      ValorX := Total - ((Parce - 1) * Valor);
      EdParcelaX.Text := Geral.TFT(FloatToStr(ValorX), 2, siPositivo);
    end else begin
      EdParcelaX.Text := Geral.TFT(FloatToStr(Valor), 2, siPositivo);
      Valor1 := Total - ((Parce - 1) * Valor);
      EdParcela1.Text := Geral.TFT(FloatToStr(Valor1), 2, siPositivo);
    end;
    for i := 1 to Parce do
    begin
      if i= 1 then ValorA := Valor1
      else if i = Parce then ValorA := ValorX
      else ValorA := Valor;
      if LaValor.Caption = CO_CREDITO then ValorC := ValorA else ValorC := 0;
      if LaValor.Caption = CO_DEBITO  then ValorD := ValorA else ValorD := 0;
      //
      if RGPeriodo.ItemIndex = 0 then
        Data := MLAGeral.IncrementaMeses(TPVencimento.Date, i-1, True)
      else
        Data := TPVencimento.Date + (DiasP * (i-1));
      //
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('INSERT INTO parcpagtos SET Parcela=:P0, ');
      DmodG.QrUpdPID1.SQL.Add('Data=:P1, Credito=:P2, Debito=:P3, Doc=:P4');
      DmodG.QrUpdPID1.Params[00].AsInteger := i;
      DmodG.QrUpdPID1.Params[01].AsString := FormatDateTime(VAR_FORMATDATE, Data);
      DmodG.QrUpdPID1.Params[02].AsFloat := ValorC;
      DmodG.QrUpdPID1.Params[03].AsFloat := ValorD;
      DmodG.QrUpdPID1.Params[04].AsFloat := Pagto_Doc;
      DmodG.QrUpdPID1.ExecSQL;
      if not RGIncremento.Enabled then Pagto_Doc := Pagto_Doc
      else Pagto_Doc := Pagto_Doc + RGIncremento.ItemIndex
    end;
    TbParcpagtos.Open;
    TbParcpagtos.EnableControls;
  end;
  QrSoma.Close;
  QrSoma.Open;
  EdSoma.Text := FormatFloat('#,###,##0.00', QrSomaVALOR.Value);
  QrSoma.Close;
end;

procedure TFmMyPagtos2.CkArredondarClick(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmMyPagtos2.RGArredondarClick(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmMyPagtos2.TbParcpagtosBeforeOpen(DataSet: TDataSet);
//var
  //Texto: String;
  //i: Integer;
begin
(*  if LaValor.Caption = CO_CREDITO then Texto := 'Credito' else Texto := 'Debito';
  for i := 0 to DBGParcelas.Columns.Count -1 do
  begin
    if DBGParcelas.Columns[i].FieldName = 'Credito' then
       DBGParcelas.Columns[i].FieldName := Texto;
    if DBGParcelas.Columns[i].FieldName = 'Debito'  then
       DBGParcelas.Columns[i].FieldName := Texto;
  end;*)
end;

procedure TFmMyPagtos2.TPVencimentoExit(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmMyPagtos2.TbParcpagtosAfterPost(DataSet: TDataSet);
begin
  QrSoma.Close;
  QrSoma.Open;
  EdSoma.Text := FormatFloat('#,###,##0.00', QrSomaVALOR.Value);
  QrSoma.Close;
end;

procedure TFmMyPagtos2.RGIncrementoClick(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmMyPagtos2.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, FTitulo, Image1,
  PainelTitulo, True, 0);
end;

procedure TFmMyPagtos2.EdTrocoParaChange(Sender: TObject);
begin
  CalculaTroco;
end;

procedure TFmMyPagtos2.EdValorChange(Sender: TObject);
begin
  CalculaTroco;
end;

procedure TFmMyPagtos2.CalculaTroco;
var
  APagar, ATrocar, Troco: Double;
begin
  APagar  := Geral.DMV(EdValor.Text);
  ATrocar := Geral.DMV(EdTrocoPara.Text);
  //
  Troco   := ATrocar-APagar;
  EdTrocoVal.Text := Geral.TFT(FloatToStr(Troco), 2, siNegativo);
end;

procedure TFmMyPagtos2.RGReciboClick(Sender: TObject);
begin
  if RGRecibo.ItemIndex = 3 then EdDiaRecibo.SetFocus;
end;

procedure TFmMyPagtos2.EdCNPJCPFExit(Sender: TObject);
var
  Num : String;
  CPF : String;
begin
  CPF := Geral.SoNumero_TT(EdCNPJCPF.Text);
  if CPF <> CO_VAZIO then
    begin
    Num := MLAGeral.CalculaCNPJCPF(CPF);
    if MLAGeral.FormataCNPJ_TFT(CPF) <> Num then
    begin
      Application.MessageBox(PChar('N�mero inv�lido'), 'Erro', MB_OK+MB_ICONERROR);
      EdCNPJCPF.SetFocus;
    end else EdCNPJCPF.Text := Geral.FormataCNPJ_TT(CPF);
  end else EdCNPJCPF.Text := CO_VAZIO;
end;

procedure TFmMyPagtos2.EdBancoExit(Sender: TObject);
begin

  EdBanco.Text := FormatFloat('000',
    Geral.DMV(Geral.TFT(EdBanco.Text, 0, siPositivo)));
end;

procedure TFmMyPagtos2.EdDevedorChange(Sender: TObject);
begin
  if (EdNome.Text = '') and (CBDevedor.Text <> '')
  then EdNome.Text := CBDevedor.Text;
end;

procedure TFmMyPagtos2.EdNomeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
  begin
    EdNome.Text    := CBDevedor.Text;
    EdCNPJCPF.Text := QrDevedorCNPJ_CPF_TXT.Value;
    EdBanco.Text   := IntToStr(QrDevedorBanco.Value);
    EdAgencia.Text := QrDevedorAgencia.Value;
    EdConta.Text   := QrDevedorContaCorrente.Value;
  end;
end;

procedure TFmMyPagtos2.QrDevedorCalcFields(DataSet: TDataSet);
begin
  QrDevedorCNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrDevedorCNPJ_CPF.Value);
end;

procedure TFmMyPagtos2.DBGParcelasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then if TbParcPagtos.State in ([dsEdit, dsInsert])
  then TbParcpagtos.Post;
end;

procedure TFmMyPagtos2.EdNotaFiscalExit(Sender: TObject);
begin
  EdNotaFiscal.Text := Geral.TFT(EdNotaFiscal.Text, 0, siPositivo);
  if EdDuplicata.Text = '' then EdDuplicata.Text := EdNotaFiscal.Text;
end;

procedure TFmMyPagtos2.SpeedButton1Click(Sender: TObject);
begin
  FmPrincipal.CadastroDeCarteiras(QrCarteirasTipo.Value,
  Geral.IMV(EdCarteira.Text));
end;

procedure TFmMyPagtos2.SpeedButton2Click(Sender: TObject);
var
  Dias : Integer;
begin
  Dias := Geral.IMV(
    InputBox(Caption, 'Digite o n�mero de dias.', '' ));
  TPVencimento.Date := TPVencimento.Date + Dias;
end;

procedure TFmMyPagtos2.QrCarteirasAfterScroll(DataSet: TDataSet);
begin
  case QrCarteirasTipo.Value of
    0 :
    begin
      EdDocumento.Enabled :=  False;
      TPVencimento.Enabled := False;
      LaDocumento.Enabled :=  False;
      LaVencimento.Enabled := False;
    end;
    1 :
    begin
      EdDocumento.Enabled :=  True;
      TPVencimento.Enabled := False;
      LaDocumento.Enabled :=  True;
      LaVencimento.Enabled := False;
    end;
    2 :
    begin
      EdDocumento.Enabled :=  True;
      TPVencimento.Enabled := True;
      LaDocumento.Enabled :=  True;
      LaVencimento.Enabled := True;
    end;
  end;
end;

procedure TFmMyPagtos2.SpeedButton3Click(Sender: TObject);
begin
  FmPrincipal.CadastroDeContas;
end;

procedure TFmMyPagtos2.ReopenCarteiras();
begin
  QrCarteiras.Close;
  if Uppercase(Application.Title) = 'LESEW' then
  begin
    QrCarteiras.SQL.Clear;
    QrCarteiras.SQL.Add('SELECT car.*');
    QrCarteiras.SQL.Add('FROM carteiras car');
    QrCarteiras.SQL.Add('LEFT JOIN carteirasu cau ON cau.Codigo = car.Codigo');
    if VAR_USUARIO > 0 then
      QrCarteiras.SQL.Add('WHERE cau.Usuario =' + IntToStr(VAR_USUARIO));
    QrCarteiras.SQL.Add('GROUP BY car.Codigo');
    QrCarteiras.SQL.Add('ORDER BY car.Nome');
  end;
  QrCarteiras.Open;
end;

end.

