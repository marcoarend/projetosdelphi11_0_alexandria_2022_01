unit TxaCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Menus, Grids,
  DBGrids, dmkDBGridDAC, dmkDBGrid, dmkEdit, Variants, dmkLabel,
  dmkDBLookupComboBox, dmkEditCB, UnDmkProcFunc, dmkGeral;

type
  TFmTxaCad = class(TForm)
    PainelDados: TPanel;
    DsTxaCad: TDataSource;
    QrTxaCad: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PanelFill002: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtCond: TBitBtn;
    BtTaxa: TBitBtn;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdNome: TEdit;
    PMTaxa: TPopupMenu;
    Crianovogrupo1: TMenuItem;
    Alteragrupoatual1: TMenuItem;
    Excluigrupoatual1: TMenuItem;
    PainelItens: TPanel;
    Panel6: TPanel;
    BitBtn1: TBitBtn;
    Panel7: TPanel;
    BitBtn2: TBitBtn;
    Panel8: TPanel;
    Panel9: TPanel;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    DDBEdit2: TDBEdit;
    Label8: TLabel;
    DsCodiNivel: TDataSource;
    QrCodiNivel: TmySQLQuery;
    DsEntiCond: TDataSource;
    QrEntiCond: TmySQLQuery;
    QrEntiCondCodCond: TIntegerField;
    QrEntiCondCodEnti: TIntegerField;
    QrEntiCondNOMECOND: TWideStringField;
    PMCond: TPopupMenu;
    AdicionaUHaogrupo1: TMenuItem;
    RetiraUHselecionadadogrupo1: TMenuItem;
    QrTxaAdm: TmySQLQuery;
    DsTxaAdm: TDataSource;
    dmkDBGrid1: TdmkDBGrid;
    QrTxaCadNome: TWideStringField;
    QrTxaCadLk: TIntegerField;
    QrTxaCadDataCad: TDateField;
    QrTxaCadDataAlt: TDateField;
    QrTxaCadUserCad: TIntegerField;
    QrTxaCadUserAlt: TIntegerField;
    QrTxaCadAlterWeb: TSmallintField;
    QrTxaCadAtivo: TSmallintField;
    QrTxaCadCodigo: TIntegerField;
    QrTxaAdmCodigo: TIntegerField;
    QrTxaAdmControle: TAutoIncField;
    QrTxaAdmEmpresa: TIntegerField;
    QrTxaAdmNivel: TSmallintField;
    QrTxaAdmCodiNivel: TIntegerField;
    QrTxaAdmTaxaPerc: TFloatField;
    QrTxaAdmLk: TIntegerField;
    QrTxaAdmDataCad: TDateField;
    QrTxaAdmDataAlt: TDateField;
    QrTxaAdmUserCad: TIntegerField;
    QrTxaAdmUserAlt: TIntegerField;
    QrTxaAdmAlterWeb: TSmallintField;
    QrTxaAdmAtivo: TSmallintField;
    QrTxaAdmNOMENIVEL: TWideStringField;
    QrTxaAdmNOMECODIGO: TWideStringField;
    QrTxaAdmNOMECLII: TWideStringField;
    Panel10: TPanel;
    Label11: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    dmkEdTaxaPerc: TdmkEdit;
    Label3: TLabel;
    RGNivel: TRadioGroup;
    CBCodiNivel: TdmkDBLookupComboBox;
    EdCodiNivel: TdmkEditCB;
    Label12: TLabel;
    QrCodiNivelCodigo: TIntegerField;
    QrCodiNivelNome: TWideStringField;
    Alterataxadoitemselecionado1: TMenuItem;
    N1: TMenuItem;
    QrTxaAdmCOND: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtTaxaClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTxaCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTxaCadBeforeOpen(DataSet: TDataSet);
    procedure Crianovogrupo1Click(Sender: TObject);
    procedure Alteragrupoatual1Click(Sender: TObject);
    procedure Excluigrupoatual1Click(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure BtCondClick(Sender: TObject);
    procedure AdicionaUHaogrupo1Click(Sender: TObject);
    procedure RetiraUHselecionadadogrupo1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure EdCodiNivelChange(Sender: TObject);
    procedure QrTxaCadAfterScroll(DataSet: TDataSet);
    procedure RGNivelClick(Sender: TObject);
    procedure Alterataxadoitemselecionado1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //
    //procedure ReopenPropriet();
    //procedure ReopenCondImov();
    procedure ReopenTxaAdm(Controle: Integer);
    function NivelSelecionado(Tipo: Integer; Compl_Pre, Compl_Pos:
             String): String;
    procedure ReopenCodiNivel(Codigo: Integer);
  public
    { Public declarations }
  end;

var
  FmTxaCad: TFmTxaCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTxaCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTxaCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTxaCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTxaCad.DefParams;
begin
  VAR_GOTOTABELA := 'TxaCad';
  VAR_GOTOMYSQLTABLE := QrTxaCad;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM TxaCad');
  VAR_SQLx.Add('WHERE Codigo <> 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmTxaCad.MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
      PainelItens.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if Status = CO_INCLUSAO then
      begin
        EdCodigo.Text   := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text     := '';
        //EdUsername.Text := '';
        //EdPassword.Text := '';
      end else begin
        EdCodigo.Text   := DBEdCodigo.Text;
        EdNome.Text     := DBEdNome.Text;
        //EdUsername.Text := QrTxaCadUsername.Value;
        //EdPassword.Text := QrTxaCadUsername.Value;
      end;
      EdNome.SetFocus;
    end;
    2:
    begin
      PainelItens.Visible    := True;
      PainelDados.Visible    := False;
      PainelControle.Visible := False;
      //
      if Status = CO_INCLUSAO then
      begin
        EdEmpresa.Text       := '';
        CBEmpresa.KeyValue   := Null;
        //
        RGNivel.ItemIndex    := 0;
        //
        EdCodiNivel.Text     := '';
        CBCodiNivel.KeyValue := Null;
      end else begin
        QrEntiCond.Locate('CodCond', QrTxaAdmEmpresa.Value, []);
        EdEmpresa.Text       := IntToStr(QrTxaAdmCOND.Value);
        CBEmpresa.KeyValue   := QrTxaAdmCOND.Value;
        //
        RGNivel.ItemIndex    := QrTxaAdmNivel.Value;
        //
        EdCodiNivel.Text     := IntToStr(QrTxaAdmCodiNivel.Value);
        CBCodiNivel.KeyValue := QrTxaAdmCodiNivel.Value;
        //
        dmkEdTaxaPerc.ValueVariant := QrTxaAdmTaxaPerc.Value;
      end;
      if RGNivel.ItemIndex < 0 then RGNivel.ItemIndex := 0;
      EdEmpresa.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmTxaCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTxaCad.AlteraRegistro;
var
  TxaCad : Integer;
begin
  TxaCad := QrTxaCadCodigo.Value;
  if not UMyMod.SelLockY(TxaCad, Dmod.MyDB, 'TxaCad', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(TxaCad, Dmod.MyDB, 'TxaCad', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmTxaCad.IncluiRegistro;
var
  Cursor : TCursor;
  TxaCad : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    TxaCad := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'TxaCad', 'TxaCad', 'Codigo');
    if Length(FormatFloat(FFormatFloat, TxaCad))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, CO_INCLUSAO, TxaCad);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmTxaCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTxaCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTxaCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTxaCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTxaCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTxaCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTxaCad.BtTaxaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTaxa, BtTaxa);
end;

procedure TFmTxaCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTxaCadCodigo.Value;
  Close;
end;

procedure TFmTxaCad.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := Geral.IMV(EdCodigo.Text);
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, LaTipo.Caption, 'TxaCad', False,
  [
    'Nome'
  ], ['Codigo'],
  [
  EdNome.Text
  ], [Codigo]) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'TxaCad', 'Codigo');
    MostraEdicao(0, CO_TRAVADO, 0);
    LocCod(Codigo,Codigo);
  end;
end;

procedure TFmTxaCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'TxaCad', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'TxaCad', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'TxaCad', 'Codigo');
end;

procedure TFmTxaCad.FormCreate(Sender: TObject);
begin
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  Panel8.Align      := alClient;
  Panel10.Align     := alClient;
  dmkDBGrid1.Align  := alClient;
  CriaOForm;
  //
  QrEntiCond.Open;
  QrCodiNivel.Open;
end;

procedure TFmTxaCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTxaCadCodigo.Value,LaRegistro.Caption);
end;

procedure TFmTxaCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTxaCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmTxaCad.QrTxaCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTxaCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTxaCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrTxaCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'TxaCad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmTxaCad.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmTxaCad.QrTxaCadBeforeOpen(DataSet: TDataSet);
begin
  QrTxaCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmTxaCad.Crianovogrupo1Click(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmTxaCad.Alteragrupoatual1Click(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmTxaCad.Excluigrupoatual1Click(Sender: TObject);
begin
  //
end;

procedure TFmTxaCad.EdEmpresaChange(Sender: TObject);
begin
  //ReopenPropriet();
  //ReopenCondImov();
end;

procedure TFmTxaCad.BtCondClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCond, BtCond);
end;

procedure TFmTxaCad.BitBtn2Click(Sender: TObject);
begin
  MostraEdicao(0, CO_TRAVADO, 0);
end;

procedure TFmTxaCad.BitBtn1Click(Sender: TObject);
var
  //Apto, Propr, Empresa,
  Codigo, Controle, Nivel, CodiNivel: Integer;
begin
  Codigo    := QrTxaCadCodigo.Value;
  //Empresa   := Geral.IMV(EdEmpresa.Text);
  Nivel     := RGNivel.ItemIndex;
  CodiNivel := Geral.IMV(EdCodiNivel.Text);
  //
  if (Codigo = 0) then
  begin
    Application.MessageBox('Defina o grupo!', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  //
  if (Nivel = 0) then
  begin
    Application.MessageBox('Defina o n�vel do plano de contas!', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  //
  if (CodiNivel = 0) then
  begin
    Application.MessageBox(PChar('Defina o ' + NivelSelecionado(0, '', '') + '!'),
      'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  //
  {if (Apto = 0) then
  begin
    Application.MessageBox('Defina o im�vel (UH)!', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;}
  //
  //Controle := UMyMod.BuscaEmLivreY_Def('TxaAdm', 'Controle', CO_INCLUSAO, 0);
  //
  Controle := QrTxaAdmControle.Value;
  //
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, LaTipo.Caption, 'TxaAdm', True,
  [
    'Codigo', 'Empresa', 'Nivel', 'CodiNivel', 'TaxaPerc'
  ], ['Controle'],
  [
  Codigo, QrEntiCondCodEnti.Value, Nivel, CodiNivel, dmkEdTaxaPerc.ValueVariant
  ], [Controle]) then
  begin
    MostraEdicao(0, CO_TRAVADO, 0);
    ReopenTxaAdm(Controle);
  end;
end;

{
procedure TFmTxaCad.ReopenPropriet();
var
  Cond: Integer;
begin
  EdPropriet.Text := '';
  CBPropriet.KeyValue := Null;
  //
  Cond := Geral.IMV(EdEmpresa.Text);
  QrPropriet.Close;
  QrPropriet.SQL.Clear;
  QrPropriet.SQL.Add('SELECT DISTINCT ent.Codigo,');
  QrPropriet.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP');
  QrPropriet.SQL.Add('FROM entidades ent');
  QrPropriet.SQL.Add('LEFT JOIN condimov cim ON ent.Codigo=cim.Propriet');
  QrPropriet.SQL.Add('WHERE ent.Cliente2="V"');
  QrPropriet.SQL.Add('');
  if Cond <> 0 then
    QrPropriet.SQL.Add('AND cim.Codigo=' + IntToStr(Cond));
  QrPropriet.SQL.Add('');
  QrPropriet.SQL.Add('ORDER BY NOMEPROP');
  QrPropriet.Open;
end;
}
{
procedure TFmTxaCad.ReopenCondImov();
var
  Cond, Propriet: Integer;
begin
  EdCondImov.Text := '';
  CBCondImov.KeyValue := Null;
  //
  Cond := Geral.IMV(EdEmpresa.Text);
  Propriet := Geral.IMV(EdPropriet.Text);
  QrCondImov.Close;
  QrCondImov.SQL.Clear;
  QrCondImov.SQL.Add('SELECT cim.Conta, cim.Unidade');
  QrCondImov.SQL.Add('FROM condimov cim');
  QrCondImov.SQL.Add('WHERE cim.Codigo<>0');
  QrCondImov.SQL.Add('AND cim.Propriet<>0');
  QrCondImov.SQL.Add('');
  if Cond <> 0 then
    QrCondImov.SQL.Add('AND cim.Codigo=' + IntToStr(Cond));
  if Propriet <> 0 then
    QrCondImov.SQL.Add('AND cim.Propriet=' + IntToStr(Propriet));
  QrCondImov.SQL.Add('');
  QrCondImov.SQL.Add('ORDER BY cim.Unidade');
  QrCondImov.Open;
end;
}

procedure TFmTxaCad.EdCodiNivelChange(Sender: TObject);
begin
  //ReopenCondImov;
end;

procedure TFmTxaCad.QrTxaCadAfterScroll(DataSet: TDataSet);
begin
  ReopenTxaAdm(0);
end;

procedure TFmTxaCad.ReopenTxaAdm(Controle: Integer);
begin
  QrTxaAdm.Close;
  QrTxaAdm.Params[0].AsInteger := QrTxaCadCodigo.Value;
  QrTxaAdm.Open;
  //
  if Controle > 0 then
    QrTxaAdm.Locate('Controle', Controle, []);
end;

function TFmTxaCad.NivelSelecionado(Tipo: Integer;
Compl_Pre, Compl_Pos: String): String;
const
  Txt0: array[0..5] of String = ('Nenhum', 'Conta', 'Sub-grupo', 'Grupo', 'Conjunto', 'Plano');
  Txt1: array[0..5] of String = ('do ???', 'da conta', 'do sub-grupo', 'do grupo', 'do conjunto', 'do plano');
  Txt2: array[0..5] of String = ('#ERRO#', 'contas', 'subgrupos', 'grupos', 'conjuntos', 'plano');
var
  Texto: String;
begin
  case Tipo of
    0: Texto := Txt0[RGNivel.ItemIndex];
    1: Texto := Txt1[RGNivel.ItemIndex];
    2: Texto := Txt2[RGNivel.ItemIndex];
    else Texto := ' ? ';
  end;
  Result := Compl_pre + Texto + Compl_Pos;
end;

procedure TFmTxaCad.RGNivelClick(Sender: TObject);
begin
  ReopenCodiNivel(0);
  Label12.Caption := NivelSelecionado(1, 'Item ', ':');
end;

procedure TFmTxaCad.ReopenCodiNivel(Codigo: Integer);
begin
  QrCodiNivel.Close;
  if RGNivel.ItemIndex > 0 then
  begin
    QrCodiNivel.SQL[1] := 'FROM ' + NivelSelecionado(2, '', '');
    QrCodiNivel.Open;
    //
    if Codigo <> 0 then
      QrCodiNivel.Locate('Codigo', Codigo, []);
  end;    
end;

procedure TFmTxaCad.AdicionaUHaogrupo1Click(Sender: TObject);
begin
  MostraEdicao(2, CO_INCLUSAO, 0);
end;

procedure TFmTxaCad.Alterataxadoitemselecionado1Click(Sender: TObject);
begin
  MostraEdicao(2, CO_ALTERACAO, 0);
end;

procedure TFmTxaCad.RetiraUHselecionadadogrupo1Click(Sender: TObject);
var
  Prox: Integer;
begin
  if Application.MessageBox('Confirma a retirada o item selecionado?',
  'Pergunta', MB_YESNOCANCEL) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM TxaAdm WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrTxaAdmControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Prox := UMyMod.ProximoRegistro(QrTxaAdm, 'Controle',
      QrTxaAdmControle.Value);
    ReopenTxaAdm(Prox);
    Screen.Cursor := crDefault;
  end;
  //
end;

end.
