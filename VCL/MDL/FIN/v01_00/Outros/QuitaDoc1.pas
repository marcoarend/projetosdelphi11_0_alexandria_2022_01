unit QuitaDoc1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Buttons, Db, (*DBTables,*) UnInternalConsts, UnMLAGeral,
     
    UnGOTOy, UMySQLModule, ExtCtrls, 
     
   Mask, DBCtrls,  
  LMDStaticText, LMDDBLabel,  mySQLDbTables;

type
  TFmQuitaDoc1 = class(TForm)
    QrLancto1: TMySQLQuery;
    PainelDados: TPanel;
    Label1: TLabel;
    TPData1: TDateTimePicker;
    Label2: TLabel;
    EdDoc: TLMDEdit;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    DsLancto1: TDataSource;
    QrLancto2: TMySQLQuery;
    DsLancto2: TDataSource;
    EdDescricao: TLMDEdit;
    EdLancto: TLMDStaticText;
    LMDDBLabel1: TLMDDBLabel;
    LMDDBLabel3: TLMDDBLabel;
    LMDDBLabel5: TLMDDBLabel;
    EdSub: TLMDStaticText;
    LMDDBLabel6: TLMDDBLabel;
    LMDDBLabel7: TLMDDBLabel;
    QrLancto1Sub: TSmallintField;
    QrLancto1Descricao: TWideStringField;
    QrLancto1Credito: TFloatField;
    QrLancto1Debito: TFloatField;
    QrLancto1NotaFiscal: TIntegerField;
    QrLancto1Documento: TFloatField;
    QrLancto1Vencimento: TDateField;
    QrLancto1Carteira: TIntegerField;
    QrLancto1Data: TDateField;
    QrLancto1NOMECARTEIRA: TWideStringField;
    QrLancto1TIPOCARTEIRA: TIntegerField;
    QrLancto1CARTEIRABANCO: TIntegerField;
    QrLancto2Tipo: TSmallintField;
    QrLancto2Credito: TFloatField;
    QrLancto2Debito: TFloatField;
    QrLancto2Sub: TSmallintField;
    QrLancto2Carteira: TIntegerField;
    PainelTitulo: TPanel;
    Image1: TImage;
    QrLancto1Cliente: TIntegerField;
    QrLancto1Fornecedor: TIntegerField;
    QrLancto1DataDoc: TDateField;
    QrLancto1Nivel: TIntegerField;
    QrLancto1Vendedor: TIntegerField;
    QrLancto1Account: TIntegerField;
    QrLancto1Controle: TIntegerField;
    QrLancto1CtrlIni: TIntegerField;
    QrLancto2Controle: TIntegerField;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    CBCarteira: TDBLookupComboBox;
    EdCarteira: TLMDEdit;
    Label15: TLabel;
    SpeedButton1: TSpeedButton;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasBanco: TIntegerField;
    procedure BtDesisteClick(Sender: TObject);
    procedure EdDocExit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdCarteiraChange(Sender: TObject);
    procedure EdCarteiraExit(Sender: TObject);
    procedure CBCarteiraClick(Sender: TObject);
    procedure CBCarteiraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }

  public
    { Public declarations }
    FCtrlIni, FControle: Double;
  end;

var
  FmQuitaDoc1: TFmQuitaDoc1;

implementation

uses UnMyObjects, Module, UnFinanceiro;

{$R *.DFM}

procedure TFmQuitaDoc1.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmQuitaDoc1.EdDocExit(Sender: TObject);
begin
  EdDoc.Text := MLAGeral.TBT(EdDoc.Text, siPositivo);
end;

procedure TFmQuitaDoc1.FormActivate(Sender: TObject);
var
  Controle: Double;
  Sub: Integer;
begin
  EdDoc.SetFocus;
  MyObjects.CorIniComponente();
  //
  Controle :=   Geral.DMV(EdLancto.Caption);
  Sub :=        Geral.IMV(EdSub.Caption);
  QrLancto1.Params[0].AsFloat := Controle;
  QrLancto1.Params[1].AsInteger := Sub;
  QrLancto1.Open;
  if GOTOy.Registros(QrLancto1) = 0 then
  begin
    ShowMessage('Registro n�o encontrado');
    BtConfirma.Enabled := False;
  end;
  TPData1.Date :=     QrLancto1Vencimento.Value;
  EdDescricao.Text := QrLancto1Descricao.Value;
  EdDoc.Text :=       MLAGeral.TBT(FloatToStr(QrLancto1Documento.Value), siPositivo);
  //
  QrLancto2.Close;
  QrLancto2.Params[0].AsFloat := Controle;
  QrLancto2.Params[1].AsInteger := QrLancto1Sub.Value;
  QrLancto2.Open;
  EdDescricao.setFocus;
end;

procedure TFmQuitaDoc1.BtConfirmaClick(Sender: TObject);
var
  Texto: PChar;
  Doc, Controle, Controle2, Sub, Nivel, Carteira: Integer;
  CtrlIni: Double;
begin
  Nivel    := QrLancto1Nivel.Value + 1;
  if FCtrlIni = -1000 then ShowMessage('"FCtrlIni" n�o definido');
  if FCtrlIni > 0 then CtrlIni := FCtrlIni else CtrlIni := FControle;
  //
  Controle := QrLancto1Controle.Value;
  Sub      := QrLancto1Sub.Value;
  Doc      := Geral.IMV(EdDoc.Text);
  Carteira := Geral.IMV(EdCarteira.Text);
  //
  if Carteira = 0 then
  begin
    Application.MessageBox('Defina a carteira!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if GOTOy.Registros(QrLancto2) <> 0 then
  begin
    if ((QrLancto2Credito.Value + QrLancto2Debito.Value) =
      (QrLancto1Credito.Value + QrLancto1Debito.Value))
    and (GOTOy.Registros(QrLancto2) = 1) and (QrLancto2Tipo.Value = 1) then
    begin
      Texto := PChar('J� existe uma quita��o, e ela possui o mesmo valor,'+
      ' deseja difini-la como quita��o desta emiss�o ?');
      if Application.MessageBox(Texto, PChar(VAR_APPNAME),
      MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+MB_APPLMODAL) = ID_YES then
      begin
        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add('UPDATE lanctos SET AlterWeb=1, Sit=3, ');
        Dmod.QrUpdM.SQL.Add('Carteira=:P0, Compensado=:P1,');
        Dmod.QrUpdM.SQL.Add('DataAlt=:P2, UserAlt=:P3');
        Dmod.QrUpdM.SQL.Add('WHERE Controle=:Pa AND Tipo=2');
        //
        Dmod.QrUpdM.Params[00].AsInteger := Carteira;
        Dmod.QrUpdM.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPData1.Date);
        Dmod.QrUpdM.Params[02].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
        Dmod.QrUpdM.Params[03].AsInteger := VAR_USUARIO;
        //
        Dmod.QrUpdM.Params[04].AsFloat := Controle;
        Dmod.QrUpdM.ExecSQL;
        UFinanceiro.RecalcSaldoCarteira(Carteira, QrCarteiras, True, True);

        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add('UPDATE lanctos SET AlterWeb=1, Carteira=:P0, ');
        Dmod.QrUpdM.SQL.Add('Data=:P1, DataAlt=:P2, UserAlt=:P3');
        Dmod.QrUpdM.SQL.Add('WHERE ID_Pgto=:Pa AND Tipo=1 and ID_Pgto > 0');

        Dmod.QrUpdM.Params[00].AsInteger := Carteira;
        Dmod.QrUpdM.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPData1.Date);
        Dmod.QrUpdM.Params[02].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
        Dmod.QrUpdM.Params[03].AsInteger := VAR_USUARIO;
        //
        Dmod.QrUpdM.Params[04].AsFloat := Controle;
        Dmod.QrUpdM.ExecSQL;
        UFinanceiro.RecalcSaldoCarteira(Carteira, QrCarteiras, True, True);
      end;
    end else ShowMessage('J� existe pagamentos/rolagem para esta emiss�o!');
  end else begin
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('UPDATE lanctos SET AlterWeb=1, Sit=3, ');
    Dmod.QrUpdM.SQL.Add('Compensado=:P0, Documento=:P1, Descricao=:P2, ');
    Dmod.QrUpdM.SQL.Add('DataAlt=:P3, UserAlt=:P4, Carteira=:P5 ');
    Dmod.QrUpdM.SQL.Add('WHERE Controle=:Pa AND Sub=:Pb AND Tipo=2');
    Dmod.QrUpdM.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TPData1.Date);
    Dmod.QrUpdM.Params[01].AsFloat   := Doc;
    Dmod.QrUpdM.Params[02].AsString  := EdDescricao.Text;
    Dmod.QrUpdM.Params[03].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpdM.Params[04].AsInteger := VAR_USUARIO;
    Dmod.QrUpdM.Params[05].AsInteger := Carteira;
    //
    Dmod.QrUpdM.Params[06].AsFloat   := Controle;
    Dmod.QrUpdM.Params[07].AsInteger := Sub;
    Dmod.QrUpdM.ExecSQL;
    UFinanceiro.RecalcSaldoCarteira(QrLancto1Carteira.Value,
      Qrcarteiras, True, True);

    {
    Controle2 := UMyMod.BuscaEmLivreY_Double(Dmod.MyDB, 'Livres', 'Controle', 'Lanctos', 'Lanctos', 'Controle');
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('INSERTINTO Lanctos SET Tipo=1, Genero=-5, Sit=3, ');
    Dmod.QrUpdM.SQL.Add('Data=:P0, Controle=:P1, Descricao=:P2, NotaFiscal=:P3, ');
    Dmod.QrUpdM.SQL.Add('Debito=:P4, Credito=:P5, Compensado=:P6, Documento=:P7, ');
    Dmod.QrUpdM.SQL.Add('Carteira=:P8, Cliente=:P9, Fornecedor=:P10, ');
    Dmod.QrUpdM.SQL.Add('ID_Pgto=:P11, Sub=:P12, DataCad=:P13, UserCad=:P14, ');
    Dmod.QrUpdM.SQL.Add('DataDoc=:P15, Vencimento=:P16, CtrlIni=:P17, Nivel=:P18,');
    Dmod.QrUpdM.SQL.Add('Vendedor=:P19, Account=:P20');
    Dmod.QrUpdM.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TPData1.Date);
    Dmod.QrUpdM.Params[01].AsFloat   := Controle2;
    Dmod.QrUpdM.Params[02].AsString  := EdDescricao.Text;
    Dmod.QrUpdM.Params[03].AsInteger := QrLancto1NotaFiscal.Value;
    Dmod.QrUpdM.Params[04].AsFloat   := QrLancto1Debito.Value;
    Dmod.QrUpdM.Params[05].AsFloat   := QrLancto1Credito.Value;
    Dmod.QrUpdM.Params[06].AsString  := CO_VAZIO;
    Dmod.QrUpdM.Params[07].AsFloat   := Doc;
    Dmod.QrUpdM.Params[08].AsInteger := QrLancto1CARTEIRABANCO.Value;
    Dmod.QrUpdM.Params[09].AsInteger := QrLancto1Cliente.Value;
    Dmod.QrUpdM.Params[10].AsInteger := QrLancto1Fornecedor.Value;
    Dmod.QrUpdM.Params[11].AsFloat   := Controle;
    Dmod.QrUpdM.Params[12].AsInteger := Sub;
    Dmod.QrUpdM.Params[13].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpdM.Params[14].AsInteger := VAR_USUARIO;
    Dmod.QrUpdM.Params[15].AsString  := FormatDateTime(VAR_FORMATDATE, QrLancto1DataDoc.Value);
    Dmod.QrUpdM.Params[16].AsString  := FormatDateTime(VAR_FORMATDATE, QrLancto1Vencimento.Value);
    Dmod.QrUpdM.Params[17].AsFloat   := CtrlIni;
    Dmod.QrUpdM.Params[18].AsInteger := Nivel;
    Dmod.QrUpdM.Params[19].AsInteger := QrLancto1Vendedor.Value;
    Dmod.QrUpdM.Params[20].AsInteger := QrLancto1Account.Value;

    Dmod.QrUpdM.ExecSQL;
    }
    UFinanceiro.LancamentoDefaultVARS;
    Controle2 := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'Lanctos', 'Lanctos', 'Controle');
    FLAN_Tipo                   := 1;
    FLAN_Genero                 := -5;
    FLAN_Sit                    := 3;
    FLAN_Data                   := FormatDateTime(VAR_FORMATDATE, TPData1.Date);
    FLAN_Controle               := Controle2;
    FLAN_Descricao              := EdDescricao.Text;
    FLAN_NotaFiscal             := QrLancto1NotaFiscal.Value;
    FLAN_Debito                 := QrLancto1Debito.Value;
    FLAN_Credito                := QrLancto1Credito.Value;
    FLAN_Compensado             := CO_VAZIO;
    FLAN_Documento              := Doc;
    FLAN_Carteira               := QrCarteirasBanco.Value;
    FLAN_Cliente                := QrLancto1Cliente.Value;
    FLAN_Fornecedor             := QrLancto1Fornecedor.Value;
    FLAN_ID_Pgto                := Controle;
    FLAN_Sub                    := Sub;
    FLAN_DataCad                := FormatDateTime(VAR_FORMATDATE, Date);
    FLAN_UserCad                := VAR_USUARIO;
    FLAN_DataDoc                := FormatDateTime(VAR_FORMATDATE, QrLancto1DataDoc.Value);
    FLAN_Vencimento             := FormatDateTime(VAR_FORMATDATE, QrLancto1Vencimento.Value);
    FLAN_CtrlIni                := Trunc(CtrlIni+0.01);
    FLAN_Nivel                  := Nivel;
    FLAN_Vendedor               := QrLancto1Vendedor.Value;
    FLAN_Account                := QrLancto1Account.Value;
    UFinanceiro.InsereLancamento;
    //
    UFinanceiro.RecalcSaldoCarteira(QrCarteirasBanco.Value,
      QrCarteiras, True, True);
  end;
  Close;
end;

procedure TFmQuitaDoc1.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmQuitaDoc1.FormCreate(Sender: TObject);
begin
  FCtrlIni := -1000;
  QrCarteiras.Open;
end;

procedure TFmQuitaDoc1.EdCarteiraChange(Sender: TObject);
begin
  MLAGeral.SincroI(FmQuitaDoc1, Sender, 0, siNegativo);
end;

procedure TFmQuitaDoc1.EdCarteiraExit(Sender: TObject);
begin
  MLAGeral.SincroI(FmQuitaDoc1, Sender, 1, siNegativo);
end;

procedure TFmQuitaDoc1.CBCarteiraClick(Sender: TObject);
begin
  MLAGeral.SincroI(FmQuitaDoc1, Sender, 2, siNegativo);
end;

procedure TFmQuitaDoc1.CBCarteiraKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MLAGeral.SincroI(FmQuitaDoc1, Sender, 3, siNegativo);
end;

end.
