object FmQuitaDoc1: TFmQuitaDoc1
  Left = 487
  Top = 174
  Caption = 'Quita'#231#227'o de documento'
  ClientHeight = 328
  ClientWidth = 329
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 329
    Height = 232
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    ExplicitWidth = 337
    ExplicitHeight = 234
    object Label1: TLabel
      Left = 112
      Top = 140
      Width = 67
      Height = 13
      Caption = 'Defina a data:'
    end
    object Label2: TLabel
      Left = 224
      Top = 140
      Width = 58
      Height = 13
      Caption = 'Documento:'
    end
    object Label3: TLabel
      Left = 12
      Top = 8
      Width = 62
      Height = 13
      Caption = 'Lan'#231'amento:'
    end
    object Label4: TLabel
      Left = 12
      Top = 52
      Width = 39
      Height = 13
      Caption = 'Carteira:'
    end
    object Label5: TLabel
      Left = 132
      Top = 8
      Width = 36
      Height = 13
      Caption = 'Cr'#233'dito:'
    end
    object Label6: TLabel
      Left = 232
      Top = 8
      Width = 34
      Height = 13
      Caption = 'D'#233'bito:'
    end
    object Label7: TLabel
      Left = 12
      Top = 96
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object Label9: TLabel
      Left = 12
      Top = 140
      Width = 53
      Height = 13
      Caption = 'Nota fiscal:'
    end
    object LMDDBLabel1: TLMDDBLabel
      Left = 132
      Top = 24
      Width = 97
      Height = 21
     
      Alignment = agCenterRight
      AutoSize = False
      Options = []
      ShowAccelChar = False
      DataField = 'Credito'
      DataSource = DsLancto1
    end
    object LMDDBLabel3: TLMDDBLabel
      Left = 232
      Top = 24
      Width = 93
      Height = 21
     
      Alignment = agCenterRight
      AutoSize = False
      Options = []
      ShowAccelChar = False
      DataField = 'Debito'
      DataSource = DsLancto1
    end
    object LMDDBLabel6: TLMDDBLabel
      Left = 12
      Top = 156
      Width = 97
      Height = 21
     
      Alignment = agCenterRight
      AutoSize = False
      Options = []
      ShowAccelChar = False
      DataField = 'NotaFiscal'
      DataSource = DsLancto1
    end
    object LMDDBLabel7: TLMDDBLabel
      Left = 12
      Top = 68
      Width = 62
      Height = 21
      Bevel.BorderInnerWidth = 3
     
      Alignment = agCenterRight
      AutoSize = False
      Options = []
      ShowAccelChar = False
      DataField = 'Carteira'
      DataSource = DsLancto1
    end
    object LMDDBLabel5: TLMDDBLabel
      Left = 72
      Top = 68
      Width = 253
      Height = 21
      Bevel.BorderInnerWidth = 2
     
      Alignment = agCenterLeft
      AutoSize = False
      Options = []
      ShowAccelChar = False
      DataField = 'NOMECARTEIRA'
      DataSource = DsLancto1
    end
    object Label15: TLabel
      Left = 12
      Top = 184
      Width = 98
      Height = 13
      Caption = 'Carteira de emiss'#227'o :'
    end
    object SpeedButton1: TSpeedButton
      Left = 302
      Top = 200
      Width = 23
      Height = 22
      Hint = 'Inclui item de carteira'
      Caption = '...'
    end
    object TPData1: TDateTimePicker
      Left = 112
      Top = 156
      Width = 109
      Height = 21
      Date = 37619.114129247700000000
      Time = 37619.114129247700000000
      TabOrder = 1
    end
    object EdDoc: TLMDEdit
      Left = 224
      Top = 156
      Width = 101
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 2
      OnExit = EdDocExit
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
      Text = '0'
    end
    object EdDescricao: TLMDEdit
      Left = 12
      Top = 112
      Width = 313
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 0
      OnExit = EdDocExit
      AutoSelect = True
      CustomButtons = <>
      PasswordChar = #0
    end
    object EdLancto: TLMDStaticText
      Left = 12
      Top = 24
      Width = 89
      Height = 21
      Alignment = agCenterRight
      AutoSize = False
      Bevel.BorderWidth = 1
      Bevel.BorderInnerWidth = 3
     
      Options = []
      ParentColor = True
    end
    object EdSub: TLMDStaticText
      Left = 99
      Top = 24
      Width = 29
      Height = 21
      Alignment = agCenterRight
      AutoSize = False
      Bevel.BorderWidth = 1
      Bevel.BorderInnerWidth = 3
     
      Options = []
      ParentColor = True
    end
    object CBCarteira: TDBLookupComboBox
      Left = 56
      Top = 200
      Width = 245
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCarteiras
      TabOrder = 5
      OnClick = CBCarteiraClick
      OnKeyDown = CBCarteiraKeyDown
    end
    object EdCarteira: TLMDEdit
      Left = 12
      Top = 200
      Width = 41
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 6
      OnChange = EdCarteiraChange
      OnExit = EdCarteiraExit
      AutoSelect = True
      Alignment = taRightJustify
      CustomButtons = <>
      PasswordChar = #0
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 280
    Width = 329
    Height = 48
    Align = alBottom
    TabOrder = 1
    ExplicitTop = 282
    ExplicitWidth = 337
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 12
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
      NumGlyphs = 2
    end
    object BtDesiste: TBitBtn
      Tag = 15
      Left = 236
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 329
    Height = 48
    Align = alTop
    
    
    
    
    Caption = 'Quita'#231#227'o de Documento 1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2

    ExplicitWidth = 337
    object Image1: TImage
      Left = 2
      Top = 2
      Width = 333
      Height = 44
      Align = alClient
      Transparent = True
    end
  end
  object QrLancto1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT la.Controle, la.Sub, la.Descricao, la.Credito, la.Debito,'
      'la.NotaFiscal, la.Documento, la.Vencimento, la.Carteira, Data,'
      'ca.Nome NOMECARTEIRA, ca.Tipo TIPOCARTEIRA ,'
      'ca.Banco CARTEIRABANCO, la.Cliente, la.Fornecedor, '
      'la.DataDoc, la.Nivel, la.CtrlIni, la.Vendedor, la.Account'
      'FROM lanctos la, Carteiras ca'
      'WHERE la.Controle=:P0'
      'AND la.Sub=:P1'
      'AND ca.Codigo=la.Carteira')
    Left = 168
    Top = 56
    ParamData = <
      item
        DataType = ftFloat
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLancto1Sub: TSmallintField
      FieldName = 'Sub'
      Origin = 'DBMMONEY.lanctos.Sub'
    end
    object QrLancto1Descricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'DBMMONEY.lanctos.Descricao'
      Size = 128
    end
    object QrLancto1Credito: TFloatField
      FieldName = 'Credito'
      Origin = 'DBMMONEY.lanctos.Credito'
      DisplayFormat = '#,###,##0.00  '
    end
    object QrLancto1Debito: TFloatField
      FieldName = 'Debito'
      Origin = 'DBMMONEY.lanctos.Debito'
      DisplayFormat = '#,###,##0.00  '
    end
    object QrLancto1NotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'DBMMONEY.lanctos.NotaFiscal'
    end
    object QrLancto1Documento: TFloatField
      FieldName = 'Documento'
      Origin = 'DBMMONEY.lanctos.Documento'
    end
    object QrLancto1Vencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'DBMMONEY.lanctos.Vencimento'
    end
    object QrLancto1Carteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'DBMMONEY.lanctos.Carteira'
    end
    object QrLancto1Data: TDateField
      FieldName = 'Data'
      Origin = 'DBMMONEY.lanctos.Data'
    end
    object QrLancto1NOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
    object QrLancto1TIPOCARTEIRA: TIntegerField
      FieldName = 'TIPOCARTEIRA'
      Origin = 'DBMMONEY.carteiras.Tipo'
    end
    object QrLancto1CARTEIRABANCO: TIntegerField
      FieldName = 'CARTEIRABANCO'
      Origin = 'DBMMONEY.carteiras.Banco'
    end
    object QrLancto1Cliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'DBMMONEY.lanctos.Cliente'
    end
    object QrLancto1Fornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Origin = 'DBMMONEY.lanctos.Fornecedor'
    end
    object QrLancto1DataDoc: TDateField
      FieldName = 'DataDoc'
      Origin = 'DBMMONEY.lanctos.DataDoc'
    end
    object QrLancto1Nivel: TIntegerField
      FieldName = 'Nivel'
      Origin = 'DBMMONEY.lanctos.Nivel'
    end
    object QrLancto1Vendedor: TIntegerField
      FieldName = 'Vendedor'
      Origin = 'DBMMONEY.lanctos.Vendedor'
    end
    object QrLancto1Account: TIntegerField
      FieldName = 'Account'
      Origin = 'DBMMONEY.lanctos.Account'
    end
    object QrLancto1Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLancto1CtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
  end
  object DsLancto1: TDataSource
    DataSet = QrLancto1
    Left = 196
    Top = 56
  end
  object QrLancto2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Tipo, Credito, Debito, Controle, Sub, Carteira'
      'FROM lanctos '
      'WHERE ID_Pgto=:P0 '
      'AND ID_Sub=:P1')
    Left = 168
    Top = 84
    ParamData = <
      item
        DataType = ftFloat
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLancto2Tipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'DBMMONEY.lanctos.Tipo'
    end
    object QrLancto2Credito: TFloatField
      FieldName = 'Credito'
      Origin = 'DBMMONEY.lanctos.Credito'
    end
    object QrLancto2Debito: TFloatField
      FieldName = 'Debito'
      Origin = 'DBMMONEY.lanctos.Debito'
    end
    object QrLancto2Sub: TSmallintField
      FieldName = 'Sub'
      Origin = 'DBMMONEY.lanctos.Sub'
    end
    object QrLancto2Carteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'DBMMONEY.lanctos.Carteira'
    end
    object QrLancto2Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsLancto2: TDataSource
    DataSet = QrLancto2
    Left = 196
    Top = 84
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Banco'
      'FROM carteiras '
      'WHERE Tipo=2'
      'ORDER BY Nome')
    Left = 204
    Top = 240
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteirasBanco: TIntegerField
      FieldName = 'Banco'
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 232
    Top = 240
  end
end


