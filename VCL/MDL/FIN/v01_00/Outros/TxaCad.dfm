object FmTxaCad: TFmTxaCad
  Left = 368
  Top = 194
  Caption = 'Taxas Administrativas'
  ClientHeight = 348
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 300
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 251
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
        NumGlyphs = 2
      end
      object Panel1: TPanel
        Left = 681
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
          NumGlyphs = 2
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 196
      Align = alTop
      TabOrder = 1
      object Label9: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 68
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object EdCodigo: TEdit
        Left = 16
        Top = 24
        Width = 48
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        Text = '00'
      end
      object EdNome: TEdit
        Left = 68
        Top = 24
        Width = 713
        Height = 21
        TabOrder = 1
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 300
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelControle: TPanel
      Left = 1
      Top = 251
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TdmkLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
        UpdType = utYes
        SQLType = stNil
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
          NumGlyphs = 2
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
          NumGlyphs = 2
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
          NumGlyphs = 2
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
          NumGlyphs = 2
        end
      end
      object Panel3: TPanel
        Left = 320
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtCond: TBitBtn
          Tag = 10005
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cond.'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtCondClick
          NumGlyphs = 2
        end
        object BtTaxa: TBitBtn
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Taxa'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtTaxaClick
          NumGlyphs = 2
        end
        object Panel2: TPanel
          Left = 360
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
            NumGlyphs = 2
          end
        end
      end
    end
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 52
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 68
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TDBEdit
        Left = 16
        Top = 24
        Width = 48
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsTxaCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 68
        Top = 24
        Width = 713
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsTxaCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
    end
    object dmkDBGrid1: TdmkDBGrid
      Left = 1
      Top = 131
      Width = 790
      Height = 120
      Align = alBottom
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMECLII'
          Title.Caption = 'Entidade competente'
          Width = 302
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMENIVEL'
          Title.Caption = 'N'#237'vel'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECODIGO'
          Title.Caption = 'Item selecionado do n'#237'vel'
          Width = 314
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TaxaPerc'
          Title.Caption = '% taxa'
          Visible = True
        end>
      Color = clWindow
      DataSource = DsTxaAdm
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMECLII'
          Title.Caption = 'Entidade competente'
          Width = 302
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMENIVEL'
          Title.Caption = 'N'#237'vel'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECODIGO'
          Title.Caption = 'Item selecionado do n'#237'vel'
          Width = 314
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TaxaPerc'
          Title.Caption = '% taxa'
          Visible = True
        end>
    end
  end
  object PainelItens: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 300
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Visible = False
    object Panel6: TPanel
      Left = 1
      Top = 251
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BitBtn1: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BitBtn1Click
        NumGlyphs = 2
      end
      object Panel7: TPanel
        Left = 681
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BitBtn2: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn2Click
          NumGlyphs = 2
        end
      end
    end
    object Panel8: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 228
      Align = alTop
      TabOrder = 0
      object Panel9: TPanel
        Left = 1
        Top = 1
        Width = 788
        Height = 52
        Align = alTop
        TabOrder = 0
        object Label7: TLabel
          Left = 16
          Top = 8
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdit1
        end
        object Label8: TLabel
          Left = 68
          Top = 8
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DDBEdit2
        end
        object DBEdit1: TDBEdit
          Left = 16
          Top = 24
          Width = 48
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsTxaCad
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
        end
        object DDBEdit2: TDBEdit
          Left = 68
          Top = 24
          Width = 713
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsTxaCad
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
      end
      object Panel10: TPanel
        Left = 1
        Top = 53
        Width = 788
        Height = 116
        Align = alTop
        TabOrder = 1
        object Label11: TLabel
          Left = 16
          Top = 4
          Width = 60
          Height = 13
          Caption = 'Condom'#237'nio:'
        end
        object Label3: TLabel
          Left = 528
          Top = 44
          Width = 54
          Height = 13
          Caption = 'Percentual:'
        end
        object Label12: TLabel
          Left = 16
          Top = 44
          Width = 59
          Height = 13
          Caption = 'Item do ???:'
        end
        object EdEmpresa: TdmkEditCB
          Left = 16
          Top = 20
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdEmpresaChange
          DBLookupComboBox = CBEmpresa
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 72
          Top = 20
          Width = 537
          Height = 21
          Color = clWhite
          KeyField = 'CodCond'
          ListField = 'NOMECOND'
          ListSource = DsEntiCond
          TabOrder = 1
          dmkEditCB = EdEmpresa
          UpdType = utYes
        end
        object dmkEdTaxaPerc: TdmkEdit
          Left = 528
          Top = 60
          Width = 80
          Height = 21
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '0'
        end
        object RGNivel: TRadioGroup
          Left = 612
          Top = 4
          Width = 169
          Height = 77
          Caption = ' N'#237'vel do plano: '
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Nenhum'
            'Conta'
            'Sub-grupo'
            'Grupo'
            'Conjunto'
            'Plano')
          TabOrder = 2
          OnClick = RGNivelClick
        end
        object CBCodiNivel: TdmkDBLookupComboBox
          Left = 72
          Top = 60
          Width = 453
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCodiNivel
          TabOrder = 4
          dmkEditCB = EdCodiNivel
          UpdType = utYes
        end
        object EdCodiNivel: TdmkEditCB
          Left = 16
          Top = 60
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdCodiNivelChange
          DBLookupComboBox = CBCodiNivel
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Caption = '                              Taxas Administrativas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TLabel
      Left = 709
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 708
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 483
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 481
      ExplicitHeight = 44
    end
    object PanelFill002: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 0
        NumGlyphs = 2
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 1
        NumGlyphs = 2
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 2
        OnClick = SbNumeroClick
        NumGlyphs = 2
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 3
        OnClick = SbNomeClick
        NumGlyphs = 2
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 4
        OnClick = SbQueryClick
        NumGlyphs = 2
      end
    end
  end
  object DsTxaCad: TDataSource
    DataSet = QrTxaCad
    Left = 552
    Top = 9
  end
  object QrTxaCad: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrTxaCadBeforeOpen
    AfterOpen = QrTxaCadAfterOpen
    AfterScroll = QrTxaCadAfterScroll
    SQL.Strings = (
      'SELECT * FROM TxaCad'
      'WHERE Codigo > 0')
    Left = 524
    Top = 9
    object QrTxaCadNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'txacad.Nome'
      Size = 50
    end
    object QrTxaCadLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'txacad.Lk'
    end
    object QrTxaCadDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'txacad.DataCad'
    end
    object QrTxaCadDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'txacad.DataAlt'
    end
    object QrTxaCadUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'txacad.UserCad'
    end
    object QrTxaCadUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'txacad.UserAlt'
    end
    object QrTxaCadAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'txacad.AlterWeb'
    end
    object QrTxaCadAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'txacad.Ativo'
    end
    object QrTxaCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'txacad.Codigo'
    end
  end
  object PMTaxa: TPopupMenu
    Left = 340
    Top = 256
    object Crianovogrupo1: TMenuItem
      Caption = '&Cria nova taxa'
      OnClick = Crianovogrupo1Click
    end
    object Alteragrupoatual1: TMenuItem
      Caption = '&Altera taxa atual'
      OnClick = Alteragrupoatual1Click
    end
    object Excluigrupoatual1: TMenuItem
      Caption = '&Exclui taxa atual'
      Enabled = False
      OnClick = Excluigrupoatual1Click
    end
  end
  object DsCodiNivel: TDataSource
    DataSet = QrCodiNivel
    Left = 253
    Top = 61
  end
  object QrCodiNivel: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'ORDER BY Nome'
      '')
    Left = 225
    Top = 61
    object QrCodiNivelCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCodiNivelNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsEntiCond: TDataSource
    DataSet = QrEntiCond
    Left = 197
    Top = 61
  end
  object QrEntiCond: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cnd.Codigo CodCond, ent.Codigo CodEnti,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECOND '
      'FROM entidades ent'
      'LEFT JOIN cond cnd ON ent.Codigo=cnd.Cliente'
      'WHERE ent.Cliente1="V"'
      'ORDER BY NOMECOND')
    Left = 169
    Top = 61
    object QrEntiCondCodCond: TIntegerField
      FieldName = 'CodCond'
      Origin = 'cond.Codigo'
    end
    object QrEntiCondCodEnti: TIntegerField
      FieldName = 'CodEnti'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrEntiCondNOMECOND: TWideStringField
      FieldName = 'NOMECOND'
      Origin = 'NOMECOND'
      Required = True
      Size = 100
    end
  end
  object PMCond: TPopupMenu
    Left = 452
    Top = 260
    object AdicionaUHaogrupo1: TMenuItem
      Caption = '&Adiciona condom'#237'nio '#224' taxa'
      OnClick = AdicionaUHaogrupo1Click
    end
    object Alterataxadoitemselecionado1: TMenuItem
      Caption = 'Altera taxa do item selecionado'
      OnClick = Alterataxadoitemselecionado1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object RetiraUHselecionadadogrupo1: TMenuItem
      Caption = '&Retira o condom'#237'nio selecionado da taxa'
      OnClick = RetiraUHselecionadadogrupo1Click
    end
  end
  object QrTxaAdm: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT taa.*, ELT(taa.Nivel, '#39'Conta'#39', '
      #39'Sub-grupo'#39', '#39'Grupo'#39', '#39'Conjunto'#39', '#39'Plano'#39') NOMENIVEL, '
      'ELT(taa.Nivel, cta.Nome, sgr.Nome, gru.Nome, '
      'cjt.Nome, pla.Nome) NOMECODIGO, IF(cli.Tipo=0, '
      'cli.RazaoSocial, cli.Nome) NOMECLII, cnd.Codigo COND'
      'FROM txaadm taa'
      'LEFT JOIN contas    cta ON cta.Codigo=taa.CodiNivel'
      'LEFT JOIN subgrupos sgr ON sgr.Codigo=taa.CodiNivel'
      'LEFT JOIN grupos    gru ON gru.Codigo=taa.CodiNivel'
      'LEFT JOIN conjuntos cjt ON cjt.Codigo=taa.CodiNivel'
      'LEFT JOIN plano     pla ON pla.Codigo=taa.CodiNivel'
      'LEFT JOIN entidades cli ON cli.Codigo=taa.Empresa'
      'LEFT JOIN cond      cnd ON cnd.Cliente=taa.Empresa'
      ''
      'WHERE taa.Codigo=:P0')
    Left = 582
    Top = 10
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTxaAdmCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'txaadm.Codigo'
      Required = True
    end
    object QrTxaAdmControle: TAutoIncField
      FieldName = 'Controle'
      Origin = 'txaadm.Controle'
    end
    object QrTxaAdmEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'txaadm.Empresa'
      Required = True
    end
    object QrTxaAdmNivel: TSmallintField
      FieldName = 'Nivel'
      Origin = 'txaadm.Nivel'
      Required = True
    end
    object QrTxaAdmCodiNivel: TIntegerField
      FieldName = 'CodiNivel'
      Origin = 'txaadm.CodiNivel'
      Required = True
    end
    object QrTxaAdmTaxaPerc: TFloatField
      FieldName = 'TaxaPerc'
      Origin = 'txaadm.TaxaPerc'
      Required = True
      DisplayFormat = '#,#00.0000;-#,#00.0000; '
    end
    object QrTxaAdmLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'txaadm.Lk'
    end
    object QrTxaAdmDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'txaadm.DataCad'
    end
    object QrTxaAdmDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'txaadm.DataAlt'
    end
    object QrTxaAdmUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'txaadm.UserCad'
    end
    object QrTxaAdmUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'txaadm.UserAlt'
    end
    object QrTxaAdmAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'txaadm.AlterWeb'
      Required = True
    end
    object QrTxaAdmAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'txaadm.Ativo'
      Required = True
    end
    object QrTxaAdmNOMENIVEL: TWideStringField
      FieldName = 'NOMENIVEL'
      Size = 9
    end
    object QrTxaAdmNOMECODIGO: TWideStringField
      FieldName = 'NOMECODIGO'
      Size = 50
    end
    object QrTxaAdmNOMECLII: TWideStringField
      FieldName = 'NOMECLII'
      Size = 100
    end
    object QrTxaAdmCOND: TIntegerField
      FieldName = 'COND'
      Required = True
    end
  end
  object DsTxaAdm: TDataSource
    DataSet = QrTxaAdm
    Left = 610
    Top = 10
  end
end
