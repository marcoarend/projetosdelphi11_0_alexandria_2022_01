unit LanctosCondDuplic;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     Grids, DBGrids;

type
  TFmLanctosCondDuplic = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    STNotaF: TStaticText;
    STCheque: TStaticText;
    DBGrid2: TDBGrid;
    DBGrid1: TDBGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FConfirma: Boolean;
  end;

  var
  FmLanctosCondDuplic: TFmLanctosCondDuplic;

implementation

uses LanctosCond;

{$R *.DFM}

procedure TFmLanctosCondDuplic.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLanctosCondDuplic.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLanctosCondDuplic.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLanctosCondDuplic.FormCreate(Sender: TObject);
begin
  FConfirma := False;
end;

procedure TFmLanctosCondDuplic.BtOKClick(Sender: TObject);
begin
  FConfirma := True;
  Close;
end;

end.
