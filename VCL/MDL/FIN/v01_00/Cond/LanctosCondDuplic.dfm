object FmLanctosCondDuplic: TFmLanctosCondDuplic
  Left = 352
  Top = 174
  Width = 1024
  Height = 530
  Caption = 'Duplica��o de Lan�amento'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 448
    Width = 1016
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 140
      Height = 40
      Caption = '&Insere assim mesmo'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 856
      Top = 1
      Width = 159
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 2
        Top = 3
        Width = 140
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste de inserir'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1016
    Height = 48
    Align = alTop
    
    
    
    
    Caption = 'Duplica��o de Lan�amento'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2

    object Image1: TImage
      Left = 2
      Top = 2
      Width = 1012
      Height = 44
      Align = alClient
      Transparent = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1016
    Height = 400
    Align = alClient
    TabOrder = 0
    object STNotaF: TStaticText
      Left = 1
      Top = 245
      Width = 1014
      Height = 17
      Align = alTop
      Alignment = taCenter
      Caption = 'STNotaF'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object STCheque: TStaticText
      Left = 1
      Top = 1
      Width = 1014
      Height = 17
      Align = alTop
      Alignment = taCenter
      Caption = 'STCheque'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object DBGrid2: TDBGrid
      Left = 1
      Top = 18
      Width = 1014
      Height = 227
      Align = alTop
      DataSource = FmLanctos2.DsDuplCH
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CHEQUE'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Compensado'
          Title.Caption = 'Compens.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Cr�dito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D�bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NotaFiscal'
          Title.Caption = 'N.F.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri��o'
          Width = 280
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MES'
          Title.Caption = 'M�s'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TERCEIRO'
          Title.Caption = 'Fornecedor / Cliente'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECART'
          Title.Caption = 'Carteira'
          Width = 140
          Visible = True
        end>
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 262
      Width = 1014
      Height = 137
      Align = alClient
      DataSource = FmLanctos2.DsDuplNF
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CHEQUE'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Compensado'
          Title.Caption = 'Compens.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Cr�dito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D�bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NotaFiscal'
          Title.Caption = 'N.F.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri��o'
          Width = 280
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MES'
          Title.Caption = 'M�s'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TERCEIRO'
          Title.Caption = 'Fornecedor / Cliente'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECART'
          Title.Caption = 'Carteira'
          Width = 140
          Visible = True
        end>
    end
  end
end
