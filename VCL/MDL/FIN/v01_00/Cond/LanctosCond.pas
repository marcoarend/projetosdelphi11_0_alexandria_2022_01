unit LanctosCond;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls,   UnMLAGeral, UnGOTOy,
      ComCtrls, UnMyLinguas,
  Db, mySQLDbTables, ExtCtrls,  
   Buttons, UnInternalConsts, UMySQLModule, Grids, DBGrids,
     
   LMDDBLabel, LMDCustomGroupBox, LMDCustomButtonGroup,
  LMDCustomCheckGroup, LMDCheckGroup, frxChBox, frxClass,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkEditDateTimePicker,
  dmkRadioGroup, Variants;

type
  TTipoValor = (tvNil, tvCred, tvDeb);
  TFmLanctosCond = class(TForm)
    DsContas: TDataSource;
    DsCarteiras: TDataSource;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    DsFornecedores: TDataSource;
    DsClientes: TDataSource;
    DsNF: TDataSource;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    QrContas: TmySQLQuery;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasFechamento: TIntegerField;
    QrFatura: TmySQLQuery;
    QrFaturaData: TDateField;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasNome3: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrContasDataCad: TDateField;
    QrContasDataAlt: TDateField;
    QrContasUserCad: TSmallintField;
    QrContasUserAlt: TSmallintField;
    QrContasNOMESUBGRUPO: TWideStringField;
    QrContasNOMEGRUPO: TWideStringField;
    QrContasNOMECONJUNTO: TWideStringField;
    QrContasNOMEEMPRESA: TWideStringField;
    QrNF: TmySQLQuery;
    QrNFCONTA: TWideStringField;
    QrNFNOMECLIENTE: TWideStringField;
    QrClientes: TmySQLQuery;
    QrFornecedores: TmySQLQuery;
    QrFornecedoresCodigo: TIntegerField;
    QrFornecedoresNOMEENTIDADE: TWideStringField;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    QrVendedores: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsVendedores: TDataSource;
    DsAccounts: TDataSource;
    QrAccounts: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    QrClientesAccount: TIntegerField;
    QrNFData: TDateField;
    QrNFTipo: TSmallintField;
    QrNFCarteira: TIntegerField;
    QrNFControle: TIntegerField;
    QrNFSub: TSmallintField;
    QrNFAutorizacao: TIntegerField;
    QrNFGenero: TIntegerField;
    QrNFDescricao: TWideStringField;
    QrNFNotaFiscal: TIntegerField;
    QrNFDebito: TFloatField;
    QrNFCredito: TFloatField;
    QrNFCompensado: TDateField;
    QrNFDocumento: TFloatField;
    QrNFSit: TIntegerField;
    QrNFVencimento: TDateField;
    QrNFLk: TIntegerField;
    QrNFFatID: TIntegerField;
    QrNFFatParcela: TIntegerField;
    QrNFID_Pgto: TIntegerField;
    QrNFID_Sub: TSmallintField;
    QrNFFatura: TWideStringField;
    QrNFBanco: TIntegerField;
    QrNFLocal: TIntegerField;
    QrNFCartao: TIntegerField;
    QrNFLinha: TIntegerField;
    QrNFOperCount: TIntegerField;
    QrNFLancto: TIntegerField;
    QrNFPago: TFloatField;
    QrNFMez: TIntegerField;
    QrNFFornecedor: TIntegerField;
    QrNFCliente: TIntegerField;
    QrNFMoraDia: TFloatField;
    QrNFMulta: TFloatField;
    QrNFProtesto: TDateField;
    QrNFDataCad: TDateField;
    QrNFDataAlt: TDateField;
    QrNFUserCad: TSmallintField;
    QrNFUserAlt: TSmallintField;
    QrNFDataDoc: TDateField;
    QrNFCtrlIni: TIntegerField;
    QrNFNivel: TIntegerField;
    QrNFVendedor: TIntegerField;
    QrNFAccount: TIntegerField;
    QrFunci: TmySQLQuery;
    DsFunci: TDataSource;
    QrFunciCodigo: TIntegerField;
    QrFunciNOMEENTIDADE: TWideStringField;
    CkContinuar: TCheckBox;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrCliIntCodigo: TIntegerField;
    QrCliIntNOMEENTIDADE: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PainelDados: TPanel;
    LaFunci: TLabel;
    QrLanctos: TmySQLQuery;
    QrLanctosData: TDateField;
    QrLanctosTipo: TSmallintField;
    QrLanctosCarteira: TIntegerField;
    QrLanctosAutorizacao: TIntegerField;
    QrLanctosGenero: TIntegerField;
    QrLanctosDescricao: TWideStringField;
    QrLanctosNotaFiscal: TIntegerField;
    QrLanctosDebito: TFloatField;
    QrLanctosCredito: TFloatField;
    QrLanctosCompensado: TDateField;
    QrLanctosDocumento: TFloatField;
    QrLanctosSit: TIntegerField;
    QrLanctosVencimento: TDateField;
    QrLanctosLk: TIntegerField;
    QrLanctosFatID: TIntegerField;
    QrLanctosFatParcela: TIntegerField;
    QrLanctosCONTA: TIntegerField;
    QrLanctosNOMECONTA: TWideStringField;
    QrLanctosNOMEEMPRESA: TWideStringField;
    QrLanctosNOMESUBGRUPO: TWideStringField;
    QrLanctosNOMEGRUPO: TWideStringField;
    QrLanctosNOMECONJUNTO: TWideStringField;
    QrLanctosNOMESIT: TWideStringField;
    QrLanctosAno: TFloatField;
    QrLanctosMENSAL: TWideStringField;
    QrLanctosMENSAL2: TWideStringField;
    QrLanctosBanco: TIntegerField;
    QrLanctosLocal: TIntegerField;
    QrLanctosFatura: TWideStringField;
    QrLanctosSub: TSmallintField;
    QrLanctosCartao: TIntegerField;
    QrLanctosLinha: TIntegerField;
    QrLanctosPago: TFloatField;
    QrLanctosSALDO: TFloatField;
    QrLanctosID_Sub: TSmallintField;
    QrLanctosMez: TIntegerField;
    QrLanctosFornecedor: TIntegerField;
    QrLanctoscliente: TIntegerField;
    QrLanctosMoraDia: TFloatField;
    QrLanctosNOMECLIENTE: TWideStringField;
    QrLanctosNOMEFORNECEDOR: TWideStringField;
    QrLanctosTIPOEM: TWideStringField;
    QrLanctosNOMERELACIONADO: TWideStringField;
    QrLanctosOperCount: TIntegerField;
    QrLanctosLancto: TIntegerField;
    QrLanctosMulta: TFloatField;
    QrLanctosATRASO: TFloatField;
    QrLanctosJUROS: TFloatField;
    QrLanctosDataDoc: TDateField;
    QrLanctosNivel: TIntegerField;
    QrLanctosVendedor: TIntegerField;
    QrLanctosAccount: TIntegerField;
    QrLanctosMes2: TLargeintField;
    QrLanctosProtesto: TDateField;
    QrLanctosDataCad: TDateField;
    QrLanctosDataAlt: TDateField;
    QrLanctosUserCad: TSmallintField;
    QrLanctosUserAlt: TSmallintField;
    QrLanctosControle: TIntegerField;
    QrLanctosID_Pgto: TIntegerField;
    QrLanctosCtrlIni: TIntegerField;
    QrLanctosFatID_Sub: TIntegerField;
    QrLanctosICMS_P: TFloatField;
    QrLanctosICMS_V: TFloatField;
    QrLanctosDuplicata: TWideStringField;
    QrLanctosCOMPENSADO_TXT: TWideStringField;
    QrLanctosCliInt: TIntegerField;
    QrLanctosNOMECARTEIRA: TWideStringField;
    QrLanctosSALDOCARTEIRA: TFloatField;
    DsLanctos: TDataSource;
    TabSheet2: TTabSheet;
    DsDeptos: TDataSource;
    QrDeptos: TmySQLQuery;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasTipo: TIntegerField;
    Label20: TLabel;
    QrCliCli: TmySQLQuery;
    DsCliCli: TDataSource;
    QrCliCliCodigo: TIntegerField;
    QrCliCliNOMEENTIDADE: TWideStringField;
    TabSheet3: TTabSheet;
    QrCarteirasExigeNumCheque: TSmallintField;
    CkCopiaCH: TCheckBox;
    BitBtn1: TBitBtn;
    QrDuplCH: TmySQLQuery;
    DsDuplCH: TDataSource;
    DsDuplNF: TDataSource;
    QrDuplNF: TmySQLQuery;
    QrDuplCHData: TDateField;
    QrDuplCHControle: TIntegerField;
    QrDuplCHDescricao: TWideStringField;
    QrDuplCHCredito: TFloatField;
    QrDuplCHDebito: TFloatField;
    QrDuplCHNotaFiscal: TIntegerField;
    QrDuplCHCompensado: TDateField;
    QrDuplCHMez: TIntegerField;
    QrDuplCHFornecedor: TIntegerField;
    QrDuplCHCliente: TIntegerField;
    QrDuplCHNOMECART: TWideStringField;
    QrDuplCHNOMECLI: TWideStringField;
    QrDuplCHNOMEFNC: TWideStringField;
    QrDuplCHDocumento: TFloatField;
    QrDuplCHSerieCH: TWideStringField;
    QrDuplCHCarteira: TIntegerField;
    QrDuplCHTERCEIRO: TWideStringField;
    QrDuplCHMES: TWideStringField;
    QrDuplCHCHEQUE: TWideStringField;
    QrDuplCHCOMP_TXT: TWideStringField;
    QrDuplNFData: TDateField;
    QrDuplNFControle: TIntegerField;
    QrDuplNFDescricao: TWideStringField;
    QrDuplNFCredito: TFloatField;
    QrDuplNFDebito: TFloatField;
    QrDuplNFNotaFiscal: TIntegerField;
    QrDuplNFCompensado: TDateField;
    QrDuplNFMez: TIntegerField;
    QrDuplNFFornecedor: TIntegerField;
    QrDuplNFCliente: TIntegerField;
    QrDuplNFNOMECART: TWideStringField;
    QrDuplNFNOMECLI: TWideStringField;
    QrDuplNFNOMEFNC: TWideStringField;
    QrDuplNFDocumento: TFloatField;
    QrDuplNFSerieCH: TWideStringField;
    QrDuplNFCarteira: TIntegerField;
    QrDuplNFTERCEIRO: TWideStringField;
    QrDuplNFMES: TWideStringField;
    QrDuplNFCHEQUE: TWideStringField;
    QrDuplNFCOMP_TXT: TWideStringField;
    Panel2: TPanel;
    Label15: TLabel;
    Label14: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    SpeedButton1: TSpeedButton;
    BtContas: TSpeedButton;
    LaVencimento: TLabel;
    Label11: TLabel;
    LaNF: TLabel;
    LaCred: TLabel;
    LaDeb: TLabel;
    LaMes: TLabel;
    Label13: TLabel;
    LaCliente: TLabel;
    LaFornecedor: TLabel;
    Label3: TLabel;
    LaCliInt: TLabel;
    LaDoc: TLabel;
    LaForneceRN: TLabel;
    LaForneceFA: TLabel;
    LaDepto: TLabel;
    LaCliCli: TLabel;
    SpeedButton2: TSpeedButton;
    CkPesqCH: TCheckBox;
    CkPesqNF: TCheckBox;
    Panel4: TPanel;
    BtDesiste: TBitBtn;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    PnMaskPesq: TPanel;
    Panel5: TPanel;
    EdLinkMask: TEdit;
    Label18: TLabel;
    QrPesqCodigo: TIntegerField;
    QrPesqNome: TWideStringField;
    LLBPesq: TDBLookupListBox;
    PnLink: TPanel;
    Panel6: TPanel;
    SpeedButton3: TSpeedButton;
    TabSheet4: TTabSheet;
    TabControl1: TTabControl;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    Label9: TLabel;
    Label22: TLabel;
    Panel3: TPanel;
    Label10: TLabel;
    LaVendedor: TLabel;
    LaAccount: TLabel;
    LaICMS_P: TLabel;
    LaICMS_V: TLabel;
    Panel7: TPanel;
    GBParcelamento: TGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    RGArredondar: TRadioGroup;
    RGPeriodo: TRadioGroup;
    EdDias: TLMDEdit;
    RGIncremCH: TRadioGroup;
    EdParcela1: TLMDEdit;
    EdParcelaX: TLMDEdit;
    CkArredondar: TCheckBox;
    EdSoma: TLMDEdit;
    CkParcelamento: TCheckBox;
    DBGParcelas: TDBGrid;
    TbParcpagtos: TmySQLTable;
    TbParcpagtosParcela: TIntegerField;
    TbParcpagtosData: TDateField;
    TbParcpagtosCredito: TFloatField;
    TbParcpagtosDebito: TFloatField;
    TbParcpagtosDoc: TLargeintField;
    TbParcpagtosMora: TFloatField;
    TbParcpagtosMulta: TFloatField;
    TbParcpagtosICMS_V: TFloatField;
    TbParcpagtosDuplicata: TWideStringField;
    TbParcpagtosDescricao: TWideStringField;
    DsParcPagtos: TDataSource;
    QrSoma: TmySQLQuery;
    QrSomaVALOR: TFloatField;
    CkIncremDU: TCheckBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label8: TLabel;
    Label7: TLabel;
    GroupBox3: TGroupBox;
    Label19: TLabel;
    Label21: TLabel;
    GroupBox4: TGroupBox;
    Label28: TLabel;
    Label29: TLabel;
    BitBtn2: TBitBtn;
    RGIncremDupl: TGroupBox;
    Label27: TLabel;
    EdDuplSep: TEdit;
    RGDuplSeq: TRadioGroup;
    GBIncremTxt: TGroupBox;
    Label30: TLabel;
    EdSepTxt: TEdit;
    RGSepTxt: TRadioGroup;
    CkIncremTxt: TCheckBox;
    QrDeptosCODI_1: TIntegerField;
    QrDeptosCODI_2: TLargeintField;
    QrDeptosNOME_1: TWideStringField;
    TabSheet5: TTabSheet;
    dmkEdCBGenero: TdmkEditCB;
    dmkCBGenero: TdmkDBLookupComboBox;
    dmkEdControle: TdmkEdit;
    dmkEdCarteira: TdmkEditCB;
    dmkCBCarteira: TdmkDBLookupComboBox;
    dmkEdMes: TdmkEdit;
    dmkEdDeb: TdmkEdit;
    dmkEdCred: TdmkEdit;
    dmkEdNF: TdmkEdit;
    dmkEdSerieCH: TdmkEdit;
    dmkEdDoc: TdmkEdit;
    dmkEdDuplicata: TdmkEdit;
    dmkEdTPData: TdmkEditDateTimePicker;
    dmkEdTPVencto: TdmkEditDateTimePicker;
    dmkEdCBCliInt: TdmkEditCB;
    dmkCBCliInt: TdmkDBLookupComboBox;
    Panel8: TPanel;
    Panel9: TPanel;
    Label4: TLabel;
    LaFinalidade: TLabel;
    MeConfig: TMemo;
    dmkEdCBCliente: TdmkEditCB;
    dmkCBCliente: TdmkDBLookupComboBox;
    dmkEdCBFornece: TdmkEditCB;
    dmkCBFornece: TdmkDBLookupComboBox;
    dmkCBDepto: TdmkDBLookupComboBox;
    dmkEdCBDepto: TdmkEditCB;
    dmkEdCBCliCli: TdmkEditCB;
    dmkCBCliCli: TdmkDBLookupComboBox;
    dmkEdQtde: TdmkEdit;
    dmkEdDescricao: TdmkEdit;
    dmkEdTPDataDoc: TdmkEditDateTimePicker;
    dmkEdTPCompensado: TdmkEditDateTimePicker;
    dmkEdMoraDia: TdmkEdit;
    dmkEdMulta: TdmkEdit;
    dmkEdMultaVal: TdmkEdit;
    dmkEdMoraVal: TdmkEdit;
    dmkEdValNovo: TdmkEdit;
    dmkEdPerMult: TdmkEdit;
    dmkCBFunci: TdmkDBLookupComboBox;
    dmkEdCBFunci: TdmkEditCB;
    dmkEdCBVendedor: TdmkEditCB;
    dmkCBVendedor: TdmkDBLookupComboBox;
    dmkEdCBAccount: TdmkEditCB;
    dmkCBAccount: TdmkDBLookupComboBox;
    dmkEdICMS_P: TdmkEdit;
    dmkEdICMS_V: TdmkEdit;
    dmkEdParcelas: TdmkEdit;
    Panel10: TPanel;
    Label5: TLabel;
    dmkEdCNAB_Sit: TdmkEdit;
    Label6: TLabel;
    dmkEdID_pgto: TdmkEdit;
    dmkEdFatID: TdmkEdit;
    Label12: TLabel;
    Label16: TLabel;
    dmkEdTipoCH: TdmkEdit;
    dmkEdFatNum: TdmkEdit;
    Label17: TLabel;
    Label31: TLabel;
    dmkEdFatID_Sub: TdmkEdit;
    dmkEdNivel: TdmkEdit;
    Label32: TLabel;
    Label33: TLabel;
    dmkEdFatParcela: TdmkEdit;
    dmkEdCtrlIni: TdmkEdit;
    Label34: TLabel;
    Label35: TLabel;
    dmkEdDescoPor: TdmkEdit;
    dmkEdDescoVal: TdmkEdit;
    Label36: TLabel;
    Label37: TLabel;
    dmkEdUnidade: TdmkEdit;
    dmkEdDoc2: TdmkEdit;
    Label38: TLabel;
    Label39: TLabel;
    dmkEdNFVal: TdmkEdit;
    Label40: TLabel;
    Label41: TLabel;
    dmkEdTipoAnt: TdmkEdit;
    dmkEdCartAnt: TdmkEdit;
    Label42: TLabel;
    dmkEdPercJuroM: TdmkEdit;
    dmkEdPercMulta: TdmkEdit;
    Label43: TLabel;
    Label44: TLabel;
    dmkEd_: TdmkEdit;
    dmkRGTipoCH: TdmkRadioGroup;
    Button1: TButton;
    Label45: TLabel;
    dmkEdExecs: TdmkEdit;
    Label46: TLabel;
    dmkEdOneCliInt: TdmkEdit;
    QrLanctosFatNum: TFloatField;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtContasClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure QrContasAfterScroll(DataSet: TDataSet);
    procedure EdDuplicataKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TabSheet2Resize(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure TPDataChange(Sender: TObject);
    procedure QrCarteirasAfterScroll(DataSet: TDataSet);
    procedure QrCliIntAfterScroll(DataSet: TDataSet);
    procedure SpeedButton2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrDuplCHCalcFields(DataSet: TDataSet);
    procedure QrDuplNFCalcFields(DataSet: TDataSet);
    procedure EdLinkMaskChange(Sender: TObject);
    procedure LLBPesqDblClick(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure LLBPesqKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdLinkMaskKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CkParcelamentoClick(Sender: TObject);
    procedure RGPeriodoClick(Sender: TObject);
    procedure DBGParcelasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CkArredondarClick(Sender: TObject);
    procedure RGArredondarClick(Sender: TObject);
    procedure EdDiasExit(Sender: TObject);
    procedure RGIncremCHClick(Sender: TObject);
    procedure CkIncremDUClick(Sender: TObject);
    procedure EdDuplSepExit(Sender: TObject);
    procedure RGDuplSeqClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure CkIncremTxtClick(Sender: TObject);
    procedure QrDeptosAfterScroll(DataSet: TDataSet);
    procedure dmkEdCBGeneroChange(Sender: TObject);
    procedure dmkEdCBGeneroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkCBGeneroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdMesExit(Sender: TObject);
    procedure dmkEdDebExit(Sender: TObject);
    procedure dmkCBGeneroClick(Sender: TObject);
    procedure dmkEdCredExit(Sender: TObject);
    procedure dmkEdDocChange(Sender: TObject);
    procedure dmkEdTPVenctoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkCBForneceKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdCBDeptoChange(Sender: TObject);
    procedure dmkCBDeptoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdDescricaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkCBAccountKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdCBAccountKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdICMS_PExit(Sender: TObject);
    procedure dmkEdICMS_PKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdICMS_VExit(Sender: TObject);
    procedure dmkEdICMS_VKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdParcelasExit(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure dmkEdTPDataClick(Sender: TObject);
  private
    { Private declarations }
    FCriandoForm: Boolean;
    FValorAParcelar,
    FICMS: Double;
    FIDFinalidade: Integer;
    procedure VerificaEdits;
    procedure CarteirasReopen;
    procedure SetaAccount(var Key: Word; Shift: TShiftState);
    procedure ExigeFuncionario;
    procedure CalculaICMS;
    function ReopenLanctos: Boolean;

    function VerificaVencimento: Boolean;
    procedure ConfiguraVencimento;
    procedure ConfiguraComponentesCarteira;
    procedure ReopenFornecedores(Tipo: Integer);
    function NaoDuplicarLancto(): Boolean;
    procedure MostraPnMaskPesq(Link: String);
    procedure SelecionaItemDePesquisa;
    // Parcelamento
    procedure CalculaParcelas;
    function GetTipoValor: TTipoValor;
    procedure ConfiguracoesIniciais();
    procedure IncrementaExecucoes();

  public
    { Public declarations }
    {
    FCNAB_Sit, FFatID, FFatNum, FFAtPArcela, FFatID_Sub, FID_Pgto,
    FNivel, FDescoPor, FCtrlIni, FUnidade: Integer;
    FDescoVal, FNFVal, FPercJur, FPercMulta: Double;
    FDoc2: String;
    FCondPercJuros, FCondPercMulta: Double;
    FTPDataIni, FTPDataFim: TDateTimePicker;
    FDepto: Variant;
    FCondominio: Integer;
    }
    //
  end;

const
  FLargMaior = 800;
  FLargMenor = 604;

var
  FmLanctosCond: TFmLanctosCond;
  Pagto_Doc: Double;

implementation

uses UnMyObjects, Module, Contas, Principal, Entidades, ModuleCond, EmiteCheque,
  LanctosCondDuplic, FinForm, UnFinanceiro, MyDBCheck;

{$R *.DFM}

procedure TFmLanctosCond.CalculaParcelas;
var
  i, Parce, DiasP, Casas: Integer;
  Valor, Valor1, ValorX, ValorA, ValorC, ValorD, Total, Mora, Multa, Fator: Double;
  Data: TDate;
  TipoValor: TTipoValor;
  Duplicata, Descricao, DuplSeq, DuplSep, Fmt, TxtSeq: String;
begin
  TbParcpagtos.Close;
  TbParcpagtos.Open;
  TbParcpagtos.DisableControls;
  while not TbParcpagtos.Eof do TbParcpagtos.Delete;
  TbParcpagtos.EnableControls;
  TbParcpagtos.Close;
  if GBParcelamento.Visible then
  begin
    TipoValor := GetTipoValor;
    Pagto_Doc := Geral.DMV(dmkEdDoc.Text);
    DiasP := Geral.IMV(EdDias.Text);
    Parce := Geral.IMV(dmkEdParcelas.Text);
    Mora  := Geral.DMV(dmkEdMoraDia.Text);
    Multa := Geral.DMV(dmkEdMulta.Text);
    Total := FValorAParcelar;
    if Total > 0 then
      Fator := Geral.DMV(dmkEdICMS_V.Text) / Total else Fator := 0;
    if Total <= 0 then Valor := 0
    else
    begin
      Valor := (Total / Parce)*100;
      Valor := (Trunc(Valor))/100;
    end;
    if CkArredondar.Checked then Valor := int(Valor);
    Valor1 := Valor;
    ValorX := Valor;
    if RGArredondar.ItemIndex = 0 then
    begin
      EdParcela1.Text := Geral.TFT(FloatToStr(Valor), 2, siPositivo);
      ValorX := Total - ((Parce - 1) * Valor);
      EdParcelaX.Text := Geral.TFT(FloatToStr(ValorX), 2, siPositivo);
    end else begin
      EdParcelaX.Text := Geral.TFT(FloatToStr(Valor), 2, siPositivo);
      Valor1 := Total - ((Parce - 1) * Valor);
      EdParcela1.Text := Geral.TFT(FloatToStr(Valor1), 2, siPositivo);
    end;
    //Duplicata := MLAGeral.IncrementaDuplicata(dmkEdDuplicata.Text, -1);
    Duplicata := dmkEdDuplicata.Text;
    Casas := Length(IntTostr(Parce));
    fmt := '';
    for i := 1 to Casas do fmt := fmt + '0';
    DuplSep := Trim(EdDuplSep.Text);
    if DuplSep = '' then DuplSep := ' ';
    for i := 1 to Parce do
    begin
      if i= 1 then ValorA := Valor1
      else if i = Parce then ValorA := ValorX
      else ValorA := Valor;
      if TipoValor = tvCred then ValorC := ValorA else ValorC := 0;
      if TipoValor = tvDeb  then ValorD := ValorA else ValorD := 0;
      //
      if RGPeriodo.ItemIndex = 0 then
        Data := MLAGeral.IncrementaMeses(dmkEdTPVencto.Date, i-1, True)
      else
        Data := dmkEdTPVencto.Date + (DiasP * (i-1));
      //
      if dmkEdMes.Enabled then
      begin
        if CkIncremTxt.Checked then
        begin
          case RGSepTxt.ItemIndex of
            0: TxtSeq := FormatFloat(fmt, i);
            1: TxtSeq := MLAGeral.IntToColTxt(i);
            else TxtSeq := '?';
          end;
          case RGSepTxt.ItemIndex of
            0: Descricao := FormatFloat(fmt, Parce);
            1: Descricao := MLAGeral.IntToColTxt(Parce);
            else TxtSeq := '?';
          end;
          TxtSeq := TxtSeq + EdSepTxt.Text;
          Descricao := TxtSeq + Descricao + ' ' +dmkEdDescricao.Text;
        end else
          Descricao := dmkEdDescricao.Text
      end else
        Descricao := IntToStr(i) + '�/'+ IntToStr(Parce) + ' ' +dmkEdDescricao.Text;
      //Duplicata := MLAGeral.IncrementaDuplicata(Duplicata, 1);
      //
      if CkIncremDU.Checked then
      begin
        case RGDuplSeq.ItemIndex of
          0: DuplSeq := FormatFloat(fmt, i);
          1: DuplSeq := MLAGeral.IntToColTxt(i);
          else DuplSeq := '?';
        end;
        DuplSeq := DuplSep + DuplSeq;
      end else DuplSeq := '';
      Dmod.QrUpdL.SQL.Clear;
      Dmod.QrUpdL.SQL.Add('INSERT INTO parcpagtos SET Parcela=:P0, ');
      Dmod.QrUpdL.SQL.Add('Data=:P1, Credito=:P2, Debito=:P3, Doc=:P4, ');
      Dmod.QrUpdL.SQL.Add('Mora=:P5, Multa=:P6, ICMS_V=:P7, Duplicata=:P8, ');
      Dmod.QrUpdL.SQL.Add('Descricao=:P9 ');
      Dmod.QrUpdL.Params[0].AsInteger := i;
      Dmod.QrUpdL.Params[1].AsString := FormatDateTime(VAR_FORMATDATE, Data);
      Dmod.QrUpdL.Params[2].AsFloat := ValorC;
      Dmod.QrUpdL.Params[3].AsFloat := ValorD;
      Dmod.QrUpdL.Params[4].AsFloat := Pagto_Doc;
      Dmod.QrUpdL.Params[5].AsFloat := Mora;
      Dmod.QrUpdL.Params[6].AsFloat := Multa;
      Dmod.QrUpdL.Params[7].AsFloat := Fator * (ValorC+ValorD);
      Dmod.QrUpdL.Params[8].AsString := Duplicata + DuplSeq;
      Dmod.QrUpdL.Params[9].AsString := Descricao;
      Dmod.QrUpdL.ExecSQL;
      if not RGIncremCH.Enabled then Pagto_Doc := Pagto_Doc
      else Pagto_Doc := Pagto_Doc + RGIncremCH.ItemIndex;
      //
    end;
    TbParcpagtos.Open;
    TbParcpagtos.EnableControls;
  end;
  QrSoma.Close;
  QrSoma.Open;
  EdSoma.Text := FormatFloat('#,###,##0.00', QrSomaVALOR.Value);
end;

procedure TFmLanctosCond.CarteirasReopen;
begin
  QrCarteiras.Close;
  QrCarteiras.Params[0].AsInteger := FmPrincipal.FEntInt;
  QrCarteiras.Open;
end;

procedure TFmLanctosCond.VerificaEdits;
begin
  if (QrContasMensal.Value = 'V') or (Dmod.QrControleMensalSempre.Value=1) then
  begin
    dmkEdMes.Enabled := True;
    LaMes.Enabled := True;
  end else begin
    dmkEdMes.Enabled := False;
    LaMes.Enabled := False;
  end;
  //
  if (VAR_BAIXADO <> -2) and not VAR_FATURANDO then Exit;
  if QrContasCredito.Value = 'V' then
  begin
    dmkEdCred.Enabled := True;
    LaCred.Enabled := True;
    dmkEdCBCliente.Enabled := True;
    LaCliente.Enabled := True;
    dmkCBCliente.Enabled := True;
  end else begin
    dmkEdCred.Enabled := False;
    LaCred.Enabled := False;
    dmkEdCBCliente.Enabled := False;
    LaCliente.Enabled := False;
    dmkCBCliente.Enabled := False;
  end;

  if QrContasDebito.Value = 'V' then
  begin
    dmkEdDeb.Enabled := True;
    LaDeb.Enabled := True;
    dmkEdCBFornece.Enabled := True;
    LaFornecedor.Enabled := True;
    dmkCBFornece.Enabled := True;
  end else begin
    dmkEdDeb.Enabled := False;
    LaDeb.Enabled := False;
    dmkEdCBFornece.Enabled := False;
    LaFornecedor.Enabled := False;
    dmkCBFornece.Enabled := False;
  end;

end;

procedure TFmLanctosCond.BtDesisteClick(Sender: TObject);
begin
  Close;
  VAR_VALOREMITIRIMP := 0;
end;

procedure TFmLanctosCond.BtConfirmaClick(Sender: TObject);
var
  Cartao, Carteira, Genero, TipoAnt, CartAnt, NF, Sit, Linha, Depto, Fornecedor,
  Cliente, CliInt, ForneceI, Vendedor, Account, Funci, Parcelas,
  DifMes, Me1, Me2: Integer;
  Controle: Int64;
  Credito, Debito, Doc, MoraDia, Multa, ICMS_V, Difer, MultaVal, MoraVal: Double;
  Mes, Vencimento, Compensado: String;
  Inseriu: Boolean;
  MesX, AnoX: Word;
  DataX: TDateTime;
begin
  Me1 := 0;
  //
  if CkParcelamento.Checked and (LaTipo.Caption = CO_ALTERACAO) then
  begin
    Application.MessageBox('N�o pode haver parcelamento em altera��o!',
    'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  if dmkEdCBFornece.Enabled = False then Fornecedor := 0 else
    if dmkCBFornece.KeyValue <> NULL then
      Fornecedor := dmkCBFornece.KeyValue else Fornecedor := 0;
  if dmkEdCBCliente.Enabled = False then Cliente := 0 else
    if dmkCBCliente.KeyValue <> NULL then
      Cliente := dmkCBCliente.KeyValue else Cliente := 0;
  if dmkEdCBVendedor.Enabled = False then Vendedor := 0 else
    if dmkCBVendedor.KeyValue <> NULL then
      Vendedor := dmkCBVendedor.KeyValue else Vendedor := 0;
  if dmkEdCBAccount.Enabled = False then Account := 0 else
    if dmkCBAccount.KeyValue <> NULL then
      Account := dmkCBAccount.KeyValue else Account := 0;
  Depto := Geral.IMV(dmkEdCBDepto.Text);
  ForneceI := Geral.IMV(dmkEdCBCliCli.Text);
  CliInt := Geral.IMV(dmkEdCBCliInt.Text);
  if (CliInt = 0) or (CliInt <> FmPrincipal.FEntInt) then
  begin
    Application.MessageBox('ERRO! Cliente interno inv�lido!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    LaCliInt.Enabled := True;
    dmkEdCBCliInt.Enabled := True;
    dmkCBCliInt.Enabled := True;
    if dmkEdCBCliInt.Enabled then dmkEdCBCliInt.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (VAR_AVISOSCXAEDIT in ([1,3,5,7]))
  and (Vendedor < 1) and (dmkCBVendedor.Visible) and (Panel3.Visible) then
  begin
    if Application.MessageBox('Deseja continuar mesmo sem definir o vendedor?',
    'Aviso de escolha de vendedor', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      if dmkCBVendedor.Enabled then dmkCBVendedor.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  if (VAR_AVISOSCXAEDIT in ([2,3,6,7]))
  and (Account < 1) and (dmkCBAccount.Visible) then
  begin
    if Application.MessageBox('Deseja continuar mesmo sem definir o Representante?',
    'Aviso de escolha de Representante', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      if dmkCBAccount.Enabled then dmkCBAccount.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  VAR_CANCELA := False;
  if not VerificaVencimento then
  begin
   Screen.Cursor := crDefault;
   Exit;
  end;
  Cartao := 0;
  Compensado := CO_VAZIO;
  if VAR_FATURANDO then
  begin
    Cartao := FmPrincipal.CartaoDeFatura;
    Compensado := FmPrincipal.CompensacaoDeFatura;
  end else if dmkEdTPCompensado.Date > 1 then
    Compensado := Geral.FDT(dmkEdTPCompensado.Date, 1);
  if (dmkEdNF.Enabled = False) then NF := 0
  else NF := Geral.IMV(dmkEdNF.Text);
  Credito := Geral.DMV(dmkEdCred.Text);
  if VAR_CANCELA then
  begin
    Application.MessageBox('Inclus�o cancelada pelo usu�rio!',
    'Cancelaqmento de Inclus�o', MB_OK + MB_ICONEXCLAMATION);
    if dmkEdNF.Enabled then dmkEdNF.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  Debito  := Geral.DMV(dmkEdDeb.Text);
  MoraDia := Geral.DMV(dmkEdMoraDia.Text);
  Multa   := Geral.DMV(dmkEdMulta.Text);
  ICMS_V  := Geral.DMV(dmkEdICMS_V.Text);
  Difer   := Credito-Debito;
  if ((Difer<0) and (ICMS_V>0)) or ((Difer>0) and (ICMS_V<0)) then
  begin
    Application.MessageBox('ICMS incorreto! Para d�bitos o ICMS deve ser negativo!',
    'Cancelaqmento de Inclus�o', MB_OK + MB_ICONEXCLAMATION);
    if dmkEdICMS_P.Enabled then dmkEdICMS_P.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (dmkEdCred.Enabled = False) and (Credito <> 0) and (VAR_BAIXADO = -2) then
  begin
    dmkEdCred.Text := '0';
    Credito := 0;
  end;
  if (dmkEdDeb.Enabled = False) and (Debito <> 0) and (VAR_BAIXADO = -2) then
  begin
    dmkEdDeb.Text := '0';
    Debito := 0;
  end;
  if dmkCBCarteira.KeyValue <> NULL then Carteira := dmkCBCarteira.KeyValue else Carteira := 0;
  if Carteira = 0 then
  begin
    Application.MessageBox('ERRO. Defina uma carteira!', 'Erro', MB_OK+MB_ICONERROR);
    if dmkEdCarteira.Enabled then dmkEdCarteira.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (dmkCBFunci.Visible = True) and
     (Panel3.Visible = True) and (dmkCBFunci.KeyValue = NULL)then
  begin
    Application.MessageBox('ERRO. Defina um funcion�rio!', 'Erro', MB_OK+MB_ICONERROR);
    if dmkEdCBFunci.Enabled then dmkEdCBFunci.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end else Funci := Geral.IMV(dmkEdCBFunci.Text);
  if Funci = 0 then Funci := VAR_USUARIO;
  Genero := Geral.IMV(dmkEdCBGenero.Text);
  if Genero = 0 then
  begin
    Application.MessageBox('ERRO. Defina uma conta!', 'Erro', MB_OK+MB_ICONERROR);
    if dmkEdCBGenero.Enabled then dmkEdCBGenero.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (Credito > 0) and (QrContasCredito.Value = 'F') then
  begin
    Application.MessageBox('ERRO. Valor n�o pode ser cr�dito', 'Erro', MB_OK+MB_ICONERROR);
    dmkEdCred.Text;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (Debito > 0) and (QrContasDebito.Value = 'F') then
  begin
    Application.MessageBox('ERRO. Valor n�o pode ser d�bito', 'Erro', MB_OK+MB_ICONERROR);
    dmkEdDeb.Text;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (dmkEdCred.Enabled = False) and (dmkEdDeb.Enabled = False) and
  (VAR_BAIXADO = -2) then
  begin
    Application.MessageBox('ERRO. Opera��o imposs�vel. Conta sem permiss�o de d�bito ou cr�dito.', 'Erro', MB_OK+MB_ICONERROR);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (Credito = 0) and (Debito = 0) then
  begin
    Application.MessageBox('ERRO. Defina um d�bito ou um cr�dito.', 'Erro', MB_OK+MB_ICONERROR);
    if dmkEdDeb.Enabled = True then dmkEdDeb.SetFocus
    else if dmkEdCred.Enabled = True then dmkEdCred.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if dmkEdMes.Text = CO_VAZIO then Mes := CO_VAZIO else
  if (dmkEdMes.Enabled = False) then Mes := CO_VAZIO else
  begin
    Mes := (*dmkEdMes.Text[4]+dmkEdMes.Text[5]+*)dmkEdMes.Text[6]+dmkEdMes.Text[7]+(*'/'+*)
    dmkEdMes.Text[1]+dmkEdMes.Text[2](*+'/01'*);
    Me1 := Geral.Periodo2000(dmkEdTPVencto.Date);
    AnoX := Geral.IMV(Copy(Mes, 1, 2)) + 2000;
    MesX := Geral.IMV(Copy(Mes, 3, 2));
    DataX := EncodeDate(AnoX, MesX, 1);
    Me2 := Geral.Periodo2000(DataX);
    DifMes := Me2 - Me1;
    if Mes = CO_VAZIO then
    begin
      Application.MessageBox('ERRO. Defina o m�s do vencimento.', 'Erro', MB_OK+MB_ICONERROR);
      if dmkEdMes.Enabled then dmkEdMes.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  if (Mes = CO_VAZIO) and (dmkEdMes.Enabled = True) then
  begin
    Application.MessageBox('ERRO. Defina o m�s do vencimento.', 'Erro', MB_OK+MB_ICONERROR);
    if dmkEdMes.Enabled then dmkEdMes.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (dmkEdDoc.Enabled = False) and (VAR_BAIXADO = -2) then Doc := 0
  else begin
    Doc := Geral.DMV(dmkEdDoc.Text);
    if (QrCarteirasExigeNumCheque.Value = 1) and (Doc=0) then
    begin
      Application.MessageBox('Esta carteira exige o n�mero do cheque (documento)!',
      'Aviso', MB_OK+MB_ICONWARNING);
      dmkEdDoc.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  // n�o mexer no vencimento aqui!
  {if (dmkEdTPVencto.Enabled = False) then Vencimento :=
    FormatDateTime(VAR_FORMATDATE, dmkEdTPdata.Date)
  else}
  Vencimento := FormatDateTime(VAR_FORMATDATE, dmkEdTPVencto.Date);
  if VAR_IMPORTANDO then Linha := VAR_IMPLINHA else Linha := 0;
  if VAR_FATURANDO then Sit := 3
  else if VAR_IMPORTANDO then Sit := -1
    else if VAR_BAIXADO <> -2 then Sit := VAR_BAIXADO
      else if QrCarteirasTipo.Value = 2 then Sit := 0
        else Sit := 3;
  TipoAnt := StrToInt(dmkEdTipoAnt.Text);
  CartAnt := StrToInt(dmkEdCartAnt.Text);
  MultaVal := Geral.DMV(dmkEdMultaVal.Text);
  MoraVal := Geral.DMV(dmkEdMoraVal.Text);
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'Lanctos', 'Lanctos', 'Controle');
    VAR_DATAINSERIR := dmkEdTPdata.Date;

  end else begin
    Controle := Geral.IMV(dmkEdControle.Text);
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('DELETE FROM lanctos WHERE Controle=:P0');
    Dmod.QrUpdM.Params[0].AsFloat := Controle;
    Dmod.QrUpdM.ExecSQL;
    UFinanceiro.RecalcSaldoCarteira(CartAnt, nil, False, False);
    //dbiSaveChanges(Dmod.QrUpdU.Handle);
    VAR_DATAINSERIR := dmkEdTPdata.Date;
  end;

  if NaoDuplicarLancto then Exit;

  VAR_LANCTO2 := Controle;







  {Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERTINTO Lanctos SET Documento=:P0, Data=:P1, ');
  Dmod.QrUpdU.SQL.Add('Tipo=:P2, Carteira=:P3, Credito=:P4, Debito=:P5, ');
  Dmod.QrUpdU.SQL.Add('Genero=:P6, NotaFiscal=:P7, Vencimento=:P8, Mez=:P9, ');
  Dmod.QrUpdU.SQL.Add('Descricao=:P10, Sit=:P11, Controle=:P12, Cartao=:P13, ');
  Dmod.QrUpdU.SQL.Add('Compensado=:P14, Linha=:P15, Fornecedor=:P16, ');
  Dmod.QrUpdU.SQL.Add('Cliente=:P17, MoraDia=:P18, Multa=:P19, DataCad=:P20, ');
  Dmod.QrUpdU.SQL.Add('UserCad=:P21, DataDoc=:P22, Vendedor=:P23, ');
  Dmod.QrUpdU.SQL.Add('Account=:P24, FatID=:P25, FatID_Sub=:P26, ');
  Dmod.QrUpdU.SQL.Add('ICMS_P=:P27, ICMS_V=:P28, Duplicata=:P29, ');
  if dmkEdQtde.Text <> '' then
    Dmod.QrUpdU.SQL.Add('Qtde='+MLAGeral.TFT_Dot(dmkEdQtde.Text, 3, siPositivo)+',');
  Dmod.QrUpdU.SQL.Add('CliInt=:P30, Depto=:P31, SerieCH=:P32, ForneceI=:P33, ');
  Dmod.QrUpdU.SQL.Add('MultaVal=:P34, MoraVal=:P35, CNAB_Sit=:P36, ');
  Dmod.QrUpdU.SQL.Add('ID_Pgto=:P37, TipoCH=:P38 ');}
  //



  UFinanceiro.LancamentoDefaultVARS;
  {Dmod.QrUpdU.Params[00].AsFloat   :=} FLAN_Documento  := Trunc(Doc);
  {Dmod.QrUpdU.Params[01].AsString  :=} FLAN_Data       := FormatDateTime(VAR_FORMATDATE, dmkEdTPdata.Date);
  {Dmod.QrUpdU.Params[02].AsInteger :=} FLAN_Tipo       := QrCarteirasTipo.Value;
  {Dmod.QrUpdU.Params[03].AsInteger :=} FLAN_Carteira   := Carteira;
  {Dmod.QrUpdU.Params[04].AsFloat   :=} FLAN_Credito    := Credito;
  {Dmod.QrUpdU.Params[05].AsFloat   :=} FLAN_Debito     := Debito;
  {Dmod.QrUpdU.Params[06].AsInteger :=} FLAN_Genero     := Genero;
  {Dmod.QrUpdU.Params[07].AsInteger :=} FLAN_NotaFiscal := NF;
  {Dmod.QrUpdU.Params[08].AsString  :=} FLAN_Vencimento := Vencimento;
  {Dmod.QrUpdU.Params[09].AsString  :=} FLAN_Mez        := Mes;
  {Dmod.QrUpdU.Params[10].AsString  :=} FLAN_Descricao  := dmkEdDescricao.Text;
  {Dmod.QrUpdU.Params[11].AsInteger :=} FLAN_Sit        := Sit;
  {Dmod.QrUpdU.Params[12].AsFloat   :=} FLAN_Controle   := Controle;
  {Dmod.QrUpdU.Params[13].AsInteger :=} FLAN_Cartao     := Cartao;
  {Dmod.QrUpdU.Params[14].AsString  :=} FLAN_Compensado := Compensado;
  {Dmod.QrUpdU.Params[15].AsInteger :=} FLAN_Linha      := Linha;
  {Dmod.QrUpdU.Params[16].AsInteger :=} FLAN_Fornecedor := Fornecedor;
  {Dmod.QrUpdU.Params[17].AsInteger :=} FLAN_Cliente    := Cliente;
  {Dmod.QrUpdU.Params[18].AsFloat   :=} FLAN_MoraDia    := MoraDia;
  {Dmod.QrUpdU.Params[19].AsFloat   :=} FLAN_Multa      := Multa;
  {Dmod.QrUpdU.Params[20].AsString  :=} FLAN_DataCad    := FormatDateTime(VAR_FORMATDATE, Date);
  {Dmod.QrUpdU.Params[21].AsInteger :=} FLAN_UserCad    := Funci;
  {Dmod.QrUpdU.Params[22].AsString  :=} FLAN_DataDoc    := FormatDateTime(VAR_FORMATDATE, dmkEdTPDataDoc.Date);
  {Dmod.QrUpdU.Params[23].AsInteger :=} FLAN_Vendedor   := Vendedor;
  {Dmod.QrUpdU.Params[24].AsInteger :=} FLAN_Account    := Account;
  {Dmod.QrUpdU.Params[25].AsInteger :=} FLAN_FatID      := dmkEdFatID.ValueVariant;
  {Dmod.QrUpdU.Params[26].AsInteger :=} FLAN_FatID_Sub  := dmkEdFatID_Sub.ValueVariant;
  {Dmod.QrUpdU.Params[27].AsFloat   :=} FLAN_ICMS_P     := Geral.DMV(dmkEdICMS_P.Text);
  {Dmod.QrUpdU.Params[28].AsFloat   :=} FLAN_ICMS_V     := Geral.DMV(dmkEdICMS_V.Text);
  {Dmod.QrUpdU.Params[29].AsString  :=} FLAN_Duplicata  := dmkEdDuplicata.Text;
  {Dmod.QrUpdU.Params[30].AsInteger :=} FLAN_CliInt     := CliInt;
  {Dmod.QrUpdU.Params[31].AsInteger :=} FLAN_Depto      := Depto;
  {Dmod.QrUpdM.Params[32].AsInteger :=} FLAN_DescoPor   := dmkEdDescoPor.ValueVariant;
  {Dmod.QrUpdU.Params[33].AsInteger :=} FLAN_ForneceI   := ForneceI;
  {Dmod.QrUpdM.Params[34].AsFloat   :=} FLAN_DescoVal   := dmkEdDescoVal.ValueVariant;
  {Dmod.QrUpdM.Params[35].AsFloat   :=} FLAN_NFVal      := dmkEdNFVAl.ValueVariant;
  {Dmod.QrUpdM.Params[36].AsInteger :=} FLAN_Unidade    := dmkEdUnidade.ValueVariant;
  {Dmod.QrUpdM.Params[37].AsFloat   :=} //FLAN_Qtde       := Qtde;  ABAIXO
  {Dmod.QrUpdM.Params[38].AsInteger :=} FLAN_FatNum     := dmkEdFatNum.ValueVariant;
  {Dmod.QrUpdM.Params[39].AsInteger :=} FLAN_FatParcela := dmkedFatParcela.ValueVariant;
  {Dmod.QrUpdM.Params[40].AsString  :=} FLAN_Doc2       := dmkEdDoc2.ValueVariant;
  {Dmod.QrUpdU.Params[41].AsString  :=} FLAN_SerieCH    := dmkEdSerieCh.Text;
  {Dmod.QrUpdM.Params[42].AsFloat   :=} FLAN_MultaVal   := MultaVal;
  {Dmod.QrUpdM.Params[43].AsFloat   :=} FLAN_MoraVal    := MoraVal;
  {Dmod.QrUpdM.Params[44].AsInteger :=} FLAN_CtrlIni    := dmkEdCtrlIni.ValueVariant;
  {Dmod.QrUpdM.Params[45].AsInteger :=} FLAN_Nivel      := dmkEdNivel.ValueVariant;
  {Dmod.QrUpdU.Params[46].AsInteger :=} FLAN_ID_Pgto    := dmkEdID_Pgto.ValueVariant;
  {Dmod.QrUpdU.Params[47].AsInteger :=} FLAN_CNAB_Sit   := dmkEdCNAB_Sit.ValueVariant;
  {Dmod.QrUpdU.Params[48].AsInteger :=} FLAN_TipoCH     := dmkRGTipoCH.ItemIndex;
  {Dmod.QrUpdU.ExecSQL; }
  if dmkEdQtde.Text <> '' then
    FLAN_Qtde := Geral.DMV(dmkEdQtde.Text);
    //Dmod.QrUpdU.SQL.Add('Qtde='+MLAGeral.TFT_Dot(dmkEdQtde.Text, 3, siPositivo)+',');









  {Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERTINTO Lanctos SET Documento=:P0, Data=:P1, ');
  Dmod.QrUpdU.SQL.Add('Tipo=:P2, Carteira=:P3, Credito=:P4, Debito=:P5, ');
  Dmod.QrUpdU.SQL.Add('Genero=:P6, NotaFiscal=:P7, Vencimento=:P8, Mez=:P9, ');
  Dmod.QrUpdU.SQL.Add('Descricao=:P10, Sit=:P11, Controle=:P12, Cartao=:P13, ');
  Dmod.QrUpdU.SQL.Add('Compensado=:P14, Linha=:P15, Fornecedor=:P16, ');
  Dmod.QrUpdU.SQL.Add('Cliente=:P17, MoraDia=:P18, Multa=:P19,DataCad=:P20, ');
  Dmod.QrUpdU.SQL.Add('UserCad=:P21, DataDoc=:P22, Vendedor=:P23, ');
  Dmod.QrUpdU.SQL.Add('Account=:P24, FatID=:P25, FatID_Sub=:P26, ');
  Dmod.QrUpdU.SQL.Add('ICMS_P=:P27, ICMS_V=:P28, Duplicata=:P29, ');
  if dmkEdQtde.Text <> '' then
    Dmod.QrUpdU.SQL.Add('Qtde='+MLAGeral.TFT_Dot(dmkEdQtde.Text, 3, siPositivo)+',');
  Dmod.QrUpdU.SQL.Add('CliInt=:P30, Depto=:P31, SerieCH=:P32, ForneceI=:P33, ');
  Dmod.QrUpdU.SQL.Add('MultaVal=:P34, MoraVal=:P35, CNAB_Sit=:P36, ');
  Dmod.QrUpdU.SQL.Add('ID_Pgto=:P37, TipoCH=:P38 ');
  //
  Dmod.QrUpdU.Params[00].AsFloat   := Doc;
  Dmod.QrUpdU.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, dmkEdTPdata.Date);
  Dmod.QrUpdU.Params[02].AsInteger := QrCarteirasTipo.Value;
  Dmod.QrUpdU.Params[03].AsInteger := Carteira;
  Dmod.QrUpdU.Params[04].AsFloat   := Credito;
  Dmod.QrUpdU.Params[05].AsFloat   := Debito;
  Dmod.QrUpdU.Params[06].AsInteger := Genero;
  Dmod.QrUpdU.Params[07].AsInteger := NF;
  Dmod.QrUpdU.Params[08].AsString  := Vencimento;
  Dmod.QrUpdU.Params[09].AsString  := Mes;
  Dmod.QrUpdU.Params[10].AsString  := dmkEdDescricao.Text;
  Dmod.QrUpdU.Params[11].AsInteger := Sit;
  Dmod.QrUpdU.Params[12].AsFloat   := Controle;
  Dmod.QrUpdU.Params[13].AsInteger := Cartao;
  Dmod.QrUpdU.Params[14].AsString  := Compensado;
  Dmod.QrUpdU.Params[15].AsInteger := Linha;
  Dmod.QrUpdU.Params[16].AsInteger := Fornecedor;
  Dmod.QrUpdU.Params[17].AsInteger := Cliente;
  Dmod.QrUpdU.Params[18].AsFloat   := MoraDia;
  Dmod.QrUpdU.Params[19].AsFloat   := Multa;
  Dmod.QrUpdU.Params[20].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[21].AsInteger := Funci;
  Dmod.QrUpdU.Params[22].AsString  := FormatDateTime(VAR_FORMATDATE, TPDataDoc.Date);
  Dmod.QrUpdU.Params[23].AsInteger := Vendedor;
  Dmod.QrUpdU.Params[24].AsInteger := Account;
  Dmod.QrUpdU.Params[25].AsInteger := FatID;
  Dmod.QrUpdU.Params[26].AsInteger := FatID_Sub;
  Dmod.QrUpdU.Params[27].AsFloat   := Geral.DMV(EdICMS_P.Text);
  Dmod.QrUpdU.Params[28].AsFloat   := Geral.DMV(EdICMS_V.Text);
  Dmod.QrUpdU.Params[29].AsString  := dmkEdDuplicata.Text;
  Dmod.QrUpdU.Params[30].AsInteger := CliInt;
  Dmod.QrUpdU.Params[31].AsInteger := Depto;
  Dmod.QrUpdU.Params[32].AsString  := EdSerieCh.Text;
  Dmod.QrUpdU.Params[33].AsInteger := ForneceI;
  Dmod.QrUpdU.Params[34].AsFloat   := MultaVal;
  Dmod.QrUpdU.Params[35].AsFloat   := MoraVal;
  Dmod.QrUpdU.Params[36].AsInteger := FCNAB_Sit;
  Dmod.QrUpdU.Params[37].AsInteger := FID_Pgto;
  Dmod.QrUpdU.Params[38].AsInteger := CkTipoCH.Value;
  Dmod.QrUpdU.ExecSQL;}






    Parcelas := 0;
    if CkParcelamento.Checked then
    begin
      TbParcpagtos.First;
      while not TbParcpagtos.Eof do
      begin
        if (Me1 > 0) then
        begin
          Me2 := Geral.Periodo2000(TbParcpagtosData.Value) + DifMes;
          FLAN_Mez := IntToStr(MLAGeral.PeriodoToAnoMes(Me2));
          FLAN_Descricao  := TbParcPagtosDescricao.Value;
          if (CkIncremTxt.Checked = False) then
            FLAN_Descricao  := FLAN_Descricao +
              ' - ' + MLAGeral.MesEAnoDoPeriodo(Me2)
        end else
        begin
          FLAN_Mez := '';
          FLAN_Descricao  := TbParcPagtosDescricao.Value;
        end;
        FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
          'Controle', 'Lanctos', 'Lanctos', 'Controle');
        FLAN_Duplicata  := TbParcPagtosDuplicata.Value;
        FLAN_Credito    := TbParcpagtosCredito.Value;
        FLAN_Debito     := TbParcpagtosDebito.Value;
        FLAN_MoraDia    := dmkEdPercJuroM.ValueVariant;//FmCondGer.QrCondPercJuros.Value;
        FLAN_Multa      := dmkEdPercMulta.ValueVariant;//FmCondGer.QrCondPercMulta.Value;
        FLAN_ICMS_V     := TbParcpagtosICMS_V.Value;
        FLAN_Vencimento := Geral.FDT(TbParcpagtosData.Value, 1);
        FLAN_Sit        := Sit;
        //
        if UFinanceiro.InsereLancamento then
        begin
          Inc(Parcelas, 1);
          IncrementaExecucoes;
        end;
        TbParcPagtos.Next;
      end;
      Inseriu := TbParcPagtos.RecordCount = Parcelas;
      if not Inseriu then
        Application.MessageBox(PChar('ATEN��O! Foram inseridas ' +
        IntToStr(Parcelas) + ' de ' + IntToStr(TbParcPagtos.RecordCount) + '!'),
        'Aviso de diverg�ncias', MB_OK+MB_ICONWARNING);
    end else Inseriu := UFinanceiro.InsereLancamento;














  if Inseriu then
  begin
    IncrementaExecucoes;
    //
    if LaTipo.Caption = CO_INCLUSAO then
      UFinanceiro.RecalcSaldoCarteira(dmkCBCarteira.KeyValue, nil, False, False)
    else// begin if CBNovo.Checked = False then
      begin
        UFinanceiro.RecalcSaldoCarteira(dmkCBCarteira.KeyValue, nil, False, False);
        UFinanceiro.RecalcSaldoCarteira(CartAnt, nil, False, False);
    end;
    //
    VAR_VALOREMITIRIMP := VAR_VALOREMITIRIMP - Credito - Debito;
    {FmPrincipal.ReabreCarteirasELanctos(FmPrincipal.QrcarteirasTipo.Value,
    FmPrincipal.QrCarteirasCodigo.Value, Controle, 0,
    FTPDataIni, //FmCondGer.TPDataIni,
    FTPDataFim, //FmCondGer.TPDataFim,
    FDepto, FIDFinalidade);//FmCondGer.CBUH.KeyValue);
    }
    if CkCopiaCH.Checked then
        FmFinForm.ImprimeCopiaDeCh(FmPrincipal.QrLanctosControle.Value,
        Geral.IMV(dmkEdCBCliInt.Text));
    if CkContinuar.Checked then
    begin
      Application.MessageBox('Inclus�o concluida.', 'Aviso', MB_OK+MB_ICONINFORMATION);
      if Pagecontrol1.ActivePageIndex = 0 then
      if dmkEdTPData.Enabled then dmkEdTPData.SetFocus else dmkEdCBGenero.SetFocus;
    end else Close;
  end;  
  Screen.Cursor := crDefault;
end;

procedure TFmLanctosCond.FormCreate(Sender: TObject);
var
  CliInt: Integer;
begin
  Width := FLargMenor;
  FCriandoForm := True;
  QrContas.Open;
  if VAR_CXEDIT_ICM then
  begin
    LaICMS_P.Visible := True;
    LaICMS_V.Visible := True;
    dmkEdICMS_P.Visible := True;
    dmkEdICMS_V.Visible := True;
  end;
  //if Trim(VAR_CLIENTEC) <> '' then
  //begin
    QrClientes.SQL.Clear;
    QrClientes.SQL.Add('SELECT Codigo, Account, ');
    QrClientes.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrClientes.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrClientes.SQL.Add('FROM entidades');
    QrClientes.SQL.Add('WHERE Cliente1="V"');
    QrClientes.SQL.Add('OR    Cliente2="V"');
    QrClientes.SQL.Add('OR    Cliente3="V"');
    QrClientes.SQL.Add('OR    Cliente4="V"');
    QrClientes.SQL.Add('OR    Terceiro="V"');
    QrClientes.SQL.Add('ORDER BY NOMEENTIDADE');
    QrClientes.Open;
    //////
    LaCliente.Visible := True;
    dmkEdCBCliente.Visible := True;
    dmkCBCliente.Visible := True;
    //////
    dmkEdCBCliInt.Enabled := False;
    dmkCBCliInt.Enabled := False;
    dmkEdCBCliInt.Text := IntToStr(FmPrincipal.FEntInt);
    dmkCBCliInt.KeyValue := FmPrincipal.FEntInt;
    CliInt := Geral.IMV(dmkEdCBCliInt.Text);
    if (CliInt = 0) or (CliInt <> FmPrincipal.FEntInt) then
    Application.MessageBox('CUIDADO! Cliente interno n�o definido!', 'Aviso',
      MB_OK+MB_ICONWARNING);
  //end;
  ReopenFornecedores(0);
  if Trim(VAR_FORNECEF) <> '' then
  begin
    //
    LaFornecedor.Visible := True;
    dmkEdCBFornece.Visible := True;
    dmkCBFornece.Visible := True;
  end;
  if Trim(VAR_FORNECEV) <> '' then
  begin
    QrVendedores.SQL.Clear;
    QrVendedores.SQL.Add('SELECT Codigo, ');
    QrVendedores.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrVendedores.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrVendedores.SQL.Add('FROM entidades');
    QrVendedores.SQL.Add('WHERE Fornece'+VAR_FORNECEV+'="V"');
    QrVendedores.SQL.Add('ORDER BY NOMEENTIDADE');
    QrVendedores.Open;
    //////
    QrAccounts.SQL.Clear;
    QrAccounts.SQL.Add('SELECT Codigo, ');
    QrAccounts.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrAccounts.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrAccounts.SQL.Add('FROM entidades');
    QrAccounts.SQL.Add('WHERE Fornece'+VAR_FORNECEV+'="V"');
    QrAccounts.SQL.Add('ORDER BY NOMEENTIDADE');
    QrAccounts.Open;
    //////
    LaVendedor.Visible := True;
    dmkEdCBVendedor.Visible := True;
    dmkCBVendedor.Visible   := True;
    LaAccount.Visible := True;
    dmkEdCBAccount.Visible := True;
    dmkCBAccount.Visible := True;
  end;
  if VAR_LANCTOCONDICIONAL1 then
  begin
    VAR_DATAINSERIR := Date;
    dmkEdTPDataDoc.Date := Date;
    //
    dmkEdCarteira.Enabled := False;
    dmkCBCarteira.Enabled := False;
    dmkEdTPdata.Enabled := False;
  end;
  if VAR_TIPOVARCLI <> '' then ExigeFuncionario;
  PageControl1.ActivePageIndex := 0;
  TabControl1.TabIndex := 0;
end;

procedure TFmLanctosCond.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  CarteirasReopen;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    CkContinuar.Visible := True;
    CkParcelamento.Visible := True;
  end;
  if dmkEdCarteira.Enabled then dmkEdCarteira.SetFocus;
  VerificaEdits;
  if FCriandoForm then
  begin
    ConfiguracoesIniciais;
  end;
  FCriandoForm := False;
end;

procedure TFmLanctosCond.ConfiguraComponentesCarteira;
begin
  case QrCarteirasTipo.Value of
    0:
    begin
      if QrCarteirasPrazo.Value = 1 then
      begin
        LaDoc.Enabled := True;
        dmkEdSerieCH.Enabled := True;
        dmkEdDoc.Enabled := True;
        dmkEdNF.Enabled := True;
        LaNF.Enabled := True;
      end else begin
        // N�mero bloqueto no cheque
        LaDoc.Enabled := True;
        dmkEdSerieCH.Enabled := True;
        dmkEdDoc.Enabled := True;
        LaDoc.Enabled := True;
        //
        dmkEdNF.Enabled := True;
        LaNF.Enabled := True;
      end;
    end;
    1:
    begin
      LaDoc.Enabled := True;
      dmkEdSerieCH.Enabled := True;
      dmkEdDoc.Enabled := True;
    end;
    2:
    begin
      LaDoc.Enabled := True;
      dmkEdSerieCH.Enabled := True;
      dmkEdDoc.Enabled := True;
      //
      dmkEdNF.Enabled := True;
      LaNF.Enabled := True;
    end;
  end;
  ConfiguraVencimento;
end;

procedure TFmLanctosCond.BtContasClick(Sender: TObject);
begin
  if DBCheck.CriaForm(TFmContas, FmContas, afmAcessoTotal) then
  begin
    FmContas.LocCod(QrContasCodigo.Value, QrContasCodigo.Value);
    FmContas.ShowModal;
    FmContas.Destroy;
    QrContas.Close;
    QrContas.Open;
    dmkEdCBGenero.Text := IntToStr(VAR_CONTA);
    dmkCBGenero.KeyValue := VAR_CONTA;
  end;
end;

function TFmLanctosCond.VerificaVencimento: Boolean;
var
  Carteira: Integer;
  Data: TDateTime;
begin
  Result := True;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    if QrCarteirasFatura.Value = 'V' then
    begin
      if dmkCBCarteira.KeyValue <> NULL then Carteira := dmkCBCarteira.KeyValue
      else Carteira := 0;
      QrFatura.Close;
      QrFatura.Params[0].AsInteger := Carteira;
      QrFatura.Open;
      if (QrFatura.RecordCount > 0) and (dmkCBCarteira.KeyValue <> NULL) then
      begin
        Data := QrFaturaData.Value;
        while (Data - QrCarteirasFechamento.Value) < dmkEdTPdata.Date do
          Data := IncMonth(Data, 1);
        if int(dmkEdTPVencto.Date) <> Int(Data) then
        begin
          case Application.MessageBox(PChar(
          'A configura��o do sistema sugere a data de vencimento '+
          FormatDateTime(VAR_FORMATDATE3, Data)+' ao inv�s de '+
          FormatDateTime(VAR_FORMATDATE3, dmkEdTPVencto.Date)+
          '. Confirma a altera��o?'), PChar(VAR_APPNAME),
          MB_ICONQUESTION+MB_YESNOCANCEL+MB_DEFBUTTON1+MB_APPLMODAL) of
            ID_YES    : dmkEdTPVencto.Date := Data;
            ID_CANCEL : Result := False;
            ID_NO     : ;
          end;
        end;
      end;
      QrFatura.Close;
    end;
  end;
end;

procedure TFmLanctosCond.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLanctosCond.SpeedButton1Click(Sender: TObject);
begin
  VAR_CARTNUM := Geral.IMV(dmkEdCarteira.Text);
  FmPrincipal.CadastroDeCarteiras(QrCarteirasTipo.Value,
    Geral.IMV(dmkEdCarteira.Text));
  if VAR_CARTNUM <> 0 then
  begin
    QrCarteiras.Close;
    QrCarteiras.Open;
    dmkEdCarteira.Text := IntToStr(VAR_CARTNUM);
    dmkCBCarteira.KeyValue := VAR_CARTNUM;
    Dmod.DefParams;
  end;
end;

procedure TFmLanctosCond.QrContasAfterScroll(DataSet: TDataSet);
begin
  VerificaEdits;
end;

procedure TFmLanctosCond.SetaAccount(var Key: Word; Shift: TShiftState);
begin
  if key=VK_F4 then
  begin
    dmkEdCBAccount.Text   := IntToStr(QrClientesAccount.Value);
    dmkCBAccount.KeyValue := QrClientesAccount.Value;
  end;
end;

procedure TFmLanctosCond.ExigeFuncionario;
begin
  QrFunci.Close;
  QrFunci.SQL.Clear;
  QrFunci.SQL.Add('SELECT Codigo,');
  QrFunci.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
  QrFunci.SQL.Add('ELSE Nome END NOMEENTIDADE');
  QrFunci.SQL.Add('FROM entidades');
  QrFunci.SQL.Add('WHERE '+VAR_TIPOVARCLI+'="V"');
  QrFunci.SQL.Add('ORDER BY NomeENTIDADE');
  QrFunci.Open;
  Label10.Visible      := True;
  dmkEdCBFunci.Visible := True;
  dmkCBFunci.Visible   := True;
end;

procedure TFmLanctosCond.CalculaICMS;
var
  C, D, B, V, P: Double;
begin
  V := Geral.DMV(dmkEdICMS_V.Text);
  P := Geral.DMV(dmkEdICMS_P.Text);
  C := Geral.DMV(dmkEdCred.Text);
  D := Geral.DMV(dmkEdDeb.Text);
  //if C > D then B := C - D else B := D - C;
  B := C-D;
  if FICMS = 0 then V := B * P / 100
  else if B <> 0 then P := V / B * 100 else P := 0;
  dmkEdICMS_V.Text := Geral.FFT(V, 2, siPositivo);
  dmkEdICMS_P.Text := Geral.FFT(P, 2, siPositivo);
end;

procedure TFmLanctosCond.EdDuplicataKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key in ([VK_DOWN, VK_UP]) then
  begin
    if (key=VK_DOWN) then
      dmkEdDuplicata.Text := MLAGeral.DuplicataIncrementa(dmkEdDuplicata.Text, -1)
    else
      dmkEdDuplicata.Text := MLAGeral.DuplicataIncrementa(dmkEdDuplicata.Text,  1);
  end;
end;

function TFmLanctosCond.ReopenLanctos: Boolean;
var
  Ini, Fim: String;
begin
  Ini := FormatDateTime(VAR_FORMATDATE, 1);
  Fim := FormatDateTime(VAR_FORMATDATE, Date);
  QrLanctos.Close;
  QrLanctos.SQL.Clear;
  QrLanctos.SQL.Add('SELECT MOD(la.Mez, 100) Mes2,');
  QrLanctos.SQL.Add('((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,');
  QrLanctos.SQL.Add('la.*, ct.Codigo CONTA,');
  QrLanctos.SQL.Add('ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,');
  QrLanctos.SQL.Add('gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,');
  QrLanctos.SQL.Add('CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial');
  QrLanctos.SQL.Add('ELSE em.Nome END NOMEEMPRESA,');
  QrLanctos.SQL.Add('CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial');
  QrLanctos.SQL.Add('ELSE cl.Nome END NOMECLIENTE,');
  QrLanctos.SQL.Add('CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial');
  QrLanctos.SQL.Add('ELSE fo.Nome END NOMEFORNECEDOR,');
  QrLanctos.SQL.Add('ca.Nome NOMECARTEIRA, ca.Saldo SALDOCARTEIRA');
  QrLanctos.SQL.Add('FROM lanctos la');
  QrLanctos.SQL.Add('LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = 0');
  QrLanctos.SQL.Add('LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo');
  QrLanctos.SQL.Add('LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo');
  QrLanctos.SQL.Add('LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto');
  QrLanctos.SQL.Add('LEFT JOIN entidades em ON em.Codigo=ct.Empresa');
  QrLanctos.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
  QrLanctos.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor');
  QrLanctos.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
  //
  QrLanctos.SQL.Add('WHERE la.Data BETWEEN "'+Ini+'" AND "'+Fim+'"');
  //
  case TabControl1.TabIndex of
    0: QrLanctos.SQL.Add('AND la.Carteira='  +IntToStr(Geral.IMV(dmkEdCarteira.Text)));
    1: QrLanctos.SQL.Add('AND la.CliInt='    +IntToStr(Geral.IMV(dmkEdCBCliInt.Text)));
    2: QrLanctos.SQL.Add('AND la.Cliente='   +IntToStr(Geral.IMV(dmkEdCBCliente.Text)));
    3: QrLanctos.SQL.Add('AND la.Fornecedor='+IntToStr(Geral.IMV(dmkEdCBFornece.Text)));
    4: QrLanctos.SQL.Add('AND la.Genero='    +IntToStr(Geral.IMV(dmkEdCBGenero.Text)));
  end;
  QrLanctos.SQL.Add('ORDER BY la.Data DESC, la.Controle DESC');
  QrLanctos.Open;
  Result := True;
end;

procedure TFmLanctosCond.TabSheet2Resize(Sender: TObject);
begin
  (*DBGrid1.Align := alClient;
  DBGrid1.Align := alNone;
  DBGrid1.Top := 24;
  DBGrid1.Height := DBGrid1.Height - 24;*)
end;

procedure TFmLanctosCond.PageControl1Change(Sender: TObject);
begin
  ReopenLanctos;
  if PageControl1.ActivePageIndex in ([2,3]) then
    WindowState := wsMaximized
  else
    WindowState := wsNormal;
end;

procedure TFmLanctosCond.TabControl1Change(Sender: TObject);
begin
  ReopenLanctos;
end;

procedure TFmLanctosCond.TPDataChange(Sender: TObject);
begin
  ConfiguraVencimento;
end;

procedure TFmLanctosCond.ConfiguraVencimento;
begin
  if LaTipo.Caption = CO_INCLUSAO then
    dmkEdTPVencto.Date := dmkEdTPdata.Date;
end;

procedure TFmLanctosCond.QrCarteirasAfterScroll(DataSet: TDataSet);
begin
  ConfiguraComponentesCarteira;
end;

procedure TFmLanctosCond.ReopenFornecedores(Tipo: Integer);
begin
  QrFornecedores.Close;
  QrFornecedores.SQL.Clear;
  QrFornecedores.SQL.Add('SELECT Codigo, ');
  if Tipo = 0 then
  begin
    QrFornecedores.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrFornecedores.SQL.Add('ELSE Nome END NOMEENTIDADE');
    //
    LaFornecedor.Font.Color := LaForneceRN.Font.Color;
  end else begin
    QrFornecedores.SQL.Add('CASE WHEN Tipo=0 THEN Fantasia');
    QrFornecedores.SQL.Add('ELSE Apelido END NOMEENTIDADE');
    //
    LaFornecedor.Font.Color := LaForneceFA.Font.Color;
  end;
  QrFornecedores.SQL.Add('FROM entidades');
  QrFornecedores.SQL.Add('WHERE Fornece1="V"');
  QrFornecedores.SQL.Add('OR    Fornece2="V"');
  QrFornecedores.SQL.Add('OR    Fornece3="V"');
  QrFornecedores.SQL.Add('OR    Fornece4="V"');
  QrFornecedores.SQL.Add('OR    Fornece5="V"');
  QrFornecedores.SQL.Add('OR    Fornece6="V"');
  QrFornecedores.SQL.Add('OR    Terceiro="V"');
  QrFornecedores.SQL.Add('ORDER BY NOMEENTIDADE');
  QrFornecedores.Open;
  //
end;

procedure TFmLanctosCond.QrCliIntAfterScroll(DataSet: TDataSet);
begin
  case FIDFinalidade of
    1:
    begin
      //Nada
    end;
    2:
    begin
      QrCliCli.Close;
      QrCliCli.SQL.Clear;
      QrCliCli.SQL.Add('SELECT Codigo,');
      QrCliCli.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
      QrCliCli.SQL.Add('ELSE Nome END NOMEENTIDADE');
      QrCliCli.SQL.Add('FROM entidades');
      QrCliCli.SQL.Add('WHERE Cliente2="V"');
      QrCliCli.SQL.Add('AND Codigo in (');
      QrCliCli.SQL.Add('  SELECT ci2.Propriet');
      QrCliCli.SQL.Add('  FROM condimov ci2');
      QrCliCli.SQL.Add('  LEFT JOIN cond co2 ON co2.Codigo=ci2.Codigo');
      QrCliCli.SQL.Add('  WHERE co2.Cliente=:P0 ');
      QrCliCli.SQL.Add(') ORDER BY NOMEENTIDADE');
      QrCliCli.SQL.Add('');
      QrCliCli.Params[0].AsInteger := FmPrincipal.FEntInt;
      QrCliCli.Open;
    end;
  end;
end;

procedure TFmLanctosCond.SpeedButton2Click(Sender: TObject);
begin
  if DBCheck.CriaForm(TFmEntidades, FmEntidades, afmNegarComAviso) then
  begin
    FmEntidades.LocCod(QrFornecedoresCodigo.Value, QrFornecedoresCodigo.Value);
    FmEntidades.ShowModal;
    FmEntidades.Destroy;
    QrFornecedores.Close;
    QrFornecedores.Open;
    dmkEdCBFornece.Text := IntToStr(VAR_ENTIDADE);
    dmkCBFornece.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmLanctosCond.BitBtn1Click(Sender: TObject);
var
  Valor, Benef, Cidade: String;
begin
  if dmkEdDeb.Enabled then
  begin
    Valor := dmkEdDeb.Text;
    Benef := dmkCBFornece.Text;
  end else begin
    Valor := dmkEdCred.Text;
    Benef := dmkCBCliente.Text;
  end;
  //
  Benef := Geral.SemAcento(MLAGeral.Maiusculas(Benef, False));
  Cidade := Geral.SemAcento(MLAGeral.Maiusculas(Dmod.QrDonoCIDADE.Value, False));
  //
  Application.CreateForm(TFmEmiteCheque, FmEmiteCheque);
  FmEmiteCheque.TPData.Date   := dmkEdTPdata.Date;
  FmEmiteCheque.EdValor.Text  := Valor;
  FmEmiteCheque.EdBenef.Text  := Benef;
  FmEmiteCheque.EdCidade.Text := Cidade;
  //
  FmEmiteCheque.ShowModal;
  (*if FmEmiteCheque.ST_CMC7.Caption <> '' then
    EdBanda1.Text := MLAGeral.SoNumeroECMC7(FmEmiteCheque.ST_CMC7.Caption);*)
  FmEmiteCheque.Destroy;
end;

function TFmLanctosCond.NaoDuplicarLancto(): Boolean;
var
  Cheque, NotaF: Integer;
  SerieCH: String;
begin
  Result := False;
  if LaTipo.Caption <> CO_INCLUSAO then Exit;
  //
  Cheque := Geral.IMV(dmkEdDoc.Text);
  if CkPesqCH.Checked and (Cheque > 0) then
  begin
    SerieCH := Trim(dmkEdSerieCh.Text);
    QrDuplCH.Close;
    QrDuplCH.SQL.Clear;
    QrDuplCH.SQL.Add('SELECT lan.Data, lan.Controle, lan.Descricao, lan.Credito,');
    QrDuplCH.SQL.Add('lan.Debito, lan.NotaFiscal, lan.Compensado, lan.Mez,');
    QrDuplCH.SQL.Add('lan.Fornecedor, lan.Cliente, car.Nome NOMECART,');
    QrDuplCH.SQL.Add('lan.Documento, lan.SerieCH, lan.Carteira,');
    QrDuplCH.SQL.Add('IF(lan.Cliente<>0, CASE WHEN cli.Tipo=0 THEN');
    QrDuplCH.SQL.Add('cli.RazaoSocial ELSE cli.Nome END, "") NOMECLI,');
    QrDuplCH.SQL.Add('IF(lan.Fornecedor<>0, CASE WHEN fnc.Tipo=0 THEN');
    QrDuplCH.SQL.Add('fnc.RazaoSocial ELSE fnc.Nome END, "") NOMEFNC');
    QrDuplCH.SQL.Add('FROM lanctos lan');
    QrDuplCH.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    QrDuplCH.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=lan.Cliente');
    QrDuplCH.SQL.Add('LEFT JOIN entidades fnc ON fnc.Codigo=lan.Fornecedor');
    QrDuplCH.SQL.Add('WHERE ID_Pgto = 0');
    QrDuplCH.SQL.Add('AND lan.Documento=:P0');

    QrDuplCH.Params[0].AsInteger := Cheque;

    if SerieCH <> '' then
    begin
      QrDuplCH.SQL.Add('AND lan.SerieCH=:P1');
      QrDuplCH.Params[1].AsString := SerieCH;
    end;

    QrDuplCH.Open;
    Result := QrDuplCH.RecordCount > 0;
  end;

  NotaF := Geral.IMV(dmkEdNF.Text);
  if CkPesqNF.Checked and (NotaF > 0) then
  begin
    QrDuplNF.Close;
    QrDuplNF.Params[0].AsInteger := NotaF;
    QrDuplNF.Open;
    Result := Result or (QrDuplNF.RecordCount > 0);
  end;

  if Result then
  begin
    Application.CreateForm(TFmLanctosCondDuplic, FmLanctosCondDuplic);
    FmLanctosCondDuplic.FConfirma := False;
    if Cheque = 0 then FmLanctosCondDuplic.STCheque.Caption := 'Cheque: N�o pesquisado' else
      FmLanctosCondDuplic.STCheque.Caption := 'Cheque: '+SerieCH + ' ' + IntToStr(Cheque);
    if NotaF = 0 then FmLanctosCondDuplic.STNotaF.Caption := 'Nota Fiscal: N�o pesquisado' else
      FmLanctosCondDuplic.STNotaF.Caption := 'Nota Fiscal: '+IntToStr(NotaF);
    FmLanctosCondDuplic.ShowModal;
    Result := not FmLanctosCondDuplic.FConfirma;
    FmLanctosCondDuplic.Destroy;
  end;

  if Result then Screen.Cursor := crDefault;
end;


procedure TFmLanctosCond.QrDuplCHCalcFields(DataSet: TDataSet);
begin
  QrDuplCHTERCEIRO.Value := QrDuplCHNOMEFNC.Value + QrDuplCHNOMECLI.Value;
  QrDuplCHMES.Value := MLAGeral.MezToFDT(QrDuplCHMez.Value, 0, 104);
  QrDuplCHCHEQUE.Value := Trim(QrDuplCHSerieCH.Value) +
    FormatFloat('000000', QrDuplCHDocumento.Value);
  if QrDuplCHCompensado.Value = 0 then QrDuplCHCOMP_TXT.Value := '' else
    QrDuplCHCOMP_TXT.Value := Geral.FDT(QrDuplCHCompensado.Value, 2);
end;

procedure TFmLanctosCond.QrDuplNFCalcFields(DataSet: TDataSet);
begin
  QrDuplNFTERCEIRO.Value := QrDuplNFNOMEFNC.Value + QrDuplNFNOMECLI.Value;
  QrDuplNFMES.Value := MLAGeral.MezToFDT(QrDuplNFMez.Value, 0, 104);
  QrDuplNFCHEQUE.Value := Trim(QrDuplNFSerieCH.Value) +
    FormatFloat('000000', QrDuplNFDocumento.Value);
  if QrDuplNFCompensado.Value = 0 then QrDuplNFCOMP_TXT.Value := '' else
    QrDuplNFCOMP_TXT.Value := Geral.FDT(QrDuplNFCompensado.Value, 2);
end;

procedure TFmLanctosCond.MostraPnMaskPesq(Link: String);
begin
  PnLink.Caption     := Link;
  Width := FLargMaior;
  PnMaskPesq.Visible := True;
  EdLinkMask.Text    := '';
  EdLinkMask.SetFocus;
end;

procedure TFmLanctosCond.SelecionaItemDePesquisa;
begin
  if QrPesq.RecordCount > 0 then
  begin
    Width := FLargMenor;
    PnMaskPesq.Visible := False;
    if PnLink.Caption = 'Conta (Plano de contas)' then
    begin
      dmkEdCBGenero.Text     := IntToStr(QrPesqCodigo.Value);
      dmkCBGenero.KeyValue := QrPesqCodigo.Value;
      if dmkCBGenero.Visible then dmkCBGenero.SetFocus;
    end
  end;
end;

procedure TFmLanctosCond.LLBPesqDblClick(Sender: TObject);
begin
  SelecionaItemDePesquisa;
end;

procedure TFmLanctosCond.EdLinkMaskChange(Sender: TObject);
begin
  QrPesq.Close;
  if Length(EdLinkMask.Text) >= 4 then
  begin
    if PnLink.Caption = 'Conta (Plano de contas)' then
    begin
      QrPesq.Close;
      QrPesq.SQL.Clear;
      QrPesq.SQL.Add('SELECT Codigo, Nome ');
      QrPesq.SQL.Add('FROM contas ');
      QrPesq.SQL.Add('WHERE Nome LIKE "%'+EdLinkMask.Text+'%"');
      QrPesq.SQL.Add('ORDER BY Nome');
      //QrPesq.SQL.Add('');
      QrPesq.Open;
    end
  end;
end;

procedure TFmLanctosCond.SpeedButton3Click(Sender: TObject);
begin
  Width := FLargMenor;
  PnMaskPesq.Visible := False;
end;

procedure TFmLanctosCond.LLBPesqKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = 13 then SelecionaItemdePesquisa;
end;

procedure TFmLanctosCond.EdLinkMaskKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_DOWN then LLBPesq.SetFocus;
end;

procedure TFmLanctosCond.CkParcelamentoClick(Sender: TObject);
begin
  if CkParcelamento.Checked then
  begin
    GBParcelamento.Visible := True;
    DBGParcelas.Visible := True;
    CalculaParcelas;
  end else begin
    GBParcelamento.Visible := False;
    DBGParcelas.Visible := True;
  end;
end;

procedure TFmLanctosCond.RGPeriodoClick(Sender: TObject);
begin
  if RGPeriodo.ItemIndex = 1 then
  begin
    EdDias.Enabled := True;
    EdDias.SetFocus;
  end else begin
    EdDias.Enabled := False;
  end;
  CalculaParcelas;
end;

procedure TFmLanctosCond.EdDiasExit(Sender: TObject);
begin
  EdDias.Text := Geral.TFT(EdDias.Text, 0, siPositivo);
  CalculaParcelas;
end;

procedure TFmLanctosCond.RGIncremCHClick(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLanctosCond.CkArredondarClick(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLanctosCond.RGArredondarClick(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLanctosCond.DBGParcelasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then if TbParcPagtos.State in ([dsEdit, dsInsert])
  then TbParcpagtos.Post;
end;

function TFmLanctosCond.GetTipoValor: TTipoValor;
var
  D,C: Double;
begin
  if dmkEdDeb.Enabled  then D := Geral.DMV(dmkEdDeb.Text)  else D := 0;
  if dmkEdCred.Enabled then C := Geral.DMV(dmkEdCred.Text) else C := 0;
  FValorAParcelar := D + C;
  if (D>0) and (C>0) then
  begin
    Application.MessageBox('Valor n�o pode ser cr�dito e d�bito ao mesmo tempo!',
    'Erro', MB_OK+MB_ICONERROR);
    Result := tvNil;
    Exit;
  end else if D>0 then Result := tvDeb
  else if C>0 then Result := tvCred
  else Result := tvNil;
end;

procedure TFmLanctosCond.CkIncremDUClick(Sender: TObject);
begin
  RGIncremDupl.Visible := CkIncremDU.Checked;
  CalculaParcelas;
end;

procedure TFmLanctosCond.EdDuplSepExit(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLanctosCond.RGDuplSeqClick(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLanctosCond.BitBtn2Click(Sender: TObject);
var
  ValOrig, ValMult, ValJuro, ValNovo, PerMult, ValDife: Double;
begin
  ValOrig := Geral.DMV(dmkEdCred.Text) - Geral.DMV(dmkEdDeb.Text);
  if ValOrig < 0 then ValOrig := ValOrig * -1;
  ValNovo := Geral.DMV(dmkEdValNovo.Text);
  PerMult := Geral.DMV(dmkEdPerMult.Text);
  //
  ValDife := ValOrig - ValNovo;
  ValMult := Int(ValOrig * PerMult ) / 100;
  if ValMult > ValDife then ValMult := ValDife;
  ValJuro := ValDife - ValMult;
  //
  dmkEdMultaVal.Text := Geral.FFT(ValMult, 2, siNegativo);
  dmkEdMoraVal.Text  := Geral.FFT(ValJuro, 2, siNegativo);
end;

procedure TFmLanctosCond.CkIncremTxtClick(Sender: TObject);
begin
  GBIncremTxt.Visible := CkIncremTxt.Checked;
  CalculaParcelas;
end;

procedure TFmLanctosCond.QrDeptosAfterScroll(DataSet: TDataSet);
begin
  case FIDFinalidade of
    1:
    begin
      QrCliCli.Close;
      QrCliCli.SQL.Clear;
      QrCliCli.SQL.Add('SELECT ent.Codigo,');
      QrCliCli.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE');
      QrCliCli.SQL.Add('FROM intentosoc soc');
      QrCliCli.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=soc.Socio');
      QrCliCli.SQL.Add('WHERE soc.Codigo=:P0');
      QrCliCli.SQL.Add('ORDER BY NOMEENTIDADE');
      QrCliCli.SQL.Add('');
      QrCliCli.Params[0].AsInteger := QrDeptosCODI_1.Value;
      QrCliCli.Open;
      if not QrCliCli.Locate('Codigo', Geral.IMV(dmkEdCBCliCli.Text), []) then
      begin
        dmkEdCBCliCli.Text := '';
        dmkCBCliCli.KeyValue := NULL;
      end;
    end;
    2:
    begin
      //Nada
    end;
  end;
end;

procedure TFmLanctosCond.dmkEdCBGeneroChange(Sender: TObject);
begin
  VerificaEdits;
end;

procedure TFmLanctosCond.dmkEdCBGeneroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F7 then
    MostraPnMaskPesq('Conta (Plano de contas)');
end;

procedure TFmLanctosCond.dmkCBGeneroKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F7 then
    MostraPnMaskPesq('Conta (Plano de contas)');
end;

procedure TFmLanctosCond.dmkEdMesExit(Sender: TObject);
begin
  dmkEdDescricao.Text := QrContasNome2.Value+' '+dmkEdMes.Text;
end;

procedure TFmLanctosCond.dmkEdDebExit(Sender: TObject);
begin
  CalculaICMS;
end;

procedure TFmLanctosCond.dmkCBGeneroClick(Sender: TObject);
begin
  VerificaEdits;
end;

procedure TFmLanctosCond.dmkEdCredExit(Sender: TObject);
begin
  CalculaICMS;
end;

procedure TFmLanctosCond.dmkEdDocChange(Sender: TObject);
begin
  if Geral.DMV(dmkEdDoc.Text) = 0 then
  begin
    RGIncremCH.Enabled := False;
    //GBCheque.Visible := False;
  end else begin
    RGIncremCH.Enabled := True;
    //GBCheque.Visible := True;
  end;
end;

procedure TFmLanctosCond.dmkEdTPVenctoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F6 then
    dmkEdTPVencto.Date := dmkEdTPdata.Date;
end;

procedure TFmLanctosCond.ConfiguracoesIniciais();
begin
  MeConfig.Lines.Clear;
  QrDeptos.Close;
  QrDeptos.SQL.Clear;
  FIDFinalidade := Geral.IMV(Copy(LaFinalidade.Caption, 1, 3));
  case FIDFinalidade of
    1:
    begin
      //MeConfig.Lines.Add('-> Inclus�o e altera��o de la�amentos pr�prios');
      LaCliInt.Caption := 'Empresa (Filial):';
      //
      QrDeptos.SQL.Add('SELECT Codigo CODI_1, Nome NOME_1, FLOOR(0) CODI_2');
      QrDeptos.SQL.Add('FROM intentocad');
      QrDeptos.SQL.Add('WHERE Ativo=1');
      QrDeptos.SQL.Add('ORDER BY NOME_1');
      //
      QrCliInt.SQL.Clear;
      QrCliInt.SQL.Clear;
      QrCliInt.SQL.Add('SELECT Codigo, ');
      QrCliInt.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE');
      QrCliInt.SQL.Add('FROM entidades');
      QrCliInt.SQL.Add('WHERE Codigo=' + IntToStr(Dmod.QrControleDono.Value));
      QrCliInt.Open;
      //////
    end;
    2:
    begin
      //MeConfig.Lines.Add('-> Inclus�o e altera��o de la�amentos de clientes internos - Condom�nios');
      LaCliInt.Caption := 'Condom�nio (Cliente interno):';
      //
      QrDeptos.SQL.Add('SELECT Conta CODI_1, Unidade NOME_1, FLOOR(Propriet) CODI_2');
      QrDeptos.SQL.Add('FROM condimov');
      QrDeptos.SQL.Add('WHERE Codigo=:P0');
      QrDeptos.SQL.Add('ORDER BY NOME_1');
      QrDeptos.Params[0].AsInteger := dmkEdOneCliInt.ValueVariant;//FmCondGer.QrCondCodigo.Value;
      //
      if Trim(VAR_CLIENTEI) <> '' then
      begin
        QrCliInt.SQL.Clear;
        QrCliInt.SQL.Add('SELECT Codigo, ');
        QrCliInt.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE');
        QrCliInt.SQL.Add('FROM entidades');
        QrCliInt.SQL.Add('WHERE Cliente'+VAR_CLIENTEI+'="V"');
        QrCliInt.SQL.Add('ORDER BY NOMEENTIDADE');
        QrCliInt.Open;
        //////
        LaCliInt.Visible := True;
        dmkEdCBCliInt.Visible := True;
        dmkCBCliInt.Visible := True;
      end;
    end;
    else Application.MessageBox(PChar('Nenhuma finalidade foi definida para a ' +
    'janela de inclus�o / altera��o de lan�amentos!'), 'Aviso',
    MB_OK+MB_ICONWARNING);
  end;
  if QrDeptos.SQL.Text <> '' then QrDeptos.Open;
end;

procedure TFmLanctosCond.dmkCBForneceKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F5 then ReopenFornecedores(0) else
  if Key = VK_F6 then ReopenFornecedores(1);
end;

procedure TFmLanctosCond.dmkEdCBDeptoChange(Sender: TObject);
begin
  if not FCriandoForm then
  begin
    if FIDFinalidade = 2 then
    begin
      dmkEdCBCliCli.Text := FormatFloat('0', QrDeptosCODI_2.Value);
      dmkCBCliCli.KeyValue := Int(QrDeptosCODI_2.Value / 1);
    end;
  end;
end;

procedure TFmLanctosCond.dmkCBDeptoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_DELETE then
  begin
    dmkEdCBDepto.Text := '';
    dmkCBDepto.KeyValue := NULL;
  end;
end;

procedure TFmLanctosCond.dmkEdDescricaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  Mes: String;
begin
  if key in ([VK_F4, VK_F5, VK_F6]) then
  begin
    if dmkEdMes.Enabled then Mes := dmkEdMes.Text else Mes := '';
    if key=VK_F4 then dmkEdDescricao.Text := QrContasNome.Value+' '+Mes;
    if key=VK_F5 then dmkEdDescricao.Text := QrContasNome2.Value+' '+Mes;
    if key=VK_F6 then dmkEdDescricao.Text := QrContasNome3.Value+' '+Mes;
    if dmkEdDescricao.Enabled then dmkEdDescricao.SetFocus;
    dmkEdDescricao.SelStart := Length(dmkEdDescricao.Text);
    dmkEdDescricao.SelLength := 0;
  end else if key = VK_F8 then
  begin
    if dmkCBFornece.Enabled then
      dmkEdDescricao.Text := dmkEdDescricao.Text + ' - ' + dmkCBFornece.Text
    else if dmkCBCliente.Enabled then
      dmkEdDescricao.Text := dmkEdDescricao.Text + ' - ' + dmkCBCliente.Text;
  end;
end;

procedure TFmLanctosCond.dmkCBAccountKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  SetaAccount(Key, Shift);
end;

procedure TFmLanctosCond.dmkEdCBAccountKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  SetaAccount(Key, Shift);
end;

procedure TFmLanctosCond.dmkEdICMS_PExit(Sender: TObject);
begin
  CalculaICMS;
end;

procedure TFmLanctosCond.dmkEdICMS_PKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if MLAGeral.EhNumeroOuSinalOuSeparador(Key) then FICMS := 0;
end;

procedure TFmLanctosCond.dmkEdICMS_VExit(Sender: TObject);
begin
  CalculaICMS;
end;

procedure TFmLanctosCond.dmkEdICMS_VKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if MLAGeral.EhNumeroOuSinalOuSeparador(Key) then FICMS := 1;
end;

procedure TFmLanctosCond.dmkEdParcelasExit(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLanctosCond.Button1Click(Sender: TObject);
begin
  ShowMessage(IntToStr(Width));
end;

procedure TFmLanctosCond.IncrementaExecucoes();
begin
  dmkEdExecs.ValueVariant := dmkEdExecs.ValueVariant + 1;
end;

procedure TFmLanctosCond.dmkEdTPDataClick(Sender: TObject);
begin
  dmkEdTPVencto.Date := dmkEdTPData.Date;
end;

end.



