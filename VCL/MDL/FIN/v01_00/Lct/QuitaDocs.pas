unit QuitaDocs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Buttons, Db, (*DBTables,*) UnInternalConsts, UnMLAGeral,
  UnGOTOy, UMySQLModule, ExtCtrls, Mask, DBCtrls, mySQLDbTables, dmkGeral,
  dmkEdit, dmkEditCB, dmkDBLookupComboBox, dmkImage, UnDmkEnums;

type
  TFmQuitaDocs = class(TForm)
    QrLct1: TmySQLQuery;
    PainelDados: TPanel;
    Label1: TLabel;
    TPData1: TDateTimePicker;
    Label2: TLabel;
    EdDoc: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    DsLancto1: TDataSource;
    QrLct2: TmySQLQuery;
    DsLancto2: TDataSource;
    EdDescricao: TdmkEdit;
    EdLancto: TStaticText;
    LaCredito: TDBEdit;
    LaDebito: TDBEdit;
    LaCartNom: TDBEdit;
    EdSub: TStaticText;
    LaNF: TDBEdit;
    LaCartCod: TDBEdit;
    QrLct1Sub: TSmallintField;
    QrLct1Descricao: TWideStringField;
    QrLct1Credito: TFloatField;
    QrLct1Debito: TFloatField;
    QrLct1NotaFiscal: TIntegerField;
    QrLct1Documento: TFloatField;
    QrLct1Vencimento: TDateField;
    QrLct1Carteira: TIntegerField;
    QrLct1Data: TDateField;
    QrLct1NOMECARTEIRA: TWideStringField;
    QrLct1TIPOCARTEIRA: TIntegerField;
    QrLct1CARTEIRABANCO: TIntegerField;
    QrLct2Tipo: TSmallintField;
    QrLct2Credito: TFloatField;
    QrLct2Debito: TFloatField;
    QrLct2Sub: TSmallintField;
    QrLct2Carteira: TIntegerField;
    QrLct1Cliente: TIntegerField;
    QrLct1Fornecedor: TIntegerField;
    QrLct1DataDoc: TDateField;
    QrLct1Nivel: TIntegerField;
    QrLct1Vendedor: TIntegerField;
    QrLct1Account: TIntegerField;
    QrLct1Controle: TIntegerField;
    QrLct1CtrlIni: TIntegerField;
    QrLct2Controle: TIntegerField;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    CBCarteira: TdmkDBLookupComboBox;
    EdCarteira: TdmkEditCB;
    Label15: TLabel;
    SpeedButton1: TSpeedButton;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasBanco: TIntegerField;
    QrCarteirasForneceI: TIntegerField;
    QrLct1Genero: TIntegerField;
    Label8: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel1: TPanel;
    BtConfirma: TBitBtn;
    procedure BtDesisteClick(Sender: TObject);
    procedure EdDocExit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    FCtrlIni, FControle: Double;
  end;

var
  FmQuitaDocs: TFmQuitaDocs;

implementation

uses UnMyObjects, Module, UnFinanceiro, Carteiras, MyDBCheck;

{$R *.DFM}

procedure TFmQuitaDocs.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmQuitaDocs.EdDocExit(Sender: TObject);
begin
  EdDoc.Text := MLAGeral.TBT(EdDoc.Text, siPositivo);
end;

procedure TFmQuitaDocs.FormActivate(Sender: TObject);
var
  Controle: Double;
  Sub: Integer;
begin
  EdDoc.SetFocus;
  MyObjects.CorIniComponente();
  //
  Controle :=   Geral.DMV(EdLancto.Caption);
  Sub :=        Geral.IMV(EdSub.Caption);
  QrLct1.Params[0].AsFloat := Controle;
  QrLct1.Params[1].AsInteger := Sub;
  QrLct1.Open;
  if GOTOy.Registros(QrLct1) = 0 then
  begin
    ShowMessage('Registro n�o encontrado');
    BtConfirma.Enabled := False;
  end;
  TPData1.Date :=     QrLct1Vencimento.Value;
  EdDescricao.Text := QrLct1Descricao.Value;
  EdDoc.Text :=       MLAGeral.TBT(FloatToStr(QrLct1Documento.Value), siPositivo);
  //
  QrLct2.Close;
  QrLct2.Params[0].AsFloat := Controle;
  QrLct2.Params[1].AsInteger := QrLct1Sub.Value;
  QrLct2.Open;
  EdDescricao.setFocus;
end;

procedure TFmQuitaDocs.BtConfirmaClick(Sender: TObject);
var
  Texto: PChar;
  Doc, Controle, Controle2, Sub, Nivel, Carteira: Integer;
  CtrlIni: Double;
begin
  // Parei aqui
  // ver cliente interno!
  Nivel    := QrLct1Nivel.Value + 1;
  if FCtrlIni = -1000 then ShowMessage('"FCtrlIni" n�o definido');
  if FCtrlIni > 0 then CtrlIni := FCtrlIni else CtrlIni := FControle;
  //
  Controle := QrLct1Controle.Value;
  Sub      := QrLct1Sub.Value;
  Doc      := Geral.IMV(EdDoc.Text);
  Carteira := Geral.IMV(EdCarteira.Text);
  //
  if Carteira = 0 then
  begin
    Application.MessageBox('Defina a carteira!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if GOTOy.Registros(QrLct2) <> 0 then
  begin
    if ((QrLct2Credito.Value + QrLct2Debito.Value) =
      (QrLct1Credito.Value + QrLct1Debito.Value))
    and (GOTOy.Registros(QrLct2) = 1) and (QrLct2Tipo.Value = 1) then
    begin
      Texto := PChar('J� existe uma quita��o, e ela possui o mesmo valor,'+
      ' deseja difini-la como quita��o desta emiss�o ?');
      if Application.MessageBox(Texto, PChar(VAR_APPNAME),
      MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+MB_APPLMODAL) = ID_YES then
      begin
        {
        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Sit=3, ');
        Dmod.QrUpdM.SQL.Add('Carteira=:P0, Compensado=:P1,');
        Dmod.QrUpdM.SQL.Add('DataAlt=:P2, UserAlt=:P3');
        Dmod.QrUpdM.SQL.Add('WHERE Controle=:Pa AND Tipo=2');
        //
        Dmod.QrUpdM.Params[00].AsInteger := Carteira;
        Dmod.QrUpdM.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPData1.Date);
        Dmod.QrUpdM.Params[02].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
        Dmod.QrUpdM.Params[03].AsInteger := VAR_USUARIO;
        //
        Dmod.QrUpdM.Params[04].AsFloat := Controle;
        Dmod.QrUpdM.ExecSQL;
        }
        UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
        'Sit', 'Carteira', 'Compensado'], ['Controle', 'Tipo'], [3, Carteira,
        FormatDateTime(VAR_FORMATDATE, TPData1.Date)], [Controle, 2], True, '');
        //
        UFinanceiro.RecalcSaldoCarteira(Carteira, QrCarteiras, True, True);

        {
        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Carteira=:P0, ');
        Dmod.QrUpdM.SQL.Add('Data=:P1, DataAlt=:P2, UserAlt=:P3');
        Dmod.QrUpdM.SQL.Add('WHERE ID_Pgto=:Pa AND Tipo=1 AND ID_Pgto > 0');

        Dmod.QrUpdM.Params[00].AsInteger := Carteira;
        Dmod.QrUpdM.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPData1.Date);
        Dmod.QrUpdM.Params[02].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
        Dmod.QrUpdM.Params[03].AsInteger := VAR_USUARIO;
        //
        Dmod.QrUpdM.Params[04].AsFloat := Controle;
        Dmod.QrUpdM.ExecSQL;
        }
        if Controle > 0 then
        begin
          UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, ['Carteira', 'Data'], [
          'ID_Pgto', 'Tipo'], [Carteira, FormatDateTime(
          VAR_FORMATDATE, TPData1.Date)], [Controle, 1], True, '');
        end;
        UFinanceiro.RecalcSaldoCarteira(Carteira, QrCarteiras, True, True);
      end;
    end else ShowMessage('J� existe pagamentos/rolagem para esta emiss�o!');
  end else begin
    {
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Sit=3, ');
    Dmod.QrUpdM.SQL.Add('Compensado=:P0, Documento=:P1, Descricao=:P2, ');
    Dmod.QrUpdM.SQL.Add('DataAlt=:P3, UserAlt=:P4, Carteira=:P5 ');
    Dmod.QrUpdM.SQL.Add('WHERE Controle=:Pa AND Sub=:Pb AND Tipo=2');
    Dmod.QrUpdM.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TPData1.Date);
    Dmod.QrUpdM.Params[01].AsFloat   := Doc;
    Dmod.QrUpdM.Params[02].AsString  := EdDescricao.Text;
    Dmod.QrUpdM.Params[03].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpdM.Params[04].AsInteger := VAR_USUARIO;
    Dmod.QrUpdM.Params[05].AsInteger := Carteira;
    //
    Dmod.QrUpdM.Params[06].AsFloat   := Controle;
    Dmod.QrUpdM.Params[07].AsInteger := Sub;
    Dmod.QrUpdM.ExecSQL;
    }
    UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
      'Sit', 'Compensado', 'Documento', 'Descricao', 'Carteira'], [
      'Controle', 'Sub', 'Tipo'], [3, FormatDateTime(VAR_FORMATDATE, TPData1.Date),
      Doc, EdDescricao.Text, Carteira], [Controle, Sub, 2], True, '');
    //
    UFinanceiro.RecalcSaldoCarteira(QrLct1Carteira.Value,
      Qrcarteiras, True, True);

    UFinanceiro.LancamentoDefaultVARS;
    Controle2 := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', VAR_LCT, VAR_LCT, 'Controle');
    FLAN_Tipo                   := 1;
    FLAN_Genero                 := QrLct1Genero.Value; // antigo -5!!!
    FLAN_Sit                    := 3;
    FLAN_Data                   := FormatDateTime(VAR_FORMATDATE, TPData1.Date);
    FLAN_Controle               := Controle2;
    FLAN_Descricao              := EdDescricao.Text;
    FLAN_NotaFiscal             := QrLct1NotaFiscal.Value;
    FLAN_Debito                 := QrLct1Debito.Value;
    FLAN_Credito                := QrLct1Credito.Value;
    FLAN_Compensado             := CO_VAZIO;
    FLAN_Documento              := Doc;
    FLAN_Carteira               := QrCarteirasBanco.Value;
    FLAN_Cliente                := QrLct1Cliente.Value;
    FLAN_Fornecedor             := QrLct1Fornecedor.Value;
    FLAN_ID_Pgto                := Controle;
    FLAN_Sub                    := Sub;
    FLAN_DataCad                := FormatDateTime(VAR_FORMATDATE, Date);
    FLAN_UserCad                := VAR_USUARIO;
    FLAN_DataDoc                := FormatDateTime(VAR_FORMATDATE, QrLct1DataDoc.Value);
    FLAN_Vencimento             := FormatDateTime(VAR_FORMATDATE, QrLct1Vencimento.Value);
    FLAN_CtrlIni                := Trunc(CtrlIni+0.01);
    FLAN_Nivel                  := Nivel;
    FLAN_Vendedor               := QrLct1Vendedor.Value;
    FLAN_Account                := QrLct1Account.Value;
    UFinanceiro.InsereLancamento();
    //
    UFinanceiro.RecalcSaldoCarteira(QrCarteirasBanco.Value,
      QrCarteiras, True, True);
  end;
  Close;
end;

procedure TFmQuitaDocs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmQuitaDocs.SpeedButton1Click(Sender: TObject);
var
  Carteira: Integer;
begin
  VAR_CADASTRO := 0;
  Carteira := EdCarteira.ValueVariant;
  if DBCheck.CriaFm(TFmCarteiras, FmCarteiras, afmoNegarComAviso) then
  begin
    if Carteira <> 0 then
      FmCarteiras.LocCod(Carteira, Carteira);
    FmCarteiras.ShowModal;
    FmCarteiras.Destroy;
  end;
  //
  UMyMod.SetaCodigoPesquisado(EdCarteira, CBCarteira, QrCarteiras, VAR_CADASTRO);
end;

procedure TFmQuitaDocs.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stIns;
  //
  FCtrlIni := -1000;
  QrCarteiras.Open;
end;

end.
