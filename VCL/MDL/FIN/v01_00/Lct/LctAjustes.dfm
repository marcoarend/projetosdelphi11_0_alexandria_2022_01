object FmLctAjustes: TFmLctAjustes
  Left = 339
  Top = 185
  Caption = 'FIN-AJUST-001 :: Ajustes de Lan'#231'amentos'
  ClientHeight = 494
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 672
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Visible = False
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Ajustes de Lan'#231'amentos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 700
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 701
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 620
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 398
    Align = alClient
    TabOrder = 0
    object PnCorrige: TPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 41
      Align = alTop
      TabOrder = 0
      object LaAviso: TLabel
        Left = 4
        Top = 4
        Width = 13
        Height = 13
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object PB1: TProgressBar
        Left = 4
        Top = 20
        Width = 773
        Height = 17
        TabOrder = 0
      end
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 42
      Width = 782
      Height = 355
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' Compensa'#231#245'es n'#227'o corrigidas '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGNC: TDBGrid
          Left = 0
          Top = 0
          Width = 774
          Height = 327
          Align = alClient
          DataSource = DsNC
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencto'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CtrlQuitPg'
              Title.Caption = 'Ctrl Quit/pg'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Empresa'
              Title.Caption = 'Dono cart'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Carteira'
              Title.Caption = 'ID Carteira'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CART'
              Title.Caption = 'Nome da carteira'
              Width = 355
              Visible = True
            end>
        end
      end
    end
  end
  object QrNC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lct.Data, lct.Vencimento, lct.Controle, lct.CtrlQuitPg,'
      'car.ForneceI Empresa, lct.Carteira, car.Nome NO_CART'
      'FROM lanctos lct'
      'LEFT JOIN carteiras car ON car.Codigo=lct.Carteira'
      'WHERE lct.Sit in (2,3)'
      'AND lct.Compensado < 2'
      'AND car.Tipo=2'
      'ORDER BY NO_CART, lct.Data')
    Left = 232
    Top = 164
    object QrNCData: TDateField
      FieldName = 'Data'
      Origin = 'lanctos.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNCVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'lanctos.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNCControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'lanctos.Controle'
      DisplayFormat = '0;-0; '
    end
    object QrNCCtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
      Origin = 'lanctos.CtrlQuitPg'
      DisplayFormat = '0;-0; '
    end
    object QrNCEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'carteiras.ForneceI'
      DisplayFormat = '0;-0; '
    end
    object QrNCCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'lanctos.Carteira'
      DisplayFormat = '0;-0; '
    end
    object QrNCNO_CART: TWideStringField
      FieldName = 'NO_CART'
      Origin = 'carteiras.Nome'
      Size = 100
    end
  end
  object DsNC: TDataSource
    DataSet = QrNC
    Left = 260
    Top = 164
  end
end
