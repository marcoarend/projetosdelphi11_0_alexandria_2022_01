object FmLctMudaCart: TFmLctMudaCart
  Left = 404
  Top = 197
  Caption = 'Quita'#231#227'o em Caixa'
  ClientHeight = 162
  ClientWidth = 514
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 514
    Height = 66
    Align = alClient
    TabOrder = 0
    object Label15: TLabel
      Left = 8
      Top = 8
      Width = 67
      Height = 13
      Caption = 'Nova carteira:'
    end
    object EdCarteira: TdmkEditCB
      Left = 8
      Top = 24
      Width = 41
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCarteira
    end
    object CBCarteira: TdmkDBLookupComboBox
      Left = 51
      Top = 24
      Width = 454
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCarteiras
      TabOrder = 1
      dmkEditCB = EdCarteira
      UpdType = utYes
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 114
    Width = 514
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object BtCancela: TBitBtn
      Tag = 15
      Left = 402
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Cancela'
      TabOrder = 1
      OnClick = BtCancelaClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 514
    Height = 48
    Align = alTop
    Caption = 'Quita'#231#227'o em Caixa'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 512
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 468
      ExplicitHeight = 44
    end
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Tipo, ForneceI'
      'FROM carteiras'
      'ORDER BY Nome')
    Left = 376
    Top = 12
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 404
    Top = 12
  end
end
