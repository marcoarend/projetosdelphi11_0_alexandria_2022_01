unit LctEncerraMes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, DBCtrls, dmkGeral,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, Mask, UnDmkEnums;

type
  TFmLctEncerraMes = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrLast: TmySQLQuery;
    QrLastData: TDateField;
    QrPeriodos: TmySQLQuery;
    QrFeitos: TmySQLQuery;
    QrFeitosAM: TWideStringField;
    QrPeriodosAM: TWideStringField;
    QrPeriodosAno: TLargeintField;
    QrPeriodosMes: TLargeintField;
    QrPeriodosPeriodo: TLargeintField;
    QrFim: TmySQLQuery;
    QrMov: TmySQLQuery;
    QrFimCarteira: TIntegerField;
    QrFimSaldo: TFloatField;
    QrFimCredito: TFloatField;
    QrFimDebito: TFloatField;
    QrFimLctos: TLargeintField;
    QrLocAnt: TmySQLQuery;
    QrLocAntSaldoFim: TFloatField;
    QrLctoEncer: TmySQLQuery;
    DsLctoEncer: TDataSource;
    QrLctoEncerData: TDateField;
    QrLctoEncerCarteira: TIntegerField;
    QrLctoEncerLctos: TIntegerField;
    QrLctoEncerSaldoIni: TFloatField;
    QrLctoEncerCreditos: TFloatField;
    QrLctoEncerDebitos: TFloatField;
    QrLctoEncerSaldoFim: TFloatField;
    QrLctoEncerDataCad: TDateField;
    QrLctoEncerDataAlt: TDateField;
    QrLctoEncerUserCad: TIntegerField;
    QrLctoEncerUserAlt: TIntegerField;
    QrMeses: TmySQLQuery;
    DsMeses: TDataSource;
    QrLctoEncerNO_CARTEIRA: TWideStringField;
    QrMesesSaldoIni: TFloatField;
    QrMesesCreditos: TFloatField;
    QrMesesDebitos: TFloatField;
    QrMesesSaldoFim: TFloatField;
    QrMesesAM: TWideStringField;
    QrMesesData: TDateField;
    QrIniCart: TmySQLQuery;
    Label2: TLabel;
    Edit1: TEdit;
    Panel5: TPanel;
    DBGrid1: TDBGrid;
    Panel6: TPanel;
    GradeMeses: TDBGrid;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    LaAno: TLabel;
    LaMes: TLabel;
    CBMes: TComboBox;
    CBAno: TComboBox;
    BtEncerra: TBitBtn;
    BtDesfaz: TBitBtn;
    QrMovCarteira: TIntegerField;
    QrMovLctos: TLargeintField;
    QrMovCredito: TFloatField;
    QrMovDebito: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtEncerraClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure QrMesesBeforeClose(DataSet: TDataSet);
    procedure QrMesesAfterScroll(DataSet: TDataSet);
    procedure BtDesfazClick(Sender: TObject);
  private
    { Private declarations }
    FEmpresa: Integer;
    function EncerraPeriodoSelecionado(): Boolean;
    procedure ReopenMeses(Data: TDateTime);
    procedure ReopenLctoEncer(Carteira: Integer);
  public
    { Public declarations }
  end;

  var
  FmLctEncerraMes: TFmLctEncerraMes;

implementation

uses UnMyObjects, ModuleGeral, Module, UMySQLModule, UnInternalConsts;

{$R *.DFM}

procedure TFmLctEncerraMes.BtDesfazClick(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma o cancelamento do encerramento do m�s: ' +
  QrMesesAM.Value + '?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      DMod.QrUpd.SQL.Clear;
      DMod.QrUpd.SQL.Add('DELETE lae');
      DMod.QrUpd.SQL.Add('FROM lctoencer lae, carteiras car');
      DMod.QrUpd.SQL.Add('WHERE lae.Carteira=car.Codigo');
      DMod.QrUpd.SQL.Add('AND car.ForneceI=:P0');
      DMod.QrUpd.SQL.Add('AND lae.Data=:P1');
      DMod.QrUpd.Params[00].AsInteger := FEmpresa;
      DMod.QrUpd.Params[01].AsString  := Geral.FDT(QrMesesData.Value, 1);
      DMod.QrUpd.ExecSQL;
      //
      ReopenMeses(0);
    finally
      Screen.Cursor := crDefault;
    end;  
  end;
end;

procedure TFmLctEncerraMes.BtEncerraClick(Sender: TObject);
var
  DtIni, DtFim: TDateTime;
  DataI, DataF: String;
  Continua: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    if MyObjects.FIC(FEmpresa = 0, EdEmpresa, 'Informe o cliente interno') then Exit;
    //
    QrLast.Close;
    QrLast.Params[0].AsInteger := FEmpresa;
    QrLast.Open;
    //
    QrFeitos.Close;
    QrFeitos.Params[0].AsInteger := FEmpresa;
    QrFeitos.Open;
    //
    DtIni := QrLastData.Value + 1;
    DtFim := (MyObjects.CBAnoECBMesToDate(CBAno, CBMes, 1, 1) -1);
    if DtIni >= DtFim then
    begin
      Geral.MensagemBox('M�s j� encerrado!', 'Aviso', MB_OK+MB_ICONWARNING);
      Screen.Cursor := crDefault;
      Exit;
    end;
    DataI := Geral.FDT(DtIni, 1);
    DataF := Geral.FDT(DtFim, 1);
    //
    QrPeriodos.Close;
    QrPeriodos.Params[00].AsInteger := FEmpresa;
    QrPeriodos.Params[01].AsString  := DataI;
    QrPeriodos.Params[02].AsString  := DataF;
    QrPeriodos.Open;
    //
    Continua := ID_NO;
    case QrPeriodos.RecordCount of
      0: Geral.MensagemBox('N�o dados para encerramento de m�s solicitado!', 'Aviso', MB_OK+MB_ICONWARNING);
      1: Continua := ID_YES;
      else Continua := Geral.MensagemBox('Para encerrar o m�s solicitado' +
      ' � necess�rio encerrar ' + FormatFloat('0', QrPeriodos.RecordCount-1) +
      ' per�odos anteriores!'+#13#10+'Deseja encerrar assim mesmo?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION);
    end;
    if Continua = ID_YES then
    begin
      QrPeriodos.First;
      while not QrPeriodos.Eof do
      begin
        if not EncerraPeriodoSelecionado() then
        begin
          Screen.Cursor := crDefault;
          Break;
        end;
        //
        QrPeriodos.Next;
      end;
    end;
    Edit1.Text := '';
  finally
    ReopenMeses(0);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLctEncerraMes.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLctEncerraMes.EdEmpresaChange(Sender: TObject);
begin
  if EdEmpresa.ValueVariant = 0 then
    FEmpresa := 0
  else
    FEmpresa := DModG.QrEmpresasCodigo.Value;
  ReopenMeses(0);
end;

function TFmLctEncerraMes.EncerraPeriodoSelecionado(): Boolean;
var
  DataI, DataF, DataA: String;
  DataD: TDateTime;
  //
  Lctos, Carteira, Certos: Integer;
  SaldoIni, Creditos, Debitos, SaldoFim: Double;
  AnoMes, Data: String;
  A, B: Integer;
begin
  Result := False;
  DataD := Geral.PeriodoToDate(QrPeriodosPeriodo.Value, 1, False, detJustSum);
  DataI := Geral.FDT(DataD, 1);
  AnoMes := Geral.FDT(DataD, 20);
  DataD := Geral.PeriodoToDate(QrPeriodosPeriodo.Value + 1, 1, False, detJustSum) - 1;
  DataF := Geral.FDT(DataD, 1);
  DataD := Geral.PeriodoToDate(QrPeriodosPeriodo.Value, 1, False, detJustSum) - 1;
  DataA := Geral.FDT(DataD, 1);
  Edit1.Text := 'Encerrando periodo de ' + DataI + ' at� ' + DataF + '...';
  //
  QrMov.Close;
  QrMov.Params[00].AsInteger := FEmpresa;
  QrMov.Params[01].AsString := DataI;
  QrMov.Params[02].AsString := DataF;
  QrMov.Open;
  //  Deve ser depois pois tem lookup
  QrFim.Close;
  QrFim.Params[00].AsInteger := FEmpresa;
  QrFim.Params[01].AsString  := DataF;
  QrFim.Open;
  //
  Certos := 0;
  QrFim.First;
  while not QrFim.Eof do
  begin
    Carteira := QrFimCarteira.Value;
    //Lctos    := QrFimLctos.Value;
    Creditos := QrFimCredito.Value;
    Debitos  := QrFimDebito.Value;
    SaldoFim := QrFimSaldo.Value;
    SaldoIni := SaldoFim - Creditos + Debitos;
    Data     := DataF;
    //
    QrLocAnt.Close;
    QrLocAnt.Params[00].AsInteger := Carteira;
    QrLocAnt.Params[01].AsString  := DataA;
    QrLocAnt.Open;
    //
    A := Round(QrLocAntSaldoFim.Value * 100);
    B := Round(SaldoIni * 100);
    if A = B then
      Certos := Certos + 1;
    //
    QrFim.Next;
  end;
  if Certos <> QrFim.RecordCount then
  begin
    Geral.MensagemBox('ERRO de confer�ncia de saldos iniciais!'#13#10+
    'Carteiras com saldos incorretos: ' +
    FormatFloat('0', QrFim.RecordCount - Certos) + #13#10 +
    'AVISE A DERMATEK!', 'Erro', MB_OK+MB_ICONERROR);
    Screen.Cursor := crDefault;
    Exit;
  end;
  QrFim.First;
  while not QrFim.Eof do
  begin
    Carteira := QrFimCarteira.Value;
    Lctos    := QrFimLctos.Value;
    Creditos := QrFimCredito.Value;
    Debitos  := QrFimDebito.Value;
    SaldoFim := QrFimSaldo.Value;
    SaldoIni := SaldoFim - Creditos + Debitos;
    Data     := DataF;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'lctoencer', False, [
    'Lctos', 'SaldoIni', 'Creditos', 'Debitos', 'SaldoFim'], [
    'Data', 'Carteira'], [
    Lctos, SaldoIni, Creditos, Debitos, SaldoFim], [
    Data, Carteira], True);
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE ' + VAR_LCT + ' SET Encerrado=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Carteira=:P1');
    Dmod.QrUpd.SQL.Add('AND Data BETWEEN :P2 AND :P3');
    Dmod.QrUpd.Params[00].AsString  := AnoMes;
    Dmod.QrUpd.Params[01].AsInteger := Carteira;
    Dmod.QrUpd.Params[02].AsString  := DataI;
    Dmod.QrUpd.Params[03].AsString  := DataF;
    Dmod.QrUpd.ExecSQL;
    //
    QrFim.Next;
  end;
  //
  Result := True;
end;

procedure TFmLctEncerraMes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLctEncerraMes.FormCreate(Sender: TObject);
begin
  CBEmpresa.ListSource := DModG.DsEmpresas;
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, -1);
end;

procedure TFmLctEncerraMes.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLctEncerraMes.QrMesesAfterScroll(DataSet: TDataSet);
begin
  ReopenLctoEncer(0);
  BtDesfaz.Enabled := QrMeses.RecNo = 1;
end;

procedure TFmLctEncerraMes.QrMesesBeforeClose(DataSet: TDataSet);
begin
  BtDesfaz.Enabled := False;
  QrLctoEncer.Close;
end;

procedure TFmLctEncerraMes.ReopenLctoEncer(Carteira: Integer);
begin
  QrLctoEncer.Close;
  if EdEmpresa.ValueVariant > 0 then
  begin
    QrLctoEncer.Params[0].AsInteger := FEmpresa;
    QrLctoEncer.Params[1].AsString  := Geral.FDT(QrMesesData.Value, 1);
    QrLctoEncer.Open;
    if Carteira <> 0 then
      QrLctoEncer.Locate('Carteira', Carteira, []);
  end;
end;

procedure TFmLctEncerraMes.ReopenMeses(Data: TDateTime);
begin
  QrMeses.Close;
  if EdEmpresa.ValueVariant > 0 then
  begin
    QrMeses.Params[0].AsInteger := FEmpresa;
    QrMeses.Open;
    if Data > 2 then
      QrMeses.Locate('Data', Data, []);
  end;
end;

{ Parei aqui
  TODO:
  Verificar se todas carteiras est�o com saldo inicial zerado.
}
end.
