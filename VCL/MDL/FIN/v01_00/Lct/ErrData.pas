unit ErrData;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, ComCtrls,
  dmkEditDateTimePicker, dmkGeral, Variants;

type
  TFmErrData = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    LaTipo: TdmkLabel;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    TPData: TdmkEditDateTimePicker;
    Label3: TLabel;
    EdLancto: TdmkEdit;
    Label4: TLabel;
    QrPesq1: TmySQLQuery;
    SpeedButton1: TSpeedButton;
    QrPesq1Controle: TIntegerField;
    QrErr: TmySQLQuery;
    DsErr: TDataSource;
    DBGrid1: TDBGrid;
    QrErrLogCad: TWideStringField;
    QrErrLogAlt: TWideStringField;
    QrErrDataCad: TDateField;
    QrErrDataAlt: TDateField;
    QrErrData: TDateField;
    QrErrControle: TIntegerField;
    QrErrCredito: TFloatField;
    QrErrDebito: TFloatField;
    QrErrDescricao: TWideStringField;
    QrErrDocumento: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEmpresaEnter(Sender: TObject);
    procedure EdEmpresaExit(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    FEmpresa: Integer;
    procedure ReopenCarteiras();
  public
    { Public declarations }
  end;

  var
  FmErrData: TFmErrData;

implementation

{$R *.DFM}

uses UnMyObjects, ModuleGeral, Module;

procedure TFmErrData.BtOKClick(Sender: TObject);
begin
  QrErr.Close;
  QrErr.SQL.Clear;
  QrErr.SQL.Add('SELECT se1.Login LogCad, se2.Login LogAlt, lan.DataCad, lan.DataAlt,');
  QrErr.SQL.Add('lan.Data, lan.Controle, lan.Credito, lan.Debito, lan.Descricao, lan.Documento');
  QrErr.SQL.Add('FROM lanctos lan');
  QrErr.SQL.Add('LEFT JOIN senhas se1 ON se1.Numero=lan.UserCad');
  QrErr.SQL.Add('LEFT JOIN senhas se2 ON se2.Numero=lan.UserAlt');
  QrErr.SQL.Add('WHERE lan.Carteira=:P0');
  QrErr.SQL.Add('AND lan.Data < :P1');
  QrErr.SQL.Add('AND lan.Controle > :P2');
  //
  QrErr.Params[00].AsInteger := EdCarteira.ValueVariant;
  QrErr.Params[01].AsString  := Geral.FDT(TPData.Date, 1);
  QrErr.Params[02].AsInteger := EdLancto.ValueVariant;
  QrErr.Open;
end;

procedure TFmErrData.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmErrData.EdEmpresaChange(Sender: TObject);
begin
  if not EdEmpresa.Focused then
    ReopenCarteiras();
end;

procedure TFmErrData.EdEmpresaEnter(Sender: TObject);
begin
  FEmpresa := EdEmpresa.ValueVariant;
end;

procedure TFmErrData.EdEmpresaExit(Sender: TObject);
begin
  if FEmpresa <> EdEmpresa.ValueVariant then
  begin
    FEmpresa := EdEmpresa.ValueVariant;
    ReopenCarteiras();
  end;
end;

procedure TFmErrData.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmErrData.FormCreate(Sender: TObject);// Adicionado por .DFM > .PAS
CB?.ListSource = DModG.DsEmpresas;

begin
  FEmpresa := 0;
end;

procedure TFmErrData.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmErrData.ReopenCarteiras();
begin
  QrCarteiras.Close;
  QrCarteiras.Params[0].AsInteger := DModG.QrEmpresasCodigo.Value;
  QrCarteiras.Open;
  //
  EdCarteira.ValueVariant := 0;
  CBCarteira.KeyValue     := Null;
end;

procedure TFmErrData.SpeedButton1Click(Sender: TObject);
begin
{
SELECT MAX(Controle) Controle
FROM lanctos
WHERE Carteira=:P0
AND Data=:P1
}
  QrPesq1.Close;
  QrPesq1.Params[00].AsInteger := EdCarteira.ValueVariant;
  QrPesq1.Params[01].AsString  := Geral.FDT(TPData.Date, 1);
  QrPesq1.Open;
  //
  EdLancto.ValueVariant := QrPesq1Controle.Value;
end;

end.
