unit LctEdita;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, Variants,
  mySQLDbTables, Mask, dmkCheckBox, ComCtrls, dmkEditDateTimePicker,
  UnFinanceiro, dmkValUsu, UnDmkEnums;

type
  TFmLctEdita = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    QrCliInt: TmySQLQuery;
    QrCliIntCodigo: TIntegerField;
    QrCliIntNOMEENTIDADE: TWideStringField;
    DsCliInt: TDataSource;
    DsClientes: TDataSource;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesAccount: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    QrDeptos: TmySQLQuery;
    QrDeptosCODI_1: TIntegerField;
    QrDeptosCODI_2: TLargeintField;
    QrDeptosNOME_1: TWideStringField;
    DsDeptos: TDataSource;
    QrForneceI: TmySQLQuery;
    QrForneceICodigo: TIntegerField;
    QrForneceINOMEENTIDADE: TWideStringField;
    DsForneceI: TDataSource;
    DsFornecedores: TDataSource;
    QrFornecedores: TmySQLQuery;
    QrFornecedoresCodigo: TIntegerField;
    QrFornecedoresNOMEENTIDADE: TWideStringField;
    QrFunci: TmySQLQuery;
    QrFunciCodigo: TIntegerField;
    QrFunciNOMEENTIDADE: TWideStringField;
    DsFunci: TDataSource;
    DsVendedores: TDataSource;
    QrVendedores: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    QrAccounts: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsAccounts: TDataSource;
    DsCarteiras: TDataSource;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasFechamento: TIntegerField;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasTipo: TIntegerField;
    QrCarteirasExigeNumCheque: TSmallintField;
    QrCarteirasForneceI: TIntegerField;
    QrCarteirasBanco1: TIntegerField;
    QrCarteirasUsaTalao: TSmallintField;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasNome3: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrContasDataCad: TDateField;
    QrContasDataAlt: TDateField;
    QrContasUserCad: TSmallintField;
    QrContasUserAlt: TSmallintField;
    QrContasNOMESUBGRUPO: TWideStringField;
    QrContasNOMEGRUPO: TWideStringField;
    QrContasNOMECONJUNTO: TWideStringField;
    QrContasNOMEEMPRESA: TWideStringField;
    QrContasNOMEPLANO: TWideStringField;
    DsContas: TDataSource;
    PainelDados: TPanel;
    LaFunci: TLabel;
    Panel3: TPanel;
    PnLancto1: TPanel;
    Label14: TLabel;
    SbCarteira: TSpeedButton;
    BtContas: TSpeedButton;
    Label1: TLabel;
    Label2: TLabel;
    LaMes: TLabel;
    LaDeb: TLabel;
    LaCred: TLabel;
    LaNF: TLabel;
    LaDoc: TLabel;
    Label11: TLabel;
    LaVencimento: TLabel;
    LaCliInt: TLabel;
    LaCliente: TLabel;
    LaFornecedor: TLabel;
    SpeedButton2: TSpeedButton;
    LaForneceRN: TLabel;
    LaForneceFA: TLabel;
    Label15: TLabel;
    EdControle: TdmkEdit;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    CBGenero: TdmkDBLookupComboBox;
    EdCBGenero: TdmkEditCB;
    EdTPData: TdmkEditDateTimePicker;
    EdMes: TdmkEdit;
    EdDeb: TdmkEdit;
    EdCred: TdmkEdit;
    EdNF: TdmkEdit;
    EdSerieCH: TdmkEdit;
    EdDoc: TdmkEdit;
    EdDuplicata: TdmkEdit;
    EdTPVencto: TdmkEditDateTimePicker;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    EdCBCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdCBFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    PnDepto: TPanel;
    LaDepto: TLabel;
    CBDepto: TdmkDBLookupComboBox;
    EdCBDepto: TdmkEditCB;
    PnForneceI: TPanel;
    LaForneceI: TLabel;
    CBForneceI: TdmkDBLookupComboBox;
    EdCBForneceI: TdmkEditCB;
    PnAccount: TPanel;
    LaAccount: TLabel;
    EdCBAccount: TdmkEditCB;
    CBAccount: TdmkDBLookupComboBox;
    PnLancto3: TPanel;
    CkPesqNF: TCheckBox;
    CkPesqCH: TCheckBox;
    CkCancelado: TdmkCheckBox;
    CkPesqVal: TCheckBox;
    PnLancto2: TPanel;
    Label13: TLabel;
    Label20: TLabel;
    EdDescricao: TdmkEdit;
    EdQtde: TdmkEdit;
    PnMaskPesq: TPanel;
    PnLink: TPanel;
    Panel6: TPanel;
    SpeedButton3: TSpeedButton;
    Panel11: TPanel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Panel5: TPanel;
    Panel12: TPanel;
    Label18: TLabel;
    EdLinkMask: TEdit;
    LLBPesq: TDBLookupListBox;
    LaTipo: TdmkLabel;
    MeConfig: TMemo;
    Panel4: TPanel;
    LaICMS_P: TLabel;
    EdICMS_P: TdmkEdit;
    EdICMS_V: TdmkEdit;
    LaICMS_V: TLabel;
    EdCBVendedor: TdmkEditCB;
    CBVendedor: TdmkDBLookupComboBox;
    LaVendedor: TLabel;
    Label10: TLabel;
    EdCBFunci: TdmkEditCB;
    CBFunci: TdmkDBLookupComboBox;
    LaDataDoc: TLabel;
    EdTPDataDoc: TdmkEditDateTimePicker;
    QrPesq: TmySQLQuery;
    QrPesqCodigo: TIntegerField;
    QrPesqNome: TWideStringField;
    QrPesqNOMEPLANO: TWideStringField;
    QrPesqNOMESUBGRUPO: TWideStringField;
    QrPesqNOMEGRUPO: TWideStringField;
    QrPesqNOMECONJUNTO: TWideStringField;
    DsPesq: TDataSource;
    EdSub: TdmkEdit;
    EdTipo: TdmkEdit;
    Label3: TLabel;
    TPCompensado: TdmkEditDateTimePicker;
    EdSit: TdmkEdit;
    Label4: TLabel;
    QrEventosCad: TmySQLQuery;
    QrEventosCadCodigo: TIntegerField;
    QrEventosCadCodUsu: TIntegerField;
    QrEventosCadNome: TWideStringField;
    DsEventosCad: TDataSource;
    QrIndiPag: TmySQLQuery;
    QrIndiPagCodigo: TIntegerField;
    QrIndiPagNome: TWideStringField;
    DsIndiPag: TDataSource;
    LaEventosCad: TLabel;
    EdEventosCad: TdmkEditCB;
    CBEventosCad: TdmkDBLookupComboBox;
    SbEventosCad: TSpeedButton;
    Label60: TLabel;
    EdIndiPag: TdmkEditCB;
    CBIndiPag: TdmkDBLookupComboBox;
    SbIndiPag: TSpeedButton;
    dmkValUsu1: TdmkValUsu;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdCarteiraChange(Sender: TObject);
    procedure SbCarteiraClick(Sender: TObject);
    procedure EdTPDataClick(Sender: TObject);
    procedure EdCBGeneroChange(Sender: TObject);
    procedure EdCBGeneroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGeneroClick(Sender: TObject);
    procedure CBGeneroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtContasClick(Sender: TObject);
    procedure EdMesExit(Sender: TObject);
    procedure EdDebExit(Sender: TObject);
    procedure EdCredExit(Sender: TObject);
    procedure EdDocChange(Sender: TObject);
    procedure EdDocExit(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure EdCBAccountKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBAccountKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdDescricaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdICMS_PExit(Sender: TObject);
    procedure EdICMS_PKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdICMS_VKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdICMS_VExit(Sender: TObject);
    procedure EdLinkMaskChange(Sender: TObject);
    procedure EdLinkMaskKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure LLBPesqDblClick(Sender: TObject);
    procedure LLBPesqEnter(Sender: TObject);
    procedure LLBPesqExit(Sender: TObject);
    procedure LLBPesqKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrCliIntBeforeScroll(DataSet: TDataSet);
    procedure QrClientesAfterScroll(DataSet: TDataSet);
    procedure QrCarteirasAfterScroll(DataSet: TDataSet);
    procedure EdTPDataChange(Sender: TObject);
    procedure QrContasAfterScroll(DataSet: TDataSet);
    procedure BtOKClick(Sender: TObject);
    procedure EdCliIntChange(Sender: TObject);
    procedure SbEventosCadClick(Sender: TObject);
    procedure SbIndiPagClick(Sender: TObject);
  private
    { Private declarations }
    FICMS: Double;
    FCriandoForm: Boolean;
    procedure CalculaICMS();
    procedure CarteirasReopen();
    procedure ConfiguracoesIniciais();
    procedure ConfiguraComponentesCarteira();
    procedure ConfiguraVencimento();
    procedure ExigeFuncionario();
    procedure MostraPnMaskPesq(Link: String);
    procedure ReopenFornecedores(Tipo: Integer);
    procedure SelecionaItemDePesquisa();
    procedure SetaAccount(var Key: Word; Shift: TShiftState);
    procedure VerificaEdits();
    procedure VerificaAlteracaoDeTipoDeCarteira(TipoAnt, TipoNew: Integer);
    procedure EventosCadReopen(Entidade: Integer);
    procedure ReopenIndiPag();
  public
    { Public declarations }
    FOneAccount, FOneCliInt: Integer;
    FFinalidade: TLanctoFinalidade;
    FQrCrt, FQrLct: TmySQLQuery;
  end;

  var
  FmLctEdita: TFmLctEdita;

implementation

uses UnMyObjects, ModuleGeral, Module, UnInternalConsts, Principal, ModuleFin, MyDBCheck,
Contas, Entidades, UMySQLModule, EventosCad, IndiPag;

{$R *.DFM}

procedure TFmLctEdita.BtContasClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    FmContas.LocCod(QrContasCodigo.Value, QrContasCodigo.Value);
    FmContas.ShowModal;
    FmContas.Destroy;
    QrContas.Close;
    QrContas.Open;
    EdCBGenero.Text := IntToStr(VAR_CONTA);
    CBGenero.KeyValue := VAR_CONTA;
    //
    if (LaTipo.Caption = CO_INCLUSAO) (*and (CkDuplicando.Checked = False)*) then
      EdDescricao.Text := CBGenero.Text;
  end;
end;

procedure TFmLctEdita.BtDesisteClick(Sender: TObject);
begin
  Close;
  VAR_VALOREMITIRIMP := 0;
end;

procedure TFmLctEdita.BtOKClick(Sender: TObject);
begin
  if MyObjects.FIC(EdCliInt.ValueVariant = 0, EdCliInt, 'Informe o ' +
  Copy(LaCliInt.Caption, 1, Length(LaCliInt.Caption)-1) + '!') then
    Exit;
  //
  if (QrContasDebito.Value <> 'V') or (CkCancelado.Checked) then
    EdDeb.ValueVariant := 0;
  if (QrContasCredito.Value <> 'V') or (CkCancelado.Checked) then
    EdCred.ValueVariant := 0;
  //
  if MyObjects.FIC((EdCred.ValueVariant + EdDeb.ValueVariant = 0) and
  (CkCancelado.Checked = False), nil, 'Informe o valor!') then
    Exit;
  VerificaAlteracaoDeTipoDeCarteira(EdTipo.OldValor, EdTipo.ValueVariant);
  if UMyMod.ExecSQLInsUpdFm(FmLctEdita, LaTipo.SQLType, VAR_LCT, 0, Dmod.QrUpd) then
  begin
    if EdCarteira.OldValor <> EdCarteira.ValueVariant then
    begin
      UFinanceiro.RecalcSaldoCarteira(EdCarteira.ValueVariant, nil, False, False);
    end;
    UFinanceiro.RecalcSaldoCarteira(EdCarteira.OldValor, FQrCrt, True, True);
    if FQrLct <> nil then
      FQrLct.Locate('Controle', EdControle.ValueVariant, []);
    Close;
  end;
end;

procedure TFmLctEdita.CalculaICMS();
var
  C, D, B, V, P: Double;
begin
  V := Geral.DMV(EdICMS_V.Text);
  P := Geral.DMV(EdICMS_P.Text);
  C := Geral.DMV(EdCred.Text);
  D := Geral.DMV(EdDeb.Text);
  //if C > D then B := C - D else B := D - C;
  B := C-D;
  if FICMS = 0 then V := B * P / 100
  else if B <> 0 then P := V / B * 100 else P := 0;
  EdICMS_V.Text := Geral.FFT(V, 2, siPositivo);
  EdICMS_P.Text := Geral.FFT(P, 2, siPositivo);
end;

procedure TFmLctEdita.CarteirasReopen;
begin
  QrCarteiras.Close;
  QrCarteiras.SQL.Clear;
  QrCarteiras.SQL.Add('SELECT car.*');
  QrCarteiras.SQL.Add('FROM carteiras car');
  QrCarteiras.SQL.Add('WHERE car.ForneceI IN (' + VAR_LIB_EMPRESAS + ')');
  QrCarteiras.SQL.Add('ORDER BY Nome');
  QrCarteiras.Open;
end;

procedure TFmLctEdita.CBAccountKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  SetaAccount(Key, Shift);
end;

procedure TFmLctEdita.CBGeneroClick(Sender: TObject);
begin
  VerificaEdits();
end;

procedure TFmLctEdita.CBGeneroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F7 then
    MostraPnMaskPesq('Conta (Plano de contas)');
end;

procedure TFmLctEdita.ConfiguracoesIniciais();
begin
  QrCliInt.Close;
  QrCliInt.Open;
  //
  MeConfig.Lines.Clear;
  QrDeptos.Close;
  QrDeptos.SQL.Clear;
  //FIDFinalidade := Geral.IMV(Copy(LaFinalidade.Caption, 1, 3));
  case FFinalidade of
    lfProprio:
    begin
      LaCliInt.Caption := 'Empresa (Filial):';
      if Uppercase(Application.Title) = 'SYNKER' then
      begin
        //
        QrDeptos.SQL.Add('SELECT Codigo CODI_1, Nome NOME_1, FLOOR(0) CODI_2');
        QrDeptos.SQL.Add('FROM intentocad');
        QrDeptos.SQL.Add('WHERE Ativo=1');
        QrDeptos.SQL.Add('ORDER BY NOME_1');
        //
        {
        QrCliInt.SQL.Clear;
        QrCliInt.SQL.Clear;
        QrCliInt.SQL.Add('SELECT Codigo, ');
        QrCliInt.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE');
        QrCliInt.SQL.Add('FROM entidades');
        QrCliInt.SQL.Add('WHERE Codigo=' + IntToStr(Dmod.QrControleDono.Value));
        QrCliInt.Open;
        }
        //////
        //PnDepto.Visible   := True;
        //PnForneceI.Visible := True;
      end;
    end;
    lfCondominio:
    begin
      LaCliInt.Caption := 'Condom�nio (Cliente interno):';
      //
      //
      QrDeptos.SQL.Add('SELECT Conta CODI_1, Unidade NOME_1, FLOOR(Propriet) CODI_2');
      QrDeptos.SQL.Add('FROM condimov imv');
      QrDeptos.SQL.Add('LEFT JOIN cond con ON con.Codigo = imv.Codigo');
      //QrDeptos.SQL.Add('WHERE Codigo=:P0');
      QrDeptos.SQL.Add('WHERE con.Cliente=:P0');
      QrDeptos.SQL.Add('ORDER BY NOME_1');
      QrDeptos.Params[0].AsInteger := FOneCliInt;//EdOneCliInt.ValueVariant;//FmCondGer.QrCondCodigo.Value;
      //
      if Trim(VAR_CLIENTEI) <> '' then
      begin
        {
        QrCliInt.SQL.Clear;
        QrCliInt.SQL.Add('SELECT Codigo, ');
        QrCliInt.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE');
        QrCliInt.SQL.Add('FROM entidades');
        QrCliInt.SQL.Add('WHERE Cliente'+VAR_CLIENTEI+'="V"');
        QrCliInt.SQL.Add('ORDER BY NOMEENTIDADE');
        QrCliInt.Open;
        }
        //////
        LaCliInt.Visible := True;
        EdCliInt.Visible := True;
        CBCliInt.Visible := True;
      end;
      PnDepto.Visible   := True;
      PnForneceI.Visible := True;
    end;
    lfCicloCurso:
    begin
      {
      QrCliInt.SQL.Clear;
      QrCliInt.SQL.Add('SELECT Codigo, ');
      QrCliInt.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE');
      QrCliInt.SQL.Add('FROM entidades');
      QrCliInt.SQL.Add('WHERE CliInt>0 OR Codigo=-1');
      QrCliInt.SQL.Add('ORDER BY NOMEENTIDADE');
      QrCliInt.Open;
      }
      LaCliInt.Caption        := 'Empresa (Filial):';
      LaDepto.Caption         := '';
      LaAccount.Caption       := 'Respons�vel interno pelo movimento:';
      EdCBForneceI.Visible := False;
      CBForneceI.Visible   := False;
      //
      QrAccounts.SQL.Clear;
      QrAccounts.SQL.Add('SELECT Codigo, ');
      QrAccounts.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE');
      QrAccounts.SQL.Add('FROM entidades');
      QrAccounts.SQL.Add('WHERE Fornece2="V" OR Fornece3="V"');
      QrAccounts.Open;
      //
      PnAccount.Visible           := True;
      if LaTipo.Caption = CO_INCLUSAO then
      begin
        EdCBAccount.ValueVariant := FOneAccount;
        CBAccount.KeyValue       := FOneAccount;
      end;
    end;
    else Application.MessageBox(PChar('Nenhuma finalidade foi definida para a ' +
    'janela de inclus�o / altera��o de lan�amentos!'), 'Aviso',
    MB_OK+MB_ICONWARNING);
  end;
  if QrDeptos.SQL.Text <> '' then QrDeptos.Open;
end;

procedure TFmLctEdita.ConfiguraComponentesCarteira;
begin
  case QrCarteirasTipo.Value of
    0:
    begin
      if (QrCarteirasPrazo.Value = 1) then
      begin
        if (QrCarteirasUsaTalao.Value = 0) then
        begin
          LaDoc.Enabled := True;
          EdSerieCH.Enabled := True;
          EdDoc.Enabled := True;
        end;
        EdNF.Enabled := True;
        LaNF.Enabled := True;
      end else begin
        // N�mero bloqueto no cheque
        if (QrCarteirasUsaTalao.Value = 0) then
        begin
          LaDoc.Enabled := True;
          EdSerieCH.Enabled := True;
          EdDoc.Enabled := True;
        end;
        //
        EdNF.Enabled := True;
        LaNF.Enabled := True;
      end;
    end;
    1:
    begin
      if (QrCarteirasUsaTalao.Value = 0) then
      begin
        LaDoc.Enabled := True;
        EdSerieCH.Enabled := True;
        EdDoc.Enabled := True;
      end;
    end;
    2:
    begin
      if (QrCarteirasUsaTalao.Value = 0) then
      begin
        LaDoc.Enabled := True;
        EdSerieCH.Enabled := True;
        EdDoc.Enabled := True;
      end;
      //
      EdNF.Enabled := True;
      LaNF.Enabled := True;
    end;
  end;
  ConfiguraVencimento();
end;

procedure TFmLctEdita.ConfiguraVencimento();
begin
  if LaTipo.Caption = CO_INCLUSAO then
    EdTPVencto.Date := EdTPdata.Date;
end;

procedure TFmLctEdita.EdCarteiraChange(Sender: TObject);
var
  UsaTalao: Boolean;
begin
  UsaTalao := QrCarteirasUsaTalao.Value = 0;
  EdSerieCH.Enabled   := UsaTalao;
  EdDoc.Enabled       := UsaTalao;
  LaDoc.Enabled       := UsaTalao;
  EdTipo.ValueVariant := QrCarteirasTipo.Value;
end;

procedure TFmLctEdita.EdCBAccountKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  SetaAccount(Key, Shift);
end;

procedure TFmLctEdita.EdCBGeneroChange(Sender: TObject);
begin
  VerificaEdits();
  if (LaTipo.Caption = CO_INCLUSAO) (*and (CkDuplicando.Checked = False)*) then
    if EdCBGenero.ValueVariant <> 0 then
      EdDescricao.Text := QrContasNome.Value;
end;

procedure TFmLctEdita.EdCBGeneroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F7 then
    MostraPnMaskPesq('Conta (Plano de contas)');
end;

procedure TFmLctEdita.EdCliIntChange(Sender: TObject);
begin
  EventosCadReopen(EdCliInt.ValueVariant);
end;

procedure TFmLctEdita.EdCredExit(Sender: TObject);
begin
  CalculaICMS;
  //if CkParcelamento.Checked then CalculaParcelas;
end;

procedure TFmLctEdita.EdDebExit(Sender: TObject);
begin
  CalculaICMS();
  //if CkParcelamento.Checked then CalculaParcelas;
end;

procedure TFmLctEdita.EdDescricaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Mes: String;
begin
  if key in ([VK_F4, VK_F5, VK_F6]) then
  begin
    if EdMes.Enabled then Mes := EdMes.Text else Mes := '';
    if key=VK_F4 then EdDescricao.Text := QrContasNome.Value+' '+Mes;
    if key=VK_F5 then EdDescricao.Text := QrContasNome2.Value+' '+Mes;
    if key=VK_F6 then EdDescricao.Text := QrContasNome3.Value+' '+Mes;
    if EdDescricao.Enabled then EdDescricao.SetFocus;
    EdDescricao.SelStart := Length(EdDescricao.Text);
    EdDescricao.SelLength := 0;
  end else if key = VK_F8 then
  begin
    if CBFornece.Enabled then
      EdDescricao.Text := EdDescricao.Text + ' - ' + CBFornece.Text
    else if CBCliente.Enabled then
      EdDescricao.Text := EdDescricao.Text + ' - ' + CBCliente.Text;
  end;
end;

procedure TFmLctEdita.EdDocChange(Sender: TObject);
begin
{
  if Geral.DMV(EdDoc.Text) = 0 then
    RGIncremCH.Enabled := False
  else
    RGIncremCH.Enabled := True;
}
end;

procedure TFmLctEdita.EdDocExit(Sender: TObject);
begin
  //if CkParcelamento.Checked then CalculaParcelas;
end;

procedure TFmLctEdita.EdICMS_PExit(Sender: TObject);
begin
  CalculaICMS();
end;

procedure TFmLctEdita.EdICMS_PKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if MLAGeral.EhNumeroOuSinalOuSeparador(Key) then FICMS := 0;
end;

procedure TFmLctEdita.EdICMS_VExit(Sender: TObject);
begin
  CalculaICMS();
end;

procedure TFmLctEdita.EdICMS_VKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if MLAGeral.EhNumeroOuSinalOuSeparador(Key) then FICMS := 0;
end;

procedure TFmLctEdita.EdLinkMaskChange(Sender: TObject);
begin
  QrPesq.Close;
  if Length(EdLinkMask.Text) >= 3 then
  begin
    if PnLink.Caption = 'Conta (Plano de contas)' then
    begin
      QrPesq.Close;
      QrPesq.Params[0].AsString := '%' + EdLinkMask.Text + '%';
      QrPesq.Open;
      LLBPesq.ListSource := DsPesq;
    end
  end;
end;

procedure TFmLctEdita.EdLinkMaskKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_DOWN then LLBPesq.SetFocus;
end;

procedure TFmLctEdita.EdMesExit(Sender: TObject);
begin
  if (LaTipo.Caption = CO_INCLUSAO) (*and (CkDuplicando.Checked = False)*)  then
    EdDescricao.Text := QrContasNome2.Value + ' ' + EdMes.Text;
end;

procedure TFmLctEdita.EdTPDataChange(Sender: TObject);
begin
  ConfiguraVencimento();
end;

procedure TFmLctEdita.EdTPDataClick(Sender: TObject);
begin
  EdTPVencto.Date := EdTPData.Date;
end;

procedure TFmLctEdita.EventosCadReopen(Entidade: Integer);
begin
  QrEventosCad.Close;
  QrEventosCad.Params[0].AsInteger := Entidade;
  QrEventosCad.Open;
  //
  LaEventosCad.Enabled := QrEventosCad.RecordCount > 0;
  EdEventosCad.Enabled := QrEventosCad.RecordCount > 0;
  CBEventosCad.Enabled := QrEventosCad.RecordCount > 0;
  SbEventosCad.Enabled := QrEventosCad.RecordCount > 0;
  if not QrEventosCad.Locate('CodUsu', QrEventosCadCodUsu.Value, []) then
  begin
    EdEventosCad.ValueVariant := 0;
    CBEventosCad.KeyValue     := 0;
  end;
end;

procedure TFmLctEdita.ExigeFuncionario();
begin
  QrFunci.Close;
  QrFunci.SQL.Clear;
  QrFunci.SQL.Add('SELECT Codigo,');
  QrFunci.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
  QrFunci.SQL.Add('ELSE Nome END NOMEENTIDADE');
  QrFunci.SQL.Add('FROM entidades');
  QrFunci.SQL.Add('WHERE '+VAR_TIPOVARCLI+'="V"');
  QrFunci.SQL.Add('ORDER BY NomeENTIDADE');
  QrFunci.Open;
  Label10.Visible      := True;
  EdCBFunci.Visible := True;
  CBFunci.Visible   := True;
end;

procedure TFmLctEdita.FormActivate(Sender: TObject);
var
  Carteira: Integer;
begin
  if FFinalidade = lfUnknow then
  begin
    Geral.MensagemBox('ID da Finalidade n�o definida! Avise a DERMATEK!',
    'Erro', MB_OK+MB_ICONERROR);
    Close;
    Exit;
  end;
  CarteirasReopen;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    //CkContinuar.Visible := True;
    //CkParcelamento.Visible := True;
    //
    if VLAN_QrCarteiras <> nil then
    begin
      Carteira := VLAN_QrCarteiras.FieldByName('Codigo').AsInteger;
      if Carteira <> 0 then
      begin
        EdCarteira.ValueVariant := Carteira;
        CBCarteira.KeyValue     := Carteira;
      end;
    end;
  end;
  if EdCarteira.Enabled then EdCarteira.SetFocus;
  VerificaEdits();
  if FCriandoForm then
  begin
    ConfiguracoesIniciais();
  end;
  FCriandoForm := False;
  MyObjects.CorIniComponente();
  EdCarteiraChange(Self);
end;

procedure TFmLctEdita.FormCreate(Sender: TObject);
var
  CliInt: Integer;
begin
  EdTPData.MinDate := VAR_DATA_MINIMA;
  PnLancto1.BevelOuter  := bvNone;
  PnDepto.BevelOuter    := bvNone;
  PnForneceI.BevelOuter := bvNone;
  PnAccount.BevelOuter  := bvNone;
  //PnEdit05.BevelOuter := bvNone;
  //PnEdit06.BevelOuter := bvNone;
  PnLancto2.BevelOuter  := bvNone;
  PnLancto3.BevelOuter  := bvNone;
  //Width := FLargMenor;
  FCriandoForm := True;
  QrContas.Open;
  if VAR_CXEDIT_ICM then
  begin
    LaICMS_P.Visible := True;
    LaICMS_V.Visible := True;
    EdICMS_P.Visible := True;
    EdICMS_V.Visible := True;
  end;
  //if Trim(VAR_CLIENTEC) <> '' then
  //begin
    QrClientes.SQL.Clear;
    QrClientes.SQL.Add('SELECT Codigo, Account, ');
    QrClientes.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrClientes.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrClientes.SQL.Add('FROM entidades');
    QrClientes.SQL.Add('WHERE Cliente1="V"');
    QrClientes.SQL.Add('OR    Cliente2="V"');
    QrClientes.SQL.Add('OR    Cliente3="V"');
    QrClientes.SQL.Add('OR    Cliente4="V"');
    QrClientes.SQL.Add('OR    Terceiro="V"');
    QrClientes.SQL.Add('ORDER BY NOMEENTIDADE');
    QrClientes.Open;
    //////
    LaCliente.Visible := True;
    EdCBCliente.Visible := True;
    CBCliente.Visible := True;
    //////
    //EdCliInt.Enabled := False;
    //CBCliInt.Enabled := False;
    if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
    EdCliInt.Text := IntToStr(CliInt);
    CBCliInt.KeyValue := CliInt;
  //end;
  ReopenFornecedores(0);
  if Trim(VAR_FORNECEF) <> '' then
  begin
    //
    LaFornecedor.Visible := True;
    EdCBFornece.Visible := True;
    CBFornece.Visible := True;
  end;
  if Trim(VAR_FORNECEV) <> '' then
  begin
    QrVendedores.SQL.Clear;
    QrVendedores.SQL.Add('SELECT Codigo, ');
    QrVendedores.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrVendedores.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrVendedores.SQL.Add('FROM entidades');
    QrVendedores.SQL.Add('WHERE Fornece'+VAR_FORNECEV+'="V"');
    QrVendedores.SQL.Add('ORDER BY NOMEENTIDADE');
    QrVendedores.Open;
    //////
    QrAccounts.SQL.Clear;
    QrAccounts.SQL.Add('SELECT Codigo, ');
    QrAccounts.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrAccounts.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrAccounts.SQL.Add('FROM entidades');
    QrAccounts.SQL.Add('WHERE Fornece'+VAR_FORNECEV+'="V"');
    QrAccounts.SQL.Add('ORDER BY NOMEENTIDADE');
    QrAccounts.Open;
    //////
    LaVendedor.Visible := True;
    CBVendedor.Visible := True;
    CBVendedor.Visible   := True;
    PnAccount.Visible := True;
  end;
  EdTPDataDoc.Date := Date;
  if VAR_LANCTOCONDICIONAL1 then
  begin
    VAR_DATAINSERIR := Date;
    EdTPDataDoc.Enabled := False;
    LaDataDoc.Enabled := False;
    //
    EdCarteira.Enabled := False;
    CBCarteira.Enabled := False;
    EdTPdata.Enabled := False;
  end;
  if VAR_TIPOVARCLI <> '' then ExigeFuncionario();
  //PageControl1.ActivePageIndex := 0;
  //TabControl1.TabIndex := 0;
  ReopenIndiPag();
end;

procedure TFmLctEdita.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLctEdita.LLBPesqDblClick(Sender: TObject);
begin
  SelecionaItemDePesquisa();
end;

procedure TFmLctEdita.LLBPesqEnter(Sender: TObject);
begin
  DBEdit1.DataSource := DsPesq;
  DBEdit2.DataSource := DsPesq;
  DBEdit3.DataSource := DsPesq;
  DBEdit4.DataSource := DsPesq;
end;

procedure TFmLctEdita.LLBPesqExit(Sender: TObject);
begin
  DBEdit1.DataSource := DsContas;
  DBEdit2.DataSource := DsContas;
  DBEdit3.DataSource := DsContas;
  DBEdit4.DataSource := DsContas;
end;

procedure TFmLctEdita.LLBPesqKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then SelecionaItemdePesquisa();
end;

procedure TFmLctEdita.MostraPnMaskPesq(Link: String);
begin
{ Acho que n�o precisa 2011-01-26
  PnLink.Caption     := Link;
  EdLinkMask.Text    := '';
  EdLinkMask.SetFocus;
}
end;

procedure TFmLctEdita.QrCarteirasAfterScroll(DataSet: TDataSet);
begin
  ConfiguraComponentesCarteira();
end;

procedure TFmLctEdita.QrClientesAfterScroll(DataSet: TDataSet);
begin
  case FFinalidade of
    lfProprio: // Pr�prio
    begin
      {  Ver como fazer no self da adm de condom�nios!
      QrForneceI.Close;
      QrForneceI.SQL.Clear;
      QrForneceI.SQL.Add('SELECT ent.Codigo,');
      QrForneceI.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE');
      QrForneceI.SQL.Add('FROM intentosoc soc');
      QrForneceI.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=soc.Socio');
      QrForneceI.SQL.Add('WHERE soc.Codigo=:P0');
      QrForneceI.SQL.Add('ORDER BY NOMEENTIDADE');
      QrForneceI.SQL.Add('');
      QrForneceI.Params[0].AsInteger := QrDeptosCODI_1.Value;
      try
        QrForneceI.Open;
        if not QrForneceI.Locate('Codigo', Geral.IMV(EdCBForneceI.Text), []) then
        begin
          EdCBForneceI.Text := '';
          CBForneceI.KeyValue := Null;
        end;
      finally
        ;
      end;
      }
    end;
    lfCondominio: // Condom�nios
    begin
      //Nada
    end;
    lfCicloCurso: // Cursos c�clicos
    begin
      //Nada
    end;
  end;
end;

procedure TFmLctEdita.QrCliIntBeforeScroll(DataSet: TDataSet);
begin
{
  if FFinalidade = 0 then
    FFinalidade := Geral.IMV(Copy(LaFinalidade.Caption, 1, 3));
}
  case FFinalidade of
    lfProprio: // Pr�prios
    begin
      //Nada
    end;
    lfCondominio: // Condom�nios
    begin
      QrForneceI.Close;
      QrForneceI.SQL.Clear;
{
      QrForneceI.SQL.Add('SELECT Codigo,');
      QrForneceI.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
      QrForneceI.SQL.Add('ELSE Nome END NOMEENTIDADE');
      QrForneceI.SQL.Add('FROM entidades');
      QrForneceI.SQL.Add('WHERE Cliente2="V"');
      QrForneceI.SQL.Add('AND Codigo in (');
      QrForneceI.SQL.Add('  SELECT ci2.Propriet');
      QrForneceI.SQL.Add('  FROM condimov ci2');
      QrForneceI.SQL.Add('  LEFT JOIN cond co2 ON co2.Codigo=ci2.Codigo');
      QrForneceI.SQL.Add('  WHERE co2.Cliente IN (' + VAR_LIB_EMPRESAS + ')');
      QrForneceI.SQL.Add(') ORDER BY NOMEENTIDADE');
}
      QrForneceI.SQL.Add('SELECT DISTINCT ent.Codigo,');
      QrForneceI.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
      QrForneceI.SQL.Add('ELSE ent.Nome END NOMEENTIDADE');
      QrForneceI.SQL.Add('FROM condimov ci2');
      QrForneceI.SQL.Add('LEFT JOIN cond con ON con.Codigo = ci2.Codigo');
      QrForneceI.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=ci2.Propriet');
      //QrForneceI.SQL.Add('WHERE ci2.Codigo IN (' + VAR_LIB_EMPRESAS + ')');
      QrForneceI.SQL.Add('WHERE con.Cliente IN (' + VAR_LIB_EMPRESAS + ')');
      QrForneceI.SQL.Add('ORDER BY NOMEENTIDADE');
      QrForneceI.Open;
    end;
    lfCicloCurso: // Cursos c�clicos
    begin
      //Nada ?
    end;
  end;
end;

procedure TFmLctEdita.QrContasAfterScroll(DataSet: TDataSet);
begin
  VerificaEdits();
end;

procedure TFmLctEdita.ReopenFornecedores(Tipo: Integer);
begin
  QrFornecedores.Close;
  QrFornecedores.SQL.Clear;
  QrFornecedores.SQL.Add('SELECT Codigo, ');
  if Tipo = 0 then
  begin
    QrFornecedores.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrFornecedores.SQL.Add('ELSE Nome END NOMEENTIDADE');
    //
    LaFornecedor.Font.Color := LaForneceRN.Font.Color;
  end else begin
    QrFornecedores.SQL.Add('CASE WHEN Tipo=0 THEN Fantasia');
    QrFornecedores.SQL.Add('ELSE Apelido END NOMEENTIDADE');
    //
    LaFornecedor.Font.Color := LaForneceFA.Font.Color;
  end;
  QrFornecedores.SQL.Add('FROM entidades');
  QrFornecedores.SQL.Add('WHERE Fornece1="V"');
  QrFornecedores.SQL.Add('OR    Fornece2="V"');
  QrFornecedores.SQL.Add('OR    Fornece3="V"');
  QrFornecedores.SQL.Add('OR    Fornece4="V"');
  QrFornecedores.SQL.Add('OR    Fornece5="V"');
  QrFornecedores.SQL.Add('OR    Fornece6="V"');
  QrFornecedores.SQL.Add('OR    Terceiro="V"');
  QrFornecedores.SQL.Add('ORDER BY NOMEENTIDADE');
  QrFornecedores.Open;
  //
end;

procedure TFmLctEdita.ReopenIndiPag();
begin
  QrIndiPag.Close;
  QrIndiPag.Open;
end;

procedure TFmLctEdita.SbCarteiraClick(Sender: TObject);
begin
  VAR_CADASTRO := EdCarteira.ValueVariant;
  FmPrincipal.CadastroDeCarteiras(QrCarteirasTipo.Value,
    Geral.IMV(EdCarteira.Text));
  QrCarteiras.Close;
  QrCarteiras.Open;
  // N�o fazer, a carteira talvez n�o seje do cliente interno selecionado!
  //dmkEdCarteira.Text := IntToStr(VAR_CARTNUM);
  //dmkCBCarteira.KeyValue := VAR_CARTNUM;
  DmodFin.DefParams(VLAN_QrCarteiras, nil, False, 'TFmLctEdita.SbCarteiraClick()');
end;

procedure TFmLctEdita.SbEventosCadClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmEventosCad, FmEventosCad, afmoNegarComAviso) then
  begin
    FmEventosCad.ShowModal;
    FmEventosCad.Destroy;
    //
    if VAR_CADASTRO <> 0 then
      UMyMod.SetaCodUsuDeCodigo(EdEventosCad, CBEventosCad, QrEventosCad,
        VAR_CADASTRO);
  end;
end;

procedure TFmLctEdita.SbIndiPagClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmIndiPag, FmIndiPag, afmoNegarComAviso) then
  begin
    FmIndiPag.ShowModal;
    FmIndiPag.Destroy;
    //
    if VAR_CADASTRO <> 0 then
      UMyMod.SetaCodigoPesquisado(EdIndiPag, CBIndiPag, QrIndiPag,
        VAR_CADASTRO);
  end;
end;

procedure TFmLctEdita.SelecionaItemDePesquisa;
begin
  if QrPesq.RecordCount > 0 then
  begin
    if PnLink.Caption = 'Conta (Plano de contas)' then
    begin
      EdCBGenero.Text     := IntToStr(QrPesqCodigo.Value);
      CBGenero.KeyValue := QrPesqCodigo.Value;
      if CBGenero.Visible then CBGenero.SetFocus;
    end
  end;
end;

procedure TFmLctEdita.SetaAccount(var Key: Word; Shift: TShiftState);
begin
  if key=VK_F4 then
  begin
    EdCBAccount.Text   := IntToStr(QrClientesAccount.Value);
    CBAccount.KeyValue := QrClientesAccount.Value;
  end;
end;

procedure TFmLctEdita.SpeedButton2Click(Sender: TObject);
begin
  VAR_ENTIDADE := 0;
  DModG.CadastroDeEntidade(EdCBFornece.ValueVariant, fmcadSelecionar, fmcadSelecionar);
  QrFornecedores.Close;
  QrFornecedores.Open;
  if VAR_ENTIDADE <> 0 then
  begin
    EdCBFornece.Text := IntToStr(VAR_ENTIDADE);
    CBFornece.KeyValue := VAR_ENTIDADE;
  end;
end;

procedure TFmLctEdita.VerificaAlteracaoDeTipoDeCarteira(TipoAnt, TipoNew: Integer);
begin
  if CkCancelado.Checked then
  begin
    EdSit.ValueVariant := 4;
    TPCompensado.Date := EdTPData.Date;
  end else begin
    if (TipoNew <> 2) then
    begin
      // alterou para � vista
      if (TipoNew = 0) and (QrCarteirasPrazo.Value = 1) then
        TPCompensado.Date := EdTPVencto.Date
      else
        TPCompensado.Date := EdTPData.Date;
      //
      EdSit.ValueVariant := 3;
      //
    end else
    // alterou para emiss�o
    // e a emiss�o s� pode ser editada se estiver sem quita��o!
    begin
      TPCompensado.Date  := 0;
      EdSit.ValueVariant := 0;
    end;
    // o que fazer com CtrlQuitPag e ID_Pgto?
    // se mudar uma quita��o com eles ele n�o pode ser alterado!
    // o certo seria n�o permitir a altera��o neste caso!
  end;  
end;

procedure TFmLctEdita.VerificaEdits();
begin
  if (QrContasMensal.Value = 'V') or (Dmod.QrControleMensalSempre.Value=1) then
  begin
    EdMes.Enabled := True;
    LaMes.Enabled := True;
  end else begin
    EdMes.Enabled := False;
    LaMes.Enabled := False;
  end;
  //
  if (VAR_BAIXADO <> -2) and not VAR_FATURANDO then Exit;
  if QrContasCredito.Value = 'V' then
  begin
    EdCred.Enabled := True;
    LaCred.Enabled := True;
    EdCBCliente.Enabled := True;
    LaCliente.Enabled := True;
    CBCliente.Enabled := True;
  end else begin
    EdCred.Enabled := False;
    LaCred.Enabled := False;
    EdCBCliente.Enabled := False;
    LaCliente.Enabled := False;
    CBCliente.Enabled := False;
  end;

  if QrContasDebito.Value = 'V' then
  begin
    EdDeb.Enabled := True;
    LaDeb.Enabled := True;
    EdCBFornece.Enabled := True;
    LaFornecedor.Enabled := True;
    CBFornece.Enabled := True;
  end else begin
    EdDeb.Enabled := False;
    LaDeb.Enabled := False;
    EdCBFornece.Enabled := False;
    LaFornecedor.Enabled := False;
    CBFornece.Enabled := False;
  end;

end;

end.
