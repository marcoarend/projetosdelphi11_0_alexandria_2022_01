object FmLctEncerraMes: TFmLctEncerraMes
  Left = 339
  Top = 185
  Caption = 'FIN-PLCTA-032 :: Encerramento Mensal Financeiro'
  ClientHeight = 573
  ClientWidth = 962
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 525
    Width = 962
    Height = 48
    Align = alBottom
    TabOrder = 1
    object Panel2: TPanel
      Left = 850
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtEncerra: TBitBtn
      Tag = 10
      Left = 8
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Encerra'
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BtEncerraClick
    end
    object BtDesfaz: TBitBtn
      Tag = 12
      Left = 100
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Desfaz'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtDesfazClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 962
    Height = 48
    Align = alTop
    Caption = 'Encerramento Mensal Financeiro'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 960
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 962
    Height = 477
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 962
      Height = 45
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 70
        Height = 13
        Caption = 'Cliente interno:'
      end
      object Label2: TLabel
        Left = 556
        Top = 4
        Width = 34
        Height = 13
        Caption = 'Avisos:'
      end
      object EdEmpresa: TdmkEditCB
        Left = 12
        Top = 20
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInt64
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdEmpresaChange
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 56
        Top = 20
        Width = 493
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object Edit1: TEdit
        Left = 556
        Top = 20
        Width = 397
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
    end
    object Panel5: TPanel
      Left = 358
      Top = 45
      Width = 604
      Height = 432
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 604
        Height = 432
        Align = alClient
        DataSource = DsLctoEncer
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Carteira'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_CARTEIRA'
            Title.Caption = 'Descri'#231#227'o da carteira'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Lctos'
            Title.Caption = 'Lan'#231'tos'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SaldoIni'
            Title.Caption = 'Saldo inicial'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Creditos'
            Title.Caption = 'Cr'#233'ditos'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Debitos'
            Title.Caption = 'D'#233'bitos'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SaldoFim'
            Title.Caption = 'Saldo final'
            Visible = True
          end>
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 45
      Width = 358
      Height = 432
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 2
      object GradeMeses: TDBGrid
        Left = 0
        Top = 72
        Width = 358
        Height = 360
        Align = alClient
        DataSource = DsMeses
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'AM'
            Title.Caption = 'Ano/M'#234's'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SaldoIni'
            Title.Caption = 'Saldo inicial'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Creditos'
            Title.Caption = 'Cr'#233'ditos'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Debitos'
            Title.Caption = 'D'#233'bitos'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SaldoFim'
            Title.Caption = 'Saldo Final'
            Width = 68
            Visible = True
          end>
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 358
        Height = 72
        Align = alTop
        TabOrder = 1
        object GroupBox1: TGroupBox
          Left = 12
          Top = 4
          Width = 233
          Height = 61
          Caption = ' Per'#237'odo a ser encerrado: '
          TabOrder = 0
          object LaAno: TLabel
            Left = 156
            Top = 14
            Width = 22
            Height = 13
            Caption = 'Ano:'
          end
          object LaMes: TLabel
            Left = 8
            Top = 14
            Width = 23
            Height = 13
            Caption = 'M'#234's:'
          end
          object CBMes: TComboBox
            Left = 8
            Top = 31
            Width = 145
            Height = 21
            Color = clWhite
            DropDownCount = 12
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7622183
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            Text = 'CBMes'
          end
          object CBAno: TComboBox
            Left = 156
            Top = 31
            Width = 69
            Height = 21
            Color = clWhite
            DropDownCount = 3
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 7622183
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            Text = 'CBAno'
          end
        end
      end
    end
  end
  object QrLast: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(lae.Data) Data '
      'FROM lctoencer lae'
      'LEFT JOIN carteiras car ON car.Codigo=lae.Carteira'
      'WHERE car.ForneceI=:P0'
      '')
    Left = 40
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLastData: TDateField
      FieldName = 'Data'
      Required = True
    end
  end
  object QrPeriodos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT YEAR(lan.Data) Ano, MONTH(lan.Data) Mes,'
      '((YEAR(lan.Data) - 2000) * 12) + MONTH(lan.Data) Periodo,'
      'CONCAT(LPAD(MONTH(lan.Data), 2, "0"), YEAR(lan.Data)) AM'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'WHERE car.ForneceI=:P0'
      'AND lan.Data BETWEEN :P1 AND :P2')
    Left = 68
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPeriodosAM: TWideStringField
      FieldName = 'AM'
      Size = 6
    end
    object QrPeriodosAno: TLargeintField
      FieldName = 'Ano'
    end
    object QrPeriodosMes: TLargeintField
      FieldName = 'Mes'
    end
    object QrPeriodosPeriodo: TLargeintField
      FieldName = 'Periodo'
    end
  end
  object QrFeitos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT'
      'CONCAT(LPAD(MONTH(lae.Data), 2, "0"), YEAR(lae.Data)) AM'
      'FROM lctoencer lae'
      'LEFT JOIN carteiras car ON car.Codigo=lae.Carteira'
      'WHERE car.ForneceI=:P0'
      'ORDER BY AM DESC'
      '')
    Left = 96
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFeitosAM: TWideStringField
      FieldName = 'AM'
      Size = 6
    end
  end
  object QrFim: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lan.Carteira, SUM(lan.Credito-lan.Debito) Saldo'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'WHERE car.Tipo <> 2'
      'AND car.ForneceI=:P0'
      'AND Data <= :P1'
      'GROUP BY car.Codigo')
    Left = 40
    Top = 232
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrFimCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrFimSaldo: TFloatField
      FieldName = 'Saldo'
    end
    object QrFimCredito: TFloatField
      FieldKind = fkLookup
      FieldName = 'Credito'
      LookupDataSet = QrMov
      LookupKeyFields = 'Carteira'
      LookupResultField = 'Credito'
      KeyFields = 'Carteira'
      Lookup = True
    end
    object QrFimDebito: TFloatField
      FieldKind = fkLookup
      FieldName = 'Debito'
      LookupDataSet = QrMov
      LookupKeyFields = 'Carteira'
      LookupResultField = 'Debito'
      KeyFields = 'Carteira'
      Lookup = True
    end
    object QrFimLctos: TLargeintField
      FieldKind = fkLookup
      FieldName = 'Lctos'
      LookupDataSet = QrMov
      LookupKeyFields = 'Carteira'
      LookupResultField = 'lctos'
      KeyFields = 'Carteira'
      Lookup = True
    end
  end
  object QrMov: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lan.Carteira, COUNT(Controle) Lctos,'
      'SUM(lan.Credito) Credito, SUM(lan.Debito) Debito'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'WHERE car.Tipo <> 2'
      'AND car.ForneceI=:P0'
      'AND Data BETWEEN :P1 AND :P2'
      'GROUP BY car.Codigo')
    Left = 68
    Top = 232
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrMovCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrMovLctos: TLargeintField
      FieldName = 'Lctos'
      Required = True
    end
    object QrMovCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrMovDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object QrLocAnt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SaldoFim'
      'FROM lctoencer'
      'WHERE Carteira=:P0'
      'AND Data=:P1'
      ' ')
    Left = 96
    Top = 232
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocAntSaldoFim: TFloatField
      FieldName = 'SaldoFim'
    end
  end
  object QrLctoEncer: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Nome NO_CARTEIRA, lae.*'
      'FROM lctoencer lae'
      'LEFT JOIN carteiras car ON car.Codigo=lae.Carteira'
      'WHERE car.ForneceI=:P0'
      'AND lae.Data=:P1'
      'ORDER BY NO_CARTEIRA')
    Left = 248
    Top = 232
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLctoEncerData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctoEncerCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctoEncerLctos: TIntegerField
      FieldName = 'Lctos'
      DisplayFormat = '0;-0; '
    end
    object QrLctoEncerSaldoIni: TFloatField
      FieldName = 'SaldoIni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLctoEncerCreditos: TFloatField
      FieldName = 'Creditos'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLctoEncerDebitos: TFloatField
      FieldName = 'Debitos'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLctoEncerSaldoFim: TFloatField
      FieldName = 'SaldoFim'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrLctoEncerDataCad: TDateField
      FieldName = 'DataCad'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctoEncerDataAlt: TDateField
      FieldName = 'DataAlt'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctoEncerUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLctoEncerUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLctoEncerNO_CARTEIRA: TWideStringField
      FieldName = 'NO_CARTEIRA'
      Size = 100
    end
  end
  object DsLctoEncer: TDataSource
    DataSet = QrLctoEncer
    Left = 276
    Top = 232
  end
  object QrMeses: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrMesesBeforeClose
    AfterScroll = QrMesesAfterScroll
    SQL.Strings = (
      'SELECT lae.Data, SUM(lae.SaldoIni) SaldoIni,'
      'SUM(lae.Creditos) Creditos, SUM(lae.Debitos) Debitos,'
      'SUM(lae.SaldoFim) SaldoFim,'
      'CONCAT(YEAR(lae.Data), "/", LPAD(MONTH(lae.Data), 2, "0")) AM'
      'FROM lctoencer lae'
      'LEFT JOIN carteiras car ON car.Codigo=lae.Carteira'
      'WHERE car.ForneceI=:P0'
      'GROUP BY Data'
      'ORDER BY AM DESC')
    Left = 248
    Top = 204
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMesesSaldoIni: TFloatField
      FieldName = 'SaldoIni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrMesesCreditos: TFloatField
      FieldName = 'Creditos'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrMesesDebitos: TFloatField
      FieldName = 'Debitos'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrMesesSaldoFim: TFloatField
      FieldName = 'SaldoFim'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrMesesAM: TWideStringField
      FieldName = 'AM'
      Size = 7
    end
    object QrMesesData: TDateField
      FieldName = 'Data'
    end
  end
  object DsMeses: TDataSource
    DataSet = QrMeses
    Left = 276
    Top = 204
  end
  object QrIniCart: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Inicial) Inicial'
      'FROM carteiras'
      'WHERE ForneceI=:P0')
    Left = 168
    Top = 280
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
end
