unit LctAjustesB;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, dmkGeral,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, frxClass, frxDBSet, UnDmkEnums, UnDmkEnums, dmkImage;

type
  TFmLctAjustesB = class(TForm)
    Panel1: TPanel;
    QrEmiss: TmySQLQuery;
    Panel3: TPanel;
    QrQuit: TmySQLQuery;
    QrEmissData: TDateField;
    QrEmissCtrlQuitPg: TIntegerField;
    QrSemQuita: TmySQLQuery;
    QrSemQuitaControle: TIntegerField;
    QrSemQuitaData: TDateField;
    QrSemQuitaDescricao: TWideStringField;
    DsSemQuita: TDataSource;
    DBGrid1: TDBGrid;
    QrEmissDescricao: TWideStringField;
    QrEmissControle: TIntegerField;
    QrEmissCompensado: TDateField;
    QrEmissCarteira: TIntegerField;
    QrPgto: TmySQLQuery;
    LaResu: TLabel;
    QrSemQuitaCarteira: TIntegerField;
    QrSemQuitaCredito: TFloatField;
    QrSemQuitaDebito: TFloatField;
    QrEmissCredito: TFloatField;
    QrEmissDebito: TFloatField;
    frxDsSemQuita: TfrxDBDataset;
    frxSemQuita: TfrxReport;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BitBtn1: TBitBtn;
    Panel5: TPanel;
    BtOK: TBitBtn;
    RGQuais: TRadioGroup;
    BtImprimir: TBitBtn;
    PB1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtImprimirClick(Sender: TObject);
  private
    { Private declarations }
    F_sem_quita_: String;
  public
    { Public declarations }
  end;

  var
  FmLctAjustesB: TFmLctAjustesB;

implementation

{$R *.DFM}

uses UnMyObjects, Module, ModuleGeral, UCreate, UMySQLModule;


procedure TFmLctAjustesB.BtImprimirClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxSemQuita, 'Lan�amentos sem quita��o localizada');
end;

procedure TFmLctAjustesB.BtOKClick(Sender: TObject);
var
  Controle, Carteira, N: Integer;
  Data, Descricao, Total: String;
  Credito, Debito: Double;
begin
  N := 0;
  if MyObjects.FIC(RGQuais.ItemIndex = -1, RGQuais,
    'Informe "quais" ser�o pesquisados!') then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    F_sem_quita_ := UCriar.RecriaTempTableNovo(ntrttSemQuita, DmodG.QrUpdPID1, False);
    QrEmiss.Close;
    QrEmiss.SQL.Clear;
    QrEmiss.SQL.Add('SELECT Data, Controle, CtrlQuitPg,');
    QrEmiss.SQL.Add('Descricao, Compensado, Carteira,');
    QrEmiss.SQL.Add('Credito, Debito');
    QrEmiss.SQL.Add('FROM ' + FTabLctA);
    QrEmiss.SQL.Add('WHERE Tipo=2');
    QrEmiss.SQL.Add('AND ((Compensado>2) or (Sit>1))');
    case RGQuais.ItemIndex of
      1: QrEmiss.SQL.Add('AND CtrlQuitPg > 0');
      2: QrEmiss.SQL.Add('AND CtrlQuitPg = 0');
    end;
    QrEmiss.Open;
    PB1.Position := 0;
    PB1.Max := QrEmiss.RecordCount;
    Total := IntToStr(QrEmiss.RecordCount);
    MyObjects.Informa(LaResu, False, 'Registros n�o localizados: 0');
    QrEmiss.First;
    while not QrEmiss.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      MyObjects.Informa(LaAviso, True, 'Pesquisando registo ' +
      IntToStr(QrEmiss.RecNo) + ' de ' + Total);
      //
      if QrEmissCtrlQuitPg.Value > 0 then
      begin
        QrQuit.Close;
        QrQuit.Params[0].AsInteger := QrEmissCtrlQuitPg.Value;
        QrQuit.Open;
        //
        if QrQuit.RecordCount = 0 then
        begin
          Carteira  :=  QrEmissCarteira.Value;
          Controle  :=  QrEmissControle.Value;
          Data      :=  Geral.FDT(QrEmissData.Value, 1);
          Descricao :=  QrEmissDescricao.Value;
          Credito   :=  QrEmissCredito.Value;
          Debito    :=  QrEmissDebito.Value;
          //
          UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, '_sem_quita_', False, [
          'Carteira', 'Data', 'Descricao', 'Credito', 'Debito'], ['Controle'], [
          Carteira, Data, Descricao, Credito, Debito], [Controle], False);
          //
          N := N + 1;
          MyObjects.Informa(LaResu, False, 'Registros n�o localizados: ' +
          IntToStr(N));
        end;
      end else
      begin
        QrPgto.Close;
        QrPgto.Params[0].AsInteger := QrEmissControle.Value;
        QrPgto.Open;
        //
        if QrPgto.RecordCount = 0 then
        begin
          Carteira  :=  QrEmissCarteira.Value;
          Controle  :=  QrEmissControle.Value;
          Data      :=  Geral.FDT(QrEmissData.Value, 1);
          Descricao :=  QrEmissDescricao.Value;
          Credito   :=  QrEmissCredito.Value;
          Debito    :=  QrEmissDebito.Value;
          //
          UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, '_sem_quita_', False, [
          'Carteira', 'Data', 'Descricao', 'Credito', 'Debito'], ['Controle'], [
          Carteira, Data, Descricao, Credito, Debito], [Controle], False);
          //
          N := N + 1;
          MyObjects.Informa(LaResu, False, 'Registros n�o localizados: ' +
          IntToStr(N));
        end;
      end;
      //
      QrEmiss.Next;
    end;
    MyObjects.Informa(LaAviso, False,
      'Pesquisa finalizada! Total de lan�amentos pesquisados: ' + Total);
    QrSemQuita.Close;
    QrSemQuita.Open;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLctAjustesB.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLctAjustesB.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLctAjustesB.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrSemQuita.Close;
  QrSemQuita.DataBase := DModG.MyPID_DB;
  try
    QrSemQuita.Open;
  except
    ; // Nada
  end;
end;

procedure TFmLctAjustesB.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
