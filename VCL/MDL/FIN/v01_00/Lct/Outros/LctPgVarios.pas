unit LctPgVarios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral,  
    
     Db, mySQLDbTables,
  DBCtrls,    Grids, DBGrids, ComCtrls,
  UnFinanceiro, Variants, dmkEdit, dmkEditDateTimePicker, Mask, dmkGeral;

type
  THackDBGrid = class(TDBGrid);
  TFmLctPgVarios = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasTipo: TIntegerField;
    Label3: TLabel;
    Label2: TLabel;
    TPData: TdmkEditDateTimePicker;
    EdMulta: TdmkEdit;
    EdTaxaM: TdmkEdit;
    Label4: TLabel;
    DBGrid1: TDBGrid;
    TbLctoEdit: TmySQLTable;
    DsLctoEdit: TDataSource;
    Panel1: TPanel;
    BtCancela: TBitBtn;
    TbLctoEditControle: TIntegerField;
    TbLctoEditSub: TIntegerField;
    TbLctoEditDescricao: TWideStringField;
    TbLctoEditData: TDateField;
    TbLctoEditVencimento: TDateField;
    TbLctoEditMultaVal: TFloatField;
    TbLctoEditJurosVal: TFloatField;
    TbLctoEditValorOri: TFloatField;
    TbLctoEditValorPgt: TFloatField;
    QrLancto1: TmySQLQuery;
    QrLancto1Sub: TSmallintField;
    QrLancto1Descricao: TWideStringField;
    QrLancto1Credito: TFloatField;
    QrLancto1Debito: TFloatField;
    QrLancto1NotaFiscal: TIntegerField;
    QrLancto1Documento: TFloatField;
    QrLancto1Vencimento: TDateField;
    QrLancto1Carteira: TIntegerField;
    QrLancto1Data: TDateField;
    QrLancto1NOMECARTEIRA: TWideStringField;
    QrLancto1TIPOCARTEIRA: TIntegerField;
    QrLancto1CARTEIRABANCO: TIntegerField;
    QrLancto1Cliente: TIntegerField;
    QrLancto1Fornecedor: TIntegerField;
    QrLancto1DataDoc: TDateField;
    QrLancto1Nivel: TIntegerField;
    QrLancto1Vendedor: TIntegerField;
    QrLancto1Account: TIntegerField;
    QrLancto1Controle: TIntegerField;
    QrLancto1CtrlIni: TIntegerField;
    QrLancto1Genero: TIntegerField;
    QrLancto1Mez: TIntegerField;
    QrLancto1Duplicata: TWideStringField;
    QrLancto1Doc2: TWideStringField;
    QrLancto1SerieCH: TWideStringField;
    QrLancto1MoraDia: TFloatField;
    QrLancto1Multa: TFloatField;
    QrLancto1ICMS_P: TFloatField;
    QrLancto1ICMS_V: TFloatField;
    QrLancto1CliInt: TIntegerField;
    QrLancto1Depto: TIntegerField;
    QrLancto1DescoPor: TIntegerField;
    QrLancto1ForneceI: TIntegerField;
    QrLancto1Unidade: TIntegerField;
    QrLancto1Qtde: TFloatField;
    QrLancto1FatID: TIntegerField;
    QrLancto1FatID_Sub: TIntegerField;
    QrLancto1DescoVal: TFloatField;
    QrLancto1NFVal: TFloatField;
    QrLancto1FatParcela: TIntegerField;
    QrLancto2: TmySQLQuery;
    QrLancto2Tipo: TSmallintField;
    QrLancto2Credito: TFloatField;
    QrLancto2Debito: TFloatField;
    QrLancto2Sub: TSmallintField;
    QrLancto2Carteira: TIntegerField;
    QrLancto2Controle: TIntegerField;
    QrLancto1FatNum: TFloatField;
    EdTotalPago: TdmkEdit;
    Label1: TLabel;
    QrSum: TmySQLQuery;
    DsSum: TDataSource;
    QrSumValorPgt: TFloatField;
    SpeedButton1: TSpeedButton;
    QrSumValorOri: TFloatField;
    QrTot: TmySQLQuery;
    DsTot: TDataSource;
    QrTotValorOri: TFloatField;
    QrTotValorPgt: TFloatField;
    QrTotMultaVal: TFloatField;
    QrTotJurosVal: TFloatField;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    QrSumMultaVal: TFloatField;
    QrSumJurosVal: TFloatField;
    Label7: TLabel;
    DBEdit3: TDBEdit;
    Label8: TLabel;
    DBEdit4: TDBEdit;
    PB1: TProgressBar;
    procedure BtOKClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdMultaExit(Sender: TObject);
    procedure EdTaxaMExit(Sender: TObject);
    procedure TPDataExit(Sender: TObject);
    procedure TbLctoEditBeforePost(DataSet: TDataSet);
    procedure TbLctoEditAfterPost(DataSet: TDataSet);
    procedure QrSumAfterOpen(DataSet: TDataSet);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaMultaEJuros;
  public
    { Public declarations }
    FCalcular: Boolean;

  end;

  var
  FmLctPgVarios: TFmLctPgVarios;

implementation

{$R *.DFM}

uses UnMyObjects, Module, CondGer, UMySQLModule, Principal, UnInternalConsts, ModuleCond,
ModuleGeral;

procedure TFmLctPgVarios.BtCancelaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLctPgVarios.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  FCalcular := True;
  CalculaMultaEJuros;
end;

procedure TFmLctPgVarios.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLctPgVarios.QrSumAfterOpen(DataSet: TDataSet);
begin
  EdTotalPago.ValueVariant := QrSumValorPgt.Value;
end;

procedure TFmLctPgVarios.SpeedButton1Click(Sender: TObject);
var
  Dias, Multa, Juros, Fator, Total, Need_, Dif: Double;
begin
  Screen.Cursor := crHourGlass;
  Multa := EdMulta.ValueVariant / 100;
  Need_ := EdTotalPago.ValueVariant;
  TbLctoEdit.First;
  while not TbLctoEdit.Eof do
  begin
    TbLctoEdit.Edit;
    TbLctoEditMultaVal.Value := TbLctoEditValorOri.Value * Multa;
    TbLctoEdit.Post;
    //
    TbLctoEdit.Next;
  end;
  QrTot.Close;
  QrTot.Open;
  Juros := EdTotalPago.ValueVariant - QrTotValorOri.Value - QrTotMultaVal.Value;
  Fator := Juros / QrTotValorOri.Value;
  //
  Dias := Trunc(TPData.Date) - TbLctoEditVencimento.Value;
  //
  if Dias > 0 then
    EdTaxaM.ValueVariant := Fator * 100 / Dias * 30
  else
    EdTaxaM.ValueVariant := 0;
  //
  TbLctoEdit.First;
  while not TbLctoEdit.Eof do
  begin
    Juros := TbLctoEditValorOri.Value * Fator;
    Total := TbLctoEditMultaVal.Value + TbLctoEditValorOri.Value + Juros;
    TbLctoEdit.Edit;
    TbLctoEditJurosVal.Value := Juros;
    TbLctoEditValorPgt.Value := Total;
    TbLctoEdit.Post;
    //
    TbLctoEdit.Next;
  end;
  //
  QrSum.Close;
  QrSum.Open;
  //
  Dif := Need_ - QrSumValorPgt.Value;
  if Dif <> 0 then
  begin
    TbLctoEdit.Edit;
    TbLctoEditJurosVal.Value := TbLctoEditJurosVal.Value + Dif;
    TbLctoEditValorPgt.Value := TbLctoEditValorPgt.Value + Dif;
    TbLctoEdit.Post;
    //
    QrSum.Close;
    QrSum.Open;
    //
  end;
  EdMulta.Enabled := False;
  EdTaxaM.Enabled := False;
  Screen.Cursor := crDefault;
end;

procedure TFmLctPgVarios.FormCreate(Sender: TObject);
begin
  //
  TPData.Date := Date;
  FCalcular := False;
  QrCarteiras.Close;
  QrCarteiras.Params[0].AsInteger := DCond.QrCondCliente.Value;
  QrCarteiras.Open;
  //
  QrSum.Close;
  QrSum.DataBase := DModG.MyPID_DB;
  QrTot.Close;
  QrTot.DataBase := DModG.MyPID_DB;
  //
  TbLctoEdit.Close;
  TbLctoEdit.DataBase := DModG.MyPID_DB;
  TbLctoEdit.Open;
end;

procedure TFmLctPgVarios.EdMultaExit(Sender: TObject);
begin
  EdMulta.Text := Geral.TFT(EdMulta.Text, 6, siPositivo);
  CalculaMultaEJuros;
end;

procedure TFmLctPgVarios.EdTaxaMExit(Sender: TObject);
begin
  EdTaxaM.Text := Geral.TFT(EdTaxaM.Text, 6, siPositivo);
  CalculaMultaEJuros;
end;

procedure TFmLctPgVarios.TPDataExit(Sender: TObject);
begin
  CalculaMultaEJuros;
end;

procedure TFmLctPgVarios.CalculaMultaEJuros;
var
  Venct, Pagto: TDateTime;
  MultaV, MultaT, TaxaM, JurosV, JurosT: Double;
  Dias, Controle: Integer;
begin
  if FCalcular then
  begin
    MultaT := Geral.DMV(EdMulta.Text);
    TaxaM  := Geral.DMV(EdTaxaM.Text);
    Pagto  := Trunc(TPData.Date);
    Venct  := UMyMod.CalculaDataDeposito(TbLctoEditVencimento.Value);
    TbLctoEdit.DisableControls;
    Controle := TbLctoEditControle.Value;
    TbLctoEdit.First;
    while not TbLctoEdit.Eof do
    begin
      JurosV := 0;
      MultaV := 0;
      if Venct < Pagto then
        Dias := UMyMod.DiasUteis(Venct + 1, Pagto) else
        Dias := 0;
      if Dias > 0 then
      begin
        JurosT := MLAGeral.CalculaJuroComposto(TaxaM, Pagto - Venct);
        JurosV := Round(JurosT * TbLctoEditValorOri.Value) / 100;
        MultaV := Round(MultaT * TbLctoEditValorOri.Value) / 100;
      end;
      //
      TbLctoEdit.Edit;
      TbLctoEditData.Value     := Pagto;
      TbLctoEditJurosVal.Value := JurosV;
      TbLctoEditMultaVal.Value := MultaV;
      TbLctoEditValorPgt.Value := TbLctoEditValorOri.Value + JurosV + MultaV;
      TbLctoEdit.Post;
      //
      TbLctoEdit.Next;
    end;
    TbLctoEdit.Locate('Controle', Controle, []);
  end;
  TbLctoEdit.EnableControls;
end;

procedure TFmLctPgVarios.TbLctoEditBeforePost(DataSet: TDataSet);
  function GetNomeCampo: String;
  begin
    Result := DBGrid1.Columns[THackDBGrid(DBGrid1).Col-1].FieldName;
  end;
var
  MultaV, JurosV, TotalV, TotalI: Double;
  Campo: String;
begin
  MultaV := TbLctoEditMultaVal.Value;
  JurosV := TbLctoEditJurosVal.Value;
  TotalV := TbLctoEditValorPgt.Value;
  TotalI := TbLctoEditValorOri.Value;
  if TotalV <> MultaV + JurosV then
  begin
    Campo := GetNomeCampo;
    if Campo = 'ValorPgt' then
    begin
      if TotalV < TotalI then
      begin
        MultaV := 0;
        JurosV := 0;
      end else begin
        JurosV := TotalV - MultaV - TotalI;
        if JurosV < 0 then
        begin
          MultaV := MultaV + JurosV;
          JurosV := 0;
        end;
        if MultaV < 0 then
        begin
          TotalV := TotalV + MultaV;
          MultaV := 0;
        end;
      end;
      if TotalV < 0 then TotalV := 0;
      TbLctoEditJurosVal.Value := JurosV;
      TbLctoEditMultaVal.Value := MultaV;
      TbLctoEditValorPgt.Value := TotalV;
    end;
    if Campo = 'JurosVal' then
      TbLctoEditValorPgt.Value := TotalI + JurosV + MultaV;
    if Campo = 'MultaVal' then
      TbLctoEditValorPgt.Value := TotalI + JurosV + MultaV;
  end;
end;

procedure TFmLctPgVarios.TbLctoEditAfterPost(DataSet: TDataSet);
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('DELETE FROM lctoedit WHERE Controle < 1');
  DModG.QrUpdPID1.SQL.Add('');
  DModG.QrUpdPID1.ExecSQL;
  TbLctoEdit.Refresh;
end;

procedure TFmLctPgVarios.BtOKClick(Sender: TObject);
  procedure QuitaRegistroAtual;
  var
    Texto: PChar;
    Doc, Controle, Controle2, Credito, Debito: Double;
    Sub: Integer;
  begin
    QrLancto1.Close;
    QrLancto1.Params[00].AsInteger := TbLctoEditControle.Value;
    QrLancto1.Params[01].AsInteger := TbLctoEditSub.Value;
    QrLancto1.Open;
    //
    QrLancto2.Close;
    QrLancto2.Params[0].AsFloat   := TbLctoEditControle.Value;
    QrLancto2.Params[1].AsInteger := QrLancto1Sub.Value;
    QrLancto2.Open;
    //
    if QrLancto1Debito.Value > 0 then
    begin
      Debito := TbLctoEditValorPgt.Value;
      Credito := 0;
    end else begin
      Debito := 0;
      Credito := TbLctoEditValorPgt.Value;
    end;
    FLAN_Nivel := QrLancto1Nivel.Value + 1;
    if QrLancto1CtrlIni.Value > 0 then
      FLAN_CtrlIni := Trunc(QrLancto1CtrlIni.Value)
    else
      FLAN_CtrlIni := Trunc(QrLancto1Controle.Value);
    //
    Controle := QrLancto1Controle.Value;
    Sub := QrLancto1Sub.Value;
    Doc := QrLancto1Documento.Value;
    //
    if QrLancto2.RecordCount <> 0 then
    begin
      if ((QrLancto2Credito.Value + QrLancto2Debito.Value) =
        (QrLancto1Credito.Value + QrLancto1Debito.Value))
      and (QrLancto2.RecordCount = 1) and (QrLancto2Tipo.Value = 1) then
      begin
        Texto := PChar('J� existe uma quita��o, e ela possui o mesmo valor,'+
        ' deseja difini-la como quita��o desta emiss�o ?');
        if Application.MessageBox(Texto, PChar(VAR_APPNAME),
        MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+MB_APPLMODAL) = ID_YES then
        begin
          {
          Dmod.QrUpdM.SQL.Clear;
          Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Sit=3, Compensado=:P0,');
          Dmod.QrUpdM.SQL.Add('DataAlt=:P1, UserAlt=:P2');
          Dmod.QrUpdM.SQL.Add('WHERE Controle=:P3 AND Tipo=2');
          Dmod.QrUpdM.Params[0].AsString := FormatDateTime(VAR_FORMATDATE, TbLctoEditData.Value);
          Dmod.QrUpdM.Params[1].AsString := FormatDateTime(VAR_FORMATDATE, Date);
          Dmod.QrUpdM.Params[2].AsInteger := VAR_USUARIO;
          Dmod.QrUpdM.Params[3].AsFloat := Controle;
  //        Dmod.QrUpdM.Params[2].AsInteger := Sub; precisa??
          Dmod.QrUpdM.ExecSQL;
          }
          UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
          'Sit', 'Compensado'], ['Controle', 'Tipo'], [
          3, FormatDateTime(VAR_FORMATDATE, TbLctoEditData.Value)], [
          Controle, 2], True, '');
          //
          Dmod.RecalcSaldoCarteira(2, QrLancto1Carteira.Value, 1);

          {
          Dmod.QrUpdM.SQL.Clear;
          Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Data=:P0,');
          Dmod.QrUpdM.SQL.Add('DataAlt=:P1, UserAlt=:P2');
          Dmod.QrUpdM.SQL.Add('WHERE ID_Pgto=:P3 AND Tipo=1 AND ID_Pgto>0');
          Dmod.QrUpdM.Params[0].AsString := FormatDateTime(VAR_FORMATDATE, TbLctoEditData.Value);
          Dmod.QrUpdM.Params[1].AsString := FormatDateTime(VAR_FORMATDATE, Date);
          Dmod.QrUpdM.Params[2].AsInteger := VAR_USUARIO;
          Dmod.QrUpdM.Params[3].AsFloat := Controle;
          Dmod.QrUpdM.ExecSQL;
          }
          if Controle > 0 then
          begin
            UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
            'Data'], ['ID_Pgto', 'Tipo'], [
            FormatDateTime(VAR_FORMATDATE, TbLctoEditData.Value)],
            [Controle, 1], True, '');
          end;
          Dmod.RecalcSaldoCarteira(1, QrLancto1Carteira.Value, 1);
        end;
      end else ShowMessage('J� existe pagamentos/rolagem para esta emiss�o!');
    end else begin
      Controle2 := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        VAR_LCT, VAR_LCT, 'Controle');
      //
      {
      Dmod.QrUpdM.SQL.Clear;
      Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Sit=3, Compensado=:P0,');
      Dmod.QrUpdM.SQL.Add('Documento=:P1, Descricao=:P2, ID_Pgto=:P3, ');
      Dmod.QrUpdM.SQL.Add('DataAlt=:Pa, UserAlt=:Pb');
      Dmod.QrUpdM.SQL.Add('WHERE Controle=:Pc AND Sub=:Pd AND Tipo=2');
      Dmod.QrUpdM.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TbLctoEditData.Value);
      Dmod.QrUpdM.Params[01].AsFloat   := Doc;
      Dmod.QrUpdM.Params[02].AsString  := TbLctoEditDescricao.Value;
      Dmod.QrUpdM.Params[03].AsFloat   := Controle2;
      //
      Dmod.QrUpdM.Params[04].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
      Dmod.QrUpdM.Params[05].AsInteger := VAR_USUARIO;
      Dmod.QrUpdM.Params[06].AsFloat   := Controle;
      Dmod.QrUpdM.Params[07].AsInteger := Sub;
      Dmod.QrUpdM.ExecSQL;
      }
      UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, ['Sit', 'Compensado',
      'Documento', 'Descricao', 'ID_Pgto'], ['Controle', 'Sub', 'Tipo'], [
      3, FormatDateTime(VAR_FORMATDATE, TbLctoEditData.Value),
      Doc, TbLctoEditDescricao.Value, Controle2], [Controle, Sub, 2], True, '');
      //
      Dmod.RecalcSaldoCarteira(QrLancto1TIPOCARTEIRA.Value, QrLancto1Carteira.Value, 1);

      //
      UFinanceiro.LancamentoDefaultVARS;
      FLAN_Data       := Geral.FDT(TbLctoEditData.Value, 1);
      FLAN_Vencimento := Geral.FDT(QrLancto1Vencimento.Value, 1);
      FLAN_DataCad    := Geral.FDT(Date, 1);
      FLAN_Mez        := MLAGeral.ITS_Null(QrLancto1Mez.Value);
      FLAN_Descricao  := TbLctoEditDescricao.Value;
      FLAN_Compensado := Geral.FDT(TbLctoEditData.Value, 1);
      FLAN_Duplicata  := QrLancto1Duplicata.Value;
      FLAN_Doc2       := QrLancto1Doc2.Value;
      FLAN_SerieCH    := QrLancto1SerieCH.Value;

      FLAN_Documento  := Trunc(QrLancto1Documento.Value);
      FLAN_Tipo       := 1;
      FLAN_Carteira   := QrLancto1CARTEIRABANCO.Value;
      FLAN_Credito    := Credito;
      FLAN_Debito     := Debito;
      FLAN_Genero     := QrLancto1Genero.Value;
      FLAN_NotaFiscal := QrLancto1NotaFiscal.Value;
      FLAN_Sit        := 3;
      FLAN_Cartao     := 0;
      FLAN_Linha      := 0;
      FLAN_Fornecedor := QrLancto1Fornecedor.Value;
      FLAN_Cliente    := QrLancto1Cliente.Value;
      FLAN_MoraDia    := QrLancto1MoraDia.Value;
      FLAN_Multa      := QrLancto1Multa.Value;
      //FLAN_UserCad    := VAR_USUARIO;
      //FLAN_DataDoc    := Geral.FDT(Date, 1);
      FLAN_Vendedor   := QrLancto1Vendedor.Value;
      FLAN_Account    := QrLancto1Account.Value;
      FLAN_ICMS_P     := QrLancto1ICMS_P.Value;
      FLAN_ICMS_V     := QrLancto1ICMS_V.Value;
      FLAN_CliInt     := QrLancto1CliInt.Value;
      FLAN_Depto      := QrLancto1Depto.Value;
      FLAN_DescoPor   := QrLancto1DescoPor.Value;
      FLAN_ForneceI   := QrLancto1ForneceI.Value;
      FLAN_DescoVal   := QrLancto1DescoVal.Value;
      FLAN_NFVal      := QrLancto1NFVal.Value;
      FLAN_Unidade    := QrLancto1Unidade.Value;
      FLAN_Qtde       := QrLancto1Qtde.Value;
      FLAN_FatID      := QrLancto1FatID.Value;
      FLAN_FatID_Sub  := QrLancto1FatID_Sub.Value;
      FLAN_FatNum     := Trunc(QrLancto1FatNum.Value);
      FLAN_FatParcela := QrLancto1FatParcela.Value;
      FLAN_MultaVal   := TbLctoEditMultaVal.Value;
      FLAN_MoraVal    := TbLctoEditJurosVal.Value;
      FLAN_ID_Pgto    := Round(Controle);
      FLAN_Controle   := Round(Controle2);
      //
      UFinanceiro.InsereLancamento;
      //Dmod.RecalcSaldoCarteira(1, QrLancto1CARTEIRABANCO.Value, 0);
    end;
  end;
begin
  Screen.Cursor := crHourGlass;
  PB1.Position := 0;
  PB1.Max := TbLctoEdit.RecordCount;
  TbLctoEdit.First;
  while not TbLctoEdit.Eof do
  begin
    PB1.Position := PB1.Position + 1;
    PB1.Update;
    Application.ProcessMessages;
    if TbLctoEditControle.Value <> 0 then QuitaRegistroAtual;
    TbLctoEdit.Next;
  end;
  FmCondGer.AtualizaTodasCarteiras();
  Screen.Cursor := crDefault;
  Close;
end;

end.


