unit Lct1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, UnMLAGeral, UnGOTOy, ComCtrls, UnMyLinguas,
  Db, mySQLDbTables, ExtCtrls, Buttons, UnInternalConsts, UMySQLModule, Grids,
  DBGrids, Variants, dmkGeral, dmkEdit, dmkEditCB, dmkDBLookupComboBox,
  dmkEditDateTimePicker, dmkLabel, dmkCheckBox, MyDBCheck;

type
  TFmLct1 = class(TForm)
    DsContas: TDataSource;
    DsCarteiras: TDataSource;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    DsFornecedores: TDataSource;
    DsClientes: TDataSource;
    DsNF: TDataSource;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    QrContas: TmySQLQuery;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasFechamento: TIntegerField;
    QrFatura: TmySQLQuery;
    QrFaturaData: TDateField;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasNome3: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrContasDataCad: TDateField;
    QrContasDataAlt: TDateField;
    QrContasUserCad: TSmallintField;
    QrContasUserAlt: TSmallintField;
    QrContasNOMESUBGRUPO: TWideStringField;
    QrContasNOMEGRUPO: TWideStringField;
    QrContasNOMECONJUNTO: TWideStringField;
    QrContasNOMEEMPRESA: TWideStringField;
    QrNF: TmySQLQuery;
    QrNFCONTA: TWideStringField;
    QrNFNOMECLIENTE: TWideStringField;
    QrClientes: TmySQLQuery;
    QrFornecedores: TmySQLQuery;
    QrFornecedoresCodigo: TIntegerField;
    QrFornecedoresNOMEENTIDADE: TWideStringField;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    QrVendedores: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsVendedores: TDataSource;
    DsAccounts: TDataSource;
    QrAccounts: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    QrClientesAccount: TIntegerField;
    QrNFData: TDateField;
    QrNFTipo: TSmallintField;
    QrNFCarteira: TIntegerField;
    QrNFControle: TIntegerField;
    QrNFSub: TSmallintField;
    QrNFAutorizacao: TIntegerField;
    QrNFGenero: TIntegerField;
    QrNFDescricao: TWideStringField;
    QrNFNotaFiscal: TIntegerField;
    QrNFDebito: TFloatField;
    QrNFCredito: TFloatField;
    QrNFCompensado: TDateField;
    QrNFDocumento: TFloatField;
    QrNFSit: TIntegerField;
    QrNFVencimento: TDateField;
    QrNFLk: TIntegerField;
    QrNFFatID: TIntegerField;
    QrNFFatParcela: TIntegerField;
    QrNFID_Pgto: TIntegerField;
    QrNFID_Sub: TSmallintField;
    QrNFFatura: TWideStringField;
    QrNFBanco: TIntegerField;
    QrNFLocal: TIntegerField;
    QrNFCartao: TIntegerField;
    QrNFLinha: TIntegerField;
    QrNFOperCount: TIntegerField;
    QrNFLancto: TIntegerField;
    QrNFPago: TFloatField;
    QrNFMez: TIntegerField;
    QrNFFornecedor: TIntegerField;
    QrNFCliente: TIntegerField;
    QrNFMoraDia: TFloatField;
    QrNFMulta: TFloatField;
    QrNFProtesto: TDateField;
    QrNFDataCad: TDateField;
    QrNFDataAlt: TDateField;
    QrNFUserCad: TSmallintField;
    QrNFUserAlt: TSmallintField;
    QrNFDataDoc: TDateField;
    QrNFCtrlIni: TIntegerField;
    QrNFNivel: TIntegerField;
    QrNFVendedor: TIntegerField;
    QrNFAccount: TIntegerField;
    QrFunci: TmySQLQuery;
    DsFunci: TDataSource;
    QrFunciCodigo: TIntegerField;
    QrFunciNOMEENTIDADE: TWideStringField;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrCliIntCodigo: TIntegerField;
    QrCliIntAccount: TIntegerField;
    QrCliIntNOMEENTIDADE: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PainelDados: TPanel;
    QrLct: TmySQLQuery;
    QrLctData: TDateField;
    QrLctTipo: TSmallintField;
    QrLctCarteira: TIntegerField;
    QrLctAutorizacao: TIntegerField;
    QrLctGenero: TIntegerField;
    QrLctDescricao: TWideStringField;
    QrLctNotaFiscal: TIntegerField;
    QrLctDebito: TFloatField;
    QrLctCredito: TFloatField;
    QrLctCompensado: TDateField;
    QrLctDocumento: TFloatField;
    QrLctSit: TIntegerField;
    QrLctVencimento: TDateField;
    QrLctLk: TIntegerField;
    QrLctFatID: TIntegerField;
    QrLctFatParcela: TIntegerField;
    QrLctCONTA: TIntegerField;
    QrLctNOMECONTA: TWideStringField;
    QrLctNOMEEMPRESA: TWideStringField;
    QrLctNOMESUBGRUPO: TWideStringField;
    QrLctNOMEGRUPO: TWideStringField;
    QrLctNOMECONJUNTO: TWideStringField;
    QrLctNOMESIT: TWideStringField;
    QrLctAno: TFloatField;
    QrLctMENSAL: TWideStringField;
    QrLctMENSAL2: TWideStringField;
    QrLctBanco: TIntegerField;
    QrLctLocal: TIntegerField;
    QrLctFatura: TWideStringField;
    QrLctSub: TSmallintField;
    QrLctCartao: TIntegerField;
    QrLctLinha: TIntegerField;
    QrLctPago: TFloatField;
    QrLctSALDO: TFloatField;
    QrLctID_Sub: TSmallintField;
    QrLctMez: TIntegerField;
    QrLctFornecedor: TIntegerField;
    QrLctcliente: TIntegerField;
    QrLctMoraDia: TFloatField;
    QrLctNOMECLIENTE: TWideStringField;
    QrLctNOMEFORNECEDOR: TWideStringField;
    QrLctTIPOEM: TWideStringField;
    QrLctNOMERELACIONADO: TWideStringField;
    QrLctOperCount: TIntegerField;
    QrLctLancto: TIntegerField;
    QrLctMulta: TFloatField;
    QrLctATRASO: TFloatField;
    QrLctJUROS: TFloatField;
    QrLctDataDoc: TDateField;
    QrLctNivel: TIntegerField;
    QrLctVendedor: TIntegerField;
    QrLctAccount: TIntegerField;
    QrLctMes2: TLargeintField;
    QrLctProtesto: TDateField;
    QrLctDataCad: TDateField;
    QrLctDataAlt: TDateField;
    QrLctUserCad: TSmallintField;
    QrLctUserAlt: TSmallintField;
    QrLctControle: TIntegerField;
    QrLctID_Pgto: TIntegerField;
    QrLctCtrlIni: TIntegerField;
    QrLctFatID_Sub: TIntegerField;
    QrLctICMS_P: TFloatField;
    QrLctICMS_V: TFloatField;
    QrLctDuplicata: TWideStringField;
    QrLctCOMPENSADO_TXT: TWideStringField;
    QrLctCliInt: TIntegerField;
    QrLctNOMECARTEIRA: TWideStringField;
    QrLctSALDOCARTEIRA: TFloatField;
    DsLct: TDataSource;
    TabSheet2: TTabSheet;
    TabControl1: TTabControl;
    DBGrid1: TDBGrid;
    DsDeptos: TDataSource;
    QrDeptos: TmySQLQuery;
    QrDeptosCodigo: TIntegerField;
    QrDeptosNome: TWideStringField;
    QrCarteirasPrazo: TSmallintField;
    Panel1: TPanel;
    Label8: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    LaICMS_P: TLabel;
    LaICMS_V: TLabel;
    EdICMS_V: TdmkEdit;
    EdICMS_P: TdmkEdit;
    TPDataDoc: TdmkEditDateTimePicker;
    EdMulta: TdmkEdit;
    EdMoraDia: TdmkEdit;
    Label10: TLabel;
    EdFunci: TdmkEditCB;
    CBFunci: TdmkDBLookupComboBox;
    LaVendedor: TLabel;
    EdVendedor: TdmkEditCB;
    CBVendedor: TdmkDBLookupComboBox;
    EdAccount: TdmkEditCB;
    CBAccount: TdmkDBLookupComboBox;
    LaAccount: TLabel;
    PnMaskPesq: TPanel;
    Panel5: TPanel;
    Label5: TLabel;
    EdLinkMask: TEdit;
    LLBPesq: TDBLookupListBox;
    PnLink: TPanel;
    Panel6: TPanel;
    SpeedButton3: TSpeedButton;
    QrPesq: TmySQLQuery;
    QrPesqCodigo: TIntegerField;
    QrPesqNome: TWideStringField;
    DsPesq: TDataSource;
    QrNFFatNum: TFloatField;
    QrLctFatNum: TFloatField;
    CBDepto: TdmkDBLookupComboBox;
    EdDepto: TdmkEditCB;
    Label18: TLabel;
    Panel3: TPanel;
    BtDesiste: TBitBtn;
    LaFunci: TLabel;
    EdTipoAnt: TLabel;
    EdCartAnt: TLabel;
    Panel4: TPanel;
    CBCliInt: TdmkDBLookupComboBox;
    EdCliInt: TdmkEditCB;
    LaCliInt: TLabel;
    TPData: TdmkEditDateTimePicker;
    Label1: TLabel;
    EdCodigo: TdmkEdit;
    Label14: TLabel;
    Label15: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    TPVencimento: TdmkEditDateTimePicker;
    LaVencimento: TLabel;
    Label2: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    BtContas: TSpeedButton;
    LaMes: TLabel;
    EdMes: TdmkEdit;
    LaDeb: TLabel;
    EdDeb: TdmkEdit;
    LaCred: TLabel;
    EdCred: TdmkEdit;
    LaNF: TLabel;
    EdNF: TdmkEdit;
    LaDoc: TLabel;
    EdDoc: TdmkEdit;
    EdDuplicata: TdmkEdit;
    Label11: TLabel;
    Label3: TLabel;
    Label13: TLabel;
    EdDescricao: TdmkEdit;
    EdQtde: TdmkEdit;
    LaCliente: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdFornecedor: TdmkEditCB;
    LaFornecedor: TLabel;
    CBFornecedor: TdmkDBLookupComboBox;
    EdSerieCH: TdmkEdit;
    Label6: TLabel;
    LaMezUp: TLabel;
    LaMezDown: TLabel;
    LaNFUp: TLabel;
    LaNFDown: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    LaDocUp: TLabel;
    LaDocDown: TLabel;
    LaDuplicataUp: TLabel;
    LaDuplicataDown: TLabel;
    QrCarteirasTipo: TIntegerField;
    CkCancelado: TdmkCheckBox;
    LaEventosCad: TLabel;
    CBEventosCad: TdmkDBLookupComboBox;
    EdEventosCad: TdmkEditCB;
    QrEventosCad: TmySQLQuery;
    DsEventosCad: TDataSource;
    QrEventosCadCodigo: TIntegerField;
    QrEventosCadCodUsu: TIntegerField;
    QrEventosCadNome: TWideStringField;
    SbEventosCad: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton4: TSpeedButton;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdCarteiraChange(Sender: TObject);
    procedure EdMesExit(Sender: TObject);
    procedure BtContasClick(Sender: TObject);
    procedure EdDebExit(Sender: TObject);
    procedure EdCredExit(Sender: TObject);
    procedure EdNFExit(Sender: TObject);
    procedure EdDocExit(Sender: TObject);
    procedure EdDescricaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormResize(Sender: TObject);
    procedure EdMoraDiaExit(Sender: TObject);
    procedure EdMultaExit(Sender: TObject);
    procedure EdMoraDiaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdMultaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton1Click(Sender: TObject);
    procedure QrContasAfterScroll(DataSet: TDataSet);
    procedure EdAccountKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBAccountKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdFunciChange(Sender: TObject);
    procedure EdICMS_PExit(Sender: TObject);
    procedure EdICMS_VExit(Sender: TObject);
    procedure EdICMS_PKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdICMS_VKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdDuplicataKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TabSheet2Resize(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure TPDataChange(Sender: TObject);
    procedure EdQtdeExit(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure LLBPesqDblClick(Sender: TObject);
    procedure LLBPesqKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdLinkMaskChange(Sender: TObject);
    procedure EdLinkMaskKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdContaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBContaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure LaNFUpClick(Sender: TObject);
    procedure LaNFDownClick(Sender: TObject);
    procedure LaDocUpClick(Sender: TObject);
    procedure LaDocDownClick(Sender: TObject);
    procedure LaDuplicataUpClick(Sender: TObject);
    procedure LaDuplicataDownClick(Sender: TObject);
    procedure LaMezUpClick(Sender: TObject);
    procedure LaMezDownClick(Sender: TObject);
    procedure TPDataClick(Sender: TObject);
    procedure SbEventosCadClick(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
    procedure VerificaEdits;
    procedure CarteirasReopen(CliInt: Integer);
    procedure SetaAccount(var Key: Word; Shift: TShiftState);
    procedure ExigeFuncionario;
    procedure CalculaICMS;
    function ReopenLct: Boolean;
    function VerificaVencimento: Boolean;

    procedure IncrementaMez(Key: Word);
    procedure ConfiguraVencimento();
    procedure MostraPnMaskPesq(Link: String);
    procedure SelecionaItemDePesquisa;
    procedure ReconfiguraComponentes(Carteira: Integer);

    procedure EventosCadReopen(CliInt: Integer);
  public
    { Public declarations }
    FatID, FatID_Sub, FICMS: Integer;
  end;

const
  FLargMaior = 800;
  FLargMenor = 594;

var
  FmLct1: TFmLct1;

implementation

uses UnMyObjects, Module, Contas, Principal, EventosCad, Entidades, UnFinanceiro;

{$R *.DFM}

procedure TFmLct1.CarteirasReopen(CliInt: Integer);
begin
  QrCarteiras.Close;
  QrCarteiras.Params[0].AsInteger := CliInt;
  QrCarteiras.Open;
end;

procedure TFmLct1.VerificaEdits;
begin
  if (QrContasMensal.Value = 'V') or (Dmod.QrControleMensalSempre.Value=1) then
  begin
    EdMes.Enabled := True;
    LaMes.Enabled := True;
  end else begin
    EdMes.Enabled := False;
    LaMes.Enabled := False;
  end;
  //
  if (VAR_BAIXADO <> -2) and not VAR_FATURANDO then Exit;
  if QrContasCredito.Value = 'V' then
  begin
    EdCred.Enabled := True;
    LaCred.Enabled := True;
    EdCliente.Enabled := True;
    LaCliente.Enabled := True;
    CBCliente.Enabled := True;
  end else begin
    EdCred.Enabled := False;
    LaCred.Enabled := False;
    EdCliente.Enabled := False;
    LaCliente.Enabled := False;
    CBCliente.Enabled := False;
  end;

  if QrContasDebito.Value = 'V' then
  begin
    EdDeb.Enabled := True;
    LaDeb.Enabled := True;
    EdFornecedor.Enabled := True;
    LaFornecedor.Enabled := True;
    CBFornecedor.Enabled := True;
  end else begin
    EdDeb.Enabled := False;
    LaDeb.Enabled := False;
    EdFornecedor.Enabled := False;
    LaFornecedor.Enabled := False;
    CBFornecedor.Enabled := False;
  end;

end;

procedure TFmLct1.BtDesisteClick(Sender: TObject);
begin
  Close;
  VAR_VALOREMITIRIMP := 0;
end;

procedure TFmLct1.BtConfirmaClick(Sender: TObject);
var
  Cartao, Carteira, Genero, TipoAnt, CartAnt, NF, Sit, Linha, Depto, Cliente,
  Fornecedor, CliInt, Vendedor, Account, Funci, TipoCart, EventosCad: Integer;
  Controle: Int64;
  Credito, Debito, Doc, MoraDia, Multa, ICMS_V, Difer, ICMS_P: Double;
  DataDoc, Data, Mes, Vencimento, Compensado, Qtde: String;
begin
  Screen.Cursor := crHourGlass;
  //
  EventosCad := Geral.IMV(EdEventosCad.Text);
  if EventosCad <> 0 then
    EventosCad := QrEventosCadCodigo.Value;
  //  
  CliInt := Geral.IMV(EdCliInt.Text);
  if MyObjects.FIC(CliInt = 0, EdCliInt, 'Informe a empresa!') then Exit;
  if EdFornecedor.Enabled = False then Fornecedor := 0 else
    if CBFornecedor.KeyValue <> NULL then
      Fornecedor := CBFornecedor.KeyValue else Fornecedor := 0;
  if EdCliente.Enabled = False then Cliente := 0 else
    if CBCliente.KeyValue <> NULL then
      Cliente := CBCliente.KeyValue else Cliente := 0;
  if EdVendedor.Enabled = False then Vendedor := 0 else
    if CBVendedor.KeyValue <> NULL then
      Vendedor := CBVendedor.KeyValue else Vendedor := 0;
  if EdAccount.Enabled = False then Account := 0 else
    if CBAccount.KeyValue <> NULL then
      Account := CBAccount.KeyValue else Account := 0;
  if EdDepto.Enabled = False then Depto := 0 else
    if CBDepto.KeyValue <> NULL then
      Depto := CBDepto.KeyValue else Depto := 0;
  if (VAR_AVISOSCXAEDIT in ([1,3,5,7])) and (Vendedor < 1) and (CBVendedor.Visible) then
  begin
    if Application.MessageBox('Deseja continuar mesmo sem definir o vendedor?',
    'Aviso de escolha de vendedor', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      CBVendedor.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  if (VAR_AVISOSCXAEDIT in ([2,3,6,7])) and (Account < 1) and (CBAccount.Visible) then
  begin
    if Application.MessageBox('Deseja continuar mesmo sem definir o Representante?',
    'Aviso de escolha de Representante', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      CBAccount.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  if (VAR_AVISOSCXAEDIT in ([4,5,6,7])) and (CliInt < 1) and (CBCliInt.Visible) then
  begin
    if Application.MessageBox('Deseja continuar mesmo sem definir o Cliente Interno?',
    'Aviso de escolha do cliente interno', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      CBCliInt.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  VAR_CANCELA := False;
  if not VerificaVencimento then
  begin
   Screen.Cursor := crDefault;
   Exit;
  end;
  Cartao := 0;
  Compensado := CO_VAZIO;
  if VAR_FATURANDO then
  begin
    Cartao := FmPrincipal.CartaoDeFatura;
    Compensado := FmPrincipal.CompensacaoDeFatura;
  end;
  if (EdNF.Enabled = False) then NF := 0
  else NF := Geral.IMV(EdNF.Text);
  Credito := Geral.DMV(EdCred.Text);
  if VAR_CANCELA then
  begin
    Application.MessageBox('Inclus�o cancelada pelo usu�rio!',
    'Cancelaqmento de Inclus�o', MB_OK + MB_ICONEXCLAMATION);
    EdNF.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  Debito  := Geral.DMV(EdDeb.Text);
  MoraDia := Geral.DMV(EdMoraDia.Text);
  Multa   := Geral.DMV(EdMulta.Text);
  ICMS_V  := Geral.DMV(EdICMS_V.Text);
  Difer   := Credito-Debito;
  if ((Difer<0) and (ICMS_V>0)) or ((Difer>0) and (ICMS_V<0)) then
  begin
    Application.MessageBox('ICMS incorreto! Para d�bitos o ICMS deve ser negativo!',
    'Cancelaqmento de Inclus�o', MB_OK + MB_ICONEXCLAMATION);
    EdICMS_P.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (EdCred.Enabled = False) and (Credito <> 0) and (VAR_BAIXADO = -2) then
  begin
    EdCred.Text := '0';
    Credito := 0;
  end;
  if (EdDeb.Enabled = False) and (Debito <> 0) and (VAR_BAIXADO = -2) then
  begin
    EdDeb.Text := '0';
    Debito := 0;
  end;
  if CBCarteira.KeyValue <> NULL then
  begin
    Carteira := CBCarteira.KeyValue;
    TipoCart := QrCarteirasTipo.Value;
  end else begin
    Carteira := 0;
    TipoCart := 0;
  end;
  if Carteira = 0 then
  begin
    Application.MessageBox('ERRO. Defina um item de carteira!', 'Erro', MB_OK+MB_ICONERROR);
    EdCarteira.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (CBFunci.Visible=True) and (CBFunci.KeyValue=NULL)then
  begin
    Application.MessageBox('ERRO. Defina um funcion�rio!', 'Erro', MB_OK+MB_ICONERROR);
    EdFunci.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end else Funci := Geral.IMV(EdFunci.Text);
  //if Funci = 0 then Funci := VAR_USUARIO;
  Genero := Geral.IMV(EdConta.Text);
  if Genero = 0 then
  begin
    Application.MessageBox('ERRO. Defina uma conta!', 'Erro', MB_OK+MB_ICONERROR);
    EdConta.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (Credito > 0) and (QrContasCredito.Value = 'F') then
  begin
    Application.MessageBox('ERRO. Valor n�o pode ser cr�dito', 'Erro', MB_OK+MB_ICONERROR);
    EdCred.Text;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (Debito > 0) and (QrContasDebito.Value = 'F') then
  begin
    Application.MessageBox('ERRO. Valor n�o pode ser d�bito', 'Erro', MB_OK+MB_ICONERROR);
    EdDeb.Text;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (EdCred.Enabled = False) and (EdDeb.Enabled = False) and
  (VAR_BAIXADO = -2) then
  begin
    Application.MessageBox('ERRO. Opera��o imposs�vel. Conta sem permiss�o de d�bito ou cr�dito.', 'Erro', MB_OK+MB_ICONERROR);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (Credito = 0) and (Debito = 0) and (CkCancelado.Checked = False) then
  begin
    Application.MessageBox('ERRO. Defina um d�bito ou um cr�dito.', 'Erro', MB_OK+MB_ICONERROR);
    if EdDeb.Enabled = True then EdDeb.SetFocus
    else if EdCred.Enabled = True then EdCred.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if EdMes.Text = CO_VAZIO then Mes := CO_VAZIO else
  if (EdMes.Enabled = False) then Mes := CO_VAZIO else
  begin
    //Mes := EdMes.Text[6]+EdMes.Text[7]+EdMes.Text[1]+EdMes.Text[2];
    Mes := EdMes.Text[4]+EdMes.Text[5]+EdMes.Text[1]+EdMes.Text[2];
    if Mes = CO_VAZIO then
    begin
      Application.MessageBox('ERRO. Defina o m�s do vencimento.', 'Erro', MB_OK+MB_ICONERROR);
      EdMes.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  if (Mes = CO_VAZIO) and (EdMes.Enabled = True) then
  begin
    Application.MessageBox('ERRO. Defina o m�s do vencimento.', 'Erro', MB_OK+MB_ICONERROR);
    EdMes.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (EdDoc.Enabled = False) and (VAR_BAIXADO = -2) then Doc := 0
  else Doc := Geral.DMV(EdDoc.Text);
  if (TPVencimento.Enabled = False) then Vencimento :=
    FormatDateTime(VAR_FORMATDATE, TPData.Date)
  else Vencimento := FormatDateTime(VAR_FORMATDATE, TPVencimento.Date);
  if VAR_IMPORTANDO then Linha := VAR_IMPLINHA else Linha := 0;
  if VAR_FATURANDO then Sit := 3
  else if VAR_IMPORTANDO then Sit := -1
    else if VAR_BAIXADO <> -2 then Sit := VAR_BAIXADO
      else if QrCarteirasTipo.Value = 2 then Sit := 0
        else Sit := 3;
  TipoAnt := StrToInt(EdTipoAnt.Caption);
  CartAnt := StrToInt(EdCartAnt.Caption);
  if LaTipo.SQLType = stIns then
  begin
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', VAR_LCT, VAR_LCT, 'Controle');
    VAR_DATAINSERIR := TPdata.Date;

  end else begin
    Controle := Geral.IMV(EdCodigo.Text);
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('DELETE FROM ' + VAR_LCT + ' WHERE Controle=:P0');
    Dmod.QrUpdM.Params[0].AsFloat := Controle;
    Dmod.QrUpdM.ExecSQL;
    Dmod.RecalcSaldoCarteira(TipoAnt, CartAnt, 0);
    //dbiSaveChanges(Dmod.QrUpdU.Handle);
    VAR_DATAINSERIR := TPData.Date;
  end;
  if CkCancelado.Checked then
  begin
    Credito    := 0;
    Debito     := 0;
    Compensado := FormatDateTime(VAR_FORMATDATE, TPData.Date);
    Sit        := 4;
  end;
  VAR_LANCTO2 := Controle;

{
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERT INTO ' + VAR_LCT + ' SET Documento=:P0, Data=:P1, ');
  Dmod.QrUpdU.SQL.Add('Tipo=:P2, Carteira=:P3, Credito=:P4, Debito=:P5, ');
  Dmod.QrUpdU.SQL.Add('Genero=:P6, NotaFiscal=:P7, Vencimento=:P8, Mez=:P9, ');
  Dmod.QrUpdU.SQL.Add('Descricao=:P10, Sit=:P11, Controle=:P12, Cartao=:P13, ');
  Dmod.QrUpdU.SQL.Add('Compensado=:P14, Linha=:P15, Fornecedor=:P16, ');
  Dmod.QrUpdU.SQL.Add('Cliente=:P17, MoraDia=:P18, Multa=:P19,DataCad=:P20, ');
  Dmod.QrUpdU.SQL.Add('UserCad=:P21, DataDoc=:P22, Vendedor=:P23, ');
  Dmod.QrUpdU.SQL.Add('Account=:P24, FatID=:P25, FatID_Sub=:P26, ');
  Dmod.QrUpdU.SQL.Add('ICMS_P=:P27, ICMS_V=:P28, Duplicata=:P29, ');
  Dmod.QrUpdU.SQL.Add('EventosCad=:P30, ');

  if EdQtde.Text <> '' then
    Dmod.QrUpdU.SQL.Add('Qtde='+MLAGeral.TFT_Dot(EdQtde.Text, 3, siPositivo)+',');
  Dmod.QrUpdU.SQL.Add('CliInt=:Pa, Depto=:Pb, SerieCH=:Pc ');
  Dmod.QrUpdU.Params[00].AsFloat   := Doc;
  Dmod.QrUpdU.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPData.Date);
  Dmod.QrUpdU.Params[02].AsInteger := TipoCart;
  Dmod.QrUpdU.Params[03].AsInteger := Carteira;
  Dmod.QrUpdU.Params[04].AsFloat   := Credito;
  Dmod.QrUpdU.Params[05].AsFloat   := Debito;
  Dmod.QrUpdU.Params[06].AsInteger := Genero;
  Dmod.QrUpdU.Params[07].AsInteger := NF;
  Dmod.QrUpdU.Params[08].AsString  := Vencimento;
  Dmod.QrUpdU.Params[09].AsString  := Mes;
  Dmod.QrUpdU.Params[10].AsString  := EdDescricao.Text;
  Dmod.QrUpdU.Params[11].AsInteger := Sit;
  Dmod.QrUpdU.Params[12].AsFloat   := Controle;
  Dmod.QrUpdU.Params[13].AsInteger := Cartao;
  Dmod.QrUpdU.Params[14].AsString  := Compensado;
  Dmod.QrUpdU.Params[15].AsInteger := Linha;
  Dmod.QrUpdU.Params[16].AsInteger := Fornecedor;
  Dmod.QrUpdU.Params[17].AsInteger := Cliente;
  Dmod.QrUpdU.Params[18].AsFloat   := MoraDia;
  Dmod.QrUpdU.Params[19].AsFloat   := Multa;
  Dmod.QrUpdU.Params[20].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[21].AsInteger := Funci;
  Dmod.QrUpdU.Params[22].AsString  := FormatDateTime(VAR_FORMATDATE, TPDataDoc.Date);
  Dmod.QrUpdU.Params[23].AsInteger := Vendedor;
  Dmod.QrUpdU.Params[24].AsInteger := Account;
  Dmod.QrUpdU.Params[25].AsInteger := FatID;
  Dmod.QrUpdU.Params[26].AsInteger := FatID_Sub;
  Dmod.QrUpdU.Params[27].AsFloat   := Geral.DMV(EdICMS_P.Text);
  Dmod.QrUpdU.Params[28].AsFloat   := Geral.DMV(EdICMS_V.Text);
  Dmod.QrUpdU.Params[29].AsString  := EdDuplicata.Text;
  Dmod.QrUpdU.Params[30].AsInteger := EventosCad;
  //
  Dmod.QrUpdU.Params[31].AsInteger := CliInt;
  Dmod.QrUpdU.Params[32].AsInteger := Depto;
  Dmod.QrUpdU.Params[33].AsString  := EdSerieCH.Text;
  Dmod.QrUpdU.ExecSQL;
}





  if EdQtde.Text <> '' then
    Qtde := MLAGeral.TFT_Dot(EdQtde.Text, 3, siPositivo)
  else  
    Qtde := '';
  Data    := FormatDateTime(VAR_FORMATDATE, TPData.Date);
  DataDoc := FormatDateTime(VAR_FORMATDATE, TPDataDoc.Date);
  ICMS_P  := Geral.DMV(EdICMS_P.Text);
  ICMS_V  := Geral.DMV(EdICMS_V.Text);
  //
  if UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpdU, stIns(*LaTipo.SQLType*), False, [
  'Documento', 'Data', 'Tipo', 'Carteira', 'Credito', 'Debito',
  'Genero', 'NotaFiscal', 'Vencimento', 'Mez', 'Descricao', 'Sit',
  'Cartao', 'Compensado', 'Linha', 'Fornecedor',
  'Cliente', 'MoraDia', 'Multa', 'DataDoc', 'Vendedor',
  'Account', 'FatID', 'FatID_Sub', 'ICMS_P', 'ICMS_V',
  'Duplicata', 'EventosCad', 'CliInt', 'Depto', 'SerieCH',
  'Qtde'
  ], ['Controle'], [Doc, Data, TipoCart, Carteira, Credito, Debito,
  Genero, NF, Vencimento, Mes, EdDescricao.Text, Sit,
  Cartao, Compensado, Linha, Fornecedor,
  Cliente, MoraDia, Multa, DataDoc, Vendedor,
  Account, FatID, FatID_Sub, ICMS_P, ICMS_V,
  EdDuplicata.Text, EventosCad, CliInt, Depto, EdSerieCH.Text,
  Qtde
  ], [Controle], True, '') then
  begin
    //dbiSaveChanges(Dmod.QrUpdU.Handle);
    (*if RGCarteira.ItemIndex <> TipoAnt then
    begin
      FmPrincipal.MostraCarteiras(RGCarteira.ItemIndex);
    end;*)
    if LaTipo.SQLType = stIns then
      Dmod.RecalcSaldoCarteira(TipoCart, CBCarteira.KeyValue, 1)
    else// begin if CBNovo.Checked = False then
      begin
        Dmod.RecalcSaldoCarteira(TipoCart, CBCarteira.KeyValue, 0);
        Dmod.RecalcSaldoCarteira(TipoAnt, CartAnt, 1);
    end;
    //
    VAR_VALOREMITIRIMP := VAR_VALOREMITIRIMP - Credito - Debito;
    FmPrincipal.ReabreCarteirasELct(FmPrincipal.QrcarteirasTipo.Value,
    FmPrincipal.QrCarteirasCodigo.Value, Controle, 0);
    if Application.MessageBox('Inclus�o concluida. Deseja continuar incluindo?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      LaTipo.SQLType := stIns;
      if TPData.Enabled then TPData.SetFocus else EdConta.SetFocus;
    end else Close;
  end;  
  Screen.Cursor := crDefault;
end;

procedure TFmLct1.FormCreate(Sender: TObject);
begin
  //Width := FLargMenor;
  if VAR_DATAINSERIR < Date - 366 then VAR_DATAINSERIR := Date;
  QrContas.Open;
  QrDeptos.Open;
  if VAR_CXEDIT_ICM then
  begin
    LaICMS_P.Visible := True;
    LaICMS_V.Visible := True;
    EdICMS_P.Visible := True;
    EdICMS_V.Visible := True;
  end;
  //if Trim(VAR_CLIENTEC) <> '' then
  //begin
    QrClientes.SQL.Clear;
    QrClientes.SQL.Add('SELECT Codigo, Account, ');
    QrClientes.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrClientes.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrClientes.SQL.Add('FROM entidades');
    QrClientes.SQL.Add('WHERE Cliente1="V"');
    QrClientes.SQL.Add('OR    Cliente2="V"');
    QrClientes.SQL.Add('ORDER BY NOMEENTIDADE');
    QrClientes.Open;
    //////
    LaCliente.Visible := True;
    EdCliente.Visible := True;
    CBCliente.Visible := True;
  //end;
  QrCliInt.Close;
  QrCliInt.Open;
  {  M�todo novo
  if Trim(VAR_CLIENTEI) <> '' then
  begin
    QrCliInt.SQL.Add('SELECT Codigo, Account, ');
    QrCliInt.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrCliInt.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrCliInt.SQL.Add('FROM entidades');
    QrCliInt.SQL.Add('WHERE Cliente'+VAR_CLIENTEI+'="V"');
    QrCliInt.SQL.Add('ORDER BY NOMEENTIDADE');
    QrCliInt.Open;
    //////
    LaCliInt.Visible := True;
    EdCliInt.Visible := True;
    CBCliInt.Visible := True;
  end;
  }
  QrFornecedores.Open;
  if Trim(VAR_FORNECEF) <> '' then
  begin
    QrFornecedores.SQL.Clear;
    QrFornecedores.SQL.Add('SELECT Codigo, ');
    QrFornecedores.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrFornecedores.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrFornecedores.SQL.Add('FROM entidades');
    QrFornecedores.SQL.Add('WHERE Fornece1="V"');
    QrFornecedores.SQL.Add('OR    Fornece2="V"');
    QrFornecedores.SQL.Add('OR    Fornece3="V"');
    QrFornecedores.SQL.Add('OR    Fornece4="V"');
    QrFornecedores.SQL.Add('ORDER BY NOMEENTIDADE');
    //////
    LaFornecedor.Visible := True;
    EdFornecedor.Visible := True;
    CBFornecedor.Visible := True;
  end;
  if Trim(VAR_FORNECEV) <> '' then
  begin
    QrVendedores.SQL.Clear;
    QrVendedores.SQL.Add('SELECT Codigo, ');
    QrVendedores.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrVendedores.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrVendedores.SQL.Add('FROM entidades');
    QrVendedores.SQL.Add('WHERE Fornece'+VAR_FORNECEV+'="V"');
    QrVendedores.SQL.Add('ORDER BY NOMEENTIDADE');
    QrVendedores.Open;
    //////
    QrAccounts.SQL.Clear;
    QrAccounts.SQL.Add('SELECT Codigo, ');
    QrAccounts.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrAccounts.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrAccounts.SQL.Add('FROM entidades');
    QrAccounts.SQL.Add('WHERE Fornece'+VAR_FORNECEV+'="V"');
    QrAccounts.SQL.Add('ORDER BY NOMEENTIDADE');
    QrAccounts.Open;
    //////
    LaVendedor.Visible := True;
    EdVendedor.Visible := True;
    CBVendedor.Visible := True;
    LaAccount.Visible := True;
    EdAccount.Visible := True;
    CBAccount.Visible := True;
  end;
  if VAR_LANCTOCONDICIONAL1 then
  begin
    VAR_DATAINSERIR := Date;
    TPDataDoc.Date := Date;
    //
    EdCarteira.Enabled := False;
    CBCarteira.Enabled := False;
    TPDAta.Enabled := False;
  end;
  if VAR_TIPOVARCLI <> '' then ExigeFuncionario;
  PageControl1.ActivePageIndex := 0;
  TabControl1.TabIndex := 0;
end;

procedure TFmLct1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if (QrCliInt.RecordCount = 1) and (CBCliInt.KeyValue = Null) then
  begin
    EdCliInt.ValueVariant := QrCliIntCodigo.Value;
    CBCliInt.KeyValue     := QrCliIntCodigo.Value;
  end;
  if (CBCliInt.KeyValue = Null) and EdCliInt.Visible and EdCliInt.Enabled then
    EdCliInt.SetFocus
  else TPData.SetFocus;
end;

procedure TFmLct1.ReconfiguraComponentes(Carteira: Integer);
begin
  case Carteira of
    0:
    begin
      if QrCarteirasPrazo.Value = 0 then
      begin
        EdDoc.Enabled := True;
        LaDoc.Enabled := True;
        EdNF.Enabled := True;
        LaNF.Enabled := True;
      end else begin
        EdDoc.Enabled := False;
        LaDoc.Enabled := False;
        EdNF.Enabled := True;
        LaNF.Enabled := True;
      end;
    end;
    1:
    begin
      EdDoc.Enabled := True;
      LaDoc.Enabled := True;
    end;
    2:
    begin
      EdDoc.Enabled := True;
      LaDoc.Enabled := True;
      EdNF.Enabled := True;
      LaNF.Enabled := True;
    end;
  end;
  ConfiguraVencimento;
end;

procedure TFmLct1.EdCarteiraChange(Sender: TObject);
var
  Carteira: Integer;
begin
  Carteira := Geral.IMV(EdCarteira.Text);
  ReconfiguraComponentes(Carteira);
end;

procedure TFmLct1.EdMesExit(Sender: TObject);
begin
  //EdMes.Text := MLAGeral.TST(EdMes.Text, True);
  EdDescricao.Text := QrContasNome2.Value+' '+EdMes.Text;
  if EdMes.Text = CO_VAZIO then EdMes.SetFocus;
end;

procedure TFmLct1.BtContasClick(Sender: TObject);
var
  Cursor : TCursor;
begin
  //if UMyMod.AcessoNegadoAoForm(ivTabPerfis, 'Contas', 0) then Exit;
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Application.CreateForm(TFmContas, FmContas);
  finally
    FmContas.LocCod(QrContasCodigo.Value, QrContasCodigo.Value);
    Screen.Cursor := Cursor;
  end;
  FmContas.ShowModal;
  FmContas.Destroy;
  QrContas.Close;
  QrContas.Open;
  EdConta.Text := IntToStr(VAR_CONTA);
  CBConta.KeyValue := VAR_CONTA;
  //Dmod.DefParams;
end;

procedure TFmLct1.EdDebExit(Sender: TObject);
begin
   EdDeb.Text := Geral.TFT(EdDeb.Text, 2, siPositivo);
   CalculaICMS;
end;

procedure TFmLct1.EdCredExit(Sender: TObject);
begin
   EdCred.Text := Geral.TFT(EdCred.Text, 2, siPositivo);
   CalculaICMS;
end;

procedure TFmLct1.EdNFExit(Sender: TObject);
begin
   EdNF.Text := Geral.TFT(EdNF.Text, 0, siPositivo);
end;

procedure TFmLct1.EdDocExit(Sender: TObject);
begin
   EdDoc.Text := MLAGeral.TBT(EdDoc.Text, siPositivo);
end;

procedure TFmLct1.IncrementaMez(Key: Word);
var
  Periodo: Integer;
begin
  if key in ([VK_DOWN, VK_UP]) then
  begin
    if EdMes.Text = '' then
      Periodo := Geral.Periodo2000(Date)
    else begin
      Periodo := MLAGeral.MensalToPeriodo(EdMes.Text);
      if key=VK_DOWN then Periodo := Periodo -1;
      if key=VK_UP   then Periodo := Periodo +1;
    end;
    EdMes.Text := dmkPF.PeriodoToMensal(Periodo);
  end;
end;

function TFmLct1.VerificaVencimento: Boolean;
var
  Carteira: Integer;
  Data: TDateTime;
begin
  Result := True;
  if LaTipo.SQLType = stIns then
  begin
    if QrCarteirasFatura.Value = 'V' then
    begin
      if CBCarteira.KeyValue <> NULL then Carteira := CBCarteira.KeyValue
      else Carteira := 0;
      QrFatura.Close;
      QrFatura.Params[0].AsInteger := Carteira;
      QrFatura.Open;
      if (QrFatura.RecordCount > 0) and (CBCarteira.KeyValue <> NULL) then
      begin
        Data := QrFaturaData.Value;
        while (Data - QrCarteirasFechamento.Value) < TPData.Date do
          Data := IncMonth(Data, 1);
        if int(TPVencimento.Date) <> Int(Data) then
        begin
          case Application.MessageBox(PChar(
          'A configura��o do sistema sugere a data de vencimento '+
          FormatDateTime(VAR_FORMATDATE3, Data)+' ao inv�s de '+
          FormatDateTime(VAR_FORMATDATE3, TPVencimento.Date)+
          '. Confirma a altera��o?'), PChar(VAR_APPNAME),
          MB_ICONQUESTION+MB_YESNOCANCEL+MB_DEFBUTTON1+MB_APPLMODAL) of
            ID_YES    : TPVencimento.Date := Data;
            ID_CANCEL : Result := False;
            ID_NO     : ;
          end;
        end;
      end;
      QrFatura.Close;
    end;
  end;
end;

procedure TFmLct1.EdDescricaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Mes: String;
begin
  if key in ([VK_F4, VK_F5, VK_F6]) then
  begin
    if EdMes.Enabled then Mes := EdMes.Text else Mes := '';
    if key=VK_F4 then EdDescricao.Text := QrContasNome.Value+' '+Mes;
    if key=VK_F5 then EdDescricao.Text := QrContasNome2.Value+' '+Mes;
    if key=VK_F6 then EdDescricao.Text := QrContasNome3.Value+' '+Mes;
    EdDescricao.SetFocus;
    EdDescricao.SelStart := Length(EdDescricao.Text);
    EdDescricao.SelLength := 0;
  end;
end;

procedure TFmLct1.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLct1.EdMoraDiaExit(Sender: TObject);
begin
  EdMoraDia.Text := Geral.TFT(EdMoraDia.Text, 2, siPositivo);
end;

procedure TFmLct1.EdMultaExit(Sender: TObject);
begin
  EdMulta.Text := Geral.TFT(EdMulta.Text, 2, siPositivo);
end;

procedure TFmLct1.EdMoraDiaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
  begin
    if QrContasCredito.Value = 'V' then
      FmPrincipal.CriaCalcPercent(EdCred.Text, '1,0000', cpJurosMes)
    else
      FmPrincipal.CriaCalcPercent(EdDeb.Text, '1,0000', cpJurosMes);
    EdMoraDia.Text := Geral.TFT(FloatToStr(VAR_VALOR), 2, siPositivo);
  end;
end;

procedure TFmLct1.EdMultaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
  begin
    if QrContasCredito.Value = 'V' then
      FmPrincipal.CriaCalcPercent(EdCred.Text, '2,0000', cpMulta)
    else
      FmPrincipal.CriaCalcPercent(EdDeb.Text, '2,0000', cpMulta);
    EdMulta.Text := Geral.TFT(FloatToStr(VAR_VALOR), 2, siPositivo);
  end;
end;

procedure TFmLct1.SpeedButton1Click(Sender: TObject);
begin
  //if UMyMod.AcessoNegadoAoForm(ivTabPerfis, 'Carteiras', 0) then Exit;
  VAR_CARTNUM := Geral.IMV(EdCarteira.Text);
  FmPrincipal.CadastroDeCarteiras(QrCarteirasTipo.Value,
    QrCarteirasCodigo.Value);
  QrCarteiras.Close;
  QrCarteiras.Open;
  EdCarteira.Text := IntToStr(VAR_CARTNUM);
  CBCarteira.KeyValue := VAR_CARTNUM;
  Dmod.DefParams;
end;

procedure TFmLct1.SpeedButton2Click(Sender: TObject);
begin
  VAR_ENTIDADE := 0;
  if DBCheck.CriaFm(TFmEntidades, FmEntidades, afmoNegarComAviso) then
  begin
    FmEntidades.ShowModal;
    FmEntidades.Destroy;
    //
    if VAR_ENTIDADE <> 0 then
    begin
      EdFornecedor.ValueVariant := VAR_ENTIDADE;
      CBFornecedor.KeyValue     := VAR_ENTIDADE;
    end;
  end;
end;

procedure TFmLct1.QrContasAfterScroll(DataSet: TDataSet);
begin
  VerificaEdits;
end;

procedure TFmLct1.SetaAccount(var Key: Word; Shift: TShiftState);
begin
  if key=VK_F4 then
  begin
    EdAccount.Text := IntToStr(QrClientesAccount.Value);
    CBAccount.KeyValue := QrClientesAccount.Value;
  end;
end;

procedure TFmLct1.EdAccountKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  SetaAccount(Key, Shift);
end;

procedure TFmLct1.CBAccountKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  SetaAccount(Key, Shift);
end;

procedure TFmLct1.EdFunciChange(Sender: TObject);
var
  CliInt: Integer;
begin
  CliInt := Geral.IMV(EdCliInt.Text);
  CarteirasReopen(CliInt);
  EventosCadReopen(CliInt);
end;

procedure TFmLct1.ExigeFuncionario;
begin
  QrFunci.Close;
  QrFunci.SQL.Clear;
  QrFunci.SQL.Add('SELECT Codigo,');
  QrFunci.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
  QrFunci.SQL.Add('ELSE Nome END NOMEENTIDADE');
  QrFunci.SQL.Add('FROM entidades');
  QrFunci.SQL.Add('WHERE '+VAR_TIPOVARCLI+'="V"');
  QrFunci.SQL.Add('ORDER BY NomeENTIDADE');
  QrFunci.Open;
  Label10.Visible := True;
  EdFunci.Visible := True;
  CBFunci.Visible := True;
end;

procedure TFmLct1.EdICMS_PExit(Sender: TObject);
begin
  CalculaICMS;
  EdICMS_P.Text := Geral.TFT(EdICMS_P.Text, 2, siPositivo);
end;

procedure TFmLct1.EdICMS_VExit(Sender: TObject);
begin
  CalculaICMS;
  EdICMS_V.Text := Geral.TFT(EdICMS_V.Text, 2, siPositivo);
end;

procedure TFmLct1.EdICMS_PKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if MLAGeral.EhNumeroOuSinalOuSeparador(Key) then FICMS := 0;
end;

procedure TFmLct1.EdICMS_VKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if MLAGeral.EhNumeroOuSinalOuSeparador(Key) then FICMS := 1;
end;

procedure TFmLct1.CalculaICMS;
var
  C, D, B, V, P: Double;
begin
  V := Geral.DMV(EdICMS_V.Text);
  P := Geral.DMV(EdICMS_P.Text);
  C := Geral.DMV(EdCred.Text);
  D := Geral.DMV(EdDeb.Text);
  //if C > D then B := C - D else B := D - C;
  B := C-D;
  if FICMS = 0 then V := B * P / 100
  else if B <> 0 then P := V / B * 100 else P := 0;
  EdICMS_V.Text := Geral.FFT(V, 2, siPositivo);
  EdICMS_P.Text := Geral.FFT(P, 2, siPositivo);
end;

procedure TFmLct1.EdNFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  NF: Integer;
begin
  NF := Geral.IMV(EdNF.Text);
  if key=VK_DOWN then if NF > 0 then EdNF.Text := IntToStr(NF-1);
  if key=VK_UP                  then EdNF.Text := IntToStr(NF+1);
end;

procedure TFmLct1.EdDuplicataKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key in ([VK_DOWN, VK_UP]) then
  begin
    if (key=VK_DOWN) then
      EdDuplicata.Text := MLAGeral.DuplicataIncrementa(EdDuplicata.Text, -1)
    else
      EdDuplicata.Text := MLAGeral.DuplicataIncrementa(EdDuplicata.Text,  1);
  end;
end;

function TFmLct1.ReopenLct: Boolean;
var
  Ini, Fim: String;
begin
  Ini := FormatDateTime(VAR_FORMATDATE, 1);
  Fim := FormatDateTime(VAR_FORMATDATE, Date);
  QrLct.Close;
  QrLct.SQL.Clear;
  QrLct.SQL.Add('SELECT MOD(la.Mez, 100) Mes2,');
  QrLct.SQL.Add('((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,');
  QrLct.SQL.Add('la.*, ct.Codigo CONTA,');
  QrLct.SQL.Add('ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,');
  QrLct.SQL.Add('gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,');
  QrLct.SQL.Add('CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial');
  QrLct.SQL.Add('ELSE em.Nome END NOMEEMPRESA,');
  QrLct.SQL.Add('CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial');
  QrLct.SQL.Add('ELSE cl.Nome END NOMECLIENTE,');
  QrLct.SQL.Add('CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial');
  QrLct.SQL.Add('ELSE fo.Nome END NOMEFORNECEDOR,');
  QrLct.SQL.Add('ca.Nome NOMECARTEIRA, ca.Saldo SALDOCARTEIRA');
  QrLct.SQL.Add('FROM ' + VAR_LCT + ' la');
  QrLct.SQL.Add('LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = 0');
  QrLct.SQL.Add('LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo');
  QrLct.SQL.Add('LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo');
  QrLct.SQL.Add('LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto');
  QrLct.SQL.Add('LEFT JOIN entidades em ON em.Codigo=ct.Empresa');
  QrLct.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
  QrLct.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor');
  QrLct.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
  //
  QrLct.SQL.Add('WHERE la.Data BETWEEN "'+Ini+'" AND "'+Fim+'"');
  //
  case TabControl1.TabIndex of
    0: QrLct.SQL.Add('AND la.Carteira='  +IntToStr(Geral.IMV(EdCarteira.Text)));
    1: QrLct.SQL.Add('AND la.CliInt='    +IntToStr(Geral.IMV(EdCliInt.Text)));
    2: QrLct.SQL.Add('AND la.Cliente='   +IntToStr(Geral.IMV(EdCliente.Text)));
    3: QrLct.SQL.Add('AND la.Fornecedor='+IntToStr(Geral.IMV(EdFornecedor.Text)));
    4: QrLct.SQL.Add('AND la.Genero='    +IntToStr(Geral.IMV(EdConta.Text)));
  end;
  QrLct.SQL.Add('ORDER BY la.Data DESC, la.Controle DESC');
  QrLct.Open;
  Result := True;
end;

procedure TFmLct1.TabSheet2Resize(Sender: TObject);
begin
  (*DBGrid1.Align := alClient;
  DBGrid1.Align := alNone;
  DBGrid1.Top := 24;
  DBGrid1.Height := DBGrid1.Height - 24;*)
end;

procedure TFmLct1.PageControl1Change(Sender: TObject);
begin
  ReopenLct;
end;

procedure TFmLct1.TabControl1Change(Sender: TObject);
begin
  ReopenLct;
end;

procedure TFmLct1.TPDataChange(Sender: TObject);
begin
  ConfiguraVencimento;
end;

procedure TFmLct1.TPDataClick(Sender: TObject);
begin
  ConfiguraVencimento;
end;

procedure TFmLct1.ConfiguraVencimento();
begin
  case QrCarteirasTipo.Value of
    0:
    begin
      if QrCarteirasPrazo.Value = 0 then
      begin
        LaVencimento.Enabled := True;
        TPVencimento.Enabled := True;
      end else begin
        LaVencimento.Enabled := False;
        TPVencimento.Enabled := False;
        TPVencimento.Date    := TPData.Date;
      end;
    end;
    1:
    begin
      LaVencimento.Enabled := False;
      TPVencimento.Enabled := False;
      TPVencimento.Date    := TPData.Date;
    end;
    2:
    begin
      LaVencimento.Enabled := True;
      TPVencimento.Enabled := True;
    end;
  end;
end;

procedure TFmLct1.EdQtdeExit(Sender: TObject);
begin
  EdQtde.Text := Geral.TFT_NULL(EdQtde.Text, 3, siPositivo);
end;

procedure TFmLct1.EventosCadReopen(CliInt: Integer);
begin
  QrEventosCad.Close;
  QrEventosCad.Params[0].AsInteger := CliInt;
  QrEventosCad.Open;
  //
  LaEventosCad.Enabled := QrEventosCad.RecordCount > 0;
  EdEventosCad.Enabled := QrEventosCad.RecordCount > 0;
  CBEventosCad.Enabled := QrEventosCad.RecordCount > 0;
  SbEventosCad.Enabled := QrEventosCad.RecordCount > 0;
  if not QrEventosCad.Locate('CodUsu', QrEventosCadCodUsu.Value, []) then
  begin
    EdEventosCad.ValueVariant := 0;
    CBEventosCad.KeyValue     := 0;
  end;
end;

procedure TFmLct1.MostraPnMaskPesq(Link: String);
begin
  PnLink.Caption     := Link;
  //Width := FLargMaior;
  //PnMaskPesq.Visible := True;
  EdLinkMask.Text    := '';
  EdLinkMask.SetFocus;
end;

procedure TFmLct1.SbEventosCadClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmEventosCad, FmEventosCad, afmoNegarComAviso) then
  begin
    FmEventosCad.ShowModal;
    FmEventosCad.Destroy;
    //
    if VAR_CADASTRO <> 0 then
      UMyMod.SetaCodUsuDeCodigo(EdEventosCad, CBEventosCad, QrEventosCad,
        VAR_CADASTRO);
  end;
end;

procedure TFmLct1.SelecionaItemDePesquisa;
begin
  if QrPesq.RecordCount > 0 then
  begin
    //Width := FLargMenor;
    //PnMaskPesq.Visible := False;
    if PnLink.Caption = 'Conta (Plano de contas)' then
    begin
      EdConta.Text     := IntToStr(QrPesqCodigo.Value);
      CBConta.KeyValue := QrPesqCodigo.Value;
      if CBConta.Visible then CBConta.SetFocus;
    end
  end;
end;

procedure TFmLct1.SpeedButton3Click(Sender: TObject);
begin
  //Width := FLargMenor;
  //PnMaskPesq.Visible := False;
end;

procedure TFmLct1.SpeedButton4Click(Sender: TObject);
begin
  VAR_ENTIDADE := 0;
  if DBCheck.CriaFm(TFmEntidades, FmEntidades, afmoNegarComAviso) then
  begin
    FmEntidades.ShowModal;
    FmEntidades.Destroy;
    //
    if VAR_ENTIDADE <> 0 then
    begin
      EdCliente.ValueVariant := VAR_ENTIDADE;
      CBCliente.KeyValue     := VAR_ENTIDADE;
    end;
  end;
end;

procedure TFmLct1.LaDocDownClick(Sender: TObject);
begin
  EdDoc.ValueVariant := EdDoc.ValueVariant - 1;
end;

procedure TFmLct1.LaDocUpClick(Sender: TObject);
begin
  EdDoc.ValueVariant := EdDoc.ValueVariant + 1;
end;

procedure TFmLct1.LaDuplicataDownClick(Sender: TObject);
begin
  EdDuplicata.Text := MLAGeral.DuplicataIncrementa(EdDuplicata.Text, -1);
end;

procedure TFmLct1.LaDuplicataUpClick(Sender: TObject);
begin
  EdDuplicata.Text := MLAGeral.DuplicataIncrementa(EdDuplicata.Text,  1);
end;

procedure TFmLct1.LaMezDownClick(Sender: TObject);
begin
  IncrementaMez(VK_DOWN);
end;

procedure TFmLct1.LaMezUpClick(Sender: TObject);
begin
  IncrementaMez(VK_UP);
end;

procedure TFmLct1.LaNFDownClick(Sender: TObject);
begin
  EdNF.ValueVariant := EdNF.ValueVariant - 1;
end;

procedure TFmLct1.LaNFUpClick(Sender: TObject);
begin
  EdNF.ValueVariant := EdNF.ValueVariant + 1;
end;

procedure TFmLct1.LLBPesqDblClick(Sender: TObject);
begin
  SelecionaItemDePesquisa;
end;

procedure TFmLct1.LLBPesqKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then SelecionaItemdePesquisa;
end;

procedure TFmLct1.EdLinkMaskChange(Sender: TObject);
begin
  QrPesq.Close;
  if Length(EdLinkMask.Text) >= 4 then
  begin
    if PnLink.Caption = 'Conta (Plano de contas)' then
    begin
      QrPesq.Close;
      QrPesq.SQL.Clear;
      QrPesq.SQL.Add('SELECT Codigo, Nome ');
      QrPesq.SQL.Add('FROM contas ');
      QrPesq.SQL.Add('WHERE Nome LIKE "%'+EdLinkMask.Text+'%"');
      QrPesq.SQL.Add('ORDER BY Nome');
      //QrPesq.SQL.Add('');
      QrPesq.Open;
    end
  end;
end;

procedure TFmLct1.EdLinkMaskKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_DOWN then LLBPesq.SetFocus;
end;

procedure TFmLct1.EdContaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F7 then
    MostraPnMaskPesq('Conta (Plano de contas)');
end;

procedure TFmLct1.CBContaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F7 then
    MostraPnMaskPesq('Conta (Plano de contas)');
end;

end.

