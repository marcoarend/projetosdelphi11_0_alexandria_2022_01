object FmLct1: TFmLct1
  Left = 339
  Top = 169
  Caption = 'FIN-LANCT-101 :: Edi'#231#227'o de Lan'#231'amentos [E]'
  ClientHeight = 593
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label16: TLabel
    Left = 231
    Top = 126
    Width = 11
    Height = 12
    Caption = 'p'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Wingdings 3'
    Font.Style = []
    ParentFont = False
  end
  object Label17: TLabel
    Left = 247
    Top = 126
    Width = 11
    Height = 12
    Caption = 'q'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Wingdings 3'
    Font.Style = []
    ParentFont = False
  end
  object PainelControle: TPanel
    Left = 0
    Top = 545
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object LaFunci: TLabel
      Left = 488
      Top = 8
      Width = 6
      Height = 13
      Caption = '0'
      Visible = False
    end
    object EdTipoAnt: TLabel
      Left = 472
      Top = 8
      Width = 6
      Height = 13
      Caption = '0'
      Visible = False
    end
    object EdCartAnt: TLabel
      Left = 456
      Top = 8
      Width = 6
      Height = 13
      Caption = '0'
      Visible = False
    end
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 8
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
      NumGlyphs = 2
    end
    object Panel3: TPanel
      Left = 676
      Top = 1
      Width = 107
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 7
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 40
    Align = alTop
    Caption = 'Edi'#231#227'o de Lan'#231'amentos [E]'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TdmkLabel
      Left = 705
      Top = 1
      Width = 78
      Height = 38
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 712
      ExplicitTop = 2
      ExplicitHeight = 36
    end
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 704
      Height = 38
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 710
      ExplicitHeight = 36
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 40
    Width = 784
    Height = 505
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    OnChange = PageControl1Change
    object TabSheet1: TTabSheet
      Caption = 'Edi'#231#227'o'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object PainelDados: TPanel
        Left = 0
        Top = 0
        Width = 577
        Height = 477
        Align = alLeft
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Panel1: TPanel
          Left = 0
          Top = 346
          Width = 577
          Height = 131
          Align = alBottom
          TabOrder = 0
          Visible = False
          object Label8: TLabel
            Left = 8
            Top = 4
            Width = 103
            Height = 13
            Caption = 'Mora dia ($) [F4 calc]:'
          end
          object Label7: TLabel
            Left = 120
            Top = 4
            Width = 88
            Height = 13
            Caption = 'Multa ($) [F4 calc]:'
          end
          object Label9: TLabel
            Left = 232
            Top = 4
            Width = 82
            Height = 13
            Caption = 'Data documento:'
          end
          object LaICMS_P: TLabel
            Left = 348
            Top = 4
            Width = 37
            Height = 13
            Caption = '% ICMS'
            Visible = False
          end
          object LaICMS_V: TLabel
            Left = 452
            Top = 4
            Width = 56
            Height = 13
            Caption = 'Valor ICMS:'
            Visible = False
          end
          object Label10: TLabel
            Left = 8
            Top = 44
            Width = 58
            Height = 13
            Caption = 'Funcion'#225'rio:'
            Visible = False
          end
          object LaVendedor: TLabel
            Left = 8
            Top = 88
            Width = 49
            Height = 13
            Caption = 'Vendedor:'
            Visible = False
          end
          object LaAccount: TLabel
            Left = 288
            Top = 88
            Width = 185
            Height = 13
            Caption = 'Representante ([F4 ] para preferencial):'
            Visible = False
          end
          object Label18: TLabel
            Left = 288
            Top = 44
            Width = 70
            Height = 13
            Caption = 'Departamento:'
            Enabled = False
          end
          object EdICMS_V: TdmkEdit
            Left = 452
            Top = 20
            Width = 101
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            Visible = False
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnExit = EdICMS_VExit
            OnKeyDown = EdICMS_VKeyDown
          end
          object EdICMS_P: TdmkEdit
            Left = 348
            Top = 20
            Width = 101
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            Visible = False
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnExit = EdICMS_PExit
            OnKeyDown = EdICMS_PKeyDown
          end
          object TPDataDoc: TdmkEditDateTimePicker
            Left = 232
            Top = 20
            Width = 112
            Height = 21
            Date = 37617.480364108800000000
            Time = 37617.480364108800000000
            Color = clWhite
            TabOrder = 2
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
          object EdMulta: TdmkEdit
            Left = 120
            Top = 20
            Width = 105
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnExit = EdMultaExit
            OnKeyDown = EdMultaKeyDown
          end
          object EdMoraDia: TdmkEdit
            Left = 8
            Top = 20
            Width = 105
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnExit = EdMoraDiaExit
            OnKeyDown = EdMoraDiaKeyDown
          end
          object EdFunci: TdmkEditCB
            Left = 8
            Top = 60
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            Visible = False
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdFunciChange
            DBLookupComboBox = CBFunci
          end
          object CBFunci: TdmkDBLookupComboBox
            Left = 64
            Top = 60
            Width = 221
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsFunci
            TabOrder = 6
            Visible = False
            dmkEditCB = EdFunci
            UpdType = utYes
          end
          object EdVendedor: TdmkEditCB
            Left = 8
            Top = 104
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 9
            Visible = False
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBVendedor
          end
          object CBVendedor: TdmkDBLookupComboBox
            Left = 64
            Top = 104
            Width = 221
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsVendedores
            TabOrder = 10
            Visible = False
            dmkEditCB = EdVendedor
            UpdType = utYes
          end
          object EdAccount: TdmkEditCB
            Left = 288
            Top = 104
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 11
            Visible = False
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnKeyDown = EdAccountKeyDown
            DBLookupComboBox = CBAccount
          end
          object CBAccount: TdmkDBLookupComboBox
            Left = 344
            Top = 104
            Width = 225
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsAccounts
            TabOrder = 12
            Visible = False
            OnKeyDown = CBAccountKeyDown
            dmkEditCB = EdAccount
            UpdType = utYes
          end
          object CBDepto: TdmkDBLookupComboBox
            Left = 348
            Top = 60
            Width = 220
            Height = 21
            Color = clWhite
            Enabled = False
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsDeptos
            TabOrder = 8
            dmkEditCB = EdDepto
            UpdType = utYes
          end
          object EdDepto: TdmkEditCB
            Left = 288
            Top = 60
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdFunciChange
            DBLookupComboBox = CBDepto
          end
        end
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 577
          Height = 346
          Align = alClient
          TabOrder = 1
          object LaCliInt: TLabel
            Left = 4
            Top = 4
            Width = 120
            Height = 13
            Caption = 'Empresa (cliente Interno):'
          end
          object Label1: TLabel
            Left = 456
            Top = 44
            Width = 26
            Height = 13
            Caption = 'Data:'
          end
          object Label14: TLabel
            Left = 488
            Top = 4
            Width = 62
            Height = 13
            Caption = 'Lan'#231'amento:'
          end
          object Label15: TLabel
            Left = 4
            Top = 44
            Width = 76
            Height = 13
            Caption = 'Item da carteira:'
          end
          object SpeedButton1: TSpeedButton
            Left = 428
            Top = 60
            Width = 21
            Height = 21
            Hint = 'Inclui item de carteira'
            Caption = '...'
            OnClick = SpeedButton1Click
          end
          object LaVencimento: TLabel
            Left = 456
            Top = 84
            Width = 59
            Height = 13
            Caption = 'Vencimento:'
          end
          object Label2: TLabel
            Left = 4
            Top = 84
            Width = 97
            Height = 13
            Caption = 'Conta [F7] pesquisa:'
          end
          object BtContas: TSpeedButton
            Left = 428
            Top = 100
            Width = 21
            Height = 21
            Hint = 'Inclui conta'
            Caption = '...'
            OnClick = BtContasClick
          end
          object LaMes: TLabel
            Left = 4
            Top = 124
            Width = 23
            Height = 13
            Caption = 'M'#234's:'
          end
          object LaDeb: TLabel
            Left = 80
            Top = 124
            Width = 34
            Height = 13
            Caption = 'D'#233'bito:'
          end
          object LaCred: TLabel
            Left = 172
            Top = 124
            Width = 36
            Height = 13
            Caption = 'Cr'#233'dito:'
          end
          object LaNF: TLabel
            Left = 264
            Top = 124
            Width = 23
            Height = 13
            Caption = 'N.F.:'
          end
          object LaDoc: TLabel
            Left = 388
            Top = 124
            Width = 58
            Height = 13
            Caption = 'Documento:'
          end
          object Label11: TLabel
            Left = 484
            Top = 124
            Width = 48
            Height = 13
            Caption = 'Duplicata:'
          end
          object Label3: TLabel
            Left = 4
            Top = 164
            Width = 43
            Height = 13
            Caption = 'Quantid.:'
          end
          object Label13: TLabel
            Left = 60
            Top = 164
            Width = 114
            Height = 13
            Caption = 'Descri'#231#227'o [F4 : F5 : F6]:'
          end
          object LaCliente: TLabel
            Left = 4
            Top = 204
            Width = 94
            Height = 13
            Caption = 'Membro ou terceiro:'
          end
          object LaFornecedor: TLabel
            Left = 4
            Top = 244
            Width = 57
            Height = 13
            Caption = 'Fornecedor:'
          end
          object Label6: TLabel
            Left = 328
            Top = 124
            Width = 51
            Height = 13
            Caption = 'S'#233'rie doc.:'
          end
          object LaMezUp: TLabel
            Left = 43
            Top = 126
            Width = 11
            Height = 12
            Caption = 'p'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Wingdings 3'
            Font.Style = []
            ParentFont = False
            OnClick = LaMezUpClick
          end
          object LaMezDown: TLabel
            Left = 59
            Top = 126
            Width = 11
            Height = 12
            Caption = 'q'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Wingdings 3'
            Font.Style = []
            ParentFont = False
            OnClick = LaMezDownClick
          end
          object LaNFUp: TLabel
            Left = 295
            Top = 126
            Width = 11
            Height = 12
            Caption = 'p'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Wingdings 3'
            Font.Style = []
            ParentFont = False
            OnClick = LaNFUpClick
          end
          object LaNFDown: TLabel
            Left = 311
            Top = 126
            Width = 11
            Height = 12
            Caption = 'q'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Wingdings 3'
            Font.Style = []
            ParentFont = False
            OnClick = LaNFDownClick
          end
          object LaDocUp: TLabel
            Left = 451
            Top = 126
            Width = 11
            Height = 12
            Caption = 'p'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Wingdings 3'
            Font.Style = []
            ParentFont = False
            OnClick = LaDocUpClick
          end
          object LaDocDown: TLabel
            Left = 467
            Top = 126
            Width = 11
            Height = 12
            Caption = 'q'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Wingdings 3'
            Font.Style = []
            ParentFont = False
            OnClick = LaDocDownClick
          end
          object LaDuplicataUp: TLabel
            Left = 539
            Top = 126
            Width = 11
            Height = 12
            Caption = 'p'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Wingdings 3'
            Font.Style = []
            ParentFont = False
            OnClick = LaDuplicataUpClick
          end
          object LaDuplicataDown: TLabel
            Left = 555
            Top = 126
            Width = 11
            Height = 12
            Caption = 'q'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Wingdings 3'
            Font.Style = []
            ParentFont = False
            OnClick = LaDuplicataDownClick
          end
          object LaEventosCad: TLabel
            Left = 4
            Top = 284
            Width = 37
            Height = 13
            Caption = 'Evento:'
          end
          object SbEventosCad: TSpeedButton
            Left = 548
            Top = 300
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbEventosCadClick
          end
          object SpeedButton2: TSpeedButton
            Left = 548
            Top = 260
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton2Click
          end
          object SpeedButton4: TSpeedButton
            Left = 548
            Top = 220
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton4Click
          end
          object CBCliInt: TdmkDBLookupComboBox
            Left = 64
            Top = 20
            Width = 421
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsCliInt
            TabOrder = 1
            dmkEditCB = EdCliInt
            UpdType = utYes
          end
          object EdCliInt: TdmkEditCB
            Left = 4
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdFunciChange
            DBLookupComboBox = CBCliInt
          end
          object TPData: TdmkEditDateTimePicker
            Left = 456
            Top = 60
            Width = 112
            Height = 21
            Date = 37617.480364108800000000
            Time = 37617.480364108800000000
            Color = clWhite
            TabOrder = 5
            OnClick = TPDataClick
            OnChange = TPDataChange
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
          object EdCodigo: TdmkEdit
            Left = 488
            Top = 20
            Width = 77
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8281908
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdCarteira: TdmkEditCB
            Left = 4
            Top = 60
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdCarteiraChange
            DBLookupComboBox = CBCarteira
          end
          object CBCarteira: TdmkDBLookupComboBox
            Left = 60
            Top = 60
            Width = 365
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCarteiras
            TabOrder = 4
            dmkEditCB = EdCarteira
            UpdType = utYes
          end
          object TPVencimento: TdmkEditDateTimePicker
            Left = 456
            Top = 100
            Width = 112
            Height = 21
            Date = 37617.480364108800000000
            Time = 37617.480364108800000000
            Color = clWhite
            TabOrder = 8
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
          object EdConta: TdmkEditCB
            Left = 4
            Top = 100
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnKeyDown = EdContaKeyDown
            DBLookupComboBox = CBConta
          end
          object CBConta: TdmkDBLookupComboBox
            Left = 60
            Top = 100
            Width = 365
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsContas
            TabOrder = 7
            OnKeyDown = CBContaKeyDown
            dmkEditCB = EdConta
            UpdType = utYes
          end
          object EdMes: TdmkEdit
            Left = 4
            Top = 140
            Width = 72
            Height = 21
            Alignment = taCenter
            Enabled = False
            TabOrder = 9
            FormatType = dmktfMesAno
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = Null
            OnExit = EdMesExit
          end
          object EdDeb: TdmkEdit
            Left = 80
            Top = 140
            Width = 88
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnExit = EdDebExit
          end
          object EdCred: TdmkEdit
            Left = 172
            Top = 140
            Width = 88
            Height = 21
            Alignment = taRightJustify
            TabOrder = 11
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnExit = EdCredExit
          end
          object EdNF: TdmkEdit
            Left = 264
            Top = 140
            Width = 60
            Height = 21
            Alignment = taRightJustify
            TabOrder = 12
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnExit = EdNFExit
            OnKeyDown = EdNFKeyDown
          end
          object EdDoc: TdmkEdit
            Left = 388
            Top = 140
            Width = 92
            Height = 21
            Alignment = taRightJustify
            TabOrder = 14
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnExit = EdDocExit
          end
          object EdDuplicata: TdmkEdit
            Left = 484
            Top = 140
            Width = 84
            Height = 21
            Alignment = taCenter
            TabOrder = 15
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnKeyDown = EdDuplicataKeyDown
          end
          object EdDescricao: TdmkEdit
            Left = 60
            Top = 180
            Width = 508
            Height = 21
            TabOrder = 17
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnKeyDown = EdDescricaoKeyDown
          end
          object EdQtde: TdmkEdit
            Left = 4
            Top = 180
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 16
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnExit = EdQtdeExit
          end
          object EdCliente: TdmkEditCB
            Left = 4
            Top = 220
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 18
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBCliente
          end
          object CBCliente: TdmkDBLookupComboBox
            Left = 60
            Top = 220
            Width = 488
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsClientes
            TabOrder = 19
            dmkEditCB = EdCliente
            UpdType = utYes
          end
          object EdFornecedor: TdmkEditCB
            Left = 4
            Top = 260
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 20
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBFornecedor
          end
          object CBFornecedor: TdmkDBLookupComboBox
            Left = 60
            Top = 260
            Width = 488
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsFornecedores
            TabOrder = 21
            dmkEditCB = EdFornecedor
            UpdType = utYes
          end
          object EdSerieCH: TdmkEdit
            Left = 328
            Top = 140
            Width = 56
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 13
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnKeyDown = EdDescricaoKeyDown
          end
          object CkCancelado: TdmkCheckBox
            Left = 8
            Top = 324
            Width = 433
            Height = 17
            Caption = 
              'Lan'#231'amento cancelado (cr'#233'dito e d'#233'bito ser'#227'o zerados e status se' +
              'r'#225' igual a cancelado).'
            TabOrder = 24
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
          object EdEventosCad: TdmkEditCB
            Left = 4
            Top = 300
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 22
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBEventosCad
          end
          object CBEventosCad: TdmkDBLookupComboBox
            Left = 60
            Top = 300
            Width = 488
            Height = 21
            Color = clWhite
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsEventosCad
            TabOrder = 23
            dmkEditCB = EdEventosCad
            UpdType = utYes
          end
        end
      end
      object PnMaskPesq: TPanel
        Left = 577
        Top = 0
        Width = 199
        Height = 477
        Align = alClient
        Caption = 'PnMaskPesq'
        TabOrder = 1
        object Panel5: TPanel
          Left = 1
          Top = 21
          Width = 197
          Height = 44
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 0
          object Label5: TLabel
            Left = 4
            Top = 4
            Width = 186
            Height = 13
            Caption = 'Digite parte da descri'#231#227'o (min. 4 letras):'
          end
          object EdLinkMask: TEdit
            Left = 4
            Top = 20
            Width = 201
            Height = 21
            TabStop = False
            TabOrder = 0
            OnChange = EdLinkMaskChange
            OnKeyDown = EdLinkMaskKeyDown
          end
        end
        object LLBPesq: TDBLookupListBox
          Left = 1
          Top = 65
          Width = 197
          Height = 407
          Align = alClient
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsPesq
          TabOrder = 1
          TabStop = False
          OnDblClick = LLBPesqDblClick
          OnKeyDown = LLBPesqKeyDown
        end
        object PnLink: TPanel
          Left = 1
          Top = 1
          Width = 197
          Height = 20
          Align = alTop
          BevelOuter = bvLowered
          Caption = 'Sem link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          object Panel6: TPanel
            Left = 178
            Top = 1
            Width = 18
            Height = 18
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 0
            object SpeedButton3: TSpeedButton
              Left = 0
              Top = 0
              Width = 18
              Height = 18
              Caption = 'x'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Layout = blGlyphBottom
              ParentFont = False
              OnClick = SpeedButton3Click
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Hist'#243'rico'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object TabControl1: TTabControl
        Left = 0
        Top = 0
        Width = 776
        Height = 477
        Align = alClient
        TabOrder = 0
        Tabs.Strings = (
          'Carteira'
          'Cliente interno'
          'Membro'
          'Fornecedor'
          'Conta')
        TabIndex = 0
        OnChange = TabControl1Change
        object DBGrid1: TDBGrid
          Left = 4
          Top = 24
          Width = 768
          Height = 449
          Align = alClient
          DataSource = DsLct
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Data'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Documento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMERELACIONADO'
              Title.Caption = 'Empresa'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NotaFiscal'
              Title.Caption = 'N.F.'
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'Duplicata'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Caption = 'D'#233'bito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Caption = 'Cr'#233'dito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencim.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COMPENSADO_TXT'
              Title.Caption = 'Compen.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MENSAL'
              Title.Caption = 'M'#234's'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESIT'
              Title.Caption = 'Situa'#231#227'o'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Lan'#231'amento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sub'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO'
              Title.Caption = 'Saldo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ICMS_P'
              Title.Caption = '% ICMS'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ICMS_V'
              Title.Caption = '$ ICMS'
              Visible = True
            end>
        end
      end
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 340
    Top = 68
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 404
    Top = 68
  end
  object DsFornecedores: TDataSource
    DataSet = QrFornecedores
    Left = 232
    Top = 372
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 516
    Top = 372
  end
  object DsNF: TDataSource
    DataSet = QrNF
    Left = 36
    Top = 5
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrContasAfterScroll
    SQL.Strings = (
      'SELECT co.*, sg.Nome NOMESUBGRUPO ,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'CASE 1'
      'WHEN cl.Tipo=0 THEN cl.RazaoSocial'
      'ELSE cl.Nome END NOMEEMPRESA'
      'FROM contas co, Subgrupos sg,'
      'Grupos gr, Conjuntos cj, Entidades cl'
      'WHERE sg.Codigo=co.Subgrupo'
      'AND gr.Codigo=sg.Grupo'
      'AND cj.Codigo=gr.Conjunto'
      'AND cl.Codigo=co.Empresa'
      'AND co.Terceiro=0'
      'AND co.Codigo>0'
      'ORDER BY co.Nome')
    Left = 312
    Top = 68
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrContasNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object QrContasNome3: TWideStringField
      FieldName = 'Nome3'
      Required = True
      Size = 50
    end
    object QrContasID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrContasSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrContasEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrContasCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContasDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContasMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContasExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrContasMensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrContasMensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrContasMensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrContasMenscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrContasMensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrContasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContasTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrContasExcel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrContasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContasUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrContasUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrContasNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Required = True
      Size = 50
    end
    object QrContasNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Required = True
      Size = 50
    end
    object QrContasNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Required = True
      Size = 50
    end
    object QrContasNOMEEMPRESA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Fatura, Fechamento, Prazo, Tipo'
      'FROM carteiras '
      'WHERE ForneceI=:P0'
      'AND ('
      '  Ativo=1'
      '  OR'
      '  Saldo<>0'
      ')'
      'ORDER BY Nome')
    Left = 376
    Top = 68
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrCarteirasFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrCarteirasFechamento: TIntegerField
      FieldName = 'Fechamento'
    end
    object QrCarteirasPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object QrFatura: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(Data) Data '
      'FROM faturas'
      'WHERE Emissao=:P0')
    Left = 536
    Top = 204
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFaturaData: TDateField
      FieldKind = fkInternalCalc
      FieldName = 'Data'
      Required = True
    end
  end
  object QrNF: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT la.*, co.Nome CONTA,'
      'CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMECLIENTE '
      'FROM lanctos la, Contas co, Entidades en'
      'WHERE co.Codigo=la.Genero'
      'AND la.Credito>0 '
      'AND en.Codigo=la.Cliente'
      'AND la.NotaFiscal=:P0')
    Left = 8
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFCONTA: TWideStringField
      FieldName = 'CONTA'
      Required = True
      Size = 50
    end
    object QrNFNOMECLIENTE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrNFData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrNFTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrNFCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrNFControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrNFSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrNFAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrNFGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrNFDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrNFNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrNFDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrNFCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrNFCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrNFDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrNFSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrNFVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrNFLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrNFID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrNFID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrNFFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrNFBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrNFLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrNFCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrNFLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrNFOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrNFLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrNFPago: TFloatField
      FieldName = 'Pago'
    end
    object QrNFMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrNFFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrNFCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrNFMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrNFMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrNFProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrNFDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrNFUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrNFDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrNFCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrNFNivel: TIntegerField
      FieldName = 'Nivel'
      Required = True
    end
    object QrNFVendedor: TIntegerField
      FieldName = 'Vendedor'
      Required = True
    end
    object QrNFAccount: TIntegerField
      FieldName = 'Account'
      Required = True
    end
    object QrNFFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Account,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'OR Cliente2="V"'
      'ORDER BY NomeENTIDADE')
    Left = 488
    Top = 372
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrClientesAccount: TIntegerField
      FieldName = 'Account'
      Required = True
    end
  end
  object QrFornecedores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece1="V"'
      'OR Fornece2="V"'
      'OR Fornece3="V"'
      'OR Fornece4="V"'
      'ORDER BY NomeENTIDADE')
    Left = 204
    Top = 372
    object QrFornecedoresCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFornecedoresNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object QrVendedores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE 1'
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NomeENTIDADE')
    Left = 204
    Top = 412
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object StringField1: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsVendedores: TDataSource
    DataSet = QrVendedores
    Left = 232
    Top = 412
  end
  object DsAccounts: TDataSource
    DataSet = QrAccounts
    Left = 516
    Top = 412
  end
  object QrAccounts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,  '
      'CASE 1'
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NomeENTIDADE')
    Left = 488
    Top = 412
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object StringField2: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object QrFunci: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NomeENTIDADE')
    Left = 204
    Top = 332
    object QrFunciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFunciNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsFunci: TDataSource
    DataSet = QrFunci
    Left = 232
    Top = 332
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Account,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE CliInt <> 0'
      'OR Codigo < -10'
      'ORDER BY NomeENTIDADE')
    Left = 488
    Top = 332
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrCliIntNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 516
    Top = 332
  end
  object QrLct: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2, '
      '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, '
      'la.*, ct.Codigo CONTA, '
      'ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial '
      'ELSE em.Nome END NOMEEMPRESA, '
      'CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial '
      'ELSE cl.Nome END NOMECLIENTE,'
      'CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial '
      'ELSE fo.Nome END NOMEFORNECEDOR,'
      'ca.Nome NOMECARTEIRA, ca.Saldo SALDOCARTEIRA'
      'FROM lanctos la'
      
        'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = ' +
        '0'
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo'
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      'ORDER BY la.Data, la.Controle')
    Left = 188
    Top = 40
    object QrLctData: TDateField
      FieldName = 'Data'
      Origin = 'lanctos.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'lanctos.Tipo'
    end
    object QrLctCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'lanctos.Carteira'
    end
    object QrLctAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      Origin = 'lanctos.Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrLctGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'lanctos.Genero'
    end
    object QrLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'lanctos.Descricao'
      Size = 25
    end
    object QrLctNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'lanctos.NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrLctDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'lanctos.Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'lanctos.Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'lanctos.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'lanctos.Documento'
      DisplayFormat = '000000; ; '
    end
    object QrLctSit: TIntegerField
      FieldName = 'Sit'
      Origin = 'lanctos.Sit'
    end
    object QrLctVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'lanctos.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'lanctos.Lk'
    end
    object QrLctFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'lanctos.FatID'
    end
    object QrLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'lanctos.FatParcela'
    end
    object QrLctCONTA: TIntegerField
      FieldName = 'CONTA'
      Origin = 'contas.Codigo'
    end
    object QrLctNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      FixedChar = True
      Size = 128
    end
    object QrLctNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Origin = 'contas.Nome'
      Size = 128
    end
    object QrLctNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Origin = 'subgrupos.Nome'
      Size = 4
    end
    object QrLctNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Origin = 'grupos.Nome'
      Size = 4
    end
    object QrLctNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Origin = 'conjuntos.Nome'
      Size = 4
    end
    object QrLctNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrLctAno: TFloatField
      FieldName = 'Ano'
    end
    object QrLctMENSAL: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrLctMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrLctBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLctLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrLctFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrLctSub: TSmallintField
      FieldName = 'Sub'
      DisplayFormat = '00; ; '
    end
    object QrLctCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrLctLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrLctPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLctSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrLctID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrLctMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLctcliente: TIntegerField
      FieldName = 'cliente'
    end
    object QrLctMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLctNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrLctNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrLctTIPOEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrLctNOMERELACIONADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMERELACIONADO'
      Size = 50
      Calculated = True
    end
    object QrLctOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrLctLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrLctMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLctATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrLctJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLctDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLctNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrLctVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLctAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrLctMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrLctProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrLctDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLctDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLctUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrLctUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLctID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrLctCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrLctFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLctICMS_P: TFloatField
      FieldName = 'ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrLctCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrLctCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLctNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrLctSALDOCARTEIRA: TFloatField
      FieldName = 'SALDOCARTEIRA'
    end
    object QrLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object DsLct: TDataSource
    DataSet = QrLct
    Left = 216
    Top = 40
  end
  object DsDeptos: TDataSource
    DataSet = QrDeptos
    Left = 516
    Top = 252
  end
  object QrDeptos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM departamentos'
      'ORDER BY Nome')
    Left = 488
    Top = 252
    object QrDeptosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDeptosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'WHERE Nome LIKE :P0'
      'ORDER BY Nome')
    Left = 624
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'contas.Codigo'
    end
    object QrPesqNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'contas.Nome'
      Size = 50
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 652
    Top = 144
  end
  object QrEventosCad: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrContasAfterScroll
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM eventoscad'
      'WHERE Ativo=1'
      'AND CliInt=:P0'
      'ORDER BY Nome')
    Left = 324
    Top = 356
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEventosCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEventosCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEventosCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsEventosCad: TDataSource
    DataSet = QrEventosCad
    Left = 352
    Top = 356
  end
end
