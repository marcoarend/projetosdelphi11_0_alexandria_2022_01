object FmLctPgVarios: TFmLctPgVarios
  Left = 404
  Top = 197
  Caption = 'Compensa'#231#227'o em Conta Corrente'
  ClientHeight = 496
  ClientWidth = 927
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 927
    Height = 57
    Align = alTop
    TabOrder = 0
    object Label3: TLabel
      Left = 12
      Top = 8
      Width = 85
      Height = 13
      Caption = 'Data da quita'#231#227'o:'
    end
    object Label2: TLabel
      Left = 148
      Top = 8
      Width = 40
      Height = 13
      Caption = 'Multa %:'
    end
    object Label4: TLabel
      Left = 244
      Top = 8
      Width = 63
      Height = 13
      Caption = 'Juros %/m'#234's:'
    end
    object Label1: TLabel
      Left = 344
      Top = 8
      Width = 54
      Height = 13
      Caption = 'Total pago:'
    end
    object SpeedButton1: TSpeedButton
      Left = 440
      Top = 24
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object TPData: TdmkEditDateTimePicker
      Left = 12
      Top = 24
      Width = 133
      Height = 21
      Date = 39411.393054282400000000
      Time = 39411.393054282400000000
      TabOrder = 0
      OnExit = TPDataExit
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object EdMulta: TdmkEdit
      Left = 148
      Top = 24
      Width = 93
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdMultaExit
    end
    object EdTaxaM: TdmkEdit
      Left = 244
      Top = 24
      Width = 93
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdTaxaMExit
    end
    object EdTotalPago: TdmkEdit
      Left = 344
      Top = 24
      Width = 93
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object GroupBox1: TGroupBox
      Left = 496
      Top = 1
      Width = 430
      Height = 55
      Align = alRight
      Enabled = False
      TabOrder = 4
      object Label5: TLabel
        Left = 8
        Top = 12
        Width = 63
        Height = 13
        Caption = 'Valor original:'
        FocusControl = DBEdit1
      end
      object Label6: TLabel
        Left = 112
        Top = 12
        Width = 54
        Height = 13
        Caption = 'Valor pago:'
        FocusControl = DBEdit2
      end
      object Label7: TLabel
        Left = 216
        Top = 12
        Width = 55
        Height = 13
        Caption = 'Valor multa:'
        FocusControl = DBEdit3
      end
      object Label8: TLabel
        Left = 320
        Top = 12
        Width = 52
        Height = 13
        Caption = 'Valor juros:'
        FocusControl = DBEdit4
      end
      object DBEdit1: TDBEdit
        Left = 8
        Top = 28
        Width = 100
        Height = 21
        DataField = 'ValorOri'
        DataSource = DsSum
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 112
        Top = 28
        Width = 100
        Height = 21
        DataField = 'ValorPgt'
        DataSource = DsSum
        TabOrder = 1
      end
      object DBEdit3: TDBEdit
        Left = 216
        Top = 28
        Width = 100
        Height = 21
        DataField = 'MultaVal'
        DataSource = DsSum
        TabOrder = 2
      end
      object DBEdit4: TDBEdit
        Left = 320
        Top = 28
        Width = 100
        Height = 21
        DataField = 'JurosVal'
        DataSource = DsSum
        TabOrder = 3
      end
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 448
    Width = 927
    Height = 48
    Align = alBottom
    TabOrder = 2
    object BtOK: TBitBtn
      Tag = 14
      Left = 16
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel1: TPanel
      Left = 811
      Top = 1
      Width = 115
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtCancela: TBitBtn
        Tag = 15
        Left = 6
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Cancela'
        TabOrder = 0
        OnClick = BtCancelaClick
        NumGlyphs = 2
      end
    end
    object PB1: TProgressBar
      Left = 128
      Top = 20
      Width = 673
      Height = 17
      TabOrder = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 927
    Height = 48
    Align = alTop
    Caption = 'Compensa'#231#227'o em Conta Corrente'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 3
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 925
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 105
    Width = 927
    Height = 343
    Align = alClient
    DataSource = DsLctoEdit
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Data'
        Title.Alignment = taCenter
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ValorOri'
        Title.Caption = 'Valor original'
        Width = 71
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ValorPgt'
        Title.Caption = 'Valor pagto'
        Width = 71
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'JurosVal'
        Title.Caption = '$ Juros'
        Width = 71
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MultaVal'
        Title.Caption = '$ Multa'
        Width = 71
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Vencimento'
        Title.Alignment = taCenter
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Descricao'
        Title.Caption = 'Descri'#231#227'o'
        Width = 339
        Visible = True
      end>
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Tipo'
      'FROM carteiras'
      'WHERE Tipo=0'
      'AND ForneceI=:P0'
      'ORDER BY Nome')
    Left = 152
    Top = 72
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 180
    Top = 72
  end
  object TbLctoEdit: TmySQLTable
    Database = DModG.MyPID_DB
    BeforePost = TbLctoEditBeforePost
    AfterPost = TbLctoEditAfterPost
    TableName = 'LctoEdit'
    Left = 200
    Top = 196
    object TbLctoEditControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'lctoedit.Controle'
      ReadOnly = True
    end
    object TbLctoEditSub: TIntegerField
      FieldName = 'Sub'
      Origin = 'lctoedit.Sub'
      ReadOnly = True
    end
    object TbLctoEditDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'lctoedit.Descricao'
      ReadOnly = True
      Size = 255
    end
    object TbLctoEditData: TDateField
      FieldName = 'Data'
      Origin = 'lctoedit.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object TbLctoEditVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'lctoedit.Vencimento'
      ReadOnly = True
      DisplayFormat = 'dd/mm/yy'
    end
    object TbLctoEditMultaVal: TFloatField
      FieldName = 'MultaVal'
      Origin = 'lctoedit.MultaVal'
      DisplayFormat = '#,###,##0.00'
    end
    object TbLctoEditJurosVal: TFloatField
      FieldName = 'JurosVal'
      Origin = 'lctoedit.JurosVal'
      DisplayFormat = '#,###,##0.00'
    end
    object TbLctoEditValorOri: TFloatField
      FieldName = 'ValorOri'
      Origin = 'lctoedit.ValorOri'
      ReadOnly = True
      DisplayFormat = '#,###,##0.00'
    end
    object TbLctoEditValorPgt: TFloatField
      FieldName = 'ValorPgt'
      Origin = 'lctoedit.ValorPgt'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsLctoEdit: TDataSource
    DataSet = TbLctoEdit
    Left = 228
    Top = 196
  end
  object QrLancto1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT la.Controle, la.Sub, la.Descricao, la.Credito, la.Debito,'
      'la.NotaFiscal, la.Documento, la.Vencimento, la.Carteira, Data,'
      'ca.Nome NOMECARTEIRA, ca.Tipo TIPOCARTEIRA ,'
      'ca.Banco CARTEIRABANCO, la.Cliente, la.Fornecedor, '
      'la.DataDoc, la.Nivel, la.CtrlIni, la.Vendedor, la.Account,'
      
        'la.Genero, la.Mez, la.Duplicata, la.Doc2, la.SerieCH, la.MoraDia' +
        ','
      
        'la.Multa, la.ICMS_P, la.ICMS_V, la.CliInt, la.Depto, la.DescoPor' +
        ','
      'la.ForneceI, la.Unidade, la.Qtde, la.FatID, la.FatID_Sub, '
      'la.FatNum, la.DescoVal, la.NFVal, la.FatParcela'
      'FROM lanctos la, Carteiras ca'
      'WHERE la.Controle=:P0'
      'AND la.Sub=:P1'
      'AND ca.Codigo=la.Carteira')
    Left = 656
    Top = 240
    ParamData = <
      item
        DataType = ftFloat
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLancto1Sub: TSmallintField
      FieldName = 'Sub'
      Origin = 'DBMMONEY.lanctos.Sub'
    end
    object QrLancto1Descricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'DBMMONEY.lanctos.Descricao'
      Size = 128
    end
    object QrLancto1Credito: TFloatField
      FieldName = 'Credito'
      Origin = 'DBMMONEY.lanctos.Credito'
      DisplayFormat = '#,###,##0.00  '
    end
    object QrLancto1Debito: TFloatField
      FieldName = 'Debito'
      Origin = 'DBMMONEY.lanctos.Debito'
      DisplayFormat = '#,###,##0.00  '
    end
    object QrLancto1NotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'DBMMONEY.lanctos.NotaFiscal'
    end
    object QrLancto1Documento: TFloatField
      FieldName = 'Documento'
      Origin = 'DBMMONEY.lanctos.Documento'
    end
    object QrLancto1Vencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'DBMMONEY.lanctos.Vencimento'
    end
    object QrLancto1Carteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'DBMMONEY.lanctos.Carteira'
    end
    object QrLancto1Data: TDateField
      FieldName = 'Data'
      Origin = 'DBMMONEY.lanctos.Data'
    end
    object QrLancto1NOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
    object QrLancto1TIPOCARTEIRA: TIntegerField
      FieldName = 'TIPOCARTEIRA'
      Origin = 'DBMMONEY.carteiras.Tipo'
    end
    object QrLancto1CARTEIRABANCO: TIntegerField
      FieldName = 'CARTEIRABANCO'
      Origin = 'DBMMONEY.carteiras.Banco'
    end
    object QrLancto1Cliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'DBMMONEY.lanctos.Cliente'
    end
    object QrLancto1Fornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Origin = 'DBMMONEY.lanctos.Fornecedor'
    end
    object QrLancto1DataDoc: TDateField
      FieldName = 'DataDoc'
      Origin = 'DBMMONEY.lanctos.DataDoc'
    end
    object QrLancto1Nivel: TIntegerField
      FieldName = 'Nivel'
      Origin = 'DBMMONEY.lanctos.Nivel'
    end
    object QrLancto1Vendedor: TIntegerField
      FieldName = 'Vendedor'
      Origin = 'DBMMONEY.lanctos.Vendedor'
    end
    object QrLancto1Account: TIntegerField
      FieldName = 'Account'
      Origin = 'DBMMONEY.lanctos.Account'
    end
    object QrLancto1Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLancto1CtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrLancto1Genero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLancto1Mez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrLancto1Duplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrLancto1Doc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrLancto1SerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrLancto1MoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLancto1Multa: TFloatField
      FieldName = 'Multa'
    end
    object QrLancto1ICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrLancto1ICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrLancto1CliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLancto1Depto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLancto1DescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrLancto1ForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLancto1Unidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrLancto1Qtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrLancto1FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLancto1FatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLancto1DescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrLancto1NFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrLancto1FatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLancto1FatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object QrLancto2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Tipo, Credito, Debito, Controle, Sub, Carteira'
      'FROM lanctos '
      'WHERE ID_Pgto=:P0 '
      'AND ID_Sub=:P1')
    Left = 684
    Top = 240
    ParamData = <
      item
        DataType = ftFloat
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLancto2Tipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'DBMMONEY.lanctos.Tipo'
    end
    object QrLancto2Credito: TFloatField
      FieldName = 'Credito'
      Origin = 'DBMMONEY.lanctos.Credito'
    end
    object QrLancto2Debito: TFloatField
      FieldName = 'Debito'
      Origin = 'DBMMONEY.lanctos.Debito'
    end
    object QrLancto2Sub: TSmallintField
      FieldName = 'Sub'
      Origin = 'DBMMONEY.lanctos.Sub'
    end
    object QrLancto2Carteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'DBMMONEY.lanctos.Carteira'
    end
    object QrLancto2Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrSum: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterOpen = QrSumAfterOpen
    SQL.Strings = (
      'SELECT SUM(ValorOri) ValorOri, SUM(ValorPgt) ValorPgt,'
      'SUM(MultaVal) MultaVal, SUM(JurosVal) JurosVal'
      'FROM lctoedit')
    Left = 200
    Top = 224
    object QrSumValorPgt: TFloatField
      FieldName = 'ValorPgt'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumValorOri: TFloatField
      FieldName = 'ValorOri'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumMultaVal: TFloatField
      FieldName = 'MultaVal'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumJurosVal: TFloatField
      FieldName = 'JurosVal'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSum: TDataSource
    DataSet = QrSum
    Left = 228
    Top = 224
  end
  object QrTot: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT SUM(ValorOri) ValorOri, SUM(ValorPgt) ValorPgt,'
      'SUM(MultaVal) MultaVal, SUM(JurosVal) JurosVal'
      'FROM lctoedit')
    Left = 200
    Top = 252
    object QrTotValorOri: TFloatField
      FieldName = 'ValorOri'
    end
    object QrTotValorPgt: TFloatField
      FieldName = 'ValorPgt'
    end
    object QrTotMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrTotJurosVal: TFloatField
      FieldName = 'JurosVal'
    end
  end
  object DsTot: TDataSource
    DataSet = QrTot
    Left = 228
    Top = 252
  end
end
