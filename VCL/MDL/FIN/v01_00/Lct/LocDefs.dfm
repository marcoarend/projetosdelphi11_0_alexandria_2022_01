object FmLocDefs: TFmLocDefs
  Left = 334
  Top = 177
  Caption = 'Defini'#231#245'es para Localiza'#231#227'o de Lan'#231'amentos'
  ClientHeight = 614
  ClientWidth = 703
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 84
    Width = 703
    Height = 482
    Align = alClient
    TabOrder = 0
    object Bevel1: TBevel
      Left = 24
      Top = 302
      Width = 185
      Height = 57
    end
    object Bevel2: TBevel
      Left = 216
      Top = 302
      Width = 185
      Height = 57
    end
    object LaForneceRN: TLabel
      Left = 208
      Top = 205
      Width = 85
      Height = 13
      Caption = '[F5] Raz'#227'o/Nome'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object LaForneceFA: TLabel
      Left = 300
      Top = 205
      Width = 101
      Height = 13
      Caption = '[F6] Fantasia/Apelido'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 300
      Top = 254
      Width = 101
      Height = 13
      Caption = '[F6] Fantasia/Apelido'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 208
      Top = 254
      Width = 85
      Height = 13
      Caption = '[F5] Raz'#227'o/Nome'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object CkDataIni: TCheckBox
      Left = 24
      Top = 8
      Width = 149
      Height = 17
      Caption = 'Data inicial ou '#250'nica'
      TabOrder = 0
    end
    object CkDataFim: TCheckBox
      Left = 216
      Top = 8
      Width = 97
      Height = 17
      Caption = 'Data final'
      TabOrder = 2
      OnClick = CkDataFimClick
    end
    object TPDataIni: TdmkEditDateTimePicker
      Left = 24
      Top = 28
      Width = 186
      Height = 21
      CalColors.TextColor = clMenuText
      Date = 37636.777157974500000000
      Time = 37636.777157974500000000
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object TPDataFim: TdmkEditDateTimePicker
      Left = 216
      Top = 28
      Width = 186
      Height = 21
      Date = 37636.777203761600000000
      Time = 37636.777203761600000000
      TabOrder = 3
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object CkConta: TCheckBox
      Left = 24
      Top = 108
      Width = 373
      Height = 17
      Caption = 'Conta'
      TabOrder = 8
    end
    object CBConta: TdmkDBLookupComboBox
      Left = 72
      Top = 128
      Width = 329
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContas
      TabOrder = 10
      dmkEditCB = EdConta
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object CkDescricao: TCheckBox
      Left = 24
      Top = 156
      Width = 373
      Height = 17
      Caption = 'Descri'#231#227'o parcial ou total (Utilize "%" para m'#225'scara):'
      TabOrder = 11
    end
    object EdDescricao: TEdit
      Left = 24
      Top = 177
      Width = 377
      Height = 21
      TabOrder = 12
      Text = '%%'
    end
    object CkDebito: TCheckBox
      Left = 32
      Top = 306
      Width = 153
      Height = 17
      Caption = 'D'#233'bito (m'#237'nimo e m'#225'ximo)'
      TabOrder = 19
    end
    object CkCredito: TCheckBox
      Left = 223
      Top = 306
      Width = 154
      Height = 17
      Caption = 'Cr'#233'dito (m'#237'nimo e m'#225'ximo)'
      TabOrder = 22
    end
    object CkNF: TCheckBox
      Left = 24
      Top = 366
      Width = 49
      Height = 17
      Caption = 'N.F.'
      TabOrder = 25
    end
    object CkDoc: TCheckBox
      Left = 184
      Top = 366
      Width = 46
      Height = 17
      Caption = 'Doc.'
      TabOrder = 29
    end
    object CkControle: TCheckBox
      Left = 265
      Top = 366
      Width = 61
      Height = 17
      Caption = 'Controle'
      TabOrder = 31
    end
    object EdDebMin: TdmkEdit
      Left = 32
      Top = 330
      Width = 76
      Height = 21
      Alignment = taRightJustify
      TabOrder = 20
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdDebMinExit
    end
    object EdCredMin: TdmkEdit
      Left = 223
      Top = 330
      Width = 76
      Height = 21
      Alignment = taRightJustify
      TabOrder = 23
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdCredMinExit
    end
    object EdNF: TdmkEdit
      Left = 24
      Top = 386
      Width = 76
      Height = 21
      Alignment = taRightJustify
      TabOrder = 26
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 6
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdDoc: TdmkEdit
      Left = 184
      Top = 386
      Width = 76
      Height = 21
      Alignment = taRightJustify
      TabOrder = 30
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 6
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdControle: TdmkEdit
      Left = 265
      Top = 386
      Width = 76
      Height = 21
      Alignment = taRightJustify
      TabOrder = 32
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object RgOrdem: TRadioGroup
      Left = 520
      Top = 4
      Width = 173
      Height = 185
      Caption = ' Ordem dos resultados: '
      ItemIndex = 0
      Items.Strings = (
        'Data, Controle'
        'Controle'
        'Nota Fiscal'
        'Documento'
        'Descri'#231#227'o'
        'D'#233'bito, Cr'#233'dito'
        'Cr'#233'dito, D'#233'bito')
      TabOrder = 37
    end
    object EdDuplicata: TdmkEdit
      Left = 104
      Top = 386
      Width = 76
      Height = 21
      Alignment = taRightJustify
      TabOrder = 28
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object CkDuplicata: TCheckBox
      Left = 104
      Top = 366
      Width = 49
      Height = 17
      Caption = 'Dupl.'
      TabOrder = 27
    end
    object EdDebMax: TdmkEdit
      Left = 114
      Top = 329
      Width = 76
      Height = 21
      Alignment = taRightJustify
      TabOrder = 21
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object EdCredMax: TdmkEdit
      Left = 303
      Top = 330
      Width = 76
      Height = 21
      Alignment = taRightJustify
      TabOrder = 24
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object CkExcelGru: TCheckBox
      Left = 24
      Top = 460
      Width = 381
      Height = 17
      Caption = 'Somente lan'#231'amentos sem contas sazonais.'
      TabOrder = 36
    end
    object EdConta: TdmkEditCB
      Left = 24
      Top = 128
      Width = 45
      Height = 21
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBConta
      IgnoraDBLookupComboBox = False
    end
    object CGCarteiras: TdmkCheckGroup
      Left = 520
      Top = 195
      Width = 173
      Height = 105
      Caption = ' Carteiras: '
      Items.Strings = (
        'Caixa (esp'#233'cie)'
        'Banco (conta corrente)'
        'Emiss'#227'o banc'#225'ria')
      TabOrder = 38
      UpdType = utYes
      Value = 0
      OldValor = 0
    end
    object CkCliInt: TCheckBox
      Left = 24
      Top = 414
      Width = 373
      Height = 17
      Caption = 'Cliente interno:'
      TabOrder = 33
    end
    object EdCliInt: TdmkEditCB
      Left = 24
      Top = 434
      Width = 45
      Height = 21
      Alignment = taRightJustify
      TabOrder = 34
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCliInt
      IgnoraDBLookupComboBox = False
    end
    object CBCliInt: TdmkDBLookupComboBox
      Left = 72
      Top = 434
      Width = 329
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENT'
      ListSource = DsCliInt
      TabOrder = 35
      dmkEditCB = EdCliInt
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object CkVctoIni: TCheckBox
      Left = 24
      Top = 56
      Width = 149
      Height = 17
      Caption = 'Vencimento inicial ou '#250'nico'
      TabOrder = 4
    end
    object TPVctoIni: TdmkEditDateTimePicker
      Left = 24
      Top = 76
      Width = 186
      Height = 21
      CalColors.TextColor = clMenuText
      Date = 37636.777157974500000000
      Time = 37636.777157974500000000
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object CkVctoFim: TCheckBox
      Left = 216
      Top = 56
      Width = 97
      Height = 17
      Caption = 'Vencimento final'
      TabOrder = 6
      OnClick = CkDataFimClick
    end
    object TPVctoFim: TdmkEditDateTimePicker
      Left = 216
      Top = 76
      Width = 186
      Height = 21
      Date = 37636.777203761600000000
      Time = 37636.777203761600000000
      TabOrder = 7
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object CBFornece: TdmkDBLookupComboBox
      Left = 72
      Top = 224
      Width = 329
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENT'
      ListSource = DsFornece
      TabOrder = 15
      OnKeyDown = CBForneceKeyDown
      dmkEditCB = EdFornece
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdFornece: TdmkEditCB
      Left = 24
      Top = 224
      Width = 45
      Height = 21
      Alignment = taRightJustify
      TabOrder = 14
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnKeyDown = EdForneceKeyDown
      DBLookupComboBox = CBFornece
      IgnoraDBLookupComboBox = False
    end
    object CkFornece: TCheckBox
      Left = 24
      Top = 204
      Width = 84
      Height = 17
      Caption = 'Fornecedor'
      TabOrder = 13
    end
    object CkCliente: TCheckBox
      Left = 24
      Top = 253
      Width = 84
      Height = 17
      Caption = 'Cliente'
      TabOrder = 16
    end
    object EdCliente: TdmkEditCB
      Left = 24
      Top = 273
      Width = 45
      Height = 21
      Alignment = taRightJustify
      TabOrder = 17
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnKeyDown = EdClienteKeyDown
      DBLookupComboBox = CBCliente
      IgnoraDBLookupComboBox = False
    end
    object CBCliente: TdmkDBLookupComboBox
      Left = 72
      Top = 273
      Width = 329
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENT'
      ListSource = DsCliente
      TabOrder = 18
      OnKeyDown = CBClienteKeyDown
      dmkEditCB = EdCliente
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 566
    Width = 703
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 16
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
      NumGlyphs = 2
    end
    object Panel1: TPanel
      Left = 573
      Top = 1
      Width = 129
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      Caption = 'Panel1'
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 15
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 703
    Height = 48
    Align = alTop
    Caption = 'Defini'#231#245'es para Localiza'#231#227'o de Lan'#231'amentos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 701
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 712
      ExplicitHeight = 44
    end
  end
  object Painel2: TPanel
    Left = 0
    Top = 48
    Width = 703
    Height = 36
    Align = alTop
    Caption = 'Informe somente os dados que tem certeza!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 3
    object Image2: TImage
      Left = 1
      Top = 1
      Width = 701
      Height = 34
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 712
      ExplicitHeight = 32
    end
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM contas'
      'ORDER BY Nome')
    Left = 8
    Top = 52
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 36
    Top = 52
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 92
    Top = 52
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 64
    Top = 52
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object QrFornece: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 120
    Top = 52
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsFornece: TDataSource
    DataSet = QrFornece
    Left = 148
    Top = 52
  end
  object QrCliente: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 176
    Top = 52
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsCliente: TDataSource
    DataSet = QrCliente
    Left = 204
    Top = 52
  end
end
