object FmLctPrevia: TFmLctPrevia
  Left = 339
  Top = 185
  Caption = 'FIN-LANCT-004 :: Inclus'#227'o Pr'#233'via de Lan'#231'amento'
  ClientHeight = 262
  ClientWidth = 893
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 214
    Width = 893
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 781
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 893
    Height = 48
    Align = alTop
    Caption = 'Inclus'#227'o Pr'#233'via de Lan'#231'amento'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 813
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 814
      Top = 1
      Width = 78
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 712
      ExplicitTop = 2
      ExplicitHeight = 36
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 893
    Height = 166
    Align = alClient
    TabOrder = 0
    object PnLancto1: TPanel
      Left = 1
      Top = 1
      Width = 568
      Height = 164
      Align = alLeft
      TabOrder = 0
      object Label14: TLabel
        Left = 8
        Top = 2
        Width = 62
        Height = 13
        Caption = 'Lan'#231'amento:'
      end
      object SbCarteira: TSpeedButton
        Left = 540
        Top = 18
        Width = 21
        Height = 21
        Hint = 'Inclui item de carteira'
        Caption = '...'
        OnClick = SbCarteiraClick
      end
      object BtContas: TSpeedButton
        Left = 540
        Top = 58
        Width = 21
        Height = 21
        Hint = 'Inclui conta'
        Caption = '...'
        OnClick = BtContasClick
      end
      object Label1: TLabel
        Left = 8
        Top = 42
        Width = 99
        Height = 13
        Caption = 'Data do lan'#231'amento:'
      end
      object Label2: TLabel
        Left = 112
        Top = 42
        Width = 31
        Height = 13
        Caption = 'Conta:'
      end
      object LaDebito: TLabel
        Left = 9
        Top = 82
        Width = 34
        Height = 13
        Caption = 'D'#233'bito:'
        Enabled = False
      end
      object LaCredito: TLabel
        Left = 85
        Top = 82
        Width = 36
        Height = 13
        Caption = 'Cr'#233'dito:'
        Enabled = False
      end
      object Label15: TLabel
        Left = 88
        Top = 2
        Width = 168
        Height = 13
        Caption = 'Carteira (Extrato banc'#225'rio ou caixa):'
      end
      object Label13: TLabel
        Left = 163
        Top = 83
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object EdControle: TdmkEdit
        Left = 8
        Top = 18
        Width = 76
        Height = 21
        Alignment = taRightJustify
        Color = clInactiveCaption
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBackground
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdCarteira: TdmkEditCB
        Left = 88
        Top = 18
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Carteira'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdCarteiraChange
        DBLookupComboBox = CBCarteira
        IgnoraDBLookupComboBox = False
      end
      object CBCarteira: TdmkDBLookupComboBox
        Left = 148
        Top = 18
        Width = 389
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCrts
        TabOrder = 2
        dmkEditCB = EdCarteira
        QryCampo = 'Carteira'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CBGenero: TdmkDBLookupComboBox
        Left = 176
        Top = 58
        Width = 361
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCtas
        TabOrder = 5
        dmkEditCB = EdGenero
        QryCampo = 'Genero'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdGenero: TdmkEditCB
        Left = 117
        Top = 58
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Genero'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdGeneroChange
        DBLookupComboBox = CBGenero
        IgnoraDBLookupComboBox = False
      end
      object TPData: TdmkEditDateTimePicker
        Left = 8
        Top = 58
        Width = 106
        Height = 21
        Date = 39615.655720300900000000
        Time = 39615.655720300900000000
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'Data'
        UpdType = utYes
      end
      object EdDebito: TdmkEdit
        Left = 9
        Top = 98
        Width = 73
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'Debito'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdCredito: TdmkEdit
        Left = 85
        Top = 98
        Width = 73
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'Credito'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdDescricao: TdmkEdit
        Left = 163
        Top = 98
        Width = 398
        Height = 21
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Saldo inicial transferido'
        QryCampo = 'Descricao'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Saldo inicial transferido'
      end
    end
    object Panel11: TPanel
      Left = 569
      Top = 1
      Width = 323
      Height = 164
      Align = alClient
      TabOrder = 1
      object Label51: TLabel
        Left = 4
        Top = 2
        Width = 52
        Height = 13
        Caption = 'Sub-grupo:'
        FocusControl = DBEdit1
      end
      object Label52: TLabel
        Left = 4
        Top = 42
        Width = 32
        Height = 13
        Caption = 'Grupo:'
        FocusControl = DBEdit2
      end
      object Label53: TLabel
        Left = 4
        Top = 82
        Width = 45
        Height = 13
        Caption = 'Conjunto:'
        FocusControl = DBEdit3
      end
      object Label54: TLabel
        Left = 4
        Top = 122
        Width = 30
        Height = 13
        Caption = 'Plano:'
        FocusControl = DBEdit4
      end
      object DBEdit1: TDBEdit
        Left = 4
        Top = 18
        Width = 312
        Height = 21
        DataField = 'NOMESUBGRUPO'
        DataSource = DsCtas
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 4
        Top = 58
        Width = 312
        Height = 21
        DataField = 'NOMEGRUPO'
        DataSource = DsCtas
        TabOrder = 1
      end
      object DBEdit3: TDBEdit
        Left = 4
        Top = 98
        Width = 312
        Height = 21
        DataField = 'NOMECONJUNTO'
        DataSource = DsCtas
        TabOrder = 2
      end
      object DBEdit4: TDBEdit
        Left = 4
        Top = 138
        Width = 312
        Height = 21
        DataField = 'NOMEPLANO'
        DataSource = DsCtas
        TabOrder = 3
      end
    end
  end
  object QrCrts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Fatura, Fechamento, Prazo, '
      'Tipo, ExigeNumCheque, ForneceI'
      'FROM carteiras '
      'WHERE Tipo <> 2'
      'AND ForneceI=:P0'
      'ORDER BY Nome')
    Left = 4
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCrtsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCrtsNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrCrtsFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrCrtsFechamento: TIntegerField
      FieldName = 'Fechamento'
    end
    object QrCrtsPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrCrtsTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCrtsExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
    end
    object QrCrtsForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
  end
  object QrCtas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT co.*, pl.Nome NOMEPLANO, sg.Grupo, '
      'sg.Nome NOMESUBGRUPO, gr.Conjunto,'
      'gr.Nome NOMEGRUPO, cj.Plano, cj.Nome NOMECONJUNTO,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMEEMPRESA'
      'FROM contas co'
      'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo'
      'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano'
      'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa'
      'WHERE co.Terceiro=0'
      'AND co.Codigo>0'
      'ORDER BY co.Nome')
    Left = 60
    Top = 4
    object QrCtasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCtasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrCtasNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object QrCtasNome3: TWideStringField
      FieldName = 'Nome3'
      Required = True
      Size = 50
    end
    object QrCtasID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrCtasSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrCtasEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrCtasCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrCtasDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrCtasMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrCtasExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrCtasMensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrCtasMensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrCtasMensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrCtasMenscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrCtasMensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrCtasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCtasTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrCtasExcel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrCtasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCtasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCtasUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrCtasUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrCtasNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Required = True
      Size = 50
    end
    object QrCtasNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Required = True
      Size = 50
    end
    object QrCtasNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Required = True
      Size = 50
    end
    object QrCtasNOMEEMPRESA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrCtasNOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Size = 50
    end
    object QrCtasGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrCtasConjunto: TIntegerField
      FieldName = 'Conjunto'
    end
    object QrCtasPlano: TIntegerField
      FieldName = 'Plano'
    end
  end
  object DsCtas: TDataSource
    DataSet = QrCtas
    Left = 88
    Top = 4
  end
  object DsCrts: TDataSource
    DataSet = QrCrts
    Left = 32
    Top = 4
  end
  object QrIni: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MIN(Data) Data'
      'FROM lanctos'
      'WHERE CliInt=:P0'
      'AND Carteira=:P1')
    Left = 316
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrIniData: TDateField
      FieldName = 'Data'
      Required = True
    end
  end
end
