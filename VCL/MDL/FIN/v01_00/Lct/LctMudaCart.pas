unit LctMudaCart;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, DBCtrls, ComCtrls,
  Grids, DBGrids, dmkEdit, dmkEditCB, dmkDBLookupComboBox, dmkGeral;

type
  TFmLctMudaCart = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    BtCancela: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    QrCarteirasTipo: TIntegerField;
    Label15: TLabel;
    QrCarteirasForneceI: TIntegerField;
    procedure BtOKClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    SohPermiteTipo1: Boolean;
    FCarteiraTip, FCarteiraSel: Integer;
  end;

  var
  FmLctMudaCart: TFmLctMudaCart;

implementation

{$R *.DFM}

uses UnMyObjects, Module;

procedure TFmLctMudaCart.BtOKClick(Sender: TObject);
begin
  FCarteiraSel := Geral.IMV(EdCarteira.Text);
  FCarteiraTip := QrCarteirasTipo.Value;
  if SohPermiteTipo1 and  (FCarteiraTip <> 1) then
  begin
    Application.MessageBox('Tipo de carteira n�o permitida para quita��o!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Close;
end;

procedure TFmLctMudaCart.BtCancelaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLctMudaCart.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLctMudaCart.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLctMudaCart.FormCreate(Sender: TObject);
begin
  QrCarteiras.Open;
  FCarteiraSel := -1000;
end;

end.
