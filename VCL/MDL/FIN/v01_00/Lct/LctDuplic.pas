unit LctDuplic;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids;

type
  TFmLctDuplic = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    STNotaF: TStaticText;
    STCheque: TStaticText;
    GradeCH: TDBGrid;
    GradeNF: TDBGrid;
    StaticText1: TStaticText;
    GradeDU: TDBGrid;
    BtEstahEmLoop: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtEstahEmLoopClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FConfirma: Integer;
  end;

  var
  FmLctDuplic: TFmLctDuplic;

implementation

uses UnMyObjects, ModuleFin;

{$R *.DFM}

procedure TFmLctDuplic.BtSaidaClick(Sender: TObject);
begin
  FConfirma := 0; // N�o inclui o item atual
  Close;
end;

procedure TFmLctDuplic.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLctDuplic.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLctDuplic.FormCreate(Sender: TObject);
begin
  FConfirma := 0; // N�o inclui o item atual
  GradeCH.DataSource := DModFin.DsDuplCH;
  GradeNF.DataSource := DModFin.DsDuplNF;
  GradeDU.DataSource := DModFin.DsDuplVal;
end;

procedure TFmLctDuplic.BtEstahEmLoopClick(Sender: TObject);
begin
  FConfirma := -1; // Informa para deixar um poss�vel loop de inclus�es!
  Close;
end;

procedure TFmLctDuplic.BtOKClick(Sender: TObject);
begin
  FConfirma := 1; // Confirma a inclus�o
  Close;
end;

end.
