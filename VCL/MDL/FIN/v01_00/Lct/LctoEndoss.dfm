object FmLctoEndoss: TFmLctoEndoss
  Left = 339
  Top = 185
  Caption = 'FIN-ENDOS-001 :: Endosso de Documentos'
  ClientHeight = 494
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Endosso de Documentos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 782
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 446
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 208
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 26
        Height = 13
        Caption = 'Data:'
        FocusControl = DBEdit1
      end
      object Label2: TLabel
        Left = 76
        Top = 4
        Width = 42
        Height = 13
        Caption = 'Controle:'
        FocusControl = DBEdit2
      end
      object Label3: TLabel
        Left = 136
        Top = 4
        Width = 22
        Height = 13
        Caption = 'Sub:'
        FocusControl = DBEdit3
      end
      object Label4: TLabel
        Left = 168
        Top = 4
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
        FocusControl = DBEdit4
      end
      object Label5: TLabel
        Left = 232
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Hist'#243'rico:'
        FocusControl = DBEdit5
      end
      object Label6: TLabel
        Left = 480
        Top = 44
        Width = 53
        Height = 13
        Caption = 'Nota fiscal:'
        FocusControl = DBEdit6
      end
      object Label7: TLabel
        Left = 540
        Top = 44
        Width = 51
        Height = 13
        Caption = 'S'#233'rie doc.:'
        FocusControl = DBEdit7
      end
      object Label8: TLabel
        Left = 624
        Top = 44
        Width = 58
        Height = 13
        Caption = 'Documento:'
        FocusControl = DBEdit8
      end
      object Label9: TLabel
        Left = 708
        Top = 44
        Width = 59
        Height = 13
        Caption = 'Vencimento:'
        FocusControl = DBEdit9
      end
      object Label10: TLabel
        Left = 8
        Top = 44
        Width = 61
        Height = 13
        Caption = 'Identificador:'
        FocusControl = DBEdit10
      end
      object Label11: TLabel
        Left = 72
        Top = 44
        Width = 36
        Height = 13
        Caption = 'Sub ID:'
        FocusControl = DBEdit11
      end
      object Label12: TLabel
        Left = 136
        Top = 44
        Width = 41
        Height = 13
        Caption = 'Gerador:'
        FocusControl = DBEdit12
      end
      object Label13: TLabel
        Left = 200
        Top = 44
        Width = 39
        Height = 13
        Caption = 'Parcela:'
        FocusControl = DBEdit13
      end
      object Label14: TLabel
        Left = 8
        Top = 84
        Width = 54
        Height = 13
        Caption = 'Fornecedor'
        FocusControl = DBEdit14
      end
      object Label15: TLabel
        Left = 388
        Top = 84
        Width = 32
        Height = 13
        Caption = 'Cliente'
        FocusControl = DBEdit15
      end
      object Label16: TLabel
        Left = 8
        Top = 124
        Width = 23
        Height = 13
        Caption = 'CliInt'
        FocusControl = DBEdit16
      end
      object Label17: TLabel
        Left = 264
        Top = 44
        Width = 48
        Height = 13
        Caption = 'Duplicata:'
        FocusControl = DBEdit17
      end
      object Label18: TLabel
        Left = 72
        Top = 124
        Width = 32
        Height = 13
        Caption = 'Depto:'
        FocusControl = DBEdit18
      end
      object Label19: TLabel
        Left = 404
        Top = 44
        Width = 44
        Height = 13
        Caption = 'S'#233'rie NF:'
        FocusControl = DBEdit19
      end
      object Label20: TLabel
        Left = 128
        Top = 126
        Width = 31
        Height = 13
        Caption = 'Conta:'
      end
      object Label21: TLabel
        Left = 452
        Top = 126
        Width = 52
        Height = 13
        Caption = 'Sub-grupo:'
      end
      object Label22: TLabel
        Left = 8
        Top = 166
        Width = 32
        Height = 13
        Caption = 'Grupo:'
      end
      object Label23: TLabel
        Left = 308
        Top = 166
        Width = 45
        Height = 13
        Caption = 'Conjunto:'
      end
      object Label24: TLabel
        Left = 608
        Top = 164
        Width = 34
        Height = 13
        Caption = 'D'#233'bito:'
        FocusControl = DBEdit22
      end
      object Label25: TLabel
        Left = 692
        Top = 164
        Width = 36
        Height = 13
        Caption = 'Cr'#233'dito:'
        FocusControl = DBEdit23
      end
      object DBEdit1: TDBEdit
        Left = 8
        Top = 20
        Width = 64
        Height = 21
        DataField = 'Data'
        DataSource = DsLct
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 76
        Top = 20
        Width = 56
        Height = 21
        DataField = 'Controle'
        DataSource = DsLct
        TabOrder = 1
      end
      object DBEdit3: TDBEdit
        Left = 136
        Top = 20
        Width = 28
        Height = 21
        DataField = 'Sub'
        DataSource = DsLct
        TabOrder = 2
      end
      object DBEdit4: TDBEdit
        Left = 168
        Top = 20
        Width = 60
        Height = 21
        DataField = 'Qtde'
        DataSource = DsLct
        TabOrder = 3
      end
      object DBEdit5: TDBEdit
        Left = 232
        Top = 20
        Width = 541
        Height = 21
        DataField = 'Descricao'
        DataSource = DsLct
        TabOrder = 4
      end
      object DBEdit6: TDBEdit
        Left = 480
        Top = 60
        Width = 56
        Height = 21
        DataField = 'NotaFiscal'
        DataSource = DsLct
        TabOrder = 5
      end
      object DBEdit7: TDBEdit
        Left = 540
        Top = 60
        Width = 80
        Height = 21
        DataField = 'SerieCH'
        DataSource = DsLct
        TabOrder = 6
      end
      object DBEdit8: TDBEdit
        Left = 624
        Top = 60
        Width = 80
        Height = 21
        DataField = 'Documento'
        DataSource = DsLct
        TabOrder = 7
      end
      object DBEdit9: TDBEdit
        Left = 708
        Top = 60
        Width = 64
        Height = 21
        DataField = 'Vencimento'
        DataSource = DsLct
        TabOrder = 8
      end
      object DBEdit10: TDBEdit
        Left = 8
        Top = 60
        Width = 60
        Height = 21
        DataField = 'FatID'
        DataSource = DsLct
        TabOrder = 9
      end
      object DBEdit11: TDBEdit
        Left = 72
        Top = 60
        Width = 60
        Height = 21
        DataField = 'FatID_Sub'
        DataSource = DsLct
        TabOrder = 10
      end
      object DBEdit12: TDBEdit
        Left = 136
        Top = 60
        Width = 60
        Height = 21
        DataField = 'FatNum'
        DataSource = DsLct
        TabOrder = 11
      end
      object DBEdit13: TDBEdit
        Left = 200
        Top = 60
        Width = 60
        Height = 21
        DataField = 'FatParcela'
        DataSource = DsLct
        TabOrder = 12
      end
      object DBEdit14: TDBEdit
        Left = 8
        Top = 100
        Width = 60
        Height = 21
        DataField = 'Fornecedor'
        DataSource = DsLct
        TabOrder = 13
      end
      object DBEdit15: TDBEdit
        Left = 388
        Top = 100
        Width = 60
        Height = 21
        DataField = 'Cliente'
        DataSource = DsLct
        TabOrder = 14
      end
      object DBEdit16: TDBEdit
        Left = 8
        Top = 140
        Width = 60
        Height = 21
        DataField = 'CliInt'
        DataSource = DsLct
        TabOrder = 15
      end
      object DBEdit17: TDBEdit
        Left = 264
        Top = 60
        Width = 136
        Height = 21
        DataField = 'Duplicata'
        DataSource = DsLct
        TabOrder = 16
      end
      object DBEdit18: TDBEdit
        Left = 72
        Top = 140
        Width = 53
        Height = 21
        DataField = 'Depto'
        DataSource = DsLct
        TabOrder = 17
      end
      object DBEdit19: TDBEdit
        Left = 404
        Top = 60
        Width = 72
        Height = 21
        DataField = 'SerieNF'
        DataSource = DsLct
        TabOrder = 18
      end
      object DBEdit01: TDBEdit
        Left = 128
        Top = 140
        Width = 320
        Height = 21
        DataField = 'NOMECONTA'
        DataSource = DsLct
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 19
      end
      object DBEdit02: TDBEdit
        Left = 452
        Top = 140
        Width = 320
        Height = 21
        DataField = 'NOMESUBGRUPO'
        DataSource = DsLct
        TabOrder = 20
      end
      object DBEdit03: TDBEdit
        Left = 8
        Top = 180
        Width = 296
        Height = 21
        DataField = 'NOMEGRUPO'
        DataSource = DsLct
        TabOrder = 21
      end
      object DBEdit04: TDBEdit
        Left = 308
        Top = 180
        Width = 296
        Height = 21
        DataField = 'NOMECONJUNTO'
        DataSource = DsLct
        TabOrder = 22
      end
      object DBEdit20: TDBEdit
        Left = 452
        Top = 100
        Width = 321
        Height = 21
        DataField = 'NOMECLIENTE'
        DataSource = DsLct
        TabOrder = 23
      end
      object DBEdit21: TDBEdit
        Left = 72
        Top = 100
        Width = 313
        Height = 21
        DataField = 'NOMEFORNECEDOR'
        DataSource = DsLct
        TabOrder = 24
      end
      object DBEdit22: TDBEdit
        Left = 608
        Top = 180
        Width = 80
        Height = 21
        DataField = 'Debito'
        DataSource = DsLct
        TabOrder = 25
      end
      object DBEdit23: TDBEdit
        Left = 692
        Top = 180
        Width = 80
        Height = 21
        DataField = 'Credito'
        DataSource = DsLct
        TabOrder = 26
      end
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 209
      Width = 782
      Height = 236
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = 'Endossantes'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object dmkDBGLct: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 774
          Height = 160
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'NOMERELACIONADO'
              Title.Caption = 'Cliente / Fornecedor'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID'
              Title.Caption = 'ID endosso'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Title.Caption = 'Val. endosso'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OriCtrl'
              Title.Caption = 'Ctrl origem'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OriSub'
              Title.Caption = 'Sub origem'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatNum'
              Title.Caption = 'Bloqueto'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatID'
              Title.Caption = 'Tipo'
              Width = 29
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_Pgto'
              Title.Caption = 'ID Pagto'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SERIE_CHEQUE'
              Title.Caption = 'S'#233'rie/Docum.'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Duplicata'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 164
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Caption = 'D'#233'bito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Caption = 'Cr'#233'dito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencim.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COMPENSADO_TXT'
              Title.Caption = 'Compen.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MENSAL'
              Title.Caption = 'M'#234's'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESIT'
              Title.Caption = 'Situa'#231#227'o'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Lan'#231'to'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sub'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NotaFiscal'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Caption = 'Quantidade'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEFORNECEI'
              Title.Caption = 'Fonte pagadora'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MultaVal'
              Title.Caption = 'Multa paga'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MoraVal'
              Title.Caption = 'Juros pagos'
              Width = 56
              Visible = True
            end>
          Color = clWindow
          DataSource = DsLctoEndossaN
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          FieldsCalcToOrder.Strings = (
            'NOMESIT=Sit,Vencimento'
            'SERIE_CHEQUE=SerieCH,Documento'
            'COMPENSADO_TXT=Compensado'
            'MENSAL=Mez')
          Columns = <
            item
              Expanded = False
              FieldName = 'NOMERELACIONADO'
              Title.Caption = 'Cliente / Fornecedor'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID'
              Title.Caption = 'ID endosso'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Title.Caption = 'Val. endosso'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OriCtrl'
              Title.Caption = 'Ctrl origem'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OriSub'
              Title.Caption = 'Sub origem'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatNum'
              Title.Caption = 'Bloqueto'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatID'
              Title.Caption = 'Tipo'
              Width = 29
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_Pgto'
              Title.Caption = 'ID Pagto'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SERIE_CHEQUE'
              Title.Caption = 'S'#233'rie/Docum.'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Duplicata'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 164
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Caption = 'D'#233'bito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Caption = 'Cr'#233'dito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencim.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COMPENSADO_TXT'
              Title.Caption = 'Compen.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MENSAL'
              Title.Caption = 'M'#234's'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESIT'
              Title.Caption = 'Situa'#231#227'o'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Lan'#231'to'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sub'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NotaFiscal'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Caption = 'Quantidade'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEFORNECEI'
              Title.Caption = 'Fonte pagadora'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MultaVal'
              Title.Caption = 'Multa paga'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MoraVal'
              Title.Caption = 'Juros pagos'
              Width = 56
              Visible = True
            end>
        end
        object PainelConfirma: TPanel
          Left = 0
          Top = 160
          Width = 774
          Height = 48
          Align = alBottom
          ParentBackground = False
          TabOrder = 1
          object Panel2: TPanel
            Left = 662
            Top = 1
            Width = 111
            Height = 46
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 0
            object BtSaida: TBitBtn
              Tag = 13
              Left = 2
              Top = 3
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Hint = 'Sai da janela atual'
              Caption = '&Sa'#237'da'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaidaClick
            end
          end
          object BtInclui: TBitBtn
            Tag = 10
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Inclui'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtIncluiClick
          end
          object BtAltera: TBitBtn
            Tag = 11
            Left = 96
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Altera'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtAlteraClick
          end
          object BtExclui: TBitBtn
            Tag = 12
            Left = 188
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Exclui'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnClick = BtExcluiClick
          end
          object BtRefresh: TBitBtn
            Tag = 18
            Left = 281
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Atualiza'
            NumGlyphs = 2
            TabOrder = 4
            OnClick = BtRefreshClick
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Endossados'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object dmkDBGrid1: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 774
          Height = 208
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'NOMERELACIONADO'
              Title.Caption = 'Cliente / Fornecedor'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID'
              Title.Caption = 'ID endosso'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Title.Caption = 'Val. endosso'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OriCtrl'
              Title.Caption = 'Ctrl origem'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OriSub'
              Title.Caption = 'Sub origem'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatNum'
              Title.Caption = 'Bloqueto'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatID'
              Title.Caption = 'Tipo'
              Width = 29
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_Pgto'
              Title.Caption = 'ID Pagto'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SERIE_CHEQUE'
              Title.Caption = 'S'#233'rie/Docum.'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Duplicata'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 164
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Caption = 'D'#233'bito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Caption = 'Cr'#233'dito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencim.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COMPENSADO_TXT'
              Title.Caption = 'Compen.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MENSAL'
              Title.Caption = 'M'#234's'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESIT'
              Title.Caption = 'Situa'#231#227'o'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Lan'#231'to'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sub'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NotaFiscal'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Caption = 'Quantidade'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEFORNECEI'
              Title.Caption = 'Fonte pagadora'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MultaVal'
              Title.Caption = 'Multa paga'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MoraVal'
              Title.Caption = 'Juros pagos'
              Width = 56
              Visible = True
            end>
          Color = clWindow
          DataSource = DsLctoEndossaD
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          FieldsCalcToOrder.Strings = (
            'NOMESIT=Sit,Vencimento'
            'SERIE_CHEQUE=SerieCH,Documento'
            'COMPENSADO_TXT=Compensado'
            'MENSAL=Mez')
          Columns = <
            item
              Expanded = False
              FieldName = 'NOMERELACIONADO'
              Title.Caption = 'Cliente / Fornecedor'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID'
              Title.Caption = 'ID endosso'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Title.Caption = 'Val. endosso'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OriCtrl'
              Title.Caption = 'Ctrl origem'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OriSub'
              Title.Caption = 'Sub origem'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatNum'
              Title.Caption = 'Bloqueto'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatID'
              Title.Caption = 'Tipo'
              Width = 29
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_Pgto'
              Title.Caption = 'ID Pagto'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SERIE_CHEQUE'
              Title.Caption = 'S'#233'rie/Docum.'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Duplicata'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 164
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Caption = 'D'#233'bito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Caption = 'Cr'#233'dito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencim.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COMPENSADO_TXT'
              Title.Caption = 'Compen.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MENSAL'
              Title.Caption = 'M'#234's'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESIT'
              Title.Caption = 'Situa'#231#227'o'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Lan'#231'to'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sub'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NotaFiscal'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Caption = 'Quantidade'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEFORNECEI'
              Title.Caption = 'Fonte pagadora'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MultaVal'
              Title.Caption = 'Multa paga'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MoraVal'
              Title.Caption = 'Juros pagos'
              Width = 56
              Visible = True
            end>
        end
      end
    end
  end
  object QrLct: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2, '
      '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, '
      'la.*, ct.Codigo CONTA, ca.Prazo,'
      'ca.Banco1, ca.Agencia1, ca.Conta1, '
      'ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial '
      'ELSE em.Nome END NOMEEMPRESA, '
      'CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial '
      'ELSE cl.Nome END NOMECLIENTE,'
      'CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial '
      'ELSE fo.Nome END NOMEFORNECEDOR,'
      'CASE 1 WHEN fi.Tipo=0 THEN fi.RazaoSocial '
      'ELSE fi.Nome END NOMEFORNECEI'
      'FROM lanctos la'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      
        'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = ' +
        '0'
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo'
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'LEFT JOIN entidades fi ON fi.Codigo=la.ForneceI'
      'WHERE la.Controle=:P0'
      'AND la.Sub=:P1'
      'ORDER BY la.Data, la.Controle')
    Left = 4
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLctMes2: TLargeintField
      FieldName = 'Mes2'
      Required = True
    end
    object QrLctAno: TFloatField
      FieldName = 'Ano'
    end
    object QrLctData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrLctTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrLctCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLctSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrLctAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrLctGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLctQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLctNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrLctDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrLctSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrLctDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrLctSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLctVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrLctFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLctFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLctID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrLctID_Quit: TIntegerField
      FieldName = 'ID_Quit'
      Required = True
    end
    object QrLctID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrLctFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrLctEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrLctBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLctAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrLctContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrLctCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrLctLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrLctCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrLctLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrLctOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrLctLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrLctPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLctMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLctCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLctCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLctForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLctMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLctMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLctMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrLctMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrLctProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrLctDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLctCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrLctNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrLctVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLctAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrLctICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrLctICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrLctDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrLctDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLctDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrLctDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrLctDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Required = True
    end
    object QrLctUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrLctNFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrLctAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrLctExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrLctDoc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrLctCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
    end
    object QrLctTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Required = True
    end
    object QrLctReparcel: TIntegerField
      FieldName = 'Reparcel'
      Required = True
    end
    object QrLctAtrelado: TIntegerField
      FieldName = 'Atrelado'
      Required = True
    end
    object QrLctPagMul: TFloatField
      FieldName = 'PagMul'
      Required = True
    end
    object QrLctPagJur: TFloatField
      FieldName = 'PagJur'
      Required = True
    end
    object QrLctSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
      Required = True
    end
    object QrLctMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
    end
    object QrLctProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrLctLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLctDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLctDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLctUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLctUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLctAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrLctAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrLctSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 5
    end
    object QrLctCONTA: TIntegerField
      FieldName = 'CONTA'
      Required = True
    end
    object QrLctPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrLctBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrLctAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrLctConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrLctNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrLctNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 50
    end
    object QrLctNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 50
    end
    object QrLctNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Size = 50
    end
    object QrLctNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrLctNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrLctNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      Size = 100
    end
    object QrLctNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
  end
  object DsLct: TDataSource
    DataSet = QrLct
    Left = 32
    Top = 8
  end
  object QrLctoEndossaN: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLctoEndossaNCalcFields
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2, '
      '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, '
      'la.*, ct.Codigo CONTA, ca.Prazo,'
      'ca.Banco1, ca.Agencia1, ca.Conta1, '
      'ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial '
      'ELSE em.Nome END NOMEEMPRESA, '
      'CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial '
      'ELSE cl.Nome END NOMECLIENTE,'
      'CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial '
      'ELSE fo.Nome END NOMEFORNECEDOR,'
      'CASE 1 WHEN fi.Tipo=0 THEN fi.RazaoSocial '
      'ELSE fi.Nome END NOMEFORNECEI,'
      'len.ID, len.Valor, len.OriCtrl, len.OriSub'
      'FROM lanctos la'
      
        'LEFT JOIN lctoendoss len ON len.OriCtrl=la.Controle AND len.OriS' +
        'ub=la.Sub'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      
        'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = ' +
        '0'
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo'
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'LEFT JOIN entidades fi ON fi.Codigo=la.ForneceI'
      'WHERE len.Controle=:P0'
      'AND len.Sub=:P1'
      'ORDER BY la.Data, la.Controle')
    Left = 64
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLctoEndossaNData: TDateField
      FieldName = 'Data'
      Origin = 'lanctos.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctoEndossaNTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'lanctos.Tipo'
    end
    object QrLctoEndossaNCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'lanctos.Carteira'
    end
    object QrLctoEndossaNAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      Origin = 'lanctos.Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrLctoEndossaNGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'lanctos.Genero'
    end
    object QrLctoEndossaNDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'lanctos.Descricao'
      Size = 25
    end
    object QrLctoEndossaNNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'lanctos.NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrLctoEndossaNDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'lanctos.Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctoEndossaNCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'lanctos.Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctoEndossaNCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'lanctos.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctoEndossaNDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'lanctos.Documento'
      DisplayFormat = '000000; ; '
    end
    object QrLctoEndossaNSit: TIntegerField
      FieldName = 'Sit'
      Origin = 'lanctos.Sit'
    end
    object QrLctoEndossaNVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'lanctos.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctoEndossaNLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'lanctos.Lk'
    end
    object QrLctoEndossaNFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'lanctos.FatID'
    end
    object QrLctoEndossaNFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'lanctos.FatParcela'
    end
    object QrLctoEndossaNCONTA: TIntegerField
      FieldName = 'CONTA'
      Origin = 'contas.Codigo'
    end
    object QrLctoEndossaNNOMECONTA: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      FixedChar = True
      Size = 50
    end
    object QrLctoEndossaNNOMEEMPRESA: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMEEMPRESA'
      Origin = 'NOMEEMPRESA'
      Size = 100
    end
    object QrLctoEndossaNNOMESUBGRUPO: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMESUBGRUPO'
      Origin = 'subgrupos.Nome'
      Size = 50
    end
    object QrLctoEndossaNNOMEGRUPO: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMEGRUPO'
      Origin = 'grupos.Nome'
      Size = 50
    end
    object QrLctoEndossaNNOMECONJUNTO: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMECONJUNTO'
      Origin = 'conjuntos.Nome'
      Size = 50
    end
    object QrLctoEndossaNNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrLctoEndossaNAno: TFloatField
      FieldName = 'Ano'
      Origin = 'Ano'
    end
    object QrLctoEndossaNMENSAL: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrLctoEndossaNMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrLctoEndossaNBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'lanctos.Banco'
    end
    object QrLctoEndossaNLocal: TIntegerField
      FieldName = 'Local'
      Origin = 'lanctos.Local'
    end
    object QrLctoEndossaNFatura: TWideStringField
      FieldName = 'Fatura'
      Origin = 'lanctos.Fatura'
      FixedChar = True
      Size = 1
    end
    object QrLctoEndossaNSub: TSmallintField
      FieldName = 'Sub'
      Origin = 'lanctos.Sub'
      DisplayFormat = '00; ; '
    end
    object QrLctoEndossaNCartao: TIntegerField
      FieldName = 'Cartao'
      Origin = 'lanctos.Cartao'
    end
    object QrLctoEndossaNLinha: TIntegerField
      FieldName = 'Linha'
      Origin = 'lanctos.Linha'
    end
    object QrLctoEndossaNPago: TFloatField
      FieldName = 'Pago'
      Origin = 'lanctos.Pago'
    end
    object QrLctoEndossaNSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrLctoEndossaNID_Sub: TSmallintField
      FieldName = 'ID_Sub'
      Origin = 'lanctos.ID_Sub'
    end
    object QrLctoEndossaNMez: TIntegerField
      FieldName = 'Mez'
      Origin = 'lanctos.Mez'
    end
    object QrLctoEndossaNFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Origin = 'lanctos.Fornecedor'
    end
    object QrLctoEndossaNcliente: TIntegerField
      FieldName = 'cliente'
      Origin = 'lanctos.Cliente'
    end
    object QrLctoEndossaNMoraDia: TFloatField
      FieldName = 'MoraDia'
      Origin = 'lanctos.MoraDia'
    end
    object QrLctoEndossaNNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Origin = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrLctoEndossaNNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      Origin = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrLctoEndossaNTIPOEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrLctoEndossaNNOMERELACIONADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMERELACIONADO'
      Size = 50
      Calculated = True
    end
    object QrLctoEndossaNOperCount: TIntegerField
      FieldName = 'OperCount'
      Origin = 'lanctos.OperCount'
    end
    object QrLctoEndossaNLancto: TIntegerField
      FieldName = 'Lancto'
      Origin = 'lanctos.Lancto'
    end
    object QrLctoEndossaNMulta: TFloatField
      FieldName = 'Multa'
      Origin = 'lanctos.Multa'
    end
    object QrLctoEndossaNATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrLctoEndossaNJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLctoEndossaNDataDoc: TDateField
      FieldName = 'DataDoc'
      Origin = 'lanctos.DataDoc'
    end
    object QrLctoEndossaNNivel: TIntegerField
      FieldName = 'Nivel'
      Origin = 'lanctos.Nivel'
    end
    object QrLctoEndossaNVendedor: TIntegerField
      FieldName = 'Vendedor'
      Origin = 'lanctos.Vendedor'
    end
    object QrLctoEndossaNAccount: TIntegerField
      FieldName = 'Account'
      Origin = 'lanctos.Account'
    end
    object QrLctoEndossaNMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
      Origin = 'Mes2'
    end
    object QrLctoEndossaNProtesto: TDateField
      FieldName = 'Protesto'
      Origin = 'lanctos.Protesto'
    end
    object QrLctoEndossaNDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'lanctos.DataCad'
    end
    object QrLctoEndossaNDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'lanctos.DataAlt'
    end
    object QrLctoEndossaNUserCad: TSmallintField
      FieldName = 'UserCad'
      Origin = 'lanctos.UserCad'
    end
    object QrLctoEndossaNUserAlt: TSmallintField
      FieldName = 'UserAlt'
      Origin = 'lanctos.UserAlt'
    end
    object QrLctoEndossaNControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'lanctos.Controle'
      Required = True
    end
    object QrLctoEndossaNID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Origin = 'lanctos.ID_Pgto'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrLctoEndossaNCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Origin = 'lanctos.CtrlIni'
      Required = True
    end
    object QrLctoEndossaNFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
      Origin = 'lanctos.FatID_Sub'
    end
    object QrLctoEndossaNICMS_P: TFloatField
      FieldName = 'ICMS_P'
      Origin = 'lanctos.ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctoEndossaNICMS_V: TFloatField
      FieldName = 'ICMS_V'
      Origin = 'lanctos.ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctoEndossaNDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Origin = 'lanctos.Duplicata'
      Size = 10
    end
    object QrLctoEndossaNCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrLctoEndossaNCliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'lanctos.CliInt'
    end
    object QrLctoEndossaNDepto: TIntegerField
      FieldName = 'Depto'
      Origin = 'lanctos.Depto'
    end
    object QrLctoEndossaNDescoPor: TIntegerField
      FieldName = 'DescoPor'
      Origin = 'lanctos.DescoPor'
    end
    object QrLctoEndossaNPrazo: TSmallintField
      FieldName = 'Prazo'
      Origin = 'carteiras.Prazo'
    end
    object QrLctoEndossaNForneceI: TIntegerField
      FieldName = 'ForneceI'
      Origin = 'lanctos.ForneceI'
    end
    object QrLctoEndossaNQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'lanctos.Qtde'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrLctoEndossaNEmitente: TWideStringField
      FieldName = 'Emitente'
      Origin = 'lanctos.Emitente'
      Size = 30
    end
    object QrLctoEndossaNAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrLctoEndossaNContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Origin = 'lanctos.ContaCorrente'
      Size = 15
    end
    object QrLctoEndossaNCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Origin = 'lanctos.CNPJCPF'
      Size = 15
    end
    object QrLctoEndossaNDescoVal: TFloatField
      FieldName = 'DescoVal'
      Origin = 'lanctos.DescoVal'
    end
    object QrLctoEndossaNDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Origin = 'lanctos.DescoControle'
      Required = True
    end
    object QrLctoEndossaNUnidade: TIntegerField
      FieldName = 'Unidade'
      Origin = 'lanctos.Unidade'
      Required = True
    end
    object QrLctoEndossaNNFVal: TFloatField
      FieldName = 'NFVal'
      Origin = 'lanctos.NFVal'
      Required = True
    end
    object QrLctoEndossaNAntigo: TWideStringField
      FieldName = 'Antigo'
      Origin = 'lanctos.Antigo'
    end
    object QrLctoEndossaNExcelGru: TIntegerField
      FieldName = 'ExcelGru'
      Origin = 'lanctos.ExcelGru'
    end
    object QrLctoEndossaNSerieCH: TWideStringField
      DisplayWidth = 10
      FieldName = 'SerieCH'
      Origin = 'lanctos.SerieCH'
      Size = 10
    end
    object QrLctoEndossaNSERIE_CHEQUE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SERIE_CHEQUE'
      Calculated = True
    end
    object QrLctoEndossaNDoc2: TWideStringField
      FieldName = 'Doc2'
      Origin = 'lanctos.Doc2'
    end
    object QrLctoEndossaNNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Origin = 'NOMEFORNECEI'
      Size = 100
    end
    object QrLctoEndossaNMoraVal: TFloatField
      FieldName = 'MoraVal'
      Origin = 'lanctos.MoraVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctoEndossaNMultaVal: TFloatField
      FieldName = 'MultaVal'
      Origin = 'lanctos.MultaVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctoEndossaNCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
      Origin = 'lanctos.CNAB_Sit'
    end
    object QrLctoEndossaNBanco1: TIntegerField
      FieldName = 'Banco1'
      Origin = 'carteiras.Banco1'
    end
    object QrLctoEndossaNAgencia1: TIntegerField
      FieldName = 'Agencia1'
      Origin = 'carteiras.Agencia1'
    end
    object QrLctoEndossaNConta1: TWideStringField
      FieldName = 'Conta1'
      Origin = 'carteiras.Conta1'
      Size = 15
    end
    object QrLctoEndossaNTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Origin = 'lanctos.TipoCH'
      Required = True
    end
    object QrLctoEndossaNFatNum: TFloatField
      FieldName = 'FatNum'
      Origin = 'lanctos.FatNum'
    end
    object QrLctoEndossaNSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Origin = 'lanctos.SerieNF'
      Size = 5
    end
    object QrLctoEndossaNID: TIntegerField
      FieldName = 'ID'
    end
    object QrLctoEndossaNValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLctoEndossaNOriCtrl: TIntegerField
      FieldName = 'OriCtrl'
    end
    object QrLctoEndossaNOriSub: TIntegerField
      FieldName = 'OriSub'
    end
  end
  object DsLctoEndossaN: TDataSource
    DataSet = QrLctoEndossaN
    Left = 92
    Top = 8
  end
  object QrLE2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Sub, ID, OriCtrl, OriSub '
      'FROM lctoendoss'
      'WHERE Controle=:P0'
      'AND Sub=:P1'
      '')
    Left = 124
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLE2Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLE2Sub: TIntegerField
      FieldName = 'Sub'
    end
    object QrLE2ID: TIntegerField
      FieldName = 'ID'
    end
    object QrLE2OriCtrl: TIntegerField
      FieldName = 'OriCtrl'
    end
    object QrLE2OriSub: TIntegerField
      FieldName = 'OriSub'
    end
  end
  object QrLctoEndossaD: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLctoEndossaDCalcFields
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2, '
      '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, '
      'la.*, ct.Codigo CONTA, ca.Prazo,'
      'ca.Banco1, ca.Agencia1, ca.Conta1, '
      'ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial '
      'ELSE em.Nome END NOMEEMPRESA, '
      'CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial '
      'ELSE cl.Nome END NOMECLIENTE,'
      'CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial '
      'ELSE fo.Nome END NOMEFORNECEDOR,'
      'CASE 1 WHEN fi.Tipo=0 THEN fi.RazaoSocial '
      'ELSE fi.Nome END NOMEFORNECEI,'
      'len.ID, len.Valor, len.OriCtrl, len.OriSub'
      'FROM lanctos la'
      
        'LEFT JOIN lctoendoss len ON len.Controle=la.Controle AND len.Sub' +
        '=la.Sub'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      
        'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = ' +
        '0'
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo'
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'LEFT JOIN entidades fi ON fi.Codigo=la.ForneceI'
      'WHERE len.OriCtrl=:P0'
      'AND len.OriSub=:P1'
      'ORDER BY la.Data, la.Controle')
    Left = 156
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLctoEndossaDData: TDateField
      FieldName = 'Data'
      Origin = 'lanctos.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctoEndossaDTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'lanctos.Tipo'
    end
    object QrLctoEndossaDCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'lanctos.Carteira'
    end
    object QrLctoEndossaDAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      Origin = 'lanctos.Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrLctoEndossaDGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'lanctos.Genero'
    end
    object QrLctoEndossaDDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'lanctos.Descricao'
      Size = 25
    end
    object QrLctoEndossaDNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'lanctos.NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrLctoEndossaDDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'lanctos.Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctoEndossaDCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'lanctos.Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctoEndossaDCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'lanctos.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctoEndossaDDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'lanctos.Documento'
      DisplayFormat = '000000; ; '
    end
    object QrLctoEndossaDSit: TIntegerField
      FieldName = 'Sit'
      Origin = 'lanctos.Sit'
    end
    object QrLctoEndossaDVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'lanctos.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctoEndossaDLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'lanctos.Lk'
    end
    object QrLctoEndossaDFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'lanctos.FatID'
    end
    object QrLctoEndossaDFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'lanctos.FatParcela'
    end
    object QrLctoEndossaDCONTA: TIntegerField
      FieldName = 'CONTA'
      Origin = 'contas.Codigo'
    end
    object QrLctoEndossaDNOMECONTA: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      FixedChar = True
      Size = 50
    end
    object QrLctoEndossaDNOMEEMPRESA: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMEEMPRESA'
      Origin = 'NOMEEMPRESA'
      Size = 100
    end
    object QrLctoEndossaDNOMESUBGRUPO: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMESUBGRUPO'
      Origin = 'subgrupos.Nome'
      Size = 50
    end
    object QrLctoEndossaDNOMEGRUPO: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMEGRUPO'
      Origin = 'grupos.Nome'
      Size = 50
    end
    object QrLctoEndossaDNOMECONJUNTO: TWideStringField
      DisplayWidth = 128
      FieldName = 'NOMECONJUNTO'
      Origin = 'conjuntos.Nome'
      Size = 50
    end
    object QrLctoEndossaDNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrLctoEndossaDAno: TFloatField
      FieldName = 'Ano'
      Origin = 'Ano'
    end
    object QrLctoEndossaDMENSAL: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrLctoEndossaDMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrLctoEndossaDBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'lanctos.Banco'
    end
    object QrLctoEndossaDLocal: TIntegerField
      FieldName = 'Local'
      Origin = 'lanctos.Local'
    end
    object QrLctoEndossaDFatura: TWideStringField
      FieldName = 'Fatura'
      Origin = 'lanctos.Fatura'
      FixedChar = True
      Size = 1
    end
    object QrLctoEndossaDSub: TSmallintField
      FieldName = 'Sub'
      Origin = 'lanctos.Sub'
      DisplayFormat = '00; ; '
    end
    object QrLctoEndossaDCartao: TIntegerField
      FieldName = 'Cartao'
      Origin = 'lanctos.Cartao'
    end
    object QrLctoEndossaDLinha: TIntegerField
      FieldName = 'Linha'
      Origin = 'lanctos.Linha'
    end
    object QrLctoEndossaDPago: TFloatField
      FieldName = 'Pago'
      Origin = 'lanctos.Pago'
    end
    object QrLctoEndossaDSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrLctoEndossaDID_Sub: TSmallintField
      FieldName = 'ID_Sub'
      Origin = 'lanctos.ID_Sub'
    end
    object QrLctoEndossaDMez: TIntegerField
      FieldName = 'Mez'
      Origin = 'lanctos.Mez'
    end
    object QrLctoEndossaDFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Origin = 'lanctos.Fornecedor'
    end
    object QrLctoEndossaDcliente: TIntegerField
      FieldName = 'cliente'
      Origin = 'lanctos.Cliente'
    end
    object QrLctoEndossaDMoraDia: TFloatField
      FieldName = 'MoraDia'
      Origin = 'lanctos.MoraDia'
    end
    object QrLctoEndossaDNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Origin = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrLctoEndossaDNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      Origin = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrLctoEndossaDTIPOEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrLctoEndossaDNOMERELACIONADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMERELACIONADO'
      Size = 50
      Calculated = True
    end
    object QrLctoEndossaDOperCount: TIntegerField
      FieldName = 'OperCount'
      Origin = 'lanctos.OperCount'
    end
    object QrLctoEndossaDLancto: TIntegerField
      FieldName = 'Lancto'
      Origin = 'lanctos.Lancto'
    end
    object QrLctoEndossaDMulta: TFloatField
      FieldName = 'Multa'
      Origin = 'lanctos.Multa'
    end
    object QrLctoEndossaDATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrLctoEndossaDJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLctoEndossaDDataDoc: TDateField
      FieldName = 'DataDoc'
      Origin = 'lanctos.DataDoc'
    end
    object QrLctoEndossaDNivel: TIntegerField
      FieldName = 'Nivel'
      Origin = 'lanctos.Nivel'
    end
    object QrLctoEndossaDVendedor: TIntegerField
      FieldName = 'Vendedor'
      Origin = 'lanctos.Vendedor'
    end
    object QrLctoEndossaDAccount: TIntegerField
      FieldName = 'Account'
      Origin = 'lanctos.Account'
    end
    object QrLctoEndossaDMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
      Origin = 'Mes2'
    end
    object QrLctoEndossaDProtesto: TDateField
      FieldName = 'Protesto'
      Origin = 'lanctos.Protesto'
    end
    object QrLctoEndossaDDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'lanctos.DataCad'
    end
    object QrLctoEndossaDDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'lanctos.DataAlt'
    end
    object QrLctoEndossaDUserCad: TSmallintField
      FieldName = 'UserCad'
      Origin = 'lanctos.UserCad'
    end
    object QrLctoEndossaDUserAlt: TSmallintField
      FieldName = 'UserAlt'
      Origin = 'lanctos.UserAlt'
    end
    object QrLctoEndossaDControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'lanctos.Controle'
      Required = True
    end
    object QrLctoEndossaDID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Origin = 'lanctos.ID_Pgto'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrLctoEndossaDCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Origin = 'lanctos.CtrlIni'
      Required = True
    end
    object QrLctoEndossaDFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
      Origin = 'lanctos.FatID_Sub'
    end
    object QrLctoEndossaDICMS_P: TFloatField
      FieldName = 'ICMS_P'
      Origin = 'lanctos.ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctoEndossaDICMS_V: TFloatField
      FieldName = 'ICMS_V'
      Origin = 'lanctos.ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctoEndossaDDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Origin = 'lanctos.Duplicata'
      Size = 10
    end
    object QrLctoEndossaDCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrLctoEndossaDCliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'lanctos.CliInt'
    end
    object QrLctoEndossaDDepto: TIntegerField
      FieldName = 'Depto'
      Origin = 'lanctos.Depto'
    end
    object QrLctoEndossaDDescoPor: TIntegerField
      FieldName = 'DescoPor'
      Origin = 'lanctos.DescoPor'
    end
    object QrLctoEndossaDPrazo: TSmallintField
      FieldName = 'Prazo'
      Origin = 'carteiras.Prazo'
    end
    object QrLctoEndossaDForneceI: TIntegerField
      FieldName = 'ForneceI'
      Origin = 'lanctos.ForneceI'
    end
    object QrLctoEndossaDQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'lanctos.Qtde'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrLctoEndossaDEmitente: TWideStringField
      FieldName = 'Emitente'
      Origin = 'lanctos.Emitente'
      Size = 30
    end
    object QrLctoEndossaDAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrLctoEndossaDContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Origin = 'lanctos.ContaCorrente'
      Size = 15
    end
    object QrLctoEndossaDCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Origin = 'lanctos.CNPJCPF'
      Size = 15
    end
    object QrLctoEndossaDDescoVal: TFloatField
      FieldName = 'DescoVal'
      Origin = 'lanctos.DescoVal'
    end
    object QrLctoEndossaDDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Origin = 'lanctos.DescoControle'
      Required = True
    end
    object QrLctoEndossaDUnidade: TIntegerField
      FieldName = 'Unidade'
      Origin = 'lanctos.Unidade'
      Required = True
    end
    object QrLctoEndossaDNFVal: TFloatField
      FieldName = 'NFVal'
      Origin = 'lanctos.NFVal'
      Required = True
    end
    object QrLctoEndossaDAntigo: TWideStringField
      FieldName = 'Antigo'
      Origin = 'lanctos.Antigo'
    end
    object QrLctoEndossaDExcelGru: TIntegerField
      FieldName = 'ExcelGru'
      Origin = 'lanctos.ExcelGru'
    end
    object QrLctoEndossaDSerieCH: TWideStringField
      DisplayWidth = 10
      FieldName = 'SerieCH'
      Origin = 'lanctos.SerieCH'
      Size = 10
    end
    object QrLctoEndossaDSERIE_CHEQUE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SERIE_CHEQUE'
      Calculated = True
    end
    object QrLctoEndossaDDoc2: TWideStringField
      FieldName = 'Doc2'
      Origin = 'lanctos.Doc2'
    end
    object QrLctoEndossaDNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Origin = 'NOMEFORNECEI'
      Size = 100
    end
    object QrLctoEndossaDMoraVal: TFloatField
      FieldName = 'MoraVal'
      Origin = 'lanctos.MoraVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctoEndossaDMultaVal: TFloatField
      FieldName = 'MultaVal'
      Origin = 'lanctos.MultaVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctoEndossaDCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
      Origin = 'lanctos.CNAB_Sit'
    end
    object QrLctoEndossaDBanco1: TIntegerField
      FieldName = 'Banco1'
      Origin = 'carteiras.Banco1'
    end
    object QrLctoEndossaDAgencia1: TIntegerField
      FieldName = 'Agencia1'
      Origin = 'carteiras.Agencia1'
    end
    object QrLctoEndossaDConta1: TWideStringField
      FieldName = 'Conta1'
      Origin = 'carteiras.Conta1'
      Size = 15
    end
    object QrLctoEndossaDTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Origin = 'lanctos.TipoCH'
      Required = True
    end
    object QrLctoEndossaDFatNum: TFloatField
      FieldName = 'FatNum'
      Origin = 'lanctos.FatNum'
    end
    object QrLctoEndossaDSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Origin = 'lanctos.SerieNF'
      Size = 5
    end
    object QrLctoEndossaDID: TIntegerField
      FieldName = 'ID'
    end
    object QrLctoEndossaDValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLctoEndossaDOriCtrl: TIntegerField
      FieldName = 'OriCtrl'
    end
    object QrLctoEndossaDOriSub: TIntegerField
      FieldName = 'OriSub'
    end
  end
  object DsLctoEndossaD: TDataSource
    DataSet = QrLctoEndossaD
    Left = 184
    Top = 8
  end
end
