unit LctAjustes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, ComCtrls,
  DB, mySQLDbTables;

type
  TFmLctAjustes = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    LaTipo: TdmkLabel;
    PnCorrige: TPanel;
    LaAviso: TLabel;
    PB1: TProgressBar;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGNC: TDBGrid;
    QrNC: TmySQLQuery;
    DsNC: TDataSource;
    QrNCData: TDateField;
    QrNCVencimento: TDateField;
    QrNCControle: TIntegerField;
    QrNCCtrlQuitPg: TIntegerField;
    QrNCEmpresa: TIntegerField;
    QrNCCarteira: TIntegerField;
    QrNCNO_CART: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FResult: Integer;
  public
    { Public declarations }
    function ErrosEncontrados(): Integer;
  end;

  var
  FmLctAjustes: TFmLctAjustes;

implementation

uses UnMyObjects, Module, UnFinanceiro;

{$R *.DFM}

procedure TFmLctAjustes.BtOKClick(Sender: TObject);
begin
  FResult := 0;
  UFinanceiro.VerificaID_Pgto_x_Compensado(LaAviso, PB1, QrNC);
  //
  if ErrosEncontrados() > 0 then
    BtSaida.Visible := True
  else
    Close;  
end;

procedure TFmLctAjustes.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmLctAjustes.ErrosEncontrados(): Integer;
begin
  Result := FResult;
  if QrNC.State = dsBrowse then
    Result := Result + QrNC.RecordCount;
end;

procedure TFmLctAjustes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLctAjustes.FormCreate(Sender: TObject);
begin
  FResult := 1;
end;

procedure TFmLctAjustes.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.
