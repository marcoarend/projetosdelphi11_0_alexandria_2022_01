object FmLctEdit: TFmLctEdit
  Left = 339
  Top = 169
  Caption = 'FIN-LANCT-001 :: Inclus'#227'o de Lan'#231'amentos [A]'
  ClientHeight = 619
  ClientWidth = 904
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelControle: TPanel
    Left = 0
    Top = 571
    Width = 904
    Height = 48
    Align = alBottom
    TabOrder = 1
    object LaAviso: TLabel
      Left = 368
      Top = 4
      Width = 13
      Height = 13
      Caption = '...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 148
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtConfirmaClick
    end
    object CkContinuar: TCheckBox
      Left = 12
      Top = 7
      Width = 113
      Height = 17
      Caption = 'Continuar inserindo.'
      TabOrder = 0
      Visible = False
    end
    object CkCopiaCH: TCheckBox
      Left = 12
      Top = 24
      Width = 129
      Height = 17
      Caption = 'Fazer c'#243'pia de cheque.'
      TabOrder = 2
    end
    object BitBtn1: TBitBtn
      Tag = 10042
      Left = 260
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = 'C&heque'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = BitBtn1Click
    end
    object Panel4: TPanel
      Left = 784
      Top = 1
      Width = 119
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      Caption = 'Panel4'
      TabOrder = 4
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 15
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
    object CkNaoPesquisar: TCheckBox
      Left = 368
      Top = 24
      Width = 169
      Height = 17
      Caption = 'N'#227'o fazer nenhuma pesquisa.'
      TabOrder = 5
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 904
    Height = 40
    Align = alTop
    Caption = 'Inclus'#227'o de Lan'#231'amentos [A]'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 825
      Top = 1
      Width = 78
      Height = 38
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 712
      ExplicitTop = 2
      ExplicitHeight = 36
    end
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 824
      Height = 38
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 710
      ExplicitHeight = 36
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 40
    Width = 904
    Height = 531
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 0
    OnChange = PageControl1Change
    object TabSheet1: TTabSheet
      Caption = 'Edi'#231#227'o'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object PainelDados: TPanel
        Left = 0
        Top = 0
        Width = 896
        Height = 503
        Align = alClient
        ParentBackground = False
        TabOrder = 0
        object LaFunci: TLabel
          Left = 496
          Top = 4
          Width = 6
          Height = 13
          Caption = '0'
          Visible = False
        end
        object Panel2: TPanel
          Left = 1
          Top = 1
          Width = 572
          Height = 501
          Align = alLeft
          TabOrder = 1
          object PnLancto1: TPanel
            Left = 1
            Top = 1
            Width = 570
            Height = 199
            Align = alTop
            TabOrder = 0
            object Label14: TLabel
              Left = 8
              Top = 2
              Width = 62
              Height = 13
              Caption = 'Lan'#231'amento:'
            end
            object SbCarteira: TSpeedButton
              Left = 540
              Top = 18
              Width = 21
              Height = 21
              Hint = 'Inclui item de carteira'
              Caption = '...'
              OnClick = SbCarteiraClick
            end
            object BtContas: TSpeedButton
              Left = 540
              Top = 58
              Width = 21
              Height = 21
              Hint = 'Inclui conta'
              Caption = '...'
              OnClick = BtContasClick
            end
            object Label1: TLabel
              Left = 8
              Top = 42
              Width = 99
              Height = 13
              Caption = 'Data do lan'#231'amento:'
            end
            object Label2: TLabel
              Left = 112
              Top = 42
              Width = 97
              Height = 13
              Caption = 'Conta [F7] pesquisa:'
            end
            object LaMes: TLabel
              Left = 8
              Top = 82
              Width = 23
              Height = 13
              Caption = 'M'#234's:'
              Enabled = False
            end
            object LaDeb: TLabel
              Left = 65
              Top = 82
              Width = 34
              Height = 13
              Caption = 'D'#233'bito:'
              Enabled = False
            end
            object LaCred: TLabel
              Left = 141
              Top = 82
              Width = 36
              Height = 13
              Caption = 'Cr'#233'dito:'
              Enabled = False
            end
            object LaNF: TLabel
              Left = 217
              Top = 82
              Width = 23
              Height = 13
              Caption = 'N.F.:'
            end
            object LaDoc: TLabel
              Left = 274
              Top = 82
              Width = 104
              Height = 13
              Caption = 'S'#233'rie  e docum. (CH) :'
            end
            object Label11: TLabel
              Left = 381
              Top = 82
              Width = 48
              Height = 13
              Caption = 'Duplicata:'
            end
            object LaVencimento: TLabel
              Left = 449
              Top = 82
              Width = 80
              Height = 13
              Caption = 'Vencimento [F6]:'
            end
            object LaCliInt: TLabel
              Left = 8
              Top = 122
              Width = 60
              Height = 13
              Caption = 'Condom'#237'nio:'
            end
            object LaCliente: TLabel
              Left = 8
              Top = 162
              Width = 35
              Height = 13
              Caption = 'Cliente:'
            end
            object LaFornecedor: TLabel
              Left = 292
              Top = 162
              Width = 57
              Height = 13
              Caption = 'Fornecedor:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object SpeedButton2: TSpeedButton
              Left = 539
              Top = 178
              Width = 22
              Height = 21
              Hint = 'Inclui conta'
              Caption = '...'
              OnClick = SpeedButton2Click
            end
            object LaForneceRN: TLabel
              Left = 352
              Top = 162
              Width = 85
              Height = 13
              Caption = '[F5] Raz'#227'o/Nome'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object LaForneceFA: TLabel
              Left = 444
              Top = 162
              Width = 101
              Height = 13
              Caption = '[F6] Fantasia/Apelido'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label15: TLabel
              Left = 88
              Top = 2
              Width = 256
              Height = 13
              Caption = 'Carteira (Extrato banc'#225'rio, emiss'#227'o banc'#225'ria ou caixa):'
            end
            object dmkEdControle: TdmkEdit
              Left = 8
              Top = 18
              Width = 76
              Height = 21
              Alignment = taRightJustify
              Color = clInactiveCaption
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBackground
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Controle'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
            end
            object dmkEdCarteira: TdmkEditCB
              Left = 88
              Top = 18
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Carteira'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              OnChange = dmkEdCarteiraChange
              DBLookupComboBox = dmkCBCarteira
              IgnoraDBLookupComboBox = False
            end
            object dmkCBCarteira: TdmkDBLookupComboBox
              Left = 144
              Top = 18
              Width = 393
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsCarteiras
              TabOrder = 2
              dmkEditCB = dmkEdCarteira
              QryCampo = 'Carteira'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object dmkCBGenero: TdmkDBLookupComboBox
              Left = 172
              Top = 58
              Width = 365
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsContas
              TabOrder = 5
              OnClick = dmkCBGeneroClick
              OnKeyDown = dmkCBGeneroKeyDown
              dmkEditCB = dmkEdCBGenero
              QryCampo = 'Genero'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object dmkEdCBGenero: TdmkEditCB
              Left = 117
              Top = 58
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Genero'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              OnChange = dmkEdCBGeneroChange
              OnKeyDown = dmkEdCBGeneroKeyDown
              DBLookupComboBox = dmkCBGenero
              IgnoraDBLookupComboBox = False
            end
            object dmkEdTPData: TdmkEditDateTimePicker
              Left = 8
              Top = 58
              Width = 106
              Height = 21
              Date = 39615.655720300900000000
              Time = 39615.655720300900000000
              TabOrder = 3
              OnClick = dmkEdTPDataClick
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'Data'
              UpdType = utYes
            end
            object dmkEdMes: TdmkEdit
              Left = 8
              Top = 98
              Width = 54
              Height = 21
              Alignment = taCenter
              Enabled = False
              TabOrder = 6
              FormatType = dmktfMesAno
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfLong
              HoraFormat = dmkhfShort
              QryCampo = 'Mez'
              UpdType = utYes
              Obrigatorio = True
              PermiteNulo = False
              ValueVariant = Null
              OnExit = dmkEdMesExit
            end
            object dmkEdDeb: TdmkEdit
              Left = 65
              Top = 98
              Width = 73
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 7
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'Debito'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              OnExit = dmkEdDebExit
            end
            object dmkEdCred: TdmkEdit
              Left = 141
              Top = 98
              Width = 73
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 8
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'Credito'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              OnExit = dmkEdCredExit
            end
            object dmkEdNF: TdmkEdit
              Left = 217
              Top = 98
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 9
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 6
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000000'
              QryCampo = 'NotaFiscal'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
            end
            object dmkEdSerieCH: TdmkEdit
              Left = 276
              Top = 98
              Width = 41
              Height = 21
              CharCase = ecUpperCase
              TabOrder = 10
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'SerieCH'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              OnExit = dmkEdSerieCHExit
            end
            object dmkEdDoc: TdmkEdit
              Left = 316
              Top = 98
              Width = 62
              Height = 21
              Alignment = taRightJustify
              TabOrder = 11
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 6
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000000'
              QryCampo = 'Documento'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              OnChange = dmkEdDocChange
              OnExit = dmkEdDocExit
            end
            object dmkEdDuplicata: TdmkEdit
              Left = 381
              Top = 98
              Width = 65
              Height = 21
              TabOrder = 12
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'Duplicata'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              OnExit = dmkEdDuplicataExit
            end
            object dmkEdTPVencto: TdmkEditDateTimePicker
              Left = 449
              Top = 98
              Width = 112
              Height = 21
              Date = 39615.672523148100000000
              Time = 39615.672523148100000000
              TabOrder = 13
              OnClick = dmkEdTPVenctoClick
              OnChange = dmkEdTPVenctoChange
              OnKeyDown = dmkEdTPVenctoKeyDown
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'Vencimento'
              UpdType = utYes
            end
            object dmkEdCBCliInt: TdmkEditCB
              Left = 8
              Top = 138
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 14
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'CliInt'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              OnChange = dmkEdCBCliIntChange
              DBLookupComboBox = dmkCBCliInt
              IgnoraDBLookupComboBox = False
            end
            object dmkCBCliInt: TdmkDBLookupComboBox
              Left = 68
              Top = 138
              Width = 493
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsCliInt
              TabOrder = 15
              dmkEditCB = dmkEdCBCliInt
              QryCampo = 'CliInt'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object dmkEdCBCliente: TdmkEditCB
              Left = 8
              Top = 178
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 16
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Cliente'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = dmkCBCliente
              IgnoraDBLookupComboBox = False
            end
            object dmkCBCliente: TdmkDBLookupComboBox
              Left = 65
              Top = 178
              Width = 221
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsClientes
              TabOrder = 17
              dmkEditCB = dmkEdCBCliente
              QryCampo = 'Cliente'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object dmkEdCBFornece: TdmkEditCB
              Left = 292
              Top = 178
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 18
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Fornecedor'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              OnKeyDown = dmkCBForneceKeyDown
              DBLookupComboBox = dmkCBFornece
              IgnoraDBLookupComboBox = False
            end
            object dmkCBFornece: TdmkDBLookupComboBox
              Left = 352
              Top = 178
              Width = 185
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsFornecedores
              TabOrder = 19
              OnKeyDown = dmkCBForneceKeyDown
              dmkEditCB = dmkEdCBFornece
              QryCampo = 'Fornecedor'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
          object PnDepto: TPanel
            Left = 1
            Top = 200
            Width = 570
            Height = 39
            Align = alTop
            TabOrder = 1
            Visible = False
            object LaDepto: TLabel
              Left = 8
              Top = 2
              Width = 177
              Height = 13
              Caption = 'Unidade habitacional do condom'#237'nio:'
            end
            object dmkCBDepto: TdmkDBLookupComboBox
              Left = 68
              Top = 18
              Width = 493
              Height = 21
              KeyField = 'CODI_1'
              ListField = 'NOME_1'
              ListSource = DsDeptos
              TabOrder = 1
              dmkEditCB = dmkEdCBDepto
              QryCampo = 'Depto'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object dmkEdCBDepto: TdmkEditCB
              Left = 8
              Top = 18
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Depto'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = dmkCBDepto
              IgnoraDBLookupComboBox = False
            end
          end
          object PnForneceI: TPanel
            Left = 1
            Top = 239
            Width = 570
            Height = 37
            Align = alTop
            TabOrder = 2
            Visible = False
            object LaForneceI: TLabel
              Left = 8
              Top = 2
              Width = 172
              Height = 13
              Caption = 'Propriet'#225'rio da unidade habitacional:'
            end
            object dmkCBForneceI: TdmkDBLookupComboBox
              Left = 68
              Top = 16
              Width = 493
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsForneceI
              TabOrder = 1
              dmkEditCB = dmkEdCBForneceI
              QryCampo = 'ForneceI'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object dmkEdCBForneceI: TdmkEditCB
              Left = 8
              Top = 16
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'ForneceI'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = dmkCBForneceI
              IgnoraDBLookupComboBox = False
            end
          end
          object PnAccount: TPanel
            Left = 1
            Top = 276
            Width = 570
            Height = 39
            Align = alTop
            TabOrder = 3
            Visible = False
            object LaAccount: TLabel
              Left = 8
              Top = 2
              Width = 43
              Height = 13
              Caption = 'Account:'
            end
            object dmkEdCBAccount: TdmkEditCB
              Left = 8
              Top = 18
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Account'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              OnKeyDown = dmkEdCBAccountKeyDown
              DBLookupComboBox = dmkCBAccount
              IgnoraDBLookupComboBox = False
            end
            object dmkCBAccount: TdmkDBLookupComboBox
              Left = 68
              Top = 18
              Width = 493
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsAccounts
              TabOrder = 1
              OnKeyDown = dmkDBLookupComboBox1KeyDown
              dmkEditCB = dmkEdCBAccount
              QryCampo = 'Depto'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
          object PnLancto3: TPanel
            Left = 1
            Top = 438
            Width = 570
            Height = 62
            Align = alBottom
            TabOrder = 5
            object CkPesqNF: TCheckBox
              Left = 8
              Top = 4
              Width = 201
              Height = 17
              Caption = 'Pesquisar duplica'#231#227'o de nota fiscal.'
              Checked = True
              State = cbChecked
              TabOrder = 0
            end
            object CkPesqCH: TCheckBox
              Left = 221
              Top = 4
              Width = 201
              Height = 17
              Caption = 'Pesquisar duplica'#231#227'o de cheque.'
              Checked = True
              State = cbChecked
              TabOrder = 1
            end
            object CkCancelado: TdmkCheckBox
              Left = 8
              Top = 42
              Width = 433
              Height = 17
              Caption = 
                'Lan'#231'amento cancelado (cr'#233'dito e d'#233'bito ser'#227'o zerados e status se' +
                'r'#225' igual a cancelado).'
              TabOrder = 3
              UpdType = utYes
              ValCheck = #0
              ValUncheck = #0
              OldValor = #0
            end
            object CkPesqVal: TCheckBox
              Left = 8
              Top = 23
              Width = 397
              Height = 17
              Caption = 
                'Pesquisa lan'#231'amentos cruzando o valor com a conta e o m'#234's de com' +
                'pet'#234'ncia.'
              Checked = True
              State = cbChecked
              TabOrder = 2
            end
          end
          object PnLancto2: TPanel
            Left = 1
            Top = 315
            Width = 570
            Height = 123
            Align = alClient
            TabOrder = 4
            object Label13: TLabel
              Left = 83
              Top = 2
              Width = 144
              Height = 13
              Caption = 'Descri'#231#227'o [F4], [F5], [F6], [F8]:'
            end
            object Label20: TLabel
              Left = 8
              Top = 2
              Width = 58
              Height = 13
              Caption = 'Quantidade:'
            end
            object LaEventosCad: TLabel
              Left = 8
              Top = 41
              Width = 37
              Height = 13
              Caption = 'Evento:'
            end
            object Label60: TLabel
              Left = 8
              Top = 81
              Width = 121
              Height = 13
              Caption = 'Indica'#231#227'o de pagamento:'
            end
            object SbEventosCad: TSpeedButton
              Left = 540
              Top = 57
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SbEventosCadClick
            end
            object SbIndiPag: TSpeedButton
              Left = 540
              Top = 97
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SbIndiPagClick
            end
            object dmkEdDescricao: TdmkEdit
              Left = 83
              Top = 17
              Width = 478
              Height = 21
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'Descricao'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              OnKeyDown = dmkEdDescricaoKeyDown
            end
            object dmkEdQtde: TdmkEdit
              Left = 8
              Top = 17
              Width = 71
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
            object EdEventosCad: TdmkEditCB
              Left = 8
              Top = 57
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBEventosCad
              IgnoraDBLookupComboBox = False
            end
            object CBEventosCad: TdmkDBLookupComboBox
              Left = 68
              Top = 57
              Width = 472
              Height = 21
              Color = clWhite
              KeyField = 'CodUsu'
              ListField = 'Nome'
              ListSource = DsEventosCad
              TabOrder = 3
              dmkEditCB = EdEventosCad
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdIndiPag: TdmkEditCB
              Left = 8
              Top = 97
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'IndiPag'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBIndiPag
              IgnoraDBLookupComboBox = False
            end
            object CBIndiPag: TdmkDBLookupComboBox
              Left = 68
              Top = 97
              Width = 472
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsIndiPag
              TabOrder = 5
              dmkEditCB = EdIndiPag
              QryCampo = 'IndiPag'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
        end
        object PnMaskPesq: TPanel
          Left = 573
          Top = 1
          Width = 322
          Height = 501
          Align = alClient
          TabOrder = 0
          object PnLink: TPanel
            Left = 1
            Top = 1
            Width = 320
            Height = 20
            Align = alTop
            BevelOuter = bvLowered
            Caption = 'Sem link'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            object Panel6: TPanel
              Left = 301
              Top = 1
              Width = 18
              Height = 18
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 0
              object SpeedButton3: TSpeedButton
                Left = 0
                Top = 0
                Width = 18
                Height = 18
                Caption = 'x'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Layout = blGlyphBottom
                ParentFont = False
                Visible = False
                OnClick = SpeedButton3Click
              end
            end
          end
          object Panel11: TPanel
            Left = 1
            Top = 332
            Width = 320
            Height = 168
            Align = alBottom
            TabOrder = 1
            object Label51: TLabel
              Left = 4
              Top = 4
              Width = 52
              Height = 13
              Caption = 'Sub-grupo:'
              FocusControl = DBEdit1
            end
            object Label52: TLabel
              Left = 4
              Top = 44
              Width = 32
              Height = 13
              Caption = 'Grupo:'
              FocusControl = DBEdit2
            end
            object Label53: TLabel
              Left = 4
              Top = 84
              Width = 45
              Height = 13
              Caption = 'Conjunto:'
              FocusControl = DBEdit3
            end
            object Label54: TLabel
              Left = 4
              Top = 124
              Width = 30
              Height = 13
              Caption = 'Plano:'
              FocusControl = DBEdit4
            end
            object DBEdit1: TDBEdit
              Left = 4
              Top = 20
              Width = 312
              Height = 21
              DataField = 'NOMESUBGRUPO'
              DataSource = DsContas
              TabOrder = 0
            end
            object DBEdit2: TDBEdit
              Left = 4
              Top = 60
              Width = 312
              Height = 21
              DataField = 'NOMEGRUPO'
              DataSource = DsContas
              TabOrder = 1
            end
            object DBEdit3: TDBEdit
              Left = 4
              Top = 100
              Width = 312
              Height = 21
              DataField = 'NOMECONJUNTO'
              DataSource = DsContas
              TabOrder = 2
            end
            object DBEdit4: TDBEdit
              Left = 4
              Top = 140
              Width = 312
              Height = 21
              DataField = 'NOMEPLANO'
              DataSource = DsContas
              TabOrder = 3
            end
          end
          object Panel5: TPanel
            Left = 1
            Top = 21
            Width = 320
            Height = 311
            Align = alClient
            TabOrder = 2
            object Panel12: TPanel
              Left = 1
              Top = 1
              Width = 318
              Height = 44
              Align = alTop
              BevelOuter = bvLowered
              TabOrder = 0
              object Label18: TLabel
                Left = 4
                Top = 4
                Width = 186
                Height = 13
                Caption = 'Digite parte da descri'#231#227'o (min. 3 letras):'
              end
              object EdLinkMask: TEdit
                Left = 4
                Top = 20
                Width = 309
                Height = 21
                TabOrder = 0
                OnChange = EdLinkMaskChange
                OnKeyDown = EdLinkMaskKeyDown
              end
            end
            object LLBPesq: TDBLookupListBox
              Left = 1
              Top = 45
              Width = 318
              Height = 264
              Align = alClient
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsPesq
              TabOrder = 1
              OnDblClick = LLBPesqDblClick
              OnEnter = LLBPesqEnter
              OnExit = LLBPesqExit
              OnKeyDown = LLBPesqKeyDown
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Outros dados'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 896
        Height = 189
        Align = alTop
        ParentBackground = False
        TabOrder = 0
        object LaDataDoc: TLabel
          Left = 4
          Top = 4
          Width = 82
          Height = 13
          Caption = 'Data documento:'
        end
        object Label22: TLabel
          Left = 4
          Top = 44
          Width = 65
          Height = 13
          Caption = 'Compensado:'
          Enabled = False
        end
        object GroupBox1: TGroupBox
          Left = 108
          Top = 4
          Width = 461
          Height = 137
          TabOrder = 2
          object GroupBox2: TGroupBox
            Left = 8
            Top = 8
            Width = 233
            Height = 61
            Caption = ' Multa e juros a cobrar: '
            TabOrder = 0
            object Label8: TLabel
              Left = 8
              Top = 16
              Width = 83
              Height = 13
              Caption = 'Juros ao M'#234's (%):'
            end
            object Label7: TLabel
              Left = 120
              Top = 16
              Width = 46
              Height = 13
              Caption = 'Multa (%):'
            end
            object dmkEdMoraDia: TdmkEdit
              Left = 8
              Top = 32
              Width = 109
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              QryCampo = 'MoraDia'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
            object dmkEdMulta: TdmkEdit
              Left = 120
              Top = 32
              Width = 106
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              QryCampo = 'Multa'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
          end
          object GroupBox3: TGroupBox
            Left = 8
            Top = 72
            Width = 233
            Height = 61
            Caption = ' Multa e juros pagos: '
            TabOrder = 1
            object Label19: TLabel
              Left = 120
              Top = 16
              Width = 38
              Height = 13
              Caption = 'Multa $:'
            end
            object Label21: TLabel
              Left = 8
              Top = 16
              Width = 37
              Height = 13
              Caption = 'Juros $:'
            end
            object dmkEdMultaVal: TdmkEdit
              Left = 120
              Top = 32
              Width = 106
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              QryCampo = 'MultaVal'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
            object dmkEdMoraVal: TdmkEdit
              Left = 8
              Top = 32
              Width = 109
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 4
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,0000'
              QryCampo = 'MoraVal'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
          end
          object GroupBox4: TGroupBox
            Left = 244
            Top = 8
            Width = 209
            Height = 125
            Caption = 'Rec'#225'lculo: '
            TabOrder = 2
            object Label28: TLabel
              Left = 8
              Top = 16
              Width = 63
              Height = 13
              Caption = 'Valor original:'
            end
            object Label29: TLabel
              Left = 116
              Top = 16
              Width = 46
              Height = 13
              Caption = 'Multa (%):'
            end
            object BitBtn2: TBitBtn
              Tag = 180
              Left = 43
              Top = 68
              Width = 120
              Height = 40
              Cursor = crHandPoint
              Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
              Caption = '&Recalcular'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              NumGlyphs = 2
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              OnClick = BitBtn2Click
            end
            object dmkEdValNovo: TdmkEdit
              Left = 8
              Top = 32
              Width = 105
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
            object dmkEdPerMult: TdmkEdit
              Left = 116
              Top = 32
              Width = 85
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
          end
        end
        object dmkEdTPDataDoc: TdmkEditDateTimePicker
          Left = 4
          Top = 20
          Width = 101
          Height = 21
          Date = 39615.655720300900000000
          Time = 39615.655720300900000000
          TabOrder = 0
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DataDoc'
          UpdType = utYes
        end
        object dmkEdTPCompensado: TdmkEditDateTimePicker
          Left = 4
          Top = 60
          Width = 101
          Height = 21
          Date = 39615.887948310200000000
          Time = 39615.887948310200000000
          Enabled = False
          TabOrder = 1
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'Compensado'
          UpdType = utYes
        end
        object dmkRGTipoCH: TdmkRadioGroup
          Left = 4
          Top = 84
          Width = 101
          Height = 101
          Caption = ' Tipo de cheque: '
          ItemIndex = 0
          Items.Strings = (
            '?'
            'Visado'
            'Cruzado'
            'Ambos')
          TabOrder = 3
          QryCampo = 'TipoCH'
          UpdType = utYes
          OldValor = 0
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 189
        Width = 896
        Height = 314
        Align = alClient
        ParentBackground = False
        TabOrder = 1
        Visible = False
        object Label10: TLabel
          Left = 8
          Top = 4
          Width = 58
          Height = 13
          Caption = 'Funcion'#225'rio:'
          Visible = False
        end
        object LaVendedor: TLabel
          Left = 8
          Top = 48
          Width = 49
          Height = 13
          Caption = 'Vendedor:'
          Visible = False
        end
        object LaICMS_P: TLabel
          Left = 288
          Top = 4
          Width = 37
          Height = 13
          Caption = '% ICMS'
          Visible = False
        end
        object LaICMS_V: TLabel
          Left = 392
          Top = 4
          Width = 56
          Height = 13
          Caption = 'Valor ICMS:'
          Visible = False
        end
        object dmkCBFunci: TdmkDBLookupComboBox
          Left = 68
          Top = 20
          Width = 217
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsFunci
          TabOrder = 1
          dmkEditCB = dmkEdCBFunci
          QryCampo = 'UserCad'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object dmkEdCBFunci: TdmkEditCB
          Left = 8
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'UserCad'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = dmkCBFunci
          IgnoraDBLookupComboBox = False
        end
        object dmkEdCBVendedor: TdmkEditCB
          Left = 8
          Top = 64
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Vendedor'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = dmkCBVendedor
          IgnoraDBLookupComboBox = False
        end
        object dmkCBVendedor: TdmkDBLookupComboBox
          Left = 68
          Top = 64
          Width = 217
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsVendedores
          TabOrder = 5
          dmkEditCB = dmkEdCBVendedor
          QryCampo = 'Vendedor'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object dmkEdICMS_P: TdmkEdit
          Left = 288
          Top = 20
          Width = 101
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMS_P'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnExit = dmkEdICMS_PExit
          OnKeyDown = dmkEdICMS_PKeyDown
        end
        object dmkEdICMS_V: TdmkEdit
          Left = 392
          Top = 20
          Width = 101
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ICMS_V'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnExit = dmkEdICMS_VExit
          OnKeyDown = dmkEdICMS_VKeyDown
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Parcelamento'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 640
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 896
        Height = 156
        Align = alTop
        ParentBackground = False
        TabOrder = 0
        object GBParcelamento: TGroupBox
          Left = 1
          Top = 1
          Width = 894
          Height = 154
          Align = alClient
          Caption = ' Parcelamento autom'#225'tico: '
          TabOrder = 1
          Visible = False
          object Label23: TLabel
            Left = 8
            Top = 20
            Width = 44
            Height = 13
            Caption = 'Parcelas:'
          end
          object Label24: TLabel
            Left = 508
            Top = 24
            Width = 78
            Height = 13
            Caption = 'Primeira parcela:'
          end
          object Label25: TLabel
            Left = 588
            Top = 24
            Width = 70
            Height = 13
            Caption = #218'ltima parcela:'
          end
          object Label26: TLabel
            Left = 660
            Top = 24
            Width = 97
            Height = 13
            Caption = 'Total parcelamento: '
          end
          object RGArredondar: TRadioGroup
            Left = 308
            Top = 16
            Width = 193
            Height = 45
            Caption = '   Arredondar '
            Columns = 2
            ItemIndex = 1
            Items.Strings = (
              '1'#170' parcela'
              #218'ltima parcela')
            TabOrder = 7
            OnClick = RGArredondarClick
          end
          object RGPeriodo: TRadioGroup
            Left = 60
            Top = 16
            Width = 141
            Height = 45
            Caption = ' Periodo: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Mensal'
              '         dias')
            TabOrder = 1
            OnClick = RGPeriodoClick
          end
          object EdDias: TdmkEdit
            Left = 151
            Top = 32
            Width = 24
            Height = 21
            Alignment = taRightJustify
            Color = clBtnFace
            Enabled = False
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '7'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 7
            OnExit = EdDiasExit
          end
          object RGIncremCH: TRadioGroup
            Left = 204
            Top = 16
            Width = 101
            Height = 45
            Caption = ' Increm. cheque.: '
            Columns = 2
            Enabled = False
            ItemIndex = 1
            Items.Strings = (
              'N'#227'o'
              'Sim')
            TabOrder = 3
            OnClick = RGIncremCHClick
          end
          object EdParcela1: TdmkEdit
            Left = 508
            Top = 40
            Width = 77
            Height = 21
            Alignment = taRightJustify
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdParcelaX: TdmkEdit
            Left = 588
            Top = 40
            Width = 69
            Height = 21
            Alignment = taRightJustify
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object CkArredondar: TCheckBox
            Left = 320
            Top = 14
            Width = 77
            Height = 17
            Caption = 'Arredondar: '
            TabOrder = 6
            OnClick = CkArredondarClick
          end
          object EdSoma: TdmkEdit
            Left = 660
            Top = 40
            Width = 97
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Color = clBtnFace
            ReadOnly = True
            TabOrder = 8
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object RGIncremDupl: TGroupBox
            Left = 8
            Top = 66
            Width = 381
            Height = 73
            Caption = '   Incremento de duplicata: '
            TabOrder = 10
            Visible = False
            object Label27: TLabel
              Left = 4
              Top = 36
              Width = 52
              Height = 13
              Caption = 'Separador:'
            end
            object EdDuplSep: TEdit
              Left = 60
              Top = 32
              Width = 21
              Height = 21
              TabOrder = 0
              Text = '/'
              OnExit = EdDuplSepExit
            end
            object RGDuplSeq: TRadioGroup
              Left = 84
              Top = 16
              Width = 293
              Height = 53
              Caption = ' Sequenciador: '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'Num'#233'rico: 1, 2, 3, etc.'
                'Alfab'#233'tico: A, B, C, etc.')
              TabOrder = 1
              OnClick = RGDuplSeqClick
            end
          end
          object GBIncremTxt: TGroupBox
            Left = 392
            Top = 66
            Width = 381
            Height = 73
            Caption = '       '
            TabOrder = 11
            Visible = False
            object Label30: TLabel
              Left = 4
              Top = 36
              Width = 52
              Height = 13
              Caption = 'Separador:'
            end
            object EdSepTxt: TEdit
              Left = 60
              Top = 32
              Width = 21
              Height = 21
              TabOrder = 0
              Text = '/'
              OnExit = EdDuplSepExit
            end
            object RGSepTxt: TRadioGroup
              Left = 84
              Top = 20
              Width = 293
              Height = 49
              Caption = ' Sequenciador: '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'Num'#233'rico: 1, 2, 3, etc.'
                'Alfab'#233'tico: A, B, C, etc.')
              TabOrder = 1
              OnClick = RGDuplSeqClick
            end
          end
          object CkIncremDU: TCheckBox
            Left = 20
            Top = 63
            Width = 137
            Height = 17
            Caption = 'Incremento da duplicata: '
            TabOrder = 9
            OnClick = CkIncremDUClick
          end
          object CkIncremTxt: TCheckBox
            Left = 408
            Top = 64
            Width = 121
            Height = 17
            Caption = 'Incremento do texto: '
            TabOrder = 12
            OnClick = CkIncremTxtClick
          end
          object dmkEdParcelas: TdmkEdit
            Left = 8
            Top = 36
            Width = 45
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 3
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '2'
            ValMax = '1000'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '002'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 2
            OnExit = dmkEdParcelasExit
          end
        end
        object CkParcelamento: TCheckBox
          Left = 12
          Top = 1
          Width = 153
          Height = 17
          Caption = ' Parcelamento autom'#225'tico: '
          TabOrder = 0
          Visible = False
          OnClick = CkParcelamentoClick
        end
      end
      object DBGParcelas: TDBGrid
        Left = 0
        Top = 156
        Width = 896
        Height = 347
        TabStop = False
        Align = alClient
        DataSource = DsParcPagtos
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnKeyDown = DBGParcelasKeyDown
        Columns = <
          item
            Expanded = False
            FieldName = 'Parcela'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Data'
            Title.Caption = 'Vencimento'
            Width = 65
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Debito'
            Title.Caption = 'D'#233'bito'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Credito'
            Title.Caption = 'Cr'#233'dito'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Doc'
            Title.Caption = 'Docum.'
            Width = 49
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Mora'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Multa'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ICMS_V'
            Title.Caption = '$ ICMS'
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Duplicata'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descricao'
            Visible = True
          end>
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Hist'#243'rico'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object TabControl1: TTabControl
        Left = 0
        Top = 0
        Width = 896
        Height = 503
        Align = alClient
        TabOrder = 0
        Tabs.Strings = (
          'Carteira'
          'Cliente interno'
          'Cliente'
          'Fornecedor'
          'Conta')
        TabIndex = 0
        OnChange = TabControl1Change
        object DBGrid1: TDBGrid
          Left = 4
          Top = 24
          Width = 888
          Height = 475
          Align = alClient
          DataSource = DsLct
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Data'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Documento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMERELACIONADO'
              Title.Caption = 'Empresa'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NotaFiscal'
              Title.Caption = 'N.F.'
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'Duplicata'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Caption = 'D'#233'bito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Caption = 'Cr'#233'dito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencim.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COMPENSADO_TXT'
              Title.Caption = 'Compen.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MENSAL'
              Title.Caption = 'M'#234's'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESIT'
              Title.Caption = 'Situa'#231#227'o'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Lan'#231'amento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sub'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO'
              Title.Caption = 'Saldo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ICMS_P'
              Title.Caption = '% ICMS'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ICMS_V'
              Title.Caption = '$ ICMS'
              Visible = True
            end>
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = ' Dados complementares '
      ImageIndex = 4
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 640
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 896
        Height = 79
        Align = alTop
        Caption = 'Panel8'
        ParentBackground = False
        TabOrder = 0
        object Panel9: TPanel
          Left = 1
          Top = 1
          Width = 894
          Height = 24
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label4: TLabel
            Left = 8
            Top = 4
            Width = 112
            Height = 13
            Caption = 'Configura'#231#227'o da janela:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object LaFinalidade: TLabel
            Left = 124
            Top = 4
            Width = 22
            Height = 13
            Caption = '000'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
        end
        object MeConfig: TMemo
          Left = 1
          Top = 25
          Width = 894
          Height = 53
          Align = alClient
          Enabled = False
          TabOrder = 1
        end
      end
      object Panel10: TPanel
        Left = 0
        Top = 79
        Width = 896
        Height = 424
        Align = alClient
        Enabled = False
        ParentBackground = False
        TabOrder = 1
        ExplicitHeight = 561
        object Label5: TLabel
          Left = 8
          Top = 8
          Width = 47
          Height = 13
          Caption = 'CNAB_Sit'
          Enabled = False
        end
        object Label6: TLabel
          Left = 8
          Top = 32
          Width = 39
          Height = 13
          Caption = 'ID_Pgto'
          Enabled = False
        end
        object Label12: TLabel
          Left = 8
          Top = 80
          Width = 26
          Height = 13
          Caption = 'FatID'
          Enabled = False
        end
        object Label16: TLabel
          Left = 8
          Top = 56
          Width = 36
          Height = 13
          Caption = 'TipoCH'
          Enabled = False
        end
        object Label17: TLabel
          Left = 8
          Top = 128
          Width = 37
          Height = 13
          Caption = 'FatNum'
          Enabled = False
        end
        object Label31: TLabel
          Left = 8
          Top = 104
          Width = 51
          Height = 13
          Caption = 'FatID_Sub'
          Enabled = False
        end
        object Label32: TLabel
          Left = 8
          Top = 176
          Width = 24
          Height = 13
          Caption = 'Nivel'
          Enabled = False
        end
        object Label33: TLabel
          Left = 8
          Top = 152
          Width = 51
          Height = 13
          Caption = 'FatParcela'
          Enabled = False
        end
        object Label34: TLabel
          Left = 8
          Top = 224
          Width = 26
          Height = 13
          Caption = 'CtrlIni'
          Enabled = False
        end
        object Label35: TLabel
          Left = 8
          Top = 200
          Width = 47
          Height = 13
          Caption = 'DescoPor'
          Enabled = False
        end
        object Label36: TLabel
          Left = 8
          Top = 272
          Width = 46
          Height = 13
          Caption = 'DescoVal'
          Enabled = False
        end
        object Label37: TLabel
          Left = 8
          Top = 248
          Width = 40
          Height = 13
          Caption = 'Unidade'
          Enabled = False
        end
        object Label38: TLabel
          Left = 160
          Top = 8
          Width = 26
          Height = 13
          Caption = 'Doc2'
          Enabled = False
        end
        object Label39: TLabel
          Left = 8
          Top = 296
          Width = 29
          Height = 13
          Caption = 'NFVal'
          Enabled = False
        end
        object Label40: TLabel
          Left = 160
          Top = 32
          Width = 35
          Height = 13
          Caption = 'CartAnt'
          Enabled = False
        end
        object Label41: TLabel
          Left = 160
          Top = 56
          Width = 37
          Height = 13
          Caption = 'TipoAnt'
          Enabled = False
        end
        object Label42: TLabel
          Left = 160
          Top = 80
          Width = 47
          Height = 13
          Caption = 'PercJuros'
          Enabled = False
        end
        object Label43: TLabel
          Left = 160
          Top = 104
          Width = 48
          Height = 13
          Caption = 'PercMulta'
          Enabled = False
        end
        object Label44: TLabel
          Left = 160
          Top = 128
          Width = 26
          Height = 13
          Caption = 'ICMS'
          Enabled = False
        end
        object Label45: TLabel
          Left = 160
          Top = 296
          Width = 56
          Height = 13
          Caption = 'Execu'#231#245'es:'
          Enabled = False
        end
        object Label46: TLabel
          Left = 160
          Top = 200
          Width = 70
          Height = 13
          Caption = 'Cliente interno:'
          Enabled = False
        end
        object Label3: TLabel
          Left = 160
          Top = 224
          Width = 68
          Height = 13
          Caption = 'Forneced. int.:'
          Enabled = False
        end
        object Label47: TLabel
          Left = 160
          Top = 248
          Width = 69
          Height = 13
          Caption = 'Vendedor  int.:'
          Enabled = False
        end
        object Label48: TLabel
          Left = 160
          Top = 272
          Width = 60
          Height = 13
          Caption = 'Account int.:'
          Enabled = False
        end
        object Label49: TLabel
          Left = 160
          Top = 152
          Width = 35
          Height = 13
          Caption = 'Cliente:'
          Enabled = False
        end
        object Label50: TLabel
          Left = 160
          Top = 176
          Width = 60
          Height = 13
          Caption = 'Fornecedor.:'
          Enabled = False
        end
        object Label9: TLabel
          Left = 160
          Top = 320
          Width = 48
          Height = 13
          Caption = 'Protocolo:'
          Enabled = False
        end
        object dmkEdCNAB_Sit: TdmkEdit
          Left = 80
          Top = 4
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CNAB_Sit'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object dmkEdID_pgto: TdmkEdit
          Left = 80
          Top = 28
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ID_Pgto'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object dmkEdFatID: TdmkEdit
          Left = 80
          Top = 76
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'FatID'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object dmkEdTipoCH: TdmkEdit
          Left = 80
          Top = 52
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'TipoCH'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object dmkEdFatNum: TdmkEdit
          Left = 80
          Top = 124
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'FatNum'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object dmkEdFatID_Sub: TdmkEdit
          Left = 80
          Top = 100
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'FatID_Sub'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object dmkEdNivel: TdmkEdit
          Left = 80
          Top = 172
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Nivel'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object dmkEdFatParcela: TdmkEdit
          Left = 80
          Top = 148
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'FatParcela'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object dmkEdCtrlIni: TdmkEdit
          Left = 80
          Top = 220
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 8
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CtrlIni'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object dmkEdDescoPor: TdmkEdit
          Left = 80
          Top = 196
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 9
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'DescoPor'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object dmkEdDescoVal: TdmkEdit
          Left = 80
          Top = 268
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 10
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'DescoVal'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object dmkEdUnidade: TdmkEdit
          Left = 80
          Top = 244
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 11
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Unidade'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object dmkEdDoc2: TdmkEdit
          Left = 232
          Top = 4
          Width = 72
          Height = 21
          Enabled = False
          TabOrder = 12
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Doc2'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '0'
        end
        object dmkEdNFVal: TdmkEdit
          Left = 80
          Top = 292
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 13
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'NFVal'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object dmkEdTipoAnt: TdmkEdit
          Left = 232
          Top = 52
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 14
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Tipo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object dmkEdCartAnt: TdmkEdit
          Left = 232
          Top = 28
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 15
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Carteira'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object dmkEdPercJuroM: TdmkEdit
          Left = 232
          Top = 76
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 16
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object dmkEdPercMulta: TdmkEdit
          Left = 232
          Top = 100
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 17
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object dmkEd_: TdmkEdit
          Left = 232
          Top = 124
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 18
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object dmkEdExecs: TdmkEdit
          Left = 232
          Top = 292
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 19
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object dmkEdOneCliInt: TdmkEdit
          Left = 232
          Top = 196
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 20
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-1000000'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object dmkEdOneForneceI: TdmkEdit
          Left = 232
          Top = 220
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 21
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-1000000'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object dmkEdOneVendedor: TdmkEdit
          Left = 232
          Top = 244
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 22
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-1000000'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object dmkEdOneAccount: TdmkEdit
          Left = 232
          Top = 268
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 23
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-1000000'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object dmkEdOneCliente: TdmkEdit
          Left = 232
          Top = 148
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 24
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-1000000'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object dmkEdOneFornecedor: TdmkEdit
          Left = 232
          Top = 172
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 25
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-1000000'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object CkDuplicando: TCheckBox
          Left = 8
          Top = 320
          Width = 97
          Height = 17
          Caption = 'Duplicando?'
          TabOrder = 26
        end
        object EdProtocolo: TdmkEdit
          Left = 232
          Top = 316
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 27
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Protocolo'
          UpdCampo = 'Protocolo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
      end
    end
    object TabSheet6: TTabSheet
      Caption = ' Dados de Edi'#231#227'o '
      ImageIndex = 5
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 640
      object Panel13: TPanel
        Left = 0
        Top = 0
        Width = 896
        Height = 62
        Align = alTop
        Enabled = False
        TabOrder = 0
        object GroupBox5: TGroupBox
          Left = 36
          Top = 1
          Width = 621
          Height = 60
          Caption = ' Dados para altera'#231#227'o '
          TabOrder = 0
          object Label55: TLabel
            Left = 140
            Top = 16
            Width = 26
            Height = 13
            Caption = 'Data:'
          end
          object Label56: TLabel
            Left = 12
            Top = 16
            Width = 42
            Height = 13
            Caption = 'Controle:'
          end
          object Label57: TLabel
            Left = 76
            Top = 16
            Width = 22
            Height = 13
            Caption = 'Sub:'
          end
          object Label58: TLabel
            Left = 256
            Top = 16
            Width = 24
            Height = 13
            Caption = 'Tipo:'
          end
          object Label59: TLabel
            Left = 320
            Top = 16
            Width = 39
            Height = 13
            Caption = 'Carteira:'
          end
          object EdOldControle: TdmkEdit
            Left = 12
            Top = 32
            Width = 60
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object TPOldData: TdmkEditDateTimePicker
            Left = 140
            Top = 32
            Width = 112
            Height = 21
            Date = 40568.480364108800000000
            Time = 40568.480364108800000000
            Color = clWhite
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
          object EdOldSub: TdmkEdit
            Left = 76
            Top = 32
            Width = 60
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdOldTipo: TdmkEdit
            Left = 256
            Top = 32
            Width = 60
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdOldCarteira: TdmkEdit
            Left = 320
            Top = 32
            Width = 60
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
        end
      end
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 504
    Top = 440
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 504
    Top = 412
  end
  object DsFornecedores: TDataSource
    DataSet = QrFornecedores
    Left = 504
    Top = 300
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 652
    Top = 200
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrContasAfterScroll
    SQL.Strings = (
      'SELECT co.*, pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMEEMPRESA'
      'FROM contas co'
      'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo'
      'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano'
      'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa'
      'WHERE co.Terceiro=0'
      'AND co.Codigo>0'
      'ORDER BY co.Nome')
    Left = 476
    Top = 440
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrContasNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object QrContasNome3: TWideStringField
      FieldName = 'Nome3'
      Required = True
      Size = 50
    end
    object QrContasID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrContasSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrContasEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrContasCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContasDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContasMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContasExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrContasMensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrContasMensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrContasMensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrContasMenscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrContasMensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrContasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContasTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrContasExcel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrContasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContasUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrContasUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrContasNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Required = True
      Size = 50
    end
    object QrContasNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Required = True
      Size = 50
    end
    object QrContasNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Required = True
      Size = 50
    end
    object QrContasNOMEEMPRESA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrContasNOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Size = 50
    end
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCarteirasAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Nome, Fatura, Fechamento, Prazo, '
      'Tipo, ExigeNumCheque, ForneceI, Banco1, UsaTalao'
      'FROM carteiras '
      'WHERE ForneceI=:P0'
      'ORDER BY Nome')
    Left = 476
    Top = 412
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrCarteirasFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrCarteirasFechamento: TIntegerField
      FieldName = 'Fechamento'
    end
    object QrCarteirasPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCarteirasExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrCarteirasBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrCarteirasUsaTalao: TSmallintField
      FieldName = 'UsaTalao'
    end
  end
  object QrFatura: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(Data) Data '
      'FROM faturas'
      'WHERE Emissao=:P0')
    Left = 504
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFaturaData: TDateField
      FieldKind = fkInternalCalc
      FieldName = 'Data'
      Required = True
    end
  end
  object QrFornecedores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece1="V"'
      'OR Fornece2="V"'
      'OR Fornece3="V"'
      'OR Fornece4="V"'
      'OR Fornece5="V"'
      'OR Fornece6="V"'
      'OR Terceiro="V"'
      'ORDER BY NomeENTIDADE')
    Left = 476
    Top = 300
    object QrFornecedoresCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFornecedoresNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object QrVendedores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE 1'
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NomeENTIDADE')
    Left = 476
    Top = 356
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object StringField1: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsVendedores: TDataSource
    DataSet = QrVendedores
    Left = 504
    Top = 356
  end
  object DsAccounts: TDataSource
    DataSet = QrAccounts
    Left = 504
    Top = 384
  end
  object QrAccounts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,  '
      'CASE 1'
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NomeENTIDADE')
    Left = 476
    Top = 384
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object StringField2: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object QrFunci: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NomeENTIDADE')
    Left = 476
    Top = 328
    object QrFunciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFunciNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsFunci: TDataSource
    DataSet = QrFunci
    Left = 504
    Top = 328
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCliIntAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Account,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE CliInt <> 0'
      'ORDER BY NomeENTIDADE')
    Left = 624
    Top = 172
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 652
    Top = 172
  end
  object QrLct: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2, '
      '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, '
      'la.*, ct.Codigo CONTA, '
      'ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial '
      'ELSE em.Nome END NOMEEMPRESA, '
      'CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial '
      'ELSE cl.Nome END NOMECLIENTE,'
      'CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial '
      'ELSE fo.Nome END NOMEFORNECEDOR,'
      'ca.Nome NOMECARTEIRA, ca.Saldo SALDOCARTEIRA'
      'FROM VAR_LCT la'
      
        'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = ' +
        '0'
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo'
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      'ORDER BY la.Data, la.Controle')
    Left = 544
    Top = 8
    object QrLctData: TDateField
      FieldName = 'Data'
      Origin = 'lanctos.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'lanctos.Tipo'
    end
    object QrLctCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'lanctos.Carteira'
    end
    object QrLctAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      Origin = 'lanctos.Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrLctGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'lanctos.Genero'
    end
    object QrLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'lanctos.Descricao'
      Size = 25
    end
    object QrLctNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'lanctos.NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrLctDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'lanctos.Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'lanctos.Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'lanctos.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'lanctos.Documento'
      DisplayFormat = '000000; ; '
    end
    object QrLctSit: TIntegerField
      FieldName = 'Sit'
      Origin = 'lanctos.Sit'
    end
    object QrLctVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'lanctos.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'lanctos.Lk'
    end
    object QrLctFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'lanctos.FatID'
    end
    object QrLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'lanctos.FatParcela'
    end
    object QrLctCONTA: TIntegerField
      FieldName = 'CONTA'
      Origin = 'contas.Codigo'
    end
    object QrLctNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      FixedChar = True
      Size = 128
    end
    object QrLctNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Origin = 'contas.Nome'
      Size = 128
    end
    object QrLctNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Origin = 'subgrupos.Nome'
      Size = 4
    end
    object QrLctNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Origin = 'grupos.Nome'
      Size = 4
    end
    object QrLctNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Origin = 'conjuntos.Nome'
      Size = 4
    end
    object QrLctNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrLctAno: TFloatField
      FieldName = 'Ano'
    end
    object QrLctMENSAL: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrLctMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrLctBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLctLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrLctFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrLctSub: TSmallintField
      FieldName = 'Sub'
      DisplayFormat = '00; ; '
    end
    object QrLctCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrLctLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrLctPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLctSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrLctID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrLctMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLctcliente: TIntegerField
      FieldName = 'cliente'
    end
    object QrLctMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLctNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrLctNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrLctTIPOEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrLctNOMERELACIONADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMERELACIONADO'
      Size = 50
      Calculated = True
    end
    object QrLctOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrLctLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrLctMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLctATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrLctJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLctDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLctNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrLctVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLctAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrLctMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrLctProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrLctDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLctDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLctUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrLctUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLctID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrLctCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrLctFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLctICMS_P: TFloatField
      FieldName = 'ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrLctCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrLctCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLctNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrLctSALDOCARTEIRA: TFloatField
      FieldName = 'SALDOCARTEIRA'
    end
    object QrLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object DsLct: TDataSource
    DataSet = QrLct
    Left = 572
    Top = 8
  end
  object DsDeptos: TDataSource
    DataSet = QrDeptos
    Left = 652
    Top = 228
  end
  object QrDeptos: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrDeptosAfterScroll
    SQL.Strings = (
      'SELECT Codigo CODI_1, FLOOR(0) CODI_2, Nome NOME_1'
      'FROM intentocad'
      'ORDER BY NOME_1')
    Left = 624
    Top = 228
    object QrDeptosCODI_1: TIntegerField
      FieldName = 'CODI_1'
      Required = True
    end
    object QrDeptosCODI_2: TLargeintField
      FieldName = 'CODI_2'
      Required = True
    end
    object QrDeptosNOME_1: TWideStringField
      FieldName = 'NOME_1'
      Required = True
      Size = 100
    end
  end
  object QrForneceI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY NomeENTIDADE')
    Left = 476
    Top = 272
    object QrForneceICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceINOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsForneceI: TDataSource
    DataSet = QrForneceI
    Left = 504
    Top = 272
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT co.Codigo, co.Nome, '
      'pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO'
      'FROM contas co'
      'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo'
      'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano'
      'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa'
      'WHERE co.Nome LIKE :P0'
      'AND co.Codigo>0'
      'ORDER BY co.Nome')
    Left = 624
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'contas.Codigo'
    end
    object QrPesqNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'contas.Nome'
      Size = 50
    end
    object QrPesqNOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Size = 50
    end
    object QrPesqNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 50
    end
    object QrPesqNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 50
    end
    object QrPesqNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Size = 50
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 652
    Top = 144
  end
  object TbParcpagtos: TmySQLTable
    Database = Dmod.ZZDB
    TableName = 'parcpagtos'
    Left = 625
    Top = 85
    object TbParcpagtosParcela: TIntegerField
      FieldName = 'Parcela'
    end
    object TbParcpagtosData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object TbParcpagtosCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object TbParcpagtosDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object TbParcpagtosDoc: TLargeintField
      FieldName = 'Doc'
    end
    object TbParcpagtosMora: TFloatField
      FieldName = 'Mora'
      DisplayFormat = '#,###,##0.00'
    end
    object TbParcpagtosMulta: TFloatField
      FieldName = 'Multa'
      DisplayFormat = '#,###,##0.00'
    end
    object TbParcpagtosICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '#,###,##0.00'
    end
    object TbParcpagtosDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 30
    end
    object TbParcpagtosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
  end
  object DsParcPagtos: TDataSource
    DataSet = TbParcpagtos
    Left = 653
    Top = 85
  end
  object QrSoma: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT SUM(Credito+Debito) VALOR'
      'FROM parcpagtos')
    Left = 624
    Top = 113
    object QrSomaVALOR: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'VALOR'
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Account,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'OR Cliente2="V"'
      'OR Cliente3="V"'
      'OR Cliente4="V"'
      'OR Terceiro="V"'
      'ORDER BY NomeENTIDADE')
    Left = 624
    Top = 200
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object QrEventosCad: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrContasAfterScroll
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM eventoscad'
      'WHERE Ativo=1'
      'AND Entidade=:P0'
      'ORDER BY Nome')
    Left = 476
    Top = 468
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEventosCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEventosCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEventosCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsEventosCad: TDataSource
    DataSet = QrEventosCad
    Left = 504
    Top = 468
  end
  object QrIndiPag: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM indipag'
      'ORDER BY Nome'
      '')
    Left = 624
    Top = 256
    object QrIndiPagCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrIndiPagNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsIndiPag: TDataSource
    DataSet = QrIndiPag
    Left = 652
    Top = 256
  end
end
