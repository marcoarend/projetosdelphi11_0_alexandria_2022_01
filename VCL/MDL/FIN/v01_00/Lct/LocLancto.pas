unit LocLancto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ExtCtrls, StdCtrls, Buttons, Db, (*DBTables,*) UnMLAGeral,
  UnInternalConsts, UnGOTOy, mySQLDbTables, dmkGeral, ComCtrls;

type
  TFmLocLancto = class(TForm)
    Panel1: TPanel;
    PnConfirma1: TPanel;
    DBGLct: TDBGrid;
    DsLoc: TDataSource;
    LaInfo: TStaticText;
    BtLocaliza1: TBitBtn;
    BtAltera: TBitBtn;
    BtSoma: TBitBtn;
    BtInclui: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    QrLoc: TmySQLQuery;
    QrLocMes2: TLargeintField;
    QrLocAno: TFloatField;
    QrLocData: TDateField;
    QrLocTipo: TSmallintField;
    QrLocCarteira: TIntegerField;
    QrLocSub: TSmallintField;
    QrLocAutorizacao: TIntegerField;
    QrLocGenero: TIntegerField;
    QrLocDescricao: TWideStringField;
    QrLocNotaFiscal: TIntegerField;
    QrLocDebito: TFloatField;
    QrLocCredito: TFloatField;
    QrLocCompensado: TDateField;
    QrLocDocumento: TFloatField;
    QrLocSit: TIntegerField;
    QrLocVencimento: TDateField;
    QrLocLk: TIntegerField;
    QrLocFatID: TIntegerField;
    QrLocFatParcela: TIntegerField;
    QrLocID_Sub: TSmallintField;
    QrLocFatura: TWideStringField;
    QrLocBanco: TIntegerField;
    QrLocLocal: TIntegerField;
    QrLocCartao: TIntegerField;
    QrLocLinha: TIntegerField;
    QrLocOperCount: TIntegerField;
    QrLocLancto: TIntegerField;
    QrLocPago: TFloatField;
    QrLocFornecedor: TIntegerField;
    QrLocCliente: TIntegerField;
    QrLocMoraDia: TFloatField;
    QrLocMulta: TFloatField;
    QrLocProtesto: TDateField;
    QrLocDataCad: TDateField;
    QrLocDataAlt: TDateField;
    QrLocUserCad: TSmallintField;
    QrLocUserAlt: TSmallintField;
    QrLocDataDoc: TDateField;
    QrLocNivel: TIntegerField;
    QrLocNOMEGENERO: TWideStringField;
    QrLocNOMECARTEIRA: TWideStringField;
    QrLocMENSAL: TWideStringField;
    QrLocVendedor: TIntegerField;
    QrLocAccount: TIntegerField;
    QrLocControle: TIntegerField;
    QrLocID_Pgto: TIntegerField;
    QrLocMez: TIntegerField;
    QrLocCtrlIni: TIntegerField;
    QrLocFatID_Sub: TIntegerField;
    QrLocICMS_P: TFloatField;
    QrLocICMS_V: TFloatField;
    QrLocDuplicata: TWideStringField;
    QrLocCliInt: TIntegerField;
    QrLocDepto: TIntegerField;
    QrLocDescoPor: TIntegerField;
    QrLocForneceI: TIntegerField;
    QrLocQtde: TFloatField;
    QrLocEmitente: TWideStringField;
    QrLocAgencia: TIntegerField;
    QrLocContaCorrente: TWideStringField;
    QrLocCNPJCPF: TWideStringField;
    QrLocDescoVal: TFloatField;
    QrLocNFVal: TFloatField;
    QrLct: TmySQLQuery;
    DsLct: TDataSource;
    QrLctData: TDateField;
    QrLocDescoControle: TIntegerField;
    QrLocAntigo: TWideStringField;
    QrLocUnidade: TIntegerField;
    QrLocFatNum: TFloatField;
    QrLocCLIENTE_INTERNO: TIntegerField;
    PnConfirma2: TPanel;
    BitBtn1: TBitBtn;
    BtConfirma: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtLocaliza1Click(Sender: TObject);
    procedure QrLocCalcFields(DataSet: TDataSet);
    procedure DBGLctKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtSomaClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrLocAfterOpen(DataSet: TDataSet);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure DBGLctDblClick(Sender: TObject);
  private
    { Private declarations }
    //procedure AlteraRegistro(Altera: Integer);
    procedure SomaLinhas;

  public
    { Public declarations }
    FDTPDataIni: TDateTimePicker;
    FQrCarteiras, FQrLct: TmySQLQuery;
    FCliLoc: Integer;
    FConfirmou, FLocSohCliInt: Boolean;
  end;

var
  FmLocLancto: TFmLocLancto;

implementation

uses UnMyObjects, Principal, Module, LocDefs, ModuleFin, ModuleGeral;

{$R *.DFM}

procedure TFmLocLancto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  FQrLct := FmLocDefs.FQrLct;
  FDTPDataIni := FmLocDefs.FDTPDataIni;
end;

procedure TFmLocLancto.FormCreate(Sender: TObject);
begin
  if Screen.Width > 800 then DBGLct.Columns[4].Width := 246;
  //
  {FmLocLancto.FQrLct := FQrLct;
  FmLocLancto.FDTPDataIni := FDTPDataIni;
  FmLocLancto.ShowModal;
  FmLocLancto.Destroy;}
end;

procedure TFmLocLancto.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocLancto.BtLocaliza1Click(Sender: TObject);
  function LocalizaLancto(Lancto: Double): Boolean;
  begin
    if FQrLct.State = dsInactive then
      FQrLct.Open;
    if FQrLct.Locate('Controle', Lancto, []) then
    begin
      FmLocDefs.Close;
      Screen.Cursor := crDefault;
      FmLocLancto.Close;
      //
      Result := True
    end else Result := False;
  end;
var
  Lancto: Double;
begin
  if not DmodG.LocalizaClienteInterno(QrLocCLIENTE_INTERNO.Value,
    'Localiza��o n�o permitida! Voc� est� tentando localizar um lan�amento ' +
    'de um cliente interno diferente do que est� ' +
    'em gerenciamento no momento!' + #13#10 +
    //'Cliente interno gerenciado:' + IntToStr() + #13#10 +
    'Cliente interno do lan�amento:' + IntToStr(QrLocCLIENTE_INTERNO.Value)) then
      Exit;
  Screen.Cursor := crHourGlass;
  try
    if FDTPDataIni <> nil then
    begin
      if QrLocData.Value < FDTPDataIni.Date then
      begin
        FDTPDataIni.Date := QrLocData.Value;
        VAR_FL_DataIni   := QrLocData.Value;
        VAR_FL_DataFim   := Date;
      end else
        FDTPDataIni.Date := 1;
    end else FDTPDataIni.Date := 1;
    //
    VAR_LANCTO2 := QrLocControle.Value;
    Lancto      := QrLocControle.Value;
    //Sub         := QrLocSub.Value;
    if DmodFin.DefParams(FQrCarteiras, nil, True, 'TFmLocLancto.BtLocaliza1Click()') then
    begin
      if not DmodFin.LocCod(QrLocCarteira.Value, QrLocCarteira.Value,
      FQrCarteiras, 'TFmLocLancto.BtLocaliza1Click(') then
        Application.MessageBox(PChar('N�o foi poss�vel localizar a carteira ' +
        IntToStr(QrLocCarteira.Value) + '!' + #13#10 +
        'Verifique se esta carteira pertence realmente a empresa ' +
        VAR_LIB_EMPRESAS + '!'), 'Aviso', MB_OK+MB_ICONWARNING)
      else begin
        if not LocalizaLancto(Lancto) then
        begin
          if not LocalizaLancto(Lancto) then
            Application.MessageBox(PChar('N�o foi poss�vel localizar o ' +
            'lan�amento ' + FormatFloat('0', Lancto) + '!'), 'Aviso',
          MB_OK+MB_ICONWARNING)
        end;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLocLancto.QrLocCalcFields(DataSet: TDataSet);
begin
  if QrLocMes2.Value > 0 then
    QrLocMENSAL.Value := FormatFloat('00', QrLocMes2.Value)+'/'
    +Copy(FormatFloat('0000', QrLocAno.Value), 3, 2)
   else QrLocMENSAL.Value := CO_VAZIO;
end;

procedure TFmLocLancto.DBGLctDblClick(Sender: TObject);
begin
  if PnConfirma2.Visible then
    BtConfirmaClick(Self);
end;

procedure TFmLocLancto.DBGLctKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  //if key=13 then AlteraRegistro(1);
end;

procedure TFmLocLancto.BtSomaClick(Sender: TObject);
begin
  if BtAltera.Enabled = True then
  begin
    BtInclui.Enabled := False;
    BtAltera.Enabled := False;
    BtLocaliza1.Enabled := False;
    DBGLct.Options := DBGLct.Options + [dgMultiSelect];
  end else begin
    SomaLinhas;
    BtInclui.Enabled := True;
    BtAltera.Enabled := True;
    BtLocaliza1.Enabled := True;
    DBGLct.Options := DBGLct.Options - [dgMultiSelect];
  end;
end;

procedure TFmLocLancto.SomaLinhas;
var
  Debito, Credito: Double;
  i: Integer;
begin
  Debito := 0;
  Credito := 0;
  with DBGLct.DataSource.DataSet do
  for i:= 0 to DBGLct.SelectedRows.Count-1 do
  begin
    GotoBookmark(pointer(DBGLct.SelectedRows.Items[i]));
    Debito := Debito + QrLocDebito.Value;
    Credito := Credito + QrLocCredito.Value;
  end;
  Geral.MensagemBox('Resultado:' + #13#10 +
  FormatFloat('#,###,###,###,##0.00', Credito) + ' - ' +
  FormatFloat('#,###,###,###,##0.00', Debito) + ' = ' +
  FormatFloat('#,###,###,###,##0.00', Credito - Debito),
  'Mensagem', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmLocLancto.BtConfirmaClick(Sender: TObject);
begin
  FConfirmou := True;
  Close;
end;

procedure TFmLocLancto.BtIncluiClick(Sender: TObject);
begin
  //AlteraRegistro(0);
  //N�o da para fazer por cousa dos juros e multas e etc...
  //FmPrincipal.IncluiNovoLancto();
end;

procedure TFmLocLancto.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLocLancto.QrLocAfterOpen(DataSet: TDataSet);
begin
  if GOTOy.Registros(QrLoc) = 0 then
    LaInfo.Caption := 'N�o foram encontrados registros.'
    else if GOTOy.Registros(QrLoc) = 1 then
      LaInfo.Caption := 'Foi encontrado 1 registro.'
      else if GOTOy.Registros(QrLoc) > 1 then LaInfo.Caption :=
        'Foram encontrados '+IntToStr(GOTOy.Registros(QrLoc))+' registros.';
end;

procedure TFmLocLancto.BtAlteraClick(Sender: TObject);
begin
  //AlteraRegistro(0);
  //N�o da para fazer por cousa dos juros e multas e etc...
  //FmPrincipal.IncluiNovoLancto();
end;

end.
