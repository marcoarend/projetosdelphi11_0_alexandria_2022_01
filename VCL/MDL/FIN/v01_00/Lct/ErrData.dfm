object FmErrData: TFmErrData
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: Lan'#231'amentos Suspeitos'
  ClientHeight = 494
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 672
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Lan'#231'amentos Suspeitos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 700
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 701
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 620
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 89
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 4
      Width = 70
      Height = 13
      Caption = 'Cliente interno:'
    end
    object Label2: TLabel
      Left = 12
      Top = 44
      Width = 39
      Height = 13
      Caption = 'Carteira:'
    end
    object Label3: TLabel
      Left = 528
      Top = 44
      Width = 26
      Height = 13
      Caption = 'Data:'
    end
    object Label4: TLabel
      Left = 644
      Top = 44
      Width = 68
      Height = 13
      Caption = #218'ltimo Lan'#231'to:'
    end
    object SpeedButton1: TSpeedButton
      Left = 748
      Top = 60
      Width = 21
      Height = 21
      Caption = '?'
      OnClick = SpeedButton1Click
    end
    object EdEmpresa: TdmkEditCB
      Left = 12
      Top = 20
      Width = 44
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInt64
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdEmpresaChange
      OnEnter = EdEmpresaEnter
      OnExit = EdEmpresaExit
      DBLookupComboBox = CBEmpresa
    end
    object CBEmpresa: TdmkDBLookupComboBox
      Left = 56
      Top = 20
      Width = 713
      Height = 21
      KeyField = 'Filial'
      ListField = 'NOMEFILIAL'
      ListSource = DModG.DsEmpresas
      TabOrder = 1
      dmkEditCB = EdEmpresa
      UpdType = utYes
    end
    object EdCarteira: TdmkEditCB
      Left = 12
      Top = 60
      Width = 44
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInt64
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCarteira
    end
    object CBCarteira: TdmkDBLookupComboBox
      Left = 56
      Top = 60
      Width = 469
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCarteiras
      TabOrder = 3
      dmkEditCB = EdCarteira
      UpdType = utYes
    end
    object TPData: TdmkEditDateTimePicker
      Left = 528
      Top = 60
      Width = 112
      Height = 21
      Date = 40407.653280208330000000
      Time = 40407.653280208330000000
      TabOrder = 4
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object EdLancto: TdmkEdit
      Left = 644
      Top = 60
      Width = 101
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 137
    Width = 784
    Height = 309
    Align = alClient
    DataSource = DsErr
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Data'
        Width = 53
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Controle'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Descricao'
        Title.Caption = 'Descri'#231#227'o'
        Width = 164
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Credito'
        Title.Caption = 'Cr'#233'dito'
        Width = 68
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Debito'
        Title.Caption = 'D'#233'bito'
        Width = 68
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Documento'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LogCad'
        Title.Caption = 'Login lan'#231'ou'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DataCad'
        Title.Caption = 'Data lct.'
        Width = 53
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LogAlt'
        Title.Caption = 'Login alterou'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DataAlt'
        Title.Caption = 'Data alter.'
        Width = 53
        Visible = True
      end>
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM carteiras'
      'WHERE ForneceI=:P0')
    Left = 152
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 180
    Top = 64
  end
  object QrPesq1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(Controle) Controle'
      'FROM lanctos'
      'WHERE Carteira=:P0'
      'AND Data=:P1')
    Left = 212
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPesq1Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
  end
  object QrErr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT se1.Login LogCad, se2.Login LogAlt, lan.DataCad, lan.Data' +
        'Alt,'
      
        'lan.Data, lan.Controle, lan.Credito, lan.Debito, lan.Descricao, ' +
        'lan.Documento'
      'FROM lanctos lan'
      'LEFT JOIN senhas se1 ON se1.Numero=lan.UserCad'
      'LEFT JOIN senhas se2 ON se2.Numero=lan.UserAlt'
      'WHERE lan.Carteira=:P0'
      'AND lan.Data < :P1'
      'AND lan.Controle > :P2')
    Left = 244
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrErrLogCad: TWideStringField
      FieldName = 'LogCad'
      Required = True
      Size = 30
    end
    object QrErrLogAlt: TWideStringField
      FieldName = 'LogAlt'
      Required = True
      Size = 30
    end
    object QrErrDataCad: TDateField
      FieldName = 'DataCad'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrErrDataAlt: TDateField
      FieldName = 'DataAlt'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrErrData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrErrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrErrCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrErrDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrErrDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrErrDocumento: TFloatField
      FieldName = 'Documento'
    end
  end
  object DsErr: TDataSource
    DataSet = QrErr
    Left = 272
    Top = 64
  end
end
