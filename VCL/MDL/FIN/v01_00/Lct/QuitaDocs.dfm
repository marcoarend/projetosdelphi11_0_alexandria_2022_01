object FmQuitaDocs: TFmQuitaDocs
  Left = 487
  Top = 174
  Caption = 'FIN-LANCT-003 :: Quita'#231#227'o de Lan'#231'amento'
  ClientHeight = 391
  ClientWidth = 408
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 408
    Height = 229
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    ExplicitWidth = 393
    ExplicitHeight = 237
    object Label1: TLabel
      Left = 140
      Top = 140
      Width = 67
      Height = 13
      Caption = 'Defina a data:'
    end
    object Label2: TLabel
      Left = 252
      Top = 140
      Width = 58
      Height = 13
      Caption = 'Documento:'
    end
    object Label3: TLabel
      Left = 40
      Top = 8
      Width = 62
      Height = 13
      Caption = 'Lan'#231'amento:'
    end
    object Label4: TLabel
      Left = 40
      Top = 52
      Width = 39
      Height = 13
      Caption = 'Carteira:'
      Enabled = False
    end
    object Label5: TLabel
      Left = 160
      Top = 8
      Width = 36
      Height = 13
      Caption = 'Cr'#233'dito:'
      Enabled = False
    end
    object Label6: TLabel
      Left = 260
      Top = 8
      Width = 34
      Height = 13
      Caption = 'D'#233'bito:'
      Enabled = False
    end
    object Label7: TLabel
      Left = 40
      Top = 96
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object Label9: TLabel
      Left = 40
      Top = 140
      Width = 53
      Height = 13
      Caption = 'Nota fiscal:'
      Enabled = False
    end
    object Label15: TLabel
      Left = 40
      Top = 184
      Width = 95
      Height = 13
      Caption = 'Carteira de emiss'#227'o:'
    end
    object SpeedButton1: TSpeedButton
      Left = 330
      Top = 200
      Width = 23
      Height = 22
      Hint = 'Inclui item de carteira'
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object Label8: TLabel
      Left = 140
      Top = 184
      Width = 152
      Height = 13
      Caption = 'Ser'#225' criada outra emiss'#227'o!'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LaCredito: TDBEdit
      Left = 160
      Top = 24
      Width = 97
      Height = 21
      DataField = 'Credito'
      DataSource = DsLancto1
      Enabled = False
      TabOrder = 7
    end
    object LaDebito: TDBEdit
      Left = 260
      Top = 24
      Width = 93
      Height = 21
      DataField = 'Debito'
      DataSource = DsLancto1
      Enabled = False
      TabOrder = 8
    end
    object LaNF: TDBEdit
      Left = 40
      Top = 156
      Width = 97
      Height = 21
      DataField = 'NotaFiscal'
      DataSource = DsLancto1
      Enabled = False
      TabOrder = 9
    end
    object LaCartCod: TDBEdit
      Left = 40
      Top = 68
      Width = 62
      Height = 21
      DataField = 'Carteira'
      DataSource = DsLancto1
      Enabled = False
      TabOrder = 10
    end
    object LaCartNom: TDBEdit
      Left = 100
      Top = 68
      Width = 253
      Height = 21
      DataField = 'NOMECARTEIRA'
      DataSource = DsLancto1
      Enabled = False
      TabOrder = 11
    end
    object TPData1: TDateTimePicker
      Left = 140
      Top = 156
      Width = 109
      Height = 21
      Date = 37619.114129247700000000
      Time = 37619.114129247700000000
      TabOrder = 1
    end
    object EdDoc: TdmkEdit
      Left = 252
      Top = 156
      Width = 101
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnExit = EdDocExit
    end
    object EdDescricao: TdmkEdit
      Left = 40
      Top = 112
      Width = 313
      Height = 21
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnExit = EdDocExit
    end
    object EdLancto: TStaticText
      Left = 40
      Top = 24
      Width = 89
      Height = 21
      AutoSize = False
      BorderStyle = sbsSingle
      TabOrder = 3
    end
    object EdSub: TStaticText
      Left = 128
      Top = 24
      Width = 29
      Height = 21
      AutoSize = False
      BorderStyle = sbsSingle
      TabOrder = 4
    end
    object CBCarteira: TdmkDBLookupComboBox
      Left = 84
      Top = 200
      Width = 245
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCarteiras
      TabOrder = 5
      dmkEditCB = EdCarteira
      UpdType = utYes
    end
    object EdCarteira: TdmkEditCB
      Left = 40
      Top = 200
      Width = 41
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCarteira
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 408
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitLeft = -391
    ExplicitWidth = 784
    object GB_R: TGroupBox
      Left = 360
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 736
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 312
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 688
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 294
        Height = 32
        Caption = 'Quita'#231#227'o de Documento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 294
        Height = 32
        Caption = 'Quita'#231#227'o de Documento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 294
        Height = 32
        Caption = 'Quita'#231#227'o de Documento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 277
    Width = 408
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitLeft = -376
    ExplicitTop = 378
    ExplicitWidth = 784
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 404
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 780
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 321
    Width = 408
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitLeft = -376
    ExplicitTop = 375
    ExplicitWidth = 784
    object PnSaiDesis: TPanel
      Left = 262
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 638
      object BtDesiste: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
        NumGlyphs = 2
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 260
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 636
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        TabOrder = 0
        OnClick = BtConfirmaClick
        NumGlyphs = 2
      end
    end
  end
  object QrLct1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT la.Controle, la.Sub, la.Descricao, la.Credito, la.Debito,'
      'la.NotaFiscal, la.Documento, la.Vencimento, la.Carteira, Data,'
      'ca.Nome NOMECARTEIRA, ca.Tipo TIPOCARTEIRA ,'
      'ca.Banco CARTEIRABANCO, la.Cliente, la.Fornecedor, '
      'la.DataDoc, la.Nivel, la.CtrlIni, la.Vendedor, la.Account,'
      'la.Genero'
      'FROM lanctos la, Carteiras ca'
      'WHERE la.Controle=:P0'
      'AND la.Sub=:P1'
      'AND ca.Codigo=la.Carteira')
    Left = 136
    Top = 148
    ParamData = <
      item
        DataType = ftFloat
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLct1Sub: TSmallintField
      FieldName = 'Sub'
      Origin = 'DBMMONEY.lanctos.Sub'
    end
    object QrLct1Descricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'DBMMONEY.lanctos.Descricao'
      Size = 128
    end
    object QrLct1Credito: TFloatField
      FieldName = 'Credito'
      Origin = 'DBMMONEY.lanctos.Credito'
      DisplayFormat = '#,###,##0.00  '
    end
    object QrLct1Debito: TFloatField
      FieldName = 'Debito'
      Origin = 'DBMMONEY.lanctos.Debito'
      DisplayFormat = '#,###,##0.00  '
    end
    object QrLct1NotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'DBMMONEY.lanctos.NotaFiscal'
    end
    object QrLct1Documento: TFloatField
      FieldName = 'Documento'
      Origin = 'DBMMONEY.lanctos.Documento'
    end
    object QrLct1Vencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'DBMMONEY.lanctos.Vencimento'
    end
    object QrLct1Carteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'DBMMONEY.lanctos.Carteira'
    end
    object QrLct1Data: TDateField
      FieldName = 'Data'
      Origin = 'DBMMONEY.lanctos.Data'
    end
    object QrLct1NOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
    object QrLct1TIPOCARTEIRA: TIntegerField
      FieldName = 'TIPOCARTEIRA'
      Origin = 'DBMMONEY.carteiras.Tipo'
    end
    object QrLct1CARTEIRABANCO: TIntegerField
      FieldName = 'CARTEIRABANCO'
      Origin = 'DBMMONEY.carteiras.Banco'
    end
    object QrLct1Cliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'DBMMONEY.lanctos.Cliente'
    end
    object QrLct1Fornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Origin = 'DBMMONEY.lanctos.Fornecedor'
    end
    object QrLct1DataDoc: TDateField
      FieldName = 'DataDoc'
      Origin = 'DBMMONEY.lanctos.DataDoc'
    end
    object QrLct1Nivel: TIntegerField
      FieldName = 'Nivel'
      Origin = 'DBMMONEY.lanctos.Nivel'
    end
    object QrLct1Vendedor: TIntegerField
      FieldName = 'Vendedor'
      Origin = 'DBMMONEY.lanctos.Vendedor'
    end
    object QrLct1Account: TIntegerField
      FieldName = 'Account'
      Origin = 'DBMMONEY.lanctos.Account'
    end
    object QrLct1Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLct1CtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrLct1Genero: TIntegerField
      FieldName = 'Genero'
    end
  end
  object DsLancto1: TDataSource
    DataSet = QrLct1
    Left = 164
    Top = 148
  end
  object QrLct2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Tipo, Credito, Debito, Controle, Sub, Carteira'
      'FROM lanctos '
      'WHERE ID_Pgto=:P0 '
      'AND ID_Sub=:P1')
    Left = 136
    Top = 176
    ParamData = <
      item
        DataType = ftFloat
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLct2Tipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'DBMMONEY.lanctos.Tipo'
    end
    object QrLct2Credito: TFloatField
      FieldName = 'Credito'
      Origin = 'DBMMONEY.lanctos.Credito'
    end
    object QrLct2Debito: TFloatField
      FieldName = 'Debito'
      Origin = 'DBMMONEY.lanctos.Debito'
    end
    object QrLct2Sub: TSmallintField
      FieldName = 'Sub'
      Origin = 'DBMMONEY.lanctos.Sub'
    end
    object QrLct2Carteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'DBMMONEY.lanctos.Carteira'
    end
    object QrLct2Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsLancto2: TDataSource
    DataSet = QrLct2
    Left = 164
    Top = 176
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Banco, ForneceI'
      'FROM carteiras '
      'WHERE Tipo=2'
      'ORDER BY Nome')
    Left = 204
    Top = 240
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteirasBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 232
    Top = 240
  end
end
