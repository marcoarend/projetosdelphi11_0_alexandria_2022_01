unit LctMudaText;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmLctMudaText = class(TForm)
    Panel1: TPanel;
    EdAtual: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    EdNovo: TEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    PnSaiDesis: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FMuda: Boolean;
    FAtual, FNovo: String;
  end;

  var
  FmLctMudaText: TFmLctMudaText;

implementation

{$R *.DFM}

uses UnMyObjects;

procedure TFmLctMudaText.BtSaidaClick(Sender: TObject);
begin
  FMuda  := False;
  FAtual := '';
  FNovo  := '';
  Close;
end;

procedure TFmLctMudaText.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLctMudaText.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLctMudaText.BtOKClick(Sender: TObject);
begin
  FMuda  := False;
  FAtual := EdAtual.Text;
  FNovo  := EdNovo.Text;
  if (FAtual <> '') or (FNovo <> '') then FMuda := True;
  Close;
end;

procedure TFmLctMudaText.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stUpd;
  //
  FMuda := False;
end;

end.
