unit LctGerencia;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mySQLDbTables, ComCtrls, dmkDBGrid, MyDBCheck, DBCtrls,
  dmkDBLookupComboBox, dmkEditCB, dmkEdit, dmkEditDateTimePicker,
  stdctrls, UnMyVCLref, dmkGeral, dmkLabel, dmkRadioGroup, dmkMemo, dmkdbEdit,
  dmkCheckBox, DBGrids, UnMLAGeral, UnDmkEnums;

type
  TLctIndexType = (litControle_Sub, litControle, litAtrelado);
  TUnLctGerencia = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  Upd_Lct(QrAux, QrUpd: TmySQLQuery; SQLCampos, SQLIndex:
      array of String; ValCampos, ValIndex: array of Variant): Boolean;
  end;
var
  LctGer: TUnLctGerencia;

{
const
  strControle_Sub: array [0..1] of String = ('Controle', 'Sub');
  strControle    : array [0..0] of String = ('Controle');
  strAtrelado    : array [0..0] of String = ('Atrelado');
}

implementation

uses
  ModuleGeral, UMySQLModule, UnInternalConsts, UnMsgInt, ModuleFin, Module,
  UnMySQLCuringa, LctDuplic, QuitaDoc2;

{ TUnLctGerencia }

function TUnLctGerencia.Upd_Lct(QrAux, QrUpd: TmySQLQuery;
  SQLCampos, SQLIndex: array of String; ValCampos,
  ValIndex: array of Variant): Boolean;
var
  i, j, k: Integer;
  Valor: String;
begin
  Result := False;
  QrAux.Close;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('SELECT COUNT(*) Itens FROM ' + VAR_LCT);
  //QrAux.SQL.Add('WHERE Data <= ' + Geral.FDT(VAR_LCT_ENCERRADO, 1));
  QrAux.SQL.Add('WHERE Encerrado>0');
  j := High(SQLIndex);
  for i := Low(SQLIndex) to j do
  begin
    Valor := Geral.VariavelToString(ValIndex[i]);
    QrUpd.SQL.Add('AND ' + SQLIndex[i] + '=' + Valor);
  end;
  QrAux.Open;
  if QrAux.FieldByName('Itens').AsInteger > 0 then
  begin
    k := 0;
    j := High(SQLCampos);
    for i := Low(SQLCampos) to j do
    begin
      if LowerCase(SQLCampos[i]) = LowerCase('data')       then k := k + 1;
      if LowerCase(SQLCampos[i]) = LowerCase('tipo')       then k := k + 1;
      if LowerCase(SQLCampos[i]) = LowerCase('credito')    then k := k + 1;
      if LowerCase(SQLCampos[i]) = LowerCase('debito')     then k := k + 1;
      if LowerCase(SQLCampos[i]) = LowerCase('compensado') then k := k + 1;
      if LowerCase(SQLCampos[i]) = LowerCase('sit')        then k := k + 1;
      if LowerCase(SQLCampos[i]) = LowerCase('pago')       then k := k + 1;
      if LowerCase(SQLCampos[i]) = LowerCase('ativo')      then k := k + 1;
      if LowerCase(SQLCampos[i]) = LowerCase('cancelado')  then k := k + 1;
    end;
    if k > 0 then
    begin
      Geral.MensagemBox('Altera��o da tabela de lan�amentos cancelada!' + #13#10
      + 'Existem ' + IntToStr(k) + ' campos que n�o podem ser alterados, pois ' +
      IntToStr(QrAux.FieldByName('Itens').AsInteger) + ' lan�amentos pertencem ' +
      'a per�odos encerrados' +
      // (Data igual ou inferior a ' + Geral.FDT(VAR_DATA_ENCERRADO, 2) +
      '!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  //
  Result := UMyMod.SQLInsUpd(QrUpd, stUpd, VAR_LCT, False, SQLCampos, SQLIndex,
    ValCampos, ValIndex, True);
end;

end.
