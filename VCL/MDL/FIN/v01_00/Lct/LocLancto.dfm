object FmLocLancto: TFmLocLancto
  Left = 333
  Top = 177
  Caption = 'Localiza'#231#227'o de Lan'#231'amentos'
  ClientHeight = 536
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 440
    Align = alClient
    TabOrder = 0
    object DBGLct: TDBGrid
      Left = 1
      Top = 1
      Width = 790
      Height = 390
      Align = alClient
      DataSource = DsLoc
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGLctDblClick
      OnKeyDown = DBGLctKeyDown
      Columns = <
        item
          Expanded = False
          FieldName = 'Data'
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Documento'
          Title.Caption = 'Doc.'
          Width = 55
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECARTEIRA'
          Title.Caption = 'Carteira'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEGENERO'
          Title.Caption = 'Conta'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 170
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D'#233'bito'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Cr'#233'dito'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NotaFiscal'
          Title.Caption = 'N.F.'
          Width = 55
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Autorizacao'
          Title.Caption = 'Autoriz.'
          Width = 55
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MENSAL'
          Title.Caption = 'M'#234's'
          Width = 40
          Visible = True
        end>
    end
    object PnConfirma2: TPanel
      Left = 1
      Top = 391
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 1
      Visible = False
      object BitBtn1: TBitBtn
        Tag = 13
        Left = 689
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtConfirmaClick
        NumGlyphs = 2
      end
    end
  end
  object PnConfirma1: TPanel
    Left = 0
    Top = 488
    Width = 792
    Height = 48
    Align = alBottom
    TabOrder = 1
    object LaInfo: TStaticText
      Left = 16
      Top = 16
      Width = 265
      Height = 17
      AutoSize = False
      BorderStyle = sbsSunken
      TabOrder = 0
    end
    object BtLocaliza1: TBitBtn
      Tag = 22
      Left = 292
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Localizar'
      TabOrder = 1
      OnClick = BtLocaliza1Click
      NumGlyphs = 2
    end
    object BtAltera: TBitBtn
      Tag = 11
      Left = 476
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Altera banco atual'
      Caption = '&Altera'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtAlteraClick
      NumGlyphs = 2
    end
    object BtSoma: TBitBtn
      Tag = 254
      Left = 592
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Altera banco atual'
      Caption = '&Soma'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = BtSomaClick
      NumGlyphs = 2
    end
    object BtInclui: TBitBtn
      Tag = 10
      Left = 384
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Inclui novo banco'
      Caption = '&Inclui'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      Visible = False
      OnClick = BtIncluiClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 684
      Top = 1
      Width = 107
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 5
      object BtSaida: TBitBtn
        Tag = 13
        Left = 9
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Caption = 'Localiza'#231#227'o de Lan'#231'amentos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 790
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object DsLoc: TDataSource
    DataSet = QrLoc
    Left = 188
    Top = 72
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrLocAfterOpen
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2, '
      '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, '
      'la.*, '
      'co.Nome NOMEGENERO, ca.Nome NOMECARTEIRA'
      'FROM lanctos la, Contas co, Carteiras ca'
      'WHERE co.Codigo=la.Genero'
      'AND ca.Codigo=la.Carteira'
      '')
    Left = 160
    Top = 72
    object QrLocMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrLocAno: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'Ano'
    end
    object QrLocData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLocTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrLocCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrLocSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrLocAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrLocGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLocDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLocNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      DisplayFormat = '000000;-000000; '
    end
    object QrLocDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLocCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrLocDocumento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '000000;-000000; '
    end
    object QrLocSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLocVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLocLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLocFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLocID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrLocFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrLocBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLocLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrLocCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrLocLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrLocOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrLocLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrLocPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLocFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLocCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLocMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLocMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLocProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrLocDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrLocUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrLocDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLocNivel: TIntegerField
      FieldName = 'Nivel'
      Required = True
    end
    object QrLocNOMEGENERO: TWideStringField
      FieldName = 'NOMEGENERO'
      Required = True
      Size = 50
    end
    object QrLocNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Required = True
      Size = 100
    end
    object QrLocMENSAL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 100
      Calculated = True
    end
    object QrLocVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLocAccount: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Account'
      Calculated = True
    end
    object QrLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLocID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrLocMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLocCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrLocFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLocICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrLocICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrLocDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrLocCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLocDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLocDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrLocForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLocQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrLocEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrLocAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrLocContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrLocCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrLocDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrLocNFVal: TFloatField
      FieldName = 'NFVal'
    end
    object QrLocDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Required = True
    end
    object QrLocAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrLocUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrLocFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrLocCLIENTE_INTERNO: TIntegerField
      FieldName = 'CLIENTE_INTERNO'
    end
  end
  object QrLct: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data FROM lanctos')
    Left = 232
    Top = 264
    object QrLctData: TDateField
      FieldName = 'Data'
    end
  end
  object DsLct: TDataSource
    DataSet = QrLct
    Left = 260
    Top = 264
  end
end
