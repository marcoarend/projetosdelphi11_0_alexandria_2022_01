object FmLctDuplic: TFmLctDuplic
  Left = 256
  Top = 174
  Caption = 'FIN-LANCT-002 :: Duplica'#231#227'o de Lan'#231'amento'
  ClientHeight = 573
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 525
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 140
      Height = 40
      Caption = '&Insere assim mesmo'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 648
      Top = 1
      Width = 359
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 15
        Left = 210
        Top = 3
        Width = 140
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste de inserir'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
      object BtEstahEmLoop: TBitBtn
        Tag = 15
        Left = 6
        Top = 3
        Width = 203
        Height = 40
        Cursor = crHandPoint
        Caption = '&Parar sequ'#234'ncia de inclus'#245'es '
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Visible = False
        OnClick = BtEstahEmLoopClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Duplica'#231#227'o de Lan'#231'amento'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 1012
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 477
    Align = alClient
    TabOrder = 0
    object STNotaF: TStaticText
      Left = 1
      Top = 160
      Width = 1006
      Height = 17
      Align = alTop
      Alignment = taCenter
      Caption = 'STNotaF'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object STCheque: TStaticText
      Left = 1
      Top = 1
      Width = 1006
      Height = 17
      Align = alTop
      Alignment = taCenter
      Caption = 'STCheque'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object GradeCH: TDBGrid
      Left = 1
      Top = 18
      Width = 1006
      Height = 142
      Align = alTop
      DataSource = DModFin.DsDuplCH
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CHEQUE'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Compensado'
          Title.Caption = 'Compens.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Cr'#233'dito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D'#233'bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NotaFiscal'
          Title.Caption = 'N.F.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 280
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MES'
          Title.Caption = 'M'#234's'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TERCEIRO'
          Title.Caption = 'Fornecedor / Cliente'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECART'
          Title.Caption = 'Carteira'
          Width = 140
          Visible = True
        end>
    end
    object GradeNF: TDBGrid
      Left = 1
      Top = 177
      Width = 1006
      Height = 142
      Align = alTop
      DataSource = DModFin.DsDuplNF
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CHEQUE'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Compensado'
          Title.Caption = 'Compens.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Cr'#233'dito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D'#233'bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NotaFiscal'
          Title.Caption = 'N.F.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 280
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MES'
          Title.Caption = 'M'#234's'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TERCEIRO'
          Title.Caption = 'Fornecedor / Cliente'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECART'
          Title.Caption = 'Carteira'
          Width = 140
          Visible = True
        end>
    end
    object StaticText1: TStaticText
      Left = 1
      Top = 319
      Width = 1006
      Height = 17
      Align = alTop
      Alignment = taCenter
      Caption = 'Valox  X  Conta (Plano)  X  M'#234's de compet'#234'ncia'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
    end
    object GradeDU: TDBGrid
      Left = 1
      Top = 336
      Width = 1006
      Height = 140
      Align = alClient
      DataSource = DModFin.DsDuplVal
      TabOrder = 5
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CHEQUE'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Compensado'
          Title.Caption = 'Compens.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Caption = 'Cr'#233'dito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'D'#233'bito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NotaFiscal'
          Title.Caption = 'N.F.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 280
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MES'
          Title.Caption = 'M'#234's'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TERCEIRO'
          Title.Caption = 'Fornecedor / Cliente'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECART'
          Title.Caption = 'Carteira'
          Width = 140
          Visible = True
        end>
    end
  end
end
