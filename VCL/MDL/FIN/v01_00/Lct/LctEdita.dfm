object FmLctEdita: TFmLctEdita
  Left = 339
  Top = 185
  Caption = 'FIN-LANCT-005 :: Edi'#231#227'o de Lan'#231'amentos [A]'
  ClientHeight = 785
  ClientWidth = 904
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 737
    Width = 904
    Height = 48
    Align = alBottom
    TabOrder = 1
    object Label3: TLabel
      Left = 409
      Top = 4
      Width = 82
      Height = 13
      Caption = 'Compensado em:'
      Enabled = False
    end
    object Label4: TLabel
      Left = 525
      Top = 4
      Width = 15
      Height = 13
      Caption = 'Sit:'
      Enabled = False
    end
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 792
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
    object TPCompensado: TdmkEditDateTimePicker
      Left = 409
      Top = 20
      Width = 112
      Height = 21
      Date = 0.672523148103209700
      Time = 0.672523148103209700
      Enabled = False
      TabOrder = 2
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      QryCampo = 'Compensado'
      UpdCampo = 'Compensado'
      UpdType = utYes
    end
    object EdSit: TdmkEdit
      Left = 524
      Top = 20
      Width = 20
      Height = 21
      Alignment = taRightJustify
      Color = clMenu
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGray
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Sit'
      UpdCampo = 'Sit'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OldValor = '0'
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 904
    Height = 48
    Align = alTop
    Caption = 'Edi'#231#227'o de Lan'#231'amentos [A]'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 839
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 840
      Top = 1
      Width = 63
      Height = 17
      Align = alRight
      Caption = 'Altera'#231#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      UpdType = utYes
      SQLType = stUpd
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 904
    Height = 689
    Align = alClient
    TabOrder = 0
    object PainelDados: TPanel
      Left = 1
      Top = 1
      Width = 902
      Height = 504
      Align = alTop
      ParentBackground = False
      TabOrder = 0
      object LaFunci: TLabel
        Left = 496
        Top = 4
        Width = 6
        Height = 13
        Caption = '0'
        Visible = False
      end
      object Panel3: TPanel
        Left = 1
        Top = 1
        Width = 572
        Height = 502
        Align = alLeft
        TabOrder = 1
        object PnLancto1: TPanel
          Left = 1
          Top = 1
          Width = 570
          Height = 199
          Align = alTop
          TabOrder = 0
          object Label14: TLabel
            Left = 8
            Top = 2
            Width = 62
            Height = 13
            Caption = 'Lan'#231'amento:'
          end
          object SbCarteira: TSpeedButton
            Left = 540
            Top = 18
            Width = 21
            Height = 21
            Hint = 'Inclui item de carteira'
            Caption = '...'
            OnClick = SbCarteiraClick
          end
          object BtContas: TSpeedButton
            Left = 540
            Top = 58
            Width = 21
            Height = 21
            Hint = 'Inclui conta'
            Caption = '...'
            OnClick = BtContasClick
          end
          object Label1: TLabel
            Left = 8
            Top = 42
            Width = 99
            Height = 13
            Caption = 'Data do lan'#231'amento:'
          end
          object Label2: TLabel
            Left = 112
            Top = 42
            Width = 97
            Height = 13
            Caption = 'Conta [F7] pesquisa:'
          end
          object LaMes: TLabel
            Left = 8
            Top = 82
            Width = 23
            Height = 13
            Caption = 'M'#234's:'
            Enabled = False
          end
          object LaDeb: TLabel
            Left = 65
            Top = 82
            Width = 34
            Height = 13
            Caption = 'D'#233'bito:'
            Enabled = False
          end
          object LaCred: TLabel
            Left = 141
            Top = 82
            Width = 36
            Height = 13
            Caption = 'Cr'#233'dito:'
            Enabled = False
          end
          object LaNF: TLabel
            Left = 217
            Top = 82
            Width = 23
            Height = 13
            Caption = 'N.F.:'
          end
          object LaDoc: TLabel
            Left = 274
            Top = 82
            Width = 104
            Height = 13
            Caption = 'S'#233'rie  e docum. (CH) :'
          end
          object Label11: TLabel
            Left = 381
            Top = 82
            Width = 48
            Height = 13
            Caption = 'Duplicata:'
          end
          object LaVencimento: TLabel
            Left = 449
            Top = 82
            Width = 80
            Height = 13
            Caption = 'Vencimento [F6]:'
          end
          object LaCliInt: TLabel
            Left = 8
            Top = 122
            Width = 60
            Height = 13
            Caption = 'Condom'#237'nio:'
          end
          object LaCliente: TLabel
            Left = 8
            Top = 162
            Width = 35
            Height = 13
            Caption = 'Cliente:'
          end
          object LaFornecedor: TLabel
            Left = 292
            Top = 162
            Width = 57
            Height = 13
            Caption = 'Fornecedor:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object SpeedButton2: TSpeedButton
            Left = 539
            Top = 178
            Width = 22
            Height = 21
            Hint = 'Inclui conta'
            Caption = '...'
            OnClick = SpeedButton2Click
          end
          object LaForneceRN: TLabel
            Left = 352
            Top = 162
            Width = 85
            Height = 13
            Caption = '[F5] Raz'#227'o/Nome'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object LaForneceFA: TLabel
            Left = 444
            Top = 162
            Width = 101
            Height = 13
            Caption = '[F6] Fantasia/Apelido'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label15: TLabel
            Left = 96
            Top = 2
            Width = 256
            Height = 13
            Caption = 'Carteira (Extrato banc'#225'rio, emiss'#227'o banc'#225'ria ou caixa):'
          end
          object EdControle: TdmkEdit
            Left = 8
            Top = 18
            Width = 60
            Height = 21
            Alignment = taRightJustify
            Color = clInactiveCaption
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBackground
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Controle'
            UpdCampo = 'Controle'
            UpdType = utIdx
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdCarteira: TdmkEditCB
            Left = 96
            Top = 18
            Width = 48
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Carteira'
            UpdCampo = 'Carteira'
            UpdType = utIdx
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdCarteiraChange
            DBLookupComboBox = CBCarteira
            IgnoraDBLookupComboBox = False
          end
          object CBCarteira: TdmkDBLookupComboBox
            Left = 144
            Top = 18
            Width = 393
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCarteiras
            TabOrder = 4
            dmkEditCB = EdCarteira
            QryCampo = 'Carteira'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object CBGenero: TdmkDBLookupComboBox
            Left = 172
            Top = 58
            Width = 365
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsContas
            TabOrder = 7
            OnClick = CBGeneroClick
            OnKeyDown = CBGeneroKeyDown
            dmkEditCB = EdCBGenero
            QryCampo = 'Genero'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdCBGenero: TdmkEditCB
            Left = 117
            Top = 58
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Genero'
            UpdCampo = 'Genero'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdCBGeneroChange
            OnKeyDown = EdCBGeneroKeyDown
            DBLookupComboBox = CBGenero
            IgnoraDBLookupComboBox = False
          end
          object EdTPData: TdmkEditDateTimePicker
            Left = 8
            Top = 58
            Width = 106
            Height = 21
            Date = 40569.655720300900000000
            Time = 40569.655720300900000000
            TabOrder = 5
            OnClick = EdTPDataClick
            OnChange = EdTPDataChange
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            QryCampo = 'Data'
            UpdCampo = 'Data'
            UpdType = utIdx
          end
          object EdMes: TdmkEdit
            Left = 8
            Top = 98
            Width = 54
            Height = 21
            Alignment = taCenter
            Enabled = False
            TabOrder = 8
            FormatType = dmktfMesAno
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfLong
            HoraFormat = dmkhfShort
            QryCampo = 'Mez'
            UpdCampo = 'Mez'
            UpdType = utYes
            Obrigatorio = True
            PermiteNulo = False
            ValueVariant = Null
            OnExit = EdMesExit
          end
          object EdDeb: TdmkEdit
            Left = 65
            Top = 98
            Width = 73
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 9
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'Debito'
            UpdCampo = 'Debito'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnExit = EdDebExit
          end
          object EdCred: TdmkEdit
            Left = 141
            Top = 98
            Width = 73
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 10
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'Credito'
            UpdCampo = 'Credito'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnExit = EdCredExit
          end
          object EdNF: TdmkEdit
            Left = 217
            Top = 98
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 11
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 6
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000'
            QryCampo = 'NotaFiscal'
            UpdCampo = 'NotaFiscal'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdSerieCH: TdmkEdit
            Left = 276
            Top = 98
            Width = 41
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 12
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SerieCH'
            UpdCampo = 'SerieCH'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdDoc: TdmkEdit
            Left = 316
            Top = 98
            Width = 62
            Height = 21
            Alignment = taRightJustify
            TabOrder = 13
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 6
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000'
            QryCampo = 'Documento'
            UpdCampo = 'Documento'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdDocChange
            OnExit = EdDocExit
          end
          object EdDuplicata: TdmkEdit
            Left = 381
            Top = 98
            Width = 65
            Height = 21
            TabOrder = 14
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Duplicata'
            UpdCampo = 'Duplicata'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdTPVencto: TdmkEditDateTimePicker
            Left = 449
            Top = 98
            Width = 112
            Height = 21
            Date = 40569.672523148100000000
            Time = 40569.672523148100000000
            TabOrder = 15
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            QryCampo = 'Vencimento'
            UpdCampo = 'Vencimento'
            UpdType = utYes
          end
          object EdCliInt: TdmkEditCB
            Left = 8
            Top = 138
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 16
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CliInt'
            UpdCampo = 'CliInt'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdCliIntChange
            DBLookupComboBox = CBCliInt
            IgnoraDBLookupComboBox = False
          end
          object CBCliInt: TdmkDBLookupComboBox
            Left = 68
            Top = 138
            Width = 493
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsCliInt
            TabOrder = 17
            dmkEditCB = EdCliInt
            QryCampo = 'CliInt'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdCBCliente: TdmkEditCB
            Left = 8
            Top = 178
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 18
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Cliente'
            UpdCampo = 'Cliente'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBCliente
            IgnoraDBLookupComboBox = False
          end
          object CBCliente: TdmkDBLookupComboBox
            Left = 65
            Top = 178
            Width = 221
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsClientes
            TabOrder = 19
            dmkEditCB = EdCBCliente
            QryCampo = 'Cliente'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdCBFornece: TdmkEditCB
            Left = 292
            Top = 178
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 20
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Fornecedor'
            UpdCampo = 'Fornecedor'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBFornece
            IgnoraDBLookupComboBox = False
          end
          object CBFornece: TdmkDBLookupComboBox
            Left = 352
            Top = 178
            Width = 185
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsFornecedores
            TabOrder = 21
            dmkEditCB = EdCBFornece
            QryCampo = 'Fornecedor'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdSub: TdmkEdit
            Left = 68
            Top = 18
            Width = 13
            Height = 21
            Alignment = taRightJustify
            Color = clMenu
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGray
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Sub'
            UpdCampo = 'Sub'
            UpdType = utIdx
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OldValor = '0'
          end
          object EdTipo: TdmkEdit
            Left = 84
            Top = 18
            Width = 13
            Height = 21
            Alignment = taRightJustify
            Color = clMenu
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGray
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Tipo'
            UpdCampo = 'Tipo'
            UpdType = utIdx
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OldValor = '0'
          end
        end
        object PnDepto: TPanel
          Left = 1
          Top = 200
          Width = 570
          Height = 39
          Align = alTop
          TabOrder = 1
          Visible = False
          object LaDepto: TLabel
            Left = 8
            Top = 2
            Width = 177
            Height = 13
            Caption = 'Unidade habitacional do condom'#237'nio:'
          end
          object CBDepto: TdmkDBLookupComboBox
            Left = 68
            Top = 18
            Width = 493
            Height = 21
            KeyField = 'CODI_1'
            ListField = 'NOME_1'
            ListSource = DsDeptos
            TabOrder = 1
            dmkEditCB = EdCBDepto
            QryCampo = 'Depto'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdCBDepto: TdmkEditCB
            Left = 8
            Top = 18
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Depto'
            UpdCampo = 'Depto'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBDepto
            IgnoraDBLookupComboBox = False
          end
        end
        object PnForneceI: TPanel
          Left = 1
          Top = 239
          Width = 570
          Height = 37
          Align = alTop
          TabOrder = 2
          Visible = False
          object LaForneceI: TLabel
            Left = 8
            Top = 2
            Width = 172
            Height = 13
            Caption = 'Propriet'#225'rio da unidade habitacional:'
          end
          object CBForneceI: TdmkDBLookupComboBox
            Left = 68
            Top = 16
            Width = 493
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsForneceI
            TabOrder = 1
            dmkEditCB = EdCBForneceI
            QryCampo = 'ForneceI'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdCBForneceI: TdmkEditCB
            Left = 8
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'ForneceI'
            UpdCampo = 'ForneceI'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBForneceI
            IgnoraDBLookupComboBox = False
          end
        end
        object PnAccount: TPanel
          Left = 1
          Top = 276
          Width = 570
          Height = 39
          Align = alTop
          TabOrder = 3
          Visible = False
          object LaAccount: TLabel
            Left = 8
            Top = 2
            Width = 43
            Height = 13
            Caption = 'Account:'
          end
          object EdCBAccount: TdmkEditCB
            Left = 8
            Top = 18
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Account'
            UpdCampo = 'Account'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnKeyDown = EdCBAccountKeyDown
            DBLookupComboBox = CBAccount
            IgnoraDBLookupComboBox = False
          end
          object CBAccount: TdmkDBLookupComboBox
            Left = 68
            Top = 18
            Width = 493
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsAccounts
            TabOrder = 1
            OnKeyDown = CBAccountKeyDown
            dmkEditCB = EdCBAccount
            QryCampo = 'Account'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object PnLancto3: TPanel
          Left = 1
          Top = 439
          Width = 570
          Height = 62
          Align = alBottom
          TabOrder = 5
          object CkPesqNF: TCheckBox
            Left = 8
            Top = 4
            Width = 201
            Height = 17
            Caption = 'Pesquisar duplica'#231#227'o de nota fiscal.'
            Checked = True
            State = cbChecked
            TabOrder = 0
          end
          object CkPesqCH: TCheckBox
            Left = 221
            Top = 4
            Width = 201
            Height = 17
            Caption = 'Pesquisar duplica'#231#227'o de cheque.'
            Checked = True
            State = cbChecked
            TabOrder = 1
          end
          object CkCancelado: TdmkCheckBox
            Left = 8
            Top = 42
            Width = 433
            Height = 17
            Caption = 
              'Lan'#231'amento cancelado (cr'#233'dito e d'#233'bito ser'#227'o zerados e status se' +
              'r'#225' igual a cancelado).'
            TabOrder = 3
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
          object CkPesqVal: TCheckBox
            Left = 8
            Top = 23
            Width = 397
            Height = 17
            Caption = 
              'Pesquisa lan'#231'amentos cruzando o valor com a conta e o m'#234's de com' +
              'pet'#234'ncia.'
            Checked = True
            State = cbChecked
            TabOrder = 2
          end
        end
        object PnLancto2: TPanel
          Left = 1
          Top = 315
          Width = 570
          Height = 124
          Align = alClient
          TabOrder = 4
          object Label13: TLabel
            Left = 83
            Top = 2
            Width = 144
            Height = 13
            Caption = 'Descri'#231#227'o [F4], [F5], [F6], [F8]:'
          end
          object Label20: TLabel
            Left = 8
            Top = 2
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object LaEventosCad: TLabel
            Left = 8
            Top = 41
            Width = 37
            Height = 13
            Caption = 'Evento:'
          end
          object SbEventosCad: TSpeedButton
            Left = 540
            Top = 57
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbEventosCadClick
          end
          object Label60: TLabel
            Left = 8
            Top = 81
            Width = 121
            Height = 13
            Caption = 'Indica'#231#227'o de pagamento:'
          end
          object SbIndiPag: TSpeedButton
            Left = 540
            Top = 97
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbIndiPagClick
          end
          object EdDescricao: TdmkEdit
            Left = 83
            Top = 17
            Width = 478
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Descricao'
            UpdCampo = 'Descricao'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnKeyDown = EdDescricaoKeyDown
          end
          object EdQtde: TdmkEdit
            Left = 8
            Top = 17
            Width = 71
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'Qtde'
            UpdCampo = 'Qtde'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdEventosCad: TdmkEditCB
            Left = 8
            Top = 57
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBEventosCad
            IgnoraDBLookupComboBox = False
          end
          object CBEventosCad: TdmkDBLookupComboBox
            Left = 68
            Top = 57
            Width = 472
            Height = 21
            Color = clWhite
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsEventosCad
            TabOrder = 3
            dmkEditCB = EdEventosCad
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdIndiPag: TdmkEditCB
            Left = 8
            Top = 97
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'IndiPag'
            UpdCampo = 'IndiPag'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBIndiPag
            IgnoraDBLookupComboBox = False
          end
          object CBIndiPag: TdmkDBLookupComboBox
            Left = 68
            Top = 97
            Width = 472
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsIndiPag
            TabOrder = 5
            dmkEditCB = EdIndiPag
            QryCampo = 'IndiPag'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
      end
      object PnMaskPesq: TPanel
        Left = 573
        Top = 1
        Width = 328
        Height = 502
        Align = alClient
        TabOrder = 0
        object PnLink: TPanel
          Left = 1
          Top = 1
          Width = 326
          Height = 20
          Align = alTop
          BevelOuter = bvLowered
          Caption = 'Sem link'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          object Panel6: TPanel
            Left = 307
            Top = 1
            Width = 18
            Height = 18
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 0
            object SpeedButton3: TSpeedButton
              Left = 0
              Top = 0
              Width = 18
              Height = 18
              Caption = 'x'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Layout = blGlyphBottom
              ParentFont = False
              Visible = False
            end
          end
        end
        object Panel11: TPanel
          Left = 1
          Top = 333
          Width = 326
          Height = 168
          Align = alBottom
          TabOrder = 1
          object Label51: TLabel
            Left = 4
            Top = 4
            Width = 52
            Height = 13
            Caption = 'Sub-grupo:'
            FocusControl = DBEdit1
          end
          object Label52: TLabel
            Left = 4
            Top = 44
            Width = 32
            Height = 13
            Caption = 'Grupo:'
            FocusControl = DBEdit2
          end
          object Label53: TLabel
            Left = 4
            Top = 84
            Width = 45
            Height = 13
            Caption = 'Conjunto:'
            FocusControl = DBEdit3
          end
          object Label54: TLabel
            Left = 4
            Top = 124
            Width = 30
            Height = 13
            Caption = 'Plano:'
            FocusControl = DBEdit4
          end
          object DBEdit1: TDBEdit
            Left = 4
            Top = 20
            Width = 312
            Height = 21
            DataField = 'NOMESUBGRUPO'
            DataSource = DsContas
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 4
            Top = 60
            Width = 312
            Height = 21
            DataField = 'NOMEGRUPO'
            DataSource = DsContas
            TabOrder = 1
          end
          object DBEdit3: TDBEdit
            Left = 4
            Top = 100
            Width = 312
            Height = 21
            DataField = 'NOMECONJUNTO'
            DataSource = DsContas
            TabOrder = 2
          end
          object DBEdit4: TDBEdit
            Left = 4
            Top = 140
            Width = 312
            Height = 21
            DataField = 'NOMEPLANO'
            DataSource = DsContas
            TabOrder = 3
          end
        end
        object Panel5: TPanel
          Left = 1
          Top = 21
          Width = 326
          Height = 312
          Align = alClient
          TabOrder = 2
          object Panel12: TPanel
            Left = 1
            Top = 1
            Width = 324
            Height = 44
            Align = alTop
            BevelOuter = bvLowered
            TabOrder = 0
            object Label18: TLabel
              Left = 4
              Top = 4
              Width = 186
              Height = 13
              Caption = 'Digite parte da descri'#231#227'o (min. 3 letras):'
            end
            object EdLinkMask: TEdit
              Left = 4
              Top = 20
              Width = 309
              Height = 21
              TabOrder = 0
              OnChange = EdLinkMaskChange
              OnKeyDown = EdLinkMaskKeyDown
            end
          end
          object LLBPesq: TDBLookupListBox
            Left = 1
            Top = 45
            Width = 324
            Height = 264
            Align = alClient
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPesq
            TabOrder = 1
            OnDblClick = LLBPesqDblClick
            OnEnter = LLBPesqEnter
            OnExit = LLBPesqExit
            OnKeyDown = LLBPesqKeyDown
          end
        end
      end
    end
    object MeConfig: TMemo
      Left = 1
      Top = 553
      Width = 902
      Height = 135
      Align = alClient
      Enabled = False
      TabOrder = 1
    end
    object Panel4: TPanel
      Left = 1
      Top = 505
      Width = 902
      Height = 48
      Align = alTop
      TabOrder = 2
      object LaICMS_P: TLabel
        Left = 8
        Top = 4
        Width = 37
        Height = 13
        Caption = '% ICMS'
        Visible = False
      end
      object LaICMS_V: TLabel
        Left = 112
        Top = 4
        Width = 56
        Height = 13
        Caption = 'Valor ICMS:'
        Visible = False
      end
      object LaVendedor: TLabel
        Left = 500
        Top = 4
        Width = 49
        Height = 13
        Caption = 'Vendedor:'
        Visible = False
      end
      object Label10: TLabel
        Left = 216
        Top = 4
        Width = 58
        Height = 13
        Caption = 'Funcion'#225'rio:'
        Enabled = False
        Visible = False
      end
      object LaDataDoc: TLabel
        Left = 784
        Top = 4
        Width = 82
        Height = 13
        Caption = 'Data documento:'
      end
      object EdICMS_P: TdmkEdit
        Left = 8
        Top = 20
        Width = 101
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'ICMS_P'
        UpdCampo = 'ICMS_P'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnExit = EdICMS_PExit
        OnKeyDown = EdICMS_PKeyDown
      end
      object EdICMS_V: TdmkEdit
        Left = 112
        Top = 20
        Width = 101
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'ICMS_V'
        UpdCampo = 'ICMS_V'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnExit = EdICMS_VExit
        OnKeyDown = EdICMS_VKeyDown
      end
      object EdCBVendedor: TdmkEditCB
        Left = 500
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Vendedor'
        UpdCampo = 'Vendedor'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBVendedor
        IgnoraDBLookupComboBox = False
      end
      object CBVendedor: TdmkDBLookupComboBox
        Left = 560
        Top = 20
        Width = 217
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsVendedores
        TabOrder = 3
        dmkEditCB = EdCBVendedor
        QryCampo = 'Vendedor'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCBFunci: TdmkEditCB
        Left = 216
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBFunci
        IgnoraDBLookupComboBox = False
      end
      object CBFunci: TdmkDBLookupComboBox
        Left = 276
        Top = 20
        Width = 217
        Height = 21
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsFunci
        TabOrder = 5
        dmkEditCB = EdCBFunci
        QryCampo = 'UserCad'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdTPDataDoc: TdmkEditDateTimePicker
        Left = 784
        Top = 20
        Width = 112
        Height = 21
        Date = 39615.655720300900000000
        Time = 39615.655720300900000000
        TabOrder = 6
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DataDoc'
        UpdCampo = 'DataDoc'
        UpdType = utYes
      end
    end
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    BeforeScroll = QrCliIntBeforeScroll
    SQL.Strings = (
      'SELECT Codigo, Account,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE CliInt <> 0'
      'ORDER BY NomeENTIDADE')
    Left = 624
    Top = 172
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 652
    Top = 172
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 652
    Top = 200
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrClientesAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Account,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'OR Cliente2="V"'
      'OR Cliente3="V"'
      'OR Cliente4="V"'
      'OR Terceiro="V"'
      'ORDER BY NomeENTIDADE')
    Left = 624
    Top = 200
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object QrDeptos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo CODI_1, FLOOR(0) CODI_2, Nome NOME_1'
      'FROM intentocad'
      'ORDER BY NOME_1')
    Left = 624
    Top = 228
    object QrDeptosCODI_1: TIntegerField
      FieldName = 'CODI_1'
      Required = True
    end
    object QrDeptosCODI_2: TLargeintField
      FieldName = 'CODI_2'
      Required = True
    end
    object QrDeptosNOME_1: TWideStringField
      FieldName = 'NOME_1'
      Required = True
      Size = 100
    end
  end
  object DsDeptos: TDataSource
    DataSet = QrDeptos
    Left = 652
    Top = 228
  end
  object QrForneceI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY NomeENTIDADE')
    Left = 476
    Top = 272
    object QrForneceICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceINOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsForneceI: TDataSource
    DataSet = QrForneceI
    Left = 504
    Top = 272
  end
  object DsFornecedores: TDataSource
    DataSet = QrFornecedores
    Left = 504
    Top = 300
  end
  object QrFornecedores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece1="V"'
      'OR Fornece2="V"'
      'OR Fornece3="V"'
      'OR Fornece4="V"'
      'OR Fornece5="V"'
      'OR Fornece6="V"'
      'OR Terceiro="V"'
      'ORDER BY NomeENTIDADE')
    Left = 476
    Top = 300
    object QrFornecedoresCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFornecedoresNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object QrFunci: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NomeENTIDADE')
    Left = 476
    Top = 328
    object QrFunciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFunciNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsFunci: TDataSource
    DataSet = QrFunci
    Left = 504
    Top = 328
  end
  object DsVendedores: TDataSource
    DataSet = QrVendedores
    Left = 504
    Top = 356
  end
  object QrVendedores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE 1'
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NomeENTIDADE')
    Left = 476
    Top = 356
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object StringField1: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object QrAccounts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,  '
      'CASE 1'
      'WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NomeENTIDADE')
    Left = 476
    Top = 384
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object StringField2: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsAccounts: TDataSource
    DataSet = QrAccounts
    Left = 504
    Top = 384
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 504
    Top = 412
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCarteirasAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Nome, Fatura, Fechamento, Prazo, '
      'Tipo, ExigeNumCheque, ForneceI, Banco1, UsaTalao'
      'FROM carteiras '
      'WHERE ForneceI=:P0'
      'ORDER BY Nome')
    Left = 476
    Top = 412
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrCarteirasFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrCarteirasFechamento: TIntegerField
      FieldName = 'Fechamento'
    end
    object QrCarteirasPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCarteirasExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrCarteirasBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrCarteirasUsaTalao: TSmallintField
      FieldName = 'UsaTalao'
    end
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrContasAfterScroll
    SQL.Strings = (
      'SELECT co.*, pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMEEMPRESA'
      'FROM contas co'
      'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo'
      'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano'
      'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa'
      'WHERE co.Terceiro=0'
      'AND co.Codigo>0'
      'ORDER BY co.Nome')
    Left = 476
    Top = 440
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrContasNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 50
    end
    object QrContasNome3: TWideStringField
      FieldName = 'Nome3'
      Required = True
      Size = 50
    end
    object QrContasID: TWideStringField
      FieldName = 'ID'
      Size = 50
    end
    object QrContasSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrContasEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrContasCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
    object QrContasDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContasMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContasExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Size = 1
    end
    object QrContasMensdia: TSmallintField
      FieldName = 'Mensdia'
    end
    object QrContasMensdeb: TFloatField
      FieldName = 'Mensdeb'
    end
    object QrContasMensmind: TFloatField
      FieldName = 'Mensmind'
    end
    object QrContasMenscred: TFloatField
      FieldName = 'Menscred'
    end
    object QrContasMensminc: TFloatField
      FieldName = 'Mensminc'
    end
    object QrContasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContasTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrContasExcel: TWideStringField
      FieldName = 'Excel'
      Size = 6
    end
    object QrContasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContasUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrContasUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrContasNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Required = True
      Size = 50
    end
    object QrContasNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Required = True
      Size = 50
    end
    object QrContasNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Required = True
      Size = 50
    end
    object QrContasNOMEEMPRESA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEEMPRESA'
      Size = 100
    end
    object QrContasNOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 504
    Top = 440
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT co.Codigo, co.Nome, '
      'pl.Nome NOMEPLANO, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO'
      'FROM contas co'
      'LEFT JOIN subgrupos sg ON sg.Codigo=co.Subgrupo'
      'LEFT JOIN grupos    gr ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN plano     pl ON pl.Codigo=cj.Plano'
      'LEFT JOIN entidades cl ON cl.Codigo=co.Empresa'
      'WHERE co.Nome LIKE :P0'
      'AND co.Codigo>0'
      'ORDER BY co.Nome')
    Left = 624
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'contas.Codigo'
    end
    object QrPesqNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'contas.Nome'
      Size = 50
    end
    object QrPesqNOMEPLANO: TWideStringField
      FieldName = 'NOMEPLANO'
      Size = 50
    end
    object QrPesqNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Size = 50
    end
    object QrPesqNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Size = 50
    end
    object QrPesqNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Size = 50
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 652
    Top = 144
  end
  object QrEventosCad: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrContasAfterScroll
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM eventoscad'
      'WHERE Ativo=1'
      'AND Entidade=:P0'
      'ORDER BY Nome')
    Left = 476
    Top = 468
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEventosCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEventosCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEventosCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsEventosCad: TDataSource
    DataSet = QrEventosCad
    Left = 504
    Top = 468
  end
  object QrIndiPag: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM indipag'
      'ORDER BY Nome'
      '')
    Left = 624
    Top = 256
    object QrIndiPagCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrIndiPagNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsIndiPag: TDataSource
    DataSet = QrIndiPag
    Left = 652
    Top = 256
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdEventosCad
    Panel = PainelDados
    QryCampo = 'EventosCad'
    UpdCampo = 'EventosCad'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 24
    Top = 12
  end
end
