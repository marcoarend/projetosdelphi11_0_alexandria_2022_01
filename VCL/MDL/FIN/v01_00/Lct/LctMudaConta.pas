unit LctMudaConta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, DBCtrls, ComCtrls,
  Grids, DBGrids, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral;

type
  TFmLctMudaConta = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    BtCancela: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    Label15: TLabel;
    procedure BtOKClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    SohPermiteTipo1: Boolean;
    FContaSel: Integer;
  end;

  var
  FmLctMudaConta: TFmLctMudaConta;

implementation

{$R *.DFM}

uses UnMyObjects, Module;

procedure TFmLctMudaConta.BtOKClick(Sender: TObject);
begin
  FContaSel := Geral.IMV(EdConta.Text);
  Close;
end;

procedure TFmLctMudaConta.BtCancelaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLctMudaConta.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLctMudaConta.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLctMudaConta.FormCreate(Sender: TObject);
begin
  QrContas.Open;
  FContaSel := -1000;
end;

end.
