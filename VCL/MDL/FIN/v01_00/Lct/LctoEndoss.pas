unit LctoEndoss;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, mySQLDbTables, Mask, DBCtrls,
  Grids, DBGrids, ComCtrls, dmkGeral, dmkDBGrid, Variants, UnDmkEnums;

type
  TFmLctoEndoss = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel3: TPanel;
    QrLct: TmySQLQuery;
    DsLct: TDataSource;
    QrLctMes2: TLargeintField;
    QrLctAno: TFloatField;
    QrLctData: TDateField;
    QrLctTipo: TSmallintField;
    QrLctCarteira: TIntegerField;
    QrLctControle: TIntegerField;
    QrLctSub: TSmallintField;
    QrLctAutorizacao: TIntegerField;
    QrLctGenero: TIntegerField;
    QrLctQtde: TFloatField;
    QrLctDescricao: TWideStringField;
    QrLctNotaFiscal: TIntegerField;
    QrLctDebito: TFloatField;
    QrLctCredito: TFloatField;
    QrLctCompensado: TDateField;
    QrLctSerieCH: TWideStringField;
    QrLctDocumento: TFloatField;
    QrLctSit: TIntegerField;
    QrLctVencimento: TDateField;
    QrLctFatID: TIntegerField;
    QrLctFatID_Sub: TIntegerField;
    QrLctFatNum: TFloatField;
    QrLctFatParcela: TIntegerField;
    QrLctID_Pgto: TIntegerField;
    QrLctID_Quit: TIntegerField;
    QrLctID_Sub: TSmallintField;
    QrLctFatura: TWideStringField;
    QrLctEmitente: TWideStringField;
    QrLctBanco: TIntegerField;
    QrLctAgencia: TIntegerField;
    QrLctContaCorrente: TWideStringField;
    QrLctCNPJCPF: TWideStringField;
    QrLctLocal: TIntegerField;
    QrLctCartao: TIntegerField;
    QrLctLinha: TIntegerField;
    QrLctOperCount: TIntegerField;
    QrLctLancto: TIntegerField;
    QrLctPago: TFloatField;
    QrLctMez: TIntegerField;
    QrLctFornecedor: TIntegerField;
    QrLctCliente: TIntegerField;
    QrLctCliInt: TIntegerField;
    QrLctForneceI: TIntegerField;
    QrLctMoraDia: TFloatField;
    QrLctMulta: TFloatField;
    QrLctMoraVal: TFloatField;
    QrLctMultaVal: TFloatField;
    QrLctProtesto: TDateField;
    QrLctDataDoc: TDateField;
    QrLctCtrlIni: TIntegerField;
    QrLctNivel: TIntegerField;
    QrLctVendedor: TIntegerField;
    QrLctAccount: TIntegerField;
    QrLctICMS_P: TFloatField;
    QrLctICMS_V: TFloatField;
    QrLctDuplicata: TWideStringField;
    QrLctDepto: TIntegerField;
    QrLctDescoPor: TIntegerField;
    QrLctDescoVal: TFloatField;
    QrLctDescoControle: TIntegerField;
    QrLctUnidade: TIntegerField;
    QrLctNFVal: TFloatField;
    QrLctAntigo: TWideStringField;
    QrLctExcelGru: TIntegerField;
    QrLctDoc2: TWideStringField;
    QrLctCNAB_Sit: TSmallintField;
    QrLctTipoCH: TSmallintField;
    QrLctReparcel: TIntegerField;
    QrLctAtrelado: TIntegerField;
    QrLctPagMul: TFloatField;
    QrLctPagJur: TFloatField;
    QrLctSubPgto1: TIntegerField;
    QrLctMultiPgto: TIntegerField;
    QrLctProtocolo: TIntegerField;
    QrLctLk: TIntegerField;
    QrLctDataCad: TDateField;
    QrLctDataAlt: TDateField;
    QrLctUserCad: TIntegerField;
    QrLctUserAlt: TIntegerField;
    QrLctAlterWeb: TSmallintField;
    QrLctAtivo: TSmallintField;
    QrLctSerieNF: TWideStringField;
    QrLctCONTA: TIntegerField;
    QrLctPrazo: TSmallintField;
    QrLctBanco1: TIntegerField;
    QrLctAgencia1: TIntegerField;
    QrLctConta1: TWideStringField;
    QrLctNOMECONTA: TWideStringField;
    QrLctNOMESUBGRUPO: TWideStringField;
    QrLctNOMEGRUPO: TWideStringField;
    QrLctNOMECONJUNTO: TWideStringField;
    QrLctNOMEEMPRESA: TWideStringField;
    QrLctNOMECLIENTE: TWideStringField;
    QrLctNOMEFORNECEDOR: TWideStringField;
    QrLctNOMEFORNECEI: TWideStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    Label14: TLabel;
    DBEdit14: TDBEdit;
    Label15: TLabel;
    DBEdit15: TDBEdit;
    Label16: TLabel;
    DBEdit16: TDBEdit;
    Label17: TLabel;
    DBEdit17: TDBEdit;
    Label18: TLabel;
    DBEdit18: TDBEdit;
    Label19: TLabel;
    DBEdit19: TDBEdit;
    DBEdit01: TDBEdit;
    Label20: TLabel;
    Label21: TLabel;
    DBEdit02: TDBEdit;
    DBEdit03: TDBEdit;
    Label22: TLabel;
    Label23: TLabel;
    DBEdit04: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    Label24: TLabel;
    DBEdit22: TDBEdit;
    Label25: TLabel;
    DBEdit23: TDBEdit;
    QrLctoEndossaN: TmySQLQuery;
    DsLctoEndossaN: TDataSource;
    QrLctoEndossaNData: TDateField;
    QrLctoEndossaNTipo: TSmallintField;
    QrLctoEndossaNCarteira: TIntegerField;
    QrLctoEndossaNAutorizacao: TIntegerField;
    QrLctoEndossaNGenero: TIntegerField;
    QrLctoEndossaNDescricao: TWideStringField;
    QrLctoEndossaNNotaFiscal: TIntegerField;
    QrLctoEndossaNDebito: TFloatField;
    QrLctoEndossaNCredito: TFloatField;
    QrLctoEndossaNCompensado: TDateField;
    QrLctoEndossaNDocumento: TFloatField;
    QrLctoEndossaNSit: TIntegerField;
    QrLctoEndossaNVencimento: TDateField;
    QrLctoEndossaNLk: TIntegerField;
    QrLctoEndossaNFatID: TIntegerField;
    QrLctoEndossaNFatParcela: TIntegerField;
    QrLctoEndossaNCONTA: TIntegerField;
    QrLctoEndossaNNOMECONTA: TWideStringField;
    QrLctoEndossaNNOMEEMPRESA: TWideStringField;
    QrLctoEndossaNNOMESUBGRUPO: TWideStringField;
    QrLctoEndossaNNOMEGRUPO: TWideStringField;
    QrLctoEndossaNNOMECONJUNTO: TWideStringField;
    QrLctoEndossaNNOMESIT: TWideStringField;
    QrLctoEndossaNAno: TFloatField;
    QrLctoEndossaNMENSAL: TWideStringField;
    QrLctoEndossaNMENSAL2: TWideStringField;
    QrLctoEndossaNBanco: TIntegerField;
    QrLctoEndossaNLocal: TIntegerField;
    QrLctoEndossaNFatura: TWideStringField;
    QrLctoEndossaNSub: TSmallintField;
    QrLctoEndossaNCartao: TIntegerField;
    QrLctoEndossaNLinha: TIntegerField;
    QrLctoEndossaNPago: TFloatField;
    QrLctoEndossaNSALDO: TFloatField;
    QrLctoEndossaNID_Sub: TSmallintField;
    QrLctoEndossaNMez: TIntegerField;
    QrLctoEndossaNFornecedor: TIntegerField;
    QrLctoEndossaNcliente: TIntegerField;
    QrLctoEndossaNMoraDia: TFloatField;
    QrLctoEndossaNNOMECLIENTE: TWideStringField;
    QrLctoEndossaNNOMEFORNECEDOR: TWideStringField;
    QrLctoEndossaNTIPOEM: TWideStringField;
    QrLctoEndossaNNOMERELACIONADO: TWideStringField;
    QrLctoEndossaNOperCount: TIntegerField;
    QrLctoEndossaNLancto: TIntegerField;
    QrLctoEndossaNMulta: TFloatField;
    QrLctoEndossaNATRASO: TFloatField;
    QrLctoEndossaNJUROS: TFloatField;
    QrLctoEndossaNDataDoc: TDateField;
    QrLctoEndossaNNivel: TIntegerField;
    QrLctoEndossaNVendedor: TIntegerField;
    QrLctoEndossaNAccount: TIntegerField;
    QrLctoEndossaNMes2: TLargeintField;
    QrLctoEndossaNProtesto: TDateField;
    QrLctoEndossaNDataCad: TDateField;
    QrLctoEndossaNDataAlt: TDateField;
    QrLctoEndossaNUserCad: TSmallintField;
    QrLctoEndossaNUserAlt: TSmallintField;
    QrLctoEndossaNControle: TIntegerField;
    QrLctoEndossaNID_Pgto: TIntegerField;
    QrLctoEndossaNCtrlIni: TIntegerField;
    QrLctoEndossaNFatID_Sub: TIntegerField;
    QrLctoEndossaNICMS_P: TFloatField;
    QrLctoEndossaNICMS_V: TFloatField;
    QrLctoEndossaNDuplicata: TWideStringField;
    QrLctoEndossaNCOMPENSADO_TXT: TWideStringField;
    QrLctoEndossaNCliInt: TIntegerField;
    QrLctoEndossaNDepto: TIntegerField;
    QrLctoEndossaNDescoPor: TIntegerField;
    QrLctoEndossaNPrazo: TSmallintField;
    QrLctoEndossaNForneceI: TIntegerField;
    QrLctoEndossaNQtde: TFloatField;
    QrLctoEndossaNEmitente: TWideStringField;
    QrLctoEndossaNAgencia: TIntegerField;
    QrLctoEndossaNContaCorrente: TWideStringField;
    QrLctoEndossaNCNPJCPF: TWideStringField;
    QrLctoEndossaNDescoVal: TFloatField;
    QrLctoEndossaNDescoControle: TIntegerField;
    QrLctoEndossaNUnidade: TIntegerField;
    QrLctoEndossaNNFVal: TFloatField;
    QrLctoEndossaNAntigo: TWideStringField;
    QrLctoEndossaNExcelGru: TIntegerField;
    QrLctoEndossaNSerieCH: TWideStringField;
    QrLctoEndossaNSERIE_CHEQUE: TWideStringField;
    QrLctoEndossaNDoc2: TWideStringField;
    QrLctoEndossaNNOMEFORNECEI: TWideStringField;
    QrLctoEndossaNMoraVal: TFloatField;
    QrLctoEndossaNMultaVal: TFloatField;
    QrLctoEndossaNCNAB_Sit: TSmallintField;
    QrLctoEndossaNBanco1: TIntegerField;
    QrLctoEndossaNAgencia1: TIntegerField;
    QrLctoEndossaNConta1: TWideStringField;
    QrLctoEndossaNTipoCH: TSmallintField;
    QrLctoEndossaNFatNum: TFloatField;
    QrLctoEndossaNSerieNF: TWideStringField;
    QrLctoEndossaNID: TIntegerField;
    QrLctoEndossaNValor: TFloatField;
    QrLctoEndossaNOriCtrl: TIntegerField;
    QrLctoEndossaNOriSub: TIntegerField;
    QrLE2: TmySQLQuery;
    QrLE2Controle: TIntegerField;
    QrLE2Sub: TIntegerField;
    QrLE2ID: TIntegerField;
    QrLE2OriCtrl: TIntegerField;
    QrLE2OriSub: TIntegerField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    dmkDBGLct: TdmkDBGrid;
    PainelConfirma: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtRefresh: TBitBtn;
    TabSheet2: TTabSheet;
    dmkDBGrid1: TdmkDBGrid;
    QrLctoEndossaD: TmySQLQuery;
    DsLctoEndossaD: TDataSource;
    QrLctoEndossaDData: TDateField;
    QrLctoEndossaDTipo: TSmallintField;
    QrLctoEndossaDCarteira: TIntegerField;
    QrLctoEndossaDAutorizacao: TIntegerField;
    QrLctoEndossaDGenero: TIntegerField;
    QrLctoEndossaDDescricao: TWideStringField;
    QrLctoEndossaDNotaFiscal: TIntegerField;
    QrLctoEndossaDDebito: TFloatField;
    QrLctoEndossaDCredito: TFloatField;
    QrLctoEndossaDCompensado: TDateField;
    QrLctoEndossaDDocumento: TFloatField;
    QrLctoEndossaDSit: TIntegerField;
    QrLctoEndossaDVencimento: TDateField;
    QrLctoEndossaDLk: TIntegerField;
    QrLctoEndossaDFatID: TIntegerField;
    QrLctoEndossaDFatParcela: TIntegerField;
    QrLctoEndossaDCONTA: TIntegerField;
    QrLctoEndossaDNOMECONTA: TWideStringField;
    QrLctoEndossaDNOMEEMPRESA: TWideStringField;
    QrLctoEndossaDNOMESUBGRUPO: TWideStringField;
    QrLctoEndossaDNOMEGRUPO: TWideStringField;
    QrLctoEndossaDNOMECONJUNTO: TWideStringField;
    QrLctoEndossaDNOMESIT: TWideStringField;
    QrLctoEndossaDAno: TFloatField;
    QrLctoEndossaDMENSAL: TWideStringField;
    QrLctoEndossaDMENSAL2: TWideStringField;
    QrLctoEndossaDBanco: TIntegerField;
    QrLctoEndossaDLocal: TIntegerField;
    QrLctoEndossaDFatura: TWideStringField;
    QrLctoEndossaDSub: TSmallintField;
    QrLctoEndossaDCartao: TIntegerField;
    QrLctoEndossaDLinha: TIntegerField;
    QrLctoEndossaDPago: TFloatField;
    QrLctoEndossaDSALDO: TFloatField;
    QrLctoEndossaDID_Sub: TSmallintField;
    QrLctoEndossaDMez: TIntegerField;
    QrLctoEndossaDFornecedor: TIntegerField;
    QrLctoEndossaDcliente: TIntegerField;
    QrLctoEndossaDMoraDia: TFloatField;
    QrLctoEndossaDNOMECLIENTE: TWideStringField;
    QrLctoEndossaDNOMEFORNECEDOR: TWideStringField;
    QrLctoEndossaDTIPOEM: TWideStringField;
    QrLctoEndossaDNOMERELACIONADO: TWideStringField;
    QrLctoEndossaDOperCount: TIntegerField;
    QrLctoEndossaDLancto: TIntegerField;
    QrLctoEndossaDMulta: TFloatField;
    QrLctoEndossaDATRASO: TFloatField;
    QrLctoEndossaDJUROS: TFloatField;
    QrLctoEndossaDDataDoc: TDateField;
    QrLctoEndossaDNivel: TIntegerField;
    QrLctoEndossaDVendedor: TIntegerField;
    QrLctoEndossaDAccount: TIntegerField;
    QrLctoEndossaDMes2: TLargeintField;
    QrLctoEndossaDProtesto: TDateField;
    QrLctoEndossaDDataCad: TDateField;
    QrLctoEndossaDDataAlt: TDateField;
    QrLctoEndossaDUserCad: TSmallintField;
    QrLctoEndossaDUserAlt: TSmallintField;
    QrLctoEndossaDControle: TIntegerField;
    QrLctoEndossaDID_Pgto: TIntegerField;
    QrLctoEndossaDCtrlIni: TIntegerField;
    QrLctoEndossaDFatID_Sub: TIntegerField;
    QrLctoEndossaDICMS_P: TFloatField;
    QrLctoEndossaDICMS_V: TFloatField;
    QrLctoEndossaDDuplicata: TWideStringField;
    QrLctoEndossaDCOMPENSADO_TXT: TWideStringField;
    QrLctoEndossaDCliInt: TIntegerField;
    QrLctoEndossaDDepto: TIntegerField;
    QrLctoEndossaDDescoPor: TIntegerField;
    QrLctoEndossaDPrazo: TSmallintField;
    QrLctoEndossaDForneceI: TIntegerField;
    QrLctoEndossaDQtde: TFloatField;
    QrLctoEndossaDEmitente: TWideStringField;
    QrLctoEndossaDAgencia: TIntegerField;
    QrLctoEndossaDContaCorrente: TWideStringField;
    QrLctoEndossaDCNPJCPF: TWideStringField;
    QrLctoEndossaDDescoVal: TFloatField;
    QrLctoEndossaDDescoControle: TIntegerField;
    QrLctoEndossaDUnidade: TIntegerField;
    QrLctoEndossaDNFVal: TFloatField;
    QrLctoEndossaDAntigo: TWideStringField;
    QrLctoEndossaDExcelGru: TIntegerField;
    QrLctoEndossaDSerieCH: TWideStringField;
    QrLctoEndossaDSERIE_CHEQUE: TWideStringField;
    QrLctoEndossaDDoc2: TWideStringField;
    QrLctoEndossaDNOMEFORNECEI: TWideStringField;
    QrLctoEndossaDMoraVal: TFloatField;
    QrLctoEndossaDMultaVal: TFloatField;
    QrLctoEndossaDCNAB_Sit: TSmallintField;
    QrLctoEndossaDBanco1: TIntegerField;
    QrLctoEndossaDAgencia1: TIntegerField;
    QrLctoEndossaDConta1: TWideStringField;
    QrLctoEndossaDTipoCH: TSmallintField;
    QrLctoEndossaDFatNum: TFloatField;
    QrLctoEndossaDSerieNF: TWideStringField;
    QrLctoEndossaDID: TIntegerField;
    QrLctoEndossaDValor: TFloatField;
    QrLctoEndossaDOriCtrl: TIntegerField;
    QrLctoEndossaDOriSub: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure QrLctoEndossaNCalcFields(DataSet: TDataSet);
    procedure QrLctoEndossaDCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReopenFlacto();
    procedure AtualizaEndossadoEEndossantes();
  public
    { Public declarations }
    //Definir ao criar !!
    FControle, FSub: Integer;
    FTPDataIni: TDateTimePicker;
    FCarteiras, FLct: TmySQLQuery;
    // FIM definir ao criar

    procedure ReopenLancto(EndossaN, EndossaD: Integer; ShowBestGrid: Boolean);
  end;

  var
  FmLctoEndoss: TFmLctoEndoss;

implementation

uses UnFinanceiro, UnInternalConsts, GetValor, UMySQLModule, Module, UnMyObjects,
  MyDBCheck, ModuleFin;

{$R *.DFM}

procedure TFmLctoEndoss.BtAlteraClick(Sender: TObject);
var
  Valor: Variant;
  ID: Integer;
begin
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
  Valor, 2, 0, '', '', True, 'Valor do endosso', 'Informe o valor: ',
  0, Valor) then
  begin
    ID := QrLctoEndossaNID.Value;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'lctoendoss', False, [
    'Valor'], ['ID'], [Valor], [ID], True) then
    begin
      AtualizaEndossadoEEndossantes();
      ReopenLancto(ID, QrLctoEndossaDID.Value, False);
    end;
  end;
  ReopenFlacto();
end;

procedure TFmLctoEndoss.BtExcluiClick(Sender: TObject);
begin
  QrLE2.Close;
  QrLE2.Params := QrLctoEndossaN.Params;
  QrLE2.Open;
  if DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrLctoEndossaN, TDBGrid(dmkDBGLct),
  'lctoendoss', ['ID'], ['ID'], istPergunta, '') > 0 then
  begin
    while not QrLE2.Eof do
    begin
      DmodFin.AtualizaEndossos(QrLE2Controle.Value,
        QrLE2Sub.Value, QrLE2OriCtrl.Value, QrLE2OriSub.Value);
      //
      QrLE2.Next;
    end;
  end;
  ReopenFlacto();
end;

procedure TFmLctoEndoss.BtIncluiClick(Sender: TObject);
var
  Valor: Variant;
  ID, Controle, Sub, OriCtrl, OriSub: Integer;
  Credito, Debito: Double;
begin
  Credito := QrLctCredito.Value;
  Debito  := QrLctDebito.Value;
  if (Credito > 0) and (Debito > 0) then
  begin
    Geral.MensagemBox('O endosso s� � permitido em lan�amentos s� de ' +
    'd�bito ou s�e de credito!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  VAR_LOC_LCTO_TRUE := False;
  VAR_LOC_LCTO_CTRL := 0;
  VAR_LOC_LCTO_SUB  := 0;
  //
  UFinanceiro.LocalizarLancamento(FTPDataIni, FCarteiras, FLct, True, 1);
  //
  if VAR_LOC_LCTO_TRUE then
  begin
    if VAR_LOC_LCTO_CRED > 0 then
    begin
      if Credito > 0  then
      begin
        Geral.MensagemBox('O endossante de cr�dito n�o pode ' +
        'endossar outro cr�dito!', 'Aviso', MB_OK+MB_ICONWARNING);
        Exit;
      end;
      Valor := VAR_LOC_LCTO_CRED;
    end else begin
      Valor := VAR_LOC_LCTO_DEBI;
      if Debito > 0  then
      begin
        Geral.MensagemBox('O endossante de d�dito n�o pode ' +
        'endossar outro d�bito!', 'Aviso', MB_OK+MB_ICONWARNING);
        Exit;
      end;
    end;
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
    Valor, 2, 0, '', '', True, 'Valor do endosso', 'Informe o valor: ',
    0, Valor) then
    begin
      Controle := FControle;
      Sub      := FSub;
      OriCtrl  := VAR_LOC_LCTO_CTRL;
      OriSub   := VAR_LOC_LCTO_SUB;
      ID := UMyMod.BuscaEmLivreY_Def('lctoendoss', 'ID', stIns, 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'lctoendoss', False, [
      'Controle', 'Sub', 'OriCtrl', 'OriSub', 'Valor'], ['ID'], [
      Controle, Sub, OriCtrl, OriSub, Valor], [ID], True) then
      begin
        DmodFin.AtualizaEndossos(Controle, Sub, OriCtrl, OriSub);
        ReopenLancto(ID, QrLctoEndossaDID.Value, False);
      end;
    end;
  end;
  ReopenFlacto();
end;

procedure TFmLctoEndoss.BtRefreshClick(Sender: TObject);
begin
  AtualizaEndossadoEEndossantes();
end;

procedure TFmLctoEndoss.AtualizaEndossadoEEndossantes();
begin
  Screen.Cursor := crHourGlass;
  try
    DmodFin.AtualizaEndossos(QrLctControle.Value, QrLctSub.Value, 0, 0);
    ReopenFlacto();
    QrLctoEndossaN.First;
    while not QrLctoEndossaN.Eof do
    begin
      DmodFin.AtualizaEndossos(0, 0, QrLctoEndossaNControle.Value, QrLctoEndossaNSub.Value);
      QrLctoEndossaN.Next;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLctoEndoss.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLctoEndoss.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLctoEndoss.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.DefineTituloDBGrid(TDBGrid(dmkDBGLct), 'FatNum', VAR_TITULO_FATNUM);
end;

procedure TFmLctoEndoss.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLctoEndoss.QrLctoEndossaDCalcFields(DataSet: TDataSet);
begin
  if QrLctoEndossaDMes2.Value > 0 then
    QrLctoEndossaDMENSAL.Value := FormatFloat('00', QrLctoEndossaDMes2.Value)+'/'
    +Copy(FormatFloat('0000', QrLctoEndossaDAno.Value), 3, 2)
   else QrLctoEndossaDMENSAL.Value := CO_VAZIO;
  if QrLctoEndossaDMes2.Value > 0 then
    QrLctoEndossaDMENSAL2.Value := FormatFloat('0000', QrLctoEndossaDAno.Value)+'/'+
    FormatFloat('00', QrLctoEndossaDMes2.Value)+'/01'
   else QrLctoEndossaDMENSAL2.Value := CO_VAZIO;

  //
  QrLctoEndossaDNOMESIT.Value := UFinanceiro.NomeSitLancto(QrLctoEndossaDSit.Value,
    QrLctoEndossaDTipo.Value, QrLctoEndossaDPrazo.Value, QrLctoEndossaDVencimento.Value,
    0(*QrLctoEndossaDReparcel.Value*));
  //
  case QrLctoEndossaDSit.Value of
    0: QrLctoEndossaDSALDO.Value := QrLctoEndossaDCredito.Value - QrLctoEndossaDDebito.Value;
    1: QrLctoEndossaDSALDO.Value := (QrLctoEndossaDCredito.Value - QrLctoEndossaDDebito.Value) - QrLctoEndossaDPago.Value;
    else QrLctoEndossaDSALDO.Value := 0;
  end;
  QrLctoEndossaDNOMERELACIONADO.Value := CO_VAZIO;
  if QrLctoEndossaDcliente.Value <> 0 then
  QrLctoEndossaDNOMERELACIONADO.Value := QrLctoEndossaDNOMECLIENTE.Value;
  if QrLctoEndossaDFornecedor.Value <> 0 then
  QrLctoEndossaDNOMERELACIONADO.Value := QrLctoEndossaDNOMERELACIONADO.Value +
  QrLctoEndossaDNOMEFORNECEDOR.Value;
  //
  if QrLctoEndossaDVencimento.Value > Date then QrLctoEndossaDATRASO.Value := 0
    else QrLctoEndossaDATRASO.Value := Date - QrLctoEndossaDVencimento.Value;
  //
  QrLctoEndossaDJUROS.Value :=
    Trunc(QrLctoEndossaDATRASO.Value * QrLctoEndossaDMoraDia.Value * 100)/100;
  //
  if QrLctoEndossaDCompensado.Value = 0 then
     QrLctoEndossaDCOMPENSADO_TXT.Value := '' else
     QrLctoEndossaDCOMPENSADO_TXT.Value :=
     FormatDateTime(VAR_FORMATDATE3, QrLctoEndossaDCompensado.Value);
  QrLctoEndossaDSERIE_CHEQUE.Value := QrLctoEndossaDSerieCH.Value +
    FormatFloat('000000;-0; ', QrLctoEndossaDDocumento.Value);
end;

procedure TFmLctoEndoss.QrLctoEndossaNCalcFields(DataSet: TDataSet);
begin
  if QrLctoEndossaNMes2.Value > 0 then
    QrLctoEndossaNMENSAL.Value := FormatFloat('00', QrLctoEndossaNMes2.Value)+'/'
    +Copy(FormatFloat('0000', QrLctoEndossaNAno.Value), 3, 2)
   else QrLctoEndossaNMENSAL.Value := CO_VAZIO;
  if QrLctoEndossaNMes2.Value > 0 then
    QrLctoEndossaNMENSAL2.Value := FormatFloat('0000', QrLctoEndossaNAno.Value)+'/'+
    FormatFloat('00', QrLctoEndossaNMes2.Value)+'/01'
   else QrLctoEndossaNMENSAL2.Value := CO_VAZIO;

  //
  QrLctoEndossaNNOMESIT.Value := UFinanceiro.NomeSitLancto(QrLctoEndossaNSit.Value,
    QrLctoEndossaNTipo.Value, QrLctoEndossaNPrazo.Value, QrLctoEndossaNVencimento.Value,
    0(*QrLctoEndossaNReparcel.Value*));
  //
  case QrLctoEndossaNSit.Value of
    0: QrLctoEndossaNSALDO.Value := QrLctoEndossaNCredito.Value - QrLctoEndossaNDebito.Value;
    1: QrLctoEndossaNSALDO.Value := (QrLctoEndossaNCredito.Value - QrLctoEndossaNDebito.Value) - QrLctoEndossaNPago.Value;
    else QrLctoEndossaNSALDO.Value := 0;
  end;
  QrLctoEndossaNNOMERELACIONADO.Value := CO_VAZIO;
  if QrLctoEndossaNcliente.Value <> 0 then
  QrLctoEndossaNNOMERELACIONADO.Value := QrLctoEndossaNNOMECLIENTE.Value;
  if QrLctoEndossaNFornecedor.Value <> 0 then
  QrLctoEndossaNNOMERELACIONADO.Value := QrLctoEndossaNNOMERELACIONADO.Value +
  QrLctoEndossaNNOMEFORNECEDOR.Value;
  //
  if QrLctoEndossaNVencimento.Value > Date then QrLctoEndossaNATRASO.Value := 0
    else QrLctoEndossaNATRASO.Value := Date - QrLctoEndossaNVencimento.Value;
  //
  QrLctoEndossaNJUROS.Value :=
    Trunc(QrLctoEndossaNATRASO.Value * QrLctoEndossaNMoraDia.Value * 100)/100;
  //
  if QrLctoEndossaNCompensado.Value = 0 then
     QrLctoEndossaNCOMPENSADO_TXT.Value := '' else
     QrLctoEndossaNCOMPENSADO_TXT.Value :=
     FormatDateTime(VAR_FORMATDATE3, QrLctoEndossaNCompensado.Value);
  QrLctoEndossaNSERIE_CHEQUE.Value := QrLctoEndossaNSerieCH.Value +
    FormatFloat('000000;-0; ', QrLctoEndossaNDocumento.Value);
end;

procedure TFmLctoEndoss.ReopenFlacto;
var
  Controle, Sub: Integer;
begin
  if FLct <> nil then
  begin
    Controle := FLct.FieldByName('Controle').AsInteger;
    Sub      := FLct.FieldByName('Sub').AsInteger;
    //
    FLct.Close;
    FLct.Open;
    FLct.Locate('Controle;Sub', VarArrayOf([Controle, Sub]), []);
  end;
end;

procedure TFmLctoEndoss.ReopenLancto(EndossaN, EndossaD: Integer;
ShowBestGrid: Boolean);
begin
  QrLct.Close;
  QrLct.Params[00].AsInteger := FControle;
  QrLct.Params[01].AsInteger := FSub;
  QrLct.Open;
  //
  QrLctoEndossaN.Close;
  QrLctoEndossaN.Params[00].AsInteger := FControle;
  QrLctoEndossaN.Params[01].AsInteger := FSub;
  QrLctoEndossaN.Open;
  QrLctoEndossaN.Locate('ID', EndossaN, []);
  //
  QrLctoEndossaD.Close;
  QrLctoEndossaD.Params[00].AsInteger := FControle;
  QrLctoEndossaD.Params[01].AsInteger := FSub;
  QrLctoEndossaD.Open;
  QrLctoEndossaD.Locate('ID', EndossaD, []);
  //
  if ShowBestGrid then
  begin
    if (QrLctoEndossaN.RecordCount = 0) and
       (QrLctoEndossaD.RecordCount > 0) then
    PageControl1.ActivePageIndex := 1;
  end;
end;

end.
