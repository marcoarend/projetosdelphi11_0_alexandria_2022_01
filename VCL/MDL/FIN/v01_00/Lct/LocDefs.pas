unit LocDefs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, UnMLAGeral, UnGOTOy, Buttons, UnInternalConsts, ExtCtrls,
  Db, (*DBTables,*) DBCtrls, mySQLDbTables, Variants, dmkGeral, dmkEditCB, dmkEdit,
  dmkDBLookupComboBox, dmkEditDateTimePicker, dmkCheckGroup, DmkDAC_PF;

type
  TFmLocDefs = class(TForm)
    QrContas: TMySQLQuery;
    DsContas: TDataSource;
    PainelDados: TPanel;
    PainelControle: TPanel;
    CkDataIni: TCheckBox;
    CkDataFim: TCheckBox;
    TPDataIni: TdmkEditDateTimePicker;
    TPDataFim: TdmkEditDateTimePicker;
    CkConta: TCheckBox;
    CBConta: TdmkDBLookupComboBox;
    CkDescricao: TCheckBox;
    EdDescricao: TEdit;
    CkDebito: TCheckBox;
    CkCredito: TCheckBox;
    CkNF: TCheckBox;
    CkDoc: TCheckBox;
    CkControle: TCheckBox;
    EdDebMin: TdmkEdit;
    EdCredMin: TdmkEdit;
    EdNF: TdmkEdit;
    EdDoc: TdmkEdit;
    EdControle: TdmkEdit;
    RgOrdem: TRadioGroup;
    BtConfirma: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Painel2: TPanel;
    Image2: TImage;
    EdDuplicata: TdmkEdit;
    CkDuplicata: TCheckBox;
    EdDebMax: TdmkEdit;
    EdCredMax: TdmkEdit;
    Bevel1: TBevel;
    Bevel2: TBevel;
    CkExcelGru: TCheckBox;
    EdConta: TdmkEditCB;
    CGCarteiras: TdmkCheckGroup;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    CkCliInt: TCheckBox;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    DsCliInt: TDataSource;
    QrCliInt: TmySQLQuery;
    QrCliIntCodigo: TIntegerField;
    QrCliIntNOMEENT: TWideStringField;
    CkVctoIni: TCheckBox;
    TPVctoIni: TdmkEditDateTimePicker;
    CkVctoFim: TCheckBox;
    TPVctoFim: TdmkEditDateTimePicker;
    CBFornece: TdmkDBLookupComboBox;
    EdFornece: TdmkEditCB;
    CkFornece: TCheckBox;
    CkCliente: TCheckBox;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    QrFornece: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsFornece: TDataSource;
    QrCliente: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsCliente: TDataSource;
    LaForneceRN: TLabel;
    LaForneceFA: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CkDataFimClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdDebMinExit(Sender: TObject);
    procedure EdCredMinExit(Sender: TObject);
    procedure EdForneceKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBForneceKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure ReopenCliente(NomeFantasia: Boolean);
    procedure ReopenFornece(NomeFantasia: Boolean);
  public
    { Public declarations }
    FQuemChamou: Integer;
    FQrCarteiras, FQrLct: TmySQLQuery;
    FDTPDataIni: TDateTimePicker;
    FComponentClass: TComponentClass;
    FReference: TComponent;
    F_CliInt, FShowForm: Integer;
    FQrLoc: TmySQLQuery;
    FLocSohCliInt: Boolean;
    FModuleLctX: TDataModule;
    //
    procedure ReopenCliInt();
  end;

var
  FmLocDefs: TFmLocDefs;

implementation

uses UnMyObjects, Module, LocLancto;

{$R *.DFM}

procedure TFmLocDefs.BtConfirmaClick(Sender: TObject);
var
  Cursor : TCursor;
  DataIni, DataFim, VctoIni, VctoFim: String;
  i: Integer;
begin
  if FShowForm = 0 then
  begin
    MyObjects.CriaForm_AcessoTotal(TFmLocLancto, FmLocLancto);
    FmLocLancto.FConfirmou    := False;
    FmLocLancto.FQrCarteiras  := FQrCarteiras;
    FmLocLancto.FQrLct        := FQrLct;
    FmLocLancto.FLocSohCliInt := FLocSohCliInt;
    FmLocLancto.FCliLoc       := Geral.IMV(EdCliInt.Text);
    FQrLoc := FmLocLancto.QrLoc;
    case FQuemChamou of
      0: ; // Nada
      1:
      begin
        FmLocLancto.PnConfirma2.Visible := True;
        FmLocLancto.PnConfirma1.Visible := False;
      end;
      2: ;// EventosCad
    end;
  end;
  //
  i := 0;
  DataIni := FormatDateTime(VAR_FORMATDATE, TPDataIni.Date);
  DataFim := FormatDateTime(VAR_FORMATDATE, TPDataFim.Date);
  VctoIni := FormatDateTime(VAR_FORMATDATE, TPVctoIni.Date);
  VctoFim := FormatDateTime(VAR_FORMATDATE, TPVctoFim.Date);
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  Refresh;
  try
    FQrLoc.SQL.Clear;
    //FQrLoc.SQL.Add('SELECT MONTH(la.Mes) Mes2, YEAR(la.Mes) Ano, ');
    FQrLoc.SQL.Add('SELECT MOD(la.Mez, 100) Mes2,');
    FQrLoc.SQL.Add('((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,');
    FQrLoc.SQL.Add('ca.Nome NOMECARTEIRA, la.*, co.Nome NOMEGENERO, ');
    FQrLoc.SQL.Add('ca.ForneceI CLIENTE_INTERNO ');
    FQrLoc.SQL.Add('FROM ' + VAR_LCT + ' la, carteiras ca, contas co');
    FQrLoc.SQL.Add('WHERE ca.Codigo=la.Carteira');
    FQrLoc.SQL.Add('AND co.Codigo=la.Genero');
    // configura data de emiss�o
    if CkDataFim.Checked then
      FQrLoc.SQL.Add('AND la.Data BETWEEN '''+DataIni+''' AND '''+DataFim+'''')
      else if CkDataIni.Checked then
        FQrLoc.SQL.Add('AND la.Data='''+DataIni+'''');
    // configura data de vencimento
    if CkVctoFim.Checked then
      FQrLoc.SQL.Add('AND la.Vencimento BETWEEN '''+VctoIni+''' AND '''+VctoFim+'''')
      else if CkVctoIni.Checked then
        FQrLoc.SQL.Add('AND la.Vencimento='''+VctoIni+'''');
    // configara Conta
    if CkConta.Checked = True then
    begin
      if CBConta.KeyValue <> Null then
      FQrLoc.SQL.Add('AND la.Genero = '''+IntToStr(CBConta.KeyValue)+'''');
    end;
    // configara Fornecedor
    if CkFornece.Checked = True then
    begin
      if CBFornece.KeyValue <> Null then
      FQrLoc.SQL.Add('AND la.Fornecedor = '''+IntToStr(CBFornece.KeyValue)+'''');
    end;
    // configara Cliente
    if CkCliente.Checked = True then
    begin
      if CBCliente.KeyValue <> Null then
      FQrLoc.SQL.Add('AND la.Cliente = '''+IntToStr(CBCliente.KeyValue)+'''');
    end;
    // configara descri��o
    if CkDescricao.Checked = True then
      FQrLoc.SQL.Add('AND la.Descricao LIKE '''+EdDescricao.Text+'''');
    // configara valores
    if CkDebito.Checked then i := i + 1;
    if CkCredito.Checked then i := i + 2;
    case i of
      1: FQrLoc.SQL.Add('AND la.Debito BETWEEN :P0 AND :P1');
      2: FQrLoc.SQL.Add('AND la.Credito BETWEEN :P0 AND :P1');
      3:
      begin
        FQrLoc.SQL.Add('AND la.Debito BETWEEN :P0 AND :P1');
        FQrLoc.SQL.Add('AND la.Credito BETWEEN :P2 AND :P3');
      end;
    end;
    case i of
      1:
      begin
        FQrLoc.Params[0].AsFloat := Geral.DMV(EdDebMin.Text);
        FQrLoc.Params[1].AsFloat := Geral.DMV(EdDebMax.Text);
      end;
      2:
      begin
        FQrLoc.Params[0].AsFloat := Geral.DMV(EdCredMin.Text);
        FQrLoc.Params[1].AsFloat := Geral.DMV(EdCredMax.Text);
      end;
      3:
      begin
        FQrLoc.Params[0].AsFloat := Geral.DMV(EdDebMin.Text);
        FQrLoc.Params[1].AsFloat := Geral.DMV(EdDebMax.Text);
        FQrLoc.Params[2].AsFloat := Geral.DMV(EdCredMin.Text);
        FQrLoc.Params[3].AsFloat := Geral.DMV(EdCredMax.Text);
      end;
    end;
    if CkNF.Checked = True then
      FQrLoc.SQL.Add('AND la.NotaFiscal='+
      IntToStr(Geral.IMV(EdNF.Text)));
    // configara Duplicata
    if CkDuplicata.Checked = True then
      FQrLoc.SQL.Add('AND la.Duplicata='+
      EdDuplicata.Text);
    // configara Controle
    if CkControle.Checked = True then
      FQrLoc.SQL.Add('AND la.Controle='+IntToStr(Geral.IMV(EdControle.Text)));
    // configara Documento
    if CkDoc.Checked = True then
      FQrLoc.SQL.Add('AND la.Documento='+IntToStr(Geral.IMV(EdDoc.Text)));
    // Configura ExcelGru
    if CkExcelGru.Checked = True then
      FQrLoc.SQL.Add('AND (la.ExcelGru IS Null OR la.ExcelGru = 0)');
    // Tipo de carteiras
    case CGcarteiras.Value of
      1: FQrLoc.SQL.Add('AND ca.Tipo = 0');
      2: FQrLoc.SQL.Add('AND ca.Tipo = 1');
      3: FQrLoc.SQL.Add('AND ca.Tipo in (0,1)');
      4: FQrLoc.SQL.Add('AND ca.Tipo = 2');
      5: FQrLoc.SQL.Add('AND ca.Tipo in (0,2)');
      6: FQrLoc.SQL.Add('AND ca.Tipo in (1,2)');
      7: ; // Qualquer coisa
      else Application.MessageBox('Nenhum tipo de carteira foi definido!',
        'Aviso', MB_OK+MB_ICONWARNING);
    end;
    if CkCliInt.Checked then FQrLoc.SQL.Add('AND ca.ForneceI = ' +
      FormatFloat('0', Geral.IMV(EdCliInt.Text)));
      
    // Ordem
    case RGOrdem.ItemIndex of
      0: FQrLoc.SQL.Add('ORDER BY la.Data, la.Controle');
      1: FQrLoc.SQL.Add('ORDER BY la.Controle');
      2: FQrLoc.SQL.Add('ORDER BY la.NotaFiscal');
      3: FQrLoc.SQL.Add('ORDER BY la.Documento');
      4: FQrLoc.SQL.Add('ORDER BY la.Descricao');
      5: FQrLoc.SQL.Add('ORDER BY la.Debito, la.Credito');
      6: FQrLoc.SQL.Add('ORDER BY la.Credito, la.Debito');
    end;
    try
      FQrLoc.Open;
    except
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(FQrLoc, '', nil, True, True);
    end;
  finally
    Screen.Cursor := Cursor;
  end;

  //

  case FShowForm of
    0:
    begin
      FmLocLancto.ShowModal;
      //
      VAR_LOC_LCTO_TRUE := FmLocLancto.FConfirmou;
      VAR_LOC_LCTO_CTRL := FmLocLancto.QrLocControle.Value;
      VAR_LOC_LCTO_SUB  := FmLocLancto.QrLocSub.Value;
      VAR_LOC_LCTO_DEBI := FmLocLancto.QrLocDebito.Value;
      VAR_LOC_LCTO_CRED := FmLocLancto.QrLocCredito.Value;
      //
      FmLocLancto.Destroy;
      if FQuemChamou = 1 then
        Close;
    end;
    1: Close;
    2: Close; // EventosCad
  end;
end;

procedure TFmLocDefs.FormCreate(Sender: TObject);
begin
  TPDataIni.Date := 0;
  TPDataFim.Date := 0;
  TPVctoIni.Date := 0;
  TPVctoFim.Date := 0;
  FShowForm      := 0;
  QrContas.Open;
  QrCliente.Open;
  ReopenCliente(False);
  ReopenFornece(False);
end;

procedure TFmLocDefs.CBClienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then ReopenCliente(False) else
  if Key = VK_F6 then ReopenCliente(True);
end;

procedure TFmLocDefs.CBForneceKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then ReopenFornece(False) else
  if Key = VK_F6 then ReopenFornece(True);
end;

procedure TFmLocDefs.CkDataFimClick(Sender: TObject);
begin
  if CkDataFim.Checked = True then CkDataIni.Checked := True;

end;

procedure TFmLocDefs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLocDefs.BtDesisteClick(Sender: TObject);
begin
  FShowForm := -1;
  Close;
end;

procedure TFmLocDefs.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
  MLAGeral.LoadTextBitmapToPanel(0, 0,
  'Informe somente os dados que tem certeza!', Image2, Painel2, True, 0);
end;

procedure TFmLocDefs.ReopenCliente(NomeFantasia: Boolean);
var
  SQL: String;
begin
  if NomeFantasia then
    SQL := 'IF(Tipo=0, Fantasia, Apelido)'
  else
    SQL := 'IF(Tipo=0, RazaoSocial, Nome)';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCliente, Dmod.MyDB, [
  'SELECT Codigo, '+ SQL +' NOMEENT ',
  'FROM entidades ',
  'ORDER BY NOMEENT ',
  '']);
end;

procedure TFmLocDefs.ReopenCliInt;
begin
  QrCliInt.Close;
  QrCliInt.SQL.Clear;
  QrCliInt.SQL.Add('SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENT');
  QrCliInt.SQL.Add('FROM entidades');
  QrCliInt.SQL.Add('WHERE Codigo IN (' + VAR_LIB_EMPRESAS + ')');
  QrCliInt.SQL.Add('ORDER BY NOMEENT');
  QrCliInt.Open;
end;

procedure TFmLocDefs.ReopenFornece(NomeFantasia: Boolean);
var
  SQL: String;
begin
  if NomeFantasia then
    SQL := 'IF(Tipo=0, Fantasia, Apelido)'
  else
    SQL := 'IF(Tipo=0, RazaoSocial, Nome)';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFornece, Dmod.MyDB, [
  'SELECT Codigo, '+ SQL +' NOMEENT ',
  'FROM entidades ',
  'ORDER BY NOMEENT ',
  '']);
end;

procedure TFmLocDefs.EdDebMinExit(Sender: TObject);
begin
   if Geral.DMV(EdDebMax.Text) < Geral.DMV(EdDebMin.Text) then
     EdDebMax.Text := EdDebMin.Text;
end;

procedure TFmLocDefs.EdForneceKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then ReopenFornece(False) else
  if Key = VK_F6 then ReopenFornece(True);
end;

procedure TFmLocDefs.EdClienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F5 then ReopenCliente(False) else
  if Key = VK_F6 then ReopenCliente(True);
end;

procedure TFmLocDefs.EdCredMinExit(Sender: TObject);
begin
   if Geral.DMV(EdCredMax.Text) < Geral.DMV(EdCredMin.Text) then
     EdCredMax.Text := EdCredMin.Text;
end;

end.

