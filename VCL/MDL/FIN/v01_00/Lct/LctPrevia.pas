unit LctPrevia;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, Mask, DBCtrls,
  ComCtrls, dmkEditDateTimePicker, dmkDBLookupComboBox, dmkEditCB, dmkEdit, DB,
  mySQLDbTables, dmkLabel, UnDmkEnums;

type
  TFmLctPrevia = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrCrts: TmySQLQuery;
    QrCrtsCodigo: TIntegerField;
    QrCrtsNome: TWideStringField;
    QrCrtsFatura: TWideStringField;
    QrCrtsFechamento: TIntegerField;
    QrCrtsPrazo: TSmallintField;
    QrCrtsTipo: TIntegerField;
    QrCrtsExigeNumCheque: TSmallintField;
    QrCrtsForneceI: TIntegerField;
    QrCtas: TmySQLQuery;
    QrCtasCodigo: TIntegerField;
    QrCtasNome: TWideStringField;
    QrCtasNome2: TWideStringField;
    QrCtasNome3: TWideStringField;
    QrCtasID: TWideStringField;
    QrCtasSubgrupo: TIntegerField;
    QrCtasEmpresa: TIntegerField;
    QrCtasCredito: TWideStringField;
    QrCtasDebito: TWideStringField;
    QrCtasMensal: TWideStringField;
    QrCtasExclusivo: TWideStringField;
    QrCtasMensdia: TSmallintField;
    QrCtasMensdeb: TFloatField;
    QrCtasMensmind: TFloatField;
    QrCtasMenscred: TFloatField;
    QrCtasMensminc: TFloatField;
    QrCtasLk: TIntegerField;
    QrCtasTerceiro: TIntegerField;
    QrCtasExcel: TWideStringField;
    QrCtasDataCad: TDateField;
    QrCtasDataAlt: TDateField;
    QrCtasUserCad: TSmallintField;
    QrCtasUserAlt: TSmallintField;
    QrCtasNOMESUBGRUPO: TWideStringField;
    QrCtasNOMEGRUPO: TWideStringField;
    QrCtasNOMECONJUNTO: TWideStringField;
    QrCtasNOMEEMPRESA: TWideStringField;
    QrCtasNOMEPLANO: TWideStringField;
    DsCtas: TDataSource;
    DsCrts: TDataSource;
    PnLancto1: TPanel;
    Label14: TLabel;
    SbCarteira: TSpeedButton;
    BtContas: TSpeedButton;
    Label1: TLabel;
    Label2: TLabel;
    LaDebito: TLabel;
    LaCredito: TLabel;
    Label15: TLabel;
    EdControle: TdmkEdit;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    CBGenero: TdmkDBLookupComboBox;
    EdGenero: TdmkEditCB;
    TPData: TdmkEditDateTimePicker;
    EdDebito: TdmkEdit;
    EdCredito: TdmkEdit;
    Label13: TLabel;
    EdDescricao: TdmkEdit;
    Panel11: TPanel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    LaTipo: TdmkLabel;
    QrIni: TmySQLQuery;
    QrIniData: TDateField;
    QrCtasGrupo: TIntegerField;
    QrCtasConjunto: TIntegerField;
    QrCtasPlano: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbCarteiraClick(Sender: TObject);
    procedure BtContasClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdGeneroChange(Sender: TObject);
    procedure EdCarteiraChange(Sender: TObject);
  private
    { Private declarations }
    procedure VerificaEdits();
  public
    FEntCod: Integer;
    { Public declarations }
  end;

  var
  FmLctPrevia: TFmLctPrevia;

implementation

uses UnMyObjects, Contas, MyDBCheck, UnInternalConsts, UMySQLModule, Module, dmkGeral,
  Principal, ModuleGeral;

{$R *.DFM}

procedure TFmLctPrevia.BtContasClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    FmContas.LocCod(QrCtasCodigo.Value, QrCtasCodigo.Value);
    FmContas.ShowModal;
    FmContas.Destroy;
    QrCtas.Close;
    QrCtas.Open;
    {
    EdGenero.Text := IntToStr(VAR_CONTA);
    CBGenero.KeyValue := VAR_CONTA;
    }
  end;
end;

procedure TFmLctPrevia.BtOKClick(Sender: TObject);
var
  Data, Descricao: String;
  Controle, Tipo, Carteira, Genero, SubGrupo, Grupo, Conjunto, Plano: Integer;
  Debito, Credito: Double;
begin
  Data      := Geral.FDT(TPData.Date, 1);
  Descricao := EdDescricao.Text;
  Controle  := EdControle.ValueVariant;
  Tipo      := QrCrtsTipo.Value;
  Carteira  := EdCarteira.ValueVariant;
  Genero    := EdGenero.ValueVariant;
  Debito    := EdDebito.ValueVariant;
  Credito   := EdCredito.ValueVariant;
  SubGrupo  := QrCtasSubGrupo.Value;
  Grupo     := QrCtasGrupo.Value;
  Conjunto  := QrCtasConjunto.Value;
  Plano     := QrCtasPlano.Value;
  //
  if UMyMod.SQLInsUpd(DModG.QrUpdPID1, LaTipo.SQLType, 'prevlct1', False, [
  'Data', 'Tipo', 'Carteira',
  'Genero', 'Descricao', 'Debito',
  'Credito', 'SubGrupo', 'Grupo',
  'Conjunto', 'Plano', 'Entidade'
  ], ['Controle'], [
  Data, Tipo, Carteira,
  Genero, Descricao, Debito,
  Credito, SubGrupo, Grupo,
  Conjunto, Plano, FEntCod
  ], [Controle], False) then
    Close;
end;

procedure TFmLctPrevia.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLctPrevia.EdCarteiraChange(Sender: TObject);
begin
  QrIni.Close;
  QrIni.Params[00].AsInteger := FEntCod;
  QrIni.Params[01].AsInteger := EdCarteira.ValueVariant;
  QrIni.Open;
  //
  TPData.Date := QrIniData.Value;
end;

procedure TFmLctPrevia.EdGeneroChange(Sender: TObject);
begin
  VerificaEdits();
end;

procedure TFmLctPrevia.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  VerificaEdits();
end;

procedure TFmLctPrevia.FormCreate(Sender: TObject);
begin
  TPData.Date := 0;
end;

procedure TFmLctPrevia.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLctPrevia.SbCarteiraClick(Sender: TObject);
begin
  VAR_CARTNUM := Geral.IMV(EdCarteira.Text);
  FmPrincipal.CadastroDeCarteiras(QrCrtsTipo.Value, EdCarteira.ValueVariant);
  QrCrts.Close;
  QrCrts.Open;
end;

procedure TFmLctPrevia.VerificaEdits();
begin
  {
  if (QrCtasMensal.Value = 'V') or (Dmod.QrControleMensalSempre.Value=1) then
  begin
    EdMes.Enabled := True;
    LaMes.Enabled := True;
  end else begin
    EdMes.Enabled := False;
    LaMes.Enabled := False;
  end;
  }
  //
  if QrCtasCredito.Value = 'V' then
  begin
    EdCredito.Enabled := True;
    LaCredito.Enabled := True;
    {
    EdCliente.Enabled := True;
    LaCliente.Enabled := True;
    CBCliente.Enabled := True;
    }
  end else begin
    EdCredito.Enabled := False;
    LaCredito.Enabled := False;
    {
    EdCliente.Enabled := False;
    LaCliente.Enabled := False;
    CBCliente.Enabled := False;
    }
  end;

  if QrCtasDebito.Value = 'V' then
  begin
    EdDebito.Enabled := True;
    LaDebito.Enabled := True;
    {
    EdFornece.Enabled := True;
    LaFornecedor.Enabled := True;
    CBFornece.Enabled := True;
    }
  end else begin
    EdDebito.Enabled := False;
    LaDebito.Enabled := False;
    {
    EdFornece.Enabled := False;
    LaFornecedor.Enabled := False;
    CBFornece.Enabled := False;
    }
  end;
end;

end.
