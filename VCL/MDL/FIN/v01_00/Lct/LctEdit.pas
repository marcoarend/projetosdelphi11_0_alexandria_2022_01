unit LctEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, UnMLAGeral, UnGOTOy, ComCtrls, UnMyLinguas, Db,
  mySQLDbTables, ExtCtrls, Buttons, UnInternalConsts, UMySQLModule, Grids,
  DBGrids, frxChBox, frxClass, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkEditDateTimePicker, dmkRadioGroup, Variants, dmkGeral, dmkCheckBox,
  UnDmkProcFunc, UnDmkEnums;

type
  TTipoValor = (tvNil, tvCred, tvDeb);
  TFmLctEdit = class(TForm)
    DsContas: TDataSource;
    DsCarteiras: TDataSource;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    DsFornecedores: TDataSource;
    DsClientes: TDataSource;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    QrContas: TmySQLQuery;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasFechamento: TIntegerField;
    QrFatura: TmySQLQuery;
    QrFaturaData: TDateField;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasNome3: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrContasDataCad: TDateField;
    QrContasDataAlt: TDateField;
    QrContasUserCad: TSmallintField;
    QrContasUserAlt: TSmallintField;
    QrContasNOMESUBGRUPO: TWideStringField;
    QrContasNOMEGRUPO: TWideStringField;
    QrContasNOMECONJUNTO: TWideStringField;
    QrContasNOMEEMPRESA: TWideStringField;
    QrFornecedores: TmySQLQuery;
    QrFornecedoresCodigo: TIntegerField;
    QrFornecedoresNOMEENTIDADE: TWideStringField;
    QrVendedores: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsVendedores: TDataSource;
    DsAccounts: TDataSource;
    QrAccounts: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    QrFunci: TmySQLQuery;
    DsFunci: TDataSource;
    QrFunciCodigo: TIntegerField;
    QrFunciNOMEENTIDADE: TWideStringField;
    CkContinuar: TCheckBox;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrCliIntCodigo: TIntegerField;
    QrCliIntNOMEENTIDADE: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PainelDados: TPanel;
    LaFunci: TLabel;
    QrLct: TmySQLQuery;
    QrLctData: TDateField;
    QrLctTipo: TSmallintField;
    QrLctCarteira: TIntegerField;
    QrLctAutorizacao: TIntegerField;
    QrLctGenero: TIntegerField;
    QrLctDescricao: TWideStringField;
    QrLctNotaFiscal: TIntegerField;
    QrLctDebito: TFloatField;
    QrLctCredito: TFloatField;
    QrLctCompensado: TDateField;
    QrLctDocumento: TFloatField;
    QrLctSit: TIntegerField;
    QrLctVencimento: TDateField;
    QrLctLk: TIntegerField;
    QrLctFatID: TIntegerField;
    QrLctFatParcela: TIntegerField;
    QrLctCONTA: TIntegerField;
    QrLctNOMECONTA: TWideStringField;
    QrLctNOMEEMPRESA: TWideStringField;
    QrLctNOMESUBGRUPO: TWideStringField;
    QrLctNOMEGRUPO: TWideStringField;
    QrLctNOMECONJUNTO: TWideStringField;
    QrLctNOMESIT: TWideStringField;
    QrLctAno: TFloatField;
    QrLctMENSAL: TWideStringField;
    QrLctMENSAL2: TWideStringField;
    QrLctBanco: TIntegerField;
    QrLctLocal: TIntegerField;
    QrLctFatura: TWideStringField;
    QrLctSub: TSmallintField;
    QrLctCartao: TIntegerField;
    QrLctLinha: TIntegerField;
    QrLctPago: TFloatField;
    QrLctSALDO: TFloatField;
    QrLctID_Sub: TSmallintField;
    QrLctMez: TIntegerField;
    QrLctFornecedor: TIntegerField;
    QrLctcliente: TIntegerField;
    QrLctMoraDia: TFloatField;
    QrLctNOMECLIENTE: TWideStringField;
    QrLctNOMEFORNECEDOR: TWideStringField;
    QrLctTIPOEM: TWideStringField;
    QrLctNOMERELACIONADO: TWideStringField;
    QrLctOperCount: TIntegerField;
    QrLctLancto: TIntegerField;
    QrLctMulta: TFloatField;
    QrLctATRASO: TFloatField;
    QrLctJUROS: TFloatField;
    QrLctDataDoc: TDateField;
    QrLctNivel: TIntegerField;
    QrLctVendedor: TIntegerField;
    QrLctAccount: TIntegerField;
    QrLctMes2: TLargeintField;
    QrLctProtesto: TDateField;
    QrLctDataCad: TDateField;
    QrLctDataAlt: TDateField;
    QrLctUserCad: TSmallintField;
    QrLctUserAlt: TSmallintField;
    QrLctControle: TIntegerField;
    QrLctID_Pgto: TIntegerField;
    QrLctCtrlIni: TIntegerField;
    QrLctFatID_Sub: TIntegerField;
    QrLctICMS_P: TFloatField;
    QrLctICMS_V: TFloatField;
    QrLctDuplicata: TWideStringField;
    QrLctCOMPENSADO_TXT: TWideStringField;
    QrLctCliInt: TIntegerField;
    QrLctNOMECARTEIRA: TWideStringField;
    QrLctSALDOCARTEIRA: TFloatField;
    DsLct: TDataSource;
    TabSheet2: TTabSheet;
    DsDeptos: TDataSource;
    QrDeptos: TmySQLQuery;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasTipo: TIntegerField;
    QrForneceI: TmySQLQuery;
    DsForneceI: TDataSource;
    QrForneceICodigo: TIntegerField;
    QrForneceINOMEENTIDADE: TWideStringField;
    TabSheet3: TTabSheet;
    QrCarteirasExigeNumCheque: TSmallintField;
    CkCopiaCH: TCheckBox;
    BitBtn1: TBitBtn;
    Panel2: TPanel;
    Panel4: TPanel;
    BtDesiste: TBitBtn;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    PnMaskPesq: TPanel;
    QrPesqCodigo: TIntegerField;
    QrPesqNome: TWideStringField;
    PnLink: TPanel;
    Panel6: TPanel;
    SpeedButton3: TSpeedButton;
    TabSheet4: TTabSheet;
    TabControl1: TTabControl;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    LaDataDoc: TLabel;
    Label22: TLabel;
    Panel3: TPanel;
    Label10: TLabel;
    LaVendedor: TLabel;
    LaICMS_P: TLabel;
    LaICMS_V: TLabel;
    Panel7: TPanel;
    GBParcelamento: TGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    RGArredondar: TRadioGroup;
    RGPeriodo: TRadioGroup;
    EdDias: TdmkEdit;
    RGIncremCH: TRadioGroup;
    EdParcela1: TdmkEdit;
    EdParcelaX: TdmkEdit;
    CkArredondar: TCheckBox;
    EdSoma: TdmkEdit;
    CkParcelamento: TCheckBox;
    DBGParcelas: TDBGrid;
    TbParcpagtos: TmySQLTable;
    TbParcpagtosParcela: TIntegerField;
    TbParcpagtosData: TDateField;
    TbParcpagtosCredito: TFloatField;
    TbParcpagtosDebito: TFloatField;
    TbParcpagtosDoc: TLargeintField;
    TbParcpagtosMora: TFloatField;
    TbParcpagtosMulta: TFloatField;
    TbParcpagtosICMS_V: TFloatField;
    TbParcpagtosDuplicata: TWideStringField;
    TbParcpagtosDescricao: TWideStringField;
    DsParcPagtos: TDataSource;
    QrSoma: TmySQLQuery;
    QrSomaVALOR: TFloatField;
    CkIncremDU: TCheckBox;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label8: TLabel;
    Label7: TLabel;
    GroupBox3: TGroupBox;
    Label19: TLabel;
    Label21: TLabel;
    GroupBox4: TGroupBox;
    Label28: TLabel;
    Label29: TLabel;
    BitBtn2: TBitBtn;
    RGIncremDupl: TGroupBox;
    Label27: TLabel;
    EdDuplSep: TEdit;
    RGDuplSeq: TRadioGroup;
    GBIncremTxt: TGroupBox;
    Label30: TLabel;
    EdSepTxt: TEdit;
    RGSepTxt: TRadioGroup;
    CkIncremTxt: TCheckBox;
    QrDeptosCODI_1: TIntegerField;
    QrDeptosCODI_2: TLargeintField;
    QrDeptosNOME_1: TWideStringField;
    TabSheet5: TTabSheet;
    Panel8: TPanel;
    Panel9: TPanel;
    Label4: TLabel;
    LaFinalidade: TLabel;
    MeConfig: TMemo;
    dmkEdTPDataDoc: TdmkEditDateTimePicker;
    dmkEdTPCompensado: TdmkEditDateTimePicker;
    dmkEdMoraDia: TdmkEdit;
    dmkEdMulta: TdmkEdit;
    dmkEdMultaVal: TdmkEdit;
    dmkEdMoraVal: TdmkEdit;
    dmkEdValNovo: TdmkEdit;
    dmkEdPerMult: TdmkEdit;
    dmkCBFunci: TdmkDBLookupComboBox;
    dmkEdCBFunci: TdmkEditCB;
    dmkEdCBVendedor: TdmkEditCB;
    dmkCBVendedor: TdmkDBLookupComboBox;
    dmkEdICMS_P: TdmkEdit;
    dmkEdICMS_V: TdmkEdit;
    dmkEdParcelas: TdmkEdit;
    Panel10: TPanel;
    Label5: TLabel;
    dmkEdCNAB_Sit: TdmkEdit;
    Label6: TLabel;
    dmkEdID_pgto: TdmkEdit;
    dmkEdFatID: TdmkEdit;
    Label12: TLabel;
    Label16: TLabel;
    dmkEdTipoCH: TdmkEdit;
    dmkEdFatNum: TdmkEdit;
    Label17: TLabel;
    Label31: TLabel;
    dmkEdFatID_Sub: TdmkEdit;
    dmkEdNivel: TdmkEdit;
    Label32: TLabel;
    Label33: TLabel;
    dmkEdFatParcela: TdmkEdit;
    dmkEdCtrlIni: TdmkEdit;
    Label34: TLabel;
    Label35: TLabel;
    dmkEdDescoPor: TdmkEdit;
    dmkEdDescoVal: TdmkEdit;
    Label36: TLabel;
    Label37: TLabel;
    dmkEdUnidade: TdmkEdit;
    dmkEdDoc2: TdmkEdit;
    Label38: TLabel;
    Label39: TLabel;
    dmkEdNFVal: TdmkEdit;
    Label40: TLabel;
    Label41: TLabel;
    dmkEdTipoAnt: TdmkEdit;
    dmkEdCartAnt: TdmkEdit;
    Label42: TLabel;
    dmkEdPercJuroM: TdmkEdit;
    dmkEdPercMulta: TdmkEdit;
    Label43: TLabel;
    Label44: TLabel;
    dmkEd_: TdmkEdit;
    dmkRGTipoCH: TdmkRadioGroup;
    Label45: TLabel;
    dmkEdExecs: TdmkEdit;
    Label46: TLabel;
    dmkEdOneCliInt: TdmkEdit;
    QrLctFatNum: TFloatField;
    PnLancto1: TPanel;
    Label14: TLabel;
    dmkEdControle: TdmkEdit;
    dmkEdCarteira: TdmkEditCB;
    dmkCBCarteira: TdmkDBLookupComboBox;
    SbCarteira: TSpeedButton;
    BtContas: TSpeedButton;
    dmkCBGenero: TdmkDBLookupComboBox;
    dmkEdCBGenero: TdmkEditCB;
    dmkEdTPData: TdmkEditDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
    LaMes: TLabel;
    dmkEdMes: TdmkEdit;
    dmkEdDeb: TdmkEdit;
    LaDeb: TLabel;
    dmkEdCred: TdmkEdit;
    LaCred: TLabel;
    LaNF: TLabel;
    dmkEdNF: TdmkEdit;
    dmkEdSerieCH: TdmkEdit;
    LaDoc: TLabel;
    dmkEdDoc: TdmkEdit;
    dmkEdDuplicata: TdmkEdit;
    Label11: TLabel;
    LaVencimento: TLabel;
    dmkEdTPVencto: TdmkEditDateTimePicker;
    LaCliInt: TLabel;
    dmkEdCBCliInt: TdmkEditCB;
    dmkCBCliInt: TdmkDBLookupComboBox;
    dmkEdCBCliente: TdmkEditCB;
    LaCliente: TLabel;
    dmkCBCliente: TdmkDBLookupComboBox;
    dmkEdCBFornece: TdmkEditCB;
    LaFornecedor: TLabel;
    dmkCBFornece: TdmkDBLookupComboBox;
    SpeedButton2: TSpeedButton;
    LaForneceRN: TLabel;
    LaForneceFA: TLabel;
    Label15: TLabel;
    PnDepto: TPanel;
    dmkCBDepto: TdmkDBLookupComboBox;
    dmkEdCBDepto: TdmkEditCB;
    LaDepto: TLabel;
    PnForneceI: TPanel;
    dmkCBForneceI: TdmkDBLookupComboBox;
    dmkEdCBForneceI: TdmkEditCB;
    LaForneceI: TLabel;
    PnAccount: TPanel;
    LaAccount: TLabel;
    dmkEdCBAccount: TdmkEditCB;
    dmkCBAccount: TdmkDBLookupComboBox;
    PnLancto3: TPanel;
    CkPesqNF: TCheckBox;
    CkPesqCH: TCheckBox;
    PnLancto2: TPanel;
    dmkEdDescricao: TdmkEdit;
    Label13: TLabel;
    dmkEdQtde: TdmkEdit;
    Label20: TLabel;
    Label3: TLabel;
    dmkEdOneForneceI: TdmkEdit;
    Label47: TLabel;
    dmkEdOneVendedor: TdmkEdit;
    Label48: TLabel;
    dmkEdOneAccount: TdmkEdit;
    Label49: TLabel;
    dmkEdOneCliente: TdmkEdit;
    dmkEdOneFornecedor: TdmkEdit;
    Label50: TLabel;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesAccount: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    QrContasNOMEPLANO: TWideStringField;
    Panel11: TPanel;
    Label51: TLabel;
    DBEdit1: TDBEdit;
    Label52: TLabel;
    DBEdit2: TDBEdit;
    Label53: TLabel;
    DBEdit3: TDBEdit;
    Label54: TLabel;
    DBEdit4: TDBEdit;
    QrPesqNOMEPLANO: TWideStringField;
    QrPesqNOMESUBGRUPO: TWideStringField;
    QrPesqNOMEGRUPO: TWideStringField;
    QrPesqNOMECONJUNTO: TWideStringField;
    Panel5: TPanel;
    Panel12: TPanel;
    Label18: TLabel;
    EdLinkMask: TEdit;
    LLBPesq: TDBLookupListBox;
    QrCarteirasForneceI: TIntegerField;
    CkCancelado: TdmkCheckBox;
    CkPesqVal: TCheckBox;
    LaAviso: TLabel;
    CkNaoPesquisar: TCheckBox;
    QrCarteirasBanco1: TIntegerField;
    CkDuplicando: TCheckBox;
    QrCarteirasUsaTalao: TSmallintField;
    Label9: TLabel;
    EdProtocolo: TdmkEdit;
    TabSheet6: TTabSheet;
    Panel13: TPanel;
    GroupBox5: TGroupBox;
    Label55: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    EdOldControle: TdmkEdit;
    TPOldData: TdmkEditDateTimePicker;
    EdOldSub: TdmkEdit;
    EdOldTipo: TdmkEdit;
    EdOldCarteira: TdmkEdit;
    LaEventosCad: TLabel;
    EdEventosCad: TdmkEditCB;
    CBEventosCad: TdmkDBLookupComboBox;
    Label60: TLabel;
    EdIndiPag: TdmkEditCB;
    CBIndiPag: TdmkDBLookupComboBox;
    SbEventosCad: TSpeedButton;
    SbIndiPag: TSpeedButton;
    QrEventosCad: TmySQLQuery;
    QrEventosCadCodigo: TIntegerField;
    QrEventosCadCodUsu: TIntegerField;
    QrEventosCadNome: TWideStringField;
    DsEventosCad: TDataSource;
    QrIndiPag: TmySQLQuery;
    QrIndiPagCodigo: TIntegerField;
    QrIndiPagNome: TWideStringField;
    DsIndiPag: TDataSource;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtContasClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbCarteiraClick(Sender: TObject);
    procedure QrContasAfterScroll(DataSet: TDataSet);
    procedure EdDuplicataKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TabSheet2Resize(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure TPDataChange(Sender: TObject);
    procedure QrCarteirasAfterScroll(DataSet: TDataSet);
    procedure QrCliIntAfterScroll(DataSet: TDataSet);
    procedure SpeedButton2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure EdLinkMaskChange(Sender: TObject);
    procedure LLBPesqDblClick(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure LLBPesqKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdLinkMaskKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CkParcelamentoClick(Sender: TObject);
    procedure RGPeriodoClick(Sender: TObject);
    procedure DBGParcelasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CkArredondarClick(Sender: TObject);
    procedure RGArredondarClick(Sender: TObject);
    procedure EdDiasExit(Sender: TObject);
    procedure RGIncremCHClick(Sender: TObject);
    procedure CkIncremDUClick(Sender: TObject);
    procedure EdDuplSepExit(Sender: TObject);
    procedure RGDuplSeqClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure CkIncremTxtClick(Sender: TObject);
    procedure QrDeptosAfterScroll(DataSet: TDataSet);
    procedure dmkEdCBGeneroChange(Sender: TObject);
    procedure dmkEdCBGeneroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkCBGeneroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdMesExit(Sender: TObject);
    procedure dmkEdDebExit(Sender: TObject);
    procedure dmkCBGeneroClick(Sender: TObject);
    procedure dmkEdCredExit(Sender: TObject);
    procedure dmkEdDocChange(Sender: TObject);
    procedure dmkEdTPVenctoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkCBForneceKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdCBDeptoChange(Sender: TObject);
    procedure dmkCBDeptoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdDescricaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkCBAccountKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdICMS_PExit(Sender: TObject);
    procedure dmkEdICMS_PKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdICMS_VExit(Sender: TObject);
    procedure dmkEdICMS_VKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdParcelasExit(Sender: TObject);
    procedure dmkEdCBAccountKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkDBLookupComboBox1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdTPDataClick(Sender: TObject);
    procedure LLBPesqEnter(Sender: TObject);
    procedure LLBPesqExit(Sender: TObject);
    procedure dmkEdTPVenctoChange(Sender: TObject);
    procedure dmkEdTPVenctoClick(Sender: TObject);
    procedure dmkEdDuplicataExit(Sender: TObject);
    procedure dmkEdDocExit(Sender: TObject);
    procedure dmkEdSerieCHExit(Sender: TObject);
    procedure dmkEdCarteiraChange(Sender: TObject);
    procedure SbEventosCadClick(Sender: TObject);
    procedure SbIndiPagClick(Sender: TObject);
    procedure dmkEdCBCliIntChange(Sender: TObject);
  private
    { Private declarations }
    FCriandoForm: Boolean;
    FValorAParcelar,
    FICMS: Double;
    FIDFinalidade: TIDFinalidadeLct;
    FParcPagtos: String;
    procedure VerificaEdits();
    procedure CarteirasReopen;
    procedure SetaAccount(var Key: Word; Shift: TShiftState);
    procedure ExigeFuncionario();
    procedure CalculaICMS;
    function ReopenLct: Boolean;

    function VerificaVencimento: Boolean;
    procedure ConfiguraVencimento;
    procedure ConfiguraComponentesCarteira;
    procedure ReopenFornecedores(Tipo: Integer);
    procedure MostraPnMaskPesq(Link: String);
    procedure SelecionaItemDePesquisa;
    // Parcelamento
    procedure CalculaParcelas;
    function GetTipoValor: TTipoValor;
    procedure ConfiguracoesIniciais();
    procedure IncrementaExecucoes();
    procedure EventosCadReopen(Entidade: Integer);
    procedure ReopenIndiPag();

  public
    { Public declarations }
    //FQrCarteiras: TmySQLQuery; Usar UFinanceiro.VLAN_QrCarteiras
    // ??? FCarteira,
    {
    FCNAB_Sit, FFatID, FFatNum, FFAtPArcela, FFatID_Sub, FID_Pgto,
    FNivel, FDescoPor, FCtrlIni, FUnidade: Integer;
    FDescoVal, FNFVal, FPercJur, FPercMulta: Double;
    FDoc2: String;
    FCondPercJuros, FCondPercMulta: Double;
    FTPDataIni, FTPDataFim: TDateTimePicker;
    FDepto: Variant;
    FCondominio: Integer;
    }
    //
  end;

const
  FLargMaior = 800;
  FLargMenor = 604;

var
  FmLctEdit: TFmLctEdit;
  Pagto_Doc: Double;

implementation

uses UnMyObjects, Contas, Principal, Entidades, UnFinanceiro, FinForm,
MyDBCheck, ModuleFin, ModuleGeral, UnMyPrinters, UCreate, EventosCad, IndiPag,
Module;

{$R *.DFM}

procedure TFmLctEdit.CalculaParcelas;
var
  i, Parce, DiasP, Casas: Integer;
  Valor, Valor1, ValorX, ValorA, ValorC, ValorD, Total, Mora, Multa, Fator: Double;
  Data: TDate;
  TipoValor: TTipoValor;
  Duplicata, Descricao, DuplSeq, DuplSep, Fmt, TxtSeq: String;
begin
  Screen.Cursor := crHourGlass;
  try
    TbParcpagtos.Close;
    FParcPagtos := UCriar.RecriaTempTable('ParcPagtos', DModG.QrUpdPID1, False);
    TbParcpagtos.Database := DModG.MyPID_DB;
    {  N�o precisa mais com a recra��o autom�tica!
    TbParcpagtos.Open;
    TbParcpagtos.DisableControls;
    while not TbParcpagtos.Eof do TbParcpagtos.Delete;
    TbParcpagtos.EnableControls;
    TbParcpagtos.Close;
    }
    if GBParcelamento.Visible then
    begin
      TipoValor := GetTipoValor;
      Pagto_Doc := Geral.DMV(dmkEdDoc.Text);
      DiasP := Geral.IMV(EdDias.Text);
      Parce := Geral.IMV(dmkEdParcelas.Text);
      Mora  := Geral.DMV(dmkEdMoraDia.Text);
      Multa := Geral.DMV(dmkEdMulta.Text);
      Total := FValorAParcelar;
      if Total > 0 then
        Fator := Geral.DMV(dmkEdICMS_V.Text) / Total else Fator := 0;
      if Total <= 0 then Valor := 0
      else
      begin
        Valor := (Total / Parce)*100;
        Valor := (Trunc(Valor))/100;
      end;
      if CkArredondar.Checked then Valor := int(Valor);
      Valor1 := Valor;
      ValorX := Valor;
      if RGArredondar.ItemIndex = 0 then
      begin
        EdParcela1.ValueVariant := Valor;
        ValorX := Total - ((Parce - 1) * Valor);
        EdParcelaX.ValueVariant := ValorX;
      end else begin
        EdParcelaX.ValueVariant := Valor;
        Valor1 := Total - ((Parce - 1) * Valor);
        EdParcela1.ValueVariant := Valor1;
      end;
      //Duplicata := MLAGeral.IncrementaDuplicata(dmkEdDuplicata.Text, -1);
      Duplicata := dmkEdDuplicata.Text;
      Casas := Length(IntTostr(Parce));
      fmt := '';
      for i := 1 to Casas do fmt := fmt + '0';
      DuplSep := Trim(EdDuplSep.Text);
      if DuplSep = '' then DuplSep := ' ';
      for i := 1 to Parce do
      begin
        if i= 1 then ValorA := Valor1
        else if i = Parce then ValorA := ValorX
        else ValorA := Valor;
        if TipoValor = tvCred then ValorC := ValorA else ValorC := 0;
        if TipoValor = tvDeb  then ValorD := ValorA else ValorD := 0;
        //
        if RGPeriodo.ItemIndex = 0 then
          Data := MLAGeral.IncrementaMeses(dmkEdTPVencto.Date, i-1, True)
        else
          Data := dmkEdTPVencto.Date + (DiasP * (i-1));
        //
        if dmkEdMes.Enabled then
        begin
          if CkIncremTxt.Checked then
          begin
            case RGSepTxt.ItemIndex of
              0: TxtSeq := FormatFloat(fmt, i);
              1: TxtSeq := dmkPF.IntToColTxt(i);
              else TxtSeq := '?';
            end;
            case RGSepTxt.ItemIndex of
              0: Descricao := FormatFloat(fmt, Parce);
              1: Descricao := dmkPF.IntToColTxt(Parce);
              else TxtSeq := '?';
            end;
            TxtSeq := TxtSeq + EdSepTxt.Text;
            Descricao := TxtSeq + Descricao + ' ' + dmkEdDescricao.Text;
          end else
            Descricao := dmkEdDescricao.Text
        end else
          Descricao := IntToStr(i) + '�/'+ IntToStr(Parce) + ' ' +dmkEdDescricao.Text;
        //Duplicata := MLAGeral.IncrementaDuplicata(Duplicata, 1);
        //
        if CkIncremDU.Checked then
        begin
          case RGDuplSeq.ItemIndex of
            0: DuplSeq := FormatFloat(fmt, i);
            1: DuplSeq := dmkPF.IntToColTxt(i);
            else DuplSeq := '?';
          end;
          DuplSeq := DuplSep + DuplSeq;
        end else DuplSeq := '';
        DmodG.QrUpdPID1.SQL.Clear;
        DmodG.QrUpdPID1.SQL.Add('INSERT INTO parcpagtos SET Parcela=:P0, ');
        DmodG.QrUpdPID1.SQL.Add('Data=:P1, Credito=:P2, Debito=:P3, Doc=:P4, ');
        DmodG.QrUpdPID1.SQL.Add('Mora=:P5, Multa=:P6, ICMS_V=:P7, Duplicata=:P8, ');
        DmodG.QrUpdPID1.SQL.Add('Descricao=:P9 ');
        DmodG.QrUpdPID1.Params[0].AsInteger := i;
        DmodG.QrUpdPID1.Params[1].AsString := FormatDateTime(VAR_FORMATDATE, Data);
        DmodG.QrUpdPID1.Params[2].AsFloat := ValorC;
        DmodG.QrUpdPID1.Params[3].AsFloat := ValorD;
        DmodG.QrUpdPID1.Params[4].AsFloat := Pagto_Doc;
        DmodG.QrUpdPID1.Params[5].AsFloat := Mora;
        DmodG.QrUpdPID1.Params[6].AsFloat := Multa;
        DmodG.QrUpdPID1.Params[7].AsFloat := Fator * (ValorC+ValorD);
        DmodG.QrUpdPID1.Params[8].AsString := Duplicata + DuplSeq;
        DmodG.QrUpdPID1.Params[9].AsString := Descricao;
        DmodG.QrUpdPID1.ExecSQL;
        if not RGIncremCH.Enabled then Pagto_Doc := Pagto_Doc
        else Pagto_Doc := Pagto_Doc + RGIncremCH.ItemIndex;
        //
      end;
      TbParcpagtos.Open;
      TbParcpagtos.EnableControls;
    end;
    QrSoma.Close;
    QrSoma.Database := DModG.MyPID_DB;
    QrSoma.Open;
    EdSoma.Text := FormatFloat('#,###,##0.00', QrSomaVALOR.Value);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLctEdit.CarteirasReopen;
begin
  QrCarteiras.Close;
  QrCarteiras.SQL.Clear;
  QrCarteiras.SQL.Add('SELECT car.*');
  QrCarteiras.SQL.Add('FROM carteiras car');
  QrCarteiras.SQL.Add('WHERE car.ForneceI IN (' + VAR_LIB_EMPRESAS + ')');
  QrCarteiras.SQL.Add('ORDER BY Nome');
  QrCarteiras.Open;
end;

procedure TFmLctEdit.VerificaEdits();
begin
  if (QrContasMensal.Value = 'V') or (Dmod.QrControleMensalSempre.Value=1) then
  begin
    dmkEdMes.Enabled := True;
    LaMes.Enabled := True;
  end else begin
    dmkEdMes.Enabled := False;
    LaMes.Enabled := False;
  end;
  //
  if (VAR_BAIXADO <> -2) and not VAR_FATURANDO then Exit;
  if QrContasCredito.Value = 'V' then
  begin
    dmkEdCred.Enabled := True;
    LaCred.Enabled := True;
    dmkEdCBCliente.Enabled := True;
    LaCliente.Enabled := True;
    dmkCBCliente.Enabled := True;
  end else begin
    dmkEdCred.Enabled := False;
    LaCred.Enabled := False;
    dmkEdCBCliente.Enabled := False;
    LaCliente.Enabled := False;
    dmkCBCliente.Enabled := False;
  end;

  if QrContasDebito.Value = 'V' then
  begin
    dmkEdDeb.Enabled := True;
    LaDeb.Enabled := True;
    dmkEdCBFornece.Enabled := True;
    LaFornecedor.Enabled := True;
    dmkCBFornece.Enabled := True;
  end else begin
    dmkEdDeb.Enabled := False;
    LaDeb.Enabled := False;
    dmkEdCBFornece.Enabled := False;
    LaFornecedor.Enabled := False;
    dmkCBFornece.Enabled := False;
  end;

end;

procedure TFmLctEdit.BtDesisteClick(Sender: TObject);
begin
  Close;
  VAR_VALOREMITIRIMP := 0;
end;

procedure TFmLctEdit.BtConfirmaClick(Sender: TObject);
var
  Cartao, Carteira, Genero, TipoAnt, CartAnt, NF, Sit, Linha, Depto, Fornecedor,
  Cliente, CliInt, ForneceI, Vendedor, Account, Funci, Parcelas,
  DifMes, Me1, Me2: Integer;
  Controle: Int64;
  Credito, Debito, MoraDia, Multa, ICMS_V, Difer, MultaVal, MoraVal: Double;
  Mes, Vencimento, Compensado: String;
  Inseriu: Boolean;
  MesX, AnoX: Word;
  DataX: TDateTime;
  ValParc: Double;
  SerieCH: String;
  Documen: Double;
begin
  try
  LaAviso.Caption := 'Preparando inclus�o';
  //
  Me1 := 0;
  //
  if CkParcelamento.Checked and (LaTipo.Caption = CO_ALTERACAO) then
  begin
    Geral.MensagemBox('N�o pode haver parcelamento em altera��o!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //  Veriificar integridade do parcelamento
  if CkParcelamento.Checked then
  begin
    ValParc := 0;
    if dmkEdCred.Enabled then
      ValParc := ValParc + dmkEdCred.ValueVariant;
    if dmkEdDeb.Enabled then
      ValParc := ValParc - dmkEdDeb.ValueVariant;
    if ValParc < 0 then ValParc := ValParc * -1;
    if ValParc <> EdSoma.ValueVariant then
    begin
      Geral.MensagemBox('Valor do parcelamento n�o confere com d�bito/cr�dito!',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  // fim integridade parcelamento

  Screen.Cursor := crHourGlass;
  if dmkEdCBFornece.Enabled = False then Fornecedor := 0 else
    if dmkCBFornece.KeyValue <> Null then
      Fornecedor := dmkCBFornece.KeyValue else Fornecedor := 0;
  if dmkEdCBCliente.Enabled = False then Cliente := 0 else
    if dmkCBCliente.KeyValue <> Null then
      Cliente := dmkCBCliente.KeyValue else Cliente := 0;
  Vendedor := Geral.IMV(dmkEdCBVendedor.Text);
  Account  := Geral.IMV(dmkEdCBAccount.Text);
  Depto    := Geral.IMV(dmkEdCBDepto.Text);
  ForneceI := Geral.IMV(dmkEdCBForneceI.Text);
  CliInt   := Geral.IMV(dmkEdCBCliInt.Text);
  if (CliInt = 0) or not DmodG.EmpresaLogada(CliInt)then
  begin
    Geral.MensagemBox('ERRO! Cliente interno inv�lido!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    LaCliInt.Enabled := True;
    dmkEdCBCliInt.Enabled := True;
    dmkCBCliInt.Enabled := True;
    if dmkEdCBCliInt.Enabled then dmkEdCBCliInt.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (VAR_AVISOSCXAEDIT in ([1,3,5,7]))
  and (Vendedor < 1) and (dmkCBVendedor.Visible) and (Panel3.Visible) then
  begin
    if Geral.MensagemBox('Deseja continuar mesmo sem definir o vendedor?',
    'Aviso de escolha de vendedor', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      if dmkCBVendedor.Enabled then dmkCBVendedor.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  if (VAR_AVISOSCXAEDIT in ([2,3,6,7]))
  and (Account < 1) and (dmkCBAccount.Visible) then
  begin
    if Geral.MensagemBox('Deseja continuar mesmo sem definir o Representante?',
    'Aviso de escolha de Representante', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      if dmkCBAccount.Enabled then dmkCBAccount.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  VAR_CANCELA := False;
  if not VerificaVencimento then
  begin
   Screen.Cursor := crDefault;
   Exit;
  end;
  Cartao := 0;
  Compensado := CO_VAZIO;
  if VAR_FATURANDO then
  begin
    {
    desabilitei at� ser necess�rio 2009-12-21
    Cartao := FmPrincipal.CartaoDeFatura;
    Compensado := FmPrincipal.CompensacaoDeFatura;
    }
  end else if dmkEdTPCompensado.Date > 1 then
  begin
  // 2013-03-13
    if (CkDuplicando.Checked) or (LaTipo.Caption = CO_INCLUSAO) then
      Compensado := '0000-00-00'
    else
  // FIM 2013-03-13
      Compensado := Geral.FDT(dmkEdTPCompensado.Date, 1);
  end;
  if (dmkEdNF.Enabled = False) then NF := 0
  else NF := Geral.IMV(dmkEdNF.Text);
  Credito := Geral.DMV(dmkEdCred.Text);
  if VAR_CANCELA then
  begin
    Geral.MensagemBox('Inclus�o cancelada pelo usu�rio!',
    'Cancelaqmento de Inclus�o', MB_OK + MB_ICONEXCLAMATION);
    if dmkEdNF.Enabled then dmkEdNF.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  Debito  := Geral.DMV(dmkEdDeb.Text);
  MoraDia := Geral.DMV(dmkEdMoraDia.Text);
  Multa   := Geral.DMV(dmkEdMulta.Text);
  ICMS_V  := Geral.DMV(dmkEdICMS_V.Text);
  Difer   := Credito-Debito;
  if ((Difer<0) and (ICMS_V>0)) or ((Difer>0) and (ICMS_V<0)) then
  begin
    Geral.MensagemBox('ICMS incorreto! Para d�bitos o ICMS deve ser negativo!',
    'Cancelaqmento de Inclus�o', MB_OK + MB_ICONEXCLAMATION);
    if dmkEdICMS_P.Enabled then dmkEdICMS_P.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (dmkEdCred.Enabled = False) and (Credito <> 0) and (VAR_BAIXADO = -2) then
  begin
    dmkEdCred.Text := '0';
    Credito := 0;
  end;
  if (dmkEdDeb.Enabled = False) and (Debito <> 0) and (VAR_BAIXADO = -2) then
  begin
    dmkEdDeb.Text := '0';
    Debito := 0;
  end;
  if dmkCBCarteira.KeyValue <> Null then Carteira := dmkCBCarteira.KeyValue else Carteira := 0;
  if Carteira = 0 then
  begin
    Geral.MensagemBox('AVISO. Defina uma carteira!', 'Aviso', MB_OK+MB_ICONWARNING);
    if dmkEdCarteira.Enabled then dmkEdCarteira.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (dmkCBFunci.Visible = True) and
     (Panel3.Visible = True) and (dmkCBFunci.KeyValue = Null)then
  begin
    Geral.MensagemBox('AVISO. Defina um funcion�rio!', 'Aviso', MB_OK+MB_ICONWARNING);
    if dmkEdCBFunci.Enabled then dmkEdCBFunci.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end else Funci := Geral.IMV(dmkEdCBFunci.Text);
  if Funci = 0 then Funci := VAR_USUARIO;
  Genero := Geral.IMV(dmkEdCBGenero.Text);
  if Genero = 0 then
  begin
    Geral.MensagemBox('AVISO. Defina uma conta!', 'Aviso', MB_OK+MB_ICONWARNING);
    if dmkEdCBGenero.Enabled then dmkEdCBGenero.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (Credito > 0) and (QrContasCredito.Value = 'F') then
  begin
    Geral.MensagemBox('AVISO. Valor n�o pode ser cr�dito', 'Aviso', MB_OK+MB_ICONWARNING);
    dmkEdCred.Text;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (Debito > 0) and (QrContasDebito.Value = 'F') then
  begin
    Geral.MensagemBox('AVISO. Valor n�o pode ser d�bito', 'Aviso', MB_OK+MB_ICONWARNING);
    dmkEdDeb.Text;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (dmkEdCred.Enabled = False) and (dmkEdDeb.Enabled = False) and
  (VAR_BAIXADO = -2) then
  begin
    Geral.MensagemBox('AVISO. Opera��o imposs�vel. Conta sem permiss�o de d�bito ou cr�dito.', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (Credito = 0) and (Debito = 0) and (CkCancelado.Checked = False) then
  begin
    Geral.MensagemBox('AVISO. Defina um d�bito ou um cr�dito.', 'Aviso', MB_OK+MB_ICONWARNING);
    if dmkEdDeb.Enabled = True then dmkEdDeb.SetFocus
    else if dmkEdCred.Enabled = True then dmkEdCred.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  if dmkEdMes.Text = CO_VAZIO then Mes := CO_VAZIO else
  if (dmkEdMes.Enabled = False) then Mes := CO_VAZIO else
  begin
    Mes := (*dmkEdMes.Text[4]+dmkEdMes.Text[5]+*)dmkEdMes.Text[6]+dmkEdMes.Text[7]+(*'/'+*)
    dmkEdMes.Text[1]+dmkEdMes.Text[2](*+'/01'*);
    Me1 := Geral.Periodo2000(dmkEdTPVencto.Date);
    AnoX := Geral.IMV(Copy(Mes, 1, 2)) + 2000;
    MesX := Geral.IMV(Copy(Mes, 3, 2));
    DataX := EncodeDate(AnoX, MesX, 1);
    Me2 := Geral.Periodo2000(DataX);
    DifMes := Me2 - Me1;
    if Mes = CO_VAZIO then
    begin
      Geral.MensagemBox('AVISO. Defina o m�s do vencimento.', 'Aviso', MB_OK+MB_ICONWARNING);
      if dmkEdMes.Enabled then dmkEdMes.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  if (Mes = CO_VAZIO) and (dmkEdMes.Enabled = True) then
  begin
    Geral.MensagemBox('AVISO. Defina o m�s do vencimento.', 'Aviso', MB_OK+MB_ICONWARNING);
    if dmkEdMes.Enabled then dmkEdMes.SetFocus;
    Screen.Cursor := crDefault;
    Exit;
  end;
  // n�o mexer no vencimento aqui!
  {if (dmkEdTPVencto.Enabled = False) then Vencimento :=
    FormatDateTime(VAR_FORMATDATE, dmkEdTPdata.Date)
  else}
  Vencimento := FormatDateTime(VAR_FORMATDATE, dmkEdTPVencto.Date);
  if VAR_IMPORTANDO then Linha := VAR_IMPLINHA else Linha := 0;
  if VAR_FATURANDO then Sit := 3
  else if VAR_IMPORTANDO then Sit := -1
    else if VAR_BAIXADO <> -2 then Sit := VAR_BAIXADO
      else if QrCarteirasTipo.Value = 2 then Sit := 0
        else Sit := 3;
  TipoAnt := StrToInt(dmkEdTipoAnt.Text);
  CartAnt := StrToInt(dmkEdCartAnt.Text);
  MultaVal := Geral.DMV(dmkEdMultaVal.Text);
  MoraVal := Geral.DMV(dmkEdMoraVal.Text);
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', VAR_LCT, VAR_LCT, 'Controle');
    VAR_DATAINSERIR := dmkEdTPdata.Date;

  end else begin
    Controle := Geral.IMV(dmkEdControle.Text);
    Dmod.QrUpdM.SQL.Clear;
    ////////////////////////////////////////////////////////////////////////////
    // eliminar?                                                               //
    // ou s� alterar caso tenha que voltar a usar a altera��o neste fm?        //
    ////////////////////////////////////////////////////////////////////////////
    {
    Dmod.QrUpdM.SQL.Add(DELETE_FROM + VAR_LCT + ' WHERE Controle=:P0');
    Dmod.QrUpdM.Params[0].AsFloat := Controle;
    Dmod.QrUpdM.ExecSQL;
    UFinanceiro.RecalcSaldoCarteira(CartAnt, nil, False, False);
    //dbiSaveChanges(Dmod.QrUpdU.Handle);
    }
    UFinanceiro.ExcluiLct_Unico(True, Dmod.MyDB, TPOldData.Date,
      EdOldTipo.ValueVariant, EdOldCarteira.ValueVariant,
      EdOldControle.ValueVariant, EdOldSub.ValueVariant, False);
    VAR_DATAINSERIR := dmkEdTPdata.Date;
  end;

 if CkCancelado.Checked then
  begin
    Credito    := 0;
    Debito     := 0;
    Compensado := FormatDateTime(VAR_FORMATDATE, dmkEdTPData.Date);
    Sit        := 4;
  end;
  // 2013-03-13
  if (CkDuplicando.Checked) or (LaTipo.Caption = CO_INCLUSAO) then
  begin
    if QrCarteirasTipo.Value = 2 then
      Sit := 0;
  end;
  // FIM 2013-03-13
  if not CkNaoPesquisar.Checked then
  begin
    LaAviso.Caption := 'Verificando duplica��es';
    if UFinanceiro.NaoDuplicarLancto(LaTipo.Caption, dmkEdDoc.ValueVariant,
    CkPesqCH.Checked, dmkEdSerieCH.Text, dmkEdNF.ValueVariant, CkPesqNF.Checked,
    Credito, Debito, Genero, Mes, CkPesqVal.Checked,
    Geral.IMV(dmkEdCBCliInt.Text), False,
    Carteira, Cliente, Fornecedor, LaAviso) <> 1 then
    begin
      Geral.MensagemBox('Inclus�o cancelada pelo usu�rio!', 'Aviso', MB_OK+MB_ICONWARNING);
      LaAviso.Caption := '...';
      Exit;
    end;
  end;

  if (LaTipo.Caption = CO_ALTERACAO) then
  begin
    Documen := dmkEdDoc.ValueVariant;
    SerieCH := dmkEdSerieCH.Text;
  end else
  if (QrCarteirasUsaTalao.Value = 1) and (LaTipo.Caption = CO_INCLUSAO)  and
  (dmkEdDoc.Enabled = False) and CkDuplicando.Checked then
  begin
    Documen := dmkEdDoc.ValueVariant;
    SerieCH := dmkEdSerieCH.Text;
    //
  end else
  if (QrCarteirasUsaTalao.Value = 1) and (LaTipo.Caption = CO_INCLUSAO) and
  (dmkEdDoc.ValueVariant = 0) then
  begin
    if CkParcelamento.Checked then
    begin
      if UMyMod.ObtemQtdeCHTaloes(QrCarteirasCodigo.Value) < TbParcpagtos.RecordCount then
        if Geral.MensagemBox(
        'N�o existem folhas de cheque suficientes para todos lan�amentos.' +
        #13#10 + 'Deseja continuar assim mesmo?', 'Pergunta',
        MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
          Exit;
    end;
    //  Parcelamento ser� feito adiante
    if CkParcelamento.Checked = False then
    begin
      if not UMyMod.ObtemProximoCHTalao(QrCarteirasCodigo.Value,
      dmkEdSerieCH, dmkEdDoc) then
        Exit
      else begin
        Documen := dmkEdDoc.ValueVariant;
        SerieCH := dmkEdSerieCH.Text;
      end;
    end;
  end else// Doc := dmkEdDoc.ValueVariant;
  if (dmkEdDoc.Enabled = False) and (VAR_BAIXADO = -2) then
  Documen := 0
  else begin
    Documen := Geral.DMV(dmkEdDoc.Text);
    SerieCH := dmkEdSerieCH.Text;
    if (QrCarteirasExigeNumCheque.Value = 1) and (Documen=0) then
    begin
      Geral.MensagemBox('Esta carteira exige o n�mero do cheque (documento)!',
      'Aviso', MB_OK+MB_ICONWARNING);
      dmkEdDoc.SetFocus;
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;

  VAR_LANCTO2 := Controle;

  LaAviso.Caption := 'Inserindo dados';
  UFinanceiro.LancamentoDefaultVARS;
  FLAN_IndiPag    := EdIndiPag.ValueVariant;
  FLAN_EventosCad := EdEventosCad.ValueVariant;
  if FLAN_EventosCad <> 0 then
    FLAN_EventosCad := QrEventosCadCodigo.Value;
  FLAN_Documento  := Trunc(Documen);
  FLAN_Data       := FormatDateTime(VAR_FORMATDATE, dmkEdTPdata.Date);
  FLAN_Tipo       := QrCarteirasTipo.Value;
  FLAN_Carteira   := Carteira;
  FLAN_Credito    := Credito;
  FLAN_Debito     := Debito;
  FLAN_Genero     := Genero;
  FLAN_NotaFiscal := NF;
  FLAN_Vencimento := Vencimento;
  FLAN_Mez        := Mes;
  FLAN_Descricao  := dmkEdDescricao.Text;
  FLAN_Sit        := Sit;
  FLAN_Controle   := Controle;
  FLAN_Cartao     := Cartao;
  FLAN_Compensado := Compensado;
  FLAN_Linha      := Linha;
  FLAN_Fornecedor := Fornecedor;
  FLAN_Cliente    := Cliente;
  FLAN_MoraDia    := MoraDia;
  FLAN_Multa      := Multa;
  FLAN_DataCad    := FormatDateTime(VAR_FORMATDATE, Date);
  FLAN_UserCad    := Funci;
  FLAN_DataDoc    := FormatDateTime(VAR_FORMATDATE, dmkEdTPDataDoc.Date);
  FLAN_Vendedor   := Vendedor;
  FLAN_Account    := Account;
  FLAN_FatID      := dmkEdFatID.ValueVariant;
  FLAN_FatID_Sub  := dmkEdFatID_Sub.ValueVariant;
  FLAN_ICMS_P     := Geral.DMV(dmkEdICMS_P.Text);
  FLAN_ICMS_V     := Geral.DMV(dmkEdICMS_V.Text);
  FLAN_Duplicata  := dmkEdDuplicata.Text;
  FLAN_CliInt     := CliInt;
  FLAN_Depto      := Depto;
  FLAN_DescoPor   := dmkEdDescoPor.ValueVariant;
  FLAN_ForneceI   := ForneceI;
  FLAN_DescoVal   := dmkEdDescoVal.ValueVariant;
  FLAN_NFVal      := dmkEdNFVAl.ValueVariant;
  FLAN_Unidade    := dmkEdUnidade.ValueVariant;
  //FLAN_Qtde       := Qtde;  ABAIXO
  FLAN_FatNum     := MLAGeral.I64MV(dmkEdFatNum.ValueVariant);
  FLAN_FatParcela := dmkedFatParcela.ValueVariant;
  FLAN_Doc2       := dmkEdDoc2.ValueVariant;
  FLAN_SerieCH    := SerieCH;
  FLAN_MultaVal   := MultaVal;
  FLAN_MoraVal    := MoraVal;
  FLAN_CtrlIni    := dmkEdCtrlIni.ValueVariant;
  FLAN_Nivel      := dmkEdNivel.ValueVariant;
  FLAN_ID_Pgto    := dmkEdID_Pgto.ValueVariant;
  FLAN_CNAB_Sit   := dmkEdCNAB_Sit.ValueVariant;
  FLAN_TipoCH     := dmkRGTipoCH.ItemIndex;
  if CkDuplicando.Checked then
    FLAN_Protocolo  := 0
  else
    FLAN_Protocolo  := EdProtocolo.ValueVariant;
  //
  if dmkEdQtde.Text <> '' then
    FLAN_Qtde := Geral.DMV(dmkEdQtde.Text);

  Parcelas := 0;
  if CkParcelamento.Checked then
  begin
    TbParcpagtos.First;
    while not TbParcpagtos.Eof do
    begin
      if (Me1 > 0) then
      begin
        Me2 := Geral.Periodo2000(TbParcpagtosData.Value) + DifMes;
        FLAN_Mez := IntToStr(MLAGeral.PeriodoToAnoMes(Me2));
        FLAN_Descricao  := TbParcPagtosDescricao.Value;
        if (CkIncremTxt.Checked = False) then
          FLAN_Descricao  := FLAN_Descricao +
            ' - ' + MLAGeral.MesEAnoDoPeriodo(Me2)
      end else
      begin
        FLAN_Mez := '';
        FLAN_Descricao  := TbParcPagtosDescricao.Value;
      end;
      FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
        'Controle', VAR_LCT, VAR_LCT, 'Controle');
      FLAN_Duplicata  := TbParcPagtosDuplicata.Value;
      FLAN_Credito    := TbParcpagtosCredito.Value;
      FLAN_Debito     := TbParcpagtosDebito.Value;
      FLAN_MoraDia    := dmkEdPercJuroM.ValueVariant;//FmCondGer.QrCondPercJuros.Value;
      FLAN_Multa      := dmkEdPercMulta.ValueVariant;//FmCondGer.QrCondPercMulta.Value;
      FLAN_ICMS_V     := TbParcpagtosICMS_V.Value;
      FLAN_Vencimento := Geral.FDT(TbParcpagtosData.Value, 1);
      FLAN_Sit        := Sit;
      if (QrCarteirasUsaTalao.Value = 1) and (LaTipo.Caption = CO_INCLUSAO)  and
      (dmkEdDoc.Enabled = False) and CkDuplicando.Checked then
      begin
        FLAN_Documento := dmkEdDoc.ValueVariant;
        FLAN_SerieCH   := dmkEdSerieCH.Text;
      end else
      if (QrCarteirasUsaTalao.Value = 1) and (LaTipo.Caption = CO_INCLUSAO)  and
      (CkDuplicando.Checked = False) then
      begin
        if not UMyMod.ObtemProximoCHTalao(QrCarteirasCodigo.Value,
        dmkEdSerieCH, dmkEdDoc) then
          Exit
        else
        begin
          FLAN_Documento := dmkEdDoc.ValueVariant;
          FLAN_SerieCH   := dmkEdSerieCH.Text;
        end;
      end else FLAN_Documento := TbParcpagtosDoc.Value;
      //
      if UFinanceiro.InsereLancamento() then
      begin
        Inc(Parcelas, 1);
        IncrementaExecucoes;
      end;
      TbParcPagtos.Next;
    end;
    Inseriu := TbParcPagtos.RecordCount = Parcelas;
    if not Inseriu then
      Geral.MensagemBox('ATEN��O! Foram inseridas ' +
      Geral.FF0(Parcelas) + ' de ' + Geral.FF0(TbParcPagtos.RecordCount) + '!',
      'Aviso de diverg�ncias', MB_OK+MB_ICONWARNING);
  end else Inseriu := UFinanceiro.InsereLancamento();


  LaAviso.Caption := 'Incrementando execu��es';
  if Inseriu then
  begin
    IncrementaExecucoes;
    //
    if LaTipo.Caption = CO_INCLUSAO then
      UFinanceiro.RecalcSaldoCarteira(dmkCBCarteira.KeyValue, nil, False, False)
    else// begin if CBNovo.Checked = False then
      begin
        UFinanceiro.RecalcSaldoCarteira(dmkCBCarteira.KeyValue, nil, False, False);
        UFinanceiro.RecalcSaldoCarteira(CartAnt, nil, False, False);
    end;
    //
    VAR_VALOREMITIRIMP := VAR_VALOREMITIRIMP - Credito - Debito;
    if CkCopiaCH.Checked then
      FmFinForm.ImprimeCopiaDeCh(FLAN_Controle, Geral.IMV(dmkEdCBCliInt.Text));
    if (CkContinuar.Checked) and (LaTipo.Caption = CO_INCLUSAO) then
    begin
      CkParcelamento.Checked       := False;
      PageControl1.ActivePageIndex := 0;
      Geral.MensagemBox('Inclus�o concluida.', 'Aviso', MB_OK+MB_ICONINFORMATION);
      if Pagecontrol1.ActivePageIndex = 0 then
      if dmkEdTPData.Enabled then dmkEdTPData.SetFocus else dmkEdCBGenero.SetFocus;
      // Tal�o de cheque!
      if dmkEdDoc.Enabled = False then
      begin
        dmkEdDoc.ValueVariant := 0;
        dmkEdSerieCH.Text     := '';
      end;
    end else Close;
  end;
  finally
    Screen.Cursor := crDefault;
    LaAviso.Caption := '...';
  end;  
end;

procedure TFmLctEdit.FormCreate(Sender: TObject);
var
  CliInt: Integer;
begin
  dmkEdTPData.MinDate := VAR_DATA_MINIMA;
  PnLancto1.BevelOuter  := bvNone;
  PnDepto.BevelOuter    := bvNone;
  PnForneceI.BevelOuter := bvNone;
  PnAccount.BevelOuter  := bvNone;
  //PnEdit05.BevelOuter := bvNone;
  //PnEdit06.BevelOuter := bvNone;
  PnLancto2.BevelOuter  := bvNone;
  PnLancto3.BevelOuter  := bvNone;
  //Width := FLargMenor;
  FCriandoForm := True;
  QrContas.Open;
  if VAR_CXEDIT_ICM then
  begin
    LaICMS_P.Visible := True;
    LaICMS_V.Visible := True;
    dmkEdICMS_P.Visible := True;
    dmkEdICMS_V.Visible := True;
  end;
  //if Trim(VAR_CLIENTEC) <> '' then
  //begin
    QrClientes.SQL.Clear;
    QrClientes.SQL.Add('SELECT Codigo, Account, ');
    QrClientes.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrClientes.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrClientes.SQL.Add('FROM entidades');
    QrClientes.SQL.Add('WHERE Cliente1="V"');
    QrClientes.SQL.Add('OR    Cliente2="V"');
    QrClientes.SQL.Add('OR    Cliente3="V"');
    QrClientes.SQL.Add('OR    Cliente4="V"');
    QrClientes.SQL.Add('OR    Terceiro="V"');
    QrClientes.SQL.Add('ORDER BY NOMEENTIDADE');
    QrClientes.Open;
    //////
    LaCliente.Visible := True;
    dmkEdCBCliente.Visible := True;
    dmkCBCliente.Visible := True;
    //////
    //dmkEdCBCliInt.Enabled := False;
    //dmkCBCliInt.Enabled := False;
    if not DmodG.SelecionarCliInt(CliInt, True) then Exit;
    dmkEdCBCliInt.Text := IntToStr(CliInt);
    dmkCBCliInt.KeyValue := CliInt;
  //end;
  ReopenFornecedores(0);
  if Trim(VAR_FORNECEF) <> '' then
  begin
    //
    LaFornecedor.Visible := True;
    dmkEdCBFornece.Visible := True;
    dmkCBFornece.Visible := True;
  end;
  if Trim(VAR_FORNECEV) <> '' then
  begin
    QrVendedores.SQL.Clear;
    QrVendedores.SQL.Add('SELECT Codigo, ');
    QrVendedores.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrVendedores.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrVendedores.SQL.Add('FROM entidades');
    QrVendedores.SQL.Add('WHERE Fornece'+VAR_FORNECEV+'="V"');
    QrVendedores.SQL.Add('ORDER BY NOMEENTIDADE');
    QrVendedores.Open;
    //////
    QrAccounts.SQL.Clear;
    QrAccounts.SQL.Add('SELECT Codigo, ');
    QrAccounts.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrAccounts.SQL.Add('ELSE Nome END NOMEENTIDADE');
    QrAccounts.SQL.Add('FROM entidades');
    QrAccounts.SQL.Add('WHERE Fornece'+VAR_FORNECEV+'="V"');
    QrAccounts.SQL.Add('ORDER BY NOMEENTIDADE');
    QrAccounts.Open;
    //////
    LaVendedor.Visible := True;
    dmkEdCBVendedor.Visible := True;
    dmkCBVendedor.Visible   := True;
    PnAccount.Visible := True;
  end;
  dmkEdTPDataDoc.Date := Date;
  if VAR_LANCTOCONDICIONAL1 then
  begin
    VAR_DATAINSERIR := Date;
    dmkEdTPDataDoc.Enabled := False;
    LaDataDoc.Enabled := False;
    //
    dmkEdCarteira.Enabled := False;
    dmkCBCarteira.Enabled := False;
    dmkEdTPdata.Enabled := False;
  end;
  if VAR_TIPOVARCLI <> '' then ExigeFuncionario();
  PageControl1.ActivePageIndex := 0;
  TabControl1.TabIndex := 0;
  ReopenIndiPag();
end;

procedure TFmLctEdit.FormActivate(Sender: TObject);
var
  Carteira: Integer;
begin
  CarteirasReopen;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    CkContinuar.Visible := True;
    CkParcelamento.Visible := True;
    //
    if VLAN_QrCarteiras <> nil then
    begin
      Carteira := VLAN_QrCarteiras.FieldByName('Codigo').AsInteger;
      if Carteira <> 0 then
      begin
        dmkEdCarteira.ValueVariant := Carteira;
        dmkCBCarteira.KeyValue     := Carteira;
      end;
    end;
  end;
  if dmkEdCarteira.Enabled then dmkEdCarteira.SetFocus;
  VerificaEdits();
  if FCriandoForm then
  begin
    ConfiguracoesIniciais();
  end;
  FCriandoForm := False;
  MyObjects.CorIniComponente();
  dmkEdCarteiraChange(Self);
end;

procedure TFmLctEdit.ConfiguraComponentesCarteira;
begin
  case QrCarteirasTipo.Value of
    0:
    begin
      if (QrCarteirasPrazo.Value = 1) then
      begin
        if (QrCarteirasUsaTalao.Value = 0) then
        begin
          LaDoc.Enabled := True;
          dmkEdSerieCH.Enabled := True;
          dmkEdDoc.Enabled := True;
        end;
        dmkEdNF.Enabled := True;
        LaNF.Enabled := True;
      end else begin
        // N�mero bloqueto no cheque
        if (QrCarteirasUsaTalao.Value = 0) then
        begin
          LaDoc.Enabled := True;
          dmkEdSerieCH.Enabled := True;
          dmkEdDoc.Enabled := True;
        end;
        //
        dmkEdNF.Enabled := True;
        LaNF.Enabled := True;
      end;
    end;
    1:
    begin
      if (QrCarteirasUsaTalao.Value = 0) then
      begin
        LaDoc.Enabled := True;
        dmkEdSerieCH.Enabled := True;
        dmkEdDoc.Enabled := True;
      end;
    end;
    2:
    begin
      if (QrCarteirasUsaTalao.Value = 0) then
      begin
        LaDoc.Enabled := True;
        dmkEdSerieCH.Enabled := True;
        dmkEdDoc.Enabled := True;
      end;
      //
      dmkEdNF.Enabled := True;
      LaNF.Enabled := True;
    end;
  end;
  ConfiguraVencimento;
end;

procedure TFmLctEdit.BtContasClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    FmContas.LocCod(QrContasCodigo.Value, QrContasCodigo.Value);
    FmContas.ShowModal;
    FmContas.Destroy;
    QrContas.Close;
    QrContas.Open;
    dmkEdCBGenero.Text := IntToStr(VAR_CONTA);
    dmkCBGenero.KeyValue := VAR_CONTA;
    //
    if (LaTipo.Caption = CO_INCLUSAO) and (CkDuplicando.Checked = False) then
      dmkEdDescricao.Text := dmkCBGenero.Text;
    //Dmod.DefParams;
  end;
end;

function TFmLctEdit.VerificaVencimento: Boolean;
var
  Carteira: Integer;
  Data: TDateTime;
begin
  Result := True;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    if QrCarteirasFatura.Value = 'V' then
    begin
      if dmkCBCarteira.KeyValue <> Null then Carteira := dmkCBCarteira.KeyValue
      else Carteira := 0;
      QrFatura.Close;
      QrFatura.Params[0].AsInteger := Carteira;
      QrFatura.Open;
      if (QrFatura.RecordCount > 0) and (dmkCBCarteira.KeyValue <> Null) then
      begin
        Data := QrFaturaData.Value;
        while (Data - QrCarteirasFechamento.Value) < dmkEdTPdata.Date do
          Data := IncMonth(Data, 1);
        if int(dmkEdTPVencto.Date) <> Int(Data) then
        begin
          case Geral.MensagemBox(
          'A configura��o do sistema sugere a data de vencimento ' +
          FormatDateTime(VAR_FORMATDATE3, Data)+' ao inv�s de ' +
          FormatDateTime(VAR_FORMATDATE3, dmkEdTPVencto.Date) +
          '. Confirma a altera��o?', VAR_APPNAME,
          MB_ICONQUESTION+MB_YESNOCANCEL+MB_DEFBUTTON1+MB_APPLMODAL) of
            ID_YES    : dmkEdTPVencto.Date := Data;
            ID_CANCEL : Result := False;
            ID_NO     : ;
          end;
        end;
      end;
      QrFatura.Close;
    end;
  end;
end;

procedure TFmLctEdit.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLctEdit.SbCarteiraClick(Sender: TObject);
begin
  VAR_CARTNUM := Geral.IMV(dmkEdCarteira.Text);
  FmPrincipal.CadastroDeCarteiras(QrCarteirasTipo.Value,
    Geral.IMV(dmkEdCarteira.Text));
  QrCarteiras.Close;
  QrCarteiras.Open;
  // N�o fazer, a carteira talvez n�o seje do cliente interno selecionado!
  //dmkEdCarteira.Text := IntToStr(VAR_CARTNUM);
  //dmkCBCarteira.KeyValue := VAR_CARTNUM;
  DmodFin.DefParams(VLAN_QrCarteiras, nil, False, 'TFmLctEdit.SbCarteiraClick()');
end;

procedure TFmLctEdit.SbEventosCadClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmEventosCad, FmEventosCad, afmoNegarComAviso) then
  begin
    FmEventosCad.ShowModal;
    FmEventosCad.Destroy;
    //
    if VAR_CADASTRO <> 0 then
      UMyMod.SetaCodUsuDeCodigo(EdEventosCad, CBEventosCad, QrEventosCad,
        VAR_CADASTRO);
  end;
end;

procedure TFmLctEdit.SbIndiPagClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmIndiPag, FmIndiPag, afmoNegarComAviso) then
  begin
    FmIndiPag.ShowModal;
    FmIndiPag.Destroy;
    //
    if VAR_CADASTRO <> 0 then
      UMyMod.SetaCodigoPesquisado(EdIndiPag, CBIndiPag, QrIndiPag,
        VAR_CADASTRO);
  end;
end;

procedure TFmLctEdit.QrContasAfterScroll(DataSet: TDataSet);
begin
  VerificaEdits();
end;

procedure TFmLctEdit.SetaAccount(var Key: Word; Shift: TShiftState);
begin
  if key=VK_F4 then
  begin
    dmkEdCBAccount.Text   := IntToStr(QrClientesAccount.Value);
    dmkCBAccount.KeyValue := QrClientesAccount.Value;
  end;
end;

procedure TFmLctEdit.ExigeFuncionario();
begin
  QrFunci.Close;
  QrFunci.SQL.Clear;
  QrFunci.SQL.Add('SELECT Codigo,');
  QrFunci.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
  QrFunci.SQL.Add('ELSE Nome END NOMEENTIDADE');
  QrFunci.SQL.Add('FROM entidades');
  QrFunci.SQL.Add('WHERE '+VAR_TIPOVARCLI+'="V"');
  QrFunci.SQL.Add('ORDER BY NomeENTIDADE');
  QrFunci.Open;
  Label10.Visible      := True;
  dmkEdCBFunci.Visible := True;
  dmkCBFunci.Visible   := True;
end;

procedure TFmLctEdit.CalculaICMS;
var
  C, D, B, V, P: Double;
begin
  V := Geral.DMV(dmkEdICMS_V.Text);
  P := Geral.DMV(dmkEdICMS_P.Text);
  C := Geral.DMV(dmkEdCred.Text);
  D := Geral.DMV(dmkEdDeb.Text);
  //if C > D then B := C - D else B := D - C;
  B := C-D;
  if FICMS = 0 then V := B * P / 100
  else if B <> 0 then P := V / B * 100 else P := 0;
  dmkEdICMS_V.Text := Geral.FFT(V, 2, siPositivo);
  dmkEdICMS_P.Text := Geral.FFT(P, 2, siPositivo);
end;

procedure TFmLctEdit.EdDuplicataKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key in ([VK_DOWN, VK_UP]) then
  begin
    if (key=VK_DOWN) then
      dmkEdDuplicata.Text := MLAGeral.DuplicataIncrementa(dmkEdDuplicata.Text, -1)
    else
      dmkEdDuplicata.Text := MLAGeral.DuplicataIncrementa(dmkEdDuplicata.Text,  1);
  end;
end;

function TFmLctEdit.ReopenLct: Boolean;
var
  Ini, Fim: String;
begin
  Ini := FormatDateTime(VAR_FORMATDATE, 1);
  Fim := FormatDateTime(VAR_FORMATDATE, Date);
  QrLct.Close;
  QrLct.SQL.Clear;
  QrLct.SQL.Add('SELECT MOD(la.Mez, 100) Mes2,');
  QrLct.SQL.Add('((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano,');
  QrLct.SQL.Add('la.*, ct.Codigo CONTA,');
  QrLct.SQL.Add('ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,');
  QrLct.SQL.Add('gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,');
  QrLct.SQL.Add('CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial');
  QrLct.SQL.Add('ELSE em.Nome END NOMEEMPRESA,');
  QrLct.SQL.Add('CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial');
  QrLct.SQL.Add('ELSE cl.Nome END NOMECLIENTE,');
  QrLct.SQL.Add('CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial');
  QrLct.SQL.Add('ELSE fo.Nome END NOMEFORNECEDOR,');
  QrLct.SQL.Add('ca.Nome NOMECARTEIRA, ca.Saldo SALDOCARTEIRA');
  QrLct.SQL.Add('FROM ' + VAR_LCT + ' la');
  QrLct.SQL.Add('LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = 0');
  QrLct.SQL.Add('LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo');
  QrLct.SQL.Add('LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo');
  QrLct.SQL.Add('LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto');
  QrLct.SQL.Add('LEFT JOIN entidades em ON em.Codigo=ct.Empresa');
  QrLct.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
  QrLct.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor');
  QrLct.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
  //
  QrLct.SQL.Add('WHERE la.Data BETWEEN "'+Ini+'" AND "'+Fim+'"');
  //
  case TabControl1.TabIndex of
    0: QrLct.SQL.Add('AND la.Carteira='  +IntToStr(Geral.IMV(dmkEdCarteira.Text)));
    1: QrLct.SQL.Add('AND ca.ForneceI='  +IntToStr(Geral.IMV(dmkEdCBCliInt.Text)));
    2: QrLct.SQL.Add('AND la.Cliente='   +IntToStr(Geral.IMV(dmkEdCBCliente.Text)));
    3: QrLct.SQL.Add('AND la.Fornecedor='+IntToStr(Geral.IMV(dmkEdCBFornece.Text)));
    4: QrLct.SQL.Add('AND la.Genero='    +IntToStr(Geral.IMV(dmkEdCBGenero.Text)));
  end;
  QrLct.SQL.Add('ORDER BY la.Data DESC, la.Controle DESC');
  QrLct.Open;
  Result := True;
end;

procedure TFmLctEdit.TabSheet2Resize(Sender: TObject);
begin
  (*DBGrid1.Align := alClient;
  DBGrid1.Align := alNone;
  DBGrid1.Top := 24;
  DBGrid1.Height := DBGrid1.Height - 24;*)
end;

procedure TFmLctEdit.PageControl1Change(Sender: TObject);
begin
  ReopenLct;
{
  if PageControl1.ActivePageIndex in ([2,3]) then
    WindowState := wsMaximized
  else
    WindowState := wsNormal;
}
end;

procedure TFmLctEdit.TabControl1Change(Sender: TObject);
begin
  ReopenLct;
end;

procedure TFmLctEdit.TPDataChange(Sender: TObject);
begin
  ConfiguraVencimento;
end;

procedure TFmLctEdit.ConfiguraVencimento;
begin
  if LaTipo.Caption = CO_INCLUSAO then
    dmkEdTPVencto.Date := dmkEdTPdata.Date;
end;

procedure TFmLctEdit.QrCarteirasAfterScroll(DataSet: TDataSet);
begin
  ConfiguraComponentesCarteira;
end;

procedure TFmLctEdit.ReopenFornecedores(Tipo: Integer);
begin
  QrFornecedores.Close;
  QrFornecedores.SQL.Clear;
  QrFornecedores.SQL.Add('SELECT Codigo, ');
  if Tipo = 0 then
  begin
    QrFornecedores.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
    QrFornecedores.SQL.Add('ELSE Nome END NOMEENTIDADE');
    //
    LaFornecedor.Font.Color := LaForneceRN.Font.Color;
  end else begin
    QrFornecedores.SQL.Add('CASE WHEN Tipo=0 THEN Fantasia');
    QrFornecedores.SQL.Add('ELSE Apelido END NOMEENTIDADE');
    //
    LaFornecedor.Font.Color := LaForneceFA.Font.Color;
  end;
  QrFornecedores.SQL.Add('FROM entidades');
  QrFornecedores.SQL.Add('WHERE Fornece1="V"');
  QrFornecedores.SQL.Add('OR    Fornece2="V"');
  QrFornecedores.SQL.Add('OR    Fornece3="V"');
  QrFornecedores.SQL.Add('OR    Fornece4="V"');
  QrFornecedores.SQL.Add('OR    Fornece5="V"');
  QrFornecedores.SQL.Add('OR    Fornece6="V"');
  QrFornecedores.SQL.Add('OR    Terceiro="V"');
  QrFornecedores.SQL.Add('ORDER BY NOMEENTIDADE');
  QrFornecedores.Open;
  //
end;

procedure TFmLctEdit.ReopenIndiPag();
begin
  QrIndiPag.Close;
  QrIndiPag.Open;
end;

procedure TFmLctEdit.QrCliIntAfterScroll(DataSet: TDataSet);
begin
  if FIDFinalidade = idflIndefinido then
    FIDFinalidade := TIDFinalidadeLct(Geral.IMV(Copy(LaFinalidade.Caption, 1, 3)));
  case FIDFinalidade of
    idflProprios: //1: // Pr�prios
    begin
      //Nada
    end;
    idflCondominios://2: // Condom�nios
    begin
      QrForneceI.Close;
      QrForneceI.SQL.Clear;
{
      QrForneceI.SQL.Add('SELECT Codigo,');
      QrForneceI.SQL.Add('CASE WHEN Tipo=0 THEN RazaoSocial');
      QrForneceI.SQL.Add('ELSE Nome END NOMEENTIDADE');
      QrForneceI.SQL.Add('FROM entidades');
      QrForneceI.SQL.Add('WHERE Cliente2="V"');
      QrForneceI.SQL.Add('AND Codigo in (');
      QrForneceI.SQL.Add('  SELECT ci2.Propriet');
      QrForneceI.SQL.Add('  FROM condimov ci2');
      QrForneceI.SQL.Add('  LEFT JOIN cond co2 ON co2.Codigo=ci2.Codigo');
      QrForneceI.SQL.Add('  WHERE co2.Cliente IN (' + VAR_LIB_EMPRESAS + ')');
      QrForneceI.SQL.Add(') ORDER BY NOMEENTIDADE');
}
      QrForneceI.SQL.Add('SELECT DISTINCT ent.Codigo,');
      QrForneceI.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
      QrForneceI.SQL.Add('ELSE ent.Nome END NOMEENTIDADE');
      QrForneceI.SQL.Add('FROM condimov ci2');
      QrForneceI.SQL.Add('LEFT JOIN cond con ON con.Codigo = ci2.Codigo');
      QrForneceI.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=ci2.Propriet');
      //QrForneceI.SQL.Add('WHERE ci2.Codigo IN (' + VAR_LIB_EMPRESAS + ')');
      QrForneceI.SQL.Add('WHERE con.Cliente IN (' + VAR_LIB_EMPRESAS + ')');
      QrForneceI.SQL.Add('ORDER BY NOMEENTIDADE');
      QrForneceI.SQL.Add('');
      QrForneceI.Open;
    end;
    idflCursosCiclicos: //3: // Cursos c�clicos
    begin
      //Nada ?
    end;
  end;
end;

procedure TFmLctEdit.SpeedButton2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntidades, FmEntidades, afmoNegarComAviso) then
  begin
    FmEntidades.LocCod(QrFornecedoresCodigo.Value, QrFornecedoresCodigo.Value);
    FmEntidades.ShowModal;
    FmEntidades.Destroy;
  end;
  QrFornecedores.Close;
  QrFornecedores.Open;
  dmkEdCBFornece.Text := IntToStr(VAR_ENTIDADE);
  dmkCBFornece.KeyValue := VAR_ENTIDADE;
end;

procedure TFmLctEdit.BitBtn1Click(Sender: TObject);
{
var
  Valor, Benef, Cidade: String;
}
begin
  MyPrinters.EmiteCheque(0, 0, QrCarteirasBanco1.Value,
    dmkEdDeb.ValueVariant, dmkCBFornece.Text,
    dmkEdCred.ValueVariant, dmkCBCliente.Text,
    ''(*Serie*), 0(*Cheque*), 0(*Data*), 0(*Carteira*),
    ''(*CampoNomeFornecedor*), nil (*Grade*), VAR_LCT, -1, 0, 0);
 {
  if dmkEdDeb.Enabled then
  begin
    Valor := dmkEdDeb.Text;
    Benef := dmkCBFornece.Text;
  end else begin
    Valor := dmkEdCred.Text;
    Benef := dmkCBCliente.Text;
  end;
  //
  Benef := Geral.SemAcento(Geral.Maiusculas(Benef, False));
  Cidade := Geral.SemAcento(Geral.Maiusculas(Dmod.QrDonoCIDADE.Value, False));
  //
  if DBCheck.CriaFm(TFmEmiteCheque, FmEmiteCheque, afmoNegarComAviso) then
  begin
    FmEmiteCheque.TPData.Date   := dmkEdTPdata.Date;
    FmEmiteCheque.EdValor.Text  := Valor;
    FmEmiteCheque.EdBenef.Text  := Benef;
    FmEmiteCheque.EdCidade.Text := Cidade;
    //
    FmEmiteCheque.ShowModal;
    (*if FmEmiteCheque.ST_CMC7.Caption <> '' then
      EdBanda1.Text := MLAGeral.SoNumeroECMC7(FmEmiteCheque.ST_CMC7.Caption);*)
    FmEmiteCheque.Destroy;
  end;
  }
end;

procedure TFmLctEdit.MostraPnMaskPesq(Link: String);
begin
  PnLink.Caption     := Link;
  //Width := FLargMaior;
  //PnMaskPesq.Visible := True;
  EdLinkMask.Text    := '';
  EdLinkMask.SetFocus;
end;

procedure TFmLctEdit.SelecionaItemDePesquisa;
begin
  if QrPesq.RecordCount > 0 then
  begin
    //Width := FLargMenor;
    //PnMaskPesq.Visible := False;
    if PnLink.Caption = 'Conta (Plano de contas)' then
    begin
      dmkEdCBGenero.Text     := IntToStr(QrPesqCodigo.Value);
      dmkCBGenero.KeyValue := QrPesqCodigo.Value;
      if dmkCBGenero.Visible then dmkCBGenero.SetFocus;
    end
  end;
end;

procedure TFmLctEdit.LLBPesqDblClick(Sender: TObject);
begin
  SelecionaItemDePesquisa;
end;

procedure TFmLctEdit.LLBPesqEnter(Sender: TObject);
begin
  DBEdit1.DataSource := DsPesq;
  DBEdit2.DataSource := DsPesq;
  DBEdit3.DataSource := DsPesq;
  DBEdit4.DataSource := DsPesq;
end;

procedure TFmLctEdit.LLBPesqExit(Sender: TObject);
begin
  DBEdit1.DataSource := DsContas;
  DBEdit2.DataSource := DsContas;
  DBEdit3.DataSource := DsContas;
  DBEdit4.DataSource := DsContas;
end;

procedure TFmLctEdit.EdLinkMaskChange(Sender: TObject);
begin
  QrPesq.Close;
  if Length(EdLinkMask.Text) >= 3 then
  begin
    if PnLink.Caption = 'Conta (Plano de contas)' then
    begin
      QrPesq.Close;
      QrPesq.Params[0].AsString := '%' + EdLinkMask.Text + '%';
      QrPesq.Open;
      LLBPesq.ListSource := DsPesq;
    end
  end;
end;

procedure TFmLctEdit.SpeedButton3Click(Sender: TObject);
begin
  //Width := FLargMenor;
  //PnMaskPesq.Visible := False;
end;

procedure TFmLctEdit.LLBPesqKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = 13 then SelecionaItemdePesquisa;
end;

procedure TFmLctEdit.EdLinkMaskKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_DOWN then LLBPesq.SetFocus;
end;

procedure TFmLctEdit.EventosCadReopen(Entidade: Integer);
begin
  QrEventosCad.Close;
  QrEventosCad.Params[0].AsInteger := Entidade;
  QrEventosCad.Open;
  //
  LaEventosCad.Enabled := QrEventosCad.RecordCount > 0;
  EdEventosCad.Enabled := QrEventosCad.RecordCount > 0;
  CBEventosCad.Enabled := QrEventosCad.RecordCount > 0;
  SbEventosCad.Enabled := QrEventosCad.RecordCount > 0;
  if not QrEventosCad.Locate('CodUsu', QrEventosCadCodUsu.Value, []) then
  begin
    EdEventosCad.ValueVariant := 0;
    CBEventosCad.KeyValue     := 0;
  end;
end;

procedure TFmLctEdit.CkParcelamentoClick(Sender: TObject);
begin
  if CkParcelamento.Checked then
  begin
    GBParcelamento.Visible := True;
    DBGParcelas.Visible := True;
    CalculaParcelas;
  end else begin
    GBParcelamento.Visible := False;
    DBGParcelas.Visible := True;
  end;
end;

procedure TFmLctEdit.RGPeriodoClick(Sender: TObject);
begin
  if RGPeriodo.ItemIndex = 1 then
  begin
    EdDias.Enabled := True;
    EdDias.SetFocus;
  end else begin
    EdDias.Enabled := False;
  end;
  CalculaParcelas;
end;

procedure TFmLctEdit.EdDiasExit(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLctEdit.RGIncremCHClick(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLctEdit.CkArredondarClick(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLctEdit.RGArredondarClick(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLctEdit.DBGParcelasKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then if TbParcPagtos.State in ([dsEdit, dsInsert])
  then TbParcpagtos.Post;
end;

function TFmLctEdit.GetTipoValor: TTipoValor;
var
  D,C: Double;
begin
  if dmkEdDeb.Enabled  then D := Geral.DMV(dmkEdDeb.Text)  else D := 0;
  if dmkEdCred.Enabled then C := Geral.DMV(dmkEdCred.Text) else C := 0;
  FValorAParcelar := D + C;
  if (D>0) and (C>0) then
  begin
    Geral.MensagemBox('Valor n�o pode ser cr�dito e d�bito ao mesmo tempo!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Result := tvNil;
    Exit;
  end else if D>0 then Result := tvDeb
  else if C>0 then Result := tvCred
  else Result := tvNil;
end;

procedure TFmLctEdit.CkIncremDUClick(Sender: TObject);
begin
  RGIncremDupl.Visible := CkIncremDU.Checked;
  CalculaParcelas;
end;

procedure TFmLctEdit.EdDuplSepExit(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLctEdit.RGDuplSeqClick(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLctEdit.BitBtn2Click(Sender: TObject);
var
  ValOrig, ValMult, ValJuro, ValNovo, PerMult, ValDife: Double;
begin
  ValOrig := Geral.DMV(dmkEdCred.Text) - Geral.DMV(dmkEdDeb.Text);
  if ValOrig < 0 then ValOrig := ValOrig * -1;
  ValNovo := Geral.DMV(dmkEdValNovo.Text);
  PerMult := Geral.DMV(dmkEdPerMult.Text);
  //
  ValDife := ValOrig - ValNovo;
  ValMult := Int(ValOrig * PerMult ) / 100;
  if ValMult > ValDife then ValMult := ValDife;
  ValJuro := ValDife - ValMult;
  //
  dmkEdMultaVal.Text := Geral.FFT(ValMult, 2, siNegativo);
  dmkEdMoraVal.Text  := Geral.FFT(ValJuro, 2, siNegativo);
end;

procedure TFmLctEdit.CkIncremTxtClick(Sender: TObject);
begin
  GBIncremTxt.Visible := CkIncremTxt.Checked;
  CalculaParcelas;
end;

procedure TFmLctEdit.QrDeptosAfterScroll(DataSet: TDataSet);
begin
  case FIDFinalidade of
    idflProprios: // 1: // Pr�prio
    begin
      QrForneceI.Close;
      QrForneceI.SQL.Clear;
      QrForneceI.SQL.Add('SELECT ent.Codigo,');
      QrForneceI.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENTIDADE');
      QrForneceI.SQL.Add('FROM intentosoc soc');
      QrForneceI.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=soc.Socio');
      QrForneceI.SQL.Add('WHERE soc.Codigo=:P0');
      QrForneceI.SQL.Add('ORDER BY NOMEENTIDADE');
      QrForneceI.SQL.Add('');
      QrForneceI.Params[0].AsInteger := QrDeptosCODI_1.Value;
      QrForneceI.Open;
      if not QrForneceI.Locate('Codigo', Geral.IMV(dmkEdCBForneceI.Text), []) then
      begin
        dmkEdCBForneceI.Text := '';
        dmkCBForneceI.KeyValue := Null;
      end;
    end;
    idflCondominios: // 2: // Condom�nios
    begin
      //Nada
    end;
    idflCursosCiclicos: // 3: // Cursos c�clicos
    begin
      //Nada
    end;
  end;
end;

procedure TFmLctEdit.dmkEdCBGeneroChange(Sender: TObject);
begin
  VerificaEdits();
  if (LaTipo.Caption = CO_INCLUSAO) and (CkDuplicando.Checked = False) then
    if dmkEdCBGenero.ValueVariant <> 0 then
      dmkEdDescricao.Text := QrContasNome.Value;
end;

procedure TFmLctEdit.dmkEdCBGeneroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F7 then
    MostraPnMaskPesq('Conta (Plano de contas)');
end;

procedure TFmLctEdit.dmkCBGeneroKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F7 then
    MostraPnMaskPesq('Conta (Plano de contas)');
end;

procedure TFmLctEdit.dmkDBLookupComboBox1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  SetaAccount(Key, Shift);
end;

procedure TFmLctEdit.dmkEdMesExit(Sender: TObject);
begin
  if (LaTipo.Caption = CO_INCLUSAO) and (CkDuplicando.Checked = False)  then
    dmkEdDescricao.Text := QrContasNome2.Value+' '+dmkEdMes.Text;
end;

procedure TFmLctEdit.dmkEdDebExit(Sender: TObject);
begin
  CalculaICMS;
  if CkParcelamento.Checked then CalculaParcelas;
end;

procedure TFmLctEdit.dmkCBGeneroClick(Sender: TObject);
begin
  VerificaEdits();
end;

procedure TFmLctEdit.dmkEdCredExit(Sender: TObject);
begin
  CalculaICMS;
  if CkParcelamento.Checked then CalculaParcelas;
end;

procedure TFmLctEdit.dmkEdDocChange(Sender: TObject);
begin
  if Geral.DMV(dmkEdDoc.Text) = 0 then
  begin
    RGIncremCH.Enabled := False;
    //GBCheque.Visible := False;
  end else begin
    RGIncremCH.Enabled := True;
    //GBCheque.Visible := True;
  end;
end;

procedure TFmLctEdit.dmkEdDocExit(Sender: TObject);
begin
  if CkParcelamento.Checked then CalculaParcelas;
end;

procedure TFmLctEdit.dmkEdDuplicataExit(Sender: TObject);
begin
  if CkParcelamento.Checked then CalculaParcelas;
end;

procedure TFmLctEdit.dmkEdTPDataClick(Sender: TObject);
begin
  dmkEdTPVencto.Date := dmkEdTPData.Date;
end;

procedure TFmLctEdit.dmkEdTPVenctoChange(Sender: TObject);
begin
  if CkParcelamento.Checked then CalculaParcelas;
end;

procedure TFmLctEdit.dmkEdTPVenctoClick(Sender: TObject);
begin
  if CkParcelamento.Checked then CalculaParcelas;
end;

procedure TFmLctEdit.dmkEdTPVenctoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F6 then
    dmkEdTPVencto.Date := dmkEdTPdata.Date;
end;

procedure TFmLctEdit.ConfiguracoesIniciais();
begin
  QrCliInt.Close;
  QrCliInt.Open;
  //
  MeConfig.Lines.Clear;
  QrDeptos.Close;
  QrDeptos.SQL.Clear;
  FIDFinalidade := TIDFinalidadeLct(Geral.IMV(Copy(LaFinalidade.Caption, 1, 3)));
  case FIDFinalidade of
    idflProprios: //1: // Pr�prios
    begin
      LaCliInt.Caption := 'Empresa (Filial):';
      if Uppercase(Application.Title) = 'SYNKER' then
      begin
        //
        QrDeptos.SQL.Add('SELECT Codigo CODI_1, Nome NOME_1, FLOOR(0) CODI_2');
        QrDeptos.SQL.Add('FROM intentocad');
        QrDeptos.SQL.Add('WHERE Ativo=1');
        QrDeptos.SQL.Add('ORDER BY NOME_1');
        //
        {
        QrCliInt.SQL.Clear;
        QrCliInt.SQL.Clear;
        QrCliInt.SQL.Add('SELECT Codigo, ');
        QrCliInt.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE');
        QrCliInt.SQL.Add('FROM entidades');
        QrCliInt.SQL.Add('WHERE Codigo=' + IntToStr(Dmod.QrControleDono.Value));
        QrCliInt.Open;
        }
        //////
        //PnDepto.Visible   := True;
        //PnForneceI.Visible := True;
      end;
    end;
    idflCondominios: //2: // Condom�nios
    begin
      LaCliInt.Caption := 'Condom�nio (Cliente interno):';
      //
      QrDeptos.SQL.Add('SELECT imv.Conta CODI_1, imv.Unidade NOME_1,');
      QrDeptos.SQL.Add('FLOOR(imv.Propriet) CODI_2');
      QrDeptos.SQL.Add('FROM condimov imv');
      QrDeptos.SQL.Add('LEFT JOIN cond con ON con.Codigo = imv.Codigo');
      //QrDeptos.SQL.Add('WHERE Codigo=:P0');
      QrDeptos.SQL.Add('WHERE con.Cliente=:P0');
      QrDeptos.SQL.Add('ORDER BY NOME_1');
      QrDeptos.Params[0].AsInteger := dmkEdOneCliInt.ValueVariant;//FmCondGer.QrCondCodigo.Value;
      //
      if Trim(VAR_CLIENTEI) <> '' then
      begin
        {
        QrCliInt.SQL.Clear;
        QrCliInt.SQL.Add('SELECT Codigo, ');
        QrCliInt.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE');
        QrCliInt.SQL.Add('FROM entidades');
        QrCliInt.SQL.Add('WHERE Cliente'+VAR_CLIENTEI+'="V"');
        QrCliInt.SQL.Add('ORDER BY NOMEENTIDADE');
        QrCliInt.Open;
        }
        //////
        LaCliInt.Visible := True;
        dmkEdCBCliInt.Visible := True;
        dmkCBCliInt.Visible := True;
      end;
      PnDepto.Visible   := True;
      PnForneceI.Visible := True;
    end;
    idflCursosCiclicos: // 3: // Cursos c�clicos
    begin
      {
      QrCliInt.SQL.Clear;
      QrCliInt.SQL.Add('SELECT Codigo, ');
      QrCliInt.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE');
      QrCliInt.SQL.Add('FROM entidades');
      QrCliInt.SQL.Add('WHERE CliInt>0 OR Codigo=-1');
      QrCliInt.SQL.Add('ORDER BY NOMEENTIDADE');
      QrCliInt.Open;
      }
      LaCliInt.Caption        := 'Empresa (Filial):';
      LaDepto.Caption         := '';
      LaAccount.Caption       := 'Respons�vel interno pelo movimento:';
      dmkEdCBForneceI.Visible := False;
      dmkCBForneceI.Visible   := False;
      //
      QrAccounts.SQL.Clear;
      QrAccounts.SQL.Add('SELECT Codigo, ');
      QrAccounts.SQL.Add('IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE');
      QrAccounts.SQL.Add('FROM entidades');
      QrAccounts.SQL.Add('WHERE Fornece2="V" OR Fornece3="V"');
      QrAccounts.Open;
      //
      PnAccount.Visible           := True;
      if LaTipo.Caption = CO_INCLUSAO then
      begin
        dmkEdCBAccount.ValueVariant := dmkEdOneAccount.ValueVariant;
        dmkCBAccount.KeyValue       := dmkEdOneAccount.ValueVariant;
      end;
    end;
    else Geral.MensagemBox('Nenhuma finalidade foi definida para a ' +
    'janela de inclus�o / altera��o de lan�amentos!', 'Aviso',
    MB_OK+MB_ICONWARNING);
  end;
  if QrDeptos.SQL.Text <> '' then QrDeptos.Open;
end;

procedure TFmLctEdit.dmkCBForneceKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_F5 then ReopenFornecedores(0) else
  if Key = VK_F6 then ReopenFornecedores(1);
end;

procedure TFmLctEdit.dmkEdCBDeptoChange(Sender: TObject);
begin
  if not FCriandoForm then
  begin
    //if FIDFinalidade = 2 then
    if FIDFinalidade = idflCondominios then
    begin
      if dmkEdCBDepto.ValueVariant = 0 then
      begin
        dmkEdCBForneceI.Text := '0';
        dmkCBForneceI.KeyValue := Null;
      end else begin
        dmkEdCBForneceI.Text := FormatFloat('0', QrDeptosCODI_2.Value);
        dmkCBForneceI.KeyValue := Int(QrDeptosCODI_2.Value / 1);
      end;
    end;
  end;
end;

procedure TFmLctEdit.dmkCBDeptoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_DELETE then
  begin
    dmkEdCBDepto.Text := '';
    dmkCBDepto.KeyValue := Null;
  end;
end;

procedure TFmLctEdit.dmkEdDescricaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  Mes: String;
begin
  if key in ([VK_F4, VK_F5, VK_F6]) then
  begin
    if dmkEdMes.Enabled then Mes := dmkEdMes.Text else Mes := '';
    if key=VK_F4 then dmkEdDescricao.Text := QrContasNome.Value+' '+Mes;
    if key=VK_F5 then dmkEdDescricao.Text := QrContasNome2.Value+' '+Mes;
    if key=VK_F6 then dmkEdDescricao.Text := QrContasNome3.Value+' '+Mes;
    if dmkEdDescricao.Enabled then dmkEdDescricao.SetFocus;
    dmkEdDescricao.SelStart := Length(dmkEdDescricao.Text);
    dmkEdDescricao.SelLength := 0;
  end else if key = VK_F8 then
  begin
    if dmkCBFornece.Enabled then
      dmkEdDescricao.Text := dmkEdDescricao.Text + ' - ' + dmkCBFornece.Text
    else if dmkCBCliente.Enabled then
      dmkEdDescricao.Text := dmkEdDescricao.Text + ' - ' + dmkCBCliente.Text;
  end;
end;

procedure TFmLctEdit.dmkCBAccountKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  SetaAccount(Key, Shift);
end;

procedure TFmLctEdit.dmkEdICMS_PExit(Sender: TObject);
begin
  CalculaICMS;
end;

procedure TFmLctEdit.dmkEdICMS_PKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if MLAGeral.EhNumeroOuSinalOuSeparador(Key) then FICMS := 0;
end;

procedure TFmLctEdit.dmkEdICMS_VExit(Sender: TObject);
begin
  CalculaICMS;
end;

procedure TFmLctEdit.dmkEdICMS_VKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if MLAGeral.EhNumeroOuSinalOuSeparador(Key) then FICMS := 1;
end;

procedure TFmLctEdit.dmkEdCarteiraChange(Sender: TObject);
var
  UsaTalao: Boolean;
begin
  UsaTalao := QrCarteirasUsaTalao.Value = 0;
  dmkEdSerieCH.Enabled := UsaTalao;
  dmkEdDoc.Enabled     := UsaTalao;
  LaDoc.Enabled        := UsaTalao;
end;

procedure TFmLctEdit.dmkEdCBAccountKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  SetaAccount(Key, Shift);
end;

procedure TFmLctEdit.dmkEdCBCliIntChange(Sender: TObject);
begin
  EventosCadReopen(dmkEdCBCliInt.ValueVariant);
end;

procedure TFmLctEdit.dmkEdParcelasExit(Sender: TObject);
begin
  CalculaParcelas;
end;

procedure TFmLctEdit.dmkEdSerieCHExit(Sender: TObject);
begin
  if CkParcelamento.Checked then CalculaParcelas;
end;

procedure TFmLctEdit.IncrementaExecucoes();
begin
  dmkEdExecs.ValueVariant := dmkEdExecs.ValueVariant + 1;
end;

end.



