unit ConciliaXLSLoad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, DBCtrls, Grids,
  ComObj, ComCtrls, Variants, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UnDmkEnums;

type
  TFmConciliaXLSLoad = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrBancos: TmySQLQuery;
    DsBancos: TDataSource;
    QrBancosCodigo: TIntegerField;
    QrBancosNome: TWideStringField;
    QrBancosXlsLinha: TIntegerField;
    QrBancosXlsData: TWideStringField;
    QrBancosXlsHist: TWideStringField;
    QrBancosXlsDocu: TWideStringField;
    QrBancosXlsHiDo: TWideStringField;
    QrBancosXlsCred: TWideStringField;
    QrBancosXlsDebi: TWideStringField;
    QrBancosXlsCrDb: TWideStringField;
    QrBancosXlsDouC: TWideStringField;
    QrBancosXlsTCDB: TSmallintField;
    QrBancosXlsComp: TWideStringField;
    QrBancosXlsCPMF: TWideStringField;
    QrBancosXlsSldo: TWideStringField;
    Panel3: TPanel;
    Label1: TLabel;
    EdArquivo: TEdit;
    SpeedButton1: TSpeedButton;
    OpenDialog1: TOpenDialog;
    Label2: TLabel;
    EdBanco: TdmkEditCB;
    CBBanco: TdmkDBLookupComboBox;
    Panel1: TPanel;
    PB1: TProgressBar;
    LaAviso: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Grade2: TStringGrid;
    Grade1: TStringGrid;
    BtSalva: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure Grade2DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure BtSalvaClick(Sender: TObject);
  private
    { Private declarations }
    function Xls_To_StringGrid(AGrid: TStringGrid; AXLSFile: string): Boolean;
  public
    { Public declarations }
  end;

  var
  FmConciliaXLSLoad: TFmConciliaXLSLoad;

implementation

uses UnMyObjects, Module, UCreate, Concilia, ModuleGeral;

const
  FTits = 11;
  FTitulos: array[0..FTits-1] of String = (
{00}                            'Data',
{01}                            'C',
{02}                            '*',
{03}                            'Hist�rico',
{04}                            'Documento',
{05}                            'Lan�amento',
{06}                            'Cr�dito',
{07}                            'D�bito',
{08}                            '$ D/C',
{09}                            '<',
{10}                            'Saldo');

{$R *.DFM}

procedure TFmConciliaXLSLoad.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmConciliaXLSLoad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmConciliaXLSLoad.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmConciliaXLSLoad.FormCreate(Sender: TObject);
var
  Banco: Integer;
begin
  QrBancos.Open;
  //
  Banco := Geral.ReadAppKeyLM('Concilia\Banco', Application.Title, ktInteger, 0);
  //
  if Banco > 0 then
  begin
    EdBanco.Text := IntToStr(Banco);
    CBBanco.KeyValue := Banco;
  end;
end;

procedure TFmConciliaXLSLoad.SpeedButton1Click(Sender: TObject);
begin
  if OpenDialog1.Execute then
    EdArquivo.Text := OpenDialog1.FileName;
end;

procedure TFmConciliaXLSLoad.BtOKClick(Sender: TObject);
var
  Banco: Integer;
  //Arquivo: String;
  Erros: String;
begin
  PB1.Position := 0;
  LaAviso.Caption := 'Verificando configura��es do banco ...';
  Banco := Geral.IMV(EdBanco.Text);
  if Banco = 0 then
  begin
    Application.MessageBox('Informe o banco!', 'Erro', MB_OK+MB_ICONERROR);
    EdBanco.SetFocus;
    LaAviso.Caption := 'Informe o banco!';
    Exit;
  end;
  Erros := '';
  if QrBancosXlsLinha.Value < 1 then
    Erros := Erros + '-> Linha: Linha inicial inv�lida' +Chr(13) +Chr(10);
  if QrBancosXlsData.Value = '' then
    Erros := Erros + '-> Data: Data do movimento' +Chr(13) +Chr(10);
  if (QrBancosXlsHist.Value = '') and
     (QrBancosXlsDocu.Value = '') and
     (QrBancosXlsHiDo.Value = '') then
  Erros := Erros + '-> Link: (Hist�rico ou Documento ou ambos)' +Chr(13) +Chr(10);
  if (QrBancosXlsCred.Value = '') and
     (QrBancosXlsDebi.Value = '') and
     (QrBancosXlsCrDb.Value = '') then
  Erros := Erros + '-> Valor: (Cr�dito ou D�bito ou ambos)' +Chr(13) +Chr(10);
  //
  if Erros <> '' then
  begin
     Erros := 'O banco ' + FormatFloat('000', QrBancosCodigo.Value) + ' n�o ' +
     'est� configurado corretamente para importa��o de extrato banc�rio em ' +
     'formato excel. Abaixo est� listado o que falta configurar para uma ' +
     'importa��o com o m�nimo de dados necess�rios para haver consolida��o!' +
     Chr(13) + Chr(10) + Erros +Chr(13) + Chr(10) + Chr(13) + Chr(10) +
     'Esta configura��o deve ser feita no cadastro do banco.';
     Application.MessageBox(PChar(Erros), 'Erro', MB_OK+MB_ICONERROR);
     LaAviso.Caption := 'Configure o banco em seu cadastro!';
     Exit;
  end;
  BtSalva.Enabled := Xls_To_StringGrid(Grade1, EdArquivo.Text);
  LaAviso.Caption := '...';
  //
  Geral.WriteAppKeyLM2('Concilia\Banco', Application.Title, Geral.IMV(EdBanco.Text), ktInteger);
end;

function TFmConciliaXLSLoad.Xls_To_StringGrid(AGrid: TStringGrid; AXLSFile: string): Boolean;
  procedure AtulizaTitulo(var n: Integer; const Titulo: String);
  begin
    inc(n, 1);
    Grade2.ColCount := n + 1;
    Grade2.Cells[n, 0] := Titulo;
  end;
  procedure AtulizaColuna(var n: Integer; const Lin2, Lin1, Coluna: Integer;
    const CharWid: Double; var ColWid: array of Integer);
  var
    i, Item, Larg: Integer;
    s: String;
    Neg: Boolean;
  begin
    if Coluna < 1 then Exit;
    inc(n, 1);
    s := Grade1.Cells[Coluna, Lin1];
    Item := -1;
    for i := 0 to FTits-1 do
      if FTitulos[i] = Grade2.Cells[n, 0] then Item := i;
    if Item in ([6,7,8,10]) then
    begin
      if Length(s) > 0 then
      begin
        Neg := (Uppercase(s[1]) = 'D');
        if not Neg then Neg := (Uppercase(s[Length(s)]) = 'D');
      end else Neg := False;
      s := MLAGeral.SoNumeroValor_TT(s);
      if Length(s) > 0 then
      begin
        if (s[Length(s)] = '-') then
          s := '-' + Copy(s, 1, Length(s)-1)
        else if Neg then s := '-' + s;
      end;
      s := Geral.TFT(s, 2, siNegativo);
    end else if Item = 0 then
    begin
      s := MLAGeral.SoNumeroEExtra_TT(s, '/', True);
      s := Geral.FDT(Geral.ValidaDataSimples(Trim(s), True), 2);
    end;
    if s = '0,00' then s := '';
    Grade2.Cells[n, Lin2] := s;
    Larg := Length(s);
    if Larg > 2 then
    begin
      Larg := Round(Larg * CharWid);
      if Larg > ColWid[n] then ColWid[n] := Larg;
    end;
  end;
  function EliminaLinhaVazia: Boolean;
  var
    i: Integer;
  begin
    if Grade2.RowCount = 2 then
    begin
      Result := False;
      Exit;
    end;
    Result := True;
    for i := 1 to Grade2.ColCount -1 do
      if Trim(Grade2.Cells[i, Grade2.RowCount -1]) <> '' then Result := False
  end;
const
  xlCellTypeLastCell = $0000000B;
  CharWid = 7.2;
var
  XLApp, Sheet: OLEVariant;
  RangeMatrix: Variant;
  x, y, k, r, Larg, m, n: Integer;
  XLSData, XLSComp, XLSCPMF, XLSHist, XLSDocu, XLSHiDO, XLSCred, XLSDebi,
  XLSDouC, XLSCrDb, XLSSldo: Integer;
  ColWid: array[0..255] of Integer;
begin
  Screen.Cursor := crHourGlass;
  Result := False;
  XLApp := CreateOleObject('Excel.Application');
  try
    XLApp.Visible := False;
    XLApp.Workbooks.Open(AXLSFile);
    Sheet := XLApp.Workbooks[ExtractFileName(AXLSFile)].WorkSheets[1];
    Sheet.Cells.SpecialCells(xlCellTypeLastCell, EmptyParam).Activate;
    x := XLApp.ActiveCell.Row;
    y := XLApp.ActiveCell.Column;
    AGrid.RowCount := x;
    AGrid.ColCount := y + 1;
    PB1.Max := x * (y + 1);
    LaAviso.Caption := 'Lendo arquivo excel ...';
    Update;
    RangeMatrix := XLApp.Range['A1', XLApp.Cells.Item[X, Y]].Value;
    k := 1; // 2?
    m := Round(CharWid * 3);
    for n := 0 to 255 do ColWid[n] := m;
    repeat
      Update;
      Application.ProcessMessages;
      AGrid.Cells[0, (k - 1)] := FormatFloat('000', (k - 1));
      for r := 1 to y do
      begin
        PB1.Position := PB1.Position + 1;
        Update;
        Application.ProcessMessages;
        //
        AGrid.Cells[r, (k - 1)] := RangeMatrix[K, R];
        Larg := Round((Length(RangeMatrix[K, R]) + 1) * CharWid);
        if Larg > ColWid[r] then
          ColWid[r] := Larg;
      end;
      Inc(k, 1);
      AGrid.RowCount := k + 2;
    until k > x;
    RangeMatrix := Unassigned;
    (*Application.MessageBox(PChar('Os dados foram importados com sucesso do '+
      'arquivo: '+Chr(13)+Chr(10)+EdArquivo.Text), 'Informa��o',
      MB_OK+MB_ICONINFORMATION);*)
  finally
    if not VarIsEmpty(XLApp) then
    begin
      XLApp.Quit;
      XLAPP := Unassigned;
      Sheet := Unassigned;
      Result := True;
    end;
    Screen.Cursor := crDefault;
  end;
  for n := 0 to AGrid.ColCount -1 do
    AGrid.ColWidths[n] := ColWid[n];
  //
  //  Le dados solicitados da Grade1 e grava na Grade 2
  //
  m := Round(CharWid * 3);
  for n := 0 to 255 do ColWid[n] := m;
  XLSData := MLAGeral.XLSColCharToColNum(QrBancosXLSData.Value);
  XLSComp := MLAGeral.XLSColCharToColNum(QrBancosXlsComp.Value);
  XLSCPMF := MLAGeral.XLSColCharToColNum(QrBancosXlsCPMF.Value);
  XLSHist := MLAGeral.XLSColCharToColNum(QrBancosXlsHist.Value);
  XLSDocu := MLAGeral.XLSColCharToColNum(QrBancosXlsDocu.Value);
  XLSHiDO := MLAGeral.XLSColCharToColNum(QrBancosXlsHiDo.Value);
  XLSCred := MLAGeral.XLSColCharToColNum(QrBancosXlsCred.Value);
  XLSDebi := MLAGeral.XLSColCharToColNum(QrBancosXlsDebi.Value);
  XLSCrDb := MLAGeral.XLSColCharToColNum(QrBancosXlsCrDb.Value);
  XLSDouC := MLAGeral.XLSColCharToColNum(QrBancosXlsDouC.Value);
  XLSSldo := MLAGeral.XLSColCharToColNum(QrBancosXlsSldo.Value);
  k := Grade1.RowCount - QrBancosXlsLinha.Value + 1;
  //
  n := 0;
  Grade2.ColCount := 20; // 12
  if XLSData > 0 then AtulizaTitulo(n, FTitulos[00]);
  if XLSComp > 0 then AtulizaTitulo(n, FTitulos[01]);
  if XLSCPMF > 0 then AtulizaTitulo(n, FTitulos[02]);
  if XLSHist > 0 then AtulizaTitulo(n, FTitulos[03]);
  if XLSDocu > 0 then AtulizaTitulo(n, FTitulos[04]);
  if XLSHiDo > 0 then AtulizaTitulo(n, FTitulos[05]);
  if XLSCred > 0 then AtulizaTitulo(n, FTitulos[06]);
  if XLSDebi > 0 then AtulizaTitulo(n, FTitulos[07]);
  if XLSCrDb > 0 then AtulizaTitulo(n, FTitulos[08]);
  if XLSDouC > 0 then AtulizaTitulo(n, FTitulos[09]);
  if XLSSldo > 0 then AtulizaTitulo(n, FTitulos[10]);
  //
  PB1.Position := 0;
  PB1.Max := Grade1.RowCount - QrBancosXlsLinha.Value -1;
  Update;
  Application.ProcessMessages;
  //
  if k < 3 then Grade2.RowCount := 2 else
  begin
    Grade2.RowCount := k;
    y := 0;
    for m := QrBancosXlsLinha.Value -1 to Grade1.RowCount - 1 do
    begin
      PB1.Position := PB1.Position + 1;
      Update;
      Application.ProcessMessages;
      //
      n := 0;
      inc(y, 1);
      AtulizaColuna(n, y, m, XLSData, CharWid, ColWid);
      AtulizaColuna(n, y, m, XLSComp, CharWid, ColWid);
      AtulizaColuna(n, y, m, XLSCPMF, CharWid, ColWid);
      AtulizaColuna(n, y, m, XLSHist, CharWid, ColWid);
      AtulizaColuna(n, y, m, XLSDocu, CharWid, ColWid);
      AtulizaColuna(n, y, m, XLSHiDo, CharWid, ColWid);
      AtulizaColuna(n, y, m, XLSCred, CharWid, ColWid);
      AtulizaColuna(n, y, m, XLSDebi, CharWid, ColWid);
      AtulizaColuna(n, y, m, XLSCrDb, CharWid, ColWid);
      AtulizaColuna(n, y, m, XLSDouC, CharWid, ColWid);
      AtulizaColuna(n, y, m, XLSSldo, CharWid, ColWid);
    end;
  end;
  for n := 0 to Grade2.ColCount -1 do
    Grade2.ColWidths[n] := ColWid[n];
  //
  // Limpar Linhas vazias
  //
  LaAviso.Caption := 'Eliminando linhas vazias...';
  while EliminaLinhaVazia do
    Grade2.RowCount := Grade2.RowCount - 1;
end;

procedure TFmConciliaXLSLoad.Grade2DrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  i, Item, OldAlign: Integer;
begin
  Item := -1;
  for i := 0 to FTits-1 do if FTitulos[i] = Grade2.Cells[ACol, 0] then Item := i;
  if ARow = 0 then
  begin
    //
  end else
  if ACol = 0 then begin
    if ARow <> 0 then
    begin
      OldAlign := SetTextAlign(Grade2.Canvas.Handle, TA_CENTER);
      Grade2.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Geral.TFD(Grade2.Cells[Acol, ARow], 3, siPositivo));
      SetTextAlign(Grade2.Canvas.Handle, OldAlign);
    end;
  end else
  if Item = 0 then
  begin
    OldAlign := SetTextAlign(Grade2.Canvas.Handle, TA_CENTER);
    Grade2.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
      Grade2.Cells[Acol, ARow]);
    SetTextAlign(Grade2.Canvas.Handle, OldAlign);
  end else
  if Item in ([6,7,8,10]) then
  begin
    OldAlign := SetTextAlign(Grade2.Canvas.Handle, TA_RIGHT);
    Grade2.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
     Geral.TFT_NULL(Grade2.Cells[Acol, ARow], 2, siNegativo));
    SetTextAlign(Grade2.Canvas.Handle, OldAlign);
  end else
  begin
    OldAlign := SetTextAlign(Grade2.Canvas.Handle, TA_LEFT);
    Grade2.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
      Grade2.Cells[Acol, ARow]);
    SetTextAlign(Grade2.Canvas.Handle, OldAlign);
  end;
end;

procedure TFmConciliaXLSLoad.BtSalvaClick(Sender: TObject);
  function ObtemItem(Titulo: String): Integer;
  var
    j: Integer;
  begin
    Result := -1;
    for j := 0 to FTits-1 do
    begin
      if (FTitulos[j] = Titulo) then
        Result := j;
    end;
  end;
var
  col, i, Item, Clien, Forne, Perio: Integer;
  DataM: TDateTime;
  Valor, Saldo, Credi, Debit, CrDb: Double;
  Histo, Docum, CPMF, Trans, CompF, Lanct, CouD: String;
begin
  Screen.Cursor := crHourGlass;
  try
    //Ucriar.AtualizaTabelaControleLocal('CartConcilia', '', '', 0, '', 0);
    PB1.Position := 0;
    PB1.Max := Grade2.RowCount - 1;
    FmConcilia.FConcilia := UCriar.RecriaTempTable(FmConcilia.FConcilia, DmodG.QrUpdPID1, False);
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FmConcilia.FConcilia + ' SET ');
    DmodG.QrUpdPID1.SQL.Add('Linha=:P0, DataM=:P1, Texto=:P2, Docum=:P3, ');
    DmodG.QrUpdPID1.SQL.Add('Valor=:P4, Saldo=:P5, Clien=:P6, Forne=:P7, ');
    DmodG.QrUpdPID1.SQL.Add('Perio=:P8');
    //
    for i := 1 to Grade2.RowCount -1 do
    begin
      PB1.Position := PB1.Position + 1;

      DataM := 0;
      Valor := 0;
      Credi := 0;
      Debit := 0;
      CrDb  := 0;
      Saldo := 0;
      Histo := '';
      Docum := '';
      CPMF  := '';
      CompF := '';
      Trans := '';

      //

      for col := 1 to Grade2.ColCount -1 do
      begin
        //Item := -1;
        item := ObtemItem(Grade2.Cells[col, 0]);
        case Item of
           0: DataM := Geral.ValidaDataSimples(Grade2.Cells[col, i], True);
           1: CompF := Grade2.Cells[col, i];
           2: CPMF  := Grade2.Cells[col, i];
           3: Histo := Grade2.Cells[col, i];
           4: Docum := Grade2.Cells[col, i];
           5: Lanct := Grade2.Cells[col, i];
           6: Credi := Geral.DMV(Grade2.Cells[col, i]);
           7: Debit := Geral.DMV(Grade2.Cells[col, i]);
           8: CrDb  := Geral.DMV(Grade2.Cells[col, i]);
           9: CouD  := Grade2.Cells[col, i];
          10: Saldo := Geral.DMV(Grade2.Cells[col, i]);
        end;
      end;
      if DataM > 0 then
      begin
        Valor := Valor + Credi - Debit + CrDb;
        Histo := Histo + Lanct;
        if (Valor <> 0) or (Saldo <> 0) then
        begin
          Clien := 0;
          Forne := 0;
          Perio := 0;
          DmodG.QrUpdPID1.Params[00].AsInteger := i;
          DmodG.QrUpdPID1.Params[01].AsString  := Geral.FDT(DataM, 1);
          DmodG.QrUpdPID1.Params[02].AsString  := Histo;
          DmodG.QrUpdPID1.Params[03].AsString  := Docum;
          DmodG.QrUpdPID1.Params[04].AsFloat   := Valor;
          DmodG.QrUpdPID1.Params[05].AsFloat   := Saldo;
          DmodG.QrUpdPID1.Params[06].AsInteger := Clien;
          DmodG.QrUpdPID1.Params[07].AsInteger := Forne;
          DmodG.QrUpdPID1.Params[08].AsInteger := Perio;
          //
          DmodG.QrUpdPID1.ExecSQL;
        end;
      end;
    end;
    Application.MessageBox('Os dados foram salvos com sucesso!', 'Informa��o',
      MB_OK+MB_ICONINFORMATION);
    PB1.Position := 0;
  finally
    Screen.Cursor := crDefault;
  end;
  Close;
end;

end.
