object FmConciliaOFX: TFmConciliaOFX
  Left = 339
  Top = 185
  Caption = 'FIN-CONCI-004 :: Abrir Arquivos Money / Quicken'
  ClientHeight = 416
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 368
    Width = 792
    Height = 48
    Align = alBottom
    TabOrder = 0
    object Panel2: TPanel
      Left = 680
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Caption = 'Abrir Arquivos Money / Quicken'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 790
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 73
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Label1: TLabel
      Left = 8
      Top = 4
      Width = 39
      Height = 13
      Caption = 'Arquivo:'
    end
    object LaAviso: TLabel
      Left = 420
      Top = 48
      Width = 9
      Height = 13
      Caption = '...'
    end
    object EdArquivo: TEdit
      Left = 8
      Top = 20
      Width = 681
      Height = 21
      TabOrder = 0
    end
    object PBInfo: TProgressBar
      Left = 8
      Top = 48
      Width = 405
      Height = 17
      TabOrder = 1
    end
    object BtOK: TBitBtn
      Tag = 14
      Left = 692
      Top = 12
      Width = 90
      Height = 40
      Caption = '&Abrir'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtOKClick
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 121
    Width = 792
    Height = 247
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Memo1: TMemo
      Left = 0
      Top = 17
      Width = 792
      Height = 230
      Align = alClient
      ReadOnly = True
      TabOrder = 0
    end
    object StaticText1: TStaticText
      Left = 0
      Top = 0
      Width = 38
      Height = 17
      Align = alTop
      Alignment = taCenter
      Caption = 'Avisos:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
  end
  object QrBancos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM bancos'
      'ORDER BY Nome')
    Left = 12
    Top = 12
    object QrBancosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrBancosXlsLinha: TIntegerField
      FieldName = 'XlsLinha'
    end
    object QrBancosXlsData: TWideStringField
      FieldName = 'XlsData'
      Size = 2
    end
    object QrBancosXlsHist: TWideStringField
      FieldName = 'XlsHist'
      Size = 2
    end
    object QrBancosXlsDocu: TWideStringField
      FieldName = 'XlsDocu'
      Size = 2
    end
    object QrBancosXlsHiDo: TWideStringField
      FieldName = 'XlsHiDo'
      Size = 2
    end
    object QrBancosXlsCred: TWideStringField
      FieldName = 'XlsCred'
      Size = 2
    end
    object QrBancosXlsDebi: TWideStringField
      FieldName = 'XlsDebi'
      Size = 2
    end
    object QrBancosXlsCrDb: TWideStringField
      FieldName = 'XlsCrDb'
      Size = 2
    end
    object QrBancosXlsDouC: TWideStringField
      FieldName = 'XlsDouC'
      Size = 2
    end
    object QrBancosXlsTCDB: TSmallintField
      FieldName = 'XlsTCDB'
    end
    object QrBancosXlsComp: TWideStringField
      FieldName = 'XlsComp'
      Size = 2
    end
    object QrBancosXlsCPMF: TWideStringField
      FieldName = 'XlsCPMF'
      Size = 2
    end
    object QrBancosXlsSldo: TWideStringField
      FieldName = 'XlsSldo'
      Size = 2
    end
  end
  object DsBancos: TDataSource
    DataSet = QrBancos
    Left = 40
    Top = 12
  end
end
