unit ConciliaOFX;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, DBCtrls, Grids,
  ComObj, ComCtrls, dmkGeral;

type
  TFmConciliaOFX = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrBancos: TmySQLQuery;
    DsBancos: TDataSource;
    QrBancosCodigo: TIntegerField;
    QrBancosNome: TWideStringField;
    QrBancosXlsLinha: TIntegerField;
    QrBancosXlsData: TWideStringField;
    QrBancosXlsHist: TWideStringField;
    QrBancosXlsDocu: TWideStringField;
    QrBancosXlsHiDo: TWideStringField;
    QrBancosXlsCred: TWideStringField;
    QrBancosXlsDebi: TWideStringField;
    QrBancosXlsCrDb: TWideStringField;
    QrBancosXlsDouC: TWideStringField;
    QrBancosXlsTCDB: TSmallintField;
    QrBancosXlsComp: TWideStringField;
    QrBancosXlsCPMF: TWideStringField;
    QrBancosXlsSldo: TWideStringField;
    Panel3: TPanel;
    Label1: TLabel;
    EdArquivo: TEdit;
    Panel1: TPanel;
    PBInfo: TProgressBar;
    LaAviso: TLabel;
    Memo1: TMemo;
    StaticText1: TStaticText;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    //procedure BtSalvaClick(Sender: TObject);
  private
    { Private declarations }
    FArquivo: String;
    procedure CarregaExtrato;
    procedure Le_OFC_OFX;
    procedure Le_OFC100;
    procedure Le_QIF(Tipo: Integer);
    procedure CriaLinha;
    function LeSoNumero(Linha : string) : string;
    function LeLinha(Linha : string; Tipo : integer) : string;
    function LeLinha2(Linha : string; Tipo : integer) : string;
    function LeValor(Txt: String) : Double;
    function LeOperData(Texto : String) : Integer;
    function LeData1(Texto : String) : String;
    function LeData2(Texto : String; Tipo: Integer) : String;
    function LeData3(Texto : String): TDateTime;
    procedure ResetaDadosIns;
  public
    { Public declarations }
    FMostra, FVarre: Boolean;
  end;

  var
  FmConciliaOFX: TFmConciliaOFX;
  ArqOF: TStringList;
  Imp_Data, Imp_Memo, Imp_IDConta, Imp_Doc: String;
  Imp_Valor, Imp_Saldo: Double;
  Imp_Linha: Integer;
  Imp_Verif, AbortaBaixa: Boolean;

implementation

uses UnMyObjects, Module, UCreate, UnInternalConsts, UnMsgInt, Concilia,
  ModuleGeral;

const
  FTits = 11;
  FTitulos: array[0..FTits-1] of String = (
{00}                            'Data',
{01}                            'C',
{02}                            '*',
{03}                            'Hist�rico',
{04}                            'Documento',
{05}                            'Lan�amento',
{06}                            'Cr�dito',
{07}                            'D�bito',
{08}                            '$ D/C',
{09}                            '<',
{10}                            'Saldo');

{$R *.DFM}

procedure TFmConciliaOFX.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmConciliaOFX.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmConciliaOFX.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmConciliaOFX.FormCreate(Sender: TObject);
{var
  Banco: Integer;}
begin
  {QrBancos.Open;
  Banco := Geral.ReadAppKeyLM('Concilia\Banco', Application.Title, ktInteger, 0);
  if Banco > 0 then
  begin
    EdBanco.Text := IntToStr(Banco);
    CBBanco.KeyValue := Banco;
  end;}
end;

procedure TFmConciliaOFX.BtOKClick(Sender: TObject);
var
  F: TextFile;
  S, S1, Titulo, IniDir, Filtro: string;
  Cursor : TCursor;
  i: Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  ResetaDadosIns;
  //Ucriar.AtualizaTabelaControleLocal('CartConcilia', '', '', 0, '', 0);
  PBInfo.Position := 0;
  //PBInfo.Max := Grade2.RowCount - 1;
  FmConcilia.FConcilia := UCriar.RecriaTempTable(FmConcilia.FConcilia, DmodG.QrUpdPID1, False);

  {Imp_IDConta := CO_VAZIO;

  Dmod.QrUpdM.Close;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('Delete FROM importits');
  Dmod.QrUpdM.ExecSQL;

  Dmod.QrUpdM.Close;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('Delete FROM import');
  Dmod.QrUpdM.ExecSQL;

  Dmod.QrUpdM.Close;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('Delete FROM importemi');
  Dmod.QrUpdM.ExecSQL;

  Dmod.QrUpdM.Close;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Sit=0, Linha=0 WHERE Sit=-1');
  Dmod.QrUpdM.ExecSQL;}

//  if OpenDialog1.Execute then
  FArquivo := EdArquivo.Text;
  Titulo  := 'Abrir Arquivo de Concilia��o Banc�ria';
  IniDir  := '';
  Filtro  := 'Arquivos OFC|*.ofc;Arquivos OFX|*.ofx';
  if MyObjects.FileOpenDialog(Self, IniDir, FArquivo, Titulo, Filtro, [], FArquivo) then
  begin
    try
      Imp_Verif := True;
      LaAviso.Caption := 'Carregando arquivo';
      Screen.Cursor := crHourglass;
      ArqOF := TStringList.Create;
      {Dmod.QrUpdM.Close;
      Dmod.QrUpdM.SQL.Clear;
      Dmod.QrUpdM.SQL.Add('INSERT INTO import SET Caminho=:Caminho');
      Dmod.QrUpdM.Params[0].AsString := OpenDialog1.FileName;
      Dmod.QrUpdM.ExecSQL;
      Dmod.QrUpdM.Close;}
      AssignFile(F, FArquivo);
      Reset(F);
      while not Eof(F) do
      begin
        Readln(F, S);
        if S <> '' then
          ArqOF.Add(S);
      end;
      CloseFile(F);
      if ArqOF.Count = 1 then
      begin
        S1 := '';
        for i := 1 to Length(S) do
          if Ord(S[i]) = 10 then S1 := S1 + Char(13) else S1 := S1 + s[i];
        ArqOF.Clear;
        ArqOF.Text := S1;
      end;
      LaAviso.Caption := 'Criando tabela';
      CarregaExtrato;
      {LocalizaLinha;
      QrImportIts.DisableControls;
      VerificaExtrato;
      LaAviso.Caption := CO_VAZIO;
      PBInfo.Position := 0;
      Imp_Verif := False;
      LocalizaLinha;
      QrImportIts.First;
      QrImportIts.EnableControls;
      BtVerificar.Enabled := True;
      if VAR_USUARIO < 1 then BtVerificar.Visible := True;
      DBGExtrato.SetFocus;}
    finally
      Imp_Verif := False;
      Screen.Cursor := Cursor;
    end;
  end;
  FMostra := True;
  FVarre := True;
  Close;
end;

procedure TFmConciliaOFX.CarregaExtrato;
begin
  Screen.Cursor := crHourGlass;
  try
    PBInfo.Position := 0;
    if (LeLinha(ArqOF[0],1) = 'OFC') then
    begin
      VAR_FILEEXTRATO := 1;
      PBInfo.Max := ArqOF.Count;
      Le_OFC_OFX;
    end
    else if ( (LeLinha(ArqOF[10],1) = 'OFX') or
     (LeLinha(ArqOF[09],1) = 'OFX') or
     (Copy(LeLinha(ArqOF[00],1), 1, 3) = 'OFX') ) then
    begin
      VAR_FILEEXTRATO := 2;
      PBInfo.Max := ArqOF.Count;
      Le_OFC_OFX;
    end
    else if ArqOF[0] = '!Account' then   //???? [!Account]
    begin
      VAR_FILEEXTRATO := 0;
      PBInfo.Max := (ArqOF.Count div 5)-1;
      Le_QIF(0);
    end
    else if ArqOF[0] = '!Accoun' then   //???? [!Account]
    begin
      VAR_FILEEXTRATO := 0;
      Le_QIF(0);
    end
    else if ArqOF[0] = '0' then   //???? [!Account]
    begin
      VAR_FILEEXTRATO := 0;
      Le_OFC100;
    end
    else if ArqOF[0] = '' then   //???? [!Account]
    begin
      VAR_FILEEXTRATO := 0;
      Le_QIF(4);
    end
    else
    begin
      ShowMessage(ArqOF[0]);
      ShowMessage(FIN_MSG_IMPOSSOPER+Chr(13)+Chr(10)+FIN_MSG_ARQUIVOINVALIDO);
      Exit;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmConciliaOFX.Le_OFC_OFX();
{ Testado:

  Ita� :
        Money 2000/2001
        Quicken
  Banco do Brasil:
        Testando...
}
var
  Index, Operacao, Count, F: Integer;
  Valor : Double;
  Inicio, Fim, Tipo, Texto: String;
  Dia : TDateTime;
begin
  Dia := 1;
  Operacao := 0;
  Inicio := CO_VAZIO;
  Fim := CO_VAZIO;
  Count := 0;
  for Index := 0 to ArqOF.Count-1 do
  begin
    PBInfo.Position := Index;
    PBInfo.Update;
    Tipo := LeLinha(ArqOF[Index],1);
    Texto := LeLinha(ArqOF[Index],2);
    if Texto <> '' then
    begin
      F := pos('</', Texto);
      if F > 1 then
        Texto := Copy(Texto, 1, F - 1);
    end;
  //////////////////////////////////////////////////////////////////////////////
    if (Tipo = 'CLTID') or (Tipo = 'ACCTID') then
    begin
      if (Texto <> '') then Imp_IDCONTA := Texto;
    end;
  //
    if (Tipo = 'DTSTART') then Inicio := Texto;
  //
    if (Tipo = 'DTEND') then
    begin
      {Fim := Texto;
      Dmod.QrUpdM.Close;
      Dmod.QrUpdM.SQL.Clear;
      Dmod.QrUpdM.SQL.Add('UPDATE import SET Inicio=:Inicio, Fim=:Fim');
      Dmod.QrUpdM.Params[0].AsString := Inicio;
      Dmod.QrUpdM.Params[1].AsString := Fim;
      Dmod.QrUpdM.ExecSQL;
      QrImport.Close;
      QrImport.Open;}
    end;


  //////////////////////////////////////////////////////////////////////////////
     if (Tipo = 'STMTTRN') then
       if (Texto <> '') then
         Memo1.Lines.Add('Item desconhecido: '+ ArqOF[Index]);
  //
     if (Tipo = 'GENTRN') then
       if (Texto <> '') then
         Memo1.Lines.Add('Item desconhecido: '+ ArqOF[Index]);
  //
{     if (Tipo = 'TRNTYPE') then
       if not (Texto in (['1','DEBIT','CREDIT'])) then
         Memo1.Lines.Add('Tipo desconhecido: '+ Texto);
}  //
     if (Tipo = 'DTPOSTED') then
     begin
       Imp_Data := LeData1(Texto);
       if VAR_FILEEXTRATO = 2 then
       begin
         if Dia <> LeData3(Imp_Data) then
         begin
           Dia := Ledata3(Imp_Data);
           Operacao := 1;
         end
         else
           Operacao := Operacao + 1;
       end;
     end;
  //
     if (Tipo = 'TRNAMT') then
     begin
       Valor := LeValor(Texto);
       Imp_Valor := Valor;
       {if Valor > 0 then
         Imp_Valor := Valor
       else
         Imp_Valor := Valor * -1;}
     end;
  //
//     if (Tipo = 'FITID') then
  //     FitID := StrToInt(Texto);
  //
     if (Tipo = 'CHKNUM') then if (Texto <> '') then Imp_Doc := Texto;
     if (Tipo = 'CHECKNUM') then
       if (Texto <> '') then
       begin
         if (Length(Texto) = 11) then
         begin
           if (Geral.ValidaDataSimples(Copy(Texto,1,8), True)<>Dia)
           and (LeOperData(Texto)<>Operacao) then
             Imp_Doc := Texto;
         end
         else Imp_Doc := Texto;
       end;
   //
     if (Tipo = 'MEMO') then Imp_Memo := Texto;
  //
     if (Tipo = '/GENTRN') then
       if (Texto <> '') then
         Memo1.Lines.Add('Item desconhecido: '+ ArqOF[Index]);
  //
     if (Tipo = '/STMTTRN') then
     begin
       if (Texto <> '') then
         Memo1.Lines.Add('Item desconhecido: '+ ArqOF[Index]);
        Count := Count + 1;

       Imp_Linha := Count;

       CriaLinha;
     end;
  ////////////////////////////////////////////////////////////////////
     if (Tipo = '/STMTRS') then
       if (Texto <> '') then
         Memo1.Lines.Add('Item desconhecido: '+ ArqOF[Index]);
  //
     if (Tipo = '/TRNRS') then
       if (Texto <> '') then
         Memo1.Lines.Add('Item desconhecido: '+ ArqOF[Index]);
  //
     if (Tipo = '/OFC>') then
       if (Texto <> '') then
         Memo1.Lines.Add('Item desconhecido: '+ ArqOF[Index]);
  end;
  {Dmod.QrUpdM.Close;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE import SET CountLinhas=:Count');
  Dmod.QrUpdM.Params[0].AsInteger := Count;
  Dmod.QrUpdM.ExecSQL;}
end;

function TFmConciliaOFX.LeLinha2(Linha : string; Tipo : integer) : string;
begin
  Result := CO_VAZIO;
  if Linha <> CO_VAZIO then
  case Tipo of
    1: Result := Linha[1];
    2: Result := Copy(Linha, 2, Length(Linha)-1);
  end;
end;

function TFmConciliaOFX.LeSoNumero(Linha : string) : string;
var
  i: Integer;
begin
  Result := CO_VAZIO;
  if Linha <> CO_VAZIO then
  for i := 1 to Length(Linha) do
    if (Linha[i] in ['0'..'9']) then Result := Result + Linha[i];

end;

function TFmConciliaOFX.LeData1(Texto : String) : String;
var
  Ano, Mes, Dia : integer;
begin
  Ano := StrToInt(Texto[1]+Texto[2]+Texto[3]+Texto[4]);
  Mes := StrToInt(Texto[5]+Texto[6]);
  Dia := StrToInt(Texto[7]+Texto[8]);
  Result := FormatDateTime(VAR_FORMATDATE, EncodeDate(Ano,Mes,Dia));
end;

function TFmConciliaOFX.LeData3(Texto : String): TDateTime;
var
  Ano, Mes, Dia : integer;
begin
  Ano := StrToInt(Texto[1]+Texto[2]+Texto[3]+Texto[4]);
  Mes := StrToInt(Texto[6]+Texto[7]);
  Dia := StrToInt(Texto[9]+Texto[10]);
  Result := EncodeDate(Ano,Mes,Dia);
end;

function TFmConciliaOFX.LeData2(Texto : String; Tipo: Integer) : String;
var
  Ano, Mes, Dia : integer;
begin
  Ano := 0;
  Mes := 0;
  Dia := 0;
  if Tipo = 0 then
  begin
    Ano := StrToInt(Texto[7]+Texto[8]);
    if Ano > 90 then Ano := Ano + 1900
    else Ano := Ano + 2000;
    Mes := StrToInt(Texto[1]+Texto[2]);
    Dia := StrToInt(Texto[4]+Texto[5]);
  end;
  if Tipo = 4 then
  begin
    Ano := StrToInt(Texto[5]+Texto[6]+Texto[7]+Texto[8]);
    Mes := StrToInt(Texto[1]+Texto[2]);
    Dia := StrToInt(Texto[3]+Texto[4]);
  end;
  Result := FormatDateTime(VAR_FORMATDATE, EncodeDate(Ano,Mes,Dia));
end;

function TFmConciliaOFX.LeValor(Txt : String) : Double;
var
  Inicio, i  : integer;
  Valor, Decimal, Texto: String;
  Ponto : Boolean;
  a, b: Double;
  Virgula: Char;
begin
  Texto := Trim(Txt);
  if pos(',', Texto) > 0 then
    Virgula := ','
  else
    Virgula := '.';
  Ponto := False;
  Valor := '';
  Decimal := '';
  if Texto = CO_VAZIO then Texto := '0';
  if Texto[1] = '-' then
    inicio := 2
  else
    inicio := 1;
  for i := inicio to Length(Texto) do
  begin
    if (Texto[i] = Virgula) then Ponto := True;
    if Texto[i] in ['0'..'9'] then
    begin
      if Ponto = False then
        Valor := Valor + Texto[i]
      else
        Decimal := Decimal + Texto[i];
    end;
  end;
  if Inicio = 2 then Inicio := -1;
  a := Geral.DMV(Valor);
  b := Geral.DMV(Decimal) / 100;
  LeValor := (a+b) * Inicio;
end;

procedure TFmConciliaOFX.Le_OFC100;
{
 Fazendo: Ita� OFC 1.0 (Money 1995 a 1999)
}
var
  //Count,
  index, Linha: Integer;
  S: String;
  F: TextFile;
begin
  ShowMessage('Este formato de arquivo talvez n�o forne�a o n�mero completo do documento!');
  //Count := 0;
  ArqOF.Free;
  Imp_Verif := True;
  LaAviso.Caption := 'Recarregando arquivo';
  Screen.Cursor := crHourglass;
  ArqOF := TStringList.Create;
  AssignFile(F, FArquivo);
  Reset(F);
  while not Eof(F) do
  begin
    Readln(F, S);
//    if S <> '' then
      ArqOF.Add(S);
  end;
  CloseFile(F);
  Imp_IDConta := LeSoNumero(ArqOF[5])+LeSoNumero(ArqOF[7]);
  (*Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE import SET ID_Conta=:P0, Inicio=:P1, Fim=:P2');
  Dmod.QrUpdM.Params[0].AsString := Imp_IDConta;
  Dmod.QrUpdM.Params[1].AsString := LeSoNumero(ArqOF[09]);
  Dmod.QrUpdM.Params[2].AsString := LeSoNumero(ArqOF[10]);
  Dmod.QrUpdM.ExecSQL;

  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('INSERT INTO importits SET Linha=:P0, Data=:P1, ');
  Dmod.QrUpdM.SQL.Add('Credito=:P2, Debito=:P3, Doc=:P4, Memo=:P5, ');
  Dmod.QrUpdM.SQL.Add('Descricao=:P6');*)

  Imp_Linha := 0;
  for Index := 1 to ((ArqOF.Count div 60)-1) do
  begin
    PBInfo.Position := Index;
    PBInfo.Update;
    Linha := 40+ ((Index-1)*60);
    Imp_Valor := Geral.DMV(ArqOF[(Linha+25)]+','+ArqOF[(Linha+26)]);
    Imp_Data := ArqOF[(Linha+11)];
    Imp_Doc  := ArqOF[(Linha+7)];
    Imp_Memo := ArqOF[(Linha+60)];
    Imp_Linha := Imp_Linha + 1;
    if ArqOF[(Linha+23)] = '1' then  Imp_Valor := -Imp_Valor;
    //
    CriaLinha;
  end;
  {Dmod.QrUpdM.Close;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE import SET CountLinhas=:Count');
  Dmod.QrUpdM.Params[0].AsInteger := Count;
  Dmod.QrUpdM.ExecSQL;}
end;

procedure TFmConciliaOFX.Le_QIF(Tipo: Integer);
{
 Testado: Real[cc_xxxxxxxx.qif] onde x=data
}
var
  Index, Count, Linha : Integer;
begin
  Count := 0;
  if Tipo = 0 then begin
    Imp_IDConta := LeLinha2(ArqOF[1],2);

    {Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('UPDATE import SET ID_Conta=:P0, Inicio = 0000/00/00, Fim = 0000/00/00');
    Dmod.QrUpdM.Params[0].AsString := Imp_IDConta;
    Dmod.QrUpdM.ExecSQL;}
  end;

  {Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('INSERT INTO importits SET Linha=:P0, Data=:P1, ');
  Dmod.QrUpdM.SQL.Add('Credito=:P2, Debito=:P3, Doc=:P4, Memo=:P5, ');
  Dmod.QrUpdM.SQL.Add('Descricao=:P6');}

  Imp_Linha := 0;
  for Index := 1 to ((ArqOF.Count div 5)-1) do
  begin
    PBInfo.Position := Index;
    PBInfo.Update;
    Linha := Index*5 - Tipo;
    Imp_Valor := LeValor(Copy(ArqOF[Linha+1], 2, Length(ArqOF[Linha+1])-1));
    Imp_Memo := Copy(ArqOF[Linha+3], 2, Length(ArqOF[Linha+3])-1);
    if Imp_Valor < 0 then Imp_Valor := Imp_Valor * -1;
    Imp_Data := LeData2(Copy(ArqOF[Linha], 2, Length(ArqOF[Linha])-1), Tipo);
    Imp_Doc := Copy(ArqOF[Linha+2], 2, Length(ArqOF[Linha+2])-1);
    Imp_Linha := Imp_Linha + 1;
    //
    CriaLinha;
  end;

  // SISTEMA ANTIGO
(*  for Index := 5 to ArqOF.Count-1 do
  begin
     Tipo := LeLinha2(Linha],1);
     Texto := LeLinha2(ArqOF[Index],2);

  //
    if (Tipo = 'D') then Imp_Data := LeData2(Texto);
  //
    if (Tipo = 'T') then
    begin
      if (Texto <> '') then
      begin
        Valor := LeValor(Texto);
        if Valor > 0 then
          Imp_Credito := Valor
        else
          Imp_Debito := Valor * -1;
      end;
    end;
  //
    if (Tipo = 'N') then
    begin
       if (Texto <> '') then
       Imp_Doc := StrToFloat(Texto);
    end;
  //
    if (Tipo = 'M') then Imp_Memo := Texto;
  //
    if (Tipo = '^') then
    begin
      if (Texto <> CO_VAZIO) then ShowMessage('Item desconhecido: '+ ArqOF[Index]);
      Count := Count + 1;

      Imp_Linha := Count;

      CriaLinha;
    end;
  end;*)
  Dmod.QrUpdM.Close;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE import SET CountLinhas=:Count');
  Dmod.QrUpdM.Params[0].AsInteger := Count;
  Dmod.QrUpdM.ExecSQL;
end;

function TFmConciliaOFX.LeLinha(Linha : string; Tipo : integer) : string;
var
  i : integer;
  Texto : string;
  Fim, Inicio : boolean;
begin
  Inicio := False;
  Fim := False;
  Texto := '';
  for i := 1 to Length(linha) do
  begin
    if Linha[i] = '>' then
    begin
      Inicio := False;
      Fim := True;
    end;
    if (Inicio=True) and (Fim=False) and(Tipo=1) then Texto := Texto+Linha[i];
    if (Fim=True) and (Inicio=True) and (Tipo=2) then Texto := Texto+Linha[i];
    if (Linha[i] = '<') or (Linha[i] = '>') then Inicio := True;
  end;
  LeLinha := Texto;
end;

function TFmConciliaOFX.LeOperData(Texto : String) : Integer;
begin
  Result := StrToInt(Texto[9]+Texto[10]+Texto[11]);
end;

procedure TFmConciliaOFX.CriaLinha;
begin
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FmConcilia.FConcilia + ' SET ');
  DmodG.QrUpdPID1.SQL.Add('Linha=:P0, DataM=:P1, Texto=:P2, Docum=:P3, ');
  DmodG.QrUpdPID1.SQL.Add('Valor=:P4, Saldo=:P5, Clien=:P6, Forne=:P7, ');
  DmodG.QrUpdPID1.SQL.Add('Perio=:P8, NOMEDEPTO=:P9, NOMECONTA=:P10, ');
  DmodG.QrUpdPID1.SQL.Add('NOMETERCE=:P11, NOMECARTE=:P12, TIPOCARTE=:P13');

  DmodG.QrUpdPID1.Params[00].AsInteger := Imp_Linha;
  DmodG.QrUpdPID1.Params[01].AsString  := Imp_Data;
  DmodG.QrUpdPID1.Params[02].AsString  := Imp_Memo;
  DmodG.QrUpdPID1.Params[03].AsString  := Imp_Doc;
  DmodG.QrUpdPID1.Params[04].AsFloat   := Imp_Valor;
  DmodG.QrUpdPID1.Params[05].AsFloat   := Imp_Saldo;
  DmodG.QrUpdPID1.Params[06].AsInteger := 0;
  DmodG.QrUpdPID1.Params[07].AsInteger := 0;
  DmodG.QrUpdPID1.Params[08].AsInteger := 0;
  //
  DmodG.QrUpdPID1.Params[09].AsString  := '';
  DmodG.QrUpdPID1.Params[10].AsString  := '';
  DmodG.QrUpdPID1.Params[11].AsString  := '';
  DmodG.QrUpdPID1.Params[12].AsString  := '';
  DmodG.QrUpdPID1.Params[13].AsInteger := 0;
  DmodG.QrUpdPID1.ExecSQL;
  //
  ResetaDadosIns;
end;

procedure TFmConciliaOFX.ResetaDadosIns;
begin
  Imp_Data := '';
  Imp_Valor := 0;
  Imp_Doc := '';
  Imp_Memo := '';
end;

end.

