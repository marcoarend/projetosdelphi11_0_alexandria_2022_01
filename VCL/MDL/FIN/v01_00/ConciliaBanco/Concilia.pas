unit Concilia;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, UnMLAGeral, Grids, ExtCtrls, ComCtrls, Db, mySQLDbTables,
  DBGrids, DBCtrls, Menus, Mask, frxClass, frxDBSet, ComObj, UnFinanceiro,
  Variants, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB, UnDmkProcFunc,
  UnDmkEnums;

type
  TFmConcilia = class(TForm)
    OpenDialog3: TOpenDialog;
    PainelTitulo: TPanel;
    Image1: TImage;
    PnImporta: TPanel;
    PB1: TProgressBar;
    Grade: TStringGrid;
    Panel2: TPanel;
    BtImporta: TBitBtn;
    BtSalva: TBitBtn;
    BtVerSalvos: TBitBtn;
    PnConcilia: TPanel;
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    QrConcilia0: TmySQLQuery;
    DsConcilia0: TDataSource;
    Panel3: TPanel;
    Label1: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrConcilia0Linha: TIntegerField;
    QrConcilia0DataM: TDateField;
    QrConcilia0Texto: TWideStringField;
    QrConcilia0Docum: TWideStringField;
    QrConcilia0Valor: TFloatField;
    QrConcilia0Saldo: TFloatField;
    QrConcilia0Conta: TIntegerField;
    BtLinkar: TBitBtn;
    PMLinkar: TPopupMenu;
    Informaaconta1: TMenuItem;
    QrConcilia0Acao: TIntegerField;
    QrConcilia0NOMEACAO: TWideStringField;
    N2: TMenuItem;
    Fazervarreduraparaconsolidaoporlink1: TMenuItem;
    QrLocLanct0: TmySQLQuery;
    QrLocCta: TmySQLQuery;
    QrLocCtaConta: TIntegerField;
    QrConcilia0EXPLICACAO: TWideStringField;
    Label2: TLabel;
    QrCarteirasBanco1: TIntegerField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    QrLocDsc: TmySQLQuery;
    QrMascaras: TmySQLQuery;
    QrMascarasTexto: TWideStringField;
    QrMascarasDoc: TWideStringField;
    QrMascarasConta: TIntegerField;
    QrDesLoc: TmySQLQuery;
    QrDesLocLinha: TIntegerField;
    QrLocDscControle: TIntegerField;
    QrConcilia0DifEr: TFloatField;
    BtCadastro: TBitBtn;
    PMCadastro: TPopupMenu;
    CadastrarnovoLinkdeconta1: TMenuItem;
    Cadastrarnovoitemignorvel1: TMenuItem;
    Panel4: TPanel;
    BitBtn3: TBitBtn;
    BtConciliar: TBitBtn;
    BtAcao: TBitBtn;
    PMAcao: TPopupMenu;
    Ignora1: TMenuItem;
    Limpa1: TMenuItem;
    Mudaaoparainclui1: TMenuItem;
    QrImpede: TmySQLQuery;
    QrImpedeItens: TLargeintField;
    QrCarteirasEntiBank: TIntegerField;
    QrLocCtaCliente: TIntegerField;
    QrLocCtaFornece: TIntegerField;
    QrLocCtaUsaEntBank: TSmallintField;
    QrConcilia0Clien: TIntegerField;
    QrConcilia0Forne: TIntegerField;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    N1: TMenuItem;
    AlteraClienteFornecedor1: TMenuItem;
    AlteraDepto1: TMenuItem;
    N3: TMenuItem;
    xcluirlinhasselecionadas1: TMenuItem;
    QrLocCtaMensal: TWideStringField;
    QrConcilia0Perio: TIntegerField;
    QrConcilia0Periodo_TXT: TWideStringField;
    Alteraperodomsdecompetnciadolanamento1: TMenuItem;
    Excluirlinhasselecionadas1: TMenuItem;
    ExcluirTODOSitensIgnorados1: TMenuItem;
    QrCarteirasTipo: TIntegerField;
    QrDupl: TmySQLQuery;
    QrConcilia0SERIE: TWideStringField;
    QrDuplControle: TIntegerField;
    QrConcilia0CtrlE: TIntegerField;
    ExcluirTODOSitensDuplicados1: TMenuItem;
    ExcluirTODOSitensJQuitados1: TMenuItem;
    N4: TMenuItem;
    QrLocLanct0Documento: TFloatField;
    QrLocLanct0Debito: TFloatField;
    QrLocLanct0Controle: TIntegerField;
    QrLocLanct0Sub: TSmallintField;
    QrLocLanct0Credito: TFloatField;
    QrLocLanct0Carteira: TIntegerField;
    QrLocLanct0Descricao: TWideStringField;
    QrLocLanct0NotaFiscal: TIntegerField;
    QrLocLanct0Cliente: TIntegerField;
    QrLocLanct0Fornecedor: TIntegerField;
    QrLocLanct0Account: TIntegerField;
    QrLocLanct0Vendedor: TIntegerField;
    QrLocLanct0Tipo: TSmallintField;
    QrLocLanct0Vencimento: TDateField;
    QrLocLanct0Genero: TIntegerField;
    QrLocLanct0CliInt: TIntegerField;
    QrLocLanct0Mez: TIntegerField;
    N5: TMenuItem;
    Transferncia1: TMenuItem;
    QrCarteirasForneceI: TIntegerField;
    QrConcilia0CartT: TIntegerField;
    BitBtn1: TBitBtn;
    DeixarapenasConciliveis1: TMenuItem;
    Manteritenssemerro1: TMenuItem;
    Nomanteritenssemerro1: TMenuItem;
    N6: TMenuItem;
    Altera1: TMenuItem;
    QrConcilia0CtaLk: TIntegerField;
    QrLocCtaComposHist: TSmallintField;
    frxConcilia: TfrxReport;
    frxDsConcilia: TfrxDBDataset;
    BitBtn4: TBitBtn;
    PMImporta: TPopupMenu;
    Excel1: TMenuItem;
    Money20001: TMenuItem;
    Timer1: TTimer;
    BitBtn5: TBitBtn;
    BitBtn2: TBitBtn;
    QrConcilia0CHEQUE: TFloatField;
    QrConcilia0Depto: TIntegerField;
    QrConcilia0NOMEDEPTO: TWideStringField;
    QrConcilia0NOMECONTA: TWideStringField;
    QrConcilia0NOMETERCE: TWideStringField;
    QrConcilia0NOMECARTE: TWideStringField;
    QrConcilia0TIPOCARTE: TIntegerField;
    QrLocCtaNome: TWideStringField;
    QrCarteirasNOMEBCO: TWideStringField;
    BitBtn6: TBitBtn;
    Selecionacontaadequada1: TMenuItem;
    Selecionacontaadequada2: TMenuItem;
    PMDoubleClick: TPopupMenu;
    Selecionacontaadequada3: TMenuItem;
    AlteraClienteFornecedor2: TMenuItem;
    AlteraDepto2: TMenuItem;
    Alteraperiodo2: TMenuItem;
    AlteraHistorico2: TMenuItem;
    N7: TMenuItem;
    InformaraContasercriadoumlanamento1: TMenuItem;
    ransferncia1: TMenuItem;
    N8: TMenuItem;
    Gerenciamentodelinks1: TMenuItem;
    QrLocCtaMultiCtas: TIntegerField;
    DBGrid2: TDBGrid;
    QrContasLnkIts: TmySQLQuery;
    QrContasLnkItsSubCtrl: TIntegerField;
    QrContasLnkItsGenero: TIntegerField;
    QrContasLnkItsTexto: TWideStringField;
    QrContasLnkItsTipoCalc: TSmallintField;
    QrContasLnkItsFator: TFloatField;
    QrContasLnkItsConsidera: TSmallintField;
    QrContasLnkItsCliente: TIntegerField;
    QrContasLnkItsFornece: TIntegerField;
    QrContasLnkItsUsaEntBank: TSmallintField;
    QrContasLnkItsComposHist: TSmallintField;
    QrConcilia1: TmySQLQuery;
    DsConcilia1: TDataSource;
    QrConcilia1Linha: TIntegerField;
    QrConcilia1DataM: TDateField;
    QrConcilia1Texto: TWideStringField;
    QrConcilia1Valor: TFloatField;
    QrConcilia1Docum: TWideStringField;
    QrConcilia1Saldo: TFloatField;
    QrConcilia1Conta: TIntegerField;
    QrConcilia1Acao: TIntegerField;
    QrConcilia1NOMEACAO: TWideStringField;
    QrConcilia1EXPLICACAO: TWideStringField;
    QrConcilia1DifEr: TFloatField;
    QrConcilia1Clien: TIntegerField;
    QrConcilia1Forne: TIntegerField;
    QrConcilia1Perio: TIntegerField;
    QrConcilia1Periodo_TXT: TWideStringField;
    QrConcilia1SERIE: TWideStringField;
    QrConcilia1CtrlE: TIntegerField;
    QrConcilia1CartT: TIntegerField;
    QrConcilia1CtaLk: TIntegerField;
    QrConcilia1CHEQUE: TFloatField;
    QrConcilia1Depto: TIntegerField;
    QrConcilia1NOMEDEPTO: TWideStringField;
    QrConcilia1NOMECONTA: TWideStringField;
    QrConcilia1NOMETERCE: TWideStringField;
    QrConcilia1NOMECARTE: TWideStringField;
    QrConcilia1TIPOCARTE: TIntegerField;
    QrLocCtaControle: TIntegerField;
    QrContasLnkItsNOMEGENERO: TWideStringField;
    QrContasLnkItsMensal: TWideStringField;
    QrContasLnkItsNOME_CLIENTE: TWideStringField;
    QrContasLnkItsNOME_FORNECE: TWideStringField;
    QrLocCtaNOME_CLIENTE: TWideStringField;
    QrLocCtaNOME_FORNECE: TWideStringField;
    Splitter1: TSplitter;
    QrLocLanct1: TmySQLQuery;
    QrLocLanct1Valor: TFloatField;
    QrLocLanct1Itens: TLargeintField;
    QrConcilia0Nivel: TIntegerField;
    QrConcilia0LinIt: TIntegerField;
    Gerarnmerododocumentopelohistrico1: TMenuItem;
    N9: TMenuItem;
    irarpontodonmerododocumento1: TMenuItem;
    procedure BtImportaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure GradeDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure BtSalvaClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BtVerSalvosClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdCarteiraChange(Sender: TObject);
    procedure Informaaconta1Click(Sender: TObject);
    procedure BtLinkarClick(Sender: TObject);
    procedure QrConcilia0CalcFields(DataSet: TDataSet);
    procedure Fazervarreduraparaconsolidaoporlink1Click(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure CadastrarnovoLinkdeconta1Click(Sender: TObject);
    procedure Cadastrarnovoitemignorvel1Click(Sender: TObject);
    procedure BtCadastroClick(Sender: TObject);
    procedure QrConcilia0AfterOpen(DataSet: TDataSet);
    procedure QrConcilia0BeforeClose(DataSet: TDataSet);
    procedure BtAcaoClick(Sender: TObject);
    procedure Limpa1Click(Sender: TObject);
    procedure Ignora1Click(Sender: TObject);
    procedure Mudaaoparainclui1Click(Sender: TObject);
    procedure BtConciliarClick(Sender: TObject);
    procedure AlteraClienteFornecedor1Click(Sender: TObject);
    procedure AlteraDepto1Click(Sender: TObject);
    procedure PMAcaoPopup(Sender: TObject);
    procedure Alteraperodomsdecompetnciadolanamento1Click(Sender: TObject);
    procedure Excluirlinhasselecionadas1Click(Sender: TObject);
    procedure ExcluirTODOSitensIgnorados1Click(Sender: TObject);
    procedure ExcluirTODOSitensDuplicados1Click(Sender: TObject);
    procedure ExcluirTODOSitensJQuitados1Click(Sender: TObject);
    procedure Transferncia1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Manteritenssemerro1Click(Sender: TObject);
    procedure Nomanteritenssemerro1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure frxConciliaGetValue(const VarName: String;
      var Value: Variant);
    procedure BitBtn4Click(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure Money20001Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure Selecionacontaadequada1Click(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure PMLinkarPopup(Sender: TObject);
    procedure Selecionacontaadequada3Click(Sender: TObject);
    procedure Selecionacontaadequada2Click(Sender: TObject);
    procedure PMDoubleClickPopup(Sender: TObject);
    procedure AlteraHistorico2Click(Sender: TObject);
    procedure InformaraContasercriadoumlanamento1Click(Sender: TObject);
    procedure ransferncia1Click(Sender: TObject);
    procedure AlteraClienteFornecedor2Click(
      Sender: TObject);
    procedure AlteraDepto2Click(Sender: TObject);
    procedure Alteraperiodo2Click(Sender: TObject);
    procedure Gerenciamentodelinks1Click(Sender: TObject);
    procedure QrConcilia0AfterScroll(DataSet: TDataSet);
    procedure QrConcilia1CalcFields(DataSet: TDataSet);
    procedure QrCarteirasAfterOpen(DataSet: TDataSet);
    procedure Gerarnmerododocumentopelohistrico1Click(Sender: TObject);
    procedure irarpontodonmerododocumento1Click(Sender: TObject);
  private
    { Private declarations }
    FNewForne, FNewDepto, FNewClien, FNewPerio: Integer;
    FNewClienNome, FNewForneNome, FNewDeptoNome: String;
    FNewVal: Double;
    //
    //Inclus�o na tabela "Concilia"
    F_Linha, F_Nivel, F_LinIt, F_Acao, F_Conta, F_Clien, F_Forne, F_Depto,
    F_Perio, F_CtrlE, F_CartT,F_CtaLk, F_TIPOCARTE: Integer;
    F_DataM: TDateTime;
    F_Texto, F_Docum: String;
    F_Valor, F_Saldo, F_DifEr: Double;
    F_NOMEDEPTO, F_NOMECONTA, F_NOMETERCE, F_NOMECARTE: String;
    // FIM Inclus�o na tabela "Concilia"
    //
    FEmiData: TDateTime;
    FEmiQtd: Integer;
    FEmiVal: Double;
    //
    procedure ExecuteAcao(Acao: Integer);
    function QuitaDocumento: Boolean;
    procedure ReopenLocLanct0(const SitA, SitB: Integer; const Doc:
              Double; const Banco: Integer;
              const SerieCH: String; var Val: Double; var Qtd: Integer);
    procedure ReopenLocLanct1(const Doc: Double; const Banco: Integer;
              const SerieCH: String; const Data: TDateTime; var Val: Double; var Qtd: Integer);
    function CompoemHistorico(Query: TmySQLQuery; Nivel: Integer): String;
    procedure MostraConciliacao;
    procedure PreparaSQLLocCta(Tipo: Integer);
    procedure SelecionaContaAdequada();
    procedure AlteraHistorico();
    procedure InformaAConta();
    procedure TransfernciaEntreCarteiras();
    procedure ReopenConcilia1(Item: Integer);
    procedure LimpaVars();
    function InsereConcilia(): Boolean;
    function Texto_NOMEACAO(Acao: Integer): String;
    function Texto_EXPLICACAO(Acao: Integer): String;
    function VerificaSeEstaQuitado(const Tipo: Integer; const DocTxt, Serie:
             String; var Diferenca: Double): Boolean;
  public
    { Public declarations }
    FConcilia, FNomeCliInt: String;
    FCondominio, FCartConcilia, F_CliInt: Integer;
    FMostra, FVarre: Boolean;
    FQrLct, FQrCarteiras: TmySQLQuery;
    FDTPicker: TDateTimePicker;
    FModuleLctX: TDataModule;
    //
    function Xls_To_StringGrid(AGrid: TStringGrid; AXLSFile: string): Boolean;
    procedure ReopenConcilia(Linha: Integer);
    procedure ExcluirNaoConciliaveis(Nivel: Integer);
    procedure UpdateAcao0();
    procedure UpdateAcao1();

  end;

var
  FmConcilia: TFmConcilia;

implementation

uses UnMyObjects, UCreate, Module, SelConta, UnInternalConsts, ContasLnkEdit, UMySQLModule,
Principal, SelCod, Periodo, GetCarteira, UCashier, ConciliaXLSLoad, ConciliaOFX,
Transfer2, MyDBCheck, ModuleGeral, GModule, LocDefs, ConciliaMultiCtas,
ContasLnk, GetValor, ModuleBco;

{$R *.DFM}

function TFmConcilia.Xls_To_StringGrid(AGrid: TStringGrid; AXLSFile: string): Boolean;
const
  xlCellTypeLastCell = $0000000B;
var
  XLApp, Sheet: OLEVariant;
  RangeMatrix: Variant;
  x, y, k, r: Integer;
begin
  Screen.Cursor := crHourGlass;
  Result := False;
  XLApp := CreateOleObject('Excel.Application');
  try
    XLApp.Visible := False;
    XLApp.Workbooks.Open(AXLSFile);
    Sheet := XLApp.Workbooks[ExtractFileName(AXLSFile)].WorkSheets[1];
    Sheet.Cells.SpecialCells(xlCellTypeLastCell, EmptyParam).Activate;
    x := XLApp.ActiveCell.Row;
    y := XLApp.ActiveCell.Column;
    AGrid.RowCount := x;
    AGrid.ColCount := y + 1;
    RangeMatrix := XLApp.Range['A1', XLApp.Cells.Item[X, Y]].Value;
    k := 1; // 2?
    repeat
      Update;
      Application.ProcessMessages;
      AGrid.Cells[0, (k - 1)] := FormatFloat('000', (k - 1));
      for r := 1 to y do
        AGrid.Cells[r, (k - 1)] := RangeMatrix[K, R];
      Inc(k, 1);
      AGrid.RowCount := k + 2;
    until k > x;
    RangeMatrix := Unassigned;
    Geral.MB_Info('Os dados foram importados com sucesso do arquivo: ' +#13#10 +
      OpenDialog3.FileName);
  finally
    if not VarIsEmpty(XLApp) then
    begin
      XLApp.Quit;
      XLAPP := Unassigned;
      Sheet := Unassigned;
      Result := True;
    end;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmConcilia.BtImportaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImporta, BtImporta);
end;

procedure TFmConcilia.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  Grade.Cells[00,00] := 'Seq';
  Grade.Cells[01,00] := 'Data';
  Grade.Cells[02,00] := 'Descri��o';
  Grade.Cells[03,00] := 'Documento';
  Grade.Cells[04,00] := 'Valor(R$)';
  Grade.Cells[05,00] := 'Saldo(R$)';
  //
  Grade.ColWidths[00] := 032;
  Grade.ColWidths[01] := 064;
  Grade.ColWidths[02] := 240;
  Grade.ColWidths[03] := 064;
  Grade.ColWidths[04] := 072;
  Grade.ColWidths[05] := 072;
  //
  Timer1.Enabled := True;
end;

procedure TFmConcilia.Gerarnmerododocumentopelohistrico1Click(Sender: TObject);
begin
  ExecuteAcao(95);
end;

procedure TFmConcilia.Gerenciamentodelinks1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContasLnk, FmContasLnk, afmoNegarComAviso) then
  begin
    FmContasLnk.ShowModal;
    FmContasLnk.Destroy;
  end;
end;

procedure TFmConcilia.GradeDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
{var
  OldAlign: Integer;}
begin
  {if ARow = 0 then
  begin
    //
  end else if ACol = 0 then begin
    if ARow <> 0 then begin
      OldAlign := SetTextAlign(Grade.Canvas.Handle, TA_CENTER);
      Grade.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Geral.TFD(Grade.Cells[Acol, ARow], 3, siPositivo));
      SetTextAlign(Grade.Canvas.Handle, OldAlign);
    end;
  end else if ACol = 01 then begin
      OldAlign := SetTextAlign(Grade.Canvas.Handle, TA_CENTER);
      Grade.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Grade.Cells[Acol, ARow]);
      SetTextAlign(Grade.Canvas.Handle, OldAlign);
  end else if ACol = 02 then begin
      OldAlign := SetTextAlign(Grade.Canvas.Handle, TA_LEFT);
      Grade.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        Grade.Cells[Acol, ARow]);
      SetTextAlign(Grade.Canvas.Handle, OldAlign);
  end else if ACol = 03 then begin
      OldAlign := SetTextAlign(Grade.Canvas.Handle, TA_CENTER);
      Grade.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Grade.Cells[Acol, ARow]);
      SetTextAlign(Grade.Canvas.Handle, OldAlign);
  end else if ACol = 04 then begin
      OldAlign := SetTextAlign(Grade.Canvas.Handle, TA_RIGHT);
      Grade.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Geral.TFT(Grade.Cells[Acol, ARow], 2, siNegativo));
      SetTextAlign(Grade.Canvas.Handle, OldAlign);
  end else if ACol = 05 then begin
      OldAlign := SetTextAlign(Grade.Canvas.Handle, TA_RIGHT);
      Grade.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Geral.TFT(Grade.Cells[Acol, ARow], 2, siNegativo));
      SetTextAlign(Grade.Canvas.Handle, OldAlign);
  end}
end;

procedure TFmConcilia.BtSalvaClick(Sender: TObject);
var
  i, Clien, Forne, Perio: Integer;
  DataM: TDateTime;
  Valor, Saldo: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    //Ucriar.AtualizaTabelaControleLocal('CartConcilia', '', '', 0, '', 0);
    PB1.Position := 0;
    PB1.Max := Grade.RowCount - 1;
    UCriar.RecriaTempTable(FConcilia, DmodG.QrUpdPID1, False);
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FConcilia + ' SET ');
    DmodG.QrUpdPID1.SQL.Add('Linha=:P0, DataM=:P1, Texto=:P2, Docum=:P3, ');
    DmodG.QrUpdPID1.SQL.Add('Valor=:P4, Saldo=:P5, Clien=:P6, Forne=:P7, ');
    DmodG.QrUpdPID1.SQL.Add('Perio=:P8');
    //
    for i := 1 to Grade.RowCount -1 do
    begin
      PB1.Position := PB1.Position + 1;
      DataM := Geral.ValidaDataSimples(Grade.Cells[1, i], True);
      if DataM > 0 then
      begin
        Valor := Geral.DMV(Grade.Cells[4, i]);
        Saldo := Geral.DMV(Grade.Cells[5, i]);
        //
        if Valor <> 0 then
        begin
          Clien := 0;
          Forne := 0;
          Perio := 0;
          DmodG.QrUpdPID1.Params[00].AsInteger := i;
          DmodG.QrUpdPID1.Params[01].AsString  := Geral.FDT(DataM, 1);
          DmodG.QrUpdPID1.Params[02].AsString  := Grade.Cells[2, i];
          DmodG.QrUpdPID1.Params[03].AsString  := Grade.Cells[3, i];
          DmodG.QrUpdPID1.Params[04].AsFloat   := Valor;
          DmodG.QrUpdPID1.Params[05].AsFloat   := Saldo;
          DmodG.QrUpdPID1.Params[06].AsInteger := Clien;
          DmodG.QrUpdPID1.Params[07].AsInteger := Forne;
          DmodG.QrUpdPID1.Params[08].AsInteger := Perio;
          //
          DmodG.QrUpdPID1.ExecSQL;
        end;
      end;
    end;
    Application.MessageBox('Os dados foram salvos com sucesso!', 'Informa��o',
      MB_OK+MB_ICONINFORMATION);
    PB1.Position := 0;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmConcilia.BitBtn2Click(Sender: TObject);
begin
  Close;
end;

procedure TFmConcilia.BtVerSalvosClick(Sender: TObject);
begin
  MostraConciliacao;
end;

procedure TFmConcilia.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  FConcilia := 'Concilia';
  QrConcilia0.Database := DModG.MyPID_DB;
  QrConcilia1.Database := DModG.MyPID_DB;
  QrImpede.Database   := DModG.MyPID_DB;
  QrDesLoc.Database   := DModG.MyPID_DB;
  //
  //Fazer concilia��o excel geral com configExcel
  FCartConcilia := 0;
  FMostra := False;
  FVarre := False;
  PnImporta.Align := alClient;
  //
  if (Uppercase(Application.Title) = 'SYNDIC')
  or (Uppercase(Application.Title) = 'SYNKER') then
  begin
    for i := 0 to DBGrid1.Columns.Count - 1 do
      if Uppercase(DBGrid1.Columns[i].FieldName) = 'NOMEDEPTO' then
        DBGrid1.Columns[i].Title.Caption := 'U.H.';
     AlteraDepto1.Caption := 'Altera U.H. das linhas selecionadas';
  end;
end;

procedure TFmConcilia.MostraConciliacao;
begin
  Update;
  Application.ProcessMessages;
  Screen.Cursor := crHourGlass;
  try
    if F_CliInt = 0 then
    begin
      Application.MessageBox(PChar('N�o h� cliente interno selecionado!'),
      'Aviso', MB_OK+MB_ICONWARNING);
      //Ucriar.AtualizaTabelaControleLocal('CartConcilia', '', '', 0, '', 0);
      Exit;
    end;
    QrCarteiras.Close;
    QrCarteiras.Params[0].AsInteger := F_CliInt;//FmCondGer.QrCondCliente.Value;
    QrCarteiras.Open;
    //
    //if Dmod.QrLocCtrlCartConcilia.Value <> 0 then
    if FCartConcilia <> 0 then
    begin
      if not QrCarteiras.Locate('Codigo', FCartConcilia, [])
      then begin
        Application.MessageBox(PChar('O extrato carregado n�o pertence ao ' +
        'cliente interno selecionado ou o c�digo do banco n�o est� definido no ' +
        'cadastro da carteira ou o banco n�o existe na tabela de bancos!'),
        'Aviso', MB_OK+MB_ICONWARNING);
        //Ucriar.AtualizaTabelaControleLocal('CartConcilia', '', '', 0, '', 0);
        Exit;
      end;
    end;
    PnConcilia.Visible := True;
    PnImporta.Visible  := False;
    //
    ReopenConcilia(0);
    if FCartConcilia > 0 then
    begin
      EdCarteira.Text := FormatFloat('0', FCartConcilia);
      CBCarteira.KeyValue := FCartConcilia;
    end;
    //
    if FVarre then
    begin
      FVarre := False;
      if Geral.IMV(EdCarteira.Text) > 0 then
        Fazervarreduraparaconsolidaoporlink1Click(Self);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmConcilia.EdCarteiraChange(Sender: TObject);
begin
  if QrConcilia0.State = dsBrowse then
    BtLinkar.Enabled := (Geral.IMV(EdCarteira.Text) <> 0)
    and (QrConcilia0.State <> dsInactive)
    and (QrConcilia0.RecordCount > 0)
  else BtLinkar.Enabled := False;
  BtCadastro.Enabled := BtLinkar.Enabled;
  BtConciliar.Enabled := BtLinkar.Enabled;
end;

procedure TFmConcilia.BtLinkarClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLinkar, BtLinkar);
end;

procedure TFmConcilia.ReopenConcilia(Linha: Integer);
begin
  QrConcilia0.Close;
  try
    QrConcilia0.Open;
  except
    FConcilia := UCriar.RecriaTempTable(FConcilia, DmodG.QrUpdPID1, False);
    {
    Application.MessageBox(PChar('A tabela temporaria de concilia��o foi ' +
    'recriada porque algum erro impediu a sua abertura!' + #13#10 +
    'Caso j� exista uma importa��o tente refaz�-la!' + #13#10 +
    'Caso seja seu primeiro acesso � concilia��o, desconsidere este aviso.'),
    'Aviso', MB_OK+MB_ICONINFORMATION);
    }
    QrConcilia0.Open;
    // Evitar erro em locate!
    //Exit;
  end;
  //
  QrConcilia0.Locate('Linha', Linha, []);
end;

procedure TFmConcilia.ReopenConcilia1(Item: Integer);
begin
  QrConcilia1.Close;
  QrConcilia1.Params[0].AsInteger := QrConcilia0Linha.Value;
  QrConcilia1.Open;
end;

procedure TFmConcilia.Informaaconta1Click(Sender: TObject);
begin
  InformaAConta();
end;

procedure TFmConcilia.InformaAConta();
var
  DataM, Texto, Docum: String;
  DifEr, Valor, Saldo: Double;
  Acao, Conta, Clien, Forne, Depto, Perio, CtrlE, CartT, CtaLk: Integer;
  NOMEDEPTO, NOMECONTA, NOMETERCE, NOMECARTE: String;
  TIPOCARTE: Integer;
  // Index
  Linha, Nivel, LinIt: Integer;
begin
  VAR_CONTA := 0;
  if MyObjects.CriaForm_AcessoTotal(TFmSelConta, FmSelConta) then
  begin
    FmSelConta.QrContas.Close;
    FmSelConta.QrContas.SQL.Clear;
    FmSelConta.QrContas.SQL.Add('SELECT Codigo, Nome');
    FmSelConta.QrContas.SQL.Add('FROM contas');
    if QrConcilia0Valor.Value > 0 then
      FmSelConta.QrContas.SQL.Add('WHERE Credito="V"')
    else
      FmSelConta.QrContas.SQL.Add('WHERE Debito="V"');
    FmSelConta.QrContas.SQL.Add('ORDER BY Nome');
    FmSelConta.QrContas.Open;
    //
    FmSelConta.EdDescricao.Text := QrConcilia0Texto.Value;
    FmSelConta.ShowModal;
    FmSelConta.Destroy;
    if VAR_CONTA <> 0 then
    begin
      if QrLocCtaMensal.Value <> 'V' then Perio := 0 else
        Perio := MLAGeral.DataToPeriodo(QrConcilia0DataM.Value);
{
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FConcilia + ' SET ');
      DmodG.QrUpdPID1.SQL.Add('Conta=:P0, NOMECONTA=:P1, Perio=:P2, Acao=1 ');
      DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:P3 AND Nivel=0');
      DmodG.QrUpdPID1.Params[00].AsInteger := VAR_CONTA;
      DmodG.QrUpdPID1.Params[01].AsString  := VAR_CONTA_NOME;
      DmodG.QrUpdPID1.Params[02].AsInteger := Perio;
      //
      DmodG.QrUpdPID1.Params[03].AsInteger := QrConcilia0Linha.Value;
      DmodG.QrUpdPID1.ExecSQL;
}
      DataM     := Geral.FDT(QrConcilia0DataM.Value, 1);
      Texto     := VAR_HISTORICO;
      Docum     := QrConcilia0Docum.Value;
      Valor     := QrConcilia0Valor.Value;
      Saldo     := QrConcilia0Saldo.Value;
      Acao      := QrConcilia0Acao.Value;
      Conta     := VAR_CONTA;
      Clien     := VAR_CLIENTE;
      Forne     := VAR_FORNECE;
      Depto     := QrConcilia0Depto.Value;
      //Perio     := QrConcilia0Perio.Value; feito acima
      CtrlE     := QrConcilia0CtrlE.Value;
      CartT     := QrConcilia0CartT.Value;
      CtaLk     := QrConcilia0CtaLk.Value;
      DifEr     := QrConcilia0DifEr.Value;
      NOMEDEPTO := QrConcilia0NOMEDEPTO.Value;
      NOMECONTA := VAR_CONTA_NOME;
      NOMETERCE := VAR_NO_TERC;
      NOMECARTE := QrConcilia0NOMECARTE.Value;
      TIPOCARTE := QrConcilia0TIPOCARTE.Value;
      // Index  := QrConcilia0L.Value;
      Linha     := QrConcilia0Linha.Value;
      Nivel     := QrConcilia0Nivel.Value;
      LinIt     := QrConcilia0LinIt.Value;

      if UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stUpd, 'concilia', False, [
      'DataM', 'Texto', 'Docum',
      'Valor', 'Saldo', 'Acao',
      'Conta', 'Clien', 'Forne',
      'Depto', 'Perio', 'CtrlE',
      'CartT', 'CtaLk', 'DifEr',
      'NOMEDEPTO', 'NOMECONTA', 'NOMETERCE',
      'NOMECARTE', 'TIPOCARTE'], [
      'Linha', 'Nivel', 'LinIt'], [
      DataM, Texto, Docum,
      Valor, Saldo, Acao,
      Conta, Clien, Forne,
      Depto, Perio, CtrlE,
      CartT, CtaLk, DifEr,
      NOMEDEPTO, NOMECONTA, NOMETERCE,
      NOMECARTE, TIPOCARTE], [
      Linha, Nivel, LinIt], False) then
      begin
        ReopenConcilia(QrConcilia0Linha.Value);
        EdCarteira.Enabled := False;
        CBCarteira.Enabled := False;
      end;
    end;
  end;
end;

procedure TFmConcilia.InformaraContasercriadoumlanamento1Click(Sender: TObject);
begin
  InformaAConta();
end;

function TFmConcilia.InsereConcilia(): Boolean;
begin
  Result := UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'concilia', False, [
  'DataM', 'Texto', 'Docum',
  'Valor', 'Saldo', 'Acao',
  'Conta', 'Clien', 'Forne',
  'Depto', 'Perio', 'CtrlE',
  'CartT', 'CtaLk', 'DifEr',
  'NOMEDEPTO', 'NOMECONTA', 'NOMETERCE',
  'NOMECARTE', 'TIPOCARTE'], [
  'Linha', 'Nivel', 'LinIt'], [
  F_DataM, F_Texto, F_Docum,
  F_Valor, F_Saldo, F_Acao,
  F_Conta, F_Clien, F_Forne,
  F_Depto, F_Perio, F_CtrlE,
  F_CartT, F_CtaLk, F_DifEr,
  F_NOMEDEPTO, F_NOMECONTA, F_NOMETERCE,
  F_NOMECARTE, F_TIPOCARTE], [
  F_Linha, F_Nivel, F_LinIt], False);
end;

procedure TFmConcilia.irarpontodonmerododocumento1Click(Sender: TObject);
begin
  ExecuteAcao(94);
end;

procedure TFmConcilia.QrConcilia0CalcFields(DataSet: TDataSet);
var
  Serie: String;
  Cheque: Double;
begin
//
  QrConcilia0NOMEACAO.Value   := Texto_NOMEACAO(QrConcilia0Acao.Value);
  QrConcilia0EXPLICACAO.Value := Texto_EXPLICACAO(QrConcilia0Acao.Value);
  {
  case QrConcilia0Acao.Value of
    -5: QrConcilia0NOMEACAO.Value := 'Duplicado';
    -4: QrConcilia0NOMEACAO.Value := 'J� quitado';
    -3: QrConcilia0NOMEACAO.Value := 'Pgto parc.';
    -2: QrConcilia0NOMEACAO.Value := 'Erro ctas';
    -1: QrConcilia0NOMEACAO.Value := 'Erro valor';
     0: QrConcilia0NOMEACAO.Value := '';
     1: QrConcilia0NOMEACAO.Value := 'Incluir';
     2: QrConcilia0NOMEACAO.Value := 'Baixar';
     3: QrConcilia0NOMEACAO.Value := 'Ignorar';
     5: QrConcilia0NOMEACAO.Value := 'Transferir';
     6: QrConcilia0NOMEACAO.Value := 'Multi-incl.)';
    else QrConcilia0NOMEACAO.Value := '** ??? **';
  end;
  case QrConcilia0Acao.Value of
    -5: QrConcilia0EXPLICACAO.Value := 'J� existe um lan�amento para esta linha';
    -4: QrConcilia0EXPLICACAO.Value := 'O documento foi localizado mas j� foi quitado';
    -3: QrConcilia0EXPLICACAO.Value := 'O documento foi localizado mas j� foi pago parcialmente';
    -2: QrConcilia0EXPLICACAO.Value := 'Foi localizado mais de uma conta que atende os requisitos';
    -1: QrConcilia0EXPLICACAO.Value := 'Valor de emiss�o n�o confere com informado no extrato';
     0: QrConcilia0EXPLICACAO.Value := '';
     1: QrConcilia0EXPLICACAO.Value := '';
     2: QrConcilia0EXPLICACAO.Value := '';
     3: QrConcilia0EXPLICACAO.Value := '';
     5: QrConcilia0EXPLICACAO.Value := QrConcilia0NOMECARTE.Value;
     6: QrConcilia0EXPLICACAO.Value := 'Ser�o incluidos os multi-itens desta linha (descritos abaixo)';
    else QrConcilia0EXPLICACAO.Value := 'A��o n�o implementada ou n�o descrita';
  end;
  }
  QrConcilia0Periodo_TXT.Value := dmkPF.PeriodoToMensal(QrConcilia0Perio.Value);
  MLAGeral.DecodeDocumento(QrConcilia0Docum.Value, Serie, Cheque);
  QrConcilia0SERIE.Value := Serie;
  QrConcilia0CHEQUE.Value := Cheque;

end;

procedure TFmConcilia.QrConcilia1CalcFields(DataSet: TDataSet);
var
  Serie: String;
  Cheque: Double;
begin
  QrConcilia1NOMEACAO.Value   := Texto_NOMEACAO(QrConcilia1Acao.Value);
  QrConcilia1EXPLICACAO.Value := Texto_EXPLICACAO(QrConcilia1Acao.Value);
  //
  QrConcilia1Periodo_TXT.Value := dmkPF.PeriodoToMensal(QrConcilia1Perio.Value);
  MLAGeral.DecodeDocumento(QrConcilia1Docum.Value, Serie, Cheque);
  QrConcilia1SERIE.Value := Serie;
  QrConcilia1CHEQUE.Value := Cheque;
end;

procedure TFmConcilia.DBGrid1DblClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotaoObject(PMDoubleClick, Sender, 0, 0);
end;

procedure TFmConcilia.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  Cor: TColor;
begin
  if (Column.FieldName = 'NOMEACAO') then
  begin
    if      QrConcilia0Acao.Value < 0 then Cor := clRed
    else if QrConcilia0Acao.Value = 1 then Cor := clGreen
    else if QrConcilia0Acao.Value = 2 then Cor := clBlue
    else if QrConcilia0Acao.Value = 3 then Cor := clGrayText
    else if QrConcilia0Acao.Value = 4 then Cor := clFuchsia
    else if QrConcilia0Acao.Value = 5 then Cor := clNavy
    else if QrConcilia0Acao.Value = 6 then Cor := clGreen
    else Cor := clBlack;
    with DBGrid1.Canvas do
    begin
      if Cor = clBlack then Font.Style := [] else Font.Style := [fsBold];
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'Texto') or (Column.FieldName = 'Docum') then
  begin
    if QrConcilia0Acao.Value = 0 then Cor := clRed
    else Cor := clBlack;
    with DBGrid1.Canvas do
    begin
      if Cor = clBlack then Font.Style := [] else Font.Style := [fsBold];
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
end;

procedure TFmConcilia.Fazervarreduraparaconsolidaoporlink1Click(
  Sender: TObject);
var
  Step, Linha, n, i, k, SitA, SitB, Banc, Docm, QtdLC: Integer;
  Serie, DocTxt, Docum, Doc, Txt: String;
  Diferenca, ValLC: Double;
  Localizado: Boolean;
begin
  if QrCarteirasEntiBank.Value = 0 then
  if Application.MessageBox(PChar('O banco da conta corrente selecionada n�o '+
  'possui entidade associada no seu cadastro. Os lan�amentos diretos poder�o ' +
  'ficar sem fornecedor/cliente. Para atrelar uma entidade a este banco, ' +
  'clique em cancelar nesta janela, v� ' +
  'no cadastro de entidades, cadastre uma entidade nova com o nome do banco, ' +
  'depois v� no cadastro de bancos e informe a entidade rec�m cadastrada na ' +
  '"Entidade banc�ria: (utilizado na concilia��o banc�ria)"' + #13+#10+
  'Deseja continuar a concilia��o sem cadastrar a entidade banc�ria?'), 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then Exit;
  k := 0;
  Linha := QrConcilia0Linha.Value;
  QrConcilia0.First;
  //
  PreparaSQLLocCta(1);
  //
  while not QrConcilia0.Eof do
  begin
    Update;
    Application.ProcessMessages;
    Localizado := False;
    //
    if QrConcilia0Acao.Value < 1 then
    begin
      //////////////////////////////////////////////////////////////////////////
      // Primeiro procurar pelo documento
      //////////////////////////////////////////////////////////////////////////

      DocTxt := '';
      Serie := '';
      Docum := Trim(QrConcilia0Docum.Value);
      n := Length(Docum);
      if n > 0 then
      begin
        Step := 0;
        for i := 1 to n do
        begin
          if Docum[i] in ['0'..'9'] then
          begin
            if Step < 3 then
            begin
              Step := 2;
              DocTxt := DocTxt + Docum[i];
            end else Serie := Serie + Docum[i];
          end else begin
            if Step < 2 then Step := 1 else Step := 3;
            Serie := Serie + Docum[i];
          end;
        end;
        if DocTxt <> '' then
        begin
          SitA := 0;
          SitB := 0;
          Banc := QrCarteirasCodigo.Value;
          Docm := Geral.IMV(DocTxt);
          ReopenLocLanct0(SitA, SitB, Docm, Banc, Serie, ValLC, QtdLC);
          //
          if QtdLC > 0 then
          begin
            Localizado := True;
            Diferenca := ValLC - QrConcilia0Valor.Value;
            if (Diferenca > 0.009) or (Diferenca < -0.009) then
            begin
              //
              DmodG.QrUpdPID1.SQL.Clear;
              DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FConcilia + ' SET Acao=-1, DifEr=:P0 ');
              DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:P1 AND Nivel=0');
              DmodG.QrUpdPID1.Params[00].AsFloat   := Diferenca;
              DmodG.QrUpdPID1.Params[01].AsInteger := QrConcilia0Linha.Value;
              DmodG.QrUpdPID1.ExecSQL;
              //
            end else begin
              DmodG.QrUpdPID1.SQL.Clear;
              DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FConcilia + ' SET Acao=2 ');
              DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:P0 AND Nivel=0');
              DmodG.QrUpdPID1.Params[00].AsInteger := QrConcilia0Linha.Value;
              DmodG.QrUpdPID1.ExecSQL;
              //
            end;
          end else
            // Parei aqui!! ver lentid�o
            VerificaSeEstaQuitado(0, DocTxt, Serie, Diferenca);
        end;
      end;

      //////////////////////////////////////////////////////////////////////////
      // Procurar pela conta
      //////////////////////////////////////////////////////////////////////////

      if not Localizado then
      begin
        QrLocCta.Close;
        QrLocCta.Params[00].AsString  := QrConcilia0Texto.Value;
        QrLocCta.Params[01].AsString  := QrConcilia0Docum.Value;
        QrLocCta.Params[02].AsInteger := F_CliInt;//FmCondGer.QrCondCliente.Value;
        QrLocCta.Params[03].AsInteger := QrCarteirasBanco1.Value;
        QrLocCta.Open;
        case QrLocCta.RecordCount of
          0: ; // Nada
          1:
          begin
            if QrLocCtaMultiCtas.Value = 0 then
              UpdateAcao0()
            else begin
              // Parei aqui
              FEmiData := QrConcilia0DataM.Value;
              FEmiVal  := QrConcilia0Valor.Value;
              FEmiQtd  := QrLocCtaMultiCtas.Value;
              if not VerificaSeEstaQuitado(1, DocTxt, Serie, Diferenca) then
                UpdateAcao1();
            end;
          end
          else begin
            DmodG.QrUpdPID1.SQL.Clear;
            DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FConcilia + ' SET ');
            DmodG.QrUpdPID1.SQL.Add('Conta=:P0, NOMECONTA=:P1, Acao=-2 ');
            DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:P2 AND Nivel=0');
            DmodG.QrUpdPID1.Params[00].AsInteger := QrLocCtaConta.Value;
            DmodG.QrUpdPID1.Params[01].AsString  := QrLocCtaNome.Value;
            //
            DmodG.QrUpdPID1.Params[02].AsInteger := QrConcilia0Linha.Value;
            DmodG.QrUpdPID1.ExecSQL;
          end;
        end;
      end;

      //////////////////////////////////////////////////////////////////////////
      // Desconsidera��es
      //////////////////////////////////////////////////////////////////////////

      if not Localizado then
      begin
        QrLocDsc.Close;
        QrLocDsc.Params[00].AsString  := QrConcilia0Texto.Value;
        QrLocDsc.Params[01].AsString  := QrConcilia0Docum.Value;
        QrLocDsc.Params[02].AsInteger := F_CliInt;//FmCondGer.QrCondCliente.Value;
        QrLocDsc.Params[03].AsInteger := QrCarteirasBanco1.Value;
        QrLocDsc.Open;
        if QrLocDsc.RecordCount > 0 then
        begin
          DmodG.QrUpdPID1.SQL.Clear;
          DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FConcilia + ' SET Acao=3 ');
          DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:P0 AND Nivel=0');
          DmodG.QrUpdPID1.Params[00].AsInteger := QrConcilia0Linha.Value;
          DmodG.QrUpdPID1.ExecSQL;
        end;
      end;
    end;
    if not Localizado then Inc(k,1);
    QrConcilia0.Next;
  end;

  //////////////////////////////////////////////////////////////////////////
  // Procurar pela conta com m�scara
  //////////////////////////////////////////////////////////////////////////

  PreparaSQLLocCta(2);

  if k > 1 then
  begin
    QrMascaras.Close;
    QrMascaras.Params[00].AsInteger := 1; // Procurar pela conta
    QrMascaras.Params[01].AsInteger := QrCarteirasBanco1.Value;
    QrMascaras.Params[02].AsInteger := F_CliInt; //FmCondGer.QrCondCliente.Value;
    QrMascaras.Open;
    //
    while not QrMascaras.Eof do
    begin
      Doc := QrMascarasDoc.Value;
      Txt := QrMascarasTexto.Value;
      QrDesLoc.Close;
      QrDesLoc.SQL.Clear;
      QrDesLoc.SQL.Add('SELECT Linha FROM ' + FConcilia + '');
      QrDesLoc.SQL.Add('WHERE Acao = 0 AND Nivel=0 ');
      if Pos('%', Doc) > 0 then
        QrDesLoc.SQL.Add('  AND Docum LIKE "'+Doc+'"')
      else
        QrDesLoc.SQL.Add('  AND Docum = "'+Doc+'"');
      if Pos('%', Txt) > 0 then
        QrDesLoc.SQL.Add('  AND Texto LIKE "'+Txt+'"')
      else
        QrDesLoc.SQL.Add('  AND Texto = "'+Txt+'"');
      QrDesLoc.Open;
      //
      while not QrDesLoc.Eof do
      begin
        if QrConcilia0.Locate('Linha', QrDesLocLinha.Value, []) then
        begin
          QrLocCta.Close;
          QrLocCta.Params[0].AsInteger := QrMascarasConta.Value;
          QrLocCta.Open;

          if QrLocCta.RecordCount > 0 then UpdateAcao0() else
            Application.MessageBox(PChar('A linha ' +
          IntToStr(QrDesLocLinha.Value) + ' foi localizada para link ' +
          'por conta com m�scara, mas a conta n�o foi localizada!'),
          'Aviso', MB_OK+MB_ICONWARNING);
        end else Application.MessageBox(PChar('A linha ' +
          IntToStr(QrDesLocLinha.Value) + ' n�o foi localizada para link ' +
          'por conta com m�scara!'), 'Aviso', MB_OK+MB_ICONWARNING);
        //DmodG.QrUpdPID1.Params[0].AsInteger := QrDesLocLinha.Value;
        //DmodG.QrUpdPID1.ExecSQL;
        QrDesLoc.Next;
      end;
      QrMascaras.Next;
    end;
  end;

  //////////////////////////////////////////////////////////////////////////
  // Desconsidera��es com m�scara
  //////////////////////////////////////////////////////////////////////////

  if k > 1 then
  begin
    QrMascaras.Close;
    QrMascaras.Params[00].AsInteger := 0; // Desconsidera��o
    QrMascaras.Params[01].AsInteger := QrCarteirasBanco1.Value;
    QrMascaras.Params[02].AsInteger := F_CliInt; //FmCondGer.QrCondCliente.Value;
    QrMascaras.Open;
    //
    while not QrMascaras.Eof do
    begin
      Doc := QrMascarasDoc.Value;
      Txt := QrMascarasTexto.Value;
      QrDesLoc.Close;
      QrDesLoc.SQL.Clear;
      QrDesLoc.SQL.Add('SELECT Linha FROM ' + FConcilia + '');
      QrDesLoc.SQL.Add('WHERE Acao = 0 AND Nivel=0 ');
      if Pos('%', Doc) > 0 then
        QrDesLoc.SQL.Add('  AND Docum LIKE "'+Doc+'"')
      else
        QrDesLoc.SQL.Add('  AND Docum = "'+Doc+'"');
      if Pos('%', Txt) > 0 then
        QrDesLoc.SQL.Add('  AND Texto LIKE "'+Txt+'"')
      else
        QrDesLoc.SQL.Add('  AND Texto = "'+Txt+'"');
      QrDesLoc.Open;
      //
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FConcilia + ' SET Acao=3');
      DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:P0 AND Nivel=0');
      while not QrDesLoc.Eof do
      begin
        DmodG.QrUpdPID1.Params[0].AsInteger := QrDesLocLinha.Value;
        DmodG.QrUpdPID1.ExecSQL;
        QrDesLoc.Next;
      end;
      QrMascaras.Next;
    end;
  end;



  ReopenConcilia(Linha);
  EdCarteira.Enabled := False;
  CBCarteira.Enabled := False;
  //QrConcilia0.Locate('Linha', Linha, []);
  QrConcilia0.Locate('Acao', 0, []);
end;

procedure TFmConcilia.CadastrarnovoLinkdeconta1Click(Sender: TObject);
begin
  VAR_CONTA := 0;
  if DBCheck.CriaFm(TFmContasLnkEdit, FmContasLnkEdit, afmoNegarComAviso) then
  begin
    FmContasLnkEdit.LaTipo.SQLType := stIns;
    FmContasLnkEdit.FConta    := 0;
    FmContasLnkEdit.FControle := 0;
    FmContasLnkEdit.EdTexto.Text := QrConcilia0Texto.Value;
    FmContasLnkEdit.EdDoc.Text := QrConcilia0Docum.Value;
    FmContasLnkEdit.EdCliInt.Text := FormatFloat('0', F_CliInt(*FmCondGer.QrCondCliente.Value*));
    FmContasLnkEdit.CBCliInt.KeyValue := F_CliInt;//FmCondGer.QrCondCliente.Value;
    if Geral.IMV(EdCarteira.Text) <> 0 then
    begin
      FmContasLnkEdit.EdBanco.Text := IntToStr(QrCarteirasBanco1.Value);
      FmContasLnkEdit.CBBanco.KeyValue := QrCarteirasBanco1.Value;
    end;
    FmContasLnkEdit.ShowModal;
    FmContasLnkEdit.Destroy;
  end;
end;

procedure TFmConcilia.Cadastrarnovoitemignorvel1Click(Sender: TObject);
begin
  VAR_CONTA := 0;
  if DBCheck.CriaFm(TFmContasLnkEdit, FmContasLnkEdit, afmoNegarComAviso) then
  begin
    FmContasLnkEdit.LaTipo.SQLType := stIns;
    FmContasLnkEdit.FConta    := 0;
    FmContasLnkEdit.FControle := 0;
    FmContasLnkEdit.EdTexto.Text := QrConcilia0Texto.Value;
    FmContasLnkEdit.EdDoc.Text := QrConcilia0Docum.Value;
    FmContasLnkEdit.EdCliInt.Text := FormatFloat('0', F_CliInt(*FmCondGer.QrCondCliente.Value*));
    FmContasLnkEdit.CBCliInt.KeyValue := F_CliInt;//FmCondGer.QrCondCliente.Value;
    //
    FmContasLnkEdit.EdConta.Text := '';
    FmContasLnkEdit.CBConta.KeyValue := NULL;
    FmContasLnkEdit.EdConta.Enabled := False;
    FmContasLnkEdit.CBConta.Enabled := False;
    FmContasLnkEdit.LaConta.Enabled := False;
    //
    if Geral.IMV(EdCarteira.Text) <> 0 then
    begin
      FmContasLnkEdit.EdBanco.Text := IntToStr(QrCarteirasBanco1.Value);
      FmContasLnkEdit.CBBanco.KeyValue := QrCarteirasBanco1.Value;
    end;
    FmContasLnkEdit.ShowModal;
    FmContasLnkEdit.Destroy;
  end;
end;

procedure TFmConcilia.BtCadastroClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCadastro, BtCadastro);
end;

procedure TFmConcilia.QrCarteirasAfterOpen(DataSet: TDataSet);
begin
  DmBco.BancosEstaoCadastrados(True);
end;

procedure TFmConcilia.QrConcilia0AfterOpen(DataSet: TDataSet);
begin
  BtAcao.Enabled := QrConcilia0.RecordCount > 0;
end;

procedure TFmConcilia.QrConcilia0AfterScroll(DataSet: TDataSet);
begin
  ReopenConcilia1(0);
end;

procedure TFmConcilia.QrConcilia0BeforeClose(DataSet: TDataSet);
begin
  BtAcao.Enabled := False;
end;

procedure TFmConcilia.BtAcaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAcao, BtAcao);
end;

procedure TFmConcilia.Limpa1Click(Sender: TObject);
begin
  ExecuteAcao(0);
end;

procedure TFmConcilia.LimpaVars;
begin
  F_Linha     := 0;
  F_Nivel     := 0;
  F_LinIt     := 0;
  F_Acao      := 0;
  F_Conta     := 0;
  F_Clien     := 0;
  F_Forne     := 0;
  F_Depto     := 0;
  F_Perio     := 0;
  F_CtrlE     := 0;
  F_CartT     := 0;
  F_CtaLk     := 0;
  F_TIPOCARTE := 0;
  //
  F_DataM     := 0;
  //
  F_Saldo     := 0;
  F_DifEr     := 0;
  //
  F_Texto     := '';
  F_Docum     := '';
  F_NOMEDEPTO := '';
  F_NOMECONTA := '';
  F_NOMETERCE := '';
  F_NOMECARTE := '';
end;

procedure TFmConcilia.Ignora1Click(Sender: TObject);
begin
  ExecuteAcao(3);
end;

procedure TFmConcilia.Mudaaoparainclui1Click(Sender: TObject);
begin
  ExecuteAcao(1);
end;

procedure TFmConcilia.AlteraClienteFornecedor1Click(Sender: TObject);
begin
  ExecuteAcao(98);
end;

procedure TFmConcilia.AlteraClienteFornecedor2Click(
  Sender: TObject);
begin
  ExecuteAcao(98);
end;

procedure TFmConcilia.AlteraDepto1Click(Sender: TObject);
begin
  ExecuteAcao(99);
end;

procedure TFmConcilia.AlteraDepto2Click(Sender: TObject);
begin
  ExecuteAcao(99);
end;

procedure TFmConcilia.Alteraperodomsdecompetnciadolanamento1Click(
  Sender: TObject);
begin
  ExecuteAcao(96);
end;

procedure TFmConcilia.Alteraperiodo2Click(
  Sender: TObject);
begin
  ExecuteAcao(96);
end;

procedure TFmConcilia.ExecuteAcao(Acao: Integer);
const
  Aviso = '...';
  Campo = 'Descricao';
var
  Titulo, Prompt: String;
  //
  procedure DefineAcao(Acao, Linha: Integer);
    procedure ExecuteAcao(Acao, Linha: Integer);
    begin
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FConcilia + ' SET Acao=:P0');
      DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:P1 AND Nivel=0');
      DmodG.QrUpdPID1.Params[00].AsInteger := Acao;
      DmodG.QrUpdPID1.Params[01].AsInteger := Linha;
      DmodG.QrUpdPID1.ExecSQL;
    end;
    procedure AlteraTerceiro(Linha: Integer);
    begin
      if FNewVal >= 0 then
      begin
        DmodG.QrUpdPID1.SQL.Clear;
        DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FConcilia + ' ');
        DmodG.QrUpdPID1.SQL.Add('SET Clien=:P0, NOMETERCE=:P1, Forne=0 ');
        DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:P2 AND Nivel=0');
        DmodG.QrUpdPID1.Params[00].AsInteger := FNewClien;
        DmodG.QrUpdPID1.Params[01].AsString  := FNewClienNome;
        DmodG.QrUpdPID1.Params[02].AsInteger := Linha;
        DmodG.QrUpdPID1.ExecSQL;
      end else begin
        DmodG.QrUpdPID1.SQL.Clear;
        DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FConcilia + ' SET ');
        DmodG.QrUpdPID1.SQL.Add('Clien=0, Forne=:P0, NOMEFORNE=:P1 ');
        DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:P2 AND Nivel=0');
        DmodG.QrUpdPID1.Params[00].AsInteger := FNewForne;
        DmodG.QrUpdPID1.Params[01].AsString  := FNewForneNome;
        DmodG.QrUpdPID1.Params[02].AsInteger := Linha;
        DmodG.QrUpdPID1.ExecSQL;
      end;
    end;
    procedure AlteraDepto(Linha: Integer);
    begin
    if (Uppercase(Application.Title) = 'SYNDIC')
    or (Uppercase(Application.Title) = 'SYNKER') then
    begin
        DmodG.QrUpdPID1.SQL.Clear;
        DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FConcilia + ' SET ');
        DmodG.QrUpdPID1.SQL.Add('Depto=:P0, NOMEDEPTO=:P1  ');
        DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:P2 AND Nivel=0');
        DmodG.QrUpdPID1.Params[00].AsInteger := FNewDepto;
        DmodG.QrUpdPID1.Params[01].AsString  := FNewDeptoNome;
        DmodG.QrUpdPID1.Params[02].AsInteger := Linha;
        DmodG.QrUpdPID1.ExecSQL;
      end;
    end;
    procedure ExcluiLinha(Linha: Integer);
    begin
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add(DELETE_FROM  + FConcilia + ' ');
      DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:P0 AND Nivel=0');
      DmodG.QrUpdPID1.Params[00].AsInteger := Linha;
      DmodG.QrUpdPID1.ExecSQL;
    end;
    procedure AlteraPeriodo(Linha: Integer);
    begin
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FConcilia + ' SET Perio=:P0 ');
      DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:P1 AND Nivel=0');
      DmodG.QrUpdPID1.Params[00].AsInteger := FNewPerio;
      DmodG.QrUpdPID1.Params[01].AsInteger := Linha;
      DmodG.QrUpdPID1.ExecSQL;
    end;
    procedure DefineNumeroDocumento(Linha: Integer);
    var
      I, J: Integer;
      Txt: String;
      Ini, Fim: Boolean;
      C: AnsiChar;
    begin
      J   := Length(QrConcilia0Texto.Value);
      Fim := False;
      Ini := False;
      Txt := '';
      for I := J downto 1 do
      begin
        C := QrConcilia0Texto.Value[I];
        if C in (['.', '0'..'9']) then
        begin
          if Ini = False then
            Ini := True;
          if C in (['0'..'9']) then
            Txt := C + Txt;
        end else begin
          if Ini = True then
            Fim := True;
        end;
        if Fim then Break;
      end;
      //ShowMessage(Txt);
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FConcilia + ' SET Docum=:P0 ');
      DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:P1 AND Nivel=0');
      DmodG.QrUpdPID1.Params[00].AsString  := Txt;
      DmodG.QrUpdPID1.Params[01].AsInteger := Linha;
      DmodG.QrUpdPID1.ExecSQL;
    end;
    procedure CorrigeNumeroDocumento(Linha: Integer);
    var
      I, J: Integer;
      Txt: String;
      C: AnsiChar;
    begin
      J   := Length(QrConcilia0Docum.Value);
      Txt := '';
      for I := 1 to J do
      begin
        C := QrConcilia0Docum.Value[I];
        if C in (['0'..'9']) then
          Txt := Txt + C;
      end;
      //ShowMessage(Txt);
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FConcilia + ' SET Docum=:P0 ');
      DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:P1 AND Nivel=0');
      DmodG.QrUpdPID1.Params[00].AsString  := Txt;
      DmodG.QrUpdPID1.Params[01].AsInteger := Linha;
      DmodG.QrUpdPID1.ExecSQL;
    end;
  var
    Ano, Mes, Dia: Word;
    Cancelou: Boolean;
  begin
    if Acao = 96 then
    begin
      if (FNewPerio = 0) then
      begin
        DecodeDate(Date, Ano, Mes, Dia);
        MLAGeral.EscolhePeriodo_MesEAno(TFmPeriodo, FmPeriodo, Mes, Ano, Cancelou, True, True);
        FNewPerio := ((Ano - 2000) * 12) + Mes;
      end;
    end;
    VAR_SELCOD := 0;
    if Acao = 98 then
    begin
      FNewVal := QrConcilia0Valor.Value;
      if (FNewVal < 0) and (FNewForne = 0) then
      begin
{
        if DBCheck.CriaFm(TFm SelCod, Fm SelCod, afmoNegarComAviso) then
        begin
          Fm SelCod.Caption := 'Fornecedor';
          Fm SelCod.LaPrompt.Caption := 'Fornecedor';
          Fm SelCod.QrSel.Close;
          Fm SelCod.QrSel.SQL.Clear;
          Fm SelCod.QrSel.SQL.Add('SELECT CASE WHEN Tipo=0 THEN RazaoSocial');
          Fm SelCod.QrSel.SQL.Add('ELSE Nome END Descricao, Codigo');
          Fm SelCod.QrSel.SQL.Add('FROM entidades');
          Fm SelCod.QrSel.SQL.Add('WHERE Fornece1="V" OR Fornece2="V"');
          Fm SelCod.QrSel.SQL.Add('OR Fornece3="V" OR Fornece4="V"');
          Fm SelCod.QrSel.SQL.Add('OR Fornece5="V" OR Fornece5="V"');
          Fm SelCod.QrSel.SQL.Add('ORDER BY Descricao');
          Fm SelCod.QrSel.Open;
          //
          Fm SelCod.ShowModal;
          FNewForneNome := Fm SelCod.CBSel.Text;
          Fm SelCod.Destroy;
          FNewForne := VAR_SELCOD;
        end;
}
        Titulo := 'Fornecedor';
        Prompt := 'Fornecedor:';
        FNewForne := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
        'SELECT CASE WHEN Tipo=0 THEN RazaoSocial ',
        'ELSE Nome END Descricao, Codigo ',
        'FROM entidades ',
        'WHERE Fornece1="V" OR Fornece2="V" ',
        'OR Fornece3="V" OR Fornece4="V" ',
        'OR Fornece5="V" OR Fornece6="V" ',
        'OR Fornece7="V" OR Fornece8="V" ',
        'ORDER BY Descricao ',
        ''], Dmod.MyDB, False);
        FNewForneNome := VAR_SELNOM;
      end;
      if (FNewVal > 0) and (FNewClien = 0) then
      begin
{
        if DBCheck.CriaFm(TFm SelCod, Fm SelCod, afmoNegarComAviso) then
        begin
          Fm SelCod.Caption := 'Cliente';
          Fm SelCod.LaPrompt.Caption := 'Cliente';
          Fm SelCod.QrSel.Close;
          Fm SelCod.QrSel.SQL.Clear;
          Fm SelCod.QrSel.SQL.Add('SELECT CASE WHEN Tipo=0 THEN RazaoSocial');
          Fm SelCod.QrSel.SQL.Add('ELSE Nome END Descricao, Codigo');
          Fm SelCod.QrSel.SQL.Add('FROM entidades');
          Fm SelCod.QrSel.SQL.Add('WHERE Cliente1="V" OR Cliente2="V"');
          Fm SelCod.QrSel.SQL.Add('OR Cliente3="V" OR Cliente4="V"');
          Fm SelCod.QrSel.SQL.Add('ORDER BY Descricao');
          Fm SelCod.QrSel.Open;
          //
          Fm SelCod.ShowModal;
          FNewClienNome := Fm SelCod.CBSel.Text;
          Fm SelCod.Destroy;
          FNewClien := VAR_SELCOD;
        end;
}
        Titulo := 'Cliente';
        Prompt := 'Cliente:';
        FNewClien := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
        'SELECT CASE WHEN Tipo=0 THEN RazaoSocial ',
        'ELSE Nome END Descricao, Codigo ',
        'FROM entidades ',
        'WHERE Cliente1="V" OR Cliente2="V" ',
        'OR Cliente3="V" OR Cliente4="V" ',
        'ORDER BY Descricao ',
        ''], Dmod.MyDB, False);
        FNewClienNome := VAR_SELNOM;
      end;
    end;
    if Acao = 99 then
    begin
      if (FNewDepto = 0) then
      begin
{
        if DBCheck.CriaFm(TFm SelCod, Fm SelCod, afmoNegarComAviso) then
        begin
          Fm SelCod.Caption := 'Unidade habitacional';
          Fm SelCod.LaPrompt.Caption := 'Unidade habitacional';
          Fm SelCod.QrSel.Close;
          Fm SelCod.QrSel.SQL.Clear;
          Fm SelCod.QrSel.SQL.Add('SELECT Conta Codigo, Unidade Descricao');
          Fm SelCod.QrSel.SQL.Add('FROM condimov');
          Fm SelCod.QrSel.SQL.Add('WHERE Codigo=:P0');
          Fm SelCod.QrSel.SQL.Add('ORDER BY Descricao');
          Fm SelCod.QrSel.Params[0].AsInteger := FCondominio;//FmCondGer.QrCondCodigo.Value;
          Fm SelCod.QrSel.Open;
          //
          Fm SelCod.ShowModal;
          FNewDeptoNome := Fm SelCod.CBSel.Text;
          Fm SelCod.Destroy;
          FNewDepto := VAR_SELCOD;
        end;
}
        Titulo := 'Unidade habitacional';
        Prompt := 'Unidade habitacional:';
        FNewDepto := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
        'SELECT Conta Codigo, Unidade Descricao ',
        'FROM condimov ',
        'WHERE Codigo=' + Geral.FF0(FCondominio),
        'ORDER BY Descricao ',
        ''], Dmod.MyDB, False);
        FNewDeptoNome := VAR_SELNOM;
      end;
    end;
    case Acao of
      94: CorrigeNumeroDocumento(Linha);
      95: DefineNumeroDocumento(Linha);
      96: AlteraPeriodo(Linha);
      97: ExcluiLinha(Linha);
      98: AlteraTerceiro(Linha);
      99: AlteraDepto(Linha);
      else ExecuteAcao(Acao, Linha);
    end;
  end;
var
  i: Integer;
begin
  Screen.Cursor := crHourGlass;
  FNewForne := 0;
  FNewClien := 0;
  FNewDepto := 0;
  FNewPerio := 0;
  if Acao = 97 then
  begin
    if DBGrid1.SelectedRows.Count < 2 then
    begin
      if Application.MessageBox(PChar('Confirma a exclus�o dos '+
      IntToStr(DBGrid1.SelectedRows.Count) + ' itens selecionados?'),
      'Exclus�o de Linhas', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then Exit;
    end else begin
      if Application.MessageBox('Confirma a exclus�o do item selecionado?',
      'Exclus�o de Linha', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then Exit;
    end;
  end;
  if DBGrid1.SelectedRows.Count > 0 then
  begin
    with DBGrid1.DataSource.DataSet do
    for i:= 0 to DBGrid1.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(DBGrid1.SelectedRows.Items[i]));
      DefineAcao(Acao, QrConcilia0Linha.Value);
    end;
  end else DefineAcao(Acao, QrConcilia0Linha.Value);
  //
  ReopenConcilia(QrConcilia0Linha.Value);
  Screen.Cursor := crDefault;
end;

procedure TFmConcilia.PMAcaoPopup(Sender: TObject);
begin
  AlteraClienteFornecedor1.Enabled               := QrConcilia0Acao.Value = 1;
  AlteraDepto1.Enabled                           := QrConcilia0Acao.Value = 1;
  Alteraperodomsdecompetnciadolanamento1.Enabled := QrConcilia0Acao.Value = 1;
  Selecionacontaadequada1.Enabled                := QrConcilia0Acao.Value = -2;
end;

procedure TFmConcilia.PMDoubleClickPopup(Sender: TObject);
begin
  Selecionacontaadequada3.Enabled  := QrConcilia0Acao.Value = -2;
  AlteraClienteFornecedor2.Enabled := QrConcilia0Acao.Value = 1;
  AlteraDepto2.Enabled             := QrConcilia0Acao.Value = 1;
  Alteraperiodo2.Enabled           := QrConcilia0Acao.Value = 1;
end;

procedure TFmConcilia.PMLinkarPopup(Sender: TObject);
begin
  Selecionacontaadequada2.Enabled := QrConcilia0Acao.Value = -2;
end;

procedure TFmConcilia.Excluirlinhasselecionadas1Click(Sender: TObject);
begin
  ExecuteAcao(97);
end;

procedure TFmConcilia.ExcluirTODOSitensIgnorados1Click(
  Sender: TObject);
var
  Linha: Integer;
begin
  if Application.MessageBox(PChar('Confirma a exclus�o de todos itens ignorados?'),
  'Exclus�o de Linhas Ignoradas', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    Linha := QrConcilia0Linha.Value;
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add(DELETE_FROM  + FConcilia + ' ');
    DmodG.QrUpdPID1.SQL.Add('WHERE Acao=3 AND Nivel=0');
    DmodG.QrUpdPID1.ExecSQL;
    //
    ReopenConcilia(Linha);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmConcilia.BtConciliarClick(Sender: TObject);
  function InsereLinhaAtual(Query: TmySQLQuery; Nivel: Integer): Boolean;
  var
    Continua: Boolean;
    NomeDepto: String;
  begin
    with UMyMod do
    begin
      UFinanceiro.LancamentoDefaultVARS;
      Continua          := True;
      FLAN_Data         := Geral.FDT(Query.FieldByName('DataM').AsDateTime, 1);
      FLAN_Compensado   := FLAN_Data;
      FLAN_Vencimento   := FLAN_Data;
      FLAN_Doc2         := Query.FieldByName('Docum').AsString;
      MLAGeral.DecodeDocumento(FLAN_Doc2, FLAN_SerieCH, FLAN_Documento);
      if Query.FieldByName('Valor').AsFloat < 0 then
      begin
        FLAN_Debito     := - Query.FieldByName('Valor').AsFloat;
        FLAN_Fornecedor := Query.FieldByName('Forne').AsInteger;
        if FLAN_Fornecedor = 0 then
        begin
          if Application.MessageBox(PChar('O fornecedor n�o foi definido ' +
          'para a linha ' + IntToStr(Query.FieldByName('Linha').AsInteger) + '. Deseja ' +
          'continuar assim mesmo?'), 'Pergunta',
          MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then Continua := False;
        end;
      end else begin
        FLAN_Credito := Query.FieldByName('Valor').AsFloat;
        FLAN_Cliente := Query.FieldByName('Clien').AsInteger;
        if FLAN_Cliente = 0 then
        begin
          if Application.MessageBox(PChar('O Cliente n�o foi definido ' +
          'para a linha ' + IntToStr(Query.FieldByName('Linha').AsInteger) + '. Deseja ' +
          'continuar assim mesmo?'), 'Pergunta',
          MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then Continua := False;
        end;
      end;
      FLAN_Carteira   := Geral.IMV(EdCarteira.Text);
      FLAN_Genero     := Query.FieldByName('Conta').AsInteger;
      FLAN_CliInt     := F_CliInt; //FmCondGer.QrCondCliente.Value;
      FLAN_Tipo       := 1; // Conta corrente banc�ria
      FLAN_Depto      := Query.FieldByName('Depto').AsInteger; // Unidade Habitacional no syndic / synker
      FLAN_Descricao  := CompoemHistorico(Query, Nivel);
      if Query.FieldByName('Perio').AsInteger = 0 then FLAN_Mez := '' else
      begin
        FLAN_Mez      := FormatFloat('0', MLAGeral.PeriodoToAnoMes(Query.FieldByName('Perio').AsInteger));
        FLAN_Descricao:= FLAN_Descricao + ' - ' +
          MLAGeral.MesEAnoDoPeriodo(Query.FieldByName('Perio').AsInteger);
      end;
      if FLAN_Depto > 0 then
      begin
        NomeDepto := DModG.ObtemNomeDepto(FLAN_Depto);
        if NomeDepto <> '' then
          FLAN_Descricao:= FLAN_Descricao +  ' - ' + NomeDepto;
      end;
      FLAN_Sit := 3;
      //
      if Continua then
      begin
        FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
          'Controle', VAR_LCT, VAR_LCT, 'Controle');
        if UFinanceiro.InsereLancamento() then
        begin
          DmodG.QrUpdPID1.SQL.Clear;
          DmodG.QrUpdPID1.SQL.Add(DELETE_FROM  + FConcilia + ' ');
          DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:P0 AND Nivel=:P1');
          DmodG.QrUpdPID1.Params[00].AsInteger := Query.FieldByName('Linha').AsInteger;
          DmodG.QrUpdPID1.Params[01].AsInteger := Nivel;
          DmodG.QrUpdPID1.ExecSQL;
        end;
      end;
    end;
    Result := True;
  end;
var
  Segue, Linha, SitA, SitB, Banc, QtdLC: Integer;
  Seri(*, NomeDepto*): String;
  Docm, Diferenca, ValLC: Double;
  //Continua: Boolean;
begin
  if EdCarteira.Enabled then
  begin
    Application.MessageBox(PChar('� necess�rio nova "varredura para ' +
    'consolida��o por link". Essa op��o aparece ao clicar no bot�o "Linkar"!'),
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  QrImpede.Close;
  QrImpede.Open;
  if QrImpedeItens.Value > 0 then
  begin
    Segue := Application.MessageBox(PChar('H� ' + IntToStr(QrImpedeItens.Value) +
    ' itens impedindo a execuss�o da concilia��o banc�ria. Deseja prosseguir ' +
    'desconsiderando-os?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION);
  end else Segue := ID_YES;
  if Segue <> ID_YES then Exit;
  //
  Screen.Cursor := crHourGlass;
  Linha := QrConcilia0Linha.Value;
  QrConcilia0.First;
  while not QrConcilia0.Eof do
  begin
    case QrConcilia0Acao.Value of
      1,4: InsereLinhaAtual(QrConcilia0, 0);
      2:
      begin
        SitA := 0;
        SitB := 0;
        Seri := QrConcilia0Serie.Value;
        Banc := QrCarteirasCodigo.Value;
        Docm := QrConcilia0CHEQUE.Value;
        ReopenLocLanct0(SitA, SitB, Docm, Banc, Seri, ValLC, QtdLC);
        //
        if QtdLC > 0 then
        begin
          Diferenca := ValLC - QrConcilia0Valor.Value;
          if (Diferenca > 0.009) or (Diferenca < -0.009) then
          begin
            //tem diferen�a no valor
            Application.MessageBox(PChar('N�o foi poss�vel baixar o item da ' +
            'linha ' + IntToStr(QrConcilia0Linha.Value) + '. H� uma diferen�a ' +
            'de valor: ' +Dmod.QrControleMoeda.Value + ' ' + Geral.FFT(
            Diferenca, 2, siNegativo) + '.'), 'AVISO', MB_OK+MB_ICONWARNING);
          end else begin
            //ok pode baixar
            if QuitaDocumento then
            begin
              DmodG.QrUpdPID1.SQL.Clear;
              DmodG.QrUpdPID1.SQL.Add(DELETE_FROM  + FConcilia + ' ');
              DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:P0 AND Nivel=0');
              DmodG.QrUpdPID1.Params[00].AsInteger := QrConcilia0Linha.Value;
              DmodG.QrUpdPID1.ExecSQL;
            end;
          end;
        end else begin
          SitA := 2; // Pago
          SitB := 3; // Quitado
          Seri := QrConcilia0Serie.Value;
          Banc := QrCarteirasCodigo.Value;
          Docm := QrConcilia0CHEQUE.Value;
          ReopenLocLanct0(SitA, SitB, Docm, Banc, Seri, ValLC, QtdLC);
          //
          if QtdLC > 0 then
          begin
            Diferenca := ValLC - QrConcilia0Valor.Value;
            if (Diferenca > 0.009) or (Diferenca < -0.009) then
            begin
              // pago parcial
              Application.MessageBox(PChar('N�o foi poss�vel baixar o item da ' +
              'linha ' + IntToStr(QrConcilia0Linha.Value) + '. O documento j� ' +
              'est� pago parcialmente.'), 'AVISO', MB_OK+MB_ICONWARNING);
            end else begin
              // j� quitado
              Application.MessageBox(PChar('N�o foi poss�vel baixar o item da ' +
              'linha ' + IntToStr(QrConcilia0Linha.Value) + '. O documento j� ' +
              'foi quitado.'), 'AVISO', MB_OK+MB_ICONWARNING);
            end;
          end;
        end;
      end;
      5:
      begin
        try
          UFinanceiro.CriarTransferCart(0, FQrLct(*FmPrincipal.QrLct*),
          FQrCarteiras, False, F_CliInt(*FmCondGer.QrCondCliente.Value*), 0, 0, 0);
          try
            if QrConcilia0Valor.Value < 0 then
            begin
              FmTransfer2.EdOrigem.ValueVariant  := QrCarteirasCodigo.Value;
              FmTransfer2.CBOrigem.KeyValue      := QrCarteirasCodigo.Value;
              FmTransfer2.EdDestino.ValueVariant := QrConcilia0CartT.Value;
              FmTransfer2.CBDestino.KeyValue     := QrConcilia0CartT.Value;
              FmTransfer2.EdValor.Text           := Geral.FFT(-QrConcilia0Valor.Value, 2, siPositivo);
            end else begin
              FmTransfer2.EdDestino.ValueVariant := QrCarteirasCodigo.Value;
              FmTransfer2.CBDestino.KeyValue     := QrCarteirasCodigo.Value;
              FmTransfer2.EdOrigem.ValueVariant  := QrConcilia0CartT.Value;
              FmTransfer2.CBOrigem.KeyValue      := QrConcilia0CartT.Value;
              FmTransfer2.EdValor.Text           := Geral.FFT(QrConcilia0Valor.Value, 2, siPositivo);
            end;
            FmTransfer2.TPData.Date := QrConcilia0DataM.Value;
            //
            FmTransfer2.BtConfirmaClick(Self);
            //
            DmodG.QrUpdPID1.SQL.Clear;
            DmodG.QrUpdPID1.SQL.Add(DELETE_FROM  + FConcilia + ' ');
            DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:P0 AND Nivel=0');
            DmodG.QrUpdPID1.Params[00].AsInteger := QrConcilia0Linha.Value;
            DmodG.QrUpdPID1.ExecSQL;
          finally
            FmTransfer2.Close;
          end;
        except
          Application.MessageBox(PChar('Ocorreu um erro ao realizar a ' +
          'transfer�ncia da linha ' + IntToStr(QrConcilia0Linha.Value) +
          '. Voc� poder� realiz�-la manualmente no menu de op��es do ' +
          'gerenciamento do condom�nio'), 'ERRO', MB_OK+MB_ICONERROR);
          raise;
        end;
      end;
      6:
      begin
        QrConcilia1.First;
        while not QrConcilia1.Eof do
        begin
          InsereLinhaAtual(QrConcilia1, 1);
          QrConcilia1.Next;
        end;
        DmodG.QrUpdPID1.SQL.Clear;
        DmodG.QrUpdPID1.SQL.Add(DELETE_FROM  + FConcilia + ' ');
        DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:P0 AND Nivel=0');
        DmodG.QrUpdPID1.Params[00].AsInteger := QrConcilia0Linha.Value;
        DmodG.QrUpdPID1.ExecSQL;
      end;
    end;
    QrConcilia0.Next;
  end;
  UFinanceiro.AtualizaVencimentos;
  UFinanceiro.RecalcSaldoCarteira(QrCarteirasCodigo.Value, QrCarteiras,
    True, True);
  ReopenConcilia(Linha);
  Screen.Cursor := crDefault;
  //
  Geral.WriteAppKeyLM2('Concilia\Carteira', Application.Title, Geral.IMV(EdCarteira.Text), ktInteger);
  //
  Application.MessageBox('Concilia��o finalizada!', 'Mensagem', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmConcilia.ExcluirTODOSitensDuplicados1Click(
  Sender: TObject);
var
  Linha: Integer;
begin
  if Application.MessageBox(PChar('Confirma a exclus�o de todos itens duplicados?'),
  'Exclus�o de Linhas de Itens Duplicados', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    Linha := QrConcilia0Linha.Value;
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add(DELETE_FROM  + FConcilia + ' ');
    DmodG.QrUpdPID1.SQL.Add('WHERE Acao=-5 AND Nivel=0');
    DmodG.QrUpdPID1.ExecSQL;
    //
    ReopenConcilia(Linha);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmConcilia.ExcluirTODOSitensJQuitados1Click(
  Sender: TObject);
var
  Linha: Integer;
begin
  if Application.MessageBox(PChar('Confirma a exclus�o de todos itens j� quitados?'),
  'Exclus�o de Linhas de Itens j� quitados', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    Linha := QrConcilia0Linha.Value;
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add(DELETE_FROM  + FConcilia + ' ');
    DmodG.QrUpdPID1.SQL.Add('WHERE Acao=-4 AND Nivel=0');
    DmodG.QrUpdPID1.ExecSQL;
    //
    ReopenConcilia(Linha);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmConcilia.ReopenLocLanct0(const SitA, SitB: Integer; const Doc:
  Double; const Banco: Integer;
  const SerieCH: String; var Val: Double; var Qtd: Integer);
begin
  QrLocLanct0.Close;
  QrLocLanct0.SQL.Clear;
  QrLocLanct0.SQL.Add('SELECT lan.Documento, lan.Debito, lan.Credito,');
  QrLocLanct0.SQL.Add('lan.Controle, lan.Sub, lan.Carteira, lan.Descricao,');
  QrLocLanct0.SQL.Add('lan.NotaFiscal, lan.Cliente, lan.Fornecedor,');
  QrLocLanct0.SQL.Add('lan.Account, lan.Vendedor, lan.Tipo, lan.Vencimento,');
  QrLocLanct0.SQL.Add('lan.Genero, lan.CliInt, lan.Mez');
  QrLocLanct0.SQL.Add('FROM ' + VAR_LCT + ' lan');
  QrLocLanct0.SQL.Add('LEFT JOIN carteiras car ON lan.Carteira = car.Codigo');
  QrLocLanct0.SQL.Add('WHERE car.Tipo=2');
  QrLocLanct0.SQL.Add('AND car.NotConcBco=0');
  QrLocLanct0.SQL.Add('AND lan.Sit IN (:P0, :P1)');
  QrLocLanct0.SQL.Add('AND (car.IgnorSerie=1');
  QrLocLanct0.SQL.Add('     OR lan.SerieCH=:P2)');
  QrLocLanct0.SQL.Add('AND lan.Documento=:P3');
  QrLocLanct0.SQL.Add('AND car.Banco=:P4');
  QrLocLanct0.Params[00].AsInteger := SitA;
  QrLocLanct0.Params[01].AsInteger := SitB;
  QrLocLanct0.Params[02].AsString  := SerieCH;
  QrLocLanct0.Params[03].AsFloat   := Doc;
  QrLocLanct0.Params[04].AsInteger := Banco;//QrCarteirasCodigo.Value;
  QrLocLanct0.Open;
  //
  Val := 0;
  Qtd := QrLocLanct0.RecordCount;
  while not QrLocLanct0.Eof do
  begin
    Val := Val - QrLocLanct0Debito.Value + QrLocLanct0Credito.Value;
    QrLocLanct0.Next;
  end;
end;

procedure TFmConcilia.ReopenLocLanct1(const Doc: Double; const Banco: Integer;
  const SerieCH: String; const Data: TDateTime; var Val: Double; var Qtd: Integer);
begin
  QrLocLanct1.Close;
  QrLocLanct1.SQL.Clear;
  QrLocLanct1.SQL.Add('SELECT SUM(Credito - Debito) Valor,');
  QrLocLanct1.SQL.Add('COUNT(Controle) Itens');
  QrLocLanct1.SQL.Add('FROM ' + VAR_LCT + ' lan');
  QrLocLanct1.SQL.Add('LEFT JOIN carteiras car ON lan.Carteira = car.Codigo');
  QrLocLanct1.SQL.Add('WHERE car.Codigo=:P0');
  QrLocLanct1.SQL.Add('AND (car.IgnorSerie=1');
  QrLocLanct1.SQL.Add('     OR lan.SerieCH=:P1)');
  QrLocLanct1.SQL.Add('AND lan.Documento=:P2');
  QrLocLanct1.SQL.Add('AND lan.Data=:P3');
  QrLocLanct1.Params[00].AsInteger := Banco;
  QrLocLanct1.Params[01].AsString  := SerieCH;
  QrLocLanct1.Params[02].AsFloat   := Doc;
  QrLocLanct1.Params[03].AsString  := Geral.FDT(Data, 1);
  QrLocLanct1.Open;
  //
  Val := QrLocLanct1Valor.Value;
  Qtd := QrLocLanct1Itens.Value;
end;

procedure TFmConcilia.Selecionacontaadequada1Click(Sender: TObject);
begin
  SelecionaContaAdequada();
end;

procedure TFmConcilia.Selecionacontaadequada2Click(Sender: TObject);
begin
  SelecionaContaAdequada();
end;

procedure TFmConcilia.Selecionacontaadequada3Click(Sender: TObject);
begin
  SelecionaContaAdequada();
end;

procedure TFmConcilia.SelecionaContaAdequada();
begin
  PreparaSQLLocCta(1);
  //
  QrLocCta.Params[00].AsString  := QrConcilia0Texto.Value;
  QrLocCta.Params[01].AsString  := QrConcilia0Docum.Value;
  QrLocCta.Params[02].AsInteger := F_CliInt;//FmCondGer.QrCondCliente.Value;
  QrLocCta.Params[03].AsInteger := QrCarteirasBanco1.Value;
  QrLocCta.Open;
  //
  if DBCheck.CriaFm(TFmConciliaMultiCtas, FmConciliaMultiCtas, afmoNegarComAviso) then
  begin
    FmConciliaMultiCtas.ShowModal;
    FmConciliaMultiCtas.Destroy;
  end;
end;

function TFmConcilia.QuitaDocumento: Boolean;
var
  Controle2: Integer;
  DataM, Mez: String;
begin
  try
    QrLocLanct0.First;
    while not QrLocLanct0.Eof do
    begin
      DataM := FormatDateTime(VAR_FORMATDATE, QrConcilia0DataM.Value);
      Mez :=Geral.TFT_NULL(FormatFloat('0', QrLocLanct0Mez.Value), 0, siNegativo);
      //
      {
      Dmod.QrUpdM.SQL.Clear;
      Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Sit=3, Compensado=:P0,');
      Dmod.QrUpdM.SQL.Add('DataAlt=:P4, UserAlt=:P5');
      Dmod.QrUpdM.SQL.Add('WHERE Controle=:P6 AND Sub=:P7 AND Tipo=2');
      Dmod.QrUpdM.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, QrConcilia0DataM.Value);
      Dmod.QrUpdM.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
      Dmod.QrUpdM.Params[02].AsInteger := VAR_USUARIO;
      Dmod.QrUpdM.Params[03].AsFloat   := QrLocLanct0Controle.Value;
      Dmod.QrUpdM.Params[04].AsInteger := QrLocLanct0Sub.Value;
      Dmod.QrUpdM.ExecSQL;
      }
      UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
      'Sit', 'Compensado'], ['Controle', 'Sub', 'Tipo'], [3,
      FormatDateTime(VAR_FORMATDATE, QrConcilia0DataM.Value)], [
      QrLocLanct0Controle.Value, QrLocLanct0Sub.Value, 2], True, '');
      //
      UFinanceiro.RecalcSaldoCarteira(QrLocLanct0Carteira.Value, nil, False, False);
      UFinanceiro.LancamentoDefaultVARS;
      Controle2 := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        VAR_LCT, VAR_LCT, 'Controle');
      FLAN_Tipo         := 1;
      FLAN_Sit          := 3;
      FLAN_Data         := DataM;
      FLAN_Controle     := Controle2;
      FLAN_Descricao    := QrLocLanct0Descricao.Value;
      FLAN_NotaFiscal   := QrLocLanct0NotaFiscal.Value;
      FLAN_Debito       := QrLocLanct0Debito.Value;
      FLAN_Credito      := QrLocLanct0Credito.Value;
      FLAN_Compensado   := DataM;
      FLAN_Documento    := QrConcilia0CHEQUE.Value;
      FLAN_Carteira     := Geral.IMV(EdCarteira.Text);
      FLAN_Cliente      := QrLocLanct0Cliente.Value;
      FLAN_Fornecedor   := QrLocLanct0Fornecedor.Value;
      FLAN_ID_Pgto      := QrLocLanct0Controle.Value;
      FLAN_Sub          := QrLocLanct0Sub.Value;
      FLAN_Vencimento   := FormatDateTime(VAR_FORMATDATE, QrLocLanct0Vencimento.Value);
      FLAN_CtrlIni      := QrLocLanct0Controle.Value;//CtrlIni;
      FLAN_Nivel        := 0;//Nivel;
      FLAN_Vendedor     := QrLocLanct0Vendedor.Value;
      FLAN_Account      := QrLocLanct0Account.Value;
      FLAN_SerieCH      := QrConcilia0SERIE.Value;
      FLAN_Genero       := QrLocLanct0Genero.Value;
      FLAN_CliInt       := QrLocLanct0CliInt.Value;
      FLAN_Mez          := Mez;
      //
      UFinanceiro.InsereLancamento();
      //
      QrLocLanct0.Next;
    end;
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

procedure TFmConcilia.ransferncia1Click(Sender: TObject);
begin
  TransfernciaEntreCarteiras();
end;

procedure TFmConcilia.Transferncia1Click(Sender: TObject);
begin
  TransfernciaEntreCarteiras();
end;

procedure TFmConcilia.TransfernciaEntreCarteiras();
var
  Carteira: Integer;
begin
  if DBCheck.CriaFm(TFmGetCarteira, FmGetCarteira, afmoNegarComAviso) then
  begin
    FmGetCarteira.QrSelCarteiras.Close;
    FmGetCarteira.QrSelCarteiras.SQL.Clear;
    FmGetCarteira.QrSelCarteiras.SQL.Add('SELECT car.* ');
    FmGetCarteira.QrSelCarteiras.SQL.Add('FROM carteiras car');
    FmGetCarteira.QrSelCarteiras.SQL.Add('WHERE car.ForneceI='+FormatFloat('0', QrCarteirasForneceI.Value));
    FmGetCarteira.QrSelCarteiras.SQL.Add('AND car.Codigo <> '+FormatFloat('0', QrCarteirasCodigo.Value));
    FmGetCarteira.QrSelCarteiras.SQL.Add('ORDER BY car.Nome');
    FmGetCarteira.QrSelCarteiras.Open;
    FmGetCarteira.ShowModal;
    Carteira := FmGetCarteira.FCarteira;
    FmGetCarteira.Destroy;
    if Carteira <> -1000 then
    begin
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FConcilia + ' SET Conta=-1, Acao=5, CartT=:P0 ');
      DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:P1 AND Nivel=0');
      DmodG.QrUpdPID1.Params[00].AsInteger := Carteira;
      DmodG.QrUpdPID1.Params[01].AsInteger := QrConcilia0Linha.Value;
      DmodG.QrUpdPID1.ExecSQL;
      //
      ReopenConcilia(QrConcilia0Linha.Value);
      (*EdCarteira.Enabled := False;
      CBCarteira.Enabled := False;*)
    end;
  end;
end;

procedure TFmConcilia.BitBtn1Click(Sender: TObject);
begin
  QrConcilia0.Locate('Acao', 0, []);
end;

procedure TFmConcilia.Manteritenssemerro1Click(Sender: TObject);
begin
  ExcluirNaoConciliaveis(0);
end;

procedure TFmConcilia.Nomanteritenssemerro1Click(Sender: TObject);
begin
  ExcluirNaoConciliaveis(1);
end;

procedure TFmConcilia.ExcluirNaoConciliaveis(Nivel: Integer);
var
  Linha: Integer;
begin
  if Application.MessageBox(PChar('Confirma a exclus�o de todos itens n�o ' +
  'concili�veis?'), 'Exclus�o de Linhas dos Itens n�o Concili�veis',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    Linha := QrConcilia0Linha.Value;
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add(DELETE_FROM  + FConcilia + ' ');
    DmodG.QrUpdPID1.SQL.Add('WHERE (Acao<:P0');
    DmodG.QrUpdPID1.SQL.Add('OR Acao=3) AND Nivel=0');
    DmodG.QrUpdPID1.Params[0].AsInteger := Nivel;
    DmodG.QrUpdPID1.ExecSQL;
    //
    ReopenConcilia(Linha);
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmConcilia.Altera1Click(Sender: TObject);
begin
  AlteraHistorico();
end;

procedure TFmConcilia.AlteraHistorico();
var
  Texto: String;
begin
  Texto := QrConcilia0Texto.Value;
  if InputQuery('Altera��o de Hist�rico', 'Informe o texto do novo hist�rico', Texto) then
  begin
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FConcilia + ' SET Texto=:P0 ');
    DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:P1 AND Nivel=0');
    DmodG.QrUpdPID1.Params[00].AsString  := Texto;
    DmodG.QrUpdPID1.Params[01].AsInteger := QrConcilia0Linha.Value;
    DmodG.QrUpdPID1.ExecSQL;
    //
    ReopenConcilia(QrConcilia0Linha.Value);
  end;
end;

procedure TFmConcilia.AlteraHistorico2Click(Sender: TObject);
begin
  AlteraHistorico();
end;

function TFmConcilia.CompoemHistorico(Query: TmySQLQuery; Nivel: Integer): String;
begin
  if Query.FieldByName('CtaLk').AsInteger > 0 then
  begin
    Result := '';
    if MLAGeral.IntInConjunto(1, Query.FieldByName('CtaLk').AsInteger) then
      Result := Result + Query.FieldByName('Texto').AsString;
    if MLAGeral.IntInConjunto(2, Query.FieldByName('CtaLk').AsInteger) then
    begin
      if Trim(Result) <> '' then Result := Result + ' - ';
      Result := Result + Query.FieldByName('NOMECONTA').AsString;
    end;
    if MLAGeral.IntInConjunto(4, Query.FieldByName('CtaLk').AsInteger) then
    begin
      if Trim(Result) <> '' then Result := Result + ' - ';
      Result := Result + Query.FieldByName('NOMETERCE').AsString;
    end;
  end else Result := Query.FieldByName('Texto').AsString;
{
  if Nivel = 1 then
    Result := Result + ' ' + Query.FieldByName('Texto').AsString;
}
end;

procedure TFmConcilia.frxConciliaGetValue(const VarName: String;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VAR_HORA_DATA') = 0 then Value :=
    Geral.FDT(Now(), 8)
  else if AnsiCompareText(VarName, 'VARF_BANCO') = 0 then Value :=
    QrCarteirasBanco1.Value
  else if AnsiCompareText(VarName, 'VARF_CONDOMINIO') = 0 then Value :=
    FNomeCliInt(*FmCondGer.QrCondNOMECLI.Value*)
  else if AnsiCompareText(VarName, 'VARF_CONTA_VINCUL') = 0 then Value :=
    CBCarteira.Text
end;

procedure TFmConcilia.BitBtn4Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxConcilia, 'Extrato de conta corrente');
end;

procedure TFmConcilia.UpdateAcao0();
var
  Perio, Clien, Forne, CtrlE, Acao: Integer;
  NomeCli, NomeFrn, NomeTer, DataM: String;
  Debito, Credito: Double;
begin
  if QrConcilia0Valor.Value < 0 then
  begin
    Clien := 0;
    if QrLocCtaUsaEntBank.Value = 1 then
    begin
      Forne   := QrCarteirasEntiBank.Value;
      NomeFrn := QrCarteirasNOMEBCO.Value;
    end else begin
      Forne   := QrLocCtaFornece.Value;
      //NomeFrn := QrLocCtaNome.Value;
      NomeFrn := QrLocCtaNOME_FORNECE.Value;
    end;
  end else begin
    Forne := 0;
    if QrLocCtaUsaEntBank.Value = 1 then
    begin
      Clien   := QrCarteirasEntiBank.Value;
      NomeCli := QrCarteirasNOMEBCO.Value;
    end else begin
      Clien   := QrLocCtaCliente.Value;
      //NomeCli := QrLocCtaNome.Value;
      NomeCli := QrLocCtaNOME_CLIENTE.Value;
    end;
  end;
  if QrLocCtaMensal.Value = 'V' then
    Perio := Geral.Periodo2000(QrConcilia0DataM.Value)
  else Perio := 0;
  // Verifica poss�vel duplicidade
  DataM := Geral.FDT(QrConcilia0DataM.Value, 1);
  if QrConcilia0Valor.Value < 0 then
  begin
    Debito  := - QrConcilia0Valor.Value;
    Credito := 0;
  end else begin
    Credito := QrConcilia0Valor.Value;
    Debito  := 0;
  end;
  QrDupl.Close;
  QrDupl.SQL.Clear;
  QrDupl.SQL.Add('SELECT Controle');
  QrDupl.SQL.Add('FROM ' + VAR_LCT);
  QrDupl.SQL.Add('WHERE Data=:P0');
  QrDupl.SQL.Add('AND Vencimento=:P1');
  QrDupl.SQL.Add('AND Compensado=:P2');
  QrDupl.SQL.Add('AND LEFT(Descricao, LENGTH(:P3))=:P4');
  QrDupl.SQL.Add('AND SerieCH=:P5');
  QrDupl.SQL.Add('AND Documento=:P6');
  QrDupl.SQL.Add('AND Debito=:P7');
  QrDupl.SQL.Add('AND Credito=:P8');
  QrDupl.SQL.Add('AND Carteira=:P9');
  QrDupl.Params[00].AsString  := DataM;
  QrDupl.Params[01].AsString  := DataM;
  QrDupl.Params[02].AsString  := DataM;
  QrDupl.Params[03].AsString  := QrConcilia0Texto.Value;
  QrDupl.Params[04].AsString  := QrConcilia0Texto.Value;
  QrDupl.Params[05].AsString  := QrConcilia0SERIE.Value;
  QrDupl.Params[06].AsFloat   := QrConcilia0CHEQUE.Value;
  QrDupl.Params[07].AsFloat   := Debito;
  QrDupl.Params[08].AsFloat   := Credito;
  QrDupl.Params[09].AsInteger := Geral.IMV(EdCarteira.Text);
  QrDupl.Open;
  if QrDupl.RecordCount > 0 then
  begin
    Acao  := -5;
    CtrlE := QrDuplControle.Value;
  end else begin
    Acao  := 1;
    CtrlE := 0;
  end;
  //
  if Clien <> 0 then NomeTer := NomeCli else NomeTer := NomeFrn;
  //
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FConcilia + ' SET Conta=:P0, Acao=:P1, ');
  DmodG.QrUpdPID1.SQL.Add('Clien=:P2, Forne=:P3, Perio=:P4, CtrlE=:P5, ');
  DmodG.QrUpdPID1.SQL.Add('CtaLk=:P6, NOMETERCE=:P7, NOMECONTA=:P8 ');
  DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:Pa AND Nivel=0');
  DmodG.QrUpdPID1.Params[00].AsInteger := QrLocCtaConta.Value;
  DmodG.QrUpdPID1.Params[01].AsInteger := Acao;
  DmodG.QrUpdPID1.Params[02].AsInteger := Clien;
  DmodG.QrUpdPID1.Params[03].AsInteger := Forne;
  DmodG.QrUpdPID1.Params[04].AsInteger := Perio;
  DmodG.QrUpdPID1.Params[05].AsInteger := CtrlE;
  DmodG.QrUpdPID1.Params[06].AsInteger := QrLocCtaComposHist.Value;
  DmodG.QrUpdPID1.Params[07].AsString  := NomeTer;
  DmodG.QrUpdPID1.Params[08].AsString  := QrLocCtaNome.Value;
  //
  DmodG.QrUpdPID1.Params[09].AsInteger := QrConcilia0Linha.Value;
  DmodG.QrUpdPID1.ExecSQL;
end;

procedure TFmConcilia.UpdateAcao1();
  procedure DefineDados_F(LinIt: Integer; Valor, Falta: Double);
  begin
    if QrConcilia0Valor.Value < 0 then
    begin
      F_Clien := 0;
      if QrContasLnkItsUsaEntBank.Value = 1 then
      begin
        F_Forne     := QrCarteirasEntiBank.Value;
        F_NOMETERCE := QrCarteirasNOMEBCO.Value;
      end else begin
        F_Forne     := QrContasLnkItsFornece.Value;
        F_NOMETERCE := QrContasLnkItsNOME_FORNECE.Value;
      end;
    end else begin
      F_Forne := 0;
      if QrContasLnkItsUsaEntBank.Value = 1 then
      begin
        F_Clien     := QrCarteirasEntiBank.Value;
        F_NOMETERCE := QrCarteirasNOMEBCO.Value;
      end else begin
        F_Clien     := QrContasLnkItsCliente.Value;
        F_NOMETERCE := QrContasLnkItsNOME_CLIENTE.Value;
      end;
    end;
    //
    if QrContasLnkItsMensal.Value <> 'V' then
      F_Perio := 0
    else
      F_Perio := MLAGeral.DataToPeriodo(QrConcilia0DataM.Value);
    //
    F_Linha := QrConcilia0Linha.Value;
    F_Nivel := 1; // Varios lan�tos
    F_DataM := QrConcilia0DataM.Value;
    F_LinIt := LinIt;
    F_Texto := QrConcilia0Texto.Value + ' ' + QrContasLnkItsTexto.Value;
    F_Docum := QrConcilia0Docum.Value;
    F_Valor := Valor;
    F_Saldo := Falta;
    F_Acao  := 1; //Incluir
    F_Conta := QrContasLnkItsGenero.Value;
    //F_Clien := QrContasLnkItsCliente.Value;
    //F_Forne := QrContasLnkItsFornece.Value;
    F_Depto := QrConcilia0Depto.Value;
    //F_Perio := QrConcilia0Perio.Value; Abaixo!!
    F_CtrlE := QrConcilia0CtrlE.Value;
    F_CartT := QrConcilia0CartT.Value;
    F_CtaLk := QrConcilia0CtaLk.Value;
    F_DifEr := QrConcilia0DifEr.Value;
    F_NOMEDEPTO := QrConcilia0NOMEDEPTO.Value;
    F_NOMECONTA := QrContasLnkItsNOMEGENERO.Value;
    //F_NOMETERCE := QrConcilia0NOMETERCE.Value;
    F_NOMECARTE := QrConcilia0NOMECARTE.Value;
    F_TIPOCARTE := QrConcilia0TIPOCARTE.Value;
    //
  end;
var
  Valor, Falta, Total: Double;
  Debitar: Boolean;
  ValCaption, FormCaption: String;
  WidthCaption, LinIt, Sinal: Integer;
  ValVar: Variant;
  //
begin
  LinIt := 0;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('DELETE FROM concilia WHERE Nivel=1 AND Linha=:P0');
  DModG.QrUpdPID1.Params[0].AsInteger := QrConcilia0Linha.Value;
  DModG.QrUpdPID1.ExecSQL;
  //
  QrContasLnkIts.Close;
  QrContasLnkIts.Params[0].AsInteger := QrLocCtaControle.Value;
  QrContasLnkIts.Open;
  //
  if QrContasLnkIts.RecordCount <> QrLocCtaMultiCtas.Value then
  begin
    Geral.MensagemBox('Multi itens de lan�amentos n�o conferem!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Total := QrConcilia0Valor.Value;
  Debitar := Total < 0;
  if Debitar then Sinal := -1 else Sinal := 1;
  Total := Total * Sinal;
  Falta := Total;
  //
  while not QrContasLnkIts.Eof do
  begin
    LinIt := LinIt + 1;
    Valor := 0;
    //
    LimpaVars();
    //
    // N�o executar o c�digo abaixo, pois a soma deve bater com o total!!!
    {
    // se for o �ltimo, define o valar como o restante (evitar erro de arredondamento?)
    if QrContasLnkIts.RecNo = QrContasLnkIts.RecordCount then
    begin
      Valor := Falta;
    end else begin
    }
      case QrContasLnkItsTipoCalc.Value of
        0:
        begin
          FormCaption := QrConcilia0Texto.Value + ': ' + QrConcilia0Docum.Value;
          ValCaption  := '$ conta: ' + QrContasLnkItsNOMEGENERO.Value +
          ' [Texto: ' + QrContasLnkItsTexto.Value + ']';
          WidthCaption := Length(ValCaption) * 7;
          if WidthCaption < 100 then WidthCaption := 100;
          if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble, 0, 2, 0,
          '0,01', FormatFloat('0.00', Falta), True,
          FormCaption, ValCaption, WidthCaption, ValVar) then
          begin
            Valor := Geral.DMV(ValVar);
          end else (*?? PAREI AQUI ???*);
        end;
        1:
        begin
          Valor := QrContasLnkItsFator.Value;
        end;
        2:
        begin
          Valor := Round(Total * QrContasLnkItsFator.Value) / 100;
        end;
        3:
        begin
          Valor := Round(Falta * QrContasLnkItsFator.Value) / 100;
        end;
      end;
    //end;
    Falta := Falta - Valor;
    DefineDados_F(LinIt, Valor * Sinal, Falta);
    //
    InsereConcilia();
    //
    QrContasLnkIts.Next;
  end;
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FConcilia + ' SET ');
  DmodG.QrUpdPID1.SQL.Add('Conta=0, NOMECONTA="", Perio=0, Acao=6 ');
  DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:P0 AND Nivel=0');
  //
  DmodG.QrUpdPID1.Params[00].AsInteger := QrConcilia0Linha.Value;
  DmodG.QrUpdPID1.ExecSQL;
  //
end;

function TFmConcilia.VerificaSeEstaQuitado(const Tipo: Integer; const DocTxt,
Serie: String; var Diferenca: Double): Boolean;
var
  SitA, SitB, Banc, Docm: Integer;
  ValLC: Double;
  QtdLC: Integer;
begin
  Result := False;
  SitA := 2;// Pago
  SitB := 3;// Quitado
  Banc := QrCarteirasCodigo.Value;
  Docm := Geral.IMV(DocTxt);
  case Tipo of
    0:
    begin
      ReopenLocLanct0(SitA, SitB, Docm, Banc, Serie, ValLC, QtdLC);
      Result := QtdLC > 0;
    end;
    1:
    begin
      ReopenLocLanct1(Docm, Banc, Serie, FEmiData, ValLC, QtdLC);
      Result := (QtdLC = FEmiQtd) and (ValLC = FEmiVal);
    end;
  end;
  //
  if Result then
  begin
    //Localizado := True;
    Diferenca := ValLC - QrConcilia0Valor.Value;
    if (Diferenca > 0.009) or (Diferenca < -0.009) then
    begin
      //
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FConcilia + ' SET Acao=-3, DifEr=:P0 ');
      DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:P1 AND Nivel=0');
      DmodG.QrUpdPID1.Params[00].AsFloat   := Diferenca;
      DmodG.QrUpdPID1.Params[01].AsInteger := QrConcilia0Linha.Value;
      DmodG.QrUpdPID1.ExecSQL;
      //
    end else begin
      // j� quitado
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FConcilia + ' SET Acao=-4 ');
      DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:P0 AND Nivel=0');
      DmodG.QrUpdPID1.Params[00].AsInteger := QrConcilia0Linha.Value;
      DmodG.QrUpdPID1.ExecSQL;
      //
    end;
  end;
end;

procedure TFmConcilia.Excel1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmConciliaXLSLoad, FmConciliaXLSLoad, afmoNegarComAviso) then
  begin
    FmConciliaXLSLoad.ShowModal;
    FmConciliaXLSLoad.Destroy;
  end;
end;

procedure TFmConcilia.Money20001Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmConciliaOFX, FmConciliaOFX, afmoNegarComAviso) then
  begin
    FmConciliaOFX.ShowModal;
    FmConciliaOFX.Destroy;
    FMostra := FmConciliaOFX.FMostra;
    FVarre  := FmConciliaOFX.FVarre;
  end;
end;

function TFmConcilia.Texto_EXPLICACAO(Acao: Integer): String;
begin
  case Acao of
    -5: Result := 'J� existe um lan�amento para esta linha';
    -4: Result := 'O documento foi localizado mas j� foi quitado';
    -3: Result := 'O documento foi localizado mas j� foi pago parcialmente';
    -2: Result := 'Foi localizado mais de uma conta que atende os requisitos';
    -1: Result := 'Valor de emiss�o n�o confere com informado no extrato';
     0: Result := '';
     1: Result := '';
     2: Result := '';
     3: Result := '';
     5: Result := QrConcilia0NOMECARTE.Value;
     6: Result := 'Ser�o incluidos os multi-itens desta linha (descritos abaixo)';
    else Result := 'A��o n�o implementada ou n�o descrita';
  end;
end;

function TFmConcilia.Texto_NOMEACAO(Acao: Integer): String;
begin
  case Acao of
    -5: Result := 'Duplicado';
    -4: Result := 'J� quitado';
    -3: Result := 'Pgto parc.';
    -2: Result := 'Erro ctas';
    -1: Result := 'Erro valor';
     0: Result := '';
     1: Result := 'Incluir';
     2: Result := 'Baixar';
     3: Result := 'Ignorar';
     5: Result := 'Transferir';
     6: Result := 'Multi-incl.';
    else Result := '** ??? **';
  end;
end;

procedure TFmConcilia.Timer1Timer(Sender: TObject);
begin
  if FMostra then
  begin
    FMostra := False;
    MostraConciliacao;
  end;
end;

procedure TFmConcilia.BitBtn5Click(Sender: TObject);
begin
  PnImporta.Visible  := True;
  PnConcilia.Visible := False;
end;

procedure TFmConcilia.BitBtn6Click(Sender: TObject);
//var
  //CliInt: Integer;
begin
  VAR_Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  try
    if MyObjects.CriaForm_AcessoTotal(TFmLocDefs, FmLocDefs) then
    begin
      if F_CliInt <> 0 then
      begin
        FmLocDefs.EdCliInt.Text     := IntToStr(F_CliInt);
        FmLocDefs.CBCliInt.KeyValue := F_CliInt;
        FmLocDefs.CkCliInt.Checked  := True;
      end;
      FmLocDefs.FQrLct        := FQrLct;
      FmLocDefs.FQrCarteiras  := QrCarteiras;
      FmLocDefs.FDTPDataIni   := FDTPicker;
      FmLocDefs.F_CliInt      := F_CliInt; 
      FmLocDefs.FLocSohCliInt := False;
      FmLocDefs.ShowModal;
      FmLocDefs.Destroy;
    end;
  finally
    Screen.Cursor := VAR_Cursor;
  end;
end;

procedure TFmConcilia.PreparaSQLLocCta(Tipo: Integer);
begin
  QrLocCta.Close;
  QrLocCta.SQL.Clear;
  QrLocCta.SQL.Add('SELECT DISTINCT lnk.Codigo Conta, lnk.Cliente,');
  QrLocCta.SQL.Add('lnk.Fornece, lnk.UsaEntBank, lnk.ComposHist, ');
  QrLocCta.SQL.Add('lnk.MultiCtas, lnk.Controle, con.Mensal, con.Nome, ');
  QrLocCta.SQL.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOME_CLIENTE,');
  QrLocCta.SQL.Add('IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NOME_FORNECE');
  QrLocCta.SQL.Add('FROM contaslnk lnk');
  QrLocCta.SQL.Add('LEFT JOIN contas con ON con.Codigo=lnk.Codigo');
  QrLocCta.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=lnk.Cliente');
  QrLocCta.SQL.Add('LEFT JOIN entidades frn ON frn.Codigo=lnk.Fornece');
  case Tipo of
    1:
    begin
      QrLocCta.SQL.Add('WHERE lnk.Considera=1');
      QrLocCta.SQL.Add('AND lnk.Texto = TRIM(:P0)');
      QrLocCta.SQL.Add('AND lnk.Doc = TRIM(:P1)');
      QrLocCta.SQL.Add('AND lnk.CliInt IN (0, :P2)');
      QrLocCta.SQL.Add('AND lnk.Banco IN (0, :P3)');
      //
    end;
    2:
    begin
      QrLocCta.SQL.Add('WHERE lnk.Codigo=:P0');
      //
    end;
  end;
end;

end.

