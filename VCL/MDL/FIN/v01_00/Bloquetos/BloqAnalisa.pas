unit BloqAnalisa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkGeral;

type
  TFmBloqAnalisa = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    LaTipo: TdmkLabel;
    Panel5: TPanel;
    GradeLct: TDBGrid;
    QrLct: TmySQLQuery;
    DsLct: TDataSource;
    QrLctData: TDateField;
    QrLctTIPO_TXT: TWideStringField;
    QrLctCarteira: TIntegerField;
    QrLctControle: TIntegerField;
    QrLctSub: TSmallintField;
    QrLctGenero: TIntegerField;
    QrLctDescricao: TWideStringField;
    QrLctCredito: TFloatField;
    QrLctDebito: TFloatField;
    QrLctCompensado: TDateField;
    QrLctSit: TIntegerField;
    QrLctVencimento: TDateField;
    QrLctFatID: TIntegerField;
    QrLctPago: TFloatField;
    QrLctMez: TIntegerField;
    QrLctFornecedor: TIntegerField;
    QrLctCliente: TIntegerField;
    QrLctCliInt: TIntegerField;
    QrLctForneceI: TIntegerField;
    QrLctDepto: TIntegerField;
    QrLctPagMul: TFloatField;
    QrLctPagJur: TFloatField;
    QrLctCtrlQuitPg: TIntegerField;
    QrLctNO_CARTEIRA: TWideStringField;
    QrLctNO_CONTA: TWideStringField;
    QrLctVALOR: TFloatField;
    QrLctCOMPENSA_TXT: TWideStringField;
    QrLctID_pgto: TIntegerField;
    QrLctUnidade: TWideStringField;
    QrLctMES: TWideStringField;
    QrLctNOMESIT: TWideStringField;
    QrLctTipo: TSmallintField;
    QrLctPrazo: TSmallintField;
    QrLctReparcel: TIntegerField;
    QrLctUserAlt: TIntegerField;
    QrLctDataAlt: TDateField;
    QrLctLogin: TWideStringField;
    Panel6: TPanel;
    LaOriDest: TLabel;
    DBGrid2: TDBGrid;
    QrOriDest: TmySQLQuery;
    DsOriDest: TDataSource;
    QrOriDestLogin: TWideStringField;
    QrOriDestData: TDateField;
    QrOriDestTipo: TSmallintField;
    QrOriDestPrazo: TSmallintField;
    QrOriDestTIPO_TXT: TWideStringField;
    QrOriDestCarteira: TIntegerField;
    QrOriDestControle: TIntegerField;
    QrOriDestSub: TSmallintField;
    QrOriDestReparcel: TIntegerField;
    QrOriDestGenero: TIntegerField;
    QrOriDestDescricao: TWideStringField;
    QrOriDestCredito: TFloatField;
    QrOriDestDebito: TFloatField;
    QrOriDestCompensado: TDateField;
    QrOriDestSit: TIntegerField;
    QrOriDestVencimento: TDateField;
    QrOriDestFatID: TIntegerField;
    QrOriDestPago: TFloatField;
    QrOriDestMez: TIntegerField;
    QrOriDestFornecedor: TIntegerField;
    QrOriDestCliente: TIntegerField;
    QrOriDestCliInt: TIntegerField;
    QrOriDestForneceI: TIntegerField;
    QrOriDestDepto: TIntegerField;
    QrOriDestPagMul: TFloatField;
    QrOriDestPagJur: TFloatField;
    QrOriDestCtrlQuitPg: TIntegerField;
    QrOriDestID_pgto: TIntegerField;
    QrOriDestVALOR: TFloatField;
    QrOriDestCOMPENSA_TXT: TWideStringField;
    QrOriDestNO_CARTEIRA: TWideStringField;
    QrOriDestNO_CONTA: TWideStringField;
    QrOriDestUnidade: TWideStringField;
    QrOriDestUserAlt: TIntegerField;
    QrOriDestDataAlt: TDateField;
    QrOriDestMES: TWideStringField;
    QrOriDestNOMESIT: TWideStringField;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel7: TPanel;
    SbEntiEmit: TSpeedButton;
    Label9: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Panel8: TPanel;
    Label1: TLabel;
    EdFatNum: TdmkEdit;
    QrCNAB_Lei: TmySQLQuery;
    QrCNAB_LeiCodigo: TIntegerField;
    QrCNAB_LeiNossoNum: TWideStringField;
    QrCNAB_LeiBanco: TIntegerField;
    QrCNAB_LeiValTitul: TFloatField;
    QrCNAB_LeiCarteira: TIntegerField;
    QrCNAB_LeiDiretorio: TIntegerField;
    QrCNAB_LeiArquivo: TWideStringField;
    QrCNAB_LeiQUITADATA_TXT: TWideStringField;
    DBGrid3: TDBGrid;
    DsCNAB_Lei: TDataSource;
    QrLctFatNum: TFloatField;
    QrOriDestFatNum: TFloatField;
    BtDescompensa: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbEntiEmitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrLctCalcFields(DataSet: TDataSet);
    procedure QrLctBeforeClose(DataSet: TDataSet);
    procedure QrLctAfterScroll(DataSet: TDataSet);
    procedure QrOriDestCalcFields(DataSet: TDataSet);
    procedure QrOriDestBeforeClose(DataSet: TDataSet);
    procedure QrOriDestAfterScroll(DataSet: TDataSet);
    procedure BtDescompensaClick(Sender: TObject);
    procedure QrLctAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmBloqAnalisa: TFmBloqAnalisa;

implementation

uses UnMyObjects, Module, UMySQLModule, ModuleGeral, UnInternalConsts, UnFinanceiro;

{$R *.DFM}

procedure TFmBloqAnalisa.BtDescompensaClick(Sender: TObject);
  procedure DesfazCompensacao();
  begin
    UFinanceiro.SQLInsUpd_Lct(DMod.QrUpd, stUpd, False, ['Compensado', 'Sit',
    'CtrlQuitPg'], ['Controle'], [0, 0, 0], [QrLctControle.Value], True, 'AND Tipo=2');
    {
    DMod.QrUpd.SQL.Clear;
    DMod.QrUpd.SQL.Add('UPDATE lanctos SET Compensado=0, Sit=0, ID_Pgto=0');
    DMod.QrUpd.SQL.Add('WHERE Controle=:P0');
    DMod.QrUpd.Params[0].AsInteger := QrLctControle.Value;
    DMod.QrUpd.ExecSQL;
    }
  end;
var
  I: Integer;
begin
  if GradeLct.SelectedRows.Count > 1 then
  begin
    //PB1.Max := GradeLct.SelectedRows.Count;
    with GradeLct.DataSource.DataSet do
    for i:= 0 to GradeLct.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(GradeLct.SelectedRows.Items[i]));
      DesfazCompensacao();
    end;
  end else DesfazCompensacao();
  //
  QrLct.Close;
  QrLct.Open;
end;

procedure TFmBloqAnalisa.BtOKClick(Sender: TObject);
var
  Entidade: Integer;
  //FatNum: Integer;
begin
  if not UMyMod.ObtemCodigoDeCodUsu(EdEmpresa, Entidade, 'Informe a empresa',
  'Codigo', 'FILIAL') then Exit;
  if MyObjects.FIC(EdFatNum.ValueVariant = 0, EdFatNum, 'Informe o n�mero do bloqueto') then Exit;

  // Parei aqui colocar sql na query e fazer especial para o syndic > depto=uh
  QrLct.Close;
  QrLct.Params[00].AsInteger := Entidade;
  QrLct.Params[01].AsInteger := Entidade;
  QrLct.Params[02].AsFloat   := EdFatNum.ValueVariant;
  QrLct.Open;
  //
end;

procedure TFmBloqAnalisa.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmBloqAnalisa.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmBloqAnalisa.FormCreate(Sender: TObject);
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0, EdEmpresa, CBEmpresa);
end;

procedure TFmBloqAnalisa.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmBloqAnalisa.QrLctAfterOpen(DataSet: TDataSet);
begin
  BtDescompensa.Enabled := QrLct.RecordCount > 0;
end;

procedure TFmBloqAnalisa.QrLctAfterScroll(DataSet: TDataSet);
var
  Controle: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    QrOriDest.Close;
    if QrLctID_Pgto.Value > 0 then
    begin
      Controle := QrLctID_Pgto.Value;
      LaOriDest.Caption := 'Dados de origem:';
      LaOriDest.Font.Color := clRed;
    end else
    if QrLctCtrlQuitPg.Value > 0 then
    begin
      Controle := QrLctCtrlQuitPg.Value;
      LaOriDest.Caption := 'Dados da quita��o:';
      LaOriDest.Font.Color := clBlue;
    end else begin
      // N�o precisa. est� no onclose do QrLct
      Controle := 0;
    end;
    //
    if Controle <> 0 then
    begin
      QrOriDest.Params[0].AsInteger := Controle;
      QrOriDest.Open;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmBloqAnalisa.QrLctBeforeClose(DataSet: TDataSet);
begin
  QrOriDest.Close;
  LaOriDest.Font.Color := clWindowText;
  LaOriDest.Caption := '...';
  BtDescompensa.Enabled := False;
end;

procedure TFmBloqAnalisa.QrLctCalcFields(DataSet: TDataSet);
begin
  QrLctMES.Value := dmkPF.MezToMesEAno(QrLctMez.Value);
  QrLctNOMESIT.Value := UFinanceiro.NomeSitLancto(QrLctSit.Value,
    QrLctTipo.Value, QrLctPrazo.Value, QrLctVencimento.Value,
    QrLctReparcel.Value);
end;

procedure TFmBloqAnalisa.QrOriDestAfterScroll(DataSet: TDataSet);
begin
  QrCNAB_Lei.Close;
  QrCNAB_Lei.Params[00].AsFloat   := QrLctFatNum.Value;
  QrCNAB_Lei.Params[01].AsInteger := QrLctCliInt.Value;
  QrCNAB_Lei.Params[02].AsInteger := QrOriDestCarteira.Value;
  QrCNAB_Lei.Open;
end;

procedure TFmBloqAnalisa.QrOriDestBeforeClose(DataSet: TDataSet);
begin
  QrCNAB_Lei.Close;
end;

procedure TFmBloqAnalisa.QrOriDestCalcFields(DataSet: TDataSet);
begin
  QrOriDestMES.Value := dmkPF.MezToMesEAno(QrOriDestMez.Value);
  QrOriDestNOMESIT.Value := UFinanceiro.NomeSitLancto(QrOriDestSit.Value,
    QrOriDestTipo.Value, QrOriDestPrazo.Value, QrOriDestVencimento.Value,
    QrOriDestReparcel.Value);

end;

procedure TFmBloqAnalisa.SbEntiEmitClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  DModG.CadastroDeEntidade(DModG.QrEmpresasCodigo.Value, fmcadSelecionar, fmcadSelecionar);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodUsuDeCodigo(EdEmpresa, CBEmpresa, DModG.QrEmpresas, VAR_CADASTRO);
end;

end.
