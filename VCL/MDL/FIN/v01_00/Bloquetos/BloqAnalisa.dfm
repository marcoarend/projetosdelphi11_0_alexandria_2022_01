object FmBloqAnalisa: TFmBloqAnalisa
  Left = 339
  Top = 185
  Caption = 'BLQ-ANALI-001 :: An'#225'lise de Lan'#231'amentos de Bloquetos'
  ClientHeight = 602
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 554
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 18
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Pesquisa'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object BtDescompensa: TBitBtn
      Left = 112
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Descompensa'
      Enabled = False
      TabOrder = 2
      OnClick = BtDescompensaClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'An'#225'lise de Lan'#231'amentos de Bloquetos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 924
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 925
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 620
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 506
    Align = alClient
    TabOrder = 0
    object Panel5: TPanel
      Left = 1
      Top = 101
      Width = 1006
      Height = 404
      Align = alBottom
      TabOrder = 0
      object GradeLct: TDBGrid
        Left = 1
        Top = 1
        Width = 1004
        Height = 283
        Align = alClient
        DataSource = DsLct
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Data'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TIPO_TXT'
            Title.Caption = 'TIP'
            Width = 24
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Carteira'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_CARTEIRA'
            Title.Caption = 'Descri'#231#227'o da carteira'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descricao'
            Title.Caption = 'Descri'#231#227'o'
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MES'
            Title.Caption = 'M'#234's'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VALOR'
            Title.Caption = 'Valor'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Vencimento'
            Title.Caption = 'Vencim.'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMESIT'
            Title.Caption = 'Situa'#231#227'o'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'COMPENSA_TXT'
            Title.Caption = 'Compens.'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ID_pgto'
            Title.Caption = 'ID Emiss'#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CtrlQuitPg'
            Title.Caption = 'ID Pagto'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pago'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PagJur'
            Title.Caption = '$ Juros'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PagMul'
            Title.Caption = '$ Multa'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Depto'
            Title.Caption = 'ID UH'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Unidade'
            Width = 78
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Login'
            Title.Caption = 'Login alt.'
            Width = 78
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DataAlt'
            Title.Caption = 'Data alt.'
            Width = 56
            Visible = True
          end>
      end
      object Panel6: TPanel
        Left = 1
        Top = 284
        Width = 1004
        Height = 119
        Align = alBottom
        TabOrder = 1
        object LaOriDest: TLabel
          Left = 1
          Top = 1
          Width = 17
          Height = 13
          Align = alTop
          Alignment = taCenter
          Caption = '....'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBGrid2: TDBGrid
          Left = 1
          Top = 14
          Width = 1002
          Height = 104
          Align = alClient
          DataSource = DsOriDest
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TIPO_TXT'
              Title.Caption = 'TIP'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Carteira'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CARTEIRA'
              Title.Caption = 'Descri'#231#227'o da carteira'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MES'
              Title.Caption = 'M'#234's'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VALOR'
              Title.Caption = 'Valor'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencim.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESIT'
              Title.Caption = 'Situa'#231#227'o'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COMPENSA_TXT'
              Title.Caption = 'Compens.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_pgto'
              Title.Caption = 'ID Emiss'#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CtrlQuitPg'
              Title.Caption = 'ID Pagto'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pago'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PagJur'
              Title.Caption = '$ Juros'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PagMul'
              Title.Caption = '$ Multa'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Depto'
              Title.Caption = 'ID UH'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Width = 78
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Login'
              Title.Caption = 'Login alt.'
              Width = 78
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataAlt'
              Title.Caption = 'Data alt.'
              Width = 56
              Visible = True
            end>
        end
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 100
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 496
        Height = 100
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 496
          Height = 48
          Align = alTop
          TabOrder = 0
          object SbEntiEmit: TSpeedButton
            Left = 466
            Top = 20
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbEntiEmitClick
          end
          object Label9: TLabel
            Left = 8
            Top = 4
            Width = 44
            Height = 13
            Caption = 'Empresa:'
          end
          object EdEmpresa: TdmkEditCB
            Left = 8
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBEmpresa
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 64
            Top = 20
            Width = 400
            Height = 21
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 1
            dmkEditCB = EdEmpresa
            UpdType = utYes
          end
        end
        object Panel8: TPanel
          Left = 0
          Top = 48
          Width = 496
          Height = 52
          Align = alTop
          TabOrder = 1
          object Label1: TLabel
            Left = 8
            Top = 4
            Width = 45
            Height = 13
            Caption = 'Bloqueto:'
          end
          object EdFatNum: TdmkEdit
            Left = 8
            Top = 20
            Width = 128
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
        end
      end
      object DBGrid3: TDBGrid
        Left = 496
        Top = 0
        Width = 510
        Height = 100
        Align = alClient
        DataSource = DsCNAB_Lei
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
  end
  object QrLct: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrLctAfterOpen
    BeforeClose = QrLctBeforeClose
    AfterScroll = QrLctAfterScroll
    OnCalcFields = QrLctCalcFields
    SQL.Strings = (
      'SELECT usa.Login, lan.Data, lan.Tipo, car.Prazo,'
      'ELT(lan.Tipo+1, "CXA", "EXT", "EMI") TIPO_TXT,'
      'lan.Carteira, lan.Controle, lan.Sub, lan.Reparcel,'
      'lan.Genero, lan.Descricao, lan.Credito, lan.FatNum,'
      'lan.Debito, lan.Compensado, lan.Sit, lan.Vencimento,'
      'lan.FatID, lan.Pago, lan.Mez, lan.Fornecedor,'
      'lan.Cliente, lan.CliInt, lan.ForneceI, lan.Depto,'
      'lan.PagMul, lan.PagJur, lan.CtrlQuitPg, lan.ID_pgto,'
      '(lan.Credito - lan.Debito) VALOR, IF(lan.Compensado<2,'
      '"", DATE_FORMAT(lan.Compensado, "%d/%m/%y"))'
      'COMPENSA_TXT, car.Nome NO_CARTEIRA, cnt.Nome '
      'NO_CONTA, imv.Unidade, lan.UserAlt, lan.DataAlt'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN contas cnt ON cnt.Codigo=lan.Genero'
      'LEFT JOIN condimov imv ON imv.Conta=lan.Depto'
      'LEFT JOIN senhas usa ON usa.Numero=lan.UserAlt'
      'WHERE (lan.CliInt=:P0 OR car.ForneceI=:P1)'
      'AND FatNum = :P2'
      '')
    Left = 20
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrLctLogin: TWideStringField
      FieldName = 'Login'
      Required = True
      Size = 30
    end
    object QrLctData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLctPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrLctTIPO_TXT: TWideStringField
      FieldName = 'TIPO_TXT'
      Size = 3
    end
    object QrLctCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLctSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrLctReparcel: TIntegerField
      FieldName = 'Reparcel'
    end
    object QrLctGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLctCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLctDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLctCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrLctSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLctVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrLctPago: TFloatField
      FieldName = 'Pago'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLctMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLctCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLctCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLctForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLctDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLctPagMul: TFloatField
      FieldName = 'PagMul'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLctPagJur: TFloatField
      FieldName = 'PagJur'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLctCtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
    end
    object QrLctID_pgto: TIntegerField
      FieldName = 'ID_pgto'
    end
    object QrLctVALOR: TFloatField
      FieldName = 'VALOR'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrLctCOMPENSA_TXT: TWideStringField
      FieldName = 'COMPENSA_TXT'
      Size = 10
    end
    object QrLctNO_CARTEIRA: TWideStringField
      FieldName = 'NO_CARTEIRA'
      Size = 100
    end
    object QrLctNO_CONTA: TWideStringField
      FieldName = 'NO_CONTA'
      Size = 50
    end
    object QrLctUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrLctUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLctDataAlt: TDateField
      FieldName = 'DataAlt'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Required = True
      Size = 5
      Calculated = True
    end
    object QrLctNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Calculated = True
    end
    object QrLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object DsLct: TDataSource
    DataSet = QrLct
    Left = 48
    Top = 200
  end
  object QrOriDest: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrOriDestBeforeClose
    AfterScroll = QrOriDestAfterScroll
    OnCalcFields = QrOriDestCalcFields
    SQL.Strings = (
      'SELECT usa.Login, lan.Data, lan.Tipo, car.Prazo,'
      'ELT(lan.Tipo+1, "CXA", "EXT", "EMI") TIPO_TXT,'
      'lan.Carteira, lan.Controle, lan.Sub, lan.Reparcel,'
      'lan.Genero, lan.Descricao, lan.Credito, lan.FatNum, '
      'lan.Debito, lan.Compensado, lan.Sit, lan.Vencimento,'
      'lan.FatID, lan.Pago, lan.Mez, lan.Fornecedor,'
      'lan.Cliente, lan.CliInt, lan.ForneceI, lan.Depto,'
      'lan.PagMul, lan.PagJur, lan.CtrlQuitPg, lan.ID_pgto,'
      '(lan.Credito - lan.Debito) VALOR, IF(lan.Compensado<2,'
      '"", DATE_FORMAT(lan.Compensado, "%d/%m/%y"))'
      'COMPENSA_TXT, car.Nome NO_CARTEIRA, cnt.Nome '
      'NO_CONTA, imv.Unidade, lan.UserAlt, lan.DataAlt'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN contas cnt ON cnt.Codigo=lan.Genero'
      'LEFT JOIN condimov imv ON imv.Conta=lan.Depto'
      'LEFT JOIN senhas usa ON usa.Numero=lan.UserAlt'
      'WHERE lan.Controle = :P0'
      '')
    Left = 20
    Top = 228
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOriDestLogin: TWideStringField
      FieldName = 'Login'
      Required = True
      Size = 30
    end
    object QrOriDestData: TDateField
      FieldName = 'Data'
    end
    object QrOriDestTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrOriDestPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrOriDestTIPO_TXT: TWideStringField
      FieldName = 'TIPO_TXT'
      Size = 3
    end
    object QrOriDestCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrOriDestControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOriDestSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrOriDestReparcel: TIntegerField
      FieldName = 'Reparcel'
    end
    object QrOriDestGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrOriDestDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrOriDestCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrOriDestDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrOriDestCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrOriDestSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrOriDestVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrOriDestFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrOriDestPago: TFloatField
      FieldName = 'Pago'
    end
    object QrOriDestMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrOriDestFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrOriDestCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrOriDestCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrOriDestForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrOriDestDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrOriDestPagMul: TFloatField
      FieldName = 'PagMul'
    end
    object QrOriDestPagJur: TFloatField
      FieldName = 'PagJur'
    end
    object QrOriDestCtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
    end
    object QrOriDestID_pgto: TIntegerField
      FieldName = 'ID_pgto'
    end
    object QrOriDestVALOR: TFloatField
      FieldName = 'VALOR'
    end
    object QrOriDestCOMPENSA_TXT: TWideStringField
      FieldName = 'COMPENSA_TXT'
      Size = 10
    end
    object QrOriDestNO_CARTEIRA: TWideStringField
      FieldName = 'NO_CARTEIRA'
      Size = 100
    end
    object QrOriDestNO_CONTA: TWideStringField
      FieldName = 'NO_CONTA'
      Size = 50
    end
    object QrOriDestUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrOriDestUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOriDestDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOriDestMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Required = True
      Size = 5
      Calculated = True
    end
    object QrOriDestNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Calculated = True
    end
    object QrOriDestFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object DsOriDest: TDataSource
    DataSet = QrOriDest
    Left = 48
    Top = 228
  end
  object QrCNAB_Lei: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, NossoNum, Banco, ValTitul,'
      'Carteira, Diretorio, Arquivo,  IF(QuitaData<2,'
      '"", DATE_FORMAT(QuitaData, "%d/%m/%Y")) QUITADATA_TXT  '
      'FROM cnab_lei'
      'WHERE IDNum=:P0'
      'AND Entidade=:P1'
      'AND Carteira=:P2'
      'ORDER BY Codigo DESC')
    Left = 20
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCNAB_LeiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCNAB_LeiNossoNum: TWideStringField
      FieldName = 'NossoNum'
    end
    object QrCNAB_LeiBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrCNAB_LeiValTitul: TFloatField
      FieldName = 'ValTitul'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrCNAB_LeiCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrCNAB_LeiDiretorio: TIntegerField
      FieldName = 'Diretorio'
    end
    object QrCNAB_LeiArquivo: TWideStringField
      FieldName = 'Arquivo'
    end
    object QrCNAB_LeiQUITADATA_TXT: TWideStringField
      FieldName = 'QUITADATA_TXT'
      Size = 10
    end
  end
  object DsCNAB_Lei: TDataSource
    DataSet = QrCNAB_Lei
    Left = 48
    Top = 256
  end
end
