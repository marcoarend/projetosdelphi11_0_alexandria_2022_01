object FmPrincipalImp: TFmPrincipalImp
  Left = 372
  Top = 220
  Caption = 'FIN-RELAT-007 :: Relat'#243'rios de Movimentos Financeiros'
  ClientHeight = 319
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 223
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 92
      Align = alTop
      BevelOuter = bvLowered
      TabOrder = 0
      object Label1: TLabel
        Left = 548
        Top = 48
        Width = 55
        Height = 13
        Caption = 'Data inicial:'
      end
      object Label2: TLabel
        Left = 668
        Top = 48
        Width = 48
        Height = 13
        Caption = 'Data final:'
      end
      object LaCliente: TLabel
        Left = 8
        Top = 48
        Width = 39
        Height = 13
        Caption = 'Carteira:'
      end
      object Label6: TLabel
        Left = 8
        Top = 8
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object TPIni: TDateTimePicker
        Left = 548
        Top = 64
        Width = 112
        Height = 21
        Date = 36558.516375590300000000
        Time = 36558.516375590300000000
        TabOrder = 4
      end
      object TPFim: TDateTimePicker
        Left = 664
        Top = 64
        Width = 112
        Height = 21
        Date = 37660.516439456000000000
        Time = 37660.516439456000000000
        TabOrder = 5
      end
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 24
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdEmpresaChange
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 64
        Top = 24
        Width = 713
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCarteira: TdmkEditCB
        Left = 8
        Top = 64
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBCarteira
        IgnoraDBLookupComboBox = False
      end
      object CBCarteira: TdmkDBLookupComboBox
        Left = 64
        Top = 64
        Width = 480
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCarteiras
        TabOrder = 3
        dmkEditCB = EdCarteira
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 93
      Width = 790
      Height = 129
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = 'Extrato de Quitados'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 782
          Height = 101
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object CkNF: TCheckBox
            Left = 28
            Top = 40
            Width = 97
            Height = 17
            Caption = 'Separar por N.F.'
            TabOrder = 0
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Movimento total'
        ImageIndex = 1
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 782
          Height = 101
          Align = alClient
          TabOrder = 0
          object Panel6: TPanel
            Left = 1
            Top = 1
            Width = 279
            Height = 99
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 279
              Height = 69
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Panel9: TPanel
                Left = 0
                Top = 0
                Width = 279
                Height = 69
                Align = alTop
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object RG1: TRadioGroup
                  Left = 0
                  Top = 0
                  Width = 133
                  Height = 69
                  Align = alLeft
                  Caption = ' Ordem 1: '
                  ItemIndex = 2
                  Items.Strings = (
                    'Nenhuma'
                    'Cliente/fornecedor'
                    'Conta (Plano)')
                  TabOrder = 0
                  OnClick = RG1Click
                end
                object RG2: TRadioGroup
                  Left = 133
                  Top = 0
                  Width = 146
                  Height = 69
                  Align = alClient
                  Caption = ' Ordem 2: '
                  ItemIndex = 1
                  Items.Strings = (
                    'Nenhuma'
                    'Cliente/fornecedor'
                    'Conta (Plano)')
                  TabOrder = 1
                end
              end
            end
            object Panel8: TPanel
              Left = 0
              Top = 69
              Width = 279
              Height = 30
              Align = alClient
              BevelOuter = bvLowered
              TabOrder = 1
              object Panel10: TPanel
                Left = 1
                Top = 1
                Width = 134
                Height = 28
                Align = alLeft
                TabOrder = 0
                object Ck1: TCheckBox
                  Left = 4
                  Top = 4
                  Width = 97
                  Height = 17
                  Caption = 'Agrupa'
                  TabOrder = 0
                end
              end
              object Panel11: TPanel
                Left = 135
                Top = 1
                Width = 143
                Height = 28
                Align = alClient
                TabOrder = 1
                object Ck2: TCheckBox
                  Left = 4
                  Top = 4
                  Width = 97
                  Height = 17
                  Caption = 'Agrupa'
                  TabOrder = 0
                end
              end
            end
          end
          object Panel12: TPanel
            Left = 280
            Top = 1
            Width = 501
            Height = 99
            Align = alClient
            BevelOuter = bvLowered
            ParentBackground = False
            TabOrder = 1
            object Label4: TLabel
              Left = 8
              Top = 8
              Width = 100
              Height = 13
              Caption = 'Cliente / Fornecedor:'
            end
            object Label5: TLabel
              Left = 8
              Top = 52
              Width = 31
              Height = 13
              Caption = 'Conta:'
            end
            object EdCliFor: TdmkEditCB
              Left = 8
              Top = 24
              Width = 65
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBCliFor
              IgnoraDBLookupComboBox = False
            end
            object CBCliFor: TdmkDBLookupComboBox
              Left = 76
              Top = 24
              Width = 417
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsCliente1
              TabOrder = 1
              dmkEditCB = EdCliFor
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdConta: TdmkEditCB
              Left = 8
              Top = 68
              Width = 65
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBConta
              IgnoraDBLookupComboBox = False
            end
            object CBConta: TdmkDBLookupComboBox
              Left = 76
              Top = 68
              Width = 417
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsContas
              TabOrder = 3
              dmkEditCB = EdConta
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 271
    Width = 792
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtImprime: TBitBtn
      Tag = 5
      Left = 8
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Imprime'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtImprimeClick
    end
    object BtSair: TBitBtn
      Tag = 13
      Left = 692
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSairClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Caption = 'Relat'#243'rios de Movimentos Financeiros'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 790
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object QrInicial: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Inicial) Inicial '
      'FROM carteiras'
      'WHERE Tipo<2'
      'AND Codigo=:P0')
    Left = 16
    Top = 8
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrInicialInicial: TFloatField
      FieldName = 'Inicial'
      Origin = 'DBMMONEY.carteiras.Inicial'
    end
  end
  object QrAnterior: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Credito) Credito,'
      'SUM(Debito) Debito,'
      'SUM(Credito - Debito) Saldo'
      'FROM lanctos'
      'WHERE Data<:P0'
      'AND (Tipo=1 OR (Tipo=0 AND Vencimento<:P1))'
      'AND Carteira=:P2'
      '')
    Left = 48
    Top = 8
    ParamData = <
      item
        DataType = ftDate
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrAnteriorCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrAnteriorDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrAnteriorSaldo: TFloatField
      FieldName = 'Saldo'
    end
  end
  object QrExtrato: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrExtratoCalcFields
    SQL.Strings = (
      'SELECT la.*, co.Nome NOMECONTA, ca.Nome NOMECARTEIRA,'
      'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial '
      'ELSE cl.Nome END NOMECLIENTE,'
      'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial '
      'ELSE fo.Nome END NOMEFORNECEDOR'
      'FROM lanctos la'
      'LEFT JOIN contas    co ON co.Codigo=la.Genero'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'WHERE la.Data BETWEEN :P0 AND :P1'
      'AND (la.Tipo=1 OR (la.Tipo=0 AND Vencimento<=:P2))'
      'AND la.Carteira=:P3'
      'ORDER BY Data, NotaFiscal, Carteira, Controle')
    Left = 80
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrExtratoData: TDateField
      FieldName = 'Data'
      Origin = 'DBMMONEY.lanctos.Data'
    end
    object QrExtratoTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'DBMMONEY.lanctos.Tipo'
    end
    object QrExtratoCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'DBMMONEY.lanctos.Carteira'
    end
    object QrExtratoSub: TSmallintField
      FieldName = 'Sub'
      Origin = 'DBMMONEY.lanctos.Sub'
    end
    object QrExtratoAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      Origin = 'DBMMONEY.lanctos.Autorizacao'
    end
    object QrExtratoGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'DBMMONEY.lanctos.Genero'
    end
    object QrExtratoDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'DBMMONEY.lanctos.Descricao'
      Size = 128
    end
    object QrExtratoNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'DBMMONEY.lanctos.NotaFiscal'
    end
    object QrExtratoDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'DBMMONEY.lanctos.Debito'
    end
    object QrExtratoCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'DBMMONEY.lanctos.Credito'
    end
    object QrExtratoCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'DBMMONEY.lanctos.Compensado'
    end
    object QrExtratoDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'DBMMONEY.lanctos.Documento'
    end
    object QrExtratoSit: TIntegerField
      FieldName = 'Sit'
      Origin = 'DBMMONEY.lanctos.Sit'
    end
    object QrExtratoVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'DBMMONEY.lanctos.Vencimento'
    end
    object QrExtratoLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMMONEY.lanctos.Lk'
    end
    object QrExtratoFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'DBMMONEY.lanctos.FatID'
    end
    object QrExtratoFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'DBMMONEY.lanctos.FatParcela'
    end
    object QrExtratoID_Sub: TSmallintField
      FieldName = 'ID_Sub'
      Origin = 'DBMMONEY.lanctos.ID_Sub'
    end
    object QrExtratoFatura: TWideStringField
      FieldName = 'Fatura'
      Origin = 'DBMMONEY.lanctos.Fatura'
      FixedChar = True
      Size = 128
    end
    object QrExtratoBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'DBMMONEY.lanctos.Banco'
    end
    object QrExtratoLocal: TIntegerField
      FieldName = 'Local'
      Origin = 'DBMMONEY.lanctos.Local'
    end
    object QrExtratoCartao: TIntegerField
      FieldName = 'Cartao'
      Origin = 'DBMMONEY.lanctos.Cartao'
    end
    object QrExtratoLinha: TIntegerField
      FieldName = 'Linha'
      Origin = 'DBMMONEY.lanctos.Linha'
    end
    object QrExtratoOperCount: TIntegerField
      FieldName = 'OperCount'
      Origin = 'DBMMONEY.lanctos.OperCount'
    end
    object QrExtratoLancto: TIntegerField
      FieldName = 'Lancto'
      Origin = 'DBMMONEY.lanctos.Lancto'
    end
    object QrExtratoPago: TFloatField
      FieldName = 'Pago'
      Origin = 'DBMMONEY.lanctos.Pago'
    end
    object QrExtratoMez: TIntegerField
      FieldName = 'Mez'
      Origin = 'DBMMONEY.lanctos.Mez'
    end
    object QrExtratoFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Origin = 'DBMMONEY.lanctos.Fornecedor'
    end
    object QrExtratoCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'DBMMONEY.lanctos.Cliente'
    end
    object QrExtratoMoraDia: TFloatField
      FieldName = 'MoraDia'
      Origin = 'DBMMONEY.lanctos.MoraDia'
    end
    object QrExtratoMulta: TFloatField
      FieldName = 'Multa'
      Origin = 'DBMMONEY.lanctos.Multa'
    end
    object QrExtratoProtesto: TDateField
      FieldName = 'Protesto'
      Origin = 'DBMMONEY.lanctos.Protesto'
    end
    object QrExtratoDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'DBMMONEY.lanctos.DataCad'
    end
    object QrExtratoDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'DBMMONEY.lanctos.DataAlt'
    end
    object QrExtratoUserCad: TSmallintField
      FieldName = 'UserCad'
      Origin = 'DBMMONEY.lanctos.UserCad'
    end
    object QrExtratoUserAlt: TSmallintField
      FieldName = 'UserAlt'
      Origin = 'DBMMONEY.lanctos.UserAlt'
    end
    object QrExtratoNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'DBMMONEY.contas.Nome'
      Size = 128
    end
    object QrExtratoNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
    object QrExtratoControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrExtratoID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrExtratoNOMERELACIONADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMERELACIONADO'
      Size = 100
      Calculated = True
    end
    object QrExtratoNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrExtratoNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      Size = 100
    end
    object QrExtratoQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object QrMovimento: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrMovimentoCalcFields
    SQL.Strings = (
      'SELECT co.Nome NOMECONTA,'
      
        'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial ELSE cl.Nome END NOMECLI' +
        'ENTE,'
      
        'CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial ELSE ci.Nome END NOMECLI' +
        'INT,'
      
        'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ELSE fo.Nome END NOMEFOR' +
        'NECEDOR, '
      
        'CASE WHEN cl.Tipo=0 THEN cl.EContato ELSE cl.PContato END CLCont' +
        'ato,'
      
        'CASE WHEN ci.Tipo=0 THEN ci.EContato ELSE ci.PContato END CICont' +
        'ato,'
      
        'CASE WHEN fo.Tipo=0 THEN fo.EContato ELSE fo.PContato END FOCont' +
        'ato, '
      'CASE WHEN cl.Tipo=0 THEN cl.ETe1 ELSE cl.PTe1 END CLTel1,'
      'CASE WHEN ci.Tipo=0 THEN ci.ETe1 ELSE ci.PTe1 END CITel1,'
      'CASE WHEN fo.Tipo=0 THEN fo.ETe1 ELSE fo.PTe1 END FOTel1, '
      'la.*'
      'FROM lanctos la'
      'LEFT JOIN contas    co ON co.Codigo=la.Genero'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades ci ON ci.Codigo=la.CliInt'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'WHERE la.Data BETWEEN :P0 AND :P1'
      'AND co.Codigo=la.Genero'
      'AND la.Carteira=:P2'
      'ORDER BY la.CliInt, la.Cliente, la.Fornecedor, la.Data')
    Left = 132
    Top = 284
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrMovimentoNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrMovimentoNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrMovimentoNOMECLIINT: TWideStringField
      FieldName = 'NOMECLIINT'
      Size = 100
    end
    object QrMovimentoNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      Size = 100
    end
    object QrMovimentoData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrMovimentoTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrMovimentoCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrMovimentoControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrMovimentoSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrMovimentoAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrMovimentoGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrMovimentoDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrMovimentoNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrMovimentoDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrMovimentoCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrMovimentoCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrMovimentoDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrMovimentoSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrMovimentoVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrMovimentoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMovimentoFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMovimentoFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrMovimentoID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrMovimentoID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrMovimentoFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrMovimentoBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrMovimentoLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrMovimentoCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrMovimentoLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrMovimentoOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrMovimentoLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrMovimentoPago: TFloatField
      FieldName = 'Pago'
    end
    object QrMovimentoMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrMovimentoFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrMovimentoCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrMovimentoMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrMovimentoMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrMovimentoProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrMovimentoDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrMovimentoCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrMovimentoNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrMovimentoVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrMovimentoAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrMovimentoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMovimentoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMovimentoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMovimentoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMovimentoFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrMovimentoICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrMovimentoICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrMovimentoDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrMovimentoCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrMovimentoTERCEIRO: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'TERCEIRO'
      Calculated = True
    end
    object QrMovimentoNOMETERCEIRO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETERCEIRO'
      Size = 100
      Calculated = True
    end
    object QrMovimentoCLContato: TWideStringField
      FieldName = 'CLContato'
      Size = 60
    end
    object QrMovimentoCIContato: TWideStringField
      FieldName = 'CIContato'
      Size = 60
    end
    object QrMovimentoFOContato: TWideStringField
      FieldName = 'FOContato'
      Size = 60
    end
    object QrMovimentoCLTel1: TWideStringField
      FieldName = 'CLTel1'
    end
    object QrMovimentoCITel1: TWideStringField
      FieldName = 'CITel1'
    end
    object QrMovimentoFOTel1: TWideStringField
      FieldName = 'FOTel1'
    end
    object QrMovimentoTETel1: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TETel1'
      Calculated = True
    end
    object QrMovimentoTEContato: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEContato'
      Size = 100
      Calculated = True
    end
    object QrMovimentoTXTCITel1: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTCITel1'
      Size = 40
      Calculated = True
    end
    object QrMovimentoTXTTETel1: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTTETel1'
      Size = 40
      Calculated = True
    end
    object QrMovimentoNOMETECONTATO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETECONTATO'
      Size = 100
      Calculated = True
    end
    object QrMovimentoVALOR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALOR'
      Calculated = True
    end
    object QrMovimentoFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrMovimentoKGT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'KGT'
      Calculated = True
    end
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Codigo, car.Nome, car.ForneceI,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT'
      'FROM carteiras car'
      'LEFT JOIN entidades ent ON ent.Codigo=car.ForneceI'
      'WHERE car.ForneceI=:P0'
      'ORDER BY car.Nome')
    Left = 696
    Top = 66
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrCarteirasNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 724
    Top = 66
  end
  object QrClienteI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'WHERE Cliente2="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 696
    Top = 146
    object QrClienteICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClienteINOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClienteI: TDataSource
    DataSet = QrClienteI
    Left = 724
    Top = 146
  end
  object QrCliente1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'CASE WHEN tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 696
    Top = 190
    object QrCliente1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliente1NOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsCliente1: TDataSource
    DataSet = QrCliente1
    Left = 724
    Top = 190
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'ORDER BY Nome')
    Left = 696
    Top = 234
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 724
    Top = 234
  end
  object frxExtrato: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.741561411990000000
    ReportOptions.LastChange = 39720.741561411990000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure ReportTitle1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  PageHeader1.Visible := False;                                 ' +
        '                           '
      'end;'
      ''
      'procedure ColumnHeader1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      
        '  PageHeader1.Visible := True;                                  ' +
        '                          '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxExtratoGetValue
    Left = 112
    Top = 36
    Datasets = <
      item
        DataSet = frxDsCarteiras
        DataSetName = 'frxDsCarteiras'
      end
      item
      end
      item
        DataSet = frxDsExtrato
        DataSetName = 'frxDsExtrato'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 96.378006460000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo18: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo19: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'EXTRATO DE CARTEIRA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCarteiras."NO_ENT"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCarteiras."Codigo"] - [frxDsCarteiras."Nome"]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Top = 75.590600000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Per'#237'odo: ')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 52.913420000000000000
          Top = 75.590600000000000000
          Width = 442.205010000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[PERIODO]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 495.118430000000000000
          Top = 75.590600000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Saldo inicial:')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 566.929500000000000000
          Top = 75.590600000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[INICIAL]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 23.779530000000000000
        Top = 411.968770000000000000
        Width = 680.315400000000000000
        object Memo9: TfrxMemoView
          Left = 261.338590000000000000
          Top = 1.889763780000000000
          Width = 64.000000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo em')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 325.779530000000000000
          Top = 1.889763780000000000
          Width = 256.000000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsExtrato."Data"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 584.425170000000000000
          Top = 1.889763780000000000
          Width = 96.000000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SALDODIA]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 21.779530000000000000
        Top = 328.819110000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsExtrato."Data"'
        object Memo1: TfrxMemoView
          Left = 5.338590000000000000
          Width = 404.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsExtrato."Data"]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 13.228346460000000000
        Top = 374.173470000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsExtrato
        DataSetName = 'frxDsExtrato'
        RowCount = 0
        object Memo13: TfrxMemoView
          Left = -0.000002440000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Data'
          DataSet = frxDsExtrato
          DataSetName = 'frxDsExtrato'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[frxDsExtrato."Data"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 34.015745590000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsExtrato
          DataSetName = 'frxDsExtrato'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39', <frxDsExtrato."Documento">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 585.826783860000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39', <frxDsExtrato."NotaFiscal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo53: TfrxMemoView
          Left = 241.889761340000000000
          Width = 188.976377950000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Descricao'
          DataSet = frxDsExtrato
          DataSetName = 'frxDsExtrato'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtrato."Descricao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          Left = 619.842519690000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SALDO]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 71.811021180000000000
          Width = 132.283464570000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'NOMECONTA'
          DataSet = frxDsExtrato
          DataSetName = 'frxDsExtrato'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtrato."NOMECONTA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 430.866139290000000000
          Width = 154.960629921260000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'NOMERELACIONADO'
          DataSet = frxDsExtrato
          DataSetName = 'frxDsExtrato'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtrato."NOMERELACIONADO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 204.094485750000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Qtde'
          DataSet = frxDsExtrato
          DataSetName = 'frxDsExtrato'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtrato."Qtde"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 90.708720000000000000
        Top = 139.842610000000000000
        Visible = False
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo38: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line8: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'EXTRATO DE CARTEIRA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCarteiras."NO_ENT"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCarteiras."Codigo"] - [frxDsCarteiras."Nome"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Top = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo:  [PERIODO]')
          ParentFont = False
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        Height = 14.456546460000000000
        Top = 253.228510000000000000
        Width = 680.315400000000000000
        object Memo29: TfrxMemoView
          Left = -0.000002440000000000
          Top = 1.228200000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 34.015745590000000000
          Top = 1.228200000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'Cheque')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 241.889761340000000000
          Top = 1.228200000000000000
          Width = 188.976377950000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 585.826783860000000000
          Top = 1.228200000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 619.842519690000000000
          Top = 1.228200000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 71.811021180000000000
          Top = 1.228200000000000000
          Width = 132.283464570000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 430.866139290000000000
          Top = 1.228200000000000000
          Width = 154.960629920000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente / Fornecedor')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 204.094485750000000000
          Top = 1.228200000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantid.')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 15.118120000000000000
        Top = 495.118430000000000000
        Width = 680.315400000000000000
        object Memo5: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object frxDsExtrato: TfrxDBDataset
    UserName = 'frxDsExtrato'
    CloseDataSource = False
    DataSet = QrExtrato
    BCDToCurrency = False
    Left = 140
    Top = 36
  end
  object frxExtrato2: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.748834988400000000
    ReportOptions.LastChange = 39720.748834988400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure ReportTitle1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  PageHeader1.Visible := False;                                 ' +
        '                              '
      'end;'
      ''
      'procedure ColumnHeader1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  PageHeader1.Visible := True;                                  ' +
        '                             '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxExtratoGetValue
    Left = 168
    Top = 36
    Datasets = <
      item
        DataSet = frxDsCarteiras
        DataSetName = 'frxDsCarteiras'
      end
      item
      end
      item
        DataSet = frxDsExtrato
        DataSetName = 'frxDsExtrato'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object GroupFooter1: TfrxGroupFooter
        Height = 20.000000000000000000
        Top = 472.441250000000000000
        Width = 680.315400000000000000
        object Memo41: TfrxMemoView
          Left = 333.338590000000000000
          Width = 244.661410000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo em: [frxDsExtrato."Data"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 584.425170000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SALDODIA]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Top = 321.260050000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsExtrato."Data"'
        object Line8: TfrxLineView
          Left = -11.338590000000000000
          Height = 17.007874020000000000
          ShowHint = False
          Frame.Style = fsDot
          Frame.Typ = [ftLeft]
        end
      end
      object MasterData1: TfrxMasterData
        Height = 13.228346460000000000
        Top = 385.512060000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'MasterData1OnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsExtrato
        DataSetName = 'frxDsExtrato'
        RowCount = 0
        object Memo1: TfrxMemoView
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Data'
          DataSet = frxDsExtrato
          DataSetName = 'frxDsExtrato'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[frxDsExtrato."Data"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Left = 34.015748030000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsExtrato
          DataSetName = 'frxDsExtrato'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39', <frxDsExtrato."Documento">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 585.826786300000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39', <frxDsExtrato."NotaFiscal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 241.889763780000000000
          Width = 188.976377950000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Descricao'
          DataSet = frxDsExtrato
          DataSetName = 'frxDsExtrato'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtrato."Descricao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          Left = 619.842522130000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SALDO]')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          Left = 71.811023620000000000
          Width = 132.283464570000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'NOMECONTA'
          DataSet = frxDsExtrato
          DataSetName = 'frxDsExtrato'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtrato."NOMECONTA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          Left = 430.866141730000000000
          Width = 154.960629920000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'NOMERELACIONADO'
          DataSet = frxDsExtrato
          DataSetName = 'frxDsExtrato'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtrato."NOMERELACIONADO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          Left = 204.094488190000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Qtde'
          DataSet = frxDsExtrato
          DataSetName = 'frxDsExtrato'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtrato."Qtde"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        Height = 14.456546460000000000
        Top = 245.669450000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'ColumnHeader1OnBeforePrint'
        object Memo10: TfrxMemoView
          Top = 1.228200000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 34.015748030000000000
          Top = 1.228200000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'Cheque')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 241.889763780000000000
          Top = 1.228200000000000000
          Width = 188.976377950000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 585.826786300000000000
          Top = 1.228200000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 619.842522130000000000
          Top = 1.228200000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 71.811023620000000000
          Top = 1.228200000000000000
          Width = 132.283464570000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 430.866141730000000000
          Top = 1.228200000000000000
          Width = 154.960629920000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cliente / Fornecedor')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 204.094488190000000000
          Top = 1.228200000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantid.')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        Height = 24.566929130000000000
        Top = 423.307360000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'GroupFooter2OnBeforePrint'
        object Memo15: TfrxMemoView
          Left = 585.118120000000000000
          Top = 3.590290000000000000
          Width = 95.338590000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsExtrato."Debito">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 393.559060000000000000
          Top = 3.590289999999980000
          Width = 28.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'N.F. ')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 425.559060000000000000
          Top = 3.590289999999980000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsExtrato."NotaFiscal"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 545.118120000000000000
          Top = 3.590290000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Valor:')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        Height = 17.007874020000000000
        Top = 343.937230000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'GroupHeader2OnBeforePrint'
        Condition = 'frxDsExtrato."NotaFiscal"'
        object Memo12: TfrxMemoView
          Width = 680.314960630000000000
          Height = 17.007874015748000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'N.F.: [FormatFloat('#39'000000'#39', <frxDsExtrato."NotaFiscal">)]')
          ParentFont = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        Height = 90.708720000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'ReportTitle1OnBeforePrint'
        object Shape1: TfrxShapeView
          Left = 0.000002440000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo5: TfrxMemoView
          Left = 7.559062440000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line13: TfrxLineView
          Left = 0.000002440000000000
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo6: TfrxMemoView
          Left = 7.559062440000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 574.488562440000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 105.826842440000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'EXTRATO DE CARTEIRA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 0.000002440000000000
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCarteiras."NO_ENT"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 0.000002440000000000
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCarteiras."Codigo"] - [frxDsCarteiras."Nome"]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 0.000002440000000000
          Top = 75.590600000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Per'#237'odo: ')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 52.913422440000000000
          Top = 75.590600000000000000
          Width = 442.205010000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[PERIODO]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 495.118432440000000000
          Top = 75.590600000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Saldo inicial:')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 566.929502440000000000
          Top = 75.590600000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[INICIAL]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 90.708720000000000000
        Top = 132.283550000000000000
        Visible = False
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Left = 0.000002440000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo38: TfrxMemoView
          Left = 7.559062440000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line14: TfrxLineView
          Left = 0.000002440000000000
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 7.559062440000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488562440000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 105.826842440000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'EXTRATO DE CARTEIRA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 0.000002440000000000
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCarteiras."NO_ENT"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 0.000002440000000000
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCarteiras."Codigo"] - [frxDsCarteiras."Nome"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 0.000002440000000000
          Top = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo:  [PERIODO]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 18.897637800000000000
        Top = 551.811380000000000000
        Width = 680.315400000000000000
        object Memo3: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897637795275590000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object frxDsMovimento: TfrxDBDataset
    UserName = 'frxDsMovimento'
    CloseDataSource = False
    DataSet = QrMovimento
    BCDToCurrency = False
    Left = 188
    Top = 284
  end
  object frxDsCarteiras: TfrxDBDataset
    UserName = 'frxDsCarteiras'
    CloseDataSource = False
    DataSet = QrCarteiras
    BCDToCurrency = False
    Left = 668
    Top = 64
  end
  object frxMovimento1: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.917146261600000000
    ReportOptions.LastChange = 40390.429861180560000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure RT1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  PH1.Visible := False;                                         ' +
        '   '
      'end;'
      ''
      'procedure GH1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  PH1.Visible := True;                                          '
      'end;'
      ''
      'begin'
      
        '  MeuMemo11.Text   := <VAR_MEMO11>;                             ' +
        '                    '
      
        '  MeuMemo12.Text   := <VAR_MEMO12>;                             ' +
        '                    '
      
        '  MeuMemo13.Text   := <VAR_MEMO13>;                             ' +
        '                    '
      '  //        '
      
        '  MeuMemo21.Text   := <VAR_MEMO21>;                             ' +
        '                    '
      
        '  MeuMemo22.Text   := <VAR_MEMO22>;                             ' +
        '                    '
      
        '  MeuMemo23.Text   := <VAR_MEMO23>;                             ' +
        '                    '
      '  //        '
      '  MemoX.Text := <VAR_DEFMEMO2>;'
      '  MemoY.Text := <VAR_DEFMEMO1>;'
      '    '
      '// GH1'
      '  GH1.Visible := <VAR_SHOW1>;'
      '  GH1.Condition := <VAR_CONDITION1>;'
      '  //if <VAR_SHOW2> then GH1.Height := 48 else GH1.Height := 68;'
      '// GH2'
      '  GH2.Visible := <VAR_SHOW2>;'
      '  GH2.Condition := <VAR_CONDITION2>;'
      'end.')
    OnGetValue = frxExtratoGetValue
    Left = 160
    Top = 284
    Datasets = <
      item
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsMovimento
        DataSetName = 'frxDsMovimento'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object RT1: TfrxReportTitle
        Height = 113.385900000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'RT1OnBeforePrint'
        object Shape1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo32: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo38: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'MOVIMENTO DE CARTEIRA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          Top = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Periodo: [PERIODO]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Empresa: [VAR_NOMECLIINT]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Top = 94.488250000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente/fornecedor: [VAR_NOMECLIFOR]')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Carteira: [VAR_NOMECARTEIRA]')
          ParentFont = False
        end
      end
      object PH1: TfrxPageHeader
        Height = 13.228346460000000000
        Top = 154.960730000000000000
        Visible = False
        Width = 680.315400000000000000
        object Memo28: TfrxMemoView
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.LeftLine.Width = 1.000000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 34.015743150000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'Doc.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 215.433080630000000000
          Width = 226.771653540000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 442.204734170000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Left = 612.283513380000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          Frame.RightLine.Width = 1.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 79.370073860000000000
          Width = 136.062992130000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 532.913420000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'Duplicata')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 578.267689680000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          Frame.RightLine.Width = 1.000000000000000000
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 487.559372440000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.TopLine.Width = 1.000000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        Height = 49.133858270000000000
        Top = 230.551330000000000000
        Width = 680.315400000000000000
        OnAfterPrint = 'GH1OnAfterPrint'
        Condition = 'frxDsMovimento."NOMECLIINT"'
        object Memo10: TfrxMemoView
          Top = 34.015750470000000000
          Width = 680.314960630000000000
          Height = 13.228346456692900000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Top = 3.779530000000000000
          Width = 680.314960630000000000
          Height = 32.125984250000000000
          ShowHint = False
          Color = 13948116
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold, fsItalic]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 0.755905510000000000
          Top = 3.779530000000000000
          Width = 128.000000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            '[CAPTION1]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 415.748031500000000000
          Top = 3.779530000000000000
          Width = 78.000000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            '[CAPTION2]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 536.692918270000000000
          Top = 3.779530000000000000
          Width = 78.000000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            '[CAPTION3]')
          ParentFont = False
          WordWrap = False
        end
        object MeuMemo11: TfrxMemoView
          Left = 0.755905510000000000
          Top = 18.897650000000000000
          Width = 413.763760000000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeuMemo12: TfrxMemoView
          Left = 415.748031500000000000
          Top = 18.897650000000000000
          Width = 120.000000000000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Telefone')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeuMemo13: TfrxMemoView
          Left = 536.692918270000000000
          Top = 18.897650000000000000
          Width = 139.338590000000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Contato')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Top = 35.905511811023620000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 34.015743150000000000
          Top = 35.905511811023620000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'Doc.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 215.433080630000000000
          Top = 35.905511811023620000
          Width = 226.771653540000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 442.204734170000000000
          Top = 35.905511811023620000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 612.283513380000000000
          Top = 35.905511811023620000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 79.370073860000000000
          Top = 35.905511811023620000
          Width = 136.062992130000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 532.913420000000000000
          Top = 35.905511811023620000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'Duplicata')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Left = 578.267689680000000000
          Top = 35.905511811023620000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          Left = 487.559372440000000000
          Top = 35.905511811023620000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GH2: TfrxGroupHeader
        Height = 28.346466460000000000
        Top = 302.362400000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsMovimento."TERCEIRO"'
        object Memo3: TfrxMemoView
          Top = 15.118107800000000000
          Width = 680.314960630000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Width = 680.314960630000000000
          Height = 15.118110236220470000
          ShowHint = False
          Color = 15724527
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Frame.LeftLine.Width = 1.000000000000000000
          Frame.RightLine.Width = 1.000000000000000000
          ParentFont = False
        end
        object MeuMemo21: TfrxMemoView
          Left = 0.755905510000000000
          Width = 413.543290000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8472869
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '          [VAR_MEMO21]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeuMemo22: TfrxMemoView
          Left = 416.204700000000000000
          Width = 120.000000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8472869
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VAR_MEMO22]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object MeuMemo23: TfrxMemoView
          Left = 540.204700000000000000
          Width = 135.559060000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8472869
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VAR_MEMO23]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Top = 15.118120000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          Wysiwyg = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 34.015743150000000000
          Top = 15.118120000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Doc.')
          ParentFont = False
          Wysiwyg = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 215.433080630000000000
          Top = 15.118120000000000000
          Width = 226.771653540000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
          Wysiwyg = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 442.204734170000000000
          Top = 15.118120000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
          Wysiwyg = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 612.283513380000000000
          Top = 15.118120000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          Wysiwyg = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 79.370073860000000000
          Top = 15.118120000000000000
          Width = 136.062992130000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
          Wysiwyg = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Left = 532.913420000000000000
          Top = 15.118120000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Duplicata')
          ParentFont = False
          Wysiwyg = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Left = 578.267689680000000000
          Top = 15.118120000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
          Wysiwyg = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Left = 487.559372440000000000
          Top = 15.118120000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
          Wysiwyg = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 24.000000000000000000
        Top = 393.071120000000000000
        Width = 680.315400000000000000
        object Memo1: TfrxMemoView
          Width = 680.314960630000000000
          Height = 24.000000000000000000
          OnBeforePrint = 'Memo1OnBeforePrint'
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          ParentFont = False
        end
        object MemoX: TfrxMemoView
          Width = 680.314960630000000000
          Height = 17.007874020000000000
          ShowHint = False
          Color = 15724527
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8472869
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Frame.LeftLine.Width = 1.000000000000000000
          Frame.RightLine.Width = 1.000000000000000000
          Memo.UTF8W = (
            '          Total X')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 570.000000000000000000
          Width = 84.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8472869
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDsMovimento."VALOR">)]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter2: TfrxGroupFooter
        Height = 20.000000000000000000
        Top = 438.425480000000000000
        Width = 680.315400000000000000
        object MemoY: TfrxMemoView
          Width = 680.314960630000000000
          Height = 20.000000000000000000
          ShowHint = False
          Color = 13948116
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            'Total Y')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 570.000000000000000000
          Top = 2.424860000000000000
          Width = 84.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[SUM(<frxDsMovimento."VALOR">)]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DadosMestre1: TfrxMasterData
        Height = 13.228346460000000000
        Top = 355.275820000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsMovimento
        DataSetName = 'frxDsMovimento'
        RowCount = 0
        object Memo2: TfrxMemoView
          Width = 680.314960630000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight]
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = -0.000002440000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Data'
          DataSet = frxDsMovimento
          DataSetName = 'frxDsMovimento'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovimento."Data"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 34.015740710000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsMovimento
          DataSetName = 'frxDsMovimento'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39',<frxDsMovimento."Documento">)]'
            '')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 215.433078190000000000
          Width = 226.771653540000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Descricao'
          DataSet = frxDsMovimento
          DataSetName = 'frxDsMovimento'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovimento."Descricao"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 442.204731730000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsMovimento
          DataSetName = 'frxDsMovimento'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39',<frxDsMovimento."NotaFiscal">)]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 612.283510940000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'VALOR'
          DataSet = frxDsMovimento
          DataSetName = 'frxDsMovimento'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMovimento."VALOR"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 79.370071420000000000
          Width = 136.062992125984000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'NOMECONTA'
          DataSet = frxDsMovimento
          DataSetName = 'frxDsMovimento'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovimento."NOMECONTA"]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 532.913417560000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Duplicata'
          DataSet = frxDsMovimento
          DataSetName = 'frxDsMovimento'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[frxDsMovimento."Duplicata"]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 578.267687240000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Vencimento'
          DataSet = frxDsMovimento
          DataSetName = 'frxDsMovimento'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovimento."Vencimento"]')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 487.559370000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataSet = frxDsMovimento
          DataSetName = 'frxDsMovimento'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39',<frxDsMovimento."Controle">)]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 28.000000000000000000
        Top = 517.795610000000000000
        Width = 680.315400000000000000
        object Memo56: TfrxMemoView
          Left = 570.000000000000000000
          Top = 1.952379999999950000
          Width = 84.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMovimento."VALOR">)]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 486.000000000000000000
          Top = 1.952379999999950000
          Width = 84.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL:')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 18.897650000000000000
        Top = 566.929500000000000000
        Width = 680.315400000000000000
        object Memo5: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897637795275590000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 108
    Top = 8
  end
end
