unit Envia;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, UnMLAGeral, UnInternalConsts, Buttons, Db, (*DBTables,*)
  dmkGeral;

type
  TFmEnvia = class(TForm)
    OpenDialog1: TOpenDialog;
    Panel2: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label9: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    EdSMTP: TEdit;
    EdConta: TEdit;
    EdEMail: TEdit;
    EdDono: TEdit;
    EdAssunto: TEdit;
    MePara: TMemo;
    MeCega: TMemo;
    MeCC: TMemo;
    MeCorpo: TMemo;
    Memo5: TMemo;
    LBAnexos: TListBox;
    CheckBox1: TCheckBox;
    RadioGroup1: TRadioGroup;
    EdDialUp: TEdit;
    BtLimpa: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    BtSaida: TBitBtn;
    BtEMail: TBitBtn;
    BtSalvar: TBitBtn;
    procedure Button1Click(Sender: TObject);
    procedure LBAnexosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure NMSMTP1AttachmentNotFound(Filename: String);
    procedure NMSMTP1AuthenticationFailed(var Handled: Boolean);
    procedure NMSMTP1Connect(Sender: TObject);
    procedure NMSMTP1SendStart(Sender: TObject);
    procedure NMSMTP1EncodeStart(Filename: String);
    procedure NMSMTP1EncodeEnd(Filename: String);
    procedure NMSMTP1Failure(Sender: TObject);
    procedure NMSMTP1Success(Sender: TObject);
    procedure NMSMTP1HeaderIncomplete(var handled: Boolean;
      hiType: Integer);
    procedure NMSMTP1RecipientNotFound(Recipient: String);
    procedure FormActivate(Sender: TObject);
    procedure BtEMailClick(Sender: TObject);
    procedure BtLimpaClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtSalvarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmEnvia: TFmEnvia;

implementation

uses UnMyObjects, Principal, Module;

{$R *.DFM}

procedure TFmEnvia.Button1Click(Sender: TObject);
begin
(*  if NMSMTP1.Connected then
    NMSMTP1.Disconnect
  else
  begin
    NMSMTP1.Host := Edit1.Text;
    NMSMTP1.UserID := Edit2.Text;
    NMSMTP1.Connect;
  end;*)
end;

procedure TFmEnvia.LBAnexosKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_INSERT then
    if OpenDialog1.Execute then
      LBAnexos.Items.Add(OpenDialog1.FileName);
  if Key = VK_DELETE then
    LBAnexos.Items.Delete(LBAnexos.ItemIndex);
end;

procedure TFmEnvia.NMSMTP1AttachmentNotFound(Filename: String);
begin
  Memo5.Lines.Add('O arquivo anexado: '+FileName+' n�o existe');
end;

procedure TFmEnvia.NMSMTP1AuthenticationFailed(var Handled: Boolean);
//var
 // S: String;
begin
  {
  S := NMSMTP1.UserID;
  if InputQuery('Falha de autentica��o', 'Nome de conta inv�lido. Novo nome de conta: ', S) then
  begin
    NMSMTP1.UserID := S;
    Handled := TRUE;
  end;
  }
end;

procedure TFmEnvia.NMSMTP1Connect(Sender: TObject);
begin
  Memo5.Lines.Add('Conectado');
end;

procedure TFmEnvia.NMSMTP1SendStart(Sender: TObject);
begin
  Memo5.Lines.Add('Enviando Mensagem');
end;

procedure TFmEnvia.NMSMTP1EncodeStart(Filename: String);
begin
  Memo5.Lines.Add('Codificando '+FileName);
end;

procedure TFmEnvia.NMSMTP1EncodeEnd(Filename: String);
begin
  Memo5.Lines.Add(FileName+' codificado');
end;

procedure TFmEnvia.NMSMTP1Failure(Sender: TObject);
begin
  Memo5.Lines.Add('A entrega da mensagem falhou');
end;

procedure TFmEnvia.NMSMTP1Success(Sender: TObject);
begin
  Memo5.Lines.Add('Mensagem enviada com sucesso');
end;

procedure TFmEnvia.NMSMTP1HeaderIncomplete(var handled: Boolean;
  hiType: Integer);
//var
 // S: String;
begin
  {
  case hiType of
    hiFromAddress:
      if InputQuery('O e-mail de envio est� vazio', 'Informe o endere�o: ', S) then
      begin
        NMSMTP1.PostMessage.FromAddress := S;
        Handled := TRUE;
      end;

    hiToAddress:
      if InputQuery('Destinat�rio vazio', 'Informe o endere�o: ', S) then
      begin
        NMSMTP1.PostMessage.ToAddress.Text := S;
        Handled := TRUE;
      end;

  end;
  }
end;

procedure TFmEnvia.NMSMTP1RecipientNotFound(Recipient: String);
begin
  Memo5.Lines.Add('Destinat�rio '+Recipient+' inv�lido.');
end;

procedure TFmEnvia.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  MeCorpo.SetFocus;
end;

procedure TFmEnvia.BtEMailClick(Sender: TObject);
//var
  //Cursor: TCursor;
begin
  {
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    if NMSMTP1.Connected then NMSMTP1.Disconnect;
    try
      NMSMTP1.Host := EdSMTP.Text;
      NMSMTP1.UserID := EdConta.Text;
      NMSMTP1.Connect;
    except
      winexec(PChar('rundll32.exe rnaui.dll, RnaDial '+EdDialUp.Text), SW_SHOWNORMAL);
      NMSMTP1.Host := EdSMTP.Text;
      NMSMTP1.UserID := EdConta.Text;
      NMSMTP1.Connect;
      if not NMSMTP1.Connected then
      begin
        ShowMessage('N�o foi poss�vel encontrar o provedor. Verifique a conex�o.');
        Exit;
      end;
    end;
    NMSMTP1.ClearParams := CheckBox1.Checked;

    NMSMTP1.SubType := mtPlain;
    case RadioGroup1.ItemIndex of
      0: NMSMTP1.EncodeType := uuMime;
      1: NMSMTP1.EncodeType := uuCode;
    end;
    NMSMTP1.PostMessage.FromAddress := EdEMail.Text;
    NMSMTP1.PostMessage.FromName := EdDono.Text;
    NMSMTP1.PostMessage.ToAddress.Text := MePara.Text;
    NMSMTP1.PostMessage.ToCarbonCopy.Text := MeCC.Text;
    NMSMTP1.PostMessage.ToBlindCarbonCopy.Text := MeCega.Text;
    NMSMTP1.PostMessage.Body.Text := MeCorpo.Text;

    NMSMTP1.PostMessage.Attachments.Text := LBAnexos.Items.Text;
    NMSMTP1.PostMessage.Subject := EdAssunto.Text;
    NMSMTP1.PostMessage.LocalProgram :=
      Application.Title+' '+FmPrincipal.StatusBar.Panels[3].Text;
    NMSMTP1.PostMessage.Date := DateToStr(Date);
    NMSMTP1.PostMessage.ReplyTo := EdEMail.Text;
    try
      NMSMTP1.SendMail;
      (*Dmod.QrUpdW.SQL.Clear;
      Dmod.QrUpdW.SQL.Add('UPDATE pqped SET DataEmail=:P0 WHERE Codigo=:P1');
      Dmod.QrUpdW.Params[0].AsDateTime := Now;
      Dmod.QrUpdW.Params[1].AsInteger := FmPQPed.QrPQPedCodigo.Value;
      Dmod.QrUpdW.ExecSQL;*)
      ShowMessage('E-mail enviado com sucesso!')
    except
      ShowMessage('Falha durante o envio do e-mail!');
    end;
  finally
    Screen.Cursor := Cursor;
  end;
  }
end;

procedure TFmEnvia.BtLimpaClick(Sender: TObject);
begin
  {
  NMSMTP1.ClearParameters;
  EdEmail.Clear;
  EdAssunto.Clear;
  EdDono.Clear;
  MePara.Clear;
  MeCC.Clear;
  MeCega.Clear;
  MeCorpo.Clear;
  Memo5.Clear;
  LBAnexos.Clear;
  }
end;

procedure TFmEnvia.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEnvia.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo,
  True, 0);
end;

procedure TFmEnvia.BtSalvarClick(Sender: TObject);
var
  Cam: String;
begin
  Cam := Application.Title+'\Padroes\Enviar';
  //
  Geral.WriteAppKeyLM2('SMTP',   Cam, EdSMTP.Text,     ktString);
  Geral.WriteAppKeyLM2('Conta',  Cam, EdConta.Text,    ktString);
  Geral.WriteAppKeyLM2('EMail',  Cam, EdEMail.Text,    ktString);
  Geral.WriteAppKeyLM2('Dono',   Cam, EdDono.Text,     ktString);
  Geral.WriteAppKeyLM2('DialUp', Cam, EdDialUp.Text,   ktString);
  Geral.WriteAppKeyLM2('Cega',   Cam, MeCega.Lines[0], ktString);
end;

procedure TFmEnvia.FormCreate(Sender: TObject);
var
  Cam: String;
begin
  Cam := Application.Title+'\Padroes\Enviar';
  //
  EdSMTP.Text     := Geral.ReadAppKeyLM('SMTP',  Cam, ktString, '');
  EdConta.Text    := Geral.ReadAppKeyLM('Conta', Cam, ktString, '');
  EdEMail.Text    := Geral.ReadAppKeyLM('EMail', Cam, ktString, '');
  EdDono.Text     := Geral.ReadAppKeyLM('Dono',  Cam, ktString, '');
  EdDialUp.Text   := Geral.ReadAppKeyLM('DialUp',Cam, ktString, '');
  MeCega.Lines[0] := Geral.ReadAppKeyLM('Cega',  Cam, ktString, '');
end;

end.

