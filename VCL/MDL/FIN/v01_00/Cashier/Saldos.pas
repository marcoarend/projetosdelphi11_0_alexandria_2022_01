unit Saldos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Buttons, ExtCtrls, Db, (*DBTables,*) mySQLDbTables,
  UMySQLModule, UnMyLinguas, frxClass, frxDBSet, dmkGeral, Mask, DBCtrls;

type
  TFmSaldos = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    PainelControle: TPanel;
    BtImprime: TBitBtn;
    BtSair: TBitBtn;
    TPData: TDateTimePicker;
    Label1: TLabel;
    QrTotalSaldo: TmySQLQuery;
    QrTotalSaldoNome: TWideStringField;
    QrTotalSaldoSaldo: TFloatField;
    QrTotalSaldoTipo: TIntegerField;
    QrTotalSaldoNOMETIPO: TWideStringField;
    QrHist: TmySQLQuery;
    RGTipo: TRadioGroup;
    QrHistSALDO: TFloatField;
    QrHistACUMULADO: TFloatField;
    CkNaoZero: TCheckBox;
    CkExclusivo: TCheckBox;
    frxDsTotalSaldo: TfrxDBDataset;
    frxHist: TfrxReport;
    frxDsHist: TfrxDBDataset;
    QrHistData: TDateField;
    QrHistNOMECARTEIRA: TWideStringField;
    QrHistControle: TIntegerField;
    QrHistsub: TSmallintField;
    QrHistNome: TWideStringField;
    QrHistHISTORICO: TWideStringField;
    QrHistNotaFiscal: TIntegerField;
    QrHistDebito: TFloatField;
    QrHistCredito: TFloatField;
    QrHistCompensado: TDateField;
    QrHistDocumento: TFloatField;
    QrHistVencimento: TDateField;
    Qr1: TmySQLQuery;
    Qr1Data: TDateField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    Ds1: TDataSource;
    frxSaldos: TfrxReport;
    procedure BtSairClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrTotalSaldoCalcFields(DataSet: TDataSet);
    procedure QrHistCalcFields(DataSet: TDataSet);
    procedure frxSaldosGetValue(const VarName: String; var Value: Variant);
  private
    { Private declarations }
    //FSaldos: String;
    procedure ImprimeSaldoEm();
    procedure DefineDataSets(Frx: TfrxReport);
  public
    { Public declarations }
  end;

var
  FmSaldos: TFmSaldos;
  Acum: Double;

implementation

uses UnMyObjects, UnMLAGeral, UnInternalConsts, Module, UCreate, ModuleGeral, UnFinanceiro,
ModuleFin;

{$R *.DFM}

procedure TFmSaldos.BtSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSaldos.DefineDataSets(Frx: TfrxReport);
begin
  Frx.DataSets.Clear;
  Frx.DataSets.Add(DModG.frxDsDono);
  Frx.DataSets.Add(DmodG.frxDsMaster);
  Frx.DataSets.Add(frxDsTotalSaldo);
  Frx.DataSets.Add(FmSaldos.frxDsHist);
end;

procedure TFmSaldos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSaldos.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmSaldos.FormCreate(Sender: TObject);
begin
  DefineDataSets(frxHist);
  DefineDataSets(frxSaldos);
  //
  TPData.Date := Date;
  Qr1.Open;
end;

procedure TFmSaldos.BtImprimeClick(Sender: TObject);
begin
  UFinanceiro.AtualizaPagos();
  ImprimeSaldoEm();
end;

procedure TFmSaldos.ImprimeSaldoEm();
var
  Data: String;
begin
  Data := FormatDateTime(VAR_FORMATDATE, int(TPData.Date));
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  if RGTipo.ItemIndex = 0 then
  begin
    DmodFin.GeraDadosSaldos(TPData.Date, CkExclusivo.Checked, CkNaoZero.Checked);
    QrTotalSaldo.Close;
    QrTotalSaldo.Database := DModG.MyPID_DB;
    QrTotalSaldo.Open;
    MyObjects.frxMostra(frxSaldos, 'Saldos de carteiras');
    QrTotalSaldo.Close;
  end else
  begin
    QrHist.Close;
    QrHist.Params[0].AsString := Data;
    QrHist.Open;
    MyObjects.frxMostra(frxHist, 'Saldos de contas');
    QrHist.Close;
  end;
end;

procedure TFmSaldos.QrTotalSaldoCalcFields(DataSet: TDataSet);
begin
  case QrTotalSaldoTipo.Value of
    1: QrTotalSaldoNOMETIPO.Value := 'Em carteira';
    2: QrTotalSaldoNOMETIPO.Value := 'Consignado';
    3: QrTotalSaldoNOMETIPO.Value := 'A vencer';
  else QrTotalSaldoNOMETIPO.Value := 'Desconhecido';
  end;
end;

procedure TFmSaldos.QrHistCalcFields(DataSet: TDataSet);
begin
  QrHistSALDO.Value := QrHistCredito.Value - QrHistDebito.Value;
  if QrHist.BOF then
    Acum := 0;
  Acum := Acum + QrHistSALDO.Value;
  QrHistACUMULADO.Value := Acum;
end;

procedure TFmSaldos.frxSaldosGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'SALDO_DE_CARTEIRAS' then
     Value := 'Saldo das carteiras em '+
     FormatDateTime(VAR_FORMATDATE3, TPData.Date)
  else
  if VarName = 'VARF_EMPRESA' then
    Value := DModG.QrEmpresasNOMEFILIAL.Value
  else
  if VarName = 'VARF_DATA' then
    Value := Geral.FDT(TPData.Date, 2)
end;

end.

