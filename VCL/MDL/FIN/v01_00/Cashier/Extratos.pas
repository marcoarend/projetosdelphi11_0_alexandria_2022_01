unit Extratos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UnMLAGeral, UnInternalConsts, ExtCtrls, StdCtrls, Buttons,  ComCtrls, Db,
  (*DBTables,*) DBCtrls, UnGOTOy, Variants, mySQLDbTables, DockForm, frxClass,
  frxDBSet, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkEditDateTimePicker, dmkPermissoes, UnDmkProcFunc, UnDmkEnums;

type
  TTipoSaidaRelatorio = (tsrImpressao, tsrExportRTF);
  TFmExtratos = class(TForm)
    PainelDados: TPanel;
    PainelControle: TPanel;
    BtImprime: TBitBtn;
    BtSair: TBitBtn;
    QrExtrato: TmySQLQuery;
    QrInicial: TmySQLQuery;
    QrInicialInicial: TFloatField;
    QrAnterior: TmySQLQuery;
    QrAnteriorCredito: TFloatField;
    QrAnteriorDebito: TFloatField;
    QrAnteriorSaldo: TFloatField;
    QrAnterior2: TmySQLQuery;
    QrConsolidado: TmySQLQuery;
    QrConsolidadoCredito: TFloatField;
    QrConsolidadoDebito: TFloatField;
    QrConsolidadoSaldo: TFloatField;
    QrAnterior2SALDO: TFloatField;
    QrTerceiros: TmySQLQuery;
    DsTerceiros: TDataSource;
    QrFluxo: TmySQLQuery;
    QrFluxoVENCIDO: TDateField;
    QrFluxoVENCER: TDateField;
    QrFluxoNOMEVENCIDO: TWideStringField;
    QrTerceirosCodigo: TIntegerField;
    QrTerceirosNOMECONTATO: TWideStringField;
    CkGrade: TCheckBox;
    EdA: TdmkEdit;
    EdZ: TdmkEdit;
    CkNiveis: TCheckBox;
    GBOmiss: TGroupBox;
    CkAnos: TCheckBox;
    CkMeses: TCheckBox;
    CkDias: TCheckBox;
    EdAnos: TdmkEdit;
    EdMeses: TdmkEdit;
    EdDias: TdmkEdit;
    BtSalvar: TBitBtn;
    GBPerdido: TGroupBox;
    CkAnos2: TCheckBox;
    CkMeses2: TCheckBox;
    CkDias2: TCheckBox;
    EdAnos2: TdmkEdit;
    EdMeses2: TdmkEdit;
    EdDias2: TdmkEdit;
    BtGravar: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    QrPagRecIts: TmySQLQuery;
    StringField5: TWideStringField;
    QrPagRecItsNOMEVENCIDO: TWideStringField;
    QrPagRecItsVALOR: TFloatField;
    QrPagRecItsPAGO_REAL: TFloatField;
    QrPagRecItsATRAZODD: TFloatField;
    QrPagRecItsATUALIZADO: TFloatField;
    QrPagRecItsMULTA_REAL: TFloatField;
    QrPagRecItsVENCER: TDateField;
    QrPagRecItsVENCIDO: TDateField;
    QrPagRecX: TmySQLQuery;
    StringField7: TWideStringField;
    StringField8: TWideStringField;
    QrPagRecXNOMEVENCIDO: TWideStringField;
    QrPagRecXVALOR: TFloatField;
    QrPagRecXPAGO_REAL: TFloatField;
    QrPagRecXATRAZODD: TFloatField;
    QrPagRecXATUALIZADO: TFloatField;
    QrPagRecXMULTA_REAL: TFloatField;
    QrPagRecXVENCER: TDateField;
    QrPagRecXVENCIDO: TDateField;
    QrPagRec1: TmySQLQuery;
    QrPagRec1PAGO_ROLADO: TWideStringField;
    QrPagRecXPENDENTE: TFloatField;
    QrPagRec1DEVIDO: TFloatField;
    QrPagRec1PEND_TOTAL: TFloatField;
    QrPagRec: TmySQLQuery;
    QrPagRecXData: TDateField;
    QrPagRecXTipo: TSmallintField;
    QrPagRecXCarteira: TIntegerField;
    QrPagRecXSub: TSmallintField;
    QrPagRecXAutorizacao: TIntegerField;
    QrPagRecXGenero: TIntegerField;
    QrPagRecXDescricao: TWideStringField;
    QrPagRecXNotaFiscal: TIntegerField;
    QrPagRecXDebito: TFloatField;
    QrPagRecXCredito: TFloatField;
    QrPagRecXCompensado: TDateField;
    QrPagRecXDocumento: TFloatField;
    QrPagRecXSit: TIntegerField;
    QrPagRecXVencimento: TDateField;
    QrPagRecXLk: TIntegerField;
    QrPagRecXFatID: TIntegerField;
    QrPagRecXFatParcela: TIntegerField;
    QrPagRecXID_Sub: TSmallintField;
    QrPagRecXFatura: TWideStringField;
    QrPagRecXBanco: TIntegerField;
    QrPagRecXLocal: TIntegerField;
    QrPagRecXCartao: TIntegerField;
    QrPagRecXLinha: TIntegerField;
    QrPagRecXOperCount: TIntegerField;
    QrPagRecXLancto: TIntegerField;
    QrPagRecXPago: TFloatField;
    QrPagRecXFornecedor: TIntegerField;
    QrPagRecXCliente: TIntegerField;
    QrPagRecXMoraDia: TFloatField;
    QrPagRecXMulta: TFloatField;
    QrPagRecXProtesto: TDateField;
    QrPagRecXDataCad: TDateField;
    QrPagRecXDataAlt: TDateField;
    QrPagRecXUserCad: TSmallintField;
    QrPagRecXUserAlt: TSmallintField;
    QrPagRecXDataDoc: TDateField;
    QrPagRecXNivel: TIntegerField;
    QrPagRecXVendedor: TIntegerField;
    QrPagRecXAccount: TIntegerField;
    QrPagRecXNOMECONTA: TWideStringField;
    QrPagRecXNOMECARTEIRA: TWideStringField;
    QrPagRecXSALDO: TFloatField;
    QrPagRecXEhCRED: TWideStringField;
    QrPagRecXEhDEB: TWideStringField;
    QrPagRecXNOME_TERCEIRO: TWideStringField;
    QrPagRecXTERCEIRO: TLargeintField;
    QrPagRec1Data: TDateField;
    QrPagRec1DataDoc: TDateField;
    QrPagRec1Vencimento: TDateField;
    QrPagRec1TERCEIRO: TFloatField;
    QrPagRec1NOME_TERCEIRO: TWideStringField;
    QrPagRec1NOMEVENCIDO: TWideStringField;
    QrPagRec1NOMECONTA: TWideStringField;
    QrPagRec1Descricao: TWideStringField;
    QrPagRec1CtrlPai: TFloatField;
    QrPagRec1Tipo: TFloatField;
    QrPagRec1Documento: TFloatField;
    QrPagRec1NotaFiscal: TFloatField;
    QrPagRec1Valor: TFloatField;
    QrPagRec1MoraDia: TFloatField;
    QrPagRec1PAGO_REAL: TFloatField;
    QrPagRec1PENDENTE: TFloatField;
    QrPagRec1ATUALIZADO: TFloatField;
    QrPagRec1MULTA_REAL: TFloatField;
    QrPagRec1ATRAZODD: TFloatField;
    QrPagRecItsData: TDateField;
    QrPagRecItsTipo: TSmallintField;
    QrPagRecItsCarteira: TIntegerField;
    QrPagRecItsSub: TSmallintField;
    QrPagRecItsAutorizacao: TIntegerField;
    QrPagRecItsGenero: TIntegerField;
    QrPagRecItsDescricao: TWideStringField;
    QrPagRecItsNotaFiscal: TIntegerField;
    QrPagRecItsDebito: TFloatField;
    QrPagRecItsCredito: TFloatField;
    QrPagRecItsCompensado: TDateField;
    QrPagRecItsDocumento: TFloatField;
    QrPagRecItsSit: TIntegerField;
    QrPagRecItsVencimento: TDateField;
    QrPagRecItsLk: TIntegerField;
    QrPagRecItsFatID: TIntegerField;
    QrPagRecItsFatParcela: TIntegerField;
    QrPagRecItsID_Sub: TSmallintField;
    QrPagRecItsFatura: TWideStringField;
    QrPagRecItsBanco: TIntegerField;
    QrPagRecItsLocal: TIntegerField;
    QrPagRecItsCartao: TIntegerField;
    QrPagRecItsLinha: TIntegerField;
    QrPagRecItsOperCount: TIntegerField;
    QrPagRecItsLancto: TIntegerField;
    QrPagRecItsPago: TFloatField;
    QrPagRecItsFornecedor: TIntegerField;
    QrPagRecItsCliente: TIntegerField;
    QrPagRecItsMoraDia: TFloatField;
    QrPagRecItsMulta: TFloatField;
    QrPagRecItsProtesto: TDateField;
    QrPagRecItsDataCad: TDateField;
    QrPagRecItsDataAlt: TDateField;
    QrPagRecItsUserCad: TSmallintField;
    QrPagRecItsUserAlt: TSmallintField;
    QrPagRecItsDataDoc: TDateField;
    QrPagRecItsNivel: TIntegerField;
    QrPagRecItsVendedor: TIntegerField;
    QrPagRecItsAccount: TIntegerField;
    QrPagRecItsNOMECONTA: TWideStringField;
    QrPagRecItsNOMECARTEIRA: TWideStringField;
    QrPagRecItsSALDO: TFloatField;
    QrPagRecItsEhCRED: TWideStringField;
    QrPagRecItsEhDEB: TWideStringField;
    QrPagRecItsNOME_TERCEIRO: TWideStringField;
    QrPagRecItsTERCEIRO: TLargeintField;
    QrExtratoNOMECONTA: TWideStringField;
    QrExtratoNOMECARTEIRA: TWideStringField;
    QrFluxoNOMECONTA: TWideStringField;
    QrFluxoNOMECARTEIRA: TWideStringField;
    QrFluxoSALDO: TFloatField;
    DsAccounts: TDataSource;
    QrAccounts: TmySQLQuery;
    QrAccountsCodigo: TIntegerField;
    QrAccountsNOMECONTATO: TWideStringField;
    QrVendedores: TmySQLQuery;
    DsVendedores: TDataSource;
    QrVendedoresCodigo: TIntegerField;
    QrVendedoresNOMECONTATO: TWideStringField;
    QrPagRecXControle: TIntegerField;
    QrPagRecXID_Pgto: TIntegerField;
    QrPagRecXMez: TIntegerField;
    QrPagRecXCtrlIni: TIntegerField;
    QrPagRec1ID_Pgto: TFloatField;
    QrPagRec1CtrlIni: TFloatField;
    QrPagRec1Controle: TFloatField;
    QrPagRecItsControle: TIntegerField;
    QrPagRecItsID_Pgto: TIntegerField;
    QrPagRecItsMez: TIntegerField;
    QrPagRecItsCtrlIni: TIntegerField;
    QrPagRecData: TDateField;
    QrPagRecTipo: TSmallintField;
    QrPagRecCarteira: TIntegerField;
    QrPagRecControle: TIntegerField;
    QrPagRecSub: TSmallintField;
    QrPagRecAutorizacao: TIntegerField;
    QrPagRecGenero: TIntegerField;
    QrPagRecDescricao: TWideStringField;
    QrPagRecNotaFiscal: TIntegerField;
    QrPagRecDebito: TFloatField;
    QrPagRecCredito: TFloatField;
    QrPagRecCompensado: TDateField;
    QrPagRecDocumento: TFloatField;
    QrPagRecSit: TIntegerField;
    QrPagRecVencimento: TDateField;
    QrPagRecLk: TIntegerField;
    QrPagRecFatID: TIntegerField;
    QrPagRecFatParcela: TIntegerField;
    QrPagRecID_Pgto: TIntegerField;
    QrPagRecID_Sub: TSmallintField;
    QrPagRecFatura: TWideStringField;
    QrPagRecBanco: TIntegerField;
    QrPagRecLocal: TIntegerField;
    QrPagRecCartao: TIntegerField;
    QrPagRecLinha: TIntegerField;
    QrPagRecOperCount: TIntegerField;
    QrPagRecLancto: TIntegerField;
    QrPagRecPago: TFloatField;
    QrPagRecMez: TIntegerField;
    QrPagRecFornecedor: TIntegerField;
    QrPagRecCliente: TIntegerField;
    QrPagRecMoraDia: TFloatField;
    QrPagRecMulta: TFloatField;
    QrPagRecProtesto: TDateField;
    QrPagRecDataCad: TDateField;
    QrPagRecDataAlt: TDateField;
    QrPagRecUserCad: TSmallintField;
    QrPagRecUserAlt: TSmallintField;
    QrPagRecDataDoc: TDateField;
    QrPagRecCtrlIni: TIntegerField;
    QrPagRecNivel: TIntegerField;
    QrPagRecVendedor: TIntegerField;
    QrPagRecAccount: TIntegerField;
    QrPagRecNOMECONTA: TWideStringField;
    QrPagRecNOMECARTEIRA: TWideStringField;
    QrPagRecSALDO: TFloatField;
    QrPagRecEhCRED: TWideStringField;
    QrPagRecEhDEB: TWideStringField;
    QrPagRecNOME_TERCEIRO: TWideStringField;
    QrPagRecTERCEIRO: TLargeintField;
    QrPagRecVENCER: TDateField;
    QrPagRecVALOR: TFloatField;
    QrPagRecVENCIDO: TDateField;
    QrPagRecATRAZODD: TFloatField;
    QrPagRecMULTA_REAL: TFloatField;
    QrPagRecATUALIZADO: TFloatField;
    QrPagRecPAGO_REAL: TFloatField;
    QrPagRecNOMEVENCIDO: TWideStringField;
    BtEMail: TBitBtn;
    QrFluxoData: TDateField;
    QrFluxoTipo: TSmallintField;
    QrFluxoCarteira: TIntegerField;
    QrFluxoControle: TIntegerField;
    QrFluxoSub: TSmallintField;
    QrFluxoAutorizacao: TIntegerField;
    QrFluxoGenero: TIntegerField;
    QrFluxoDescricao: TWideStringField;
    QrFluxoNotaFiscal: TIntegerField;
    QrFluxoDebito: TFloatField;
    QrFluxoCredito: TFloatField;
    QrFluxoCompensado: TDateField;
    QrFluxoDocumento: TFloatField;
    QrFluxoSit: TIntegerField;
    QrFluxoVencimento: TDateField;
    QrFluxoLk: TIntegerField;
    QrFluxoFatID: TIntegerField;
    QrFluxoFatParcela: TIntegerField;
    QrFluxoID_Pgto: TIntegerField;
    QrFluxoID_Sub: TSmallintField;
    QrFluxoFatura: TWideStringField;
    QrFluxoBanco: TIntegerField;
    QrFluxoLocal: TIntegerField;
    QrFluxoCartao: TIntegerField;
    QrFluxoLinha: TIntegerField;
    QrFluxoOperCount: TIntegerField;
    QrFluxoLancto: TIntegerField;
    QrFluxoPago: TFloatField;
    QrFluxoMez: TIntegerField;
    QrFluxoFornecedor: TIntegerField;
    QrFluxoCliente: TIntegerField;
    QrFluxoMoraDia: TFloatField;
    QrFluxoMulta: TFloatField;
    QrFluxoProtesto: TDateField;
    QrFluxoDataCad: TDateField;
    QrFluxoDataAlt: TDateField;
    QrFluxoUserCad: TSmallintField;
    QrFluxoUserAlt: TSmallintField;
    QrFluxoDataDoc: TDateField;
    QrFluxoCtrlIni: TIntegerField;
    QrFluxoNivel: TIntegerField;
    QrFluxoVendedor: TIntegerField;
    QrFluxoAccount: TIntegerField;
    QrExtratoData: TDateField;
    QrExtratoTipo: TSmallintField;
    QrExtratoCarteira: TIntegerField;
    QrExtratoControle: TIntegerField;
    QrExtratoSub: TSmallintField;
    QrExtratoAutorizacao: TIntegerField;
    QrExtratoGenero: TIntegerField;
    QrExtratoDescricao: TWideStringField;
    QrExtratoNotaFiscal: TIntegerField;
    QrExtratoDebito: TFloatField;
    QrExtratoCredito: TFloatField;
    QrExtratoCompensado: TDateField;
    QrExtratoDocumento: TFloatField;
    QrExtratoSit: TIntegerField;
    QrExtratoVencimento: TDateField;
    QrExtratoLk: TIntegerField;
    QrExtratoFatID: TIntegerField;
    QrExtratoFatParcela: TIntegerField;
    QrExtratoID_Pgto: TIntegerField;
    QrExtratoID_Sub: TSmallintField;
    QrExtratoFatura: TWideStringField;
    QrExtratoBanco: TIntegerField;
    QrExtratoLocal: TIntegerField;
    QrExtratoCartao: TIntegerField;
    QrExtratoLinha: TIntegerField;
    QrExtratoOperCount: TIntegerField;
    QrExtratoLancto: TIntegerField;
    QrExtratoPago: TFloatField;
    QrExtratoMez: TIntegerField;
    QrExtratoFornecedor: TIntegerField;
    QrExtratoCliente: TIntegerField;
    QrExtratoMoraDia: TFloatField;
    QrExtratoMulta: TFloatField;
    QrExtratoProtesto: TDateField;
    QrExtratoDataCad: TDateField;
    QrExtratoDataAlt: TDateField;
    QrExtratoUserCad: TSmallintField;
    QrExtratoUserAlt: TSmallintField;
    QrExtratoDataDoc: TDateField;
    QrExtratoCtrlIni: TIntegerField;
    QrExtratoNivel: TIntegerField;
    QrExtratoVendedor: TIntegerField;
    QrExtratoAccount: TIntegerField;
    QrTerceirosTel1: TWideStringField;
    QrTerceirosTel2: TWideStringField;
    QrTerceirosTel3: TWideStringField;
    QrTerceirosTEL1_TXT: TWideStringField;
    QrTerceirosTEL2_TXT: TWideStringField;
    QrTerceirosTEL3_TXT: TWideStringField;
    QrTerceirosTELEFONES: TWideStringField;
    QrPagRecXDuplicata: TWideStringField;
    QrPagRecFatID_Sub: TIntegerField;
    QrPagRecICMS_P: TFloatField;
    QrPagRecICMS_V: TFloatField;
    QrPagRecDuplicata: TWideStringField;
    QrPagRec1Duplicata: TWideStringField;
    QrPagRecXFatID_Sub: TIntegerField;
    QrPagRecXICMS_P: TFloatField;
    QrPagRecXICMS_V: TFloatField;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    frxFin_Relat_005_02_B1: TfrxReport;
    frxDsPagRec1: TfrxDBDataset;
    frxFin_Relat_005_02_B2: TfrxReport;
    frxDsPagRec: TfrxDBDataset;
    frxFin_Relat_005_00: TfrxReport;
    frxDsExtrato: TfrxDBDataset;
    frxFin_Relat_005_01_A08: TfrxReport;
    frxDsFluxo: TfrxDBDataset;
    PainelA: TPanel;
    Label9: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrExtratoQtde: TFloatField;
    PainelB: TPanel;
    RGTipo: TRadioGroup;
    PainelC: TPanel;
    PainelD: TPanel;
    CBAccount: TdmkDBLookupComboBox;
    EdAccount: TdmkEditCB;
    LaAccount: TLabel;
    CBTerceiro: TdmkDBLookupComboBox;
    EdTerceiro: TdmkEditCB;
    LaTerceiro: TLabel;
    CBConta: TdmkDBLookupComboBox;
    EdConta: TdmkEditCB;
    Label4: TLabel;
    CBVendedor: TdmkDBLookupComboBox;
    EdVendedor: TdmkEditCB;
    LaVendedor: TLabel;
    CkOmiss: TCheckBox;
    QrFluxoQtde: TFloatField;
    PnDatas2: TPanel;
    GBVencto: TGroupBox;
    LaVenctI: TLabel;
    LaVenctF: TLabel;
    TPVctoIni: TdmkEditDateTimePicker;
    TPVctoFim: TdmkEditDateTimePicker;
    CkVencto: TCheckBox;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    TPDocIni: TdmkEditDateTimePicker;
    TPDocFim: TdmkEditDateTimePicker;
    CkDataDoc: TCheckBox;
    GroupBox3: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    TPCompIni: TdmkEditDateTimePicker;
    TPCompFim: TdmkEditDateTimePicker;
    CkDataComp: TCheckBox;
    PnDatas1: TPanel;
    GroupBox1: TGroupBox;
    Label101: TLabel;
    Label102: TLabel;
    TPEmissIni: TdmkEditDateTimePicker;
    TPEmissFim: TdmkEditDateTimePicker;
    CkEmissao: TCheckBox;
    PainelE: TPanel;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    CkOrdem1: TCheckBox;
    Panel2: TPanel;
    CkOrdem2: TCheckBox;
    RGHistorico: TRadioGroup;
    QrPagRecQtde: TFloatField;
    QrPagRecXQtde: TFloatField;
    QrPagRec1Qtde: TFloatField;
    QrPagRecXNO_UH: TWideStringField;
    QrPagRecNO_UH: TWideStringField;
    QrPagRecItsNO_UH: TWideStringField;
    QrPagRec1NO_UH: TWideStringField;
    CkSubstituir: TCheckBox;
    QrIni: TmySQLQuery;
    QrIniValor: TFloatField;
    PB1: TProgressBar;
    PnFluxo: TPanel;
    Label3: TLabel;
    Label7: TLabel;
    GroupBox5: TGroupBox;
    Label8: TLabel;
    Label10: TLabel;
    TPCre: TDateTimePicker;
    TPDeb: TDateTimePicker;
    QrMovant: TmySQLQuery;
    QrMovantMovim: TFloatField;
    QrMovantVALOR: TFloatField;
    DsFlxo: TDataSource;
    QrFlxo: TmySQLQuery;
    QrExtr: TmySQLQuery;
    QrExtratoDataE: TDateField;
    QrExtratoDataV: TDateField;
    QrExtratoDataQ: TDateField;
    QrExtratoDataX: TDateField;
    QrExtratoTexto: TWideStringField;
    QrExtratoDocum: TWideStringField;
    QrExtratoNotaF: TWideStringField;
    QrExtratoSaldo: TFloatField;
    QrExtratoCartC: TIntegerField;
    QrExtratoCartN: TWideStringField;
    QrExtratoCodig: TIntegerField;
    QrExtratoCtrle: TIntegerField;
    QrExtratoCtSub: TIntegerField;
    QrExtratoCART_ORIG: TWideStringField;
    QrExtratoID_Pg: TIntegerField;
    QrExtratoTipoI: TIntegerField;
    DsExtr: TDataSource;
    QrFlxoQUITACAO: TWideStringField;
    QrFlxoData: TDateField;
    QrFlxoTipo: TSmallintField;
    QrFlxoCarteira: TIntegerField;
    QrFlxoControle: TIntegerField;
    QrFlxoSub: TSmallintField;
    QrFlxoAutorizacao: TIntegerField;
    QrFlxoGenero: TIntegerField;
    QrFlxoQtde: TFloatField;
    QrFlxoDescricao: TWideStringField;
    QrFlxoNotaFiscal: TIntegerField;
    QrFlxoDebito: TFloatField;
    QrFlxoCredito: TFloatField;
    QrFlxoCompensado: TDateField;
    QrFlxoSerieCH: TWideStringField;
    QrFlxoDocumento: TFloatField;
    QrFlxoSit: TIntegerField;
    QrFlxoVencimento: TDateField;
    QrFlxoFatID: TIntegerField;
    QrFlxoFatID_Sub: TIntegerField;
    QrFlxoFatParcela: TIntegerField;
    QrFlxoID_Pgto: TIntegerField;
    QrFlxoID_Sub: TSmallintField;
    QrFlxoFatura: TWideStringField;
    QrFlxoEmitente: TWideStringField;
    QrFlxoBanco: TIntegerField;
    QrFlxoAgencia: TIntegerField;
    QrFlxoContaCorrente: TWideStringField;
    QrFlxoCNPJCPF: TWideStringField;
    QrFlxoLocal: TIntegerField;
    QrFlxoCartao: TIntegerField;
    QrFlxoLinha: TIntegerField;
    QrFlxoOperCount: TIntegerField;
    QrFlxoLancto: TIntegerField;
    QrFlxoPago: TFloatField;
    QrFlxoMez: TIntegerField;
    QrFlxoFornecedor: TIntegerField;
    QrFlxoCliente: TIntegerField;
    QrFlxoCliInt: TIntegerField;
    QrFlxoForneceI: TIntegerField;
    QrFlxoMoraDia: TFloatField;
    QrFlxoMulta: TFloatField;
    QrFlxoMoraVal: TFloatField;
    QrFlxoMultaVal: TFloatField;
    QrFlxoProtesto: TDateField;
    QrFlxoDataDoc: TDateField;
    QrFlxoCtrlIni: TIntegerField;
    QrFlxoNivel: TIntegerField;
    QrFlxoVendedor: TIntegerField;
    QrFlxoAccount: TIntegerField;
    QrFlxoICMS_P: TFloatField;
    QrFlxoICMS_V: TFloatField;
    QrFlxoDuplicata: TWideStringField;
    QrFlxoDepto: TIntegerField;
    QrFlxoDescoPor: TIntegerField;
    QrFlxoDescoVal: TFloatField;
    QrFlxoDescoControle: TIntegerField;
    QrFlxoUnidade: TIntegerField;
    QrFlxoNFVal: TFloatField;
    QrFlxoAntigo: TWideStringField;
    QrFlxoExcelGru: TIntegerField;
    QrFlxoDoc2: TWideStringField;
    QrFlxoCNAB_Sit: TSmallintField;
    QrFlxoTipoCH: TSmallintField;
    QrFlxoLk: TIntegerField;
    QrFlxoDataCad: TDateField;
    QrFlxoDataAlt: TDateField;
    QrFlxoUserCad: TIntegerField;
    QrFlxoUserAlt: TIntegerField;
    QrFlxoNOMECART: TWideStringField;
    QrLct: TmySQLQuery;
    frxDsExtr: TfrxDBDataset;
    QrExtrVALOR: TFloatField;
    QrExtrCredi: TFloatField;
    QrExtrDebit: TFloatField;
    QrFlxoNOMERELACIONADO: TWideStringField;
    frxFin_Relat_005_01_A07: TfrxReport;
    frxFin_Relat_005_01_A06: TfrxReport;
    RGFonte: TRadioGroup;
    QrPagRecSERIEDOC: TWideStringField;
    QrPagRecSerieCH: TWideStringField;
    dmkPermissoes1: TdmkPermissoes;
    CkDocEmAberto: TCheckBox;
    frxPagRec1: TfrxReport;
    QrPagRecCRED: TFloatField;
    QrPagRecDEBI: TFloatField;
    QrPagRecSerieNF: TWideStringField;
    QrPagRecFatNum: TFloatField;
    QrPagRecID_Quit: TIntegerField;
    QrPagRecEmitente: TWideStringField;
    QrPagRecAgencia: TIntegerField;
    QrPagRecContaCorrente: TWideStringField;
    QrPagRecCNPJCPF: TWideStringField;
    QrPagRecCliInt: TIntegerField;
    QrPagRecForneceI: TIntegerField;
    QrPagRecMoraVal: TFloatField;
    QrPagRecMultaVal: TFloatField;
    QrPagRecDepto: TIntegerField;
    QrPagRecDescoPor: TIntegerField;
    QrPagRecDescoVal: TFloatField;
    QrPagRecDescoControle: TIntegerField;
    QrPagRecUnidade: TIntegerField;
    QrPagRecNFVal: TFloatField;
    QrPagRecAntigo: TWideStringField;
    QrPagRecExcelGru: TIntegerField;
    QrPagRecDoc2: TWideStringField;
    QrPagRecCNAB_Sit: TSmallintField;
    QrPagRecTipoCH: TSmallintField;
    QrPagRecReparcel: TIntegerField;
    QrPagRecAtrelado: TIntegerField;
    QrPagRecPagMul: TFloatField;
    QrPagRecPagJur: TFloatField;
    QrPagRecSubPgto1: TIntegerField;
    QrPagRecMultiPgto: TIntegerField;
    QrPagRecProtocolo: TIntegerField;
    QrPagRecCtrlQuitPg: TIntegerField;
    QrPagRecEndossas: TSmallintField;
    QrPagRecEndossan: TFloatField;
    QrPagRecEndossad: TFloatField;
    QrPagRecCancelado: TSmallintField;
    QrPagRecEventosCad: TIntegerField;
    QrPagRecEncerrado: TIntegerField;
    QrPagRecAlterWeb: TSmallintField;
    QrPagRecAtivo: TSmallintField;
    QrPagRecErrCtrl: TIntegerField;
    frxFin_Relat_005_02_A08: TfrxReport;
    frxFin_Relat_005_02_A07: TfrxReport;
    frxFin_Relat_005_02_A06: TfrxReport;
    procedure FormActivate(Sender: TObject);
    procedure BtSairClick(Sender: TObject);
    procedure RGTipoClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrFluxoCalcFields(DataSet: TDataSet);
    procedure CBTerceiroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrPagRecCalcFields(DataSet: TDataSet);
    procedure CkOmissClick(Sender: TObject);
    procedure BtSalvarClick(Sender: TObject);
    procedure BtGravarClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPagRecItsCalcFields(DataSet: TDataSet);
    procedure QrPagRecXCalcFields(DataSet: TDataSet);
    procedure QrPagRec1CalcFields(DataSet: TDataSet);
    procedure BtEMailClick(Sender: TObject);
    procedure QrTerceirosCalcFields(DataSet: TDataSet);
    procedure frxFin_Relat_005_02_B1GetValue(const VarName: String;
      var Value: Variant);
    procedure frxFin_Relat_005_00GetValue(const VarName: String;
      var Value: Variant);
    procedure QrMovantCalcFields(DataSet: TDataSet);
    procedure QrExtrBeforeOpen(DataSet: TDataSet);
    procedure QrExtrCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FEmpresa: Integer;
    FTabLctA: String;
    procedure R00_ImprimeExtratoConsolidado();
    procedure R01_ImprimeFluxoDeCaixa();
    procedure R02_ImprimeContasAPagar(Sender: TObject; Saida: TTipoSaidaRelatorio);
    procedure SQLContasAPagar(Query: TmySQLQuery; Tipo: Integer);
    procedure CreateDockableWindows;
    procedure GeraRelatorio(Saida: TTipoSaidaRelatorio);
    function SQL_Ordenar(): String;
  public
    { Public declarations }
  end;

var
  FmExtratos: TFmExtratos;

implementation

uses UnMyobjects, UCreate, UnFinanceiro, Module, Envia, ModuleGeral, UMySQLModule;

{$R *.DFM}

{FmMyGlyfs}

const
{  Colors: array[0..6] of TColor = (clWhite, clBlue, clGreen, clRed, clTeal,
                                   clPurple, clLime);
  ColStr: array[0..6] of string = ('White', 'Blue', 'Green', 'Red', 'Teal',
                                   'Purple', 'Lime');}
  Colors: array[0..0] of TColor = (clWhite);
  ColStr: array[0..0] of string = ('White');

var
  Ext_EmisI, Ext_EmisF, Ext_VctoI, Ext_VctoF, Ext_DataH, Ext_DataA,
  Ext_DocI,  Ext_DocF,  Ext_CompI, Ext_CompF: String;
  Ext_Saldo,
  Ext_VALORA,     Ext_VALORB,     Ext_VALORC,
  Ext_PENDENTEA,  Ext_PENDENTEB,  Ext_PENDENTEC,
  Ext_ATUALIZADOA, Ext_ATUALIZADOB, Ext_ATUALIZADOC,
  Ext_DEVIDOA, Ext_DEVIDOB, Ext_DEVIDOC,
  Ext_PAGO_REALA, Ext_PAGO_REALB, Ext_PAGO_REALC: Double;
  Ext_Vencto: TDate;
  DockWindows: array[0..0] of TFmDockForm;


procedure TFmExtratos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmExtratos.BtSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFmExtratos.RGTipoClick(Sender: TObject);
begin
  PnDatas2.Visible    := RGTipo.ItemIndex in ([2,3,4,5,6,7,8,9]);
  PainelD.Visible     := RGTipo.ItemIndex in ([2,3,4,5,6,7,8,9]);
  PnFluxo.Visible     := RGTipo.ItemIndex = 1;
  CkOrdem1.Visible    := RGTipo.ItemIndex <> 1;
  RGOrdem1.Visible    := RGTipo.ItemIndex <> 1;
  CkOrdem2.Visible    := RGTipo.ItemIndex <> 1;
  RGOrdem2.Visible    := RGTipo.ItemIndex <> 1;
  RGHistorico.Visible := RGTipo.ItemIndex in ([6,7]);
  RGFonte.Visible     := RGTipo.ItemIndex in ([1,2,3,4,5,8,9]);
  //
  EdA.Visible        := True;
  EdZ.Visible        := True;
  CkNiveis.Visible   := True;
  //CkNiveis.Checked   := False;
  CkOmiss.Visible    := False;
  GBOmiss.Visible    := False;
  GBPerdido.Visible  := False;
  LaAccount.Visible  := False;
  EdAccount.Visible  := False;
  CBAccount.Visible  := False;
  LaVendedor.Visible := False;
  EdVendedor.Visible := False;
  CBVendedor.Visible := False;
  //
  //CkVencto.Visible      := True;
  GBVencto.Visible      := True;
  CkSubstituir.Visible  := False;
  CkDocEmAberto.Visible := False;
  CkEmissao.Caption    := 'Data da emiss�o:';
  case RGTipo.ItemIndex of
    0:
    begin
      GBVencto.Visible := True;
      //
      EdA.Visible := False;
      EdZ.Visible := False;
      CkNiveis.Visible := False;
      //
      if TPEmissIni.Date > date then TPEmissIni.Date := date;
      if TPEmissFim.Date < date then TPEmissFim.Date := date;
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date;
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      if TPVctoIni.Date > date then TPVctoIni.Date := date;
      if TPVctoFim.Date < date then TPVctoFim.Date := date;
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date;
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CBTerceiro.Enabled := False;
      EdTerceiro.Enabled := False;
      LaTerceiro.Enabled := False;
    end;
    1:
    begin
      EdA.Visible           := False;
      EdZ.Visible           := False;
      CkNiveis.Visible      := False;
      CkGrade.Visible       := False;
      CkSubstituir.Visible  := True;
      CkDocEmAberto.Visible := True;
      CkEmissao.Caption     := 'Per�odo:';
      //
      if TPEmissFim.Date < date then TPEmissFim.Date := date;
      TPEmissIni.MinDate := 1;
      TPEmissIni.MaxDate := Date+7305;//Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := Date;
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      if TPVctoFim.Date < date then TPVctoFim.Date := date;
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := Date;
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      CBTerceiro.Enabled := False;
      EdTerceiro.Enabled := False;
      LaTerceiro.Enabled := False;
    end;
    2:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Fornecedor';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
    end;
    3:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Cliente';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
      ////
      LaAccount.Visible := True;
      EdAccount.Visible := True;
      CBAccount.Visible := True;
      LaVendedor.Visible := True;
      EdVendedor.Visible := True;
      CBVendedor.Visible := True;
    end;
    4:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
    end;
    5:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Cliente';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
    end;
    6:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Fornecedor';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
    end;
    7:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Cliente';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      CkOmiss.Visible := True;
      GBOmiss.Visible := CkOmiss.Checked;
      ////
      LaAccount.Visible := True;
      EdAccount.Visible := True;
      CBAccount.Visible := True;
      LaVendedor.Visible := True;
      EdVendedor.Visible := True;
      CBVendedor.Visible := True;
    end;
    8:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Cliente';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      GBPerdido.Visible := True;
    end;
    9:
    begin
      CBTerceiro.Enabled := True;
      EdTerceiro.Enabled := True;
      LaTerceiro.Enabled := True;
      LaTerceiro.Caption := 'Cliente';
      ////
      TPEmissIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPEmissFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPEmissFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      //
      TPVctoIni.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoIni.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      TPVctoFim.MinDate := 1;//StrToDate(FormatDateTime(VAR_FORMATDATE, 1));
      TPVctoFim.MaxDate := Date+7305;//StrToDate(FormatDateTime(VAR_FORMATDATE, Date+7305));
      ////
      GBPerdido.Visible := True;
    end;
  end;
  if not RGOrdem1.Enabled then RGOrdem1.ItemIndex := 0;
  if not RGOrdem2.Enabled then RGOrdem2.ItemIndex := 1;
  if RGTipo.ItemIndex in ([0,1]) then
  begin
    CkVencto.Enabled  := False;
    GBVencto.Enabled  := False;
    LaVenctI.Enabled  := False;
    LaVenctF.Enabled  := False;
    TPVctoIni.Enabled := False;
    TPVctoFim.Enabled := False;
  end else begin
    CkVencto.Enabled  := True;
    GBVencto.Enabled  := True;
    LaVenctI.Enabled  := True;
    LaVenctF.Enabled  := True;
    TPVctoIni.Enabled := True;
    TPVctoFim.Enabled := True;
  end;
end;

procedure TFmExtratos.BtImprimeClick(Sender: TObject);
begin
  GeraRelatorio(tsrImpressao);
end;

procedure TFmExtratos.FormCreate(Sender: TObject);
begin
  CBEmpresa.ListSource := DModG.DsEmpresas;
  TPCre.Date := Int(Date);
  TPDeb.Date := Int(Date);
  //
  RGOrdem1.Items[7] := MLAGeral.TxtUH();
  RGOrdem2.Items[7] := MLAGeral.TxtUH();
  //
  CreateDockableWindows;
  TPEmissIni.Date := Date -60;
  TPVctoIni.Date  := Date -60;
  TPDocIni.Date   := Date -90;
  TPDocFim.Date   := Date +60;
  TPCompIni.Date  := Date;
  TPCompFim.Date  := Date;
  RGTipo.ItemIndex := 1;
  QrTerceiros.Open;
  QrAccounts.Open;
  QrVendedores.Open;
  QrContas.Open;
  ///////
  EdAnos.Text := Geral.ReadAppKeyCU('Anos', 'Dermatek\Extratos\Venctos',
                ktString,'0');
  EdMeses.Text := Geral.ReadAppKeyCU('Meses', 'Dermatek\Extratos\Venctos',
                ktString,'0');
  EdDias.Text := Geral.ReadAppKeyCU('Dias', 'Dermatek\Extratos\Venctos',
                ktString,'0');
  CkAnos.Checked := Geral.ReadAppKeyCU('Ck_Anos', 'Dermatek\Extratos\Venctos',
                ktBoolean, False);
  CkMeses.Checked := Geral.ReadAppKeyCU('Ck_Meses', 'Dermatek\Extratos\Venctos',
                ktBoolean, False);
  CkDias.Checked := Geral.ReadAppKeyCU('Ck_Dias', 'Dermatek\Extratos\Venctos',
                ktBoolean, False);
  ///////
  EdAnos2.Text := Geral.ReadAppKeyCU('Anos', 'Dermatek\Extratos\Perdidos',
                ktString,'0');
  EdMeses2.Text := Geral.ReadAppKeyCU('Meses', 'Dermatek\Extratos\Perdidos',
                ktString,'0');
  EdDias2.Text := Geral.ReadAppKeyCU('Dias', 'Dermatek\Extratos\Perdidos',
                ktString,'0');
  CkAnos2.Checked := Geral.ReadAppKeyCU('Ck_Anos', 'Dermatek\Extratos\Perdidos',
                ktBoolean, False);
  CkMeses2.Checked := Geral.ReadAppKeyCU('Ck_Meses', 'Dermatek\Extratos\Perdidos',
                ktBoolean, False);
  CkDias2.Checked := Geral.ReadAppKeyCU('Ck_Dias', 'Dermatek\Extratos\Perdidos',
                ktBoolean, False);
end;

procedure TFmExtratos.R00_ImprimeExtratoConsolidado();
begin
  // Deprecado
  QrInicial.Close;
  QrInicial.Params[0].AsInteger := FEmpresa;
  QrInicial.Open;
  // fim deprecado
  QrAnterior.Close;
  QrAnterior.Params[00].AsString  := Ext_EmisI;
  QrAnterior.Params[01].AsString  := Ext_EmisI;
  QrAnterior.Params[02].AsInteger := FEmpresa;
  QrAnterior.Open;
  //
  Ext_Saldo := QrInicialInicial.Value + QrAnteriorSaldo.Value;
  QrInicial.Close;
  QrAnterior.Close;
  //
  QrExtrato.Close;
  QrExtrato.Params[00].AsString  := Ext_EmisI;
  QrExtrato.Params[01].AsString  := Ext_EmisF;
  QrExtrato.Params[02].AsString  := Ext_EmisF;
  QrExtrato.Params[03].AsInteger := FEmpresa;
  QrExtrato.Open;
  //
  MyObjects.frxMostra(frxFin_Relat_005_00, 'Extrato');
  QrExtrato.Close;
end;

procedure TFmExtratos.R01_ImprimeFluxoDeCaixa();
  function Fmt(Data: TDateTime): String;
  begin
    Result := '"' + FormatDateTime('YYYY-MM-DD', Data) + '"';
  end;
var
  Quitacao, Quitacao2, Texto, Docum, NotaF: String;
  DataE, DataV, DataQ, DataX: Variant;
  Codig: Integer;
  Saldo: Double;
  DataI: TDateTime;
begin
  Screen.Cursor := crHourGlass;
  PB1.Position := 0;
  PB1.Visible  := True;
  //   S A L D O    I N I C I A L
  QrIni.Close;
  QrIni.Params[0].AsInteger := FEmpresa;
  QrIni.Open;

  //   M O V I M E N T O
  if CkEmissao.Checked then DataI := TPEmissIni.Date else DataI := 0;
  QrMovant.Close;
  QrMovant.Params[0].AsInteger := FEmpresa;
  QrMovant.Params[1].AsString  := Geral.FDT(DataI, 1);
  QrMovant.Open;

  //   L A N � A M E N T O S
  if CkDocEmAberto.Checked then
  begin
    Quitacao2 := 'Date(IF(car.Tipo <> 2, lan.Data,IF(lan.Sit > 1, lan.Compensado, ' +
  'IF(lan.Vencimento<SYSDATE(), IF(lan.Debito>0, lan.Vencimento, lan.Vencimento ), ' +
  ' lan.Vencimento))))';
  end;
  Quitacao := 'Date(IF(car.Tipo <> 2, lan.Data,IF(lan.Sit > 1, lan.Compensado, ' +
  'IF(lan.Vencimento<SYSDATE(), IF(lan.Debito>0, ' + Fmt(TPDeb.Date) +
  ', ' + Fmt(TPCre.Date) + '), lan.Vencimento))))';
  //
  QrFlxo.Close;
  QrFlxo.SQL.Clear;
  if CkDocEmAberto.Checked then
    QrFlxo.SQL.Add('SELECT ' + Quitacao2 + ' QUITACAO, ')
  else
    QrFlxo.SQL.Add('SELECT ' + Quitacao + ' QUITACAO, ');
  QrFlxo.SQL.Add('lan.*, car.Nome NOMECART, ');
  QrFlxo.SQL.Add('IF(lan.Cliente<>0,');
  QrFlxo.SQL.Add('  IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome),');
  QrFlxo.SQL.Add('  IF (lan.Fornecedor<>0,');
  QrFlxo.SQL.Add('    IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome), "")) NOMERELACIONADO');
  QrFlxo.SQL.Add('FROM ' + VAR_LCT + ' lan');
  QrFlxo.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrFlxo.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=lan.Cliente');
  QrFlxo.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=lan.Fornecedor');
  //
  QrFlxo.SQL.Add('WHERE car.Fornecei=' + IntToStr(FEmpresa));
  QrFlxo.SQL.Add(dmkPF.SQL_Periodo('AND ' + Quitacao,
    TPEmissIni.Date, TPEmissFim.Date, CkEmissao.Checked, CkEmissao.Checked));
  QrFlxo.SQL.Add('AND lan.Genero > 0');
  QrFlxo.SQL.Add('AND ((car.Tipo<>2) OR (lan.Sit < 2))');
  //
  QrFlxo.SQL.Add('ORDER BY QUITACAO, Credito DESC, Debito');
  QrFlxo.Open;

  PB1.Max := QrFlxo.RecordCount + 1;

  // INSERE DADOS TABELA LOCAL
  UCriar.RecriaTempTable('extratocc2', DModG.QrUpdPID1, False);
  //
  Codig := 1;
  PB1.Position := Codig;
  PB1.Update;
  Application.ProcessMessages;
  //
  Saldo := QrMovantVALOR.Value;
  UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'ExtratoCC2', false,
  [
    'DataE', 'DataV', 'DataQ',
    'DataX', 'Texto', 'Docum',
    'NotaF', 'Credi', 'Debit',
    'Saldo', 'CartC', 'CartN',
    'TipoI', 'Ctrle', 'ID_Pg'
  ], ['Codig'], [
    Null, Null, Null,
    MLAGeral.FDT_NULL(DataI, 1), 'SALDO ANTERIOR', '',
    '', 0, 0,
    Saldo, 0, '*** T O D A S ***', 1, 0, 0
  ], [Codig], True);
  //
  QrFlxo.First;
  while not QrFlxo.Eof do
  begin
    Codig := Codig + 1;

    PB1.Position := Codig;
    PB1.Update;
    Application.ProcessMessages;

    Saldo := Saldo + QrFlxoCredito.Value - QrFlxoDebito.Value;
    DataE := MLAGeral.FDT_NULL(QrFlxoData.Value, 1);
    DataV := MLAGeral.FDT_NULL(QrFlxoVencimento.Value, 1);
    DataQ := MLAGeral.FDT_NULL(QrFlxoCompensado.Value, 1);
    DataX := QrFlxoQUITACAO.Value;
    //
    if CkSubstituir.Checked then
      Texto := QrFlxoNOMERELACIONADO.Value
    else
      Texto := QrFlxoDescricao.Value;
    //
    if QrFlxoQtde.Value > 0 then
      Texto := FloatToStr(QrFlxoQtde.Value) + ' ' + Texto;
    if QrFlxoDocumento.Value > 0 then
      Docum := FormatFloat('000000', QrFlxoDocumento.Value) else Docum := '';
    if QrFlxoSerieCH.Value <> '' then Docum := QrFlxoSerieCH.Value + Docum;
    if QrFlxoNotaFiscal.Value > 0 then
      NotaF := FormatFloat('000000', QrFlxoNotaFiscal.Value) else NotaF := '';
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'ExtratoCC2', false,
    [
      'DataE', 'DataV', 'DataQ',
      'DataX', 'Texto', 'Docum',
      'NotaF', 'Credi', 'Debit',
      'Saldo', 'CartC', 'CartN',
      'TipoI', 'Ctrle', 'ID_Pg', 'CtSub'
    ], ['Codig'], [
      DataE, DataV, DataQ,
      DataX, Texto, Docum,
      NotaF, QrFlxoCredito.Value, QrFlxoDebito.Value,
      Saldo, QrFlxoCarteira.Value, QrFlxoNOMECART.Value,
      2, QrFlxoControle.Value, QrFlxoID_Pgto.Value, QrFlxoSub.Value
    ], [Codig], True);
    //
    QrFlxo.Next;
  end;
  PB1.Visible := False;

  QrExtr.Close;
  QrExtr.Open;
  Screen.Cursor := crDefault;

  case RGFonte.ItemIndex of
    0: MyObjects.frxMostra(frxFin_Relat_005_01_A06, 'Relat�rio de Fluxo de Caixa');
    1: MyObjects.frxMostra(frxFin_Relat_005_01_A07, 'Relat�rio de Fluxo de Caixa');
    2: MyObjects.frxMostra(frxFin_Relat_005_01_A08, 'Relat�rio de Fluxo de Caixa');
  end;
end;


procedure TFmExtratos.R02_ImprimeContasAPagar(Sender: TObject;
 Saida: TTipoSaidaRelatorio);
var
  CtrlPai: Double;
  DockWindow: TFmDockForm;
  Maximo, Conta: Integer;
  RelFrx: TfrxReport;
begin
  VAR_CURSOR := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  VAR_ORDENAR := RGOrdem1.ItemIndex * 10 + RGOrdem2.ItemIndex;
  ////
  SQLContasAPagar(QrPagRecX,   0);
  if RGTipo.ItemIndex in ([4,5]) then
    // ERRADO! mudei 2008.07.22
    //SQLContasAPagar(QrPagRec,    0)
    SQLContasAPagar(QrPagRec,    3)
  else
    SQLContasAPagar(QrPagRec,    1);
  //MLAGeral.LeMeuSQL(QrPagRec, '', nil, True, True);
  SQLContasAPagar(QrPagRecIts, 2);
  if RGTipo.ItemIndex in ([6,7]) then
  begin
    DockWindow := DockWindows[(Sender as TComponent).Tag];
    DockWindow.Show;
    DockWindow.Refresh;
    UCriar.RecriaTempTable('PagRec1', DModG.QrUpdPID1, False);
    UCriar.RecriaTempTable('PagRec2', DModG.QrUpdPID1, False);
    Maximo := QrPagRecX.RecordCount;
    QrPagRecX.First;
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO pagrec1 SET ');
    DModG.QrUpdPID1.SQL.Add('Data=:P0, Documento=:P1, NotaFiscal=:P2, ');
    DModG.QrUpdPID1.SQL.Add('NOMEVENCIDO=:P3, NOMECONTA=:P4, Descricao=:P5, ');
    DModG.QrUpdPID1.SQL.Add('Controle=:P6, Valor=:P7, MoraDia=:P8, ');
    DModG.QrUpdPID1.SQL.Add('PAGO_REAL=:P9, ATRAZODD=:P10, ATUALIZADO=:P11, ');
    DModG.QrUpdPID1.SQL.Add('MULTA_REAL=:P12, DataDoc=:P13, Vencimento=:P14, ');
    DModG.QrUpdPID1.SQL.Add('TERCEIRO=:P15, NOME_TERCEIRO=:P16, CtrlPai=:P17, ');
    DModG.QrUpdPID1.SQL.Add('Tipo=:P18, Pendente=:P19, ID_Pgto=:P20, ');
    DModG.QrUpdPID1.SQL.Add('CtrlIni=:P21, Duplicata=:P22, Qtde=:P23, ');
    DModG.QrUpdPID1.SQL.Add('NO_UH=:P24 ');
    Screen.Cursor := VAR_CURSOR;
    Conta := 0;
    DockWindow.AtualizaDados(Conta, Maximo);
    while not QrPagRecX.Eof do
    begin
      Conta := Conta + 1;
      DockWindow.AtualizaDados(Conta, Maximo);
      Update;
      Application.ProcessMessages;
      if VAR_PARAR then
      begin
        UCriar.RecriaTempTable('PagRec1', DModG.QrUpdPID1, False);
        UCriar.RecriaTempTable('PagRec2', DModG.QrUpdPID1, False);
        Exit;
      end;
      if QrPagRecXCtrlIni.Value > 0 then
        CtrlPai := QrPagRecXCtrlIni.Value
      else CtrlPai := QrPagRecXControle.Value;
      DModG.QrUpdPID1.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, QrPagRecXData.Value);
      DModG.QrUpdPID1.Params[01].AsFloat   := QrPagRecXDocumento.Value;
      DModG.QrUpdPID1.Params[02].AsFloat   := QrPagRecXNotaFiscal.Value;
      DModG.QrUpdPID1.Params[03].AsString  := QrPagRecXNOMEVENCIDO.Value;
      DModG.QrUpdPID1.Params[04].AsString  := QrPagRecXNOMECONTA.Value;
      DModG.QrUpdPID1.Params[05].AsString  := QrPagRecXDescricao.Value;
      DModG.QrUpdPID1.Params[06].AsFloat   := QrPagRecXControle.Value;
      DModG.QrUpdPID1.Params[07].AsFloat   := QrPagRecXValor.Value;
      DModG.QrUpdPID1.Params[08].AsFloat   := QrPagRecXMoraDia.Value;
      DModG.QrUpdPID1.Params[09].AsFloat   := QrPagRecXPAGO_REAL.Value;
      DModG.QrUpdPID1.Params[10].AsFloat   := QrPagRecXATRAZODD.Value;
      DModG.QrUpdPID1.Params[11].AsFloat   := QrPagRecXATUALIZADO.Value;
      DModG.QrUpdPID1.Params[12].AsFloat   := QrPagRecXMULTA_REAL.Value;
      //
      DModG.QrUpdPID1.Params[13].AsString  := FormatDateTime(VAR_FORMATDATE, QrPagRecXDataDoc.Value);
      DModG.QrUpdPID1.Params[14].AsString  := FormatDateTime(VAR_FORMATDATE, QrPagRecXVencimento.Value);
      DModG.QrUpdPID1.Params[15].AsFloat   := QrPagRecXTerceiro.Value;
      DModG.QrUpdPID1.Params[16].AsString  := QrPagRecXNOME_TERCEIRO.Value;
      DModG.QrUpdPID1.Params[17].AsFloat   := CtrlPai;
      DModG.QrUpdPID1.Params[18].AsFloat   := QrPagRecXTipo.Value;
      DModG.QrUpdPID1.Params[19].AsFloat   := QrPagRecXPENDENTE.Value;
      DModG.QrUpdPID1.Params[20].AsFloat   := QrPagRecXID_Pgto.Value;
      DModG.QrUpdPID1.Params[21].AsFloat   := QrPagRecXCtrlIni.Value;
      DModG.QrUpdPID1.Params[22].AsString  := QrPagRecXDuplicata.Value;
      DModG.QrUpdPID1.Params[23].AsFloat   := QrPagRecXQtde.Value;
      DModG.QrUpdPID1.Params[24].AsString  := QrPagRecXNO_UH.Value;

      DModG.QrUpdPID1.ExecSQL;
      QrPagRecX.Next;
    end;
    DockWindow.Hide;
    with QrPagRec1.SQL do
    begin
      Clear;
      Add('SELECT * FROM pagrec1');
      Add(SQL_Ordenar());
{
      case VAR_ORDENAR of
        00: Add('ORDER BY NOME_TERCEIRO, DataDoc, CtrlPai, Data, Vencimento, Controle');
        01: Add('ORDER BY NOME_TERCEIRO, DataDoc, CtrlPai, Data, Vencimento, Controle');
        02: Add('ORDER BY NOME_TERCEIRO, Data, CtrlPai, DataDoc, Vencimento, Controle');
        03: Add('ORDER BY NOME_TERCEIRO, Vencimento, CtrlPai, DataDoc, Data, Controle');
        04: Add('ORDER BY NOME_TERCEIRO, Documento, Vencimento, CtrlPai, DataDoc, Data, Controle');
        05: Add('ORDER BY NOME_TERCEIRO, NotaFiscal, Vencimento, CtrlPai, DataDoc, Data, Controle');
        06: Add('ORDER BY NOME_TERCEIRO, Duplicata, NotaFiscal, Vencimento, CtrlPai, DataDoc, Data, Controle');
        07: Add('ORDER BY NOME_TERCEIRO, NO_UH, Vencimento, CtrlPai, DataDoc, Data, Controle');
        //
        10: Add('ORDER BY DataDoc, NOME_TERCEIRO, CtrlPai, Data, Vencimento, Controle');
        11: Add('ORDER BY DataDoc, NOME_TERCEIRO, CtrlPai, Data, Vencimento, Controle');
        12: Add('ORDER BY DataDoc, Data, CtrlPai, NOME_TERCEIRO, Vencimento, Controle');
        13: Add('ORDER BY DataDoc, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle');
        14: Add('ORDER BY DataDoc, Documento, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle');
        15: Add('ORDER BY DataDoc, NotaFiscal, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle');
        16: Add('ORDER BY DataDoc, Duplicata, NotaFiscal, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle');
        17: Add('ORDER BY DataDoc, NO_UH, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle');
        //
        20: Add('ORDER BY Data, NOME_TERCEIRO, CtrlPai, DataDoc, Vencimento, Controle');
        21: Add('ORDER BY Data, DataDoc, CtrlPai, NOME_TERCEIRO, Vencimento, Controle');
        22: Add('ORDER BY Data, NOME_TERCEIRO, CtrlPai, DataDoc, Vencimento, Controle');
        23: Add('ORDER BY Data, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle');
        24: Add('ORDER BY Data, Documento, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle');
        25: Add('ORDER BY Data, NotaFiscal, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle');
        26: Add('ORDER BY Data, Duplicata, NotaFiscal, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle');
        27: Add('ORDER BY Data, NO_UH, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle');
        //
        30: Add('ORDER BY Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle');
        31: Add('ORDER BY Vencimento, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle');
        32: Add('ORDER BY Vencimento, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle');
        33: Add('ORDER BY Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle');
        34: Add('ORDER BY Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle');
        35: Add('ORDER BY Vencimento, NotaFiscal, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle');
        36: Add('ORDER BY Vencimento, Duplicata,NotaFiscal, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle');
        37: Add('ORDER BY Vencimento, NO_UH, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle');
        //
        40: Add('ORDER BY Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle');
        41: Add('ORDER BY Documento, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle');
        42: Add('ORDER BY Documento, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle');
        43: Add('ORDER BY Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle');
        44: Add('ORDER BY Documento, Controle, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data');
        45: Add('ORDER BY Documento, NotaFiscal, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data');
        46: Add('ORDER BY Documento, Duplicata, NotaFiscal, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data');
        47: Add('ORDER BY Documento, NO_UH, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data');
        //
        50: Add('ORDER BY NotaFiscal, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle');
        51: Add('ORDER BY NotaFiscal, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle');
        52: Add('ORDER BY NotaFiscal, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle');
        53: Add('ORDER BY NotaFiscal, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle');
        54: Add('ORDER BY NotaFiscal, Controle, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data');
        55: Add('ORDER BY NotaFiscal, Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data');
        56: Add('ORDER BY NotaFiscal, Duplicata, Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data');
        57: Add('ORDER BY NotaFiscal, NO_UH, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data');
        //
        60: Add('ORDER BY Duplicata, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle');
        61: Add('ORDER BY Duplicata, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle');
        62: Add('ORDER BY Duplicata, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle');
        63: Add('ORDER BY Duplicata, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle');
        64: Add('ORDER BY Duplicata, Controle, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data');
        65: Add('ORDER BY Duplicata, Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data');
        66: Add('ORDER BY Duplicata, Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data');
        67: Add('ORDER BY Duplicata, NO_UH, Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data');
        //
        70: Add('ORDER BY NO_UH, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle');
        71: Add('ORDER BY NO_UH, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle');
        72: Add('ORDER BY NO_UH, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle');
        73: Add('ORDER BY NO_UH, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle');
        74: Add('ORDER BY NO_UH, Controle, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data');
        75: Add('ORDER BY NO_UH, Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data');
        76: Add('ORDER BY NO_UH, Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data');
        77: Add('ORDER BY NO_UH, Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data');
        //
      end;
}
    end;
    QrPagRec1.Database := DModG.MyPID_DB;
    QrPagRec1.Open;
    case RGHistorico.ItemIndex of
      0: Relfrx := frxFin_Relat_005_02_B1;
      1: Relfrx := frxFin_Relat_005_02_B2;
      else Relfrx := nil;
    end;
  end else begin
    case RGFonte.ItemIndex of
      0: RelFrx := frxFin_Relat_005_02_A06;
      1: RelFrx := frxFin_Relat_005_02_A07;
      2: RelFrx := frxFin_Relat_005_02_A08;
      3: RelFrx := frxPagRec1;
      else RelFrx := nil;
    end;
  end;
  if Relfrx <> nil then
    MyObjects.frxMostra(Relfrx, 'Hist�rico de lan�amentos')
  else
    Geral.MensagemBox('Relat�rio n�o definido!', 'Erro', MB_OK+MB_ICONERROR);
  QrPagRec1.Close;
  QrPagRec.Close;
  QrPagRecIts.Close;
  Screen.Cursor := VAR_CURSOR;
end;

procedure TFmExtratos.SQLContasAPagar(Query: TmySQLQuery; Tipo: Integer);
var
  Anos, Meses, Dias: Word;
begin
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT la.*, co.Nome NOMECONTA, ca.Nome NOMECARTEIRA,');
  if (RGTipo.ItemIndex in [4,5]) and (RGFonte.ItemIndex in [0, 1, 2]) then
  begin
    Query.SQL.Add('IF((la.ID_Pgto = 0), (IF(la.Sit=0, (la.Credito-la.Debito), ');
    Query.SQL.Add('IF(la.Sit=1, (la.Credito-la.Debito-la.Pago), 0))), ');
    Query.SQL.Add('(lz.Credito-lz.Debito-lz.Pago)) SALDO,');
  end else
  begin
    Query.SQL.Add('IF(la.Sit=0, (la.Credito-la.Debito),');
    Query.SQL.Add('IF(la.Sit=1, (la.Credito-la.Debito-la.Pago), 0)) SALDO,');
  end;
  {
  Query.SQL.Add('IF(la.Sit=0, (la.Credito-la.Debito),');
  Query.SQL.Add('IF(la.Sit=1, (la.Credito-la.Debito-la.Pago), 0)) SALDO,');
  }
  Query.SQL.Add('co.Credito EhCRED, co.Debito EhDEB, ');
  Query.SQL.Add('IF ((la.Fornecedor>0) AND (fo.Tipo=0), fo.RazaoSocial, ');
  Query.SQL.Add('IF ((la.Fornecedor>0) AND (fo.Tipo=1), fo.Nome, ');
  Query.SQL.Add('IF ((la.Cliente>0) AND (cl.Tipo=0), cl.RazaoSocial, ');
  Query.SQL.Add('IF ((la.Cliente>0) AND (cl.Tipo=1), cl.Nome, "???")))) NOME_TERCEIRO, ');
  Query.SQL.Add('CASE WHEN la.Fornecedor>0 then la.Fornecedor ELSE la.Cliente END TERCEIRO,');
  Query.SQL.Add('0 CtrlPai, ');
  if VAR_KIND_DEPTO = kdUH then
    Query.SQL.Add('imv.Unidade NO_UH, ')
  else
    Query.SQL.Add('"" NO_UH, ');
  if (RGTipo.ItemIndex in [4,5]) and (RGFonte.ItemIndex in [0, 1, 2]) then
  begin
    Query.SQL.Add('IF(la.ID_Pgto = 0, la.Credito, lz.Credito) CRED, lz.Credito,');
    Query.SQL.Add('IF(la.ID_Pgto = 0, la.Debito, lz.Debito) DEBI, lz.Debito');
  end else
    Query.SQL.Add('la.Credito CRED, la.Debito DEBI');
  Query.SQL.Add('FROM ' + VAR_LCT + ' la, contas co, carteiras ca, entidades cl, entidades fo');
  if VAR_KIND_DEPTO = kdUH then
    Query.SQL.Add(',condimov imv');
  if (RGTipo.ItemIndex in [4,5]) and (RGFonte.ItemIndex in [0, 1, 2]) then
    Query.SQL.Add('LEFT JOIN ' + VAR_LCT + ' lz ON lz.Controle = la.ID_Pgto');
  Query.SQL.Add('WHERE fo.Codigo=la.Fornecedor AND cl.Codigo=la.Cliente');
  Query.SQL.Add('AND la.Genero<>-1');
  Query.SQL.Add('AND la.CliInt=' + Geral.FF0(DModG.QrEmpresasCodigo.Value));

  //  2010-08-02 - N�o usa mais Genero=-5 > Outras carteiras!
  Query.SQL.Add('AND ( la.Tipo<>2 OR (la.Tipo=2 AND la.Sit < 2) )');
  // fim 2010-08-02
  Query.SQL.Add('AND ca.ForneceI =' + FormatFloat('0', FEmpresa));

  //if RGTipo.ItemIndex in ([6,7]) then
  if CkEmissao.Checked then
    Query.SQL.Add('AND la.Data BETWEEN "'+Ext_EmisI+'" AND "'+Ext_EmisF+'"');
  //else
  if CkVencto.Checked then
    Query.SQL.Add('AND la.Vencimento BETWEEN "'+Ext_VctoI+'" AND "'+Ext_VctoF+'"');
  if CkDataDoc.Checked then
    Query.SQL.Add('AND la.DataDoc BETWEEN "'+Ext_DocI+'" AND "'+Ext_DocF+'"');
  if CkDataComp.Checked then
  begin
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('UPDATE ' + VAR_LCT + ' SET Compensado=Data');
    Dmod.QrUpdU.SQL.Add('WHERE Tipo<>2');
    Dmod.QrUpdU.ExecSQL;
    Query.SQL.Add('AND la.Compensado BETWEEN "'+Ext_CompI+'" AND "'+Ext_CompF+'"');
  end;
  if RGTipo.ItemIndex in ([2,3]) then
  begin
    Query.SQL.Add('AND la.Tipo=2');
    Query.SQL.Add('AND la.Sit<2');
  end else
  if RGTipo.ItemIndex in ([5]) then
  begin
    //Query.SQL.Add('AND la.Tipo=2');
    Query.SQL.Add('AND la.Sit>1');
  end;
  Query.SQL.Add('AND co.Codigo=la.Genero');
  Query.SQL.Add('AND ca.Codigo=la.Carteira');
  if VAR_KIND_DEPTO = kdUH then
    Query.SQL.Add('AND la.Depto=imv.Conta');

  if RGTipo.ItemIndex in ([2,4,6]) then
  begin
    Query.SQL.Add('AND la.Debito>0');
    if CBTerceiro.KeyValue <> NULL then
      Query.SQL.Add('AND la.Fornecedor ='+IntToStr(CBTerceiro.KeyValue));
  end else if RGTipo.ItemIndex in ([3,5,7]) then
  begin
    Query.SQL.Add('AND la.Credito>0');
    if CBTerceiro.KeyValue <> NULL then
      Query.SQL.Add('AND la.Cliente='+IntToStr(CBTerceiro.KeyValue));
  end;
  if RGTipo.ItemIndex in ([3,7]) then
  begin
    if CBAccount.KeyValue <> NULL then
      Query.SQL.Add('AND la.Account ='+IntToStr(CBAccount.KeyValue));
    if CBVendedor.KeyValue <> NULL then
      Query.SQL.Add('AND la.Vendedor ='+IntToStr(CBVendedor.KeyValue));
  end;
  if CkNiveis.Checked then
  begin
    if RGTipo.ItemIndex in ([2,4,6]) then
    Query.SQL.Add('AND fo.Nivel BETWEEN "'+EdA.Text+'" AND "'+EdZ.Text+'"');
    if RGTipo.ItemIndex in ([3,5,7]) then
    Query.SQL.Add('AND cl.Nivel BETWEEN "'+EdA.Text+'" AND "'+EdZ.Text+'"');
  end;
  if CkOmiss.Checked then
  begin
    if CkAnos.Checked  then Anos :=  Geral.IMV(EdAnos.Text)  else Anos := 0;
    if CkMeses.Checked then Meses := Geral.IMV(EdMeses.Text) else Meses := 0;
    if CkDias.Checked  then Dias :=  Geral.IMV(EdDias.Text)  else Dias := 0;
    Ext_Vencto := IncMonth(Date, -1 * ((Anos*12)+Meses)) - Dias;
    Query.SQL.Add('AND la.Vencimento >= "'+
    FormatDateTime(VAR_FORMATDATE, Ext_Vencto)+'"');
  end;
  if RGTipo.ItemIndex in ([8,9]) then
  begin
    if CkAnos2.Checked  then Anos :=  Geral.IMV(EdAnos2.Text)  else Anos := 0;
    if CkMeses2.Checked then Meses := Geral.IMV(EdMeses2.Text) else Meses := 0;
    if CkDias2.Checked  then Dias :=  Geral.IMV(EdDias2.Text)  else Dias := 0;
    Ext_Vencto := IncMonth(Date, -1 * ((Anos*12)+Meses)) - Dias;
    Query.SQL.Add('AND la.Vencimento < "'+
    FormatDateTime(VAR_FORMATDATE, Ext_Vencto)+'"');
  end;
  //
  if RGTipo.ItemIndex in ([2,3,4,5,6,7,8,9]) then
  begin
    if CBConta.KeyValue <> NULL then
      Query.SQL.Add('AND la.Genero ='+IntToStr(CBConta.KeyValue));
  end;
  case Tipo of
    0: Query.SQL.Add('');
    1: Query.SQL.Add('AND la.ID_Pgto=0');
    2: Query.SQL.Add('AND la.ID_Pgto<>0');
    // Novo 2008.07.22
    3: Query.SQL.Add('AND la.Compensado>0');
  end;
  Query.SQL.Add(SQL_Ordenar());
  {
  case VAR_ORDENAR of
    00: Query.SQL.Add('ORDER BY NOME_TERCEIRO, DataDoc, Data, Vencimento, Carteira, Controle');
    01: Query.SQL.Add('ORDER BY NOME_TERCEIRO, DataDoc, Data, Vencimento, Carteira, Controle');
    02: Query.SQL.Add('ORDER BY NOME_TERCEIRO, Data, DataDoc, Vencimento, Carteira, Controle');
    03: Query.SQL.Add('ORDER BY NOME_TERCEIRO, Vencimento, DataDoc, Data, Carteira, Controle');
    //
    10: Query.SQL.Add('ORDER BY DataDoc, NOME_TERCEIRO, Data, Vencimento, Carteira, Controle');
    11: Query.SQL.Add('ORDER BY DataDoc, NOME_TERCEIRO, Data, Vencimento, Carteira, Controle');
    12: Query.SQL.Add('ORDER BY DataDoc, Data, NOME_TERCEIRO, Vencimento, Carteira, Controle');
    13: Query.SQL.Add('ORDER BY DataDoc, Vencimento, NOME_TERCEIRO, Data, Carteira, Controle');
    //
    20: Query.SQL.Add('ORDER BY Data, NOME_TERCEIRO, DataDoc, Vencimento, Carteira, Controle');
    21: Query.SQL.Add('ORDER BY Data, DataDoc, NOME_TERCEIRO, Vencimento, Carteira, Controle');
    22: Query.SQL.Add('ORDER BY Data, NOME_TERCEIRO, DataDoc, Vencimento, Carteira, Controle');
    23: Query.SQL.Add('ORDER BY Data, Vencimento, NOME_TERCEIRO, DataDoc, Carteira, Controle');
    //
    30: Query.SQL.Add('ORDER BY Vencimento, NOME_TERCEIRO, DataDoc, Data, Carteira, Controle');
    31: Query.SQL.Add('ORDER BY Vencimento, DataDoc, NOME_TERCEIRO, Data, Carteira, Controle');
    32: Query.SQL.Add('ORDER BY Vencimento, Data, NOME_TERCEIRO, DataDoc, Carteira, Controle');
    33: Query.SQL.Add('ORDER BY Vencimento, NOME_TERCEIRO, DataDoc, Data, Carteira, Controle');
    //
  end;
  }
  Query.Open;
  //
end;

function TFmExtratos.SQL_Ordenar(): String;
begin
  case VAR_ORDENAR of
    00: Result := 'ORDER BY NOME_TERCEIRO, DataDoc, CtrlPai, Data, Vencimento, Controle';
    01: Result := 'ORDER BY NOME_TERCEIRO, DataDoc, CtrlPai, Data, Vencimento, Controle';
    02: Result := 'ORDER BY NOME_TERCEIRO, Data, CtrlPai, DataDoc, Vencimento, Controle';
    03: Result := 'ORDER BY NOME_TERCEIRO, Vencimento, CtrlPai, DataDoc, Data, Controle';
    04: Result := 'ORDER BY NOME_TERCEIRO, Documento, Vencimento, CtrlPai, DataDoc, Data, Controle';
    05: Result := 'ORDER BY NOME_TERCEIRO, NotaFiscal, Vencimento, CtrlPai, DataDoc, Data, Controle';
    06: Result := 'ORDER BY NOME_TERCEIRO, Duplicata, NotaFiscal, Vencimento, CtrlPai, DataDoc, Data, Controle';
    07: Result := 'ORDER BY NOME_TERCEIRO, NO_UH, Vencimento, CtrlPai, DataDoc, Data, Controle';
    //
    10: Result := 'ORDER BY DataDoc, NOME_TERCEIRO, CtrlPai, Data, Vencimento, Controle';
    11: Result := 'ORDER BY DataDoc, NOME_TERCEIRO, CtrlPai, Data, Vencimento, Controle';
    12: Result := 'ORDER BY DataDoc, Data, CtrlPai, NOME_TERCEIRO, Vencimento, Controle';
    13: Result := 'ORDER BY DataDoc, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle';
    14: Result := 'ORDER BY DataDoc, Documento, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle';
    15: Result := 'ORDER BY DataDoc, NotaFiscal, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle';
    16: Result := 'ORDER BY DataDoc, Duplicata, NotaFiscal, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle';
    17: Result := 'ORDER BY DataDoc, NO_UH, Vencimento, CtrlPai, NOME_TERCEIRO, Data, Controle';
    //
    20: Result := 'ORDER BY Data, NOME_TERCEIRO, CtrlPai, DataDoc, Vencimento, Controle';
    21: Result := 'ORDER BY Data, DataDoc, CtrlPai, NOME_TERCEIRO, Vencimento, Controle';
    22: Result := 'ORDER BY Data, NOME_TERCEIRO, CtrlPai, DataDoc, Vencimento, Controle';
    23: Result := 'ORDER BY Data, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    24: Result := 'ORDER BY Data, Documento, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    25: Result := 'ORDER BY Data, NotaFiscal, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    26: Result := 'ORDER BY Data, Duplicata, NotaFiscal, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    27: Result := 'ORDER BY Data, NO_UH, Vencimento, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    //
    30: Result := 'ORDER BY Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    31: Result := 'ORDER BY Vencimento, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle';
    32: Result := 'ORDER BY Vencimento, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    33: Result := 'ORDER BY Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    34: Result := 'ORDER BY Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    35: Result := 'ORDER BY Vencimento, NotaFiscal, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    36: Result := 'ORDER BY Vencimento, Duplicata,NotaFiscal, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    37: Result := 'ORDER BY Vencimento, NO_UH, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    //
    40: Result := 'ORDER BY Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    41: Result := 'ORDER BY Documento, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle';
    42: Result := 'ORDER BY Documento, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    43: Result := 'ORDER BY Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    44: Result := 'ORDER BY Documento, Controle, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    45: Result := 'ORDER BY Documento, NotaFiscal, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    46: Result := 'ORDER BY Documento, Duplicata, NotaFiscal, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    47: Result := 'ORDER BY Documento, NO_UH, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    //
    50: Result := 'ORDER BY NotaFiscal, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    51: Result := 'ORDER BY NotaFiscal, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle';
    52: Result := 'ORDER BY NotaFiscal, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    53: Result := 'ORDER BY NotaFiscal, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    54: Result := 'ORDER BY NotaFiscal, Controle, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    55: Result := 'ORDER BY NotaFiscal, Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    56: Result := 'ORDER BY NotaFiscal, Duplicata, Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    57: Result := 'ORDER BY NotaFiscal, NO_UH, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    //
    60: Result := 'ORDER BY Duplicata, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    61: Result := 'ORDER BY Duplicata, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle';
    62: Result := 'ORDER BY Duplicata, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    63: Result := 'ORDER BY Duplicata, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    64: Result := 'ORDER BY Duplicata, Controle, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    65: Result := 'ORDER BY Duplicata, Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    66: Result := 'ORDER BY Duplicata, Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    67: Result := 'ORDER BY Duplicata, NO_UH, Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    //
    70: Result := 'ORDER BY NO_UH, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    71: Result := 'ORDER BY NO_UH, DataDoc, CtrlPai, NOME_TERCEIRO, Data, Controle';
    72: Result := 'ORDER BY NO_UH, Data, CtrlPai, NOME_TERCEIRO, DataDoc, Controle';
    73: Result := 'ORDER BY NO_UH, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data, Controle';
    74: Result := 'ORDER BY NO_UH, Controle, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    75: Result := 'ORDER BY NO_UH, Documento, Vencimento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    76: Result := 'ORDER BY NO_UH, Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    77: Result := 'ORDER BY NO_UH, Vencimento, Documento, NOME_TERCEIRO, CtrlPai, DataDoc, Data';
    //
    else Result := '';
  end;
end;

procedure TFmExtratos.QrExtrBeforeOpen(DataSet: TDataSet);
begin
  QrLct.Close;
  QrLct.Params[0].AsInteger := FEmpresa;
  QrLct.Open;
end;

procedure TFmExtratos.QrExtrCalcFields(DataSet: TDataSet);
begin
  QrExtrVALOR.Value := QrExtrCredi.Value - QrExtrDebit.Value;
end;

procedure TFmExtratos.QrFluxoCalcFields(DataSet: TDataSet);
begin
//  if QrFluxoVencimento.Value < Date then
//    QrFluxoVENCER.Value := Date
//  else
  QrFluxoVENCER.Value := QrFluxoVencimento.Value;
  if QrFluxoVencimento.Value < Date then
    QrFluxoVENCIDO.Value := QrFluxoVencimento.Value;
  if QrFluxoVencimento.Value < Date then
    QrFluxoNOMEVENCIDO.Value := FormatDateTime(VAR_FORMATDATE3, QrFluxoVencimento.Value)
  else QrFluxoNOMEVENCIDO.Value := CO_VAZIO;
end;

procedure TFmExtratos.QrMovantCalcFields(DataSet: TDataSet);
begin
  QrMovantVALOR.Value := QrMovantMovim.Value + QrIniValor.Value;
end;

procedure TFmExtratos.CBTerceiroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_DELETE then CBTerceiro.KeyValue := NULL;
end;

procedure TFmExtratos.QrPagRecCalcFields(DataSet: TDataSet);
var
  MoraDia: Double;
  SerDoc: String;
begin
  QrPagRecVENCER.Value := QrPagRecVencimento.Value;
  //QrPagRecVALOR.Value := QrPagRecCredito.Value - QrPagRecDebito.Value;
  QrPagRecVALOR.Value := QrPagRecCRED.Value - QrPagRecDEBI.Value; 
  ///////////////////////////////
  QrPagRecVENCIDO.Value := 0;
  if (QrPagRecVencimento.Value < Date) and (QrPagRecTipo.Value <> 1) then
    if QrPagRecSit.Value < 2 then
      QrPagRecVENCIDO.Value := QrPagRecVencimento.Value;
  ///////////////////////////////
  if (RGTipo.ItemIndex in [4, 5]) and (RGFonte.ItemIndex in [0, 1, 2]) then
  begin
     QrPagRecATRAZODD.Value   := 0;
     QrPagRecMULTA_REAL.Value := QrPagRecMultaVal.Value;
  end else
  begin
    if QrPagRecVENCIDO.Value > 0 then
    begin
       QrPagRecATRAZODD.Value := Trunc(Date) - QrPagRecVENCIDO.Value;
       QrPagRecMULTA_REAL.Value := QrPagRecMulta.Value / 100 * QrPagRecVALOR.Value;
    end else begin
       QrPagRecATRAZODD.Value := 0;
       QrPagRecMULTA_REAL.Value := 0;
    end;
  end;
  {
  if QrPagRecVENCIDO.Value > 0 then
  begin
     QrPagRecATRAZODD.Value := Trunc(Date) - QrPagRecVENCIDO.Value;
     QrPagRecMULTA_REAL.Value := QrPagRecMulta.Value / 100 * QrPagRecVALOR.Value;
  end else begin
     QrPagRecATRAZODD.Value := 0;
     QrPagRecMULTA_REAL.Value := 0;
  end;
  }
  if (QrPagRecEhCRED.Value = 'V') and (QrPagRecEhDEB.Value = 'F') then
    MoraDia := QrPagRecMoraDia.Value
  else if (QrPagRecEhCRED.Value = 'F') and (QrPagRecEhDEB.Value = 'V') then
    MoraDia := QrPagRecMoraDia.Value else MoraDia := 0;
  if (RGTipo.ItemIndex = 3) and (RGFonte.ItemIndex in [0, 1, 2]) then
    QrPagRecATUALIZADO.Value := QrPagRecSALDO.Value
  else
    QrPagRecATUALIZADO.Value := QrPagRecSALDO.Value + (QrPagRecSALDO.Value *
      (QrPagRecATRAZODD.Value / 30) * MoraDia / 100) + QrPagRecMULTA_REAL.Value;
  {
  QrPagRecATUALIZADO.Value := QrPagRecSALDO.Value + (QrPagRecSALDO.Value *
  (QrPagRecATRAZODD.Value / 30) * MoraDia / 100) + QrPagRecMULTA_REAL.Value;
  }
  ///////////////////////////////
  if QrPagRecTipo.Value = 1 then
  begin
    QrPagRecPAGO_REAL.Value := QrPagRecCredito.Value - QrPagRecDebito.Value;
  end else if QrPagRecTipo.Value = 0 then
  begin
    if QrPagRecVENCIDO.Value > 0 then
      QrPagRecPAGO_REAL.Value := QrPagRecCredito.Value - QrPagRecDebito.Value
    else
      QrPagRecPAGO_REAL.Value := 0;
  end else
  begin
    QrPagRecPAGO_REAL.Value := QrPagRecPago.Value;
  end;
  {
  if QrPagRecTipo.Value = 1 then
  begin
    QrPagRecPAGO_REAL.Value := QrPagRecVALOR.Value;
  end else if QrPagRecTipo.Value = 0 then
  begin
    if QrPagRecVENCIDO.Value > 0 then
    QrPagRecPAGO_REAL.Value := QrPagRecVALOR.Value
    else QrPagRecPAGO_REAL.Value := 0;
  end else
  begin
    QrPagRecPAGO_REAL.Value := QrPagRecPago.Value;
  end;
  }
  ///////////////////////////////
  if QrPagRecVencimento.Value < Date then
    QrPagRecNOMEVENCIDO.Value := FormatDateTime(VAR_FORMATDATE3, QrPagRecVencimento.Value)
  else QrPagRecNOMEVENCIDO.Value := CO_VAZIO;
  //////////////////////////
  if QrPagRecDocumento.Value > 0 then
    SerDoc := FormatFloat('000000', QrPagRecDocumento.Value) else SerDoc := '';
  if Length(QrPagRecSerieCH.Value) > 0 then SerDoc := QrPagRecSerieCH.Value + SerDoc;
  QrPagRecSERIEDOC.Value := SerDoc;
end;

procedure TFmExtratos.CkOmissClick(Sender: TObject);
begin
  GBOmiss.Visible := CkOmiss.Checked;
end;

procedure TFmExtratos.BtSalvarClick(Sender: TObject);
begin
  Geral.WriteAppKeyCU('Anos',  'Dermatek\Extratos\Venctos', EdAnos.Text,  ktString);
  Geral.WriteAppKeyCU('Meses', 'Dermatek\Extratos\Venctos', EdMeses.Text, ktString);
  Geral.WriteAppKeyCU('Dias',  'Dermatek\Extratos\Venctos', EdDias.Text,  ktString);
  /////
  Geral.WriteAppKeyCU('Ck_Anos',  'Dermatek\Extratos\Venctos', CkAnos.Checked,  ktBoolean);
  Geral.WriteAppKeyCU('Ck_Meses', 'Dermatek\Extratos\Venctos', CkMeses.Checked, ktBoolean);
  Geral.WriteAppKeyCU('Ck_Dias',  'Dermatek\Extratos\Venctos', CkDias.Checked,  ktBoolean);
end;

procedure TFmExtratos.BtGravarClick(Sender: TObject);
begin
  Geral.WriteAppKeyCU('Anos',  'Dermatek\Extratos\Perdidos', EdAnos2.Text,  ktString);
  Geral.WriteAppKeyCU('Meses', 'Dermatek\Extratos\Perdidos', EdMeses2.Text, ktString);
  Geral.WriteAppKeyCU('Dias',  'Dermatek\Extratos\Perdidos', EdDias2.Text,  ktString);
  /////
  Geral.WriteAppKeyCU('Ck_Anos',  'Dermatek\Extratos\Perdidos', CkAnos2.Checked,  ktBoolean);
  Geral.WriteAppKeyCU('Ck_Meses', 'Dermatek\Extratos\Perdidos', CkMeses2.Checked, ktBoolean);
  Geral.WriteAppKeyCU('Ck_Dias',  'Dermatek\Extratos\Perdidos', CkDias2.Checked,  ktBoolean);
end;

procedure TFmExtratos.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmExtratos.QrPagRecItsCalcFields(DataSet: TDataSet);
var
  MoraDia: Double;
begin
  QrPagRecItsVENCER.Value := QrPagRecItsVencimento.Value;
  QrPagRecItsVALOR.Value := QrPagRecItsCredito.Value - QrPagRecItsDebito.Value;
  ///////////////////////////////
  QrPagRecItsVENCIDO.Value := 0;
  if (QrPagRecItsVencimento.Value < Date) and (QrPagRecItsTipo.Value <> 1) then
    if QrPagRecItsSit.Value < 2 then
      QrPagRecItsVENCIDO.Value := QrPagRecItsVencimento.Value;
  ///////////////////////////////
  if QrPagRecItsVENCIDO.Value > 0 then
//  begin
  begin
     QrPagRecItsATRAZODD.Value := Trunc(Date) - QrPagRecItsVENCIDO.Value;
     QrPagRecItsMULTA_REAL.Value := QrPagRecItsMulta.Value / 100 * QrPagRecItsVALOR.Value;
  end else begin
     QrPagRecItsATRAZODD.Value := 0;
     QrPagRecItsMULTA_REAL.Value := 0;
  end;
  if (QrPagRecItsEhCRED.Value = 'V') and (QrPagRecItsEhDEB.Value = 'F') then
    MoraDia := QrPagRecItsMoraDia.Value
  else if (QrPagRecItsEhCRED.Value = 'F') and (QrPagRecItsEhDEB.Value = 'V') then
    MoraDia := QrPagRecItsMoraDia.Value else MoraDia := 0;
  QrPagRecItsATUALIZADO.Value := QrPagRecItsSALDO.Value + (QrPagRecItsSALDO.Value *
  (QrPagRecItsATRAZODD.Value / 30) * MoraDia / 100) + QrPagRecItsMULTA_REAL.Value;
  ///////////////////////////////
  if QrPagRecItsTipo.Value = 1 then
  begin
    QrPagRecItsPAGO_REAL.Value := QrPagRecItsVALOR.Value;
  end else if QrPagRecItsTipo.Value = 0 then
  begin
    if QrPagRecItsVENCIDO.Value > 0 then
    QrPagRecItsPAGO_REAL.Value := QrPagRecItsVALOR.Value
    else QrPagRecItsPAGO_REAL.Value := 0;
  end else
  begin
    QrPagRecItsPAGO_REAL.Value := QrPagRecItsPago.Value;
  end;
  ///////////////////////////////
  if QrPagRecItsVencimento.Value < Date then
    QrPagRecItsNOMEVENCIDO.Value := FormatDateTime(VAR_FORMATDATE3, QrPagRecItsVencimento.Value)
  else QrPagRecItsNOMEVENCIDO.Value := CO_VAZIO;
  //////////////////////////
end;

procedure TFmExtratos.QrPagRec1CalcFields(DataSet: TDataSet);
var
  PagRec: String;
begin
  case Trunc(QrPagRec1Tipo.Value) of
    0: PagRec := ' Pago';
    1: PagRec := ' Depositado';
    2: PagRec := ' Rolado';
  end;
  PagRec := PagRec + ' ['+FormatFloat('000000', QrPagRec1ID_Pgto.Value)+ '] ';
  QrPagRec1PAGO_ROLADO.Value := PagRec;
  /////////////////////////////////////
  QrPagRec1DEVIDO.Value     := QrPagRec1ATUALIZADO.Value;
  QrPagRec1PEND_TOTAL.Value := QrPagRec1ATUALIZADO.Value;
end;

procedure TFmExtratos.CreateDockableWindows;
var
  I: Integer;
begin
  for I := 0 to High(DockWindows) do
  begin
    DockWindows[I] := TFmDockForm.Create(Application);
    DockWindows[I].Caption := 'C�lculos em Progresso';
//    DockWindows[I].Memo1.Color := Colors[I];
//    DockWindows[I].Memo1.Font.Color := Colors[I] xor $00FFFFFF;
//    DockWindows[I].Memo1.Text := ColStr[I] + ' window ';
  end;
end;

procedure TFmExtratos.QrPagRecXCalcFields(DataSet: TDataSet);
var
  MoraDia: Double;
begin
  QrPagRecXVENCER.Value := QrPagRecXVencimento.Value;
  QrPagRecXVALOR.Value := QrPagRecXCredito.Value - QrPagRecXDebito.Value;
  ///////////////////////////////
  QrPagRecXVENCIDO.Value := 0;
  if (QrPagRecXVencimento.Value < Date) and (QrPagRecXTipo.Value <> 1) then
    if QrPagRecXSit.Value < 2 then
      QrPagRecXVENCIDO.Value := QrPagRecXVencimento.Value;
  ///////////////////////////////
  if QrPagRecXVENCIDO.Value > 0 then
//  begin
  begin
     QrPagRecXATRAZODD.Value := Trunc(Date) - QrPagRecXVENCIDO.Value;
     QrPagRecXMULTA_REAL.Value := QrPagRecXMulta.Value / 100 * QrPagRecXVALOR.Value;
  end else begin
     QrPagRecXATRAZODD.Value := 0;
     QrPagRecXMULTA_REAL.Value := 0;
  end;
  if (QrPagRecXEhCRED.Value = 'V') and (QrPagRecXEhDEB.Value = 'F') then
    MoraDia := QrPagRecXMoraDia.Value
  else if (QrPagRecXEhCRED.Value = 'F') and (QrPagRecXEhDEB.Value = 'V') then
    MoraDia := QrPagRecXMoraDia.Value
  else if (QrPagRecXID_Pgto.Value <> QrPagRecXControle.Value) then
    MoraDia := QrPagRecXMoraDia.Value
  else MoraDia := 0;
  QrPagRecXATUALIZADO.Value := QrPagRecXSALDO.Value + (QrPagRecXSALDO.Value *
  (QrPagRecXATRAZODD.Value / 30) * MoraDia / 100) + QrPagRecXMULTA_REAL.Value;
  ///////////////////////////////
  if QrPagRecXTipo.Value = 1 then
  begin
    QrPagRecXPAGO_REAL.Value := QrPagRecXVALOR.Value;
  end else if QrPagRecXTipo.Value = 0 then
  begin
    if QrPagRecXVENCIDO.Value > 0 then
    QrPagRecXPAGO_REAL.Value := QrPagRecXVALOR.Value
    else QrPagRecXPAGO_REAL.Value := 0;
  end else
  begin
    if QrPagRecXCompensado.Value > 0 then
      QrPagRecXPAGO_REAL.Value := QrPagRecXVALOR.Value
    else QrPagRecXPAGO_REAL.Value := QrPagRecXPago.Value;
  end;
  QrPagRecXPENDENTE.Value := QrPagRecXVALOR.Value - QrPagRecXPAGO_REAL.Value;
  ///////////////////////////////
  if QrPagRecXVencimento.Value < Date then
    QrPagRecXNOMEVENCIDO.Value := FormatDateTime(VAR_FORMATDATE3, QrPagRecXVencimento.Value)
  else QrPagRecXNOMEVENCIDO.Value := CO_VAZIO;
  //////////////////////////
end;

procedure TFmExtratos.BtEMailClick(Sender: TObject);
var
  Executavel: String;
begin
  if RGTipo.ItemIndex < 2 then
  begin
    Application.MessageBox('Em constru��o', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Executavel := 'C:\Dermatek\CR\Relatorio.zip';
  GeraRelatorio(tsrExportRTF);
  Application.CreateForm(TFmEnvia, FmEnvia);
  with FmEnvia do
  begin
    EdAssunto.Text := RGTipo.Items[RGTipo.ItemIndex];
    LBAnexos.Items.Add(VAR_ARQUIVO+'.csv');
    LBAnexos.Items.Add(VAR_ARQUIVO+'.frp');
    if Application.MessageBox('Deseja incluir o visualizador de relat�rio?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) =  ID_YES then
    begin
      if FileExists(Executavel) then LBAnexos.Items.Add(Executavel)
      else Application.MessageBox(PChar(Executavel+' n�o encontrado!'), 'Aviso!',
        MB_OK+MB_ICONWARNING);
    end;
    ShowModal;
    Destroy;
  end;
end;

procedure TFmExtratos.GeraRelatorio(Saida: TTipoSaidaRelatorio);
begin
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Informe a empresa!') then Exit;
  FEmpresa := DModG.QrEmpresasCodigo.Value;

  VAR_PARAR := False;
  Ext_VALORA := 0;
  Ext_VALORB := 0;
  Ext_VALORC := 0;
  Ext_PENDENTEA := 0;
  Ext_PENDENTEB := 0;
  Ext_PENDENTEC := 0;
  Ext_ATUALIZADOA := 0;
  Ext_ATUALIZADOB := 0;
  Ext_ATUALIZADOC := 0;
  Ext_DEVIDOA := 0;
  Ext_DEVIDOB := 0;
  Ext_DEVIDOC := 0;
  Ext_PAGO_REALA := 0;
  Ext_PAGO_REALB := 0;
  Ext_PAGO_REALC := 0;
  if (RGOrdem1.Enabled) and (RGOrdem1.ItemIndex = -1) then
  begin
    Application.MessageBox('Defina a "Ordem 1"!', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  if (RGOrdem2.Enabled) and (RGOrdem2.ItemIndex = -1) then
  begin
    Application.MessageBox('Defina uma "Ordem 2"!', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  MLAGeral.ExcluiSQLsDermatek;
  if TPEmissFim.Date < TPEmissIni.Date then
  begin
    Application.MessageBox('"Data de emiss�o" final menor que inicial!', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  if TPVctoFim.Date < TPVctoIni.Date then
  begin
    Application.MessageBox('"Data de vencimento" final menor que inicial!', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  if TPDocFim.Date < TPDocIni.Date then
  begin
    Application.MessageBox('"Data do documento" final menor que inicial!', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  if (CBTerceiro.KeyValue <> NULL) and (Trim(CBTerceiro.Text) = CO_VAZIO) then
  begin
    ShowMessage('Fornecedor/Cliente sem nome. Tecle "DEL" para limpar.');
    CBTerceiro.SetFocus;
    Exit;
  end;
  Ext_Saldo := 0;
  if CkEmissao.Checked then
  begin
    Ext_EmisI := FormatDateTime(VAR_FORMATDATE, TPEmissIni.Date);
    Ext_EmisF := FormatDateTime(VAR_FORMATDATE, TPEmissFim.Date);
  end else begin
    Ext_EmisI := FormatDateTime(VAR_FORMATDATE, TPEmissIni.Date);
    Ext_EmisF := FormatDateTime(VAR_FORMATDATE, TPEmissFim.Date);
  end;
  Ext_VctoI := FormatDateTime(VAR_FORMATDATE, TPVctoIni.Date);
  Ext_VctoF := FormatDateTime(VAR_FORMATDATE, TPVctoFim.Date);
  Ext_CompI := FormatDateTime(VAR_FORMATDATE, TPCompIni.Date);
  Ext_CompF := FormatDateTime(VAR_FORMATDATE, TPCompFim.Date);
  Ext_DocI  := FormatDateTime(VAR_FORMATDATE, TPDocIni.Date);
  Ext_DocF  := FormatDateTime(VAR_FORMATDATE, TPDocFim.Date);
  Ext_DataH := FormatDateTime(VAR_FORMATDATE, Date);
  Ext_DataA := FormatDateTime(VAR_FORMATDATE, TPEmissIni.Date-1);
  //
  //Corrige lan�amentos com problema
  {$IFDEF DEFINE_VARLCT}
    FTabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, EdEmpresa.ValueVariant);
  {$ELSE}
    FTabLctA := VAR_LCT;
  {$ENDIF}
  if UFinanceiro.LancamentosComProblemas(FTabLctA) then
    Exit;
  //
  case RGTipo.ItemIndex of
    0: R00_ImprimeExtratoConsolidado();
    1: R01_ImprimeFluxoDeCaixa();
    2: R02_ImprimeContasAPagar(Self, Saida);
    3: R02_ImprimeContasAPagar(Self, Saida);
    4: R02_ImprimeContasAPagar(Self, Saida);
    5: R02_ImprimeContasAPagar(Self, Saida);
    6: R02_ImprimeContasAPagar(Self, Saida);
    7: R02_ImprimeContasAPagar(Self, Saida);
    8: R02_ImprimeContasAPagar(Self, Saida);
    9: R02_ImprimeContasAPagar(Self, Saida);
    else Geral.MensagemBox('Relat�rio n�o implementado', 'Erro',
    MB_OK+MB_ICONERROR);
  end;
end;

procedure TFmExtratos.QrTerceirosCalcFields(DataSet: TDataSet);
begin
  if QrTerceirosTel1.Value = '' then QrTerceirosTEL1_TXT.Value := '' else
     QrTerceirosTEL1_TXT.Value := '[ ' + Geral.FormataTelefone_TT(
     QrTerceirosTel1.Value) + ' ]';
  //
  if QrTerceirosTel2.Value = '' then QrTerceirosTEL2_TXT.Value := '' else
     QrTerceirosTEL2_TXT.Value := '[ ' + Geral.FormataTelefone_TT(
     QrTerceirosTel2.Value) + ' ]';
  //
  if QrTerceirosTel3.Value = '' then QrTerceirosTEL3_TXT.Value := '' else
     QrTerceirosTEL3_TXT.Value := '[ ' + Geral.FormataTelefone_TT(
     QrTerceirosTel3.Value) + ' ]';
  //
  QrTerceirosTELEFONES.Value := QrTerceirosTEL1_TXT.Value +
     QrTerceirosTEL2_TXT.Value + QrTerceirosTEL3_TXT.Value;
end;

procedure TFmExtratos.frxFin_Relat_005_02_B1GetValue(const VarName: String;
  var Value: Variant);
var
  Liga: String;
begin
  if VarName = 'TIPOREL' then
    Value := RGTipo.ItemIndex
  else if VarName = 'REPRESENTANTE' then
  begin
    if CBAccount.KeyValue = NULL then Value := 'TODOS'
    else Value := CBAccount.Text;
  end else if VarName = 'CLI_INT' then
    Value := CBEmpresa.Text
  else if VarName = 'VENDEDOR' then
  begin
    if CBVendedor.KeyValue = NULL then Value := 'TODOS'
    else Value := CBVendedor.Text;
  end else if VarName = 'NOME_CONTA' then
  begin
    if CBConta.KeyValue = NULL then Value := 'TODAS'
    else Value := CBConta.Text;
  end;
  ////////////////////////////////////////
  if VarName = 'VALOR_A' then Value := Ext_VALORA;
  if VarName = 'VALOR_B' then Value := Ext_VALORB;
  if VarName = 'VALOR_C' then Value := Ext_VALORC / 2; // two pass
  ///////////////////////////////////////
  if VarName = 'PAGO_REAL_A' then Value := Ext_PAGO_REALA;
  if VarName = 'PAGO_REAL_B' then Value := Ext_PAGO_REALB;
  if VarName = 'PAGO_REAL_C' then Value := Ext_PAGO_REALC / 2;
  ///////////////////////////////////////
  if VarName = 'ATUALIZADO_A' then Value := Ext_ATUALIZADOA;
  if VarName = 'ATUALIZADO_B' then Value := Ext_ATUALIZADOB;
  if VarName = 'ATUALIZADO_C' then Value := Ext_ATUALIZADOC / 2;
  ///////////////////////////////////////
  if VarName = 'DEVIDO_A' then Value := Ext_DEVIDOA;
  if VarName = 'DEVIDO_B' then Value := Ext_DEVIDOB;
  if VarName = 'DEVIDO_C' then Value := Ext_DEVIDOC / 2;
  ///////////////////////////////////////
  if VarName = 'PENDENTE_A' then Value := Ext_PENDENTEA;
  if VarName = 'PENDENTE_B' then Value := Ext_PENDENTEB;
  if VarName = 'PENDENTE_C' then Value := Ext_PENDENTEC / 2;
  ///////////////////////////////////////

  if VarName = 'GRUPO1' then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRecTERCEIRO.Value)+ QrPagRecNOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRecDataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRecData.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRecVencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRecDocumento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRecNotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRecDuplicata.Value;
      7: Value := MLAGeral.TxtUH() + ': ' +QrPagRecNO_UH.Value;
    end;
  end;
  if VarName = 'GRUPO2' then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRecTERCEIRO.Value)+ QrPagRecNOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRecDataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRecData.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRecVencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRecDocumento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRecNotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRecDuplicata.Value;
      7: Value := MLAGeral.TxtUH() + ': ' +QrPagRecNO_UH.Value;
    end;
  end;
  if VarName = 'GRUPOA' then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRec1TERCEIRO.Value)+ QrPagRec1NOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRec1DataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Data.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Vencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRec1Documento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRec1NotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRec1Duplicata.Value;
      7: Value := MLAGeral.TxtUH() + ': ' +QrPagRec1NO_UH.Value;
    end;
  end;
  if VarName = 'GRUPOB' then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRec1TERCEIRO.Value)+ QrPagRec1NOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRec1DataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Data.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Vencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRec1Documento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRec1NotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRec1Duplicata.Value;
      7: Value := MLAGeral.TxtUH() + ': ' +QrPagRec1NO_UH.Value;
    end;
  end;
  if VarName = 'GRUPOC' then
  begin
    Value := 'geral';
  end;
  if VarName = 'OMITIDOS' then
  begin
    if CkOmiss.Checked then Value := 'Omitidos valores abertos vencidos antes de '+
      FormatDateTime(VAR_FORMATDATE3, Ext_Vencto)
    else if RGTipo.ItemIndex in ([8,9]) then Value :=
      'Somente valores abertos vencidos antes de '+
      FormatDateTime(VAR_FORMATDATE3, Ext_Vencto)
    else Value := '  ';
  end;
  if VarName = 'NIVEIS' then
  begin
    if CkNiveis.Checked then Value := 'N�veis '+EdA.Text+' a '+EdZ.Text
    else Value := '  ';
  end;
  if VarName = 'TERCEIRO' then
  begin
    if RGTipo.ItemIndex = 2 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 3 then Value := 'Cliente: ';
    if RGTipo.ItemIndex = 4 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 5 then Value := 'Cliente: ';
    if RGTipo.ItemIndex = 6 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 7 then Value := 'Cliente: ';
    if RGTipo.ItemIndex = 8 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 9 then Value := 'Cliente: ';
  end;
  if VarName = 'INICIAL' then Value := Ext_Saldo;
  if VarName = 'NOMEREL' then
  begin
    case RGTipo.ItemIndex of
      2: Value := 'RELAT�RIO DE CONTAS A PAGAR';
      3: Value := 'RELAT�RIO DE CONTAS A RECEBER';
      4: Value := 'RELAT�RIO DE CONTAS PAGAS';
      5: Value := 'RELAT�RIO DE CONTAS RECEBIDAS';
      6: Value := 'RELAT�RIO DE EMISS�O DE CONTAS A PAGAR';
      7: Value := 'RELAT�RIO DE EMISS�O DE CONTAS A RECEBER';
      8: Value := 'RELAT�RIO DE D�VIDA ATIVA DE CONTAS A PAGAR';
      9: Value := 'RELAT�RIO DE D�VIDA ATIVA DE CONTAS A RECEBER';
    end;
  end;

  if VarName = 'SALDO' then
  begin
    Value := QrExtratoCredito.Value - QrExtratoDebito.Value;
    Ext_Saldo := Ext_Saldo + Value;
  end;
  if VarName = 'SALDO2' then
  begin
    Value := QrFluxoSALDO.Value;
    Ext_Saldo := Ext_Saldo + Value;
  end;
  if VarName = 'SALDO4' then
  begin
    Value := QrPagRecSALDO.Value;
    Ext_Saldo := Ext_Saldo + Value;
  end;
  if VarName = 'FORNECEDOR' then
    if CBTerceiro.KeyValue = NULL then Value := 'TODOS' else
    Value := IntToStr(CBTerceiro.KeyValue)+' - '+CBTerceiro.Text + ' ' +
    QrTerceirosTELEFONES.Text;
  if VarName = 'SALDODIA' then Value := Ext_Saldo;
  if VarName = 'PERIODO' then
  begin
    Value := '';
    if not CkEmissao.Checked and not CkVencto.Checked
    and not CkDataDoc.Checked then Value := 'N�o definido';
    if CkEmissao.Checked and CkVencto.Checked then
      Liga := ' com ' else Liga := ' ';
    if CkEmissao.Checked then
      Value := 'Emiss�o de '+
        FormatDateTime(VAR_FORMATDATE3, TPEmissIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPEmissFim.Date) + Liga;
    if (CkEmissao.Checked or CkVencto.Checked) and CkDataDoc.Checked then
      Liga := ' com ' else Liga := ' ';
    if CkVencto.Checked then
      Value := Value + 'Vencimento de '+
        FormatDateTime(VAR_FORMATDATE3, TPVctoIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPVctoFim.Date) + Liga;
    if CkDataDoc.Checked then
      Value := Value + 'Datadoc de '+
        FormatDateTime(VAR_FORMATDATE3, TPDocIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPDocFim.Date);
  end

  // user function

  else if VarName = 'VFR_CODITION_A' then
  begin
    case RGOrdem1.ItemIndex of
      00: Value := 'frxDsPagRec1."NOME_TERCEIRO"';
      01: Value := 'frxDsPagRec1."DataDoc"';
      02: Value := 'frxDsPagRec1."Data"';
      03: Value := 'frxDsPagRec1."Vencimento"';
      04: Value := 'frxDsPagRec1."Documento"';
      05: Value := 'frxDsPagRec1."NotaFiscal"';
      06: Value := 'frxDsPagRec1."Duplicata"';
      07: Value := 'frxDsPagRec1."NO_UH"';
    end;
  end
  else
  if VarName = 'VFR_CODITION_B' then
  begin
    case RGOrdem2.ItemIndex of
      00: Value := 'frxDsPagRec1."NOME_TERCEIRO"';
      01: Value := 'frxDsPagRec1."DataDoc"';
      02: Value := 'frxDsPagRec1."Data"';
      03: Value := 'frxDsPagRec1."Vencimento"';
      04: Value := 'frxDsPagRec1."Documento"';
      05: Value := 'frxDsPagRec1."NotaFiscal"';
      06: Value := 'frxDsPagRec1."Duplicata"';
      07: Value := 'frxDsPagRec1."NO_UH"';
    end;
  end else if VarName = 'VFR_CODITION_A1' then
  begin
    case RGOrdem1.ItemIndex of
      00: Value := 'frxDsPagRec."NOME_TERCEIRO"';
      01: Value := 'frxDsPagRec."DataDoc"';
      02: Value := 'frxDsPagRec."Data"';
      03: Value := 'frxDsPagRec."Vencimento"';
      04: Value := 'frxDsPagRec."Documento"';
      05: Value := 'frxDsPagRec."NotaFiscal"';
      06: Value := 'frxDsPagRec."Duplicata"';
      07: Value := 'frxDsPagRec."NO_UH"';
    end;
  end
  else
  if VarName = 'VFR_CODITION_B1' then
  begin
    case RGOrdem2.ItemIndex of
      00: Value := 'frxDsPagRec."NOME_TERCEIRO"';
      01: Value := 'frxDsPagRec."DataDoc"';
      02: Value := 'frxDsPagRec."Data"';
      03: Value := 'frxDsPagRec."Vencimento"';
      04: Value := 'frxDsPagRec."Documento"';
      05: Value := 'frxDsPagRec."NotaFiscal"';
      06: Value := 'frxDsPagRec."Duplicata"';
      07: Value := 'frxDsPagRec."NO_UH"';
    end;
  end;
  if VarName = 'VFR_ORD' then if CBTerceiro.KeyValue = NULL then
  Value := False else Value := True;
  if VarName = 'VARF_GRADE' then Value := CkGrade.Checked;
  if VarName = 'VFR_GRUPO1' then
  begin
    if CkOrdem1.Checked then Value := RGOrdem1.ItemIndex else Value := -1;
  end;
  if VarName = 'VFR_GRUPO2' then
  begin
    if CkOrdem2.Checked then Value := RGOrdem2.ItemIndex else Value := -1;
  end;
  if VarName = 'VFR_GRUPOA' then
  begin
    if CkOrdem1.Checked then Value := RGOrdem1.ItemIndex else Value := -1;
    Ext_VALORA := 0;
    Ext_DEVIDOA := 0;
    Ext_PENDENTEA := 0;
    Ext_PAGO_REALA := 0;
    Ext_ATUALIZADOA := 0;
  end;
  if VarName = 'VFR_GRUPOB' then
  begin
    if CkOrdem2.Checked then Value := RGOrdem2.ItemIndex else Value := -1;
    Ext_VALORB := 0;
    Ext_DEVIDOB := 0;
    Ext_PENDENTEB := 0;
    Ext_PAGO_REALB := 0;
    Ext_ATUALIZADOB := 0;
  end;
  (*if VarName = 'VFR_GRUPOC' then
  begin
    Value := 0;
    Ext_ValueORC := 0;
    Ext_DEVIDOC := 0;
    Ext_PENDENTEC := 0;
    Ext_PAGO_REALC := 0;
    Ext_ATUALIZADOC := 0;
  end; *)
  if VarName = 'VFR_ID_PAGTO' then
  begin
    if QrPagRec1CtrlPai.Value = QrPagRec1Controle.Value then
    begin
      Value := 0;
      Ext_VALORA := Ext_VALORA + QrPagRec1VALOR.Value;
      Ext_VALORB := Ext_VALORB + QrPagRec1VALOR.Value;
      Ext_VALORC := Ext_VALORC + QrPagRec1VALOR.Value;
      /////
      Ext_PAGO_REALA := Ext_PAGO_REALA + QrPagRec1PAGO_REAL.Value;
      Ext_PAGO_REALB := Ext_PAGO_REALB + QrPagRec1PAGO_REAL.Value;
      Ext_PAGO_REALC := Ext_PAGO_REALC + QrPagRec1PAGO_REAL.Value;
      /////
      Ext_ATUALIZADOA := Ext_ATUALIZADOA + QrPagRec1ATUALIZADO.Value;
      Ext_ATUALIZADOB := Ext_ATUALIZADOB + QrPagRec1ATUALIZADO.Value;
      Ext_ATUALIZADOC := Ext_ATUALIZADOC + QrPagRec1ATUALIZADO.Value;
      /////
      Ext_DEVIDOA := Ext_DEVIDOA + QrPagRec1DEVIDO.Value;
      Ext_DEVIDOB := Ext_DEVIDOB + QrPagRec1DEVIDO.Value;
      Ext_DEVIDOC := Ext_DEVIDOC + QrPagRec1DEVIDO.Value;
      /////
    end else Value := 1;
    Ext_PENDENTEA := Ext_PENDENTEA + QrPagRec1PEND_TOTAL.Value;
    Ext_PENDENTEB := Ext_PENDENTEB + QrPagRec1PEND_TOTAL.Value;
    Ext_PENDENTEC := Ext_PENDENTEC + QrPagRec1PEND_TOTAL.Value;
  end else
  if VarName = 'VAR_NOMEEMPRESA' then
    Value := CBEmpresa.Text
  else
  if VarName = 'VAR_UH' then
      Value := MLAGeral.TxtUH()
  else    
end;

procedure TFmExtratos.frxFin_Relat_005_00GetValue(const VarName: String;
  var Value: Variant);
var
  Liga: String;
begin
  if VarName = 'SUBST' then
  begin
    if CkSubstituir.Checked then
      Value := 'Cliente / Fornecedor'
    else
      Value := 'Descri��o';
  end;
  if VarName = 'REPRESENTANTE' then
  begin
    if CBAccount.KeyValue = NULL then Value := 'TODOS'
    else Value := CBAccount.Text;
  end else if VarName = 'CLI_INT' then
    Value := CBEmpresa.Text
  else if VarName = 'VENDEDOR' then
  begin
    if CBVendedor.KeyValue = NULL then Value := 'TODOS'
    else Value := CBVendedor.Text;
  end else if VarName = 'NOME_CONTA' then
  begin
    if CBConta.KeyValue = NULL then Value := 'TODAS'
    else Value := CBConta.Text;
  end;
  ////////////////////////////////////////
  if VarName = 'VALOR_A' then Value := Ext_VALORA;
  if VarName = 'VALOR_B' then Value := Ext_VALORB;
  if VarName = 'VALOR_C' then Value := Ext_VALORC / 2; // two pass
  ///////////////////////////////////////
  if VarName = 'PAGO_REAL_A' then Value := Ext_PAGO_REALA;
  if VarName = 'PAGO_REAL_B' then Value := Ext_PAGO_REALB;
  if VarName = 'PAGO_REAL_C' then Value := Ext_PAGO_REALC / 2;
  ///////////////////////////////////////
  if VarName = 'ATUALIZADO_A' then Value := Ext_ATUALIZADOA;
  if VarName = 'ATUALIZADO_B' then Value := Ext_ATUALIZADOB;
  if VarName = 'ATUALIZADO_C' then Value := Ext_ATUALIZADOC / 2;
  ///////////////////////////////////////
  if VarName = 'DEVIDO_A' then Value := Ext_DEVIDOA;
  if VarName = 'DEVIDO_B' then Value := Ext_DEVIDOB;
  if VarName = 'DEVIDO_C' then Value := Ext_DEVIDOC / 2;
  ///////////////////////////////////////
  if VarName = 'PENDENTE_A' then Value := Ext_PENDENTEA;
  if VarName = 'PENDENTE_B' then Value := Ext_PENDENTEB;
  if VarName = 'PENDENTE_C' then Value := Ext_PENDENTEC / 2;
  ///////////////////////////////////////

  if VarName = 'GRUPO1' then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRecTERCEIRO.Value)+ QrPagRecNOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRecDataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRecData.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRecVencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRecDocumento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRecNotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRecDuplicata.Value;
      7: Value := MLAGeral.TxtUH() + ': ' +QrPagRecNO_UH.Value;
    end;
  end;
  if VarName = 'GRUPO2' then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRecTERCEIRO.Value)+ QrPagRecNOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRecDataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRecData.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRecVencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRecDocumento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRecNotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRecDuplicata.Value;
      7: Value := MLAGeral.TxtUH() + ': ' +QrPagRecNO_UH.Value;
    end;
  end;
  if VarName = 'GRUPOA' then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRec1TERCEIRO.Value)+ QrPagRec1NOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRec1DataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Data.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Vencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRec1Documento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRec1NotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRec1Duplicata.Value;
      7: Value := MLAGeral.TxtUH() + ': ' +QrPagRec1NO_UH.Value;
    end;
  end;
  if VarName = 'GRUPOB' then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := 'Entidade: ' +FormatFloat(' 000000 - ', QrPagRec1TERCEIRO.Value)+ QrPagRec1NOME_TERCEIRO.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrPagRec1DataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Data.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrPagRec1Vencimento.Value);
      4: Value := 'Documento: ' +FormatFloat(' 000000 ', QrPagRec1Documento.Value);
      5: Value := 'Nota Fiscal: ' +FormatFloat(' 000000 ', QrPagRec1NotaFiscal.Value);
      6: Value := 'Duplicata: ' +QrPagRec1Duplicata.Value;
      7: Value := MLAGeral.TxtUH() + ': ' +QrPagRec1NO_UH.Value;
    end;
  end;
  if VarName = 'GRUPOC' then
  begin
    Value := 'geral';
  end;
  if VarName = 'OMITIDOS' then
  begin
    if CkOmiss.Checked then Value := 'Omitidos valores abertos vencidos antes de '+
      FormatDateTime(VAR_FORMATDATE3, Ext_Vencto)
    else if RGTipo.ItemIndex in ([8,9]) then Value :=
      'Somente valores abertos vencidos antes de '+
      FormatDateTime(VAR_FORMATDATE3, Ext_Vencto)
    else Value := '  ';
  end;
  if VarName = 'NIVEIS' then
  begin
    if CkNiveis.Checked then Value := 'N�veis '+EdA.Text+' a '+EdZ.Text
    else Value := '  ';
  end;
  if VarName = 'TERCEIRO' then
  begin
    if RGTipo.ItemIndex = 2 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 3 then Value := 'Cliente: ';
    if RGTipo.ItemIndex = 4 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 5 then Value := 'Cliente: ';
    if RGTipo.ItemIndex = 6 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 7 then Value := 'Cliente: ';
    if RGTipo.ItemIndex = 8 then Value := 'Fornecedor: ';
    if RGTipo.ItemIndex = 9 then Value := 'Cliente: ';
  end;
  if VarName = 'INICIAL' then Value := Ext_Saldo;
  if VarName = 'NOMEREL' then
  begin
    case RGTipo.ItemIndex of
      1: Value := 'RELAT�RIO FLUXO DE CAIXA';
      2: Value := 'RELAT�RIO DE CONTAS A PAGAR';
      3: Value := 'RELAT�RIO DE CONTAS A RECEBER';
      4: Value := 'RELAT�RIO DE CONTAS PAGAS';
      5: Value := 'RELAT�RIO DE CONTAS RECEBIDAS';
      6: Value := 'RELAT�RIO DE EMISS�O DE CONTAS A PAGAR';
      7: Value := 'RELAT�RIO DE EMISS�O DE CONTAS A RECEBER';
      8: Value := 'RELAT�RIO DE D�VIDA ATIVA DE CONTAS A PAGAR';
      9: Value := 'RELAT�RIO DE D�VIDA ATIVA DE CONTAS A RECEBER';
    end;
  end;

  if VarName = 'SALDO' then
  begin
    Value := QrExtratoCredito.Value - QrExtratoDebito.Value;
    Ext_Saldo := Ext_Saldo + Value;
  end;
  if VarName = 'SALDO2' then
  begin
    Value := QrFluxoSALDO.Value;
    Ext_Saldo := Ext_Saldo + Value;
  end;
  if VarName = 'SALDO4' then
  begin
    Value := QrPagRecSALDO.Value;
    Ext_Saldo := Ext_Saldo + Value;
  end;
  if VarName = 'FORNECEDOR' then
    if CBTerceiro.KeyValue = NULL then Value := 'TODOS' else
    Value := IntToStr(CBTerceiro.KeyValue)+' - '+CBTerceiro.Text + ' ' +
    QrTerceirosTELEFONES.Text;
  if VarName = 'SALDODIA' then Value := Ext_Saldo;
  if VarName = 'PERIODO' then
  begin
    Value := '';
    if not CkEmissao.Checked and not CkVencto.Checked
    and not CkDataDoc.Checked then Value := 'N�o definido';
    if CkEmissao.Checked and CkVencto.Checked then
      Liga := ' com ' else Liga := ' ';
    if CkEmissao.Checked then
      Value := 'Emiss�o de '+
        FormatDateTime(VAR_FORMATDATE3, TPEmissIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPEmissFim.Date) + Liga;
    if (CkEmissao.Checked or CkVencto.Checked) and CkDataDoc.Checked then
      Liga := ' com ' else Liga := ' ';
    if CkVencto.Checked then
      Value := Value + 'Vencimento de '+
        FormatDateTime(VAR_FORMATDATE3, TPVctoIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPVctoFim.Date) + Liga;
    if CkDataDoc.Checked then
      Value := Value + 'Datadoc de '+
        FormatDateTime(VAR_FORMATDATE3, TPDocIni.Date)+ CO_ATE+
        FormatDateTime(VAR_FORMATDATE3, TPDocFim.Date);
  end else
  if VarName = 'VAR_NOMEEMPRESA' then
    Value := CBEmpresa.Text;
end;

end.

