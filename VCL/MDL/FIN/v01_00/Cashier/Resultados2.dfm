object FmResultados2: TFmResultados2
  Left = 402
  Top = 166
  Caption = 'FIN-RELAT-002 :: Resultados Financeiros por Per'#237'odo'
  ClientHeight = 584
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 536
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtImprime: TBitBtn
      Tag = 5
      Left = 8
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Imprime'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtImprimeClick
    end
    object Panel3: TPanel
      Left = 908
      Top = 1
      Width = 99
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSair: TBitBtn
        Tag = 13
        Left = 4
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSairClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 488
    Align = alClient
    TabOrder = 0
    object Panel4: TPanel
      Left = 1
      Top = 225
      Width = 1006
      Height = 262
      Align = alClient
      TabOrder = 1
      object PCTipoRel: TPageControl
        Left = 1
        Top = 1
        Width = 1004
        Height = 260
        ActivePage = TabSheet3
        Align = alClient
        TabOrder = 0
        OnChange = PCTipoRelChange
        object TabSheet1: TTabSheet
          Caption = 'Resultados por CONTAS '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 996
            Height = 232
            Align = alClient
            ParentBackground = False
            TabOrder = 0
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' Resultados por LAN'#199'AMENTOS '
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 996
            Height = 232
            Align = alClient
            ParentBackground = False
            TabOrder = 0
          end
        end
        object TabSheet4: TTabSheet
          Caption = ' Extrato por Lan'#231'amentos '
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel10: TPanel
            Left = 0
            Top = 0
            Width = 996
            Height = 232
            Align = alClient
            ParentBackground = False
            TabOrder = 0
            object DBGrid1: TDBGrid
              Left = 1
              Top = 25
              Width = 994
              Height = 206
              Align = alClient
              DataSource = DsResultLA
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
            end
            object Panel11: TPanel
              Left = 1
              Top = 1
              Width = 994
              Height = 24
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              object CkAgrupaCta: TCheckBox
                Left = 4
                Top = 4
                Width = 209
                Height = 17
                Caption = 'Agrupa pela conta do plano de contas.'
                TabOrder = 0
                OnClick = CkAgrupaCtaClick
              end
              object CkZeraAcumCta: TCheckBox
                Left = 216
                Top = 3
                Width = 185
                Height = 17
                Caption = 'Zera acumulado para cada conta.'
                Checked = True
                Enabled = False
                State = cbChecked
                TabOrder = 1
              end
            end
          end
        end
        object TabSheet3: TTabSheet
          Caption = ' Balancete industrial modelo 01 '
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 996
            Height = 232
            Align = alClient
            ParentBackground = False
            TabOrder = 0
            object Panel9: TPanel
              Left = 1
              Top = 1
              Width = 994
              Height = 56
              Align = alTop
              TabOrder = 0
              object RGPeriodoBalInd: TRadioGroup
                Left = 4
                Top = 4
                Width = 341
                Height = 45
                Caption = ' Per'#237'odo a ser pesquisado: '
                Columns = 3
                ItemIndex = 2
                Items.Strings = (
                  'Data'
                  'Vencimento'
                  'Data doc.')
                TabOrder = 0
              end
            end
          end
        end
      end
    end
    object Panel5: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 224
      Align = alTop
      TabOrder = 0
      object Label3: TLabel
        Left = 392
        Top = 8
        Width = 45
        Height = 13
        Caption = 'Conjunto:'
      end
      object Label4: TLabel
        Left = 392
        Top = 52
        Width = 32
        Height = 13
        Caption = 'Grupo:'
      end
      object Label5: TLabel
        Left = 392
        Top = 96
        Width = 52
        Height = 13
        Caption = 'Sub-grupo:'
      end
      object Label6: TLabel
        Left = 392
        Top = 140
        Width = 31
        Height = 13
        Caption = 'Conta:'
      end
      object Label1: TLabel
        Left = 392
        Top = 180
        Width = 158
        Height = 13
        Caption = 'Cliente Interno (vazio para todos):'
      end
      object CkDataIni: TCheckBox
        Left = 8
        Top = 4
        Width = 170
        Height = 17
        Caption = 'Data inicial:'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object TPDataIni: TdmkEditDateTimePicker
        Left = 8
        Top = 24
        Width = 186
        Height = 21
        Date = 37660.516375590300000000
        Time = 37660.516375590300000000
        TabOrder = 1
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object CkVctoIni: TCheckBox
        Left = 8
        Top = 48
        Width = 170
        Height = 17
        Caption = 'Vencimento inicial:'
        TabOrder = 4
      end
      object TPVctoIni: TdmkEditDateTimePicker
        Left = 8
        Top = 68
        Width = 186
        Height = 21
        Date = 37660.516375590300000000
        Time = 37660.516375590300000000
        TabOrder = 5
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object CkDtDocIni: TCheckBox
        Left = 8
        Top = 92
        Width = 170
        Height = 17
        Caption = 'Data doc. inicial'
        TabOrder = 8
      end
      object TPDtDocIni: TdmkEditDateTimePicker
        Left = 8
        Top = 112
        Width = 186
        Height = 21
        Date = 37660.516375590300000000
        Time = 37660.516375590300000000
        TabOrder = 9
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object CkDataFim: TCheckBox
        Left = 200
        Top = 4
        Width = 170
        Height = 17
        Caption = 'Data final:'
        Checked = True
        State = cbChecked
        TabOrder = 2
      end
      object TPDataFim: TdmkEditDateTimePicker
        Left = 200
        Top = 24
        Width = 186
        Height = 21
        Date = 37660.516439456000000000
        Time = 37660.516439456000000000
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object CkVctoFim: TCheckBox
        Left = 200
        Top = 48
        Width = 170
        Height = 17
        Caption = 'Vencimento final:'
        TabOrder = 6
      end
      object TPVctoFim: TdmkEditDateTimePicker
        Left = 200
        Top = 68
        Width = 186
        Height = 21
        Date = 37660.516439456000000000
        Time = 37660.516439456000000000
        TabOrder = 7
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object CkDtDocFim: TCheckBox
        Left = 200
        Top = 92
        Width = 170
        Height = 17
        Caption = 'Data doc. final:'
        TabOrder = 10
      end
      object TPDtDocFim: TdmkEditDateTimePicker
        Left = 200
        Top = 112
        Width = 186
        Height = 21
        Date = 37660.516439456000000000
        Time = 37660.516439456000000000
        TabOrder = 11
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object EdConjunto: TdmkEditCB
        Left = 392
        Top = 24
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 15
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdConjuntoChange
        DBLookupComboBox = CBConjunto
        IgnoraDBLookupComboBox = False
      end
      object CBConjunto: TdmkDBLookupComboBox
        Left = 448
        Top = 24
        Width = 548
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsConjuntos
        TabOrder = 16
        dmkEditCB = EdConjunto
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CBGrupo: TdmkDBLookupComboBox
        Left = 448
        Top = 68
        Width = 548
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsGrupos
        TabOrder = 18
        dmkEditCB = EdGrupo
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdGrupo: TdmkEditCB
        Left = 392
        Top = 68
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 17
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGrupoChange
        DBLookupComboBox = CBGrupo
        IgnoraDBLookupComboBox = False
      end
      object EdSubgrupo: TdmkEditCB
        Left = 392
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 19
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdSubgrupoChange
        DBLookupComboBox = CBSubgrupo
        IgnoraDBLookupComboBox = False
      end
      object CBSubgrupo: TdmkDBLookupComboBox
        Left = 448
        Top = 112
        Width = 548
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSubgrupos
        TabOrder = 20
        dmkEditCB = EdSubgrupo
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CBConta: TdmkDBLookupComboBox
        Left = 448
        Top = 156
        Width = 548
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsContas
        TabOrder = 22
        dmkEditCB = EdConta
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdConta: TdmkEditCB
        Left = 392
        Top = 156
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 21
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBConta
        IgnoraDBLookupComboBox = False
      end
      object RGExclusivos: TRadioGroup
        Left = 8
        Top = 137
        Width = 377
        Height = 41
        Caption = ' Tipo de lan'#231'amentos: '
        Columns = 3
        ItemIndex = 1
        Items.Strings = (
          'Sem exclusivos'
          'Com exclusivos'
          'S'#243' exclusivos')
        TabOrder = 12
      end
      object CkNome2: TCheckBox
        Left = 8
        Top = 184
        Width = 377
        Height = 17
        Caption = 'Utilizar a descri'#231#227'o substituta nas contas.'
        Checked = True
        State = cbChecked
        TabOrder = 13
      end
      object CkSaldo: TCheckBox
        Left = 8
        Top = 200
        Width = 377
        Height = 17
        Caption = 'Informar saldo final do fluxo de caixa.'
        Checked = True
        State = cbChecked
        TabOrder = 14
      end
      object EdCliInt: TdmkEditCB
        Left = 392
        Top = 196
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 23
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliInt
        IgnoraDBLookupComboBox = False
      end
      object CBCliInt: TdmkDBLookupComboBox
        Left = 448
        Top = 196
        Width = 548
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECLI'
        ListSource = DsCliInt
        TabOrder = 24
        dmkEditCB = EdCliInt
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Resultados Financeiros por Per'#237'odo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 392
      ExplicitHeight = 44
    end
  end
  object QrGrupos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM grupos'
      'ORDER BY Nome')
    Left = 836
    Top = 8
    object QrGruposCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.grupos.Codigo'
    end
    object QrGruposNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.grupos.Nome'
      Size = 4
    end
    object QrGruposConjunto: TIntegerField
      FieldName = 'Conjunto'
      Origin = 'DBMMONEY.grupos.Conjunto'
    end
    object QrGruposLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMMONEY.grupos.Lk'
    end
  end
  object DsGrupos: TDataSource
    DataSet = QrGrupos
    Left = 864
    Top = 8
  end
  object QrConjuntos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM conjuntos'
      'ORDER BY Nome')
    Left = 780
    Top = 8
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.conjuntos.Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.conjuntos.Nome'
      Size = 4
    end
    object IntegerField2: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMMONEY.conjuntos.Lk'
    end
  end
  object DsConjuntos: TDataSource
    DataSet = QrConjuntos
    Left = 808
    Top = 8
  end
  object QrSubgrupos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM subgrupos'
      'ORDER BY Nome')
    Left = 892
    Top = 8
    object QrSubgruposCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.subgrupos.Codigo'
    end
    object QrSubgruposNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.subgrupos.Nome'
      Size = 4
    end
    object QrSubgruposGrupo: TIntegerField
      FieldName = 'Grupo'
      Origin = 'DBMMONEY.subgrupos.Grupo'
    end
    object QrSubgruposLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMMONEY.subgrupos.Lk'
    end
  end
  object DsSubgrupos: TDataSource
    DataSet = QrSubgrupos
    Left = 920
    Top = 8
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM contas'
      'ORDER BY Nome')
    Left = 948
    Top = 8
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.contas.Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.contas.Nome'
      Size = 25
    end
    object QrContasNome2: TWideStringField
      FieldName = 'Nome2'
      Origin = 'DBMMONEY.contas.Nome2'
      Size = 4
    end
    object QrContasID: TWideStringField
      FieldName = 'ID'
      Origin = 'DBMMONEY.contas.ID'
      Size = 128
    end
    object QrContasSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Origin = 'DBMMONEY.contas.Subgrupo'
    end
    object QrContasEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'DBMMONEY.contas.Empresa'
    end
    object QrContasCredito: TWideStringField
      FieldName = 'Credito'
      Origin = 'DBMMONEY.contas.Credito'
      FixedChar = True
      Size = 1
    end
    object QrContasDebito: TWideStringField
      FieldName = 'Debito'
      Origin = 'DBMMONEY.contas.Debito'
      FixedChar = True
      Size = 1
    end
    object QrContasMensal: TWideStringField
      FieldName = 'Mensal'
      Origin = 'DBMMONEY.contas.Mensal'
      FixedChar = True
      Size = 1
    end
    object QrContasExclusivo: TWideStringField
      FieldName = 'Exclusivo'
      Origin = 'DBMMONEY.contas.Exclusivo'
      FixedChar = True
      Size = 1
    end
    object QrContasMensdia: TSmallintField
      FieldName = 'Mensdia'
      Origin = 'DBMMONEY.contas.Mensdia'
    end
    object QrContasMensdeb: TFloatField
      FieldName = 'Mensdeb'
      Origin = 'DBMMONEY.contas.Mensdeb'
    end
    object QrContasMensmind: TFloatField
      FieldName = 'Mensmind'
      Origin = 'DBMMONEY.contas.Mensmind'
    end
    object QrContasMenscred: TFloatField
      FieldName = 'Menscred'
      Origin = 'DBMMONEY.contas.Menscred'
    end
    object QrContasMensminc: TFloatField
      FieldName = 'Mensminc'
      Origin = 'DBMMONEY.contas.Mensminc'
    end
    object QrContasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMMONEY.contas.Lk'
    end
    object QrContasTerceiro: TIntegerField
      FieldName = 'Terceiro'
      Origin = 'DBMMONEY.contas.Terceiro'
    end
    object QrContasExcel: TWideStringField
      FieldName = 'Excel'
      Origin = 'DBMMONEY.contas.Excel'
      Size = 128
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 976
    Top = 8
  end
  object QrResultLA: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrResultLACalcFields
    SQL.Strings = (
      'SELECT la.*, co.Subgrupo, sg.Grupo,  ca.Nome NOMECARTEIRA,'
      'gr.Conjunto, co.Nome NOMECONTA,  co.Nome2 NOMECONTA2, '
      'sg.Nome NOMESUBGRUPO, gr.Nome NOMEGRUPO,'
      'cj.Nome NOMECONJUNTO '
      'FROM lanctos la, Contas co, Subgrupos sg,'
      'Grupos gr, Conjuntos cj, Carteiras ca'
      'WHERE la.Genero>0'
      'AND ca.Codigo=la.Carteira'
      'AND Data BETWEEN :P0 AND :P1 '
      'AND co.Codigo=la.Genero'
      'AND sg.Codigo=co.Subgrupo'
      'AND gr.Codigo=sg.Grupo'
      'AND cj.Codigo=gr.Conjunto'
      'AND co.Exclusivo="F"'
      'ORDER BY cj.Codigo, gr.Codigo, sg.Codigo, co.Codigo ')
    Left = 64
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrResultLANOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'DBMMONEY.contas.Nome'
      Size = 128
    end
    object QrResultLANOMECONTA2: TWideStringField
      FieldName = 'NOMECONTA2'
      Origin = 'DBMMONEY.contas.Nome2'
      Size = 128
    end
    object QrResultLANOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Origin = 'DBMMONEY.subgrupos.Nome'
      Size = 128
    end
    object QrResultLANOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Origin = 'DBMMONEY.grupos.Nome'
      Size = 128
    end
    object QrResultLANOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Origin = 'DBMMONEY.conjuntos.Nome'
      Size = 128
    end
    object QrResultLANOMECONTA3: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECONTA3'
      Size = 50
      Calculated = True
    end
    object QrResultLANOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
    object QrResultLAData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrResultLATipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrResultLACarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrResultLAControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrResultLASub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrResultLAAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrResultLAGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrResultLADescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrResultLANotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrResultLADebito: TFloatField
      FieldName = 'Debito'
    end
    object QrResultLACredito: TFloatField
      FieldName = 'Credito'
    end
    object QrResultLACompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrResultLADocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrResultLASit: TIntegerField
      FieldName = 'Sit'
    end
    object QrResultLAVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrResultLALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrResultLAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrResultLAFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrResultLAID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrResultLAID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrResultLAFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrResultLABanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrResultLALocal: TIntegerField
      FieldName = 'Local'
    end
    object QrResultLACartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrResultLALinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrResultLAOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrResultLALancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrResultLAPago: TFloatField
      FieldName = 'Pago'
    end
    object QrResultLAMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrResultLAFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrResultLACliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrResultLAMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrResultLAMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrResultLAProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrResultLADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrResultLADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrResultLAUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrResultLAUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrResultLADataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrResultLACtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrResultLANivel: TIntegerField
      FieldName = 'Nivel'
      Required = True
    end
    object QrResultLAVendedor: TIntegerField
      FieldName = 'Vendedor'
      Required = True
    end
    object QrResultLAAccount: TIntegerField
      FieldName = 'Account'
      Required = True
    end
    object QrResultLASubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrResultLAGrupo: TIntegerField
      FieldName = 'Grupo'
      Required = True
    end
    object QrResultLAConjunto: TIntegerField
      FieldName = 'Conjunto'
      Required = True
    end
    object QrResultLAFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object QrResultCO: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrResultCOCalcFields
    SQL.Strings = (
      'SELECT SUM(la.Debito) DEBITO, SUM(la.Credito) CREDITO,'
      'la.Data, la.Documento, la.NotaFiscal, '
      'co.Subgrupo, sg.Grupo, gr.Conjunto, '
      'co.Nome NOMECONTA, co.Nome2 NOMECONTA2,'
      'sg.Nome NOMESUBGRUPO, gr.Nome NOMEGRUPO, '
      'cj.Nome NOMECONJUNTO, cj.OrdemLista cjOrdemLista, '
      'gr.OrdemLista grOrdemLista, sg.OrdemLista sgOrdemLista, '
      'co.OrdemLista coOrdemLista '
      'FROM lanctos la, Contas co, Subgrupos sg,'
      'Grupos gr, Conjuntos cj'
      'WHERE la.Genero>0'
      'AND Data BETWEEN :P0 AND :P1 '
      'AND co.Codigo=la.Genero'
      'AND sg.Codigo=co.Subgrupo'
      'AND gr.Codigo=sg.Grupo'
      'AND cj.Codigo=gr.Conjunto'
      'AND co.Exclusivo="F"'
      'GROUP BY la.Genero'
      'ORDER BY cj.Nome, gr.Nome, sg.Nome, co.Nome, co.Nome2 ')
    Left = 152
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrResultCODEBITO: TFloatField
      FieldName = 'DEBITO'
      Origin = 'DBMMONEY.lanctos.Debito'
    end
    object QrResultCOCREDITO: TFloatField
      FieldName = 'CREDITO'
      Origin = 'DBMMONEY.lanctos.Credito'
    end
    object QrResultCONOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'DBMMONEY.contas.Nome'
      Size = 128
    end
    object QrResultCONOMECONTA2: TWideStringField
      FieldName = 'NOMECONTA2'
      Origin = 'DBMMONEY.contas.Nome2'
      Size = 128
    end
    object QrResultCONOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Origin = 'DBMMONEY.subgrupos.Nome'
      Size = 128
    end
    object QrResultCONOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Origin = 'DBMMONEY.grupos.Nome'
      Size = 128
    end
    object QrResultCONOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Origin = 'DBMMONEY.conjuntos.Nome'
      Size = 128
    end
    object QrResultCONOMECONTA3: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECONTA3'
      Size = 50
      Calculated = True
    end
    object QrResultCOData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrResultCODocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrResultCONotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrResultCOSubgrupo: TIntegerField
      FieldName = 'Subgrupo'
      Required = True
    end
    object QrResultCOGrupo: TIntegerField
      FieldName = 'Grupo'
      Required = True
    end
    object QrResultCOConjunto: TIntegerField
      FieldName = 'Conjunto'
      Required = True
    end
    object QrResultCOcjOrdemLista: TIntegerField
      FieldName = 'cjOrdemLista'
      Required = True
    end
    object QrResultCOgrOrdemLista: TIntegerField
      FieldName = 'grOrdemLista'
      Required = True
    end
    object QrResultCOsgOrdemLista: TIntegerField
      FieldName = 'sgOrdemLista'
      Required = True
    end
    object QrResultCOcoOrdemLista: TIntegerField
      FieldName = 'coOrdemLista'
      Required = True
    end
  end
  object QrSInicial: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Inicial) Inicial FROM carteiras')
    Left = 744
    Top = 8
    object QrSInicialInicial: TFloatField
      FieldName = 'Inicial'
      Origin = 'DBMMONEY.carteiras.Inicial'
    end
  end
  object QrMovimento: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(la.Credito) Credito, SUM(la.Debito) Debito'
      'FROM lanctos la, Contas co'
      'WHERE la.Genero > 0'
      'AND la.Data <=:P0'
      'AND co.Codigo=la.Genero'
      'AND co.Exclusivo='#39'F'#39)
    Left = 716
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMovimentoCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'DBMMONEY.lanctos.Credito'
    end
    object QrMovimentoDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'DBMMONEY.lanctos.Debito'
    end
  end
  object frxResultCO: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39652.739028310200000000
    ReportOptions.LastChange = 39652.739028310200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxResultCOGetValue
    Left = 96
    Top = 8
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsResultCO
        DataSetName = 'frxDsResultCO'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 60.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          Left = 544.881880000000000000
          Top = 4.881880000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 8.000000000000000000
          Top = 21.102350000000000000
          Width = 716.000000000000000000
          Height = 30.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Relat'#243'rio de Resultados por CONTAS')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 42.897650000000000000
        Top = 718.110700000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          Left = 473.102350000000000000
          Top = 2.550710000000090000
          Width = 244.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 20.897340000000000000
        Top = 483.779840000000000000
        Width = 718.110700000000000000
        object Memo8: TfrxMemoView
          Left = 411.968477090000000000
          Top = 2.897339999999990000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultCO."DEBITO">)]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 80.440940000000000000
          Top = 2.897340000000040000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 514.015728500000000000
          Top = 2.897339999999990000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultCO."CREDITO">)]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 144.440940000000000000
          Top = 2.897340000000040000
          Width = 256.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultCO."NOMESUBGRUPO"]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 616.062982360000000000
          Top = 2.897339999999990000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[<SUM(<frxDsResultCO."CREDITO">)> - <SUM(<frxDsResultCO."DEBITO"' +
              '>)>]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 936.661720000000000000
          Top = 17.795300000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultCO."DEBITO">)]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 1024.661720000000000000
          Top = 17.795300000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultCO."CREDITO">)]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 18.000000000000000000
        Top = 442.205010000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsResultCO
        DataSetName = 'frxDsResultCO'
        RowCount = 0
        object Memo15: TfrxMemoView
          Left = 116.000000000000000000
          Width = 292.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DataField = 'NOMECONTA3'
          DataSet = frxDsResultCO
          DataSetName = 'frxDsResultCO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsResultCO."NOMECONTA3"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 411.968477090000000000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          ShowHint = False
          DataSet = frxDsResultCO
          DataSetName = 'frxDsResultCO'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResultCO."DEBITO"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 514.015728500000000000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          ShowHint = False
          DataField = 'CREDITO'
          DataSet = frxDsResultCO
          DataSetName = 'frxDsResultCO'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResultCO."CREDITO"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 74.000000000000000000
        Top = 102.047310000000000000
        Width = 718.110700000000000000
        object Memo55: TfrxMemoView
          Left = 176.000000000000000000
          Top = 38.968460000000000000
          Width = 540.000000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Vecto]')
          ParentFont = False
        end
        object Picture2: TfrxPictureView
          Left = 4.000000000000000000
          Top = 5.291280000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo46: TfrxMemoView
          Left = 176.000000000000000000
          Top = 22.968460000000000000
          Width = 540.000000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Periodo]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 175.637910000000000000
          Top = 54.692950000000000000
          Width = 540.000000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[DtDoc]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 176.000000000000000000
          Width = 552.000000000000000000
          Height = 22.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 37.559060000000000000
        Top = 657.638220000000000000
        Width = 718.110700000000000000
        object Memo14: TfrxMemoView
          Left = 411.968770000000000000
          Top = 11.338590000000100000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultCO."DEBITO">)]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 514.016021410000000000
          Top = 11.338590000000100000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultCO."CREDITO">)]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 616.063275270000000000
          Top = 11.338590000000100000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[<SUM(<frxDsResultCO."CREDITO">)> - <SUM(<frxDsResultCO."DEBITO"' +
              '>)>]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 79.370130000000000000
          Top = 11.338590000000100000
          Width = 322.110390000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'RESULTADO DO PER'#205'ODO:')
          ParentFont = False
        end
        object Line3: TfrxLineView
          Top = 11.338590000000100000
          Width = 718.110700000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        Height = 26.000000000000000000
        Top = 200.315090000000000000
        Width = 718.110700000000000000
        object Memo29: TfrxMemoView
          Left = 0.440940000000000000
          Top = 2.582560000000000000
          Width = 56.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 60.440940000000000000
          Top = 2.582560000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haBlock
          Memo.UTF8W = (
            'Doc.')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 112.440940000000000000
          Top = 2.582560000000000000
          Width = 284.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 400.440940000000000000
          Top = 2.582560000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haBlock
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 453.543307086614000000
          Top = 2.582560000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 544.440940000000000000
          Top = 2.582560000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 631.181102362205100000
          Top = 2.582560000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        Height = 22.000000000000000000
        Top = 396.850650000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsResultCO."NOMESUBGRUPO"'
        object Memo2: TfrxMemoView
          Left = 49.574830000000000000
          Top = 1.826529999999990000
          Width = 664.787570000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultCO."NOMESUBGRUPO"]')
          ParentFont = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        Height = 22.645330000000000000
        Top = 529.134200000000000000
        Width = 718.110700000000000000
        object Memo7: TfrxMemoView
          Left = 80.440940000000000000
          Top = 1.322509999999970000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 144.440940000000000000
          Top = 1.322509999999970000
          Width = 256.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultCO."NOMEGRUPO"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 411.968477090000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultCO."DEBITO">)]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 514.015728500000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultCO."CREDITO">)]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 616.062982360000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[<SUM(<frxDsResultCO."CREDITO">)> - <SUM(<frxDsResultCO."DEBITO"' +
              '>)>]')
          ParentFont = False
        end
      end
      object GroupFooter4: TfrxGroupFooter
        Height = 21.779530000000000000
        Top = 574.488560000000000000
        Width = 718.110700000000000000
        object Memo37: TfrxMemoView
          Left = 80.440940000000000000
          Top = 3.527209999999970000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 144.440940000000000000
          Top = 3.527209999999970000
          Width = 256.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultCO."NOMECONJUNTO"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 411.968477090000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultCO."DEBITO">)]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 514.015728500000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultCO."CREDITO">)]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 616.062982360000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            
              '[<SUM(<frxDsResultCO."CREDITO">)> - <SUM(<frxDsResultCO."DEBITO"' +
              '>)>]')
          ParentFont = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        Height = 28.000000000000000000
        Top = 347.716760000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsResultCO."NOMEGRUPO"'
        object Line2: TfrxLineView
          Left = 8.000000000000000000
          Top = 25.180890000000000000
          Width = 732.000000000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Memo39: TfrxMemoView
          Left = 8.000000000000000000
          Top = 3.180889999999980000
          Width = 706.362400000000000000
          Height = 22.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultCO."NOMEGRUPO"]')
          ParentFont = False
        end
      end
      object GroupHeader4: TfrxGroupHeader
        Height = 36.000000000000000000
        Top = 287.244280000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsResultCO."NOMECONJUNTO"'
        object Line1: TfrxLineView
          Left = 8.000000000000000000
          Top = 30.094310000000000000
          Width = 732.000000000000000000
          ShowHint = False
          Frame.Style = fsDouble
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
        object Memo40: TfrxMemoView
          Left = 8.000000000000000000
          Top = 0.094310000000007220
          Width = 706.362400000000000000
          Height = 26.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultCO."NOMECONJUNTO"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsResultCO: TfrxDBDataset
    UserName = 'frxDsResultCO'
    CloseDataSource = False
    DataSet = QrResultCO
    BCDToCurrency = False
    Left = 124
    Top = 8
  end
  object frxResultLA: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39652.818819074100000000
    ReportOptions.LastChange = 39652.818819074100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  ReportSummary1.Visible := <VARF_SALDOFINAL_VISIBLE>;          ' +
        '                                 '
      'end.')
    OnGetValue = frxResultCOGetValue
    Left = 8
    Top = 8
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsResultLA
        DataSetName = 'frxDsResultLA'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 26.456692910000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo32: TfrxMemoView
          Left = 491.338582677165000000
          Width = 188.976377952756000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Width = 680.314960630000000000
          Height = 26.456692910000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Relat'#243'rio de Resultados por LAN'#199'AMENTOS')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 20.330240000000000000
        Top = 737.008350000000000000
        Width = 680.315400000000000000
        object Memo31: TfrxMemoView
          Left = 434.881880000000000000
          Top = 2.330240000000000000
          Width = 244.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 18.000000000000000000
        Top = 480.000310000000000000
        Width = 680.315400000000000000
        object Memo8: TfrxMemoView
          Left = 453.543282680000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Debito">)]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = -0.000029290000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 529.133833860000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Credito">)]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 68.031471650000000000
          Width = 385.511894020000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultLA."NOMECONTA3"]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 604.724385040000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Credito">) - SUM(<frxDsResultLA."Debito">)]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        Height = 19.826530000000000000
        Top = 355.275820000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsResultLA."NOMESUBGRUPO"'
        object Memo2: TfrxMemoView
          Top = 1.826530000000000000
          Width = 430.456710000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultLA."NOMESUBGRUPO"]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 15.118110240000000000
        Top = 442.205010000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsResultLA
        DataSetName = 'frxDsResultLA'
        RowCount = 0
        object Memo13: TfrxMemoView
          Width = 45.354330710000000000
          Height = 15.118110236220500000
          ShowHint = False
          DataField = 'Data'
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[frxDsResultLA."Data"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 45.354330710000000000
          Width = 45.354330710000000000
          Height = 15.118110236220500000
          ShowHint = False
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[frxDsResultLA."Documento"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 90.708661420000000000
          Width = 264.566929133858000000
          Height = 15.118110236220500000
          ShowHint = False
          DataField = 'Descricao'
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsResultLA."Descricao"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 355.275566140000000000
          Width = 52.913385830000000000
          Height = 15.118110236220500000
          ShowHint = False
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39', <frxDsResultLA."NotaFiscal">)]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 453.543282680000000000
          Width = 75.590551180000000000
          Height = 15.118110236220500000
          ShowHint = False
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResultLA."Debito"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 529.133833860000000000
          Width = 75.590551180000000000
          Height = 15.118110236220500000
          ShowHint = False
          DataField = 'Credito'
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResultLA."Credito"]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 408.188951970000000000
          Width = 45.354330710000000000
          Height = 15.118110236220500000
          ShowHint = False
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsResultLA."Controle"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 604.724385040000000000
          Width = 75.590551180000000000
          Height = 15.118110236220500000
          ShowHint = False
          DataField = 'NOMECARTEIRA'
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsResultLA."NOMECARTEIRA"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 73.653670240000000000
        Top = 68.031540000000000000
        Width = 680.315400000000000000
        object Picture2: TfrxPictureView
          Left = 3.322820000000000000
          Top = 1.511750000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo55: TfrxMemoView
          Left = 196.897650000000000000
          Top = 39.031540000000000000
          Width = 540.000000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Vecto]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 196.897650000000000000
          Top = 23.031540000000000000
          Width = 540.000000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Periodo]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 196.535560000000000000
          Top = 54.756030000000000000
          Width = 540.000000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[DtDoc]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 196.897650000000000000
          Width = 552.000000000000000000
          Height = 22.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 30.000000000000000000
        Top = 684.094930000000000000
        Width = 680.315400000000000000
        object Memo12: TfrxMemoView
          Left = -0.236240000000000000
          Top = 6.582250000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo anterior:')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 95.763760000000000000
          Top = 6.582250000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[Anterior]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 191.763760000000000000
          Top = 6.582250000000000000
          Width = 56.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito:')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 391.763760000000000000
          Top = 6.582250000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[SCredito]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 331.763760000000000000
          Top = 6.582250000000000000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito:')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 475.763760000000000000
          Top = 6.582250000000000000
          Width = 96.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo Final:')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 571.763760000000000000
          Top = 6.582250000000000000
          Width = 107.338590000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[Final]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 247.763760000000000000
          Top = 6.582250000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[SDebito]')
          ParentFont = False
        end
        object Line13: TfrxLineView
          Top = 3.779530000000000000
          Width = 679.338590000000000000
          ShowHint = False
          Frame.Style = fsDouble
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        Height = 18.440940000000000000
        Top = 166.299320000000000000
        Width = 680.315400000000000000
        object Memo29: TfrxMemoView
          Width = 45.354330708661400000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 45.354330710000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haBlock
          Memo.UTF8W = (
            'Doc.')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 90.708661420000000000
          Width = 264.566929133858000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 355.275566140000000000
          Width = 52.913385830000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haBlock
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 453.543282680000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 529.133833860000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 604.724385040000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Nome carteira')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 408.188951970000000000
          Width = 45.354330710000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haBlock
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 22.000000000000000000
        Top = 396.850650000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsResultLA."NOMECONTA3"'
        object Memo1: TfrxMemoView
          Top = 0.251700000000000000
          Width = 430.456710000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultLA."NOMECONTA3"]')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        Height = 18.000000000000000000
        Top = 521.575140000000000000
        Width = 680.315400000000000000
        object Memo4: TfrxMemoView
          Left = -0.000029290000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 68.031471650000000000
          Width = 385.511894020000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultLA."NOMESUBGRUPO"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 453.543282680000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Debito">)]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 529.133833860000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Credito">)]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 604.724385040000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Credito">) - SUM(<frxDsResultLA."Debito">)]')
          ParentFont = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        Height = 18.000000000000000000
        Top = 563.149970000000000000
        Width = 680.315400000000000000
        object Memo7: TfrxMemoView
          Left = -0.000029290000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 68.031471650000000000
          Width = 385.511894020000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultLA."NOMEGRUPO"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 453.543282680000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Debito">)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 529.133833860000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Credito">)]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 604.724385040000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Credito">) - SUM(<frxDsResultLA."Debito">)]')
          ParentFont = False
        end
      end
      object GroupFooter4: TfrxGroupFooter
        Height = 18.000000000000000000
        Top = 604.724800000000000000
        Width = 680.315400000000000000
        object Memo37: TfrxMemoView
          Left = -0.000029290000000000
          Width = 60.472440940000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 68.031471650000000000
          Width = 385.511894020000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultLA."NOMECONJUNTO"]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 453.543282680000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Debito">)]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 529.133833860000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Credito">)]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 604.724385040000000000
          Width = 75.590551180000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsResultLA."Credito">) - SUM(<frxDsResultLA."Debito">)]')
          ParentFont = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        Height = 28.000000000000000000
        Top = 306.141930000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsResultLA."NOMEGRUPO"'
        object Line2: TfrxLineView
          Top = 22.677165350000000000
          Width = 678.897650000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Memo39: TfrxMemoView
          Width = 430.456710000000000000
          Height = 22.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultLA."NOMEGRUPO"]')
          ParentFont = False
        end
      end
      object GroupHeader4: TfrxGroupHeader
        Height = 36.000000000000000000
        Top = 245.669450000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsResultLA."NOMECONJUNTO"'
        object Line1: TfrxLineView
          Top = 30.094310000000000000
          Width = 678.897650000000000000
          ShowHint = False
          Frame.Style = fsDouble
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
        object Memo40: TfrxMemoView
          Top = 0.094310000000000000
          Width = 430.456710000000000000
          Height = 26.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsResultLA."NOMECONJUNTO"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsResultLA: TfrxDBDataset
    UserName = 'frxDsResultLA'
    CloseDataSource = False
    DataSet = QrResultLA
    BCDToCurrency = False
    Left = 36
    Top = 8
  end
  object frxInd01: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39652.739028310200000000
    ReportOptions.LastChange = 39652.739028310200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxResultCOGetValue
    Left = 552
    Top = 8
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsInd01
        DataSetName = 'frxDsInd01'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 60.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          Left = 544.881880000000000000
          Top = 4.881880000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 8.000000000000000000
          Top = 21.102350000000000000
          Width = 716.000000000000000000
          Height = 30.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'BALANCETE MENSAL')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 42.897650000000000000
        Top = 718.110700000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          Left = 473.102350000000000000
          Top = 2.550710000000090000
          Width = 244.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 20.897340000000000000
        Top = 483.779840000000000000
        Width = 718.110700000000000000
        object Memo8: TfrxMemoView
          Left = 411.968477090000000000
          Top = 2.897339999999990000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsInd01."DEBITO">)]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 80.440940000000000000
          Top = 2.897340000000040000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 514.015728500000000000
          Top = 2.897339999999990000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsInd01."CREDITO">)]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 144.440940000000000000
          Top = 2.897340000000040000
          Width = 256.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsInd01."NOMESUBGRUPO"]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 616.062982360000000000
          Top = 2.897339999999990000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[<SUM(<frxDsInd01."CREDITO">)> - <SUM(<frxDsInd01."DEBITO">)>]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 936.661720000000000000
          Top = 17.795300000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsInd01."DEBITO">)]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 1024.661720000000000000
          Top = 17.795300000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsInd01."CREDITO">)]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 18.000000000000000000
        Top = 442.205010000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsInd01
        DataSetName = 'frxDsInd01'
        RowCount = 0
        object Memo15: TfrxMemoView
          Left = 116.000000000000000000
          Width = 292.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DataField = 'NOMECONTA3'
          DataSet = frxDsInd01
          DataSetName = 'frxDsInd01'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsInd01."NOMECONTA3"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 411.968477090000000000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          ShowHint = False
          DataSet = frxDsInd01
          DataSetName = 'frxDsInd01'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsInd01."DEBITO"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 514.015728500000000000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          ShowHint = False
          DataField = 'CREDITO'
          DataSet = frxDsInd01
          DataSetName = 'frxDsInd01'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsInd01."CREDITO"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 74.000000000000000000
        Top = 102.047310000000000000
        Width = 718.110700000000000000
        object Memo55: TfrxMemoView
          Left = 176.000000000000000000
          Top = 38.968460000000000000
          Width = 540.000000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Vecto]')
          ParentFont = False
        end
        object Picture2: TfrxPictureView
          Left = 4.000000000000000000
          Top = 5.291280000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo46: TfrxMemoView
          Left = 176.000000000000000000
          Top = 22.968460000000000000
          Width = 540.000000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Periodo]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 175.637910000000000000
          Top = 54.692950000000000000
          Width = 264.094310000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[DtDoc]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 176.000000000000000000
          Width = 552.000000000000000000
          Height = 22.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 445.984540000000000000
          Top = 54.803149610000000000
          Width = 267.873840000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[DI01]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 37.559060000000000000
        Top = 657.638220000000000000
        Width = 718.110700000000000000
        object Memo14: TfrxMemoView
          Left = 411.968770000000000000
          Top = 11.338590000000100000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsInd01."DEBITO">)]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 514.016021410000000000
          Top = 11.338590000000100000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsInd01."CREDITO">)]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 616.063275270000000000
          Top = 11.338590000000000000
          Width = 102.047244090000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[<SUM(<frxDsInd01."CREDITO">)> - <SUM(<frxDsInd01."DEBITO">)>]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 79.370130000000000000
          Top = 11.338590000000100000
          Width = 322.110390000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'RESULTADO DO PER'#205'ODO:')
          ParentFont = False
        end
        object Line3: TfrxLineView
          Top = 11.338590000000100000
          Width = 718.110700000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        Height = 26.000000000000000000
        Top = 200.315090000000000000
        Width = 718.110700000000000000
        object Memo29: TfrxMemoView
          Left = 0.440940000000000000
          Top = 2.582560000000000000
          Width = 56.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 60.440940000000000000
          Top = 2.582560000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haBlock
          Memo.UTF8W = (
            'Doc.')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 112.440940000000000000
          Top = 2.582560000000000000
          Width = 284.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 400.440940000000000000
          Top = 2.582560000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haBlock
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 453.543307086614000000
          Top = 2.582560000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 544.440940000000000000
          Top = 2.582560000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 631.181102362205100000
          Top = 2.582560000000000000
          Width = 84.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        Height = 22.000000000000000000
        Top = 396.850650000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsInd01."NOMESUBGRUPO"'
        object Memo2: TfrxMemoView
          Left = 49.574830000000000000
          Top = 1.826529999999990000
          Width = 664.787570000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsInd01."NOMESUBGRUPO"]')
          ParentFont = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        Height = 22.645330000000000000
        Top = 529.134200000000000000
        Width = 718.110700000000000000
        object Memo7: TfrxMemoView
          Left = 80.440940000000000000
          Top = 1.322509999999970000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 144.440940000000000000
          Top = 1.322509999999970000
          Width = 256.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsInd01."NOMEGRUPO"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 411.968477090000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsInd01."DEBITO">)]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 514.015728500000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsInd01."CREDITO">)]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 616.062982360000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[<SUM(<frxDsInd01."CREDITO">)> - <SUM(<frxDsInd01."DEBITO">)>]')
          ParentFont = False
        end
      end
      object GroupFooter4: TfrxGroupFooter
        Height = 21.779530000000000000
        Top = 574.488560000000000000
        Width = 718.110700000000000000
        object Memo37: TfrxMemoView
          Left = 80.440940000000000000
          Top = 3.527209999999970000
          Width = 60.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 144.440940000000000000
          Top = 3.527209999999970000
          Width = 256.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsInd01."NOMECONJUNTO"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 411.968477090000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsInd01."DEBITO">)]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 514.015728500000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsInd01."CREDITO">)]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 616.062982360000000000
          Top = 3.779530000000020000
          Width = 102.047244094488000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[<SUM(<frxDsInd01."CREDITO">)> - <SUM(<frxDsInd01."DEBITO">)>]')
          ParentFont = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        Height = 28.000000000000000000
        Top = 347.716760000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsInd01."NOMEGRUPO"'
        object Line2: TfrxLineView
          Left = 8.000000000000000000
          Top = 25.180890000000000000
          Width = 732.000000000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Memo39: TfrxMemoView
          Left = 8.000000000000000000
          Top = 3.180889999999980000
          Width = 706.362400000000000000
          Height = 22.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsInd01."NOMEGRUPO"]')
          ParentFont = False
        end
      end
      object GroupHeader4: TfrxGroupHeader
        Height = 36.000000000000000000
        Top = 287.244280000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsInd01."NOMECONJUNTO"'
        object Line1: TfrxLineView
          Left = 8.000000000000000000
          Top = 30.094310000000000000
          Width = 732.000000000000000000
          ShowHint = False
          Frame.Style = fsDouble
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
        object Memo40: TfrxMemoView
          Left = 8.000000000000000000
          Top = 0.094310000000007220
          Width = 706.362400000000000000
          Height = 26.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsInd01."NOMECONJUNTO"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsInd01: TfrxDBDataset
    UserName = 'frxDsInd01'
    CloseDataSource = False
    DataSet = QrInd01
    BCDToCurrency = False
    Left = 580
    Top = 8
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMECLI'
      'FROM entidades'
      'ORDER BY NOMECLI'
      ' '#10
      ''
      ''
      '')
    Left = 948
    Top = 36
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Required = True
      Size = 100
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 976
    Top = 36
  end
  object frxExtratLA: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39652.818819074100000000
    ReportOptions.LastChange = 39652.818819074100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Acumulado: Extended;'
      '    '
      'procedure MasterData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  if Engine.FinalPass then                                      ' +
        '            '
      
        '    Acumulado := Acumulado - <frxDsResultLA."Debito"> + <frxDsRe' +
        'sultLA."Credito">;                                              ' +
        '                    '
      'end;'
      ''
      'procedure GroupHeader1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <VARF_ZERA_ACUM_CTA> then'
      '    Acumulado := 0;                                         '
      'end;'
      ''
      'begin'
      '  Acumulado := 0;'
      
        '  GroupHeader1.Visible := <VARF_AGRUPA_CTA>;                    ' +
        '                                                                ' +
        '                '
      
        '  GroupFooter1.Visible := <VARF_AGRUPA_CTA>;                    ' +
        '                                                                ' +
        '                '
      'end.')
    OnGetValue = frxResultCOGetValue
    Left = 184
    Top = 8
    Datasets = <
      item
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsResultLA
        DataSetName = 'frxDsResultLA'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageFooter1: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 404.409710000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 15.118110240000000000
        Top = 306.141930000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'MasterData1OnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsResultLA
        DataSetName = 'frxDsResultLA'
        RowCount = 0
        object Memo13: TfrxMemoView
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Data'
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[frxDsResultLA."Data"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 45.354330710000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39',<frxDsResultLA."Documento">)]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 90.708661420000000000
          Width = 264.566929130000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataField = 'Descricao'
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsResultLA."Descricao"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 355.275566140000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39', <frxDsResultLA."NotaFiscal">)]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 453.543282680000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39',<frxDsResult' +
              'LA."Debito">)]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 529.133833860000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[FormatFloat('#39'#,###,###,##0.00;-#,###,###,##0.00; '#39',<frxDsResult' +
              'LA."Credito">)]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 408.188951970000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39',<frxDsResultLA."Controle">)]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 604.724385040000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[ACUMULADO]')
          ParentFont = False
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        Height = 15.118110240000000000
        Top = 188.976500000000000000
        Width = 680.315400000000000000
        object Memo29: TfrxMemoView
          Width = 45.354330708661400000
          Height = 15.118110236220470000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 45.354330710000000000
          Width = 45.354330708661420000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'Doc.')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 90.708661420000000000
          Width = 264.566929133858000000
          Height = 15.118110236220470000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 355.275566140000000000
          Width = 52.913385830000000000
          Height = 15.118110236220470000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 453.543282680000000000
          Width = 75.590551180000000000
          Height = 15.118110236220470000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 529.133833860000000000
          Width = 75.590551180000000000
          Height = 15.118110236220470000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 604.724385040000000000
          Width = 75.590551180000000000
          Height = 15.118110236220470000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Acumulado')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 408.188951970000000000
          Width = 45.354330710000000000
          Height = 15.118110236220470000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
        end
      end
      object PageHeader4: TfrxPageHeader
        Height = 147.401670000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          Top = 102.047310000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Shape3: TfrxShapeView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          Left = 7.559060000000000000
          Top = 37.795300000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 151.181200000000000000
          Top = 56.692950000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'RESULTADO POR EXTRATO DE LAN'#199'AMENTOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 56.692950000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Left = 529.134200000000000000
          Top = 56.692950000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 79.370130000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NOME_EMPRESA]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Left = 7.559060000000000000
          Top = 102.047310000000000000
          Width = 665.196850390000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODOS]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 120.944960000000000000
          Width = 665.196850390000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NOME_NIVEL]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 18.897650000000000000
        Top = 264.567100000000000000
        Width = 680.315400000000000000
        OnBeforePrint = 'GroupHeader1OnBeforePrint'
        Condition = 'frxDsResultLA."NOMECONTA"'
        object Memo3: TfrxMemoView
          Top = 3.779530000000000000
          Width = 680.315229130000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = frxDsResultLA
          DataSetName = 'frxDsResultLA'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              '[FormatFloat('#39'000'#39', <frxDsResultLA."Genero">)] - [frxDsResultLA.' +
              '"NOMECONTA"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Top = 343.937230000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object DsResultLA: TDataSource
    DataSet = QrResultLA
    Left = 64
    Top = 36
  end
  object QrInd01: TmySQLQuery
    Database = Dmod.ZZDB
    Left = 608
    Top = 8
  end
end
