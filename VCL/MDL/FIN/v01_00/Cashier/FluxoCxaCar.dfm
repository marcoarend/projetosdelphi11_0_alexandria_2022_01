object FmFluxoCxaCar: TFmFluxoCxaCar
  Left = 339
  Top = 185
  Caption = 'FIN-FLCXA-002 :: Fluxo de caixa por carteira'
  ClientHeight = 379
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 331
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtImprime: TBitBtn
      Tag = 5
      Left = 9
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Imprime'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtImprimeClick
    end
    object Panel2: TPanel
      Left = 672
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Fluxo de caixa por carteira'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 782
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = -4
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 283
    Align = alClient
    TabOrder = 0
    object PainelA: TPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 52
      Align = alTop
      TabOrder = 0
      object Label9: TLabel
        Left = 8
        Top = 8
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 24
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 64
        Top = 24
        Width = 709
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 53
      Width = 782
      Height = 70
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object GroupBox1: TGroupBox
        Left = 8
        Top = 2
        Width = 200
        Height = 65
        Caption = ' Per'#237'odo: '
        TabOrder = 0
        object CkIni: TCheckBox
          Left = 8
          Top = 16
          Width = 90
          Height = 17
          Caption = 'Data inicial:'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object TPIni: TDateTimePicker
          Left = 8
          Top = 36
          Width = 90
          Height = 21
          CalColors.TextColor = clMenuText
          Date = 37636.777157974500000000
          Time = 37636.777157974500000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object CkFim: TCheckBox
          Left = 100
          Top = 16
          Width = 90
          Height = 17
          Caption = 'Data final:'
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
        object TPFim: TDateTimePicker
          Left = 100
          Top = 36
          Width = 90
          Height = 21
          Date = 37636.777203761600000000
          Time = 37636.777203761600000000
          TabOrder = 3
        end
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 123
      Width = 782
      Height = 70
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object Label4: TLabel
        Left = 214
        Top = 40
        Width = 432
        Height = 13
        Caption = 
          '** Se a data informada for inferior a data inicial, os valores v' +
          'encidos ser'#227'o desconsiderados.'
      end
      object Label3: TLabel
        Left = 214
        Top = 21
        Width = 534
        Height = 13
        Caption = 
          '* Data em que se estima recebimentos (cr'#233'ditos) e/ou pagamentos ' +
          '(d'#233'bitos) de valores vencidos e n'#227'o quitados. '
      end
      object GroupBox2: TGroupBox
        Left = 8
        Top = 2
        Width = 200
        Height = 65
        Caption = ' Datas futuras de valores pendentes*: '
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 20
          Width = 49
          Height = 13
          Caption = 'Cr'#233'ditos**:'
        end
        object Label2: TLabel
          Left = 100
          Top = 20
          Width = 47
          Height = 13
          Caption = 'D'#233'bitos**:'
        end
        object TPCre: TDateTimePicker
          Left = 8
          Top = 36
          Width = 90
          Height = 21
          CalColors.TextColor = clMenuText
          Date = 37636.777157974500000000
          Time = 37636.777157974500000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object TPDeb: TDateTimePicker
          Left = 100
          Top = 36
          Width = 90
          Height = 21
          Date = 37636.777203761600000000
          Time = 37636.777203761600000000
          TabOrder = 1
        end
      end
    end
    object Panel5: TPanel
      Left = 1
      Top = 193
      Width = 782
      Height = 70
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object RGFonte: TRadioGroup
        Left = 8
        Top = 2
        Width = 90
        Height = 65
        Caption = ' Fonte: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          '06'
          '07'
          '08'
          'xx')
        TabOrder = 0
      end
      object CkSubstituir: TCheckBox
        Left = 108
        Top = 15
        Width = 241
        Height = 17
        Caption = 'Substituir a descri'#231#227'o pelo cliente \ fornecedor'
        TabOrder = 1
      end
      object CkDocEmAberto: TCheckBox
        Left = 108
        Top = 38
        Width = 319
        Height = 17
        Caption = 'Considerar datas originais de documentos em aberto no saldo'
        TabOrder = 2
      end
    end
    object PB1: TProgressBar
      Left = 1
      Top = 265
      Width = 782
      Height = 17
      Align = alBottom
      TabOrder = 4
      Visible = False
    end
  end
  object QrIni: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Inicial) Valor'
      'FROM carteiras '
      'WHERE Tipo <> 2'
      'AND ForneceI=:P0')
    Left = 536
    Top = 194
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrIniValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrMovant: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrMovantCalcFields
    SQL.Strings = (
      'SELECT SUM(lan.Credito-lan.Debito) Movim, '
      'lan.Carteira, car.Nome'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'WHERE car.Tipo <> 2'
      'AND car.ForneceI=:P0'
      'AND lan.Carteira=:P1'
      'AND Data < :P2'
      'GROUP BY lan.Carteira')
    Left = 564
    Top = 194
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrMovantMovim: TFloatField
      FieldName = 'Movim'
    end
    object QrMovantVALOR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALOR'
      Calculated = True
    end
    object QrMovantCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrMovantNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrFlxo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT Date(IF(car.Tipo <> 2, lan.Data,IF(lan.Sit > 1, lan.Compe' +
        'nsado, IF(lan.Vencimento<SYSDATE(), IF(lan.Debito>0, "2007-12-11' +
        '", "2007-12-11"), lan.Vencimento)))) QUITACAO, '
      'lan.*, car.Nome NOMECART,'
      'IF(lan.Cliente>0,'
      '  IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome),'
      '  IF (lan.Fornecedor>0,'
      
        '    IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome), "")) NOMERELACIONADO' +
        ','
      'car.Tipo CARTIP, car.Banco CARBAN '
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN entidades cl ON cl.Codigo=lan.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=lan.Fornecedor'
      'WHERE car.Fornecei=1'
      
        'AND Date(IF(car.Tipo <> 2, lan.Data,IF(lan.Sit > 1, lan.Compensa' +
        'do, IF(lan.Vencimento<SYSDATE(), IF(lan.Debito>0, "2007-12-11", ' +
        '"2007-12-11"), lan.Vencimento)))) >= "2007-12-11"'
      'ORDER BY QUITACAO, Credito DESC, Debito')
    Left = 620
    Top = 194
    object QrFlxoQUITACAO: TWideStringField
      FieldName = 'QUITACAO'
      Size = 10
    end
    object QrFlxoData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrFlxoTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrFlxoCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrFlxoControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrFlxoSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrFlxoAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrFlxoGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrFlxoQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrFlxoDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrFlxoNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrFlxoDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrFlxoCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrFlxoCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrFlxoSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrFlxoDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrFlxoSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrFlxoVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrFlxoFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrFlxoFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrFlxoFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrFlxoID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrFlxoID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrFlxoFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrFlxoEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrFlxoBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrFlxoContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrFlxoCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrFlxoLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrFlxoCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrFlxoLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrFlxoOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrFlxoLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrFlxoPago: TFloatField
      FieldName = 'Pago'
    end
    object QrFlxoMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrFlxoFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrFlxoCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrFlxoCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrFlxoForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrFlxoMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrFlxoMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrFlxoMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrFlxoMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrFlxoProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrFlxoDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrFlxoCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrFlxoNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrFlxoVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrFlxoAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrFlxoICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrFlxoICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrFlxoDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrFlxoDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrFlxoDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrFlxoDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrFlxoDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Required = True
    end
    object QrFlxoUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrFlxoNFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrFlxoAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrFlxoExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrFlxoDoc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrFlxoCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
    end
    object QrFlxoTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Required = True
    end
    object QrFlxoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFlxoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFlxoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFlxoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFlxoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFlxoNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Size = 100
    end
    object QrFlxoNOMERELACIONADO: TWideStringField
      FieldName = 'NOMERELACIONADO'
      Size = 100
    end
    object QrFlxoCARTIP: TIntegerField
      FieldName = 'CARTIP'
    end
    object QrFlxoCARBAN: TIntegerField
      FieldName = 'CARBAN'
    end
    object QrFlxoAgencia: TIntegerField
      FieldName = 'Agencia'
    end
  end
  object DsFlxo: TDataSource
    DataSet = QrFlxo
    Left = 648
    Top = 194
  end
  object QrExtr: TmySQLQuery
    Database = DModG.MyPID_DB
    BeforeOpen = QrExtrBeforeOpen
    OnCalcFields = QrExtrCalcFields
    SQL.Strings = (
      'SELECT loc.*, car.Nome NOMECAR'
      'FROM extratocc2 loc'
      'LEFT JOIN synker.carteiras car ON car.Codigo = loc.tCrt'
      'ORDER BY NOMECAR, Codig, Texto')
    Left = 620
    Top = 222
    object QrExtratoDataE: TDateField
      FieldName = 'DataE'
      Origin = 'extratocc2.DataE'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrExtratoDataV: TDateField
      FieldName = 'DataV'
      Origin = 'extratocc2.DataV'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrExtratoDataQ: TDateField
      FieldName = 'DataQ'
      Origin = 'extratocc2.DataQ'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrExtratoDataX: TDateField
      FieldName = 'DataX'
      Origin = 'extratocc2.DataX'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrExtratoTexto: TWideStringField
      FieldName = 'Texto'
      Origin = 'extratocc2.Texto'
      Size = 255
    end
    object QrExtratoDocum: TWideStringField
      FieldName = 'Docum'
      Origin = 'extratocc2.Docum'
      Size = 30
    end
    object QrExtratoNotaF: TWideStringField
      FieldName = 'NotaF'
      Origin = 'extratocc2.NotaF'
      Size = 30
    end
    object QrExtratoSaldo: TFloatField
      FieldName = 'Saldo'
      Origin = 'extratocc2.Saldo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrExtrCredi: TFloatField
      FieldName = 'Credi'
      Origin = 'extratocc2.Credi'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrExtrDebit: TFloatField
      FieldName = 'Debit'
      Origin = 'extratocc2.Debit'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrExtratoCartC: TIntegerField
      FieldName = 'CartC'
      Origin = 'extratocc2.CartC'
    end
    object QrExtratoCartN: TWideStringField
      FieldName = 'CartN'
      Origin = 'extratocc2.CartN'
      Size = 100
    end
    object QrExtratoCodig: TIntegerField
      FieldName = 'Codig'
      Origin = 'extratocc2.Codig'
    end
    object QrExtratoCtrle: TIntegerField
      FieldName = 'Ctrle'
      Origin = 'extratocc2.Ctrle'
    end
    object QrExtratoCtSub: TIntegerField
      FieldName = 'CtSub'
      Origin = 'extratocc2.CtSub'
    end
    object QrExtratoCART_ORIG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'CART_ORIG'
      LookupDataSet = QrLct
      LookupKeyFields = 'Controle'
      LookupResultField = 'NOMECART'
      KeyFields = 'ID_Pg'
      Size = 100
      Lookup = True
    end
    object QrExtratoID_Pg: TIntegerField
      FieldName = 'ID_Pg'
      Origin = 'extratocc2.ID_Pg'
    end
    object QrExtratoTipoI: TIntegerField
      FieldName = 'TipoI'
      Origin = 'extratocc2.TipoI'
    end
    object QrExtrVALOR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALOR'
      Calculated = True
    end
    object QrExtrtCrt: TIntegerField
      FieldName = 'tCrt'
    end
    object QrExtrNOMECAR: TWideStringField
      FieldName = 'NOMECAR'
      Size = 100
    end
  end
  object DsExtr: TDataSource
    DataSet = QrExtr
    Left = 648
    Top = 222
  end
  object frxFin_Relat_005_01_A06: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.635407800900000000
    ReportOptions.LastChange = 40623.739010775460000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxFin_Relat_005_01_A06GetValue
    Left = 592
    Top = 166
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsExtr
        DataSetName = 'frxDsExtr'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ColumnHeader1: TfrxColumnHeader
        Height = 13.228346460000000000
        Top = 128.000000000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          Left = 35.905511811023620000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'S'#233'rie - Doc.')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 130.393700790000000000
          Width = 175.748031500000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[SUBST]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 96.377952755905510000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 306.141732280000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 519.685039370000000000
          Width = 126.614173228346500000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Carteira')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Width = 35.905511810000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Dta fluxo')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 646.299630000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ID')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 359.055118110236200000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 411.968503937007900000
          Width = 35.905511810000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Emiss.')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 447.874015748031500000
          Width = 35.905511810000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Venceu')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 483.779527560000000000
          Width = 35.905511811023620000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Quita'#231#227'o')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 22.677180000000000000
        Top = 416.000000000000000000
        Width = 680.315400000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo23: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 90.708720000000000000
        Top = 16.000000000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo38: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[NOMEREL]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_NOMEEMPRESA]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Top = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo:  [PERIODO]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 13.228346460000000000
        Top = 252.000000000000000000
        Width = 680.315400000000000000
        DataSet = frxDsExtr
        DataSetName = 'frxDsExtr'
        RowCount = 0
        object Memo3: TfrxMemoView
          Left = 35.905511810000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Docum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[frxDsExtr."Docum"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 130.393700790000000000
          Width = 175.748031500000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Texto'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."Texto"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 96.377952760000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'NotaF'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[frxDsExtr."NotaF"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 306.141732280000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."VALOR"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 519.685039370000000000
          Width = 126.614173230000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'CartN'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."CartN"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Width = 35.905511810000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'DataX'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataX"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 646.299630000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Ctrle'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."Ctrle"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 359.055118110000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."Saldo"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 411.968503940000000000
          Width = 35.905511810000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'DataE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataE"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 447.874015750000000000
          Width = 35.905511810000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'DataV'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataV"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 483.779527560000000000
          Width = 35.905511810000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'DataQ'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataQ"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 13.228346460000000000
        Top = 200.000000000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsExtr."NOMECAR"'
        object Memo26: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Carteira: [frxDsExtr."NOMECAR"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Top = 316.000000000000000000
        Width = 680.315400000000000000
      end
      object GroupHeader2: TfrxGroupHeader
        Top = 232.000000000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsExtr."DataX"'
      end
      object GroupFooter2: TfrxGroupFooter
        Height = 13.228346460000000000
        Top = 284.000000000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          Left = 362.834880000000000000
          Width = 256.000000000000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo em: [frxDsExtr."DataX"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 619.102569690000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."Saldo"]')
          ParentFont = False
        end
      end
      object ColumnFooter1: TfrxColumnFooter
        Height = 20.000000000000000000
        Top = 376.000000000000000000
        Width = 680.315400000000000000
        object Memo27: TfrxMemoView
          Left = 362.830000000000000000
          Width = 256.000000000000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo TOTAL:')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 619.100000000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[FTOTFlxCxaCar]')
          ParentFont = False
        end
      end
    end
  end
  object frxFin_Relat_005_01_A07: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.635407800900000000
    ReportOptions.LastChange = 40429.473596203700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxFin_Relat_005_01_A06GetValue
    Left = 620
    Top = 166
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsExtr
        DataSetName = 'frxDsExtr'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        Height = 13.228346460000000000
        Top = 252.000000000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsExtr
        DataSetName = 'frxDsExtr'
        RowCount = 0
        object Memo3: TfrxMemoView
          Left = 41.574803150000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Docum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[frxDsExtr."Docum"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 136.062992130000000000
          Width = 158.740157480315000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Texto'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."Texto"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 102.047244094488200000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'NotaF'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[frxDsExtr."NotaF"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 294.803149610000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."VALOR"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 525.354391730000000000
          Width = 120.944881889763800000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'CartN'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."CartN"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'DataX'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataX"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 646.299630000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Ctrle'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."Ctrle"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 347.716535430000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."Saldo"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 400.629921259842500000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'DataE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataE"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 442.204724409448800000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'DataV'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataV"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 483.779527559055100000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'DataQ'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataQ"]')
          ParentFont = False
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        Height = 13.228346460000000000
        Top = 128.000000000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          Left = 41.574803150000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'S'#233'rie - Doc.')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 136.062992130000000000
          Width = 158.740157480000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[SUBST]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 102.047244094488200000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 294.803149610000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 525.354330708661400000
          Width = 120.944881889763800000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Carteira')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Dta fluxo')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 646.299630000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ID')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 347.716535433070900000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 400.629921259842500000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Emiss.')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 442.204724409448800000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Venceu')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 483.779527560000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Quita'#231#227'o')
          ParentFont = False
        end
      end
      object Band1: TfrxGroupHeader
        Top = 232.000000000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsExtr."DataX"'
      end
      object Band2: TfrxGroupFooter
        Height = 13.228346460000000000
        Top = 284.000000000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          Left = 363.574830000000000000
          Width = 256.000000000000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo em: [frxDsExtr."DataX"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 619.842519690000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."Saldo"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 22.677180000000000000
        Top = 408.000000000000000000
        Width = 680.315400000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo23: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 90.708720000000000000
        Top = 16.000000000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo38: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[NOMEREL]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_NOMEEMPRESA]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Top = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo:  [PERIODO]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 13.228346460000000000
        Top = 200.000000000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsExtr."NOMECAR"'
        object Memo26: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228346456692910000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Carteira: [frxDsExtr."NOMECAR"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Top = 316.000000000000000000
        Width = 680.315400000000000000
      end
      object ColumnFooter1: TfrxColumnFooter
        Height = 13.228346460000000000
        Top = 376.000000000000000000
        Width = 680.315400000000000000
        object Memo27: TfrxMemoView
          Left = 364.000000000000000000
          Width = 256.000000000000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo TOTAL:')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 620.267689690000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[FTOTFlxCxaCar]')
          ParentFont = False
        end
      end
    end
  end
  object frxFin_Relat_005_01_A08: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.635407800900000000
    ReportOptions.LastChange = 40429.473596203700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxFin_Relat_005_01_A06GetValue
    Left = 648
    Top = 166
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsExtr
        DataSetName = 'frxDsExtr'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        Height = 13.228346460000000000
        Top = 256.000000000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsExtr
        DataSetName = 'frxDsExtr'
        RowCount = 0
        object Memo3: TfrxMemoView
          Left = 47.244094490000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Docum'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[frxDsExtr."Docum"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 141.732273700000000000
          Width = 147.401574800000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Texto'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."Texto"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 107.716525670000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'NotaF'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[frxDsExtr."NotaF"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 289.133858267716500000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."VALOR"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 536.692981730000000000
          Width = 109.606299210000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'CartN'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."CartN"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'DataX'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataX"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 646.299630000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'Ctrle'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."Ctrle"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 342.047244094488200000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."Saldo"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 394.960629921259800000
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'DataE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataE"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 442.204724409448800000
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'DataV'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataV"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 489.448818897637800000
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          ShowHint = False
          DataField = 'DataQ'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataQ"]')
          ParentFont = False
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        Height = 13.228346460000000000
        Top = 128.000000000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          Left = 47.244094490000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'S'#233'rie - Doc.')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 141.732273700000000000
          Width = 147.401574800000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[SUBST]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 107.716525670000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 289.133858270000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 536.692913385826800000
          Width = 109.606299210000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Carteira')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Dta fluxo')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 646.299630000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ID')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 342.047244094488200000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 394.960629921259800000
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Emiss.')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 442.204724410000000000
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Venceu')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 489.448818897637800000
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Quita'#231#227'o')
          ParentFont = False
        end
      end
      object Band1: TfrxGroupHeader
        Top = 236.000000000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsExtr."DataX"'
      end
      object Band2: TfrxGroupFooter
        Height = 13.228346460000000000
        Top = 288.000000000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          Left = 363.574830000000000000
          Width = 256.000000000000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo em: [frxDsExtr."DataX"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 619.842519690000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."Saldo"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 22.677180000000000000
        Top = 412.000000000000000000
        Width = 680.315400000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo23: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 90.708720000000000000
        Top = 16.000000000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo38: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[NOMEREL]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_NOMEEMPRESA]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Top = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo:  [PERIODO]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 15.118110240000000000
        Top = 200.000000000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsExtr."NOMECAR"'
        object Memo26: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Carteira: [frxDsExtr."NOMECAR"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Top = 320.000000000000000000
        Width = 680.315400000000000000
      end
      object ColumnFooter1: TfrxColumnFooter
        Height = 13.228346460000000000
        Top = 380.000000000000000000
        Width = 680.315400000000000000
        object Memo27: TfrxMemoView
          Left = 364.000000000000000000
          Width = 256.000000000000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo TOTAL:')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 620.267689690000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[FTOTFlxCxaCar]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsExtr: TfrxDBDataset
    UserName = 'frxDsExtr'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DataE=DataE'
      'DataV=DataV'
      'DataQ=DataQ'
      'DataX=DataX'
      'Texto=Texto'
      'Docum=Docum'
      'NotaF=NotaF'
      'Saldo=Saldo'
      'Credi=Credi'
      'Debit=Debit'
      'CartC=CartC'
      'CartN=CartN'
      'Codig=Codig'
      'Ctrle=Ctrle'
      'CtSub=CtSub'
      'CART_ORIG=CART_ORIG'
      'ID_Pg=ID_Pg'
      'TipoI=TipoI'
      'VALOR=VALOR'
      'tCrt=tCrt'
      'NOMECAR=NOMECAR')
    DataSet = QrExtr
    BCDToCurrency = False
    Left = 676
    Top = 222
  end
  object QrLct: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lan.Controle, lan.Carteira, IF(lan.Controle=0, "", '
      'IF(lan.Carteira=0, "", car.Nome)) NOMECART '
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'WHERE car.ForneceI=:P0')
    Left = 508
    Top = 194
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrFlxoCar: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrMovantCalcFields
    SQL.Strings = (
      'SELECT Codigo CART '
      'FROM carteiras'
      'WHERE Fornecei=:P0'
      'AND Tipo <> 2')
    Left = 592
    Top = 194
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFlxoCarCART: TIntegerField
      FieldName = 'CART'
    end
  end
end
