unit DescDupli;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, Db, mySQLDbTables, ComCtrls,
  frxClass, frxDBSet, Variants, dmkGeral, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, UnDmkProcFunc;

type
  TFmDescDupli = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Label2: TLabel;
    EdDescoPor: TdmkEditCB;
    CBDescoPor: TdmkDBLookupComboBox;
    DsDescopor: TDataSource;
    QrDescopor: TmySQLQuery;
    QrDescoporCodigo: TIntegerField;
    QrDescoporNOMEENTIDADE: TWideStringField;
    GBEmiss: TGroupBox;
    CkEmiss: TCheckBox;
    TPEIni: TDateTimePicker;
    Label1: TLabel;
    Label3: TLabel;
    TPEFim: TDateTimePicker;
    CkVenct: TCheckBox;
    GBVenct: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    TPVIni: TDateTimePicker;
    TPVFim: TDateTimePicker;
    QrDescos: TmySQLQuery;
    QrDescosData: TDateField;
    QrDescosVencimento: TDateField;
    QrDescosCarteira: TIntegerField;
    QrDescosNOMECARTEIRA: TWideStringField;
    QrDescosControle: TIntegerField;
    QrDescosGenero: TIntegerField;
    QrDescosNOMECONTA: TWideStringField;
    QrDescosDescricao: TWideStringField;
    QrDescosNotaFiscal: TIntegerField;
    QrDescosCredito: TFloatField;
    QrDescosCompensado: TDateField;
    QrDescosCliente: TIntegerField;
    QrDescosDuplicata: TWideStringField;
    QrDescosDescoPor: TIntegerField;
    QrDescosDescoVal: TFloatField;
    QrDescosNOMECLIENTE: TWideStringField;
    QrDescosNOMEDESCOPOR: TWideStringField;
    RGAgrup1: TRadioGroup;
    Label6: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    frxDescos: TfrxReport;
    frxDsDescos: TfrxDBDataset;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CkEmissClick(Sender: TObject);
    procedure CkVenctClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxDescosGetValue(const VarName: String;
      var Value: Variant);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmDescDupli: TFmDescDupli;

implementation

{$R *.DFM}

uses UnMyObjects, UnMLAGeral, UnInternalConsts;

procedure TFmDescDupli.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmDescDupli.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmDescDupli.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDescDupli.FormCreate(Sender: TObject);
begin
  QrDescopor.Open;
  QrClientes.Open;
  TPEIni.Date := Date;
  TPEFim.Date := Date;
  TPVIni.Date := Date;
  TPVFim.Date := Date;
end;

procedure TFmDescDupli.CkEmissClick(Sender: TObject);
begin
  GBEmiss.Visible := CkEmiss.Checked;
end;

procedure TFmDescDupli.CkVenctClick(Sender: TObject);
begin
  GBVenct.Visible := CkVenct.Checked;
end;

procedure TFmDescDupli.BtOKClick(Sender: TObject);
var
  Descopor, Cliente: Integer;
begin
  DescoPor := Geral.IMV(EdDescopor.Text);
  Cliente := Geral.IMV(EdCliente.Text);
  QrDescos.Close;
  QrDescos.SQL.Clear;
  QrDescos.SQL.Add('SELECT la.Data, la.Vencimento, la.Carteira,');
  QrDescos.SQL.Add('ca.Nome NOMECARTEIRA, la.Controle,');
  QrDescos.SQL.Add('la.Genero, co.Nome NOMECONTA, la.Descricao,');
  QrDescos.SQL.Add('la.NotaFiscal, la.Credito, la.Compensado,');
  QrDescos.SQL.Add('la.Cliente, la.Duplicata, la.DescoPor, la.DescoVal,');
  QrDescos.SQL.Add('CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial');
  QrDescos.SQL.Add('ELSE cl.Nome END NOMECLIENTE,');
  QrDescos.SQL.Add('CASE WHEN dp.Tipo=0 THEN dp.RazaoSocial');
  QrDescos.SQL.Add('ELSE dp.Nome END NOMEDESCOPOR');
  QrDescos.SQL.Add('FROM ' + VAR_LCT + ' la');
  QrDescos.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
  QrDescos.SQL.Add('LEFT JOIN contas    co ON co.Codigo=la.Genero');
  QrDescos.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
  QrDescos.SQL.Add('LEFT JOIN entidades dp ON dp.Codigo=la.DescoPor');
  QrDescos.SQL.Add('WHERE (la.DescoPor<>0 OR la.DescoVal<>0)');
  QrDescos.SQL.Add('');
  if Descopor <> 0 then
    QrDescos.SQL.Add('AND la.Descopor='+IntToStr(DescoPor));
  if Cliente <> 0 then
    QrDescos.SQL.Add('AND la.Cliente='+IntToStr(Cliente));
  QrDescos.SQL.Add(dmkPF.SQL_Periodo('AND la.Data ', TPEIni.Date,
    TPEFim.Date, CkEmiss.Checked, CkEmiss.Checked));
  QrDescos.SQL.Add(dmkPF.SQL_Periodo('AND la.Vencimento ',
    TPVIni.Date, TPVFim.Date, CkVenct.Checked, CkVenct.Checked));
  //
  case RGAgrup1.ItemIndex of
    //0: ;
    1: QrDescos.SQL.Add('ORDER BY NOMEDESCOPOR, la.Data, la.Vencimento');
    2: QrDescos.SQL.Add('ORDER BY NOMECLIENTE, la.Data, la.Vencimento');
    3: QrDescos.SQL.Add('ORDER BY NOMECARTEIRA, la.Data, la.Vencimento');
    4: QrDescos.SQL.Add('ORDER BY NOMECONTA, la.Data, la.Vencimento');
    5: QrDescos.SQL.Add('ORDER BY la.Data, la.Vencimento');
    else QrDescos.SQL.Add('ORDER BY la.Data, la.Vencimento');
  end;
  QrDescos.Open;
  MyObjects.frxMostra(frxDescos, 'Desconto de duplicatas');
end;

procedure TFmDescDupli.frxDescosGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'CLIENTE_' then
  begin
    if CBCliente.KeyValue = NULL then Value := 'TODOS'
    else Value := CBCliente.Text;
  end else if VarName = 'DESCO_POR' then
  begin
    if CBDescoPor.KeyValue = NULL then Value := 'TODOS'
    else Value := CBDescoPor.Text;
  end else if VarName = 'GRUPO1' then
  begin
    case RGAgrup1.ItemIndex of
      0: Value := '';
      1: Value := 'Descontado por: ' +FormatFloat(' 000000 - ', QrDescosDescoPor.Value)+ QrDescosNOMEDESCOPOR.Value;
      2: Value := 'Cliente: ' +FormatFloat(' 000000 - ', QrDescosCliente.Value)+ QrDescosNOMECLIENTE.Value;
      3: Value := 'Carteira: ' +FormatFloat(' 000000 - ', QrDescosCarteira.Value)+ QrDescosNOMECARTEIRA.Value;
      4: Value := 'Conta: ' +FormatFloat(' 000000 - ', QrDescosGenero.Value)+ QrDescosNOMECONTA.Value;
      5: Value := 'Emitido em ' + FormatDateTime(VAR_FORMATDATE2, QrDescosData.Value);
      6: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrDescosVencimento.Value);
    end;
  end else if VarName = 'PeriodoE' then
  begin
    Value := '';
    if not CkEmiss.Checked then Value := 'N�o definido' else
      Value :=
      FormatDateTime(VAR_FORMATDATE3, TPEIni.Date)+ CO_ATE+
      FormatDateTime(VAR_FORMATDATE3, TPEFim.Date);
  end else if VarName = 'PeriodoV' then
  begin
    Value := '';
    if not CkVenct.Checked then Value := 'N�o definido' else
      Value :=
      FormatDateTime(VAR_FORMATDATE3, TPVIni.Date)+ CO_ATE+
      FormatDateTime(VAR_FORMATDATE3, TPVFim.Date);
  end;

  // User function

  if VarName = 'VFR_GRUPO1' then Value := RGAgrup1.ItemIndex

end;

end.

