unit PrincipalImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, Buttons, ExtCtrls, (*DBTables,*)   frxClass, frxDBSet,
  UnInternalConsts, ComCtrls, UnMLAGeral, mySQLDbTables, DBCtrls, dmkGeral,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, Variants, dmkPermissoes;

type
  TFmPrincipalImp = class(TForm)
    QrInicial: TmySQLQuery;
    Panel1: TPanel;
    Panel2: TPanel;
    QrInicialInicial: TFloatField;
    BtImprime: TBitBtn;
    BtSair: TBitBtn;
    QrAnterior: TmySQLQuery;
    QrExtrato: TmySQLQuery;
    QrAnteriorCredito: TFloatField;
    QrAnteriorDebito: TFloatField;
    QrAnteriorSaldo: TFloatField;
    QrExtratoData: TDateField;
    QrExtratoTipo: TSmallintField;
    QrExtratoCarteira: TIntegerField;
    QrExtratoSub: TSmallintField;
    QrExtratoAutorizacao: TIntegerField;
    QrExtratoGenero: TIntegerField;
    QrExtratoDescricao: TWideStringField;
    QrExtratoNotaFiscal: TIntegerField;
    QrExtratoDebito: TFloatField;
    QrExtratoCredito: TFloatField;
    QrExtratoCompensado: TDateField;
    QrExtratoDocumento: TFloatField;
    QrExtratoSit: TIntegerField;
    QrExtratoVencimento: TDateField;
    QrExtratoLk: TIntegerField;
    QrExtratoFatID: TIntegerField;
    QrExtratoFatParcela: TIntegerField;
    QrExtratoID_Sub: TSmallintField;
    QrExtratoFatura: TWideStringField;
    QrExtratoBanco: TIntegerField;
    QrExtratoLocal: TIntegerField;
    QrExtratoCartao: TIntegerField;
    QrExtratoLinha: TIntegerField;
    QrExtratoOperCount: TIntegerField;
    QrExtratoLancto: TIntegerField;
    QrExtratoPago: TFloatField;
    QrExtratoMez: TIntegerField;
    QrExtratoFornecedor: TIntegerField;
    QrExtratoCliente: TIntegerField;
    QrExtratoMoraDia: TFloatField;
    QrExtratoMulta: TFloatField;
    QrExtratoProtesto: TDateField;
    QrExtratoDataCad: TDateField;
    QrExtratoDataAlt: TDateField;
    QrExtratoUserCad: TSmallintField;
    QrExtratoUserAlt: TSmallintField;
    QrExtratoNOMECONTA: TWideStringField;
    QrExtratoNOMECARTEIRA: TWideStringField;
    PainelTitulo: TPanel;
    Image1: TImage;
    QrExtratoControle: TIntegerField;
    QrExtratoID_Pgto: TIntegerField;
    Panel3: TPanel;
    Label1: TLabel;
    TPIni: TDateTimePicker;
    TPFim: TDateTimePicker;
    Label2: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    QrMovimento: TmySQLQuery;
    QrMovimentoNOMECONTA: TWideStringField;
    QrMovimentoNOMECLIENTE: TWideStringField;
    QrMovimentoNOMECLIINT: TWideStringField;
    QrMovimentoNOMEFORNECEDOR: TWideStringField;
    QrMovimentoData: TDateField;
    QrMovimentoTipo: TSmallintField;
    QrMovimentoCarteira: TIntegerField;
    QrMovimentoControle: TIntegerField;
    QrMovimentoSub: TSmallintField;
    QrMovimentoAutorizacao: TIntegerField;
    QrMovimentoGenero: TIntegerField;
    QrMovimentoDescricao: TWideStringField;
    QrMovimentoNotaFiscal: TIntegerField;
    QrMovimentoDebito: TFloatField;
    QrMovimentoCredito: TFloatField;
    QrMovimentoCompensado: TDateField;
    QrMovimentoDocumento: TFloatField;
    QrMovimentoSit: TIntegerField;
    QrMovimentoVencimento: TDateField;
    QrMovimentoLk: TIntegerField;
    QrMovimentoFatID: TIntegerField;
    QrMovimentoFatParcela: TIntegerField;
    QrMovimentoID_Pgto: TIntegerField;
    QrMovimentoID_Sub: TSmallintField;
    QrMovimentoFatura: TWideStringField;
    QrMovimentoBanco: TIntegerField;
    QrMovimentoLocal: TIntegerField;
    QrMovimentoCartao: TIntegerField;
    QrMovimentoLinha: TIntegerField;
    QrMovimentoOperCount: TIntegerField;
    QrMovimentoLancto: TIntegerField;
    QrMovimentoPago: TFloatField;
    QrMovimentoMez: TIntegerField;
    QrMovimentoFornecedor: TIntegerField;
    QrMovimentoCliente: TIntegerField;
    QrMovimentoMoraDia: TFloatField;
    QrMovimentoMulta: TFloatField;
    QrMovimentoProtesto: TDateField;
    QrMovimentoDataDoc: TDateField;
    QrMovimentoCtrlIni: TIntegerField;
    QrMovimentoNivel: TIntegerField;
    QrMovimentoVendedor: TIntegerField;
    QrMovimentoAccount: TIntegerField;
    QrMovimentoDataCad: TDateField;
    QrMovimentoDataAlt: TDateField;
    QrMovimentoUserCad: TIntegerField;
    QrMovimentoUserAlt: TIntegerField;
    QrMovimentoFatID_Sub: TIntegerField;
    QrMovimentoICMS_P: TFloatField;
    QrMovimentoICMS_V: TFloatField;
    QrMovimentoDuplicata: TWideStringField;
    QrMovimentoCliInt: TIntegerField;
    QrMovimentoTERCEIRO: TIntegerField;
    QrMovimentoNOMETERCEIRO: TWideStringField;
    QrMovimentoCLContato: TWideStringField;
    QrMovimentoCIContato: TWideStringField;
    QrMovimentoFOContato: TWideStringField;
    QrMovimentoCLTel1: TWideStringField;
    QrMovimentoCITel1: TWideStringField;
    QrMovimentoFOTel1: TWideStringField;
    QrMovimentoTETel1: TWideStringField;
    QrMovimentoTEContato: TWideStringField;
    QrMovimentoTXTCITel1: TWideStringField;
    QrMovimentoTXTTETel1: TWideStringField;
    QrMovimentoNOMETECONTATO: TWideStringField;
    QrMovimentoVALOR: TFloatField;
    LaCliente: TLabel;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel9: TPanel;
    RG1: TRadioGroup;
    RG2: TRadioGroup;
    Panel8: TPanel;
    Panel10: TPanel;
    Ck1: TCheckBox;
    Panel11: TPanel;
    Ck2: TCheckBox;
    Panel12: TPanel;
    EdCliFor: TdmkEditCB;
    CBCliFor: TdmkDBLookupComboBox;
    Label4: TLabel;
    QrClienteI: TmySQLQuery;
    DsClienteI: TDataSource;
    QrCliente1: TmySQLQuery;
    DsCliente1: TDataSource;
    Label5: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    QrContas: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    DsContas: TDataSource;
    QrCliente1Codigo: TIntegerField;
    QrCliente1NOMEENTIDADE: TWideStringField;
    QrClienteICodigo: TIntegerField;
    QrClienteINOMEENTIDADE: TWideStringField;
    QrExtratoNOMERELACIONADO: TWideStringField;
    QrExtratoNOMECLIENTE: TWideStringField;
    QrExtratoNOMEFORNECEDOR: TWideStringField;
    Panel4: TPanel;
    CkNF: TCheckBox;
    QrExtratoQtde: TFloatField;
    QrMovimentoFatNum: TFloatField;
    frxExtrato: TfrxReport;
    frxDsExtrato: TfrxDBDataset;
    frxExtrato2: TfrxReport;
    frxDsMovimento: TfrxDBDataset;
    QrMovimentoKGT: TIntegerField;
    QrCarteirasForneceI: TIntegerField;
    frxDsCarteiras: TfrxDBDataset;
    QrCarteirasNO_ENT: TWideStringField;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label6: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    frxMovimento1: TfrxReport;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSairClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrMovimentoCalcFields(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure RG1Click(Sender: TObject);
    procedure QrExtratoCalcFields(DataSet: TDataSet);
    procedure frxExtratoGetValue(const VarName: String;
      var Value: Variant);
    procedure EdEmpresaChange(Sender: TObject);
  private
    { Private declarations }
    FDataI, FDataF, FDataH, FDataA: String;
    FSaldo: Double;
    FCarteira: Integer;
    procedure ImprimeExtrato;
    procedure ImprimeFaturamentoPorTerceiro;
    function DefineCondicao(RG: TRadioGroup): String;
    function DefineMeuMemo1(RG: TRadioGroup): String;
    function DefineMeuMemo2(RG: TRadioGroup): String;
    function DefineMeuMemo3(RG: TRadioGroup): String;
    function DefineTotal(RG: TRadioGroup): String;
    procedure ReopenCarteiras();
    procedure DefineDataSets(Frx: TfrxReport);
  public
    { Public declarations }
  end;

var
  FmPrincipalImp: TFmPrincipalImp;

implementation

uses UnMyObjects, Module, ModuleGeral;

{$R *.DFM}

procedure TFmPrincipalImp.FormCreate(Sender: TObject);
begin
  CBEmpresa.ListSource := DModG.DsEmpresas;
  DefineDataSets(frxExtrato);
  DefineDataSets(frxExtrato2);
  DefineDataSets(frxMovimento1);
  //
  TPIni.Date := Date-30;
  TPFim.Date := Date;
  QrCarteiras.Open;
  QrClienteI.Open;
  QrCliente1.Open;
  QrContas.Open;
  EdCarteira.Text     := IntToStr(Dmod.QrCarteirasCodigo.Value);
  CBCarteira.KeyValue := Dmod.QrCarteirasCodigo.Value;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmPrincipalImp.BtSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPrincipalImp.BtImprimeClick(Sender: TObject);
begin
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Informe a empresa') then Exit;
  //
  FCarteira := Geral.IMV(EdCarteira.Text);
  MLAGeral.ExcluiSQLsDermatek;
  //
  FSaldo := 0;
  FDataI := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  FDataF := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  FDataH := FormatDateTime(VAR_FORMATDATE, Date);
  FDataA := FormatDateTime(VAR_FORMATDATE, TPIni.Date-1);
  case PageControl1.ActivePageIndex of
    -1: Application.MessageBox('Escolha o relat�rio!', 'Erro', MB_OK+MB_ICONERROR);
    0: ImprimeExtrato;
    1: ImprimeFaturamentoPorTerceiro;
    else Application.MessageBox('Relat�rio n�o implementado!', 'Aviso',
         MB_OK+MB_ICONEXCLAMATION);
  end;
  //
  QrInicial.Close;
  QrAnterior.Close;
end;

procedure TFmPrincipalImp.ImprimeExtrato;
begin
  //
  if MyObjects.FIC(FCarteira = 0, EdCarteira, 'Informe a carteira') then Exit;
  //
  QrInicial.Close;
  QrInicial.Params[0].AsInteger := FCarteira;
  //MLAGeral.LeSQL2(QrInicial, 'Inicial', IntToStr(Carteira), True);
  QrInicial.Open;
  //
  QrAnterior.Close;
  QrAnterior.Params[0].AsString := FDataI;
  QrAnterior.Params[1].AsString := FDataI;
  QrAnterior.Params[2].AsInteger := FCarteira;
  //MLAGeral.LeSQL2(QrAnterior, 'Anterior', FDataI+', '+FDataI+', '+
  //IntToStr(Carteira), True);
  QrAnterior.Open;
  FSaldo := QrInicialInicial.Value + QrAnteriorSaldo.Value;
  //
  QrExtrato.Close;
  QrExtrato.Params[0].AsString  := FDataI;
  QrExtrato.Params[1].AsString  := FDataF;
  QrExtrato.Params[2].AsString  := FDataF;
  QrExtrato.Params[3].AsInteger := FCarteira;
  //MLAGeral.LeSQL2(QrExtrato, 'Extrato', FDataI+', '+FDataF+', '+
  //FDataF+', '+IntToStr(Carteira), True);
  QrExtrato.Open;
  //
  if not CkNF.Checked then
  begin
    MyObjects.frxMostra(frxExtrato, 'Extrato de Carteira �nica');
    QrExtrato.Close;
  end else begin
    //frExtrato2.PrepareReport;
    //frExtrato2.ShowPreparedReport;
    MyObjects.frxMostra(frxExtrato2, 'Extrato de Carteira �nica');
    QrExtrato.Close;
  end;
end;

procedure TFmPrincipalImp.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmPrincipalImp.ImprimeFaturamentoPorTerceiro;
var
  Ordem: String;
  MyOrd: Integer;
begin
  MyOrd := RG1.ItemIndex*10+RG2.ItemIndex;
  case MyOrd of
    00: Ordem := '';
    01: Ordem := 'ORDER BY la.Cliente, la.Fornecedor, la.Data';
    02: Ordem := 'ORDER BY la.Genero, la.Data';
    10: Ordem := 'ORDER BY la.Cliente, la.Fornecedor, la.Data';
    11: Ordem := 'ORDER BY la.Cliente, la.Fornecedor, la.Data';
    12: Ordem := 'ORDER BY la.Cliente, la.Fornecedor, la.Genero, la.Data';
    20: Ordem := 'ORDER BY la.Genero, la.Data';
    21: Ordem := 'ORDER BY la.Genero, la.Cliente, la.Fornecedor, la.Data';
    22: Ordem := 'ORDER BY la.Genero, la.Data';
    else Ordem := '';
  end;
  QrMovimento.Close;
  QrMovimento.SQL.Clear;
  QrMovimento.SQL.Add('SELECT co.Nome NOMECONTA,');
  QrMovimento.SQL.Add('CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial ELSE cl.Nome END NOMECLIENTE,');
  QrMovimento.SQL.Add('CASE WHEN ci.Tipo=0 THEN ci.RazaoSocial ELSE ci.Nome END NOMECLIINT,');
  QrMovimento.SQL.Add('CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ELSE fo.Nome END NOMEFORNECEDOR,');
  QrMovimento.SQL.Add('CASE WHEN cl.Tipo=0 THEN cl.EContato ELSE cl.PContato END CLContato,');
  QrMovimento.SQL.Add('CASE WHEN ci.Tipo=0 THEN ci.EContato ELSE ci.PContato END CIContato,');
  QrMovimento.SQL.Add('CASE WHEN fo.Tipo=0 THEN fo.EContato ELSE fo.PContato END FOContato,');
  QrMovimento.SQL.Add('CASE WHEN cl.Tipo=0 THEN cl.ETe1 ELSE cl.PTe1 END CLTel1,');
  QrMovimento.SQL.Add('CASE WHEN ci.Tipo=0 THEN ci.ETe1 ELSE ci.PTe1 END CITel1,');
  QrMovimento.SQL.Add('CASE WHEN fo.Tipo=0 THEN fo.ETe1 ELSE fo.PTe1 END FOTel1,');
  QrMovimento.SQL.Add('la.*');
  QrMovimento.SQL.Add('FROM ' + VAR_LCT + ' la');
  QrMovimento.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
  QrMovimento.SQL.Add('LEFT JOIN contas    co ON co.Codigo=la.Genero');
  QrMovimento.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
  QrMovimento.SQL.Add('LEFT JOIN entidades ci ON ci.Codigo=la.CliInt');
  QrMovimento.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor');
  QrMovimento.SQL.Add('WHERE la.Data BETWEEN :P0 AND :P1');
  QrMovimento.SQL.Add('AND co.Codigo=la.Genero');
  QrMovimento.SQL.Add('AND ca.ForneceI=' + FormatFloat('0', DModG.QrEmpresasCodigo.Value));
  //
  if EdCarteira.ValueVariant <> 0 then
    QrMovimento.SQL.Add('AND la.Carteira='  + FormatFloat('0', +EdCarteira.ValueVariant));
  //
  //
  if EdCliFor.ValueVariant <> 0 then
    QrMovimento.SQL.Add('AND (la.Cliente='  + FormatFloat('0',
    EdCliFor.ValueVariant) + ' OR la.Fornecedor=' + EdCliFor.Text+')');
  //
  if EdConta.ValueVariant <> 0 then
  QrMovimento.SQL.Add('AND la.Genero='  + FormatFloat('0', EdConta.ValueVariant));
  //
  QrMovimento.SQL.Add(ORDEM);
  QrMovimento.SQL.Add('');
  QrMovimento.SQL.Add('');
  QrMovimento.Params[00].AsString  := FDataI;
  QrMovimento.Params[01].AsString  := FDataF;
  QrMovimento.Open;
  MyObjects.frxMostra(frxMovimento1, 'Movimento de Carteira �nica');
end;

procedure TFmPrincipalImp.QrMovimentoCalcFields(DataSet: TDataSet);
begin
  QrMovimentoKGT.Value := 0;
  if QrMovimentoCliente.Value = 0 then
  begin
    QrMovimentoTERCEIRO.Value := QrMovimentoFornecedor.Value;
    QrMovimentoNOMETERCEIRO.Value := QrMovimentoNOMEFORNECEDOR.Value;
    QrMovimentoNOMETECONTATO.Value := QrMovimentoFOContato.Value;
    QrMovimentoTXTTETel1.Value :=
      MLAGeral.FormataTelefone_TT_Curto(QrMovimentoFOTel1.Value);
  end else begin
    QrMovimentoTERCEIRO.Value := QrMovimentoCliente.Value;
    QrMovimentoNOMETERCEIRO.Value := QrMovimentoNOMECLIENTE.Value;
    QrMovimentoNOMETECONTATO.Value := QrMovimentoCLContato.Value;
    QrMovimentoTXTTETel1.Value :=
      MLAGeral.FormataTelefone_TT_Curto(QrMovimentoCLTel1.Value);
  end;
  QrMovimentoTXTCITel1.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrMovimentoCITel1.Value);
  QrMovimentoVALOR.Value := QrMovimentoCredito.Value - QrMovimentoDebito.Value;
end;

procedure TFmPrincipalImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPrincipalImp.ReopenCarteiras();
var
  Carteira: Integer;
begin
  Carteira := EdCarteira.ValueVariant;
  //
  QrCarteiras.Close;
  QrCarteiras.Params[0].AsInteger := DModG.QrEmpresasCodigo.Value;
  QrCarteiras.Open;
  if QrCarteiras.Locate('Codigo', Carteira, []) then
  begin
    EdCarteira.ValueVariant := Carteira;
    CBCarteira.KeyValue     := Carteira;
  end else begin
    EdCarteira.ValueVariant := 0;
    CBCarteira.KeyValue     := Null;
  end;
end;

procedure TFmPrincipalImp.RG1Click(Sender: TObject);
begin
  RG2.Enabled := True;
end;

function TFmPrincipalImp.DefineCondicao(RG: TRadioGroup): String;
begin
  case RG.ItemIndex of
    0: Result := 'frxDsMovimento."KGT"';
    1: Result := 'frxDsMovimento."NOMETERCEIRO"';
    2: Result := 'frxDsMovimento."NOMECONTA"';
  end;
end;

procedure TFmPrincipalImp.DefineDataSets(Frx: TfrxReport);
begin
  Frx.DataSets.Clear;
  Frx.DataSets.Add(DModG.frxDsDono);
  Frx.DataSets.Add(frxDsCarteiras);
  Frx.DataSets.Add(frxDsExtrato);
  Frx.DataSets.Add(DModG.frxDsMaster);
  Frx.DataSets.Add(frxDsMovimento);
end;

function TFmPrincipalImp.DefineTotal(RG: TRadioGroup): String;
begin
  case RG.ItemIndex of
    0: Result := '';
    1: Result := 'Total do terceiro [frxDsMovimento."NOMETERCEIRO"]: ';
    2: Result := 'Total da conta [frxDsMovimento."NOMECONTA"]: ';
  end;
end;

function TFmPrincipalImp.DefineMeuMemo1(RG: TRadioGroup): String;
begin
  case RG.ItemIndex of
    0: Result := '';
    1: Result := '[frxDsMovimento."NOMETERCEIRO"]';
    2: Result := '[frxDsMovimento."NOMECONTA"]';
  end;
end;

function TFmPrincipalImp.DefineMeuMemo2(RG: TRadioGroup): String;
begin
  case RG.ItemIndex of
    0: Result := '';
    1: Result := '[frxDsMovimento."TXTTETel1"]';
    2: Result := '';
  end;
end;

function TFmPrincipalImp.DefineMeuMemo3(RG: TRadioGroup): String;
begin
  case RG.ItemIndex of
    0: Result := '';
    1: Result := '[frxDsMovimento."TEContato"]';
    2: Result := '';
  end;
end;

procedure TFmPrincipalImp.EdEmpresaChange(Sender: TObject);
begin
  ReopenCarteiras();
end;

procedure TFmPrincipalImp.QrExtratoCalcFields(DataSet: TDataSet);
begin
  if QrExtratoCliente.Value <> 0 then
     QrExtratoNOMERELACIONADO.Value := QrExtratoNOMECLIENTE.Value else
  if QrExtratoFornecedor.Value <> 0 then
     QrExtratoNOMERELACIONADO.Value := QrExtratoNOMEFORNECEDOR.Value else
  QrExtratoNOMERELACIONADO.Value := '';
end;

procedure TFmPrincipalImp.frxExtratoGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'INICIAL' then
  begin
    FSaldo := QrInicialInicial.Value + QrAnteriorSaldo.Value;
    Value  := FSaldo;
  end
  else if VarName = 'NOMEREL' then Value := 'EXTRATO DE CARTEIRA �NICA'
  else if VarName = 'VAR_NOMECARTEIRA' then Value := CBCarteira.Text
  else if VarName = 'VAR_NOMECLIINT' then Value := CBEmpresa.Text
  else if VarName = 'VAR_NOMECLIFOR' then Value := CBCliFor.Text
  else if VarName = 'VAR_NOMECONTA' then  Value := CBConta.Text
  else if VarName = 'SALDO' then
  begin
    Value := QrExtratoCredito.Value - QrExtratoDebito.Value;
    FSaldo := FSaldo + Value;
  end else if VarName = 'SALDODIA' then Value := FSaldo
  else if VarName = 'PERIODO' then Value :=
    FormatDateTime(VAR_FORMATDATE3, TPIni.Date)+ CO_ATE+
    FormatDateTime(VAR_FORMATDATE3, TPFim.Date)
  else if VarName = 'CAPTION1' then
  begin
    case RG1.ItemIndex of
      0: Value := '';
      1: Value := 'Nome';
      2: Value := 'Conta';
    end;
  end else if VarName = 'CAPTION2' then
  begin
    case RG1.ItemIndex of
      0: Value := '';
      1: Value := 'Telefone';
      2: Value := '';
    end;
  end else if VarName = 'CAPTION3' then
  begin
    case RG1.ItemIndex of
      0: Value := '';
      1: Value := 'Contato';
      2: Value := '';
    end;
  end

  // user function

  else if VarName = 'VAR_SHOW1' then
  begin
    if Ck1.Visible and Ck1.Enabled and (RG1.ItemIndex > 0) then
      Value := True else Value := False;
  end else if VarName = 'VAR_SHOW2' then begin
    if Ck2.Visible and Ck2.Enabled and (RG2.ItemIndex > 0) then
      Value := True else Value := False;
  end else if VarName = 'VAR_CONDITION1' then begin
    Value := DefineCondicao(RG1);
  end else if VarName = 'VAR_CONDITION2' then begin
    Value := DefineCondicao(RG2)
  end else if VarName = 'VAR_DEFMEMO1' then begin
    Value := DefineTotal(RG1);
  end else if VarName = 'VAR_DEFMEMO2' then begin
    Value := DefineTotal(RG2)
  {
  end else if VarName = 'VAR_DEFTIT1' then begin
    Value := DefineTitulo(RG1);
  end else if VarName = 'VAR_DEFTIT2' then begin
    Value := DefineTitulo(RG2)
  }
  end
  else if VarName = 'VAR_MEMO11' then
    Value := DefineMeuMemo1(RG1)
  else if VarName = 'VAR_MEMO12' then
    Value := DefineMeuMemo2(RG1)
  else if VarName = 'VAR_MEMO13' then
    Value := DefineMeuMemo3(RG1)
  else if VarName = 'VAR_MEMO21' then
    Value := DefineMeuMemo1(RG2)
  else if VarName = 'VAR_MEMO22' then
    Value := DefineMeuMemo2(RG2)
  else if VarName = 'VAR_MEMO23' then
    Value := DefineMeuMemo3(RG2)

end;

end.

