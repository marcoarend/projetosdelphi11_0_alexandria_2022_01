unit Resultados1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, UnGOTOy, UnMLAGeral, DBCtrls, Db, (*DBTables,*) Buttons,
  UnInternalConsts, ExtCtrls, mySQLDbTables, frxClass, frxDBSet, Variants,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, Grids, DBGrids, dmkGeral,
  UnDmkProcFunc;

type
  TFmResultados1 = class(TForm)
    QrGrupos: TMySQLQuery;
    DsGrupos: TDataSource;
    QrConjuntos: TMySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    IntegerField2: TIntegerField;
    DsConjuntos: TDataSource;
    QrSubgrupos: TMySQLQuery;
    DsSubgrupos: TDataSource;
    QrContas: TMySQLQuery;
    DsContas: TDataSource;
    QrGruposCodigo: TIntegerField;
    QrGruposNome: TWideStringField;
    QrGruposConjunto: TIntegerField;
    QrGruposLk: TIntegerField;
    QrSubgruposCodigo: TIntegerField;
    QrSubgruposNome: TWideStringField;
    QrSubgruposGrupo: TIntegerField;
    QrSubgruposLk: TIntegerField;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrContasNome2: TWideStringField;
    QrContasID: TWideStringField;
    QrContasSubgrupo: TIntegerField;
    QrContasEmpresa: TIntegerField;
    QrContasCredito: TWideStringField;
    QrContasDebito: TWideStringField;
    QrContasMensal: TWideStringField;
    QrContasExclusivo: TWideStringField;
    QrContasMensdia: TSmallintField;
    QrContasMensdeb: TFloatField;
    QrContasMensmind: TFloatField;
    QrContasMenscred: TFloatField;
    QrContasMensminc: TFloatField;
    QrContasLk: TIntegerField;
    QrContasTerceiro: TIntegerField;
    QrContasExcel: TWideStringField;
    QrResultLA: TMySQLQuery;
    QrResultLANOMECONTA: TWideStringField;
    QrResultLANOMESUBGRUPO: TWideStringField;
    QrResultLANOMEGRUPO: TWideStringField;
    QrResultLANOMECONJUNTO: TWideStringField;
    QrResultLANOMECONTA3: TWideStringField;
    QrResultLANOMECONTA2: TWideStringField;
    QrResultCO: TMySQLQuery;
    QrResultCODEBITO: TFloatField;
    QrResultCOCREDITO: TFloatField;
    QrResultCONOMECONTA: TWideStringField;
    QrResultCONOMECONTA2: TWideStringField;
    QrResultCONOMESUBGRUPO: TWideStringField;
    QrResultCONOMEGRUPO: TWideStringField;
    QrResultCONOMECONJUNTO: TWideStringField;
    QrResultCONOMECONTA3: TWideStringField;
    QrSInicial: TMySQLQuery;
    QrSInicialInicial: TFloatField;
    QrMovimento: TMySQLQuery;
    QrMovimentoCredito: TFloatField;
    QrMovimentoDebito: TFloatField;
    QrResultLANOMECARTEIRA: TWideStringField;
    Panel1: TPanel;
    BtImprime: TBitBtn;
    Panel2: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    QrResultLAData: TDateField;
    QrResultLATipo: TSmallintField;
    QrResultLACarteira: TIntegerField;
    QrResultLAControle: TIntegerField;
    QrResultLASub: TSmallintField;
    QrResultLAAutorizacao: TIntegerField;
    QrResultLAGenero: TIntegerField;
    QrResultLADescricao: TWideStringField;
    QrResultLANotaFiscal: TIntegerField;
    QrResultLADebito: TFloatField;
    QrResultLACredito: TFloatField;
    QrResultLACompensado: TDateField;
    QrResultLADocumento: TFloatField;
    QrResultLASit: TIntegerField;
    QrResultLAVencimento: TDateField;
    QrResultLALk: TIntegerField;
    QrResultLAFatID: TIntegerField;
    QrResultLAFatParcela: TIntegerField;
    QrResultLAID_Pgto: TIntegerField;
    QrResultLAID_Sub: TSmallintField;
    QrResultLAFatura: TWideStringField;
    QrResultLABanco: TIntegerField;
    QrResultLALocal: TIntegerField;
    QrResultLACartao: TIntegerField;
    QrResultLALinha: TIntegerField;
    QrResultLAOperCount: TIntegerField;
    QrResultLALancto: TIntegerField;
    QrResultLAPago: TFloatField;
    QrResultLAMez: TIntegerField;
    QrResultLAFornecedor: TIntegerField;
    QrResultLACliente: TIntegerField;
    QrResultLAMoraDia: TFloatField;
    QrResultLAMulta: TFloatField;
    QrResultLAProtesto: TDateField;
    QrResultLADataCad: TDateField;
    QrResultLADataAlt: TDateField;
    QrResultLAUserCad: TSmallintField;
    QrResultLAUserAlt: TSmallintField;
    QrResultLADataDoc: TDateField;
    QrResultLACtrlIni: TIntegerField;
    QrResultLANivel: TIntegerField;
    QrResultLAVendedor: TIntegerField;
    QrResultLAAccount: TIntegerField;
    QrResultLASubgrupo: TIntegerField;
    QrResultLAGrupo: TIntegerField;
    QrResultLAConjunto: TIntegerField;
    QrResultCOData: TDateField;
    QrResultCODocumento: TFloatField;
    QrResultCONotaFiscal: TIntegerField;
    QrResultCOSubgrupo: TIntegerField;
    QrResultCOGrupo: TIntegerField;
    QrResultCOConjunto: TIntegerField;
    frxResultCO: TfrxReport;
    frxDsResultCO: TfrxDBDataset;
    frxResultLA: TfrxReport;
    frxDsResultLA: TfrxDBDataset;
    QrResultLAFatNum: TFloatField;
    Panel3: TPanel;
    BtSair: TBitBtn;
    frxInd01: TfrxReport;
    frxDsInd01: TfrxDBDataset;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrCliIntCodigo: TIntegerField;
    QrCliIntNOMECLI: TWideStringField;
    Panel4: TPanel;
    RGTipoRel: TRadioGroup;
    Panel5: TPanel;
    CkDataIni: TCheckBox;
    TPDataIni: TDateTimePicker;
    CkVctoIni: TCheckBox;
    TPVctoIni: TDateTimePicker;
    CkDtDocIni: TCheckBox;
    TPDtDocIni: TDateTimePicker;
    CkDataFim: TCheckBox;
    TPDataFim: TDateTimePicker;
    CkVctoFim: TCheckBox;
    TPVctoFim: TDateTimePicker;
    CkDtDocFim: TCheckBox;
    TPDtDocFim: TDateTimePicker;
    Label3: TLabel;
    EdConjunto: TdmkEditCB;
    CBConjunto: TdmkDBLookupComboBox;
    CBGrupo: TdmkDBLookupComboBox;
    EdGrupo: TdmkEditCB;
    Label4: TLabel;
    Label5: TLabel;
    EdSubgrupo: TdmkEditCB;
    CBSubgrupo: TdmkDBLookupComboBox;
    CBConta: TdmkDBLookupComboBox;
    EdConta: TdmkEditCB;
    Label6: TLabel;
    RGExclusivos: TRadioGroup;
    CkNome2: TCheckBox;
    CkSaldo: TCheckBox;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    RGPeriodoBalInd: TRadioGroup;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    QrResultCOcjOrdemLista: TIntegerField;
    QrResultCOgrOrdemLista: TIntegerField;
    QrResultCOsgOrdemLista: TIntegerField;
    QrResultCOcoOrdemLista: TIntegerField;
    QrInd01: TmySQLQuery;
    procedure BtSairClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrResultLACalcFields(DataSet: TDataSet);
    procedure QrResultCOCalcFields(DataSet: TDataSet);
    procedure QrConjuntosAfterScroll(DataSet: TDataSet);
    procedure QrGruposAfterScroll(DataSet: TDataSet);
    procedure QrSubgruposAfterScroll(DataSet: TDataSet);
    procedure CBConjuntoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBGrupoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBSubgrupoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBContaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormResize(Sender: TObject);
    procedure frxResultCOGetValue(const VarName: String;
      var Value: Variant);
    procedure RGTipoRelClick(Sender: TObject);
  private
    { Private declarations }
    FInd01: String;
  public
    { Public declarations }
    FTabLctA: String;
  end;

var
  FmResultados1: TFmResultados1;
  RES_Anterior, RES_Final, RES_Credito, RES_Debito, RES_Periodo: Double;

implementation

uses UnMyObjects, Module, ModuleGeral, UCreate;

{$R *.DFM}

procedure TFmResultados1.BtSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFmResultados1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmResultados1.FormCreate(Sender: TObject);
begin
  QrInd01.Close;
  QrInd01.DataBase := DModG.MyPID_DB;
  //
  QrConjuntos.Open;
  QrCliInt.Open;
  TPDataIni.Date  := Geral.Data1(IncMonth(Date, -1));
  TPDataFim.Date  := Geral.Data1(Date)-1;
  TPVctoIni.Date  := Geral.Data1(IncMonth(Date, -1));
  TPVctoFim.Date  := Geral.Data1(Date)-1;
  TPDtDocIni.Date := Geral.Data1(IncMonth(Date, -1));
  TPDtDocFim.Date := Geral.Data1(Date)-1;
  //
  EdCliInt.ValueVariant := VAR_LIB_EMPRESAS;
  CBCliInt.KeyValue     := VAR_LIB_EMPRESAS;
end;

{
procedure TFmResultados1.BtImprimeClick(Sender: TObject);
  procedure AbrirMovimento(Data, Vcto: String);
  begin
    QrMovimento.Close;
    QrMovimento.SQL.Clear;
    QrMovimento.SQL.Add('SELECT SUM(la.Credito) Credito, SUM(la.Debito) Debito');
    QrMovimento.SQL.Add('FROM lct la, Contas co');
    QrMovimento.SQL.Add('WHERE la.Genero > 0');
    QrMovimento.SQL.Add('AND la.ID_Pgto = 0');

    if CkDataFim.Checked then
      QrMovimento.SQL.Add('AND la.Data <= "' + Data + '"');
    if CkVctoFim.Checked then
      QrMovimento.SQL.Add('AND la.Vencimento <= "' + Vcto + '"');

    QrMovimento.SQL.Add('AND co.Codigo=la.Genero');
    case RGExclusivos.ItemIndex of
      0: QrMovimento.SQL.Add('AND co.Exclusivo=''F''');
      2: QrMovimento.SQL.Add('AND co.Exclusivo=''V''');
    end;
    //QrMovimento.Params[0].AsString := Fim;
    QrMovimento.Open;
  end;
var
  Conjunto, Grupo, Subgrupo, Conta: Integer;
  DataAnt, DataIni, DataFim,
  VctoAnt, VctoIni, VctoFim,
  DDocAnt, DDocIni, DDocFim,
  Campos, Linha: String;
  DataIDate, DataFDate: TDateTime;
  DataITrue, DataFTrue: Boolean;
  Credito, Debito: Double;
begin
  //Parei Aqui !! Saldos da C/C

  //Acertar saldos finais rel novo

  DataAnt := FormatDateTime(VAR_FORMATDATE, TPDataIni.Date-1);
  DataIni := FormatDateTime(VAR_FORMATDATE, TPDataIni.Date);
  DataFim := FormatDateTime(VAR_FORMATDATE, TPDataFim.Date);

  VctoAnt := FormatDateTime(VAR_FORMATDATE, TPVctoIni.Date-1);
  VctoIni := FormatDateTime(VAR_FORMATDATE, TPVctoIni.Date);
  VctoFim := FormatDateTime(VAR_FORMATDATE, TPVctoFim.Date);

  DDocAnt := FormatDateTime(VAR_FORMATDATE, TPDtDocIni.Date-1);
  DDocIni := FormatDateTime(VAR_FORMATDATE, TPDtDocIni.Date);
  DDocFim := FormatDateTime(VAR_FORMATDATE, TPDtDocFim.Date);
  ////////////
  QrSInicial.Close;
  QrSInicial.Open;

  AbrirMovimento(DataFim, VctoFim);

  RES_Credito := QrMovimentoCredito.Value;
  RES_Debito := QrMovimentoDebito.Value;
  if RGExclusivos.ItemIndex <> 2 then
    RES_Final := QrSInicialInicial.Value + RES_Credito - RES_Debito
  else RES_Final := RES_Credito - RES_Debito;
  RES_Periodo := RES_Credito - RES_Debito;
  ///////////
  AbrirMovimento(DataAnt, VctoAnt);
  //
  if RGExclusivos.ItemIndex <> 2 then
  RES_Anterior := QrSInicialInicial.Value else RES_Anterior := 0;
  RES_Anterior := RES_Anterior + QrMovimentoCredito.Value - QrMovimentoDebito.Value;
  RES_Credito := RES_Credito - QrMovimentoCredito.Value;
  RES_Debito := RES_Debito - QrMovimentoDebito.Value;
  ///////////
  if CBConjunto.KeyValue = NULL then Conjunto := -1000
  else Conjunto := CBConjunto.KeyValue;
  //
  if CBGrupo.KeyValue = NULL then Grupo := -1000
  else Grupo := CBGrupo.KeyValue;
  //
  if CBSubgrupo.KeyValue = NULL then Subgrupo := -1000
  else Subgrupo := CBSubgrupo.KeyValue;
  //
  if CBConta.KeyValue = NULL then Conta := -1000
  else Conta := CBConta.KeyValue;
  //
  // Resultado por CONTAS
  // Balancete industrial modelo 01
  if RGTipoRel.ItemIndex in ([0,2]) then
  begin
    QrResultCO.Close;
    QrResultCO.SQL.Clear;
    QrResultCO.SQL.Add('SELECT SUM(la.Debito) DEBITO, SUM(la.Credito) CREDITO,');
    QrResultCO.SQL.Add('la.Data, la.Documento, la.NotaFiscal,');
    QrResultCO.SQL.Add('co.Subgrupo, sg.Grupo, gr.Conjunto,');
    QrResultCO.SQL.Add('co.Nome NOMECONTA, co.Nome2 NOMECONTA2,');
    QrResultCO.SQL.Add('sg.Nome NOMESUBGRUPO, gr.Nome NOMEGRUPO,');
    QrResultCO.SQL.Add('cj.Nome NOMECONJUNTO, cj.OrdemLista cjOrdemLista,');
    QrResultCO.SQL.Add('gr.OrdemLista grOrdemLista, sg.OrdemLista sgOrdemLista,');
    QrResultCO.SQL.Add('co.OrdemLista coOrdemLista');
    QrResultCO.SQL.Add('FROM lct la, Contas co, Subgrupos sg,');
    QrResultCO.SQL.Add('Grupos gr, Conjuntos cj');
    QrResultCO.SQL.Add('WHERE la.Genero>0 AND la.ID_Pgto = 0');

    QrResultCO.SQL.Add(dmkPF.SQL_Periodo('AND Data ',
      TPDataIni.Date, TPDataFim.Date, CkDataIni.Checked, CkDataFim.Checked));
    QrResultCO.SQL.Add(dmkPF.SQL_Periodo('AND Vencimento ',
      TPVctoIni.Date, TPVctoFim.Date, CkVctoIni.Checked, CkVctoFim.Checked));
    QrResultCO.SQL.Add(dmkPF.SQL_Periodo('AND DataDoc ',
      TPDtDocIni.Date, TPDtDocFim.Date, CkDtDocIni.Checked, CkDtDocFim.Checked));

    QrResultCO.SQL.Add('AND co.Codigo=la.Genero');
    QrResultCO.SQL.Add('AND sg.Codigo=co.Subgrupo');
    QrResultCO.SQL.Add('AND gr.Codigo=sg.Grupo');
    QrResultCO.SQL.Add('AND cj.Codigo=gr.Conjunto');
    ////////////
    if Conjunto <> -1000 then
      QrResultCO.SQL.Add('AND cj.Codigo='+IntToStr(Conjunto));
    ////////////
    if Grupo <> -1000 then
      QrResultCO.SQL.Add('AND gr.Codigo='+IntToStr(Grupo));
    ////////////
    if Subgrupo <> -1000 then
      QrResultCO.SQL.Add('AND sg.Codigo='+IntToStr(Subgrupo));
    ////////////
    if Conta <> -1000 then
      QrResultCO.SQL.Add('AND co.Codigo='+IntToStr(Conta));
    ////////////
    case RGExclusivos.ItemIndex of
      0: QrResultCO.SQL.Add('AND co.Exclusivo=''F''');
      2: QrResultCO.SQL.Add('AND co.Exclusivo=''V''');
    end;
    ////////////
    // Balancete industrial modelo 01
    if RGTipoRel.ItemIndex = 2 then
      QrResultCO.SQL.Add('AND NOT (co.NotPrntBal & 1)');

    ///
    QrResultCO.SQL.Add('GROUP BY la.Genero');
    QrResultCO.SQL.Add('ORDER BY cj.OrdemLista, cj.Nome, gr.OrdemLista, ');
    QrResultCO.SQL.Add('gr.Nome, sg.OrdemLista, sg.Nome, co.OrdemLista, ');
    QrResultCO.SQL.Add('co.Nome, co.Nome2');
    QrResultCO.Open;
    //
    if GOTOy.Registros(QrResultCO) = 0 then
      ShowMessage('N�o foi encontrado nenhum registro!')
    else
      if RGTipoRel.ItemIndex = 0 then
        MyObjects.frxMostra(frxResultCO, 'Resultado por contas')
  end;
  // Resultado por LAN�AMENTOS
  if RGTipoRel.ItemIndex = 1 then
  begin
    QrResultLA.Close;
    QrResultLA.SQL.Clear;
    QrResultLA.SQL.Add('SELECT la.*, co.Subgrupo, sg.Grupo, ca.Nome NOMECARTEIRA, ');
    QrResultLA.SQL.Add('gr.Conjunto, co.Nome NOMECONTA, co.Nome2 NOMECONTA2, ');
    QrResultLA.SQL.Add('sg.Nome NOMESUBGRUPO, gr.Nome NOMEGRUPO,');
    QrResultLA.SQL.Add('cj.Nome NOMECONJUNTO');
    QrResultLA.SQL.Add('FROM lct la, Contas co, Subgrupos sg,');
    QrResultLA.SQL.Add('Grupos gr, Conjuntos cj, Carteiras ca');
    QrResultLA.SQL.Add('WHERE la.Genero>0 AND la.ID_Pgto = 0');

    QrResultLA.SQL.Add(dmkPF.SQL_Periodo('AND Data ',
      TPDataIni.Date, TPDataFim.Date, CkDataIni.Checked, CkDataFim.Checked));
    QrResultLA.SQL.Add(dmkPF.SQL_Periodo('AND Vencimento ',
      TPVctoIni.Date, TPVctoFim.Date, CkVctoIni.Checked, CkVctoFim.Checked));
    QrResultLA.SQL.Add(dmkPF.SQL_Periodo('AND DataDoc ',
      TPDtDocIni.Date, TPDtDocFim.Date, CkDtDocIni.Checked, CkDtDocFim.Checked));

    QrResultLA.SQL.Add('AND ca.Codigo=la.Carteira');
    QrResultLA.SQL.Add('AND co.Codigo=la.Genero');
    QrResultLA.SQL.Add('AND sg.Codigo=co.Subgrupo');
    QrResultLA.SQL.Add('AND gr.Codigo=sg.Grupo');
    QrResultLA.SQL.Add('AND cj.Codigo=gr.Conjunto');
    ////////////
    if Conjunto <> -1000 then
      QrResultLA.SQL.Add('AND cj.Codigo='+IntToStr(Conjunto));
    ////////////
    if Grupo <> -1000 then
      QrResultLA.SQL.Add('AND gr.Codigo='+IntToStr(Grupo));
    ////////////
    if Subgrupo <> -1000 then
      QrResultLA.SQL.Add('AND sg.Codigo='+IntToStr(Subgrupo));
    ////////////
    if Conta <> -1000 then
      QrResultLA.SQL.Add('AND co.Codigo='+IntToStr(Conta));
    ////////////
    case RGExclusivos.ItemIndex of
      0: QrResultLA.SQL.Add('AND co.Exclusivo=''F''');
      2: QrResultLA.SQL.Add('AND co.Exclusivo=''V''');
    end;
    ////////////
    QrResultLA.SQL.Add('');
    QrResultLA.SQL.Add('ORDER BY cj.Nome, gr.Nome, sg.Nome, co.Nome, la.Data');
    QrResultLA.Open;
    if GOTOy.Registros(QrResultLA) = 0 then
      Application.MessageBox('N�o foi encontrado nenhum registro!', 'Erro',
        MB_OK+MB_ICONERROR)
    else
      MyObjects.frxMostra(frxResultLA, 'Resultado por lan�amentos');
  end;
  if RGTipoRel.ItemIndex = 2 then
  begin
    /// Parei aqui !!!
    QrInd01_.Close;
    QrInd01_.SQL.Clear;
    QrInd01_.SQL.Add('DROP TABLE Ind01;');
    QrInd01_.SQL.Add('CREATE TABLE Ind01 (');
    QrInd01_.SQL.Add('  DEBITO       float        ,');
    QrInd01_.SQL.Add('  CREDITO      float        ,');
    QrInd01_.SQL.Add('  NOMECONTA    varchar(100) ,');
    QrInd01_.SQL.Add('  NOMECONTA2   varchar(100) ,');
    QrInd01_.SQL.Add('  NOMESUBGRUPO varchar(100) ,');
    QrInd01_.SQL.Add('  NOMEGRUPO    varchar(100) ,');
    QrInd01_.SQL.Add('  NOMECONJUNTO varchar(100) ,');
    QrInd01_.SQL.Add('  NOMECONTA3   varchar(100) ,');
    QrInd01_.SQL.Add('  Data         date         ,');
    QrInd01_.SQL.Add('  Documento    float        ,');
    QrInd01_.SQL.Add('  NotaFiscal   integer      ,');
    QrInd01_.SQL.Add('  Subgrupo     integer      ,');
    QrInd01_.SQL.Add('  Grupo        integer      ,');
    QrInd01_.SQL.Add('  Conjunto     integer      ,');
    QrInd01_.SQL.Add('  cjOrdemLista integer      ,');
    QrInd01_.SQL.Add('  grOrdemLista integer      ,');
    QrInd01_.SQL.Add('  sgOrdemLista integer      ,');
    QrInd01_.SQL.Add('  coOrdemLista integer       ');
    QrInd01_.SQL.Add(');');
    //
    Campos := 'INSERT INTO Ind01 (DEBITO, CREDITO, NOMECONTA, NOMECONTA2, ' +
    'NOMESUBGRUPO, NOMEGRUPO, NOMECONJUNTO, NOMECONTA3, Data, Documento, ' +
    'NotaFiscal, Subgrupo, Grupo, Conjunto, cjOrdemLista, grOrdemLista, ' +
    'sgOrdemLista, coOrdemLista) VALUES (';
    //
    QrResultCO.First;
    while not QrResultCO.Eof do
    begin
      if QrResultCODEBITO.Value > QrResultCOCREDITO.Value then
      begin
        Credito := 0;
        Debito  := QrResultCODEBITO.Value - QrResultCOCREDITO.Value;
      end else begin
        Credito := QrResultCOCREDITO.Value - QrResultCODEBITO.Value;
        Debito  := 0;
      end;
      Linha := Campos +
      dmkPF.FFP(QrResultCODEBITO.Value, 2) +
      ', ' + dmkPF.FFP(QrResultCOCREDITO.Value, 2) +
      ', "' + QrResultCONOMECONTA.Value + '"' +
      ', "' + QrResultCONOMECONTA2.Value + '"' +
      ', "' + QrResultCONOMESUBGRUPO.Value + '"' +
      ', "' + QrResultCONOMEGRUPO.Value + '"' +
      ', "' + QrResultCONOMECONJUNTO.Value + '"' +
      ', "' + QrResultCONOMECONTA3.Value + '"' +
      ', "' + Geral.FDT(QrResultCOData.Value, 1) + '"' +
      ',  ' + FormatFloat('0', QrResultCODocumento.Value) +
      ',  ' + FormatFloat('0', QrResultCONotaFiscal.Value) +

      ',  ' + FormatFloat('0', QrResultCOSubgrupo.Value) +
      ',  ' + FormatFloat('0', QrResultCOGrupo.Value) +
      ',  ' + FormatFloat('0', QrResultCOConjunto.Value) +

      ',  ' + FormatFloat('0', QrResultCOcjOrdemLista.Value) +
      ',  ' + FormatFloat('0', QrResultCOgrOrdemLista.Value) +
      ',  ' + FormatFloat('0', QrResultCOsgOrdemLista.Value) +
      ',  ' + FormatFloat('0', QrResultCOcoOrdemLista.Value) +
      ');';
      QrInd01_.SQL.Add(Linha);
      //
      QrResultCO.Next;
    end;
    //
    case RGPeriodoBalInd.ItemIndex of
      0:
      begin
        DataIDate := TPDataIni.Date;
        DataFDate := TPDataFim.Date;
        DataITrue := CkDataIni.Checked;
        DataFTrue := CkDataFim.Checked;
      end;
      1:
      begin
        DataIDate := TPVctoIni.Date;
        DataFDate := TPVctoFim.Date;
        DataITrue := CkVctoIni.Checked;
        DataFTrue := CkVctoFim.Checked;
      end;
      2:
      begin
        DataIDate := TPDtDocIni.Date;
        DataFDate := TPDtDocFim.Date;
        DataITrue := CkDtDocIni.Checked;
        DataFTrue := CkDtDocFim.Checked;
      end;
      else
      begin
        DataIDate := 0;
        DataFDate := 0;
        DataITrue := False;
        DataFTrue := False;
      end;
    end;
    Dmod.InsereRegistrosExtraBalanceteMensal_01(QrInd01_, DataIDate, DataFDate,
    DataITrue, DataFTrue, Geral.IMV(EdCliInt.Text), Campos, CkNome2.Checked);
    //
    QrInd01_.SQL.Add('SELECT * FROM Ind01 ');
    QrInd01_.SQL.Add('ORDER BY cjOrdemLista, NOMECONJUNTO, grOrdemLista, ');
    QrInd01_.SQL.Add('NOMEGRUPO, sgOrdemLista, NOMESUBGRUPO, coOrdemLista, ');
    QrInd01_.SQL.Add('NOMECONTA, NOMECONTA2;');
    QrInd01_.Open;
    //
    MyObjects.frxMostra(frxInd01, 'Balancete Mensal');
  end;
  QrSInicial.Close;
  QrMovimento.Close;
end;
}

procedure TFmResultados1.BtImprimeClick(Sender: TObject);
  procedure AbrirMovimento(Data, Vcto: String);
  begin
    QrMovimento.Close;
    QrMovimento.SQL.Clear;
    QrMovimento.SQL.Add('SELECT SUM(la.Credito) Credito, SUM(la.Debito) Debito');
    QrMovimento.SQL.Add('FROM ' + FTabLctA + ' la, Contas co');
    QrMovimento.SQL.Add('WHERE la.Genero > 0');
    QrMovimento.SQL.Add('AND la.ID_Pgto = 0');

    if CkDataFim.Checked then
      QrMovimento.SQL.Add('AND la.Data <= "' + Data + '"');
    if CkVctoFim.Checked then
      QrMovimento.SQL.Add('AND la.Vencimento <= "' + Vcto + '"');

    QrMovimento.SQL.Add('AND co.Codigo=la.Genero');
    case RGExclusivos.ItemIndex of
      0: QrMovimento.SQL.Add('AND co.Exclusivo=''F''');
      2: QrMovimento.SQL.Add('AND co.Exclusivo=''V''');
    end;
    //QrMovimento.Params[0].AsString := Fim;
    QrMovimento.Open;
  end;
var
  Conjunto, Grupo, Subgrupo, Conta: Integer;
  DataAnt, DataIni, DataFim,
  VctoAnt, VctoIni, VctoFim,
  DDocAnt, DDocIni, DDocFim,
  Campos, Linha: String;
  DataIDate, DataFDate: TDateTime;
  DataITrue, DataFTrue: Boolean;
  //Credito, Debito: Double;
begin
  //Parei Aqui !! Saldos da C/C

  //Acertar saldos finais rel novo

  DataAnt := FormatDateTime(VAR_FORMATDATE, TPDataIni.Date-1);
  DataIni := FormatDateTime(VAR_FORMATDATE, TPDataIni.Date);
  DataFim := FormatDateTime(VAR_FORMATDATE, TPDataFim.Date);

  VctoAnt := FormatDateTime(VAR_FORMATDATE, TPVctoIni.Date-1);
  VctoIni := FormatDateTime(VAR_FORMATDATE, TPVctoIni.Date);
  VctoFim := FormatDateTime(VAR_FORMATDATE, TPVctoFim.Date);

  DDocAnt := FormatDateTime(VAR_FORMATDATE, TPDtDocIni.Date-1);
  DDocIni := FormatDateTime(VAR_FORMATDATE, TPDtDocIni.Date);
  DDocFim := FormatDateTime(VAR_FORMATDATE, TPDtDocFim.Date);
  ////////////
  QrSInicial.Close;
  QrSInicial.Open;

  AbrirMovimento(DataFim, VctoFim);

  RES_Credito := QrMovimentoCredito.Value;
  RES_Debito := QrMovimentoDebito.Value;
  if RGExclusivos.ItemIndex <> 2 then
    RES_Final := QrSInicialInicial.Value + RES_Credito - RES_Debito
  else RES_Final := RES_Credito - RES_Debito;
  RES_Periodo := RES_Credito - RES_Debito;
  ///////////
  AbrirMovimento(DataAnt, VctoAnt);
  //
  if RGExclusivos.ItemIndex <> 2 then
  RES_Anterior := QrSInicialInicial.Value else RES_Anterior := 0;
  RES_Anterior := RES_Anterior + QrMovimentoCredito.Value - QrMovimentoDebito.Value;
  RES_Credito := RES_Credito - QrMovimentoCredito.Value;
  RES_Debito := RES_Debito - QrMovimentoDebito.Value;
  ///////////
  if CBConjunto.KeyValue = NULL then Conjunto := -1000
  else Conjunto := CBConjunto.KeyValue;
  //
  if CBGrupo.KeyValue = NULL then Grupo := -1000
  else Grupo := CBGrupo.KeyValue;
  //
  if CBSubgrupo.KeyValue = NULL then Subgrupo := -1000
  else Subgrupo := CBSubgrupo.KeyValue;
  //
  if CBConta.KeyValue = NULL then Conta := -1000
  else Conta := CBConta.KeyValue;
  //
  // Resultado por CONTAS
  // Balancete industrial modelo 01
  if RGTipoRel.ItemIndex in ([0,2]) then
  begin
    QrResultCO.Close;
    QrResultCO.SQL.Clear;
    QrResultCO.SQL.Add('SELECT SUM(la.Debito) DEBITO, SUM(la.Credito) CREDITO,');
    QrResultCO.SQL.Add('la.Data, la.Documento, la.NotaFiscal,');
    QrResultCO.SQL.Add('co.Subgrupo, sg.Grupo, gr.Conjunto,');
    QrResultCO.SQL.Add('co.Nome NOMECONTA, co.Nome2 NOMECONTA2,');
    QrResultCO.SQL.Add('sg.Nome NOMESUBGRUPO, gr.Nome NOMEGRUPO,');
    QrResultCO.SQL.Add('cj.Nome NOMECONJUNTO, cj.OrdemLista cjOrdemLista,');
    QrResultCO.SQL.Add('gr.OrdemLista grOrdemLista, sg.OrdemLista sgOrdemLista,');
    QrResultCO.SQL.Add('co.OrdemLista coOrdemLista');
    QrResultCO.SQL.Add('FROM ' + FTabLctA + ' la, Contas co, Subgrupos sg,');
    QrResultCO.SQL.Add('Grupos gr, Conjuntos cj');
    QrResultCO.SQL.Add('WHERE la.Genero>0 AND la.ID_Pgto = 0');

    QrResultCO.SQL.Add(dmkPF.SQL_Periodo('AND Data ',
      TPDataIni.Date, TPDataFim.Date, CkDataIni.Checked, CkDataFim.Checked));
    QrResultCO.SQL.Add(dmkPF.SQL_Periodo('AND Vencimento ',
      TPVctoIni.Date, TPVctoFim.Date, CkVctoIni.Checked, CkVctoFim.Checked));
    QrResultCO.SQL.Add(dmkPF.SQL_Periodo('AND DataDoc ',
      TPDtDocIni.Date, TPDtDocFim.Date, CkDtDocIni.Checked, CkDtDocFim.Checked));

    QrResultCO.SQL.Add('AND co.Codigo=la.Genero');
    QrResultCO.SQL.Add('AND sg.Codigo=co.Subgrupo');
    QrResultCO.SQL.Add('AND gr.Codigo=sg.Grupo');
    QrResultCO.SQL.Add('AND cj.Codigo=gr.Conjunto');
    ////////////
    if Conjunto <> -1000 then
      QrResultCO.SQL.Add('AND cj.Codigo='+IntToStr(Conjunto));
    ////////////
    if Grupo <> -1000 then
      QrResultCO.SQL.Add('AND gr.Codigo='+IntToStr(Grupo));
    ////////////
    if Subgrupo <> -1000 then
      QrResultCO.SQL.Add('AND sg.Codigo='+IntToStr(Subgrupo));
    ////////////
    if Conta <> -1000 then
      QrResultCO.SQL.Add('AND co.Codigo='+IntToStr(Conta));
    ////////////
    case RGExclusivos.ItemIndex of
      0: QrResultCO.SQL.Add('AND co.Exclusivo=''F''');
      2: QrResultCO.SQL.Add('AND co.Exclusivo=''V''');
    end;
    ////////////
    // Balancete industrial modelo 01
    if RGTipoRel.ItemIndex = 2 then
      QrResultCO.SQL.Add('AND NOT (co.NotPrntBal & 1)');

    ///
    QrResultCO.SQL.Add('GROUP BY la.Genero');
    QrResultCO.SQL.Add('ORDER BY cj.OrdemLista, cj.Nome, gr.OrdemLista, ');
    QrResultCO.SQL.Add('gr.Nome, sg.OrdemLista, sg.Nome, co.OrdemLista, ');
    QrResultCO.SQL.Add('co.Nome, co.Nome2');
    QrResultCO.Open;
    //
    if GOTOy.Registros(QrResultCO) = 0 then
      ShowMessage('N�o foi encontrado nenhum registro!')
    else
      if RGTipoRel.ItemIndex = 0 then
        MyObjects.frxMostra(frxResultCO, 'Resultado por contas')
  end;
  // Resultado por LAN�AMENTOS
  if RGTipoRel.ItemIndex = 1 then
  begin
    QrResultLA.Close;
    QrResultLA.SQL.Clear;
    QrResultLA.SQL.Add('SELECT la.*, co.Subgrupo, sg.Grupo, ca.Nome NOMECARTEIRA, ');
    QrResultLA.SQL.Add('gr.Conjunto, co.Nome NOMECONTA, co.Nome2 NOMECONTA2, ');
    QrResultLA.SQL.Add('sg.Nome NOMESUBGRUPO, gr.Nome NOMEGRUPO,');
    QrResultLA.SQL.Add('cj.Nome NOMECONJUNTO');
    QrResultLA.SQL.Add('FROM ' + FTabLctA + ' la, Contas co, Subgrupos sg,');
    QrResultLA.SQL.Add('Grupos gr, Conjuntos cj, Carteiras ca');
    QrResultLA.SQL.Add('WHERE la.Genero>0 AND la.ID_Pgto = 0');

    QrResultLA.SQL.Add(dmkPF.SQL_Periodo('AND Data ',
      TPDataIni.Date, TPDataFim.Date, CkDataIni.Checked, CkDataFim.Checked));
    QrResultLA.SQL.Add(dmkPF.SQL_Periodo('AND Vencimento ',
      TPVctoIni.Date, TPVctoFim.Date, CkVctoIni.Checked, CkVctoFim.Checked));
    QrResultLA.SQL.Add(dmkPF.SQL_Periodo('AND DataDoc ',
      TPDtDocIni.Date, TPDtDocFim.Date, CkDtDocIni.Checked, CkDtDocFim.Checked));

    QrResultLA.SQL.Add('AND ca.Codigo=la.Carteira');
    QrResultLA.SQL.Add('AND co.Codigo=la.Genero');
    QrResultLA.SQL.Add('AND sg.Codigo=co.Subgrupo');
    QrResultLA.SQL.Add('AND gr.Codigo=sg.Grupo');
    QrResultLA.SQL.Add('AND cj.Codigo=gr.Conjunto');
    ////////////
    if Conjunto <> -1000 then
      QrResultLA.SQL.Add('AND cj.Codigo='+IntToStr(Conjunto));
    ////////////
    if Grupo <> -1000 then
      QrResultLA.SQL.Add('AND gr.Codigo='+IntToStr(Grupo));
    ////////////
    if Subgrupo <> -1000 then
      QrResultLA.SQL.Add('AND sg.Codigo='+IntToStr(Subgrupo));
    ////////////
    if Conta <> -1000 then
      QrResultLA.SQL.Add('AND co.Codigo='+IntToStr(Conta));
    ////////////
    case RGExclusivos.ItemIndex of
      0: QrResultLA.SQL.Add('AND co.Exclusivo=''F''');
      2: QrResultLA.SQL.Add('AND co.Exclusivo=''V''');
    end;
    ////////////
    QrResultLA.SQL.Add('');
    QrResultLA.SQL.Add('ORDER BY cj.Nome, gr.Nome, sg.Nome, co.Nome, la.Data');
    QrResultLA.Open;
    if GOTOy.Registros(QrResultLA) = 0 then
      Application.MessageBox('N�o foi encontrado nenhum registro!', 'Erro',
        MB_OK+MB_ICONERROR)
    else
      MyObjects.frxMostra(frxResultLA, 'Resultado por lan�amentos');
  end;
  if RGTipoRel.ItemIndex = 2 then
  begin
    FInd01 := UCriar.RecriaTempTable('Ind01', DmodG.QrUpdPID1, False);
    QrInd01.SQL.Clear;
    /// Parei aqui !!!
    //
    Campos := 'INSERT INTO Ind01 (DEBITO, CREDITO, NOMECONTA, NOMECONTA2, ' +
    'NOMESUBGRUPO, NOMEGRUPO, NOMECONJUNTO, NOMECONTA3, Data, Documento, ' +
    'NotaFiscal, Subgrupo, Grupo, Conjunto, cjOrdemLista, grOrdemLista, ' +
    'sgOrdemLista, coOrdemLista) VALUES (';
    //
    QrResultCO.First;
    while not QrResultCO.Eof do
    begin
      {
      if QrResultCODEBITO.Value > QrResultCOCREDITO.Value then
      begin
        Credito := 0;
        Debito  := QrResultCODEBITO.Value - QrResultCOCREDITO.Value;
      end else begin
        Credito := QrResultCOCREDITO.Value - QrResultCODEBITO.Value;
        Debito  := 0;
      end;
      }
      Linha := Campos +
      dmkPF.FFP(QrResultCODEBITO.Value, 2) +
      ', ' + dmkPF.FFP(QrResultCOCREDITO.Value, 2) +
      ', "' + QrResultCONOMECONTA.Value + '"' +
      ', "' + QrResultCONOMECONTA2.Value + '"' +
      ', "' + QrResultCONOMESUBGRUPO.Value + '"' +
      ', "' + QrResultCONOMEGRUPO.Value + '"' +
      ', "' + QrResultCONOMECONJUNTO.Value + '"' +
      ', "' + QrResultCONOMECONTA3.Value + '"' +
      ', "' + Geral.FDT(QrResultCOData.Value, 1) + '"' +
      ',  ' + FormatFloat('0', QrResultCODocumento.Value) +
      ',  ' + FormatFloat('0', QrResultCONotaFiscal.Value) +

      ',  ' + FormatFloat('0', QrResultCOSubgrupo.Value) +
      ',  ' + FormatFloat('0', QrResultCOGrupo.Value) +
      ',  ' + FormatFloat('0', QrResultCOConjunto.Value) +

      ',  ' + FormatFloat('0', QrResultCOcjOrdemLista.Value) +
      ',  ' + FormatFloat('0', QrResultCOgrOrdemLista.Value) +
      ',  ' + FormatFloat('0', QrResultCOsgOrdemLista.Value) +
      ',  ' + FormatFloat('0', QrResultCOcoOrdemLista.Value) +
      ');';
      QrInd01.SQL.Add(Linha);
      //
      QrResultCO.Next;
    end;
    //
    case RGPeriodoBalInd.ItemIndex of
      0:
      begin
        DataIDate := TPDataIni.Date;
        DataFDate := TPDataFim.Date;
        DataITrue := CkDataIni.Checked;
        DataFTrue := CkDataFim.Checked;
      end;
      1:
      begin
        DataIDate := TPVctoIni.Date;
        DataFDate := TPVctoFim.Date;
        DataITrue := CkVctoIni.Checked;
        DataFTrue := CkVctoFim.Checked;
      end;
      2:
      begin
        DataIDate := TPDtDocIni.Date;
        DataFDate := TPDtDocFim.Date;
        DataITrue := CkDtDocIni.Checked;
        DataFTrue := CkDtDocFim.Checked;
      end;
      else
      begin
        DataIDate := 0;
        DataFDate := 0;
        DataITrue := False;
        DataFTrue := False;
      end;
    end;
    Dmod.InsereRegistrosExtraBalanceteMensal_01(QrInd01, DataIDate, DataFDate,
    DataITrue, DataFTrue, Geral.IMV(EdCliInt.Text), Campos, CkNome2.Checked);
    //
    QrInd01.ExecSQL;
    QrInd01.SQL.Clear;
    QrInd01.SQL.Add('SELECT * FROM Ind01 ');
    QrInd01.SQL.Add('ORDER BY cjOrdemLista, NOMECONJUNTO, grOrdemLista, ');
    QrInd01.SQL.Add('NOMEGRUPO, sgOrdemLista, NOMESUBGRUPO, coOrdemLista, ');
    QrInd01.SQL.Add('NOMECONTA, NOMECONTA2;');
    QrInd01.Open;
    //
    MyObjects.frxMostra(frxInd01, 'Balancete Mensal');
  end;
  QrSInicial.Close;
  QrMovimento.Close;
end;

procedure TFmResultados1.QrResultLACalcFields(DataSet: TDataSet);
begin
  if CkNome2.Checked then QrResultLANOMECONTA3.Value := QrResultLANOMECONTA2.Value
  else QrResultLANOMECONTA3.Value := QrResultLANOMECONTA.Value;
end;

procedure TFmResultados1.QrResultCOCalcFields(DataSet: TDataSet);
begin
  if CkNome2.Checked then QrResultCONOMECONTA3.Value := QrResultCONOMECONTA2.Value
  else QrResultCONOMECONTA3.Value := QrResultCONOMECONTA.Value;
end;

procedure TFmResultados1.QrConjuntosAfterScroll(DataSet: TDataSet);
begin
  QrGrupos.Close;
  QrGrupos.SQL.Clear;
  QrGrupos.SQL.Add('SELECT * FROM Grupos');
  if CBConjunto.KeyValue <> NULL then
    QrGrupos.SQL.Add('WHERE Conjunto=:P0');
  QrGrupos.SQL.Add('ORDER BY Nome');
  if CBConjunto.KeyValue <> NULL then
    QrGrupos.Params[0].AsInteger := CBConjunto.KeyValue;
  QrGrupos.Open;
  CBGrupo.KeyValue := NULL;
end;

procedure TFmResultados1.QrGruposAfterScroll(DataSet: TDataSet);
begin
  QrSubgrupos.Close;
  QrSubgrupos.SQL.Clear;
  QrSubgrupos.SQL.Add('SELECT * FROM Subgrupos');
  if CBGrupo.KeyValue <> NULL then
    QrSubgrupos.SQL.Add('WHERE Grupo=:P0');
  QrSubgrupos.SQL.Add('ORDER BY Nome');
  if CBGrupo.KeyValue <> NULL then
    QrSubgrupos.Params[0].AsInteger := CBGrupo.KeyValue;
  QrSubgrupos.Open;
  CBSubgrupo.KeyValue := NULL;
end;

procedure TFmResultados1.QrSubgruposAfterScroll(DataSet: TDataSet);
begin
  QrContas.Close;
  QrContas.SQL.Clear;
  QrContas.SQL.Add('SELECT * FROM Contas');
  if CBSubGrupo.KeyValue <> NULL then
    QrContas.SQL.Add('WHERE Subgrupo=:P0');
  QrContas.SQL.Add('ORDER BY Nome');
  if CBSubGrupo.KeyValue <> NULL then
    QrContas.Params[0].AsInteger := CBSubGrupo.KeyValue;
  QrContas.Open;
  CBConta.KeyValue := NULL;
end;

procedure TFmResultados1.RGTipoRelClick(Sender: TObject);
begin
  if RGTipoRel.ItemIndex = 2 then
  begin
    CkDataIni.Checked := False;
    CkDataFim.Checked := False;
    //
    CkDtDocIni.Checked := True;
    CkDtDocFim.Checked := True;
    //
  end;
end;

procedure TFmResultados1.CBConjuntoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_DELETE then CBConjunto.KeyValue := NULL;
end;

procedure TFmResultados1.CBGrupoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_DELETE then CBGrupo.KeyValue := NULL;
end;

procedure TFmResultados1.CBSubgrupoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_DELETE then CBSubgrupo.KeyValue := NULL;
end;

procedure TFmResultados1.CBContaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_DELETE then CBConta.KeyValue := NULL;
end;

procedure TFmResultados1.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmResultados1.frxResultCOGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'Periodo' then
      Value := dmkPF.PeriodoImp2(TPDataIni.Date, TPdataFim.Date,
    CkDataIni.Checked, CkDataFim.Checked, 'Emiss�o: ', '', ' ');
    //Value :=FormatDateTime(VAR_FORMATDATE3, TPDataIni.Date)+ CO_ATE+
   // FormatDateTime(VAR_FORMATDATE3, TPDataFim.Date);

  if VarName = 'Vecto' then
      Value := dmkPF.PeriodoImp2(TPVctoIni.Date, TPVctoFim.Date,
    CkVctoIni.Checked, CkVctoFim.Checked, 'Vencimento: ', '', ' ');
    //Value := FormatDateTime(VAR_FORMATDATE3, TPVctoIni.Date)+ CO_ATE+
    //FormatDateTime(VAR_FORMATDATE3, TPVctoFim.Date);

  if VarName = 'DtDoc' then
      Value := dmkPF.PeriodoImp2(TPDtDocIni.Date, TPDtDocFim.Date,
    CkDtDocIni.Checked, CkDtDocFim.Checked, 'Data do documento: ', '', ' ');
    //Value := FormatDateTime(VAR_FORMATDATE3, TPDtDocIni.Date)+ CO_ATE+
    //FormatDateTime(VAR_FORMATDATE3, TPDtDocFim.Date);

  if VarName = 'DI01' then
  begin
    case RGPeriodoBalInd.ItemIndex of
      0: Value := dmkPF.PeriodoImp2(TPDtDocIni.Date, TPDtDocFim.Date,
    CkDtDocIni.Checked, CkDtDocFim.Checked, 'Consumo: ', '', ' ');
      1: Value := dmkPF.PeriodoImp2(TPVctoIni.Date, TPVctoFim.Date,
    CkVctoIni.Checked, CkVctoFim.Checked, 'Consumo: ', '', ' ');
      2: Value := dmkPF.PeriodoImp2(TPDtDocIni.Date, TPDtDocFim.Date,
    CkDtDocIni.Checked, CkDtDocFim.Checked, 'Consumo: ', '', ' ');
      else Value := 'Per�odo do consumo de insumos qu�micos = ??/??/????';
    end;
  end;


  // no relat�rio de contas est� errado retirei no dia 2008-07-23
  if VarName = 'Anterior' then Value := RES_Anterior;
  if VarName = 'Final' then Value := RES_Final;
  if VarName = 'SCredito' then Value := RES_Credito;
  if VarName = 'SDebito' then Value := RES_Debito;
  if VarName = 'SPeriodo' then Value := RES_Periodo;

end;

end.
