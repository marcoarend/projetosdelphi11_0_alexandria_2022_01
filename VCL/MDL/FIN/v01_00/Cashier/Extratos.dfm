object FmExtratos: TFmExtratos
  Left = 297
  Top = 162
  Caption = 'FIN-RELAT-005 :: Emiss'#227'o de Extratos de Contas'
  ClientHeight = 679
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 525
    Width = 784
    Height = 106
    Align = alClient
    TabOrder = 4
    object CkGrade: TCheckBox
      Left = 8
      Top = 23
      Width = 53
      Height = 17
      Caption = 'Grade.'
      TabOrder = 3
    end
    object EdA: TdmkEdit
      Left = 8
      Top = 64
      Width = 25
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 1
      Visible = False
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdZ: TdmkEdit
      Left = 36
      Top = 64
      Width = 25
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 1
      TabOrder = 2
      Visible = False
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object CkNiveis: TCheckBox
      Left = 8
      Top = 43
      Width = 53
      Height = 21
      Caption = 'N'#237'veis'
      TabOrder = 0
      Visible = False
    end
    object GBOmiss: TGroupBox
      Left = 195
      Top = 18
      Width = 289
      Height = 83
      Caption = '                                              '
      TabOrder = 4
      Visible = False
      object CkAnos: TCheckBox
        Left = 99
        Top = 19
        Width = 55
        Height = 17
        Caption = 'Anos'
        TabOrder = 3
      end
      object CkMeses: TCheckBox
        Left = 99
        Top = 39
        Width = 55
        Height = 17
        Caption = 'Meses'
        TabOrder = 4
      end
      object CkDias: TCheckBox
        Left = 99
        Top = 59
        Width = 55
        Height = 17
        Caption = 'Dias'
        TabOrder = 5
      end
      object EdAnos: TdmkEdit
        Left = 8
        Top = 17
        Width = 77
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdMeses: TdmkEdit
        Left = 8
        Top = 37
        Width = 77
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdDias: TdmkEdit
        Left = 8
        Top = 57
        Width = 77
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object BtSalvar: TBitBtn
        Tag = 245
        Left = 164
        Top = 10
        Width = 120
        Height = 68
        Caption = 'Salvar esta '#13#10'configura'#231#227'o'
        NumGlyphs = 2
        TabOrder = 6
        OnClick = BtSalvarClick
      end
      object CkOmiss: TCheckBox
        Left = 12
        Top = 0
        Width = 137
        Height = 17
        Caption = 'Omitir abertos a mais de:'
        TabOrder = 7
        OnClick = CkOmissClick
      end
    end
    object GBPerdido: TGroupBox
      Left = 489
      Top = 18
      Width = 284
      Height = 83
      Caption = ' D'#237'vida ativa - abertos a : '
      TabOrder = 5
      Visible = False
      object CkAnos2: TCheckBox
        Left = 96
        Top = 19
        Width = 55
        Height = 17
        Caption = 'Anos'
        TabOrder = 3
      end
      object CkMeses2: TCheckBox
        Left = 96
        Top = 39
        Width = 55
        Height = 17
        Caption = 'Meses'
        TabOrder = 4
      end
      object CkDias2: TCheckBox
        Left = 96
        Top = 59
        Width = 55
        Height = 17
        Caption = 'Dias'
        TabOrder = 5
      end
      object EdAnos2: TdmkEdit
        Left = 8
        Top = 17
        Width = 77
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdMeses2: TdmkEdit
        Left = 8
        Top = 37
        Width = 77
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdDias2: TdmkEdit
        Left = 8
        Top = 57
        Width = 77
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object BtGravar: TBitBtn
        Tag = 245
        Left = 156
        Top = 10
        Width = 120
        Height = 68
        Caption = 'Salvar esta '#13#10'configura'#231#227'o'
        NumGlyphs = 2
        TabOrder = 6
        OnClick = BtGravarClick
      end
    end
    object CkSubstituir: TCheckBox
      Left = 15
      Top = 0
      Width = 241
      Height = 17
      Caption = 'Substituir a descri'#231#227'o pelo cliente \ fornecedor'
      TabOrder = 6
    end
    object CkDocEmAberto: TCheckBox
      Left = 262
      Top = 0
      Width = 319
      Height = 17
      Caption = 'Considerar datas originais de documentos em aberto no saldo'
      TabOrder = 7
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 631
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 5
    object BtImprime: TBitBtn
      Tag = 5
      Left = 8
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
      Caption = '&Imprime'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtImprimeClick
    end
    object BtSair: TBitBtn
      Tag = 13
      Left = 684
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSairClick
    end
    object BtEMail: TBitBtn
      Left = 345
      Top = 5
      Width = 90
      Height = 40
      Caption = 'En&via'
      TabOrder = 2
      Visible = False
      OnClick = BtEMailClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Emiss'#227'o de Extratos de Contas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 6
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 782
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object PainelA: TPanel
    Left = 0
    Top = 65
    Width = 784
    Height = 52
    Align = alTop
    TabOrder = 0
    object Label9: TLabel
      Left = 8
      Top = 8
      Width = 44
      Height = 13
      Caption = 'Empresa:'
    end
    object EdEmpresa: TdmkEditCB
      Left = 8
      Top = 24
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBEmpresa
      IgnoraDBLookupComboBox = False
    end
    object CBEmpresa: TdmkDBLookupComboBox
      Left = 64
      Top = 24
      Width = 709
      Height = 21
      KeyField = 'Filial'
      ListField = 'NOMEFILIAL'
      ListSource = DModG.DsEmpresas
      TabOrder = 1
      dmkEditCB = EdEmpresa
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
  end
  object PainelB: TPanel
    Left = 0
    Top = 117
    Width = 784
    Height = 96
    Align = alTop
    TabOrder = 1
    object RGTipo: TRadioGroup
      Left = 9
      Top = 1
      Width = 764
      Height = 94
      Caption = ' Tipo de relat'#243'rio: '
      Columns = 3
      Items.Strings = (
        'Extrato consolidado'
        'Fluxo de caixa'
        'Contas a pagar'
        'Contas a receber'
        'Contas pagas'
        'Contas recebidas'
        'A pagar emitidas'
        'A receber emitidas'
        'A pagar (d'#237'vida ativa)'
        'A receber (d'#237'vida ativa)')
      TabOrder = 0
      OnClick = RGTipoClick
    end
  end
  object PainelC: TPanel
    Left = 0
    Top = 213
    Width = 784
    Height = 84
    Align = alTop
    TabOrder = 2
    object PnDatas2: TPanel
      Left = 201
      Top = 1
      Width = 582
      Height = 82
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GBVencto: TGroupBox
        Left = 2
        Top = 4
        Width = 188
        Height = 72
        Caption = '                                           '
        Enabled = False
        TabOrder = 0
        object LaVenctI: TLabel
          Left = 8
          Top = 24
          Width = 30
          Height = 13
          Caption = 'In'#237'cio:'
          Enabled = False
        end
        object LaVenctF: TLabel
          Left = 8
          Top = 48
          Width = 19
          Height = 13
          Caption = 'Fim:'
          Enabled = False
        end
        object TPVctoIni: TdmkEditDateTimePicker
          Left = 48
          Top = 20
          Width = 112
          Height = 21
          Date = 36558.516375590300000000
          Time = 36558.516375590300000000
          Enabled = False
          TabOrder = 1
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object TPVctoFim: TdmkEditDateTimePicker
          Left = 48
          Top = 44
          Width = 112
          Height = 21
          Date = 37660.516439456000000000
          Time = 37660.516439456000000000
          Enabled = False
          TabOrder = 2
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object CkVencto: TCheckBox
          Left = 14
          Top = 0
          Width = 121
          Height = 17
          Caption = 'Data do vencimento: '
          Enabled = False
          TabOrder = 0
        end
      end
      object GroupBox2: TGroupBox
        Left = 192
        Top = 4
        Width = 188
        Height = 72
        Caption = '                                          '
        TabOrder = 1
        object Label1: TLabel
          Left = 12
          Top = 24
          Width = 30
          Height = 13
          Caption = 'In'#237'cio:'
        end
        object Label2: TLabel
          Left = 12
          Top = 48
          Width = 19
          Height = 13
          Caption = 'Fim:'
        end
        object TPDocIni: TdmkEditDateTimePicker
          Left = 48
          Top = 20
          Width = 112
          Height = 21
          Date = 36558.516375590300000000
          Time = 36558.516375590300000000
          TabOrder = 1
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object TPDocFim: TdmkEditDateTimePicker
          Left = 48
          Top = 44
          Width = 112
          Height = 21
          Date = 37660.516439456000000000
          Time = 37660.516439456000000000
          TabOrder = 2
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object CkDataDoc: TCheckBox
          Left = 16
          Top = 0
          Width = 117
          Height = 17
          Caption = 'Data do documento: '
          TabOrder = 0
        end
      end
      object GroupBox3: TGroupBox
        Left = 384
        Top = 4
        Width = 188
        Height = 72
        Caption = '                               '
        TabOrder = 2
        object Label5: TLabel
          Left = 12
          Top = 24
          Width = 30
          Height = 13
          Caption = 'In'#237'cio:'
        end
        object Label6: TLabel
          Left = 12
          Top = 48
          Width = 19
          Height = 13
          Caption = 'Fim:'
        end
        object TPCompIni: TdmkEditDateTimePicker
          Left = 48
          Top = 20
          Width = 112
          Height = 21
          Date = 36558.516375590300000000
          Time = 36558.516375590300000000
          TabOrder = 1
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object TPCompFim: TdmkEditDateTimePicker
          Left = 48
          Top = 44
          Width = 112
          Height = 21
          Date = 37660.516439456000000000
          Time = 37660.516439456000000000
          TabOrder = 2
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object CkDataComp: TCheckBox
          Left = 16
          Top = 0
          Width = 89
          Height = 17
          Caption = 'Compensado: '
          TabOrder = 0
        end
      end
    end
    object PnDatas1: TPanel
      Left = 1
      Top = 1
      Width = 200
      Height = 82
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      object GroupBox1: TGroupBox
        Left = 8
        Top = 4
        Width = 188
        Height = 72
        Caption = '                                     '
        TabOrder = 0
        object Label101: TLabel
          Left = 12
          Top = 24
          Width = 30
          Height = 13
          Caption = 'In'#237'cio:'
        end
        object Label102: TLabel
          Left = 12
          Top = 48
          Width = 19
          Height = 13
          Caption = 'Fim:'
        end
        object TPEmissIni: TdmkEditDateTimePicker
          Left = 48
          Top = 20
          Width = 112
          Height = 21
          Date = 36558.516375590300000000
          Time = 36558.516375590300000000
          TabOrder = 1
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object TPEmissFim: TdmkEditDateTimePicker
          Left = 48
          Top = 44
          Width = 112
          Height = 21
          Date = 37660.516439456000000000
          Time = 37660.516439456000000000
          TabOrder = 2
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object CkEmissao: TCheckBox
          Left = 16
          Top = 0
          Width = 105
          Height = 17
          Caption = 'Data da emiss'#227'o: '
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
      end
    end
  end
  object PainelD: TPanel
    Left = 0
    Top = 369
    Width = 784
    Height = 88
    Align = alTop
    TabOrder = 3
    object LaAccount: TLabel
      Left = 8
      Top = 4
      Width = 73
      Height = 13
      Caption = 'Representante:'
    end
    object LaTerceiro: TLabel
      Left = 396
      Top = 4
      Width = 57
      Height = 13
      Caption = 'Fornecedor:'
    end
    object Label4: TLabel
      Left = 396
      Top = 44
      Width = 31
      Height = 13
      Caption = 'Conta:'
    end
    object LaVendedor: TLabel
      Left = 8
      Top = 44
      Width = 49
      Height = 13
      Caption = 'Vendedor:'
    end
    object CBAccount: TdmkDBLookupComboBox
      Left = 64
      Top = 20
      Width = 320
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'NOMECONTATO'
      ListSource = DsAccounts
      TabOrder = 0
      dmkEditCB = EdAccount
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdAccount: TdmkEditCB
      Left = 8
      Top = 20
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBAccount
      IgnoraDBLookupComboBox = False
    end
    object CBTerceiro: TdmkDBLookupComboBox
      Left = 452
      Top = 20
      Width = 320
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'NOMECONTATO'
      ListSource = DsTerceiros
      TabOrder = 2
      OnKeyDown = CBTerceiroKeyDown
      dmkEditCB = EdTerceiro
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdTerceiro: TdmkEditCB
      Left = 396
      Top = 20
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBTerceiro
      IgnoraDBLookupComboBox = False
    end
    object CBConta: TdmkDBLookupComboBox
      Left = 452
      Top = 60
      Width = 320
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContas
      TabOrder = 4
      dmkEditCB = EdConta
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdConta: TdmkEditCB
      Left = 396
      Top = 60
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBConta
      IgnoraDBLookupComboBox = False
    end
    object CBVendedor: TdmkDBLookupComboBox
      Left = 64
      Top = 60
      Width = 320
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'NOMECONTATO'
      ListSource = DsVendedores
      TabOrder = 6
      OnKeyDown = CBTerceiroKeyDown
      dmkEditCB = EdVendedor
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdVendedor: TdmkEditCB
      Left = 8
      Top = 60
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBVendedor
      IgnoraDBLookupComboBox = False
    end
  end
  object PainelE: TPanel
    Left = 0
    Top = 457
    Width = 784
    Height = 68
    Align = alTop
    TabOrder = 7
    object RGOrdem1: TRadioGroup
      Left = 90
      Top = 0
      Width = 302
      Height = 66
      Caption = '                         '
      Columns = 4
      ItemIndex = 0
      Items.Strings = (
        'Entidade'
        'Dta Doc.'
        'Emiss'#227'o'
        'Vencto'
        'Doc.'
        'N.F.'
        'Duplic.'
        'UH / ??')
      TabOrder = 3
      TabStop = True
    end
    object RGOrdem2: TRadioGroup
      Left = 393
      Top = 1
      Width = 299
      Height = 66
      Caption = '                        '
      Columns = 4
      ItemIndex = 1
      Items.Strings = (
        'Entidade'
        'Dta Doc.'
        'Emiss'#227'o'
        'Vencto'
        'Doc.'
        'N.F.'
        'Duplic.'
        'UH / ??')
      TabOrder = 5
      TabStop = True
    end
    object CkOrdem1: TCheckBox
      Left = 105
      Top = 1
      Width = 73
      Height = 17
      Caption = ' Ordem 1: '
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 8
      Height = 66
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
    end
    object CkOrdem2: TCheckBox
      Left = 404
      Top = 1
      Width = 73
      Height = 17
      Caption = ' Ordem 2: '
      Checked = True
      State = cbChecked
      TabOrder = 4
    end
    object RGHistorico: TRadioGroup
      Left = 693
      Top = 1
      Width = 80
      Height = 66
      Caption = ' Emitidos: '
      ItemIndex = 0
      Items.Strings = (
        'Sint'#233'tico'
        'Anal'#237'tico')
      TabOrder = 6
    end
    object RGFonte: TRadioGroup
      Left = 8
      Top = 0
      Width = 80
      Height = 66
      Caption = ' Fonte: '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        '06'
        '07'
        '08'
        'xx')
      TabOrder = 1
    end
  end
  object PB1: TProgressBar
    Left = 0
    Top = 48
    Width = 784
    Height = 17
    Align = alTop
    TabOrder = 8
    Visible = False
  end
  object PnFluxo: TPanel
    Left = 0
    Top = 297
    Width = 784
    Height = 72
    Align = alTop
    TabOrder = 9
    object Label3: TLabel
      Left = 216
      Top = 20
      Width = 534
      Height = 13
      Caption = 
        '* Data em que se estima recebimentos (cr'#233'ditos) e/ou pagamentos ' +
        '(d'#233'bitos) de valores vencidos e n'#227'o quitados. '
    end
    object Label7: TLabel
      Left = 216
      Top = 36
      Width = 432
      Height = 13
      Caption = 
        '** Se a data informada for inferior a data inicial, os valores v' +
        'encidos ser'#227'o desconsiderados.'
    end
    object GroupBox5: TGroupBox
      Left = 8
      Top = 4
      Width = 201
      Height = 65
      Caption = ' Datas futuras de valores pendentes*: '
      TabOrder = 0
      object Label8: TLabel
        Left = 8
        Top = 20
        Width = 49
        Height = 13
        Caption = 'Cr'#233'ditos**:'
      end
      object Label10: TLabel
        Left = 100
        Top = 20
        Width = 47
        Height = 13
        Caption = 'D'#233'bitos**:'
      end
      object TPCre: TDateTimePicker
        Left = 8
        Top = 36
        Width = 90
        Height = 21
        CalColors.TextColor = clMenuText
        Date = 37636.777157974500000000
        Time = 37636.777157974500000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object TPDeb: TDateTimePicker
        Left = 100
        Top = 36
        Width = 90
        Height = 21
        Date = 37636.777203761600000000
        Time = 37636.777203761600000000
        TabOrder = 1
      end
    end
  end
  object QrExtrato: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT la.*, co.Nome NOMECONTA, ca.Nome NOMECARTEIRA'
      'FROM lanctos la, Contas co, Carteiras ca'
      'WHERE la.Data BETWEEN :P0 AND :P1'
      'AND (la.Tipo=1 OR (la.Tipo=0 AND Vencimento<=:P2))'
      'AND co.Codigo=la.Genero'
      'AND ca.Codigo=la.Carteira'
      'AND ca.ForneceI=:P3'
      'ORDER BY Data, Carteira, Controle')
    Left = 432
    Top = 48
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrExtratoNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Required = True
      Size = 50
    end
    object QrExtratoNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Required = True
      Size = 100
    end
    object QrExtratoData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrExtratoTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrExtratoCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrExtratoControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrExtratoSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrExtratoAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrExtratoGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrExtratoDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrExtratoNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrExtratoDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrExtratoCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrExtratoCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrExtratoDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrExtratoSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrExtratoVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrExtratoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrExtratoFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrExtratoFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrExtratoID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrExtratoID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrExtratoFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrExtratoBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrExtratoLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrExtratoCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrExtratoLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrExtratoOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrExtratoLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrExtratoPago: TFloatField
      FieldName = 'Pago'
    end
    object QrExtratoMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrExtratoFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrExtratoCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrExtratoMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrExtratoMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrExtratoProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrExtratoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrExtratoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrExtratoUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrExtratoUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrExtratoDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrExtratoCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrExtratoNivel: TIntegerField
      FieldName = 'Nivel'
      Required = True
    end
    object QrExtratoVendedor: TIntegerField
      FieldName = 'Vendedor'
      Required = True
    end
    object QrExtratoAccount: TIntegerField
      FieldName = 'Account'
      Required = True
    end
    object QrExtratoQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object QrInicial: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Inicial) Inicial FROM carteiras'
      'WHERE Tipo<2'
      'AND ForneceI=:P0')
    Left = 460
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrInicialInicial: TFloatField
      FieldName = 'Inicial'
      Origin = 'DBMMONEY.carteiras.Inicial'
    end
  end
  object QrAnterior: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(lan.Credito) Credito, SUM(lan.Debito) Debito,'
      'SUM(lan.Credito - lan.Debito) Saldo'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'WHERE lan.Data<:P0'
      'AND (lan.Tipo=1 OR (lan.Tipo=0 AND lan.Vencimento<:P0))'
      'AND car.ForneceI=:P1')
    Left = 488
    Top = 48
    ParamData = <
      item
        DataType = ftDate
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrAnteriorCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrAnteriorDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrAnteriorSaldo: TFloatField
      FieldName = 'Saldo'
    end
  end
  object QrAnterior2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(IF(lan.Sit=0, (lan.Credito-lan.Debito), '
      'IF(lan.Sit=1, (lan.Credito-lan.Debito+lan.Pago), 0))) SALDO '
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'WHERE lan.Vencimento BETWEEN :P0 AND :P1'
      'AND lan.Tipo in (0,2)'
      'AND car.ForneceI=:P2'
      '')
    Left = 460
    Top = 76
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrAnterior2SALDO: TFloatField
      FieldName = 'SALDO'
    end
  end
  object QrConsolidado: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(lan.Credito) Credito, SUM(lan.Debito) Debito,'
      'SUM(lan.Credito - lan.Debito) Saldo'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'WHERE (lan.Tipo=1'
      'OR (lan.Tipo=0 AND lan.Vencimento<=:P0))'
      'AND car.ForneceI=:P1')
    Left = 432
    Top = 76
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrConsolidadoCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrConsolidadoDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrConsolidadoSaldo: TFloatField
      FieldName = 'Saldo'
    end
  end
  object QrTerceiros: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTerceirosCalcFields
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN te.Tipo=0 then te.RazaoSocial'
      'ELSE te.Nome END NOMECONTATO,'
      'CASE WHEN te.Tipo=0 then te.ETe1'
      'ELSE te.PTe1 END Tel1,'
      'CASE WHEN te.Tipo=0 then te.ETe2'
      'ELSE te.PTe2 END Tel2,'
      'CASE WHEN te.Tipo=0 then te.ETe3'
      'ELSE te.PTe3 END Tel3'
      'FROM entidades te'
      'ORDER BY NOMECONTATO')
    Left = 64
    Top = 4
    object QrTerceirosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTerceirosNOMECONTATO: TWideStringField
      FieldName = 'NOMECONTATO'
      Size = 45
    end
    object QrTerceirosTel1: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Tel1'
    end
    object QrTerceirosTel2: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Tel2'
    end
    object QrTerceirosTel3: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'Tel3'
    end
    object QrTerceirosTEL1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL1_TXT'
      Size = 30
      Calculated = True
    end
    object QrTerceirosTEL2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL2_TXT'
      Size = 30
      Calculated = True
    end
    object QrTerceirosTEL3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL3_TXT'
      Size = 30
      Calculated = True
    end
    object QrTerceirosTELEFONES: TWideStringField
      DisplayWidth = 120
      FieldKind = fkCalculated
      FieldName = 'TELEFONES'
      Size = 100
      Calculated = True
    end
  end
  object DsTerceiros: TDataSource
    DataSet = QrTerceiros
    Left = 92
    Top = 4
  end
  object QrFluxo: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrFluxoCalcFields
    SQL.Strings = (
      'SELECT la.*, co.Nome NOMECONTA, ca.Nome NOMECARTEIRA,'
      'IF(la.Sit=0, (la.Credito-la.Debito), '
      'IF(la.Sit=1, (la.Credito-la.Debito+la.Pago), '
      'IF(la.Sit=3 and la.Tipo=0, (la.Credito-la.Debito), 0))) SALDO'
      'FROM lanctos la, Contas co, Carteiras ca'
      'WHERE la.Vencimento BETWEEN :P0 AND :P1'
      'AND la.Tipo in (0, 2)'
      'AND ((la.Sit<2 AND la.Tipo=2) or (la.tipo=0))'
      'AND co.Codigo=la.Genero'
      'AND ca.Codigo=la.Carteira'
      'AND ca.ForneceI=:P3'
      'ORDER BY Vencimento, Data, Carteira, Controle'
      '')
    Left = 488
    Top = 76
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrFluxoVENCIDO: TDateField
      FieldKind = fkCalculated
      FieldName = 'VENCIDO'
      Calculated = True
    end
    object QrFluxoVENCER: TDateField
      FieldKind = fkCalculated
      FieldName = 'VENCER'
      Calculated = True
    end
    object QrFluxoNOMEVENCIDO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEVENCIDO'
      Size = 50
      Calculated = True
    end
    object QrFluxoNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Required = True
      Size = 50
    end
    object QrFluxoNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Required = True
      Size = 100
    end
    object QrFluxoSALDO: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'SALDO'
    end
    object QrFluxoData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrFluxoTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrFluxoCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrFluxoControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrFluxoSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrFluxoAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrFluxoGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrFluxoDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrFluxoNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrFluxoDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrFluxoCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrFluxoCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrFluxoDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrFluxoSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrFluxoVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrFluxoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFluxoFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrFluxoFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrFluxoID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrFluxoID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrFluxoFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrFluxoBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrFluxoLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrFluxoCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrFluxoLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrFluxoOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrFluxoLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrFluxoPago: TFloatField
      FieldName = 'Pago'
    end
    object QrFluxoMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrFluxoFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrFluxoCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrFluxoMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrFluxoMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrFluxoProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrFluxoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFluxoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFluxoUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrFluxoUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrFluxoDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrFluxoCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrFluxoNivel: TIntegerField
      FieldName = 'Nivel'
      Required = True
    end
    object QrFluxoVendedor: TIntegerField
      FieldName = 'Vendedor'
      Required = True
    end
    object QrFluxoAccount: TIntegerField
      FieldName = 'Account'
      Required = True
    end
    object QrFluxoQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object QrPagRecIts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPagRecItsCalcFields
    SQL.Strings = (
      'SELECT la.*, co.Nome NOMECONTA, ca.Nome NOMECARTEIRA,'
      'IF(la.Sit=0, (la.Credito-la.Debito),'
      'IF(la.Sit=1, (la.Credito-la.Debito-la.Pago), 0)) SALDO,'
      'co.Credito EhCRED, co.Debito EhDEB,'
      'IF ((la.Fornecedor>0) AND (fo.Tipo=0), fo.RazaoSocial,'
      'IF ((la.Fornecedor>0) AND (fo.Tipo=1), fo.Nome,'
      'IF ((la.Cliente>0) AND (cl.Tipo=0), cl.RazaoSocial,'
      
        'IF ((la.Cliente>0) AND (cl.Tipo=1), cl.Nome, "ERRO SQL ??")))) N' +
        'OME_TERCEIRO,'
      
        'CASE WHEN la.Fornecedor>0 then la.Fornecedor ELSE la.Cliente END' +
        ' TERCEIRO'
      
        'FROM lanctos la, Contas co, Carteiras ca, Entidades cl, Entidade' +
        's fo'
      'WHERE fo.Codigo=la.Fornecedor AND cl.Codigo=la.Cliente'
      'AND la.Data BETWEEN "2003/12/07" AND "2004/02/05"'
      'AND la.Sit>1'
      'AND co.Codigo=la.Genero'
      'AND ca.Codigo=la.Carteira'
      'AND la.Debito>0')
    Left = 488
    Top = 104
    object StringField5: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMECLIENTE'
      LookupDataSet = QrTerceiros
      LookupKeyFields = 'Codigo'
      LookupResultField = 'NOMECONTATO'
      KeyFields = 'Cliente'
      Size = 50
      Lookup = True
    end
    object QrPagRecItsNOMEVENCIDO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEVENCIDO'
      Size = 50
      Calculated = True
    end
    object QrPagRecItsVALOR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALOR'
      Calculated = True
    end
    object QrPagRecItsPAGO_REAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PAGO_REAL'
      Calculated = True
    end
    object QrPagRecItsATRAZODD: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRAZODD'
      Calculated = True
    end
    object QrPagRecItsATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      Calculated = True
    end
    object QrPagRecItsMULTA_REAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MULTA_REAL'
      Calculated = True
    end
    object QrPagRecItsVENCER: TDateField
      FieldKind = fkCalculated
      FieldName = 'VENCER'
      Calculated = True
    end
    object QrPagRecItsVENCIDO: TDateField
      FieldKind = fkCalculated
      FieldName = 'VENCIDO'
      Calculated = True
    end
    object QrPagRecItsData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrPagRecItsTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrPagRecItsCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrPagRecItsSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrPagRecItsAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrPagRecItsGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrPagRecItsDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPagRecItsNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrPagRecItsDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrPagRecItsCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrPagRecItsCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrPagRecItsDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPagRecItsSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrPagRecItsVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrPagRecItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPagRecItsFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrPagRecItsFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrPagRecItsID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrPagRecItsFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrPagRecItsBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrPagRecItsLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrPagRecItsCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrPagRecItsLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrPagRecItsOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrPagRecItsLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrPagRecItsPago: TFloatField
      FieldName = 'Pago'
    end
    object QrPagRecItsFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrPagRecItsCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPagRecItsMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrPagRecItsMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrPagRecItsProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrPagRecItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPagRecItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPagRecItsUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrPagRecItsUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrPagRecItsDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrPagRecItsNivel: TIntegerField
      FieldName = 'Nivel'
      Required = True
    end
    object QrPagRecItsVendedor: TIntegerField
      FieldName = 'Vendedor'
      Required = True
    end
    object QrPagRecItsAccount: TIntegerField
      FieldName = 'Account'
      Required = True
    end
    object QrPagRecItsNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Required = True
      Size = 50
    end
    object QrPagRecItsNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Required = True
      Size = 100
    end
    object QrPagRecItsSALDO: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'SALDO'
    end
    object QrPagRecItsEhCRED: TWideStringField
      FieldName = 'EhCRED'
      Size = 1
    end
    object QrPagRecItsEhDEB: TWideStringField
      FieldName = 'EhDEB'
      Size = 1
    end
    object QrPagRecItsNOME_TERCEIRO: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOME_TERCEIRO'
      Required = True
      Size = 100
    end
    object QrPagRecItsTERCEIRO: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'TERCEIRO'
    end
    object QrPagRecItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPagRecItsID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrPagRecItsMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrPagRecItsCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrPagRecItsNO_UH: TWideStringField
      FieldName = 'NO_UH'
      Size = 10
    end
  end
  object QrPagRecX: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPagRecXCalcFields
    SQL.Strings = (
      'SELECT la.*, co.Nome NOMECONTA, ca.Nome NOMECARTEIRA,'
      'IF(la.Sit=0, (la.Credito-la.Debito),'
      'IF(la.Sit=1, (la.Credito-la.Debito-la.Pago), 0)) SALDO,'
      'co.Credito EhCRED, co.Debito EhDEB,'
      'IF ((la.Fornecedor>0) AND (fo.Tipo=0), fo.RazaoSocial,'
      'IF ((la.Fornecedor>0) AND (fo.Tipo=1), fo.Nome,'
      'IF ((la.Cliente>0) AND (cl.Tipo=0), cl.RazaoSocial,'
      
        'IF ((la.Cliente>0) AND (cl.Tipo=1), cl.Nome, "ERRO SQL ??")))) N' +
        'OME_TERCEIRO,'
      
        'CASE WHEN la.Fornecedor>0 then la.Fornecedor ELSE la.Cliente END' +
        ' TERCEIRO'
      
        'FROM lanctos la, Contas co, Carteiras ca, Entidades cl, Entidade' +
        's fo'
      'WHERE fo.Codigo=la.Fornecedor AND cl.Codigo=la.Cliente'
      'AND la.Data BETWEEN "2003/12/07" AND "2004/02/05"'
      'AND la.Sit>1'
      'AND co.Codigo=la.Genero'
      'AND ca.Codigo=la.Carteira'
      'AND la.Debito>0')
    Left = 460
    Top = 104
    object QrPagRecXVALOR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALOR'
      Calculated = True
    end
    object QrPagRecXPAGO_REAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PAGO_REAL'
      Calculated = True
    end
    object QrPagRecXNOMEVENCIDO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEVENCIDO'
      Size = 30
      Calculated = True
    end
    object StringField7: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMECLIENTE'
      LookupDataSet = QrTerceiros
      LookupKeyFields = 'Codigo'
      LookupResultField = 'NOMECONTATO'
      KeyFields = 'Cliente'
      Size = 50
      Lookup = True
    end
    object StringField8: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMEFORNECEDOR'
      LookupDataSet = QrTerceiros
      LookupKeyFields = 'Codigo'
      LookupResultField = 'NOMECONTATO'
      KeyFields = 'Fornecedor'
      Size = 50
      Lookup = True
    end
    object QrPagRecXATRAZODD: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRAZODD'
      Calculated = True
    end
    object QrPagRecXATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      Calculated = True
    end
    object QrPagRecXMULTA_REAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MULTA_REAL'
      Calculated = True
    end
    object QrPagRecXVENCER: TDateField
      FieldKind = fkCalculated
      FieldName = 'VENCER'
      Calculated = True
    end
    object QrPagRecXVENCIDO: TDateField
      FieldKind = fkCalculated
      FieldName = 'VENCIDO'
      Calculated = True
    end
    object QrPagRecXPENDENTE: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PENDENTE'
      Calculated = True
    end
    object QrPagRecXData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrPagRecXTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrPagRecXCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrPagRecXSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrPagRecXAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrPagRecXGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrPagRecXDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPagRecXNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrPagRecXDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrPagRecXCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrPagRecXCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrPagRecXDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPagRecXSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrPagRecXVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrPagRecXLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPagRecXFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrPagRecXFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrPagRecXID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrPagRecXFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrPagRecXBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrPagRecXLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrPagRecXCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrPagRecXLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrPagRecXOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrPagRecXLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrPagRecXPago: TFloatField
      FieldName = 'Pago'
    end
    object QrPagRecXFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrPagRecXCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPagRecXMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrPagRecXMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrPagRecXProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrPagRecXDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPagRecXDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPagRecXUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrPagRecXUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrPagRecXDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrPagRecXNivel: TIntegerField
      FieldName = 'Nivel'
      Required = True
    end
    object QrPagRecXVendedor: TIntegerField
      FieldName = 'Vendedor'
      Required = True
    end
    object QrPagRecXAccount: TIntegerField
      FieldName = 'Account'
      Required = True
    end
    object QrPagRecXNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Required = True
      Size = 50
    end
    object QrPagRecXNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Required = True
      Size = 100
    end
    object QrPagRecXSALDO: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'SALDO'
    end
    object QrPagRecXEhCRED: TWideStringField
      FieldName = 'EhCRED'
      Size = 1
    end
    object QrPagRecXEhDEB: TWideStringField
      FieldName = 'EhDEB'
      Size = 1
    end
    object QrPagRecXNOME_TERCEIRO: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOME_TERCEIRO'
      Required = True
      Size = 100
    end
    object QrPagRecXTERCEIRO: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'TERCEIRO'
    end
    object QrPagRecXControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPagRecXID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrPagRecXMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrPagRecXCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrPagRecXDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrPagRecXFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrPagRecXICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrPagRecXICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrPagRecXQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrPagRecXNO_UH: TWideStringField
      FieldName = 'NO_UH'
      Size = 10
    end
  end
  object QrPagRec1: TmySQLQuery
    Database = DModG.MyPID_DB
    OnCalcFields = QrPagRec1CalcFields
    SQL.Strings = (
      'SELECT * FROM pagrec1')
    Left = 432
    Top = 104
    object QrPagRec1PAGO_ROLADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PAGO_ROLADO'
      Size = 255
      Calculated = True
    end
    object QrPagRec1DEVIDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DEVIDO'
      Calculated = True
    end
    object QrPagRec1PEND_TOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PEND_TOTAL'
      Calculated = True
    end
    object QrPagRec1Data: TDateField
      FieldName = 'Data'
    end
    object QrPagRec1DataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrPagRec1Vencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrPagRec1TERCEIRO: TFloatField
      FieldName = 'TERCEIRO'
    end
    object QrPagRec1NOME_TERCEIRO: TWideStringField
      FieldName = 'NOME_TERCEIRO'
      Size = 255
    end
    object QrPagRec1NOMEVENCIDO: TWideStringField
      FieldName = 'NOMEVENCIDO'
      Size = 255
    end
    object QrPagRec1NOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 255
    end
    object QrPagRec1Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
    object QrPagRec1CtrlPai: TFloatField
      FieldName = 'CtrlPai'
    end
    object QrPagRec1Tipo: TFloatField
      FieldName = 'Tipo'
    end
    object QrPagRec1Documento: TFloatField
      FieldName = 'Documento'
    end
    object QrPagRec1NotaFiscal: TFloatField
      FieldName = 'NotaFiscal'
    end
    object QrPagRec1Valor: TFloatField
      FieldName = 'Valor'
    end
    object QrPagRec1MoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrPagRec1PAGO_REAL: TFloatField
      FieldName = 'PAGO_REAL'
    end
    object QrPagRec1PENDENTE: TFloatField
      FieldName = 'PENDENTE'
    end
    object QrPagRec1ATUALIZADO: TFloatField
      FieldName = 'ATUALIZADO'
    end
    object QrPagRec1MULTA_REAL: TFloatField
      FieldName = 'MULTA_REAL'
    end
    object QrPagRec1ATRAZODD: TFloatField
      FieldName = 'ATRAZODD'
    end
    object QrPagRec1ID_Pgto: TFloatField
      FieldName = 'ID_Pgto'
    end
    object QrPagRec1CtrlIni: TFloatField
      FieldName = 'CtrlIni'
    end
    object QrPagRec1Controle: TFloatField
      FieldName = 'Controle'
    end
    object QrPagRec1Duplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 15
    end
    object QrPagRec1Qtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrPagRec1NO_UH: TWideStringField
      FieldName = 'NO_UH'
      Size = 10
    end
  end
  object QrPagRec: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPagRecCalcFields
    SQL.Strings = (
      'SELECT la.*, co.Nome NOMECONTA, ca.Nome NOMECARTEIRA,'
      'IF(la.Sit=0, (la.Credito-la.Debito),'
      'IF(la.Sit=1, (la.Credito-la.Debito-la.Pago), 0)) SALDO,'
      'co.Credito EhCRED, co.Debito EhDEB, '
      'IF ((la.Fornecedor>0) AND (fo.Tipo=0), fo.RazaoSocial, '
      'IF ((la.Fornecedor>0) AND (fo.Tipo=1), fo.Nome, '
      'IF ((la.Cliente>0) AND (cl.Tipo=0), cl.RazaoSocial, '
      
        'IF ((la.Cliente>0) AND (cl.Tipo=1), cl.Nome, "??????")))) NOME_T' +
        'ERCEIRO, '
      
        'CASE WHEN la.Fornecedor>0 then la.Fornecedor ELSE la.Cliente END' +
        ' TERCEIRO,'
      'la.Credito CRED, la.Debito DEBI'
      
        'FROM lanctos la, Contas co, Carteiras ca, Entidades cl, Entidade' +
        's fo'
      'WHERE fo.Codigo=la.Fornecedor AND cl.Codigo=la.Cliente'
      'AND la.Tipo=2'
      'AND la.Sit<2'
      'AND co.Codigo=la.Genero'
      'AND ca.Codigo=la.Carteira'
      'AND la.Debito>0'
      'AND ID_Pgto=0'
      
        'ORDER BY NOME_TERCEIRO, DataDoc, Data, Vencimento, Carteira, Con' +
        'trole')
    Left = 516
    Top = 104
    object QrPagRecData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrPagRecTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrPagRecCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrPagRecControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPagRecSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrPagRecAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrPagRecGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrPagRecDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPagRecNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrPagRecDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrPagRecCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrPagRecCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrPagRecDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPagRecSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrPagRecVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrPagRecLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPagRecFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrPagRecFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrPagRecID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrPagRecID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrPagRecFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrPagRecBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrPagRecLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrPagRecCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrPagRecLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrPagRecOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrPagRecLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrPagRecPago: TFloatField
      FieldName = 'Pago'
    end
    object QrPagRecMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrPagRecFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrPagRecCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPagRecMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrPagRecMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrPagRecProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrPagRecDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPagRecDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPagRecUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrPagRecUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrPagRecDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrPagRecCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrPagRecNivel: TIntegerField
      FieldName = 'Nivel'
      Required = True
    end
    object QrPagRecVendedor: TIntegerField
      FieldName = 'Vendedor'
      Required = True
    end
    object QrPagRecAccount: TIntegerField
      FieldName = 'Account'
      Required = True
    end
    object QrPagRecNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Required = True
      Size = 50
    end
    object QrPagRecNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Required = True
      Size = 100
    end
    object QrPagRecSALDO: TFloatField
      FieldName = 'SALDO'
    end
    object QrPagRecEhCRED: TWideStringField
      FieldName = 'EhCRED'
      Size = 1
    end
    object QrPagRecEhDEB: TWideStringField
      FieldName = 'EhDEB'
      Size = 1
    end
    object QrPagRecNOME_TERCEIRO: TWideStringField
      FieldName = 'NOME_TERCEIRO'
      Required = True
      Size = 100
    end
    object QrPagRecTERCEIRO: TLargeintField
      FieldName = 'TERCEIRO'
    end
    object QrPagRecVENCER: TDateField
      FieldKind = fkCalculated
      FieldName = 'VENCER'
      Calculated = True
    end
    object QrPagRecVALOR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALOR'
      Calculated = True
    end
    object QrPagRecVENCIDO: TDateField
      FieldKind = fkCalculated
      FieldName = 'VENCIDO'
      Calculated = True
    end
    object QrPagRecATRAZODD: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRAZODD'
      Calculated = True
    end
    object QrPagRecMULTA_REAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MULTA_REAL'
      Calculated = True
    end
    object QrPagRecATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      Calculated = True
    end
    object QrPagRecPAGO_REAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PAGO_REAL'
      Calculated = True
    end
    object QrPagRecNOMEVENCIDO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEVENCIDO'
      Size = 100
      Calculated = True
    end
    object QrPagRecFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrPagRecICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrPagRecICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrPagRecDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrPagRecQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrPagRecNO_UH: TWideStringField
      FieldName = 'NO_UH'
      Size = 10
    end
    object QrPagRecSERIEDOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SERIEDOC'
      Size = 30
      Calculated = True
    end
    object QrPagRecSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrPagRecCRED: TFloatField
      FieldName = 'CRED'
    end
    object QrPagRecDEBI: TFloatField
      FieldName = 'DEBI'
    end
    object QrPagRecSerieNF: TWideStringField
      FieldName = 'SerieNF'
      Size = 3
    end
    object QrPagRecFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrPagRecID_Quit: TIntegerField
      FieldName = 'ID_Quit'
    end
    object QrPagRecEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrPagRecAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrPagRecContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrPagRecCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrPagRecCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrPagRecForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrPagRecMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrPagRecMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrPagRecDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrPagRecDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrPagRecDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrPagRecDescoControle: TIntegerField
      FieldName = 'DescoControle'
    end
    object QrPagRecUnidade: TIntegerField
      FieldName = 'Unidade'
    end
    object QrPagRecNFVal: TFloatField
      FieldName = 'NFVal'
    end
    object QrPagRecAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrPagRecExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrPagRecDoc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrPagRecCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
    end
    object QrPagRecTipoCH: TSmallintField
      FieldName = 'TipoCH'
    end
    object QrPagRecReparcel: TIntegerField
      FieldName = 'Reparcel'
    end
    object QrPagRecAtrelado: TIntegerField
      FieldName = 'Atrelado'
    end
    object QrPagRecPagMul: TFloatField
      FieldName = 'PagMul'
    end
    object QrPagRecPagJur: TFloatField
      FieldName = 'PagJur'
    end
    object QrPagRecSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
    end
    object QrPagRecMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
    end
    object QrPagRecProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrPagRecCtrlQuitPg: TIntegerField
      FieldName = 'CtrlQuitPg'
    end
    object QrPagRecEndossas: TSmallintField
      FieldName = 'Endossas'
    end
    object QrPagRecEndossan: TFloatField
      FieldName = 'Endossan'
    end
    object QrPagRecEndossad: TFloatField
      FieldName = 'Endossad'
    end
    object QrPagRecCancelado: TSmallintField
      FieldName = 'Cancelado'
    end
    object QrPagRecEventosCad: TIntegerField
      FieldName = 'EventosCad'
    end
    object QrPagRecEncerrado: TIntegerField
      FieldName = 'Encerrado'
    end
    object QrPagRecAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPagRecAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPagRecErrCtrl: TIntegerField
      FieldName = 'ErrCtrl'
    end
  end
  object DsAccounts: TDataSource
    DataSet = QrAccounts
    Left = 36
    Top = 4
  end
  object QrAccounts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN te.Tipo=0 then te.RazaoSocial'
      'ELSE te.Nome END NOMECONTATO'
      'FROM entidades te'
      'WHERE Fornece3="V"'
      'ORDER BY NOMECONTATO')
    Left = 8
    Top = 4
    object QrAccountsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrAccountsNOMECONTATO: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMECONTATO'
      Size = 100
    end
  end
  object QrVendedores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN te.Tipo=0 then te.RazaoSocial'
      'ELSE te.Nome END NOMECONTATO'
      'FROM entidades te'
      'WHERE Fornece3="V"'
      'ORDER BY NOMECONTATO')
    Left = 120
    Top = 4
    object QrVendedoresCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrVendedoresNOMECONTATO: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMECONTATO'
      Size = 100
    end
  end
  object DsVendedores: TDataSource
    DataSet = QrVendedores
    Left = 148
    Top = 4
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'ORDER BY Nome')
    Left = 176
    Top = 4
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 204
    Top = 4
  end
  object frxFin_Relat_005_02_B1: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.518688692100000000
    ReportOptions.LastChange = 39720.518688692100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  GH1.Condition := <VFR_CODITION_A>;'
      '  GH2.Condition := <VFR_CODITION_B>;'
      '  if <VFR_ID_PAGTO> <> 0 then '
      '    MasterData1.Visible := False '
      '  else '
      '    MasterData1.Visible := True;'
      'end.')
    OnGetValue = frxFin_Relat_005_02_B1GetValue
    Left = 348
    Top = 132
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPagRec1
        DataSetName = 'frxDsPagRec1'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 362.834880000000000000
        Width = 680.315400000000000000
        object Memo9: TfrxMemoView
          Width = 370.393700787401600000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [GRUPOB]:')
          ParentFont = False
          WordWrap = False
        end
        object Memo2: TfrxMemoView
          Left = 381.732281020000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."Valor">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 517.795273150000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."ATUALIZADO">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo44: TfrxMemoView
          Left = 449.763777090000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."PAGO_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          Left = 634.960627480000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."PEND_TOTAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo60: TfrxMemoView
          Left = 589.606296770000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."DEVIDO">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 283.464750000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPagRec1."Vencimento"'
        object Memo1: TfrxMemoView
          Left = 18.897637800000000000
          Width = 661.417322830000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[GRUPOB]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 24.566936460000000000
        Top = 476.220780000000000000
        Width = 680.315400000000000000
        object Memo20: TfrxMemoView
          Top = 11.338582680000000000
          Width = 370.393700787401600000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [GRUPOC]:')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 381.732281020000000000
          Top = 11.338582680000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."Valor">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 517.795273150000000000
          Top = 11.338582680000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."ATUALIZADO">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 449.763777090000000000
          Top = 11.338582680000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."PAGO_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 634.960627480000000000
          Top = 11.338590000000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."PEND_TOTAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 589.606296770000000000
          Top = 11.338582680000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."DEVIDO">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object CH1: TfrxColumnHeader
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 166.299320000000000000
        Width = 680.315400000000000000
        object Memo26: TfrxMemoView
          Left = 30.236220472440940000
          Width = 30.236220472440940000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 60.472440940000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'S'#233'rie - Doc.')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 120.944803780000000000
          Width = 170.078776770000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 291.023653780000000000
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 381.732315200000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 321.259891340000000000
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Duplic.')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Width = 30.236220472440940000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Emiss.')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 351.496094720000000000
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 517.732564170000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Atualizado')
          ParentFont = False
          WordWrap = False
        end
        object Memo73: TfrxMemoView
          Left = 495.118146850000000000
          Width = 22.677165350000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'DA')
          ParentFont = False
          WordWrap = False
        end
        object Memo74: TfrxMemoView
          Left = 449.763813700000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
          WordWrap = False
        end
        object Memo75: TfrxMemoView
          Left = 563.149645350000000000
          Width = 26.456692910000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Multa')
          ParentFont = False
          WordWrap = False
        end
        object Memo81: TfrxMemoView
          Left = 427.086648350000000000
          Width = 22.677165350000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'M.dd')
          ParentFont = False
          WordWrap = False
        end
        object Memo82: TfrxMemoView
          Left = 589.606680000000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Devido')
          ParentFont = False
          WordWrap = False
        end
        object Memo83: TfrxMemoView
          Left = 634.961040000000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pendente')
          ParentFont = False
          WordWrap = False
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        Height = 20.000000000000000000
        Top = 241.889920000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPagRec1."Data"'
        object Memo12: TfrxMemoView
          Width = 680.314960630000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[GRUPOA]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 400.630180000000000000
        Width = 680.315400000000000000
        object Memo17: TfrxMemoView
          Width = 370.393700787401600000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [GRUPOA]:')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 381.732281020000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."Valor">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          Left = 517.795273150000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."ATUALIZADO">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Left = 449.763777090000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."PAGO_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          Left = 634.960627480000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."PEND_TOTAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          Left = 589.606296770000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."DEVIDO">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 325.039580000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPagRec1
        DataSetName = 'frxDsPagRec1'
        RowCount = 0
        object Memo55: TfrxMemoView
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          DataField = 'Data'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."Data"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 60.472440940000000000
          Width = 60.472440944881890000
          Height = 13.228346460000000000
          DataField = 'Documento'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."Documento"]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 291.023653780000000000
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          DataField = 'NotaFiscal'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."NotaFiscal"]')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 381.732315200000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."Valor"]')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 30.236220470000000000
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          DataField = 'Vencimento'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."Vencimento"]')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 351.496094720000000000
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          DataField = 'Controle'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."Controle"]')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 517.795273150000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DataField = 'ATUALIZADO'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."ATUALIZADO"]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Left = 427.086648350000000000
          Width = 22.677165350000000000
          Height = 13.228346456692910000
          DataField = 'MoraDia'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."MoraDia"]')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 495.118146850000000000
          Width = 22.677165350000000000
          Height = 13.228346456692910000
          DataField = 'ATRAZODD'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."ATRAZODD"]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 449.763813700000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DataField = 'PAGO_REAL'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."PAGO_REAL"]')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 563.149645350000000000
          Width = 26.456692910000000000
          Height = 13.228346456692910000
          DataField = 'MULTA_REAL'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."MULTA_REAL"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 634.960668980000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DataField = 'PEND_TOTAL'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."PEND_TOTAL"]')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 589.606335830000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DataField = 'DEVIDO'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."DEVIDO"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 321.259891340000000000
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          DataField = 'Duplicata'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."Duplicata"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 120.944960000000000000
          Width = 170.078828030000000000
          Height = 13.228346460000000000
          DataField = 'Descricao'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPagRec1."Descricao"]')
          ParentFont = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 124.724409450000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo41: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line4: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo45: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[NOMEREL]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_NOMEEMPRESA]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Top = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo:  [PERIODO]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 68.031496060000000000
          Top = 90.708661420000000000
          Width = 612.283464570000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[FORNECEDOR]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Top = 90.708661420000000000
          Width = 68.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TERCEIRO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 574.488188980000000000
          Top = 37.952690000000000000
          Width = 105.826771650000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[NIVEIS]')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Top = 37.952690000000000000
          Width = 574.488120630000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[OMITIDOS]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 291.023622050000000000
          Top = 107.716535430000000000
          Width = 173.858267720000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[REPRESENTANTE]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          Left = 222.992125980000000000
          Top = 107.716535430000000000
          Width = 68.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Representante:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          Left = 49.133858270000000000
          Top = 107.716535430000000000
          Width = 170.078737720000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VENDEDOR]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          Left = 0.771490000000000000
          Top = 107.716535430000000000
          Width = 49.133858270000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Vendedor:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          Left = 502.677165350000000000
          Top = 107.716535430000000000
          Width = 177.637797720000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[NOME_CONTA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          Left = 468.661417320000000000
          Top = 107.716535430000000000
          Width = 33.984230000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Conta:')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 525.354670000000000000
        Width = 680.315400000000000000
        object Memo5: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228348900000000000
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object frxDsPagRec1: TfrxDBDataset
    UserName = 'frxDsPagRec1'
    CloseDataSource = False
    DataSet = QrPagRec1
    BCDToCurrency = False
    Left = 404
    Top = 132
  end
  object frxFin_Relat_005_02_B2: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.524224351900000000
    ReportOptions.LastChange = 39720.524224351900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MasterData1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <VFR_ID_PAGTO> = 0 then '
      '    MasterData1.Visible := False '
      '  else '
      '    MasterData1.Visible := True;'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxFin_Relat_005_02_B1GetValue
    Left = 376
    Top = 132
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPagRec1
        DataSetName = 'frxDsPagRec1'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 124.724409450000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo41: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line4: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo45: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[NOMEREL]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_NOMEEMPRESA]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Top = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo:  [PERIODO]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 68.031496060000000000
          Top = 90.708661420000000000
          Width = 612.283464570000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[FORNECEDOR]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Top = 90.708661420000000000
          Width = 68.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TERCEIRO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 574.488188980000000000
          Top = 37.952690000000000000
          Width = 105.826771650000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[NIVEIS]')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Top = 37.952690000000000000
          Width = 574.488120630000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[OMITIDOS]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 291.023622050000000000
          Top = 107.716535430000000000
          Width = 173.858267720000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[REPRESENTANTE]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 222.992125980000000000
          Top = 107.716535430000000000
          Width = 68.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Representante:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 49.133858270000000000
          Top = 107.716535430000000000
          Width = 170.078737720000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VENDEDOR]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 0.771490000000000000
          Top = 107.716535430000000000
          Width = 49.133858270000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Vendedor:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 502.677165350000000000
          Top = 107.716535430000000000
          Width = 177.637797720000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[NOME_CONTA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 468.661417320000000000
          Top = 107.716535430000000000
          Width = 33.984230000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Conta:')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 423.307360000000000000
        Width = 680.315400000000000000
        object Memo9: TfrxMemoView
          Width = 370.393700790000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [GRUPOB]:')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 381.732281020000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."Valor">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 517.795273150000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."ATUALIZADO">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo44: TfrxMemoView
          Left = 449.763777090000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."PAGO_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          Left = 634.960627480000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."PEND_TOTAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo60: TfrxMemoView
          Left = 589.606296770000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."DEVIDO">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 283.464750000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPagRec1."Vencimento"'
        object Memo6: TfrxMemoView
          Left = 15.118107800000000000
          Width = 661.417322830000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[GRUPOB]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 362.834880000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPagRec1
        DataSetName = 'frxDsPagRec1'
        RowCount = 0
        object Memo108: TfrxMemoView
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          DataField = 'Data'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."Data"]')
          ParentFont = False
        end
        object Memo109: TfrxMemoView
          Left = 60.472440940000000000
          Width = 60.472460470000000000
          Height = 13.228346460000000000
          DataField = 'Documento'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."Documento"]')
          ParentFont = False
        end
        object Memo110: TfrxMemoView
          Left = 291.023653780000000000
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          DataField = 'NotaFiscal'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."NotaFiscal"]')
          ParentFont = False
        end
        object Memo112: TfrxMemoView
          Left = 381.732315200000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."Valor"]')
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          Left = 30.236220470000000000
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          DataField = 'Vencimento'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."Vencimento"]')
          ParentFont = False
        end
        object Memo114: TfrxMemoView
          Left = 351.496094720000000000
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          DataField = 'Controle'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."Controle"]')
          ParentFont = False
        end
        object Memo115: TfrxMemoView
          Left = 517.795273150000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DataField = 'ATUALIZADO'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."ATUALIZADO"]')
          ParentFont = False
        end
        object Memo116: TfrxMemoView
          Left = 427.086648350000000000
          Width = 22.677165350000000000
          Height = 13.228346456692910000
          DataField = 'MoraDia'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."MoraDia"]')
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          Left = 495.118146850000000000
          Width = 22.677165350000000000
          Height = 13.228346456692910000
          DataField = 'ATRAZODD'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."ATRAZODD"]')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          Left = 449.763813700000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DataField = 'PAGO_REAL'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."PAGO_REAL"]')
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          Left = 563.149645350000000000
          Width = 26.456692910000000000
          Height = 13.228346456692910000
          DataField = 'MULTA_REAL'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."MULTA_REAL"]')
          ParentFont = False
        end
        object Memo120: TfrxMemoView
          Left = 634.960668980000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DataField = 'PEND_TOTAL'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."PEND_TOTAL"]')
          ParentFont = False
        end
        object Memo121: TfrxMemoView
          Left = 589.606335830000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DataField = 'DEVIDO'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."DEVIDO"]')
          ParentFont = False
        end
        object Memo122: TfrxMemoView
          Left = 321.259891340000000000
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          DataField = 'Duplicata'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."Duplicata"]')
          ParentFont = False
        end
        object Memo123: TfrxMemoView
          Left = 120.944960000000000000
          Width = 170.078828030000000000
          Height = 13.228346460000000000
          DataField = 'Descricao'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPagRec1."Descricao"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 20.787406460000000000
        Top = 536.693260000000000000
        Width = 680.315400000000000000
        object Memo20: TfrxMemoView
          Top = 7.559052680000000000
          Width = 370.393700790000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [GRUPOC]:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 381.732281020000000000
          Top = 7.559052680000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."Valor">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 517.795273150000000000
          Top = 7.559052680000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."ATUALIZADO">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 449.763777090000000000
          Top = 7.559052680000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."PAGO_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 634.960627480000000000
          Top = 7.559060000000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."PEND_TOTAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 589.606296770000000000
          Top = 7.559052680000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."DEVIDO">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object CH1: TfrxColumnHeader
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 166.299320000000000000
        Width = 680.315400000000000000
        object Memo76: TfrxMemoView
          Left = 30.236220470000000000
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Left = 60.472440940000000000
          Width = 60.472440944881890000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'S'#233'rie - Doc.')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Left = 120.944803780000000000
          Width = 170.078776770000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Left = 291.023653780000000000
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          Left = 381.732315200000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          Left = 321.259891340000000000
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Duplic.')
          ParentFont = False
        end
        object Memo83: TfrxMemoView
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Emiss.')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Left = 351.496094720000000000
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          Left = 517.732564170000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Atualizado')
          ParentFont = False
          WordWrap = False
        end
        object Memo86: TfrxMemoView
          Left = 495.118146850000000000
          Width = 22.677165350000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'DA')
          ParentFont = False
          WordWrap = False
        end
        object Memo87: TfrxMemoView
          Left = 449.763813700000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
          WordWrap = False
        end
        object Memo88: TfrxMemoView
          Left = 563.149645350000000000
          Width = 26.456692910000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Multa')
          ParentFont = False
          WordWrap = False
        end
        object Memo89: TfrxMemoView
          Left = 427.086648350000000000
          Width = 22.677165350000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Mora')
          ParentFont = False
          WordWrap = False
        end
        object Memo90: TfrxMemoView
          Left = 589.606680000000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Devido')
          ParentFont = False
          WordWrap = False
        end
        object Memo91: TfrxMemoView
          Left = 634.961040000000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pendente')
          ParentFont = False
          WordWrap = False
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 241.889920000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPagRec1."Data"'
        object Memo12: TfrxMemoView
          Width = 680.314960630000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[GRUPOA]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 461.102660000000000000
        Width = 680.315400000000000000
        object Memo17: TfrxMemoView
          Width = 370.393700790000000000
          Height = 13.228346456692910000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [GRUPOA]:')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 381.732281020000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."Valor">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          Left = 517.795273150000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."ATUALIZADO">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Left = 449.763777090000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."PAGO_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          Left = 634.960627480000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."PEND_TOTAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          Left = 589.606296770000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec1."DEVIDO">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object Band2: TfrxGroupHeader
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 325.039580000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsPagRec1."CtrlPai"'
        object Memo92: TfrxMemoView
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          DataField = 'Data'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."Data"]')
          ParentFont = False
        end
        object Memo93: TfrxMemoView
          Left = 60.472440940000000000
          Width = 60.472460470000000000
          Height = 13.228346460000000000
          DataField = 'Documento'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."Documento"]')
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          Left = 291.023653780000000000
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          DataField = 'NotaFiscal'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."NotaFiscal"]')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Left = 381.732315200000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."Valor"]')
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          Left = 30.236220470000000000
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          DataField = 'Vencimento'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."Vencimento"]')
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          Left = 351.496094720000000000
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          DataField = 'Controle'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."Controle"]')
          ParentFont = False
        end
        object Memo99: TfrxMemoView
          Left = 517.795273150000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DataField = 'ATUALIZADO'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."ATUALIZADO"]')
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Left = 427.086648350000000000
          Width = 22.677165350000000000
          Height = 13.228346456692910000
          DataField = 'MoraDia'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."MoraDia"]')
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Left = 495.118146850000000000
          Width = 22.677165350000000000
          Height = 13.228346456692910000
          DataField = 'ATRAZODD'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."ATRAZODD"]')
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          Left = 449.763813700000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DataField = 'PAGO_REAL'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."PAGO_REAL"]')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          Left = 563.149645350000000000
          Width = 26.456692910000000000
          Height = 13.228346456692910000
          DataField = 'MULTA_REAL'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."MULTA_REAL"]')
          ParentFont = False
        end
        object Memo104: TfrxMemoView
          Left = 634.960668980000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DataField = 'PEND_TOTAL'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."PEND_TOTAL"]')
          ParentFont = False
        end
        object Memo105: TfrxMemoView
          Left = 589.606335830000000000
          Width = 45.354330710000000000
          Height = 13.228346456692910000
          DataField = 'DEVIDO'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."DEVIDO"]')
          ParentFont = False
        end
        object Memo106: TfrxMemoView
          Left = 321.259891340000000000
          Width = 30.236220470000000000
          Height = 13.228346456692910000
          DataField = 'Duplicata'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec1."Duplicata"]')
          ParentFont = False
        end
        object Memo107: TfrxMemoView
          Left = 120.944960000000000000
          Width = 170.078828030000000000
          Height = 13.228346460000000000
          DataField = 'Descricao'
          DataSet = frxDsPagRec1
          DataSetName = 'frxDsPagRec1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPagRec1."Descricao"]')
          ParentFont = False
        end
      end
      object Band1: TfrxGroupFooter
        FillType = ftBrush
        Top = 400.630180000000000000
        Visible = False
        Width = 680.315400000000000000
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 582.047620000000000000
        Width = 680.315400000000000000
        object Memo13: TfrxMemoView
          Width = 680.315400000000000000
          Height = 13.228348900000000000
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object frxDsPagRec: TfrxDBDataset
    UserName = 'frxDsPagRec'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Data=Data'
      'Tipo=Tipo'
      'Carteira=Carteira'
      'Controle=Controle'
      'Sub=Sub'
      'Autorizacao=Autorizacao'
      'Genero=Genero'
      'Descricao=Descricao'
      'NotaFiscal=NotaFiscal'
      'Debito=Debito'
      'Credito=Credito'
      'Compensado=Compensado'
      'Documento=Documento'
      'Sit=Sit'
      'Vencimento=Vencimento'
      'Lk=Lk'
      'FatID=FatID'
      'FatParcela=FatParcela'
      'ID_Pgto=ID_Pgto'
      'ID_Sub=ID_Sub'
      'Fatura=Fatura'
      'Banco=Banco'
      'Local=Local'
      'Cartao=Cartao'
      'Linha=Linha'
      'OperCount=OperCount'
      'Lancto=Lancto'
      'Pago=Pago'
      'Mez=Mez'
      'Fornecedor=Fornecedor'
      'Cliente=Cliente'
      'MoraDia=MoraDia'
      'Multa=Multa'
      'Protesto=Protesto'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'DataDoc=DataDoc'
      'CtrlIni=CtrlIni'
      'Nivel=Nivel'
      'Vendedor=Vendedor'
      'Account=Account'
      'NOMECONTA=NOMECONTA'
      'NOMECARTEIRA=NOMECARTEIRA'
      'SALDO=SALDO'
      'EhCRED=EhCRED'
      'EhDEB=EhDEB'
      'NOME_TERCEIRO=NOME_TERCEIRO'
      'TERCEIRO=TERCEIRO'
      'VENCER=VENCER'
      'VALOR=VALOR'
      'VENCIDO=VENCIDO'
      'ATRAZODD=ATRAZODD'
      'MULTA_REAL=MULTA_REAL'
      'ATUALIZADO=ATUALIZADO'
      'PAGO_REAL=PAGO_REAL'
      'NOMEVENCIDO=NOMEVENCIDO'
      'FatID_Sub=FatID_Sub'
      'ICMS_P=ICMS_P'
      'ICMS_V=ICMS_V'
      'Duplicata=Duplicata'
      'Qtde=Qtde'
      'NO_UH=NO_UH'
      'SERIEDOC=SERIEDOC'
      'SerieCH=SerieCH'
      'CRED=CRED'
      'DEBI=DEBI'
      'SerieNF=SerieNF'
      'FatNum=FatNum'
      'ID_Quit=ID_Quit'
      'Emitente=Emitente'
      'Agencia=Agencia'
      'ContaCorrente=ContaCorrente'
      'CNPJCPF=CNPJCPF'
      'CliInt=CliInt'
      'ForneceI=ForneceI'
      'MoraVal=MoraVal'
      'MultaVal=MultaVal'
      'Depto=Depto'
      'DescoPor=DescoPor'
      'DescoVal=DescoVal'
      'DescoControle=DescoControle'
      'Unidade=Unidade'
      'NFVal=NFVal'
      'Antigo=Antigo'
      'ExcelGru=ExcelGru'
      'Doc2=Doc2'
      'CNAB_Sit=CNAB_Sit'
      'TipoCH=TipoCH'
      'Reparcel=Reparcel'
      'Atrelado=Atrelado'
      'PagMul=PagMul'
      'PagJur=PagJur'
      'SubPgto1=SubPgto1'
      'MultiPgto=MultiPgto'
      'Protocolo=Protocolo'
      'CtrlQuitPg=CtrlQuitPg'
      'Endossas=Endossas'
      'Endossan=Endossan'
      'Endossad=Endossad'
      'Cancelado=Cancelado'
      'EventosCad=EventosCad'
      'Encerrado=Encerrado'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'ErrCtrl=ErrCtrl')
    DataSet = QrPagRec
    BCDToCurrency = False
    Left = 404
    Top = 104
  end
  object frxFin_Relat_005_00: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.627769097200000000
    ReportOptions.LastChange = 39720.627769097200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure PageFooter1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  if Engine.FinalPass then'
      '  begin              '
      
        '    MeSdoIniT.Visible := False;                                 ' +
        '                      '
      '    MeSdoIniV.Visible := False;'
      '  end;              '
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxFin_Relat_005_00GetValue
    Left = 376
    Top = 48
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsExtrato
        DataSetName = 'frxDsExtrato'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 14.220470000000000000
        Top = 302.362400000000000000
        Width = 680.315400000000000000
        object Memo41: TfrxMemoView
          Left = 364.015770000000000000
          Width = 256.000000000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo em [frxDsExtrato."Data"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 619.842553860000000000
          Width = 60.472440940000000000
          Height = 13.228346456692910000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SALDODIA]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 222.992270000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsExtrato."Data"'
        object Memo1: TfrxMemoView
          Left = 6.000000000000000000
          Width = 404.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsExtrato."Data"]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 264.567100000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsExtrato
        DataSetName = 'frxDsExtrato'
        RowCount = 0
        object Memo13: TfrxMemoView
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[frxDsExtrato."Data"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 34.015748030000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;000000; '#39', <frxDsExtrato."Documento">)]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 71.811001650000000000
          Width = 154.960629920000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtrato."NOMECARTEIRA"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 585.826771650000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39', <frxDsExtrato."NotaFiscal">)]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 396.850388820000000000
          Width = 188.976377950000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtrato."Descricao"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 619.842507480000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SALDO]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 226.771643780000000000
          Width = 132.283464570000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtrato."NOMECONTA"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 359.055350000000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsExtrato
          DataSetName = 'frxDsExtrato'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0.000;-0.000; '#39', <frxDsExtrato."Qtde">)]')
          ParentFont = False
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 147.401670000000000000
        Width = 680.315400000000000000
        object Memo7: TfrxMemoView
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 34.015748030000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'Docum.')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 396.850493780000000000
          Width = 188.976377950000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 585.826786300000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 619.842522130000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 226.771753620000000000
          Width = 132.283464570000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 71.810791730000000000
          Width = 154.960629920000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Carteira')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 359.055218190000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantid.')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 377.953000000000000000
        Width = 680.315400000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo4: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 105.826840000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo38: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'EXTRATO CONSOLIDADO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_NOMEEMPRESA]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Top = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo:  [PERIODO]')
          ParentFont = False
        end
        object MeSdoIniT: TfrxMemoView
          Top = 90.708720000000000000
          Width = 585.827150000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'SALDO INICIAL: '
            '')
          ParentFont = False
        end
        object MeSdoIniV: TfrxMemoView
          Left = 585.827150000000000000
          Top = 90.708720000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[INICIAL]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsExtrato: TfrxDBDataset
    UserName = 'frxDsExtrato'
    CloseDataSource = False
    DataSet = QrExtrato
    BCDToCurrency = False
    Left = 404
    Top = 48
  end
  object frxFin_Relat_005_01_A08: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.635407800900000000
    ReportOptions.LastChange = 40429.473596203700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxFin_Relat_005_00GetValue
    Left = 376
    Top = 76
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsExtr
        DataSetName = 'frxDsExtr'
      end
      item
        DataSet = frxDsFluxo
        DataSetName = 'frxDsFluxo'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 230.551330000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsExtr
        DataSetName = 'frxDsExtr'
        RowCount = 0
        object Memo3: TfrxMemoView
          Left = 47.244094490000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'Docum'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[frxDsExtr."Docum"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 141.732273700000000000
          Width = 147.401574800000000000
          Height = 13.228346460000000000
          DataField = 'Texto'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."Texto"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 107.716525670000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          DataField = 'NotaF'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[frxDsExtr."NotaF"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 289.354291650000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."VALOR"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 536.692981730000000000
          Width = 109.606299210000000000
          Height = 13.228346460000000000
          DataField = 'CartN'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."CartN"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          DataField = 'DataX'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataX"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 646.299630000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          DataField = 'Ctrle'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."Ctrle"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 342.267672600000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."Saldo"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 395.181063310000000000
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          DataField = 'DataE'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataE"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 442.425480000000000000
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          DataField = 'DataV'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataV"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 489.559370000000000000
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          DataField = 'DataQ'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataQ"]')
          ParentFont = False
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 132.283550000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          Left = 47.244094488188980000
          Width = 60.472440944881890000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'S'#233'rie - Doc.')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 141.732273700000000000
          Width = 147.401574800000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[SUBST]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 107.716525670000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 289.354291650000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 536.692981730000000000
          Width = 109.606299210000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Carteira')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Dta fluxo')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 646.299630000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ID')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 342.267672600000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 395.181063310000000000
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Emiss.')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 442.425480000000000000
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Venceu')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 489.559370000000000000
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Quita'#231#227'o')
          ParentFont = False
        end
      end
      object Band1: TfrxGroupHeader
        FillType = ftBrush
        Top = 207.874150000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsExtr."DataX"'
      end
      object Band2: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 268.346630000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          Left = 363.574830000000000000
          Width = 256.000000000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo em: [frxDsExtr."DataX"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 619.842519690000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."Saldo"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 343.937230000000000000
        Width = 680.315400000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo23: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 90.708720000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo38: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[NOMEREL]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_NOMEEMPRESA]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Top = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo:  [PERIODO]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsFluxo: TfrxDBDataset
    UserName = 'frxDsFluxo'
    CloseDataSource = False
    DataSet = QrFluxo
    BCDToCurrency = False
    Left = 404
    Top = 76
  end
  object QrIni: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Inicial) Valor'
      'FROM carteiras '
      'WHERE Tipo <> 2'
      'AND ForneceI=:P0')
    Left = 644
    Top = 178
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrIniValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrMovant: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrMovantCalcFields
    SQL.Strings = (
      'SELECT SUM(lan.Credito-lan.Debito) Movim'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'WHERE car.Tipo <> 2'
      'AND car.ForneceI=:P0'
      'AND Data < :P1'
      '')
    Left = 672
    Top = 178
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrMovantMovim: TFloatField
      FieldName = 'Movim'
    end
    object QrMovantVALOR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALOR'
      Calculated = True
    end
  end
  object DsFlxo: TDataSource
    DataSet = QrFlxo
    Left = 728
    Top = 178
  end
  object QrFlxo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT Date(IF(car.Tipo <> 2, lan.Data,IF(lan.Sit > 1, lan.Compe' +
        'nsado, IF(lan.Vencimento<SYSDATE(), IF(lan.Debito>0, "2007-12-11' +
        '", "2007-12-11"), lan.Vencimento)))) QUITACAO, '
      'lan.*, car.Nome NOMECART,'
      'IF(lan.Cliente>0,'
      '  IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome),'
      '  IF (lan.Fornecedor>0,'
      
        '    IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome), "")) NOMERELACIONADO' +
        ' '
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN entidades cl ON cl.Codigo=lan.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=lan.Fornecedor'
      'WHERE car.Fornecei=1'
      
        'AND Date(IF(car.Tipo <> 2, lan.Data,IF(lan.Sit > 1, lan.Compensa' +
        'do, IF(lan.Vencimento<SYSDATE(), IF(lan.Debito>0, "2007-12-11", ' +
        '"2007-12-11"), lan.Vencimento)))) >= "2007-12-11"'
      'ORDER BY QUITACAO, Credito DESC, Debito')
    Left = 700
    Top = 178
    object QrFlxoQUITACAO: TWideStringField
      FieldName = 'QUITACAO'
      Size = 10
    end
    object QrFlxoData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrFlxoTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrFlxoCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrFlxoControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrFlxoSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrFlxoAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrFlxoGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrFlxoQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrFlxoDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrFlxoNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrFlxoDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrFlxoCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrFlxoCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrFlxoSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrFlxoDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrFlxoSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrFlxoVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrFlxoFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrFlxoFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrFlxoFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrFlxoID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrFlxoID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrFlxoFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrFlxoEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrFlxoBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrFlxoAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrFlxoContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrFlxoCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrFlxoLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrFlxoCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrFlxoLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrFlxoOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrFlxoLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrFlxoPago: TFloatField
      FieldName = 'Pago'
    end
    object QrFlxoMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrFlxoFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrFlxoCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrFlxoCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrFlxoForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrFlxoMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrFlxoMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrFlxoMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrFlxoMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrFlxoProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrFlxoDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrFlxoCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrFlxoNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrFlxoVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrFlxoAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrFlxoICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrFlxoICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrFlxoDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrFlxoDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrFlxoDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrFlxoDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrFlxoDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Required = True
    end
    object QrFlxoUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrFlxoNFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrFlxoAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrFlxoExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrFlxoDoc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrFlxoCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
    end
    object QrFlxoTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Required = True
    end
    object QrFlxoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFlxoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFlxoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFlxoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFlxoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFlxoNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Size = 100
    end
    object QrFlxoNOMERELACIONADO: TWideStringField
      FieldName = 'NOMERELACIONADO'
      Size = 100
    end
  end
  object QrExtr: TmySQLQuery
    Database = DModG.MyPID_DB
    BeforeOpen = QrExtrBeforeOpen
    OnCalcFields = QrExtrCalcFields
    SQL.Strings = (
      'SELECT * FROM extratocc2')
    Left = 700
    Top = 206
    object QrExtratoDataE: TDateField
      FieldName = 'DataE'
      Origin = 'extratocc2.DataE'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrExtratoDataV: TDateField
      FieldName = 'DataV'
      Origin = 'extratocc2.DataV'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrExtratoDataQ: TDateField
      FieldName = 'DataQ'
      Origin = 'extratocc2.DataQ'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrExtratoDataX: TDateField
      FieldName = 'DataX'
      Origin = 'extratocc2.DataX'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrExtratoTexto: TWideStringField
      FieldName = 'Texto'
      Origin = 'extratocc2.Texto'
      Size = 255
    end
    object QrExtratoDocum: TWideStringField
      FieldName = 'Docum'
      Origin = 'extratocc2.Docum'
      Size = 30
    end
    object QrExtratoNotaF: TWideStringField
      FieldName = 'NotaF'
      Origin = 'extratocc2.NotaF'
      Size = 30
    end
    object QrExtratoSaldo: TFloatField
      FieldName = 'Saldo'
      Origin = 'extratocc2.Saldo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrExtrCredi: TFloatField
      FieldName = 'Credi'
      Origin = 'extratocc2.Credi'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrExtrDebit: TFloatField
      FieldName = 'Debit'
      Origin = 'extratocc2.Debit'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrExtratoCartC: TIntegerField
      FieldName = 'CartC'
      Origin = 'extratocc2.CartC'
    end
    object QrExtratoCartN: TWideStringField
      FieldName = 'CartN'
      Origin = 'extratocc2.CartN'
      Size = 100
    end
    object QrExtratoCodig: TIntegerField
      FieldName = 'Codig'
      Origin = 'extratocc2.Codig'
    end
    object QrExtratoCtrle: TIntegerField
      FieldName = 'Ctrle'
      Origin = 'extratocc2.Ctrle'
    end
    object QrExtratoCtSub: TIntegerField
      FieldName = 'CtSub'
      Origin = 'extratocc2.CtSub'
    end
    object QrExtratoCART_ORIG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'CART_ORIG'
      LookupDataSet = QrLct
      LookupKeyFields = 'Controle'
      LookupResultField = 'NOMECART'
      KeyFields = 'ID_Pg'
      Size = 100
      Lookup = True
    end
    object QrExtratoID_Pg: TIntegerField
      FieldName = 'ID_Pg'
      Origin = 'extratocc2.ID_Pg'
    end
    object QrExtratoTipoI: TIntegerField
      FieldName = 'TipoI'
      Origin = 'extratocc2.TipoI'
    end
    object QrExtrVALOR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALOR'
      Calculated = True
    end
  end
  object DsExtr: TDataSource
    DataSet = QrExtr
    Left = 728
    Top = 206
  end
  object QrLct: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lan.Controle, lan.Carteira, IF(lan.Controle=0, "", '
      'IF(lan.Carteira=0, "", car.Nome)) NOMECART '
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'WHERE car.ForneceI=:P0')
    Left = 616
    Top = 178
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object frxDsExtr: TfrxDBDataset
    UserName = 'frxDsExtr'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DataE=DataE'
      'DataV=DataV'
      'DataQ=DataQ'
      'DataX=DataX'
      'Texto=Texto'
      'Docum=Docum'
      'NotaF=NotaF'
      'Saldo=Saldo'
      'Credi=Credi'
      'Debit=Debit'
      'CartC=CartC'
      'CartN=CartN'
      'Codig=Codig'
      'Ctrle=Ctrle'
      'CtSub=CtSub'
      'CART_ORIG=CART_ORIG'
      'ID_Pg=ID_Pg'
      'TipoI=TipoI'
      'VALOR=VALOR')
    DataSet = QrExtr
    BCDToCurrency = False
    Left = 672
    Top = 206
  end
  object frxFin_Relat_005_01_A07: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.635407800900000000
    ReportOptions.LastChange = 40429.473596203700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxFin_Relat_005_00GetValue
    Left = 348
    Top = 76
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsExtr
        DataSetName = 'frxDsExtr'
      end
      item
        DataSet = frxDsFluxo
        DataSetName = 'frxDsFluxo'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 230.551330000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsExtr
        DataSetName = 'frxDsExtr'
        RowCount = 0
        object Memo3: TfrxMemoView
          Left = 41.574803149606300000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'Docum'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[frxDsExtr."Docum"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 136.062992130000000000
          Width = 158.740157480315000000
          Height = 13.228346460000000000
          DataField = 'Texto'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."Texto"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 102.047244094488200000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          DataField = 'NotaF'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[frxDsExtr."NotaF"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 294.803149610000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."VALOR"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 525.354391730000000000
          Width = 120.944881889763800000
          Height = 13.228346460000000000
          DataField = 'CartN'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."CartN"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'DataX'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataX"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 646.299630000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          DataField = 'Ctrle'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."Ctrle"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 347.716535430000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."Saldo"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 400.629921259842500000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'DataE'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataE"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 442.204724409448800000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'DataV'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataV"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 483.779527559055100000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'DataQ'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataQ"]')
          ParentFont = False
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 132.283550000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          Left = 41.574803150000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'S'#233'rie - Doc.')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 136.062992130000000000
          Width = 158.740157480000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[SUBST]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 102.047244094488200000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 294.803149610000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 525.354330708661400000
          Width = 120.944881889763800000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Carteira')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Dta fluxo')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 646.299630000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ID')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 347.716535433070900000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 400.629921259842500000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Emiss.')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 442.204724409448800000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Venceu')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 483.779527560000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Quita'#231#227'o')
          ParentFont = False
        end
      end
      object Band1: TfrxGroupHeader
        FillType = ftBrush
        Top = 207.874150000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsExtr."DataX"'
      end
      object Band2: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 268.346630000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          Left = 363.574830000000000000
          Width = 256.000000000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo em: [frxDsExtr."DataX"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 619.842519690000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."Saldo"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 343.937230000000000000
        Width = 680.315400000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo23: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 90.708720000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo38: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[NOMEREL]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_NOMEEMPRESA]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Top = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo:  [PERIODO]')
          ParentFont = False
        end
      end
    end
  end
  object frxFin_Relat_005_01_A06: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.635407800900000000
    ReportOptions.LastChange = 40429.473596203700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxFin_Relat_005_00GetValue
    Left = 320
    Top = 76
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsExtr
        DataSetName = 'frxDsExtr'
      end
      item
        DataSet = frxDsFluxo
        DataSetName = 'frxDsFluxo'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 230.551330000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsExtr
        DataSetName = 'frxDsExtr'
        RowCount = 0
        object Memo3: TfrxMemoView
          Left = 35.905511811023620000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'Docum'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[frxDsExtr."Docum"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 130.393700790000000000
          Width = 175.748031496063000000
          Height = 13.228346460000000000
          DataField = 'Texto'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."Texto"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 96.377952755905510000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          DataField = 'NotaF'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            '[frxDsExtr."NotaF"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 306.141732280000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."VALOR"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 519.685039370000000000
          Width = 126.614173228346500000
          Height = 13.228346460000000000
          DataField = 'CartN'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."CartN"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Width = 35.905511811023620000
          Height = 13.228346460000000000
          DataField = 'DataX'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataX"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 646.299630000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          DataField = 'Ctrle'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."Ctrle"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 359.055118110000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."Saldo"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 411.968503940000000000
          Width = 35.905511810000000000
          Height = 13.228346460000000000
          DataField = 'DataE'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataE"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 447.874015748031500000
          Width = 35.905511810000000000
          Height = 13.228346460000000000
          DataField = 'DataV'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataV"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 483.779527560000000000
          Width = 35.905511811023620000
          Height = 13.228346460000000000
          DataField = 'DataQ'
          DataSet = frxDsExtr
          DataSetName = 'frxDsExtr'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExtr."DataQ"]')
          ParentFont = False
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 132.283550000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          Left = 35.905511811023620000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'S'#233'rie - Doc.')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 130.393700790000000000
          Width = 175.748031500000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[SUBST]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 96.377952755905510000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 306.141732280000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 519.685039370000000000
          Width = 126.614173228346500000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Carteira')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Width = 35.905511810000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Dta fluxo')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 646.299630000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ID')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 359.055118110236200000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 411.968503937007900000
          Width = 35.905511810000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Emiss.')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 447.874015748031500000
          Width = 35.905511810000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Venceu')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 483.779527560000000000
          Width = 35.905511811023620000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Quita'#231#227'o')
          ParentFont = False
        end
      end
      object Band1: TfrxGroupHeader
        FillType = ftBrush
        Top = 207.874150000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsExtr."DataX"'
      end
      object Band2: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 268.346630000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          Left = 363.574830000000000000
          Width = 256.000000000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo em: [frxDsExtr."DataX"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 619.842519690000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExtr."Saldo"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 343.937230000000000000
        Width = 680.315400000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo23: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 90.708720000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo38: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[NOMEREL]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_NOMEEMPRESA]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Top = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo:  [PERIODO]')
          ParentFont = False
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 8
    Top = 32
  end
  object frxPagRec1: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.595331481500000000
    ReportOptions.LastChange = 39720.595331481500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <VFR_GRUPO1> = -1 then GH1.Visible := False else GH1.Visibl' +
        'e := True;'
      
        '  if <VFR_GRUPO1> =  0 then GH1.Condition := '#39'frxDsPagRec."NOME_' +
        'TERCEIRO"'#39';'
      
        '  if <VFR_GRUPO1> =  1 then GH1.Condition := '#39'frxDsPagRec."DataD' +
        'oc"'#39';'
      
        '  if <VFR_GRUPO1> =  2 then GH1.Condition := '#39'frxDsPagRec."Data"' +
        #39';'
      
        '  if <VFR_GRUPO1> =  3 then GH1.Condition := '#39'frxDsPagRec."Venci' +
        'mento"'#39';'
      '  //'
      
        '  if <VFR_GRUPO1> = -1 then GF1.Visible := False else GF1.Visibl' +
        'e := True;'
      ''
      ''
      
        '  if <VFR_GRUPO2> = -1 then GH2.Visible := False else GH2.Visibl' +
        'e := True;'
      
        '  if <VFR_GRUPO2> =  0 then GH2.Condition := '#39'frxDsPagRec."NOME_' +
        'TERCEIRO"'#39';'
      
        '  if <VFR_GRUPO2> =  1 then GH2.Condition := '#39'frxDsPagRec."DataD' +
        'oc"'#39';'
      
        '  if <VFR_GRUPO2> =  2 then GH2.Condition := '#39'frxDsPagRec."Data"' +
        #39';'
      
        '  if <VFR_GRUPO2> =  3 then GH2.Condition := '#39'frxDsPagRec."Venci' +
        'mento"'#39';'
      '  //'
      
        '  if <VFR_GRUPO2> = -1 then GF2.Visible := False else GF2.Visibl' +
        'e := True;'
      ''
      '  '
      'end.')
    OnGetValue = frxFin_Relat_005_02_B1GetValue
    Left = 292
    Top = 104
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPagRec
        DataSetName = 'frxDsPagRec'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 60.000000000000000000
        Top = 18.897650000000000000
        Width = 737.008350000000000000
        object Memo32: TfrxMemoView
          Left = 558.000000000000000000
          Top = 1.102350000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 5.338590000000000000
          Top = 21.102350000000000000
          Width = 732.000000000000000000
          Height = 30.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[NOMEREL]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 24.000000000000000000
        Top = 650.079160000000000000
        Width = 737.008350000000000000
        object Memo31: TfrxMemoView
          Left = 490.000000000000000000
          Top = 4.157079999999950000
          Width = 244.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        Height = 23.779530000000000000
        Top = 457.323130000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GroupFooter1OnBeforePrint'
        object Memo9: TfrxMemoView
          Left = 5.559060000000000000
          Width = 404.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [GRUPO2]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 413.559060000000000000
          Width = 74.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."VALOR">)]')
          ParentFont = False
        end
        object Line2: TfrxLineView
          Left = 5.559060000000000000
          Top = 19.999690000000000000
          Width = 724.000000000000000000
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
        end
        object Memo25: TfrxMemoView
          Left = 613.559060000000000000
          Width = 74.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."ATUALIZADO">)]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 517.559060000000000000
          Width = 74.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."PAGO_REAL">)]')
          ParentFont = False
        end
        object Line12: TfrxLineView
          Left = 5.559060000000000000
          Height = 20.000000000000000000
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftLeft]
        end
        object Line15: TfrxLineView
          Left = 729.559060000000000000
          Height = 20.000000000000000000
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftLeft]
        end
        object Line21: TfrxLineView
          Left = 733.559060000000000000
          Height = 23.779530000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line24: TfrxLineView
          Left = 1.559060000000000000
          Height = 23.779530000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        Height = 23.779530000000000000
        Top = 370.393940000000000000
        Visible = False
        Width = 737.008350000000000000
        OnBeforePrint = 'GroupHeader1OnBeforePrint'
        Condition = 'frxDsPagRec."DataDoc"'
        object Memo1: TfrxMemoView
          Left = 5.779530000000000000
          Top = 3.779530000000020000
          Width = 724.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[GRUPO2]')
          ParentFont = False
        end
        object Line5: TfrxLineView
          Left = 729.559060000000000000
          Top = 3.779530000000020000
          Height = 20.000000000000000000
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftLeft]
        end
        object Line7: TfrxLineView
          Left = 5.559060000000000000
          Top = 3.779530000000020000
          Height = 23.779530000000000000
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftLeft]
        end
        object Line3: TfrxLineView
          Left = 5.559060000000000000
          Top = 3.779530000000020000
          Width = 724.000000000000000000
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
        end
        object Line19: TfrxLineView
          Left = 733.559060000000000000
          Height = 23.779530000000000000
          OnBeforePrint = 'Line19OnBeforePrint'
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line26: TfrxLineView
          Left = 1.559060000000000000
          Height = 23.779530000000000000
          OnBeforePrint = 'Line26OnBeforePrint'
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 20.000000000000000000
        Top = 415.748300000000000000
        Width = 737.008350000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPagRec
        DataSetName = 'frxDsPagRec'
        RowCount = 0
        object Memo13: TfrxMemoView
          Left = 5.559060000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo13OnBeforePrint'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagrec."Data"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 93.559060000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo14OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagrec."Documento"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 341.559060000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo16OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagrec."NotaFiscal"]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 237.559060000000000000
          Width = 104.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo53OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsPagrec."Descricao"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 421.559060000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo54OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."VALOR"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 133.559060000000000000
          Width = 104.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo3OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsPagrec."NOMECONTA"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 49.559060000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo6OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."NOMEVENCIDO"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 381.559060000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo8OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagrec."Controle"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 623.559060000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo23OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."ATUALIZADO"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 485.559060000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo24OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."MoraDia"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 591.559060000000000000
          Width = 32.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo37OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."ATRAZODD"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 533.559060000000000000
          Width = 58.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo39OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."PAGO_REAL"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 687.559060000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo41OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."MULTA_REAL"]')
          ParentFont = False
        end
        object Line11: TfrxLineView
          Left = 5.559060000000000000
          Height = 20.000000000000000000
          OnBeforePrint = 'Line11OnBeforePrint'
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftLeft]
        end
        object Line16: TfrxLineView
          Left = 729.559060000000000000
          Height = 20.000000000000000000
          OnBeforePrint = 'Line16OnBeforePrint'
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftLeft]
        end
        object Line20: TfrxLineView
          Left = 733.559060000000000000
          Height = 20.000000000000000000
          OnBeforePrint = 'Line20OnBeforePrint'
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line25: TfrxLineView
          Left = 1.559060000000000000
          Height = 20.000000000000000000
          OnBeforePrint = 'Line25OnBeforePrint'
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 118.000000000000000000
        Top = 102.047310000000000000
        Width = 737.008350000000000000
        object Picture2: TfrxPictureView
          Left = 5.559060000000000000
          Top = 5.070809999999990000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo26: TfrxMemoView
          Left = 177.559060000000000000
          Top = 5.070809999999990000
          Width = 552.000000000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 249.559060000000000000
          Top = 29.070810000000000000
          Width = 480.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[PERIODO]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 177.559060000000000000
          Top = 29.070810000000000000
          Width = 68.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 73.559060000000000000
          Top = 77.070810000000000000
          Width = 406.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[FORNECEDOR]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 5.559060000000000000
          Top = 77.070810000000000000
          Width = 68.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TERCEIRO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 625.559060000000000000
          Top = 53.070810000000000000
          Width = 104.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[NIVEIS]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 177.559060000000000000
          Top = 53.070810000000000000
          Width = 444.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[OMITIDOS]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 549.559060000000000000
          Top = 97.070810000000000000
          Width = 178.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[REPRESENTANTE]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Left = 481.559060000000000000
          Top = 97.070810000000000000
          Width = 68.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Representante:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 309.559060000000000000
          Top = 97.070810000000000000
          Width = 170.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VENDEDOR]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 261.559060000000000000
          Top = 97.070810000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Vendedor:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Left = 73.559060000000000000
          Top = 97.070810000000000000
          Width = 186.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[CLI_INT]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          Left = 5.559060000000000000
          Top = 97.070810000000000000
          Width = 68.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Cli.Interno:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 549.559060000000000000
          Top = 77.070810000000000000
          Width = 178.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[NOME_CONTA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 481.559060000000000000
          Top = 77.070810000000000000
          Width = 68.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Conta:')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 42.000000000000000000
        Top = 585.827150000000000000
        Width = 737.008350000000000000
        object Memo20: TfrxMemoView
          Left = 409.559060000000000000
          Top = 16.850029999999900000
          Width = 76.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."VALOR">)]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 369.559060000000000000
          Top = 16.850029999999900000
          Width = 36.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 613.559060000000000000
          Top = 16.850029999999900000
          Width = 74.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."ATUALIZADO">)]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 513.559060000000000000
          Top = 16.850029999999900000
          Width = 76.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."PAGO_REAL">)]')
          ParentFont = False
        end
        object Line6: TfrxLineView
          Left = 1.559060000000000000
          Top = 4.850029999999950000
          Width = 732.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        Height = 22.976190000000000000
        Top = 241.889920000000000000
        Width = 737.008350000000000000
        object Memo29: TfrxMemoView
          Left = -2.000000000000000000
          Top = 4.976190000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 86.000000000000000000
          Top = 4.976190000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Doc.')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 230.000000000000000000
          Top = 4.976190000000000000
          Width = 104.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 334.000000000000000000
          Top = 4.976190000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 414.000000000000000000
          Top = 4.976190000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 126.000000000000000000
          Top = 4.976190000000000000
          Width = 104.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 42.000000000000000000
          Top = 4.976190000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 374.000000000000000000
          Top = 4.976190000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Controle')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 616.000000000000000000
          Top = 4.976190000000000000
          Width = 64.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Atualizado')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 478.000000000000000000
          Top = 4.976190000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Mora dd')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 584.000000000000000000
          Top = 4.976190000000000000
          Width = 32.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'DA')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 526.000000000000000000
          Top = 4.976190000000000000
          Width = 58.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 682.000000000000000000
          Top = 4.976190000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Multa')
          ParentFont = False
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        Height = 23.842300000000000000
        Top = 325.039580000000000000
        Visible = False
        Width = 737.008350000000000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = 'frxDsPagRec."NOME_TERCEIRO"'
        object Memo12: TfrxMemoView
          Left = 9.118120000000000000
          Top = 3.842300000000020000
          Width = 720.661410000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[GRUPO1]')
          ParentFont = False
        end
        object Line17: TfrxLineView
          Left = 1.559060000000000000
          Top = 3.842300000000020000
          Height = 20.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line18: TfrxLineView
          Left = 733.559060000000000000
          Top = 3.842300000000020000
          Height = 20.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line27: TfrxLineView
          Left = 1.559060000000000000
          Top = 3.842300000000020000
          Width = 732.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.204390000000000000
        Top = 502.677490000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo17: TfrxMemoView
          Left = 1.559060000000000000
          Width = 408.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [GRUPO1]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 413.559060000000000000
          Width = 74.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."VALOR">)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 613.559060000000000000
          Width = 74.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."ATUALIZADO">)]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 517.559060000000000000
          Width = 74.000000000000000000
          Height = 18.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."PAGO_REAL">)]')
          ParentFont = False
        end
        object Line13: TfrxLineView
          Left = 1.559060000000000000
          Height = 20.000000000000000000
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftLeft]
        end
        object Line14: TfrxLineView
          Left = 733.559060000000000000
          Height = 20.000000000000000000
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftLeft]
        end
        object Line22: TfrxLineView
          Left = 733.559060000000000000
          Height = 20.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line23: TfrxLineView
          Left = 1.559060000000000000
          Height = 20.000000000000000000
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line28: TfrxLineView
          Left = 1.559060000000000000
          Top = 19.000000000000100000
          Width = 732.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
    end
  end
  object frxFin_Relat_005_02_A08: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.595331481500000000
    ReportOptions.LastChange = 40871.690657615740000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Memo24OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <TIPOREL> IN [4, 5] then'
      
        '    Memo24.Text := <frxDsPagRec."MoraVal">                      ' +
        '                                   '
      '  else'
      '    Memo24.Text := <frxDsPagRec."MoraDia">;'
      'end;'
      ''
      'begin'
      
        '  if <VFR_GRUPO1> = -1 then GH1.Visible := False else GH1.Visibl' +
        'e := True;'
      
        '  if <VFR_GRUPO1> = -1 then GF1.Visible := False else GF1.Visibl' +
        'e := True;'
      
        '  if <VFR_GRUPO2> = -1 then GH2.Visible := False else GH2.Visibl' +
        'e := True;'
      
        '  if <VFR_GRUPO2> = -1 then GF2.Visible := False else GF2.Visibl' +
        'e := True;'
      '  //'
      '  GH1.Condition := <VFR_CODITION_A1>;'
      '  GH2.Condition := <VFR_CODITION_B1>;'
      '  //'
      '  case <TIPOREL> of'
      '    2, 3:  '
      '    begin'
      '      Memo71.Visible := False; //Juros'
      
        '      Memo24.Visible := False; //DB Juros                       ' +
        '                                                         '
      '      Memo70.Visible := False; //Multa'
      '      Memo41.Visible := False; //DB Multa'
      '      Memo68.Visible := False; //Dias atraso'
      
        '      Memo37.Visible := False; //DB Dias atraso                 ' +
        '                                                                ' +
        '            '
      
        '      Memo67.Visible := True; //Atualizado                      ' +
        '                                                          '
      
        '      Memo23.Visible := True; //DB Atualizado                   ' +
        '                                                             '
      
        '      Memo25.Visible := True; //Atualizado Total 1              ' +
        '                                                                ' +
        '                    '
      
        '      Memo28.Visible := True; //Atualizado Total 2              ' +
        '                                                                ' +
        '                       '
      
        '      Memo40.Visible := True; //Atualizado Total 3              ' +
        '                                                                ' +
        '                       '
      '      //'
      
        '      Memo65.Text := '#39'Emiss.'#39'; //Emiss.                         ' +
        '                                      '
      '      Memo71.Text := '#39'$ Juros'#39'; //Juros'
      '      Memo67.Text := '#39'Saldo'#39'; //Atualizado'
      '      //'
      '      Memo69.Left := 474.34; //Pago                          '
      '      Memo39.Left := 474.34; //DB Pago'
      
        '      Memo44.Left := 474.34; //Pago Total 1                     ' +
        '                                                  '
      '      Memo45.Left := 474.34; //Pago Total 2                    '
      
        '      Memo47.Left := 474.34; //Pago Total 3                     ' +
        '                   '
      
        '      Memo67.Left := 521.58; //Atualizado                       ' +
        '                                                         '
      
        '      Memo23.Left := 521.58; //DB Atualizado                    ' +
        '                                                            '
      
        '      Memo25.Left := 521.58; //Atualizado Total 1               ' +
        '                                                                ' +
        '                   '
      
        '      Memo28.Left := 521.58; //Atualizado Total 2               ' +
        '                                                                ' +
        '                      '
      
        '      Memo40.Left := 521.58; //Atualizado Total 3               ' +
        '                                                                ' +
        '                      '
      '      //'
      '      Memo22.Visible := False; //Total Juros 1'
      '      Memo29.Visible := False; //Total Multa 1'
      '      Memo30.Visible := False; //Total Juros 2'
      '      Memo34.Visible := False; //Total Multa 2'
      '      Memo33.Visible := False; //Total Juros 3'
      
        '      Memo35.Visible := False; //Total Multa 3                  ' +
        '                                                                ' +
        '              '
      '    end;'
      '    4, 5:'
      '    begin'
      '      Memo71.Visible := True; //Juros'
      
        '      Memo24.Visible := True; //DB Juros                        ' +
        '                                                        '
      '      Memo70.Visible := True; //Multa'
      '      Memo41.Visible := True; //DB Multa'
      '      Memo68.Visible := False; //Dias atraso'
      
        '      Memo37.Visible := False; //DB Dias atraso                 ' +
        '                                                                ' +
        '            '
      
        '      Memo67.Visible := False; //Atualizado                     ' +
        '                                                           '
      
        '      Memo23.Visible := False; //DB Atualizado                  ' +
        '                                                              '
      
        '      Memo25.Visible := False; //Atualizado Total 1             ' +
        '                                                                ' +
        '                     '
      
        '      Memo28.Visible := False; //Atualizado Total 2             ' +
        '                                                                ' +
        '                        '
      
        '      Memo40.Visible := False; //Atualizado Total 3             ' +
        '                                                                ' +
        '                        '
      '      //'
      
        '      Memo65.Text := '#39'Pagto.'#39'; //Emiss.                         ' +
        '                                      '
      '      Memo71.Text := '#39'$ Juros'#39'; //Juros'
      
        '      Memo67.Text := '#39'Atualizado'#39'; //Atualizado                 ' +
        '                                                         '
      '      //'
      '      Memo69.Left := 563.15; //Pago'
      '      Memo39.Left := 563.15; //DB Pago'
      
        '      Memo44.Left := 563.15; //Pago Total 1                     ' +
        '                                                  '
      '      Memo45.Left := 563.15; //Pago Total 2                    '
      '      Memo47.Left := 563.15; //Pago Total 3'
      '      //'
      '      Memo22.Visible := True; //Total Juros 1'
      '      Memo29.Visible := True; //Total Multa 1'
      '      Memo30.Visible := True; //Total Juros 2'
      '      Memo34.Visible := True; //Total Multa 2'
      '      Memo33.Visible := True; //Total Juros 3'
      
        '      Memo35.Visible := True; //Total Multa 3                   ' +
        '                                                                ' +
        '             '
      '    end;'
      '    else              '
      '    begin'
      '      Memo71.Visible := True; //Juros'
      
        '      Memo24.Visible := True; //DB Juros                        ' +
        '                                                        '
      '      Memo70.Visible := True; //Multa'
      '      Memo41.Visible := True; //DB Multa'
      '      Memo68.Visible := True; //Dias atraso'
      
        '      Memo37.Visible := True; //DB Dias atraso                  ' +
        '                                                                ' +
        '           '
      
        '      Memo67.Visible := True; //Atualizado                      ' +
        '                                                          '
      
        '      Memo23.Visible := True; //DB Atualizado                   ' +
        '                                                             '
      
        '      Memo25.Visible := True; //Atualizado Total 1              ' +
        '                                                                ' +
        '                    '
      
        '      Memo28.Visible := True; //Atualizado Total 2              ' +
        '                                                                ' +
        '                       '
      
        '      Memo40.Visible := True; //Atualizado Total 3              ' +
        '                                                                ' +
        '                       '
      '      //'
      
        '      Memo65.Text := '#39'Emiss.'#39'; //Emiss.                         ' +
        '                                      '
      '      Memo71.Text := '#39'% Mora'#39'; //Juros'
      
        '      Memo67.Text := '#39'Atualizado'#39'; //Atualizado                 ' +
        '                                                         '
      '      //'
      '      Memo69.Left := 563.15; //Pago'
      '      Memo39.Left := 563.15; //DB Pago'
      
        '      Memo44.Left := 563.15; //Pago Total 1                     ' +
        '                                                  '
      '      Memo45.Left := 563.15; //Pago Total 2                    '
      '      Memo47.Left := 563.15; //Pago Total 3'
      
        '      Memo67.Left := 633.07; //Atualizado                       ' +
        '                                                         '
      
        '      Memo23.Left := 633.07; //DB Atualizado                    ' +
        '                                                            '
      
        '      Memo25.Left := 633.07; //Atualizado Total 1               ' +
        '                                                                ' +
        '                   '
      
        '      Memo28.Left := 633.07; //Atualizado Total 2               ' +
        '                                                                ' +
        '                      '
      
        '      Memo40.Left := 633.07; //Atualizado Total 3               ' +
        '                                                                ' +
        '                      '
      '      //'
      '      Memo22.Visible := False; //Total Juros 1'
      '      Memo29.Visible := False; //Total Multa 1'
      '      Memo30.Visible := False; //Total Juros 2'
      '      Memo34.Visible := False; //Total Multa 2'
      '      Memo33.Visible := False; //Total Juros 3'
      
        '      Memo35.Visible := False; //Total Multa 3                  ' +
        '                                                                ' +
        '              '
      '    end;'
      '  end;                                                '
      'end.')
    OnGetValue = frxFin_Relat_005_02_B1GetValue
    Left = 376
    Top = 104
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPagRec
        DataSetName = 'frxDsPagRec'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 124.724409450000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo59: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo60: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[NOMEREL]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_NOMEEMPRESA]')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Top = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo:  [PERIODO]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 68.031496060000000000
          Top = 90.708661417322830000
          Width = 612.283464570000000000
          Height = 17.007874015748030000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[FORNECEDOR]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Top = 90.708661420000000000
          Width = 68.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TERCEIRO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 574.488188976378000000
          Top = 37.952690000000000000
          Width = 105.826771653543300000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[NIVEIS]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Top = 37.952690000000000000
          Width = 574.488120630000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[OMITIDOS]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 291.023622050000000000
          Top = 107.716535430000000000
          Width = 173.858267720000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[REPRESENTANTE]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Left = 222.992125980000000000
          Top = 107.716535430000000000
          Width = 68.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Representante:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 49.133858270000000000
          Top = 107.716535430000000000
          Width = 170.078737720000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VENDEDOR]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 0.771490000000000000
          Top = 107.716535430000000000
          Width = 49.133858270000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Vendedor:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 502.677165350000000000
          Top = 107.716535430000000000
          Width = 177.637797720000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[NOME_CONTA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 468.661417320000000000
          Top = 107.716535430000000000
          Width = 33.984230000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Conta:')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 355.275820000000000000
        Width = 680.315400000000000000
        object Memo9: TfrxMemoView
          Left = 50.913420000000000000
          Width = 366.204700000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [GRUPO2] ')
          ParentFont = False
          WordWrap = False
        end
        object Memo2: TfrxMemoView
          Left = 427.086594650000000000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."VALOR">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 633.070866141732400000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."ATUALIZADO">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo44: TfrxMemoView
          Left = 563.149606300000000000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."PAGO_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 474.330708661417300000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."MoraVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          Left = 515.905511811023600000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."MULTA_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 279.685220000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsPagRec."DataDoc"'
        object Memo1: TfrxMemoView
          Width = 674.866110000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[GRUPO2]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 317.480520000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPagRec
        DataSetName = 'frxDsPagRec'
        RowCount = 0
        object Memo13: TfrxMemoView
          Width = 47.244094488188980000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagrec."Data"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 94.488206060000000000
          Width = 60.472455590000000000
          Height = 13.228346460000000000
          DataField = 'SERIEDOC'
          DataSet = frxDsPagRec
          DataSetName = 'frxDsPagRec'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."SERIEDOC"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 359.055142520000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagrec."NotaFiscal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo53: TfrxMemoView
          Left = 253.228375750000000000
          Width = 105.826766770000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPagrec."Descricao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          Left = 427.086638580000000000
          Width = 47.244094488188980000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."VALOR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 200.315021650000000000
          Width = 52.913354090000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPagrec."NOMECONTA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 47.244094490000000000
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."NOMEVENCIDO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 393.070890550000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagrec."Controle"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 633.070866140000000000
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."ATUALIZADO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          Left = 474.330708660000000000
          Width = 41.574812910000000000
          Height = 13.228346460000000000
          OnBeforePrint = 'Memo24OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."MoraDia"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Left = 610.393700787401600000
          Width = 22.677165350000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."ATRAZODD"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          Left = 563.149606300000000000
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."PAGO_REAL"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          Left = 515.905511810000000000
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."MULTA_REAL"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 154.960730000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'NO_UH'
          DataSet = frxDsPagRec
          DataSetName = 'frxDsPagRec'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPagRec."NO_UH"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 20.629550240000000000
        Top = 468.661720000000000000
        Width = 680.315400000000000000
        object Memo20: TfrxMemoView
          Left = 427.086594650000000000
          Top = 5.511440000000000000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."VALOR">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 378.897650000000000000
          Top = 5.511440000000000000
          Width = 36.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          Left = 633.070866140000000000
          Top = 5.511440000000000000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."ATUALIZADO">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          Left = 563.149606299212600000
          Top = 5.511440000000000000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."PAGO_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          Left = 474.330708661417300000
          Top = 5.669291340000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."MoraVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          Left = 515.905511811023600000
          Top = 5.669291340000000000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."MULTA_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 166.299320000000000000
        Width = 680.315400000000000000
        object Memo26: TfrxMemoView
          Left = 47.244094488188980000
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Venceu')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 94.488228030000000000
          Width = 60.472455590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'S'#233'rie - Doc.')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 253.228353780000000000
          Width = 105.826766770000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 359.054986300000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 427.086492130000000000
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 200.315043620000000000
          Width = 52.913354090000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Emiss.')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 393.071120000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'ID')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 633.118464170000000000
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Atualizado')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          Left = 610.393700787401600000
          Width = 22.677165350000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'DA')
          ParentFont = False
          WordWrap = False
        end
        object Memo69: TfrxMemoView
          Left = 563.149606299212600000
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
          WordWrap = False
        end
        object Memo70: TfrxMemoView
          Left = 515.905511810000000000
          Width = 47.244094490000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Multa')
          ParentFont = False
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          Left = 474.330708660000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Mora')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 154.960751970000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VAR_UH]')
          ParentFont = False
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        Height = 16.283240000000000000
        Top = 241.889920000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsPagRec."NOME_TERCEIRO"'
        object Memo12: TfrxMemoView
          Top = 0.062770000000000010
          Width = 675.307050000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[GRUPO1]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 393.071120000000000000
        Width = 680.315400000000000000
        object Memo17: TfrxMemoView
          Left = 46.913420000000000000
          Width = 370.204700000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [GRUPO1] ')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 427.086594650000000000
          Width = 47.244094488188980000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."VALOR">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          Left = 633.070866140000000000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."ATUALIZADO">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo45: TfrxMemoView
          Left = 563.149606299212600000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."PAGO_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          Left = 474.330708661417300000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."MoraVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          Left = 515.905511811023600000
          Width = 47.244094490000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."MULTA_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 514.016080000000000000
        Width = 680.315400000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo4: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object frxFin_Relat_005_02_A07: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.595331481500000000
    ReportOptions.LastChange = 40871.688885856480000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Memo24OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <TIPOREL> IN [4, 5] then'
      
        '    Memo24.Text := <frxDsPagRec."MoraVal">                      ' +
        '                                   '
      '  else'
      '    Memo24.Text := <frxDsPagRec."MoraDia">;'
      'end;'
      ''
      'begin'
      
        '  if <VFR_GRUPO1> = -1 then GH1.Visible := False else GH1.Visibl' +
        'e := True;'
      
        '  if <VFR_GRUPO1> = -1 then GF1.Visible := False else GF1.Visibl' +
        'e := True;'
      
        '  if <VFR_GRUPO2> = -1 then GH2.Visible := False else GH2.Visibl' +
        'e := True;'
      
        '  if <VFR_GRUPO2> = -1 then GF2.Visible := False else GF2.Visibl' +
        'e := True;'
      '  //'
      '  GH1.Condition := <VFR_CODITION_A1>;'
      '  GH2.Condition := <VFR_CODITION_B1>;'
      '  //'
      '  case <TIPOREL> of'
      '    2, 3:  '
      '    begin'
      '      Memo71.Visible := False; //Juros'
      
        '      Memo24.Visible := False; //DB Juros                       ' +
        '                                                         '
      '      Memo70.Visible := False; //Multa'
      '      Memo41.Visible := False; //DB Multa'
      '      Memo68.Visible := False; //Dias atraso'
      
        '      Memo37.Visible := False; //DB Dias atraso                 ' +
        '                                                                ' +
        '            '
      
        '      Memo67.Visible := True; //Atualizado                      ' +
        '                                                          '
      
        '      Memo23.Visible := True; //DB Atualizado                   ' +
        '                                                             '
      
        '      Memo25.Visible := True; //Atualizado Total 1              ' +
        '                                                                ' +
        '                    '
      
        '      Memo28.Visible := True; //Atualizado Total 2              ' +
        '                                                                ' +
        '                       '
      
        '      Memo40.Visible := True; //Atualizado Total 3              ' +
        '                                                                ' +
        '                       '
      '      //'
      
        '      Memo65.Text := '#39'Emiss.'#39'; //Emiss.                         ' +
        '                                      '
      '      Memo71.Text := '#39'$ Juros'#39'; //Juros'
      '      Memo67.Text := '#39'Saldo'#39'; //Atualizado'
      '      //'
      '      Memo69.Left := 497.00; //Pago                          '
      '      Memo39.Left := 497.00; //DB Pago'
      
        '      Memo44.Left := 497.00; //Pago Total 1                     ' +
        '                                                  '
      '      Memo45.Left := 497.00; //Pago Total 2                    '
      
        '      Memo47.Left := 497.00; //Pago Total 3                     ' +
        '                   '
      
        '      Memo67.Left := 542.35; //Atualizado                       ' +
        '                                                         '
      
        '      Memo23.Left := 542.35; //DB Atualizado                    ' +
        '                                                            '
      
        '      Memo25.Left := 542.35; //Atualizado Total 1               ' +
        '                                                                ' +
        '                   '
      
        '      Memo28.Left := 542.35; //Atualizado Total 2               ' +
        '                                                                ' +
        '                      '
      
        '      Memo40.Left := 542.35; //Atualizado Total 3               ' +
        '                                                                ' +
        '                      '
      '      //'
      '      Memo22.Visible := False; //Total Juros 1'
      '      Memo29.Visible := False; //Total Multa 1'
      '      Memo30.Visible := False; //Total Juros 2'
      '      Memo34.Visible := False; //Total Multa 2'
      '      Memo33.Visible := False; //Total Juros 3'
      
        '      Memo35.Visible := False; //Total Multa 3                  ' +
        '                                                                ' +
        '              '
      '    end;'
      '    4, 5:'
      '    begin'
      '      Memo71.Visible := True; //Juros'
      
        '      Memo24.Visible := True; //DB Juros                        ' +
        '                                                        '
      '      Memo70.Visible := True; //Multa'
      '      Memo41.Visible := True; //DB Multa'
      '      Memo68.Visible := False; //Dias atraso'
      
        '      Memo37.Visible := False; //DB Dias atraso                 ' +
        '                                                                ' +
        '            '
      
        '      Memo67.Visible := False; //Atualizado                     ' +
        '                                                           '
      
        '      Memo23.Visible := False; //DB Atualizado                  ' +
        '                                                              '
      
        '      Memo25.Visible := False; //Atualizado Total 1             ' +
        '                                                                ' +
        '                     '
      
        '      Memo28.Visible := False; //Atualizado Total 2             ' +
        '                                                                ' +
        '                        '
      
        '      Memo40.Visible := False; //Atualizado Total 3             ' +
        '                                                                ' +
        '                        '
      '      //'
      
        '      Memo65.Text := '#39'Pagto.'#39'; //Emiss.                         ' +
        '                                      '
      '      Memo71.Text := '#39'$ Juros'#39'; //Juros'
      
        '      Memo67.Text := '#39'Atualizado'#39'; //Atualizado                 ' +
        '                                                         '
      '      //'
      '      Memo69.Left := 568.82; //Pago'
      '      Memo39.Left := 568.82; //DB Pago'
      
        '      Memo44.Left := 568.82; //Pago Total 1                     ' +
        '                                                  '
      '      Memo45.Left := 568.82; //Pago Total 2                    '
      '      Memo47.Left := 568.82; //Pago Total 3'
      '      //'
      '      Memo22.Visible := True; //Total Juros 1'
      '      Memo29.Visible := True; //Total Multa 1'
      '      Memo30.Visible := True; //Total Juros 2'
      '      Memo34.Visible := True; //Total Multa 2'
      '      Memo33.Visible := True; //Total Juros 3'
      
        '      Memo35.Visible := True; //Total Multa 3                   ' +
        '                                                                ' +
        '             '
      '    end;'
      '    else              '
      '    begin'
      '      Memo71.Visible := True; //Juros'
      
        '      Memo24.Visible := True; //DB Juros                        ' +
        '                                                        '
      '      Memo70.Visible := True; //Multa'
      '      Memo41.Visible := True; //DB Multa'
      '      Memo68.Visible := True; //Dias atraso'
      
        '      Memo37.Visible := True; //DB Dias atraso                  ' +
        '                                                                ' +
        '           '
      
        '      Memo67.Visible := True; //Atualizado                      ' +
        '                                                          '
      
        '      Memo23.Visible := True; //DB Atualizado                   ' +
        '                                                             '
      
        '      Memo25.Visible := True; //Atualizado Total 1              ' +
        '                                                                ' +
        '                    '
      
        '      Memo28.Visible := True; //Atualizado Total 2              ' +
        '                                                                ' +
        '                       '
      
        '      Memo40.Visible := True; //Atualizado Total 3              ' +
        '                                                                ' +
        '                       '
      '      //'
      
        '      Memo65.Text := '#39'Emiss.'#39'; //Emiss.                         ' +
        '                                      '
      '      Memo71.Text := '#39'% Mora'#39'; //Juros'
      
        '      Memo67.Text := '#39'Atualizado'#39'; //Atualizado                 ' +
        '                                                         '
      '      //'
      '      Memo69.Left := 568.82; //Pago'
      '      Memo39.Left := 568.82; //DB Pago'
      
        '      Memo44.Left := 568.82; //Pago Total 1                     ' +
        '                                                  '
      '      Memo45.Left := 568.82; //Pago Total 2                    '
      '      Memo47.Left := 568.82; //Pago Total 3'
      
        '      Memo67.Left := 631.12; //Atualizado                       ' +
        '                                                         '
      
        '      Memo23.Left := 631.12; //DB Atualizado                    ' +
        '                                                            '
      
        '      Memo25.Left := 631.12; //Atualizado Total 1               ' +
        '                                                                ' +
        '                   '
      
        '      Memo28.Left := 631.12; //Atualizado Total 2               ' +
        '                                                                ' +
        '                      '
      
        '      Memo40.Left := 631.12; //Atualizado Total 3               ' +
        '                                                                ' +
        '                      '
      '      //'
      '      Memo22.Visible := False; //Total Juros 1'
      '      Memo29.Visible := False; //Total Multa 1'
      '      Memo30.Visible := False; //Total Juros 2'
      '      Memo34.Visible := False; //Total Multa 2'
      '      Memo33.Visible := False; //Total Juros 3'
      
        '      Memo35.Visible := False; //Total Multa 3                  ' +
        '                                                                ' +
        '              '
      '    end;'
      '  end;                                                '
      'end.')
    OnGetValue = frxFin_Relat_005_02_B1GetValue
    Left = 348
    Top = 104
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPagRec
        DataSetName = 'frxDsPagRec'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 124.724409450000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo59: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo60: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[NOMEREL]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_NOMEEMPRESA]')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Top = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo:  [PERIODO]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 68.031496060000000000
          Top = 90.708661417322830000
          Width = 612.283464570000000000
          Height = 17.007874015748030000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[FORNECEDOR]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Top = 90.708661420000000000
          Width = 68.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TERCEIRO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 574.488188976378000000
          Top = 37.952690000000000000
          Width = 105.826771653543300000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[NIVEIS]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Top = 37.952690000000000000
          Width = 574.488120630000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[OMITIDOS]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 291.023622050000000000
          Top = 107.716535430000000000
          Width = 173.858267720000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[REPRESENTANTE]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Left = 222.992125980000000000
          Top = 107.716535430000000000
          Width = 68.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Representante:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 49.133858270000000000
          Top = 107.716535430000000000
          Width = 170.078737720000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VENDEDOR]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 0.771490000000000000
          Top = 107.716535430000000000
          Width = 49.133858270000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Vendedor:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 502.677165350000000000
          Top = 107.716535430000000000
          Width = 177.637797720000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[NOME_CONTA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 468.661417320000000000
          Top = 107.716535430000000000
          Width = 33.984230000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Conta:')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 355.275820000000000000
        Width = 680.315400000000000000
        object Memo9: TfrxMemoView
          Width = 404.000000000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [GRUPO2]')
          ParentFont = False
          WordWrap = False
        end
        object Memo2: TfrxMemoView
          Left = 451.653540870000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."VALOR">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 631.181119450000000000
          Width = 49.133860710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."ATUALIZADO">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo44: TfrxMemoView
          Left = 568.818919610000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."PAGO_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 497.007874020000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."MoraVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          Left = 531.023622050000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."MULTA_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 279.685220000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsPagRec."DataDoc"'
        object Memo1: TfrxMemoView
          Width = 674.866110000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[GRUPO2]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 317.480520000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPagRec
        DataSetName = 'frxDsPagRec'
        RowCount = 0
        object Memo13: TfrxMemoView
          Width = 41.574803149606300000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagrec."Data"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 83.149616060000000000
          Width = 54.803149606299210000
          Height = 13.228346460000000000
          DataField = 'SERIEDOC'
          DataSet = frxDsPagRec
          DataSetName = 'frxDsPagRec'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."SERIEDOC"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 383.622044800000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagrec."NotaFiscal"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo53: TfrxMemoView
          Left = 270.236218030000000000
          Width = 113.385826770000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPagrec."Descricao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          Left = 451.653540870000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."VALOR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 183.307086614173200000
          Width = 86.929124090000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPagrec."NOMECONTA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 41.574808030000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."NOMEVENCIDO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 417.637792830000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagrec."Controle"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 631.118120000000000000
          Width = 49.133860710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."ATUALIZADO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          Left = 497.007871570000000000
          Width = 34.015752910000000000
          Height = 13.228346460000000000
          OnBeforePrint = 'Memo24OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."MoraDia"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Left = 614.173228346456700000
          Width = 17.007874015748030000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."ATRAZODD"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          Left = 568.818924490000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."PAGO_REAL"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          Left = 531.023622050000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."MULTA_REAL"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 137.952755905511800000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'NO_UH'
          DataSet = frxDsPagRec
          DataSetName = 'frxDsPagRec'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPagRec."NO_UH"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 18.739786460000000000
        Top = 468.661720000000000000
        Width = 680.315400000000000000
        object Memo20: TfrxMemoView
          Left = 451.653543307086600000
          Top = 5.511440000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."VALOR">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 365.779530000000000000
          Top = 5.511440000000000000
          Width = 36.000000000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          Left = 631.181102362204700000
          Top = 5.511440000000000000
          Width = 49.133860710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."ATUALIZADO">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          Left = 568.818919610000000000
          Top = 5.511440000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."PAGO_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          Left = 497.007874020000000000
          Top = 5.779530000000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."MoraVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          Left = 531.023622050000000000
          Top = 5.669291340000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."MULTA_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 166.299320000000000000
        Width = 680.315400000000000000
        object Memo26: TfrxMemoView
          Left = 41.574830000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Venceu')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 83.149638030000000000
          Width = 54.803149610000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'S'#233'rie - Doc.')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 270.236218030000000000
          Width = 113.385826770000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 383.622044800000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 451.653540870000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 183.417393620000000000
          Width = 86.929124090000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Emiss.')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 417.637792830000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'ID')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 631.181102360000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Atualizado')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          Left = 614.173250310000000000
          Width = 17.007874020000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'DA')
          ParentFont = False
          WordWrap = False
        end
        object Memo69: TfrxMemoView
          Left = 568.818919610000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
          WordWrap = False
        end
        object Memo70: TfrxMemoView
          Left = 531.023622047244100000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Multa')
          ParentFont = False
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          Left = 497.007871570000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '% Mora')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 138.063101970000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VAR_UH]')
          ParentFont = False
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        Height = 16.283240000000000000
        Top = 241.889920000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsPagRec."NOME_TERCEIRO"'
        object Memo12: TfrxMemoView
          Top = 0.062770000000000010
          Width = 675.307050000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[GRUPO1]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 393.071120000000000000
        Width = 680.315400000000000000
        object Memo17: TfrxMemoView
          Width = 404.220470000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [GRUPO1]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 451.653540870000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."VALOR">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          Left = 631.181119450000000000
          Width = 49.133860710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."ATUALIZADO">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo45: TfrxMemoView
          Left = 568.818919610000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."PAGO_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          Left = 497.007874020000000000
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."MoraVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          Left = 531.023622050000000000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."MULTA_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 510.236550000000000000
        Width = 680.315400000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo4: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object frxFin_Relat_005_02_A06: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.595331481500000000
    ReportOptions.LastChange = 40625.703637905100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Memo24OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <TIPOREL> IN [4, 5] then'
      
        '    Memo24.Text := <frxDsPagRec."MoraVal">                      ' +
        '                                   '
      '  else'
      '    Memo24.Text := <frxDsPagRec."MoraDia">;'
      'end;'
      ''
      'begin'
      
        '  if <VFR_GRUPO1> = -1 then GH1.Visible := False else GH1.Visibl' +
        'e := True;'
      
        '  if <VFR_GRUPO1> = -1 then GF1.Visible := False else GF1.Visibl' +
        'e := True;'
      
        '  if <VFR_GRUPO2> = -1 then GH2.Visible := False else GH2.Visibl' +
        'e := True;'
      
        '  if <VFR_GRUPO2> = -1 then GF2.Visible := False else GF2.Visibl' +
        'e := True;'
      '  //'
      '  GH1.Condition := <VFR_CODITION_A1>;'
      '  GH2.Condition := <VFR_CODITION_B1>;'
      '  //'
      '  case <TIPOREL> of'
      '    2, 3:  '
      '    begin'
      '      Memo71.Visible := False; //Juros'
      
        '      Memo24.Visible := False; //DB Juros                       ' +
        '                                                         '
      '      Memo70.Visible := False; //Multa'
      '      Memo41.Visible := False; //DB Multa'
      '      Memo68.Visible := False; //Dias atraso'
      
        '      Memo37.Visible := False; //DB Dias atraso                 ' +
        '                                                                ' +
        '            '
      
        '      Memo67.Visible := True; //Atualizado                      ' +
        '                                                          '
      
        '      Memo23.Visible := True; //DB Atualizado                   ' +
        '                                                             '
      
        '      Memo25.Visible := True; //Atualizado Total 1              ' +
        '                                                                ' +
        '                    '
      
        '      Memo28.Visible := True; //Atualizado Total 2              ' +
        '                                                                ' +
        '                       '
      
        '      Memo40.Visible := True; //Atualizado Total 3              ' +
        '                                                                ' +
        '                       '
      '      //'
      
        '      Memo65.Text := '#39'Emiss.'#39'; //Emiss.                         ' +
        '                                      '
      '      Memo71.Text := '#39'$ Juros'#39'; //Juros'
      '      Memo67.Text := '#39'Saldo'#39'; //Atualizado'
      '      //'
      '      Memo69.Left := 498.89;//Pago                          '
      '      Memo39.Left := 498.89; //DB Pago'
      
        '      Memo44.Left := 498.89; //Pago Total 1                     ' +
        '                                                  '
      '      Memo45.Left := 498.89; //Pago Total 2                    '
      
        '      Memo47.Left := 498.89; //Pago Total 3                     ' +
        '                   '
      
        '      Memo67.Left := 548.02; //Atualizado                       ' +
        '                                                         '
      
        '      Memo23.Left := 548.02; //DB Atualizado                    ' +
        '                                                            '
      
        '      Memo25.Left := 548.02; //Atualizado Total 1               ' +
        '                                                                ' +
        '                   '
      
        '      Memo28.Left := 548.02; //Atualizado Total 2               ' +
        '                                                                ' +
        '                      '
      
        '      Memo40.Left := 548.02; //Atualizado Total 3               ' +
        '                                                                ' +
        '                      '
      '      //'
      '      Memo22.Visible := False; //Total Juros 1'
      '      Memo29.Visible := False; //Total Multa 1'
      '      Memo30.Visible := False; //Total Juros 2'
      '      Memo34.Visible := False; //Total Multa 2'
      '      Memo33.Visible := False; //Total Juros 3'
      
        '      Memo35.Visible := False; //Total Multa 3                  ' +
        '                                                                ' +
        '              '
      '    end;'
      '    4, 5:'
      '    begin'
      '      Memo71.Visible := True; //Juros'
      
        '      Memo24.Visible := True; //DB Juros                        ' +
        '                                                        '
      '      Memo70.Visible := True; //Multa'
      '      Memo41.Visible := True; //DB Multa'
      '      Memo68.Visible := False; //Dias atraso'
      
        '      Memo37.Visible := False; //DB Dias atraso                 ' +
        '                                                                ' +
        '            '
      
        '      Memo67.Visible := False; //Atualizado                     ' +
        '                                                           '
      
        '      Memo23.Visible := False; //DB Atualizado                  ' +
        '                                                              '
      
        '      Memo25.Visible := False; //Atualizado Total 1             ' +
        '                                                                ' +
        '                     '
      
        '      Memo28.Visible := False; //Atualizado Total 2             ' +
        '                                                                ' +
        '                        '
      
        '      Memo40.Visible := False; //Atualizado Total 3             ' +
        '                                                                ' +
        '                        '
      '      //'
      
        '      Memo65.Text := '#39'Pagto.'#39'; //Emiss.                         ' +
        '                                      '
      '      Memo71.Text := '#39'$ Juros'#39'; //Juros'
      
        '      Memo67.Text := '#39'Atualizado'#39'; //Atualizado                 ' +
        '                                                         '
      '      //'
      '      Memo69.Left := 563.15; //Pago'
      '      Memo39.Left := 563.15; //DB Pago'
      
        '      Memo44.Left := 563.15; //Pago Total 1                     ' +
        '                                                  '
      '      Memo45.Left := 563.15; //Pago Total 2                    '
      '      Memo47.Left := 563.15; //Pago Total 3'
      '      //'
      '      Memo22.Visible := True; //Total Juros 1'
      '      Memo29.Visible := True; //Total Multa 1'
      '      Memo30.Visible := True; //Total Juros 2'
      '      Memo34.Visible := True; //Total Multa 2'
      '      Memo33.Visible := True; //Total Juros 3'
      
        '      Memo35.Visible := True; //Total Multa 3                   ' +
        '                                                                ' +
        '             '
      '    end;'
      '    else              '
      '    begin'
      '      Memo71.Visible := True; //Juros'
      
        '      Memo24.Visible := True; //DB Juros                        ' +
        '                                                        '
      '      Memo70.Visible := True; //Multa'
      '      Memo41.Visible := True; //DB Multa'
      '      Memo68.Visible := True; //Dias atraso'
      
        '      Memo37.Visible := True; //DB Dias atraso                  ' +
        '                                                                ' +
        '           '
      
        '      Memo67.Visible := True; //Atualizado                      ' +
        '                                                          '
      
        '      Memo23.Visible := True; //DB Atualizado                   ' +
        '                                                             '
      
        '      Memo25.Visible := True; //Atualizado Total 1              ' +
        '                                                                ' +
        '                    '
      
        '      Memo28.Visible := True; //Atualizado Total 2              ' +
        '                                                                ' +
        '                       '
      
        '      Memo40.Visible := True; //Atualizado Total 3              ' +
        '                                                                ' +
        '                       '
      '      //'
      
        '      Memo65.Text := '#39'Emiss.'#39'; //Emiss.                         ' +
        '                                      '
      '      Memo71.Text := '#39'% Mora'#39'; //Juros'
      
        '      Memo67.Text := '#39'Atualizado'#39'; //Atualizado                 ' +
        '                                                         '
      '      //'
      '      Memo69.Left := 563.15; //Pago'
      '      Memo39.Left := 563.15; //DB Pago'
      
        '      Memo44.Left := 563.15; //Pago Total 1                     ' +
        '                                                  '
      '      Memo45.Left := 563.15; //Pago Total 2                    '
      '      Memo47.Left := 563.15; //Pago Total 3'
      
        '      Memo67.Left := 631.12; //Atualizado                       ' +
        '                                                         '
      
        '      Memo23.Left := 631.12; //DB Atualizado                    ' +
        '                                                            '
      
        '      Memo25.Left := 631.12; //Atualizado Total 1               ' +
        '                                                                ' +
        '                   '
      
        '      Memo28.Left := 631.12; //Atualizado Total 2               ' +
        '                                                                ' +
        '                      '
      
        '      Memo40.Left := 631.12; //Atualizado Total 3               ' +
        '                                                                ' +
        '                      '
      '      //'
      '      Memo22.Visible := False; //Total Juros 1'
      '      Memo29.Visible := False; //Total Multa 1'
      '      Memo30.Visible := False; //Total Juros 2'
      '      Memo34.Visible := False; //Total Multa 2'
      '      Memo33.Visible := False; //Total Juros 3'
      
        '      Memo35.Visible := False; //Total Multa 3                  ' +
        '                                                                ' +
        '              '
      '    end;'
      '  end;                                                '
      'end.')
    OnGetValue = frxFin_Relat_005_02_B1GetValue
    Left = 320
    Top = 104
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPagRec
        DataSetName = 'frxDsPagRec'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 124.724409450000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo59: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo60: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[NOMEREL]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_NOMEEMPRESA]')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Top = 75.590600000000000000
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo:  [PERIODO]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 68.031496060000000000
          Top = 90.708661417322800000
          Width = 612.283464570000000000
          Height = 17.007874015748000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[FORNECEDOR]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Top = 90.708661420000000000
          Width = 68.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TERCEIRO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 574.488188976378000000
          Top = 37.952690000000000000
          Width = 105.826771653543000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[NIVEIS]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Top = 37.952690000000000000
          Width = 574.488120630000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[OMITIDOS]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 291.023622050000000000
          Top = 107.716535430000000000
          Width = 173.858267720000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[REPRESENTANTE]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Left = 222.992125980000000000
          Top = 107.716535430000000000
          Width = 68.000000000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Representante:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 49.133858270000000000
          Top = 107.716535430000000000
          Width = 170.078737720000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[VENDEDOR]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 0.771490000000000000
          Top = 107.716535430000000000
          Width = 49.133858270000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Vendedor:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 502.677165350000000000
          Top = 107.716535430000000000
          Width = 177.637797720000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[NOME_CONTA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 468.661417320000000000
          Top = 107.716535430000000000
          Width = 33.984230000000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Conta:')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 355.275820000000000000
        Width = 680.315400000000000000
        object Memo9: TfrxMemoView
          Left = 3.779530000000000000
          Width = 445.984251970000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [GRUPO2]')
          ParentFont = False
          WordWrap = False
        end
        object Memo2: TfrxMemoView
          Left = 449.763774650000000000
          Width = 49.133860710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."VALOR">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 631.181124330000000000
          Width = 49.133860710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."ATUALIZADO">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo44: TfrxMemoView
          Left = 563.149628270000000000
          Width = 49.133860710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."PAGO_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 498.897960000000000000
          Width = 30.236210710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."MoraVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          Left = 529.134200000000000000
          Width = 34.015740710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."MULTA_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 279.685220000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsPagRec."DataDoc"'
        object Memo1: TfrxMemoView
          Width = 674.866110000000000000
          Height = 13.228346456692900000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[GRUPO2]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 317.480520000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPagRec
        DataSetName = 'frxDsPagRec'
        RowCount = 0
        object Memo13: TfrxMemoView
          Width = 35.905511810000000000
          Height = 13.228346460000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPagrec."Data"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          Left = 71.811026060000000000
          Width = 52.913395590000000000
          Height = 13.228346460000000000
          DataField = 'SERIEDOC'
          DataSet = frxDsPagRec
          DataSetName = 'frxDsPagRec'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."SERIEDOC"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 389.291382520000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39', <frxDsPagrec."NotaFiscal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo53: TfrxMemoView
          Left = 302.362265750000000000
          Width = 86.929141180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPagrec."Descricao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo54: TfrxMemoView
          Left = 449.763818580000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."VALOR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 245.669381650000000000
          Width = 56.692901180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPagrec."NOMECONTA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 35.905511810000000000
          Width = 35.905511810000000000
          Height = 13.228346460000000000
          DataField = 'Vencimento'
          DataSet = frxDsPagRec
          DataSetName = 'frxDsPagRec'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPagRec."Vencimento"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 419.527600550000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagrec."Controle"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 631.118120000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."ATUALIZADO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          Left = 498.897647560000000000
          Width = 30.236222910000000000
          Height = 13.228346460000000000
          OnBeforePrint = 'Memo24OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."MoraDia"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Left = 612.283464566929000000
          Width = 18.897635350000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."ATRAZODD"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo39: TfrxMemoView
          Left = 563.149625830000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."PAGO_REAL"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          Left = 529.133775280000000000
          Width = 34.015745590000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPagRec."MULTA_REAL"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 124.724490000000000000
          Width = 34.015745590000000000
          Height = 13.228346460000000000
          DataField = 'NO_UH'
          DataSet = frxDsPagRec
          DataSetName = 'frxDsPagRec'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPagRec."NO_UH"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Left = 158.740260000000000000
          Width = 86.929141180000000000
          Height = 13.228346460000000000
          DataField = 'NOME_TERCEIRO'
          DataSet = frxDsPagRec
          DataSetName = 'frxDsPagRec'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPagRec."NOME_TERCEIRO"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 18.739786460000000000
        Top = 468.661720000000000000
        Width = 680.315400000000000000
        object Memo20: TfrxMemoView
          Left = 449.763774650000000000
          Top = 5.511440000000000000
          Width = 49.133860710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."VALOR">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 3.779530000000000000
          Top = 5.511440000000000000
          Width = 445.984251970000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          Left = 631.181124330000000000
          Top = 5.511440000000000000
          Width = 49.133860710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."ATUALIZADO">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          Left = 563.149628270000000000
          Top = 5.511440000000000000
          Width = 49.133860710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."PAGO_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          Left = 498.897960000000000000
          Top = 5.779530000000000000
          Width = 30.236210710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."MoraVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          Left = 529.134200000000000000
          Top = 5.669291340000000000
          Width = 34.015740710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."MULTA_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 166.299320000000000000
        Width = 680.315400000000000000
        object Memo26: TfrxMemoView
          Left = 35.905511810000000000
          Width = 35.905511810000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencto.')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 71.811048030000000000
          Width = 52.913395590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'S'#233'rie - Doc.')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 302.362243780000000000
          Width = 86.929141180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 389.291226300000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 449.763672130000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 245.669403620000000000
          Width = 56.692901180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Width = 35.905511810000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss.')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 419.527830000000000000
          Width = 30.236220470000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haBlock
          Memo.UTF8W = (
            'ID')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 631.118464170000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Atualizado')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          Left = 612.283823380000000000
          Width = 18.897637800000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'DA')
          ParentFont = False
          WordWrap = False
        end
        object Memo69: TfrxMemoView
          Left = 563.149970000000000000
          Width = 49.133858270000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
          WordWrap = False
        end
        object Memo70: TfrxMemoView
          Left = 529.134119450000000000
          Width = 34.015745590000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Multa')
          ParentFont = False
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          Left = 498.897960000000000000
          Width = 30.236222910000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Mora')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 124.724511970000000000
          Width = 34.015748031496100000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VAR_UH]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 158.740260000000000000
          Width = 86.929141180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Terceiro')
          ParentFont = False
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        Height = 13.291116460000000000
        Top = 241.889920000000000000
        Visible = False
        Width = 680.315400000000000000
        Condition = 'frxDsPagRec."NOME_TERCEIRO"'
        object Memo12: TfrxMemoView
          Top = 0.062770000000000010
          Width = 675.307050000000000000
          Height = 13.228346456692900000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[GRUPO1]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 393.071120000000000000
        Width = 680.315400000000000000
        object Memo17: TfrxMemoView
          Left = 3.779530000000000000
          Width = 445.984251970000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL [GRUPO1]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 449.763774650000000000
          Width = 49.133860710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."VALOR">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          Left = 631.181124330000000000
          Width = 49.133860710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."ATUALIZADO">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo45: TfrxMemoView
          Left = 563.149628270000000000
          Width = 49.133860710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."PAGO_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          Left = 498.897960000000000000
          Width = 30.236210710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."MoraVal">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          Left = 529.134200000000000000
          Width = 34.015740710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPagRec."MULTA_REAL">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 510.236550000000000000
        Width = 680.315400000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo4: TfrxMemoView
          Width = 680.315400000000000000
          Height = 9.448818900000000000
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
end
