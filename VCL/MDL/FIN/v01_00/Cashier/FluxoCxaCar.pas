unit FluxoCxaCar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, Variants, frxClass, frxDBSet, dmkGeral,
  UnDmkProcFunc;

type
  TFmFluxoCxaCar = class(TForm)
    PainelConfirma: TPanel;
    BtImprime: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    PainelA: TPanel;
    Label9: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    CkIni: TCheckBox;
    TPIni: TDateTimePicker;
    CkFim: TCheckBox;
    TPFim: TDateTimePicker;
    Panel4: TPanel;
    Label4: TLabel;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    TPCre: TDateTimePicker;
    TPDeb: TDateTimePicker;
    Panel5: TPanel;
    RGFonte: TRadioGroup;
    CkSubstituir: TCheckBox;
    CkDocEmAberto: TCheckBox;
    PB1: TProgressBar;
    QrIni: TmySQLQuery;
    QrIniValor: TFloatField;
    QrMovant: TmySQLQuery;
    QrMovantMovim: TFloatField;
    QrMovantVALOR: TFloatField;
    QrFlxo: TmySQLQuery;
    QrFlxoQUITACAO: TWideStringField;
    QrFlxoData: TDateField;
    QrFlxoTipo: TSmallintField;
    QrFlxoCarteira: TIntegerField;
    QrFlxoControle: TIntegerField;
    QrFlxoSub: TSmallintField;
    QrFlxoAutorizacao: TIntegerField;
    QrFlxoGenero: TIntegerField;
    QrFlxoQtde: TFloatField;
    QrFlxoDescricao: TWideStringField;
    QrFlxoNotaFiscal: TIntegerField;
    QrFlxoDebito: TFloatField;
    QrFlxoCredito: TFloatField;
    QrFlxoCompensado: TDateField;
    QrFlxoSerieCH: TWideStringField;
    QrFlxoDocumento: TFloatField;
    QrFlxoSit: TIntegerField;
    QrFlxoVencimento: TDateField;
    QrFlxoFatID: TIntegerField;
    QrFlxoFatID_Sub: TIntegerField;
    QrFlxoFatParcela: TIntegerField;
    QrFlxoID_Pgto: TIntegerField;
    QrFlxoID_Sub: TSmallintField;
    QrFlxoFatura: TWideStringField;
    QrFlxoEmitente: TWideStringField;
    QrFlxoBanco: TIntegerField;
    QrFlxoContaCorrente: TWideStringField;
    QrFlxoCNPJCPF: TWideStringField;
    QrFlxoLocal: TIntegerField;
    QrFlxoCartao: TIntegerField;
    QrFlxoLinha: TIntegerField;
    QrFlxoOperCount: TIntegerField;
    QrFlxoLancto: TIntegerField;
    QrFlxoPago: TFloatField;
    QrFlxoMez: TIntegerField;
    QrFlxoFornecedor: TIntegerField;
    QrFlxoCliente: TIntegerField;
    QrFlxoCliInt: TIntegerField;
    QrFlxoForneceI: TIntegerField;
    QrFlxoMoraDia: TFloatField;
    QrFlxoMulta: TFloatField;
    QrFlxoMoraVal: TFloatField;
    QrFlxoMultaVal: TFloatField;
    QrFlxoProtesto: TDateField;
    QrFlxoDataDoc: TDateField;
    QrFlxoCtrlIni: TIntegerField;
    QrFlxoNivel: TIntegerField;
    QrFlxoVendedor: TIntegerField;
    QrFlxoAccount: TIntegerField;
    QrFlxoICMS_P: TFloatField;
    QrFlxoICMS_V: TFloatField;
    QrFlxoDuplicata: TWideStringField;
    QrFlxoDepto: TIntegerField;
    QrFlxoDescoPor: TIntegerField;
    QrFlxoDescoVal: TFloatField;
    QrFlxoDescoControle: TIntegerField;
    QrFlxoUnidade: TIntegerField;
    QrFlxoNFVal: TFloatField;
    QrFlxoAntigo: TWideStringField;
    QrFlxoExcelGru: TIntegerField;
    QrFlxoDoc2: TWideStringField;
    QrFlxoCNAB_Sit: TSmallintField;
    QrFlxoTipoCH: TSmallintField;
    QrFlxoLk: TIntegerField;
    QrFlxoDataCad: TDateField;
    QrFlxoDataAlt: TDateField;
    QrFlxoUserCad: TIntegerField;
    QrFlxoUserAlt: TIntegerField;
    QrFlxoNOMECART: TWideStringField;
    QrFlxoNOMERELACIONADO: TWideStringField;
    DsFlxo: TDataSource;
    QrExtr: TmySQLQuery;
    QrExtratoDataE: TDateField;
    QrExtratoDataV: TDateField;
    QrExtratoDataQ: TDateField;
    QrExtratoDataX: TDateField;
    QrExtratoTexto: TWideStringField;
    QrExtratoDocum: TWideStringField;
    QrExtratoNotaF: TWideStringField;
    QrExtratoSaldo: TFloatField;
    QrExtrCredi: TFloatField;
    QrExtrDebit: TFloatField;
    QrExtratoCartC: TIntegerField;
    QrExtratoCartN: TWideStringField;
    QrExtratoCodig: TIntegerField;
    QrExtratoCtrle: TIntegerField;
    QrExtratoCtSub: TIntegerField;
    QrExtratoCART_ORIG: TWideStringField;
    QrExtratoID_Pg: TIntegerField;
    QrExtratoTipoI: TIntegerField;
    QrExtrVALOR: TFloatField;
    DsExtr: TDataSource;
    frxFin_Relat_005_01_A06: TfrxReport;
    frxFin_Relat_005_01_A07: TfrxReport;
    frxFin_Relat_005_01_A08: TfrxReport;
    frxDsExtr: TfrxDBDataset;
    QrLct: TmySQLQuery;
    QrExtrtCrt: TIntegerField;
    QrFlxoCARTIP: TIntegerField;
    QrFlxoCARBAN: TIntegerField;
    QrMovantCarteira: TIntegerField;
    QrMovantNome: TWideStringField;
    QrExtrNOMECAR: TWideStringField;
    QrFlxoCar: TmySQLQuery;
    QrFlxoCarCART: TIntegerField;
    QrFlxoAgencia: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrExtrCalcFields(DataSet: TDataSet);
    procedure QrExtrBeforeOpen(DataSet: TDataSet);
    procedure BtImprimeClick(Sender: TObject);
    procedure frxFin_Relat_005_01_A06GetValue(const VarName: string;
      var Value: Variant);
    procedure QrMovantCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FEmpresa: Integer;
    FTOTFlxCxaCar: Double;
    procedure ImprimeFluxoDeCaixa();
    procedure ReopenExtr();
  public
    { Public declarations }
  end;

  var
  FmFluxoCxaCar: TFmFluxoCxaCar;

implementation

uses ModuleGeral, UnInternalConsts, UCreate, UMySQLModule, UnMyObjects;

{$R *.DFM}

procedure TFmFluxoCxaCar.BtImprimeClick(Sender: TObject);
begin
  FEmpresa := DModG.QrEmpresasCodigo.Value;
  //
  ImprimeFluxoDeCaixa;
  ReopenExtr();
  //
  case RGFonte.ItemIndex of
    0: MyObjects.frxMostra(frxFin_Relat_005_01_A06, 'Relat�rio de Fluxo de Caixa');
    1: MyObjects.frxMostra(frxFin_Relat_005_01_A07, 'Relat�rio de Fluxo de Caixa');
    2: MyObjects.frxMostra(frxFin_Relat_005_01_A08, 'Relat�rio de Fluxo de Caixa');
  end;
end;

procedure TFmFluxoCxaCar.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFluxoCxaCar.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmFluxoCxaCar.FormCreate(Sender: TObject);
begin
  TPIni.Date := Int(Date);
  TPFim.Date := Int(Date);
  TPCre.Date := Int(Date);
  TPDeb.Date := Int(Date);
end;

procedure TFmFluxoCxaCar.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmFluxoCxaCar.frxFin_Relat_005_01_A06GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'NOMEREL' then
    Value := 'RELAT�RIO FLUXO DE CAIXA'
  else if VarName = 'VAR_NOMEEMPRESA' then
    Value := CBEmpresa.Text
  else if AnsiCompareText(VarName, 'PERIODO') = 0 then
  begin
    Value := MLAGeral.PeriodoImp2(TPIni.Date, TPFim.Date,
      CkIni.Checked, CkFim.Checked, 'Per�odo de emiss�o: ', '', '');
  end
  else if VarName = 'SUBST' then
  begin
    if CkSubstituir.Checked then
      Value := 'Cliente / Fornecedor'
    else
      Value := 'Descri��o';
  end
  else if VarName = 'FTOTFlxCxaCar' then
    Value := FTOTFlxCxaCar;
end;

procedure TFmFluxoCxaCar.ImprimeFluxoDeCaixa;
  function Fmt(Data: TDateTime): String;
  begin
    Result := '"' + FormatDateTime('YYYY-MM-DD', Data) + '"';
  end;
var
  Quitacao, Quitacao2, Texto, Docum, NotaF: String;
  DataE, DataV, DataQ, DataX: Variant;
  CartOrig, Codig: Integer;
  Saldo: Double;
  DataI: TDateTime;
begin
  Screen.Cursor := crHourGlass;
  PB1.Position := 0;
  PB1.Visible  := True;
  Saldo        := 0;
  Codig        := 0;

  FTOTFlxCxaCar := 0;

  UCriar.RecriaTempTable('extratocc2', DModG.QrUpdPID1, False);

  //   S A L D O    I N I C I A L
  QrIni.Close;
  QrIni.Params[0].AsInteger := FEmpresa;
  QrIni.Open;

  //   M O V I M E N T O
  if CkIni.Checked then
    DataI := TPIni.Date
  else
    DataI := 0;

  //   L A N � A M E N T O S
  if CkDocEmAberto.Checked then
  begin
    Quitacao2 := 'Date(IF(car.Tipo <> 2, lan.Data,IF(lan.Sit > 1, lan.Compensado, ' +
  'IF(lan.Vencimento<SYSDATE(), IF(lan.Debito>0, lan.Vencimento, lan.Vencimento ), ' +
  ' lan.Vencimento))))';
  end;
  Quitacao := 'Date(IF(car.Tipo <> 2, lan.Data,IF(lan.Sit > 1, lan.Compensado, ' +
  'IF(lan.Vencimento<SYSDATE(), IF(lan.Debito>0, ' + Fmt(TPDeb.Date) +
  ', ' + Fmt(TPCre.Date) + '), lan.Vencimento))))';
  //
  //   CARTEIRAS DO FLUXO
  {
  QrFlxoCar.Close;
  QrFlxoCar.SQL.Clear;
  QrFlxoCar.SQL.Add('SELECT IF(car.Tipo = 2, car.Banco, lan.Carteira) CART');
  QrFlxoCar.SQL.Add('FROM ' + VAR_LCT + ' lan');
  QrFlxoCar.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrFlxoCar.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=lan.Cliente');
  QrFlxoCar.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=lan.Fornecedor');
  //
  QrFlxoCar.SQL.Add('WHERE car.Fornecei=' + IntToStr(FEmpresa));
  QrFlxoCar.SQL.Add(MLAgeral.SQL_Periodo('AND ' + Quitacao,
    TPIni.Date, TPFim.Date, CkIni.Checked, CkFim.Checked));
  QrFlxoCar.SQL.Add('AND lan.Genero > 0');
  QrFlxoCar.SQL.Add('AND ((car.Tipo<>2) OR (lan.Sit < 2))');
  QrFlxoCar.SQL.Add('GROUP BY CART');
  QrFlxoCar.Open;
  }
  QrFlxoCar.Close;
  QrFlxoCar.Params[0].AsInteger := FEmpresa;
  QrFlxoCar.Open;
  //
  if QrFlxoCar.RecordCount > 0 then
  begin
    QrFlxoCar.First;
    while not QrFlxoCar.Eof do
    begin
      QrMovant.Close;
      QrMovant.Params[0].AsInteger := FEmpresa;
      QrMovant.Params[1].AsInteger := QrFlxoCarCART.Value;
      QrMovant.Params[2].AsString  := Geral.FDT(DataI, 1);
      QrMovant.Open;

      //   SALDO INICIAL
      if QrMovant.RecordCount > 0 then
      begin
        QrMovant.First;
        while not QrMovant.Eof do
        begin
          Codig := Codig + 1;
          Saldo := QrMovantVALOR.Value;

          UMyMod.SQLInsUpd_Old(DmodG.QrUpdPID1, CO_INCLUSAO, 'ExtratoCC2', false,
          [
            'DataE', 'DataV', 'DataQ',
            'DataX', 'Texto', 'Docum',
            'NotaF', 'Credi', 'Debit',
            'Saldo', 'CartC', 'CartN',
            'TipoI', 'Ctrle', 'ID_Pg',
            'tCrt'
          ], ['Codig'], [
            Null, Null, Null,
            MLAGeral.FDT_NULL(DataI, 1), 'SALDO ANTERIOR', '',
            '', 0, 0,
            Saldo, 0, QrMovantNome.Value, 1, 0, 0, QrMovantCarteira.Value
          ], [Codig]);
          //
          QrMovant.Next;
        end;
      end;
      //
      QrFlxo.Close;
      QrFlxo.SQL.Clear;
      if CkDocEmAberto.Checked then
        QrFlxo.SQL.Add('SELECT ' + Quitacao2 + ' QUITACAO, ')
      else
        QrFlxo.SQL.Add('SELECT ' + Quitacao + ' QUITACAO, ');
      QrFlxo.SQL.Add('lan.*, car.Nome NOMECART, ');
      QrFlxo.SQL.Add('IF(lan.Cliente<>0,');
      QrFlxo.SQL.Add('  IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome),');
      QrFlxo.SQL.Add('  IF (lan.Fornecedor<>0,');
      QrFlxo.SQL.Add('    IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome), "")) NOMERELACIONADO,');
      QrFlxo.SQL.Add('car.Tipo CARTIP, car.Banco CARBAN');
      QrFlxo.SQL.Add('FROM ' + VAR_LCT + ' lan');
      QrFlxo.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
      QrFlxo.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=lan.Cliente');
      QrFlxo.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=lan.Fornecedor');
      //
      QrFlxo.SQL.Add('WHERE car.Fornecei=' + IntToStr(FEmpresa));
      QrFlxo.SQL.Add(dmkPF.SQL_Periodo('AND ' + Quitacao,
        TPIni.Date, TPFim.Date, CkIni.Checked, CkFim.Checked));
      QrFlxo.SQL.Add('AND lan.Genero <> 0');
      QrFlxo.SQL.Add('AND ((car.Tipo<>2) OR (lan.Sit < 2))');
      QrFlxo.SQL.Add('AND IF(car.Tipo = 2, car.Banco, lan.Carteira) = ' + FormatFloat('0', QrFlxoCarCART.Value));
      //
      QrFlxo.SQL.Add('ORDER BY QUITACAO, Credito DESC, Debito');
      QrFlxo.Open;

      PB1.Max  := QrFlxo.RecordCount + 1;

      // INSERE DADOS TABELA LOCAL
      PB1.Position := Codig;
      PB1.Update;
      Application.ProcessMessages;
      //
      QrFlxo.First;
      while not QrFlxo.Eof do
      begin
        Codig := Codig + 1;

        if QrFlxoCARTIP.Value = 2 then
          CartOrig := QrFlxoCARBAN.Value
        else
          CartOrig := QrFlxoCarteira.Value;

        PB1.Position := Codig;
        PB1.Update;
        Application.ProcessMessages;

        Saldo := Saldo + QrFlxoCredito.Value - QrFlxoDebito.Value;
        DataE := MLAGeral.FDT_NULL(QrFlxoData.Value, 1);
        DataV := MLAGeral.FDT_NULL(QrFlxoVencimento.Value, 1);
        DataQ := MLAGeral.FDT_NULL(QrFlxoCompensado.Value, 1);
        DataX := QrFlxoQUITACAO.Value;
        //
        if CkSubstituir.Checked then
        begin
          Texto := QrFlxoNOMERELACIONADO.Value;
          //
          if Length(Texto) = 0 then
            Texto := QrFlxoDescricao.Value;
        end else
          Texto := QrFlxoDescricao.Value;
        //
        if QrFlxoQtde.Value > 0 then
          Texto := FloatToStr(QrFlxoQtde.Value) + ' ' + Texto;
        if QrFlxoDocumento.Value > 0 then
          Docum := FormatFloat('000000', QrFlxoDocumento.Value) else Docum := '';
        if QrFlxoSerieCH.Value <> '' then Docum := QrFlxoSerieCH.Value + Docum;
        if QrFlxoNotaFiscal.Value > 0 then
          NotaF := FormatFloat('000000', QrFlxoNotaFiscal.Value) else NotaF := '';
        //
        UMyMod.SQLInsUpd_Old(DmodG.QrUpdPID1, CO_INCLUSAO, 'ExtratoCC2', false,
        [
          'DataE', 'DataV', 'DataQ',
          'DataX', 'Texto', 'Docum',
          'NotaF', 'Credi', 'Debit',
          'Saldo', 'CartC', 'CartN',
          'TipoI', 'Ctrle', 'ID_Pg',
          'CtSub', 'tCrt'
        ], ['Codig'], [
          DataE, DataV, DataQ,
          DataX, Texto, Docum,
          NotaF, QrFlxoCredito.Value, QrFlxoDebito.Value,
          Saldo, QrFlxoCarteira.Value, QrFlxoNOMECART.Value,
          2, QrFlxoControle.Value, QrFlxoID_Pgto.Value,
          QrFlxoSub.Value, CartOrig
        ], [Codig]);
        //
        QrFlxo.Next;
      end;
      //
      FTOTFlxCxaCar := FTOTFlxCxaCar + Saldo; //SALDO Total de todas as carteiras 
      //
      QrFlxoCar.Next;
      //
      Saldo := 0;
    end;
  end;
  PB1.Visible   := False;
  Screen.Cursor := crDefault;
end;

procedure TFmFluxoCxaCar.QrExtrBeforeOpen(DataSet: TDataSet);
begin
  QrLct.Close;
  QrLct.Params[0].AsInteger := FEmpresa;
  QrLct.Open;
end;

procedure TFmFluxoCxaCar.QrExtrCalcFields(DataSet: TDataSet);
begin
  QrExtrVALOR.Value := QrExtrCredi.Value - QrExtrDebit.Value;
end;

procedure TFmFluxoCxaCar.QrMovantCalcFields(DataSet: TDataSet);
begin
  QrMovantVALOR.Value := QrMovantMovim.Value + QrIniValor.Value;
end;

procedure TFmFluxoCxaCar.ReopenExtr();
begin
  QrExtr.Close;
  QrExtr.SQL.Clear;
  QrExtr.SQL.Add('SELECT loc.*, car.Nome NOMECAR');
  QrExtr.SQL.Add('FROM extratocc2 loc');
  QrExtr.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo = loc.tCrt');
  QrExtr.SQL.Add('ORDER BY NOMECAR, Codig, Texto');
  QrExtr.Open;
end;

end.
