object FmEnvia: TFmEnvia
  Left = 322
  Top = 171
  Caption = 'Envio de dados por servidor de SMTP'
  ClientHeight = 516
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 40
    Width = 792
    Height = 428
    Align = alClient
    Caption = 'Panel2'
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 8
      Width = 90
      Height = 13
      Caption = 'Servidor de SMTP:'
    end
    object Label2: TLabel
      Left = 12
      Top = 48
      Width = 76
      Height = 13
      Caption = 'Nome da conta:'
    end
    object Label4: TLabel
      Left = 12
      Top = 88
      Width = 31
      Height = 13
      Caption = 'E-mail:'
    end
    object Label5: TLabel
      Left = 12
      Top = 128
      Width = 74
      Height = 13
      Caption = 'Dono da conta:'
    end
    object Label9: TLabel
      Left = 12
      Top = 168
      Width = 41
      Height = 13
      Caption = 'Assunto:'
    end
    object Label8: TLabel
      Left = 12
      Top = 284
      Width = 25
      Height = 13
      Caption = 'Para:'
    end
    object Label10: TLabel
      Left = 244
      Top = 284
      Width = 45
      Height = 13
      Caption = 'CC Cega:'
    end
    object Label11: TLabel
      Left = 244
      Top = 8
      Width = 100
      Height = 13
      Caption = 'Corpo da mensagem:'
    end
    object Label12: TLabel
      Left = 12
      Top = 348
      Width = 17
      Height = 13
      Caption = 'CC:'
    end
    object Label13: TLabel
      Left = 572
      Top = 284
      Width = 104
      Height = 13
      Caption = 'Mensagens de status:'
    end
    object Label14: TLabel
      Left = 12
      Top = 208
      Width = 110
      Height = 13
      Caption = 'Anexos (insert - delete):'
    end
    object Label15: TLabel
      Left = 244
      Top = 340
      Width = 103
      Height = 13
      Caption = 'Conex'#227'o rede dial-up:'
    end
    object EdSMTP: TEdit
      Left = 12
      Top = 24
      Width = 220
      Height = 21
      Color = clWhite
      TabOrder = 0
    end
    object EdConta: TEdit
      Left = 12
      Top = 64
      Width = 220
      Height = 21
      Color = clWhite
      TabOrder = 1
    end
    object EdEMail: TEdit
      Left = 12
      Top = 104
      Width = 220
      Height = 21
      Color = clWhite
      TabOrder = 2
    end
    object EdDono: TEdit
      Left = 12
      Top = 144
      Width = 220
      Height = 21
      Color = clWhite
      TabOrder = 3
    end
    object EdAssunto: TEdit
      Left = 12
      Top = 184
      Width = 220
      Height = 21
      Color = clWhite
      TabOrder = 4
    end
    object MePara: TMemo
      Left = 12
      Top = 300
      Width = 221
      Height = 45
      Color = clWhite
      TabOrder = 5
    end
    object MeCega: TMemo
      Left = 244
      Top = 300
      Width = 217
      Height = 37
      Color = clWhite
      TabOrder = 6
    end
    object MeCC: TMemo
      Left = 12
      Top = 364
      Width = 221
      Height = 57
      Color = clWhite
      TabOrder = 7
    end
    object MeCorpo: TMemo
      Left = 244
      Top = 24
      Width = 537
      Height = 257
      Color = clWhite
      TabOrder = 8
    end
    object Memo5: TMemo
      Left = 572
      Top = 300
      Width = 209
      Height = 121
      TabStop = False
      ReadOnly = True
      TabOrder = 9
    end
    object LBAnexos: TListBox
      Left = 12
      Top = 224
      Width = 221
      Height = 57
      Color = clWhite
      ItemHeight = 13
      TabOrder = 10
      OnKeyDown = LBAnexosKeyDown
    end
    object CheckBox1: TCheckBox
      Left = 432
      Top = 356
      Width = 133
      Height = 17
      Caption = 'Setar valor de limpeza.'
      TabOrder = 11
    end
    object RadioGroup1: TRadioGroup
      Left = 468
      Top = 284
      Width = 97
      Height = 53
      Caption = ' Encode: '
      ItemIndex = 0
      Items.Strings = (
        'MIME'
        'UUEncode')
      TabOrder = 12
    end
    object EdDialUp: TEdit
      Left = 244
      Top = 356
      Width = 177
      Height = 21
      Color = clWhite
      TabOrder = 13
    end
    object BtLimpa: TBitBtn
      Tag = 246
      Left = 412
      Top = 382
      Width = 150
      Height = 40
      Caption = '&Limpar par'#226'metros'
      NumGlyphs = 2
      TabOrder = 14
      OnClick = BtLimpaClick
    end
    object BtSalvar: TBitBtn
      Tag = 245
      Left = 244
      Top = 382
      Width = 150
      Height = 40
      Caption = 'Salvar &par'#226'metros'
      NumGlyphs = 2
      TabOrder = 15
      OnClick = BtSalvarClick
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 468
    Width = 792
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtSaida: TBitBtn
      Tag = 13
      Left = 664
      Top = 4
      Width = 120
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela de cadastro de senhas'
      Caption = '&Sa'#237'da'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtSaidaClick
    end
    object BtEMail: TBitBtn
      Tag = 244
      Left = 12
      Top = 4
      Width = 120
      Height = 40
      Caption = '&Enviar E-mail'
      TabOrder = 1
      OnClick = BtEMailClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 40
    Align = alTop
    Caption = 'Envio de dados por servidor de SMTP'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 790
      Height = 38
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 36
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 404
    Top = 180
  end
end
