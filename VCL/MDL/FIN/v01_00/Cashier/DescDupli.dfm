object FmDescDupli: TFmDescDupli
  Left = 419
  Top = 217
  Caption = 'FIN-RELAT-006 :: Desconto de Duplicatas'
  ClientHeight = 374
  ClientWidth = 472
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 472
    Height = 278
    Align = alClient
    TabOrder = 0
    object Label2: TLabel
      Left = 12
      Top = 8
      Width = 79
      Height = 13
      Caption = 'Descontado por:'
    end
    object Label6: TLabel
      Left = 12
      Top = 52
      Width = 35
      Height = 13
      Caption = 'Cliente:'
    end
    object GBVenct: TGroupBox
      Left = 240
      Top = 100
      Width = 221
      Height = 61
      Caption = ' '
      TabOrder = 7
      Visible = False
      object Label4: TLabel
        Left = 12
        Top = 16
        Width = 33
        Height = 13
        Caption = 'In'#237'cio: '
      end
      object Label5: TLabel
        Left = 116
        Top = 16
        Width = 25
        Height = 13
        Caption = 'Final:'
      end
      object TPVIni: TDateTimePicker
        Left = 12
        Top = 32
        Width = 100
        Height = 21
        Date = 38665.614473738400000000
        Time = 38665.614473738400000000
        TabOrder = 0
      end
      object TPVFim: TDateTimePicker
        Left = 116
        Top = 32
        Width = 100
        Height = 21
        Date = 38665.614473738400000000
        Time = 38665.614473738400000000
        TabOrder = 1
      end
    end
    object EdDescoPor: TdmkEditCB
      Left = 12
      Top = 24
      Width = 77
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBDescoPor
    end
    object CBDescoPor: TdmkDBLookupComboBox
      Left = 92
      Top = 24
      Width = 369
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENTIDADE'
      ListSource = DsDescopor
      TabOrder = 1
      dmkEditCB = EdDescoPor
      UpdType = utYes
    end
    object GBEmiss: TGroupBox
      Left = 12
      Top = 100
      Width = 221
      Height = 61
      Caption = ' '
      TabOrder = 5
      Visible = False
      object Label1: TLabel
        Left = 12
        Top = 16
        Width = 33
        Height = 13
        Caption = 'In'#237'cio: '
      end
      object Label3: TLabel
        Left = 116
        Top = 16
        Width = 25
        Height = 13
        Caption = 'Final:'
      end
      object TPEIni: TDateTimePicker
        Left = 12
        Top = 32
        Width = 100
        Height = 21
        Date = 38665.614473738400000000
        Time = 38665.614473738400000000
        TabOrder = 0
      end
      object TPEFim: TDateTimePicker
        Left = 116
        Top = 32
        Width = 100
        Height = 21
        Date = 38665.614473738400000000
        Time = 38665.614473738400000000
        TabOrder = 1
      end
    end
    object CkEmiss: TCheckBox
      Left = 22
      Top = 96
      Width = 103
      Height = 17
      Caption = 'Data de emiss'#227'o: '
      TabOrder = 4
      OnClick = CkEmissClick
    end
    object CkVenct: TCheckBox
      Left = 250
      Top = 96
      Width = 119
      Height = 17
      Caption = 'Data de vencimento: '
      TabOrder = 6
      OnClick = CkVenctClick
    end
    object RGAgrup1: TRadioGroup
      Left = 12
      Top = 164
      Width = 449
      Height = 65
      Caption = ' Agrupar por: '
      Columns = 4
      ItemIndex = 0
      Items.Strings = (
        'N'#227'o agrupar'
        'Descontado por'
        'Cliente'
        'Carteira'
        'Conta'
        'Data'
        'Vencimento')
      TabOrder = 8
    end
    object EdCliente: TdmkEditCB
      Left = 12
      Top = 68
      Width = 77
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCliente
    end
    object CBCliente: TdmkDBLookupComboBox
      Left = 92
      Top = 68
      Width = 369
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENTIDADE'
      ListSource = DsClientes
      TabOrder = 3
      dmkEditCB = EdCliente
      UpdType = utYes
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 326
    Width = 472
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object BtSaida: TBitBtn
      Left = 362
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSaidaClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 472
    Height = 48
    Align = alTop
    Caption = 'Desconto de Duplicatas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 470
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 468
      ExplicitHeight = 44
    end
  end
  object DsDescopor: TDataSource
    DataSet = QrDescopor
    Left = 400
    Top = 69
  end
  object QrDescopor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 372
    Top = 69
    object QrDescoporCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDescoporNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object QrDescos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT la.Data, la.Vencimento, la.Carteira, '
      'ca.Nome NOMECARTEIRA, la.Controle,'
      'la.Genero, co.Nome NOMECONTA, la.Descricao,'
      'la.NotaFiscal, la.Credito, la.Compensado,'
      'la.Cliente, la.Duplicata, la.DescoPor, la.DescoVal,'
      'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial '
      'ELSE cl.Nome END NOMECLIENTE, '
      'CASE WHEN dp.Tipo=0 THEN dp.RazaoSocial '
      'ELSE dp.Nome END NOMEDESCOPOR'
      'FROM lanctos la'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      'LEFT JOIN contas    co ON co.Codigo=la.Genero'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades dp ON dp.Codigo=la.DescoPor'
      'WHERE la.DescoPor<>0 OR la.DescoVal<>0')
    Left = 124
    Top = 317
    object QrDescosData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrDescosVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrDescosCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrDescosNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrDescosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDescosGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrDescosNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrDescosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrDescosNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrDescosCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrDescosCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrDescosCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrDescosDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrDescosDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrDescosDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrDescosNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrDescosNOMEDESCOPOR: TWideStringField
      FieldName = 'NOMEDESCOPOR'
      Size = 100
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 372
    Top = 113
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 400
    Top = 113
  end
  object frxDescos: TfrxReport
    Version = '4.10.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39719.753125532400000000
    ReportOptions.LastChange = 39719.753125532400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxDescosGetValue
    Left = 152
    Top = 316
    Datasets = <
      item
        DataSet = frxDsDescos
        DataSetName = 'frxDsDescos'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 60.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          Left = 550.000000000000000000
          Top = 9.102350000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 6.000000000000000000
          Top = 29.102350000000000000
          Width = 708.000000000000000000
          Height = 30.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'RELAT'#195#8220'RIO DE DESCONTOS DE DUPLICATAS')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 24.000000000000000000
        Top = 536.693260000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          Left = 471.102350000000000000
          Top = 1.038959999999970000
          Width = 244.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8 = (
            '[PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 18.000000000000000000
        Top = 351.496290000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsDescos
        DataSetName = 'frxDsDescos'
        RowCount = 0
        object Memo13: TfrxMemoView
          Left = 6.000000000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo13OnBeforePrint'
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsDescos."Data"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 302.000000000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo16OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000;-000000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsDescos."NotaFiscal"]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 198.000000000000000000
          Width = 104.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo53OnBeforePrint'
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsDescos."Descricao"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 430.000000000000000000
          Width = 74.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo54OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsDescos."Credito"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 94.000000000000000000
          Width = 104.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo3OnBeforePrint'
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsDescos."NOMECONTA"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 50.000000000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo6OnBeforePrint'
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsDescos."Vencimento"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 390.000000000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo8OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000;-000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsDescos."Controle"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 342.000000000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo24OnBeforePrint'
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8 = (
            '[frxDsDescos."Duplicata"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 506.000000000000000000
          Width = 74.000000000000000000
          Height = 18.000000000000000000
          OnBeforePrint = 'Memo39OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8 = (
            '[frxDsDescos."DescoVal"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 98.000000000000000000
        Top = 102.047310000000000000
        Width = 718.110700000000000000
        object Picture2: TfrxPictureView
          Left = 6.000000000000000000
          Top = 9.952690000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo26: TfrxMemoView
          Left = 178.000000000000000000
          Top = 9.952690000000000000
          Width = 536.000000000000000000
          Height = 22.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 314.000000000000000000
          Top = 33.952690000000000000
          Width = 400.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[PeriodoE]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 178.000000000000000000
          Top = 33.952690000000000000
          Width = 136.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Per'#195#173'odo de emiss'#195#163'o:')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 426.000000000000000000
          Top = 77.952690000000000000
          Width = 288.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[CLIENTE_]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 378.000000000000000000
          Top = 77.952690000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Cliente:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Left = 78.000000000000000000
          Top = 77.952690000000000000
          Width = 296.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[DESCO_POR]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          Left = 6.000000000000000000
          Top = 77.952690000000000000
          Width = 72.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Descontado por:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 314.000000000000000000
          Top = 53.952690000000000000
          Width = 400.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            '[PeriodoV]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 178.000000000000000000
          Top = 53.952690000000000000
          Width = 136.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8 = (
            'Per'#195#173'odo de vencimento:')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 42.000000000000000000
        Top = 472.441250000000000000
        Width = 718.110700000000000000
        object Memo20: TfrxMemoView
          Left = 433.779530000000000000
          Top = 1.731910000000000000
          Width = 74.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsDescos."Credito">)]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 365.779530000000000000
          Top = 1.731910000000030000
          Width = 36.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Total:')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 505.779530000000000000
          Top = 1.731910000000000000
          Width = 74.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsDescos."DescoVal">)]')
          ParentFont = False
        end
        object Line6: TfrxLineView
          Left = 1.559060000000000000
          Width = 732.000000000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        Height = 26.000000000000000000
        Top = 222.992270000000000000
        Width = 718.110700000000000000
        object Memo29: TfrxMemoView
          Left = 6.000000000000000000
          Top = 4.976190000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'Emiss'#195#163'o')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 198.000000000000000000
          Top = 4.976190000000000000
          Width = 104.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Descri'#195#167#195#163'o')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 302.000000000000000000
          Top = 4.976190000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'N.F.')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 430.000000000000000000
          Top = 4.976190000000000000
          Width = 74.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'Valor')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 94.000000000000000000
          Top = 4.976190000000000000
          Width = 104.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            'Conta')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 50.000000000000000000
          Top = 4.976190000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'Vencto')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 390.000000000000000000
          Top = 4.976190000000000000
          Width = 40.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'Controle')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 342.000000000000000000
          Top = 4.976190000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8 = (
            'Duplic.')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 506.000000000000000000
          Top = 4.976190000000000000
          Width = 74.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'Desconto')
          ParentFont = False
        end
      end
      object GH1: TfrxGroupHeader
        Height = 18.000000000000000000
        Top = 309.921460000000000000
        Visible = False
        Width = 718.110700000000000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = '[frxDsDescos."Data"]'
        object Memo12: TfrxMemoView
          Left = 6.000000000000000000
          Width = 708.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8 = (
            '[GRUPO1]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        Height = 20.000000000000000000
        Top = 393.071120000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo17: TfrxMemoView
          Left = 6.000000000000000000
          Width = 408.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            'TOTAL [GRUPO1]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 430.000000000000000000
          Width = 74.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM(<frxDsDescos."Credito">)]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 506.000000000000000000
          Width = 74.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8 = (
            '[SUM([frxDsDescos."DescoVal"])]')
          ParentFont = False
        end
      end
      object Line14: TfrxLineView
        Left = 972.330860000000000000
        Top = 769.448980000000000000
        Height = 20.000000000000000000
        ShowHint = False
        Frame.Style = fsDot
        Frame.Typ = [ftLeft]
      end
    end
  end
  object frxDsDescos: TfrxDBDataset
    UserName = 'frxDsDescos'
    CloseDataSource = False
    DataSet = QrDescos
    BCDToCurrency = False
    Left = 180
    Top = 316
  end
end
