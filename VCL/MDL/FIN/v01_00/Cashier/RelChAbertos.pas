unit RelChAbertos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, ComCtrls,
  dmkEditDateTimePicker, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkDBGrid, UnInternalConsts, frxClass, frxDBSet, dmkGeral,
  dmkPermissoes;

type
  TFmRelChAbertos = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    LaTipo: TdmkLabel;
    DsFornece: TDataSource;
    QrFornece: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrLct: TmySQLQuery;
    DsLct: TDataSource;
    QrCarts: TmySQLQuery;
    DsCarts: TDataSource;
    QrCartsAtivo: TSmallintField;
    QrCartsNome: TWideStringField;
    QrCartsCodigo: TIntegerField;
    frxRelChAbertos_06_01: TfrxReport;
    QrLctControle: TIntegerField;
    QrLctBanco1: TWideStringField;
    QrLctAgencia1: TWideStringField;
    QrLctConta1: TWideStringField;
    QrLctDescricao: TWideStringField;
    frxDsLct: TfrxDBDataset;
    QrLctNOMECART: TWideStringField;
    QrLctNOMEENT: TWideStringField;
    QrLctVencimento: TDateField;
    QrLctData: TDateField;
    QrLctDebito: TFloatField;
    QrLctSerieCH: TWideStringField;
    QrLctDocumento: TWideStringField;
    QrLctFornecedor: TIntegerField;
    QrLctDataDoc: TDateField;
    QrLctDoc: TFloatField;
    frxRelChAbertos_07_01: TfrxReport;
    frxRelChAbertos_08_01: TfrxReport;
    frxRelChAbertos_08_02: TfrxReport;
    frxRelChAbertos_07_02: TfrxReport;
    frxRelChAbertos_06_02: TfrxReport;
    QrLctNOMESIT: TWideStringField;
    QrLctSit: TIntegerField;
    QrLctPrazo: TSmallintField;
    QrLctTipo: TSmallintField;
    Panel7: TPanel;
    PainelA: TPanel;
    Label9: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Panel6: TPanel;
    RGTipo: TRadioGroup;
    Panel1: TPanel;
    Panel3: TPanel;
    GBVencto: TGroupBox;
    LaVenctI: TLabel;
    LaVenctF: TLabel;
    TPVctoIni: TdmkEditDateTimePicker;
    CkVencto: TCheckBox;
    TPVctoFim: TdmkEditDateTimePicker;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    TPDataIni: TdmkEditDateTimePicker;
    CkData: TCheckBox;
    TPDataFim: TdmkEditDateTimePicker;
    GroupBox1: TGroupBox;
    TPQuitado: TdmkEditDateTimePicker;
    CkQuitado: TCheckBox;
    Panel4: TPanel;
    LaTerceiro: TLabel;
    Label4: TLabel;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    CBConta: TdmkDBLookupComboBox;
    EdConta: TdmkEditCB;
    PainelE: TPanel;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    CkOrdem1: TCheckBox;
    CkOrdem2: TCheckBox;
    RGFonte: TRadioGroup;
    Panel5: TPanel;
    Panel8: TPanel;
    Label3: TLabel;
    dmkDBGrid1: TdmkDBGrid;
    PnCorrige: TPanel;
    LaAviso: TLabel;
    PB1: TProgressBar;
    QrLctReparcel: TIntegerField;
    QrLctCompensado: TDateField;
    QrLctCOMPENSADO_TXT: TWideStringField;
    dmkPermissoes1: TdmkPermissoes;
    RGOrdens: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure dmkDBGrid1CellClick(Column: TColumn);
    procedure BtOKClick(Sender: TObject);
    procedure frxRelChAbertos_06_01GetValue(const VarName: string;
      var Value: Variant);
    procedure QrLctCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FTabLctA: String;
    FTotIts: Integer;
    FValor: Double;
    procedure PesquisaLct(TipoRel: Integer);
    procedure CriaTempTable();
  public
    { Public declarations }
  end;

  var
  FmRelChAbertos: TFmRelChAbertos;

implementation

uses UnMyObjects, Module, ModuleGeral, UnFinanceiro;

{$R *.DFM}

procedure TFmRelChAbertos.BtOKClick(Sender: TObject);
var
  NomeRel: String;
begin
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Empresa n�o definida!') then Exit;
  if MyObjects.FIC(RGTipo.ItemIndex < 0, RGTipo, 'Tipo de relat�rio n�o definido!') then Exit;
  //
  {$IFDEF DEFINE_VARLCT}
    FTabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, EdEmpresa.ValueVariant);
  {$ELSE}
    FTabLctA := VAR_LCT;
  {$ENDIF}
  //
  if UFinanceiro.LancamentosComProblemas(FTabLctA) then
    Exit;
  //
  PesquisaLct(RGTipo.ItemIndex);
  //
  case RGTipo.ItemIndex of
    0:
    begin
      NomeRel := 'Cheques em aberto';
      case RGFonte.ItemIndex of
        0: MyObjects.frxMostra(frxRelChAbertos_06_01, NomeRel);
        1: MyObjects.frxMostra(frxRelChAbertos_07_01, NomeRel);
        2: MyObjects.frxMostra(frxRelChAbertos_08_01, NomeRel);
      end;
    end;
    1:
    begin
      NomeRel := 'Cheques emitidos';
      case RGFonte.ItemIndex of
        0: MyObjects.frxMostra(frxRelChAbertos_06_02, NomeRel);
        1: MyObjects.frxMostra(frxRelChAbertos_07_02, NomeRel);
        2: MyObjects.frxMostra(frxRelChAbertos_08_02, NomeRel);
      end;
    end;
    2:
    begin
      NomeRel := 'Cheques sem n�mero';
      case RGFonte.ItemIndex of
        0: MyObjects.frxMostra(frxRelChAbertos_06_01, NomeRel);
        1: MyObjects.frxMostra(frxRelChAbertos_07_01, NomeRel);
        2: MyObjects.frxMostra(frxRelChAbertos_08_01, NomeRel);
      end;
    end;
    else
      Geral.MensagemBox('Tipo de relat�rio n�o definido!', 'Aviso',
        MB_OK+MB_ICONWARNING);
  end;
  QrLct.Close;
end;

procedure TFmRelChAbertos.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmRelChAbertos.CriaTempTable();
begin
  DModG.QrUpdPID1.Close;
  if EdEmpresa.ValueVariant <> 0 then
  begin
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('DROP TABLE IF EXISTS _lista_x_;');
    DModG.QrUpdPID1.SQL.Add('CREATE TABLE _lista_x_');
    DModG.QrUpdPID1.SQL.Add('SELECT Codigo, Nome, Ativo');
    DModG.QrUpdPID1.SQL.Add('FROM '+ TMeuDB +'.carteiras');
    DModG.QrUpdPID1.SQL.Add('WHERE ForneceI='+ FormatFloat('0', DModG.QrEmpresasCodigo.Value));
    DModG.QrUpdPID1.SQL.Add('AND Tipo=2;');
    DModG.QrUpdPID1.ExecSQL;
    //
    QrCarts.Close;
    QrCarts.Database := DModG.MyPID_DB;
    QrCarts.Open;
  end;
end;

procedure TFmRelChAbertos.dmkDBGrid1CellClick(Column: TColumn);
var
  Codigo, Ativo: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    Ativo  := QrCartsAtivo.Value;
    Codigo := QrCartsCodigo.Value;
    //
    if Ativo = 0 then Ativo := 1 else Ativo := 0;
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('UPDATE _lista_x_ SET Ativo=:P0');
    DmodG.QrUpdPID1.SQL.Add('WHERE Codigo=:P1');
    DmodG.QrUpdPID1.Params[00].AsInteger := Ativo;
    DmodG.QrUpdPID1.Params[01].AsInteger := Codigo;
    DmodG.QrUpdPID1.ExecSQL;
    //
    QrCarts.Close;
    QrCarts.Open;
  end;
end;

procedure TFmRelChAbertos.EdEmpresaChange(Sender: TObject);
begin
  CriaTempTable();
end;

procedure TFmRelChAbertos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmRelChAbertos.FormCreate(Sender: TObject);// Adicionado por .DFM > .PAS
CB?.ListSource = DModG.DsEmpresas;

begin
  QrCarts.Close;
  QrFornece.Open;
  QrContas.Open;
  //
  TPVctoIni.Date := Date;
  TPVctoFim.Date := Date;
  TPDataIni.Date := Date;
  TPDataFim.Date := Date;
  TPQuitado.Date := Date;
end;

procedure TFmRelChAbertos.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmRelChAbertos.frxRelChAbertos_06_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VAR_NOMEEMPRESA' then
    Value := CBEmpresa.Text
  else if VarName = 'NOMEREL' then
    Value := 'RELAT�RIO DE CHEQUES'
  else if AnsiCompareText(VarName, 'PERIODO') = 0 then
    Value := MLAGeral.PeriodoImp(TPVctoIni.Date, TPVctoFim.Date, TPDataIni.Date,
      TPDataFim.Date, CkVencto.Checked, CkVencto.Checked, CkData.Checked,
      CkData.Checked, 'Per�odo de vencimento: ', 'Per�odo do documento: ')
  else if VarName = 'VFR_GRUPO1' then
    if CkOrdem1.Checked then Value := RGOrdem1.ItemIndex else Value := -1
  else if VarName = 'VFR_GRUPO2' then
    if CkOrdem2.Checked then Value := RGOrdem2.ItemIndex else Value := -1
  else if VarName = 'GRUPO1' then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := 'Fornecedor: ' +FormatFloat(' 000000 - ', QrLctFornecedor.Value)+ QrLctNOMEENT.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrLctDataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrLctData.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrLctVencimento.Value);
      4: Value := 'Documento: ' + FormatFloat(' 000000 ', QrLctDoc.Value);
    end;
  end
  else if VarName = 'GRUPO2' then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := 'Fornecedor: ' +FormatFloat(' 000000 - ', QrLctFornecedor.Value)+ QrLctNOMEENT.Value;
      1: Value := 'Data doc. de '  + FormatDateTime(VAR_FORMATDATE2, QrLctDataDoc.Value);
      2: Value := 'Emiss�o em '    + FormatDateTime(VAR_FORMATDATE2, QrLctData.Value);
      3: Value := 'Vencimento em ' + FormatDateTime(VAR_FORMATDATE2, QrLctVencimento.Value);
      4: Value := 'Documento: ' + FormatFloat(' 000000 ', QrLctDoc.Value);
    end;
  end
  else if VarName = 'VFR_CODITION_A1' then
  begin
    case RGOrdem1.ItemIndex of
      00: Value := 'frxDsLct."NOMEENT"';
      01: Value := 'frxDsLct."DataDoc"';
      02: Value := 'frxDsLct."Data"';
      03: Value := 'frxDsLct."Vencimento"';
      04: Value := 'frxDsLct."Documento"';
    end;
  end
  else if VarName = 'VFR_CODITION_B1' then
  begin
    case RGOrdem2.ItemIndex of
      00: Value := 'frxDsLct."NOMEENT"';
      01: Value := 'frxDsLct."DataDoc"';
      02: Value := 'frxDsLct."Data"';
      03: Value := 'frxDsLct."Vencimento"';
      04: Value := 'frxDsLct."Documento"';
    end;
  end
  else if VarName = 'TOTITENS' then
    Value := FTotIts
  else if VarName = 'TOTVAL' then
    Value := FValor;
end;

procedure TFmRelChAbertos.PesquisaLct(TipoRel: Integer);
  function OrdemPesq: String;
  const
    Ordens: array[0..4] of String = ('Fornecedor', 'DataDoc', 'Data', 'Vencimento', 'Documento');
  begin
    Result := 'ORDER BY ' +
    Ordens[RGOrdem1.ItemIndex] + ',' +
    Ordens[RGOrdem2.ItemIndex];
  end;
  function VerificaCarteira(): String;
  var
    Cod: String;
  begin
    Cod := '-10000000';
    if QrCarts.RecordCount > 0 then
    begin
      QrCarts.First;
      while not QrCarts.Eof do
      begin
        if QrCartsAtivo.Value = 1 then
          Cod := Cod + ',' + FormatFloat('0', QrCartsCodigo.Value);
        //
        QrCarts.Next;
      end;
    end;
    Result := Cod;
  end;
begin
  FTotIts := 0;
  FValor  := 0;
  //
  QrLct.Close;
  QrLct.SQL.Clear;
  QrLct.SQL.Add('SELECT LPAD(car.Banco1, 3, "0") Banco1,');
  QrLct.SQL.Add('LPAD(car.Agencia1, 4, "0") Agencia1, car.Conta1,');
  QrLct.SQL.Add('car.Nome NOMECART,  lan.Data, lan.Vencimento,');
  QrLct.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,');
  QrLct.SQL.Add('lan.Descricao, lan.Debito, lan.SerieCH, lan.Controle,');
  QrLct.SQL.Add('IF(lan.Documento=0, "",');
  QrLct.SQL.Add('LPAD(lan.Documento, 6, "0")) Documento,');
  QrLct.SQL.Add('lan.Fornecedor, lan.DataDoc, lan.Documento Doc,');
  QrLct.SQL.Add('lan.Sit, car.Prazo, lan.Tipo, lan.Reparcel, lan.Compensado');
  QrLct.SQL.Add('FROM ' + FTabLctA + ' lan');
  QrLct.SQL.Add('LEFT JOIN carteiras car ON car.Codigo = lan.Carteira');
  QrLct.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo = lan.Fornecedor');
  if TipoRel = 2 then
    QrLct.SQL.Add('WHERE lan.Documento = 0')
  else
    QrLct.SQL.Add('WHERE lan.Documento <> ""');
  if CkQuitado.Checked then
    QrLct.SQL.Add(dmkPF.SQL_Periodo('AND lan.Compensado ',
    0, Trunc(TPQuitado.Date), False, CkQuitado.Checked))
  else
  begin
    if TipoRel = 0 then
      QrLct.SQL.Add('AND lan.Compensado < 2 AND lan.Sit < 2');
  end;
  //
  QrLct.SQL.Add('AND lan.Carteira IN ('+ VerificaCarteira +')');
  //
  QrLct.SQL.Add(dmkPF.SQL_Periodo('AND lan.Vencimento ', TPVctoIni.Date,
    TPVctoFim.Date, CkVencto.Checked, CkVencto.Checked));
  //  
  QrLct.SQL.Add(dmkPF.SQL_Periodo('AND lan.Data ', TPDataIni.Date,
    TPDataFim.Date, CkData.Checked, CkData.Checked));
  //
  if EdFornece.ValueVariant > 0 then
    QrLct.SQL.Add('AND lan.Fornecedor = '+ EdFornece.ValueVariant);
  if EdConta.ValueVariant > 0 then
    QrContas.SQL.Add('AND lan.Genero = '+ EdConta.ValueVariant);
  //
  QrLct.SQL.Add(OrdemPesq);
  //dmkPF.LeMeuTexto(QrLct.SQL.Text);
  QrLct.Open;
  //
  if QrLct.RecordCount > 0 then
  begin
    FTotIts := QrLct.RecordCount;
    QrLct.First;
    while not QrLct.Eof do
    begin
      FValor := FValor + QrLctDebito.Value;
      QrLct.Next;
    end;
  end;
end;

procedure TFmRelChAbertos.QrLctCalcFields(DataSet: TDataSet);
begin
  QrLctCOMPENSADO_TXT.Value := Geral.FDT(QrLctCompensado.Value, 3, True);
  QrLctNOMESIT.Value := UFinanceiro.NomeSitLancto(QrLctSit.Value,
    QrLctTipo.Value, QrLctPrazo.Value, QrLctVencimento.Value,
    QrLctReparcel.Value, True);
end;

end.
