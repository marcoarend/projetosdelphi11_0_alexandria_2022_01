unit DocsAssina;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkRadioGroup,
  dmkMemo;

type
  TFmDocsAssina = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtAssinar: TBitBtn;
    LaTitulo1C: TLabel;
    BtCoAssinar: TBitBtn;
    BtValidar: TBitBtn;
    BtHash: TBitBtn;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    SBSerial: TSpeedButton;
    SBArquivo: TSpeedButton;
    SBDestino: TSpeedButton;
    Label4: TLabel;
    EdSerial: TdmkEdit;
    EdArquivo: TdmkEdit;
    EdDestino: TdmkEdit;
    EdResultado: TdmkEdit;
    RGAlgoritmo: TdmkRadioGroup;
    Label5: TLabel;
    MeHash: TdmkMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtAssinarClick(Sender: TObject);
    procedure SBSerialClick(Sender: TObject);
    procedure SBArquivoClick(Sender: TObject);
    procedure SBDestinoClick(Sender: TObject);
    procedure BtValidarClick(Sender: TObject);
    procedure BtCoAssinarClick(Sender: TObject);
    procedure BtHashClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmDocsAssina: TFmDocsAssina;

implementation

uses UnMyObjects, Module, UnitDocsAssina, ModuleGeral, CapicomListas, MyDBCheck;

{$R *.DFM}

procedure TFmDocsAssina.BtHashClick(Sender: TObject);
var
  Hash, Arquivo: String;
  AlgInt: Integer;
  Algoritmo: TAlgorithm;
begin
  AlgInt  := RGAlgoritmo.ItemIndex;
  Arquivo := EdArquivo.ValueVariant;
  //
  case AlgInt of
    0: Algoritmo := istSHA1;
    1: Algoritmo := istMD2;
    2: Algoritmo := istMD4;
    3: Algoritmo := istMD5;
    4: Algoritmo := istSHA256;
    5: Algoritmo := istSHA384;
    6: Algoritmo := istSHA512;
  end;
  Hash := UnDocsAssina.CalculaHash(Arquivo, Algoritmo);
  //
  if Hash <> '' then
    MeHash.Text := Hash
  else
    Geral.MB_Erro('Falha ao calcular Hash!');
end;

procedure TFmDocsAssina.BtAssinarClick(Sender: TObject);
var
  Serial, Arquivo, DirDestino, Resultado: String;
  Agora: TDateTime;
begin
  Agora      := DModG.ObtemAgora;
  Serial     := EdSerial.ValueVariant;
  Arquivo    := EdArquivo.ValueVariant;
  DirDestino := EdDestino.ValueVariant;
  //
  Resultado := UnDocsAssina.AssinaDocumento(Agora, Serial, Arquivo, DirDestino);
  //
  if Resultado <> '' then
  begin
    EdResultado.ValueVariant := Resultado;
    //
    Geral.MB_Aviso('Arquivo assinado com sucesso!');
  end;
end;

procedure TFmDocsAssina.BtCoAssinarClick(Sender: TObject);
var
  Serial, Arquivo, DirDestino, Resultado: String;
  Agora: TDateTime;
begin
  Agora      := DModG.ObtemAgora;
  Serial     := EdSerial.ValueVariant;
  Arquivo    := EdArquivo.ValueVariant;
  DirDestino := EdDestino.ValueVariant;
  //
  Resultado := UnDocsAssina.CoAssinaDocumento(Agora, Serial, Arquivo, DirDestino);
  //
  if Resultado <> '' then
  begin
    EdResultado.ValueVariant := Resultado;
    //
    Geral.MB_Aviso('Texto co-assinado com sucesso!');
  end;
end;

procedure TFmDocsAssina.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDocsAssina.BtValidarClick(Sender: TObject);
var
  Arquivo: String;
begin
  Arquivo := EdArquivo.ValueVariant;
  //
  UnDocsAssina.VerificaAssinaturaDeDocumento(Arquivo);
  //
  Geral.MB_Aviso('Valida��o conclu�da!');
end;

procedure TFmDocsAssina.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmDocsAssina.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmDocsAssina.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDocsAssina.SBArquivoClick(Sender: TObject);
begin
  MyObjects.DefineArquivo1(Self, EdArquivo);
end;

procedure TFmDocsAssina.SBDestinoClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDestino);
end;

procedure TFmDocsAssina.SBSerialClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCapicomListas, FmCapicomListas, afmoSoBoss) then
  begin
    FmCapicomListas.ShowModal;
    //
    if FmCapicomListas.FSelected then
      EdSerial.ValueVariant := FmCapicomListas.FSerialNumber;
    //
    FmCapicomListas.Destroy;
  end;
end;

end.
