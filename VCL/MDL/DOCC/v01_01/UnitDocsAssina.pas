unit UnitDocsAssina;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  ComCtrls, dmkGeral, Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db,
  DbCtrls, UnInternalConsts2, Variants, CAPICOM_TLB, System.Win.ComObj, ActiveX;

type
  TDinByteArray = Array of byte;
  TAlgorithm = (istSHA1, istMD2, istMD4, istMD5, istSHA256, istSHA384, istSHA512);
  TCriptAlgorit = (istRC2, istRC4, istDES, ist3DES, istAES);
  TCriptSizeKey = (istMax, ist40bit, ist56bit, ist128bit, ist192bit, ist256bit);
  TUnitDocsAssina = class(TObject)
  private
    { Private declarations }
    function  ObtemCertificado(const NumeroSerial: String;
                out Cert: ICertificate2): Boolean;
    function  IdErro(Id: Integer): String;
    function  CarregarArquivo(NomeArquivo: String;
                out Dados: WideString): Boolean; overload;
    function  CarregarArquivo(NomeArquivo: String;
                out Dados: TDinByteArray): Boolean; overload;
    function  SelCriptAlgo(CriptAlgorit: TCriptAlgorit): CAPICOM_ENCRYPTION_ALGORITHM;
    function  SelCriptKeyLen(CriptSizeKey: TCriptSizeKey): CAPICOM_ENCRYPTION_KEY_LENGTH;
    procedure SaveBinFile(FileName: String; BinArray: OleVariant);
  public
    { Public declarations }
    function  ChecarCertAssinatura(Certificado: ICertificate2): Boolean;
    function  AssinaDocumento(Agora: TDateTime; Serial, Arquivo,
                DirDestino: String): String;
    function  AssinaCodigo(Agora: TDateTime; Serial, Arquivo, Descri,
                DescriURL: String): String;
    function  CoAssinaDocumento(Agora: TDateTime; Serial, Arquivo,
                DirDestino: String): String;
    function  CalculaHash(Arquivo: String; Algoritmo: TAlgorithm): String;
    function  VerificaAssinaturaDeDocumento(Arquivo: String; MostraMsg: Boolean = True): Boolean;
  end;

var
  UnDocsAssina: TUnitDocsAssina;

const
  CAPICOM_TRUST_IS_NOT_TIME_VALID                 = $00000001;
  CAPICOM_TRUST_IS_NOT_TIME_NESTED                = $00000002;
  CAPICOM_TRUST_IS_REVOKED                        = $00000004;
  CAPICOM_TRUST_IS_NOT_SIGNATURE_VALID            = $00000008;
  CAPICOM_TRUST_IS_NOT_VALID_FOR_USAGE            = $00000010;
  CAPICOM_TRUST_IS_UNTRUSTED_ROOT                 = $00000020;
  CAPICOM_TRUST_REVOCATION_STATUS_UNKNOWN         = $00000040;
  CAPICOM_TRUST_IS_CYCLIC                         = $00000080;
  CAPICOM_TRUST_INVALID_EXTENSION                 = $00000100;
  CAPICOM_TRUST_INVALID_POLICY_CONSTRAINTS        = $00000200;
  CAPICOM_TRUST_INVALID_BASIC_CONSTRAINTS         = $00000400;
  CAPICOM_TRUST_INVALID_NAME_CONSTRAINTS          = $00000800;
  CAPICOM_TRUST_HAS_NOT_SUPPORTED_NAME_CONSTRAINT = $00001000;
  CAPICOM_TRUST_HAS_NOT_DEFINED_NAME_CONSTRAINT   = $00002000;
  CAPICOM_TRUST_HAS_NOT_PERMITTED_NAME_CONSTRAINT = $00004000;
  CAPICOM_TRUST_HAS_EXCLUDED_NAME_CONSTRAINT      = $00008000;
  CAPICOM_TRUST_IS_OFFLINE_REVOCATION             = $01000000;
  CAPICOM_TRUST_NO_ISSUANCE_CHAIN_POLICY          = $02000000;
  CAPICOM_TRUST_IS_PARTIAL_CHAIN                  = $00010000;
  CAPICOM_TRUST_CTL_IS_NOT_TIME_VALID             = $00020000;
  CAPICOM_TRUST_CTL_IS_NOT_SIGNATURE_VALID        = $00040000;
  CAPICOM_TRUST_CTL_IS_NOT_VALID_FOR_USAGE        = $00080000;

implementation

uses UnMyObjects;

{ TUnitDocsCab }

function TUnitDocsAssina.AssinaDocumento(Agora: TDateTime; Serial, Arquivo,
  DirDestino: String): String;
const
  Desatachado = False;
var
  Certificado: ICertificate2;
  Atributo: IAttribute;
  Assinante: ISigner2;
  SignedData: ISignedData;
  Util: IUtilities;
  Documento: WideString;
  Assinatura: WideString;
  ArqDestino: String;
begin
  Result := '';
  //
  if MyObjects.FIC(Serial = '', nil, 'Defina o certificado a ser utilizado para assinar o documento!') then Exit;
  if MyObjects.FIC(Arquivo = '', nil, 'Defina o arquivo a ser assinado!') then Exit;
  if MyObjects.FIC(DirDestino = '', nil, 'Defina o diret�rio de destino do arquivo a ser assinado!') then Exit;
  //
  if CarregarArquivo(Arquivo, Documento) then
  begin
    if ObtemCertificado(Serial, Certificado) then
    begin
      if not ChecarCertAssinatura(Certificado) then
      begin
        Geral.MB_Aviso('Este certificado n�o � v�lido!' + sLineBreak +
          'Selecione outro diret�rio e tente novamente!');
        Exit;
      end;
      //
      Assinante             := CoSigner.Create;
      Assinante.Certificate := Certificado;
      Assinante.Options     := CAPICOM_CERTIFICATE_INCLUDE_END_ENTITY_ONLY;
      //
      SignedData         := CoSignedData.Create;
      SignedData.Content := Documento;
      //
      Atributo       := CoAttribute.Create;
      Atributo.Name  := CAPICOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME;
      Atributo.Value := Agora;
      //
      Assinante.AuthenticatedAttributes.Add(Atributo);
      //
      Atributo       := CoAttribute.Create;
      Atributo.Name  := CAPICOM_AUTHENTICATED_ATTRIBUTE_DOCUMENT_NAME;
      Atributo.Value := ExtractFileName(Arquivo);
      //
      Assinante.AuthenticatedAttributes.Add(Atributo);
      //
      Assinatura := SignedData.Sign(Assinante, Desatachado, CAPICOM_ENCODE_BINARY);
      ArqDestino := DirDestino + '\' + ChangeFileExt(ExtractFileName(Arquivo), '.p7s');
      Util       := CoUtilities.Create;
      //
      SaveBinFile(ArqDestino, Util.BinaryStringToByteArray(Assinatura));
      //
      Result := ArqDestino;
    end else
      Geral.MB_Erro('Falha ao assinar documento!');
  end;
end;

function TUnitDocsAssina.CarregarArquivo(NomeArquivo: String;
  out Dados: WideString): Boolean;
var
  FileStream: TFileStream;
  DadosStr: String;
begin
  Result := True;
  //
  try
    FileStream := TFileStream.Create(NomeArquivo, fmOpenRead, fmShareExclusive);
    //
    SetLength(DadosStr, FileStream.Size);
    FileStream.Read(DadosStr[1], FileStream.Size);
    FileStream.Free;
    //
    Dados := DadosStr;
  except
    on E: Exception do
    begin
      Result := False;
      //
      Geral.MB_Erro(E.Message);
    end;
  end;
end;

function TUnitDocsAssina.CarregarArquivo(NomeArquivo: String;
  out Dados: TDinByteArray): Boolean;
var
  FileStream: TFileStream;
begin
  Result := True;
  try
    FileStream := TFileStream.Create(NomeArquivo, fmOpenRead, fmShareExclusive);
    //
    SetLength(Dados, FileStream.Size);
    FileStream.Read(Dados[0], FileStream.Size);
    FileStream.Free;
  except
    on E: Exception do
    begin
      Result := False;
      //
      Geral.MB_Erro(E.Message);
    end;
  end;
end;

function TUnitDocsAssina.ChecarCertAssinatura(Certificado: ICertificate2): Boolean;
var
  UsoDaChave: IKeyUsage;
begin
  OleCheck(IDispatch(Certificado.KeyUsage).QueryInterface(IKeyUsage, UsoDaChave));
  //
  Result := Certificado.HasPrivateKey and
              UsoDaChave.IsPresent and
              UsoDaChave.IsCritical and
              UsoDaChave.IsDigitalSignatureEnabled and
              UsoDaChave.IsNonRepudiationEnabled;
end;

function TUnitDocsAssina.CoAssinaDocumento(Agora: TDateTime; Serial, Arquivo,
  DirDestino: String): String;
const
  Desatachado = False;
var
  CertStore: IStore3;
  Certificado: ICertificate2;
  Atributo: IAttribute;
  Assinante: ISigner2;
  SignedData: ISignedData;
  Util: IUtilities;
  Ind: Integer;
  Documento: WideString;
  Assinatura: WideString;
  Pkcs7: TDinByteArray;
  ArqDestino: String;
begin
  Result := '';
  //
  if MyObjects.FIC(Serial = '', nil, 'Defina o certificado a ser utilizado para co-assinar o documento!') then Exit;
  if MyObjects.FIC(Arquivo = '', nil, 'Defina o arquivo a ser co-assinado!') then Exit;
  if MyObjects.FIC(DirDestino = '', nil, 'Defina o diret�rio de destino do arquivo a ser co-assinado!') then Exit;
  //
  if CarregarArquivo(Arquivo, Pkcs7) then
  begin
    if ObtemCertificado(Serial, Certificado) then
    begin
      if not ChecarCertAssinatura(Certificado) then
      begin
        Geral.MB_Aviso('Este certificado n�o � v�lido!' + sLineBreak +
          'Selecione outro diret�rio e tente novamente!');
        Exit;
      end;
      Assinante             := CoSigner.Create;
      Assinante.Certificate := Certificado;
      Assinante.Options     := CAPICOM_CERTIFICATE_INCLUDE_END_ENTITY_ONLY;
      //
      Atributo       := coAttribute.Create;
      Atributo.Name  := CAPICOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME;
      Atributo.Value := Agora;
      //
      Assinante.AuthenticatedAttributes.Add(Atributo);
      //
      Atributo       := coAttribute.Create;
      Atributo.Name  := CAPICOM_AUTHENTICATED_ATTRIBUTE_DOCUMENT_NAME;
      Atributo.Value := ExtractFileName(Arquivo);
      //
      Assinante.AuthenticatedAttributes.Add(Atributo);
      //
      SignedData := CoSignedData.Create;
      //
      ArqDestino := DirDestino + '\' + 'CO_' + ChangeFileExt(ExtractFileName(Arquivo), '.p7s');
      Util       := CoUtilities.Create;
      Assinatura := Util.ByteArrayToBinaryString(Pkcs7);
      //
      SignedData.Verify(Assinatura, Desatachado, CAPICOM_VERIFY_SIGNATURE_ONLY);
      //
      Assinatura := SignedData.CoSign(Assinante, CAPICOM_ENCODE_BINARY );
      //
      SaveBinFile(ArqDestino, Util.BinaryStringToByteArray(Assinatura));
      //
      Result := ArqDestino;
    end else
      Geral.MB_Erro('Falha ao assinar documento!');
  end;
end;

procedure TUnitDocsAssina.SaveBinFile(FileName: String; BinArray: OleVariant);
const
  adSaveCreateOverWrite = 2;
  adTypeBinary          = 1;
  adModeReadWrite       = 3;
var
  Stream: OleVariant;
begin
  Stream      := CreateOleObject('ADODB.Stream');
  Stream.type := adTypeBinary;
  Stream.mode := adModeReadWrite;
  Stream.Open;
  Stream.write(BinArray);
  Stream.SaveToFile(FileName, adSaveCreateOverWrite);
  Stream.Close;
end;

function TUnitDocsAssina.SelCriptAlgo(CriptAlgorit: TCriptAlgorit): CAPICOM_ENCRYPTION_ALGORITHM;
begin
  case CriptAlgorit of
    istRC2:
      Result := CAPICOM_ENCRYPTION_ALGORITHM_RC2;
    istRC4:
      Result := CAPICOM_ENCRYPTION_ALGORITHM_RC4;
    istDES:
      Result := CAPICOM_ENCRYPTION_ALGORITHM_DES;
    ist3DES:
      Result := CAPICOM_ENCRYPTION_ALGORITHM_3DES;
    istAES:
      Result := CAPICOM_ENCRYPTION_ALGORITHM_AES;
  end;
end;

function TUnitDocsAssina.SelCriptKeyLen(CriptSizeKey: TCriptSizeKey): CAPICOM_ENCRYPTION_KEY_LENGTH;
begin
  case CriptSizeKey of
    istMax:
      Result := CAPICOM_ENCRYPTION_KEY_LENGTH_MAXIMUM;
    ist40bit:
      Result := CAPICOM_ENCRYPTION_KEY_LENGTH_40_BITS;
    ist56bit:
      Result := CAPICOM_ENCRYPTION_KEY_LENGTH_56_BITS;
    ist128bit:
      Result := CAPICOM_ENCRYPTION_KEY_LENGTH_128_BITS;
    ist192bit:
      Result := CAPICOM_ENCRYPTION_KEY_LENGTH_192_BITS;
    ist256bit:
      Result := CAPICOM_ENCRYPTION_KEY_LENGTH_256_BITS;
  end;
end;

function TUnitDocsAssina.ObtemCertificado(const NumeroSerial: String;
  out Cert: ICertificate2): Boolean;
var
  Store: IStore;
  Certs: ICertificates;
  i: Integer;
begin
  Result := False;
  Cert   := nil;
  Store  := CoStore.Create;
  //
  //Reposit�rios de Certifcados da M�quina
  Store.Open(CAPICOM_CURRENT_USER_STORE, 'MY', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);
  //
  //Abre a lista de certificados
  Certs := Store.Certificates;
  //
  //Aloca todos os certificados instalados na m�quia
  //
  i := 0;
  //
  //loop de procura ao certificado requerido pelo n�mero serial
  while i < Certs.Count do
  begin
    //Cria objeto para acesso a leitura do certificado
    Cert := IInterface(Certs.Item[i + 1]) as ICertificate2;
    //
    if UpperCase(Cert.SerialNumber) = Uppercase(NumeroSerial) then
    begin
      //se o n�mero do serial for igual ao que queremos utilizar
      Result := True;
      Exit;
    end;
    i := i + 1;
  end;
  //
  if Result = False then
  begin
    Cert := nil;
    //
    Geral.MB_Erro('N�o foi poss�vel carregar o certificado:' + sLineBreak +
      'N�mero Serial: ' + NumeroSerial + '!');
  end;
end;

function TUnitDocsAssina.IdErro(Id: Integer): String;
begin
  case Id of
    0:                                               Result := 'OK';
      CAPICOM_TRUST_IS_NOT_TIME_VALID:                 Result := 'Per�odo inv�lido';
      CAPICOM_TRUST_IS_NOT_TIME_NESTED:                Result := 'Per�odo n�o aninhado';
      CAPICOM_TRUST_IS_REVOKED:                        Result := 'Revogado';
      CAPICOM_TRUST_IS_NOT_SIGNATURE_VALID:            Result := 'Assinatura inv�lida';
      CAPICOM_TRUST_IS_NOT_VALID_FOR_USAGE:            Result := 'Inv�lido para uso';
      CAPICOM_TRUST_IS_UNTRUSTED_ROOT:                 Result := 'Raiz n�o confi�vel';
      CAPICOM_TRUST_REVOCATION_STATUS_UNKNOWN:         Result := 'Revoga��o n�o determinada';
      CAPICOM_TRUST_IS_CYCLIC:                         Result := 'Ciclico';
      CAPICOM_TRUST_INVALID_EXTENSION:                 Result := 'Extens�o inv�lida';
      CAPICOM_TRUST_INVALID_POLICY_CONSTRAINTS:        Result := 'inv�lido - Policy Constraint';
      CAPICOM_TRUST_INVALID_BASIC_CONSTRAINTS:         Result := 'inv�lido - Basic Constraint';
      CAPICOM_TRUST_INVALID_NAME_CONSTRAINTS:          Result := 'inv�lido - Name Constraint';
      CAPICOM_TRUST_HAS_NOT_SUPPORTED_NAME_CONSTRAINT: Result := 'Name Constraint n�o suportada';
      CAPICOM_TRUST_HAS_NOT_DEFINED_NAME_CONSTRAINT:   Result := 'Name Constraint n�o definida';
      CAPICOM_TRUST_HAS_NOT_PERMITTED_NAME_CONSTRAINT: Result := 'Name Constraint n�o permitida';
      CAPICOM_TRUST_HAS_EXCLUDED_NAME_CONSTRAINT:      Result := 'Name Constraint exclu�da';
      CAPICOM_TRUST_IS_OFFLINE_REVOCATION:             Result := 'Revogado (off-line)';
      CAPICOM_TRUST_NO_ISSUANCE_CHAIN_POLICY:          Result := 'TRUST_NO_ISSUANCE_CHAIN_POLICY';
      CAPICOM_TRUST_IS_PARTIAL_CHAIN:                  Result := 'Cadeia parcial';
      CAPICOM_TRUST_CTL_IS_NOT_TIME_VALID:             Result := 'CTL expirada';
      CAPICOM_TRUST_CTL_IS_NOT_SIGNATURE_VALID:        Result := 'Assinatura da CTL inv�lida';
      CAPICOM_TRUST_CTL_IS_NOT_VALID_FOR_USAGE:        Result := 'CTL n�o v�lida para uso';
    else
      Result := 'Problema desconhecido';
  end;
end;

function TUnitDocsAssina.VerificaAssinaturaDeDocumento(Arquivo: String;
  MostraMsg: Boolean = True): Boolean;
const
  Desatachado = False;
var
  SignedData: ISignedData;
  Util: IUtilities;
  Certificado: ICertificate;
  CertErro: ICertificate;
  Cadeia: IChain2;
  Documento: WideString;
  Assinatura: WideString;
  Pkcs7: TDinByteArray;
  Ind, IndChain: Integer;
  Assinante: ISigner2;
  Res: Boolean;
begin
  Res := False;
  //
  if MostraMsg then
    if MyObjects.FIC(Arquivo = '', nil, 'Defina o arquivo a ser verificado!') then Exit;
  //
  if CarregarArquivo(Arquivo, Pkcs7) then
  begin
    SignedData := CoSignedData.Create;
    Util       := CoUtilities.Create;
    Assinatura := Util.ByteArrayToBinaryString(Pkcs7);
    //
    SignedData.Verify(Assinatura, Desatachado, CAPICOM_VERIFY_SIGNATURE_ONLY);
    //
    for Ind := 1 to SignedData.Signers.Count do
    begin
      OleCheck(IDispatch(SignedData.Signers.Item[Ind]).QueryInterface(ISigner2, Assinante));
      //
      Certificado := Assinante.Certificate;
      Cadeia      := CoChain.Create;
      //
      if not Cadeia.Build(Certificado) then
      begin
        for IndChain := 1 to Cadeia.Certificates.Count do
        begin
          if Cadeia.Status[IndChain] <> 0 then
          begin
            if MostraMsg then
              Geral.MB_Erro('Problemas com certificado.' + IdErro(Cadeia.Status[IndChain]));
            //
            OleCheck(IDispatch(Cadeia.Certificates.Item[IndChain]).QueryInterface(ICertificate2, CertErro));
            CertErro.Display;
          end else
          begin
            if MostraMsg then
              Geral.MB_Aviso('Assinatura validada com sucesso!');
            Res := True;
          end;
        end;
      end;
      Cadeia := nil;
    end;
  end;
  Result := Res;
end;

function TUnitDocsAssina.AssinaCodigo(Agora: TDateTime; Serial, Arquivo,  Descri,
  DescriURL: String): String;
var
  Certificado: ICertificate2;
  Atributo: IAttribute;
  Assinante: ISigner2;
  SignedCode: ISignedCode;
  Ind: Integer;
begin
  //For�o realizados alguns testes e n�o funcionou!
  Result := '';
  //
  if MyObjects.FIC(Serial = '', nil, 'Defina o certificado a ser utilizado para assinar o documento!') then Exit;
  if MyObjects.FIC(Arquivo = '', nil, 'Defina o arquivo a ser assinado!') then Exit;
  //
  if ObtemCertificado(Serial, Certificado) then
  begin
    if not ChecarCertAssinatura(Certificado) then
    begin
      Geral.MB_Aviso('Este certificado n�o � v�lido!' + sLineBreak +
        'Selecione outro diret�rio e tente novamente!');
      Exit;
    end;
    Assinante             := CoSigner.Create;
    Assinante.Certificate := Certificado;
    Assinante.Options     := CAPICOM_CERTIFICATE_INCLUDE_END_ENTITY_ONLY;
    //
    Atributo       := coAttribute.Create;
    Atributo.Name  := CAPICOM_AUTHENTICATED_ATTRIBUTE_SIGNING_TIME;
    Atributo.Value := Agora;
    Assinante.AuthenticatedAttributes.Add(Atributo);
    //
    Atributo       := coAttribute.Create;
    Atributo.Name  := CAPICOM_AUTHENTICATED_ATTRIBUTE_DOCUMENT_NAME;
    Atributo.Value := ExtractFileName(Arquivo);
    //
    Assinante.AuthenticatedAttributes.Add(Atributo);
    //
    SignedCode                := CoSignedCode.Create;
    SignedCode.FileName       := Arquivo;
    SignedCode.Description    := Descri;
    SignedCode.DescriptionURL := DescriURL;
    //
    SignedCode.Sign(Assinante);
  end else
    Geral.MB_Erro('Falha ao assinar documento!');
end;

function TUnitDocsAssina.CalculaHash(Arquivo: String; Algoritmo: TAlgorithm): String;
var
  HashedData: IHashedData;
  Dados     : WideString;
  AlgoHash  : CAPICOM_HASH_ALGORITHM;
begin
  Result := '';
  //
  case Algoritmo of
    istSHA1:
      AlgoHash := CAPICOM_HASH_ALGORITHM_SHA1;
    istMD2:
      AlgoHash := CAPICOM_HASH_ALGORITHM_MD2;
    istMD4:
      AlgoHash := CAPICOM_HASH_ALGORITHM_MD4;
    istMD5:
      AlgoHash := CAPICOM_HASH_ALGORITHM_MD5;
    istSHA256:
      AlgoHash := CAPICOM_HASH_ALGORITHM_SHA_256;
    istSHA384:
      AlgoHash := CAPICOM_HASH_ALGORITHM_SHA_384;
    istSHA512:
      AlgoHash := CAPICOM_HASH_ALGORITHM_SHA_512;
  end;
  if MyObjects.FIC(Arquivo = '', nil, 'Defina o arquivo para c�lculo!') then Exit;
  //
  if CarregarArquivo(Arquivo, Dados) then
  begin
    HashedData           := CoHashedData.Create;
    HashedData.Algorithm := AlgoHash;
    //
    HashedData.Hash(Dados);
    //
    Result := HashedData.Value;
  end;
end;

end.
