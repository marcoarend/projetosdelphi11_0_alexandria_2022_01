object FmDocsCab: TFmDocsCab
  Left = 339
  Top = 185
  Caption = 'CAD-DOCUM-001 :: Cadastro de Documentos'
  ClientHeight = 508
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 412
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 57
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label9: TLabel
        Left = 17
        Top = 5
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label5: TLabel
        Left = 93
        Top = 5
        Width = 36
        Height = 13
        Caption = 'Tabela:'
        Enabled = False
      end
      object Label6: TLabel
        Left = 401
        Top = 5
        Width = 61
        Height = 13
        Caption = 'ID na tabela:'
        Enabled = False
      end
      object EdCodigo: TdmkEdit
        Left = 17
        Top = 20
        Width = 71
        Height = 21
        Alignment = taRightJustify
        Color = clInactiveCaption
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBackground
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdLinkTab: TdmkEdit
        Left = 92
        Top = 20
        Width = 40
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdLinkTabChange
      end
      object EdLinkTab_TXT: TdmkEdit
        Left = 132
        Top = 20
        Width = 265
        Height = 21
        Enabled = False
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdLinkID1: TdmkEdit
        Left = 400
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 349
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel2: TPanel
        Left = 869
        Top = 15
        Width = 137
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object Panel8: TPanel
      Left = 0
      Top = 57
      Width = 1008
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object Label2: TLabel
        Left = 17
        Top = 5
        Width = 31
        Height = 13
        Caption = 'T'#237'tulo:'
      end
      object EdNome: TdmkEdit
        Left = 16
        Top = 21
        Width = 465
        Height = 21
        MaxLength = 50
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'EdNome'
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'EdNome'
        ValWarn = False
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 412
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel1: TPanel
      Left = 137
      Top = 0
      Width = 324
      Height = 348
      Align = alLeft
      TabOrder = 0
      object dmkDBGrid1: TdmkDBGridZTO
        Left = 1
        Top = 22
        Width = 322
        Height = 325
        Align = alClient
        DataSource = DsDocsCab
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        PopupMenu = PMCab
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        SortArrow.Column = '2'
        SortArrow.Position = sapLeft
        SortArrow.Direction = sadDescending
        Columns = <
          item
            Expanded = False
            FieldName = 'Ativo'
            Title.Caption = 'ok'
            Width = 17
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'T'#237'tulo'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = [fsBold]
            Width = 258
            Visible = True
          end>
      end
      object EdPesquisa: TdmkEdit
        Left = 1
        Top = 1
        Width = 322
        Height = 21
        Align = alTop
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Pesquisar'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Pesquisar'
        ValWarn = False
        OnChange = EdPesquisaChange
        OnEnter = EdPesquisaEnter
        OnExit = EdPesquisaExit
        ExplicitWidth = 282
      end
    end
    object PnCont: TPanel
      Left = 606
      Top = 1
      Width = 344
      Height = 216
      BevelOuter = bvNone
      TabOrder = 1
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 344
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 344
          Height = 41
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitHeight = 73
          object Label1: TLabel
            Left = 0
            Top = 0
            Width = 37
            Height = 13
            Align = alTop
            Caption = 'Titulo:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object DBEdit1: TDBEdit
            Left = 0
            Top = 17
            Width = 344
            Height = 21
            Align = alTop
            DataField = 'Nome'
            DataSource = DsDocsCab
            TabOrder = 0
            ExplicitTop = 13
            ExplicitWidth = 290
          end
        end
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 41
        Width = 344
        Height = 175
        Align = alClient
        DataSource = DsDocsIts
        PopupMenu = PMIts
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = DBGrid1DblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'Caminho'
            Title.Caption = 'Local e nome do aqruivo'
            Visible = True
          end>
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 348
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 2
      Visible = False
      object Panel10: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel11: TPanel
          Left = 874
          Top = 0
          Width = 130
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 3
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui nova entidade'
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 125
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera entidade atual'
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 247
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Exclui entidade atual'
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtExcluiClick
        end
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 137
      Height = 348
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 3
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 137
        Height = 93
        Align = alTop
        TabOrder = 0
        object BtTudo: TBitBtn
          Tag = 127
          Left = 8
          Top = 4
          Width = 120
          Height = 40
          Hint = 'Marca todos itens'
          Caption = '&Tudo'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtTudoClick
        end
        object BtNenhum: TBitBtn
          Tag = 128
          Left = 8
          Top = 48
          Width = 120
          Height = 40
          Hint = 'Desmarca todos itens'
          Caption = '&Nenhum'
          NumGlyphs = 2
          TabOrder = 1
          OnClick = BtNenhumClick
        end
      end
      object CBLinkTabs: TCheckListBox
        Left = 0
        Top = 93
        Width = 137
        Height = 255
        OnClickCheck = CBLinkTabsClickCheck
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ItemHeight = 13
        ParentFont = False
        TabOrder = 1
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    Visible = False
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 310
        Height = 32
        Caption = 'Cadastro de Documentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 310
        Height = 32
        Caption = 'Cadastro de Documentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 310
        Height = 32
        Caption = 'Cadastro de Documentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    Visible = False
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrDocsCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrDocsCabBeforeClose
    AfterScroll = QrDocsCabAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Nome, Texto'
      'FROM anotacoes'
      'WHERE UserCad=:P0'
      'AND Nome LIKE :P1')
    Left = 540
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDocsCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'anotacoes.Codigo'
    end
    object QrDocsCabNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'anotacoes.Nome'
      Size = 50
    end
    object QrDocsCabAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsDocsCab: TDataSource
    DataSet = QrDocsCab
    Left = 564
    Top = 9
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 384
    Top = 200
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
  end
  object QrDocsIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM docsits '
      'WHERE Codigo=1')
    Left = 660
    Top = 9
    object QrDocsItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDocsItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrDocsItsCaminho: TWideStringField
      FieldName = 'Caminho'
      Size = 255
    end
  end
  object DsDocsIts: TDataSource
    DataSet = QrDocsIts
    Left = 688
    Top = 9
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 736
    Top = 204
    object Inclui2: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui2Click
    end
    object Altera2: TMenuItem
      Caption = '&Altera'
      OnClick = Altera2Click
    end
    object Exclui2: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui2Click
    end
  end
end
