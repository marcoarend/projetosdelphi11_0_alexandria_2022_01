object FmDocsAssina: TFmDocsAssina
  Left = 339
  Top = 185
  Caption = 'CAD-DOCUM-002 :: Assinador de Documentos'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 320
        Height = 32
        Caption = 'Assinador de Documentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 320
        Height = 32
        Caption = 'Assinador de Documentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 320
        Height = 32
        Caption = 'Assinador de Documentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtAssinar: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Assinar'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtAssinarClick
      end
      object BtCoAssinar: TBitBtn
        Tag = 14
        Left = 140
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Co-Assinar'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtCoAssinarClick
      end
      object BtValidar: TBitBtn
        Tag = 14
        Left = 266
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Validar'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtValidarClick
      end
      object BtHash: TBitBtn
        Tag = 14
        Left = 392
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Hash'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtHashClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 467
        Align = alClient
        TabOrder = 0
        object Label1: TLabel
          Left = 15
          Top = 40
          Width = 29
          Height = 13
          Caption = 'Serial:'
        end
        object Label2: TLabel
          Left = 15
          Top = 80
          Width = 39
          Height = 13
          Caption = 'Arquivo:'
        end
        object Label3: TLabel
          Left = 15
          Top = 120
          Width = 39
          Height = 13
          Caption = 'Destino:'
        end
        object SBSerial: TSpeedButton
          Left = 332
          Top = 56
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SBSerialClick
        end
        object SBArquivo: TSpeedButton
          Left = 332
          Top = 96
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SBArquivoClick
        end
        object SBDestino: TSpeedButton
          Left = 332
          Top = 139
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SBDestinoClick
        end
        object Label4: TLabel
          Left = 15
          Top = 224
          Width = 51
          Height = 13
          Caption = 'Resultado:'
        end
        object Label5: TLabel
          Left = 15
          Top = 272
          Width = 28
          Height = 13
          Caption = 'Hash:'
        end
        object EdSerial: TdmkEdit
          Left = 15
          Top = 56
          Width = 314
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '6659B7491BE7CC9AD6AC4F3248E4139B'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '6659B7491BE7CC9AD6AC4F3248E4139B'
          ValWarn = False
        end
        object EdArquivo: TdmkEdit
          Left = 15
          Top = 96
          Width = 314
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdDestino: TdmkEdit
          Left = 15
          Top = 139
          Width = 314
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdResultado: TdmkEdit
          Left = 15
          Top = 241
          Width = 314
          Height = 21
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object RGAlgoritmo: TdmkRadioGroup
          Left = 359
          Top = 40
          Width = 274
          Height = 340
          Caption = 'Algoritmo'
          ItemIndex = 0
          Items.Strings = (
            'SHA-1'
            'MD2'
            'MD4'
            'MD5'
            'SHA-256'
            'SHA-384'
            'SHA-512')
          TabOrder = 4
          UpdType = utYes
          OldValor = 0
        end
        object MeHash: TdmkMemo
          Left = 15
          Top = 291
          Width = 314
          Height = 89
          Lines.Strings = (
            'MeHash')
          TabOrder = 5
          UpdType = utYes
        end
      end
    end
  end
end
