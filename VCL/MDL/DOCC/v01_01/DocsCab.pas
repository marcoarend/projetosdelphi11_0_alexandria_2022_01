unit DocsCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnInternalConsts, Db, MyDBCheck,
  mySQLDbTables, DBCtrls, ComCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UMySQLModule, dmkLabel, ABSMain, Grids, DBGrids, dmkDBGrid, frxClass,
  frxDBSet, Mask, dmkDBEdit, dmkMemo, Menus, dmkCheckBox, dmkGeral, UnGotoy,
  dmkImage, Vcl.CheckLst, Variants, dmkDBGridZTO, UnDmkProcFunc, ShellAPI,
  UnDmkEnums;

type
  TFmDocsCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    Panel1: TPanel;
    dmkDBGrid1: TdmkDBGridZTO;
    EdPesquisa: TdmkEdit;
    PnCont: TPanel;
    QrDocsCab: TmySQLQuery;
    QrDocsCabCodigo: TIntegerField;
    QrDocsCabNome: TWideStringField;
    DsDocsCab: TDataSource;
    Panel6: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    Panel7: TPanel;
    Panel9: TPanel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel3: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel10: TPanel;
    Panel11: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    PMCab: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    Panel4: TPanel;
    Panel5: TPanel;
    CBLinkTabs: TCheckListBox;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    EdLinkTab: TdmkEdit;
    Label5: TLabel;
    EdLinkTab_TXT: TdmkEdit;
    Label6: TLabel;
    EdLinkID1: TdmkEdit;
    QrDocsCabAtivo: TSmallintField;
    Panel8: TPanel;
    EdNome: TdmkEdit;
    Label2: TLabel;
    DBGrid1: TDBGrid;
    QrDocsIts: TmySQLQuery;
    DsDocsIts: TDataSource;
    QrDocsItsCodigo: TIntegerField;
    QrDocsItsControle: TIntegerField;
    QrDocsItsCaminho: TWideStringField;
    PMIts: TPopupMenu;
    Inclui2: TMenuItem;
    Altera2: TMenuItem;
    Exclui2: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdPesquisaEnter(Sender: TObject);
    procedure EdPesquisaExit(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdPesquisaChange(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure CBLinkTabsClickCheck(Sender: TObject);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure EdLinkTabChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure QrDocsCabBeforeClose(DataSet: TDataSet);
    procedure QrDocsCabAfterScroll(DataSet: TDataSet);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure Inclui2Click(Sender: TObject);
    procedure Altera2Click(Sender: TObject);
    procedure Exclui2Click(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
    (*FLinkTip: array of: Integer;
    FLinkTbN, FLinkFlN: array of String;
    FLinkDbX: array of TmySQLDatabase;
    *)
    //
    procedure MostraEdicao(Mostra : Boolean; SQLType: TSQLType; Codigo : Integer);
    procedure ReopenDocsIts(Controle: Integer);
    procedure InsUpdIts(SQLType: TSQLType);

  public
    { Public declarations }
    FLinkID1, FLinkTab: array of Variant;
    FCodigo, FControle: Integer;
    //
    procedure ReabreDocsCab(LocCod, Codigo: Integer);
    procedure DefineFiltroLink(CodTabs: array of Integer);
  end;

  var
  FmDocsCab: TFmDocsCab;

implementation

{$R *.DFM}

uses UnMyObjects, Module, MyListas, DmkDAC_PF, MyGlyfs, Principal,
  UnAnotacoes_Tabs;

const
  _Item_Menu_ = 'Item_Menu_';
  _Item_Leng_ = 9;

procedure TFmDocsCab.Altera1Click(Sender: TObject);
begin
  BtAlteraClick(Self);
end;

procedure TFmDocsCab.Altera2Click(Sender: TObject);
begin
  InsUpdIts(stUpd);
end;

procedure TFmDocsCab.BtAlteraClick(Sender: TObject);
begin
  if QrDocsCabCodigo.Value > 0 then
  begin
    MostraEdicao(True, stUpd, 0);
  end;
end;

procedure TFmDocsCab.BtConfirmaClick(Sender: TObject);
var
  Codigo, LinkTab, LinkID1: Integer;
  Nome, Texto: String;
begin
  Codigo  := EdCodigo.ValueVariant;
  Nome    := EdNome.Text;
  LinkTab := EdLinkTab.ValueVariant;
  LinkID1 := EdLinkID1.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o.') then
    Exit;
  Codigo := UMyMod.BPGS1I32('docscab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'docscab', False, [
  'Nome', 'LinkTab', 'LinkID1'], [
  'Codigo'], [
  Nome, LinkTab, LinkID1], [
  Codigo], True) then
  begin
    ReabreDocsCab(Codigo, Codigo);
    MostraEdicao(False, stLok, 0);
  end;
end;

procedure TFmDocsCab.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(False, stLok, 0);
end;

procedure TFmDocsCab.BtExcluiClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if QrDocsCabCodigo.Value > 0 then
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o do item '+
    QrDocsCabNome.Value + '?') = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      Codigo := QrDocsCabCodigo.Value;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM docscab WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := Codigo;
      Dmod.QrUpd.ExecSQL;
      //
      //ReabreDocsCab(0, 0, '');
      ReabreDocsCab(0, 0);
      //
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmDocsCab.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(True, stIns, 0);
end;

procedure TFmDocsCab.BtNenhumClick(Sender: TObject);
begin
  CBLinkTabs.CheckAll(cbUnchecked);
  ReabreDocsCab(0, 0);
end;

procedure TFmDocsCab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDocsCab.BtTudoClick(Sender: TObject);
begin
  CBLinkTabs.CheckAll(cbChecked);
  ReabreDocsCab(0, 0);
end;

procedure TFmDocsCab.CBLinkTabsClickCheck(Sender: TObject);
begin
  ReabreDocsCab(0, 0);
end;

procedure TFmDocsCab.DBGrid1DblClick(Sender: TObject);
var
  NoArq: String;
begin
  if (QrDocsIts.State <> dsInactive) and (QrDocsIts.RecordCount > 0) then
  begin
    NoArq := QrDocsItsCaminho.Value;
    if FileExists(NoArq) then
      ShellExecute(Application.Handle, PChar('open'), PChar(NoArq),
          PChar(''), nil, SW_NORMAL)
    else
      Geral.MB_Aviso('Arquivo n�o localizado:' + sLineBreak + NoArq);
  end;
end;

procedure TFmDocsCab.DefineFiltroLink(CodTabs: array of Integer);
const
  Separador = '@|@';
var
  I, J, L, P, K: Integer;
  Lista: array of string;
  Novo: TMenuItem;
  Dados: String;
begin
  CBLinkTabs.Items.Clear;
  //
  K := High(CodTabs);
  for I := 0 to K do
    CBLinkTabs.Items.Add(ARRAY_ANOTACOES_TAB[CodTabs[I]] + Separador +
    Geral.FF0(CodTabs[I])(* + Separador + Geral.FF0(CodID1s[I])*));
  //
  CBLinkTabs.Sorted := True;
  //
  SetLength(Lista, CBLinkTabs.Items.Count);
  for I := 0 to K do
    Lista[I] := CBLinkTabs.Items[I];
  //
  CBLinkTabs.Sorted := False;
  CBLinkTabs.Clear;
  L := Length(Separador);
  SetLength(FLinkTab, Length(CodTabs));
  {
  //SetLength(FLinkID1, Length(CodTabs));
  SetLength(FLinkTip, Length(CodTabs));
  SetLength(FLinkDbX, Length(CodTabs));
  SetLength(FLinkTbN, Length(CodTabs));
  SetLength(FLinkFlN, Length(CodTabs));
  }
  for I := 0 to K do
  begin
    P := Pos(Separador, Lista[I]);
    CBLinkTabs.Items.Add(Copy(Lista[I], 1, P -1));
    Dados := Copy(Lista[I], P + L);
    FLinkTab[I] := Geral.IMV(Dados);
    //P := Pos(Separador, Dados);
    //FLinkTab[I] := Geral.IMV(Copy(Dados, 1, P - 1));
    //FLinkID1[I] := Geral.IMV(Copy(Dados, P + L));
    {
    for J := 0 to K do
    begin
      if CodTabs[J] = FLinkTab[I] then
      begin
        FLinkTip[I] := LinkTip[J];
        FLinkDbX[I] := LinkDbX[J];
        FLinkTbN[I] := LinkTbN[J];
        FLinkFlN[I] := LinkFlN[J];
      end;
    end;
    }
  end;
  CBLinkTabs.ItemHeight := Trunc(CBLinkTabs.ItemHeight * 1.5);
  //
  ReabreDocsCab(0, 0); // Movido do OnCreate!!
  //
  for I := 0 to K do
  begin
    //ShowMessage (IntToStr(FLinkTab[I]) + ' - ' + IntToStr(FLinkID1[I]));
    Novo := TMenuItem.Create(Self);
    Novo.Name := _Item_Menu_ + Geral.FFN(FLinkTab[I], _Item_Leng_) + '_' + CBLinkTabs.Items[I];
    //Novo.Tag  := FCodTabs[I];
    Novo.Caption := CBLinkTabs.Items[I];
    Novo.OnClick := Inclui1.Onclick;
    try
      Inclui1.Add(Novo);
    except
      Geral.MB_Erro('ERRO!' + sLineBreak + Novo.Caption + sLineBreak +
      Inclui1.Caption);
    end;
  end;
  //
end;

procedure TFmDocsCab.EdLinkTabChange(Sender: TObject);
var
  LinkTab: Integer;
begin
  LinkTab := EdLinkTab.ValueVariant;
  EdLinkTab_TXT.Text := ARRAY_ANOTACOES_TAB[LinkTab];
end;

procedure TFmDocsCab.EdPesquisaChange(Sender: TObject);
begin
{
  if (UpperCase(EdPesquisa.ValueVariant) <> 'PESQUISAR') and
  (Length(EdPesquisa.ValueVariant) > 0) then
    ReabreDocsCab(0, 0, EdPesquisa.ValueVariant)
  else
    ReabreDocsCab(0, 0, '');
}
  ReabreDocsCab(0, 0);
end;

procedure TFmDocsCab.EdPesquisaEnter(Sender: TObject);
begin
  if Length(EdPesquisa.ValueVariant) > 0 then
    EdPesquisa.ValueVariant := '';
end;

procedure TFmDocsCab.EdPesquisaExit(Sender: TObject);
begin
  if Length(EdPesquisa.ValueVariant) = 0 then
    EdPesquisa.ValueVariant := 'Pesquisar';
end;

procedure TFmDocsCab.Exclui1Click(Sender: TObject);
begin
  BtExcluiClick(Self);
end;

procedure TFmDocsCab.Exclui2Click(Sender: TObject);
var
  Controle: Integer;
begin
  Controle := QrDocsItsControle.Value;
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a exclus�o do registro do caminho selecionado?', 'DocsIts',
  'Controle', Controle, Dmod.MyDB) = ID_YES then
  begin
    Controle := UMyMod.ProximoRegistro(QrDocsIts, 'Controle', Controle);
    ReopenDocsIts(Controle);
  end;
end;

procedure TFmDocsCab.FormActivate(Sender: TObject);
begin
  if TFmDocsCab(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmDocsCab.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  ImgTipo.SQLType := stLok;
  //
  //ReabreDocsCab(0, 0, '');
  //FLinkTab := 0;
  SetLength(FLinkID1, MAX_ANOTACOES_TAB + 1);
  for I := 0 to MAX_ANOTACOES_TAB do
    FLinkID1[I] := 0;
  //
  MostraEdicao(False, stLok, 0);
  //PnCont.Align  := alTop;
  PnCont.Align  := alClient;
end;

procedure TFmDocsCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDocsCab.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
end;

procedure TFmDocsCab.Inclui1Click(Sender: TObject);
const
  TxtErr = 'Inclus�o de anota��o cnacelada!' + sLineBreak;
var
  Nome: String;
  LinkTab, LinkID1, I, LinkTip: Integer;
  LinkDbX: TmySQLDataBase;
  LinkTbN, LinkFlN: String;
  Qry: TmySQLQuery;
  //Achou: Boolean;
begin
  //Achou := False;
  LinkID1 := 0;
  Nome := TComponent(Sender).Name;
  if Nome <> 'Inclui1' then
  begin
    Nome := Copy(Nome, pos(_Item_Menu_, Nome) + Length(_Item_Menu_), _Item_Leng_);
    LinkTab := Geral.IMV(Nome);
    if FLinkID1[LinkTab] <> Null then
    begin
      LinkID1 := FLinkID1[LinkTab];
      EdLinkTab.ValueVariant := LinkTab;
      EdLinkID1.ValueVariant := LinkID1;
      BtIncluiClick(Self);
    end else
    begin
      if not (LinkTab in [0..MAX_ANOTACOES_TAB]) then
        Geral.MB_Aviso(TxtErr + 'Tabela n�o definida!')
      else
      if FLinkID1[LinkTab] = Null then
        Geral.MB_Aviso(TxtErr + 'ID de registro n�o definido para a tabela: ' +
        sLineBreak + ARRAY_ANOTACOES_TAB[LinkTab]);
    end;
  end;
end;

procedure TFmDocsCab.Inclui2Click(Sender: TObject);
begin
  InsUpdIts(stIns);
end;

procedure TFmDocsCab.InsUpdIts(SQLType: TSQLType);
const
  Titulo = 'Selecione o arquivo';
var
  Caminho, Dir, Arq: String;
  Codigo, Controle: Integer;
begin
  Codigo         := QrDocsCabCodigo.Value;
  if SQLType = stIns then
  begin
    Controle       := 0;
    Caminho        := '';
    Dir            := 'C:\';
    Arq            := '';
  end else
  begin
    Controle       := QrDocsItsControle.Value;
    Caminho        := QrDocsItsCaminho.Value;
    Dir            := ExtractFileDir(Caminho);
    Arq            := ExtractFileName(Caminho);
  end;

  if MyObjects.FileOpenDialog(self, Dir, Arq, Titulo, '', [], Caminho) then
  begin
    Controle := UMyMod.BPGS1I32(
      'docsits', 'Controle', '', '', tsPos, SQLType, Controle);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'docsits', False, [
    'Codigo', 'Controle', 'Caminho'], [
    ], [
    Codigo, Controle, Caminho], [
    ], True) then
    begin
      ReopenDocsIts(Controle);
    end;
  end;
end;

procedure TFmDocsCab.MostraEdicao(Mostra: Boolean; SQLType: TSQLType;
  Codigo: Integer);
begin
  if Mostra then
  begin
    PnDados.Visible := False;
    PnEdita.Visible := True;
    if SQLType = stIns then
    begin
      EdCodigo.ValueVariant := FormatFloat('000', Codigo);
      EdNome.ValueVariant   := '';
      //
      EdNome.SetFocus;
    end else begin
      EdCodigo.ValueVariant := QrDocsCabCodigo.Value;
      EdNome.ValueVariant   := QrDocsCabNome.Value;
      //
      EdNome.SetFocus;
    end;
  end else begin
    PnDados.Visible := True;
    PnEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmDocsCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabDel(Exclui1, QrDocsCab, QrDocsIts);
end;

procedure TFmDocsCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Inclui2, QrDocsCab);
  MyObjects.HabilitaMenuItemItsUpd(Altera2, QrDocsIts);
  MyObjects.HabilitaMenuItemItsDel(Exclui2, QrDocsIts);
end;

procedure TFmDocsCab.QrDocsCabAfterScroll(DataSet: TDataSet);
begin
  ReopenDocsIts(0);
end;

procedure TFmDocsCab.QrDocsCabBeforeClose(DataSet: TDataSet);
begin
  QrDocsIts.Close;
end;

procedure TFmDocsCab.ReabreDocsCab(LocCod, Codigo: Integer);
var
  Usuario, I, N: Integer;
  Nome, LinkTab: String;
begin
  Screen.Cursor := crHourGlass;
  try
    if (UpperCase(EdPesquisa.ValueVariant) <> 'PESQUISAR') and
    (Length(EdPesquisa.ValueVariant) > 0) then
      Nome := 'AND Nome LIKE "%' + EdPesquisa.ValueVariant + '%"'
    else
      Nome := '';
    //
    LinkTab := '';
    for I := 0 to CBLinkTabs.Items.Count - 1 do
    begin
      if CBLinkTabs.Checked[I] then
      begin
        //LinkTab := LinkTab + ', ' + Geral.FF0(FLinkTab[I]);
        //

        N := FLinkTab[I];
        LinkTab := LinkTab + ' OR (LinkTab=' + Geral.FF0(N);
        ///
        ///
        if N <> CO_ANOTACOES_TAB_0001_UNIVERSAL then
          LinkTab := LinkTab + ' AND LinkID1=' + Geral.FF0(FLinkID1[N]);
        ///
        ///
        LinkTab := LinkTab + ')' + sLineBreak;
      end;
    end;
    if LinkTab <> '' then
    begin
      //LinkTab := 'AND LinkTab IN (' + Copy(LinkTab, 3) + ')';
      // Copy � para tirar o primeiro "OR"
      LinkTab := 'AND (' + sLineBreak + Copy(LinkTab, 4) + ')' + sLineBreak;
      //
      Usuario := VAR_USUARIO;
      UnDmkDAC_PF.AbreMySQLQuery0(QrDocsCab, Dmod.MyDB, [
      'SELECT Codigo, Nome, Ativo ',
      'FROM docscab ',
      'WHERE Codigo > 0 ',
      (*  Ver como usar!
      Geral.ATS_If((Usuario > 0) or (Usuario = -2), [
      'AND UserCad=' + Geral.FF0(Usuario)]),
      *)
      Nome,
      Geral.ATS_If( Codigo > 0, ['AND Codigo=' + Geral.FF0(Codigo)]),
      LinkTab,
      '']);
      if LocCod <> 0 then
        QrDocsCab.Locate('Codigo', Codigo, []);
      //
      EdNome.ValueVariant := QrDocsCabNome.Value;
    end else
      QrDocsCab.Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmDocsCab.ReopenDocsIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDocsIts, Dmod.MyDB, [
  'SELECT *',
  'FROM docsits ',
  'WHERE Codigo=' + Geral.FF0(QrDocsCabCodigo.Value),
  '']);
  //
  if Controle <> 0 then
    QrDocsIts.Locate('Controle', Controle, []);
end;

procedure TFmDocsCab.SbNomeClick(Sender: TObject);
begin
  //DefParams;  // precisa para evitar bug
  //... continuar aqui!
end;

procedure TFmDocsCab.SbNumeroClick(Sender: TObject);
begin
  //DefParams;  // precisa para evitar bug
  //... continuar aqui!

end;

//ver melhor o componente TdmkDBGridZTO

end.



