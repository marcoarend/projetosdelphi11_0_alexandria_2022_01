unit VerifiDBi;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls, Buttons, DB, (*DBTables,*) UnInternalConsts,
  UnMLAGeral, mySQLDbTables, dmkGeral, CheckLst, dmkImage, UnDmkEnums,
  Vcl.Menus, UnGrl_Vars;

Type
  TFmVerifiDBi = class(TForm)
    Panel2: TPanel;
    ProgressBar1: TProgressBar;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    CkEstrutura: TCheckBox;
    CkPergunta: TCheckBox;
    Timer1: TTimer;
    CkEstrutLoc: TCheckBox;
    CkRegObrigat: TCheckBox;
    CkEntidade0: TCheckBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Memo1: TMemo;
    Panel4: TPanel;
    GBAvisos1: TGroupBox;
    Panel13: TPanel;
    Panel7: TPanel;
    Panel14: TPanel;
    Panel6: TPanel;
    LaTempoI: TLabel;
    LaTempoF: TLabel;
    LaTempoT: TLabel;
    Panel8: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    LaAvisoB1: TLabel;
    LaAvisoB2: TLabel;
    LaAvisoG1: TLabel;
    LaAvisoG2: TLabel;
    LaAvisoR1: TLabel;
    LaAvisoR2: TLabel;
    LaAvisoP1: TLabel;
    LaAvisoP2: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GroupBox2: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSair: TBitBtn;
    Panel15: TPanel;
    BtConfirma: TBitBtn;
    PMTabsNoNeed: TPopupMenu;
    Excluitabelas1: TMenuItem;
    Listatabelas1: TMenuItem;
    N2: TMenuItem;
    Marcartodos2: TMenuItem;
    Desmarcartodos2: TMenuItem;
    PMClFldsNoNeed: TPopupMenu;
    Excluicampos1: TMenuItem;
    N1: TMenuItem;
    Marcartodos1: TMenuItem;
    Desmarcartodos1: TMenuItem;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    Panel10: TPanel;
    BtTabsNoNeed: TButton;
    ClTabsNoNeed: TCheckListBox;
    TabSheet3: TTabSheet;
    ClFldsNoNeed: TCheckListBox;
    Panel9: TPanel;
    BtClFldsNoNeed: TButton;
    TabSheet4: TTabSheet;
    ClIdxsNoNeed: TCheckListBox;
    Panel17: TPanel;
    BtClIdxsNoNeed: TButton;
    PMClIdxsNoNeed: TPopupMenu;
    ExcluirIndices1: TMenuItem;
    MenuItem2: TMenuItem;
    MarcarTodos3: TMenuItem;
    DesmarcarTodos3: TMenuItem;
    procedure BtSairClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PMTabsNoNeedPopup(Sender: TObject);
    procedure PMClFldsNoNeedPopup(Sender: TObject);
    procedure Excluitabelas1Click(Sender: TObject);
    procedure Listatabelas1Click(Sender: TObject);
    procedure Excluicampos1Click(Sender: TObject);
    procedure Marcartodos2Click(Sender: TObject);
    procedure Desmarcartodos2Click(Sender: TObject);
    procedure Marcartodos1Click(Sender: TObject);
    procedure Desmarcartodos1Click(Sender: TObject);
    procedure BtTabsNoNeedClick(Sender: TObject);
    procedure BtClFldsNoNeedClick(Sender: TObject);
    procedure BtClIdxsNoNeedClick(Sender: TObject);
    procedure PMClIdxsNoNeedPopup(Sender: TObject);
    procedure ExcluirIndices1Click(Sender: TObject);
    procedure MarcarTodos3Click(Sender: TObject);
    procedure DesmarcarTodos3Click(Sender: TObject);
  private
    { Private declarations }
    procedure AtualizaVersao();
  public
    { Public declarations }
    FVerifi: Boolean;
    FDBExtra: TmySQLDataBase;
    FEmpresa: Integer;
  end;

var
  FmVerifiDBi: TFmVerifiDBi;

implementation

uses UnMyObjects, Module, MyDBCheck, MyListas, Principal, DmkDAC_PF, UMySQLDB;

{$R *.DFM}

procedure TFmVerifiDBi.BtSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVerifiDBi.BtTabsNoNeedClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTabsNoNeed, BtTabsNoNeed);
end;

procedure TFmVerifiDBi.Desmarcartodos1Click(Sender: TObject);
begin
  ClFldsNoNeed.CheckAll(cbUnchecked, true, false);
end;

procedure TFmVerifiDBi.Desmarcartodos2Click(Sender: TObject);
begin
  ClTabsNoNeed.CheckAll(cbUnchecked, true, false);;
end;

procedure TFmVerifiDBi.DesmarcarTodos3Click(Sender: TObject);
begin
  ClIdxsNoNeed.CheckAll(cbUnchecked, true, false);
end;

procedure TFmVerifiDBi.Excluicampos1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  DBCheck.DropCamposTabelas(ClFldsNoNeed, Dmod.MyDBn, Memo1);
end;

procedure TFmVerifiDBi.ExcluirIndices1Click(Sender: TObject);
const
  Aviso = '';
var
  Tabela, IdxNome: String;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  DBCheck.DropIndicesTabelas(ClIdxsNoNeed, Dmod.MyDB, Memo1);
end;

procedure TFmVerifiDBi.Excluitabelas1Click(Sender: TObject);
var
  //P, N,
  I: Integer;
  Tabela: String;
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  if Geral.MB_Pergunta(
  'Deseja realmente excluir as tabelas selecionadas?') = ID_YES then
  begin
    try
      //N := 0;
      for I := ClTabsNoNeed.Items.Count - 1 downto 0 do
      begin
        if ClTabsNoNeed.Checked[I] then
        begin
          Tabela := ClTabsNoNeed.Items[I];
          Dmod.QrWeb.SQL.Clear;
          Dmod.QrWeb.SQL.Add('DROP TABLE ' + Tabela);
          Dmod.QrWeb.ExecSQL;
          //
          ClTabsNoNeed.Items.Delete(I);
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmVerifiDBi.AtualizaVersao;
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE web_params SET ',
    'Web_DBVersao=' + Geral.FF0(CO_VERSAO),
    'WHERE Empresa=' + Geral.FF0(FEmpresa),
    '']);
end;

procedure TFmVerifiDBi.BtClFldsNoNeedClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMClFldsNoNeed, BtClFldsNoNeed);
end;

procedure TFmVerifiDBi.BtClIdxsNoNeedClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMClIdxsNoNeed, BtClIdxsNoNeed);
end;

procedure TFmVerifiDBi.BtConfirmaClick(Sender: TObject);
var
  Versao, Resp: Integer;
  Verifica: Boolean;
begin
  if MyObjects.FIC(FEmpresa = 0, nil, 'Empresa n�o definida na abertura da janela!') then Exit;
  //
  Verifica := False;
  Versao   := USQLDB.ObtemVersaoAppDB(Dmod.MyDB);
  //
  if Versao <= CO_VERSAO then
    Verifica := True
  else
  begin
    Resp := Geral.MB_Pergunta('A vers�o do execut�vel � inferior a do '+
      'banco de dados atual. N�o � recomendado executar a verifica��o com '+
      'esta vers�o mais antiga, pois dados poder�o ser perdidos definitivamente.'+
      'Confirma assim mesmo a verifica��o?');
    if Resp = ID_YES then
      Verifica := True;
  end;
  if Verifica then
  begin
    if CkEntidade0.Checked then
    begin
      Dmod.QrWeb.SQL.Clear;
      Dmod.QrWeb.SQL.Add('DELETE FROM entidades');
      Dmod.QrWeb.SQL.Add('WHERE Codigo=0');
      Dmod.QrWeb.ExecSQL;
    end;
    if DBCheck.EfetuaVerificacoes(Dmod.MyDBn, nil, Memo1,
      CkEstrutura.Checked, CkEstrutLoc.Checked, True,
      CkPergunta.Checked, CkRegObrigat.Checked, LaAvisoP1, LaAvisoP2,
      LaAvisoR1, LaAvisoR2, LaAvisoB1, LaAvisoB2, LaAvisoG1, LaAvisoG2,
      LaTempoI, LaTempoF, LaTempoT, ClTabsNoNeed, ProgressBar1, ClFldsNoNeed,
      ClIdxsNoNeed) then
    begin
      AtualizaVersao;
    end;
    BtSair.Enabled      := True;
    CkPergunta.Checked  := False;
    CkEstrutLoc.Checked := False;
  end;
end;

procedure TFmVerifiDBi.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  Timer1.Enabled := FVerifi;
end;

procedure TFmVerifiDBi.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FEmpresa        := 0;
  //
  Dmod.QrWeb.Database := Dmod.MyDBn;
  //
  PageControl2.ActivePageIndex := 0;
end;

procedure TFmVerifiDBi.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVerifiDBi.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  //
  if MyObjects.FIC(FEmpresa = 0, nil, 'Empresa n�o definida na abertura da janela!') then Exit;
  //
  if DBCheck.EfetuaVerificacoes(Dmod.MyDBn, nil, Memo1, CkEstrutura.Checked,
    False, True, False, CkRegObrigat.Checked, LaAvisoP1, LaAvisoP2, LaAvisoR1,
    LaAvisoR2, LaAvisoB1, LaAvisoB2, LaAvisoG1, LaAvisoG2, LaTempoI, LaTempoF,
    LaTempoT, ClTabsNoNeed, ProgressBar1, ClFldsNoNeed, ClIdxsNoNeed) then
  begin
    AtualizaVersao;
  end;
  BtSair.Enabled      := True;
  CkPergunta.Checked  := False;
  CkEstrutLoc.Checked := False;
end;

procedure TFmVerifiDBi.FormShow(Sender: TObject);
begin
  //FmPrincipal.Hide;
end;

procedure TFmVerifiDBi.Listatabelas1Click(Sender: TObject);
begin
  Geral.MB_Info(ClTabsNoNeed.Items.Text);
end;

procedure TFmVerifiDBi.Marcartodos1Click(Sender: TObject);
begin
  ClFldsNoNeed.CheckAll(cbChecked, false, true);
end;

procedure TFmVerifiDBi.Marcartodos2Click(Sender: TObject);
begin
  ClTabsNoNeed.CheckAll(cbChecked, false, true);
end;

procedure TFmVerifiDBi.MarcarTodos3Click(Sender: TObject);
begin
  ClIdxsNoNeed.CheckAll(cbChecked, false, true);
end;

procedure TFmVerifiDBi.PMClFldsNoNeedPopup(Sender: TObject);
var
  I, N: Integer;
begin
  N := 0;
  for I := 0 to ClFldsNoNeed.Count - 1 do
  begin
    if ClFldsNoNeed.Checked[I] then
      N := N + 1;
  end;
  //
  Excluicampos1.Enabled := N > 0;
end;

procedure TFmVerifiDBi.PMClIdxsNoNeedPopup(Sender: TObject);
var
  I, N: Integer;
begin
  N := 0;
  for I := 0 to ClIdxsNoNeed.Count - 1 do
  begin
    if ClIdxsNoNeed.Checked[I] then
      N := N + 1;
  end;
  //
  ExcluirIndices1.Enabled := N > 0;
end;

procedure TFmVerifiDBi.PMTabsNoNeedPopup(Sender: TObject);
var
  I, N: Integer;
begin
  N := 0;
  for I := 0 to ClTabsNoNeed.Count - 1 do
  begin
    if ClTabsNoNeed.Checked[I] then
      N := N + 1;
  end;
  //
  Excluitabelas1.Enabled := N > 0;
  Listatabelas1.Enabled  := N > 0;
end;

procedure TFmVerifiDBi.FormHide(Sender: TObject);
begin
  //FmPrincipal.Show;
end;

end.
