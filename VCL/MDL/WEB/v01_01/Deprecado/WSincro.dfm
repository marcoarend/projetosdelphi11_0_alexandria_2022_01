object FmWSincro: TFmWSincro
  Left = 339
  Top = 185
  Caption = 'WEB-SINCR-001 :: Sincronismo Dados Web'
  ClientHeight = 518
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 470
    Width = 792
    Height = 48
    Align = alBottom
    TabOrder = 1
    object Label1: TLabel
      Left = 280
      Top = 4
      Width = 36
      Height = 13
      Caption = 'Tempo:'
    end
    object Label2: TLabel
      Left = 140
      Top = 4
      Width = 30
      Height = 13
      Caption = 'In'#237'cio:'
    end
    object Label3: TLabel
      Left = 336
      Top = 4
      Width = 41
      Height = 13
      Caption = 'T'#233'rmino:'
    end
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 105
      Height = 40
      Caption = 'Sincroni&zar'
      Enabled = False
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 680
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object EdTempo: TdmkEdit
      Left = 280
      Top = 20
      Width = 53
      Height = 21
      TabOrder = 2
      FormatType = dmktfTime
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfLong
      Texto = '00:00:00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0d
    end
    object EdIni: TdmkEdit
      Left = 140
      Top = 20
      Width = 137
      Height = 21
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfLong
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdFim: TdmkEdit
      Left = 336
      Top = 20
      Width = 137
      Height = 21
      ReadOnly = True
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfLong
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Caption = 'Sincronismo Dados Web'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 790
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 422
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel4: TPanel
      Left = 0
      Top = 241
      Width = 792
      Height = 181
      Align = alClient
      TabOrder = 1
      object RE1: TRichEdit
        Left = 1
        Top = 1
        Width = 790
        Height = 179
        Align = alClient
        TabOrder = 0
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 241
      Align = alTop
      TabOrder = 0
      object Memo1: TMemo
        Left = 732
        Top = 8
        Width = 545
        Height = 21
        Lines.Strings = (
          'Memo1')
        TabOrder = 0
        Visible = False
      end
      object RGTipo: TRadioGroup
        Left = 1
        Top = 148
        Width = 790
        Height = 44
        Align = alBottom
        Caption = ' Tipo de sincroniza'#231#227'o: '
        Columns = 2
        ItemIndex = 1
        Items.Strings = (
          'Somente dados modificados'
          'Atualizar todos dados da web')
        TabOrder = 1
      end
      object CkLista: TCheckListBox
        Left = 1
        Top = 1
        Width = 328
        Height = 147
        Align = alLeft
        ItemHeight = 13
        TabOrder = 2
      end
      object LBTabLoc: TListBox
        Left = 329
        Top = 1
        Width = 121
        Height = 147
        Align = alLeft
        ItemHeight = 13
        TabOrder = 3
      end
      object LBTabWeb: TListBox
        Left = 450
        Top = 1
        Width = 121
        Height = 147
        Align = alLeft
        ItemHeight = 13
        TabOrder = 4
      end
      object LBKeyFl1: TListBox
        Left = 571
        Top = 1
        Width = 121
        Height = 147
        Align = alLeft
        ItemHeight = 13
        TabOrder = 5
      end
      object LBDtaTyp: TListBox
        Left = 692
        Top = 1
        Width = 37
        Height = 147
        Align = alLeft
        ItemHeight = 13
        TabOrder = 6
      end
      object Panel5: TPanel
        Left = 1
        Top = 192
        Width = 790
        Height = 48
        Align = alBottom
        TabOrder = 7
        object GBLct: TGroupBox
          Left = 1
          Top = 1
          Width = 268
          Height = 46
          Align = alLeft
          Caption = ' Lan'#231'amentos financeiros: '
          TabOrder = 0
          object CkLct: TCheckBox
            Left = 12
            Top = 20
            Width = 145
            Height = 17
            Caption = 'Somente n'#227'o quitados.'
            Checked = True
            State = cbChecked
            TabOrder = 0
          end
        end
      end
    end
  end
  object QrParc: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT COUNT(*) Parcelamentos'
      'FROM bloqparc'
      'WHERE NOT (Novo IN (9,-9))')
    Left = 248
    Top = 257
    object QrParcParcelamentos: TLargeintField
      FieldName = 'Parcelamentos'
      Required = True
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 224
    Top = 241
  end
  object Qr1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 276
    Top = 257
  end
  object Qr2: TmySQLQuery
    Database = Dmod.MyDBn
    Left = 304
    Top = 257
  end
  object Qr3: TmySQLQuery
    Database = Dmod.MyDBn
    Left = 332
    Top = 257
  end
  object Query: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM aduppgs'
      'WHERE AlterWeb=1')
    Left = 360
    Top = 257
  end
  object QrQtdL: TmySQLQuery
    Database = Dmod.MyDB
    Left = 260
    Top = 313
  end
  object QrQtdW: TmySQLQuery
    Database = Dmod.MyDBn
    Left = 296
    Top = 313
  end
  object QrRegL: TmySQLQuery
    Database = Dmod.MyDB
    Left = 260
    Top = 341
  end
  object QrRegW: TmySQLQuery
    Database = Dmod.MyDBn
    Left = 296
    Top = 341
  end
  object Timer2: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer2Timer
    Left = 252
    Top = 240
  end
end
