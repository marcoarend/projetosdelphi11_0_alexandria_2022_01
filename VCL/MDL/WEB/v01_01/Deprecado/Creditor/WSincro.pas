unit WSincro;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, math, Db, mySQLDbTables, dmkEdit,
  UMySQLModule, dmkGeral;

type
  TFmWSincro = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    Panel4: TPanel;
    QrCount: TmySQLQuery;
    dmkEdit1: TdmkEdit;
    EdLo: TdmkEdit;
    dmkEdit3: TdmkEdit;
    EdLi: TdmkEdit;
    Timer1: TTimer;
    QrCountItens: TLargeintField;
    ST1: TStaticText;
    Qr1: TmySQLQuery;
    Qr2: TmySQLQuery;
    QrLotesIts: TmySQLQuery;
    Qr3: TmySQLQuery;
    QrLotes: TmySQLQuery;
    QrLotesCodigo: TIntegerField;
    QrLotesItsControle: TIntegerField;
    QrEmitBAC: TmySQLQuery;
    QrEmitBACBAC: TWideStringField;
    dmkEdit2: TdmkEdit;
    EdBa: TdmkEdit;
    EdEm: TdmkEdit;
    dmkEdit6: TdmkEdit;
    QrEmitCPF: TmySQLQuery;
    QrEmitCPFCPF: TWideStringField;
    QrSacados: TmySQLQuery;
    QrSacadosCNPJ: TWideStringField;
    dmkEdit4: TdmkEdit;
    EdSa: TdmkEdit;
    dmkEdit5: TdmkEdit;
    EdEn: TdmkEdit;
    QrEntidades: TmySQLQuery;
    dmkEdit7: TdmkEdit;
    EdCt: TdmkEdit;
    dmkEdit8: TdmkEdit;
    EdSo: TdmkEdit;
    QrContratos: TmySQLQuery;
    QrSocios: TmySQLQuery;
    Memo1: TMemo;
    QrSociosControle: TAutoIncField;
    QrContratosCodigo: TIntegerField;
    QrEntidadesCodigo: TIntegerField;
    dmkEdit9: TdmkEdit;
    EdCo: TdmkEdit;
    QrEmLot: TmySQLQuery;
    dmkEdit10: TdmkEdit;
    EdTx: TdmkEdit;
    dmkEdit11: TdmkEdit;
    EdLx: TdmkEdit;
    QrLotesTxs: TmySQLQuery;
    QrTaxas: TmySQLQuery;
    QrLotesTxsControle: TIntegerField;
    QrTaxasCodigo: TIntegerField;
    dmkEdit12: TdmkEdit;
    EdOB: TdmkEdit;
    EdOc: TdmkEdit;
    dmkEdit15: TdmkEdit;
    dmkEdit16: TdmkEdit;
    EdPO: TdmkEdit;
    QrOcorrBank: TmySQLQuery;
    QrOcorreu: TmySQLQuery;
    QrOcorrPg: TmySQLQuery;
    QrOcorrBankCodigo: TIntegerField;
    QrOcorreuCodigo: TIntegerField;
    QrOcorrPgCodigo: TIntegerField;
    dmkEdit13: TdmkEdit;
    EdCD: TdmkEdit;
    EdPC: TdmkEdit;
    dmkEdit18: TdmkEdit;
    QrAlinIts: TmySQLQuery;
    QrAlinPgs: TmySQLQuery;
    QrAlinPgsCodigo: TIntegerField;
    QrAlinItsCodigo: TIntegerField;
    dmkEdit14: TdmkEdit;
    EdPD: TdmkEdit;
    QrAdupPgs: TmySQLQuery;
    QrAdupPgsControle: TIntegerField;
    RGTipo: TRadioGroup;
    QrCartas: TmySQLQuery;
    QrCartasCodigo: TIntegerField;
    EdCa: TdmkEdit;
    dmkEdit19: TdmkEdit;
    dmkEdit17: TdmkEdit;
    EdBC: TdmkEdit;
    QrBancos: TmySQLQuery;
    IntegerField1: TIntegerField;
    dmkEdit20: TdmkEdit;
    EdFE: TdmkEdit;
    QrFeriados: TmySQLQuery;
    QrFeriadosData: TDateField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
    FArqWeb: String;
    //
    (*FTabLoc, FTabWeb, FCampo: String;
    FQuery: TmySQLQuery;
    FdataType: Integer;
    BaseDados: TmySQLDatabase;*)
    function ExportaDadosAlteradosParaWeb(TabLoc, TabWeb, Campo: String;
             Query: TmySQLQuery; dataType: Integer; BaseDados: TmySQLDatabase;
             Registros: Integer): String;
    function ExportaTodaTabelaParaWeb(TabLoc, TabWeb, Campo: String;
             Query: TmySQLQuery; dataType: Integer; BaseDados: TmySQLDatabase): String;
    procedure ExportaDadosParaWeb;
    //procedure ObtemConfigDeTabelas(Tabela: String);


  public
    { Public declarations }
  end;

  var
  FmWSincro: TFmWSincro;

implementation

{$R *.DFM}

uses Module, Principal, UnMyObjects;

procedure TFmWSincro.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWSincro.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  Timer1.Enabled := True;
end;

procedure TFmWSincro.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmWSincro.FormCreate(Sender: TObject);
begin
  FArqWeb := 'C:/Dermatek/Web/SQL_%s.%s';
  ForceDirectories('C:/Dermatek/Web');
end;

procedure TFmWSincro.BtOKClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  ExportaDadosParaWeb;
  Screen.Cursor := crDefault;
end;

procedure TFmWSincro.ExportaDadosParaWeb;
begin
  //   Lotes
  ExportaDadosAlteradosParaWeb('lotes', 'lotes', 'codigo', QrLotes, 1,
    Dmod.MyDB, Geral.IMV(Edlo.Text));

  //   LotesIts
  ExportaDadosAlteradosParaWeb('lotesits', 'lotesits', 'controle', QrLotesIts, 1,
    Dmod.MyDB, Geral.IMV(Edli.Text));

  //   EmitBAC
  ExportaDadosAlteradosParaWeb('emitbac', 'wemitbac', 'BAC', QrEmitBAC, 0,
    Dmod.MyDB, Geral.IMV(EdBa.Text));

  //   Cartas
  ExportaDadosAlteradosParaWeb('cartas', 'cartas', 'Codigo', QrCartas, 1,
    Dmod.MyDB, Geral.IMV(EdCa.Text));

  //   EmitCPF
  ExportaDadosAlteradosParaWeb('emitcpf', 'wemitcpf', 'CPF', QrEmitCPF, 0,
    Dmod.MyDB, Geral.IMV(EdEm.Text));

  //   Sacados
  ExportaDadosAlteradosParaWeb('sacados', 'wsacados', 'CNPJ', QrSacados, 0,
    Dmod.MyDB, Geral.IMV(EdSa.Text));

  //   Contratos
  ExportaDadosAlteradosParaWeb('contratos', 'contratos', 'codigo', QrContratos, 1,
    Dmod.MyDB, Geral.IMV(EdCt.Text));

  //   Entidades
  ExportaDadosAlteradosParaWeb('entidades', 'entidades', 'codigo', QrEntidades, 1,
    Dmod.MyDB, Geral.IMV(EdEn.Text));

  //   Socios
  ExportaDadosAlteradosParaWeb('socios', 'socios', 'controle', QrSocios, 1,
    Dmod.MyDB, Geral.IMV(EdSo.Text));

  //   Taxas
  ExportaDadosAlteradosParaWeb('taxas', 'taxas', 'codigo', QrTaxas, 1,
    Dmod.MyDB, Geral.IMV(EdTx.Text));

  //   LotesTxs
  ExportaDadosAlteradosParaWeb('lotestxs', 'lotestxs', 'controle', QrLotesTxs, 1,
    Dmod.MyDB, Geral.IMV(EdLx.Text));

  //   OcorrBank
  ExportaDadosAlteradosParaWeb('ocorbank', 'ocorbank', 'codigo', QrOcorrBank, 1,
    Dmod.MyDB, Geral.IMV(EdOB.Text));

  //   Ocorreu
  ExportaDadosAlteradosParaWeb('ocorreu', 'ocorreu', 'codigo', QrOcorreu, 1,
    Dmod.MyDB, Geral.IMV(EdOc.Text));

  //   OcorrPG
  ExportaDadosAlteradosParaWeb('ocorrpg', 'ocorrpg', 'codigo', QrOcorrPG, 1,
    Dmod.MyDB, Geral.IMV(EdPO.Text));

  //   AlinIts
  ExportaDadosAlteradosParaWeb('alinits', 'alinits', 'codigo', QrAlinIts, 1,
    Dmod.MyDB, Geral.IMV(EdCD.Text));

  //   AlinPgs
  ExportaDadosAlteradosParaWeb('alinpgs', 'alinpgs', 'codigo', QrAlinPgs, 1,
    Dmod.MyDB, Geral.IMV(EdPC.Text));

  //   AdupPgs
  ExportaDadosAlteradosParaWeb('aduppgs', 'aduppgs', 'controle', QrAdupPgs, 1,
    Dmod.MyDB, Geral.IMV(EdPD.Text));

  //   Bancos
  ExportaDadosAlteradosParaWeb('bancos', 'bancos', 'codigo', QrBancos, 1,
    Dmod.MyDB, Geral.IMV(EdBC.Text));

  //   Feriados
  ExportaDadosAlteradosParaWeb('feriados', 'feriados', 'data', QrFeriados, 1,
    Dmod.MyDB, Geral.IMV(EdFE.Text));

  // Coligadas


  //   EmLot
  ExportaDadosAlteradosParaWeb('emlot', 'emlot', 'codigo', QrEmLot, 1,
    Dmod.MyDB1, Geral.IMV(EdCo.Text));

  //
  ST1.Caption := 'Sincronização finalizada!';
end;

procedure TFmWSincro.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  //
  QrCount.Close;
  QrCount.SQL.Clear;
  QrCount.SQL.Add('SELECT COUNT(AlterWeb) Itens');
  QrCount.SQL.Add('FROM lotes');
  QrCount.SQL.Add('WHERE AlterWeb=1');
  QrCount.Open;
  EdLo.Text := IntToStr(QrCountItens.Value);
  //
  QrCount.Close;
  QrCount.SQL.Clear;
  QrCount.SQL.Add('SELECT COUNT(AlterWeb) Itens');
  QrCount.SQL.Add('FROM lotesits');
  QrCount.SQL.Add('WHERE AlterWeb=1');
  QrCount.Open;
  EdLi.Text := IntToStr(QrCountItens.Value);
  //
  QrCount.Close;
  QrCount.SQL.Clear;
  QrCount.SQL.Add('SELECT COUNT(AlterWeb) Itens');
  QrCount.SQL.Add('FROM emitbac');
  QrCount.SQL.Add('WHERE AlterWeb=1');
  QrCount.Open;
  EdBa.Text := IntToStr(QrCountItens.Value);
  //
  QrCount.Close;
  QrCount.SQL.Clear;
  QrCount.SQL.Add('SELECT COUNT(AlterWeb) Itens');
  QrCount.SQL.Add('FROM emitcpf');
  QrCount.SQL.Add('WHERE AlterWeb=1');
  QrCount.Open;
  EdEm.Text := IntToStr(QrCountItens.Value);
  //
  QrCount.Close;
  QrCount.SQL.Clear;
  QrCount.SQL.Add('SELECT COUNT(AlterWeb) Itens');
  QrCount.SQL.Add('FROM sacados');
  QrCount.SQL.Add('WHERE AlterWeb=1');
  QrCount.Open;
  EdSa.Text := IntToStr(QrCountItens.Value);
  //
  QrCount.Close;
  QrCount.SQL.Clear;
  QrCount.SQL.Add('SELECT COUNT(AlterWeb) Itens');
  QrCount.SQL.Add('FROM entidades');
  QrCount.SQL.Add('WHERE AlterWeb=1');
  QrCount.Open;
  EdEn.Text := IntToStr(QrCountItens.Value);
  //
  QrCount.Close;
  QrCount.SQL.Clear;
  QrCount.SQL.Add('SELECT COUNT(AlterWeb) Itens');
  QrCount.SQL.Add('FROM socios');
  QrCount.SQL.Add('WHERE AlterWeb=1');
  QrCount.Open;
  EdSo.Text := IntToStr(QrCountItens.Value);
  //
  QrCount.Close;
  QrCount.SQL.Clear;
  QrCount.SQL.Add('SELECT COUNT(AlterWeb) Itens');
  QrCount.SQL.Add('FROM contratos');
  QrCount.SQL.Add('WHERE AlterWeb=1');
  QrCount.Open;
  EdCt.Text := IntToStr(QrCountItens.Value);
  //
  QrCount.Close;
  QrCount.SQL.Clear;
  QrCount.SQL.Add('SELECT COUNT(AlterWeb) Itens');
  QrCount.SQL.Add('FROM taxas');
  QrCount.SQL.Add('WHERE AlterWeb=1');
  QrCount.Open;
  EdTx.Text := IntToStr(QrCountItens.Value);
  //
  QrCount.Close;
  QrCount.SQL.Clear;
  QrCount.SQL.Add('SELECT COUNT(AlterWeb) Itens');
  QrCount.SQL.Add('FROM lotestxs');
  QrCount.SQL.Add('WHERE AlterWeb=1');
  QrCount.Open;
  EdLx.Text := IntToStr(QrCountItens.Value);
  //
  QrCount.Close;
  QrCount.SQL.Clear;
  QrCount.SQL.Add('SELECT COUNT(AlterWeb) Itens');
  QrCount.SQL.Add('FROM ocorbank');
  QrCount.SQL.Add('WHERE AlterWeb=1');
  QrCount.Open;
  EdOB.Text := IntToStr(QrCountItens.Value);
  //
  QrCount.Close;
  QrCount.SQL.Clear;
  QrCount.SQL.Add('SELECT COUNT(AlterWeb) Itens');
  QrCount.SQL.Add('FROM ocorreu');
  QrCount.SQL.Add('WHERE AlterWeb=1');
  QrCount.Open;
  EdOc.Text := IntToStr(QrCountItens.Value);
  //
  QrCount.Close;
  QrCount.SQL.Clear;
  QrCount.SQL.Add('SELECT COUNT(AlterWeb) Itens');
  QrCount.SQL.Add('FROM ocorrpg');
  QrCount.SQL.Add('WHERE AlterWeb=1');
  QrCount.Open;
  EdPO.Text := IntToStr(QrCountItens.Value);
  //
  QrCount.Close;
  QrCount.SQL.Clear;
  QrCount.SQL.Add('SELECT COUNT(AlterWeb) Itens');
  QrCount.SQL.Add('FROM alinits');
  QrCount.SQL.Add('WHERE AlterWeb=1');
  QrCount.Open;
  EdCD.Text := IntToStr(QrCountItens.Value);
  //
  QrCount.Close;
  QrCount.SQL.Clear;
  QrCount.SQL.Add('SELECT COUNT(AlterWeb) Itens');
  QrCount.SQL.Add('FROM alinpgs');
  QrCount.SQL.Add('WHERE AlterWeb=1');
  QrCount.Open;
  EdPC.Text := IntToStr(QrCountItens.Value);
  //
  QrCount.Close;
  QrCount.SQL.Clear;
  QrCount.SQL.Add('SELECT COUNT(AlterWeb) Itens');
  QrCount.SQL.Add('FROM aduppgs');
  QrCount.SQL.Add('WHERE AlterWeb=1');
  QrCount.Open;
  EdPD.Text := IntToStr(QrCountItens.Value);
  //
  QrCount.Close;
  QrCount.SQL.Clear;
  QrCount.SQL.Add('SELECT COUNT(AlterWeb) Itens');
  QrCount.SQL.Add('FROM cartas');
  QrCount.SQL.Add('WHERE AlterWeb=1');
  QrCount.Open;
  EdCa.Text := IntToStr(QrCountItens.Value);
  //
  QrCount.Close;
  QrCount.SQL.Clear;
  QrCount.SQL.Add('SELECT COUNT(AlterWeb) Itens');
  QrCount.SQL.Add('FROM bancos');
  QrCount.SQL.Add('WHERE AlterWeb=1');
  QrCount.Open;
  EdBC.Text := IntToStr(QrCountItens.Value);
  //
  QrCount.Close;
  QrCount.SQL.Clear;
  QrCount.SQL.Add('SELECT COUNT(AlterWeb) Itens');
  QrCount.SQL.Add('FROM feriados');
  QrCount.SQL.Add('WHERE AlterWeb=1');
  QrCount.Open;
  EdFE.Text := IntToStr(QrCountItens.Value);

  if FmPrincipal.FConnections > 0 then
  begin
    QrCount.Database := Dmod.MyDB1;
    //
    QrCount.Close;
    QrCount.SQL.Clear;
    QrCount.SQL.Add('SELECT COUNT(AlterWeb) Itens');
    QrCount.SQL.Add('FROM emlot');
    QrCount.SQL.Add('WHERE AlterWeb=1');
    QrCount.Open;
    EdCo.Text := IntToStr(QrCountItens.Value);


    //
    QrCount.Database := Dmod.MyDB;
  end;
  //
end;

function TFmWSincro.ExportaDadosAlteradosParaWeb(TabLoc, TabWeb, Campo: String;
  Query: TmySQLQuery; dataType: Integer; BaseDados: TmySQLDatabase;
  Registros: Integer): String;
var
  Arq: String;
  F: TextFile;
  S, Campos, Prefix, txt: String;
begin
  if RGTipo.ItemIndex = 1 then
  begin
    ExportaTodaTabelaParaWeb(TabLoc, TabWeb, Campo, Query, dataType,
      BaseDados);
  end else if Registros = 0 then Exit;
  Qr1.Close;
  Qr1.Database := BaseDados;
  Campos := '';
  Prefix := '';
  ST1.Caption := Uppercase(TabWeb) + ' - Obtendo campos da tabela na web...';
  Update;
  Application.ProcessMessages;
  Arq := Format(FArqWeb, [TabWeb, 'sql']);
  if FileExists(Arq) then
  begin
    AssignFile(F, Arq);
    Reset(F);
    while not Eof(F) do
    begin
      Readln(F, S);
      Campos := Campos + S;
    end;
    CloseFile(F);
  end else begin
    Campos := UMyMod.ObtemCamposDeTabelaIdentica(
      Dmod.MyDBn, TabWeb, Prefix);
    Memo1.Text := Campos;
    Memo1.Lines.SaveToFile(Arq);
  end;

  //

  ST1.Caption := Uppercase(TabWeb) + ' - Exportando para arquivo temporário...';
  Update;
  Arq := Format(FArqWeb, [TabWeb, 'txt']);
  if FileExists(Arq) then DeleteFile(Arq);
  Application.ProcessMessages;
  Qr1.SQL.Clear;
  Qr1.SQL.Add('SELECT '+Campos);
  Qr1.SQL.Add('FROM '+ TabLoc);
  Qr1.SQL.Add('WHERE AlterWeb = 1');
  Qr1.SQL.Add('INTO OUTFILE "' + Arq + '"');
  Qr1.ExecSQL;

  //

  ST1.Caption := Uppercase(TabWeb) + ' - Preparando para excluir registros duplicados na web...';
  Update;
  Application.ProcessMessages;
  Query.Close;
  Query.Open;
  Qr3.SQL.Clear;
  Qr3.SQL.Add('DELETE FROM ' + TabWeb + ' WHERE ' + Campo + ' in (');
  txt := '';
  Query.First;
  while not Query.Eof do
  begin
    case dataType of
      1: txt := txt + IntToStr(Query.FieldByName(Campo).Value)+',';
      else txt := txt + '"' + Query.FieldByName(Campo).Value+'",';
    end;
    Query.Next;
  end;
  if txt <> '' then
  begin
    txt[Length(txt)] := ')';
    Qr3.SQL.Add(txt);
    //

    ST1.Caption := Uppercase(TabWeb) + ' - Excluindo registros duplicados na web...';
    Update;
    Application.ProcessMessages;
    Qr3.ExecSQL;
  end;  
  //
  ST1.Caption := Uppercase(TabWeb) + ' - Importando de arquivo temporário...';
  Update;
  Application.ProcessMessages;
  Qr2.SQL.Clear;
  Qr2.SQL.Add('LOAD DATA LOCAL INFILE "' + Arq + '"');
  Qr2.SQL.Add('INTO Table '+TabWeb);
  Qr2.ExecSQL;
  //
  Qr1.SQL.Clear;
  Qr1.SQL.Add('UPDATE '+tabloc+' SET AlterWeb=0');
  Qr1.ExecSQL;
end;

function TFmWSincro.ExportaTodaTabelaParaWeb(TabLoc, TabWeb, Campo: String;
Query: TmySQLQuery; dataType: Integer; BaseDados: TmySQLDatabase): String;
var
  Arq: String;
  F: TextFile;
  S, Campos, Prefix: String;
begin
  Qr1.Close;
  Qr1.Database := BaseDados;
  Campos := '';
  Prefix := '';
  ST1.Caption := Uppercase(TabWeb) + ' - Obtendo campos da tabela na web...';
  Update;
  Application.ProcessMessages;
  Arq := Format(FArqWeb, [TabWeb, 'sql']);
  if FileExists(Arq) then
  begin
    AssignFile(F, Arq);
    Reset(F);
    while not Eof(F) do
    begin
      Readln(F, S);
      Campos := Campos + S;
    end;
    CloseFile(F);
  end else begin
    Campos := UMyMod.ObtemCamposDeTabelaIdentica(
      Dmod.MyDBn, TabWeb, Prefix);
    Memo1.Text := Campos;
    Memo1.Lines.SaveToFile(Arq);
  end;

  //

  ST1.Caption := Uppercase(TabWeb) + ' - Exportando para arquivo temporário...';
  Update;
  Arq := Format(FArqWeb, [TabWeb, 'txt']);
  if FileExists(Arq) then DeleteFile(Arq);
  Application.ProcessMessages;
  Qr1.SQL.Clear;
  Qr1.SQL.Add('SELECT '+Campos);
  Qr1.SQL.Add('FROM '+ TabLoc);
  //Qr1.SQL.Add('WHERE AlterWeb = 1');
  Qr1.SQL.Add('INTO OUTFILE "' + Arq + '"');
  Qr1.ExecSQL;

  //

  ST1.Caption := Uppercase(TabWeb) + ' - Excluindo registros duplicados na web...';
  Update;
  Application.ProcessMessages;
  Query.Close;
  Query.Open;
  Qr3.SQL.Clear;
  Qr3.SQL.Add('DELETE FROM ' + TabWeb );
  Application.ProcessMessages;
  Qr3.ExecSQL;
  //
  ST1.Caption := Uppercase(TabWeb) + ' - Importando de arquivo temporário...';
  Update;
  Application.ProcessMessages;
  Qr2.SQL.Clear;
  Qr2.SQL.Add('LOAD DATA LOCAL INFILE "' + Arq + '"');
  Qr2.SQL.Add('INTO Table '+TabWeb);
  Qr2.ExecSQL;
  //
  Qr1.SQL.Clear;
  Qr1.SQL.Add('UPDATE '+tabloc+' SET AlterWeb=0');
  Qr1.ExecSQL;
end;

(*procedure ObtemConfigDeTabelas(Tabela: String);
begin
  if Uppercase(Tabela) = Uppercase('') then
    FTabLoc, FTabWeb, FCampo: String;
    FQuery: TmySQLQuery;
    FdataType: Integer;
    BaseDados: TmySQLDatabase;
  begin
  end else
end;*)

end.

