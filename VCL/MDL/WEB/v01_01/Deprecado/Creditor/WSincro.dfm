object FmWSincro: TFmWSincro
  Left = 339
  Top = 185
  Caption = 'Sincronismo Dados Web'
  ClientHeight = 461
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 413
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 105
      Height = 40
      Caption = 'Sincroni&zar'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 672
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Sincronismo Dados Web'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 782
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 365
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel4: TPanel
      Left = 0
      Top = 240
      Width = 784
      Height = 125
      Align = alClient
      TabOrder = 1
      object ST1: TStaticText
        Left = 1
        Top = 1
        Width = 38
        Height = 17
        Align = alTop
        Caption = 'Avisos:'
        TabOrder = 0
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 240
      Align = alTop
      TabOrder = 0
      object dmkEdit1: TdmkEdit
        Left = 8
        Top = 12
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Lotes'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Lotes'
      end
      object EdLo: TdmkEdit
        Left = 192
        Top = 12
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object dmkEdit3: TdmkEdit
        Left = 8
        Top = 36
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Itens de lotes'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Itens de lotes'
      end
      object EdLi: TdmkEdit
        Left = 192
        Top = 36
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object dmkEdit2: TdmkEdit
        Left = 272
        Top = 60
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 18
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'BACs'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'BACs'
      end
      object EdBa: TdmkEdit
        Left = 456
        Top = 60
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 19
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdEm: TdmkEdit
        Left = 456
        Top = 12
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 15
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object dmkEdit6: TdmkEdit
        Left = 272
        Top = 12
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 14
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Emitentes'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Emitentes'
      end
      object dmkEdit4: TdmkEdit
        Left = 272
        Top = 36
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 16
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Sacados'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Sacados'
      end
      object EdSa: TdmkEdit
        Left = 456
        Top = 36
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 17
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object dmkEdit5: TdmkEdit
        Left = 272
        Top = 84
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 20
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Entidades'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Entidades'
      end
      object EdEn: TdmkEdit
        Left = 456
        Top = 84
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 21
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object dmkEdit7: TdmkEdit
        Left = 536
        Top = 12
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 28
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Contratos'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Contratos'
      end
      object EdCt: TdmkEdit
        Left = 720
        Top = 12
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 29
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object dmkEdit8: TdmkEdit
        Left = 272
        Top = 108
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 22
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'S'#243'cios'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'S'#243'cios'
      end
      object EdSo: TdmkEdit
        Left = 456
        Top = 108
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 23
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object Memo1: TMemo
        Left = 8
        Top = -12
        Width = 545
        Height = 21
        Lines.Strings = (
          'Memo1')
        TabOrder = 41
        Visible = False
      end
      object dmkEdit9: TdmkEdit
        Left = 272
        Top = 132
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 24
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Coligadas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Coligadas'
      end
      object EdCo: TdmkEdit
        Left = 456
        Top = 132
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 25
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object dmkEdit10: TdmkEdit
        Left = 536
        Top = 36
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 30
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Taxas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Taxas'
      end
      object EdTx: TdmkEdit
        Left = 720
        Top = 36
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 31
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object dmkEdit11: TdmkEdit
        Left = 8
        Top = 60
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Taxas de lotes'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Taxas de lotes'
      end
      object EdLx: TdmkEdit
        Left = 192
        Top = 60
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object dmkEdit12: TdmkEdit
        Left = 8
        Top = 84
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Ocorr'#234'ncias banc'#225'rias'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Ocorr'#234'ncias banc'#225'rias'
      end
      object EdOB: TdmkEdit
        Left = 192
        Top = 84
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdOc: TdmkEdit
        Left = 192
        Top = 108
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object dmkEdit15: TdmkEdit
        Left = 8
        Top = 108
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Ocorr'#234'ncias'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Ocorr'#234'ncias'
      end
      object dmkEdit16: TdmkEdit
        Left = 8
        Top = 132
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Pagamentos de ocorr'#234'ncias'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Pagamentos de ocorr'#234'ncias'
      end
      object EdPO: TdmkEdit
        Left = 192
        Top = 132
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 11
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object dmkEdit13: TdmkEdit
        Left = 536
        Top = 84
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 34
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Cheques devolvidos'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Cheques devolvidos'
      end
      object EdCD: TdmkEdit
        Left = 720
        Top = 84
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 35
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdPC: TdmkEdit
        Left = 720
        Top = 108
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 37
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object dmkEdit18: TdmkEdit
        Left = 536
        Top = 108
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 36
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Pagamento de cheques devolvidos'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Pagamento de cheques devolvidos'
      end
      object dmkEdit14: TdmkEdit
        Left = 536
        Top = 132
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 38
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Pagamento de duplicatas em atraso'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Pagamento de duplicatas em atraso'
      end
      object EdPD: TdmkEdit
        Left = 720
        Top = 132
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 39
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object RGTipo: TRadioGroup
        Left = 1
        Top = 189
        Width = 782
        Height = 50
        Align = alBottom
        Caption = ' Tipo de sincroniza'#231#227'o: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Somente dados modificados'
          'Atualizar todos dados da web')
        TabOrder = 40
      end
      object EdCa: TdmkEdit
        Left = 720
        Top = 60
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 33
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object dmkEdit19: TdmkEdit
        Left = 536
        Top = 60
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 32
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Cartas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Cartas'
      end
      object dmkEdit17: TdmkEdit
        Left = 8
        Top = 156
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 12
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Bancos'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Bancos'
      end
      object EdBC: TdmkEdit
        Left = 192
        Top = 156
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object dmkEdit20: TdmkEdit
        Left = 272
        Top = 156
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 26
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Feriados'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Feriados'
      end
      object EdFE: TdmkEdit
        Left = 456
        Top = 156
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 27
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
    end
  end
  object QrCount: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(AlterWeb) Itens'
      'FROM lotes'
      'WHERE AlterWeb=1')
    Left = 184
    Top = 353
    object QrCountItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 160
    Top = 337
  end
  object Qr1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 212
    Top = 353
  end
  object Qr2: TmySQLQuery
    Database = Dmod.MyDBn
    Left = 240
    Top = 353
  end
  object QrLotesIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle '
      'FROM lotesits'
      'WHERE AlterWeb=1')
    Left = 160
    Top = 309
    object QrLotesItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object Qr3: TmySQLQuery
    Database = Dmod.MyDBn
    Left = 268
    Top = 353
  end
  object QrLotes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM lotes'
      'WHERE AlterWeb=1')
    Left = 188
    Top = 309
    object QrLotesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrEmitBAC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT BAC'
      'FROM emitbac'
      'WHERE AlterWeb=1')
    Left = 216
    Top = 309
    object QrEmitBACBAC: TWideStringField
      FieldName = 'BAC'
    end
  end
  object QrEmitCPF: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CPF'
      'FROM emitcpf'
      'WHERE AlterWeb=1')
    Left = 244
    Top = 309
    object QrEmitCPFCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
  end
  object QrSacados: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CNPJ'
      'FROM sacados'
      'WHERE AlterWeb=1')
    Left = 272
    Top = 309
    object QrSacadosCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 15
    end
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM entidades'
      'WHERE AlterWeb=1')
    Left = 300
    Top = 309
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrContratos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM contratos'
      'WHERE AlterWeb=1')
    Left = 328
    Top = 309
    object QrContratosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrSocios: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM socios'
      'WHERE AlterWeb=1')
    Left = 356
    Top = 309
    object QrSociosControle: TAutoIncField
      FieldName = 'Controle'
    end
  end
  object QrEmLot: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo'
      'FROM emlot'
      'WHERE AlterWeb=1')
    Left = 608
    Top = 309
  end
  object QrLotesTxs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM lotestxs'
      'WHERE AlterWeb=1')
    Left = 384
    Top = 309
    object QrLotesTxsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrTaxas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM taxas'
      'WHERE AlterWeb=1')
    Left = 412
    Top = 309
    object QrTaxasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrOcorrBank: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM ocorbank'
      'WHERE AlterWeb=1')
    Left = 440
    Top = 309
    object QrOcorrBankCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrOcorreu: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM ocorreu'
      'WHERE AlterWeb=1')
    Left = 468
    Top = 309
    object QrOcorreuCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrOcorrPg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM ocorrpg'
      'WHERE AlterWeb=1')
    Left = 496
    Top = 309
    object QrOcorrPgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrAlinIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM alinits'
      'WHERE AlterWeb=1')
    Left = 524
    Top = 309
    object QrAlinItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrAlinPgs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM alinpgs'
      'WHERE AlterWeb=1')
    Left = 552
    Top = 309
    object QrAlinPgsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrAdupPgs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM aduppgs'
      'WHERE AlterWeb=1')
    Left = 580
    Top = 309
    object QrAdupPgsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrCartas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM cartas'
      'WHERE AlterWeb=1')
    Left = 636
    Top = 309
    object QrCartasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrBancos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM bancos'
      'WHERE AlterWeb=1')
    Left = 664
    Top = 309
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrFeriados: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data'
      'FROM feriados'
      'WHERE AlterWeb=1')
    Left = 692
    Top = 309
    object QrFeriadosData: TDateField
      FieldName = 'Data'
    end
  end
end
