unit WSincro;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, math, Db, mySQLDbTables,
  UMySQLModule, CheckLst, ComCtrls, dmkEdit, unInternalConsts, dmkGeral;

type
  TFmWSincro = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    Panel4: TPanel;
    QrParc: TmySQLQuery;
    Timer1: TTimer;
    Qr1: TmySQLQuery;
    Qr2: TmySQLQuery;
    Qr3: TmySQLQuery;
    Memo1: TMemo;
    Query: TmySQLQuery;
    RGTipo: TRadioGroup;
    CkLista: TCheckListBox;
    LBTabLoc: TListBox;
    LBTabWeb: TListBox;
    LBKeyFl1: TListBox;
    LBDtaTyp: TListBox;
    QrParcParcelamentos: TLargeintField;
    QrQtdL: TmySQLQuery;
    QrQtdW: TmySQLQuery;
    QrRegL: TmySQLQuery;
    QrRegW: TmySQLQuery;
    RE1: TRichEdit;
    Panel5: TPanel;
    GBLct: TGroupBox;
    CkLct: TCheckBox;
    EdTempo: TdmkEdit;
    Timer2: TTimer;
    Label1: TLabel;
    EdIni: TdmkEdit;
    Label2: TLabel;
    EdFim: TdmkEdit;
    Label3: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
  private
    { Private declarations }
    FArqWeb1, FArqWeb2: String;
    FCamposInalterados: Boolean;
    FIniUpLoad: TDateTime;
    //
    (*FTabLoc, FTabWeb, FCampo: String;
    FQuery: TmySQLQuery;
    FdataType: Integer;
    BaseDados: TmySQLDatabase;*)
    function ExportaDadosAlteradosParaWeb(TabLoc, TabWeb, Campo: String;
             {Query: TmySQLQuery;} dataType: Integer; BaseDados: TmySQLDatabase{;
             Registros: Integer}): String;
    function ExportaTodaTabelaParaWeb(TabLoc, TabWeb, Campo: String;
             dataType: Integer; BaseDados: TmySQLDatabase): String;
    procedure ExportaDadosParaWeb();
    //procedure ObtemConfigDeTabelas(Tabela: String);
    //function CamposInalterados(): Boolean;
    procedure VerificaExcluidos;
    procedure ExcluiArquivosDeCampos();
    procedure AtualizaTempo();
  public
    { Public declarations }
    procedure ListaCheckAll();
    procedure ListasClear();
    procedure ListasAdd(Descri, TabLoc, TabWeb, KeyFld: String; DtaTyp: Integer);
  end;

  var
  FmWSincro: TFmWSincro;

implementation

{$R *.DFM}

uses UnMyObjects, Module, Principal;

procedure TFmWSincro.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWSincro.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  Timer1.Enabled := True;
end;

procedure TFmWSincro.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmWSincro.FormCreate(Sender: TObject);
begin
  Query.Database  := Dmod.MyDB;
  Qr1.Database    := Dmod.MyDB;
  QrRegL.Database := Dmod.MyDB;
  QrQtdL.Database := Dmod.MyDB;
  //
  QrParc.Database := Dmod.MyDBn;
  Qr2.Database    := Dmod.MyDBn;
  Qr3.Database    := Dmod.MyDBn;
  QrRegW.Database := Dmod.MyDBn;
  QrQtdW.Database := Dmod.MyDBn;
  //
  FArqWeb1 := 'C:\Dermatek\Web\' + Application.Title + '\Conf';
  FArqWeb2 := 'C:/Dermatek/Web/' + Application.Title + '/Data';
  ForceDirectories(FArqWeb1);
  ForceDirectories(FArqWeb2);
  FArqWeb1 := FArqWeb1 + '\SQL_%s.%s';
  FArqWeb2 := FArqWeb2 + '/SQL_%s.%s';
end;

procedure TFmWSincro.AtualizaTempo();
var
  Tempo: TTime;
begin
  Tempo := Now() - FIniUpLoad;
  EdTempo.ValueVariant := Tempo;
  EdTempo.Update;
end;

procedure TFmWSincro.BtOKClick(Sender: TObject);
var
  Agora : TDateTime;
begin
  FIniUpLoad := Now();
  EdIni.ValueVariant := Geral.FDT(Now(), 0);
  EdFim.Text := '';
  Screen.Cursor := crHourGlass;
  Timer2.Enabled := True;
  try
    FCamposInalterados := Dmod.QrControleVersao.Value = Dmod.QrControleVerWeb.Value;
    ExportaDadosParaWeb();
    if not FCamposInalterados then
    begin
      // Atualiza tabela controle
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE controle SET VerWeb=Versao');
      Dmod.QrUpd.ExecSQL;
    end;
    if RGTipo.ItemIndex = 0 then VerificaExcluidos;
    //
    // Atualiza tabela local
    Agora := Now;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE controle SET DtaSincro=:P0');
    Dmod.QrUpd.Params[0].AsDateTime := Agora;
    Dmod.QrUpd.ExecSQL;
    //
    if Uppercase(Application.Title) = 'SYNDIC' then
    begin
      // Atualiza tabela web
      Dmod.QrUpdN.SQL.Clear;
      Dmod.QrUpdN.SQL.Add('UPDATE wcontrol SET DtaSincro=:P0');
      Dmod.QrUpdN.Params[0].AsDateTime := Agora;
      Dmod.QrUpdN.ExecSQL;
    end;
    //
    EdFim.Text := Geral.FDT(Now(), 0);
  finally
    Timer2.Enabled := False;
    AtualizaTempo();
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmWSincro.ExportaDadosParaWeb();
var
  i: Integer;
  TabLoc, TabWeb, KeyFld: String;
begin
  for i := 0 to CkLista.Items.Count - 1 do
  begin
    Application.ProcessMessages;
    Update;
    //
    if CkLista.Checked[i] = True then
    begin
      TabLoc := LBTabLoc.Items[i];
      TabWeb := LBTabWeb.Items[i];
      KeyFld := LBKeyFl1.Items[i];
      TabLoc := LBTabLoc.Items[i];
      ExportaDadosAlteradosParaWeb(TabLoc, TabWeb, KeyFld, {QrLotes,} 1,
      Dmod.MyDB{, Geral.IMV(Edlo.Text)});
    end;
  end;
  //   Lotes
  {ExportaDadosAlteradosParaWeb('lotes', 'lotes', 'codigo', QrLotes, 1,
    Dmod.MyDB, Geral.IMV(Edlo.Text));
  }
  RE1.SelAttributes.Color := clBlue;
  RE1.Lines.Add('Sincroniza��o finalizada!');
end;

procedure TFmWSincro.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  ExcluiArquivosDeCampos();
  //
  if Uppercase(Application.Title) = 'SYNDIC' then
  begin
    QrParc.Close;
    QrParc.Open;
    //
    case QrParcParcelamentos.Value of
      0: ;// Nada
      1:
      begin
        Application.MessageBox(PChar('H� um novo parcelamento de d�bitos no site.' +
        ' � necess�rio process�-lo antes de sincronizar!'), 'AVISO',
        MB_OK+MB_ICONWARNING);
        Close;
      end;
      else begin
        Application.MessageBox(PChar('Existem ' + IntToStr(
        QrParcParcelamentos.Value) + ' novos parcelamentos de d�bitos no site.' +
        ' � recomend�vel process�-los antes de sincronizar!'), 'AVISO',
        MB_OK+MB_ICONWARNING);
        Close;
      end;
    end;
    // Impedir de sincronizar desfazendo la�amentos.reparcel <> 0
    BtOK.Enabled := QrParcParcelamentos.Value = 0;
  end else BtOK.Enabled := True;
end;

procedure TFmWSincro.Timer2Timer(Sender: TObject);
begin
  AtualizaTempo();
end;

function TFmWSincro.ExportaDadosAlteradosParaWeb(TabLoc, TabWeb, Campo: String;
  {Query: TmySQLQuery; }dataType: Integer; BaseDados: TmySQLDatabase{;
  Registros: Integer}): String;
var
  Arq: String;
  F: TextFile;
  S, Campos, Prefix, txt: String;
begin
  try
  if RGTipo.ItemIndex = 1 then
  begin
    ExportaTodaTabelaParaWeb(TabLoc, TabWeb, Campo, dataType, BaseDados);
    Exit;
  end;
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT * FROM ' + Lowercase(TabLoc));
  Query.SQL.Add('WHERE AlterWeb = 1');
  Query.Open;
  if Query.RecordCount = 0 then Exit;

  //

  Qr1.Close;
  Qr1.Database := BaseDados;
  Campos := '';
  Prefix := '';
  RE1.SelAttributes.Color := clBlue;
  RE1.Text := Uppercase(TabWeb) + ' - Obtendo campos da tabela na web...';
  Update;
  Application.ProcessMessages;
  Arq := Format(FArqWeb1, [TabWeb, 'sql']);
  if (FileExists(Arq) and (FCamposInalterados)) then
  begin
    AssignFile(F, Arq);
    Reset(F);
    while not Eof(F) do
    begin
      Readln(F, S);
      Campos := Campos + S;
    end;
    CloseFile(F);
  end else begin
    Campos := UMyMod.ObtemCamposDeTabelaIdentica(
      Dmod.MyDBn, TabWeb, Prefix);
    RE1.SelAttributes.Color := clBlue;
    RE1.Text := Campos;
    Memo1.Lines.SaveToFile(Arq);
  end;

  //

  RE1.SelAttributes.Color := clBlue;
  RE1.Text := Uppercase(TabWeb) + ' - Exportando para arquivo tempor�rio...';
  Update;
  Arq := Format(FArqWeb2, [TabWeb, 'txt']);
  if FileExists(Arq) then DeleteFile(Arq);
  Application.ProcessMessages;
  Qr1.SQL.Clear;
  Qr1.SQL.Add('SELECT '+Campos);
  Qr1.SQL.Add('FROM '+ Lowercase(TabLoc));
  Qr1.SQL.Add('WHERE AlterWeb = 1');
  Qr1.SQL.Add('INTO OUTFILE "' + Arq + '"');
  Qr1.ExecSQL;

  //

  RE1.SelAttributes.Color := clBlue;
  RE1.Text := Uppercase(TabWeb) + ' - Preparando para excluir registros duplicados na web...';
  Update;
  Application.ProcessMessages;
  Qr3.SQL.Clear;
  Qr3.SQL.Add(DELETE_FROM + Lowercase(TabWeb) + ' WHERE ' + Campo + ' in (');
  txt := '';
  Query.First;
  while not Query.Eof do
  begin
    case dataType of
      1: txt := txt + IntToStr(Query.FieldByName(Campo).Value)+',';
      2: txt := txt + '"' + Geral.FDT(Query.FieldByName(Campo).Value, 2)+'",';
      else txt := txt + '"' + Query.FieldByName(Campo).Value+'",';
    end;
    Query.Next;
  end;
  if txt <> '' then
  begin
    txt[Length(txt)] := ')';
    Qr3.SQL.Add(txt);
    //

    RE1.SelAttributes.Color := clBlue;
    RE1.Text := Uppercase(TabWeb) + ' - Excluindo registros duplicados na web...';
    Update;
    Application.ProcessMessages;
    Qr3.ExecSQL;
  end;  
  //
  RE1.SelAttributes.Color := clBlue;
  RE1.Text := Uppercase(TabWeb) + ' - Importando de arquivo tempor�rio...';
  Update;
  Application.ProcessMessages;
  Qr2.SQL.Clear;
  Qr2.SQL.Add('LOAD DATA LOCAL INFILE "' + Arq + '"');
  Qr2.SQL.Add('INTO Table '+TabWeb);
  Qr2.ExecSQL;
  //
  Qr1.SQL.Clear;
  Qr1.SQL.Add('UPDATE '+lowercase(tabloc)+' SET AlterWeb=0');
  Qr1.ExecSQL;
  except
    ExcluiArquivosDeCampos();
    raise;
  end;
end;

function TFmWSincro.ExportaTodaTabelaParaWeb(TabLoc, TabWeb, Campo: String;
dataType: Integer; BaseDados: TmySQLDatabase): String;
var
  Arq: String;
  F: TextFile;
  S, Campos, Prefix: String;
begin
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT * FROM ' + Lowercase(TabLoc));
  Query.Open;
  //
  Qr1.Close;
  Qr1.Database := BaseDados;
  Campos := '';
  Prefix := '';
  RE1.SelAttributes.Color := clBlue;
  RE1.Text := Uppercase(TabWeb) + ' - Obtendo campos da tabela na web...';
  Update;
  Application.ProcessMessages;
  Arq := Format(FArqWeb1, [TabWeb, 'sql']);
  if FileExists(Arq) then
  begin
    AssignFile(F, Arq);
    Reset(F);
    while not Eof(F) do
    begin
      Readln(F, S);
      Campos := Campos + S;
    end;
    CloseFile(F);
  end else begin
    Campos := UMyMod.ObtemCamposDeTabelaIdentica(
      Dmod.MyDBn, TabWeb, Prefix);
    Memo1.Text := Campos;
    Memo1.Lines.SaveToFile(Arq);
  end;

  //

  RE1.SelAttributes.Color := clBlue;
  RE1.Text := Uppercase(TabWeb) + ' - Exportando para arquivo tempor�rio...';
  Update;
  Arq := Format(FArqWeb2, [TabWeb, 'txt']);
  if FileExists(Arq) then DeleteFile(Arq);
  Application.ProcessMessages;
  Qr1.SQL.Clear;
  Qr1.SQL.Add('SELECT '+Campos);
  Qr1.SQL.Add('FROM '+ Lowercase(TabLoc));
  //Qr1.SQL.Add('WHERE AlterWeb = 1');
  if (Lowercase(TabLoc) = VAR_LCT) and CkLct.Checked then
    Qr1.SQL.Add('WHERE Sit < 2 AND Tipo = 2'); // Somente abertos
  Qr1.SQL.Add('INTO OUTFILE "' + Arq + '"');
  Qr1.ExecSQL;

  //

  RE1.SelAttributes.Color := clBlue;
  RE1.Text := Uppercase(TabWeb) + ' - Excluindo registros duplicados na web...';
  Update;
  Application.ProcessMessages;
  Qr3.SQL.Clear;
  Qr3.SQL.Add(DELETE_FROM + Lowercase(TabWeb));
  Application.ProcessMessages;
  Qr3.ExecSQL;
  //
  RE1.SelAttributes.Color := clBlue;
  RE1.Text := Uppercase(TabWeb) + ' - Importando de arquivo tempor�rio...';
  Update;
  Application.ProcessMessages;
  Qr2.SQL.Clear;
  Qr2.SQL.Add('LOAD DATA LOCAL INFILE "' + Arq + '"');
  Qr2.SQL.Add('INTO Table '+TabWeb);
  Qr2.ExecSQL;
  //
  Qr1.SQL.Clear;
  Qr1.SQL.Add('UPDATE '+lowercase(tabloc)+' SET AlterWeb=0');
  Qr1.ExecSQL;
end;

(*procedure ObtemConfigDeTabelas(Tabela: String);
begin
  if Uppercase(Tabela) = Uppercase('') then
    FTabLoc, FTabWeb, FCampo: String;
    FQuery: TmySQLQuery;
    FdataType: Integer;
    BaseDados: TmySQLDatabase;
  begin
  end else
end;*)

procedure TFmWSincro.ListasClear();
begin
  CkLista.Clear;
  LBTabLoc.Clear;
  LBTabWeb.Clear;
  LBKeyFl1.Clear;
  LBDtaTyp.Clear;
end;

procedure TFmWSincro.ListasAdd(Descri, TabLoc, TabWeb, KeyFld: String;
  DtaTyp: Integer);
begin
  CkLista.Items.Add(Descri);
  LBTabLoc.Items.Add(TabLoc);
  LBTabWeb.Items.Add(TabWeb);
  LBKeyFl1.Items.Add(KeyFld);
  LBDtaTyp.Items.Add(IntToStr(DtaTyp));
end;

procedure TFmWSincro.ListaCheckAll();
var
  i: Integer;
begin
  for i := 0 to CkLista.Items.Count - 1 do
    CkLista.Checked[i] := True;
end;

procedure TFmWSincro.VerificaExcluidos;
var
  i, Dif: Integer;
  TabLoc, TabWeb, KeyFld: String;
begin
  RE1.SelAttributes.Color := clBlue;
  RE1.Text := 'Avisos:';
  for i := 0 to CkLista.Items.Count - 1 do
  begin
    if CkLista.Checked[i] = True then
    begin
      TabLoc := LBTabLoc.Items[i];
      TabWeb := LBTabWeb.Items[i];
      KeyFld := LBKeyFl1.Items[i];





      QrQtdL.Close;
      QrQtdL.SQL.Clear;
      QrQtdL.SQL.Add('SELECT COUNT(*) Registros');
      QrQtdL.SQL.Add('FROM ' + Lowercase(TabLoc));
      QrQtdL.SQL.Add('');
      QrQtdL.Open;

      QrQtdW.Close;
      QrQtdW.SQL.Clear;
      QrQtdW.SQL.Add('SELECT COUNT(*) Registros');
      QrQtdW.SQL.Add('FROM ' + Lowercase(TabWeb));
      QrQtdW.SQL.Add('');
      QrQtdW.Open;

      Dif := QrQtdW.FieldbyName('Registros').AsInteger - QrQtdL.FieldbyName('Registros').AsInteger;
      if (Dif > 0) then
      begin
        RE1.SelAttributes.Color := clRed;
        RE1.Text := 'A tabela web "' + TabWeb + '" possui ' +
          IntToStr(Dif) + ' registros que devem ser exclu�dos!';

        QrRegL.Close;
        QrRegL.SQL.Clear;
        QrRegL.SQL.Add('SELECT ' + KeyFld);
        QrRegL.SQL.Add('FROM ' + Lowercase(TabLoc));
        QrRegL.Open;

        QrRegW.Close;
        QrRegW.SQL.Clear;
        QrRegW.SQL.Add('SELECT ' + KeyFld);
        QrRegW.SQL.Add('FROM ' + LowerCase(TabWeb));
        QrRegW.SQL.Add('');
        QrRegW.Open;

        QrRegL.First;
        while not QrRegL.Eof do
        begin

          //Parei aqui
          QrRegL.Next;
        end;

      end;




    end;
  end;
  //   Lotes
  {ExportaDadosAlteradosParaWeb('lotes', 'lotes', 'codigo', QrLotes, 1,
    Dmod.MyDB, Geral.IMV(Edlo.Text));
  }
  RE1.SelAttributes.Color := clRed;
  RE1.Text := 'Sincroniza��o finalizada!';
end;

procedure TFmWSincro.ExcluiArquivosDeCampos();
var
  SearchRec: TSearchRec;
  Result: Integer;
  Name: String;
begin
  Name := 'C:\Dermatek\Web\' + Application.Title + '\';
  Result := FindFirst(Name+'*.*', faAnyFile, SearchRec);
  while Result = 0 do
  begin
    DeleteFile(Name+SearchRec.Name);
    Result := FindNext(SearchRec);
  end;
  Application.MessageBox(PChar('Todos arquivos configura��es de upload foram ' +
  'exclu�dos para serem renovados! Tente fazer o upload novamente!'), 'Aviso',
  MB_OK+MB_ICONINFORMATION);
end;

end.

