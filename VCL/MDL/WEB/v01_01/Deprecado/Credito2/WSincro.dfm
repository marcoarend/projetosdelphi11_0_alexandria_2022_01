object FmWSincro: TFmWSincro
  Left = 339
  Top = 185
  Caption = 'WEB-SINCR-001 :: Sincronismo Dados WEB [1]'
  ClientHeight = 532
  ClientWidth = 793
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 793
    Height = 370
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel4: TPanel
      Left = 0
      Top = 240
      Width = 793
      Height = 130
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object ST1: TStaticText
        Left = 0
        Top = 0
        Width = 38
        Height = 17
        Align = alTop
        Caption = 'Avisos:'
        TabOrder = 0
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 793
      Height = 240
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object dmkEdit1: TdmkEdit
        Left = 8
        Top = 12
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Border'#244's'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Border'#244's'
        ValWarn = False
      end
      object EdLo: TdmkEdit
        Left = 192
        Top = 12
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkEdit3: TdmkEdit
        Left = 8
        Top = 36
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Itens de border'#244's'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Itens de border'#244's'
        ValWarn = False
      end
      object EdLi: TdmkEdit
        Left = 192
        Top = 36
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkEdit2: TdmkEdit
        Left = 272
        Top = 60
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'BACs'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'BACs'
        ValWarn = False
      end
      object EdBa: TdmkEdit
        Left = 456
        Top = 60
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdEm: TdmkEdit
        Left = 456
        Top = 12
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkEdit6: TdmkEdit
        Left = 272
        Top = 12
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Emitentes'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Emitentes'
        ValWarn = False
      end
      object dmkEdit4: TdmkEdit
        Left = 272
        Top = 36
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Sacados'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Sacados'
        ValWarn = False
      end
      object EdSa: TdmkEdit
        Left = 456
        Top = 36
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkEdit5: TdmkEdit
        Left = 272
        Top = 84
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Entidades'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Entidades'
        ValWarn = False
      end
      object EdEn: TdmkEdit
        Left = 456
        Top = 84
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 11
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkEdit7: TdmkEdit
        Left = 536
        Top = 12
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 12
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Contratos'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Contratos'
        ValWarn = False
      end
      object EdCt: TdmkEdit
        Left = 720
        Top = 12
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkEdit8: TdmkEdit
        Left = 272
        Top = 108
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 14
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'S'#243'cios'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'S'#243'cios'
        ValWarn = False
      end
      object EdSo: TdmkEdit
        Left = 456
        Top = 108
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 15
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Memo1: TMemo
        Left = 8
        Top = -12
        Width = 545
        Height = 21
        Lines.Strings = (
          'Memo1')
        TabOrder = 16
        Visible = False
      end
      object dmkEdit9: TdmkEdit
        Left = 272
        Top = 132
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 17
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Coligadas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Coligadas'
        ValWarn = False
      end
      object EdCo: TdmkEdit
        Left = 456
        Top = 132
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 18
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkEdit10: TdmkEdit
        Left = 536
        Top = 36
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 19
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Taxas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Taxas'
        ValWarn = False
      end
      object EdTx: TdmkEdit
        Left = 720
        Top = 36
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 20
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkEdit11: TdmkEdit
        Left = 8
        Top = 60
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 21
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Taxas de border'#244's'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Taxas de border'#244's'
        ValWarn = False
      end
      object EdLx: TdmkEdit
        Left = 192
        Top = 60
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 22
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkEdit12: TdmkEdit
        Left = 8
        Top = 84
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 23
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Ocorr'#234'ncias banc'#225'rias'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Ocorr'#234'ncias banc'#225'rias'
        ValWarn = False
      end
      object EdOB: TdmkEdit
        Left = 192
        Top = 84
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 24
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdOc: TdmkEdit
        Left = 192
        Top = 108
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 25
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkEdit15: TdmkEdit
        Left = 8
        Top = 108
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 26
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Ocorr'#234'ncias'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Ocorr'#234'ncias'
        ValWarn = False
      end
      object dmkEdit16: TdmkEdit
        Left = 8
        Top = 132
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 27
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Pagamentos de ocorr'#234'ncias'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Pagamentos de ocorr'#234'ncias'
        ValWarn = False
      end
      object EdPO: TdmkEdit
        Left = 192
        Top = 132
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 28
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkEdit13: TdmkEdit
        Left = 536
        Top = 84
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 29
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Cheques devolvidos'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Cheques devolvidos'
        ValWarn = False
      end
      object EdCD: TdmkEdit
        Left = 720
        Top = 84
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 30
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdPC: TdmkEdit
        Left = 720
        Top = 108
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 31
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkEdit18: TdmkEdit
        Left = 536
        Top = 108
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 32
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Pagamento de cheques devolvidos'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Pagamento de cheques devolvidos'
        ValWarn = False
      end
      object dmkEdit14: TdmkEdit
        Left = 536
        Top = 132
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 33
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Pagamento de duplicatas em atraso'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Pagamento de duplicatas em atraso'
        ValWarn = False
      end
      object EdPD: TdmkEdit
        Left = 720
        Top = 132
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 34
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object RGTipo: TRadioGroup
        Left = 0
        Top = 190
        Width = 793
        Height = 50
        Align = alBottom
        Caption = ' Tipo de sincroniza'#231#227'o: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Somente dados modificados'
          'Atualizar todos dados da web')
        TabOrder = 35
      end
      object EdCa: TdmkEdit
        Left = 720
        Top = 60
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 36
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkEdit19: TdmkEdit
        Left = 536
        Top = 60
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 37
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Cartas'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Cartas'
        ValWarn = False
      end
      object dmkEdit17: TdmkEdit
        Left = 8
        Top = 156
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 38
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Bancos'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Bancos'
        ValWarn = False
      end
      object EdBC: TdmkEdit
        Left = 192
        Top = 156
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 39
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkEdit20: TdmkEdit
        Left = 272
        Top = 156
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 40
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Feriados'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Feriados'
        ValWarn = False
      end
      object EdFE: TdmkEdit
        Left = 456
        Top = 156
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 41
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkEdit21: TdmkEdit
        Left = 536
        Top = 156
        Width = 180
        Height = 21
        ReadOnly = True
        TabOrder = 42
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'Clientes internos'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'Clientes internos'
        ValWarn = False
      end
      object EdCI: TdmkEdit
        Left = 720
        Top = 156
        Width = 61
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 43
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 793
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 745
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 697
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 347
        Height = 32
        Caption = 'Sincronismo Dados WEB [1]'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 347
        Height = 32
        Caption = 'Sincronismo Dados WEB [1]'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 347
        Height = 32
        Caption = 'Sincronismo Dados WEB [1]'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 418
    Width = 793
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 789
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 462
    Width = 793
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 647
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 645
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Sincronizar'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrCount: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(AlterWeb) Itens'
      'FROM lot es'
      'WHERE AlterWeb=1')
    Left = 248
    Top = 257
    object QrCountItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 224
    Top = 241
  end
  object Qr1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 276
    Top = 257
  end
  object Qr2: TMySQLQuery
    Database = Dmod.MyDBn
    Left = 304
    Top = 257
  end
  object QrLotIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatParcela'
      'FROM lct0001a'
      'WHERE FatID=301 '
      'AND AlterWeb=1')
    Left = 40
    Top = 341
    object QrLotItsFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
  end
  object Qr3: TMySQLQuery
    Database = Dmod.MyDBn
    Left = 332
    Top = 257
  end
  object QrLot: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM lot es'
      'WHERE AlterWeb=1')
    Left = 68
    Top = 341
    object QrLotCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrEmitBAC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT BAC'
      'FROM emitbac'
      'WHERE AlterWeb=1')
    Left = 96
    Top = 341
    object QrEmitBACBAC: TWideStringField
      FieldName = 'BAC'
    end
  end
  object QrEmitCPF: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CPF'
      'FROM emitcpf'
      'WHERE AlterWeb=1')
    Left = 124
    Top = 341
    object QrEmitCPFCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
  end
  object QrSacados: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CNPJ'
      'FROM sacados'
      'WHERE AlterWeb=1')
    Left = 152
    Top = 341
    object QrSacadosCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 15
    end
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM entidades'
      'WHERE AlterWeb=1')
    Left = 180
    Top = 341
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrContratos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM contratos'
      'WHERE AlterWeb=1')
    Left = 208
    Top = 341
    object QrContratosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrSocios: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM socios'
      'WHERE AlterWeb=1')
    Left = 236
    Top = 341
    object QrSociosControle: TAutoIncField
      FieldName = 'Controle'
    end
  end
  object QrEmLot: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo'
      'FROM emlot'
      'WHERE AlterWeb=1')
    Left = 488
    Top = 341
  end
  object QrLotTxs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM lot estxs'
      'WHERE AlterWeb=1')
    Left = 264
    Top = 341
    object QrLotTxsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrTaxas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM taxas'
      'WHERE AlterWeb=1')
    Left = 292
    Top = 341
    object QrTaxasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrOcorrBank: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM ocorbank'
      'WHERE AlterWeb=1')
    Left = 320
    Top = 341
    object QrOcorrBankCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrOcorreu: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM ocorreu'
      'WHERE AlterWeb=1')
    Left = 348
    Top = 341
    object QrOcorreuCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrOcorP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM ocor rpg'
      'WHERE AlterWeb=1')
    Left = 376
    Top = 341
    object QrOcorPCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrAlinIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM alinits'
      'WHERE AlterWeb=1')
    Left = 404
    Top = 341
    object QrAlinItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrChqPgs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM alin pgs'
      'WHERE AlterWeb=1')
    Left = 432
    Top = 341
    object QrChqPgsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrDupPgs: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM adup pgs'
      'WHERE AlterWeb=1')
    Left = 460
    Top = 341
    object QrDupPgsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrCartas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM cartas'
      'WHERE AlterWeb=1')
    Left = 516
    Top = 341
    object QrCartasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrFeriados: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data'
      'FROM feriados'
      'WHERE AlterWeb=1')
    Left = 572
    Top = 341
    object QrFeriadosData: TDateField
      FieldName = 'Data'
    end
  end
  object QrBancos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM bancos'
      'WHERE AlterWeb=1')
    Left = 544
    Top = 341
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrEntiCliInt: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CodCliInt'
      'FROM enticliint'
      'WHERE AlterWeb=1')
    Left = 600
    Top = 341
    object QrEntiCliIntCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
  end
end
