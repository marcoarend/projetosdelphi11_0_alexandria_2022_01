object FmWSincro2: TFmWSincro2
  Left = 339
  Top = 185
  Caption = 'WEB-SINCR-002 :: Sincronismo Dados WEB [2]'
  ClientHeight = 523
  ClientWidth = 780
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 47
    Width = 780
    Height = 363
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 238
      Width = 780
      Height = 9
      Cursor = crVSplit
      Align = alTop
      Beveled = True
    end
    object Panel4: TPanel
      Left = 0
      Top = 247
      Width = 780
      Height = 116
      Align = alClient
      TabOrder = 1
      object RE1: TRichEdit
        Left = 1
        Top = 18
        Width = 778
        Height = 97
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object PB1: TProgressBar
        Left = 1
        Top = 1
        Width = 778
        Height = 17
        Align = alTop
        TabOrder = 1
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 780
      Height = 238
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Memo1: TMemo
        Left = 0
        Top = 0
        Width = 780
        Height = 21
        Align = alTop
        Lines.Strings = (
          'Memo1')
        TabOrder = 0
        Visible = False
      end
      object CkLista: TCheckListBox
        Left = 0
        Top = 21
        Width = 386
        Height = 125
        Align = alClient
        ItemHeight = 13
        TabOrder = 1
        OnClick = CkListaClick
      end
      object LBTabLoc: TListBox
        Left = 386
        Top = 21
        Width = 119
        Height = 125
        Align = alRight
        ItemHeight = 13
        TabOrder = 2
        OnClick = LBTabLocClick
      end
      object LBTabWeb: TListBox
        Left = 505
        Top = 21
        Width = 119
        Height = 125
        Align = alRight
        ItemHeight = 13
        TabOrder = 3
        OnClick = LBTabWebClick
      end
      object LBKeyFl1: TListBox
        Left = 624
        Top = 21
        Width = 119
        Height = 125
        Align = alRight
        ItemHeight = 13
        TabOrder = 4
        OnClick = LBKeyFl1Click
      end
      object LBDtaTyp: TListBox
        Left = 743
        Top = 21
        Width = 37
        Height = 125
        Align = alRight
        ItemHeight = 13
        TabOrder = 5
        OnClick = LBDtaTypClick
      end
      object Panel8: TPanel
        Left = 0
        Top = 146
        Width = 780
        Height = 48
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 6
        object Label4: TLabel
          Left = 270
          Top = 9
          Width = 105
          Height = 13
          Caption = #218'ltmo sincronismo em:'
        end
        object GBLct: TGroupBox
          Left = 0
          Top = 0
          Width = 264
          Height = 47
          Align = alLeft
          Caption = ' Lan'#231'amentos financeiros: '
          TabOrder = 0
          object CkLct: TCheckBox
            Left = 12
            Top = 20
            Width = 142
            Height = 17
            Caption = 'Somente n'#227'o quitados.'
            Checked = True
            State = cbChecked
            TabOrder = 0
          end
        end
        object EdDtaUltimaSincro: TdmkEdit
          Left = 270
          Top = 25
          Width = 135
          Height = 21
          ReadOnly = True
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfLong
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
      object RGTipo: TRadioGroup
        Left = 0
        Top = 194
        Width = 780
        Height = 44
        Align = alBottom
        Caption = ' Tipo de sincroniza'#231#227'o: '
        Columns = 2
        ItemIndex = 1
        Items.Strings = (
          'Somente dados modificados'
          'Atualizar todos dados da web')
        TabOrder = 7
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 780
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 733
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 686
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 326
        Height = 31
        Caption = 'Sincronismo Dados WEB [2]'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 326
        Height = 31
        Caption = 'Sincronismo Dados WEB [2]'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 326
        Height = 31
        Caption = 'Sincronismo Dados WEB [2]'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 410
    Width = 780
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel6: TPanel
      Left = 2
      Top = 14
      Width = 776
      Height = 28
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 454
    Width = 780
    Height = 69
    Align = alBottom
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 14
      Width = 776
      Height = 54
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 376
        Top = 6
        Width = 36
        Height = 13
        Caption = 'Tempo:'
      end
      object Label3: TLabel
        Left = 454
        Top = 6
        Width = 41
        Height = 13
        Caption = 'T'#233'rmino:'
      end
      object Label2: TLabel
        Left = 238
        Top = 6
        Width = 30
        Height = 13
        Caption = 'In'#237'cio:'
      end
      object PnSaiDesis: TPanel
        Left = 635
        Top = 0
        Width = 142
        Height = 54
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 26
        Top = 3
        Width = 118
        Height = 39
        Caption = 'Sincroni&zar'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
      object EdTempo: TdmkEdit
        Left = 376
        Top = 22
        Width = 74
        Height = 20
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfLong
        Texto = '00:00:00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdFim: TdmkEdit
        Left = 454
        Top = 22
        Width = 135
        Height = 20
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfLong
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdIni: TdmkEdit
        Left = 238
        Top = 22
        Width = 134
        Height = 20
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfLong
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 148
        Top = 3
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 189
        Top = 3
        Width = 40
        Height = 39
        NumGlyphs = 2
        TabOrder = 6
        OnClick = BtNenhumClick
      end
    end
  end
  object QrParc: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT COUNT(*) Parcelamentos'
      'FROM bloqparc'
      'WHERE NOT (Novo IN (9,-9))')
    Left = 400
    Top = 161
    object QrParcParcelamentos: TLargeintField
      FieldName = 'Parcelamentos'
      Required = True
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 216
    Top = 161
  end
  object Qr1: TMySQLQuery
    Database = Dmod.MyDB
    Left = 428
    Top = 161
  end
  object Qr2: TMySQLQuery
    Database = Dmod.MyDBn
    Left = 456
    Top = 161
  end
  object Qr3: TMySQLQuery
    Database = Dmod.MyDBn
    Left = 484
    Top = 161
  end
  object Query: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM aduppgs'
      'WHERE AlterWeb=1')
    Left = 512
    Top = 161
  end
  object QrQtdL: TMySQLQuery
    Database = Dmod.MyDB
    Left = 340
    Top = 393
  end
  object QrQtdW: TMySQLQuery
    Database = Dmod.MyDBn
    Left = 376
    Top = 393
  end
  object QrRegL: TMySQLQuery
    Database = Dmod.MyDB
    Left = 340
    Top = 421
  end
  object QrRegW: TMySQLQuery
    Database = Dmod.MyDBn
    Left = 376
    Top = 421
  end
  object Timer2: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer2Timer
    Left = 244
    Top = 160
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 572
    Top = 8
  end
end
