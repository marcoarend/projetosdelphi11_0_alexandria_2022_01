unit UnDmkWeb;

interface

uses Classes, StdCtrls, dmkSOAP, Forms, Vcl.Controls, dmkGeral, ShellApi, dmkImage,
  ExtCtrls, Windows, Graphics, jpeg, Clipbrd, SysUtils, shlobj,
  mySQLDbTables, Dialogs, Messages, UnDmkProcFunc, SHDocVw, ActiveX, WinInet,
  {$IfDef UsaWSuport}MyDBCheck, UMySQLModule, {$EndIf} UnMyObjects, (*&ABSMain,*)
  Httpapp, Variants, ComCtrls, dmkPageControl, dmkCheckGroup,
  {$IfNDef NO_USE_MYSQLMODULE} DmkDAC_PF, {$EndIf}
  Buttons, Registry, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdHTTP, IdException, URLMon, (*UrlDownloadToFile*) UnDmkEnums, Winsock,
  System.StrUtils, Data.DB, MSHTML, IdSSLOpenSSL, IdURI, System.JSON;

type
  TDispositivo = (dtWEB, dtDesktop, dtMobile);
  TAcao        = (dtConfigura, dtMostra);
  TDownType    = (dtExec, dtTabe, dtImag, dtSQLs, dtExecAux);
  TTypeSQL     = (dtIns, dtUpd, dtAuto);
  TUnDmkWeb    = class(TObject)
  
  private
    { Private declarations }
    function  ObtemTipoDeDispositivo(Dispositivo: TDispositivo): Integer;
  public
    { Public declarations }

{$IfNDef NO_USE_MYSQLMODULE}
    function  GeralSenhaAleatoria(DataBase: TMySQLDataBase; Tabela, Campo: String): String;
    function  ConexaoRemota(DBn: TmySQLDatabase; Qry: TmySQLQuery; Aviso: Integer): Boolean;
    function  ReabreEmailConta(Query: TmySQLQuery; DataBase: TmySQLDatabase;
                TipoConta, PermisNiv: Integer): Integer;
    {$IFNDEF NAO_USA_UnitMD5}
{$IfDef UsaWSuport}
    function  VerificaAtualizacaoVersao2(MostraMensagem, MostraTextoURL: Boolean;
                AppName, AppTitle, CNPJ: String; VersaoAtual: Int64; CodAplic: Integer;
                Agora: TDateTime; Memo: TMemo; TipoDownload: TDownType;
                var Versao: Int64; var Arquivo: String;
                ForcaBaixa: Boolean = False; ApenasVerifica: Boolean = False;
                BalloonHint: TBalloonHint = nil; NaoEncerra: Boolean = False): Boolean;
{$ENdIf UsaWSuport}
    {$ENDIF}
{$EndIf}
    function  TruaduzNivel(Tipo, CodigoApp: Integer): String;
    function  VerificaSQLNiveis(TipoLogin: Integer; CampoSQL: String): String;
    function  DownloadURL(const aUrl: string; Texto: TStrings): Boolean;
    function  RemoteConnection(): Boolean; // TemInternet()
    //
    function  ObtemNomeUsuarioWEB(Codigo: Integer): String;
    function  ObtemStatusUsuarioWEB(const Codigo: Integer; var Status: Integer;
                var StatTxt: String): TColor;
    //
    function  TamanhoArquivoBytes(Arquivo: string): Double;
    function  LoginUser(Usuario, Senha: String): String;
    function  DesconectarUsuarioWEB(): Boolean;
    { ********** WOrdSer In�cio ********************************************* }
    function  WOrdSerInclui(const AplicativoSolicitante, Codigo, Cliente,
                Solicitante, Assunto, Prioridade, Modo, Status, Aplicativo,
                Responsavel, Grupo, EnvMailAbeAdm: Integer; AberturaTimeZone,
                Descricao, Sistema, Impacto, Versao, Janela, JanelaRel,
                CompoRel, Tags: String; var CodigoReturn: Integer;
                var Mensagem: String): Integer;
    function  WOrdSerIncluiCom(const AplicativoSolicitante, Codigo, Controle,
                Entidade, Para: Integer; ComRes, EnvMail: Boolean;
                Nome: String; var Mensagem: String): Integer;
    function  WOrdSerEnc(const AplicativoSolicitante, Codigo, Status: Integer;
                DataHoraTimeZone: String; Solucao: Integer; MotivReopen: String;
                var Mensagem: String): Integer;
    function  WOrdSerAtualizaStatus(const AplicativoSolicitante, Codigo,
                Status: Integer; var Mensagem: String): Integer;
    function  WOrdSerAtualizaRespons(const AplicativoSolicitante, Codigo,
                Respons: Integer; var Mensagem: String): Integer;
{$IfNDef NO_USE_MYSQLMODULE}
    function  WOrdSerEnvMail(const Query: TmySQLQuery; Tipo, Codigo, Controle: Integer;
                var Mensagem: String): Boolean;
{$EndIf}
    function  InsereWOrdSerArq(AplicativoSolicitante, Codigo,
                FTPWebArq: Integer; var Msg: String): Integer;
    { ********** WOrdSer Fim ************************************************ }
    { ********** FTP In�cio ************************************************* }
    function  FTP_ObtemDadosFTPDir_SOAP(const AplicativoSolicitante, Diretorio: Integer;
                var CliInt, FTPConfig, FTPDir: Integer; var PastaCam: String): Boolean;
    function  FTP_ObtemDadosFTPConf_SOAP(const AplicativoSolicitante, FTPConfig:
                Integer; var Passivo: Integer; var Host, User, Pass: String): Boolean;
    function  FTP_InsUpdRegistroFTPDiretorio(AplicativoSolicitante, Codigo,
                FTPConfig, TipoDir, CliInt, Depto, Nivel: Integer; Nome, Pasta,
                Caminho: String; var Msg: String): Integer;
    function  FTP_InsUpdRegistroFTPArquivo(AplicativoSolicitante: Integer;
                TipSQL: TTypeSQL; CliInt, Depto, Nivel, FTPDir: Integer; Tam,
                Nome, ArqNome: String; Origem, Ativo, Codigo: Integer;
                var Msg: String): Integer;
    { ********** FTP Fim **************************************************** }
    { ********** Termos In�cio ********************************************** }
    function Termo_AtualizaHash(AplicativoSolicitante: Integer;
               var Mensagem: String): Boolean;
    function Termo_ConfiguraHashTermo(AplicativoSolicitante: Integer;
               var Mensagem: String): Boolean;
    { ********** Termos Fim ************************************************* }
  {$IfDef cAdvToolx} //Berlin
    function  SkinMenu(Index: integer): TToolBarStyle;
  {$EndIf}
    function  CapturaTela(Dir, NomeArq: String): String;
    //
    //procedure DeleteFolder(sPath : String; Confirm: Boolean);
    procedure MostraBalloonHintMenuTopo(Button: TControl;
              BalloonHint: TBalloonHint; Titulo, Descricao: String);
    procedure ValidaModulosAplicativo(Query: TmySQLQuery; CNPJ: String; Aplicativo: Integer);
    procedure WebBrowserPost(URL: String; WebBrowser: TWebBrowser; VarName,
                VarValue: array of String);
    function  URLExiste(URL: String): Boolean;
    function  URLGet(URL: String; var Resultado: TFileStream): Boolean;
    function  URLGetMem(const URL: String; var Resultado: String; const
              AvisaErro: Boolean): Boolean;
    function  URLPost(URL: String; VarsValues: array of String; var Resultado: TStringStream): Boolean;
    function  DeleteFolder(FolderName: String; LeaveFolder: Boolean): Boolean;
    procedure MostraWebBrowser(URL: String; NavegaPad: Boolean = True;
              Show: Boolean = True; Height: Integer = 600; Width: Integer = 600);
    procedure MostraGoogleMapsPlace(URL: String; NavegaPad, Show: Boolean; Height, Width: Integer);
    function  DefineCoordenadas(const Coord: String; var Latitude, Longitude:
              Double; const NavegaPad, Show: Boolean; const Height, Width:
              Integer): Boolean;
    {$IfDef UsaWSuport}
    procedure AbrirAppAcessoRemoto();
    function  ConectarUsuarioWEB(SoAdmin: Boolean): Boolean;
    procedure MostraWSuporte(NaoGeraPrint: Boolean = False; Aba: Integer = 0;
                BalloonHint: TBalloonHint = nil; TxtPesq: String = '');
    { ********** Alertas de atualiza��es OS Ini ***************************** }
    procedure MostraJanelaWSuporte(Sender: TObject);
    procedure ConfiguraAlertaWOrdSerApp(Timer: TTimer; TrayIcon: TTrayIcon;
                BalloonHint: TBalloonHint);
    procedure AtualizaSolicitApl2(Query: TmySQLQuery; DataBase: TmySQLDatabase;
                Timer: TTimer; TrayIcon: TTrayIcon;
                TBButton: (*TToolBarButton*)TControl; BalloonHint: TBalloonHint);
    { ********** Alertas de atualiza��es OS Fim ***************************** }
    procedure ObtemDadosContaDmk(Query: TmySQLQuery; DataBase: TmySQLDatabase;
                UsuarioLocal: Integer; var CodigoCfg: Integer; var Usuario, Senha: String);
    function  InsereContaDmk(SQLType: TSQLType; QrUpd, Query: TmySQLQuery;
                DataBase: TmySQLDatabase; Usuario, Senha: String;
                CodUsuarioLocal, Codigo: Integer): Integer;
    (*
    function  VerificaSeContaDmkExiste(Query: TmySQLQuery;
                DataBase: TmySQLDatabase; Usuario: String): Integer;
    *)
    function  VerificaSeExisteSincroEmExecucao(Database: TmySQLDatabase;
                var DtaUltimaSincro: TDateTime): Boolean;
    function  ImportaRegistroDeTabelaIdenticaWeb(QueryUpdate, QueryWeb: TmySQLQuery;
                TableLoc: String; KeyFields: array of string;
                KeyValues: array of Variant): Boolean;
    function VerificaSeModuloWebEstaAtivo(DMKID_APP: Integer;
               HabilModulos: String; Database: TmySQLDatabase;
               var Msg: String): Boolean;
    //Janelas na web
    procedure MostraWTextos(DBn: TmySQLDatabase; QueryOpcoesGerl: TmySQLQuery);
    procedure MostraWCalendar(DBn: TmySQLDatabase; QryWebParams: TmySQLQuery);
    {$EndIf}
    procedure WBLoadHTML(WebBrowser: TWebBrowser; HTMLCode: WideString);
    procedure CarregaImagemFormWEB(Imagem: TdmkImage);
    function  BitmapToString(Bitmap: TBitmap): String;
    function  StringToBitmap(S: String): TBitmap;
{$IfNDef NO_USE_MYSQLMODULE}
    function  DataTimeZoneToNewTimeZone(Database: TmySQLDatabase;
                Query: TmySQLQuery; DataHora, TimeZone, NewTimeZone: String): TDateTime;
{$EndIf}
    procedure ConfiguraHTTP(IdHTTP: TIdHTTP; URL: String = '';
              IdSSL: TIdSSLIOHandlerSocketOpenSSL = nil);
    //Web
    function  VerificaConexaoWeb(var Msg: String): Integer;
    procedure LimpaWebBrowser(Browser: TWebBrowser);
    procedure CarregaTextNoWebBrowser(Browser: TWebBrowser; Texto: String);
    // Movidos so M L A G e r a l
    function DownloadFile(SourceFile, DestFile: string): Boolean;
    //function DownloadURL(const aUrl: string; Texto: TStrings): Boolean; Ja tem aqui!!
    function DownloadURL_NOCache(const aUrl: string; var s: String): Boolean;
    // Hist�rico de altera��es de aplicativos
    procedure HistoricoDeAlteracoes(DmkId_App: Integer; Versao: Int64; Acao: TAcao);
    // L I N K S  -  S I T E  D E R M A T E K
    function  ObtemLinkLostPass(DMKID_APP: Integer; Web_MyURL, WebId, Usuario: String): String;
    function  ObtemLinkTermo(IDTermo: Integer): String;
    function  ObtemLinkAjuda(DMKID_APP, IDAjuda: Integer): String;
    function  ObtemLinkHisAlt(Aplicativo: Integer; Versao: String): String;
    procedure WhatsApp_EnviaMensagem(Mensagem, Telefone: String);
  end;

var
  DmkWeb: TUnDmkWeb;
  FTarget: String;
const
  TimerInterval = 900000; //15 minutos
  CO_DMKURL_BASE = 'http://www.dermatek.net.br/?page=';
  CO_SUPORTE_TITLE = 'Suporte Dermatek';
  CO_SUPORTE_DESCRI = 'Existem novas atualiza��es! Precione a tecla F1 para visualizar!';

implementation

uses {$IfDef UsaWSuport}WLogin, WSuporte, WOrdSerIncRap, UnFTP, WebBrowser, NovaVersao, {$EndIf}
  UnInternalConsts, UnGrl_Consts, UnGrl_Vars, UnGrl_Geral, UnGrl_DmkREST;

procedure TUnDmkWeb.WhatsApp_EnviaMensagem(Mensagem, Telefone: String);
const
  Site = 'https://web.whatsapp.com/';
var
  Url: String;
begin
  if (Mensagem <> '') and (Telefone <> '') then
  begin
    Url := Site + 'send?phone=55' + Geral.SoNumero_TT(Telefone) + '&text=' + TIdURI.PathEncode(Mensagem);
    //
    DmkWeb.MostraWebBrowser(Url, True, False, 0, 0);
  end;
end;

{$IfNDef NO_USE_MYSQLMODULE}
function TUnDmkWeb.GeralSenhaAleatoria(DataBase: TMySQLDataBase; Tabela,
  Campo: String): String;

  function SenhaExiste(Tabela, Campo, Senha: String): Boolean;
  var
    Query: TMySQLQuery;
  begin
    Query := TmySQLQuery.Create(TDataModule(DataBase.Owner));
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
        'SELECT ' + Campo,
        'FROM ' + Tabela,
        'WHERE ' + Campo + '="' + Senha + '"',
        '']);
      if Query.RecordCount > 0 then
        Result := True
      else
        Result := False;
    finally
      Query.Free;
    end;
  end;

var
  Existe: Boolean;
  Senha: String;
begin
  repeat
    Senha  := dmkPF.GeraPassword(True, False, True, 32);
    Existe := SenhaExiste(Tabela, Campo, Senha);
  until
    Existe = False;
  //
  Result := Senha;
end;
{$EndIf}

procedure TUnDmkWeb.MostraBalloonHintMenuTopo(Button: TControl;
  BalloonHint: TBalloonHint; Titulo, Descricao: String);
var
  Pt: TPoint;
begin
  BalloonHint.Title       := Titulo;
  BalloonHint.Description := Descricao;
  //
  Pt   := Button.ClientToScreen(Point(0,0));
  Pt.X := Pt.X + Trunc(Button.Width div 2);
  Pt.Y := Pt.Y + Trunc(Button.Height div 2);
  //
  BalloonHint.ShowHint(Pt);
end;

procedure TUnDmkWeb.MostraGoogleMapsPlace(URL: String; NavegaPad, Show: Boolean;
  Height, Width: Integer);
const
  SiteURL = 'https://www.google.com.br/maps/place/';
var
  AllURL: String;
begin
  AllURL := StringReplace(URL, ' ', '+', [rfReplaceAll, rfIgnoreCase]);
  AllURL := SiteURL + URL;
  //
  MostraWebBrowser(AllURL, NavegaPad, Show, Height, Width);
end;

{$IfDef UsaWSuport}
{ ********** Alertas de atualiza��es OS Ini ***************************** }
procedure TUnDmkWeb.MostraJanelaWSuporte(Sender: TObject);
begin
  try
    MostraWSuporte(True, 2);
  finally
    (Sender as TTrayIcon).Visible := False;
  end;
end;

function TUnDmkWeb.ImportaRegistroDeTabelaIdenticaWeb(QueryUpdate,
  QueryWeb: TmySQLQuery; TableLoc: String; KeyFields: array of string;
  KeyValues: array of Variant): Boolean;
var
  i, Parametro, TamValCampos, TamValCamposPri: Integer;
  NomeCampo: String;
  QueryUpd: TmySQLQuery;
  Valor: Variant;
  TipoCampo: TFieldType;
  Campos, CamposPri: array of string;
  ValCampos, ValCamposPri: array of Variant;
begin
  Result := False;
  //
  if QueryWeb.RecordCount > 0 then
  begin
    try
      ValCampos    := VarArrayCreate([0, 1], varVariant);
      ValCamposPri := VarArrayCreate([0, 1], varVariant);
      //
      TamValCampos    := 1;
      TamValCamposPri := 1;
      //
      //Carrega campos
      for i := 0 to QueryWeb.FieldCount - 1 do
      begin
        NomeCampo := QueryWeb.FieldList.Fields[i].FieldName;
        //
        Parametro := AnsiIndexText(LowerCase(NomeCampo), KeyFields);
        //
        if Parametro <> -1 then
        begin
          NomeCampo := KeyFields[i];
          //
          SetLength(CamposPri, TamValCamposPri);
          CamposPri[TamValCamposPri - 1] := NomeCampo;
          //
          TamValCamposPri := TamValCamposPri + 1;
        end else
        begin
          SetLength(Campos, TamValCampos);
          Campos[TamValCampos - 1] := NomeCampo;
          //
          TamValCampos := TamValCampos + 1;
        end;
      end;
      //
      TamValCampos    := 1;
      TamValCamposPri := 1;
      //
      //Carrega valores
      for i := 0 to QueryWeb.FieldCount - 1 do
      begin
        NomeCampo := QueryWeb.FieldList.Fields[i].FieldName;
        TipoCampo := QueryWeb.FieldList.Fields[i].DataType;
        //
        Parametro := AnsiIndexText(LowerCase(NomeCampo), KeyFields);
        //
        if Parametro <> -1 then
        begin
          Valor := KeyValues[Parametro];
          //
          SetLength(ValCamposPri, TamValCamposPri);
          ValCamposPri[TamValCamposPri - 1] := Valor;
          //
          TamValCamposPri := TamValCamposPri + 1;
        end else
        begin
          Valor := QueryWeb.FieldList.Fields[i].Value;
          //
          SetLength(ValCampos, TamValCampos);
          ValCampos[TamValCampos - 1] := Valor;
          //
          TamValCampos := TamValCampos + 1;
        end;
      end;
      Result := UMyMod.SQLInsUpd(QueryUpdate, stIns, TableLoc, False,
        Campos, CamposPri, ValCampos, ValCamposPri, False);
      //
      QueryUpd.Free;
      //
      Result := True;
    except
      QueryUpd.Free;
    end;
  end;
end;

procedure TUnDmkWeb.ConfiguraAlertaWOrdSerApp(Timer: TTimer;
  TrayIcon: TTrayIcon; BalloonHint: TBalloonHint);
var
  FilePath: String;
  HLargeIco, HSmallIco: HICON;
  SmallIco: TIcon;
begin
  //Configura o timer
  try
    if BalloonHint <> nil then
    begin
      BalloonHint.Delay     := 500;
      BalloonHint.HideAfter := 30000;
      BalloonHint.Style     := bhsBalloon;
    end;
    if TrayIcon <> nil then
    begin
      TrayIcon.Visible         := False;
      TrayIcon.Animate         := True;
      TrayIcon.AnimateInterval := 0;
      TrayIcon.BalloonFlags    := bfNone;
      TrayIcon.BalloonHint     := CO_SUPORTE_DESCRI;
      TrayIcon.BalloonTimeout  := 0;
      TrayIcon.BalloonTitle    := CO_SUPORTE_TITLE;
      //
      TrayIcon.OnBalloonClick  := MostraJanelaWSuporte;
      TrayIcon.OnClick         := MostraJanelaWSuporte;
      //
      //Configura o �cone do App
      FilePath := Application.ExeName;
      //
      if ExtractIconEx(PChar(FilePath), 0, HLargeIco, HSmallIco, 1 ) = 2 then
      begin
        SmallIco := TIcon.Create;
        try
          SmallIco.Handle := HSmallIco;
          TrayIcon.Icon   := SmallIco;
        finally
          SmallIco.Free;
        end;
      end else
      begin
        DestroyIcon(HLargeIco);
        DestroyIcon(HSmallIco);
        //
        TrayIcon.Icon := nil;
      end;
    end;
    if Timer <> nil then
    begin
      Timer.Enabled  := False;
      Timer.Interval := TimerInterval;
    end;
  finally
    if Timer <> nil then
      Timer.Enabled := True;
  end;
end;

procedure TUnDmkWeb.AbrirAppAcessoRemoto;
var
  App, Diretorio, Destino: String;
begin
  App       := dmkPF.ExtractUrlFileName(CO_WEB_URL_APP_REMOTO_SUPORTE);
  Diretorio := ExtractFilePath(Application.ExeName);
  Destino   := Diretorio + App;
  //
  if not FileExists(Destino) then
  begin
    if UFTP.DownloadFileHTTPIndy(False, nil, nil, CO_WEB_URL_APP_REMOTO_SUPORTE,
      App, Destino) then
    begin
      if not Geral.AbreArquivo(Destino, True) then
        DeleteFile(Destino);
    end else
      Geral.MB_Aviso('Falha a realizar download do aplicativo de acesso remoto!');
  end else
  begin
    if not Geral.AbreArquivo(Destino, True) then
      DeleteFile(Destino);
  end;
end;

procedure TUnDmkWeb.AtualizaSolicitApl2(Query: TmySQLQuery;
  DataBase: TmySQLDatabase; Timer: TTimer; TrayIcon: TTrayIcon;
  TBButton: (*TToolBarButton*)TControl; BalloonHint: TBalloonHint);

  function AtualizaMaxHistoricoDB(CodConta, MaxHistorico: Integer): Boolean;
  var
    DataHoraSinc: String;
  begin
    Result       := False;
    DataHoraSinc := Geral.FDT(Date, 9);
    //
    if UMyMod.SQLInsUpd(Query, stUpd, 'emailconta', False,
      ['MaxHistorico', 'ErrSinc', 'DataHoraSinc'], ['Codigo'],
      [MaxHistorico, 0, DataHoraSinc], [CodConta], True) then
    begin
      Timer.Interval := TimerInterval;
      Result         := True;
    end;
  end;

  function AtualizaErrSincHistoricoDB(CodConta, ErrSinc: Integer): Boolean;
  var
    Erro: Integer;
  begin
    Result := False;
    Erro   := ErrSinc + 1;
    //
    if UMyMod.SQLInsUpd(Query, stUpd, 'emailconta', False,
      ['ErrSinc'], ['Codigo'], [Erro], [CodConta], True) then
    begin
      Timer.Interval := Timer.Interval + TimerInterval;
      Result         := True;
    end;
  end;

var
  MaxHistorico, CodConta, ErrSinc, CodRet, NovoHistorico, RecNotify: Integer;
  Usuario, Msg: String;
  DataHoraSinc: TDateTime;
  Json: TJSONObject;
  //Res: ArrayOfStrings;
begin
  if RemoteConnection then
  begin
    if Query = nil then
      Exit;
    //
    CodConta := ReabreEmailConta(Query, DataBase, 0, VAR_USUARIO);
    //
    if CodConta <> 0 then
    begin
      ErrSinc      := Query.FieldByName('ErrSinc').AsInteger;
      MaxHistorico := Query.FieldByName('MaxHistorico').AsInteger;
      Usuario      := Query.FieldByName('Usuario').AsString;
      DataHoraSinc := Query.FieldByName('DataHoraSinc').AsDateTime;
      //
      if (ErrSinc < 3) or ((ErrSinc >= 3) and (DataHoraSinc + 24 <= Date)) then
      begin
        try
          Timer.Enabled := False;
          try
            (*
            Res           := GetVCLWebServicePortType2.SOAP_VerificaMaxHistorico(Usuario);
            CodRet        := Geral.IMV(Res[0]);
            NovoHistorico := Geral.IMV(Res[2]);
            RecNotify     := Geral.IMV(Res[3]);
            *)
            if Grl_DmkREST.VerificaMaxHistorico(False, Usuario, Msg) then
            begin
              Json          := Grl_DmkREST.JsonSingleToJsonObject(Msg);
              // ini 2022-07-19
              if Json <> nil then
              begin
                CodRet        := 100;
                NovoHistorico := Geral.IMV(Grl_DmkREST.GetJsonValue(Json, 'historico'));
                RecNotify     := Geral.IMV(Grl_DmkREST.GetJsonValue(Json, 'recNotify'));
              end else
              begin
                CodRet        := 0;
                NovoHistorico := 0;
                RecNotify     := 0;
              end;
            end else
            begin
              CodRet        := 0;
              NovoHistorico := 0;
              RecNotify     := 0;
            end;
            //
            if (RecNotify = 1) and (CodRet = 100) then
            begin
              AtualizaMaxHistoricoDB(CodConta, NovoHistorico);
              //
              if NovoHistorico <> MaxHistorico then
              begin
                if (TBButton <> nil) and (BalloonHint <> nil) then
                begin
                  MostraBalloonHintMenuTopo(TBButton, BalloonHint,
                    CO_SUPORTE_TITLE, CO_SUPORTE_DESCRI);
                end;
                TrayIcon.Visible := True;
                TrayIcon.ShowBalloonHint;
              end;
            end;
          except
            AtualizaErrSincHistoricoDB(CodConta, ErrSinc);
          end;
        finally
          Timer.Enabled := True;
        end;
      end;
    end;
  end;
end;
{$EndIf}
{ ********** Alertas de atualiza��es OS Fim ***************************** }

function TUnDmkWeb.ObtemTipoDeDispositivo(Dispositivo: TDispositivo): Integer;
begin
  case Dispositivo of
    dtWEB: 
      Result := 0;
    dtDesktop: 
      Result := 1;
    dtMobile: 
      Result := 2;
  end;
end;

function TUnDmkWeb.URLExiste(URL: String): Boolean;
var
  HTTP: TIdHTTP;
begin
  Result := False;
  HTTP   := TIdHTTP.Create;
  //
  ConfiguraHTTP(HTTP); //Configurar o Proxy
  try
    HTTP.ConnectTimeout := 3000;
    HTTP.ReadTimeout    := 3000;
    HTTP.Get(URL);
    //
    if HTTP.Response.ResponseCode = 200 then
      Result := True;
  finally
    HTTP.Free;
  end;
end;

function TUnDmkWeb.URLGet(URL: String; var Resultado: TFileStream): Boolean;
var
  HTTP: TIdHTTP;
  Valor: String;
begin
  Result := False;
  HTTP   := TIdHTTP.Create;
  //
  ConfiguraHTTP(HTTP); //Configurar o Proxy
  try
    HTTP.Request.ContentType := 'application/x-www-form-urlencoded';
    try
      HTTP.Get(URL, Resultado);
    except
      // nada!
    end;
    //
    Result := True;
  finally
    HTTP.Free;
  end;
end;

function TUnDmkWeb.URLGetMem(const URL: String; var
  Resultado: String; const AvisaErro: Boolean): Boolean;
var
  IdHttp1: TIdHTTP;
begin
 Result := False;
  IdHttp1 := TIdHTTP.Create(nil);
  try
    try
      IdHttp1.ConnectTimeout := 500;
      Resultado := IdHTTP1.Get(URL);
      Result := True;
    except
      on E: Exception do
      //on E: EIdException do
      begin
       // if AvisaErro then
         // MessageDlg('ERROR: Unable to URLGetMem. ' + sLineBreak +
          //E.ClassName + ': ' + E.Message + '.', mtError, [mbOk], 0);
      end;
    end;
  finally
    IdHttp1.Free;
  end;
end;

function TUnDmkWeb.URLPost(URL: String; VarsValues: array of String;
  var Resultado: TStringStream): Boolean;
var
  HTTP: TIdHTTP;
  Vars: TStringList;
  i: Integer;
  Valor: String;
begin
  Result := False;
  Vars   := TStringList.Create;
  HTTP   := TIdHTTP.Create;
  //
  ConfiguraHTTP(HTTP); //Configurar o Proxy
  try
    for i := 0 to Length(VarsValues) - 1 do
    begin
      Valor := VarsValues[i];
      if Valor <> '' then
        Vars.Add(Valor);
    end;
    //
    HTTP.Request.ContentType := 'application/x-www-form-urlencoded';
    HTTP.Post(URL, Vars, Resultado);
    //
    Result := True;
  finally
    Vars.Free;
    HTTP.Free;
  end;
end;

procedure TUnDmkWeb.WebBrowserPost(URL: String; WebBrowser: TWebBrowser; VarName,
  VarValue: array of String);
var
  EncodedDataString: string;
  PostData: OleVariant;
  Headers: OleVariant;
  j, i, CountVarName, CountVarVal: integer;
begin
  CountVarName := Length(VarName);
  CountVarVal  := Length(VarValue);

  if CountVarName <> CountVarVal then
  begin
    Geral.MB_Erro('O total de vari�veis deve ser o mesmo do total de valores!');
    Exit;
  end;

  if CountVarName = 0 then
  begin
    Geral.MB_Erro('O total de vari�veis deve ser maior que 0 (Zero)!');
    Exit;
  end;

  for j := 0 to CountVarName - 1 do
  begin
    if j = (CountVarName - 1) then
      EncodedDataString := EncodedDataString + VarName[j] + '=' + HTTPEncode(VarValue[j])
    else
      EncodedDataString := EncodedDataString + VarName[j] + '=' + HTTPEncode(VarValue[j]) + '&'
  end;    
  {
  EncodedDataString := 'id=' + HTTPEncode(Geral.FF0(VAR_WEB_CODUSER)) + '&' +
                       'wordserass=' + HTTPEncode(Geral.FF0(EdSolicit.ValueVariant));
  }
  PostData := VarArrayCreate([0, length(EncodedDataString)-1], varByte);

  for i := 1 to length(EncodedDataString) do
    PostData[i-1] := ord(EncodedDataString[i]);

  Headers := 'Content-type: application/x-www-form-urlencoded' + sLineBreak;

  if WebBrowser <> nil then
  begin
    WebBrowser.Navigate(URL, EmptyParam, EmptyParam, PostData, Headers)
  end else
  begin
    {$IfDef UsaWSuport}
    Application.CreateForm(TFmWebBrowser, FmWebBrowser);
    FmWebBrowser.WindowState := wsMaximized;
    FmWebBrowser.FURLPadrao  := URL;
    FmWebBrowser.WebBrowser1.Navigate(URL, EmptyParam, EmptyParam, PostData, Headers);
    FmWebBrowser.ShowModal;
    FmWebBrowser.Destroy;
    {$Else}
    Geral.MB_Aviso('Para usar a janela "FmWebBrowser" � necess�rio definir a vari�vel global "UsaWSuport"!');
    {$EndIf}
  end;
end;

function TUnDmkWeb.BitmapToString(Bitmap: TBitmap): String;
const
  BPP = 8; //Note: BYTES per pixel
var
  x, y: Integer;
  S: String;
begin
  S := '';
  for y := 0 to Bitmap.Height-1 do
  begin
    for x := 0 to Bitmap.Width-1 do
    begin
      S := S + IntToHex(Bitmap.Canvas.Pixels[x,y], BPP);
    end;
    S := S + '\';
  end;
  Result := S;
end;

function TUnDmkWeb.StringToBitmap(S: String): TBitmap;
const
  BPP = 8; //Note: BYTES per pixel
var
  Bitmap: TBitmap;
  Line: String;
  P: Integer;
  x, y: Integer;
begin
  Bitmap := TBitmap.Create;
  P := pos('\', S);
  Bitmap.Width := P div BPP;
  Bitmap.Height := 1;

  Line := Copy(S, 1, P-1);
  Delete(S, 1, P);
  x := 0;
  y := 0;

  while P <> 0 do
  begin
    if length(Line) <> 0 then
    begin
      Bitmap.Canvas.Pixels[x, y] := StrToInt('$' + Copy(Line, 1, BPP));
      Delete(Line, 1, BPP);
      inc(x);
    end else
    begin
      P := pos('\', S);
      Line := Copy(S, 1, P-1);
      Delete(S, 1, P);
      inc(y);
      Bitmap.Height := y+1;
      x := 0;
    end;
  end;
  Bitmap.Height := Bitmap.Height - 1;
  Result := Bitmap;
end;

function TUnDmkWeb.CapturaTela(Dir, NomeArq: String): String;
var
  DirExiste: Boolean;
  Jpeg: TJPEGImage;
  Bmp: TBitmap;
  //Dc: HDC;
  //Cv: TCanvas;
begin
  Result := '';
  //
  if Length(Dir) = 0 then
  begin
    Dir := ExtractFilePath(Application.ExeName) + 'PrntScrn\';
    //Dir := DmkPF.ObterCaminhoMeusDocumentos() + '\Dermatek\PrntScrn';
  end;
  //
  if Length(NomeArq) = 0 then
  begin
    NomeArq   := Geral.FDT(Now, 9);
    NomeArq   := Geral.SoNumero_TT(NomeArq);
    NomeArq   := NomeArq + '.jpg';
  end;
  (*
  Desativado por causa do Windows Vista
  if DirectoryExists(Dir) then
    DeleteFolder(Dir, False);
  *)
  //
  try
    DirExiste := ForceDirectories(Dir);
  except
    DirExiste := False;
  end;
  //
  if DirExiste then
  begin
    try
      Bmp  := TBitmap.Create;
      Jpeg := TJPEGImage.Create;
      //
      Clipboard.Clear;
      keybd_event(vk_snapshot,0, 0, 0);
      Application.ProcessMessages;
      //
      try
        Bmp.LoadFromClipboardFormat(CF_BITMAP, clipboard.GetAsHandle(CF_BITMAP), 0);
      except
        ;
      end;
      if Bmp <> nil then
      begin
        Jpeg.Assign(Bmp);
        Jpeg.SaveToFile(Dir + NomeArq);
      end;
    finally
      Bmp.Free;
      Jpeg.Free;
    end;
    Result := Dir + NomeArq;
  end;
end;

procedure TUnDmkWeb.CarregaImagemFormWEB(Imagem: TdmkImage);
  function stWEB(): WideString;
  begin
    Result :=
      '00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF' +
      '00FF00FF00FF00FF00FF00FF00C99B4C00C7933B00C07D0E00C07D0E00C07D0E' +
      '00C07D0E00C07D0E00C07D0E00C7933B00C99B4C00FF00FF00FF00FF00FF00FF' +
      '00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF' +
      '\00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00F' +
      'F00FF00FF00C7933B00BE841B00A8B469008BD8A00083F7D30075FFE10075FFE' +
      '10075FFE10075FFE10083F7D30091D59F00A8B46900BE841B00C99B4C00FF00F' +
      'F00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00F' +
      'F\00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00D1B2' +
      '7300B5841A009BCE8F0075FFE1008CFFE7008CFFE7008CFFE70083F7D3006AF8' +
      'D30056F0BA0058DD920047E19D003CF2C3006AF8D30083F7D300A7C68700BE84' +
      '1B00D1B27300FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00' +
      'FF\00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BC8B2900CE9' +
      'D4100EDE3AC00BEFBE100C1FEEC009AFBE10094F9DD00E7E5BA00E9DFB000E5D' +
      'CA800FBD69100FFCF8E00FFB758009DB74C0000E397001DE5A30056F0C30086E' +
      'DC400CE9D4100C38C2B00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0' +
      '0FF\00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BC8B2900DEB76B00FF' +
      'E7BF00C1FEEC00B1FCE5009AFBE100AAF4D500FFE3AD00FFE3AD00F9DCA400F9' +
      'D69A00F8D19000F8D19000FFCB7A00EFBD5B0049CC720000E3970000DE8E003A' +
      'E1A30086EDC400CDB26300C38C2B00FF00FF00FF00FF00FF00FF00FF00FF00FF' +
      '00FF\00FF00FF00FF00FF00FF00FF00FF00FF00BC8B2900DEB76B00FFE4B600F' +
      'DF1D800FDEDCF009AFBE10083F7D300DEE9BF00FFE3AD00F9DCA400F9DCA400F' +
      '8D79500FFCF8E00D9CF8800B8D08600FFBC6200FFAD4100CEAF410000D98A000' +
      '0D98A0000DE940071E8B600CDB26300C38C2B00FF00FF00FF00FF00FF00FF00F' +
      'F00FF\00FF00FF00FF00FF00FF00FF00CDAD7000CE9D4100FFDDA300FDF1D800' +
      'FDEDCF00FFE9C600FFE9C600BBF1C900CEE9BD00FAE0AD00FFDDA300FFD89C00' +
      'FFCF8E0071E8B60029EFB30013EDB00058DD9200D5BB5500EEAD3A00F7A02800' +
      '74BE5A0000D98A0000D98A008EE1AD00B69D4400CDAD7000FF00FF00FF00FF00' +
      'FF00FF\00FF00FF00FF00FF00FF00FF00B5841A00F9D69A00FFE7BF00FDEDCF0' +
      '0FFE9C600FFE7BF00CEE9BD007FF5CF005CF7CE00BCE7B500FFD89C00E3D8980' +
      '0F4CD83003DE9AE005CE09B001DE5A30000E7A3002CD581002CD58100CEAF410' +
      '09DB74C0019D0780000DE9400AAB044008EE1AD00C8831700FF00FF00FF00FF0' +
      '0FF00FF\00FF00FF00FF00FF00C99B4C00E2BC7200F9D69A00FDEDCF00FFE9C6' +
      '00FFE9C600BBF1C9006AF8D3005CF7CE005CF7CE0056F0C30056F0BA003CEFB6' +
      '0029EFB30013EDB00082D58700DABF620012DF920018D98A00F8AB3900F8AB39' +
      '00F1A42F00F7A02800C6A5300012CB74001DE5A30099C07A00C99B4C00FF00FF' +
      '00FF00FF\00FF00FF00FF00FF00BB852000F8D79500F9DCA400FFE9C600FFE7B' +
      'F00FFE4B600FFE4B600FFE4B600A5EEBD0048F5C40048F5C4003CEFB60029EFB' +
      '3001BEFAE0009ECA9007AD3810000E69C0099C0610000E69C00BFB64E00E6AB3' +
      '900E4A63100E4A63100DD9F260012CB740000D482003DE9AE00CC821600FF00F' +
      'F00FF00FF\00FF00FF00B78E3B00D4AA5400F2C67100F9E2B400F9E2B400F9E2' +
      'B400F9E2B400FFE3AD00FFE3AD00EDE3AC003CF2C3003CEFB60029EFB30020ED' +
      'AD0009ECA900C7C86F0080D2760000E69C00A6C56500FFAD4100EEAD3A00E6AB' +
      '3900E6AB3900E0A53000E3A02900F397150038C3650006E09B00ACA95300B78E' +
      '3B00FF00FF\00FF00FF00BB8F3B00E2BC7200EFBA5200FFE7BF00F9E2B400FAE' +
      '0AD00FAE0AD00FAE0AD00FFDDA300D9E1A50027F4BA0029EFB30020EDAD0010F' +
      '0A90021E59B00FFB758009AC96F0000E69C0012DF920099C06100F8AB3900E6A' +
      '83700E6A83700DEA22A00DEA22A00E79A1B008BAD3F0000D98A007BC78100C79' +
      '33B00FF00FF\00FF00FF00B5841A00F4CD8300EFBA5200F9E2B400FAE0AD00F9' +
      'DCA400F9DCA400F9DCA400FFD89C00B5E0A0001BEFAE001BEFAE0010F0A90000' +
      'E7A30000E7A30000E7A30044DB890000E69C0000E3970000E3970066CC6600E6' +
      'A83700E4A63100DEA22A00DD9F2600DD9F2600F397150000D98A0043D89400C8' +
      '831700FF00FF\00FF00FF00B5841A00F2C67100EFBA5200F9DCA400F9DCA400F' +
      '9D69A00F9D69A00FBD69100FFCF8E00B3DC960013EDB00010F0A90000E7A3000' +
      '0E69C0000E69C0000E69C0012DF920000E3970000E3970018D98A009DB74C00E' +
      '6A83700E0A53000DEA22A00DD9F2600D99A1C00E696150012CB740006E09B00C' +
      'C821600FF00FF\00FF00FF00B5841A00EFBF6200EFBA5200F4CD8300F8D79500' +
      'F8D79500F8D19000F8D19000FFCB7A002FEEAB0000F1AC0000EEA60000EEA600' +
      '00E69C0000E69C0000E69C0000E3970000E3970000E3970070C76B00F8AB3900' +
      'E6A83700E0A53000DEA22A00DD9F2600D99A1C00E696150048BF5C0000E39700' +
      'CC821600FF00FF\00FF00FF00B5841A00EFBD5B00F1B75100EFBF6200F8D1900' +
      '0F4CD8300F4CD8300F4CD8300FFC56D0010F0A90000EEA60000EEA60000EEA60' +
      '000E69C0000E69C0000E69C0000E69C0000E69C0066CC6600F8AB3900E6A8370' +
      '0E6A83700DEA22A00DEA22A00DD9F2600D99A1C00DD930F00B49D200000E69C0' +
      '0C8831700FF00FF\00FF00FF00B5841A00F6B5490097D07400F6B54900F4C269' +
      '00F1C87800F2C67100F4C26900FFBC620065DB890000F1AC0000EEA60000EEA6' +
      '0000E69C0000E69C0000E69C0000E69C0000E69C00FFA63200EEAD3A00E6A837' +
      '00E0A53000DEA22A00DD9F2600D99A1C00D99A1C00D6941100DC900900DF8E06' +
      '00BE841B00FF00FF\00FF00FF00B5841A00E9B44A0001FDB800D5BB5500F6B54' +
      '900EFBD5B00EFBF6200EFBD5B00F8BA5500DABF620000F1AC0000EEA60000E7A' +
      '30000E7A30000E7A30000E7A30000E7A3004FD17900F8AB3900E6A83700E0A53' +
      '000E0A53000DD9F2600DD9F2600D99A1C00D7971700D6941100D6930A00D48B0' +
      '000BB852000FF00FF\00FF00FF00BB8F3B00E4A631003AE1A30074D77F00EFAF' +
      '4300ECB54B00ECB54B00ECB54B00F1B75100F1B751001DE5A30000F1AC0000EE' +
      'A60000EEA60049D782006ACD740012DF9200B5B54A00F1A42F00E4A63100E4A6' +
      '3100DEA22A00DD9F2600D99A1C00D99A1C00D6941100D6941100D88D0500CE86' +
      '0900BB924000FF00FF\00FF00FF00B78E3B00D3942700A6C5650080D27600EFA' +
      'F4300E9B44A00E9B44A00E9B44A00E9B44A00F6B54900A6C5650044DB890000E' +
      'EA6005DD27B00FFA63200FFA63200B5B54A008AC05D00F1A42F00ECA22A00E3A' +
      '02900E3A02900DD9F2600D99A1C00D7971700D6941100D6941100D48B0000C48' +
      '91100B78E3B00FF00FF\00FF00FF00FF00FF00BE841B00EEAD3A00EEAD3A00E7' +
      'AF4200E7AF4200E7AF4200E7AF4200E7AF4200E7AF4200E7AF4200B8BC580000' +
      'F1AC0000E69C00B2B9520022DD8C009DB74C0079C3620000DE94003DCE7500F3' +
      '991C00E79A1B00E79A1B00E6961500DD930F00DC900900D88D0500D48B0000BB' +
      '852000FF00FF00FF00FF\00FF00FF00FF00FF00B78E3B00DB9E2800E6AB3900E' +
      '6AB3900E6AB3900E7AF4200E7AF4200E7AF4200E7AF4200E7AF4200F8AB3900B' +
      '2B9520049D7820000E69C0000F1AC0081C05E00FE9C2000FF981A0094B54A009' +
      '4B54A006ABD5700B0A42E008BAD3F00DC9009009F9A280076AC3A00CE860900B' +
      'B944A00FF00FF00FF00FF\00FF00FF00FF00FF00FF00FF00BB852000E0A53000' +
      'E6A83700E6A83700E6A83700E6AB3900E6AB3900E6AB3900E6AB3900E6AB3900' +
      'E6AB3900F1A42F00F7A028009DB74C0000E69C00B6AD3D0018D98A0000E69C00' +
      '00E69C0000E3970000DE940000DE940000DE940000DE94009E9C1F00BE841B00' +
      'FF00FF00FF00FF00FF00FF\00FF00FF00FF00FF00FF00FF00BD9C6100C38D200' +
      '0E4A63100E0A53000E0A53000E0A53000E0A53000E0A53000E0A53000E0A5300' +
      '0E0A53000E0A53000E3A02900E3A02900F3991C0094B54A0000E69C0000E3970' +
      '000E3970000DE8E0000DE8E0000DE8E0000DE8E0026C96B00D1820D00BD9C610' +
      '0FF00FF00FF00FF00FF00FF\00FF00FF00FF00FF00FF00FF00FF00FF00B78A2F' +
      '00D3942700DEA22A00DEA22A00DEA22A00DEA22A00DEA22A00DEA22A00DEA22A' +
      '00DEA22A00DEA22A00DEA22A00DD9F2600E79A1B0037D0780000E69C0000DE94' +
      '0000DE8E0000DE8E0000DE8E0000DE8E0000D48200BC8C1400BC8B2900FF00FF' +
      '00FF00FF00FF00FF00FF00FF\00FF00FF00FF00FF00FF00FF00FF00FF00FF00F' +
      'F00B78A2F00D3942700DD9F2600DD9F2600DD9F2600DD9F2600DD9F2600DD9F2' +
      '600DD9F2600DD9F2600DD9F2600D99A1C00E696150037D0780000E69C0000E39' +
      '70000DE940000DE940000DE940019D07800CE860900C38C2B00FF00FF00FF00F' +
      'F00FF00FF00FF00FF00FF00FF\00FF00FF00FF00FF00FF00FF00FF00FF00FF00' +
      'FF00FF00FF00B78A2F00C38D2000DD9F2600DD9F2600DD9F2600DD9F2600D99A' +
      '1C00D99A1C00D99A1C00D99A1C00D99A1C00DD930F00DD930F003DCE750014D7' +
      '850000E3970000E3970048BF5C00C4891100C38C2B00FF00FF00FF00FF00FF00' +
      'FF00FF00FF00FF00FF00FF00FF\00FF00FF00FF00FF00FF00FF00FF00FF00FF0' +
      '0FF00FF00FF00FF00FF00B6995D00BB852000CF921C00DB9B1800DB9B1800D79' +
      '71700D7971700D7971700D7971700D7971700D6941100DF8E0600BB9B1D0000E' +
      '69C0038C365009F9A2800CC821600B6995D00FF00FF00FF00FF00FF00FF00FF0' +
      '0FF00FF00FF00FF00FF00FF00FF\00FF00FF00FF00FF00FF00FF00FF00FF00FF' +
      '00FF00FF00FF00FF00FF00FF00FF00FF00FF00BC8E4200BB852000C38D2000CF' +
      '921C00D6941100D6941100D6930A00D6930A00D6930A00D88D050076AC3A00C4' +
      '891100C8831700BC8E4200FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF' +
      '00FF00FF00FF00FF00FF00FF00FF\00FF00FF00FF00FF00FF00FF00FF00FF00F' +
      'F00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00BC8E4200B' +
      '78E3B00BB852000BB852000BB852000BB852000BB852000BB852000BB8F3B00B' +
      'C8E4200FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00F' +
      'F00FF00FF00FF00FF00FF00FF00FF\00FF00FF00FF00FF00FF00FF00FF00FF00' +
      'FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00' +
      'FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00' +
      'FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00' +
      'FF00FF00FF00FF00FF00FF00FF00FF\00FF00FF00FF00FF00FF00FF00FF00FF0' +
      '0FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0' +
      '0FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0' +
      '0FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0' +
      '0FF00FF00FF00FF00FF00FF00FF00FF\';
  end;
var
  Img: WideString;
begin
  Img := stWEB();
  //
  Imagem.Picture.Bitmap := StringToBitmap(Img);
end;

procedure TUnDmkWeb.CarregaTextNoWebBrowser(Browser: TWebBrowser;
  Texto: String);
var
  sl: TStringList;
  ms: TMemoryStream;
begin
  Browser.Navigate('about:blank') ;
  while Browser.ReadyState < READYSTATE_INTERACTIVE do
    Application.ProcessMessages;
  if Assigned(Browser.Document) then
  begin
    sl := TStringList.Create;
    try
      ms := TMemoryStream.Create;
      try
        sl.Text := Texto;
        sl.SaveToStream(ms) ;
        ms.Seek(0, 0) ;
        (Browser.Document as IPersistStreamInit).Load(TStreamAdapter.Create(ms)) ;
      finally
        ms.Free;
      end;
    finally
      sl.Free;
    end;
  end;
end;

{$IfDef UsaWSuport}
procedure TUnDmkWeb.MostraWSuporte(NaoGeraPrint: Boolean = False;
  Aba: Integer = 0; BalloonHint: TBalloonHint = nil; TxtPesq: String = '');

  function VerificaSeTemSubAncora(var NJ: String; TSn: TTabSheet;
    var Fm: TForm): TTabSheet;
  var
    I, J: Integer;
    PCi: TdmkPageControl;
  begin
    Result := nil;
    Fm     := nil;
    for I := 0 to TSn.ComponentCount - 1 do
    begin
      if TComponent(TSn.Components[I]) is TForm then
      begin
        Fm := TForm(TSn.Components[I]);
        for J := 0 to Fm.ComponentCount -1 do
        begin
          if Fm.Components[j] is TdmkPageControl then
          begin
            PCi := TdmkPageControl(Fm.Components[J]);
            if PCi.EhAncora then
              Result := PCi.ActivePage;
          end;
        end;
      end;
    end;
  end;
var
  p, I: Integer;
  JanName, NomeJan, PrntScrn, NomeAnc, PesqTxt: String;
  Fm, FmInterno: TForm;
  PC: TdmkPageControl;
  TS, TSn: TTabSheet;
  Rich: TRichEdit;
begin
  if BalloonHint <> nil then
    BalloonHint.HideHint;
  //
  NomeJan := '';
  NomeAnc := '';
  PesqTxt := TxtPesq;
  //
  //Setado antes para n�o dar erro na hora de gerar o Print
  if FmWSuporte = nil then
  begin
    if NaoGeraPrint = False then
      PrntScrn := CapturaTela('', '');
  end;
  if ConectarUsuarioWEB(False) then
  begin
    if NaoGeraPrint = False then
    begin
      // 2013-06-21
      //if TFm???(Self).Owner is TApplication then
      Fm := Screen.ActiveForm;
      JanName:= Trim(Fm.Name);
      //Geral.MB_Info(JanName);
      if JanName = 'FmPrincipal' then
      begin
        NomeAnc := 'Principal';
        for I := 0 to Fm.ComponentCount - 1 do
        begin
          if Fm.Components[I] is TdmkPageControl then
          begin
            PC := TdmkPageControl(Fm.Components[I]);
            if PC.EhAncora then
            begin
              TS := PC.ActivePage;
              NomeJan := Trim(TS.Caption);
              TSn := TS;
              while TSn <> nil do
              begin
                TSn := VerificaSeTemSubAncora(NomeJan, TSn, FmInterno);
                if TSn <> nil then
                  NomeJan := Trim(NomeJan + '->' + TSn.Caption);
              end;
            end;
          end;
        end;
      end else
      if JanName = 'FmdmkMsg' then
      begin
        for I := 0 to Fm.ComponentCount - 1 do
        begin
          if Fm.Components[I] is TRichEdit then
          begin
            if Fm.Components[I].Name = 'ReAviso' then
            begin
              Rich := TRichEdit(Fm.Components[I]);
              //
              PesqTxt := Rich.Text;
            end;
          end;
        end;
      end
      else
        NomeJan := Screen.ActiveForm.Caption;
      (*
      NomeJan:= Screen.ActiveForm.Caption;
      *)
      // FIM 2013-06-21

      p := Pos('::', NomeJan);
      if P > 1 then
        NomeJan := Trim(Copy(NomeJan, 1, P-1));
      //
      if NomeJan = 'Descanso' then
        NomeJan := '';
    end;
    //
    if PesqTxt = '' then
      PesqTxt := NomeJan;
    if PesqTxt = '' then
      PesqTxt := NomeAnc;
    //
    if FmWSuporte = nil then
    begin
      Application.CreateForm(TFmWSuporte, FmWSuporte);
      FmWSuporte.FPrntScrnImg := PrntScrn;
      FmWSuporte.FJanela      := NomeJan;
      FmWSuporte.FAncora      := NomeAnc;
      FmWSuporte.FTabIndex    := Aba;
      FmWSuporte.FPesqTxt     := PesqTxt;
      FmWSuporte.ShowModal;
      FmWSuporte.Destroy;
      //
      FmWSuporte := nil;
    end;
  end;
  if FileExists(PrntScrn) then
    DeleteFile(PrntScrn);
end;
{$EndIf}

{$IfDef UsaWSuport}
procedure TUnDmkWeb.MostraWTextos(DBn: TmySQLDatabase;
  QueryOpcoesGerl: TmySQLQuery);
var
  Link: String;
begin
  if ConexaoRemota(DBn, QueryOpcoesGerl, 1) and (ConectarUsuarioWEB(True)) then
  begin
    Link := 'http://www.dermatek.net.br/?page=textos&token=' + VAR_WEB_IDLOGIN;
    //
    MostraWebBrowser(Link, True, False, 0, 0);
  end;
end;
{$EndIf}

{$IfDef UsaWSuport}
procedure TUnDmkWeb.MostraWCalendar(DBn: TmySQLDatabase; QryWebParams: TmySQLQuery);
var
  Link: String;
begin
  if ConexaoRemota(DBn, QryWebParams, 1) and (ConectarUsuarioWEB(True)) then
  begin
    Link := 'http://www.dermatek.net.br/?page=calendar&token=' + VAR_WEB_IDLOGIN;
    //
    MostraWebBrowser(Link, True, False, 0, 0);
  end;
end;
{$EndIf}

procedure TUnDmkWeb.MostraWebBrowser(URL: String; NavegaPad: Boolean = True;
  Show: Boolean = True; Height: Integer = 600; Width: Integer = 600);
begin
  {$IfDef UsaWSuport}
  if not NavegaPad then
  begin
    Application.CreateForm(TFmWebBrowser, FmWebBrowser);
    FmWebBrowser.FURLPadrao := URL;
    //
    if (Height = 0) and (Width = 0) then
    begin
      FmWebBrowser.WindowState := wsMaximized;
    end else
    begin
      FmWebBrowser.WindowState := wsNormal;
      //
      if Height > 0 then
        FmWebBrowser.Height := Height;
      if Width > 0 then
         FmWebBrowser.Width := Width;
    end;
    //
    if not Show then
    begin
      FmWebBrowser.ShowModal;
      FmWebBrowser.Destroy;
    end else
      FmWebBrowser.Show;
  end else
    ShellExecute(Application.Handle, 'open', PChar(URL), '', '', 1);
  {$Else}
    ShellExecute(Application.Handle, 'open', PChar(URL), '', '', 1);
  {$EndIf}
end;

(*
function TUnDmkWeb.VerificaSeContaDmkExiste(Query: TmySQLQuery;
  DataBase: TmySQLDatabase; Usuario: String): Integer;
begin
  Result := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
    'SELECT Codigo ',
    'FROM emailconta ',
    'WHERE Usuario="' + Usuario + '"',
    'AND TipoConta=0 ',
    '']);
  if Query.RecordCount > 0 then
    Result  := Query.FieldByName('Codigo').AsInteger;
end;
*)

{$IfDef UsaWSuport}
{$IfNDef NO_USE_MYSQLMODULE}
function TUnDmkWeb.VerificaSeExisteSincroEmExecucao(Database: TmySQLDatabase;
  var DtaUltimaSincro: TDateTime): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  Qry    := TmySQLQuery.Create(TDataModule(Database.Owner));
  //
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DataBase, [
      'SELECT DtaSincro, SincroEmExec ',
      'FROM web_opcoes ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      DtaUltimaSincro := Qry.FieldByName('DtaSincro').AsDateTime;
      Result := Geral.IntToBool(Qry.FieldByName('SincroEmExec').AsInteger);
    end;
  finally
    Qry.Free;
  end;
end;
{$EndIf}
{$EndIf}

{$IfDef UsaWSuport}
function TUnDmkWeb.VerificaSeModuloWebEstaAtivo(DMKID_APP: Integer;
  HabilModulos: String; Database: TmySQLDatabase; var Msg: String): Boolean;
begin
  Msg    := '';
  Result := False;
  //
  if not Grl_Geral.LiberaModulo(DMKID_APP, Grl_Geral.ObtemSiglaModulo(mdlappWEB), HabilModulos) then //DControl
  begin
    Msg := 'Vo�� n�o est� habilitado para utilizar o m�dulo web!' + sLineBreak +
             'Solicite a libera��o junto a Dermatek!';
  end else
  begin
    (*
    if Database.Connected = False then
    begin
      Msg := 'O banco de dados WEB n�o est� conectado!';
    end else
    *)
    Result := True;
  end;
end;
{$EndIf}

{$IfNDef NO_USE_MYSQLMODULE}
function TUnDmkWeb.ReabreEmailConta(Query: TmySQLQuery;
  DataBase: TmySQLDatabase; TipoConta, PermisNiv: Integer): Integer;
begin
  Result := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
    'SELECT * ',
    'FROM emailconta ',
    'WHERE TipoConta=' + Geral.FF0(TipoConta),
    'AND PermisNiv=' + Geral.FF0(PermisNiv),
    '']);
  if Query.RecordCount > 0 then
    Result := Query.FieldByName('Codigo').AsInteger;
end;
{$EndIf}

{$IfDef UsaWSuport}
function TUnDmkWeb.InsereContaDmk(SQLType: TSQLType; QrUpd, Query: TmySQLQuery;
  DataBase: TmySQLDatabase; Usuario, Senha: String; CodUsuarioLocal,
  Codigo: Integer): Integer;
const
  TipoConta = 0; //Login Dermatek
  Nome = 'Conta Dermatek';
  //Mensagem = 'Inclus�o abortada!' + sLineBreak + 'Motivo: Esta conta j� foi cadastrada!';
var
  CodigoConta(*, LocCodigo*): Integer;
begin
  Result := 0;
  //LocCodigo := VerificaSeContaDmkExiste(Query, DataBase, Usuario);
  //
  if SQLType = stIns then
  begin
    (*
    if LocCodigo <> 0 then
    begin
      Geral.MB_Aviso(Mensagem);
      Exit;
    end;
    *)
    CodigoConta := UMyMod.BuscaNovoCodigo_Int(QrUpd, 'emailconta', 'Codigo',
                     [], [], stIns, 0, siPositivo, nil);
  end else
  begin
    CodigoConta := Codigo;
    (*
    if (LocCodigo <> 0) and (LocCodigo <> CodigoConta) then
    begin
      Geral.MB_Aviso(Mensagem);
      Exit;
    end;
    *)
  end;
  //
  QrUpd.SQL.Clear;
  if SQLType = stIns then
    QrUpd.SQL.Add('INSERT INTO emailconta SET ')
  else
    QrUpd.SQL.Add('UPDATE emailconta SET ');
  QrUpd.SQL.Add('Nome=:P0, TipoConta=:P1, PermisNiv=:P2, ');
  QrUpd.SQL.Add('Password=AES_ENCRYPT(:P3, :P4), Usuario=:P5, ');
  //
  if SQLType = stIns then
    QrUpd.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else
    QrUpd.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  //
  QrUpd.Params[00].AsString  := Nome;
  QrUpd.Params[01].AsInteger := TipoConta;
  QrUpd.Params[02].AsInteger := CodUsuarioLocal;
  QrUpd.Params[03].AsString  := Senha;
  QrUpd.Params[04].AsString  := CO_RandStrWeb01;
  QrUpd.Params[05].AsString  := Usuario;
  //
  QrUpd.Params[06].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  QrUpd.Params[07].AsInteger := VAR_USUARIO;
  QrUpd.Params[08].AsInteger := CodigoConta;
  QrUpd.ExecSQL;
  //
  Result := CodigoConta;
end;
{$EndIf}

function TUnDmkWeb.InsereWOrdSerArq(AplicativoSolicitante, Codigo,
  FTPWebArq: Integer; var Msg: String): Integer;
var
  //Res: ArrayOfStrings;
  Json: TJSONObject;
begin
  try
    (*
    Res := GetVCLWebServicePortType2.SOAP_InsereWOrdSerArq(
             AplicativoSolicitante, VAR_WEB_USR_ID, VAR_WEB_IDAPI,
             Codigo, FTPWebArq);
    Result := Geral.IMV(Res[0]);
    Msg    := Res[1];
    *)
    if Grl_DmkREST.InsereWOrdSerArq(False, VAR_WEB_USR_ID, Codigo, FTPWebArq,
      Msg) then
    begin
      Json   := Grl_DmkREST.JsonSingleToJsonObject(Msg);
      Msg    := Grl_DmkREST.GetJsonValue(Json, 'mensagem');
      Result := Geral.IMV(Grl_DmkREST.GetJsonValue(Json, 'controle'));
    end else
    begin
      Result := 0;
    end;
  except
    Result := 0;
    Msg    := 'Falha ao incluir registro!';
  end;
end;

function TUnDmkWeb.DownloadFile(SourceFile, DestFile: string): Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    Result := UrlDownloadToFile(nil, PChar(SourceFile), PChar(DestFile), 0, nil) = 0;
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    Result := False;
  end;
end;

function TUnDmkWeb.DownloadURL(const aUrl: string; Texto: TStrings): Boolean;
var
  hSession: HINTERNET;
  hService: HINTERNET;
  lpBuffer: array[0..1024 + 1] of Char;
  dwBytesRead: DWORD;
begin
  Result := False;
  // hSession := InternetOpen( 'MyApp', INTERNET_OPEN_TYPE_DIRECT, nil, nil, 0);
  hSession := InternetOpen('MyApp', INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);
  try
    if Assigned(hSession) then
    begin
      hService := InternetOpenUrl(hSession, PChar(aUrl), nil, 0, 0, 0);
      if Assigned(hService) then
        try
          while True do
          begin
            dwBytesRead := 1024;
            InternetReadFile(hService, @lpBuffer, 1024, dwBytesRead);
            if dwBytesRead = 0 then break;
            lpBuffer[dwBytesRead] := #0;
            Texto.Add(lpBuffer);
          end;
          Result := True;
        finally
          InternetCloseHandle(hService);
        end;
    end;
  finally
    InternetCloseHandle(hSession);
  end;
end;

function TUnDmkWeb.DownloadURL_NOCache(const aUrl: string;
  var s: String): Boolean;
var
  hSession: HINTERNET;
  hService: HINTERNET;
  lpBuffer: array[0..1024 + 1] of Char;
  dwBytesRead: DWORD;
begin
  Result := False;
  s := '';
  // hSession := InternetOpen( 'MyApp', INTERNET_OPEN_TYPE_DIRECT, nil, nil, 0);
  hSession := InternetOpen('MyApp', INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);
  try
    if Assigned(hSession) then
    begin
      hService := InternetOpenUrl(hSession, PChar(aUrl), nil, 0, INTERNET_FLAG_RELOAD, 0);
      if Assigned(hService) then
        try
          while True do
          begin
            dwBytesRead := 1024;
            InternetReadFile(hService, @lpBuffer, 1024, dwBytesRead);
            if dwBytesRead = 0 then break;
            lpBuffer[dwBytesRead] := #0;
            s := s + lpBuffer;
          end;
          Result := True;
        finally
          InternetCloseHandle(hService);
        end;
    end;
  finally
    InternetCloseHandle(hSession);
  end;
end;

function TUnDmkWeb.FTP_ObtemDadosFTPConf_SOAP(const AplicativoSolicitante,
  FTPConfig: Integer; var Passivo: Integer; var Host, User,
  Pass: String): Boolean;
var
  //ResConf: ArrayOfStrings;
  Mensagem: String;
  Json: TJSONObject;
begin
  try
    Result  := False;
    Host    := '';
    User    := '';
    Pass    := '';
    Passivo := 0;
    (*
    ResConf := GetVCLWebServicePortType2.SOAP_ObtemDadosFTP(
                 AplicativoSolicitante, VAR_WEB_USR_ID, VAR_WEB_IDAPI, FTPConfig);
    if ResConf[0] = '101' then
    begin
      Host    := ResConf[2];
      User    := ResConf[3];
      Pass    := ResConf[4];
      Passivo := Geral.IMV(ResConf[5]);
      //
      Result := True;
    end;
    *)
    if Grl_DmkREST.ObtemDadosFTP(False, FTPConfig, Mensagem) then
    begin
      Json := Grl_DmkREST.JsonSingleToJsonObject(Mensagem);
      //
      Host    := Grl_DmkREST.GetJsonValue(Json, 'host');
      User    := Grl_DmkREST.GetJsonValue(Json, 'user');
      Pass    := Grl_DmkREST.GetJsonValue(Json, 'pass');
      Passivo := Grl_Geral.IMV(Grl_DmkREST.GetJsonValue(Json, 'passivo'));
      //
      Result := True;
    end;
  except
    Result := False;
  end;
end;

function TUnDmkWeb.FTP_ObtemDadosFTPDir_SOAP(const AplicativoSolicitante,
  Diretorio: Integer; var CliInt, FTPConfig, FTPDir: Integer;
  var PastaCam: String): Boolean;
var
  //ResDir: ArrayOfStrings;
  Mensagem: String;
  Json: TJSONObject;
begin
  try
    Result    := False;
    FTPConfig := 0;
    FTPDir    := 0;
    PastaCam  := '';
    CliInt    := 0;
    (*
    ResDir    := GetVCLWebServicePortType2.SOAP_ObtemDadosFTPDir(
                   AplicativoSolicitante, VAR_WEB_USR_ID, VAR_WEB_IDAPI, Diretorio);
    //
    if ResDir[0] = '102' then
    begin
      FTPConfig := Geral.IMV(ResDir[2]);
      FTPDir    := Geral.IMV(ResDir[3]);
      PastaCam  := ResDir[4];
      CliInt    := Geral.IMV(ResDir[5]);
      //
      Result := True;
    end;
    *)
    if Grl_DmkREST.ObtemDadosFTPDir(False, Diretorio, Mensagem) then
    begin
      Json := Grl_DmkREST.JsonSingleToJsonObject(Mensagem);
      //
      FTPConfig := Geral.IMV(Grl_DmkREST.GetJsonValue(Json, 'ftpConfig'));
      FTPDir    := Geral.IMV(Grl_DmkREST.GetJsonValue(Json, 'codigo'));
      PastaCam  := Grl_DmkREST.GetJsonValue(Json, 'caminho');
      CliInt    := Geral.IMV(Grl_DmkREST.GetJsonValue(Json, 'cliInt'));
      //
      Result := True;
    end;
  except
    Result := False;
  end;
end;

procedure TUnDmkWeb.HistoricoDeAlteracoes(DmkId_App: Integer; Versao: Int64; Acao: TAcao);
var
  NVersao(*, IDMsg*): Int64;
  Mensagem, Link: String;
  //Res: ArrayOfStrings;
begin
  case Acao of
    dtConfigura:
      Geral.WriteAppKeyCU('NovaVersao', Application.Title, '1', ktInteger);
    dtMostra:
    begin
      NVersao := Geral.I64MV(Geral.ReadAppKeyCU('NovaVersao', Application.Title, ktInteger, 0));

      if NVersao = 1 then
      begin
        if RemoteConnection then
        begin
          try
            (*Res   := GetVCLWebServicePortType2.SOAP_VerificaHistoricoDeVersao(DmkId_App, Versao);
            IDMsg := Geral.IMV(Res[0]);

            if IDMsg = 101 then*)
            if not Grl_DmkREST.VerificaHistoricoDeVersao(False, DmkId_App, Versao, Mensagem) then
            begin
              Link := 'http://www.dermatek.net.br/?page=hisalt&aplicativo=' +
                        Geral.FF0(DmkId_App) + '&versao=' + Geral.FF0(Versao);

              MostraWebBrowser(Link, False, True, 720, 1100);
            end;
            Geral.WriteAppKeyCU('NovaVersao', Application.Title, '0', ktInteger);
          except
            ;
          end;
        end;
      end;
    end;
  end;
end;

function TUnDmkWeb.FTP_InsUpdRegistroFTPArquivo(AplicativoSolicitante: Integer;
  TipSQL: TTypeSQL; CliInt, Depto, Nivel, FTPDir: Integer; Tam, Nome,
  ArqNome: String; Origem, Ativo, Codigo: Integer; var Msg: String): Integer;
var
  TipSQLTxt: String;
  Json: TJSONObject;
  //Res: ArrayOfStrings;
begin
  try
    case TipSQL of
       dtIns: TipSQLTxt := 'dtIns';
       dtUpd: TipSQLTxt := 'dtUpd';
      dtAuto: TipSQLTxt := 'dtAuto';
    end;
    //
    (*
    Res := GetVCLWebServicePortType2.SOAP_InsUpdRegistroFTPArquivo(
             AplicativoSolicitante, VAR_WEB_USR_ID, VAR_WEB_IDAPI, TipSQLTxt,
             CliInt, Depto, Nivel, FTPDir, Tam, Nome, ArqNome, Origem,
             Ativo, Codigo);
    //
    if Geral.IMV(Res[0]) = 104 then
    begin
      Msg    := Utf8ToAnsi(Res[1]);
      Result := Geral.IMV(Res[2]);
    end else
    begin
      Msg    := Utf8ToAnsi(Res[1]);
      Result := 0;
    end;
    *)
    if Grl_DmkREST.InsUpdRegistroFTPArquivo(False, TipSQLTxt, CliInt, Depto,
      Nivel, FTPDir, Tam, Nome, ArqNome, Origem, Ativo, Codigo, Msg) then
    begin
      Json   := Grl_DmkREST.JsonSingleToJsonObject(Msg);
      Msg    := Grl_DmkREST.GetJsonValue(Json, 'mensagem');
      Result := Geral.IMV(Grl_DmkREST.GetJsonValue(Json, 'codigo'));
    end else
    begin
      Result := 0;
    end;
  except
    Msg    := 'Falha ao incluir registro!';
    Result := 0;
  end;
end;

function TUnDmkWeb.FTP_InsUpdRegistroFTPDiretorio(AplicativoSolicitante,
  Codigo, FTPConfig, TipoDir, CliInt, Depto, Nivel: Integer; Nome, Pasta,
  Caminho: String; var Msg: String): Integer;
var
  //Res: ArrayOfStrings;
  Json: TJSONObject;
begin
  try
    (*
    Res := GetVCLWebServicePortType2.SOAP_InsUpdRegistroFTPDiretorio(
             AplicativoSolicitante, VAR_WEB_USR_ID, VAR_WEB_IDAPI, Codigo,
             FTPConfig, TipoDir, CliInt, Depto, Nivel, Nome, Pasta, Caminho);
    //
    if Geral.IMV(Res[0]) = 104 then
    begin
      Msg    := Utf8ToAnsi(Res[1]);
      Result := Geral.IMV(Res[2]);
    end else
    begin
      Msg    := Utf8ToAnsi(Res[1]);
      Result := 0;
    end;
    *)
    if Grl_DmkREST.InsUpdRegistroFTPDiretorio(False, Codigo, FTPConfig, TipoDir,
      CliInt, Depto, Nivel, Nome, Pasta, Caminho, Msg) then
    begin
      Json   := Grl_DmkREST.JsonSingleToJsonObject(Msg);
      Msg    := Grl_DmkREST.GetJsonValue(Json, 'mensagem');
      Result := Geral.IMV(Grl_DmkREST.GetJsonValue(Json, 'codigo'));
    end else
    begin
      Result := 0;
    end;
  except
    Msg    := 'Falha ao incluir registro!';
    Result := 0;
  end;
end;

{$IfDef UsaWSuport}
function TUnDmkWeb.ConectarUsuarioWEB(SoAdmin: Boolean): Boolean;
var
  Conectado: Boolean;
  Res: ArrayOfStrings;
begin
  Conectado := False;
  //
  if VAR_WEB_USR_ID <> 0 then
  begin
    try
      Res := GetVCLWebServicePortType2.WEBVerificaSeEstaConectado(VAR_WEB_USR_ID);
      //
      if Res[0] = '2' then
        Conectado := False
      else
        Conectado := True;
    except
      Conectado := False;
    end;
  end;
  if not Conectado then
  begin
    if FmWLogin = nil then
    begin
      Application.CreateForm(TFmWLogin, FmWLogin);
      FmWLogin.ShowModal;
      FmWLogin.Destroy;
      //
      if SoAdmin then
      begin
        if (VAR_WEB_CONECTADO = 100) and (VAR_WEB_USR_TIPO in [0,1]) then
          Conectado := True;
      end else
      begin
        if VAR_WEB_CONECTADO = 100 then
          Conectado := True;
      end;
    end;
  end;
  Result := Conectado;
end;
{$EndIf}

{$IfNDef NO_USE_MYSQLMODULE}
function TUnDmkWeb.ConexaoRemota(DBn: TmySQLDatabase; Qry: TmySQLQuery;
  Aviso: Integer): Boolean;

  function ConectarAoSite(var IP: String; DBn: TmySQLDatabase;
    Qry: TmySQLQuery): Integer;
  const
    Msg = 'Falha ao estabelecer conex�o ao MySQL no meu site!';
  var
    Continua: Boolean;
    BD, PW, ID: String;
  begin
    Result := -2;
    if Qry.FieldByName('Web_MySQL').AsInteger > 0 then
    begin
      if not DBn.Connected then
      begin
        IP := Qry.FieldByName('Web_Host').AsString;
        ID := Qry.FieldByName('Web_User').AsString;
        PW := Qry.FieldByName('Web_Pwd_STR').AsString;
        BD := Qry.FieldByName('Web_DB').AsString;
        //
        if MyObjects.FIC(IP = '', nil, Msg) then Exit;
        if MyObjects.FIC(ID = '', nil, Msg) then Exit;
        if MyObjects.FIC(PW = '', nil, Msg) then Exit;
        if MyObjects.FIC(BD = '', nil, Msg) then Exit;
        //
        Continua := (LowerCase(IP) = 'localhost') or (IP = '127.0.0.1');
        //
        if not Continua then
          Continua := RemoteConnection(); //Tem internet
        //
        if Continua then
        begin
          try
            UnDmkDAC_PF.ConectaMyDB_DAC(DBn, BD, IP, VAR_PORTA,
              ID, PW, True, True, True);
            //
            Result := Geral.BoolToInt(DBn.Connected);
          except
            Result := 0;
          end;
        end else
          Result := 0;
      end else
        Result := 1;
    end else
      Result := -3;
  end;
var
  IP, Txt: String;
  rtrn: Integer;
begin
  rtrn := -3;
  try
    rtrn := ConectarAoSite(IP, DBn, Qry);
  finally
    if rtrn = 0 then
    begin
      case Aviso of
          0: Txt := '';// nada
          1: Txt := 'N�o h� conex�o com o servidor ' + IP + '!';
          2: Txt := 'N�o foi poss�vel se conectar ao servidor ' + IP + '!';
        else Txt := 'O aplicativo n�o est� conectado ao servidor WEB!';
      end;
      if Txt <> '' then
        Geral.MB_Aviso(Txt);
    end else
    if rtrn = -2 then
      Geral.MB_Aviso('A base de dados WEB n�o est� configurada!');
  end;
  if rtrn < 0 then rtrn := 0;
  Result := Geral.IntToBool(rtrn);
end;
{$EndIf}

procedure TUnDmkWeb.ConfiguraHTTP(IdHTTP: TIdHTTP; URL: String = '';
  IdSSL: TIdSSLIOHandlerSocketOpenSSL = nil);
var
  ProxServ, ProxUser, ProxPass: String;
  ProxBaAu, ProxEnab, ProxPort: Integer;
begin
  ProxUser := Geral.ReadAppKeyCU('ProxyUser', 'Dermatek', ktString, '');
  ProxPass := Geral.ReadAppKeyCU('ProxyPass', 'Dermatek', ktString, '');
  ProxEnab := Geral.ReadAppKeyCU('ProxyEnable', 'Dermatek', ktInteger, 0);
  ProxBaAu := Geral.ReadAppKeyCU('ProxyBasicAuth', 'Dermatek', ktInteger, 0);
  ProxServ := Geral.ReadAppKeyCU('ProxyServ', 'Dermatek', ktString, '');
  ProxPort := Geral.ReadAppKeyCU('ProxyPort', 'Dermatek', ktInteger, 0);
  //
  if (URL <> '') and (IdSSL <> nil) and (Pos('UTTPS', UpperCase(URL)) > 0) then
    IdHTTP.IOHandler := IdSSL
  else
    IdHTTP.IOHandler := nil;
  //
  if ProxEnab = 0 then
  begin
    IdHTTP.ProxyParams.BasicAuthentication := False;
    IdHTTP.ProxyParams.ProxyPassword       := '';
    IdHTTP.ProxyParams.ProxyPort           := 0;
    IdHTTP.ProxyParams.ProxyServer         := '';
    IdHTTP.ProxyParams.ProxyUsername       := '';
  end else
  begin
    IdHTTP.ProxyParams.BasicAuthentication := Geral.IntToBool(ProxBaAu);
    IdHTTP.ProxyParams.ProxyPassword       := dmkPF.PWDExDecode(ProxPass, CO_RandStrWeb01);
    IdHTTP.ProxyParams.ProxyPort           := ProxPort;
    IdHTTP.ProxyParams.ProxyServer         := ProxServ;
    IdHTTP.ProxyParams.ProxyUsername       := dmkPF.PWDExDecode(ProxUser, CO_RandStrWeb01);
  end;
end;

function TUnDmkWeb.TruaduzNivel(Tipo, CodigoApp: Integer): String;
begin
  //Os n�veis s�o espec�ficos de cada aplicativo
  //
  if CodigoApp in [17, 24] then //DControl, Bugstrol
  begin
    case Tipo of
        0: Result := 'Admininstrador';
        1: Result := 'Parceiro';
        2: Result := 'Cliente';
        3: Result := 'Usu�rio';
      else Result := 'N�o implementado';
    end;
  end
  else if CodigoApp = 4 then //Syndi2
  begin
    case Tipo of
        0: Result := 'Admininstrador';
        1: Result := 'Grupo';
        2: Result := 'S�ndico';
        3: Result := 'Cond�mino';
      else Result := 'N�o implementado';
    end;
  end
  else
  begin
    case Tipo of
        0: Result := 'N�vel 1';
        1: Result := 'N�vel 2';
        2: Result := 'N�vel 3';
        3: Result := 'N�vel 4';
      else Result := 'N�o implementado';
    end;
  end;
end;

function TUnDmkWeb.DesconectarUsuarioWEB: Boolean;
var
  Res: ArrayOfStrings;
begin
  VAR_WEB_CONECTADO    := -1;
  VAR_WEB_USR_ID       := 0;
  VAR_WEB_USR_TIPO     := -1;
  VAR_WEB_USR_ENT      := 0;
  VAR_WEB_CLIUSER      := 0;
  VAR_WEB_PERUSER      := 0;
  VAR_WEB_IDAPI        := '';
  VAR_WEB_IDLOGIN      := '';
  VAR_WEB_USR_TZDIFUTC := '';
  VAR_WEB_NOTIFYAPP    := 0;
  VAR_WEB_HASHTERMO    := '';
  VAR_WEB_TERMOUSO     := 0;
  VAR_WEB_TERMOPRIVA   := 0;
  VAR_WEB_USR_NOME     := '';
  //
  try
    Res := GetVCLWebServicePortType2.SOAP_LogoutUserDmk(ObtemTipoDeDispositivo(dtDesktop));
    //
    if Geral.IMV(Res[0]) = 100 then
      Result := True
    else
      Result := False;
  except
    Result := False;
  end;
end;

procedure TUnDmkWeb.LimpaWebBrowser(Browser: TWebBrowser);
begin
  Browser.Navigate('about:blank');
end;

function TUnDmkWeb.LoginUser(Usuario, Senha: String): String;
var
  Resul: ArrayOfStrings;
begin
  try
    Screen.Cursor := crHourGlass;
    //
    Resul := GetVCLWebServicePortType2.SOAP_LoginUserDmk(Usuario, Senha, ObtemTipoDeDispositivo(dtWEB));
    //
    VAR_WEB_CONECTADO    := Geral.IMV(Resul[0]);
    VAR_WEB_USR_ID       := Geral.IMV(Resul[2]);
    VAR_WEB_USR_TIPO     := Geral.IMV(Resul[3]);
    VAR_WEB_USR_ENT      := Geral.IMV(Resul[4]);
    VAR_WEB_CLIUSER      := Geral.IMV(Resul[5]);
    VAR_WEB_PERUSER      := Geral.IMV(Resul[6]);
    VAR_WEB_IDAPI        := Resul[7];
    VAR_WEB_IDLOGIN      := Resul[8];
    VAR_WEB_USR_TZDIFUTC := Resul[9];
    VAR_WEB_NOTIFYAPP    := Geral.IMV(Resul[10]);
    VAR_WEB_HASHTERMO    := Resul[11];
    VAR_WEB_TERMOUSO     := Geral.IMV(Resul[12]);
    VAR_WEB_TERMOPRIVA   := Geral.IMV(Resul[13]);
    VAR_WEB_USR_NOME     := Resul[14];
    //
    Screen.Cursor := crDefault;
    //
    Result := Resul[1];
  except
    VAR_WEB_CONECTADO    := 0;
    VAR_WEB_USR_ID       := 0;
    VAR_WEB_USR_TIPO     := -1;
    VAR_WEB_USR_ENT      := 0;
    VAR_WEB_CLIUSER      := 0;
    VAR_WEB_PERUSER      := 0;
    VAR_WEB_IDAPI        := '';
    VAR_WEB_IDLOGIN      := '';
    VAR_WEB_USR_TZDIFUTC := '00:00';
    VAR_WEB_NOTIFYAPP    := 0;
    VAR_WEB_HASHTERMO    := '';
    VAR_WEB_TERMOUSO     := 0;
    VAR_WEB_TERMOPRIVA   := 0;
    VAR_WEB_USR_NOME     := '';
    //
    Screen.Cursor := crDefault;
    //
    Result := 'Falha ao realizar o login!';
  end;
end;

function TUnDmkWeb.ObtemNomeUsuarioWEB(Codigo: Integer): String;
var
  //Res: ArrayOfStrings;
  Resul: String;
begin
  Resul := '';

  try
    //Res   := GetVCLWebServicePortType2.WEBObtemNomeUsuario(Codigo);
    //Resul := Res[0];
    if not Grl_DmkREST.ObtemNomeUsuario(False, Codigo, Resul) then
      Resul := '';
  except
    ;
  end;
  //
  if Length(Resul) > 0 then
    Result := Resul
  else
    Result := '';
end;

function TUnDmkWeb.ObtemStatusUsuarioWEB(const Codigo: Integer;
  var Status: Integer; var StatTxt: String): TColor;
var
  Res: ArrayOfStrings;
  (*
  Mensagem: String;
  Json: TJSONObject;
  *)
begin
  Res := GetVCLWebServicePortType2.WEBVerificaStatusUsuario(Codigo);
  (*
  if Grl_DmkREST.VerificaStatusUsuario(False, Codigo, Mensagem) then
  begin
    Json := Grl_DmkREST.JsonSingleToJsonObject(Mensagem);
    //
    Status  := Geral.IMV(Grl_DmkREST.GetJsonValue(Json, 'id'));
    StatTxt := Grl_DmkREST.GetJsonValue(Json, 'status');
  end else
  begin
    Status  := -1;
    StatTxt := 'N�o catalogado';
  end;
  *)
  //
  Status  := Geral.IMV(Res[0]);
  StatTxt := Res[1];
  //
  case Status of
    0:   Result := clGreen;
    2:   Result := clRed;
    else Result := clBlack;
  end;
end;

  {$IfDef cAdvToolx} //Berlin
function TUnDmkWeb.SkinMenu(Index: integer): TToolBarStyle;
begin
  case Index of
    0:   Result := bsOffice2003Blue;
    1:   Result := bsOffice2003Classic;
    2:   Result := bsOffice2003Olive;
    3:   Result := bsOffice2003Silver;
    4:   Result := bsOffice2007Luna;
    5:   Result := bsOffice2007Obsidian;
    6:   Result := bsOffice2007Silver;
    7:   Result := bsOfficeXP;
    8:   Result := bsWhidbeyStyle;
    9:   Result := bsWindowsXP;
    else Result := bsOffice2003Classic;
  end;
end;
  {$EndIf} //Berlin

{$IfDef UsaWSuport}
{$IfNDef NO_USE_MYSQLMODULE}
procedure TUnDmkWeb.ObtemDadosContaDmk(Query: TmySQLQuery;
  DataBase: TmySQLDatabase; UsuarioLocal: Integer; var CodigoCfg: Integer;
  var Usuario, Senha: String);
begin
  Usuario   := '';
  Senha     := '';
  CodigoCfg := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
    'SELECT Codigo, Usuario, AES_DECRYPT(Password, "'+ CO_RandStrWeb01 +'") SENHA ',
    'FROM emailconta ',
    'WHERE PermisNiv=' + Geral.FF0(UsuarioLocal),
    'AND TipoConta=0 ',
    '']);
  if Query.RecordCount > 0 then
  begin
    Usuario   := Query.FieldByName('Usuario').AsString;
    Senha     := Query.FieldByName('SENHA').AsString;
    CodigoCfg := Query.FieldByName('Codigo').AsInteger;
  end;
end;
{$EndIf}
{$EndIf}

function TUnDmkWeb.ObtemLinkLostPass(DMKID_APP: Integer; Web_MyURL, WebId,
  Usuario: String): String;
var
  Link: String;
begin
  if DMKID_APP = 17 then //DControl
  begin
    Link := Web_MyURL + '/lostpass.php?usuario=' + Usuario;
  end else
  if DMKID_APP = 24 then //Bugstrol
  begin
    Link := 'http://bugstrolweb.dermatek.net.br/' + WebId + '.php?page=lostpass&usuario=' + Usuario;
  end else
  begin
    Link := Web_MyURL + '/' + WebId + '.php?page=lostpass&usuario=' + Usuario;
  end;
  Result := Link;
end;

function TUnDmkWeb.ObtemLinkTermo(IDTermo: Integer): String;
var
  Link: String;
begin
  Link := CO_DMKURL_BASE + 'termos&id=' + Geral.FF0(IDTermo);
  //
  Result := Link;
end;

function TUnDmkWeb.ObtemLinkAjuda(DMKID_APP, IDAjuda: Integer): String;
var
  Link: String;
begin
  Link := CO_DMKURL_BASE + 'wfaq&app=' + Geral.FF0(DMKID_APP);
  //
  if IDAjuda <> 0 then
    Link := Link + '&id=' + Geral.FF0(IDAjuda);
  //
  Result := Link;
end;

function TUnDmkWeb.ObtemLinkHisAlt(Aplicativo: Integer; Versao: String): String;
var
  Link: String;
begin
  Link := CO_DMKURL_BASE + 'hisalt';
  //
  if Aplicativo <> 0 then
    Link := Link + '&aplicativo=' + Geral.FF0(Aplicativo);
  if Versao <> '' then
    Link := Link + '&versao=' + Geral.SoNumero_TT(Versao);
  //
  Result := Link;
end;

procedure TUnDmkWeb.ValidaModulosAplicativo(Query: TmySQLQuery; CNPJ: String;
  Aplicativo: Integer);
var
  //Resul: ArrayOfStrings;
  Modulos: String;
begin
  (*try
    Resul := GetVCLWebServicePortType2.WEBValidaModulosAplicativo(CNPJ, Aplicativo);
  except
    SetLength(Resul, 3);
    Resul[0] := Geral.FF0(-1); //N�o conseguiu verificar
    Resul[1] := '';
    Resul[2] := '';
  end;
  Modulos := Resul[2];
  *)
  if not Grl_DmkREST.ValidaModulosAplicativo(False, CNPJ, Aplicativo, Modulos) then
    Modulos := '';
  //
  if Modulos <> '' then
  begin
    //Achou! Atualiza
    Query.Close;
    Query.SQL.Clear;
    //Query.SQL.Add('UPDATE master SET HabilModulos = "' + Modulos + '"');
    Query.SQL.Add('UPDATE master SET HabilModulos = ' + Modulos);
    Query.ExecSQL;
  end else
  begin
    //N�o Achou! Exclui
    Query.Close;
    Query.SQL.Clear;
    Query.SQL.Add('UPDATE master SET HabilModulos = ""');
    Query.ExecSQL;
  end;
end;

{$IFNDEF NAO_USA_UnitMD5}
{$IfDef UsaWSuport}
function TUnDmkWeb.VerificaAtualizacaoVersao2(MostraMensagem,
  MostraTextoURL: Boolean; AppName, AppTitle, CNPJ: String; VersaoAtual: Int64;
  CodAplic: Integer; Agora: TDateTime; Memo: TMemo; TipoDownload: TDownType;
  var Versao: Int64; var Arquivo: String; ForcaBaixa: Boolean = False;
  ApenasVerifica: Boolean = False; BalloonHint: TBalloonHint = nil;
  NaoEncerra: Boolean = False): Boolean;

  function MostraJanelaVersao(Versao: Int64; Arq, ArqTxt: String;
    TipoDown: TDownType): Boolean;
  begin
    Result := False;
    //
    Application.CreateForm(TFmNovaVersao, FmNovaVersao);
    FmNovaVersao.FAplic                := CodAplic;
    FmNovaVersao.FVersao               := Versao;
    FmNovaVersao.STAppArqTitle.Caption := AppTitle;
    FmNovaVersao.STAviso.Caption       := 'H� uma nova vers�o de: "' + AppTitle + '" no site da dermatek:';
    FmNovaVersao.STAviso.Visible       := True;
    FmNovaVersao.StVersao.Caption      := Geral.VersaoTxt2006(Versao);
    FmNovaVersao.FURLArq               := Arq;
    FmNovaVersao.FNaoEncerra           := NaoEncerra;
    //
    if CodAplic <> 0 then
      FmNovaVersao.STAppArqName.Caption := AppName
    else
      FmNovaVersao.STAppArqName.Caption := ArqTxt;
    //
    if (TipoDown = dtExec) or (TipoDown = dtExecAux) then
    begin
      FmNovaVersao.LaLink.Caption := 'Clique aqui para visualizar o hist�rico de atualiza��es';
      FmNovaVersao.LaLink.Visible := True;
    end else
      FmNovaVersao.LaLink.Visible := False;
    case TipoDown of
          dtExec: FmNovaVersao.RGTipo.ItemIndex :=  0;
          dtTabe: FmNovaVersao.RGTipo.ItemIndex :=  1;
          dtImag: FmNovaVersao.RGTipo.ItemIndex :=  2;
          dtSQLs: FmNovaVersao.RGTipo.ItemIndex :=  3;
       dtExecAux: FmNovaVersao.RGTipo.ItemIndex :=  4;
             else FmNovaVersao.RGTipo.ItemIndex := -1;
    end;
    FmNovaVersao.ShowModal;
    FmNovaVersao.Destroy;
    //
    Result := VAR_DOWNLOAD_OK;
  end;

  function VerificaVersao(SerialKey, SerialNum: String; var Arq, ArqTxt: String;
    var MsgCod: Integer; var MsgTxt: String): Int64;
  var
    Json: TJSONObject;
    Versao: Int64;
    Mensagem: String;
  begin
    try
      Versao := VersaoAtual;
      //
      if URLExiste(defURL) then
      begin
        if Grl_DmkREST.VerificaNovaVersaoDesktop(False, AppName, SerialKey,
          SerialNum, CodAplic, Versao, ForcaBaixa, Mensagem) then
        begin
          Json := Grl_DmkREST.JsonSingleToJsonObject(Mensagem);
          //
          MsgCod := Geral.IMV(Grl_DmkREST.GetJsonValue(Json, 'messageCode'));
          MsgTxt := Grl_DmkREST.GetJsonValue(Json, 'messageValue');
          Versao := Geral.I64MV(Geral.SoNumero_TT(Grl_DmkREST.GetJsonValue(Json, 'versaoSite')));  // Bug milenio // Estouro integer //
          Arq    := Grl_DmkREST.GetJsonValue(Json, 'arquivo');
          ArqTxt := Grl_DmkREST.GetJsonValue(Json, 'arquivoTxt');
        end else
        begin
          MsgCod := 0;
          MsgTxt := 'Falha ao verificar vers�o!';
          Versao := 0;
          Arq    := '';
          ArqTxt := '';
        end;

      end else
      begin
        MsgCod := 0;
        MsgTxt := 'Falha ao verificar vers�o!';
        Versao := 0;
        Arq    := '';
        ArqTxt := '';
      end;
    except
      MsgCod := 0;
      MsgTxt := 'Falha ao verificar vers�o!';
      Versao := 0;
      Arq    := '';
      ArqTxt := '';
    end;
    //
    Result := Versao;
  end;

  (*
  function VerificaVersao(SerialKey, SerialNum: String; var Arq, ArqTxt: String;
    var MsgCod: Integer; var MsgTxt: String): Integer;
  var
    Resul: ArrayOfStrings;
    Txt: String;
    Cod, Versao: Int64;
  begin
    try
      Versao := VersaoAtual;
      //
      if URLExiste(defURL) then
      begin
        Resul := GetVCLWebServicePortType2.SOAP_VerificaNovaVersaoDmk2(AppName,
                  CodAplic, Versao, ForcaBaixa, SerialKey, SerialNum);
      end else
      begin
        SetLength(Resul, 5);
        Resul[0] := Geral.FF0(-1);
        Resul[1] := 'Falha ao verificar vers�o!';
        Resul[2] := '0';
        Resul[3] := '';
        Resul[4] := '';
      end;
    except
      SetLength(Resul, 5);
      Resul[0] := Geral.FF0(-1);
      Resul[1] := 'Falha ao verificar vers�o!';
      Resul[2] := '0';
      Resul[3] := '';
      Resul[4] := '';
    end;
    //
    if Geral.IMV(Resul[0]) = -1 then
      Txt := Resul[1]
    else
      //Est� vindo em ANSI Txt := Utf8ToAnsi(Resul[1]);
      Txt := Resul[1];
    //
    Cod    := Geral.IMV(Resul[0]);
    Versao := Geral.IMV(Resul[2]);
    Arq    := Resul[3];
    ArqTxt := Resul[4];
    MsgCod := Cod;
    MsgTxt := Txt;
    Result := Versao;
  end;
  *)
var
  MsgCod: Integer;
  Arq, ArqTxt, MsgTxt, SerialKey, SerialNum: String;
begin
  if RemoteConnection then
  begin
    try
      Screen.Cursor := crHourGlass;
      Result        := False;
      //
      if BalloonHint <> nil then
        BalloonHint.HideHint;
      //
      if MostraTextoURL and (Memo <> nil) then
        Memo.Lines.Clear;
      //
      if (CodAplic <> 0) and (CNPJ <> '') then
      begin
        SerialNum := DmkPF.CalculaValSerialNum2('', CNPJ, CodAplic, 0, True);
        SerialKey := DmkPF.CalculaValSerialKey2(Geral.FDT(Agora, 1), True);
      end else
      begin
        SerialNum := '';
        SerialKey := '';
      end;
      Versao := VerificaVersao(SerialKey, SerialNum, Arq, ArqTxt, MsgCod, MsgTxt);
      //
      if MsgCod = 105 then //Vers�o obtida com �xito!
      begin
        if not ApenasVerifica then
        begin
          if not MostraJanelaVersao(Versao, Arq, ArqTxt, TipoDownload) then
            MsgCod := 0;
        end;
      end else
      if MsgCod = 106 then //N�o h� nova vers�o!
      begin
        if not ApenasVerifica then
          Geral.MB_Aviso(Geral.FF0(MsgCod) + ' - ' + MsgTxt + sLineBreak +
            'Vers�o atual: ' + Geral.VersaoTxt2006(VersaoAtual) + sLineBreak +
            'Vers�o no site: ' + Geral.VersaoTxt2006(Versao));
      end else
      begin
        if not ApenasVerifica then
          Geral.MB_Aviso(Geral.FF0(MsgCod) + ' - ' + MsgTxt);
      end;
    finally
      //if MsgCod in [105, 106] then
      if MsgCod = 105 then
        Result := True;
      //
      Screen.Cursor := crDefault;
    end;
  end else
    Result := False;
  //
  if Result then
    Arquivo := ChangeFileExt(ArqTxt, '')
  else
    Arquivo := '';
end;
{$EndIf}
{$ENDIF}

function TUnDmkWeb.VerificaSQLNiveis(TipoLogin: Integer;
  CampoSQL: String): String;
var
  SQL: String;
begin
  case TipoLogin of
      0: SQL := CampoSQL + ' & 2 ';
      1: SQL := CampoSQL + ' & 4 ';
      2: SQL := CampoSQL + ' & 8 ';
      3: SQL := CampoSQL + ' & 16 ';
    else SQL := CampoSQL + ' & 1 ';
  end;
  Result := SQL;
end;

procedure TUnDmkWeb.WBLoadHTML(WebBrowser: TWebBrowser; HTMLCode: WideString);
var
  sl: TStringList;
  ms: TMemoryStream;
begin
  WebBrowser.Navigate('about:blank');
  //
  if HTMLCode <> '' then
  begin
    while WebBrowser.ReadyState < READYSTATE_INTERACTIVE do
      Application.ProcessMessages;
    if Assigned(WebBrowser.Document) then
    begin
      sl := TStringList.Create;
      try
        ms := TMemoryStream.Create;
        try
          sl.Text := HTMLCode;
          sl.SaveToStream(ms) ;
          ms.Seek(0, 0) ;
          (WebBrowser.Document as IPersistStreamInit).Load(TStreamAdapter.Create(ms)) ;
        finally
          ms.Free;
        end;
      finally
        sl.Free;
      end;
    end;
  end;
end;

function TUnDmkWeb.WOrdSerAtualizaStatus(const AplicativoSolicitante, Codigo,
  Status: Integer; var Mensagem: String): Integer;
(*
var
  Res: ArrayOfStrings;
*)
begin
  try
    (*
    Res := GetVCLWebServicePortType2.SOAP_WOrdSerAtualizaStatus(AplicativoSolicitante,
             VAR_WEB_USR_ID, VAR_WEB_IDAPI, Codigo, Status, VAR_WEB_USR_TIPO);
    //
    Mensagem := Res[1];
    Result   := Geral.IMV(Res[0]);
    *)
    if Grl_DmkREST.WOrdSerAtualizaStatus(False, VAR_WEB_USR_ID, Codigo, Status,
      VAR_WEB_USR_TIPO, Mensagem)
    then
      Result := 104
    else
      Result := 205;
  except
    Mensagem := 'Inclus�o abortada! Tente novamente mais tarde!';
    Result   := 0;
  end;
end;

function TUnDmkWeb.WOrdSerAtualizaRespons(const AplicativoSolicitante, Codigo,
  Respons: Integer; var Mensagem: String): Integer;
(*
var
  Res: ArrayOfStrings;
*)
begin
  try
    (*
    Res := GetVCLWebServicePortType2.SOAP_WOrdSerAtualizaRespons(AplicativoSolicitante,
             VAR_WEB_USR_ID, VAR_WEB_IDAPI, Codigo, Respons, VAR_WEB_USR_TIPO);
    //
    Mensagem := Res[1];
    Result   := Geral.IMV(Res[0]);
    *)
    if Grl_DmkREST.WOrdSerAtualizaRespons(False, VAR_WEB_USR_ID, Codigo, Respons,
      VAR_WEB_USR_TIPO, Mensagem)
    then
      Result := 104
    else
      Result := 205;
  except
    Mensagem := 'Inclus�o abortada! Tente novamente mais tarde!';
    Result   := 0;
  end;
end;

function TUnDmkWeb.WOrdSerEnc(const AplicativoSolicitante, Codigo,
  Status: Integer; DataHoraTimeZone: String; Solucao: Integer;
  MotivReopen: String; var Mensagem: String): Integer;
var
  //Res: ArrayOfStrings;
  Json: TJSONObject;
begin
  try
    (*
    Res := GetVCLWebServicePortType2.SOAP_WOrdSerEnc(AplicativoSolicitante,
             VAR_WEB_USR_ID, VAR_WEB_IDAPI, Codigo, Status, DataHoraTimeZone,
             Solucao, MotivReopen);
    //
    Mensagem := Res[1];
    Result   := Geral.IMV(Res[0]);
    *)
    Grl_DmkREST.WOrdSerEnc(False, VAR_WEB_USR_ID, Codigo, Status,
      DataHoraTimeZone, Solucao, MotivReopen, Mensagem);
    //
    Json     := Grl_DmkREST.JsonSingleToJsonObject(Mensagem);
    Mensagem := Grl_DmkREST.GetJsonValue(Json, 'message');
    Result   := Geral.IMV(Grl_DmkREST.GetJsonValue(Json, 'code'));
  except
    Mensagem := 'Inclus�o abortada! Tente novamente mais tarde!';
    Result   := 0;
  end;
end;

{$IfNDef NO_USE_MYSQLMODULE}
function TUnDmkWeb.WOrdSerEnvMail(const Query: TmySQLQuery; Tipo,
  Codigo, Controle: Integer; var Mensagem: String): Boolean;
  function LocalizaEmailSolicit(Codigo, Tipo: Integer): Boolean;
  begin
    //True  = achou!
    //False = n�o achou!
    //
    Result := False;
    //
    Query.Close;
    Query.SQL.Clear;
    Query.SQL.Add('SELECT wus.Email');
    Query.SQL.Add('FROM wordser wor');
    if Tipo = 3 then //Respons�vel
      Query.SQL.Add('LEFT JOIN wusers wus ON wus.Codigo = wor.Respons')
    else
      Query.SQL.Add('LEFT JOIN wusers wus ON wus.Codigo = wor.Solicit');
    Query.SQL.Add('WHERE wor.Codigo=:P0');
    Query.Params[0].AsInteger := Codigo;
    UnDmkDAC_PF.AbreQueryApenas(Query);
    if Query.RecordCount > 0 then
    begin
      if Length(Query.FieldByName('Email').AsString) > 0 then
        Result := True;
    end;
  end;
  function LocalizaConfigEmail(Tipo: Integer): Boolean;
  var
    Campo: String;
  begin
    Result := False;
    //
    case Tipo of
      0: Campo := 'EnvMailAbe';
      1: Campo := 'EnvMailCom';
      2: Campo := 'EnvMailEnc';
      3: Campo := 'EnvMailRes';
    end;
    Query.Close;
    Query.SQL.Clear;
    Query.SQL.Add('SELECT ' + Campo);
    Query.SQL.Add('FROM wordseropc');
    UnDmkDAC_PF.AbreQueryApenas(Query);
    if Query.RecordCount > 0 then
    begin
      if Query.FieldByName(Campo).AsInteger <> 0 then
        Result := True;
    end;
  end;
(*
var
  Res: ArrayOfStrings;
*)
begin
  //Tipo = 0 => Abertura
  //Tipo = 1 => Coment�rio
  //Tipo = 2 => Encerramento
  //Tipo = 3 => Respons�vel
  try
    if not LocalizaEmailSolicit(Codigo, Tipo) then
    begin
      if Tipo = 3 then
        Mensagem := 'O respons�vel n�o possui nenhum e-mail cadastrado para ele!'
      else
        Mensagem := 'O solicitante n�o possui nenhum e-mail cadastrado para ele!';
      Result   := False;
    end else
    if not LocalizaConfigEmail(Tipo) then
    begin
      Mensagem := 'E-mail padr�o para envio de e-mail n�o definido!';
      Result   := False;
    end else
    begin
      (*
      Res := GetVCLWebServicePortType2.WEBEnviaEmailWOrdSer(Tipo, Codigo, Controle);
      //
      Mensagem := Res[1];
      //
      if Res[0] = '1' then
        Result := True
      else
        Result := False;
      *)
      if Grl_DmkREST.EnviaEmailWOrdSer(False, VAR_WEB_USR_ID, Tipo, Codigo,
        Controle, Mensagem)
      then
        Result := True
      else
        Result := False;
    end;
  except
    Mensagem := 'Falha ao enviar e-mail!';
    Result   := False;
  end;
end;
{$EndIf}

function TUnDmkWeb.WOrdSerInclui(const AplicativoSolicitante, Codigo, Cliente,
  Solicitante, Assunto, Prioridade, Modo, Status, Aplicativo, Responsavel,
  Grupo, EnvMailAbeAdm: Integer; AberturaTimeZone, Descricao, Sistema, Impacto,
  Versao, Janela, JanelaRel, CompoRel, Tags: String; var CodigoReturn: Integer;
  var Mensagem: String): Integer;
var
  //Res: ArrayOfStrings;
  Json: TJSONObject;
begin
 //AplicativoSolicitante = C�digo do aplicativo WEB onde ser� realizada a consulta
 //AberturaTimeZone      = String Y-m-d hh:mm:ss
  try
    (*
    Res := GetVCLWebServicePortType2.SOAP_WOrdSerInclui(AplicativoSolicitante,
             VAR_WEB_USR_ID, VAR_WEB_IDAPI, Codigo, AberturaTimeZone, Cliente,
             Solicitante, Assunto, Descricao, Sistema, Impacto, Prioridade,
             Modo, Status, Aplicativo, Versao, Janela, JanelaRel, CompoRel,
             Tags, Responsavel, Grupo, EnvMailAbeAdm);
    //
    Mensagem     := Res[1];
    CodigoReturn := Geral.IMV(Res[2]);
    Result       := Geral.IMV(Res[0]);
    *)
    if Grl_DmkREST.WOrdSerInclui(False, Codigo, VAR_WEB_USR_ID, AberturaTimeZone,
      Cliente, Solicitante, Assunto, Descricao, Sistema, Impacto, Prioridade,
      Modo, Status, Aplicativo, Versao, Janela, JanelaRel, CompoRel, Tags,
      Responsavel, Grupo, EnvMailAbeAdm, Mensagem) then
    begin
      Json         := Grl_DmkREST.JsonSingleToJsonObject(Mensagem);
      Mensagem     := Grl_DmkREST.GetJsonValue(Json, 'message');
      Result       := Geral.IMV(Grl_DmkREST.GetJsonValue(Json, 'code'));
      CodigoReturn := Geral.IMV(Grl_DmkREST.GetJsonValue(Json, 'codigo'));
    end else
    begin
      Json         := Grl_DmkREST.JsonSingleToJsonObject(Mensagem);
      Mensagem     := Grl_DmkREST.GetJsonValue(Json, 'message');
      Result       := Geral.IMV(Grl_DmkREST.GetJsonValue(Json, 'code'));
      CodigoReturn := 0;
    end;
  except
    Mensagem     := 'Inclus�o abortada! Tente novamente mais tarde!';
    CodigoReturn := 0;
    Result       := 0;
  end;
end;

function TUnDmkWeb.WOrdSerIncluiCom(const AplicativoSolicitante, Codigo, Controle,
  Entidade, Para: Integer; ComRes, EnvMail: Boolean; Nome: String;
  var Mensagem: String): Integer;
var
  //Res: ArrayOfStrings;
  Json: TJSONObject;
begin
  try
    (*
    Res := GetVCLWebServicePortType2.SOAP_WOrdSerIncluiCom(AplicativoSolicitante,
             VAR_WEB_USR_ID, VAR_WEB_IDAPI, Codigo, Controle, Entidade, Para,
             Geral.BoolToInt(ComRes), Nome, Geral.BoolToInt(EnvMail));
    //
    Mensagem := Res[1];
    Result   := Geral.IMV(Res[0]);
    *)
    Grl_DmkREST.WOrdSerIncluiCom(False, VAR_WEB_USR_ID, Codigo, Controle,
      Entidade, Para, Geral.BoolToInt(ComRes), Nome, Geral.BoolToInt(EnvMail),
      Mensagem);
    //
    Json     := Grl_DmkREST.JsonSingleToJsonObject(Mensagem);
    Mensagem := Grl_DmkREST.GetJsonValue(Json, 'message');
    Result   := Geral.IMV(Grl_DmkREST.GetJsonValue(Json, 'code'));
  except
    Mensagem := 'Inclus�o abortada! Tente novamente mais tarde!';
    Result   := 0;
  end;
end;

function TUnDmkWeb.TamanhoArquivoBytes(Arquivo: string): Double;
var
  SR: TSearchRec;
  i: integer;
begin
  i := FindFirst(Arquivo, faAnyFile, SR);
  try
    if i = 0 then
      Result := SR.Size
    else
      Result := -1;
  finally
    FindClose(SR);
  end;
end;

function TUnDmkWeb.Termo_AtualizaHash(AplicativoSolicitante: Integer;
  var Mensagem: String): Boolean;
(*var
  Res: ArrayOfStrings;*)
begin
  try
    (*
    Res := GetVCLWebServicePortType2.SOAP_AtualizaHashTermo(AplicativoSolicitante,
             VAR_WEB_USR_ID, VAR_WEB_IDAPI);
    //
    if Geral.IMV(Res[0]) = 104 then
    begin
      Mensagem := Res[1];
      Result   := True;
    end else
    begin
      Mensagem := Res[1];
      Result   := False;
    end;
    *)
    Result := Grl_DmkREST.AtualizaHashTermo(False, VAR_WEB_USR_ID, Mensagem);
  except
    Mensagem := 'Falha ao atualizar dadaos! Tente novamente mais tarde!';
    Result   := False;
  end;
end;

function TUnDmkWeb.Termo_ConfiguraHashTermo(AplicativoSolicitante: Integer;
  var Mensagem: String): Boolean;
var
  Json: TJSONObject;
  //Res: ArrayOfStrings;
begin
  try
    (*
    Res := GetVCLWebServicePortType2.SOAP_ConfiguraHashTermo(AplicativoSolicitante,
             VAR_WEB_USR_ID, VAR_WEB_USR_TIPO, VAR_WEB_IDAPI);
    //
    if Geral.IMV(Res[0]) = 104 then
    begin
      Mensagem := Res[1];
      Result   := True;
      //
      VAR_WEB_HASHTERMO  := Res[2];
      VAR_WEB_TERMOUSO   := Geral.IMV(Res[3]);
      VAR_WEB_TERMOPRIVA := Geral.IMV(Res[4]);
    end else
    begin
      Mensagem := Res[1];
      Result   := False;
      //
      VAR_WEB_HASHTERMO  := '';
      VAR_WEB_TERMOUSO   := 0;
      VAR_WEB_TERMOPRIVA := 0;
    end;
    *)
    if Grl_DmkREST.ConfiguraHashTermo(False, VAR_WEB_USR_ID, VAR_WEB_USR_TIPO,
      Mensagem) then
    begin
      Json := Grl_DmkREST.JsonSingleToJsonObject(Mensagem);
      //
      Mensagem := Grl_DmkREST.GetJsonValue(Json, 'message');
      Result   := True;
      //
      VAR_WEB_HASHTERMO  := Grl_DmkREST.GetJsonValue(Json, 'hash');
      VAR_WEB_TERMOUSO   := Geral.IMV(Grl_DmkREST.GetJsonValue(Json, 'termo'));
      VAR_WEB_TERMOPRIVA := Geral.IMV(Grl_DmkREST.GetJsonValue(Json, 'privacidade'));
    end else
    begin
      Result := False;
      //
      VAR_WEB_HASHTERMO  := '';
      VAR_WEB_TERMOUSO   := 0;
      VAR_WEB_TERMOPRIVA := 0;
    end;
  except
    Mensagem := 'Falha ao atualizar dadaos! Tente novamente mais tarde!';
    Result   := False;
    //
    VAR_WEB_HASHTERMO  := '';
    VAR_WEB_TERMOUSO   := 0;
    VAR_WEB_TERMOPRIVA := 0;
  end;
end;

{
procedure TUnDmkWeb.DeleteFolder(sPath : String; Confirm: Boolean);
var
  OpStruc: TSHFileOpStruct;
  FromBuffer, ToBuffer: Array[0..128] of Char;
begin
  fillChar(OpStruc, Sizeof(OpStruc), 0);
  FillChar(FromBuffer, Sizeof(FromBuffer), 0);
  FillChar(ToBuffer, Sizeof(ToBuffer), 0);
  StrPCopy(FromBuffer, sPath);
  with OpStruc Do
  begin
    //Wnd := hHandle;
    wFunc := FO_DELETE;
    pFrom := @FromBuffer;
    pTo := @ToBuffer;
    if not confirm then
     fFlags := FOF_NOCONFIRMATION;
    fAnyOperationsAborted := False;
    hNameMappings := nil;
    //lpszProgressTitle:=nil;
  end;
  ShFileOperation(OpStruc);
end;
}
{$IfNDef NO_USE_MYSQLMODULE}
function TUnDmkWeb.DataTimeZoneToNewTimeZone(Database: TmySQLDatabase;
  Query: TmySQLQuery; DataHora, TimeZone, NewTimeZone: String): TDateTime;
begin
  //DataHora  = yyyy/mm/dd hh:nn:ss (Formato FDT = 9)
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Database, [
    'SELECT CONVERT_TZ("'+ DataHora +'", "' + TimeZone + '", "' +
    NewTimeZone + '") DataHora ']);
  if Query.RecordCount > 0 then
    Result := Query.FieldByName('DataHora').AsDateTime
  else
    Result := 0;
end;
{$EndIf}

function TUnDmkWeb.DefineCoordenadas(const Coord: String; var Latitude,
  Longitude: Double; const NavegaPad, Show: Boolean; const Height,
  Width: Integer): Boolean;
var
  MapaDescri, Coords,
  Nome, Rua, Cidade, UF, Pais: String;
  Numero: Integer;
begin
  Result := False;
  Latitude  := 0;
  Longitude := 0;
  //
  MostraGoogleMapsPlace(Coord, NavegaPad, Show, Height, Width);
  // Separar texto
  Coords := '';
  if InputQuery('Coordenadas', 'Cole aqui a coordenada:', Coords) then
    Result := DmkPF.SeparaCoordenadas(Coords, Latitude, Longitude);
end;

function TUnDmkWeb.DeleteFolder(FolderName: String; LeaveFolder: Boolean): Boolean;
var
  r: TshFileOpStruct;
begin
  Result := False;
  if not DirectoryExists(FolderName) then
    Exit;
  if LeaveFolder then
    FolderName := FolderName + ' *.* '
  else
    if FolderName[Length(FolderName)] = ' \ ' then
      Delete(FolderName,Length(FolderName), 1);
    FillChar(r, SizeOf(r), 0);
    r.wFunc  := FO_DELETE;
    r.pFrom  := PChar(FolderName);
    r.fFlags := FOF_ALLOWUNDO or FOF_NOCONFIRMATION;
    //
    Result := ((ShFileOperation(r) = 0) and (not r.fAnyOperationsAborted));
end;

function TUnDmkWeb.RemoteConnection: Boolean;
var
  flags: DWORD;
begin
  if InternetGetConnectedState(@flags, 0) then
  begin
    if not InternetCheckConnection('http://dermatek.com.br/', 1, 0) then
    begin
      Result := False;
    end else
      Result := True;
  end else
    Result := False;
end;

function TUnDmkWeb.VerificaConexaoWeb(var Msg: String): Integer;
var
  Conected: Boolean;
  Flags: DWORD;
  Res: Integer;
begin
  //-1 => Falha ao verificar
  // 0 => N�o conectado
  // 1 => Conectado via WIFI
  // 2 => Conectado via WWAN
  // 3 => Conectado via Proxy
  // 4 => Conectado via Modem Busy
  // Para Android permiss�o => Access Wifi State
  Msg := '';
  Res := 0;
  //
  Conected := InternetGetConnectedState(@flags, 0);
  if Conected then
  begin
    if (Flags and INTERNET_CONNECTION_MODEM) = INTERNET_CONNECTION_MODEM then
      Res := 2
    else if (Flags and INTERNET_CONNECTION_LAN) = INTERNET_CONNECTION_LAN then
      Res := 1
    else if (Flags and INTERNET_CONNECTION_PROXY) = INTERNET_CONNECTION_PROXY then
      Res := 3
    else if (Flags and INTERNET_CONNECTION_MODEM_BUSY) = INTERNET_CONNECTION_MODEM_BUSY then
      Res := 4
    else
      Res := -1;
  end;
  Result := Res;
end;

(*
function TUnDmkWeb.RemoteConnection: Boolean;
const
  Key = '\System\CurrentControlSet\Services\RemoteAccess';
  Value = 'Remote Connection';
var
  Reg: TRegistry;
  Buffer: DWord;
  Texto: TStringList;
begin
  Texto := TStringList.Create;
  try
    Result := DownloadURL('http://www.dermatek.com.br/TemNet.txt', Texto);
  finally
    Texto.Free;
  end;
  //
  EXIT;
  //////////////////////////////////////////////////////////////////////////////
  /// N�o Funciona
  //////////////////////////////////////////////////////////////////////////////
  ///

  Result := false;
  //
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.OpenKey(Key, false) then
    begin
      Reg.ReadBinaryData(Value, Buffer, SizeOf(Buffer));
      Result := Buffer = 1;
    end;
  finally
    Reg.CloseKey;
    Reg.Free;
  end;
end;
*)
end.
