// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://localhost:3569/soapserver.php?wsdl
//  >Import : http://localhost:3569/soapserver.php?wsdl:0
// Encoding : ISO-8859-1
// Version  : 1.0
// (17/10/2007 18:53:04 - - $Rev: 7010 $)
// ************************************************************************ //

unit dmkSOAP;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"
  // !:integer         - "http://www.w3.org/2001/XMLSchema"

  ArrayOfStrings  = array of WideString;          { "http://localhost"[GblCplx] }
  ArrayOfIntegers = array of Int64;              { "http://localhost"[GblCplx] }

  // ************************************************************************ //
  // Namespace : http://localhost/
  // soapAction: http://localhost/soapserver.php/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : rpc
  // binding   : VCLWebServiceBinding
  // service   : VCLWebService
  // port      : VCLWebServicePort
  // URL       : http://localhost:3569/soapserver.php
  // ************************************************************************ //
  VCLWebServicePortType = interface(IInvokable)
  ['{2E51F07C-1564-1B1B-A2D8-45B42189790A}']
    //Geral NOVO!!!!
    //Dispositivo0 => WEB / 1 => Desktop / 2 => Mobile
    function SOAP_VerificaNovaVersaoDmk2(const Arquivo: string;
               CodigoAplicativo, VersaoAtual: Integer; ForcaBaixa: Boolean;
               SerialKey, SerialNum: String): ArrayOfStrings; stdcall;
    function SOAP_LoginUserDmk(const Usuario, Senha: String; Dispositivo: Integer): ArrayOfStrings; stdcall;
    function SOAP_LogoutUserDmk(const Dispositivo: Integer): ArrayOfStrings; stdcall;
    function WEBVerificaSeEstaConectado(const Usuario: Integer): ArrayOfStrings; stdcall;
    function UnLockAplic(const SerialKey, SerialNum, CNPJ, DescriPC, NomePC,
      IP, VersaoAtu: String; Servidor, CodAplic: Integer; AtualizaDescriPC: Boolean;
      OSVersion, DBVersion, IPServ: String): ArrayOfStrings; stdcall;
    (* Migrado para: SOAP_WOrdSerInclui
    function WEBWOrdSerInclui(const Entidade, Solicit, Aplic, Versao: Int64; Janela:
      String; Assunto: Integer; Texto: WideString): ArrayOfStrings; stdcall;
    *)
    function SOAP_WOrdSerInclui(const AplicativoSolicitante,
               IDUsuarioSolicitante: Integer; IDAPI: String; Codigo: Integer;
               AberturaTimeZone: String; Cliente, Solicitante, Assunto: Integer;
               Descricao, Sistema, Impacto: String; Prioridade, Modo, Status,
               Aplicativo: Integer; Versao, Janela, JanelaRel, CompoRel, Tags: String;
               Responsavel, Grupo, EnvMailAbeAdm: Integer): ArrayOfStrings; stdcall;
    function SOAP_WOrdSerIncluiCom(const AplicativoSolicitante,
               IDUsuarioSolicitante: Integer; IDAPI: String; Codigo, Controle,
               Entidade, Para, ComRes: Integer; Nome: String; EnvMail: Integer): ArrayOfStrings; stdcall;
    function SOAP_WOrdSerEnc(const AplicativoSolicitante,
               IDUsuarioSolicitante: Integer; IDAPI: String; Codigo,
               Status: Integer; DataHoraTimeZone: String; Solucao: Integer;
               MotivReopen: String): ArrayOfStrings; stdcall;
    function SOAP_WOrdSerAtualizaRespons(const AplicativoSolicitante,
               IDUsuarioSolicitante: Integer; IDAPI: String; Codigo,
               Respons, TipoUsuario: Integer): ArrayOfStrings; stdcall;
    function SOAP_WOrdSerAtualizaStatus(const AplicativoSolicitante,
               IDUsuarioSolicitante: Integer; IDAPI: String; Codigo,
               Status, TipoUsuario: Integer): ArrayOfStrings; stdcall;
    function SOAP_VerificaMaxHistorico(const Usuario: String): ArrayOfStrings; stdcall;
    function WEBEnviaEmailWOrdSer(const Tipo, Codigo, Controle: Integer): ArrayOfStrings; stdcall;
    function SOAP_InsereWOrdSerArq(const AplicativoSolicitante,
               IDUsuarioSolicitante: Integer; IDAPI: String; Codigo,
               FTPWebArq: Integer): ArrayOfStrings; stdcall;
    function SOAP_InsUpdRegistroFTPDiretorio(const AplicativoSolicitante,
               IDUsuarioSolicitante: Integer; IDAPI: String; Codigo, FTPConfig,
               TipoDir, CliInt, Depto, Nivel: Integer; Nome, Pasta,
               Caminho: String): ArrayOfStrings; stdcall;
    function SOAP_InsUpdRegistroFTPArquivo(const AplicativoSolicitante,
               IDUsuarioSolicitante: Integer; IDAPI, TipSQL: String; CliInt,
               Depto, Nivel, FTPDir: Integer; Tam, Nome, ArqNome: String;
               Origem, Ativo, Codigo: Integer): ArrayOfStrings; stdcall;
    function SOAP_ObtemDadosFTPDir(const AplicativoSolicitante,
               IDUsuarioSolicitante: Integer; IDAPI: String;
               Diretorio: Integer): ArrayOfStrings; stdcall;
    function SOAP_ObtemDadosFTP(const AplicativoSolicitante,
               IDUsuarioSolicitante: Integer; IDAPI: String;
               FTPConfig: Integer): ArrayOfStrings; stdcall;
    function SOAP_AtualizaHashTermo(const AplicativoSolicitante,
               IDUsuarioSolicitante: Integer; IDAPI: String): ArrayOfStrings; stdcall;
    function SOAP_ConfiguraHashTermo(const AplicativoSolicitante,
               IDUsuarioSolicitante, TipoUsuario: Integer; IDAPI: String): ArrayOfStrings; stdcall;
    function SOAP_VerificaHistoricoDeVersao(const CodigoAplicativo, Versao: Int64): ArrayOfStrings; stdcall;
    function WEBValidaModulosAplicativo(const CNPJ: String; const Aplicativos: Integer): ArrayOfStrings; stdcall;
    function WEBObtemNomeUsuario(const Codigo: Integer): ArrayOfStrings; stdcall;
    function WEBVerificaStatusUsuario(const Usuario: Integer): ArrayOfStrings; stdcall;
    function VerificaNovaVersao(const Arquivo: String; CodigoAplicativo,
             VersaoAtual: Integer; ForcaBaixa: Boolean): ArrayOfStrings; stdcall;
 end;

function GetVCLWebServicePortType2(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): VCLWebServicePortType;

const
  defWSDL = 'http://www.dermatek.net.br/dmk_SOAP.php?wsdl';
  defURL  = 'http://www.dermatek.net.br/dmk_SOAP.php';
  (*
  defWSDL = 'http://localhost:3571/dmk_SOAP.php?wsdl';
  defURL  = 'http://localhost:3571/dmk_SOAP.php';
  *)

implementation

uses SysUtils;

function GetVCLWebServicePortType2(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): VCLWebServicePortType;
const
  defSvc  = 'VCLWebService';
  defPrt  = 'VCLWebServicePort';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as VCLWebServicePortType);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(VCLWebServicePortType), 'http://www.dermatek.net.br/', 'ISO-8859-1');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(VCLWebServicePortType), 'http://www.dermatek.net.br/dmk_SOAP.php/%operationName%');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfstrings), 'http://www.dermatek.net.br', 'ArrayOfstring');

  {
  InvRegistry.RegisterInterface(TypeInfo(VCLWebServicePortType), 'http://localhost:3571/', 'ISO-8859-1');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(VCLWebServicePortType), 'http://localhost:3571/dmk_SOAP.php/%operationName%');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfstrings), 'http://localhost:3571', 'ArrayOfstring');
  }
end.
