// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://localhost:3569/soapserver.php?wsdl
//  >Import : http://localhost:3569/soapserver.php?wsdl:0
// Encoding : ISO-8859-1
// Version  : 1.0
// (17/10/2007 18:53:04 - - $Rev: 7010 $)
// ************************************************************************ //

unit SOAPServer;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"
  // !:integer         - "http://www.w3.org/2001/XMLSchema"

  ArrayOfString = array of WideString;          { "http://localhost"[GblCplx] }
  ArrayOfInteger = array of Int64;              { "http://localhost"[GblCplx] }

  // ************************************************************************ //
  // Namespace : http://localhost/
  // soapAction: http://localhost/soapserver.php/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : rpc
  // binding   : VCLWebServiceBinding
  // service   : VCLWebService
  // port      : VCLWebServicePort
  // URL       : http://localhost:3569/soapserver.php
  // ************************************************************************ //
  VCLWebServicePortType = interface(IInvokable)
  ['{2E51F07C-1564-1B1B-A2D8-45B42189790A}']
    //Geral
    function WEBObtemDadosFTP(const CNPJ: String; Usuario: Integer): ArrayOfString; stdcall;
    function WEBObtemSenhasDB(const CNPJ: String; Usuario: Integer): ArrayOfString; stdcall;
    function WEBConectarUsuario(const Codigo: Integer; LogID: String): ArrayOfInteger; stdcall;
    function WEBObtemNomeUsuario(const Codigo: Integer): ArrayOfstring; stdcall;
    function WEBLogoutUser(const Usuario: Integer): ArrayOfstring; stdcall;
    function WEBLoginUser(const Usuario, Senha: String): ArrayOfstring; stdcall;
    function WEBVerificaStatusUsuario(const Usuario: Integer): ArrayOfstring; stdcall;
    function VerificaNovaVersao(const Arquivo: String; CodigoAplicativo, VersaoAtual: Integer; ForcaBaixa: Boolean): ArrayOfstring; stdcall;
    function UnLockAplic(const input, input2, input3, input4, input5, input6, input7: WideString): WideString; stdcall;
    //SWMS
    function SWMSAtualizaXML(const SenhaC, SenhaP, CNPJ: String; XML: WideString): ArrayOfstring; stdcall;
    function SWMSObtemXML(const Codigo, Controle: Integer): ArrayOfstring; stdcall;  // C�d, Msg, 2 a 6 = campos tabela
    function SWMSAtualizaCompet(const Codigo, CodUsu: Integer; CNPJ_EMP, Nome, Local,
      DtInscrIni, DtInscrFim, DataEvenIni, HoraEvenIni, DataEvenFim, HoraEvenFim,
      SenhaC: String; CategoriaIni, CategoriaFim: Integer;
      ContatoNome, ContatoFone, ContatoMail: String; Regulam: WideString;
      Observ_0, Observ_1, Observ_2, Observ_3, Observ_4, Observ_5, Observ_6,
      Observ_7, Observ_8, Observ_9: String; Ativo: Boolean): ArrayOfstring; stdcall; // C�d, Msg
    function SWMSObtemCompet(const SenhaC, SenhaP: String): ArrayOfstring; stdcall; // do CNPJ_EMP em diante menos o SenhaC e o Ativo
    function SWMSAtualizaCompetIts(const Codigo, Controle: Integer; Senha: String; Ativo: Boolean): ArrayOfstring; stdcall; // C�d, Msg
    function SWMSAtualizaCompetRes(const Codigo: Integer; CNPJ_EMP, DtHrResul: String; XML_Resul: WideString): ArrayOfstring; stdcall;  // C�d, Msg
    function SWMSObtemCompetRes(const Codigo: Integer; CNPJ_EMP, Senha: String): ArrayOfstring; stdcall;  // C�d, Msg, dta, xml
  end;

function GetVCLWebServicePortType(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): VCLWebServicePortType;


implementation
  uses SysUtils;

function GetVCLWebServicePortType(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): VCLWebServicePortType;
const
  defWSDL = 'http://www.dermatek.net.br/dmk_UnLockSOAP.php?wsdl';
  defURL  = 'http://www.dermatek.net.br/dmk_UnLockSOAP.php';
  defSvc  = 'VCLWebService';
  defPrt  = 'VCLWebServicePort';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as VCLWebServicePortType);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(VCLWebServicePortType), 'http://www.dermatek.net.br/', 'ISO-8859-1');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(VCLWebServicePortType), 'http://www.dermatek.net.br/dmk_UnLockSOAP.php/%operationName%');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfstring), 'http://www.dermatek.net.br', 'ArrayOfstring');
end.
