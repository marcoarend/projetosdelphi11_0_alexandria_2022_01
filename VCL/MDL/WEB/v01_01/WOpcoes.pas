unit WOpcoes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkDBLookupComboBox,
  dmkEditCB, mySQLDbTables, DmkDAC_PF, Variants, dmkCheckBox, dmkMemo,
  UnProjGroup_Consts;

type
  TFmWOpcoes = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PageControl1: TPageControl;
    TSConexoes: TTabSheet;
    GBDataBase: TGroupBox;
    Label32: TLabel;
    EdWeb_Host: TdmkEdit;
    Label33: TLabel;
    EdWeb_User: TdmkEdit;
    Label34: TLabel;
    EdWeb_Pwd: TdmkEdit;
    Label35: TLabel;
    EdWeb_DB: TdmkEdit;
    Label4: TLabel;
    EdWeb_MyURL: TdmkEdit;
    RGWeb_MySQL: TRadioGroup;
    BtImporta: TBitBtn;
    BtTestar: TBitBtn;
    GBFTP: TGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    EdWeb_FTPh: TdmkEdit;
    EdWeb_FTPu: TdmkEdit;
    EdWeb_Raiz: TdmkEdit;
    EdWeb_FTPs: TdmkEdit;
    BitBtn2: TBitBtn;
    BitBtn1: TBitBtn;
    CkWeb_FTPpassivo: TCheckBox;
    DsAppDirWeb: TDataSource;
    QrAppDirWeb: TmySQLQuery;
    IntegerField1: TIntegerField;
    QrAppDirWebNome: TWideStringField;
    Label18: TLabel;
    EdWeb_Porta: TdmkEdit;
    Label19: TLabel;
    TabSheet1: TTabSheet;
    GBWVersao: TGroupBox;
    CkAppZipar: TdmkCheckBox;
    Label3: TLabel;
    EdWebDirApp: TdmkEditCB;
    CBWebDirApp: TdmkDBLookupComboBox;
    QrWControl: TmySQLQuery;
    GroupBox2: TGroupBox;
    LaEntidade: TLabel;
    EdDono: TdmkEditCB;
    CBDono: TdmkDBLookupComboBox;
    QrEntidades: TmySQLQuery;
    IntegerField2: TIntegerField;
    DsEntidades: TDataSource;
    QrEntidadesNome: TWideStringField;
    TabSheet2: TTabSheet;
    GroupBox3: TGroupBox;
    LaPerPasAdm: TLabel;
    EdPerPasAdm: TdmkEditCB;
    CBPerPasAdm: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    LaPerPasBos: TLabel;
    EdPerPasBos: TdmkEditCB;
    CBPerPasBos: TdmkDBLookupComboBox;
    SpeedButton2: TSpeedButton;
    LaPerPasCli: TLabel;
    EdPerPasCli: TdmkEditCB;
    CBPerPasCli: TdmkDBLookupComboBox;
    SpeedButton3: TSpeedButton;
    LaPerPasUsu: TLabel;
    EdPerPasUsu: TdmkEditCB;
    CBPerPasUsu: TdmkDBLookupComboBox;
    SpeedButton4: TSpeedButton;
    QrWPerfil0: TmySQLQuery;
    DsWPerfil0: TDataSource;
    QrWPerfil0Codigo: TIntegerField;
    QrWPerfil0Nome: TWideStringField;
    DsWPerfil1: TDataSource;
    QrWPerfil1: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField1: TWideStringField;
    DsWPerfil2: TDataSource;
    QrWPerfil2: TmySQLQuery;
    IntegerField4: TIntegerField;
    StringField2: TWideStringField;
    DsWPerfil3: TDataSource;
    QrWPerfil3: TmySQLQuery;
    IntegerField5: TIntegerField;
    StringField3: TWideStringField;
    QrProAltPas: TmySQLQuery;
    QrProAltPasCodigo: TIntegerField;
    QrProAltPasNome: TWideStringField;
    DsProAltPas: TDataSource;
    QrProDowArq: TmySQLQuery;
    IntegerField6: TIntegerField;
    StringField4: TWideStringField;
    DsProDowArq: TDataSource;
    DsProAtivApl: TDataSource;
    QrProAtivApl: TmySQLQuery;
    IntegerField7: TIntegerField;
    StringField5: TWideStringField;
    GroupBox4: TGroupBox;
    Label1: TLabel;
    EdProAltPas: TdmkEditCB;
    CBProAltPas: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdProDowArq: TdmkEditCB;
    CBProDowArq: TdmkDBLookupComboBox;
    Label7: TLabel;
    EdDiasExp: TdmkEdit;
    Label5: TLabel;
    GroupBox5: TGroupBox;
    Label6: TLabel;
    CBProAtivApl: TdmkDBLookupComboBox;
    EdProAtivApl: TdmkEditCB;
    Label8: TLabel;
    MeLogoWeb: TdmkMemo;
    Label9: TLabel;
    EdCrypt: TdmkEdit;
    Label10: TLabel;
    EdMailCont: TdmkEditCB;
    CBMailCont: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    QrMailContIntegerField8: TIntegerField;
    DsMailCont: TDataSource;
    QrMailCont: TmySQLQuery;
    QrMailContNome: TWideStringField;
    TabSheet3: TTabSheet;
    GroupBox6: TGroupBox;
    Label22: TLabel;
    EdNomeAdmi: TdmkEdit;
    Label20: TLabel;
    EdUsername: TdmkEdit;
    EdPassword: TdmkEdit;
    Label21: TLabel;
    QrSenha: TmySQLQuery;
    LAWebId: TLabel;
    EdWebId: TdmkEdit;
    CkFacebookLogin: TCheckBox;
    GroupBox8: TGroupBox;
    Label13: TLabel;
    CBTermoBoss: TdmkDBLookupComboBox;
    EdTermoBoss: TdmkEditCB;
    QrTermoBoss: TmySQLQuery;
    IntegerField8: TIntegerField;
    StringField6: TWideStringField;
    DsTermoBoss: TDataSource;
    Label23: TLabel;
    EdTermoPrivaci: TdmkEditCB;
    CBTermoPrivaci: TdmkDBLookupComboBox;
    DsTermoPrivaci: TDataSource;
    QrTermoPrivaci: TmySQLQuery;
    IntegerField9: TIntegerField;
    StringField7: TWideStringField;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtImportaClick(Sender: TObject);
    procedure BtTestarClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao();
    procedure ReopenWPerfil(Tipo: Integer; Query: TmySQLQuery);
  public
    { Public declarations }
  end;

  var
  FmWOpcoes: TFmWOpcoes;

implementation

uses UnMyObjects, Module, UnFTP, UMySQLModule, MyListas, ModuleGeral,
  {$IfNDef sWUsr} UnWUsersJan, {$EndIf}
  {$IfNDef SemProtocolo} UnProtocoUnit, {$EndIf}
   UnDmkWeb, MyDBCheck, PreEmail;

{$R *.DFM}

procedure TFmWOpcoes.BitBtn1Click(Sender: TObject);
const
  Avisa = True;
begin
  UFTP.TestaConexaoFTP(Self, EdWeb_FTPh.Text, EdWeb_FTPu.Text, EdWeb_FTPs.Text,
    CkWeb_FTPpassivo.Checked, Avisa);
end;

procedure TFmWOpcoes.BitBtn2Click(Sender: TObject);
var
  Nome, URL, Host, WebRaiz, Usuario, Senha: String;
  Passivo: Integer;
begin
  UFTP.ImportaConfigFTP(Self, Nome, URL, Host, WebRaiz, Usuario, Senha, Passivo);
  //
  EdWeb_FTPh.ValueVariant  := Host;
  EdWeb_FTPu.ValueVariant  := Usuario;
  EdWeb_FTPs.Text          := Senha;
  EdWeb_Raiz.ValueVariant  := WebRaiz;
  CkWeb_FTPpassivo.Checked := Geral.IntToBool(Passivo);
end;

procedure TFmWOpcoes.BtImportaClick(Sender: TObject);
const
  Descricao = nil;
begin
  MyObjects.ImportaConfigWeb(Self, Descricao, EdWeb_Host, EdWeb_DB, nil,
    EdWeb_User, EdWeb_Pwd);
end;

procedure TFmWOpcoes.BtOKClick(Sender: TObject);
var
  Web_Host, Web_User, Web_Pwd, Web_DB, Web_MyURL, Web_FTPh, Web_FTPu,
  Web_FTPs, Web_Raiz, NomeAdmi, Username, Password, LogoWeb, WebId, Crypt: String;
  Web_Porta, Web_MySQL, Web_FTPpassivo, AppZipar, WebDirApp, Dono, PerPasAdm,
  PerPasBos, PerPasCli, PerPasUsu, ProAltPas, ProDowArq, DiasExp, ProAtivApl,
  MailCont, User_ID, FacebookLogin, TermoBoss, TermoPrivaci: Integer;
begin
  Web_Host  := EdWeb_Host.ValueVariant;
  Web_User  := EdWeb_User.ValueVariant;
  Web_Pwd   := EdWeb_Pwd.ValueVariant;
  Web_DB    := EdWeb_DB.ValueVariant;
  Web_Porta := edWeb_Porta.ValueVariant;
  Web_MyURL := EdWeb_MyURL.ValueVariant;
  Web_MySQL := RGWeb_MySQL.ItemIndex;
  //
  Web_FTPh       := EdWeb_FTPh.ValueVariant;
  Web_FTPu       := EdWeb_FTPu.ValueVariant;
  Web_FTPs       := EdWeb_FTPs.ValueVariant;
  Web_Raiz       := EdWeb_Raiz.ValueVariant;
  Web_FTPpassivo := Geral.BoolToInt(CkWeb_FTPpassivo.Checked);
  //
  WebId := EdWebId.ValueVariant;
  Dono  := EdDono.ValueVariant;
  //
  FacebookLogin := Geral.BoolToInt(CkFacebookLogin.Checked);
  //
  if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE opcoesgerl SET ',
    'Web_Host="' + Web_Host + '", ',
    'Web_User="' + Web_User + '", ',
    'Web_Pwd="' + Web_Pwd + '", ',
    'Web_DB="' + Web_DB + '", ',
    'Web_Porta="' + Geral.FF0(Web_Porta) + '", ',
    'Web_MySQL="' + Geral.FF0(Web_MySQL) + '", ',
    'Web_MyURL="' + Web_MyURL + '", ',
    'Web_FTPh="' + Web_FTPh + '", ',
    'Web_FTPu="' + Web_FTPu + '", ',
    'Web_FTPs="' + Web_FTPs + '", ',
    'Web_Raiz="' + Web_Raiz + '", ',
    'Web_FTPpassivo="' + Geral.FF0(Web_FTPpassivo) + '", ',
    'DonoWeb=' + Geral.FF0(Dono) + ', ',
    'WebId="' + WebId + '"',
    '']) then
  begin
    if Dmod.MyDBn.Connected then
    begin
      PerPasAdm := EdPerPasAdm.ValueVariant;
      PerPasBos := EdPerPasBos.ValueVariant;
      PerPasCli := EdPerPasCli.ValueVariant;
      PerPasUsu := EdPerPasUsu.ValueVariant;
      ProAltPas := EdProAltPas.ValueVariant;
      ProDowArq := EdProDowArq.ValueVariant;
      DiasExp   := EdDiasExp.ValueVariant;
      LogoWeb   := MeLogoWeb.Text;
      Crypt     := EdCrypt.ValueVariant;
      MailCont  := EdMailCont.ValueVariant;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpdN, stUpd, 'wcontrol', False,
        ['PerPasAdm', 'PerPasBos', 'PerPasCli', 'PerPasUsu', 'ProAltPas',
        'ProDowArq', 'DiasExp', 'LogoWeb', 'Crypt', 'MailCont',
        'FacebookLogin'], ['Codigo'],
        [PerPasAdm, PerPasBos, PerPasCli, PerPasUsu, ProAltPas,
        ProDowArq, DiasExp, LogoWeb, Crypt, MailCont,
        FacebookLogin], [1], True);
      //
      if CO_DMKID_APP = 17 then //DControl
      begin
        AppZipar     := Geral.BoolToInt(CkAppZipar.Checked);
        WebDirApp    := EdWebDirApp.ValueVariant;
        ProAtivApl   := EdProAtivApl.ValueVariant;
        TermoBoss    := EdTermoBoss.ValueVariant;
        TermoPrivaci := EdTermoPrivaci.ValueVariant;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpdN, stUpd, 'wcontrol', False,
          ['AppZipar', 'WebDirApp', 'ProAtivApl', 'TermoBoss',
          'TermoPrivaci'], ['Codigo'],
          [AppZipar, WebDirApp, ProAtivApl, TermoBoss,
          TermoPrivaci], [1], True);
      end;
      if CO_DMKID_APP in [4, 43] then //Syndi2, Syndi3
      begin
        NomeAdmi := EdNomeAdmi.ValueVariant;
        Username := EdUsername.ValueVariant;
        Password := EdPassword.ValueVariant;
        User_ID  := QrSenha.FieldByName('User_ID').AsInteger;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpdN, stUpd, 'wcontrol', False,
          ['NomeAdmi'], ['Codigo'],
          [NomeAdmi], [1], True);
        //
        UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
          'UPDATE users SET ',
          'Username="' + Username + '", ',
          'Password="' + Password + '" ',
          'WHERE User_ID=' + Geral.FF0(User_ID),
          '']);
      end;
    end;
    Geral.MB_Aviso('Para que as altera��es tenham efeito o aplicativo dever� ser reiniciado!');
    Close;
  end;
end;

procedure TFmWOpcoes.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWOpcoes.BtTestarClick(Sender: TObject);
const
  Avisa = True;
begin
  UnDmkDAC_PF.TestarConexaoMySQL(Self, EdWeb_Host.Text, EdWeb_User.Text,
    EdWeb_Pwd.Text, EdWeb_DB.Text, 3306, Avisa);
end;

procedure TFmWOpcoes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmWOpcoes.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UMyMod.AbreQuery(DmodG.QrOpcoesGerl, Dmod.MyDB);
  UMyMod.AbreQuery(QrEntidades, Dmod.MyDB);
  //
  PageControl1.ActivePageIndex := 0;
  //
  {$IfNDef sWUsr}
  SpeedButton1.Visible := True;
  SpeedButton2.Visible := True;
  SpeedButton3.Visible := True;
  SpeedButton4.Visible := True;
  {$Else}
  SpeedButton1.Visible := False;
  SpeedButton2.Visible := False;
  SpeedButton3.Visible := False;
  SpeedButton4.Visible := False;
  {$EndIf}
  //
  {$IfNDef SemProtocolo}
  SpeedButton6.Visible := True;
  SpeedButton7.Visible := True;
  {$Else}
  SpeedButton6.Visible := False;
  SpeedButton7.Visible := False;
  {$EndIf}
  //
  MostraEdicao();
  //
  GBFTP.Visible := CO_DMKID_APP <> 17;
  //
  //Em desenvolvimento
  CkFacebookLogin.Visible := False;
  CkFacebookLogin.Checked := False;
end;

procedure TFmWOpcoes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWOpcoes.MostraEdicao();
begin
  //Conex�es
  EdWeb_Host.ValueVariant  := DmodG.QrOpcoesGerl.FieldByName('Web_Host').AsString;
  EdWeb_User.ValueVariant  := DmodG.QrOpcoesGerl.FieldByName('Web_User').AsString;
  EdWeb_Pwd.ValueVariant   := DmodG.QrOpcoesGerl.FieldByName('Web_Pwd').AsString;
  EdWeb_DB.ValueVariant    := DmodG.QrOpcoesGerl.FieldByName('Web_DB').AsString;
  EdWeb_Porta.ValueVariant := DModG.QrOpcoesGerl.FieldByName('Web_Porta').AsInteger;
  EdWeb_MyURL.ValueVariant := DmodG.QrOpcoesGerl.FieldByName('Web_MyURL').AsString;
  RGWeb_MySQL.ItemIndex    := DmodG.QrOpcoesGerl.FieldByName('Web_MySQL').AsInteger;
  //
  EdWeb_FTPh.ValueVariant  := DmodG.QrOpcoesGerl.FieldByName('Web_FTPh').AsString;
  EdWeb_FTPu.ValueVariant  := DmodG.QrOpcoesGerl.FieldByName('Web_FTPu').AsString;
  EdWeb_FTPs.ValueVariant  := DmodG.QrOpcoesGerl.FieldByName('Web_FTPs').AsString;
  EdWeb_Raiz.ValueVariant  := DmodG.QrOpcoesGerl.FieldByName('Web_Raiz').AsString;
  CkWeb_FTPpassivo.Checked := Geral.IntToBool(DmodG.QrOpcoesGerl.FieldByName('Web_FTPpassivo').AsInteger);
  //
  EdDono.ValueVariant  := DModG.QrOpcoesGerl.FieldByName('DonoWeb').AsInteger;
  EdWebId.ValueVariant := DModG.QrOpcoesGerl.FieldByName('WebId').AsString;
  //
  TabSheet1.TabVisible := False;
  TabSheet2.TabVisible := False;
  TabSheet3.TabVisible := False;
  //
  QrAppDirWeb.Close;
  QrWControl.Close;
  QrWPerfil0.Close;
  QrWPerfil1.Close;
  QrWPerfil2.Close;
  QrWPerfil3.Close;
  QrProAltPas.Close;
  QrProDowArq.Close;
  QrAppDirWeb.Close;
  QrProAtivApl.Close;
  QrMailCont.Close;
  QrWControl.Close;
  //
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 0) then
  begin
    TabSheet2.TabVisible := True;
    //
    ReopenWPerfil(0, QrWPerfil0);
    ReopenWPerfil(1, QrWPerfil1);
    ReopenWPerfil(2, QrWPerfil2);
    ReopenWPerfil(3, QrWPerfil3);
    //
    UMyMod.AbreQuery(QrProAltPas, Dmod.MyDB); //Local para ficar mais rapido
    UMyMod.AbreQuery(QrProDowArq, Dmod.MyDB); //Local para ficar mais rapido
    UMyMod.AbreQuery(QrMailCont, Dmod.MyDB); //Local para ficar mais rapido
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrWControl, Dmod.MyDBn, [
      'SELECT * ',
      'FROM wcontrol ',
      '']);
    //
    LaPerPasAdm.Caption      := 'Perfil padr�o para senhas do tipo ' + DmkWeb.TruaduzNivel(0, CO_DMKID_APP) + ':';
    EdPerPasAdm.ValueVariant := QrWControl.FieldByName('PerPasAdm').AsInteger;
    CBPerPasAdm.KeyValue     := QrWControl.FieldByName('PerPasAdm').AsInteger;
    LaPerPasBos.Caption      := 'Perfil padr�o para senhas do tipo ' + DmkWeb.TruaduzNivel(1, CO_DMKID_APP) + ':';
    EdPerPasBos.ValueVariant := QrWControl.FieldByName('PerPasBos').AsInteger;
    CBPerPasBos.KeyValue     := QrWControl.FieldByName('PerPasBos').AsInteger;
    LaPerPasCli.Caption      := 'Perfil padr�o para senhas do tipo ' + DmkWeb.TruaduzNivel(2, CO_DMKID_APP) + ':';
    EdPerPasCli.ValueVariant := QrWControl.FieldByName('PerPasCli').AsInteger;
    CBPerPasCli.KeyValue     := QrWControl.FieldByName('PerPasCli').AsInteger;
    LaPerPasUsu.Caption      := 'Perfil padr�o para senhas do tipo ' + DmkWeb.TruaduzNivel(3, CO_DMKID_APP) + ':';
    EdPerPasUsu.ValueVariant := QrWControl.FieldByName('PerPasUsu').AsInteger;
    CBPerPasUsu.KeyValue     := QrWControl.FieldByName('PerPasUsu').AsInteger;
    EdProAltPas.ValueVariant := QrWControl.FieldByName('ProAltPas').AsInteger;
    CBProAltPas.KeyValue     := QrWControl.FieldByName('ProAltPas').AsInteger;
    EdProDowArq.ValueVariant := QrWControl.FieldByName('ProDowArq').AsInteger;
    CBProDowArq.KeyValue     := QrWControl.FieldByName('ProDowArq').AsInteger;
    EdDiasExp.ValueVariant   := QrWControl.FieldByName('DiasExp').AsInteger;
    MeLogoWeb.Text           := QrWControl.FieldByName('LogoWeb').AsString;
    EdCrypt.ValueVariant     := QrWControl.FieldByName('Crypt').AsString;
    EdMailCont.ValueVariant  := QrWControl.FieldByName('MailCont').AsInteger;
    CBMailCont.KeyValue      := QrWControl.FieldByName('MailCont').AsInteger;
    //
    CkFacebookLogin.Checked  := Geral.IntToBool(QrWControl.FieldByName('FacebookLogin').AsInteger);
    //
    if CO_DMKID_APP = 17 then //DControl
    begin
      TabSheet1.TabVisible := True;
      //
      UMyMod.AbreQuery(QrAppDirWeb, Dmod.MyDBn);
      UMyMod.AbreQuery(QrProAtivApl, Dmod.MyDB); //Local para ficar mais rapido
      UMyMod.AbreQuery(QrTermoBoss, Dmod.MyDBn);
      UMyMod.AbreQuery(QrTermoPrivaci, Dmod.MyDBn);
      //
      CkAppZipar.Checked          := Geral.IntToBool(QrWControl.FieldByName('AppZipar').AsInteger);
      EdWebDirApp.ValueVariant    := QrWControl.FieldByName('WebDirApp').AsInteger;
      CBWebDirApp.KeyValue        := QrWControl.FieldByName('WebDirApp').AsInteger;
      EdProAtivApl.ValueVariant   := QrWControl.FieldByName('ProAtivApl').AsInteger;
      CBProAtivApl.KeyValue       := QrWControl.FieldByName('ProAtivApl').AsInteger;
      EdTermoBoss.ValueVariant    := QrWControl.FieldByName('TermoBoss').AsInteger;
      CBTermoBoss.KeyValue        := QrWControl.FieldByName('TermoBoss').AsInteger;
      EdTermoPrivaci.ValueVariant := QrWControl.FieldByName('TermoPrivaci').AsInteger;
      CBTermoPrivaci.KeyValue     := QrWControl.FieldByName('TermoPrivaci').AsInteger;
    end;
    if CO_DMKID_APP = 4 then //Syndi2
    begin
      TabSheet3.TabVisible := True;
      //
      UMyMod.AbreQuery(QrSenha, Dmod.MyDBn);
      //
      EdNomeAdmi.ValueVariant := QrWControl.FieldByName('NomeAdmi').AsString;
      //
      EdUsername.ValueVariant := QrSenha.FieldByName('Username').AsString;
      EdPassword.ValueVariant := QrSenha.FieldByName('Password').AsString;
    end;
  end;
end;

procedure TFmWOpcoes.ReopenWPerfil(Tipo: Integer; Query: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDBn, [
    'SELECT Codigo, Nome ',
    'FROM wperfis ',
    'WHERE Ativo = 1 ',
    'AND Tipo=' + Geral.FF0(Tipo),
    'ORDER BY Nome ',
    '']);
end;

procedure TFmWOpcoes.SpeedButton1Click(Sender: TObject);
begin
  {$IfNDef sWUsr}
  VAR_CADASTRO := 0;
  //
  UWUsersJan.MostraWPerfis(EdPerPasAdm.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdPerPasAdm, CBPerPasAdm, QrWPerfil0, VAR_CADASTRO);
    //
    EdPerPasAdm.SetFocus;
  end;
  {$EndIf}
end;

procedure TFmWOpcoes.SpeedButton2Click(Sender: TObject);
begin
  {$IfNDef sWUsr}
  VAR_CADASTRO := 0;
  //
  UWUsersJan.MostraWPerfis(EdPerPasBos.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdPerPasBos, CBPerPasBos, QrWPerfil1, VAR_CADASTRO);
    //
    EdPerPasBos.SetFocus;
  end;
  {$EndIf}
end;

procedure TFmWOpcoes.SpeedButton3Click(Sender: TObject);
begin
  {$IfNDef sWUsr}
  VAR_CADASTRO := 0;
  //
  UWUsersJan.MostraWPerfis(EdPerPasCli.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdPerPasCli, CBPerPasCli, QrWPerfil2, VAR_CADASTRO);
    //
    EdPerPasCli.SetFocus;
  end;
  {$EndIf}
end;

procedure TFmWOpcoes.SpeedButton4Click(Sender: TObject);
begin
  {$IfNDef sWUsr}
  VAR_CADASTRO := 0;
  //
  UWUsersJan.MostraWPerfis(EdPerPasUsu.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdPerPasUsu, CBPerPasUsu, QrWPerfil3, VAR_CADASTRO);
    //
    EdPerPasUsu.SetFocus;
  end;
  {$EndIf}
end;

procedure TFmWOpcoes.SpeedButton5Click(Sender: TObject);
var
  Codigo: Integer;
begin
  VAR_CADASTRO := 0;
  Codigo       := EdMailCont.ValueVariant;
  //
  if DBCheck.CriaFm(TFmPreEmail, FmPreEmail, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmPreEmail.LocCod(Codigo, Codigo);
    FmPreEmail.ShowModal;
    FmPreEmail.Destroy;
    //
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodigoPesquisado(EdMailCont, CBMailCont, QrMailCont, VAR_CADASTRO);
      //
      EdMailCont.SetFocus;
    end;
  end;
end;

procedure TFmWOpcoes.SpeedButton6Click(Sender: TObject);
{$IfNDef SemProtocolo}
var
  Protocolo: Integer;
{$EndIf}
begin
{$IfNDef SemProtocolo}
  VAR_CADASTRO := 0;
  Protocolo    := EdProAltPas.ValueVariant;
  //
  ProtocoUnit.MostraFormProtocolos(0, Protocolo);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdProAltPas, CBProAltPas, QrProAltPas, VAR_CADASTRO);
    //
    EdProAltPas.SetFocus;
  end;
{$EndIf}
end;

procedure TFmWOpcoes.SpeedButton7Click(Sender: TObject);
{$IfNDef SemProtocolo}
var
  Protocolo: Integer;
{$EndIf}
begin
{$IfNDef SemProtocolo}
  VAR_CADASTRO := 0;
  Protocolo    := EdProDowArq.ValueVariant;
  //
  ProtocoUnit.MostraFormProtocolos(0, Protocolo);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdProDowArq, CBProDowArq, QrProDowArq, VAR_CADASTRO);
    //
    EdProDowArq.SetFocus;
  end;
{$EndIf}
end;

end.
