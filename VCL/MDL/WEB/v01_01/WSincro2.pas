unit WSincro2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, math, Db, mySQLDbTables,
  UMySQLModule, CheckLst, ComCtrls, dmkEdit, unInternalConsts, dmkGeral,
  dmkImage, dmkPermissoes, UnDmkEnums, UnDmkProcFunc, DmkDAC_PF;

type
  TFmWSincro2 = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    QrParc: TmySQLQuery;
    Timer1: TTimer;
    Qr1: TmySQLQuery;
    Qr2: TmySQLQuery;
    Qr3: TmySQLQuery;
    Memo1: TMemo;
    Query: TmySQLQuery;
    CkLista: TCheckListBox;
    LBTabLoc: TListBox;
    LBTabWeb: TListBox;
    LBKeyFl1: TListBox;
    LBDtaTyp: TListBox;
    QrParcParcelamentos: TLargeintField;
    QrQtdL: TmySQLQuery;
    QrQtdW: TmySQLQuery;
    QrRegL: TmySQLQuery;
    QrRegW: TmySQLQuery;
    RE1: TRichEdit;
    Timer2: TTimer;
    Splitter1: TSplitter;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    Label1: TLabel;
    EdTempo: TdmkEdit;
    Label3: TLabel;
    EdFim: TdmkEdit;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    PB1: TProgressBar;
    Label2: TLabel;
    EdIni: TdmkEdit;
    Panel8: TPanel;
    Label4: TLabel;
    GBLct: TGroupBox;
    CkLct: TCheckBox;
    EdDtaUltimaSincro: TdmkEdit;
    RGTipo: TRadioGroup;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure CkListaClick(Sender: TObject);
    procedure LBTabLocClick(Sender: TObject);
    procedure LBTabWebClick(Sender: TObject);
    procedure LBKeyFl1Click(Sender: TObject);
    procedure LBDtaTypClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FExecute: String;
    FArqWeb1, FArqWeb2, FDirLoc, FDirServ: String;
    FCamposInalterados: Boolean;
    FIniUpLoad: TDateTime;
    //
    (*FTabLoc, FTabWeb, FCampo: String;
    FQuery: TmySQLQuery;
    FdataType: Integer;
    BaseDados: TmySQLDatabase;*)
    function  ExportaDadosAlteradosParaWeb(TabLoc, TabWeb, Campo: String;
              {Query: TmySQLQuery;} dataType: Integer; BaseDados: TmySQLDatabase{;
              Registros: Integer}): String;
    {N�o usa
    ///////////////////////////////////////////////// Copiado do DmkDAC_PF - INI
    function  ExportaDadosAlteradosParaWeb2(ModoSicroWeb: TModoSicroWeb;
              TabLoc, TabWeb, Campo: String; DataType: TFormaFieldTxt;
              ValCampo: Variant; BaseDados: TmySQLDatabase;
              Abertos: FUploadAbertos; LaAviso1, LaAviso2: TLabel;
              DBWeb: TmySQLDatabase; Avisa: Boolean): String;
    function  ExportaTodaTabelaParaWeb2(TabLoc, TabWeb, Campo: String;
              DataType: TFormaFieldTxt; BaseDados: TmySQLDatabase;
              Abertos: FUploadAbertos; LaAviso1, LaAviso2: TLabel;
              DBWeb: TmySQLDatabase): String;
    function  ObtemCamposDeTabelaIdentica(DataBase: TmySQLDatabase;
              Tabela, Prefix: String): String;// Prefix -> 'la.' = ref tabela
    procedure DefineArqWebPaths(var ArqWeb1, ArqWeb2: String);
    ///////////////////////////////////////////////// Copiado do DmkDAC_PF - FIM
    }
    function  ExportaTodaTabelaParaWeb(TabLoc, TabWeb, Campo: String;
              dataType: Integer; BaseDados: TmySQLDatabase): String;

    function  ExportaDadosAlteradosParaTmpDir(TabLoc, TabWeb, Campo: String;
              dataType: Integer; BaseDados: TmySQLDatabase): String;
    function  ExportaTodaTabelaParaTmpDir(TabLoc, TabWeb, Campo: String;
              dataType: Integer; BaseDados: TmySQLDatabase): String;

    function  ExportaDadosAlteradosTmpDirParaWeb(TabLoc, TabWeb, Campo: String;
              dataType: Integer; BaseDados: TmySQLDatabase): String;
    function  ExportaTodaTabelaTipDirParaWeb(TabLoc, TabWeb, Campo: String;
              dataType: Integer; BaseDados: TmySQLDatabase): String;

    function  ExcluiArquivosDeCampos(): Boolean;
    procedure ExportaDadosParaWeb();
    //procedure ObtemConfigDeTabelas(Tabela: String);
    //function CamposInalterados(): Boolean;
    procedure VerificaExcluidos;
    procedure AtualizaTempo();
    procedure SelecionarTudo(Selecionar: Boolean);
    procedure SelecionaTabelas();
    procedure SincronizarListas(ItemSel: Integer);
    procedure HabilitaBotoes(Habilita: Boolean);
  public
    { Public declarations }
    FDtaUltimaSincro: TDateTime;
    FTabelasSel: TStringList;
    procedure ListaCheckAll();
    procedure ListasClear();
    procedure ListasAdd(Descri, TabLoc, TabWeb, KeyFld: String; DtaTyp: Integer);
  end;

  var
  FmWSincro2: TFmWSincro2;

implementation

{$R *.DFM}

uses UnMyObjects, Module, Principal, ModuleGeral;

procedure TFmWSincro2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWSincro2.BtTodosClick(Sender: TObject);
begin
  SelecionarTudo(True);
end;

procedure TFmWSincro2.CkListaClick(Sender: TObject);
begin
  SincronizarListas(CkLista.ItemIndex);
end;

procedure TFmWSincro2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  Timer1.Enabled := True;
end;

procedure TFmWSincro2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWSincro2.FormShow(Sender: TObject);
begin
  EdDtaUltimaSincro.ValueVariant := FDtaUltimaSincro;
  SelecionaTabelas();
end;

procedure TFmWSincro2.HabilitaBotoes(Habilita: Boolean);
begin
  BtOK.Enabled     := Habilita;
  BtTodos.Enabled  := Habilita;
  BtNenhum.Enabled := Habilita;
  Panel3.Enabled   := Habilita;
end;

procedure TFmWSincro2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PB1.Position    := 0;
  //
  Query.Database  := Dmod.MyDB;
  Qr1.Database    := Dmod.MyDB;
  QrRegL.Database := Dmod.MyDB;
  QrQtdL.Database := Dmod.MyDB;
  //
  QrParc.Database := Dmod.MyDBn;
  Qr2.Database    := Dmod.MyDBn;
  Qr3.Database    := Dmod.MyDBn;
  QrRegW.Database := Dmod.MyDBn;
  QrQtdW.Database := Dmod.MyDBn;
end;

procedure TFmWSincro2.AtualizaTempo();
var
  Tempo: TTime;
begin
  Tempo := Now() - FIniUpLoad;
  EdTempo.ValueVariant := Tempo;
  EdTempo.Update;
end;

procedure TFmWSincro2.BtNenhumClick(Sender: TObject);
begin
  SelecionarTudo(False);
end;

procedure TFmWSincro2.BtOKClick(Sender: TObject);
var
  Agora : TDateTime;
begin
  HabilitaBotoes(False);
  //
  FIniUpLoad := Now();
  EdIni.ValueVariant := Geral.FDT(Now(), 0);
  EdFim.Text := '';
  Screen.Cursor := crHourGlass;
  Timer2.Enabled := True;
  try
    // Atualiza tabela web informando que h� um sincronismo em execu��o
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
      'UPDATE wcontrol SET SincroEmExec=1 ',
      '']);
    //
    FCamposInalterados := Dmod.QrControle.FieldByName('Versao').AsInteger = Dmod.QrControle.FieldByName('VerWeb').AsInteger;
    ExportaDadosParaWeb();
    if not FCamposInalterados then
    begin
      // Atualiza tabela controle
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE controle SET VerWeb=Versao');
      Dmod.QrUpd.ExecSQL;
    end;
    if RGTipo.ItemIndex = 0 then VerificaExcluidos;
    //
    // Atualiza data sincro
    if(CkLista.Items.Count = dmkPF.CheckedItemsCount(CkLista)) then
    begin
      // Atualiza tabela local
      Agora := DModG.ObtemAgora;
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'UPDATE controle SET DtaSincro="' + Geral.FDT(Agora, 9) + '"',
        '']);
      // Atualiza tabela web
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
        'UPDATE wcontrol SET SincroEmExec=0, DtaSincro="' + Geral.FDT(Agora, 9) + '"',
        '']);
    end else
    begin
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
        'UPDATE wcontrol SET SincroEmExec=0',
        '']);
    end;
    FDtaUltimaSincro               := Agora;
    EdDtaUltimaSincro.ValueVariant := Agora;
    EdFim.ValueVariant             := Geral.FDT(Now(), 0);
  finally
    Timer2.Enabled := False;
    AtualizaTempo();
    Screen.Cursor := crDefault;
    //
    Close;
  end;
end;

procedure TFmWSincro2.ExportaDadosParaWeb();
var
  i: Integer;
  TabLoc, TabWeb, KeyFld: String;
begin
  PB1.Max := CkLista.Items.Count;
  //
  //Gera arquivos tempor�rios
  for i := 0 to CkLista.Items.Count - 1 do
  begin
    Application.ProcessMessages;
    Update;
    //
    if CkLista.Checked[i] = True then
    begin
      TabLoc := LBTabLoc.Items[i];
      TabWeb := LBTabWeb.Items[i];
      KeyFld := LBKeyFl1.Items[i];
      TabLoc := LBTabLoc.Items[i];
      //
      if DModG.EhOServidor then
      begin
        ExportaDadosAlteradosParaWeb(TabLoc, TabWeb, KeyFld, {QrLotes,} 1,
          Dmod.MyDB{, Geral.IMV(Edlo.Text)});
      end else
      begin
        //Exporta dados para o servidor
        ExportaDadosAlteradosParaTmpDir(TabLoc, TabWeb, KeyFld, {QrLotes,} 1,
          Dmod.MyDB{, Geral.IMV(Edlo.Text)});
      end;
    end;
    PB1.Position := PB1.Position + 1;
    PB1.Update;
    Application.ProcessMessages;
  end;
  PB1.Position := 0;
  //Copia para o cliente
  if dmkPF.MoveDir(FDirServ, FDirLoc, True) then
  begin
    for i := 0 to CkLista.Items.Count - 1 do
    begin
      Application.ProcessMessages;
      Update;
      //
      if CkLista.Checked[i] = True then
      begin
        TabLoc := LBTabLoc.Items[i];
        TabWeb := LBTabWeb.Items[i];
        KeyFld := LBKeyFl1.Items[i];
        TabLoc := LBTabLoc.Items[i];
        //
        //Atualiza no DB web
        ExportaDadosAlteradosTmpDirParaWeb(TabLoc, TabWeb, KeyFld, {QrLotes,} 1,
          Dmod.MyDB{, Geral.IMV(Edlo.Text)})
      end;
      PB1.Position := PB1.Position + 1;
      PB1.Update;
      Application.ProcessMessages;
    end;
  end else
    Geral.MB_Aviso('Falha ao copiar arquivos do servidor para o computador local!' +
      sLineBreak + 'Verifique se o diret�rio: "' + FDirServ + '" ' + sLineBreak +
      'est� compartilhado e acess�vel!');
  //   Lotes
  (*
  ExportaDadosAlteradosParaWeb('lotes', 'lotes', 'codigo', QrLotes, 1,
    Dmod.MyDB, Geral.IMV(Edlo.Text));
  *)
  PB1.Position            := 0;
  RE1.SelAttributes.Color := clBlue;
  RE1.Lines.Add('Sincroniza��o finalizada!');
  //
  Geral.MB_Aviso('Sincroniza��o finalizada!');
end;

procedure TFmWSincro2.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  ExcluiArquivosDeCampos();
  //
  {$IfDef ParcelamentWeb}
  QrParc.Close;
  QrParc.Open;
  {$EndIf}
  //
{
  case QrParcParcelamentos.Value of
    0: ;// Nada
    1:
    begin
      Geral.MB_Aviso('H� um novo parcelamento de d�bitos no site.' +
      ' � necess�rio process�-lo antes de sincronizar!');
      Close;
    end;
    else begin
      Geral.MB_Aviso('Existem ' + IntToStr(
      QrParcParcelamentos.Value) + ' novos parcelamentos de d�bitos no site.' +
      ' � recomend�vel process�-los antes de sincronizar!');
      Close;
    end;
  end;
  // Impedir de sincronizar desfazendo la�amentos.reparcel <> 0
}
  {$IfDef ParcelamentWeb}
  if QrParcParcelamentos.Value > 0 then
    FmPrincipal.MostraBloqParcAviso(QrParcParcelamentos.Value);
  HabilitaBotoes(QrParcParcelamentos.Value = 0);
  {$Else}
  HabilitaBotoes(True);
  {$EndIf}
end;

procedure TFmWSincro2.Timer2Timer(Sender: TObject);
begin
  AtualizaTempo();
end;

function TFmWSincro2.ExportaDadosAlteradosParaTmpDir(TabLoc, TabWeb, Campo: String;
  dataType: Integer; BaseDados: TmySQLDatabase): String;
var
  Arq: String;
  Campos, Prefix, txt: String;
  QtdFlds: Integer;
begin
  try
    if RGTipo.ItemIndex = 1 then
    begin
      ExportaTodaTabelaParaTmpDir(TabLoc, TabWeb, Campo, dataType, BaseDados);
      Exit;
    end;
    Query.Close;
    Query.SQL.Clear;
    Query.SQL.Add('SELECT * FROM ' + Lowercase(TabLoc));
    Query.SQL.Add('WHERE AlterWeb = 1');
    Query.Open;
    if Query.RecordCount = 0 then Exit;
    //
    Qr1.Close;
    Qr1.Database := BaseDados;
    Campos := '';
    Prefix := '';
    RE1.SelAttributes.Color := clBlue;
    RE1.Text := Uppercase(TabWeb) + ' - Obtendo campos da tabela na web...';
    Update;
    Application.ProcessMessages;
    //
    Campos := UMyMod.ObtemCamposDeTabelaIdentica(
      Dmod.MyDBn, TabWeb, Prefix, QtdFlds);
    //
    RE1.SelAttributes.Color := clBlue;
    RE1.Text := Uppercase(TabWeb) + ' - Exportando para arquivo tempor�rio...';
    Update;
    Arq := Format(FArqWeb2, [TabWeb, 'txt']);
    if FileExists(Arq) then DeleteFile(Arq);
    Application.ProcessMessages;
    Qr1.SQL.Clear;
    Qr1.SQL.Add('SELECT '+Campos);
    Qr1.SQL.Add('FROM '+ Lowercase(TabLoc));
    Qr1.SQL.Add('WHERE AlterWeb = 1');
    Qr1.SQL.Add('INTO OUTFILE "' + Arq + '"');
    Qr1.SQL.Add('CHARACTER SET Latin1');
    Qr1.ExecSQL;
    //
    RE1.SelAttributes.Color := clBlue;
    RE1.Text := Uppercase(TabWeb) + ' - Preparando para excluir registros duplicados na web...';
    Update;
    Application.ProcessMessages;
    Qr3.SQL.Clear;
    Qr3.SQL.Add(DELETE_FROM + Lowercase(TabWeb) + ' WHERE ' + Campo + ' in (');
    txt := '';
    Query.First;
    while not Query.Eof do
    begin
      case dataType of
        1: txt := txt + IntToStr(Query.FieldByName(Campo).Value)+',';
        2: txt := txt + '"' + Geral.FDT(Query.FieldByName(Campo).Value, 2)+'",';
        else txt := txt + '"' + Query.FieldByName(Campo).Value+'",';
      end;
      Query.Next;
    end;
    if txt <> '' then
    begin
      txt[Length(txt)] := ')';
      Qr3.SQL.Add(txt);
      //
      RE1.SelAttributes.Color := clBlue;
      RE1.Text := Uppercase(TabWeb) + ' - Excluindo registros duplicados na web...';
      Update;
      Application.ProcessMessages;
      Qr3.ExecSQL;
    end;
  except
    ExcluiArquivosDeCampos();
    raise;
  end;
end;

function TFmWSincro2.ExportaDadosAlteradosTmpDirParaWeb(TabLoc, TabWeb, Campo: String;
  dataType: Integer; BaseDados: TmySQLDatabase): String;
var
  Arq: String;
begin
  Arq := Format(FArqWeb2, [TabWeb, 'txt']);
  //
  if RGTipo.ItemIndex = 1 then
  begin
    ExportaTodaTabelaTipDirParaWeb(TabLoc, TabWeb, Campo, dataType, BaseDados);
    Exit;
  end;
  try
    RE1.SelAttributes.Color := clBlue;
    RE1.Text := Uppercase(TabWeb) + ' - Importando de arquivo tempor�rio...';
    Update;
    Application.ProcessMessages;
    //
    Qr2.SQL.Clear;
    Qr2.SQL.Add('LOAD DATA LOCAL INFILE "' + Arq + '"');
    Qr2.SQL.Add('INTO Table ' + TabWeb);
    Qr2.SQL.Add('CHARACTER SET Latin1');
    Qr2.ExecSQL;
    //
    Qr1.SQL.Clear;
    Qr1.SQL.Add('UPDATE '+lowercase(tabloc)+' SET AlterWeb=0');
    Qr1.ExecSQL;
  except
    ExcluiArquivosDeCampos();
    raise;
  end;
end;

function TFmWSincro2.ExportaDadosAlteradosParaWeb(TabLoc, TabWeb, Campo: String;
  {Query: TmySQLQuery; }dataType: Integer; BaseDados: TmySQLDatabase{;
  Registros: Integer}): String;
var
  Arq: String;
  //F: TextFile;
  //S,
  Campos, Prefix, txt: String;
  QtdFlds: Integer;
begin
  try
  if RGTipo.ItemIndex = 1 then
  begin
    ExportaTodaTabelaParaWeb(TabLoc, TabWeb, Campo, dataType, BaseDados);
    Exit;
  end;
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT * FROM ' + Lowercase(TabLoc));
  Query.SQL.Add('WHERE AlterWeb = 1');
  Query.Open;
  if Query.RecordCount = 0 then Exit;

  //

  Qr1.Close;
  Qr1.Database := BaseDados;
  Campos := '';
  Prefix := '';
  RE1.SelAttributes.Color := clBlue;
  RE1.Text := Uppercase(TabWeb) + ' - Obtendo campos da tabela na web...';
  Update;
  Application.ProcessMessages;

  Campos := UMyMod.ObtemCamposDeTabelaIdentica(
    Dmod.MyDBn, TabWeb, Prefix, QtdFlds);

  //

  RE1.SelAttributes.Color := clBlue;
  RE1.Text := Uppercase(TabWeb) + ' - Exportando para arquivo tempor�rio...';
  Update;
  Arq := Format(FArqWeb2, [TabWeb, 'txt']);
  if FileExists(Arq) then DeleteFile(Arq);
  Application.ProcessMessages;
  Qr1.SQL.Clear;
  Qr1.SQL.Add('SELECT '+Campos);
  Qr1.SQL.Add('FROM '+ Lowercase(TabLoc));
  Qr1.SQL.Add('WHERE AlterWeb = 1');
  Qr1.SQL.Add('INTO OUTFILE "' + Arq + '"');
  Qr1.SQL.Add('CHARACTER SET Latin1');
  Qr1.ExecSQL;

  //

  RE1.SelAttributes.Color := clBlue;
  RE1.Text := Uppercase(TabWeb) + ' - Preparando para excluir registros duplicados na web...';
  Update;
  Application.ProcessMessages;
  Qr3.SQL.Clear;
  Qr3.SQL.Add(DELETE_FROM + Lowercase(TabWeb) + ' WHERE ' + Campo + ' in (');
  txt := '';
  Query.First;
  while not Query.Eof do
  begin
    case dataType of
      1: txt := txt + IntToStr(Query.FieldByName(Campo).Value)+',';
      2: txt := txt + '"' + Geral.FDT(Query.FieldByName(Campo).Value, 2)+'",';
      else txt := txt + '"' + Query.FieldByName(Campo).Value+'",';
    end;
    Query.Next;
  end;
  if txt <> '' then
  begin
    txt[Length(txt)] := ')';
    Qr3.SQL.Add(txt);
    //

    RE1.SelAttributes.Color := clBlue;
    RE1.Text := Uppercase(TabWeb) + ' - Excluindo registros duplicados na web...';
    Update;
    Application.ProcessMessages;
    Qr3.ExecSQL;
  end;
  //
  Arq := Format(FArqWeb1, [TabWeb, 'txt']);
  //
  RE1.SelAttributes.Color := clBlue;
  RE1.Text := Uppercase(TabWeb) + ' - Importando de arquivo tempor�rio...';
  Update;
  Application.ProcessMessages;
  Qr2.SQL.Clear;
  Qr2.SQL.Add('LOAD DATA LOCAL INFILE "' + Arq + '"');
  Qr2.SQL.Add('INTO Table ' + TabWeb);
  Qr2.SQL.Add('CHARACTER SET Latin1');
  Qr2.ExecSQL;
  //
  Qr1.SQL.Clear;
  Qr1.SQL.Add('UPDATE '+lowercase(tabloc)+' SET AlterWeb=0');
  Qr1.ExecSQL;
  except
    ExcluiArquivosDeCampos();
    raise;
  end;
end;

function TFmWSincro2.ExportaTodaTabelaTipDirParaWeb(TabLoc, TabWeb, Campo: String;
dataType: Integer; BaseDados: TmySQLDatabase): String;
var
  Arq: String;
begin
  Arq := Format(FArqWeb2, [TabWeb, 'txt']);
  //
  RE1.SelAttributes.Color := clBlue;
  RE1.Text := Uppercase(TabWeb) + ' - Importando de arquivo tempor�rio...';
  Update;
  Application.ProcessMessages;
  //
  Qr2.SQL.Clear;
  Qr2.SQL.Add('LOAD DATA LOCAL INFILE "' + Arq + '"');
  Qr2.SQL.Add('INTO Table '+TabWeb);
  Qr2.SQL.Add('CHARACTER SET Latin1');
  Qr2.ExecSQL;
  //
  Qr1.SQL.Clear;
  Qr1.SQL.Add('UPDATE '+lowercase(tabloc)+' SET AlterWeb=0');
  Qr1.ExecSQL;
end;

function TFmWSincro2.ExportaTodaTabelaParaTmpDir(TabLoc, TabWeb, Campo: String;
dataType: Integer; BaseDados: TmySQLDatabase): String;
  function EhTabelaDeLct(NomeTb: String): Boolean;
  var
    nTbI, nTbM: String;
    nTbF: Char;
  begin
    nTbI := Copy(NomeTb, 1, 3);
    nTbM := Copy(NomeTb, 4, Length(NomeTb)-4);
    nTbF := NomeTb[Length(NomeTb)];
    //
    Result := (nTbI = 'lct') and
    (nTbF in ['a','b','d']) and
    (nTbM = Geral.SoNumero_TT(nTbM));
  end;
var
  Arq: String;
  Campos, Prefix: String;
  QtdFlds: Integer;
begin
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT * FROM ' + Lowercase(TabLoc));
  Query.Open;
  //
  Qr1.Close;
  Qr1.Database := BaseDados;
  Campos := '';
  Prefix := '';
  RE1.SelAttributes.Color := clBlue;
  RE1.Text := Uppercase(TabWeb) + ' - Obtendo campos da tabela na web...';
  Update;
  Application.ProcessMessages;
  //
  if (LowerCase(TabWeb) = 'protpakits') then
    Campos := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDBn, TabWeb, 'its.', QtdFlds)
  else
    Campos := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDBn, TabWeb, Prefix, QtdFlds);
  //
  RE1.SelAttributes.Color := clBlue;
  RE1.Text := Uppercase(TabWeb) + ' - Exportando para arquivo tempor�rio...';
  Update;
  Arq := Format(FArqWeb2, [TabWeb, 'txt']);
  if FileExists(Arq) then DeleteFile(Arq);
  Application.ProcessMessages;
  //
  Qr1.SQL.Clear;
  //
  if (LowerCase(TabLoc) = 'protpakits') then
  begin
    Qr1.SQL.Add('SELECT '+Campos);
    Qr1.SQL.Add('FROM '+ Lowercase(TabLoc) + ' its ');
    Qr1.SQL.Add('LEFT JOIN protocolos pro ON pro.Codigo = its.Codigo');
    Qr1.SQL.Add('WHERE pro.Tipo = 4'); //CR
    Qr1.SQL.Add('AND pro.Def_Retorn = 1');
    Qr1.SQL.Add('AND its.DataE < "1900-01-01"'); //Apenas n�o confirmados por causa da 2� via
    Qr1.SQL.Add('AND its.Cancelado = 0');
  end else
  begin
    Qr1.SQL.Add('SELECT '+Campos);
    Qr1.SQL.Add('FROM '+ Lowercase(TabLoc));
    //
    if CkLct.Checked then
    begin
      if (Lowercase(TabLoc) = LAN_CTOS) or EhTabelaDeLct(Lowercase(TabLoc)) then
        Qr1.SQL.Add('WHERE Sit < 2 AND Tipo = 2'); // Somente abertos
    end;
  end;
  Qr1.SQL.Add('INTO OUTFILE "' + Arq + '"');
  Qr1.SQL.Add('CHARACTER SET Latin1');
  Qr1.ExecSQL;
  //
  RE1.SelAttributes.Color := clBlue;
  RE1.Text := Uppercase(TabWeb) + ' - Excluindo registros duplicados na web...';
  Update;
  Application.ProcessMessages;
  Qr3.SQL.Clear;
  Qr3.SQL.Add(DELETE_FROM + Lowercase(TabWeb));
  Application.ProcessMessages;
  Qr3.ExecSQL;
end;

function TFmWSincro2.ExportaTodaTabelaParaWeb(TabLoc, TabWeb, Campo: String;
dataType: Integer; BaseDados: TmySQLDatabase): String;

  function EhTabelaDeLct(NomeTb: String): Boolean;
  var
    nTbI, nTbM: String;
    nTbF: Char;
  begin
    nTbI := Copy(NomeTb, 1, 3);
    nTbM := Copy(NomeTb, 4, Length(NomeTb)-4);
    nTbF := NomeTb[Length(NomeTb)];
    //
    Result := (nTbI = 'lct') and
    (nTbF in ['a','b','d']) and
    (nTbM = Geral.SoNumero_TT(nTbM));
  end;

var
  Arq: String;
  //F: TextFile;
  //S,
  Campos, Prefix: String;
  QtdFlds: Integer;
begin
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT * FROM ' + Lowercase(TabLoc));
  Query.Open;
  //
  Qr1.Close;
  Qr1.Database := BaseDados;
  Campos := '';
  Prefix := '';
  RE1.SelAttributes.Color := clBlue;
  RE1.Text := Uppercase(TabWeb) + ' - Obtendo campos da tabela na web...';
  Update;
  Application.ProcessMessages;

  Campos := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDBn, TabWeb, Prefix, QtdFlds);

  //
  RE1.SelAttributes.Color := clBlue;
  RE1.Text := Uppercase(TabWeb) + ' - Exportando para arquivo tempor�rio...';
  Update;
  Arq := Format(FArqWeb2, [TabWeb, 'txt']);
  if FileExists(Arq) then DeleteFile(Arq);
  Application.ProcessMessages;
  Qr1.SQL.Clear;
  Qr1.SQL.Add('SELECT '+Campos);
  Qr1.SQL.Add('FROM '+ Lowercase(TabLoc));
  //Qr1.SQL.Add('WHERE AlterWeb = 1');
  //if (Lowercase(TabLoc) = VAR LCT) and CkLct.Checked then
  if CkLct.Checked then
  begin
    if (Lowercase(TabLoc) = LAN_CTOS) or EhTabelaDeLct(Lowercase(TabLoc)) then
      Qr1.SQL.Add('WHERE Sit < 2 AND Tipo = 2'); // Somente abertos
  end;
  if DModG.EhOServidor then
  begin
    Arq := dmkPF.InverteBarras(Arq);
    Arq := dmkPF.DuplicaBarras(Arq);
  end;
  //
  Qr1.SQL.Add('INTO OUTFILE "' + Arq + '"');
  Qr1.SQL.Add('CHARACTER SET Latin1');
  Qr1.ExecSQL;

  //

  RE1.SelAttributes.Color := clBlue;
  RE1.Text := Uppercase(TabWeb) + ' - Excluindo registros duplicados na web...';
  Update;
  Application.ProcessMessages;
  Qr3.SQL.Clear;
  Qr3.SQL.Add(DELETE_FROM + Lowercase(TabWeb));
  Application.ProcessMessages;
  Qr3.ExecSQL;
  //
  Arq := Format(FArqWeb1, [TabWeb, 'txt']);
  //
  RE1.SelAttributes.Color := clBlue;
  RE1.Text := Uppercase(TabWeb) + ' - Importando de arquivo tempor�rio...';
  Update;
  Application.ProcessMessages;
  Qr2.SQL.Clear;
  Qr2.SQL.Add('LOAD DATA LOCAL INFILE "' + Arq + '"');
  Qr2.SQL.Add('INTO Table '+TabWeb);
  Qr2.SQL.Add('CHARACTER SET Latin1');
  Qr2.ExecSQL;
  //
  Qr1.SQL.Clear;
  Qr1.SQL.Add('UPDATE '+lowercase(tabloc)+' SET AlterWeb=0');
  Qr1.ExecSQL;
end;

(*procedure ObtemConfigDeTabelas(Tabela: String);
begin
  if Uppercase(Tabela) = Uppercase('') then
    FTabLoc, FTabWeb, FCampo: String;
    FQuery: TmySQLQuery;
    FdataType: Integer;
    BaseDados: TmySQLDatabase;
  begin
  end else
end;*)

procedure TFmWSincro2.ListasClear();
begin
  CkLista.Clear;
  LBTabLoc.Clear;
  LBTabWeb.Clear;
  LBKeyFl1.Clear;
  LBDtaTyp.Clear;
end;

procedure TFmWSincro2.SelecionarTudo(Selecionar: Boolean);
var
  i: Integer;
begin
  for i := 0 to CkLista.Count - 1 do
  begin
    CkLista.Checked[i] := Selecionar;
  end;
end;

procedure TFmWSincro2.SelecionaTabelas;
var
  i, j: Integer;
begin
  if FTabelasSel.Count > 0 then
  begin
    SelecionarTudo(False);
    for i := 0 to FTabelasSel.Count - 1 do
    begin
      for j := 0 to LBTabLoc.Count - 1 do
      begin
        if LowerCase(LBTabLoc.Items[j]) = LowerCase(FTabelasSel[i]) then
        begin
          CkLista.Checked[j] := True;
          Break;
        end;
      end;
    end;
  end;
end;

procedure TFmWSincro2.SincronizarListas(ItemSel: Integer);
begin
  CkLista.ItemIndex  := ItemSel;
  LBTabLoc.ItemIndex := ItemSel;
  LBTabWeb.ItemIndex := ItemSel;
  LBKeyFl1.ItemIndex := ItemSel;
  LBDtaTyp.ItemIndex := ItemSel;
end;

procedure TFmWSincro2.ListasAdd(Descri, TabLoc, TabWeb, KeyFld: String;
  DtaTyp: Integer);
begin
  CkLista.Items.Add(Descri);
  LBTabLoc.Items.Add(TabLoc);
  LBTabWeb.Items.Add(TabWeb);
  LBKeyFl1.Items.Add(KeyFld);
  LBDtaTyp.Items.Add(IntToStr(DtaTyp));
end;

procedure TFmWSincro2.LBDtaTypClick(Sender: TObject);
begin
  SincronizarListas(LBDtaTyp.ItemIndex);
end;

procedure TFmWSincro2.LBKeyFl1Click(Sender: TObject);
begin
  SincronizarListas(LBKeyFl1.ItemIndex);
end;

procedure TFmWSincro2.LBTabLocClick(Sender: TObject);
begin
  SincronizarListas(LBTabLoc.ItemIndex);
end;

procedure TFmWSincro2.LBTabWebClick(Sender: TObject);
begin
  SincronizarListas(LBTabWeb.ItemIndex);
end;

procedure TFmWSincro2.ListaCheckAll();
var
  i: Integer;
begin
  for i := 0 to CkLista.Items.Count - 1 do
    CkLista.Checked[i] := True;
end;

procedure TFmWSincro2.VerificaExcluidos;
var
  i, Dif: Integer;
  TabLoc, TabWeb, KeyFld: String;
begin
  RE1.SelAttributes.Color := clBlue;
  RE1.Text := 'Avisos:';
  for i := 0 to CkLista.Items.Count - 1 do
  begin
    if CkLista.Checked[i] = True then
    begin
      TabLoc := LBTabLoc.Items[i];
      TabWeb := LBTabWeb.Items[i];
      KeyFld := LBKeyFl1.Items[i];

      QrQtdL.Close;
      QrQtdL.SQL.Clear;
      QrQtdL.SQL.Add('SELECT COUNT(*) Registros');
      QrQtdL.SQL.Add('FROM ' + Lowercase(TabLoc));
      QrQtdL.SQL.Add('');
      QrQtdL.Open;

      QrQtdW.Close;
      QrQtdW.SQL.Clear;
      QrQtdW.SQL.Add('SELECT COUNT(*) Registros');
      QrQtdW.SQL.Add('FROM ' + Lowercase(TabWeb));
      QrQtdW.SQL.Add('');
      QrQtdW.Open;

      Dif := QrQtdW.FieldbyName('Registros').AsInteger - QrQtdL.FieldbyName('Registros').AsInteger;
      if (Dif > 0) then
      begin
        RE1.SelAttributes.Color := clRed;
        RE1.Text := 'A tabela web "' + TabWeb + '" possui ' +
          IntToStr(Dif) + ' registros que devem ser exclu�dos!';

        QrRegL.Close;
        QrRegL.SQL.Clear;
        QrRegL.SQL.Add('SELECT ' + KeyFld);
        QrRegL.SQL.Add('FROM ' + Lowercase(TabLoc));
        QrRegL.Open;

        QrRegW.Close;
        QrRegW.SQL.Clear;
        QrRegW.SQL.Add('SELECT ' + KeyFld);
        QrRegW.SQL.Add('FROM ' + LowerCase(TabWeb));
        QrRegW.SQL.Add('');
        QrRegW.Open;

        QrRegL.First;
        while not QrRegL.Eof do
        begin

          //Parei aqui
          QrRegL.Next;
        end;
      end;
    end;
  end;
  //   Lotes
  {ExportaDadosAlteradosParaWeb('lotes', 'lotes', 'codigo', QrLotes, 1,
    Dmod.MyDB, Geral.IMV(Edlo.Text));
  }
  RE1.SelAttributes.Color := clRed;
  RE1.Text := 'Sincroniza��o finalizada!';
end;

function TFmWSincro2.ExcluiArquivosDeCampos(): Boolean;
var
  SearchRec: TSearchRec;
  //Res: Integer;
  Arq, NameLoc, NameServ, NameDirServ, DirLoc, DirServ: String;
begin
  Result := False;
  //
  NameLoc := CO_DIR_RAIZ_DMK + '\Web\' + Application.Title + '\Data\';
  DirLoc  := CO_DIR_RAIZ_DMK2 + '/Web/' + Application.Title + '/Data/';
  //
  if not DirectoryExists(NameLoc) then
  begin
    if not ForceDirectories(NameLoc) then
    begin
      Geral.MB_Erro('Falha ao criar diret�rio tempor�rio no destino: ' + NameLoc + '!');
      Exit;
    end;
  end;
  if not DModG.EhOServidor then
  begin
    NameDirServ := '\\'+ VAR_IP +'\Dermatek';
    //
    if not DirectoryExists(NameDirServ) then
    begin
      Geral.MB_Aviso('N�o foi poss�vel localizar o seguinte diret�rio: "' + NameDirServ + '"!' +
        sLineBreak + 'Verifique se o diret�rio "' + CO_DIR_RAIZ_DMK +
        '" est� compartilhado no servidor.' + sLineBreak +
        'Em seguida tente novamente!');
      Exit;
    end;
    NameServ := NameDirServ + '\Web\' + Application.Title + '\Data\';
    DirServ  := '//'+ VAR_IP +'/Dermatek/Web/' + Application.Title + '/Data/';
    //
    if not DirectoryExists(NameServ) then
    begin
      if not ForceDirectories(NameServ) then
      begin
        Geral.MB_Erro('Falha ao criar diret�rio tempor�rio no destino: ' + NameServ + '!' +
          sLineBreak + 'Verifique se o diret�rio "' + NameDirServ +
          '" possui permiss�o para editar / criar diret�rios e arquivos!' +
          sLineBreak + 'Em seguida tente novamente!');
        Exit;
      end;
    end;
  end;
  (*
  Res := FindFirst(NameLoc + '*.*', faAnyFile, SearchRec);
  //
  Screen.Cursor := crHourGlass;
  try
    while Res = 0 do
    begin
      Arq := NameLoc + SearchRec.Name;
      //
      DeleteFile(Arq);
      //
      Res := FindNext(SearchRec);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
  *)
  if not dmkPF.RemoveArquivosDeDiretorio(NameLoc, True) then
  begin
    Geral.MB_Erro('Falha ao remover arquivos tempor�rios!');
    Exit;
  end;
  //
  if not DModG.EhOServidor then
  begin
    (*
    Res := FindFirst(NameServ + '*.*', faAnyFile, SearchRec);
    //
    Screen.Cursor := crHourGlass;
    try
      while Res = 0 do
      begin
        Arq := NameServ + SearchRec.Name;
        //
        DeleteFile(Arq);
        //
        Res := FindNext(SearchRec);
      end;
    finally
      Screen.Cursor := crDefault;
    end;
    *)
    if not dmkPF.RemoveArquivosDeDiretorio(NameServ, True) then
    begin
      Geral.MB_Erro('Falha ao remover arquivos tempor�rios!');
      Exit;
    end;
  end;
  Geral.MB_Aviso('Todos arquivos configura��es de upload foram ' +
    'exclu�dos para serem renovados!');
  //
  FArqWeb1 := CO_DIR_RAIZ_DMK + '\Web\' + Application.Title + '\Data\SQL_%s.%s';
  FArqWeb2 := DirLoc + 'SQL_%s.%s';
  FDirLoc  := CO_DIR_RAIZ_DMK + '\Web\' + Application.Title + '\';
  FDirServ := NameServ;
  //
  Result := True;
end;

{ N�o usa
///////////////////////////////////////////////////// Copiado do DmkDAC_PF - INI
function TFmWSincro2.ExportaDadosAlteradosParaWeb2(ModoSicroWeb: TModoSicroWeb;
  TabLoc, TabWeb, Campo: String; DataType: TFormaFieldTxt; ValCampo: Variant;
  BaseDados: TmySQLDatabase; Abertos: FUploadAbertos; LaAviso1, LaAviso2:
  TLabel; DBWeb: TmySQLDatabase; Avisa: Boolean): String;
var
  Arq, ArqWeb1, ArqWeb2: String;
  Campos, Prefix, Txt, Val, SQL1, SQL3: String;
  Query, Qr1, Qr2, Qr3: TmySQLQuery;
begin
  try
    case ModoSicroWeb of
      mswUnico: SQL1 := 'WHERE ' + Campo + '=' + Geral.VariavelToString(ValCampo);
      mswParcial: SQL1 := 'WHERE AlterWeb = 1';
      mswTotal:
      begin
        ExportaTodaTabelaParaWeb2(TabLoc, TabWeb, Campo, DataType, BaseDados,
        Abertos, LaAviso1, LaAviso2, DBWeb);
        Exit;
      end;
      else//mswNone:
      begin
        Geral.MB_Erro('Modo de sincroniza��o indefinida!');
        Exit;
      end;
    end;
  (*
    Query.Close;
    Query.SQL.Clear;
    Query.SQL.Add('SELECT * FROM ' + Lowercase(TabLoc));
    Query.SQL.Add('WHERE AlterWeb = 1');
    Query. O p e n ;
    if Query.RecordCount = 0 then Exit;
  *)
    DefineArqWebPaths(ArqWeb1, ArqWeb2);

    Query := TmySQLQuery.Create(TDataModule(BaseDados.Owner));
    Qr1   := TmySQLQuery.Create(TDataModule(BaseDados.Owner));
    Qr2   := TmySQLQuery.Create(TDataModule(BaseDados.Owner));
    Qr3   := TmySQLQuery.Create(TDataModule(BaseDados.Owner));

    try
      UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
        'SELECT * FROM ' + LowerCase(TabLoc),
        SQL1,
        '']);
      if Query.RecordCount = 0 then
        Exit;
      //
  (*
      Qr1.Close;
      Qr1.Database := BaseDados;
  *)
      Campos := '';
      Prefix := '';
  (*
      RE1.SelAttributes.Color := clBlue;
      RE1.Text := Uppercase(TabWeb) + ' - Obtendo campos da tabela na web...';
      Update;
      Application.ProcessMessages;
  *)
      MyObjects.Informa2(LaAviso1, LaAviso2, True,
        Uppercase(TabWeb) + ' - Obtendo campos da tabela na web...');
      Campos := ObtemCamposDeTabelaIdentica(
        DBWeb, TabWeb, Prefix);

      //

  (*
      RE1.SelAttributes.Color := clBlue;
      RE1.Text := Uppercase(TabWeb) + ' - Exportando para arquivo tempor�rio...';
      Update;
      Application.ProcessMessages;
  *)
      MyObjects.Informa2(LaAviso1, LaAviso2, True,
        Uppercase(TabWeb) + ' - Exportando para arquivo tempor�rio...');
      Arq := Format(ArqWeb2, [TabWeb, 'txt']);
      if FileExists(Arq) then
        DeleteFile(Arq);

  (*
      Qr1.SQL.Clear;
      Qr1.SQL.Add('SELECT '+Campos);
      Qr1.SQL.Add('FROM '+ Lowercase(TabLoc));
      Qr1.SQL.Add('WHERE AlterWeb = 1');
      Qr1.SQL.Add('INTO OUTFILE "' + Arq + '"');
      Qr1.ExecSQL;
  *)
      UnDmkDAC_PF.ExecutaMySQLQuery0(Qr1, Dmod.MyDB, [
        'SELECT ' + Campos,
        'FROM '+ LowerCase(TabLoc),
        SQL1,
        'INTO OUTFILE "' + Arq + '"',
        '']);

      //
  (*
      RE1.SelAttributes.Color := clBlue;
      RE1.Text := Uppercase(TabWeb) + ' - Preparando para excluir registros duplicados na web...';
      Update;
      Application.ProcessMessages;
  *)
      MyObjects.Informa2(LaAviso1, LaAviso2, True,
        Uppercase(TabWeb) + ' - Preparando para excluir registros duplicados na web...');
  (*
      Qr3.SQL.Clear;
      Qr3.SQL.Add(DELETE_FROM + Lowercase(TabWeb) + ' WHERE ' + Campo + ' in (');
  *)
      SQL3 := Geral.ATS([DELETE_FROM + Lowercase(TabWeb) + ' WHERE ' + Campo + ' in (']);
      txt := '';
      Query.First;
      while not Query.Eof do
      begin
        case DataType of
          mswInteger://1:
            txt := txt + IntToStr(Query.FieldByName(Campo).Value) + ',';
          mswDateTime: //2:
            txt := txt + '"' + Geral.FDT(Query.FieldByName(Campo).Value, 2) + '",';
          else //fftString
            txt := txt + '"' + Query.FieldByName(Campo).Value + '",';
        end;
        Query.Next;
      end;
      if txt <> '' then
      begin
        txt[Length(txt)] := ')';
        //Qr3.SQL.Add(txt);
        SQL3 := SQL3 + Geral.ATS(Txt);
        //
        (*
        RE1.SelAttributes.Color := clBlue;
        RE1.Text := Uppercase(TabWeb) + ' - Excluindo registros duplicados na web...';
        Update;
        Application.ProcessMessages;
        Qr3.ExecSQL;
        *)
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
          Uppercase(TabWeb) + ' - Excluindo registros duplicados na web...');
        UnDmkDAC_PF.ExecutaMySQLQuery0(Qr3, DBWeb, [
        SQL3,
        '']);
      end;
      //
  (*
      RE1.SelAttributes.Color := clBlue;
      RE1.Text := Uppercase(TabWeb) + ' - Importando de arquivo tempor�rio...';
      Update;
      Application.ProcessMessages;
  *)
      MyObjects.Informa2(LaAviso1, LaAviso2, True,
        Uppercase(TabWeb) + ' - Importando de arquivo tempor�rio...');

  (*
      Qr2.SQL.Clear;
      Qr2.SQL.Add('LOAD DATA LOCAL INFILE "' + Arq + '"');
      Qr2.SQL.Add('INTO Table '+TabWeb);
      Qr2.ExecSQL;
  *)
      UnDmkDAC_PF.ExecutaMySQLQuery0(Qr2, DBWeb, [
      'LOAD DATA LOCAL INFILE "' + Arq + '"',
      'INTO Table ' + TabWeb,
      '']);
      //
  (*
      Qr1.SQL.Clear;
      Qr1.SQL.Add('UPDATE '+lowercase(tabloc)+' SET AlterWeb=0');
      Qr1.ExecSQL;
  *)
      UnDmkDAC_PF.ExecutaMySQLQuery0(Qr1, Dmod.MyDB, [
        'UPDATE ' + LowerCase(TabLoc) + ' SET AlterWeb=0',
        '']);

      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');

      if Avisa then
        Geral.MB_Info('Upload web finalizado com sucesso!');
    finally
      Query.Free;
      Qr1.Free;
      Qr2.Free;
      Qr3.Free;
    end;
  except
    ExcluiArquivosDeCampos();
    raise;
  end;
end;

function TFmWSincro2.ExportaTodaTabelaParaWeb2(TabLoc, TabWeb, Campo: String;
  DataType: TFormaFieldTxt; BaseDados: TmySQLDatabase; Abertos: FUploadAbertos;
  LaAviso1, LaAviso2: TLabel; DBWeb: TmySQLDatabase): String;
  function EhTabelaDeLct(NomeTb: String): Boolean;
  var
    nTbI, nTbM: String;
    nTbF: Char;
  begin
    nTbI := Copy(NomeTb, 1, 3);
    nTbM := Copy(NomeTb, 4, Length(NomeTb)-4);
    nTbF := NomeTb[Length(NomeTb)];
    //
    Result := (nTbI = 'lct') and
    (nTbF in ['a','b','d']) and
    (nTbM = Geral.SoNumero_TT(nTbM));
  end;
var
  Arq: String;
  Campos, Prefix: String;
  //
  Query, Qr1, Qr2, Qr3: TmySQLQuery;
  ArqWeb1, ArqWeb2, SQL1: String;
begin
  DefineArqWebPaths(ArqWeb1, ArqWeb2);

  Query := TmySQLQuery.Create(TDataModule(BaseDados.Owner));
  Qr1   := TmySQLQuery.Create(TDataModule(BaseDados.Owner));
  Qr2   := TmySQLQuery.Create(TDataModule(BaseDados.Owner));
  Qr3   := TmySQLQuery.Create(TDataModule(BaseDados.Owner));

  try
(*
    Query.Close;
    Query.SQL.Clear;
    Query.SQL.Add('SELECT * FROM ' + Lowercase(TabLoc));
    Query. O p e n ;
*)
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT * FROM ' + LowerCase(TabLoc),
    '']);
    //
    //
(*
    Qr1.Close;
    Qr1.Database := BaseDados;
*)
    Campos := '';
    Prefix := '';
(*
    RE1.SelAttributes.Color := clBlue;
    RE1.Text := Uppercase(TabWeb) + ' - Obtendo campos da tabela na web...';
    Update;
    Application.ProcessMessages;
*)
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      Uppercase(TabWeb) + ' - Obtendo campos da tabela na web...');
    //
    Campos := ObtemCamposDeTabelaIdentica(
      DBWeb, TabWeb, Prefix);
    //
(*
    RE1.SelAttributes.Color := clBlue;
    RE1.Text := Uppercase(TabWeb) + ' - Exportando para arquivo tempor�rio...';
    Update;
    Application.ProcessMessages;
*)
    Arq := Format(ArqWeb2, [TabWeb, 'txt']);
    if FileExists(Arq) then
      DeleteFile(Arq);
(*
    Qr1.SQL.Clear;
    Qr1.SQL.Add('SELECT '+Campos);
    Qr1.SQL.Add('FROM '+ Lowercase(TabLoc));
*)
    SQL1 := Geral.ATS([
    'SELECT ' + Campos,
    'FROM '+ LowerCase(TabLoc),
    '']);
    case Abertos of
      ulaLct:
      begin
        if (Lowercase(TabLoc) = LAN_CTOS) or EhTabelaDeLct(Lowercase(TabLoc)) then
          SQL1 := SQL1 + Geral.Ats(['WHERE Sit < 2 AND Tipo = 2']); // Somente abertos
      end;
    end;
    SQL1 := SQL1 + Geral.Ats(['INTO OUTFILE "' + Arq + '"']);
    //Qr1.ExecSQL;
    UnDmkDAC_PF.ExecutaMySQLQuery0(Qr1, BaseDados, [
    SQL1,
    '']);
    //
(*
    RE1.SelAttributes.Color := clBlue;
    RE1.Text := Uppercase(TabWeb) + ' - Excluindo registros duplicados na web...';
    Update;
    Application.ProcessMessages;
*)
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      Uppercase(TabWeb) + ' - Excluindo registros duplicados na web...');
(*
    Qr3.SQL.Clear;
    Qr3.SQL.Add(DELETE_FROM + Lowercase(TabWeb));
    Application.ProcessMessages;
    Qr3.ExecSQL;
*)
    UnDmkDAC_PF.ExecutaMySQLQuery0(Qr3, DBWeb, [
    DELETE_FROM + Lowercase(TabWeb),
    '']);
    //
(*
    RE1.SelAttributes.Color := clBlue;
    RE1.Text := Uppercase(TabWeb) + ' - Importando de arquivo tempor�rio...';
    Update;
    Application.ProcessMessages;
*)
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      Uppercase(TabWeb) + ' - Importando de arquivo tempor�rio...');
(*
    Qr2.SQL.Clear;
    Qr2.SQL.Add('LOAD DATA LOCAL INFILE "' + Arq + '"');
    Qr2.SQL.Add('INTO Table '+TabWeb);
    Qr2.ExecSQL;
*)
    UnDmkDAC_PF.ExecutaMySQLQuery0(Qr2, DBWeb, [
    'LOAD DATA LOCAL INFILE "' + Arq + '"',
    'INTO Table ' + TabWeb,
    '']);
    //
(*
    Qr1.SQL.Clear;
    Qr1.SQL.Add('UPDATE '+lowercase(tabloc)+' SET AlterWeb=0');
    Qr1.ExecSQL;
*)
    UnDmkDAC_PF.ExecutaMySQLQuery0(Qr1, Dmod.MyDB, [
    'UPDATE ' + LowerCase(TabLoc) + ' SET AlterWeb=0',
    '']);
  finally
    Query.Free;
    Qr1.Free;
    Qr2.Free;
    Qr3.Free;
  end;
end;

procedure TFmWSincro2.DefineArqWebPaths(var ArqWeb1, ArqWeb2: String);
begin
  ArqWeb1 := 'C:\Dermatek\Web\' + Application.Title + '\Conf';
  ArqWeb2 := 'C:/Dermatek/Web/' + Application.Title + '/Data';
  ForceDirectories(ArqWeb1);
  ForceDirectories(ArqWeb2);
  ArqWeb1 := ArqWeb1 + '\SQL_%s.%s';
  ArqWeb2 := ArqWeb2 + '/SQL_%s.%s';
end;

function TFmWSincro2.ObtemCamposDeTabelaIdentica(DataBase: TmySQLDatabase;
  Tabela, Prefix: String): String;
var
  j: Integer;
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Database.Owner);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Database, [
    'SELECT * FROM ' + LowerCase(Tabela),
    'LIMIT 1',
    '']);
    //
    Result := ' ' + Prefix+Qry.Fields[0].FieldName + sLineBreak;
    for j := 1 to Qry.Fields.Count-1 do
      Result := Result + ', ' + Prefix+Qry.Fields[j].FieldName + sLineBreak;
  finally
    Qry.Free;
  end;
end;
///////////////////////////////////////////////////// Copiado do DmkDAC_PF - FIM
}

end.

