object FmWPrOpcoes: TFmWPrOpcoes
  Left = 339
  Top = 185
  Caption = 'WEB-PROTO-002 :: Op'#231#245'es de Protocolos WEB'
  ClientHeight = 315
  ClientWidth = 511
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 267
    Width = 511
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 18
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 399
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 5
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 511
    Height = 219
    Align = alClient
    TabOrder = 0
    object Label7: TLabel
      Left = 18
      Top = 167
      Width = 77
      Height = 13
      Caption = 'Expira em: (dias)'
    end
    object Label16: TLabel
      Left = 123
      Top = 187
      Width = 206
      Height = 13
      Caption = 'Quantidade de dias para expirar o protocolo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object GroupBox1: TGroupBox
      Left = 10
      Top = 4
      Width = 490
      Height = 156
      Caption = 'Protocolos para'
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 18
        Width = 138
        Height = 13
        BiDiMode = bdLeftToRight
        Caption = 'Edi'#231#227'o de cadastro na WEB:'
        ParentBiDiMode = False
      end
      object Label2: TLabel
        Left = 8
        Top = 62
        Width = 162
        Height = 13
        BiDiMode = bdLeftToRight
        Caption = 'Download de arquivos na internet:'
        ParentBiDiMode = False
      end
      object Label3: TLabel
        Left = 8
        Top = 108
        Width = 153
        Height = 13
        BiDiMode = bdLeftToRight
        Caption = 'Ativa'#231#227'o de aplicativos na WEB'
        ParentBiDiMode = False
      end
      object EdProAltPas: TdmkEditCB
        Left = 8
        Top = 34
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBProAltPas
        IgnoraDBLookupComboBox = False
      end
      object CBProAltPas: TdmkDBLookupComboBox
        Left = 64
        Top = 34
        Width = 420
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsProAltPas
        TabOrder = 1
        dmkEditCB = EdProAltPas
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdProDowArq: TdmkEditCB
        Left = 8
        Top = 78
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBProDowArq
        IgnoraDBLookupComboBox = False
      end
      object CBProDowArq: TdmkDBLookupComboBox
        Left = 64
        Top = 78
        Width = 420
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsProDowArq
        TabOrder = 3
        dmkEditCB = EdProDowArq
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdProAtivApl: TdmkEditCB
        Left = 8
        Top = 124
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBProAtivApl
        IgnoraDBLookupComboBox = False
      end
      object CBProAtivApl: TdmkDBLookupComboBox
        Left = 64
        Top = 124
        Width = 420
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsProAtivApl
        TabOrder = 5
        dmkEditCB = EdProAtivApl
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object EdDiasExp: TdmkEdit
      Left = 18
      Top = 183
      Width = 100
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 511
    Height = 48
    Align = alTop
    Caption = 'Op'#231#245'es de Protocolos WEB'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 509
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 0
    end
  end
  object QrProAltPas: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM protocolos'
      'ORDER BY Nome')
    Left = 12
    Top = 10
    object QrProAltPasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProAltPasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsProAltPas: TDataSource
    DataSet = QrProAltPas
    Left = 40
    Top = 10
  end
  object QrProDowArq: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM protocolos'
      'ORDER BY Nome')
    Left = 68
    Top = 10
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsProDowArq: TDataSource
    DataSet = QrProDowArq
    Left = 96
    Top = 10
  end
  object QrProAtivApl: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM protocolos'
      'ORDER BY Nome')
    Left = 124
    Top = 10
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsProAtivApl: TDataSource
    DataSet = QrProAtivApl
    Left = 152
    Top = 10
  end
  object QrWPrOpcoes: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * '
      'FROM wcontrol')
    Left = 436
    Top = 8
    object QrWPrOpcoesProAltPas: TIntegerField
      FieldName = 'ProAltPas'
    end
    object QrWPrOpcoesProAtivApl: TIntegerField
      FieldName = 'ProAtivApl'
    end
    object QrWPrOpcoesProDowArq: TIntegerField
      FieldName = 'ProDowArq'
    end
  end
  object DsWPrOpcoes: TDataSource
    DataSet = QrWPrOpcoes
    Left = 464
    Top = 8
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 300
    Top = 16
  end
end
