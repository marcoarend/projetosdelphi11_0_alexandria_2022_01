unit UnDmkWeb_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, Menus, UnDmkEnums, mySQLDbTables, Vcl.ComCtrls,
  Vcl.Forms, UnProjGroup_Consts;

type
  TUnDmkWeb_Jan = class(TObject)
  private
    { Private declarations }
    procedure ImportaEntidadesDaWeb(DB, DBn: TmySQLDatabase; Progress: TProgressBar);
  public
    { Public declarations }
    procedure MostraWOpcoes(DBn: TmySQLDatabase);
    (*
    procedure MostraWSincro(DMKID_APP, VersaoApp: Integer; Modulos: String;
              QryWebParams: TMySQLQuery; DBn, DB: TmySQLDatabase;
              DtaSincro: TDateTime; MeuDB, AllID_DB_NOME: String;
              Progress: TProgressBar; TabelasSel: array of String;
              VERIFI_DB_CANCEL: Boolean); deprecated; // use DmkWeb2_Jan.MostraWSincro
    *)
    procedure MostraVerifiDBi(QueryOpcoesGerl: TMySQLQuery; DBn: TmySQLDatabase;
              VERIFI_DB_CANCEL: Boolean);
  end;

var
  DmkWeb_Jan: TUnDmkWeb_Jan;


implementation

uses ModuleGeral, MyDBCheck, VerifiDBi, (*WSincro2, *)UnDmkWeb, DmkGeral, DmkDAC_PF,
  UMySQLModule, WOpcoes2, MyListas;

{ TUnALL_Jan }

procedure TUnDmkWeb_Jan.MostraVerifiDBi(QueryOpcoesGerl: TMySQLQuery;
  DBn: TmySQLDatabase; VERIFI_DB_CANCEL: Boolean);
begin
  if DmkWeb.ConexaoRemota(DBn, QueryOpcoesGerl, 3) then
  begin
    if not VERIFI_DB_CANCEL then
    begin
      Application.CreateForm(TFmVerifiDBi, FmVerifiDBi);
      FmVerifiDBi.ShowModal;
      FmVerifiDBi.Destroy;
    end;
  end;
end;

procedure TUnDmkWeb_Jan.MostraWOpcoes(DBn: TmySQLDatabase);
var
  Msg: String;
begin
  //Deve ser liberado pois nesta janela � configurado o banco de dados.
  if DmkWeb.VerificaSeModuloWebEstaAtivo(CO_DMKID_APP,
    DModG.QrMasterHabilModulos.Value, DBn, Msg) then
  begin
    if DBCheck.CriaFm(TFmWOpcoes2, FmWOpcoes2, afmoNegarComAviso) then
    begin
      FmWOpcoes2.ShowModal;
      FmWOpcoes2.Destroy;
    end;
  end else
    Geral.MB_Aviso(Msg);
end;

(*
procedure TUnDmkWeb_Jan.MostraWSincro(DMKID_APP, VersaoApp: Integer;
  Modulos: String; QryWebParams: TMySQLQuery; DBn, DB: TmySQLDatabase;
  DtaSincro: TDateTime; MeuDB, AllID_DB_NOME: String; Progress: TProgressBar;
  TabelasSel: array of String; VERIFI_DB_CANCEL: Boolean);
var
  Qry: TmySQLQuery;
  i: Integer;
  Msg, Tabela: String;
  TbsSel: TStringList;
  DtaUltimaSincro: TDateTime;
begin
  UMyMod.AbreQuery(QryWebParams, DB);
  //
  if not (DMKID_APP in [4, 17, 18, 24, 43]) then
  begin
    Geral.MB_Aviso('Este aplicativo n�o tem suporte a sincronismo WEB!');
    Exit;
  end;
  //
  if DmkWeb.VerificaSeModuloWebEstaAtivo(DMKID_APP, Modulos, DBn, Msg) then
  begin
    if Geral.IMV(QryWebParams.FieldByName('Web_DBVersao').AsString) < VersaoApp then
    begin
      if Geral.MB_Pergunta('A vers�o do banco de dados WEB est� desatualizada!' + sLineBreak +
        'Deseja realizar uma verifica��o de banco dados agora?') = ID_YES then
      begin
        MostraVerifiDBi(QryWebParams, DBn, VERIFI_DB_CANCEL);
        Exit;
      end else
        Exit;
    end;
    if DmkWeb.VerificaSeExisteSincroEmExecucao(DBn, DtaUltimaSincro) then
    begin
      if Geral.MB_Pergunta('Existe um sincroniso em execu��o! ' + sLineBreak +
       'Deseja inciar outro?') <> ID_YES
      then
        Exit;
    end;
    if DmkWeb.ConexaoRemota(DBn, QryWebParams, 1) then
    begin
      if (DMKID_APP = 24) and (Progress <> nil) then //Bugstrol
        ImportaEntidadesDaWeb(DB, DBn, Progress);
      //
      if DBCheck.CriaFm(TFmWSincro2, FmWSincro2, afmoNegarComAviso) then
      begin
        with FmWSincro2 do
        begin
          ListasClear;
          //
          case DMKID_APP of
            4, 43: //Syndi2
            begin
              ListasAdd('Cadastros de clientes na web' , 'wclients'   , 'wclients'     , 'User_ID'   , 1);
              ListasAdd('Cadastros de usu�rios na web' , 'users'      , 'users'        , 'User_ID'   , 1);
              ListasAdd('Grupos de cond�minos'         , 'condgri'    , 'condgri'      , 'Codigo'    , 1);
              ListasAdd('Itens de grupos de cond�minos', 'condgriimv' , 'condgriimv'   , 'Conttrole' , 1);
              //
              ListasAdd('Cadastros de condom�nios'     , 'cond'       , 'cond'         , 'Codigo'    , 1);
              ListasAdd('Cadastros de cond�minos'      , 'condimov'   , 'condimov'     , 'Conta'     , 1);
              ListasAdd('Cadastros de entidades'       , 'entidades'  , 'entidades'    , 'Codigo'    , 1);
              ListasAdd('Cadastros de empresas'        , 'enticliint' , 'enticliint'   , 'CodCliInt' , 1);
              ListasAdd('Cadastros de feriados'        , 'feriados'   , 'feriados'     , 'Data'      , 2);
              ListasAdd('Op��es gerais do aplicativo'  , 'opcoesgerl' , 'opcoesgerl'   , 'Codigo'    , 1);
              ListasAdd('Configura��es de boletos'     , 'cnab_cfg'   , 'cnab_cfg'     , 'Codigo'      , 1);
              //
              ListasAdd('Protocolos'                   , 'protocolos' , 'protocolos'   , 'Codigo'    , 1);
              ListasAdd('Protocolos itens'             , 'protpakits' , 'protpakits'   , 'Conta'     , 1);
              //
              ListasAdd('Carteiras financeiras'        , 'carteiras'  , 'carteiras'    , 'Codigo'    , 1);
              ListasAdd('Municipios'                   , AllID_DB_NOME + '.dtb_munici' , 'dtb_munici' , 'Codigo'    , 1);
              ListasAdd('Paises'                       , AllID_DB_NOME + '.bacen_pais' , 'bacen_pais' , 'Codigo'    , 1);
              // Controle e sub?
              //ListasAdd('Lan�amentos financeiros'      ,  LAN_CTOS    , LAN_CTOS     , 'Controle'  , 1);
              Qry := TmySQLQuery.Create(TDataModule(DB.Owner));
              try
                Qry.Database := DB;
                Qry.SQL.Clear;
                Qry.SQL.Add('SHOW TABLES FROM ' + LowerCase(MeuDB));
                Qry.SQL.Add('LIKE "lct%a"');
                Qry.Open;
                Qry.First;
                while not Qry.Eof do
                begin
                  Tabela := Qry.Fields[0].AsString;
                  ListasAdd('Lan�amentos financeiros'  ,  Tabela      , Tabela       , 'Controle'  , 1);
                  Qry.Next;
                end;
                Qry.Close;
              finally
                Qry.Free;
              end;
              // Periodos
              Qry := TmySQLQuery.Create(TDataModule(DB.Owner));
              try
                Qry.Database := DB;
                Qry.SQL.Clear;
                Qry.SQL.Add('SHOW TABLES FROM ' + LowerCase(MeuDB));
                Qry.SQL.Add('LIKE "prv%a"');
                Qry.Open;
                Qry.First;
                while not Qry.Eof do
                begin
                  Tabela := Qry.Fields[0].AsString;
                  ListasAdd('Per�odos de emiss�o de boletos'  ,  Tabela      , Tabela       , 'Codigo'  , 1);
                  Qry.Next;
                end;
                Qry.Close;
              finally
                Qry.Free;
              end;
              //
              // Consumos
              Qry := TmySQLQuery.Create(TDataModule(DB.Owner));
              try
                Qry.Database := DB;
                Qry.SQL.Clear;
                Qry.SQL.Add('SHOW TABLES FROM ' + LowerCase(MeuDB));
                Qry.SQL.Add('LIKE "cns%a"');
                Qry.Open;
                Qry.First;
                while not Qry.Eof do
                begin
                  Tabela := Qry.Fields[0].AsString;
                  ListasAdd('Consumos por per�odo'  ,  Tabela      , Tabela       , 'Controle'  , 1);
                  Qry.Next;
                end;
                Qry.Close;
              finally
                Qry.Free;
              end;
              //
              // Arrecada��es
              Qry := TmySQLQuery.Create(TDataModule(DB.Owner));
              try
                Qry.Database := DB;
                Qry.SQL.Clear;
                Qry.SQL.Add('SHOW TABLES FROM ' + LowerCase(MeuDB));
                Qry.SQL.Add('LIKE "ari%a"');
                Qry.Open;
                Qry.First;
                while not Qry.Eof do
                begin
                  Tabela := Qry.Fields[0].AsString;
                  ListasAdd('Arrecada��es por per�odo'  ,  Tabela      , Tabela       , 'Controle'  , 1);
                  Qry.Next;
                end;
                Qry.Close;
              finally
                Qry.Free;
              end;
            end;
            17: //DControl
            begin
              ListasAdd('Ajudas'                            , 'helpcab'                     , 'helpcab'    , 'Codigo'    , 1);
              ListasAdd('Ajudas - Op��es'                   , 'helpopc'                     , 'helpopc'    , 'Codigo'    , 1);
              ListasAdd('Ajudas - T�picos'                  , 'helptopic'                   , 'helptopic'  , 'Controle'  , 1);
              ListasAdd('Ajudas - FAQ'                      , 'helpfaq'                     , 'helpfaq'    , 'Codigo'    , 1);
              ListasAdd('Ajudas - Restri��es'               , 'helprestr'                   , 'helprestr'  , 'Codigo'    , 1);
              ListasAdd('Ajudas - Tipos de Restri��es'      , 'helprestip'                  , 'helprestip' , 'Codigo'    , 1);
              ListasAdd('Arrecada��es de bloquetos'         , 'arreits'                     , 'arreits'    , 'Controle'  , 1);
              ListasAdd('Cadastros aplicativos'             , 'aplicativos'                 , 'aplicativos', 'Codigo'    , 1);
              ListasAdd('Cadastros aplicativos / m�dulos'   , 'aplicmodul'                  , 'aplicmodul' , 'Controle'  , 1);
              ListasAdd('Cadastros clientes / aplicativos'  , 'cliaplic'                    , 'cliaplic'   , 'Controle'  , 1);
              ListasAdd('Cadastros clientes / m�dulos'      , 'cliaplicmo'                  , 'cliaplicmo' , 'Conta'     , 1);
              ListasAdd('Cadastros de clientes'             , 'clientes'                    , 'clientes'   , 'Codigo'    , 1);
              ListasAdd('Cadastros de empresas'             , 'enticliint'                  , 'enticliint' , 'CodCliInt' , 1);
              ListasAdd('Cadastros de feriados'             , 'feriados'                    , 'feriados'   , 'Data'      , 2);
              ListasAdd('Cadastros entidades'               , 'entidades'                   , 'entidades'  , 'Codigo'    , 1);
              ListasAdd('Cadastros entidades / Contatos'    , 'enticontat'                  , 'enticontat' , 'Controle'  , 1);
              ListasAdd('Cadastros entidades / Cont. Atrel.', 'enticonent'                  , 'enticonent' , 'Controle'  , 1);
              ListasAdd('Cadastros entidades / E-mails'     , 'entimail'                    , 'entimail'   , 'Conta'     , 1);
              ListasAdd('Cadastros entidades / Telefones'   , 'entitel'                     , 'entitel'    , 'Conta'     , 1);
              ListasAdd('Carteiras financeiras'             , 'carteiras'                   , 'carteiras'  , 'Codigo'    , 1);
              ListasAdd('Configura��o de E-mails e Contas'  , 'emailconta'                  , 'emailconta' , 'Codigo'    , 1);
              ListasAdd('Configura��es CNAB'                , 'cnab_cfg'                    , 'cnab_cfg'   , 'Codigo'    , 1);
              ListasAdd('Configura��es MySQL'               , 'mysqlconf'                   , 'mysqlconf'  , 'Codigo'    , 1);
              ListasAdd('Lista de logradouros'              , 'listalograd'                 , 'listalograd', 'Codigo'    , 1);
              ListasAdd('Modulos'                           , 'modulos'                     , 'modulos'    , 'Codigo'    , 1);
              ListasAdd('Municipios'                        , AllID_DB_NOME + '.dtb_munici' , 'dtb_munici' , 'Codigo'    , 1);
              ListasAdd('NFS-e'                             , 'nfsenfscab'                  , 'nfsenfscab' , 'NfsNumero' , 1);
              ListasAdd('Op��es de bloquetos'               , 'bloopcoes'                   , 'bloopcoes'  , 'Codigo'    , 1);
              ListasAdd('Or�amentos de bloquetos'           , 'prev'                        , 'prev'       , 'Codigo'    , 1);
              ListasAdd('Paises'                            , AllID_DB_NOME + '.bacen_pais' , 'bacen_pais' , 'Codigo'    , 1);
              ListasAdd('Pre e-mails'                       , 'preemail'                    , 'preemail'   , 'Codigo'    , 1);
              ListasAdd('Pre e-mails - Texto'               , 'preemmsg'                    , 'preemmsg'   , 'Controle'  , 1);
              ListasAdd('Protocolos'                        , 'protocolos'                  , 'protocolos' , 'Codigo'    , 1);
              ListasAdd('Protocolos itens'                  , 'protpakits'                  , 'protpakits' , 'Conta'     , 1);
              ListasAdd('Tarifas de Servi�os'               , 'srvtarifa'                   , 'srvtarifa'  , 'Codigo'    , 1);
              ListasAdd('Tipos de telefones e e-mails'      , 'entitipcto'                  , 'entitipcto' , 'Codigo'    , 1);
              ListasAdd('UFs'                               , 'ufs'                         , 'ufs'        , 'Codigo'    , 1);
              Qry := TmySQLQuery.Create(TDataModule(DB.Owner));
              try
                Qry.Database := DB;
                Qry.SQL.Clear;
                Qry.SQL.Add('SHOW TABLES FROM ' + LowerCase(MeuDB));
                Qry.SQL.Add('LIKE "lct%a"');
                Qry.Open;
                Qry.First;
                while not Qry.Eof do
                begin
                  Tabela := Qry.Fields[0].AsString;
                  ListasAdd('Lan�amentos financeiros'  ,  Tabela      , Tabela       , 'Controle'  , 1);
                  Qry.Next;
                end;
                Qry.Close;
              finally
                Qry.Free;
              end;
            end;
            18: //Exult
            begin
              ListasAdd('Cadastros de entidades', 'entidades' , 'entidades' , 'Codigo'  , 1);
              ListasAdd('Entidades'             , 'entidades' , 'entidades' , 'Codigo'  , 1);
              ListasAdd('Pr� e-mails'           , 'preemail'  , 'preemail'  , 'Codigo'  , 1);
              ListasAdd('Pre e-mails - Texto'   , 'preemmsg'  , 'preemmsg'  , 'Controle', 1);
              ListasAdd('Protocolos'            , 'protocolos', 'protocolos', 'Codigo'  , 1);
            end;
            24: //Bugstrol
            begin
              // O sincronismo destas tabelas � feito na orderm de servi�o
              // ListasAdd('Cabe�alhos de OSs'               , 'OSCab'      , 'oswcab'     , 'Codigo'    , 1);
              // ListasAdd('Monitoramentos de PMVs'          , 'OSPIPMon'   , 'oswpipmon'  , 'Controle'  , 1);
              //
              ListasAdd('Perguntas de Monitoram. de PMVs' , 'OSPipIts'   , 'ospipits'   , 'Conta'     , 1);
              ListasAdd('Produtos em PMVs de OSs'         , 'OSPipItsPr' , 'ospipitspr' , 'IDIts'     , 1);
              ListasAdd('Agentes operacionais'            , 'OSAge'      , 'osage'      , 'Controle'  , 1);
              //
              ListasAdd('Arquivos SVG Croquis vetorizados'  , 'siapimasvg'  , 'siapimasvg'  , 'Controle'  , 1);
              ListasAdd('Atributos de perguntas'            , 'PrgAtrCad'   , 'prgatrcad'   , 'Codigo'    , 1);
              ListasAdd('Boletos'                           , 'arreits'     , 'arreits'     , 'Controle'  , 1);
              ListasAdd('Cadastros de itens de perguntas'   , 'PrgLstIts'   , 'prglstits'   , 'Controle'  , 1);
              ListasAdd('Cadastros de Perguntas'            , 'PrgCadPrg'   , 'prgcadprg'   , 'Codigo'    , 1);
              ListasAdd('Cadastros de PMVs'                 , 'pipcad'      , 'pipcad'      , 'Codigo'    , 1);
              ListasAdd('Cadastros de empresas'             , 'enticliint'  , 'enticliint'  , 'CodCliInt' , 1);
              ListasAdd('Cadastros de entidades'            , 'entidades'   , 'entidades'   , 'Codigo'    , 1);
              ListasAdd('Cadastros entidades / Cargos'      , 'enticargos'  , 'enticargos'  , 'Codigo'    , 1);
              ListasAdd('Cadastros entidades / Contatos'    , 'enticontat'  , 'enticontat'  , 'Controle'  , 1);
              ListasAdd('Cadastros entidades / Cont. Atrel.', 'enticonent'  , 'enticonent'  , 'Controle'  , 1);
              ListasAdd('Cadastros entidades / E-mails'     , 'entimail'    , 'entimail'    , 'Conta'     , 1);
              ListasAdd('Cadastros entidades / Telefones'   , 'entitel'     , 'entitel'     , 'Conta'     , 1);
              ListasAdd('Cadastros equipam. monitoramento'  , 'grag1eqmo'   , 'grag1eqmo'   , 'Nivel1'    , 1);
              ListasAdd('Configura��o CNAB'                 , 'cnab_cfg'    , 'cnab_cfg'    , 'Codigo'    , 1);
              ListasAdd('Configura��o de E-mails e Contas'  , 'emailconta'  , 'emailconta'  , 'Codigo'    , 1);
              ListasAdd('Configura��es Gerais'              , 'opcoesgerl'  , 'opcoesgerl'  , 'Codigo'    , 1);
              ListasAdd('Descri��o de perguntas bin�rias'   , 'PrgBinCad'   , 'prgbincad'   , 'Codigo'    , 1);
              ListasAdd('Im�veis, m�veis ou autom�veis'     , 'siapimacad'  , 'siapimacad'  , 'Codigo'    , 1);
              ListasAdd('Itens de atributos de perguntas'   , 'PrgAtrIts'   , 'prgatrits'   , 'Controle'  , 1);
              ListasAdd('Laudos de Produtos'                , 'GraGru1Lau'  , 'gragruwlau'  , 'Controle'  , 1);
              ListasAdd('Lista de logradouros'              , 'listalograd' , 'listalograd' , 'Codigo'    , 1);
              ListasAdd('Lotes de Produtos'                 , 'GraGru1Lot'  , 'gragruwlot'  , 'Controle'  , 1);
              ListasAdd('Municipios'                        , AllID_DB_NOME + '.dtb_munici' , 'dtb_munici' , 'Codigo'    , 1);
              ListasAdd('NFS-e'                             , 'nfsenfscab'  , 'nfsenfscab'  , 'NfsNumero' , 1);
              ListasAdd('Ordens de Servi�o'                 , 'oscab'       , 'oscab'       , 'Codigo'    , 1);
              ListasAdd('Paises'                            , AllID_DB_NOME + '.bacen_pais' , 'bacen_pais' , 'Codigo'    , 1);
              ListasAdd('Pre e-mails'                       , 'preemail'    , 'preemail'    , 'Codigo'    , 1);
              ListasAdd('Pre e-mails - Texto'               , 'preemmsg'    , 'preemmsg'    , 'Controle'  , 1);
              ListasAdd('Protocolos'                        , 'protocolos'  , 'protocolos'  , 'Codigo'    , 1);
              ListasAdd('Protocolos itens'                  , 'protpakits'  , 'protpakits'  , 'Conta'     , 1);
              ListasAdd('Terrenos'                          , 'siaptercad'  , 'siaptercad'  , 'Codigo'    , 1);
              ListasAdd('Tipos de telefones e e-mails'      , 'entitipcto'  , 'entitipcto'  , 'Codigo'    , 1);
              ListasAdd('Op��es de bloquetos'               , 'bloopcoes'                   , 'bloopcoes'  , 'Codigo'    , 1);
              //
              Qry := TmySQLQuery.Create(TDataModule(DB.Owner));
              try
                Qry.Database := DB;
                Qry.SQL.Clear;
                Qry.SQL.Add('SHOW TABLES FROM ' + LowerCase(MeuDB));
                Qry.SQL.Add('LIKE "lct%a"');
                Qry.Open;
                Qry.First;
                while not Qry.Eof do
                begin
                  Tabela := Qry.Fields[0].AsString;
                  ListasAdd('Lan�amentos financeiros'  ,  Tabela      , Tabela       , 'Controle'  , 1);
                  Qry.Next;
                end;
                Qry.Close;
              finally
                Qry.Free;
              end;
            end;
          end;
          TbsSel := TStringList.Create;
          try
            for i := 0 to Length(TabelasSel) - 1 do
            begin
              Tabela := TabelasSel[i];

              if Tabela <> '' then
                TbsSel.Add(Tabela);
            end;
          finally
            FTabelasSel := TbsSel;
            //TbsSel.Free;
          end;
          FDtaUltimaSincro := DtaUltimaSincro;
          ListaCheckAll;
          ShowModal;
          Destroy;
        end;
      end;
    end else
      Geral.MB_Aviso('O sincronismo deve ser feito somente no servidor!' +
        sLineBreak + '�ltimo sincronismo em: ' +
        Geral.FDT(DtaSincro, 0));
  end else
    Geral.MB_Aviso(Msg);
end;
*)

procedure TUnDmkWeb_Jan.ImportaEntidadesDaWeb(DB, DBn: TmySQLDatabase;
  Progress: TProgressBar);

  function VerificaSeEntidadeExiste(Tipo: Integer; CNPJ, CPF: String): Integer;
  var
    Qry: TmySQLQuery;
    SQL: String;
  begin
    Result := 0;
    Qry    := TmySQLQuery.Create(TDataModule(DB.Owner));
    try
      if Tipo = 1 then //CPF
        SQL := 'WHERE CPF = "' + CPF + '"'
      else
        SQL := 'WHERE CNPJ = "' + CNPJ + '"';
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, DB, [
        'SELECT Codigo ',
        'FROM entidades ',
        SQL,
        '']);
      if Qry.RecordCount > 0 then
        Result := Qry.FieldByName('Codigo').AsInteger;
    finally
      Qry.Free;
    end;
  end;

var
  QueryWeb, QueryUpd: TmySQLQuery;
  Codigo, CodigoAnt, Tipo: Integer;
  CNPJ, CPF: String;
begin
  QueryWeb := TmySQLQuery.Create(TDataModule(DBn.Owner));
  QueryUpd := TmySQLQuery.Create(TDataModule(DBn.Owner));
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QueryWeb, DBn, [
      'SELECT * ',
      'FROM wentidades ',
      'WHERE Codigo > 0 ',
      '']);
    //
    if (QueryWeb.RecordCount > 0) then
    begin
      if Progress <> nil then
      begin
        Progress.Position := 0;
        Progress.Max      := QueryWeb.RecordCount;
      end;
      //
      while not QueryWeb.Eof do
      begin
        CodigoAnt := QueryWeb.FieldByName('Codigo').AsInteger;
        Tipo      := QueryWeb.FieldByName('Tipo').AsInteger;
        CNPJ      := QueryWeb.FieldByName('CNPJ').AsString;
        CPF       := QueryWeb.FieldByName('CPF').AsString;
        //
        Codigo := VerificaSeEntidadeExiste(Tipo, CNPJ, CPF);
        //
        if Codigo = 0 then
        begin
          Codigo := UMyMod.BuscaEmLivreY(DB, 'Livres', 'Controle',
                      'Entidades', 'Entidades', 'Codigo');
          //
          if not DmkWeb.ImportaRegistroDeTabelaIdenticaWeb(QueryUpd,
            QueryWeb, 'entidades', ['codigo', 'codusu'], [Codigo, Codigo]) then
          begin
            Geral.MB_Aviso('Falha ao importar registro da web!');
            Exit;
          end;
        end;
        UnDmkDAC_PF.ExecutaMySQLQuery0(QueryUpd, DBn, [
          'UPDATE wusers SET Entidade="' + Geral.FF0(Codigo) + '", ',
          'Cliente="' + Geral.FF0(Codigo) + '", AlterWeb = 1 ',
          'WHERE Entidade="' + Geral.FF0(CodigoAnt) + '" AND AlterWeb = -1',
          '']);
        //
        //Exclui entidade web
        UnDmkDAC_PF.ExecutaMySQLQuery0(QueryUpd, DBn, [
          'DELETE FROM wentidades WHERE Codigo=' + Geral.FF0(CodigoAnt),
          '']);
        //
        if Progress <> nil then
        begin
          Progress.Position :=  Progress.Position + 1;
          Progress.Update;
          Application.ProcessMessages;
        end;
        //
        QueryWeb.Next;
      end;
      Progress.Position := 0;
    end;
    QueryWeb.Free;
    QueryUpd.Free;
  except
    Geral.MB_Aviso('Falha ao importar registros da WEB!');
  end;
end;

end.
