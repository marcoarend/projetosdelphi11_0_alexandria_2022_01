object FmWOpcoes: TFmWOpcoes
  Left = 339
  Top = 185
  Caption = 'WEB-OPCOE-001 :: Op'#231#245'es WEB'
  ClientHeight = 554
  ClientWidth = 713
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 713
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 832
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 773
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 199
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Op'#231#245'es WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 199
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Op'#231#245'es WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 199
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Op'#231#245'es WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 47
    Width = 713
    Height = 395
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 713
      Height = 395
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 891
      ExplicitHeight = 494
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 713
        Height = 395
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 891
        ExplicitHeight = 494
        object PageControl1: TPageControl
          Left = 2
          Top = 15
          Width = 709
          Height = 378
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TSConexoes
          Align = alClient
          TabHeight = 25
          TabOrder = 0
          ExplicitTop = 18
          ExplicitHeight = 375
          object TSConexoes: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Conex'#245'es'
            object GBDataBase: TGroupBox
              Left = 11
              Top = 20
              Width = 571
              Height = 300
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Dados do banco de dados'
              TabOrder = 0
              object Label32: TLabel
                Left = 10
                Top = 22
                Width = 42
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Servidor:'
              end
              object Label33: TLabel
                Left = 10
                Top = 74
                Width = 39
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Usu'#225'rio:'
              end
              object Label34: TLabel
                Left = 10
                Top = 129
                Width = 34
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Senha:'
              end
              object Label35: TLabel
                Left = 10
                Top = 182
                Width = 81
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Banco de dados:'
              end
              object Label4: TLabel
                Left = 10
                Top = 233
                Width = 44
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'URL site:'
              end
              object Label18: TLabel
                Left = 188
                Top = 182
                Width = 28
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Porta:'
              end
              object Label19: TLabel
                Left = 60
                Top = 279
                Width = 193
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Exemplo: http://www.seusite.com'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = cl3DDkShadow
                Font.Height = -15
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object EdWeb_Host: TdmkEdit
                Left = 10
                Top = 42
                Width = 247
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdWeb_User: TdmkEdit
                Left = 10
                Top = 94
                Width = 247
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdWeb_Pwd: TdmkEdit
                Left = 10
                Top = 148
                Width = 247
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Font.Charset = SYMBOL_CHARSET
                Font.Color = clWindowText
                Font.Height = -15
                Font.Name = 'Wingdings'
                Font.Style = []
                ParentFont = False
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                PasswordChar = 'l'
              end
              object EdWeb_DB: TdmkEdit
                Left = 10
                Top = 202
                Width = 172
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdWeb_MyURL: TdmkEdit
                Left = 10
                Top = 252
                Width = 247
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object RGWeb_MySQL: TRadioGroup
                Left = 265
                Top = 22
                Width = 289
                Height = 150
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' MySQL no meu site: '
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o possuo, ou n'#227'o tenho acesso direto.'
                  'Conectar somente quando eu necessitar.'
                  'Conectar automaticamente ao iniciar.')
                TabOrder = 6
              end
              object BtImporta: TBitBtn
                Tag = 39
                Left = 265
                Top = 178
                Width = 247
                Height = 50
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Importar Config de *.llm'
                NumGlyphs = 2
                TabOrder = 7
                OnClick = BtImportaClick
              end
              object BtTestar: TBitBtn
                Tag = 10001
                Left = 265
                Top = 229
                Width = 247
                Height = 49
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '&Testar configura'#231#227'o'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -15
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 8
                OnClick = BtTestarClick
              end
              object EdWeb_Porta: TdmkEdit
                Left = 188
                Top = 202
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
            object GBFTP: TGroupBox
              Left = 590
              Top = 20
              Width = 270
              Height = 395
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Dados do servidor FTP'
              TabOrder = 1
              object Label14: TLabel
                Left = 10
                Top = 25
                Width = 42
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Servidor:'
              end
              object Label15: TLabel
                Left = 10
                Top = 76
                Width = 39
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Usu'#225'rio:'
              end
              object Label16: TLabel
                Left = 10
                Top = 128
                Width = 34
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Senha:'
              end
              object Label17: TLabel
                Left = 10
                Top = 177
                Width = 49
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Pasta raiz:'
              end
              object EdWeb_FTPh: TdmkEdit
                Left = 10
                Top = 44
                Width = 247
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdWeb_FTPu: TdmkEdit
                Left = 10
                Top = 96
                Width = 247
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdWeb_Raiz: TdmkEdit
                Left = 10
                Top = 197
                Width = 247
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdWeb_FTPs: TdmkEdit
                Left = 10
                Top = 148
                Width = 247
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Font.Charset = SYMBOL_CHARSET
                Font.Color = clWindowText
                Font.Height = -15
                Font.Name = 'Wingdings'
                Font.Style = []
                ParentFont = False
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                PasswordChar = 'l'
              end
              object BitBtn2: TBitBtn
                Tag = 39
                Left = 10
                Top = 260
                Width = 247
                Height = 49
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Importar Config de *.llm'
                NumGlyphs = 2
                TabOrder = 5
                OnClick = BitBtn2Click
              end
              object BitBtn1: TBitBtn
                Tag = 553
                Left = 10
                Top = 316
                Width = 247
                Height = 50
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '&Testar configura'#231#227'o'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -15
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 6
                OnClick = BitBtn1Click
              end
              object CkWeb_FTPpassivo: TCheckBox
                Left = 10
                Top = 230
                Width = 111
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Modo Passivo'
                TabOrder = 4
              end
            end
            object GroupBox2: TGroupBox
              Left = 11
              Top = 327
              Width = 571
              Height = 88
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Outras configura'#231#245'es'
              TabOrder = 2
              object LaEntidade: TLabel
                Left = 11
                Top = 25
                Width = 29
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Dono:'
              end
              object LAWebId: TLabel
                Left = 431
                Top = 25
                Width = 35
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'WebId:'
              end
              object EdDono: TdmkEditCB
                Left = 10
                Top = 44
                Width = 98
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBDono
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBDono: TdmkDBLookupComboBox
                Left = 111
                Top = 44
                Width = 312
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsEntidades
                TabOrder = 1
                dmkEditCB = EdDono
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdWebId: TdmkEdit
                Left = 431
                Top = 44
                Width = 123
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
          object TabSheet2: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Geral'
            ImageIndex = 2
            object Label8: TLabel
              Left = 489
              Top = 4
              Width = 129
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              BiDiMode = bdLeftToRight
              Caption = 'URL do meu logo na WEB:'
              ParentBiDiMode = False
            end
            object Label9: TLabel
              Left = 489
              Top = 92
              Width = 244
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'String de seguran'#231'a para gera'#231#227'o de senhas WEB:'
            end
            object Label10: TLabel
              Left = 489
              Top = 146
              Width = 155
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Pr'#233' e-mail para contato na WEB:'
            end
            object SpeedButton5: TSpeedButton
              Left = 841
              Top = 166
              Width = 25
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              OnClick = SpeedButton5Click
            end
            object GroupBox3: TGroupBox
              Left = 4
              Top = 4
              Width = 467
              Height = 234
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Perfis padr'#227'o'
              TabOrder = 0
              object LaPerPasAdm: TLabel
                Left = 12
                Top = 25
                Width = 171
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Perfil padr'#227'o para senhas do Tipo 0:'
              end
              object SpeedButton1: TSpeedButton
                Left = 431
                Top = 44
                Width = 26
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '...'
                OnClick = SpeedButton1Click
              end
              object LaPerPasBos: TLabel
                Left = 12
                Top = 74
                Width = 171
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Perfil padr'#227'o para senhas do Tipo 1:'
              end
              object SpeedButton2: TSpeedButton
                Left = 431
                Top = 94
                Width = 26
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '...'
                OnClick = SpeedButton2Click
              end
              object LaPerPasCli: TLabel
                Left = 12
                Top = 123
                Width = 171
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Perfil padr'#227'o para senhas do Tipo 2:'
              end
              object SpeedButton3: TSpeedButton
                Left = 431
                Top = 143
                Width = 26
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '...'
                OnClick = SpeedButton3Click
              end
              object LaPerPasUsu: TLabel
                Left = 12
                Top = 172
                Width = 171
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Perfil padr'#227'o para senhas do Tipo 3:'
              end
              object SpeedButton4: TSpeedButton
                Left = 431
                Top = 192
                Width = 26
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '...'
                OnClick = SpeedButton4Click
              end
              object EdPerPasAdm: TdmkEditCB
                Left = 12
                Top = 44
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBPerPasAdm
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBPerPasAdm: TdmkDBLookupComboBox
                Left = 81
                Top = 44
                Width = 345
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsWPerfil0
                TabOrder = 1
                dmkEditCB = EdPerPasAdm
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdPerPasBos: TdmkEditCB
                Left = 12
                Top = 94
                Width = 69
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBPerPasBos
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBPerPasBos: TdmkDBLookupComboBox
                Left = 81
                Top = 94
                Width = 345
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsWPerfil1
                TabOrder = 3
                dmkEditCB = EdPerPasBos
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdPerPasCli: TdmkEditCB
                Left = 12
                Top = 143
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBPerPasCli
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBPerPasCli: TdmkDBLookupComboBox
                Left = 81
                Top = 143
                Width = 345
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsWPerfil2
                TabOrder = 5
                dmkEditCB = EdPerPasCli
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdPerPasUsu: TdmkEditCB
                Left = 12
                Top = 192
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 6
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBPerPasUsu
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBPerPasUsu: TdmkDBLookupComboBox
                Left = 81
                Top = 192
                Width = 345
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsWPerfil3
                TabOrder = 7
                dmkEditCB = EdPerPasUsu
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
            end
            object GroupBox4: TGroupBox
              Left = 4
              Top = 245
              Width = 467
              Height = 178
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Protocolos padr'#227'o '
              TabOrder = 1
              object Label1: TLabel
                Left = 12
                Top = 25
                Width = 138
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                BiDiMode = bdLeftToRight
                Caption = 'Edi'#231#227'o de cadastro na WEB:'
                ParentBiDiMode = False
              end
              object Label2: TLabel
                Left = 12
                Top = 76
                Width = 162
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                BiDiMode = bdLeftToRight
                Caption = 'Download de arquivos na internet:'
                ParentBiDiMode = False
              end
              object Label7: TLabel
                Left = 12
                Top = 123
                Width = 77
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Expira em: (dias)'
              end
              object Label5: TLabel
                Left = 145
                Top = 146
                Width = 264
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Quantidade de dias para expirar o protocolo'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -15
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object SpeedButton6: TSpeedButton
                Left = 431
                Top = 44
                Width = 26
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '...'
                OnClick = SpeedButton6Click
              end
              object SpeedButton7: TSpeedButton
                Left = 431
                Top = 96
                Width = 26
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '...'
                OnClick = SpeedButton7Click
              end
              object EdProAltPas: TdmkEditCB
                Left = 12
                Top = 44
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBProAltPas
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBProAltPas: TdmkDBLookupComboBox
                Left = 81
                Top = 44
                Width = 345
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsProAltPas
                TabOrder = 1
                dmkEditCB = EdProAltPas
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdProDowArq: TdmkEditCB
                Left = 12
                Top = 96
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBProDowArq
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBProDowArq: TdmkDBLookupComboBox
                Left = 81
                Top = 96
                Width = 345
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsProDowArq
                TabOrder = 3
                dmkEditCB = EdProDowArq
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdDiasExp: TdmkEdit
                Left = 12
                Top = 143
                Width = 123
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
            object MeLogoWeb: TdmkMemo
              Left = 489
              Top = 25
              Width = 377
              Height = 61
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Lines.Strings = (
                'MeLogoWeb')
              TabOrder = 2
              UpdType = utYes
            end
            object EdCrypt: TdmkEdit
              Left = 489
              Top = 112
              Width = 377
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdMailCont: TdmkEditCB
              Left = 489
              Top = 166
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBMailCont
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBMailCont: TdmkDBLookupComboBox
              Left = 558
              Top = 166
              Width = 276
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsMailCont
              TabOrder = 5
              dmkEditCB = EdMailCont
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object CkFacebookLogin: TCheckBox
              Left = 489
              Top = 199
              Width = 246
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Habilitar login atrav'#233's do Facebook'
              TabOrder = 6
            end
          end
          object TabSheet1: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'DControl'
            ImageIndex = 1
            object GBWVersao: TGroupBox
              Left = 12
              Top = 12
              Width = 468
              Height = 127
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Vers'#245'es de aplicativos'
              TabOrder = 0
              object Label3: TLabel
                Left = 12
                Top = 65
                Width = 70
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Diret'#243'rio WEB:'
              end
              object CkAppZipar: TdmkCheckBox
                Left = 12
                Top = 25
                Width = 370
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Zipar o execut'#225'vel do aplicativo antes de disponibiliz'#225'-lo'
                TabOrder = 0
                UpdType = utYes
                ValCheck = '1'
                ValUncheck = '0'
                OldValor = #0
              end
              object EdWebDirApp: TdmkEditCB
                Left = 12
                Top = 85
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBWebDirApp
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBWebDirApp: TdmkDBLookupComboBox
                Left = 81
                Top = 85
                Width = 373
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsAppDirWeb
                TabOrder = 2
                dmkEditCB = EdWebDirApp
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
            end
            object GroupBox5: TGroupBox
              Left = 12
              Top = 148
              Width = 468
              Height = 86
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Protocolos padr'#227'o '
              TabOrder = 1
              object Label6: TLabel
                Left = 12
                Top = 25
                Width = 153
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                BiDiMode = bdLeftToRight
                Caption = 'Ativa'#231#227'o de aplicativos na WEB'
                ParentBiDiMode = False
              end
              object CBProAtivApl: TdmkDBLookupComboBox
                Left = 81
                Top = 44
                Width = 373
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsProAtivApl
                TabOrder = 0
                dmkEditCB = EdProAtivApl
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdProAtivApl: TdmkEditCB
                Left = 12
                Top = 44
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBProAtivApl
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
            end
            object GroupBox8: TGroupBox
              Left = 12
              Top = 241
              Width = 468
              Height = 136
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Termo padr'#227'o para senhas WEB'
              TabOrder = 2
              object Label13: TLabel
                Left = 12
                Top = 25
                Width = 68
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                BiDiMode = bdLeftToRight
                Caption = 'Termo de uso:'
                ParentBiDiMode = False
              end
              object Label23: TLabel
                Left = 12
                Top = 74
                Width = 112
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                BiDiMode = bdLeftToRight
                Caption = 'Pol'#237'tica de privacidade:'
                ParentBiDiMode = False
              end
              object CBTermoBoss: TdmkDBLookupComboBox
                Left = 81
                Top = 44
                Width = 373
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsTermoBoss
                TabOrder = 1
                dmkEditCB = EdTermoBoss
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdTermoBoss: TdmkEditCB
                Left = 12
                Top = 44
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBTermoBoss
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object EdTermoPrivaci: TdmkEditCB
                Left = 12
                Top = 94
                Width = 69
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBTermoPrivaci
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBTermoPrivaci: TdmkDBLookupComboBox
                Left = 81
                Top = 94
                Width = 373
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsTermoPrivaci
                TabOrder = 3
                dmkEditCB = EdTermoPrivaci
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
            end
          end
          object TabSheet3: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Syndi2'
            ImageIndex = 3
            object GroupBox6: TGroupBox
              Left = 12
              Top = 12
              Width = 493
              Height = 86
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Usu'#225'rio administrador'
              TabOrder = 0
              object Label22: TLabel
                Left = 12
                Top = 30
                Width = 111
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Nome do administrador:'
              end
              object Label20: TLabel
                Left = 171
                Top = 30
                Width = 74
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Usu'#225'rio Master:'
              end
              object Label21: TLabel
                Left = 331
                Top = 30
                Width = 69
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Senha Master:'
              end
              object EdNomeAdmi: TdmkEdit
                Left = 12
                Top = 49
                Width = 149
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdUsername: TdmkEdit
                Left = 171
                Top = 49
                Width = 149
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdPassword: TdmkEdit
                Left = 331
                Top = 49
                Width = 149
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Font.Charset = SYMBOL_CHARSET
                Font.Color = clWindowText
                Font.Height = -15
                Font.Name = 'Wingdings'
                Font.Style = []
                ParentFont = False
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                PasswordChar = 'l'
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 442
    Width = 713
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 887
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 486
    Width = 713
    Height = 68
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 711
      Top = 18
      Width = 178
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 709
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object DsAppDirWeb: TDataSource
    DataSet = QrAppDirWeb
    Left = 288
    Top = 13
  end
  object QrAppDirWeb: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ftpwebdir'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 260
    Top = 13
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAppDirWebNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrWControl: TMySQLQuery
    Database = Dmod.MyDB
    Left = 316
    Top = 13
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) Nome'
      'FROM entidades'
      'WHERE Ativo = 1'
      'AND Codigo <> 0'
      'ORDER BY Nome')
    Left = 344
    Top = 13
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 372
    Top = 13
  end
  object QrWPerfil0: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) Nome'
      'FROM entidades'
      'WHERE Ativo = 1'
      'AND Codigo <> 0'
      'ORDER BY Nome')
    Left = 440
    Top = 13
    object QrWPerfil0Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWPerfil0Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsWPerfil0: TDataSource
    DataSet = QrWPerfil0
    Left = 468
    Top = 13
  end
  object DsWPerfil1: TDataSource
    DataSet = QrWPerfil1
    Left = 524
    Top = 13
  end
  object QrWPerfil1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) Nome'
      'FROM entidades'
      'WHERE Ativo = 1'
      'AND Codigo <> 0'
      'ORDER BY Nome')
    Left = 496
    Top = 13
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsWPerfil2: TDataSource
    DataSet = QrWPerfil2
    Left = 580
    Top = 13
  end
  object QrWPerfil2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) Nome'
      'FROM entidades'
      'WHERE Ativo = 1'
      'AND Codigo <> 0'
      'ORDER BY Nome')
    Left = 552
    Top = 13
    object IntegerField4: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsWPerfil3: TDataSource
    DataSet = QrWPerfil3
    Left = 636
    Top = 13
  end
  object QrWPerfil3: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) Nome'
      'FROM entidades'
      'WHERE Ativo = 1'
      'AND Codigo <> 0'
      'ORDER BY Nome')
    Left = 608
    Top = 13
    object IntegerField5: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrProAltPas: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM protocolos'
      'WHERE Ativo = 1'
      'AND Tipo = 2'
      'ORDER BY Nome')
    Left = 468
    Top = 66
    object QrProAltPasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProAltPasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsProAltPas: TDataSource
    DataSet = QrProAltPas
    Left = 496
    Top = 66
  end
  object QrProDowArq: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM protocolos'
      'WHERE Ativo = 1'
      'AND Tipo = 2'
      'ORDER BY Nome')
    Left = 524
    Top = 66
    object IntegerField6: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField4: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsProDowArq: TDataSource
    DataSet = QrProDowArq
    Left = 552
    Top = 66
  end
  object DsProAtivApl: TDataSource
    DataSet = QrProAtivApl
    Left = 608
    Top = 66
  end
  object QrProAtivApl: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM protocolos'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 580
    Top = 66
    object IntegerField7: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField5: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrMailCont: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM preemail'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 636
    Top = 66
    object QrMailContIntegerField8: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMailContNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsMailCont: TDataSource
    DataSet = QrMailCont
    Left = 664
    Top = 66
  end
  object QrSenha: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Username, Password, User_ID'
      'FROM users'
      'WHERE Tipo=9')
    Left = 620
    Top = 188
  end
  object QrTermoBoss: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'#11
      'FROM wtextos'
      'WHERE Publicado = 1'
      'AND Ativo = 1'
      'ORDER BY Nome')
    Left = 460
    Top = 186
    object IntegerField8: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField6: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsTermoBoss: TDataSource
    DataSet = QrTermoBoss
    Left = 488
    Top = 186
  end
  object DsTermoPrivaci: TDataSource
    DataSet = QrTermoPrivaci
    Left = 488
    Top = 258
  end
  object QrTermoPrivaci: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'#11
      'FROM wtextos'
      'WHERE Publicado = 1'
      'AND Ativo = 1'
      'ORDER BY Nome')
    Left = 460
    Top = 258
    object IntegerField9: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField7: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
end
