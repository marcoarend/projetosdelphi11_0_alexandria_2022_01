object FmWOpcoes: TFmWOpcoes
  Left = 339
  Top = 185
  Caption = 'WEB-OPCOE-001 :: Op'#231#245'es WEB'
  ClientHeight = 657
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 166
        Height = 32
        Caption = 'Op'#231#245'es WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 166
        Height = 32
        Caption = 'Op'#231#245'es WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 166
        Height = 32
        Caption = 'Op'#231#245'es WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 495
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 495
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 495
        Align = alClient
        TabOrder = 0
        object PageControl1: TPageControl
          Left = 2
          Top = 15
          Width = 808
          Height = 478
          ActivePage = TSConexoes
          Align = alClient
          TabHeight = 25
          TabOrder = 0
          object TSConexoes: TTabSheet
            Caption = 'Conex'#245'es'
            object GBDataBase: TGroupBox
              Left = 9
              Top = 16
              Width = 464
              Height = 244
              Caption = 'Dados do banco de dados'
              TabOrder = 0
              object Label32: TLabel
                Left = 8
                Top = 18
                Width = 42
                Height = 13
                Caption = 'Servidor:'
              end
              object Label33: TLabel
                Left = 8
                Top = 60
                Width = 39
                Height = 13
                Caption = 'Usu'#225'rio:'
              end
              object Label34: TLabel
                Left = 8
                Top = 105
                Width = 34
                Height = 13
                Caption = 'Senha:'
              end
              object Label35: TLabel
                Left = 8
                Top = 148
                Width = 81
                Height = 13
                Caption = 'Banco de dados:'
              end
              object Label4: TLabel
                Left = 8
                Top = 189
                Width = 44
                Height = 13
                Caption = 'URL site:'
              end
              object Label18: TLabel
                Left = 153
                Top = 148
                Width = 28
                Height = 13
                Caption = 'Porta:'
              end
              object Label19: TLabel
                Left = 49
                Top = 227
                Width = 160
                Height = 13
                Caption = 'Exemplo: http://www.seusite.com'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = cl3DDkShadow
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
              end
              object EdWeb_Host: TdmkEdit
                Left = 8
                Top = 34
                Width = 201
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
              object EdWeb_User: TdmkEdit
                Left = 8
                Top = 76
                Width = 201
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
              object EdWeb_Pwd: TdmkEdit
                Left = 8
                Top = 120
                Width = 201
                Height = 20
                Font.Charset = SYMBOL_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Wingdings'
                Font.Style = []
                ParentFont = False
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                PasswordChar = 'l'
              end
              object EdWeb_DB: TdmkEdit
                Left = 8
                Top = 164
                Width = 140
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
              object EdWeb_MyURL: TdmkEdit
                Left = 8
                Top = 205
                Width = 201
                Height = 21
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
              object RGWeb_MySQL: TRadioGroup
                Left = 215
                Top = 18
                Width = 235
                Height = 122
                Caption = ' MySQL no meu site: '
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o possuo, ou n'#227'o tenho acesso direto.'
                  'Conectar somente quando eu necessitar.'
                  'Conectar automaticamente ao iniciar.')
                TabOrder = 6
              end
              object BtImporta: TBitBtn
                Tag = 39
                Left = 215
                Top = 145
                Width = 201
                Height = 40
                Caption = 'Importar Config de *.llm'
                NumGlyphs = 2
                TabOrder = 7
                OnClick = BtImportaClick
              end
              object BtTestar: TBitBtn
                Tag = 10001
                Left = 215
                Top = 186
                Width = 201
                Height = 40
                Cursor = crHandPoint
                Caption = '&Testar configura'#231#227'o'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 8
                OnClick = BtTestarClick
              end
              object EdWeb_Porta: TdmkEdit
                Left = 153
                Top = 164
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
              end
            end
            object GBFTP: TGroupBox
              Left = 9
              Top = 266
              Width = 464
              Height = 170
              Caption = 'Dados do servidor FTP'
              TabOrder = 1
              object Label14: TLabel
                Left = 8
                Top = 20
                Width = 42
                Height = 13
                Caption = 'Servidor:'
              end
              object Label15: TLabel
                Left = 215
                Top = 20
                Width = 39
                Height = 13
                Caption = 'Usu'#225'rio:'
              end
              object Label16: TLabel
                Left = 8
                Top = 62
                Width = 34
                Height = 13
                Caption = 'Senha:'
              end
              object Label17: TLabel
                Left = 8
                Top = 102
                Width = 49
                Height = 13
                Caption = 'Pasta raiz:'
              end
              object EdWeb_FTPh: TdmkEdit
                Left = 8
                Top = 36
                Width = 201
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
              object EdWeb_FTPu: TdmkEdit
                Left = 215
                Top = 36
                Width = 201
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
              object EdWeb_Raiz: TdmkEdit
                Left = 8
                Top = 118
                Width = 201
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
              object EdWeb_FTPs: TdmkEdit
                Left = 8
                Top = 78
                Width = 201
                Height = 20
                Font.Charset = SYMBOL_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Wingdings'
                Font.Style = []
                ParentFont = False
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                PasswordChar = 'l'
              end
              object BitBtn2: TBitBtn
                Tag = 39
                Left = 215
                Top = 69
                Width = 201
                Height = 40
                Caption = 'Importar Config de *.llm'
                NumGlyphs = 2
                TabOrder = 5
                OnClick = BitBtn2Click
              end
              object BitBtn1: TBitBtn
                Tag = 553
                Left = 215
                Top = 115
                Width = 201
                Height = 40
                Cursor = crHandPoint
                Caption = '&Testar configura'#231#227'o'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 6
                OnClick = BitBtn1Click
              end
              object CkWeb_FTPpassivo: TCheckBox
                Left = 8
                Top = 145
                Width = 90
                Height = 17
                Caption = 'Modo Passivo'
                TabOrder = 4
              end
            end
          end
          object TSGerais: TTabSheet
            Caption = 'Gerais'
            ImageIndex = 5
            object Label13: TLabel
              Left = 8
              Top = 8
              Width = 106
              Height = 13
              BiDiMode = bdLeftToRight
              Caption = 'Entidade dona do site:'
              ParentBiDiMode = False
            end
            object EdDono: TdmkEditCB
              Left = 8
              Top = 24
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBDono
              IgnoraDBLookupComboBox = False
            end
            object CBDono: TdmkDBLookupComboBox
              Left = 64
              Top = 24
              Width = 420
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsDono
              TabOrder = 1
              dmkEditCB = EdDono
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
          object TSSenhas: TTabSheet
            Caption = 'Senhas'
            ImageIndex = 1
            object Label3: TLabel
              Left = 8
              Top = 8
              Width = 104
              Height = 13
              Caption = 'Chave de criptografia:'
            end
            object EdCript: TdmkEdit
              Left = 8
              Top = 24
              Width = 209
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = True
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
          end
          object TSEmails: TTabSheet
            Caption = 'E-mails'
            ImageIndex = 2
            object Label5: TLabel
              Left = 8
              Top = 8
              Width = 138
              Height = 13
              BiDiMode = bdLeftToRight
              Caption = 'Texto de e-mail para contato:'
              ParentBiDiMode = False
            end
            object Label7: TLabel
              Left = 8
              Top = 51
              Width = 77
              Height = 13
              Caption = 'Expira em: (dias)'
            end
            object Label1: TLabel
              Left = 112
              Top = 71
              Width = 189
              Height = 13
              Caption = 'Quantidade de dias para expirar o e-mail'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object EdMailCont: TdmkEditCB
              Left = 8
              Top = 24
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBMailCont
              IgnoraDBLookupComboBox = False
            end
            object CBMailCont: TdmkDBLookupComboBox
              Left = 64
              Top = 24
              Width = 430
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsMailCont
              TabOrder = 1
              dmkEditCB = EdMailCont
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdDiasExp: TdmkEdit
              Left = 8
              Top = 67
              Width = 100
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
            end
          end
          object TSPerfis: TTabSheet
            Caption = 'Perfis de senhas'
            ImageIndex = 3
            object GroupBox4: TGroupBox
              Left = 8
              Top = 8
              Width = 490
              Height = 211
              Caption = 'Perfil padr'#227'o para entidades do tipo'
              TabOrder = 0
              object Label8: TLabel
                Left = 8
                Top = 66
                Width = 26
                Height = 13
                BiDiMode = bdLeftToRight
                Caption = 'Boss:'
                ParentBiDiMode = False
              end
              object Label10: TLabel
                Left = 8
                Top = 18
                Width = 66
                Height = 13
                BiDiMode = bdLeftToRight
                Caption = 'Administrador:'
                ParentBiDiMode = False
              end
              object Label11: TLabel
                Left = 8
                Top = 114
                Width = 35
                Height = 13
                BiDiMode = bdLeftToRight
                Caption = 'Cliente:'
                ParentBiDiMode = False
              end
              object Label12: TLabel
                Left = 8
                Top = 162
                Width = 39
                Height = 13
                BiDiMode = bdLeftToRight
                Caption = 'Usu'#225'rio:'
                ParentBiDiMode = False
              end
              object EdPerPasBos: TdmkEditCB
                Left = 8
                Top = 82
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                DBLookupComboBox = CBPerPasBos
                IgnoraDBLookupComboBox = False
              end
              object CBPerPasBos: TdmkDBLookupComboBox
                Left = 64
                Top = 82
                Width = 420
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsPerPasBos
                TabOrder = 3
                dmkEditCB = EdPerPasBos
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdPerPasAdm: TdmkEditCB
                Left = 8
                Top = 34
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                DBLookupComboBox = CBPerPasAdm
                IgnoraDBLookupComboBox = False
              end
              object CBPerPasAdm: TdmkDBLookupComboBox
                Left = 64
                Top = 34
                Width = 420
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsPerPasAdm
                TabOrder = 1
                dmkEditCB = EdPerPasAdm
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdPerPasCli: TdmkEditCB
                Left = 8
                Top = 130
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                DBLookupComboBox = CBPerPasCli
                IgnoraDBLookupComboBox = False
              end
              object CBPerPasCli: TdmkDBLookupComboBox
                Left = 64
                Top = 130
                Width = 420
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsPerPasCli
                TabOrder = 5
                dmkEditCB = EdPerPasCli
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdPerPasUsu: TdmkEditCB
                Left = 8
                Top = 178
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 6
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                DBLookupComboBox = CBPerPasUsu
                IgnoraDBLookupComboBox = False
              end
              object CBPerPasUsu: TdmkDBLookupComboBox
                Left = 64
                Top = 178
                Width = 420
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsPerPasUsu
                TabOrder = 7
                dmkEditCB = EdPerPasUsu
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
          end
          object TSDControl: TTabSheet
            Caption = 'DControl'
            ImageIndex = 4
            object Label2: TLabel
              Left = 8
              Top = 10
              Width = 259
              Height = 13
              BiDiMode = bdLeftToRight
              Caption = 'Diret'#243'rio WEB onde ser'#227'o armazenados os aplicativos:'
              ParentBiDiMode = False
            end
            object Label6: TLabel
              Left = 8
              Top = 52
              Width = 305
              Height = 13
              BiDiMode = bdLeftToRight
              Caption = 'Diret'#243'rio WEB onde ser'#227'o armazenados os aplicativos auxiliares:'
              ParentBiDiMode = False
            end
            object Label9: TLabel
              Left = 8
              Top = 97
              Width = 290
              Height = 13
              BiDiMode = bdLeftToRight
              Caption = 'Diret'#243'rio WEB onde ser'#227'o armazenados os aplicativos BETA:'
              ParentBiDiMode = False
            end
            object EdAppDirWeb: TdmkEditCB
              Left = 8
              Top = 26
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBAppDirWeb
              IgnoraDBLookupComboBox = False
            end
            object CBAppDirWeb: TdmkDBLookupComboBox
              Left = 64
              Top = 26
              Width = 420
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsAppDirWeb
              TabOrder = 1
              dmkEditCB = EdAppDirWeb
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object CBAppAuxDirWeb: TdmkDBLookupComboBox
              Left = 64
              Top = 68
              Width = 420
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsAppBetaDirWeb
              TabOrder = 3
              dmkEditCB = EdAppAuxDirWeb
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdAppAuxDirWeb: TdmkEditCB
              Left = 8
              Top = 68
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBAppAuxDirWeb
              IgnoraDBLookupComboBox = False
            end
            object CBAppBetaDirWeb: TdmkDBLookupComboBox
              Left = 64
              Top = 113
              Width = 420
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsAppAuxDirWeb
              TabOrder = 5
              dmkEditCB = EdAppBetaDirWeb
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdAppBetaDirWeb: TdmkEditCB
              Left = 8
              Top = 113
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBAppBetaDirWeb
              IgnoraDBLookupComboBox = False
            end
          end
          object TSSyndi2: TTabSheet
            Caption = 'Syndi2'
            ImageIndex = 6
            object Label20: TLabel
              Left = 8
              Top = 8
              Width = 74
              Height = 13
              Caption = 'Usu'#225'rio Master:'
            end
            object Label21: TLabel
              Left = 132
              Top = 8
              Width = 69
              Height = 13
              Caption = 'Senha Master:'
            end
            object Label22: TLabel
              Left = 256
              Top = 8
              Width = 111
              Height = 13
              Caption = 'Nome do administrador:'
            end
            object Label23: TLabel
              Left = 8
              Top = 54
              Width = 162
              Height = 13
              Caption = 'Diret'#243'rio utilizado para balancetes:'
            end
            object EdUsername: TdmkEdit
              Left = 8
              Top = 24
              Width = 121
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = True
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object EdPassword: TdmkEdit
              Left = 132
              Top = 24
              Width = 121
              Height = 21
              Font.Charset = SYMBOL_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Wingdings'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = True
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              PasswordChar = 'l'
            end
            object EdNomeAdmi: TdmkEdit
              Left = 256
              Top = 24
              Width = 121
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object CBDirWebBalancete: TdmkDBLookupComboBox
              Left = 49
              Top = 70
              Width = 328
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsDirWebBalan
              TabOrder = 4
              dmkEditCB = EdDirWebBalancete
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdDirWebBalancete: TdmkEditCB
              Left = 8
              Top = 70
              Width = 41
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBDirWebBalancete
              IgnoraDBLookupComboBox = False
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 543
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 587
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrDono: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome'
      'FROM entidades ent'
      'ORDER BY Nome')
    Left = 628
    Top = 105
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsDono: TDataSource
    DataSet = QrDono
    Left = 656
    Top = 105
  end
  object QrMailCont: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM preemail'
      'ORDER BY Nome')
    Left = 684
    Top = 105
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsMailCont: TDataSource
    DataSet = QrMailCont
    Left = 712
    Top = 105
  end
  object QrPerPasAdm: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM wperfis'
      'WHERE Tipo = 0')
    Left = 732
    Top = 157
    object IntegerField5: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField5: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrPerPasCli: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM wperfis'
      'WHERE Tipo = 2')
    Left = 564
    Top = 157
    object IntegerField6: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField6: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPerPasCli: TDataSource
    DataSet = QrPerPasCli
    Left = 592
    Top = 157
  end
  object QrPerPasUsu: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM wperfis'
      'WHERE Tipo = 3')
    Left = 620
    Top = 157
    object IntegerField7: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField7: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPerPasUsu: TDataSource
    DataSet = QrPerPasUsu
    Left = 648
    Top = 157
  end
  object QrPerPasBos: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM wperfis'
      'WHERE Tipo = 1')
    Left = 676
    Top = 157
    object IntegerField4: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField4: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPerPasBos: TDataSource
    DataSet = QrPerPasBos
    Left = 704
    Top = 157
  end
  object DsPerPasAdm: TDataSource
    DataSet = QrPerPasAdm
    Left = 760
    Top = 157
  end
  object QrAppAuxDirWeb: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ftpwebdir'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 708
    Top = 213
    object IntegerField9: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsAppDirWeb: TDataSource
    DataSet = QrAppDirWeb
    Left = 624
    Top = 213
  end
  object DsAppBetaDirWeb: TDataSource
    DataSet = QrAppBetaDirWeb
    Left = 680
    Top = 213
  end
  object QrAppDirWeb: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ftpwebdir'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 596
    Top = 213
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAppDirWebNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsAppAuxDirWeb: TDataSource
    DataSet = QrAppAuxDirWeb
    Left = 736
    Top = 213
  end
  object QrAppBetaDirWeb: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ftpwebdir'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 652
    Top = 213
    object IntegerField8: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAppBetaDirWebNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrWControl: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT *'
      'FROM wcontrol')
    Left = 492
    Top = 377
  end
  object QrDirWebBalan: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT *'
      'FROM dirweb'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 580
    Top = 292
    object IntegerField10: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDirWebBalanNome: TWideStringField
      FieldName = 'Nome'
      Size = 32
    end
  end
  object DsDirWebBalan: TDataSource
    DataSet = QrDirWebBalan
    Left = 608
    Top = 292
  end
  object QrSenha: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT User_ID, Username, Password, Tipo '
      'FROM users'
      'WHERE Tipo=9')
    Left = 636
    Top = 292
  end
end
