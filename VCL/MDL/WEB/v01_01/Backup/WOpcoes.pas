unit WOpcoes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkDBLookupComboBox,
  dmkEditCB, mySQLDbTables, DmkDAC_PF, Variants;

type
  TFmWOpcoes = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PageControl1: TPageControl;
    TSConexoes: TTabSheet;
    TSSenhas: TTabSheet;
    TSEmails: TTabSheet;
    TSPerfis: TTabSheet;
    TSDControl: TTabSheet;
    TSGerais: TTabSheet;
    GBDataBase: TGroupBox;
    Label32: TLabel;
    EdWeb_Host: TdmkEdit;
    Label33: TLabel;
    EdWeb_User: TdmkEdit;
    Label34: TLabel;
    EdWeb_Pwd: TdmkEdit;
    Label35: TLabel;
    EdWeb_DB: TdmkEdit;
    Label4: TLabel;
    EdWeb_MyURL: TdmkEdit;
    RGWeb_MySQL: TRadioGroup;
    BtImporta: TBitBtn;
    BtTestar: TBitBtn;
    GBFTP: TGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    EdWeb_FTPh: TdmkEdit;
    EdWeb_FTPu: TdmkEdit;
    EdWeb_Raiz: TdmkEdit;
    EdWeb_FTPs: TdmkEdit;
    BitBtn2: TBitBtn;
    BitBtn1: TBitBtn;
    CkWeb_FTPpassivo: TCheckBox;
    EdCript: TdmkEdit;
    Label3: TLabel;
    Label5: TLabel;
    EdMailCont: TdmkEditCB;
    CBMailCont: TdmkDBLookupComboBox;
    Label7: TLabel;
    EdDiasExp: TdmkEdit;
    Label1: TLabel;
    GroupBox4: TGroupBox;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    EdPerPasBos: TdmkEditCB;
    CBPerPasBos: TdmkDBLookupComboBox;
    EdPerPasAdm: TdmkEditCB;
    CBPerPasAdm: TdmkDBLookupComboBox;
    EdPerPasCli: TdmkEditCB;
    CBPerPasCli: TdmkDBLookupComboBox;
    EdPerPasUsu: TdmkEditCB;
    CBPerPasUsu: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdAppDirWeb: TdmkEditCB;
    CBAppDirWeb: TdmkDBLookupComboBox;
    Label6: TLabel;
    CBAppAuxDirWeb: TdmkDBLookupComboBox;
    EdAppAuxDirWeb: TdmkEditCB;
    Label9: TLabel;
    CBAppBetaDirWeb: TdmkDBLookupComboBox;
    EdAppBetaDirWeb: TdmkEditCB;
    Label13: TLabel;
    EdDono: TdmkEditCB;
    CBDono: TdmkDBLookupComboBox;
    QrDono: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsDono: TDataSource;
    QrMailCont: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    DsMailCont: TDataSource;
    QrPerPasAdm: TmySQLQuery;
    IntegerField5: TIntegerField;
    StringField5: TWideStringField;
    QrPerPasCli: TmySQLQuery;
    IntegerField6: TIntegerField;
    StringField6: TWideStringField;
    DsPerPasCli: TDataSource;
    QrPerPasUsu: TmySQLQuery;
    IntegerField7: TIntegerField;
    StringField7: TWideStringField;
    DsPerPasUsu: TDataSource;
    QrPerPasBos: TmySQLQuery;
    IntegerField4: TIntegerField;
    StringField4: TWideStringField;
    DsPerPasBos: TDataSource;
    DsPerPasAdm: TDataSource;
    QrAppAuxDirWeb: TmySQLQuery;
    IntegerField9: TIntegerField;
    StringField1: TWideStringField;
    DsAppDirWeb: TDataSource;
    DsAppBetaDirWeb: TDataSource;
    QrAppDirWeb: TmySQLQuery;
    IntegerField1: TIntegerField;
    QrAppDirWebNome: TWideStringField;
    DsAppAuxDirWeb: TDataSource;
    QrAppBetaDirWeb: TmySQLQuery;
    IntegerField8: TIntegerField;
    QrAppBetaDirWebNome: TWideStringField;
    Label18: TLabel;
    EdWeb_Porta: TdmkEdit;
    QrWControl: TmySQLQuery;
    Label19: TLabel;
    TSSyndi2: TTabSheet;
    Label20: TLabel;
    EdUsername: TdmkEdit;
    EdPassword: TdmkEdit;
    Label21: TLabel;
    Label22: TLabel;
    EdNomeAdmi: TdmkEdit;
    Label23: TLabel;
    CBDirWebBalancete: TdmkDBLookupComboBox;
    EdDirWebBalancete: TdmkEditCB;
    QrDirWebBalan: TmySQLQuery;
    IntegerField10: TIntegerField;
    QrDirWebBalanNome: TWideStringField;
    DsDirWebBalan: TDataSource;
    QrSenha: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtImportaClick(Sender: TObject);
    procedure BtTestarClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ConfiguraQueriesEspecificas();
    procedure MostraEdicao();
    procedure MostraEdicaoEspecificos();
  public
    { Public declarations }
  end;

  var
  FmWOpcoes: TFmWOpcoes;

implementation

uses UnMyObjects, Module, UnFTP, UMySQLModule, MyListas, ModuleGeral;

{$R *.DFM}

procedure TFmWOpcoes.BitBtn1Click(Sender: TObject);
const
  Avisa = True;
begin
  UFTP.TestaConexaoFTP(Self, EdWeb_FTPh.Text, EdWeb_FTPu.Text, EdWeb_FTPs.Text,
    CkWeb_FTPpassivo.Checked, Avisa);
end;

procedure TFmWOpcoes.BitBtn2Click(Sender: TObject);
var
  Nome, URL, Host, WebRaiz, Usuario, Senha: String;
  Codigo, Passivo: Integer;
begin
  UFTP.ImportaConfigFTP(Self, Nome, URL, Host, WebRaiz, Usuario, Senha, Passivo);
  //
  EdWeb_FTPh.ValueVariant  := Host;
  EdWeb_FTPu.ValueVariant  := Usuario;
  EdWeb_FTPs.Text          := Senha;
  EdWeb_Raiz.ValueVariant  := WebRaiz;
  CkWeb_FTPpassivo.Checked := Geral.IntToBool(Passivo);
end;

procedure TFmWOpcoes.BtImportaClick(Sender: TObject);
const
  Descricao = nil;
begin
  MyObjects.ImportaConfigWeb(self, Descricao, EdWeb_Host, EdWeb_DB, nil,
    EdWeb_User, EdWeb_Pwd);
end;

procedure TFmWOpcoes.BtOKClick(Sender: TObject);
var
  Web_Host, Web_User, Web_Pwd, Web_DB, Web_MyURL, Web_FTPh, Web_FTPu,
  Web_FTPs, Web_Raiz, Crypt, NomeAdmi, Username, Password: String;
  Web_Porta, Web_MySQL, Web_FTPpassivo, Dono, MailCont, DiasExp, PerPasAdm,
  PerPasBos, PerPasCli, PerPasUsu, AppDirWeb, AppAuxDirWeb, AppBetaDirWeb,
  DirWebBalancete, User_ID: Integer;
begin
  Web_Host  := EdWeb_Host.ValueVariant;
  Web_User  := EdWeb_User.ValueVariant;
  Web_Pwd   := EdWeb_Pwd.ValueVariant;
  Web_DB    := EdWeb_DB.ValueVariant;
  Web_Porta := edWeb_Porta.ValueVariant;
  Web_MyURL := EdWeb_MyURL.ValueVariant;
  Web_MySQL := RGWeb_MySQL.ItemIndex;
  //
  Web_FTPh       := EdWeb_FTPh.ValueVariant;
  Web_FTPu       := EdWeb_FTPu.ValueVariant;
  Web_FTPs       := EdWeb_FTPs.ValueVariant;
  Web_Raiz       := EdWeb_Raiz.ValueVariant;
  Web_FTPpassivo := Geral.BoolToInt(CkWeb_FTPpassivo.Checked);
  //
  Dono := EdDono.ValueVariant;
  //
  Crypt := EdCript.ValueVariant;
  //
  MailCont := EdMailCont.ValueVariant;
  DiasExp  := EdDiasExp.ValueVariant;
  //
  PerPasAdm := EdPerPasAdm.ValueVariant;
  PerPasBos := EdPerPasBos.ValueVariant;
  PerPasCli := EdPerPasCli.ValueVariant;
  PerPasUsu := EdPerPasUsu.ValueVariant;
  //
  if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE opcoesgerl SET ',
    'Web_Host="' + Web_Host + '", ',
    'Web_User="' + Web_User + '", ',
    'Web_Pwd="' + Web_Pwd + '", ',
    'Web_DB="' + Web_DB + '", ',
    'Web_Porta="' + Geral.FF0(Web_Porta) + '", ',
    'Web_MySQL="' + Geral.FF0(Web_MySQL) + '", ',
    'Web_MyURL="' + Web_MyURL + '", ',
    'Web_FTPh="' + Web_FTPh + '", ',
    'Web_FTPu="' + Web_FTPu + '", ',
    'Web_FTPs="' + Web_FTPs + '", ',
    'Web_Raiz="' + Web_Raiz + '", ',
    'Web_FTPpassivo="' + Geral.FF0(Web_FTPpassivo) + '" ',
    '']) then
  begin
    if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
      'UPDATE wcontrol SET ',
      'Dono="' + Geral.FF0(Dono) + '" ',
      '']) then
    begin
      if CO_DMKID_APP = 17 then //DControl
      begin
        AppDirWeb     := EdAppDirWeb.ValueVariant;
        AppAuxDirWeb  := EdAppAuxDirWeb.ValueVariant;
        AppBetaDirWeb := EdAppBetaDirWeb.ValueVariant;
        //
        UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
          'UPDATE wcontrol SET ',
          'AppDirWeb="' + Geral.FF0(AppDirWeb) + '", ',
          'AppAuxDirWeb="' + Geral.FF0(AppAuxDirWeb) + '", ',
          'AppBetaDirWeb="' + Geral.FF0(AppBetaDirWeb) + '", ',
          'Crypt="' + Crypt + '", ',
          'MailCont="' + Geral.FF0(MailCont) + '", ',
          'DiasExp="' + Geral.FF0(DiasExp) + '", ',
          'PerPasAdm="' + Geral.FF0(PerPasAdm) + '", ',
          'PerPasBos="' + Geral.FF0(PerPasBos) + '", ',
          'PerPasCli="' + Geral.FF0(PerPasCli) + '", ',
          'PerPasUsu="' + Geral.FF0(PerPasUsu) + '" ',
          '']);
      end;
      if CO_DMKID_APP = 24 then //Bugstrol
      begin
        UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
          'UPDATE wcontrol SET ',
          'Crypt="' + Crypt + '", ',
          'MailCont="' + Geral.FF0(MailCont) + '", ',
          'DiasExp="' + Geral.FF0(DiasExp) + '", ',
          'PerPasAdm="' + Geral.FF0(PerPasAdm) + '", ',
          'PerPasBos="' + Geral.FF0(PerPasBos) + '", ',
          'PerPasCli="' + Geral.FF0(PerPasCli) + '", ',
          'PerPasUsu="' + Geral.FF0(PerPasUsu) + '" ',
          '']);
      end;
      if CO_DMKID_APP = 4 then //Syndi2
      begin
        NomeAdmi := EdNomeAdmi.ValueVariant;
        Username := EdUsername.ValueVariant;
        Password := EdPassword.ValueVariant;
        User_ID  := QrSenha.FieldByName('User_ID').AsInteger;
        //
        if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
          'UPDATE wcontrol SET ',
          'NomeAdmi="' + NomeAdmi + '", ',
          'DirWebBalancete="' + Geral.FF0(DirWebBalancete) + '" ',
          '']) then
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
            'UPDATE users SET ',
            'Username="' + NomeAdmi + '", ',
            'Password="' + Password + '" ',
            'WHERE User_ID=' + Geral.FF0(User_ID),
            '']);
        end;
      end;
      Close;
    end;
  end;
end;

procedure TFmWOpcoes.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWOpcoes.BtTestarClick(Sender: TObject);
const
  Avisa = True;
begin
  UnDmkDAC_PF.TestarConexaoMySQL(Self, EdWeb_Host.Text, EdWeb_User.Text,
    EdWeb_Pwd.Text, EdWeb_DB.Text, 3306, Avisa);
end;

procedure TFmWOpcoes.ConfiguraQueriesEspecificas;
begin
  QrAppDirWeb.Close;
  QrAppBetaDirWeb.Close;
  QrAppAuxDirWeb.Close;
  QrPerPasAdm.Close;
  QrPerPasBos.Close;
  QrPerPasCli.Close;
  QrPerPasUsu.Close;
  //
  if CO_DMKID_APP = 17 then //DControl
  begin
    UMyMod.AbreQuery(QrAppDirWeb, Dmod.MyDBn);
    UMyMod.AbreQuery(QrAppBetaDirWeb, Dmod.MyDBn);
    UMyMod.AbreQuery(QrAppAuxDirWeb, Dmod.MyDBn);
    UMyMod.AbreQuery(QrPerPasAdm, Dmod.MyDBn);
    UMyMod.AbreQuery(QrPerPasBos, Dmod.MyDBn);
    UMyMod.AbreQuery(QrPerPasCli, Dmod.MyDBn);
    UMyMod.AbreQuery(QrPerPasUsu, Dmod.MyDBn);
  end;
  if CO_DMKID_APP = 24 then //Bugstrol
  begin
    UMyMod.AbreQuery(QrPerPasAdm, Dmod.MyDBn);
    UMyMod.AbreQuery(QrPerPasBos, Dmod.MyDBn);
    UMyMod.AbreQuery(QrPerPasCli, Dmod.MyDBn);
    UMyMod.AbreQuery(QrPerPasUsu, Dmod.MyDBn);
  end;
  if CO_DMKID_APP = 4 then //Syndi2
  begin
    UMyMod.AbreQuery(QrSenha, Dmod.MyDBn);
    UMyMod.AbreQuery(QrDirWebBalan, Dmod.MyDBn);
  end;
end;

procedure TFmWOpcoes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmWOpcoes.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UMyMod.AbreQuery(QrDono, Dmod.MyDBn);
  UMyMod.AbreQuery(QrMailCont, Dmod.MyDBn);
  UMyMod.AbreQuery(DmodG.QrOpcoesGerl, Dmod.MyDB);
  UMyMod.AbreQuery(QrWControl, Dmod.MyDBn);
  //
  PageControl1.ActivePageIndex := 0;
  //
  ConfiguraQueriesEspecificas;
  MostraEdicao();
end;

procedure TFmWOpcoes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWOpcoes.MostraEdicao();
begin
  //Conex�es
  EdWeb_Host.ValueVariant  := DmodG.QrOpcoesGerl.FieldByName('Web_Host').AsString;
  EdWeb_User.ValueVariant  := DmodG.QrOpcoesGerl.FieldByName('Web_User').AsString;
  EdWeb_Pwd.ValueVariant   := DmodG.QrOpcoesGerl.FieldByName('Web_Pwd').AsString;
  EdWeb_DB.ValueVariant    := DmodG.QrOpcoesGerl.FieldByName('Web_DB').AsString;
  EdWeb_Porta.ValueVariant := DModG.QrOpcoesGerl.FieldByName('Web_Porta').AsInteger;
  EdWeb_MyURL.ValueVariant := DmodG.QrOpcoesGerl.FieldByName('Web_MyURL').AsString;
  RGWeb_MySQL.ItemIndex    := DmodG.QrOpcoesGerl.FieldByName('Web_MySQL').AsInteger;
  //
  EdWeb_FTPh.ValueVariant  := DmodG.QrOpcoesGerl.FieldByName('Web_FTPh').AsString;
  EdWeb_FTPu.ValueVariant  := DmodG.QrOpcoesGerl.FieldByName('Web_FTPu').AsString;
  EdWeb_FTPs.ValueVariant  := DmodG.QrOpcoesGerl.FieldByName('Web_FTPs').AsString;
  EdWeb_Raiz.ValueVariant  := DmodG.QrOpcoesGerl.FieldByName('Web_Raiz').AsString;
  CkWeb_FTPpassivo.Checked := Geral.IntToBool(DmodG.QrOpcoesGerl.FieldByName('Web_FTPpassivo').AsInteger);
  //
  MostraEdicaoEspecificos();
end;

procedure TFmWOpcoes.MostraEdicaoEspecificos();
begin
  if CO_DMKID_APP = 17 then //DControl
  begin
    GBFTP.Visible         := False;
    TSDControl.TabVisible := True;
    TSGerais.TabVisible   := True;
    TSSenhas.TabVisible   := True;
    TSEmails.TabVisible   := True;
    TSPerfis.TabVisible   := True;
    TSSyndi2.TabVisible   := False;
    //
    //DControl
    EdAppDirWeb.ValueVariant     := QrWControl.FieldByName('AppDirWeb').AsInteger;
    CBAppDirWeb.KeyValue         := QrWControl.FieldByName('AppDirWeb').AsInteger;
    EdAppAuxDirWeb.ValueVariant  := QrWControl.FieldByName('AppAuxDirWeb').AsInteger;
    CBAppAuxDirWeb.KeyValue      := QrWControl.FieldByName('AppAuxDirWeb').AsInteger;
    EdAppBetaDirWeb.ValueVariant := QrWControl.FieldByName('AppBetaDirWeb').AsInteger;
    CBAppBetaDirWeb.KeyValue     := QrWControl.FieldByName('AppBetaDirWeb').AsInteger;
    //
    //Syndi2
    EdUsername.ValueVariant        := '';
    EdPassword.ValueVariant        := '';
    EdNomeAdmi.ValueVariant        := '';
    EdDirWebBalancete.ValueVariant := 0;
    CBDirWebBalancete.KeyValue     := Null;
    //
    //Gerais
    EdDono.ValueVariant := QrWControl.FieldByName('Dono').AsInteger;
    CBDono.KeyValue     := QrWControl.FieldByName('Dono').AsInteger;
    //
    //Senhas
    EdCript.ValueVariant := QrWControl.FieldByName('Crypt').AsString;
    //
    //E-mails
    EdMailCont.ValueVariant := QrWControl.FieldByName('MailCont').AsInteger;
    CBMailCont.KeyValue     := QrWControl.FieldByName('MailCont').AsInteger;
    EdDiasExp.ValueVariant  := QrWControl.FieldByName('DiasExp').AsInteger;
    //
    //Perfis de senhas
    EdPerPasAdm.ValueVariant := QrWControl.FieldByName('PerPasAdm').AsInteger;
    CBPerPasAdm.KeyValue     := QrWControl.FieldByName('PerPasAdm').AsInteger;
    EdPerPasBos.ValueVariant := QrWControl.FieldByName('PerPasBos').AsInteger;
    CBPerPasBos.KeyValue     := QrWControl.FieldByName('PerPasBos').AsInteger;
    EdPerPasCli.ValueVariant := QrWControl.FieldByName('PerPasCli').AsInteger;
    CBPerPasCli.KeyValue     := QrWControl.FieldByName('PerPasCli').AsInteger;
    EdPerPasUsu.ValueVariant := QrWControl.FieldByName('PerPasUsu').AsInteger;
    CBPerPasUsu.KeyValue     := QrWControl.FieldByName('PerPasUsu').AsInteger;
  end else
  if CO_DMKID_APP = 24 then //Bugstrol
  begin
    GBFTP.Visible         := False;
    TSDControl.TabVisible := False;
    TSGerais.TabVisible   := True;
    TSSenhas.TabVisible   := True;
    TSEmails.TabVisible   := True;
    TSPerfis.TabVisible   := True;
    TSSyndi2.TabVisible   := False;
    //
    //DControl
    EdAppDirWeb.ValueVariant     := 0;
    CBAppDirWeb.KeyValue         := 0;
    EdAppAuxDirWeb.ValueVariant  := 0;
    CBAppAuxDirWeb.KeyValue      := 0;
    EdAppBetaDirWeb.ValueVariant := 0;
    CBAppBetaDirWeb.KeyValue     := 0;
    //
    //Syndi2
    EdUsername.ValueVariant        := '';
    EdPassword.ValueVariant        := '';
    EdNomeAdmi.ValueVariant        := '';
    EdDirWebBalancete.ValueVariant := 0;
    CBDirWebBalancete.KeyValue     := Null;
    //
    //Gerais
    EdDono.ValueVariant := QrWControl.FieldByName('Dono').AsInteger;
    CBDono.KeyValue     := QrWControl.FieldByName('Dono').AsInteger;
    //
    //Senhas
    EdCript.ValueVariant := QrWControl.FieldByName('Crypt').AsString;
    //
    //E-mails
    EdMailCont.ValueVariant := QrWControl.FieldByName('MailCont').AsInteger;
    CBMailCont.KeyValue     := QrWControl.FieldByName('MailCont').AsInteger;
    EdDiasExp.ValueVariant  := QrWControl.FieldByName('DiasExp').AsInteger;
    //
    //Perfis de senhas
    EdPerPasAdm.ValueVariant := QrWControl.FieldByName('PerPasAdm').AsInteger;
    CBPerPasAdm.KeyValue     := QrWControl.FieldByName('PerPasAdm').AsInteger;
    EdPerPasBos.ValueVariant := QrWControl.FieldByName('PerPasBos').AsInteger;
    CBPerPasBos.KeyValue     := QrWControl.FieldByName('PerPasBos').AsInteger;
    EdPerPasCli.ValueVariant := QrWControl.FieldByName('PerPasCli').AsInteger;
    CBPerPasCli.KeyValue     := QrWControl.FieldByName('PerPasCli').AsInteger;
    EdPerPasUsu.ValueVariant := QrWControl.FieldByName('PerPasUsu').AsInteger;
    CBPerPasUsu.KeyValue     := QrWControl.FieldByName('PerPasUsu').AsInteger;
  end else
  if CO_DMKID_APP = 4 then //Syndi2
  begin
    GBFTP.Visible         := True;
    TSDControl.TabVisible := False;
    TSGerais.TabVisible   := True;
    TSSenhas.TabVisible   := False;
    TSEmails.TabVisible   := False;
    TSPerfis.TabVisible   := False;
    TSSyndi2.TabVisible   := True;
    //
    //DControl
    EdAppDirWeb.ValueVariant     := 0;
    CBAppDirWeb.KeyValue         := Null;
    EdAppAuxDirWeb.ValueVariant  := 0;
    CBAppAuxDirWeb.KeyValue      := Null;
    EdAppBetaDirWeb.ValueVariant := 0;
    CBAppBetaDirWeb.KeyValue     := Null;
    //
    //Syndi2
    EdUsername.ValueVariant        := QrSenha.FieldByName('Username').AsString;
    EdPassword.ValueVariant        := QrSenha.FieldByName('Password').AsString;
    EdNomeAdmi.ValueVariant        := QrWControl.FieldByName('NomeAdmi').AsString;
    EdDirWebBalancete.ValueVariant := QrWControl.FieldByName('DirWebBalancete').AsInteger;
    CBDirWebBalancete.KeyValue     := QrWControl.FieldByName('DirWebBalancete').AsInteger;
    //
    //Gerais
    EdDono.ValueVariant := QrWControl.FieldByName('Dono').AsInteger;
    CBDono.KeyValue     := QrWControl.FieldByName('Dono').AsInteger;
    //
    //Senhas
    EdCript.ValueVariant := '';
    //
    //E-mails
    EdMailCont.ValueVariant := 0;
    CBMailCont.KeyValue     := Null;
    EdDiasExp.ValueVariant  := 0;
    //
    //Perfis de senhas
    EdPerPasAdm.ValueVariant := 0;
    CBPerPasAdm.KeyValue     := Null;
    EdPerPasBos.ValueVariant := 0;
    CBPerPasBos.KeyValue     := Null;
    EdPerPasCli.ValueVariant := 0;
    CBPerPasCli.KeyValue     := Null;
    EdPerPasUsu.ValueVariant := 0;
    CBPerPasUsu.KeyValue     := Null;
  end else
  begin
    GBFTP.Visible         := True;
    TSDControl.TabVisible := False;
    TSGerais.TabVisible   := False;
    TSSenhas.TabVisible   := False;
    TSEmails.TabVisible   := False;
    TSPerfis.TabVisible   := False;
    TSSyndi2.TabVisible   := False;
    //
    //DControl
    EdAppDirWeb.ValueVariant     := 0;
    CBAppDirWeb.KeyValue         := Null;
    EdAppAuxDirWeb.ValueVariant  := 0;
    CBAppAuxDirWeb.KeyValue      := Null;
    EdAppBetaDirWeb.ValueVariant := 0;
    CBAppBetaDirWeb.KeyValue     := Null;
    //
    //Syndi2
    EdUsername.ValueVariant        := '';
    EdPassword.ValueVariant        := '';
    EdNomeAdmi.ValueVariant        := '';
    EdDirWebBalancete.ValueVariant := 0;
    CBDirWebBalancete.KeyValue     := Null;
    //
    //Gerais
    EdDono.ValueVariant := 0;
    CBDono.KeyValue     := Null;
    //
    //Senhas
    EdCript.ValueVariant := '';
    //
    //E-mails
    EdMailCont.ValueVariant := 0;
    CBMailCont.KeyValue     := Null;
    EdDiasExp.ValueVariant  := 0;
    //
    //Perfis de senhas
    EdPerPasAdm.ValueVariant := 0;
    CBPerPasAdm.KeyValue     := Null;
    EdPerPasBos.ValueVariant := 0;
    CBPerPasBos.KeyValue     := Null;
    EdPerPasCli.ValueVariant := 0;
    CBPerPasCli.KeyValue     := Null;
    EdPerPasUsu.ValueVariant := 0;
    CBPerPasUsu.KeyValue     := Null;
  end;
end;

end.
