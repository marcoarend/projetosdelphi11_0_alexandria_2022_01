unit WSuporte;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, AdvGlowButton, OleCtrls, SHDocVw, StrUtils, dmkPageControl, Menus,
  AdvMenus, UnDmkEnums;

type
  TFmWSuporte = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    LaTitulo1C: TLabel;
    PnMenu: TPanel;
    Panel1: TPanel;
    CkNavegPad: TCheckBox;
    ProgressBar1: TProgressBar;
    ImgWEB: TdmkImage;
    TabControl1: TTabControl;
    WebBrowser1: TWebBrowser;
    AdvGlowButton6: TAdvGlowButton;
    AdvPopupMenu1: TAdvPopupMenu;
    Gerenciar1: TMenuItem;
    Perguntasfrequentes1: TMenuItem;
    Histricodeatualizaes1: TMenuItem;
    N2: TMenuItem;
    Desconectar1: TMenuItem;
    Label1: TLabel;
    EdPesquisar: TdmkEdit;
    AdvGlowButton1: TAdvGlowButton;
    N1: TMenuItem;
    Licenas1: TMenuItem;
    GerenciamentodeusuriosWEB1: TMenuItem;
    N3: TMenuItem;
    Bloquetosemaberto1: TMenuItem;
    Bloquetosvencidos1: TMenuItem;
    Visualizarnotasfiscais1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure WebBrowser1ProgressChange(ASender: TObject; Progress,
      ProgressMax: Integer);
    procedure TabControl1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Gerenciar1Click(Sender: TObject);
    procedure Perguntasfrequentes1Click(Sender: TObject);
    procedure Histricodeatualizaes1Click(Sender: TObject);
    procedure Desconectar1Click(Sender: TObject);
    procedure AdvGlowButton1Click(Sender: TObject);
    procedure AdvPopupMenu1Popup(Sender: TObject);
    procedure GerenciamentodeusuriosWEB1Click(Sender: TObject);
    procedure Licenas1Click(Sender: TObject);
    procedure Bloquetosemaberto1Click(Sender: TObject);
    procedure Bloquetosvencidos1Click(Sender: TObject);
    procedure Visualizarnotasfiscais1Click(Sender: TObject);
    procedure WebBrowser1BeforeNavigate2(ASender: TObject;
      const pDisp: IDispatch; const URL, Flags, TargetFrameName, PostData,
      Headers: OleVariant; var Cancel: WordBool);
  private
    { Private declarations }
    procedure ExtractParams(var URL: OleVariant; lHtmlVars: TStringList);
    procedure trataEvento (AHtmlVars: TStringList);
    procedure ConfiguraURL(Tab: Integer);
  public
    { Public declarations }
    FPrntScrnImg, FJanela, FAncora: String;
    FTamanho: Int64;
  end;

  var
  FmWSuporte: TFmWSuporte;

implementation

uses UnMyObjects, Module, UnDmkWeb, MyListas, WOrdSerIncRap, UnDmkProcFunc;

{$R *.DFM}

procedure TFmWSuporte.AdvGlowButton1Click(Sender: TObject);
  function ObtemSysInfo(): String;
  var
    Info: String;
  begin
    Info := dmkPF.SistemaOperacional_txt;
    if TOSInfo.IsWOW64 = True then
      Info := Info + ' 64bits '
    else
      Info := Info + ' 32bits ';
    Result := Info;
  end;
begin
  Application.CreateForm(TFmWOrdSerIncRap, FmWOrdSerIncRap);
  FmWOrdSerIncRap.FPrntScrnImg := FPrntScrnImg;
  FmWOrdSerIncRap.FJanela      := FJanela;
  FmWOrdSerIncRap.FAncora      := FAncora;
  FmWOrdSerIncRap.FSisInfo     := ObtemSysInfo();
  FmWOrdSerIncRap.ShowModal;
  FmWOrdSerIncRap.Destroy;
  //
  Close;
end;

procedure TFmWSuporte.AdvPopupMenu1Popup(Sender: TObject);
var
  Visi: Boolean;
begin
  Visi := VAR_WEB_TIPUSER in [0,1,2];
  //
  GerenciamentodeusuriosWEB1.Visible := Visi;
  Licenas1.Visible                   := Visi;
  N1.Visible                         := Visi;
  Bloquetosemaberto1.Visible         := Visi;
  Bloquetosvencidos1.Visible         := Visi;
  Visualizarnotasfiscais1.Visible    := Visi;
  N3.Visible                         := Visi;
end;

procedure TFmWSuporte.Bloquetosemaberto1Click(Sender: TObject);
var
  Link: String;
begin
  if CkNavegPad.Checked then
  begin
    if VAR_WEB_TIPUSER = 2 then
      Link := 'http://www.dermatek.net.br/?page=via2&logid=' + VAR_WEB_IDLOGIN
    else
      Link := 'http://www.dermatek.net.br/?page=abertos&logid=' + VAR_WEB_IDLOGIN;
  end else
  begin
    if VAR_WEB_TIPUSER = 2 then
      Link := 'http://www.dermatek.net.br/?page=via2'
    else
      Link := 'http://www.dermatek.net.br/?page=abertos';
  end;
  //
  DmkWeb.MostraWebBrowser(Link, CkNavegPad.Checked, False, 0, 0);
  //
  Close;
end;

procedure TFmWSuporte.Bloquetosvencidos1Click(Sender: TObject);
var
  Link: String;
begin
  if CkNavegPad.Checked then
  begin
    if VAR_WEB_TIPUSER = 2 then
      Link := 'http://www.dermatek.net.br/?page=inadimp&logid=' + VAR_WEB_IDLOGIN
    else
      Link := 'http://www.dermatek.net.br/?page=inad&logid=' + VAR_WEB_IDLOGIN;
  end else
  begin
    if VAR_WEB_TIPUSER = 2 then
      Link := 'http://www.dermatek.net.br/?page=inadimp'
    else
      Link := 'http://www.dermatek.net.br/?page=inad';
  end;
  //
  DmkWeb.MostraWebBrowser(Link, CkNavegPad.Checked, False, 0, 0);
  //
  Close;
end;

procedure TFmWSuporte.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWSuporte.ConfiguraURL(Tab: Integer);
var
  URL: String;
begin
  case Tab of
    0: URL := 'http://www.dermatek.net.br/api/widgets/wid_wfaq.php?id=' +
         Geral.FF0(VAR_WEB_CODUSER) + '&limit=10&desktop=true&texto=' + FJanela;
    1: URL := 'http://www.dermatek.net.br/api/widgets/wid_wordser.php?id=' +
         Geral.FF0(VAR_WEB_CODUSER) + '&limit=10&desktop=true';
  end;
  //
  webbrowser1.Navigate(URL);
end;

procedure TFmWSuporte.Desconectar1Click(Sender: TObject);
begin
  DmkWeb.DesconectarUsuarioWEB;
  Close;
end;

procedure TFmWSuporte.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmWSuporte.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType      := stLok;
  TabControl1.TabIndex := 0;
end;

procedure TFmWSuporte.FormShow(Sender: TObject);
begin
  ConfiguraURL(TabControl1.TabIndex);
end;

procedure TFmWSuporte.GerenciamentodeusuriosWEB1Click(Sender: TObject);
var
  Link: String;
begin
  if CkNavegPad.Checked then
    Link := 'http://www.dermatek.net.br/?page=cadpass&logid=' + VAR_WEB_IDLOGIN
  else
    Link := 'http://www.dermatek.net.br/?page=cadpass';
  //
  DmkWeb.MostraWebBrowser(Link, CkNavegPad.Checked, False, 0, 0);
  //
  Close;
end;

procedure TFmWSuporte.Gerenciar1Click(Sender: TObject);
var
  Link: String;
begin
  if CkNavegPad.Checked then
    Link := 'http://www.dermatek.net.br/?page=wordser&logid=' + VAR_WEB_IDLOGIN
  else
    Link := 'http://www.dermatek.net.br/?page=wordser';
  //
  DmkWeb.MostraWebBrowser(Link, CkNavegPad.Checked, False, 0, 0);
  //
  Close;
end;

procedure TFmWSuporte.Histricodeatualizaes1Click(Sender: TObject);
var
  Link: String;
begin
  if CkNavegPad.Checked then
    Link := 'http://www.dermatek.net.br/?page=hisalt&logid=' + VAR_WEB_IDLOGIN +
      '&aplicativo=' + Geral.FF0(CO_DMKID_APP) + '&versao=' + Geral.FF0(CO_VERSAO)
  else
    Link := 'http://www.dermatek.net.br/?page=hisalt&aplicativo=' +
      Geral.FF0(CO_DMKID_APP) + '&versao=' + Geral.FF0(CO_VERSAO);
  //
  DmkWeb.MostraWebBrowser(Link, CkNavegPad.Checked, False, 0, 0);
  //
  Close;
end;

procedure TFmWSuporte.Licenas1Click(Sender: TObject);
var
  Link: String;
begin
  if CkNavegPad.Checked then
    Link := 'http://www.dermatek.net.br/?page=licencas&logid=' + VAR_WEB_IDLOGIN
  else
    Link := 'http://www.dermatek.net.br/?page=licencas';
  //
  DmkWeb.MostraWebBrowser(Link, CkNavegPad.Checked, False, 0, 0);
  //
  Close;
end;

procedure TFmWSuporte.Perguntasfrequentes1Click(Sender: TObject);
var
  Link: String;
begin
  if CkNavegPad.Checked then
    Link := 'http://www.dermatek.net.br/?page=wfaq&logid=' + VAR_WEB_IDLOGIN +
      '&janela=' + FJanela
  else
    Link := 'http://www.dermatek.net.br/?page=wfaq&janela=' + FJanela;
  //
  DmkWeb.MostraWebBrowser(Link, CkNavegPad.Checked, False, 0, 0);
  //
  Close;
end;

procedure TFmWSuporte.TabControl1Change(Sender: TObject);
begin
  ConfiguraURL(TabControl1.TabIndex);
end;

procedure TFmWSuporte.trataEvento(AHtmlVars: TStringList);
var
  Link, lStrExibir: String;
begin
  lStrExibir := AHtmlVars.Values ['url'];
  if Length(lStrExibir) > 0 then
  begin
    lStrExibir := Geral.Substitui(lStrExibir, '_', '&');
    //
    if CkNavegPad.Checked then
      Link := lStrExibir + '&logid=' + VAR_WEB_IDLOGIN
    else
      Link := lStrExibir;
    //
    DmkWeb.MostraWebBrowser(Link, CkNavegPad.Checked, False, 0, 0);
    //
    Close;
  end;
end;

procedure TFmWSuporte.Visualizarnotasfiscais1Click(Sender: TObject);
var
  Link: String;
begin
  if CkNavegPad.Checked then
    Link := 'http://www.dermatek.net.br/?page=nfsevis&logid=' + VAR_WEB_IDLOGIN
  else
    Link := 'http://www.dermatek.net.br/?page=nfsevis';
  //
  DmkWeb.MostraWebBrowser(Link, CkNavegPad.Checked, False, 0, 0);
  //
  Close;
end;

procedure TFmWSuporte.WebBrowser1BeforeNavigate2(ASender: TObject;
  const pDisp: IDispatch; const URL, Flags, TargetFrameName, PostData,
  Headers: OleVariant; var Cancel: WordBool);
var
  lHtmlVars : TStringList;
  WAddr: OleVariant;
begin
  WAddr     := URL;
  lHtmlVars := TStringList.Create;
  try
    ExtractParams(WAddr, lHtmlVars);
  finally
    TrataEvento(lHtmlVars);
    FreeAndNil(lHtmlVars);
  end;
end;

procedure TFmWSuporte.WebBrowser1ProgressChange(ASender: TObject; Progress,
  ProgressMax: Integer);
begin
  if Progress > 0 then
  begin
    ProgressBar1.Visible  := True;
    ProgressBar1.Max      := ProgressMax;
    ProgressBar1.Position := Progress
  end else
  begin
    ProgressBar1.Visible  := False;
    ProgressBar1.Position := 0
  end;
end;

procedure TFmWSuporte.ExtractParams(var URL: OleVariant; lHtmlVars: TStringList);
var
  lPos: Integer;
  lUrl, lParm: String;
begin
  { Captura apenas os par�metros, isto �, o que vem ap�s o
      sinal de interroga��o }
  lPos := AnsiPos('?', URL);
  lUrl := AnsiRightStr(URL, Length(URL) - lPos);
  { Obtem cada par�metro, separado por um & }
  lPos := 1;
  lParm := '';
  while lPos <= Length(lURL) do
  begin
    if (lUrl[lPos] = '&') then
    begin
      lHtmlVars.Add(lParm);
      lParm := '';
    end
    else
      lParm := lParm + lUrl[lPos];
    Inc(lPos);
  end;
  if lParm <> '' then
    lHtmlVars.Add(lParm);
end;

end.
