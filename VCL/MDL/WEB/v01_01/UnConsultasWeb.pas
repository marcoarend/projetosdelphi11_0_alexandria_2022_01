unit UnConsultasWeb;

interface

uses Classes, StdCtrls, Forms, Controls, dmkGeral, ExtCtrls, Windows, SysUtils,
  Dialogs, Messages, dmkEdit, dmkDBLookupComboBox, dmkEditCB, mySQLDbTables,
  Variants, UnDmkEnums, dmkEditDateTimePicker;

type
  TArrTgs = array[0..2] of String;
  TUnConsultasWeb = class(TObject)
  public
    { Public declarations }
    function  ConsultaCPF(EdNome, EdCPF: TdmkEdit): Boolean;
    function  ConsultaCNPJ(EdCNPJ, EdIE, EdRazaoSocial, EdCEP, EdRua, EdLograd,
                EdCompl, EdNumero, EdBairro, EdCidade, EdUF, EdPais: TdmkEdit;
                CBLograd, CBCodMunici, CBCodiPais, CBCodCNAE: TdmkDBLookupComboBox;
                EdCodMunici, EdCodiPais, EdCodCNAE: TdmkEditCB): Boolean;
    function  ConsultaCNPJ2(EdCNPJ, EdIE, EdRazaoSocial, EdFantasia, EdCEP,
                EdRua, EdLograd, EdCompl, EdNumero, EdBairro, EdCidade, EdUF,
                EdPais: TdmkEdit; CBLograd, CBCodMunici, CBCodiPais,
                CBCodCNAE: TdmkDBLookupComboBox; EdCodMunici, EdCodiPais,
                EdCodCNAE: TdmkEditCB): Boolean;
    function  ConsultaIE(UF: String; EdCNPJ, EdIE, EdRazaoSocial, EdFantasia,
                EdCEP, EdRua, EdLograd, EdCompl, EdNumero, EdBairro, EdCidade,
                EdUF, EdPais: TdmkEdit; CBLograd, CBCodMunici, CBCodiPais,
                CBCodCNAE: TdmkDBLookupComboBox; EdCodMunici, EdCodiPais,
                EdCodCNAE: TdmkEditCB; EdTelefone, EdEmail,
                EdSUFRAMA: TdmkEdit): Boolean;
    function  ConsultaCCC_SVRS(CodUF_IBGE: Integer; UF: String; EdCNPJ, EdIE,
                EdRazaoSocial, EdFantasia,
                EdCEP, EdRua, EdLograd, EdCompl, EdNumero, EdBairro, EdCidade,
                EdUF, EdPais: TdmkEdit; CBLograd, CBCodMunici, CBCodiPais,
                CBCodCNAE: TdmkDBLookupComboBox; EdCodMunici, EdCodiPais,
                EdCodCNAE: TdmkEditCB; EdTelefone, EdEmail,
                EdSUFRAMA: TdmkEdit; TPENatal: TdmkEditDateTimePicker): Boolean;
    function  ObtemCodigoDTBMunici(Cidade, UF: String): Integer;
    function  ObtemURLdoSINTEGRA(UF: String; CCC: Boolean): String;
    function  ObtemTagSINTEGRA(UF: String; LaCNPJ, LaIE, LaRazaoSocial,
                LaLogradouro, LaNumero, LaComplemento, LaBairro, LaMunicipio,
                LaCEP, LaUF, LaTelefone, LaEMail, LaCNAE, LaFantasia,
                LaSUFRAMA: TLabel): Boolean;
    {$IfNDef SemEntidade}
    //Mapas Geral
    procedure MapasDeEntidadesMontaEndereco(TipoForm: TFormCadEnti; Entidade:
                Integer; var MapaEndereco, MapaDescri: string);
    procedure MapasDeEntidades(TipoForm: TFormCadEnti; EnderecoLatLong,
                Descricao: array of string);
    //Google Maps
    procedure GoogleMaps_CarregaModosDeDeslocamento(Combo: TComboBox);
    function  GoogleMaps_TraduzModoDeDeslocamento(Modo: Integer): String;
    {$EndIf} // SemEntidade
  end;

var
  UConsultasWeb: TUnConsultasWeb;

implementation

uses MyDBCheck, Module, UnCEP, ModuleGeral, DmkDAC_PF,
  {$IfNDef SemEntidade1} EntiMapa, EntiRFCPF, EntiRFCNPJ, EntiRE_IE, {$EndIf}
  UnInternalConsts, UnDmkWeb, UnGrl_DmkREST, UnServices;

{ TUnConsultasWeb }

function TUnConsultasWeb.ConsultaCNPJ(EdCNPJ, EdIE, EdRazaoSocial, EdCEP, EdRua,
  EdLograd, EdCompl, EdNumero, EdBairro, EdCidade, EdUF, EdPais: TdmkEdit;
  CBLograd, CBCodMunici, CBCodiPais, CBCodCNAE: TdmkDBLookupComboBox;
  EdCodMunici, EdCodiPais, EdCodCNAE: TdmkEditCB): Boolean;
var
  TipoImport: Integer;
begin
{$IfNDef SemEntidade1}
  Result := False;
  //
  if DBCheck.CriaFm(TFmEntiRFCNPJ, FmEntiRFCNPJ, afmoNegarComAviso) then
  begin
    FmEntiRFCNPJ.FCNPJ := Geral.SoNumero_TT(EdCNPJ.ValueVariant);
    FmEntiRFCNPJ.ShowModal;
    //
    if EdCNPJ <> nil then
      EdCNPJ.ValueVariant := FmEntiRFCNPJ.FCNPJ;
    if EdRazaoSocial <> nil then
      EdRazaoSocial.ValueVariant := FmEntiRFCNPJ.FRazaoSocial;
    if EdCEP <> nil then
      EdCEP.ValueVariant := FmEntiRFCNPJ.FCEP;
    if EdLograd <> nil then
      EdLograd.ValueVariant := Geral.FF0(FmEntiRFCNPJ.FTipoLograd);
    if CBLograd <> nil then
      CBLograd.KeyValue := FmEntiRFCNPJ.FTipoLograd;
    if EdRua <> nil then
      EdRua.ValueVariant := FmEntiRFCNPJ.FRua;
    if EdCidade <> nil then
      EdCidade.ValueVariant := Geral.Maiusculas(FmEntiRFCNPJ.FCidade, True);
    if EdCodMunici <> nil then
      EdCodMunici.ValueVariant := ObtemCodigoDTBMunici(FmEntiRFCNPJ.FCidade,
                                    FmEntiRFCNPJ.FUF);
    if CBCodMunici <> nil then
      CBCodMunici.KeyValue := ObtemCodigoDTBMunici(FmEntiRFCNPJ.FCidade,
                                FmEntiRFCNPJ.FUF);
    if EdPais <> nil then
      EdPais.ValueVariant := 'BRASIL';
    if EdCodiPais <> nil then
      EdCodiPais.ValueVariant := 1058;
    if CBCodiPais <> nil then
      CBCodiPais.KeyValue := 1058;
    if EdUF <> nil then
      EdUF.ValueVariant := UpperCase(FmEntiRFCNPJ.FUF);
    if EdBairro <> nil then
      EdBairro.ValueVariant := Geral.Maiusculas(FmEntiRFCNPJ.FBairro, True);
    if EdCodCNAE <> nil then
      EdCodCNAE.ValueVariant := FmEntiRFCNPJ.FCNAE;
    if CBCodCNAE <> nil then
      CBCodCNAE.KeyValue := FmEntiRFCNPJ.FCNAE;
    if EdNumero <> nil then
      EdNumero.ValueVariant := Geral.SoNumero_TT(FmEntiRFCNPJ.FNumero);
    if EdCompl <> nil then
      EdCompl.ValueVariant := FmEntiRFCNPJ.FCompl;
    //
    TipoImport := FmEntiRFCNPJ.FTipoImport;
    //
    FmEntiRFCNPJ.Destroy;
    //
    if TipoImport = -1 then
      Exit;
    //
    if TipoImport = 0 then
    begin
      U_CEP.ConsultaCEP(EdCEP, EdLograd, EdRua, EdNumero, EdBairro,
        EdCidade, EdUF, EdPais, EdNumero, EdCompl, CBLograd,
        EdCodMunici, CBCodMunici, EdCodiPais, CBCodiPais);
    end else
    begin
      if EdIE.Enabled then
        EdIE.SetFocus;      
    end;
    Result := True;
  end;
{$EndIf}
end;

function TUnConsultasWeb.ConsultaCNPJ2(EdCNPJ, EdIE, EdRazaoSocial, EdFantasia,
  EdCEP, EdRua, EdLograd, EdCompl, EdNumero, EdBairro, EdCidade, EdUF,
  EdPais: TdmkEdit; CBLograd, CBCodMunici, CBCodiPais, CBCodCNAE: TdmkDBLookupComboBox;
  EdCodMunici, EdCodiPais, EdCodCNAE: TdmkEditCB): Boolean;
var
  Cnpj, Json: String;
  ObjCnpj: TCnpj;
begin
{$IfNDef SemEntidade1}
  Cnpj := EdCNPJ.ValueVariant;
  //
  if Cnpj = '' then
  begin
    Geral.MB_Aviso('CNPJ n�o informado!');
    EdCNPJ.SetFocus;
    //
    Result := True;
    Exit;
  end;
  //
  Result := False;
  //
  Screen.Cursor := crHourGlass;
  //
  try
    if Grl_DmkREST.ObtemDadosCnpj(False, Cnpj, Json) then
    begin
      ObjCnpj := TCnpj.Create(Json);
      //
      if ObjCnpj.Cnpj = '' then
      begin
        Geral.MB_Aviso('CNPJ n�o localizado!');
        Screen.Cursor := crDefault;
        exit;
      end;
      //
      if EdCNPJ <> nil then
        EdCNPJ.ValueVariant := ObjCnpj.Cnpj;
      if EdRazaoSocial <> nil then
        EdRazaoSocial.ValueVariant := ObjCnpj.RazaoSocial;
      if EdFantasia <> nil then
        EdFantasia.ValueVariant := ObjCnpj.Fantasia;
      if EdCEP <> nil then
        EdCEP.ValueVariant := ObjCnpj.Cep;
      if EdNumero <> nil then
      try
          EdNumero.ValueVariant := ObjCnpj.Numero
      except
          EdNumero.ValueVariant := 0;
      end;
      if EdCompl <> nil then
        EdCompl.ValueVariant := ObjCnpj.Complemento;
      if EdCodCNAE <> nil then
        EdCodCNAE.ValueVariant := Geral.SoNumero_TT(ObjCnpj.CnaeId);
      if CBCodCNAE <> nil then
        CBCodCNAE.KeyValue := Geral.SoNumero_TT(ObjCnpj.CnaeId);
      //
      U_CEP.ConsultaCEP4(EdCEP, EdLograd, EdRua, EdNumero, EdBairro,
        EdCidade, EdUF, EdPais, EdNumero, EdCompl, CBLograd,
        EdCodMunici, CBCodMunici, EdCodiPais, CBCodiPais);
      //
      EdIE.SetFocus;
    end;
  except
    ;
  end;
  //
  Screen.Cursor := crDefault;
  //
  Result := True;
{$EndIf}
end;

function TUnConsultasWeb.ConsultaCPF(EdNome, EdCPF: TdmkEdit): Boolean;
begin
{$IfNDef SemEntidade1}  Result := False;
  //
  if DBCheck.CriaFm(TFmEntiRFCPF, FmEntiRFCPF, afmoNegarComAviso) then
  begin
    FmEntiRFCPF.FCPF := Geral.SoNumero_TT(EdCPF.ValueVariant);
    FmEntiRFCPF.ShowModal;
    //
    if EdNome <> nil then
      EdNome.ValueVariant := FmEntiRFCPF.FNome;
    if EdCPF <> nil then
      EdCPF.ValueVariant := FmEntiRFCPF.FCPF;
    //
    FmEntiRFCPF.Destroy;
    //
    if (EdNome <> nil) and (EdNome.Enabled) and (EdNome.ValueVariant = '') then
    begin
      EdNome.SetFocus;
      Exit;
    end;
    //
    if (EdCPF <> nil) and (EdCPF.Enabled) then
      EdCPF.SetFocus;
    Result := True;
  end;
{$EndIf}
end;

function TUnConsultasWeb.ConsultaIE(UF: String; EdCNPJ, EdIE, EdRazaoSocial,
  EdFantasia, EdCEP, EdRua, EdLograd, EdCompl, EdNumero, EdBairro, EdCidade,
  EdUF, EdPais: TdmkEdit; CBLograd, CBCodMunici, CBCodiPais,
  CBCodCNAE: TdmkDBLookupComboBox; EdCodMunici, EdCodiPais,
  EdCodCNAE: TdmkEditCB; EdTelefone, EdEmail, EdSUFRAMA: TdmkEdit): Boolean;

  function ObtemCampoCNPJSINTEGRA(UF: String): String;
  begin
    Result := '';
    //
    if UpperCase(UF) = 'PR' then
      Result := 'Sintegra1Cnpj';
  end;

var
  URL, CNPJ: String;
  TipoImport: Integer;
  (*Navegador: TdmkNavegadores;*)
begin
{$IfNDef SemEntidade1}
  Result := False;
  //
  URL := ObtemURLdoSINTEGRA(UF, False);
  if URL = '' then
  begin
    Geral.MB_Erro('URL da UF n�o definida!');
    Exit;
  end;
  if DBCheck.CriaFm(TFmEntiRE_IE, FmEntiRE_IE, afmoNegarComAviso) then
  begin
    ObtemTagSINTEGRA(UF, FmEntiRE_IE.LaCNPJ, FmEntiRE_IE.LaIE,
      FmEntiRE_IE.LaRazaoSocial, FmEntiRE_IE.LaLogradouro, FmEntiRE_IE.LaNumero,
      FmEntiRE_IE.LaComplemento, FmEntiRE_IE.LaBairro, FmEntiRE_IE.LaMunicipio,
      FmEntiRE_IE.LaCEP, FmEntiRE_IE.LaUF, FmEntiRE_IE.LaTelefone,
      FmEntiRE_IE.LaEMail, FmEntiRE_IE.LaCNAE, FmEntiRE_IE.LaFantasia,
      FmEntiRE_IE.LaSUFRAMA);
    //
    (*
    if Uppercase(UF) = 'PB' then
      Navegador := naviChromium;
    *)
    //
    if EdCNPJ <> nil then
      CNPJ := Geral.SoNumero_TT(EdCNPJ.ValueVariant)
    else
      CNPJ := '';
    //
    FmEntiRE_IE.FUFConsulta   := Uppercase(UF);
    FmEntiRE_IE.CBRE.Text     := Uppercase(UF);
    FmEntiRE_IE.EdURL_WB.Text := URL;
    FmEntiRE_IE.FCNPJ         := CNPJ;
    FmEntiRE_IE.FCNPJCampo    := ObtemCampoCNPJSINTEGRA(Uppercase(UF));
    (*
    if Navegador = naviChromium then
      FmEntiRE_IE.RGNavegador.ItemIndex := 1
    else
      FmEntiRE_IE.RGNavegador.ItemIndex := 0;
    *)
    //  ini 2021-11-19
    //if UF = 'SP' then
      //FmEntiRE_IE.RGNavegador.ItemIndex := 0;
    //  fim 2021-11-19
    FmEntiRE_IE.ShowModal;
    //
    if FmEntiRE_IE.FImporta then
    begin
      Screen.Cursor := crHourGlass;
      try
        if EdCNPJ <> nil then
          EdCNPJ.ValueVariant := FmEntiRE_IE.FCNPJ;
        if EdRazaoSocial <> nil then
          EdRazaoSocial.ValueVariant := Trim(FmEntiRE_IE.FRazaoSocial);
        if EdFantasia <> nil then
          EdFantasia.ValueVariant := Trim(FmEntiRE_IE.FFantasia);
        if EdCEP <> nil then
          EdCEP.ValueVariant := FmEntiRE_IE.FCEP;
        if EdLograd <> nil then
          EdLograd.ValueVariant := Geral.FF0(FmEntiRE_IE.FTipoLograd);
        if CBLograd <> nil then
          CBLograd.KeyValue := FmEntiRE_IE.FTipoLograd;
        if EdRua <> nil then
          EdRua.ValueVariant := FmEntiRE_IE.FRua;
        if EdCidade <> nil then
          EdCidade.ValueVariant := Geral.Maiusculas(FmEntiRE_IE.FCidade, True);
        if EdCodMunici <> nil then
          EdCodMunici.ValueVariant := ObtemCodigoDTBMunici(FmEntiRE_IE.FCidade,
                                        FmEntiRE_IE.FUF);
        if CBCodMunici <> nil then
          CBCodMunici.KeyValue := ObtemCodigoDTBMunici(FmEntiRE_IE.FCidade,
                                    FmEntiRE_IE.FUF);
        if EdPais <> nil then
          EdPais.ValueVariant := 'BRASIL';
        if EdCodiPais <> nil then
          EdCodiPais.ValueVariant := 1058;
        if CBCodiPais <> nil then
          CBCodiPais.KeyValue := 1058;
        if EdUF <> nil then
          EdUF.ValueVariant := UpperCase(FmEntiRE_IE.FUF);
        if EdBairro <> nil then
          EdBairro.ValueVariant := Geral.Maiusculas(FmEntiRE_IE.FBairro, True);
        if EdCodCNAE <> nil then
          EdCodCNAE.ValueVariant := FmEntiRE_IE.FCNAE;
        if CBCodCNAE <> nil then
          CBCodCNAE.KeyValue := FmEntiRE_IE.FCNAE;
        if EdNumero <> nil then
          EdNumero.Text := Geral.SoNumero_TT(FmEntiRE_IE.FNumero);
        if EdCompl <> nil then
          EdCompl.ValueVariant := FmEntiRE_IE.FCompl;
        if EdIE <> nil then
        begin
          if FmEntiRE_IE.FUF <> EmptyStr then
          begin
            EdUF.Text         := Uppercase(FmEntiRE_IE.FUF);
            EdIE.LinkMsk      := UpperCase(FmEntiRE_IE.FUF);
          end else
          if UF <> EmptyStr then
          begin
            EdUF.Text         := Uppercase(UF);
            EdIE.LinkMsk      := Uppercase(UF);
          end;
          try
            EdIE.ValueVariant := FmEntiRE_IE.FIE;
          except
            ;
          end;
        end;
        if EdTelefone <> nil then
          EdTelefone.ValueVariant := FmEntiRE_IE.FTelefone;
        if EdEmail <> nil then
          EdEmail.ValueVariant := FmEntiRE_IE.FEmail;
        if EdSUFRAMA <> nil then
          EdSUFRAMA.ValueVariant := FmEntiRE_IE.FSUFRAMA;
        //
        TipoImport := FmEntiRE_IE.FTipoImport;
        //
        FmEntiRE_IE.Destroy;
        //
        if TipoImport = 0 then
        begin
          U_CEP.ConsultaCEP(EdCEP, EdLograd, EdRua, EdNumero, EdBairro,
            EdCidade, EdUF, EdPais, EdNumero, EdCompl, CBLograd,
            EdCodMunici, CBCodMunici, EdCodiPais, CBCodiPais);
        end else
        begin
          if EdIE.Enabled then
            EdIE.SetFocus;
        end;
        Result := True;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
{$EndIf}
end;

function TUnConsultasWeb.ConsultaCCC_SVRS(CodUF_IBGE: Integer; UF: String;
  EdCNPJ, EdIE, EdRazaoSocial,
  EdFantasia, EdCEP, EdRua, EdLograd, EdCompl, EdNumero, EdBairro, EdCidade,
  EdUF, EdPais: TdmkEdit; CBLograd, CBCodMunici, CBCodiPais,
  CBCodCNAE: TdmkDBLookupComboBox; EdCodMunici, EdCodiPais,
  EdCodCNAE: TdmkEditCB; EdTelefone, EdEmail, EdSUFRAMA: TdmkEdit;
  TPENatal: TdmkEditDateTimePicker): Boolean;

(*
  function ObtemCampoCNPJSINTEGRA(UF: String): String;
  begin
    Result := '';
    //
    if UpperCase(UF) = 'PR' then
      Result := 'Sintegra1Cnpj';
  end;
*)
var
(*
  TipoImport: Integer;
  URL,
*)
  CNPJ, IE: String;
begin
{$IfNDef SemEntidade1}
  Result := False;
  //
(*
  URL := ObtemURLdoSINTEGRA(UF);
  if URL = '' then
  begin
    Geral.MB_Erro('URL da UF n�o definida!');
    Exit;
  end;
*)
  if DBCheck.CriaFm(TFmEntiRE_IE, FmEntiRE_IE, afmoNegarComAviso) then
  begin
    FmEntiRE_IE.CkCCC.Checked := True;
    FmEntiRE_IE.FCodUF_IBGE := CodUF_IBGE;
(*
    ObtemTagSINTEGRA(UF, FmEntiRE_IE.LaCNPJ, FmEntiRE_IE.LaIE,
      FmEntiRE_IE.LaRazaoSocial, FmEntiRE_IE.LaLogradouro, FmEntiRE_IE.LaNumero,
      FmEntiRE_IE.LaComplemento, FmEntiRE_IE.LaBairro, FmEntiRE_IE.LaMunicipio,
      FmEntiRE_IE.LaCEP, FmEntiRE_IE.LaUF, FmEntiRE_IE.LaTelefone,
      FmEntiRE_IE.LaEMail, FmEntiRE_IE.LaCNAE, FmEntiRE_IE.LaFantasia,
      FmEntiRE_IE.LaSUFRAMA);
    //
*)
    if EdCNPJ <> nil then
    begin
      CNPJ := Geral.SoNumero_TT(EdCNPJ.ValueVariant);
      IE := '';
    end
    else
    begin
      CNPJ := '';
      //
      if EdIE <> nil then
        IE := Geral.SoNumero_TT(EdIE.ValueVariant);
    end;
    //
    FmEntiRE_IE.FUFConsulta   := Uppercase(UF);
    FmEntiRE_IE.CBRE.Text     := Uppercase(UF);
    //FmEntiRE_IE.EdURL_WB.Text := URL;
    FmEntiRE_IE.FCNPJ         := CNPJ;
    FmEntiRE_IE.FIE           := IE;
    FmEntiRE_IE.FCNPJCampo    := ''; //ObtemCampoCNPJSINTEGRA(Uppercase(UF));
    FmEntiRE_IE.ShowModal;
    //
    if FmEntiRE_IE.FImporta then
    begin
      Screen.Cursor := crHourGlass;
      try
        if EdCNPJ <> nil then
          EdCNPJ.ValueVariant := FmEntiRE_IE.FCNPJ;
        if EdRazaoSocial <> nil then
          EdRazaoSocial.ValueVariant := Trim(FmEntiRE_IE.FRazaoSocial);
        if EdFantasia <> nil then
          EdFantasia.ValueVariant := Trim(FmEntiRE_IE.FFantasia);
        if EdCEP <> nil then
          EdCEP.ValueVariant := FmEntiRE_IE.FCEP;
        if EdLograd <> nil then
          EdLograd.ValueVariant := Geral.FF0(FmEntiRE_IE.FTipoLograd);
        if CBLograd <> nil then
          CBLograd.KeyValue := FmEntiRE_IE.FTipoLograd;
        if EdRua <> nil then
          EdRua.ValueVariant := FmEntiRE_IE.FRua;
        if EdCidade <> nil then
          EdCidade.ValueVariant := Geral.Maiusculas(FmEntiRE_IE.FCidade, True);
        if EdCodMunici <> nil then
        begin
          if FmEntiRE_IE.FCodMunici <> 0 then
            EdCodMunici.ValueVariant := FmEntiRE_IE.FCodMunici
          else
            EdCodMunici.ValueVariant := ObtemCodigoDTBMunici(FmEntiRE_IE.FCidade,
                                        FmEntiRE_IE.FUF);
        end;
        if CBCodMunici <> nil then
          CBCodMunici.KeyValue := ObtemCodigoDTBMunici(FmEntiRE_IE.FCidade,
                                    FmEntiRE_IE.FUF);
        if EdPais <> nil then
          EdPais.ValueVariant := 'BRASIL';
        if EdCodiPais <> nil then
          EdCodiPais.ValueVariant := 1058;
        if CBCodiPais <> nil then
          CBCodiPais.KeyValue := 1058;
        if EdUF <> nil then
          EdUF.ValueVariant := UpperCase(FmEntiRE_IE.FUF);
        if EdBairro <> nil then
          EdBairro.ValueVariant := Geral.Maiusculas(FmEntiRE_IE.FBairro, True);
        if EdCodCNAE <> nil then
          EdCodCNAE.ValueVariant := FmEntiRE_IE.FCNAE;
        if CBCodCNAE <> nil then
          CBCodCNAE.KeyValue := FmEntiRE_IE.FCNAE;
        if EdNumero <> nil then
          EdNumero.Text := Geral.SoNumero_TT(FmEntiRE_IE.FNumero);
        if EdCompl <> nil then
          EdCompl.ValueVariant := FmEntiRE_IE.FCompl;
        if EdIE <> nil then
        begin
          if FmEntiRE_IE.FUF <> EmptyStr then
          begin
            EdUF.Text         := Uppercase(FmEntiRE_IE.FUF);
            EdIE.LinkMsk      := UpperCase(FmEntiRE_IE.FUF);
          end else
          if UF <> EmptyStr then
          begin
            EdUF.Text         := Uppercase(UF);
            EdIE.LinkMsk      := Uppercase(UF);
          end;
          try
            EdIE.ValueVariant := FmEntiRE_IE.FIE;
          except
            ;
          end;
        end;
        if EdTelefone <> nil then
          EdTelefone.ValueVariant := FmEntiRE_IE.FTelefone;
        if EdEmail <> nil then
          EdEmail.ValueVariant := FmEntiRE_IE.FEmail;
        if EdSUFRAMA <> nil then
          EdSUFRAMA.ValueVariant := FmEntiRE_IE.FSUFRAMA;
        //
(*
        TipoImport := FmEntiRE_IE.FTipoImport;
        //
        FmEntiRE_IE.Destroy;
        //
        if TipoImport = 0 then
        begin
          U_CEP.ConsultaCEP(EdCEP, EdLograd, EdRua, EdNumero, EdBairro,
            EdCidade, EdUF, EdPais, EdNumero, EdCompl, CBLograd,
            EdCodMunici, CBCodMunici, EdCodiPais, CBCodiPais);
        end else
        begin
          if EdIE.Enabled then
            EdIE.SetFocus;
        end;
*)
        if (TPENatal <> nil) and (FmEntiRE_IE.FENatal > 2) then
          TPENatal.Date := FmEntiRE_IE.FENatal;
        Result := True;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
{$EndIf}
end;

{$IfNDef SemEntidade}
procedure TUnConsultasWeb.GoogleMaps_CarregaModosDeDeslocamento(
  Combo: TComboBox);
begin
  Combo.Items.Clear;
  Combo.Items.Add('Rotas de tr�nsito padr�o usando a rede rodovi�ria'); //DRIVING
  Combo.Items.Add('Rotas de bicicleta por ciclovias e ruas de prefer�ncia'); //BICYCLING
  Combo.Items.Add('Rotas por trajetos de transporte p�blico'); //TRANSIT
  Combo.Items.Add('Rotas a p� por faixas de pedestre e cal�adas'); //WALKING
  //
  Combo.ItemIndex := 0;
end;
{$EndIf} // SemEntidade

{$IfNDef SemEntidade}
function TUnConsultasWeb.GoogleMaps_TraduzModoDeDeslocamento(
  Modo: Integer): String;
begin
  case Modo of
    0:
      Result := 'DRIVING';
    1:
      Result := 'BICYCLING';
    2:
      Result := 'TRANSIT';
    3:
      Result := 'WALKING';
    else
      Result := '';
  end;
end;
{$EndIf} // SemEntidade

{$IfNDef SemEntidade}
procedure TUnConsultasWeb.MapasDeEntidades(TipoForm: TFormCadEnti;
  EnderecoLatLong, Descricao: array of string);
var
  i: Integer;
begin
{$IfNDef SemEntidade1}
  if DmkWeb.RemoteConnection then
  begin
    if Length(Descricao) <> Length(EnderecoLatLong) then
    begin
      Geral.MB_Aviso('A quantidade de registros das vari�veis Descricao e EnderecoLatLong devem ser iguais!');
      Exit;
    end;
    if DBCheck.CriaFm(TFmEntiMapa, FmEntiMapa, afmoLiberado) then
    begin
      FmEntiMapa.LBEndereco.Items.Clear;
      FmEntiMapa.LBDescricao.Items.Clear;
      if Length(EnderecoLatLong) > 0 then
      begin
        for i := Low(EnderecoLatLong) to High(EnderecoLatLong) do
        begin
          if EnderecoLatLong[i] <> '' then
          begin
            FmEntiMapa.LBEndereco.Items.Add(Geral.SemAcento(EnderecoLatLong[i]));
            FmEntiMapa.LBDescricao.Items.Add(Descricao[i]);
          end;
        end;
      end;
      FmEntiMapa.CBModoViagem.ItemIndex    := 0;
      FmEntiMapa.CkDirectionsPanel.Checked := True;
      FmEntiMapa.FTipoForm                 := TipoForm;
      //
      if Length(EnderecoLatLong) > 0 then
        FmEntiMapa.CarregaMapa();
      //
      FmEntiMapa.ShowModal;
      FmEntiMapa.Destroy;
    end;
  end else
    Geral.MB_Aviso('Seu computador n�o est� conectado na internet!' +
      sLineBreak + 'Estabele�a uma conex�o com a internet e tente novamente!');
{$EndIf}
end;
{$EndIf} // SemEntidade

{$IfNDef SemEntidade}
procedure TUnConsultasWeb.MapasDeEntidadesMontaEndereco(TipoForm: TFormCadEnti;
  Entidade: Integer; var MapaEndereco, MapaDescri: string);
var
  QrLoc: TmySQLQuery;
  Nome, Rua, Cidade, UF, Pais: String;
  Numero: Integer;
begin
  if (Entidade <> 0) and (TipoForm <> Null) then
  begin
    QrLoc := TmySQLQuery.Create(Dmod);
    try
      case TipoForm of
        fmcadEntidades:
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
            'SELECT ent.Codigo,',
            'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME,',
            'IF (ent.Tipo=0, ent.ERua, ent.PRua) RUA,',
            'IF (ent.Tipo=0, ent.ENumero, ent.PNumero) NUMERO,',
            'euf.Nome UF,',
            'IF (ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,',
            'IF (ent.Tipo=0, ent.EPais, ent.PPais) PAIS',
            'FROM entidades ent',
            'LEFT JOIN ufs euf ON euf.Codigo=IF (ent.Tipo=0, ent.EUF, ent.PUF)',
            'WHERE ent.Codigo=' + Geral.FF0(Entidade),
            '']);
          Nome   := QrLoc.FieldByName('NOME').AsString;
          Rua    := QrLoc.FieldByName('RUA').AsString;
          Numero := QrLoc.FieldByName('NUMERO').AsInteger;
          Cidade := QrLoc.FieldByName('CIDADE').AsString;
          UF     := QrLoc.FieldByName('UF').AsString;
          Pais   := QrLoc.FieldByName('PAIS').AsString;
          //
          if Length(Nome) > 0 then
            MapaDescri := Nome;
          if Length(Rua) > 0 then
            MapaEndereco := Rua + ' ' + Geral.FF0(Numero) + ' ';
          if Length(Cidade) > 0 then
            MapaEndereco := MapaEndereco + Cidade + ' ';
          if Length(UF) > 0 then
            MapaEndereco := MapaEndereco + UF + ' ';
          if Length(Pais) > 0 then
            MapaEndereco := MapaEndereco + Pais;
        end;
        fmcadEntidade2:
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
            'SELECT ent.Codigo,',
            'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME,',
            'IF (ent.Tipo=0, ent.ERua, ent.PRua) RUA,',
            'IF (ent.Tipo=0, ent.ENumero, ent.PNumero) NUMERO,',
            'euf.Nome UF, mue.Nome CIDADE,',
            'pae.Nome PAIS',
            'FROM entidades ent',
            'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mue ON mue.Codigo = IF (ent.Tipo=0, ent.ECodMunici, ent.PCodMunici)',
            'LEFT JOIN ufs euf ON euf.Codigo=IF (ent.Tipo=0, ent.EUF, ent.PUF)',
            'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pae ON pae.Codigo = IF (ent.Tipo=0, ent.ECodiPais, ent.PCodiPais)',
            'WHERE ent.Codigo=' + Geral.FF0(Entidade),
            '']);
          Nome   := QrLoc.FieldByName('NOME').AsString;
          Rua    := QrLoc.FieldByName('RUA').AsString;
          Numero := QrLoc.FieldByName('NUMERO').AsInteger;
          Cidade := QrLoc.FieldByName('CIDADE').AsString;
          UF     := QrLoc.FieldByName('UF').AsString;
          Pais   := QrLoc.FieldByName('PAIS').AsString;
          //
          if Length(Nome) > 0 then
            MapaDescri := Nome;
          if Length(Rua) > 0 then
            MapaEndereco := Rua + ' ' + Geral.FF0(Numero) + ' ';
          if Length(Cidade) > 0 then
            MapaEndereco := MapaEndereco + Cidade + ' ';
          if Length(UF) > 0 then
            MapaEndereco := MapaEndereco + UF + ' ';
          if Length(Pais) > 0 then
            MapaEndereco := MapaEndereco + Pais;
        end;
      end;
    finally
      QrLoc.Free;
    end;
  end;
end;
{$EndIf} // SemEntidade

function TUnConsultasWeb.ObtemCodigoDTBMunici(Cidade, UF: String): Integer;
var
  QrLoc: TmySQLQuery;
begin
  QrLoc := TmySQLQuery.Create(Dmod);
  QrLoc.Close;
  QrLoc.Database := DModG.AllID_DB;
  QrLoc.SQL.Clear;
  QrLoc.SQL.Add('SELECT mun.Codigo, mun.Nome');
  QrLoc.SQL.Add('FROM dtb_munici mun');
  QrLoc.SQL.Add('WHERE LEFT(mun.Codigo, 2)="' +
    Geral.FF0(Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(UF)) + '"');
  QrLoc.SQL.Add('AND mun.Nome="' + Trim(Cidade) + '"');
  UnDmkDAC_PF.AbreQuery(QrLoc, DModG.AllID_DB);
  if QrLoc.RecordCount > 0 then
    Result := QrLoc.FieldByName('Codigo').AsInteger
  else
    Result := 0;
  QrLoc.Free;
end;

function TUnConsultasWeb.ObtemTagSINTEGRA(UF: String; LaCNPJ, LaIE,
  LaRazaoSocial, LaLogradouro, LaNumero, LaComplemento, LaBairro, LaMunicipio,
  LaCEP, LaUF, LaTelefone, LaEMail, LaCNAE, LaFantasia, LaSUFRAMA: TLabel): Boolean;

{
  procedure AvisoChrome(UF: String);
  begin
    Geral.MB_Info('Na data do desenvolvimento deste procedimento ' +
    '(27 a 28 nov 2012) o site da SEFAZ da UF ' + UF +
    ' estava com o certificado digital expirado!' +
    sLineBreak + 'Por isso o navegador inicial ser� o Chrome (R)!' +
    sLineBreak + 'Caso o site voltou a funcionar, informe a DERMATEK!');
  end;

  procedure AvisoForaDoAr(UF: String);
  begin
    Geral.MB_Info('Na data do desenvolvimento deste procedimento ' +
    '(27 a 28 nov 2012) o site da SEFAZ da UF ' + UF + ' estava fora do ar!' +
    sLineBreak + 'Caso o site voltou a funcionar, informe a DERMATEK!');
  end;

  procedure AvisoSemRetorno(UF, Mensagem: String);
  begin
    Geral.MB_Info('Na data do desenvolvimento deste procedimento ' +
    '(27 a 28 nov 2012) o site da SEFAZ da UF ' + UF +
    ' retornava a seguinte mensagem de erro:' + sLineBreak +
    Mensagem + sLineBreak +
    'Caso o site voltou a funcionar corretamente, informe a DERMATEK!');
  end;

  procedure AvisoDermatek(UF, Mensagem: String);
  begin
    Geral.MB_Info('Na data do desenvolvimento deste procedimento ' +
    '(27 a 28 nov 2012) a DERMATEK encontrou a seguinte dificuldade ' +
    'no site da SEFAZ da UF ' + UF + ':' + sLineBreak +
    Mensagem + sLineBreak +
    'Caso o site voltou a funcionar corretamente, informe a DERMATEK!');
  end;
}

begin
  Result := True;
  if Uppercase(UF) = 'AC' then
  begin
    LaCNPJ.Caption        := 'Cgc:';
    LaIE.Caption          := 'Inscri��o Estadual:';
    LaRazaoSocial.Caption := 'Raz�o Social';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := 'Logradouro:';
    LaNumero.Caption      := 'N�mero:';
    LaComplemento.Caption := 'Complemento:';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Munc�pio:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := '';
    LaCNAE.Caption        := '';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'AL' then
  begin
    LaCNPJ.Caption        := 'CNPJ:';
    LaIE.Caption          := 'Inscri��o Estadual:';
    LaRazaoSocial.Caption := 'Raz�o Social:';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := 'Logradouro:';
    LaNumero.Caption      := 'N�mero:';
    LaComplemento.Caption := 'Complemento:';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Munic�pio:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := 'Endere�o Eletr�nico:';
    LaCNAE.Caption        := 'Atividade Econ�mica:';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'AM' then
  begin
    LaCNPJ.Caption        := 'C.N.P.J:';
    LaIE.Caption          := 'Inscri��o Estadual:';
    LaRazaoSocial.Caption := 'Raz�o Social:';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := 'Logradouro:';
    LaNumero.Caption      := 'N�mero:';
    LaComplemento.Caption := 'Complemento:';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Munic�pio:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := '';
    LaCNAE.Caption        := 'CNAE Principal :';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'AP' then
  begin
    LaCNPJ.Caption        := 'CNPJ:';
    LaIE.Caption          := 'Inscri��o Estadual:';
    LaRazaoSocial.Caption := 'Raz�o Social:';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := 'Logradouro:';
    LaNumero.Caption      := 'N�mero:';
    LaComplemento.Caption := 'Complemento:';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Municipio';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := '';
    LaCNAE.Caption        := '';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'BA' then
  begin
    LaCNPJ.Caption        := 'CNPJ:';
    LaIE.Caption          := 'Inscri��o Estadual:';
    LaRazaoSocial.Caption := 'Raz�o Social:';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := 'Logradouro:';
    LaNumero.Caption      := 'N�mero:';
    LaComplemento.Caption := 'Complemento:';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Munic�pio:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := 'Endere�o Eletr�nico:';
    LaCNAE.Caption        := 'Atividade Econ�mica:';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'CE' then
  begin
    LaCNPJ.Caption        := 'CNPJ:';
    LaIE.Caption          := 'Inscri��o Estadual:';
    LaRazaoSocial.Caption := 'Raz�o Social:';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := 'Logradouro:';
    LaNumero.Caption      := 'N�mero:';
    LaComplemento.Caption := 'Complemento:';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Munic�pio:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := '';
    LaCNAE.Caption        := '';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'DF' then
  begin
    LaCNPJ.Caption        := 'CNPJ/CPF';
    LaIE.Caption          := 'CF/DF';
    LaRazaoSocial.Caption := 'Raz�o Social';
    LaFantasia.Caption    := 'Nome Fantasia';
    LaLogradouro.Caption  := 'Logradouro';
    LaNumero.Caption      := 'N�mero';
    LaComplemento.Caption := 'Complemento';
    LaBairro.Caption      := 'Bairro';
    LaMunicipio.Caption   := 'Munic�pio';
    LaCEP.Caption         := 'CEP';
    LaUF.Caption          := 'UF';
    LaTelefone.Caption    := 'Telefone';
    LaEMail.Caption       := '';
    LaCNAE.Caption        := 'Atividade Principal';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'ES' then
  begin
    LaCNPJ.Caption        := 'CNPJ:';
    LaIE.Caption          := 'Inscri��o Estadual:';
    LaRazaoSocial.Caption := 'Raz�o Social :';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := 'Logradouro:';
    LaNumero.Caption      := 'N�mero:';
    LaComplemento.Caption := 'Complemento:';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Munic�pio:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := '';
    LaCNAE.Caption        := 'Atividade Econ�mica:';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'GO' then
  begin
    LaCNPJ.Caption        := 'CNPJ:';
    LaIE.Caption          := 'Inscri��o Estadual - CCE :';
    LaRazaoSocial.Caption := 'Nome Empresarial:';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := 'Logradouro:';
    LaNumero.Caption      := 'N�mero:';
    LaComplemento.Caption := 'Complemento:';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Munic�pio:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := '';
    LaCNAE.Caption        := 'Atividade Econ�mica:';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'MA' then
  begin
    LaCNPJ.Caption        := 'CGC:';
    LaIE.Caption          := 'Inscri��o Estadual:';
    LaRazaoSocial.Caption := 'Raz�o Social:';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := 'Logradouro:';
    LaNumero.Caption      := 'N�mero:';
    LaComplemento.Caption := 'Complemento:';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Munic�pio:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := '';
    LaCNAE.Caption        := 'CNAE Principal:';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'MS' then
  begin
    LaCNPJ.Caption        := 'CNPJ';
    LaIE.Caption          := 'N�MERO DE INSCRI��O';
    LaRazaoSocial.Caption := 'RAZ�O SOCIAL / NOME';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := 'LOGRADOURO';
    LaNumero.Caption      := 'N�MERO';
    LaComplemento.Caption := 'COMPLEMENTO';
    LaBairro.Caption      := 'BAIRRO';
    LaMunicipio.Caption   := 'MUNIC�PIO';
    LaCEP.Caption         := 'CEP';
    LaUF.Caption          := 'UF';
    LaTelefone.Caption    := '';
    LaEMail.Caption       := '';
    LaCNAE.Caption        := 'DESCRI��O DA ATIVIDADE ECON�MICA';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'MT' then
  begin
    LaCNPJ.Caption        := 'CPF/CNPJ:';
    LaIE.Caption          := 'Inscri��o estadual:';
    LaRazaoSocial.Caption := 'Raz�o social:';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := 'Logradouro:';
    LaNumero.Caption      := 'N�mero:';
    LaComplemento.Caption := 'Complemento:';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Munic�pio/UF:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := '';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := '';
    LaCNAE.Caption        := 'CNAE Fiscal:';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'MG' then
  begin
    LaCNPJ.Caption        := 'CNPJ:';
    LaIE.Caption          := 'Inscri��o Estadual:';
    LaRazaoSocial.Caption := 'Nome Empresarial:';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := 'Logradouro:';
    LaNumero.Caption      := 'N�mero:';
    LaComplemento.Caption := 'Complemento:';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Munic�pio:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := '';
    LaCNAE.Caption        := 'CNAE-F Principal:';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'PA' then
  begin
    LaCNPJ.Caption        := 'CNPJ:';
    LaIE.Caption          := 'Inscri��o Estadual:';
    LaRazaoSocial.Caption := 'Raz�o Social:';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := 'Logradouro:';
    LaNumero.Caption      := 'N�mero:';
    LaComplemento.Caption := 'Complemento:';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Munic�pio:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := 'Endere�o Eletr�nico:';
    LaCNAE.Caption        := 'Principal:';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'PB' then
  begin
    //AvisoChrome('PB');
    //
    LaCNPJ.Caption        := 'CNPJ:';
    LaIE.Caption          := 'Inscri��o Estadual:';
    LaRazaoSocial.Caption := 'Raz�o Social:';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := 'Logradouro:';
    LaNumero.Caption      := 'N�mero:';
    LaComplemento.Caption := 'Complemento:';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Munic�pio:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := '';
    LaCNAE.Caption        := 'Atividade Econ�mica:';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'PE' then
  begin
    //AvisoDermatek('PE', 'FRAMES ILEG�VEIS');
    //
    LaCNPJ.Caption        := 'CNPJ/CPF:';
    LaIE.Caption          := 'Inscri��o Estadual:';
    LaRazaoSocial.Caption := 'Raz�o Social:';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := 'Logradouro:';
    LaNumero.Caption      := 'N�mero:';
    LaComplemento.Caption := 'Complemento:';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Munic�pio:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := '';
    LaCNAE.Caption        := 'Atividade Econ�mica:';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'PI' then
  begin
    Geral.MB_Info('O estado do Piau� (PI) n�o possui mais consulta pelo SINTEGRA!');
(*
    LaCNPJ.Caption        := 'CNPJ:';
    LaIE.Caption          := 'Inscri��o Estadual:';
    LaRazaoSocial.Caption := 'Raz�o Social:';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := 'Logradouro:';
    LaNumero.Caption      := 'N�mero:';
    LaComplemento.Caption := 'Complemento:';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Munic�pio:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := 'Endere�o Eletr�nico:';
    LaCNAE.Caption        := 'CNAE Prim�rio:';
    LaSUFRAMA.Caption     := '';
*)
  end else
  if Uppercase(UF) = 'PR' then
  begin
    LaCNPJ.Caption        := 'CNPJ:';
    LaIE.Caption          := 'Inscri��o Estadual:';
    LaRazaoSocial.Caption := 'Nome Empresarial:';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := 'Logradouro:';
    LaNumero.Caption      := 'N�mero:';
    LaComplemento.Caption := 'Complemento:';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Munic�pio:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := 'E-mail:';
    LaCNAE.Caption        := 'Atividade Econ�mica Principal:';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'RJ' then
  begin
    LaCNPJ.Caption        := 'CNPJ:';
    LaIE.Caption          := 'Inscri��o Estadual:';
    LaRazaoSocial.Caption := 'Raz�o  Social:';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := 'Logradouro:';
    LaNumero.Caption      := 'N�mero:';
    LaComplemento.Caption := 'Complemento:';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Munic�pio:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := '';
    LaCNAE.Caption        := 'Atividade  Econ�mica:';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'RN' then
  begin
//  AV AMINTAS BARROS 2879, DIX-SEPT ROSADO - 59062-255 NATAL/RN
    //
    LaCNPJ.Caption        := 'CPF/CNPJ:';
    LaIE.Caption          := 'Inscri��o Estadual:';
    LaRazaoSocial.Caption := 'Raz�o Social:';
    LaFantasia.Caption    := 'Nome Fantasia:';
    LaLogradouro.Caption  := 'Endere�o do Estabelecimento:';
    LaNumero.Caption      := 'N�mero';
    LaComplemento.Caption := '';
    LaBairro.Caption      := '';
    LaMunicipio.Caption   := 'Cidade:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := '';
    LaCNAE.Caption        := 'Atividade Econ�mica (CNAE) Principal:';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'RO' then
  begin
    LaCNPJ.Caption        := 'C.P.F/C.N.P.J:';
    LaIE.Caption          := 'Inscri��o Estadual:';
    LaRazaoSocial.Caption := 'Raz�o Social:';
    LaFantasia.Caption    := 'Nome Fantasia:';
    LaLogradouro.Caption  := 'Endere�o:';
    LaNumero.Caption      := 'N�mero:';
    LaComplemento.Caption := 'Complemento:';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Munic�pio:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := 'E-mail:';
    LaCNAE.Caption        := '';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'RR' then
  begin
    LaCNPJ.Caption        := 'CNPJ:';
    LaIE.Caption          := 'Inscri��o Estadual';
    LaRazaoSocial.Caption := 'Raz�o Social:';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := 'Logradouro:';
    LaNumero.Caption      := 'N�mero:';
    LaComplemento.Caption := 'Complemento:';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Munic�pio:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := '';
    LaCNAE.Caption        := '';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'RS' then
  begin
    LaCNPJ.Caption        := 'CNPJ';
    LaIE.Caption          := 'CAD ICMS';
    LaRazaoSocial.Caption := 'Raz�o Social';
    LaFantasia.Caption    := 'Nome Fantasia';
    LaLogradouro.Caption  := 'Logradouro';
    LaNumero.Caption      := 'N�mero';
    LaComplemento.Caption := 'Complemento';
    LaBairro.Caption      := 'Bairro';
    LaMunicipio.Caption   := 'Munic�pio';
    LaCEP.Caption         := 'CEP';
    LaUF.Caption          := 'UF';
    LaTelefone.Caption    := 'Telefone';
    LaEMail.Caption       := '';
    LaCNAE.Caption        := 'CAE';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'SC' then
  begin
    LaCNPJ.Caption        := 'CPF/CNPJ:';
    LaIE.Caption          := 'Inscri��o Estadual:';
    LaRazaoSocial.Caption := 'Nome/Raz�o Estadual:';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := 'Logradouro:';
    LaNumero.Caption      := 'N�mero:';
    LaComplemento.Caption := 'Complemento:';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Munic�pio:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := 'Endere�o Eletr�nico:';
    LaCNAE.Caption        := 'CNAE-Fiscal Principal:';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'SE' then
  begin
    LaCNPJ.Caption        := 'CGC:';
    LaIE.Caption          := 'Inscri��o Estadual:';
    LaRazaoSocial.Caption := 'Raz�o Social:';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := 'Logradouro:';
    LaNumero.Caption      := 'N�mero:';
    LaComplemento.Caption := 'Complemento:';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Munic�pio:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := '';
    LaCNAE.Caption        := '';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'SP' then
  begin
    LaCNPJ.Caption        := 'CNPJ:';
    //LaIE.Caption          := 'Inscri��o Estadual:';
    LaIE.Caption          := 'IE:';
    //LaRazaoSocial.Caption := 'Raz�o Social:';
    LaRazaoSocial.Caption := 'Nome Empresarial:';
    LaFantasia.Caption    := '';
    //LaLogradouro.Caption  := 'Logradouro:';
    LaLogradouro.Caption  := 'Endere�o';
    //LaNumero.Caption      := 'N�mero:';
    LaNumero.Caption      := 'N�:';
    LaComplemento.Caption := 'Complemento:';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Munic�pio:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := '';
    LaEMail.Caption       := '';
    LaCNAE.Caption        := '';
    LaSUFRAMA.Caption     := '';
  end else
  if Uppercase(UF) = 'SU' then
  begin
    LaCNPJ.Caption        := 'CGC:';
    LaIE.Caption          := 'Incri��o Estadual:';
    LaRazaoSocial.Caption := 'Raz�oSocial:';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := 'Logradouro:';
    LaNumero.Caption      := 'N�mero:';
    LaComplemento.Caption := 'Complemento:';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Munic�pio:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := '';
    LaCNAE.Caption        := 'Atividade Econ�mica:';
    LaSUFRAMA.Caption     := 'Inscri��o Suframa :';
  end else
  if Uppercase(UF) = 'TO' then
  begin
    LaCNPJ.Caption        := 'CNPJ/CPF:';
    LaIE.Caption          := 'Inscri��o Estadual:';
    LaRazaoSocial.Caption := 'Raz�o Social:';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := 'Logradouro:';
    LaNumero.Caption      := 'N�mero:';
    LaComplemento.Caption := '';
    LaBairro.Caption      := 'Bairro:';
    LaMunicipio.Caption   := 'Munic�pio:';
    LaCEP.Caption         := 'CEP:';
    LaUF.Caption          := 'UF:';
    LaTelefone.Caption    := 'Telefone:';
    LaEMail.Caption       := '';
    LaCNAE.Caption        := 'Atividade Econ�mica:';
    LaSUFRAMA.Caption     := '';
  end else
  begin
    Result := False;
    Geral.MB_Aviso('UF n�o implementada para importa��o de dados do SINTEGRA!');
    //
    LaCNPJ.Caption        := '';
    LaIE.Caption          := '';
    LaRazaoSocial.Caption := '';
    LaFantasia.Caption    := '';
    LaLogradouro.Caption  := '';
    LaNumero.Caption      := '';
    LaComplemento.Caption := '';
    LaBairro.Caption      := '';
    LaMunicipio.Caption   := '';
    LaCEP.Caption         := '';
    LaUF.Caption          := '';
    LaTelefone.Caption    := '';
    LaEMail.Caption       := '';
    LaCNAE.Caption        := '';
    LaSUFRAMA.Caption     := '';
  end;
end;

function TUnConsultasWeb.ObtemURLdoSINTEGRA(UF: String; CCC: Boolean): String;
  const
    Itens: array [0..27] of String = (
    'ac http://sefaznet.ac.gov.br/sefazonline/servlet/hpfsincon                 ',
    'al http://www.sefaz.al.gov.br/asp/sintegra/sintegra.asp?estado=AL          ',
    //'am http://www.sefaz.am.gov.br/sintegra/sintegra0.asp                       ',
    'am http://online.sefaz.am.gov.br/sintegra/index.asp                          ',
    //'ap http://intranet.sre.ap.gov.br/Sintegra/                                 ',
    'ap http://www.sefaz.ap.gov.br/sate/seg/SEGf_AcessarFuncao.jsp?cdFuncao=CAD_011',
    //'ap http://www.sintegra.ap.gov.br/                                          ',
    'ba http://www.sefaz.ba.gov.br/Sintegra/sintegra.asp?estado=BA              ',
    //'ce http://www.sefaz.ce.gov.br/Sintegra/Sintegra.Asp?estado=CE              ',
    'ce https://servicos.sefaz.ce.gov.br/internet/Sintegra/Sintegra.Asp?estado=CE ',
    'df http://www.fazenda.df.gov.br/area.cfm?id_area=110                       ',
    //'es http://www.sintegra.es.gov.br/                                          ',
    'es http://www.sintegra.es.gov.br/index.php                                   ',
    //'go http://www.sefaz.go.gov.br/sintegra/sintegra.asp                        ',
    'go http://appasp.sefaz.go.gov.br/Sintegra/Consulta/default.asp?               ',
    'ma http://www.sefaz.ma.gov.br/sintegra/sintegra.asp                        ',
    'mg http://consultasintegra.fazenda.mg.gov.br                               ',
    'ms http://www1.sefaz.ms.gov.br/Cadastro/sintegra/cadastromsCCI.asp         ',
    //'ms https://servicos.efazenda.ms.gov.br/consultapublica                   ',
    //'mt http://www.sefaz.mt.gov.br/sid/consulta/infocadastral/consultar/publica ',
    'mt https://www.sefaz.mt.gov.br/cadastro/emissaocartao/emissaocartaocontribuinteacessodireto',
    'pa http://app.sefa.pa.gov.br/Sintegra/                                     ',
    //'pb http://sintegra.receita.pb.gov.br/sintegra/sintegra.asp?estado=pb       ',
    //'pb https://saplic.receita.pb.gov.br/sintegra/SINf_ConsultaSintegra.jsp     ',
    'pb https://www4.sefaz.pb.gov.br/sintegra/SINf_ConsultaSintegra.jsp',
    //'pe http://www.sintegra.sefaz.pe.gov.br                                     ',
    //'pe http://www.sefaz.pe.gov.br/sintegra/consulta/consulta.asp',
    'pe https://efisco.sefaz.pe.gov.br/sfi_trb_gcc/PRConsultarExtratoCadastroContribuinteSINTEGRA',
    //'pi http://web.sintegra.sefaz.pi.gov.br                                     ',
    'pi http://web.sintegra.sefaz.pi.gov.br/consulta_empresa_pesquisa.asp',
    'pr http://www.sintegra.fazenda.pr.gov.br/sintegra/                         ',
    //'rj http://www.fazenda.rj.gov.br/projetoCPS                                 ',
    'rj http://www4.fazenda.rj.gov.br/sincad-web/index.jsf                        ',
    //'rn http://ww3.set.rn.gov.br/sintegra                                       ',
    //'rn http://www.set.rn.gov.br/uvt/consultacontribuinte.aspx                  ',
    'rn https://uvt2.set.rn.gov.br/#/services/consultaContribuinte',
    //'ro http://www.sefin.ro.gov.br/sint_consul.asp target=main                  ',
    //'ro http://portal.intranet.sefin.ro.gov.br/PortalContribuinte/parametropublica.jsp',
    'ro https://portalcontribuinte.sefin.ro.gov.br/Publico/parametropublica.jsp   ',
    //'rr http://portalapp.sefaz.rr.gov.br/sintegra/servlet/hwsintco              ',
    'rr https://portalapp.sefaz.rr.gov.br/siate/servlet/wp_siate_consultasintegra ',
    //'rs http://sintegra.sefaz.rs.gov.br/sef_root/inf/Sintegra_Entrada.asp       ',
    'rs https://www.sefaz.rs.gov.br/consultas/contribuinte                        ',
    //'sc http://sistemas.sef.sc.gov.br/sintegra                                  ',
    'sc http://sistemas3.sef.sc.gov.br/sintegra/consulta_empresa_pesquisa.aspx    ',
    //'se http://www.sefaz.se.gov.br/sintegra target=main                         ',
    'se https://security.sefaz.se.gov.br/SIC/sintegra/index.jsp                 ',
    //'sp http://pfeserv1.fazenda.sp.gov.br/sintegrapfe/consultaSintegraServlet   ',
    'sp https://www.cadesp.fazenda.sp.gov.br/Pages/Cadastro/Consultas/ConsultaPublica/ConsultaPublica.aspx ',
    'to http://sintegra.sefaz.to.gov.br                                         ',
    'su http://www.suframa.gov.br/sintegra                                      ');
var
  I: Integer;
begin
  if CCC then
    Result := 'https://dfe-portal.svrs.rs.gov.br/Nfe/Ccc'
  else
  begin
    Result := '';
    for I := 0 to High(Itens) do
    begin
      if Lowercase(Copy(Itens[I], 1, 2)) = Lowercase(UF) then
      begin
        Result := Trim(Copy(Itens[I], 3));
        Break;
      end;
    end;
  end;
end;

{

AC 06050265000180 ok 08673273000451
AL 01372327000157 ok
AM 15809486000180 ok
AP 03872831000141 ok
BA 04608967000101 ok
CE 07247554000137 ok
DF 10339017000121 ok
ES 63460299000187 ok
GO 00022244000175 ok
MA 06017624000106 ok
MG 21495247000104 ok
MS 05022866000117 ok
MT 07599864000110 ok
PA 04333952000188 ok
PB 61079117016443 ok s� com Chrome
PE 00507949000182 ok
PI 07554626000199 ok
PR 61286647000116 ok
RJ 33163924000168 ok
RN 07189181000195 ok
RO 04596384000108 ok 35283651000110
RR 34809632000112 ok
RS 02847039000174 ok
SC 06859974000101 ok
SE 04851237000137 ok
SP 54047360000178 ok
SU 200306014      ok 
TO 29010227836    ok 00301294000191



}

end.
