unit WGerl_Tabs;

////////////////////////////////////////////////////////////////////////////////
// Formas na pasta C:\Projetos\Delphi 2007\Outros\Web\Users
////////////////////////////////////////////////////////////////////////////////

{ Colocar no MyListas:

Uses WGerl_Tabs;


//
function TMyListas.CriaListaTabelas:
  WGerl_Tb.CarregaListaTabelas(FTabelas);
//
function TMyListas.CriaListaSQL:
  WGerl_Tb.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:
  WGerl_Tb.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
  WGerl_Tb.CarregaListaFRIndices(Tabela, FRIndices, FLIndices);
//
function TMyListas.CriaListaCampos:
  WGerl_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos);
  WGerl_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  WGerl_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}
interface

uses
  System.Generics.Collections, Windows, Messages, SysUtils, Classes, Graphics,
  Controls, Dialogs, DB, (*DBTables,*) UnMyLinguas, Forms, mysqlDBTables, dmkGeral,
  UnDmkEnums, UnGrl_Vars;

type
  TWGerl_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Database: TmySQLDatabase; Lista: TList<TTabelas>;
             DMKID_APP: Integer): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle;
             DMKID_APP: Integer): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList;
             DMKID_APP: Integer): Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>): Boolean;
  end;

//const

var
  WGerl_Tb: TWGerl_Tabs;

implementation

uses UMySQLModule;

function TWGerl_Tabs.CarregaListaTabelas(Database: TmySQLDatabase;
  Lista: TList<TTabelas>; DMKID_APP: Integer): Boolean;
begin
  try
    Result := True;
    //
    if Database.DatabaseName = VAR_DBWEB then
    begin
      MyLinguas.AdTbLst(Lista, False, Lowercase('wcontrol'), '');
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('opcoesgerl'), '');
    end;
  except
    raise;
    Result := False;
  end;
end;

function TWGerl_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList; DMKID_APP: Integer): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('wcontrol') then
  begin
    if DMKID_APP = 24 then //Bustrol
    begin
      FListaSQL.Add('Codigo|PerPasCli|PerPasUsu|PerPasAdm|PerPasBos');
      FListaSQL.Add('     1|       -2|       -3|       -1|       -4');
    end else
    if DMKID_APP = 4 then //Syndi2
    begin
      FListaSQL.Add('Codigo|PerPasCli|PerPasUsu|PerPasAdm|PerPasBos');
      FListaSQL.Add('     1|        2|        1|        4|        3');
    end;
  end;
end;

function TWGerl_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
  TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('') then
  begin
  end else if Uppercase(Tabela) = Uppercase('XXXXX') then
  begin
    //FListaSQL.Add('"Entidades","Cadastro de pessoas f�sicas e jur�dicas (clientes, fornecedores, etc.)"');
    //FListaSQL.Add('"",""');
  end;
end;


function TWGerl_Tabs.CarregaListaFRIndices(Tabela: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
    if Uppercase(Tabela) = Uppercase('wcontrol') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
  end;
end;

function TWGerl_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle;
 DMKID_APP: Integer): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('wcontrol') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaSincro';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SincroEmExec';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Crypt';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MailCont';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DiasExp';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerPasCli';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerPasUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerPasAdm';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PerPasBos';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProAltPas';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProDowArq';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LogoWeb';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FacebookLogin';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      if DMKID_APP = 17 then //DControl
      begin
        New(FRCampos);
        FRCampos.Field      := 'WebDirApp'; //Diret�rio WEB para aplicativos
        FRCampos.Tipo       := 'int(11)';
        FRCampos.Null       := '';
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
        New(FRCampos);
        FRCampos.Field      := 'AppZipar';
        FRCampos.Tipo       := 'tinyint(1)';
        FRCampos.Null       := '';
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
        New(FRCampos);
        FRCampos.Field      := 'ProAtivApl';
        FRCampos.Tipo       := 'int(11)';
        FRCampos.Null       := '';
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
        New(FRCampos);
        FRCampos.Field      := 'TermoBoss';
        FRCampos.Tipo       := 'int(11)';
        FRCampos.Null       := '';
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
        New(FRCampos);
        FRCampos.Field      := 'TermoPrivaci';
        FRCampos.Tipo       := 'int(11)';
        FRCampos.Null       := '';
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
      end;
      if DMKID_APP = 4 then //Syndi2
      begin
        New(FRCampos);
        FRCampos.Field      := 'DirWebBalancete';
        FRCampos.Tipo       := 'int(11)';
        FRCampos.Null       := '';
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
        New(FRCampos);
        FRCampos.Field      := 'MsgConDiv'; //Texto confiss�o de d�vida
        FRCampos.Tipo       := 'int(11)';
        FRCampos.Null       := '';
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
        New(FRCampos);
        FRCampos.Field      := 'MsgHome'; //Texto in�cio do site
        FRCampos.Tipo       := 'int(11)';
        FRCampos.Null       := '';
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
        New(FRCampos);
        FRCampos.Field      := 'MsgAviLog'; //Texto login - Bot�o Condom�nio
        FRCampos.Tipo       := 'int(11)';
        FRCampos.Null       := '';
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
        New(FRCampos);
        FRCampos.Field      := 'MsgAviImo'; //Texto login - Bot�o Imobili�ria
        FRCampos.Tipo       := 'int(11)';
        FRCampos.Null       := '';
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
        New(FRCampos);
        FRCampos.Field      := 'MsgAviCon'; //Texto contato
        FRCampos.Tipo       := 'int(11)';
        FRCampos.Null       := '';
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
        New(FRCampos);
        FRCampos.Field      := 'MsgNoImpBlq'; //Texto para os condom�nios que n�o
        FRCampos.Tipo       := 'int(11)';     //ir�o imprimir bloquetos
        FRCampos.Null       := '';
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
        //Eliminar no futuro - In�cio
        New(FRCampos);
        FRCampos.Field      := 'ConfisDivd';
        FRCampos.Tipo       := 'text';
        FRCampos.Null       := 'YES';
        FRCampos.Key        := '';
        FRCampos.Default    := '';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
        New(FRCampos);
        FRCampos.Field      := 'BoasVindas';
        FRCampos.Tipo       := 'text';
        FRCampos.Null       := 'YES';
        FRCampos.Key        := '';
        FRCampos.Default    := '';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
        New(FRCampos);
        FRCampos.Field      := 'AvisoLogin';
        FRCampos.Tipo       := 'text';
        FRCampos.Null       := 'YES';
        FRCampos.Key        := '';
        FRCampos.Default    := '';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
        New(FRCampos);
        FRCampos.Field      := 'AvisoImob';
        FRCampos.Tipo       := 'text';
        FRCampos.Null       := 'YES';
        FRCampos.Key        := '';
        FRCampos.Default    := '';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
        //Eliminar no futuro - Fim
        //
        (*New(FRCampos);
        FRCampos.Field      := 'BloqParc';
        FRCampos.Tipo       := 'int(11)';
        FRCampos.Null       := '';
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
        New(FRCampos);
        FRCampos.Field      := 'BloqParcPar';
        FRCampos.Tipo       := 'int(11)';
        FRCampos.Null       := '';
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
        New(FRCampos);
        FRCampos.Field      := 'BloqParcIts';
        FRCampos.Tipo       := 'int(11)';
        FRCampos.Null       := '';
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);*)
        //
        New(FRCampos);
        FRCampos.Field      := 'Sequencial';
        FRCampos.Tipo       := 'int(11)';
        FRCampos.Null       := '';
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
        New(FRCampos);
        FRCampos.Field      := 'NomeAdmi';
        FRCampos.Tipo       := 'varchar(50)';
        FRCampos.Null       := '';
        FRCampos.Key        := '';
        FRCampos.Default    := '?';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
        (* Movido para o bloopcoes
        New(FRCampos);
        FRCampos.Field      := 'MaxDias';
        FRCampos.Tipo       := 'int(11)';
        FRCampos.Null       := '';
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
        *)
      end;
    end;
  except
    raise;
    Result := False;
  end;
end;

function TWGerl_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      {New(FRCampos);
      FRCampos.Field      := 'FTPConfig';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //}
    end;
  except
    raise;
    Result := False;
  end;
end;

function TWGerl_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  // WEB-OPCOE-001 :: Op��es WEB
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-OPCOE-001';
  FRJanelas.Nome      := 'FmWOpcoes';
  FRJanelas.Descricao := 'Op��es WEB';
  FRJanelas.Modulo    := 'WEB';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-SINCR-002 :: Sincronismo Dados WEB [2]
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-SINCR-002';
  FRJanelas.Nome      := 'FmWSincro2';
  FRJanelas.Descricao := 'Sincronismo Dados WEB [2]';
  FRJanelas.Modulo    := 'WEB';
  FLJanelas.Add(FRJanelas);
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.
