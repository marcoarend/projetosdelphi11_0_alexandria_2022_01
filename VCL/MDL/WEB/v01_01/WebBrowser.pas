unit WebBrowser;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ToolWin, ImgList, StdCtrls,OleCtrls, SHDocVw, Menus,
  WBFuncs, Registry, ExtCtrls, dmkGeral,
  Vcl.Imaging.pngimage, System.ImageList;

type
  TFmWebBrowser = class(TForm)
    MainMenu1: TMainMenu;
    Arquivo1: TMenuItem;
    Editar1: TMenuItem;
    Imprimir1: TMenuItem;
    VisualizarImpresso1: TMenuItem;
    ConfigurarPgina1: TMenuItem;
    N2: TMenuItem;
    Propriedades1: TMenuItem;
    N3: TMenuItem;
    Sair1: TMenuItem;
    LocalizaNestaPgina1: TMenuItem;
    StatusBar1: TStatusBar;
    Button1: TButton;
    ProgressBar1: TProgressBar;
    WebBrowser1: TWebBrowser;
    OpenDialog1: TOpenDialog;
    ImageList1: TImageList;
    Panel2: TPanel;
    PnTop: TPanel;
    Panel1: TPanel;
    ToolBar1: TToolBar;
    BtVoltar: TToolButton;
    ToolButton2: TToolButton;
    BtAvancar: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    ToolButton16: TToolButton;
    CoolBar1: TCoolBar;
    ComboBox1: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure ComboBox1KeyPress(Sender: TObject; var Key: Char);
    procedure Button1Click(Sender: TObject);
    procedure WebBrowser1CommandStateChange(Sender: TObject;
      Command: Integer; Enable: WordBool);
    procedure WebBrowser1DocumentComplete(Sender: TObject;
      const pDisp: IDispatch; var URL: OleVariant);
    procedure WebBrowser1ProgressChange(Sender: TObject; Progress,
      ProgressMax: Integer);
    procedure FormResize(Sender: TObject);
    procedure WebBrowser1BeforeNavigate2(Sender: TObject;
      const pDisp: IDispatch; var URL, Flags, TargetFrameName, PostData,
      Headers: OleVariant; var Cancel: WordBool);
    procedure WebBrowser1StatusTextChange(Sender: TObject;
      const Text: WideString);
    procedure WebBrowser1TitleChange(Sender: TObject;
      const Text: WideString);
    procedure Imprimir1Click(Sender: TObject);
    procedure VisualizarImpresso1Click(Sender: TObject);
    procedure Propriedades1Click(Sender: TObject);
    procedure Sair1Click(Sender: TObject);
    procedure LocalizaNestaPgina1Click(Sender: TObject);
    procedure ConfigurarPgina1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtVoltarClick(Sender: TObject);
    procedure BtAvancarClick(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
    procedure ToolButton9Click(Sender: TObject);
    procedure ToolButton11Click(Sender: TObject);
    procedure ToolButton13Click(Sender: TObject);
    procedure ToolButton15Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FURLPadrao: String;
  end;

var
  FmWebBrowser: TFmWebBrowser;

implementation

uses {$IFDEF No_DB}Principal, {$ELSE}UnMyObjects, {$ENDIF} UnDmkWeb;

{$R *.dfm}

procedure ExibirURLsVisitadas(Urls: TStrings);
var
  Reg: TRegistry;
  S: TStringList;
  i: Integer;
begin
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_CURRENT_USER;
    if Reg.OpenKey('Software\Microsoft\Internet Explorer\TypedURLs', False) then
    begin
      S := TStringList.Create;
      try
        reg.GetValueNames(S);
        for i := 0 to S.Count - 1 do
        begin
          Urls.Add(reg.ReadString(S.Strings[i]));
        end;
      finally
        S.Free;
      end;
      Reg.CloseKey;
    end;
  finally
    Reg.Free;
  end;
end;

procedure TFmWebBrowser.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmWebBrowser.FormClose(Sender: TObject; var Action: TCloseAction);
var
  URL: String;
begin
  URL := ComboBox1.Text;
  //
  if (LowerCase(URL) <> 'about:blank') and (Length(URL) > 0) then
    Geral.WriteAppKeyCU('LastURL', Application.Title + '\WEB', URL, ktString);
end;

procedure TFmWebBrowser.FormCreate(Sender: TObject);
var
  MenuStyle: Integer;
begin
  ToolBar1.Align := alTop;
  WebBrowser1.ParentWindow := Handle;
  //
  MenuStyle := Geral.ReadAppKeyLM('MenuStyle', Application.Title, ktInteger, 0);
  //
  //A d v T oolBarOfficeStyler1.Style := DmkWeb.SkinMenu(MenuStyle);
  ExibirURLsVisitadas(combobox1.Items);
end;

procedure TFmWebBrowser.ComboBox1KeyPress(Sender:
TObject; var Key: Char);
begin
  if (key=#13) then
    begin
    webbrowser1.Navigate(combobox1.Text);
    end;
end;

procedure TFmWebBrowser.Button1Click(Sender: TObject);
begin
  webbrowser1.Navigate(combobox1.Text);
end;

procedure TFmWebBrowser.WebBrowser1CommandStateChange(Sender: TObject;
  Command: Integer; Enable: WordBool);
begin
  case Command of
    CSC_NAVIGATEBACK:
    begin
      //Ativa e Desativa Automaticamente o Bot�o Voltar,
      //Caso tenha alguma p�gina para voltar
      BtVoltar.Enabled := Enable;
    end;
    CSC_NAVIGATEFORWARD:
    begin
      //Ativa e Desativa Automaticamente o Bot�o Avan�ar,
      //Caso tenha alguma p�gina para voltar
      BtAvancar.Enabled := Enable;
    end;
  end;
end;

procedure TFmWebBrowser.WebBrowser1DocumentComplete(Sender: TObject;
  const pDisp: IDispatch; var URL: OleVariant);
begin
  ProgressBar1.Position := 0;
  StatusBar1.Panels[0].Text := '';
end;

procedure TFmWebBrowser.WebBrowser1ProgressChange
  (Sender: TObject; Progress,ProgressMax: Integer);
begin
  {Ele faz um rotina para saber se o valor Maximo do
   Progressbar � maior que 1 e o valor minimo tbem
   se for, ent�o ele faz a rotina}
  if (Progress >= 1) and (ProgressMax > 1) then
  begin
    //Ele tira uma valor percentual para colocar no Progressbar
    ProgressBar1.Position := Round((Progress * 100) div ProgressMax);
    ProgressBar1.Visible  := True;
  end else
  begin
    ProgressBar1.Position := 1;
    ProgressBar1.Visible  := False;
  end;
end;

procedure TFmWebBrowser.FormResize(Sender: TObject);
var
  r: TRect;
const
  SB_GETRECT = WM_USER + 10;
begin
  // Set Progressbar Position
  Statusbar1.Perform(SB_GETRECT, 1, Integer(@R));
  ProgressBar1.Parent := Statusbar1;
  ProgressBar1.SetBounds(r.Left, r.Top,
  r.Right - r.Left - 5, r.Bottom - r.Top);
end;

procedure TFmWebBrowser.FormShow(Sender: TObject);
begin
  if Length(FURLPadrao) = 0 then
    FURLPadrao := Geral.ReadAppKeyCU('URL', Application.Title, ktString, '');
  //
  ComboBox1.Text := FURLPadrao;
  //
  WebBrowser1.Navigate(FURLPadrao);
end;

procedure TFmWebBrowser.WebBrowser1BeforeNavigate2(Sender: TObject;
  const pDisp: IDispatch; var URL, Flags, TargetFrameName,
  PostData,Headers: OleVariant; var Cancel: WordBool);
begin
  {Ao Nevagar uma p�gina, ele coloca automaticamente
   o endere�o dela no combobox, para sabermos que p�gina
   estamos entrando, o endere�o da mesma}
  combobox1.Text := url;
end;

procedure TFmWebBrowser.WebBrowser1StatusTextChange(Sender:
TObject;
  const Text: WideString);
begin
  StatusBar1.Panels[0].Text := Text;
end;

procedure TFmWebBrowser.WebBrowser1TitleChange(Sender: TObject;
  const Text: WideString);
begin
  FmWebBrowser.Caption := Text;
end;

procedure TFmWebBrowser.Imprimir1Click(Sender: TObject);
begin
  WB_ShowPrintDialog(WebBrowser1);
end;

procedure TFmWebBrowser.VisualizarImpresso1Click(Sender: TObject);
begin
  WB_ShowPrintPreview(webbrowser1);
end;

procedure TFmWebBrowser.Propriedades1Click(Sender: TObject);
begin
  WB_ShowPropertiesDialog(webbrowser1);
end;

procedure TFmWebBrowser.Sair1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmWebBrowser.ToolButton11Click(Sender: TObject);
begin
  WB_ShowFindDialog(webbrowser1);

end;

procedure TFmWebBrowser.ToolButton13Click(Sender: TObject);
begin
  WB_ShowPrintDialog(WebBrowser1);

end;

procedure TFmWebBrowser.ToolButton15Click(Sender: TObject);
var
  URL: String;
begin
  URL := Geral.ReadAppKeyCU('LastURL', Application.Title + '\WEB', ktString, '');
  //
  webbrowser1.Navigate(URL);
end;

procedure TFmWebBrowser.BtVoltarClick(Sender: TObject);
begin
  webbrowser1.GoBack;
end;

procedure TFmWebBrowser.BtAvancarClick(Sender: TObject);
begin
  webbrowser1.GoForward;
end;

procedure TFmWebBrowser.ToolButton5Click(Sender: TObject);
begin
  webbrowser1.Stop;

end;

procedure TFmWebBrowser.ToolButton7Click(Sender: TObject);
begin
  webbrowser1.Refresh;

end;

procedure TFmWebBrowser.ToolButton9Click(Sender: TObject);
begin
  webbrowser1.Navigate(FURLPadrao);

end;

procedure TFmWebBrowser.LocalizaNestaPgina1Click(Sender: TObject);
begin
  WB_ShowFindDialog(webbrowser1);
end;

procedure TFmWebBrowser.ConfigurarPgina1Click(Sender: TObject);
begin
  WB_ShowPageSetup(webbrowser1);
end;

end.
