unit WPerfis;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Menus, Grids,
  DBGrids, dmkDBGridDAC, dmkDBGrid, dmkEdit, dmkLabel, dmkGeral, dmkDBEdit,
  ColorGrd, ActnColorMaps, ActnMan, dmkCheckBox, dmkValUsu, dmkPermissoes,
  ComCtrls, dmkRadioGroup, UnDmkProcFunc, UnDmkEnums, UnProjGroup_Consts;

type
  TFmWPerfis = class(TForm)
    DsWPerfis: TDataSource;
    QrWPerfis: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    QrWPerfisCodigo: TIntegerField;
    QrWPerfisNome: TWideStringField;
    QrWPerfisLk: TIntegerField;
    QrWPerfisDataCad: TDateField;
    QrWPerfisDataAlt: TDateField;
    QrWPerfisUserCad: TIntegerField;
    QrWPerfisUserAlt: TIntegerField;
    QrWPerfisAlterWeb: TSmallintField;
    QrWPerfisAtivo: TSmallintField;
    PainelDados: TPanel;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    PainelData: TPanel;
    Label2: TLabel;
    Label1: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    PainelEdita: TPanel;
    Label10: TLabel;
    Label9: TLabel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    EdNome: TdmkEdit;
    CkContinuar: TCheckBox;
    EdCodigo: TdmkEdit;
    QrLoc: TmySQLQuery;
    dmkCkAtivo: TdmkCheckBox;
    CkAtivo: TDBCheckBox;
    dmkPermissoes1: TdmkPermissoes;
    PnDados: TPanel;
    Grade: TdmkDBGridDAC;
    QrWPerfisIts: TmySQLQuery;
    DsWPerfisIts: TDataSource;
    PnControle: TPanel;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    Panel6: TPanel;
    BtRefresh: TBitBtn;
    QrLoc2: TmySQLQuery;
    QrWPerfisItsNome: TWideStringField;
    QrWPerfisItsPage: TWideStringField;
    QrWPerfisItsDescri: TWideStringField;
    QrWPerfisItsLibera: TSmallintField;
    QrWPerfisItsControle: TIntegerField;
    QrLoc3: TmySQLQuery;
    PB1: TProgressBar;
    DBRGNivel: TDBRadioGroup;
    RgTipo: TdmkRadioGroup;
    QrWPerfisTipo: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrWPerfisAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrWPerfisBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure EdNomeExit(Sender: TObject);
    procedure QrWPerfisAfterScroll(DataSet: TDataSet);
    procedure QrWPerfisBeforeClose(DataSet: TDataSet);
    procedure BtRefreshClick(Sender: TObject);
    procedure GradeCellClick(Column: TColumn);
    procedure QrWPerfisItsAfterScroll(DataSet: TDataSet);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
    //
    procedure ReabrePerfisIts(Controle: Integer);
    procedure AtualizaLibera(Ativo, Codigo: Integer);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmWPerfis: TFmWPerfis;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, UnDmkWeb, MyListas, UnGrlUsuarios;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmWPerfis.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmWPerfis.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrWPerfisCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmWPerfis.DefParams;
begin
  VAR_GOTOTABELA := 'wperfis';
  VAR_GOTOMYSQLTABLE := QrWPerfis;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDBn;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM wperfis');
  VAR_SQLx.Add('WHERE Codigo <> 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmWPerfis.EdNomeExit(Sender: TObject);
begin
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT Nome FROM wperfis WHERE Nome LIKE :P0');
    QrLoc.Params[00].AsString := EdNome.ValueVariant;
    QrLoc.Open;
    if QrLoc.RecordCount > 0 then
    begin
      Application.MessageBox('Este nome j� foi cadastrado.', 'Aviso', MB_OK+MB_ICONWARNING);
      EdNome.SetFocus;
      Exit;
    end;
  end;
end;

procedure TFmWPerfis.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmWPerfis.QueryPrincipalAfterOpen;
begin
end;

procedure TFmWPerfis.ReabrePerfisIts(Controle: Integer);
begin
  QrWPerfisIts.Close;
  QrWPerfisIts.Params[0].AsInteger := QrWPerfisCodigo.Value;
  QrWPerfisIts.Open;
  //
  if Controle <> 0 then
    QrWPerfisIts.Locate('Controle', Controle, []);
end;

procedure TFmWPerfis.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmWPerfis.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmWPerfis.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmWPerfis.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmWPerfis.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmWPerfis.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrWPerfisCodigo.Value;
  Close;
end;

procedure TFmWPerfis.BtTudoClick(Sender: TObject);
begin
  AtualizaLibera(1, QrWPerfisCodigo.Value);
end;

procedure TFmWPerfis.AtualizaLibera(Ativo, Codigo: Integer);
begin
  Dmod.QrUpdN.SQL.Clear;
  Dmod.QrUpdN.SQL.Add('UPDATE wperfisits SET Libera=:P0 WHERE Codigo=:P1');
  Dmod.QrUpdN.Params[0].AsInteger := Ativo;
  Dmod.QrUpdN.Params[1].AsInteger := Codigo;
  Dmod.QrUpdN.ExecSQL;
  //
  ReabrePerfisIts(0);
end;
    
procedure TFmWPerfis.BtConfirmaClick(Sender: TObject);
var
  Codigo, Tipo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  Tipo := RgTipo.ItemIndex;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Aviso', MB_OK+MB_ICONWARNING);
    EdNome.SetFocus;
    Exit;
  end;
  if Tipo < 0 then
  begin
    Application.MessageBox('Defina um tipo de pefil.', 'Aviso', MB_OK+MB_ICONWARNING);
    RgTipo.SetFocus;
    Exit;
  end;
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Codigo := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpdn, 'wperfis',
    'Codigo', [], [], stIns, 0, siPositivo, nil)
  else
    Codigo := QrWPerfisCodigo.Value;
  //
  Dmod.QrUpdN.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdN.SQL.Add('INSERT INTO wperfis SET ')
  else
    Dmod.QrUpdN.SQL.Add('UPDATE wperfis SET ');
  Dmod.QrUpdN.SQL.Add('Nome=:P0, Tipo=:P1, Ativo=:P2, AlterWeb=:P3, ');
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdN.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdN.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  //
  Dmod.QrUpdN.Params[00].AsString  := Nome;
  Dmod.QrUpdN.Params[01].AsInteger := Tipo;
  Dmod.QrUpdN.Params[02].AsInteger := Geral.BoolToInt(dmkCkAtivo.Checked);
  Dmod.QrUpdN.Params[03].AsInteger := 1;
  //
  Dmod.QrUpdN.Params[04].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdN.Params[05].AsInteger := VAR_USUARIO;
  Dmod.QrUpdN.Params[06].AsInteger := Codigo;
  Dmod.QrUpdN.ExecSQL;
  //
  QrWPerfis.Close;
  QrWPerfis.Open;
  //
  if CkContinuar.Checked then
  begin
    Application.MessageBox('Dados cadastrados com sucesso!', 'Aviso', MB_OK+MB_ICONWARNING);
    LaTipo.SQLType := stIns;
    MostraEdicao(True, CO_INCLUSAO, 0);
  end else
  begin
    MostraEdicao(False, CO_TRAVADO, 0);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmWPerfis.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(False, CO_TRAVADO, 0);
end;

procedure TFmWPerfis.FormCreate(Sender: TObject);
var
  Compo: TComponent;
  Query: TmySQLQuery;
  I: Integer;
begin
  for I := 0 to FmWPerfis.ComponentCount - 1 do
  begin
    Compo := FmWPerfis.Components[I];
    if Compo is TmySQLQuery then
    begin
        Query := TmySQLQuery(Compo);
        Query.Database := Dmod.MyDBn;
    end;
  end;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  CriaOForm;
  //
  DBRGNivel.Items := GrlUsuarios.ConfiguraNiveis(CO_DMKID_APP, False);
  RgTipo.Items    := GrlUsuarios.ConfiguraNiveis(CO_DMKID_APP, False);
  //
  PnDados.Align := alClient;
end;

procedure TFmWPerfis.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrWPerfisCodigo.Value,LaRegistro.Caption);
end;

procedure TFmWPerfis.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmWPerfis.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmWPerfis.QrWPerfisAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmWPerfis.QrWPerfisAfterScroll(DataSet: TDataSet);
begin
  PnControle.Visible := QrWPerfis.RecordCount > 0;
  ReabrePerfisIts(0);
end;

procedure TFmWPerfis.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmWPerfis.SbQueryClick(Sender: TObject);
begin
  LocCod(QrWPerfisCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'wperfis', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmWPerfis.FormResize(Sender: TObject);
begin
  // M L A G e r a l.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmWPerfis.GradeCellClick(Column: TColumn);
var
  Controle, Libera: Integer;
begin
  if (Column.FieldName = 'Libera') and (QrWPerfisIts.RecordCount > 0) then
  begin
    Libera   := dmkPF.EscolhaDe2Int(QrWPerfisItsLibera.Value = 1, 0, 1);
    Controle := QrWPerfisItsControle.Value;
    //
    Dmod.QrUpdN.SQL.Clear;
    Dmod.QrUpdN.SQL.Add('UPDATE wperfisits SET Libera=:P0 WHERE Controle=:P1');
    Dmod.QrUpdN.Params[0].AsInteger := Libera;
    Dmod.QrUpdN.Params[1].AsInteger := Controle;
    Dmod.QrUpdN.ExecSQL;
    //
    ReabrePerfisIts(Controle);
  end;
end;

procedure TFmWPerfis.QrWPerfisBeforeClose(DataSet: TDataSet);
begin
  QrWPerfisIts.Close;
end;

procedure TFmWPerfis.QrWPerfisBeforeOpen(DataSet: TDataSet);
begin
  QrWPerfisCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmWPerfis.QrWPerfisItsAfterScroll(DataSet: TDataSet);
begin
  if QrWPerfisIts.RecordCount > 0 then
  begin
    BtTudo.Enabled   := True;
    BtNenhum.Enabled := True;
  end else
  begin
    BtTudo.Enabled   := False;
    BtNenhum.Enabled := False;
  end;
end;

procedure TFmWPerfis.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(True, CO_INCLUSAO, 0);
end;

procedure TFmWPerfis.BtNenhumClick(Sender: TObject);
begin
  AtualizaLibera(0, QrWPerfisCodigo.Value);
end;

procedure TFmWPerfis.BtRefreshClick(Sender: TObject);
var
  Codigo, Controle, Janela: Integer;
begin
  PB1.Visible  := True;
  PB1.Position := 0;
  //
  QrLoc.Close;
  QrLoc.SQL.Clear;
  QrLoc.SQL.Add('SELECT wja.Codigo');
  QrLoc.SQL.Add('FROM wperfjan wja');
  //QrLoc.SQL.Add('WHERE wja.Geral=0');
  QrLoc.Open;
  //
  if QrLoc.RecordCount > 0 then
  begin
    QrLoc.First;
    while not QrLoc.Eof do
    begin
      Janela   := QrLoc.FieldByName('Codigo').Value;
      //
      QrLoc3.Close;
      QrLoc3.SQL.Clear;
      QrLoc3.SQL.Add('SELECT wpe.Codigo');
      QrLoc3.SQL.Add('FROM wperfis wpe');
      QrLoc3.SQL.Add('WHERE wpe.Codigo <> 0');
      QrLoc3.Open;
      //
      PB1.Max := QrLoc.RecordCount;
      //
      if QrLoc3.RecordCount > 0 then
      begin
        while not QrLoc3.Eof do
        begin
          PB1.Position := PB1.Position + 1;
          Codigo := QrLoc3.FieldByName('Codigo').Value;
          //
          QrLoc2.Close;
          QrLoc2.SQL.Clear;
          QrLoc2.SQL.Add('SELECT Janela FROM wperfisits WHERE Janela=:P0 AND Codigo=:P1');
          QrLoc2.Params[0].AsInteger := Janela;
          QrLoc2.Params[1].AsInteger := Codigo;
          QrLoc2.Open;
          //
          if QrLoc2.RecordCount = 0 then
          begin
            Controle := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpdn, 'wperfisits',
            'Controle', [], [], stIns, 0, siPositivo, nil);
            //
            Dmod.QrUpdN.SQL.Clear;
            Dmod.QrUpdN.SQL.Add('INSERT INTO wperfisits SET ');
            Dmod.QrUpdN.SQL.Add('Janela=:P0, ');
            Dmod.QrUpdN.SQL.Add('DataCad=:Pv, UserCad=:Px, Codigo=:Py, Controle=:Pz');
            //
            Dmod.QrUpdN.Params[00].AsInteger := Janela;
            //
            Dmod.QrUpdN.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
            Dmod.QrUpdN.Params[02].AsInteger := VAR_USUARIO;
            Dmod.QrUpdN.Params[03].AsInteger := Codigo;
            Dmod.QrUpdN.Params[04].AsInteger := Controle;
            Dmod.QrUpdN.ExecSQL;
          end;
          //
          QrLoc3.Next;
        end;
      end;
      QrLoc.Next;
    end;
    PB1.Visible := False;
  end;
  //Verifica se janela existe
  PB1.Position := 0;
  QrLoc.Close;
  QrLoc.SQL.Clear;
  QrLoc.SQL.Add('SELECT Janela');
  QrLoc.SQL.Add('FROM wperfisits');
  QrLoc.SQL.Add('GROUP BY Janela');  
  QrLoc.Open;
  //
  PB1.Max := QrLoc.RecordCount;
  //
  if QrLoc.RecordCount > 0 then
  begin
    while not QrLoc.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      Janela := QrLoc.FieldByName('Janela').Value;
      //
      QrLoc2.Close;
      QrLoc2.SQL.Clear;
      QrLoc2.SQL.Add('SELECT Codigo FROM wperfjan');
      //QrLoc2.SQL.Add('WHERE Geral = 0 AND Codigo=:P0');
      QrLoc2.SQL.Add('WHERE Codigo=:P0');
      QrLoc2.Params[0].AsInteger := Janela;
      QrLoc2.Open;
      //
      if QrLoc2.RecordCount = 0 then
      begin
        Dmod.QrUpdN.SQL.Clear;
        Dmod.QrUpdN.SQL.Add('DELETE FROM wperfisits WHERE Janela=:P0');
        Dmod.QrUpdN.Params[0].AsInteger := Janela;
        Dmod.QrUpdN.ExecSQL;
      end;
      //
      QrLoc.Next;
    end;
  end;
  ReabrePerfisIts(0);
end;

procedure TFmWPerfis.BtAlteraClick(Sender: TObject);
begin
  if QrWPerfisCodigo.Value > 0 then
  begin
    MostraEdicao(True, CO_ALTERACAO, 0);
  end;
end;

procedure TFmWPerfis.MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelDados.Visible := False;
    PainelEdita.Visible := True;
    if Status = CO_INCLUSAO then
    begin
      EdCodigo.ValueVariant := FormatFloat('000', Codigo);
      EdNome.ValueVariant   := '';
      RgTipo.ItemIndex      := -1;
      dmkCkAtivo.Checked    := True;
      CkContinuar.Checked   := True;
    end else begin
      EdCodigo.ValueVariant := QrWPerfisCodigo.Value;
      EdNome.ValueVariant   := QrWPerfisNome.Value;
      RgTipo.ItemIndex      := QrWPerfisTipo.Value;
      dmkCkAtivo.Checked    := Geral.IntToBool(QrWPerfisAtivo.Value);
      CkContinuar.Checked   := False;
    end;
    EdNome.SetFocus;
  end else begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

end.
