object FmWProtocol: TFmWProtocol
  Left = 368
  Top = 194
  Caption = 'WEB-PROTO-001 :: Cadastro de Protocolos WEB'
  ClientHeight = 562
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 514
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 465
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 681
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 208
      Align = alTop
      TabOrder = 0
      object Label7: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label10: TLabel
        Left = 65
        Top = 3
        Width = 34
        Height = 13
        Caption = 'Tarefa:'
      end
      object Label3: TLabel
        Left = 4
        Top = 49
        Width = 26
        Height = 13
        Caption = 'Perfil:'
      end
      object Label18: TLabel
        Left = 507
        Top = 4
        Width = 49
        Height = 13
        Caption = 'Expira em:'
      end
      object Label13: TLabel
        Left = 4
        Top = 93
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object Label14: TLabel
        Left = 4
        Top = 138
        Width = 45
        Height = 13
        Caption = 'Entidade:'
      end
      object EdCodigo: TdmkEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdTarefa: TdmkEditCB
        Left = 65
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBTarefa
        IgnoraDBLookupComboBox = False
      end
      object CBTarefa: TdmkDBLookupComboBox
        Left = 121
        Top = 20
        Width = 380
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsTarefas
        TabOrder = 2
        dmkEditCB = EdTarefa
        QryCampo = 'Cliente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdPerfil: TdmkEditCB
        Left = 4
        Top = 66
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBPerfil
        IgnoraDBLookupComboBox = False
      end
      object CBPerfil: TdmkDBLookupComboBox
        Left = 60
        Top = 66
        Width = 537
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsPerfis
        TabOrder = 5
        dmkEditCB = EdPerfil
        QryCampo = 'Cliente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPDataExp: TDateTimePicker
        Left = 507
        Top = 20
        Width = 90
        Height = 21
        Date = 39422.782497824100000000
        Time = 39422.782497824100000000
        TabOrder = 3
      end
      object EdCliente: TdmkEditCB
        Left = 4
        Top = 110
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 60
        Top = 110
        Width = 537
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsClientes
        TabOrder = 7
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdEntidade: TdmkEditCB
        Left = 4
        Top = 155
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBEntidade
        IgnoraDBLookupComboBox = False
      end
      object CBEntidade: TdmkDBLookupComboBox
        Left = 60
        Top = 155
        Width = 537
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsEntidades
        TabOrder = 9
        dmkEditCB = EdEntidade
        QryCampo = 'Cliente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 514
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelControle: TPanel
      Left = 1
      Top = 465
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TStaticText
        Left = 173
        Top = 1
        Width = 30
        Height = 17
        Align = alClient
        BevelKind = bkTile
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 320
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtProtocolo: TBitBtn
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Protocolo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtProtocoloClick
        end
        object Panel2: TPanel
          Left = 360
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 1
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 790
      Height = 440
      ActivePage = TabSheet2
      Align = alTop
      TabHeight = 25
      TabOrder = 1
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = 'Cadastro'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PainelData: TPanel
          Left = 0
          Top = 0
          Width = 782
          Height = 405
          Align = alClient
          Enabled = False
          ParentBackground = False
          TabOrder = 0
          object Label1: TLabel
            Left = 4
            Top = 4
            Width = 14
            Height = 13
            Caption = 'ID:'
            FocusControl = DBEdCodigo
          end
          object Label2: TLabel
            Left = 65
            Top = 4
            Width = 34
            Height = 13
            Caption = 'Tarefa:'
            FocusControl = DBEdNome
          end
          object Label4: TLabel
            Left = 507
            Top = 4
            Width = 49
            Height = 13
            Caption = 'Expira em:'
            FocusControl = DBEdNome
          end
          object Label5: TLabel
            Left = 4
            Top = 136
            Width = 45
            Height = 13
            Caption = 'Entidade:'
            FocusControl = dmkDBEdit2
          end
          object Label6: TLabel
            Left = 5
            Top = 179
            Width = 93
            Height = 13
            Caption = 'Cadastrado pelo IP:'
            FocusControl = DBEdNome
          end
          object Label8: TLabel
            Left = 130
            Top = 179
            Width = 102
            Height = 13
            Caption = 'Data / hora cadastro:'
          end
          object Label9: TLabel
            Left = 256
            Top = 179
            Width = 67
            Height = 13
            Caption = 'Usu'#225'rio WEB:'
          end
          object Label11: TLabel
            Left = 130
            Top = 225
            Width = 105
            Height = 13
            Caption = 'Data / hora finalizado:'
          end
          object Label12: TLabel
            Left = 5
            Top = 225
            Width = 78
            Height = 13
            Caption = 'Alterado pelo IP:'
            FocusControl = DBEdNome
          end
          object Label15: TLabel
            Left = 4
            Top = 48
            Width = 26
            Height = 13
            Caption = 'Perfil:'
            FocusControl = dmkDBEdit8
          end
          object Label16: TLabel
            Left = 4
            Top = 92
            Width = 35
            Height = 13
            Caption = 'Cliente:'
            FocusControl = dmkDBEdit9
          end
          object DBEdCodigo: TdmkDBEdit
            Left = 4
            Top = 20
            Width = 56
            Height = 21
            Hint = 'N'#186' do banco'
            TabStop = False
            DataField = 'Codigo'
            DataSource = DsWProtocol
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 0
            UpdType = utYes
            Alignment = taRightJustify
          end
          object DBEdNome: TdmkDBEdit
            Left = 65
            Top = 20
            Width = 436
            Height = 21
            Hint = 'Nome do banco'
            Color = clWhite
            DataField = 'PROTNOME'
            DataSource = DsWProtocol
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit1: TdmkDBEdit
            Left = 507
            Top = 20
            Width = 90
            Height = 21
            Hint = 'Nome do banco'
            Color = clWhite
            DataField = 'DATAEXP_TXT'
            DataSource = DsWProtocol
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit2: TdmkDBEdit
            Left = 4
            Top = 152
            Width = 593
            Height = 21
            Hint = 'Nome do banco'
            Color = clWhite
            DataField = 'NOMEENT'
            DataSource = DsWProtocol
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit3: TdmkDBEdit
            Left = 4
            Top = 196
            Width = 120
            Height = 21
            Hint = 'Nome do banco'
            Color = clWhite
            DataField = 'IPCad'
            DataSource = DsWProtocol
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit4: TdmkDBEdit
            Left = 130
            Top = 196
            Width = 120
            Height = 21
            Hint = 'Nome do banco'
            Color = clWhite
            DataField = 'DATAHORA_TXT'
            DataSource = DsWProtocol
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 5
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit5: TdmkDBEdit
            Left = 256
            Top = 196
            Width = 341
            Height = 21
            Hint = 'Nome do banco'
            Color = clWhite
            DataField = 'PersonalName'
            DataSource = DsWProtocol
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 6
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object DBRadioGroup1: TDBRadioGroup
            Left = 256
            Top = 225
            Width = 120
            Height = 38
            Caption = 'Finalizado?'
            Columns = 2
            DataField = 'Finaliz'
            DataSource = DsWProtocol
            Items.Strings = (
              'N'#227'o'
              'Sim')
            ParentBackground = True
            TabOrder = 7
            Values.Strings = (
              '0'
              '1')
          end
          object dmkDBEdit6: TdmkDBEdit
            Left = 130
            Top = 242
            Width = 120
            Height = 21
            Hint = 'Nome do banco'
            Color = clWhite
            DataField = 'DATAHORAFIN_TXT'
            DataSource = DsWProtocol
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 8
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit7: TdmkDBEdit
            Left = 4
            Top = 242
            Width = 120
            Height = 21
            Hint = 'Nome do banco'
            Color = clWhite
            DataField = 'IPAlt'
            DataSource = DsWProtocol
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 9
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit8: TdmkDBEdit
            Left = 4
            Top = 64
            Width = 593
            Height = 21
            Hint = 'Nome do banco'
            Color = clWhite
            DataField = 'NOMEPERFIL'
            DataSource = DsWProtocol
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 10
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit9: TdmkDBEdit
            Left = 4
            Top = 108
            Width = 593
            Height = 21
            Hint = 'Nome do banco'
            Color = clWhite
            DataField = 'NOMECLI'
            DataSource = DsWProtocol
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 11
            UpdType = utYes
            Alignment = taLeftJustify
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Gerencia'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 782
          Height = 235
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          object Label17: TLabel
            Left = 3
            Top = 4
            Width = 14
            Height = 13
            Caption = 'ID:'
          end
          object Label19: TLabel
            Left = 84
            Top = 3
            Width = 34
            Height = 13
            Caption = 'Tarefa:'
          end
          object Label20: TLabel
            Left = 428
            Top = 3
            Width = 26
            Height = 13
            Caption = 'Perfil:'
          end
          object Label21: TLabel
            Left = 2
            Top = 46
            Width = 45
            Height = 13
            Caption = 'Entidade:'
          end
          object Label22: TLabel
            Left = 428
            Top = 45
            Width = 35
            Height = 13
            Caption = 'Cliente:'
          end
          object Label23: TLabel
            Left = 2
            Top = 91
            Width = 39
            Height = 13
            Caption = 'Usu'#225'rio:'
          end
          object EdPesID: TdmkEdit
            Left = 3
            Top = 20
            Width = 75
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdPesTarefa: TdmkEditCB
            Left = 84
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBPesTarefa
            IgnoraDBLookupComboBox = False
          end
          object CBPesTarefa: TdmkDBLookupComboBox
            Left = 140
            Top = 20
            Width = 280
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPesTarefas
            TabOrder = 2
            dmkEditCB = EdPesTarefa
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdPesPerfil: TdmkEditCB
            Left = 428
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBPesPerfil
            IgnoraDBLookupComboBox = False
          end
          object CBPesPerfil: TdmkDBLookupComboBox
            Left = 484
            Top = 20
            Width = 287
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPesPerfis
            TabOrder = 4
            dmkEditCB = EdPesPerfil
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdPesEntidade: TdmkEditCB
            Left = 2
            Top = 63
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBPesEntidade
            IgnoraDBLookupComboBox = False
          end
          object CBPesEntidade: TdmkDBLookupComboBox
            Left = 58
            Top = 63
            Width = 362
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPesEntidades
            TabOrder = 6
            dmkEditCB = EdPesEntidade
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object CBPesCliente: TdmkDBLookupComboBox
            Left = 484
            Top = 62
            Width = 287
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPesClientes
            TabOrder = 7
            dmkEditCB = EdPesCliente
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdPesCliente: TdmkEditCB
            Left = 428
            Top = 62
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 8
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBPesCliente
            IgnoraDBLookupComboBox = False
          end
          object GBPesAbertura: TGroupBox
            Left = 2
            Top = 135
            Width = 343
            Height = 42
            Caption = ' Per'#237'odo de abertura: '
            TabOrder = 9
            object CkPesAbeIni: TCheckBox
              Left = 4
              Top = 16
              Width = 75
              Height = 17
              Caption = 'Data inicial:'
              TabOrder = 0
            end
            object TPPesAbeIni: TDateTimePicker
              Left = 77
              Top = 14
              Width = 95
              Height = 21
              CalColors.TextColor = clMenuText
              Date = 37636.777157974500000000
              Time = 37636.777157974500000000
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
            end
            object CkPesAbeFim: TCheckBox
              Left = 176
              Top = 16
              Width = 68
              Height = 17
              Caption = 'Data final:'
              TabOrder = 2
            end
            object TPPesAbeFim: TDateTimePicker
              Left = 243
              Top = 14
              Width = 95
              Height = 21
              Date = 37636.777203761600000000
              Time = 37636.777203761600000000
              TabOrder = 3
            end
          end
          object GBPesEncerr: TGroupBox
            Left = 355
            Top = 135
            Width = 343
            Height = 42
            Caption = ' Per'#237'odo de abertura: '
            TabOrder = 10
            object CkPesEncerrIni: TCheckBox
              Left = 4
              Top = 16
              Width = 75
              Height = 17
              Caption = 'Data inicial:'
              TabOrder = 0
            end
            object TpPesEncerrIni: TDateTimePicker
              Left = 77
              Top = 14
              Width = 95
              Height = 21
              CalColors.TextColor = clMenuText
              Date = 37636.777157974500000000
              Time = 37636.777157974500000000
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
            end
            object CkPesEncerrFim: TCheckBox
              Left = 176
              Top = 16
              Width = 68
              Height = 17
              Caption = 'Data final:'
              TabOrder = 2
            end
            object TPPesEncerrFim: TDateTimePicker
              Left = 243
              Top = 14
              Width = 95
              Height = 21
              Date = 37636.777203761600000000
              Time = 37636.777203761600000000
              TabOrder = 3
            end
          end
          object RGPesStatus: TRadioGroup
            Left = 428
            Top = 89
            Width = 343
            Height = 40
            Caption = 'Status:'
            Columns = 3
            Items.Strings = (
              'Em aberto'
              'Finalizado'
              'Ambos')
            TabOrder = 11
          end
          object EdPesUsuario: TdmkEditCB
            Left = 2
            Top = 108
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 12
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBPesUsuario
            IgnoraDBLookupComboBox = False
          end
          object CBPesUsuario: TdmkDBLookupComboBox
            Left = 58
            Top = 108
            Width = 362
            Height = 21
            KeyField = 'Codigo'
            ListField = 'PersonalName'
            ListSource = DsPesUsers
            TabOrder = 13
            dmkEditCB = EdPesUsuario
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object Panel6: TPanel
            Left = 1
            Top = 186
            Width = 780
            Height = 48
            Align = alBottom
            ParentBackground = False
            TabOrder = 14
            object Panel9: TPanel
              Left = 668
              Top = 1
              Width = 111
              Height = 46
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 0
            end
            object Panel10: TPanel
              Left = 1
              Top = 1
              Width = 288
              Height = 46
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object BtPesq: TBitBtn
                Tag = 22
                Left = 4
                Top = 3
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Caption = '&Pesquisa'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtPesqClick
              end
              object BtLimpar: TBitBtn
                Tag = 169
                Left = 100
                Top = 3
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Caption = '&Limpar'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 1
                OnClick = BtLimparClick
              end
            end
          end
        end
        object DBGItens: TdmkDBGrid
          Left = 0
          Top = 235
          Width = 782
          Height = 170
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATAHORA_TXT'
              Title.Caption = 'Abertura'
              Width = 130
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATAHORAFIN_TXT'
              Title.Caption = 'Encerramento'
              Width = 130
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PROTNOME'
              Title.Caption = 'Tarefa'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPERFIL'
              Title.Caption = 'Perfil'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEENT'
              Title.Caption = 'Entidade'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECLI'
              Title.Caption = 'Cliente'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PersonalName'
              Title.Caption = 'Usu'#225'rio'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATAEXP_TXT'
              Title.Caption = 'Expira em'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IPCad'
              Title.Caption = 'Cadastrado pelo IP'
              Width = 95
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATAHORA_TXT'
              Title.Caption = 'Data / hora cadastro'
              Width = 125
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IPAlt'
              Title.Caption = 'Alterado pelo IP'
              Width = 95
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATAHORAFIN_TXT'
              Title.Caption = 'Data / hora finalizado'
              Width = 125
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FINALIX_TXT'
              Title.Caption = 'Finalizado ?'
              Width = 65
              Visible = True
            end>
          Color = clWindow
          Ctl3D = True
          DataSource = DsPesWProtocol
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGItensDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATAHORA_TXT'
              Title.Caption = 'Abertura'
              Width = 130
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATAHORAFIN_TXT'
              Title.Caption = 'Encerramento'
              Width = 130
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PROTNOME'
              Title.Caption = 'Tarefa'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPERFIL'
              Title.Caption = 'Perfil'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEENT'
              Title.Caption = 'Entidade'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECLI'
              Title.Caption = 'Cliente'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PersonalName'
              Title.Caption = 'Usu'#225'rio'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATAEXP_TXT'
              Title.Caption = 'Expira em'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IPCad'
              Title.Caption = 'Cadastrado pelo IP'
              Width = 95
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATAHORA_TXT'
              Title.Caption = 'Data / hora cadastro'
              Width = 125
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IPAlt'
              Title.Caption = 'Alterado pelo IP'
              Width = 95
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATAHORAFIN_TXT'
              Title.Caption = 'Data / hora finalizado'
              Width = 125
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FINALIX_TXT'
              Title.Caption = 'Finalizado ?'
              Width = 65
              Visible = True
            end>
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Caption = 'Cadastro de Protocolos WEB'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 709
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 708
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 128
      ExplicitTop = 17
    end
  end
  object DsWProtocol: TDataSource
    DataSet = QrWProtocol
    Left = 712
    Top = 12
  end
  object QrWProtocol: TmySQLQuery
    Database = Dmod.MyDBn
    BeforeOpen = QrWProtocolBeforeOpen
    AfterOpen = QrWProtocolAfterOpen
    OnCalcFields = QrWProtocolCalcFields
    SQL.Strings = (
      'SELECT wpr.*, pro.Nome PROTNOME, per.Nome NOMEPERFIL,'
      
        'wus.PersonalName, IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOM' +
        'EENT,'
      'IF (enc.Tipo=0, enc.RazaoSocial, enc.Nome) NOMECLI'
      'FROM wprotocol wpr'
      'LEFT JOIN entidades ent ON ent.Codigo = wpr.Entidade'
      'LEFT JOIN protocolos pro ON pro.Codigo = wpr.Tarefa'
      'LEFT JOIN wusers wus ON wus.Codigo = wpr.Usuario'
      'LEFT JOIN wperfis per ON per.Codigo = wpr.Perfil'
      'LEFT JOIN clientes cli ON cli.Codigo = wpr.Cliente'
      'LEFT JOIN entidades enc ON enc.Codigo = cli.Cliente')
    Left = 684
    Top = 12
    object QrWProtocolCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWProtocolTarefa: TIntegerField
      FieldName = 'Tarefa'
    end
    object QrWProtocolDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrWProtocolLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrWProtocolDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrWProtocolDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrWProtocolUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrWProtocolUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrWProtocolAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrWProtocolAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrWProtocolUsuario: TIntegerField
      FieldName = 'Usuario'
    end
    object QrWProtocolDataExp: TDateField
      FieldName = 'DataExp'
    end
    object QrWProtocolDataFin: TDateTimeField
      FieldName = 'DataFin'
    end
    object QrWProtocolFinaliz: TSmallintField
      FieldName = 'Finaliz'
    end
    object QrWProtocolEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrWProtocolPROTNOME: TWideStringField
      FieldName = 'PROTNOME'
      Size = 100
    end
    object QrWProtocolPersonalName: TWideStringField
      FieldName = 'PersonalName'
      Size = 32
    end
    object QrWProtocolNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrWProtocolDATAEXP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATAEXP_TXT'
      Calculated = True
    end
    object QrWProtocolDATAHORA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATAHORA_TXT'
      Calculated = True
    end
    object QrWProtocolDATAHORAFIN_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATAHORAFIN_TXT'
      Calculated = True
    end
    object QrWProtocolIPCad: TWideStringField
      FieldName = 'IPCad'
      Size = 15
    end
    object QrWProtocolIPAlt: TWideStringField
      FieldName = 'IPAlt'
      Size = 15
    end
    object QrWProtocolCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrWProtocolPerfil: TIntegerField
      FieldName = 'Perfil'
    end
    object QrWProtocolNOMEPERFIL: TWideStringField
      FieldName = 'NOMEPERFIL'
      Size = 50
    end
    object QrWProtocolNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 740
    Top = 12
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo,'
      'IF (Tipo=0, RazaoSocial, Nome) Nome'
      'FROM entidades '
      'ORDER BY Nome')
    Left = 617
    Top = 90
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrEntidadesNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 645
    Top = 90
  end
  object QrTarefas: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM protocolos'
      'ORDER BY Nome')
    Left = 673
    Top = 90
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsTarefas: TDataSource
    DataSet = QrTarefas
    Left = 701
    Top = 90
  end
  object PMProtocolo: TPopupMenu
    OnPopup = PMProtocoloPopup
    Left = 432
    Top = 528
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Encerra1: TMenuItem
      Caption = '&Encerra'
      OnClick = Encerra1Click
    end
  end
  object QrPerfis: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM wperfis'
      'ORDER BY Nome')
    Left = 617
    Top = 118
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
      Origin = 'wperfis.Codigo'
      Required = True
    end
    object StringField2: TWideStringField
      FieldName = 'Nome'
      Origin = 'wperfis.Nome'
      Required = True
      Size = 50
    end
  end
  object DsPerfis: TDataSource
    DataSet = QrPerfis
    Left = 645
    Top = 118
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT cli.Codigo,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome'
      'FROM  clientes cli'
      'LEFT JOIN entidades ent ON ent.Codigo = cli.Cliente'
      'ORDER BY Nome')
    Left = 673
    Top = 118
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'clientes.Codigo'
    end
    object QrClientesNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'Nome'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 701
    Top = 118
  end
  object QrPesTarefas: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM protocolos'
      'ORDER BY Nome')
    Left = 113
    Top = 346
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsPesTarefas: TDataSource
    DataSet = QrPesTarefas
    Left = 141
    Top = 346
  end
  object QrPesPerfis: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM wperfis'
      'ORDER BY Nome')
    Left = 169
    Top = 346
    object IntegerField4: TIntegerField
      FieldName = 'Codigo'
      Origin = 'wperfis.Codigo'
      Required = True
    end
    object StringField4: TWideStringField
      FieldName = 'Nome'
      Origin = 'wperfis.Nome'
      Required = True
      Size = 50
    end
  end
  object DsPesPerfis: TDataSource
    DataSet = QrPesPerfis
    Left = 197
    Top = 346
  end
  object QrPesEntidades: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo,'
      'IF (Tipo=0, RazaoSocial, Nome) Nome'
      'FROM entidades '
      'ORDER BY Nome')
    Left = 225
    Top = 346
    object IntegerField5: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object StringField5: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsPesEntidades: TDataSource
    DataSet = QrPesEntidades
    Left = 253
    Top = 346
  end
  object QrPesClientes: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT cli.Codigo,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome'
      'FROM  clientes cli'
      'LEFT JOIN entidades ent ON ent.Codigo = cli.Cliente'
      'ORDER BY Nome')
    Left = 281
    Top = 346
    object IntegerField6: TIntegerField
      FieldName = 'Codigo'
      Origin = 'clientes.Codigo'
    end
    object StringField6: TWideStringField
      FieldName = 'Nome'
      Origin = 'Nome'
      Size = 100
    end
  end
  object DsPesClientes: TDataSource
    DataSet = QrPesClientes
    Left = 309
    Top = 346
  end
  object QrPesUsers: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, PersonalName'
      'FROM wusers'
      'ORDER BY PersonalName')
    Left = 338
    Top = 346
    object QrPesUsersCodigo: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrPesUsersPersonalName: TWideStringField
      FieldName = 'PersonalName'
      Size = 32
    end
  end
  object DsPesUsers: TDataSource
    DataSet = QrPesUsers
    Left = 366
    Top = 346
  end
  object QrPesWProtocol: TmySQLQuery
    Database = Dmod.MyDBn
    OnCalcFields = QrPesWProtocolCalcFields
    SQL.Strings = (
      'SELECT wpr.*, pro.Nome PROTNOME, per.Nome NOMEPERFIL,'
      
        'wus.PersonalName, IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOM' +
        'EENT,'
      'IF (enc.Tipo=0, enc.RazaoSocial, enc.Nome) NOMECLI'
      'FROM wprotocol wpr'
      'LEFT JOIN entidades ent ON ent.Codigo = wpr.Entidade'
      'LEFT JOIN protocolos pro ON pro.Codigo = wpr.Tarefa'
      'LEFT JOIN wusers wus ON wus.Codigo = wpr.Usuario'
      'LEFT JOIN wperfis per ON per.Codigo = wpr.Perfil'
      'LEFT JOIN clientes cli ON cli.Codigo = wpr.Cliente'
      'LEFT JOIN entidades enc ON enc.Codigo = cli.Cliente')
    Left = 395
    Top = 346
    object QrPesWProtocolCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesWProtocolTarefa: TIntegerField
      FieldName = 'Tarefa'
    end
    object QrPesWProtocolDataExp: TDateField
      FieldName = 'DataExp'
    end
    object QrPesWProtocolIPCad: TWideStringField
      FieldName = 'IPCad'
      Size = 15
    end
    object QrPesWProtocolIPAlt: TWideStringField
      FieldName = 'IPAlt'
      Size = 15
    end
    object QrPesWProtocolDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrPesWProtocolDataFin: TDateTimeField
      FieldName = 'DataFin'
    end
    object QrPesWProtocolFinaliz: TSmallintField
      FieldName = 'Finaliz'
    end
    object QrPesWProtocolUsuario: TIntegerField
      FieldName = 'Usuario'
    end
    object QrPesWProtocolCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPesWProtocolEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrPesWProtocolPerfil: TIntegerField
      FieldName = 'Perfil'
    end
    object QrPesWProtocolLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPesWProtocolDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPesWProtocolDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPesWProtocolUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPesWProtocolUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPesWProtocolAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPesWProtocolAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPesWProtocolPROTNOME: TWideStringField
      FieldName = 'PROTNOME'
      Size = 100
    end
    object QrPesWProtocolNOMEPERFIL: TWideStringField
      FieldName = 'NOMEPERFIL'
      Size = 50
    end
    object QrPesWProtocolPersonalName: TWideStringField
      FieldName = 'PersonalName'
      Size = 32
    end
    object QrPesWProtocolDATAEXP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATAEXP_TXT'
      Calculated = True
    end
    object QrPesWProtocolDATAHORA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATAHORA_TXT'
      Calculated = True
    end
    object QrPesWProtocolDATAHORAFIN_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATAHORAFIN_TXT'
      Calculated = True
    end
    object QrPesWProtocolNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrPesWProtocolNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrPesWProtocolFINALIX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FINALIX_TXT'
      Size = 5
      Calculated = True
    end
  end
  object DsPesWProtocol: TDataSource
    DataSet = QrPesWProtocol
    Left = 423
    Top = 346
  end
end
