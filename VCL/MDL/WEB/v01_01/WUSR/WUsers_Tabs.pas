unit WUsers_Tabs;

////////////////////////////////////////////////////////////////////////////////
// Formas na pasta C:\Projetos\Delphi 2007\Outros\Web\Users
////////////////////////////////////////////////////////////////////////////////

{ Colocar no MyListas:

Uses WUsers_Tabs;


//
function TMyListas.CriaListaTabelas:
  WUsers_Tb.CarregaListaTabelas(FTabelas);
//
function TMyListas.CriaListaSQL:
  WUsers_Tb.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:
  WUsers_Tb.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
  WUsers_Tb.CarregaListaFRIndices(Tabela, FRIndices, FLIndices);
//
function TMyListas.CriaListaCampos:
  WUsers_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos);
  WUsers_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  WUsers_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}
interface

uses
  System.Generics.Collections, Windows, Messages, SysUtils, Classes, Graphics,
  Controls, Dialogs, DB, (*DBTables,*) UnMyLinguas, Forms, mysqlDBTables, dmkGeral,
  UnDmkEnums, UnGrl_Vars;

type
  TWUsers_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Database: TmySQLDatabase; Lista: TList<TTabelas>;
             DMKID_APP: Integer): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle;
             DMKID_APP: Integer): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList;
             DMKID_APP: Integer): Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>): Boolean;
  end;

//const

var
  WUsers_Tb: TWUsers_Tabs;

implementation

uses UMySQLModule;

function TWUsers_Tabs.CarregaListaTabelas(Database: TmySQLDatabase;
  Lista: TList<TTabelas>; DMKID_APP: Integer): Boolean;
begin
  try
    Result := True;
    //
    if Database.DatabaseName = VAR_DBWEB then
    begin
      MyLinguas.AdTbLst(Lista, False, Lowercase('wopcoes'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('waccounts'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('wperfis'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('wperfisits'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('wperfjan'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('wprotocol'), '');
      //
      if DMKID_APP <> 4 then //O Syndic tem o gerenciamento de senhas pr�prio
        MyLinguas.AdTbLst(Lista, False, Lowercase('wusers'), '');
      //
      MyLinguas.AdTbLst(Lista, False, Lowercase('wusrgrucab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('wusrgruits'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('wtalkmsga'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('wtalkmsgb'), 'wtalkmsga');
      MyLinguas.AdTbLst(Lista, False, Lowercase('wtalkusra'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('wtalkusrb'), 'wtalkusra');
    end;
  except
    raise;
    Result := False;
  end;
end;

function TWUsers_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList; DMKID_APP: Integer): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('wperfjan') then
  begin
    if DMKID_APP = 24 then //Bustrol
    begin
      FListaSQL.Add('Codigo|Nome                          |Descri                                            |Page        |Caminho                |Geral|Menu|Nivel');
      FListaSQL.Add('     1|"Notas Fiscais"               |"Visualiza��o de Notas (NFS-es) emitidas"         |"nfsevis"   |"../modulos/nfse/"     |    0|   1|   30');
      FListaSQL.Add('     2|"Meu Cadastro"                |"Cadastro do Usu�rio"                             |"usercad"   |"../modulos/senhas/"   |    1|   0|    1');
      FListaSQL.Add('     3|"Boletos a vencer"            |"Boletos a vencer (Todos clientes)"               |"abertos"   |"../modulos/bloquetos/"|    0|   1|    6');
      FListaSQL.Add('     4|"Boletos a vencer"            |"Imprimir 2� via de boletos (Do cliente)"         |"via2"      |"../modulos/bloquetos/"|    0|   1|   14');
      FListaSQL.Add('     5|"Boletos vencidos"            |"Boletos vencidos (Todos clientes)"               |"inad"      |"../modulos/bloquetos/"|    0|   1|    6');
      FListaSQL.Add('     6|"Boletos vencidos"            |"Imprimir boletos vencidos (Do cliente)"          |"inadimp"   |"../modulos/bloquetos/"|    0|   1|   14');
      FListaSQL.Add('     7|"Arquivos"                    |"Arquivos WEB"                                    |"downloads" |"../modulos/ftp/"      |    0|   1|   30');
      FListaSQL.Add('     8|"PMV"                         |"Gerenciamento de PMVs"                           |"pmvits"    |"../modulos/bugstrol/" |    0|   1|   30');
      FListaSQL.Add('     9|"Geren. Avisos"               |"Gerenciamento de avisos"                         |"avisos"    |"../modulos/textos/"   |    0|   1|    2');
      FListaSQL.Add('    10|"Avisos"                      |"Visualiza��o de avisos"                          |"avisosview"|"../modulos/textos/"   |    0|   1|   28');
      FListaSQL.Add('    11|"Esqueci minha senha"         |"Esqueci minha senha"                             |"lostpass"  |"../modulos/senhas/"   |    1|   0|    1');
      FListaSQL.Add('    12|"Endere�o"                    |"Endere�os de clientes"                           |"addr"      |"../modulos/entidades/"|    0|   1|   30');
      FListaSQL.Add('    13|"Fotos"                       |"Visualiza��o de fotos"                           |"fotosview" |"../modulos/ftp/"      |    0|   1|   30');
      FListaSQL.Add('    14|"Recebimento de boletos"      |"P�gina de confirma��o de recebimento de boletos" |"recebibloq"|"../modulos/bloquetos/"|    1|   0|    1');
      FListaSQL.Add('    15|"Cunsulta Protocolos Boletos" |"Consulta de protocolos de impress�o de boletos"  |"proimpbol" |"../modulos/bloquetos/"|    0|   1|    2');
    end else
    if DMKID_APP = 4 then //Syndi2
    begin
      FListaSQL.Add('Codigo|Nome                          |Descri                                            |Page        |Caminho                |Geral|Menu|Nivel');
      FListaSQL.Add('     1|"Boletos vencidos"            |"Imprimir boletos vencidos (Do cliente)"          |"inadimp"   |"../modulos/bloquetos/"|    0|   1|   28');
      FListaSQL.Add('     2|"Boletos vencidos"            |"Boletos vencidos (Todos clientes)"               |"inad"      |"../modulos/bloquetos/"|    0|   1|   12');
      FListaSQL.Add('     3|"Boletos a vencer"            |"Boletos a vencer (Todos clientes)"               |"abertos"   |"../modulos/bloquetos/"|    0|   1|   12');
      FListaSQL.Add('     4|"Boletos a vencer"            |"Imprimir 2� via de boletos (Do cliente)"         |"via2"      |"../modulos/bloquetos/"|    0|   1|   28');
      FListaSQL.Add('     5|"Endere�o"                    |"Cadastro de endere�os"                           |"addr"      |"../modulos/entidades/"|    0|   1|   28');
      FListaSQL.Add('     6|"Avisos"                      |"Visualiza��o de avisos"                          |"avisosview"|"../modulos/textos/"   |    0|   1|   28');
      FListaSQL.Add('     7|"Avisos"                      |"Gerenciamento de avisos"                         |"avisos"    |"../modulos/textos/"   |    0|   1|   2');
      FListaSQL.Add('     8|"Documentos"                  |"Arquivos WEB"                                    |"downloads" |"../modulos/ftp/"      |    0|   1|   30');
      FListaSQL.Add('     9|"Fotos"                       |"Visualiza��o de fotos"                           |"fotosview" |"../modulos/ftp/"      |    0|   1|   30');
      FListaSQL.Add('    10|"Notas Fiscais"               |"Visualiza��o de Notas (NFS-es) emitidas"         |"nfsevis"   |"../modulos/nfse/"     |    0|   1|   30');
      FListaSQL.Add('    11|"Recebimento de boletos"      |"P�gina de confirma��o de recebimento de boletos" |"recebibloq"|"../modulos/bloquetos/"|    1|   0|    1');
      FListaSQL.Add('    12|"Cunsulta Protocolos Boletos" |"Consulta de protocolos de impress�o de boletos"  |"proimpbol" |"../modulos/bloquetos/"|    0|   1|    2');
    end;
  end
  else if Uppercase(Tabela) = Uppercase('wperfis') then
  begin
    if DMKID_APP = 24 then //Bustrol
    begin
      FListaSQL.Add('Codigo|Nome       |Tipo');
      FListaSQL.Add('    -1|"GERENCIAL"|   0');
      FListaSQL.Add('    -2|"CLIENTE"  |   2');
      FListaSQL.Add('    -3|"USU�RIO"  |   3');
      FListaSQL.Add('    -4|"PARCEIRO" |   1');
    end else
    if DMKID_APP = 4 then //Syndi2
    begin
      FListaSQL.Add('Codigo|Nome           |Tipo');
      FListaSQL.Add('    1|"Cond�mino"     |   3');
      FListaSQL.Add('    2|"S�ndico"       |   2');
      FListaSQL.Add('    3|"Grupo"         |   1');
      FListaSQL.Add('    4|"Administrador" |   0');
    end;
  end
  else if Uppercase(Tabela) = Uppercase('wperfisits') then
  begin
    if DMKID_APP = 24 then //Bustrol
    begin
      FListaSQL.Add('Codigo|Controle|Janela|Libera');
      FListaSQL.Add('    -1|      -1|     1|     1');
      FListaSQL.Add('    -1|      -2|     2|     1');
      FListaSQL.Add('    -1|      -3|     3|     1');
      FListaSQL.Add('    -1|      -4|     4|     1');
      FListaSQL.Add('    -1|      -5|     5|     1');
      FListaSQL.Add('    -1|      -6|     6|     1');
      FListaSQL.Add('    -1|      -7|     7|     1');
      FListaSQL.Add('    -1|      -8|     8|     1');
      FListaSQL.Add('    -2|      -9|     1|     1');
      FListaSQL.Add('    -2|     -10|     2|     1');
      FListaSQL.Add('    -2|     -11|     3|     0');
      FListaSQL.Add('    -2|     -12|     4|     1');
      FListaSQL.Add('    -2|     -13|     5|     0');
      FListaSQL.Add('    -2|     -14|     6|     1');
      FListaSQL.Add('    -2|     -15|     7|     1');
      FListaSQL.Add('    -2|     -16|     8|     1');
      FListaSQL.Add('    -1|     -17|     9|     1');
      FListaSQL.Add('    -2|     -18|     9|     0');
      FListaSQL.Add('    -1|     -19|    10|     0');
      FListaSQL.Add('    -2|     -20|    10|     1');
      FListaSQL.Add('    -1|     -21|    11|     1');
      FListaSQL.Add('    -2|     -22|    11|     1');
      FListaSQL.Add('    -1|     -23|    12|     0');
      FListaSQL.Add('    -2|     -24|    12|     0');
      FListaSQL.Add('    -3|     -25|     1|     0');
      FListaSQL.Add('    -3|     -26|     2|     0');
      FListaSQL.Add('    -3|     -27|     3|     0');
      FListaSQL.Add('    -3|     -28|     4|     0');
      FListaSQL.Add('    -3|     -29|     5|     0');
      FListaSQL.Add('    -3|     -30|     6|     0');
      FListaSQL.Add('    -3|     -31|     7|     0');
      FListaSQL.Add('    -3|     -32|     8|     0');
      FListaSQL.Add('    -3|     -33|     9|     0');
      FListaSQL.Add('    -3|     -34|    10|     0');
      FListaSQL.Add('    -3|     -35|    11|     0');
      FListaSQL.Add('    -3|     -36|    12|     0');
      FListaSQL.Add('    -4|     -37|     1|     0');
      FListaSQL.Add('    -4|     -38|     2|     0');
      FListaSQL.Add('    -4|     -39|     3|     0');
      FListaSQL.Add('    -4|     -40|     4|     0');
      FListaSQL.Add('    -4|     -41|     5|     0');
      FListaSQL.Add('    -4|     -42|     6|     0');
      FListaSQL.Add('    -4|     -43|     7|     0');
      FListaSQL.Add('    -4|     -44|     8|     0');
      FListaSQL.Add('    -4|     -45|     9|     0');
      FListaSQL.Add('    -4|     -46|    10|     0');
      FListaSQL.Add('    -4|     -47|    11|     0');
      FListaSQL.Add('    -4|     -48|    12|     0');
      FListaSQL.Add('    -4|     -49|    13|     0');
      FListaSQL.Add('    -3|     -50|    13|     0');
      FListaSQL.Add('    -2|     -51|    13|     1');
      FListaSQL.Add('    -1|     -52|    13|     1');
    end else
    if DMKID_APP = 4 then //Syndi2
    begin
      FListaSQL.Add('Codigo|Controle|Janela|Libera');
      FListaSQL.Add('     1|       1|     1|     1');
      FListaSQL.Add('     2|       2|     1|     1');
      FListaSQL.Add('     3|       3|     1|     1');
      FListaSQL.Add('     4|       4|     1|     0');
      FListaSQL.Add('     1|       5|     2|     0');
      FListaSQL.Add('     2|       6|     2|     1');
      FListaSQL.Add('     3|       7|     2|     1');
      FListaSQL.Add('     4|       8|     2|     0');
      FListaSQL.Add('     1|       9|     3|     0');
      FListaSQL.Add('     2|      10|     3|     1');
      FListaSQL.Add('     3|      11|     3|     1');
      FListaSQL.Add('     4|      12|     3|     0');
      FListaSQL.Add('     1|      13|     4|     1');
      FListaSQL.Add('     2|      14|     4|     1');
      FListaSQL.Add('     3|      15|     4|     1');
      FListaSQL.Add('     4|      16|     4|     0');
      FListaSQL.Add('     1|      17|     5|     1');
      FListaSQL.Add('     2|      18|     5|     1');
      FListaSQL.Add('     3|      19|     5|     1');
      FListaSQL.Add('     4|      20|     5|     0');
      FListaSQL.Add('     1|      21|     6|     1');
      FListaSQL.Add('     2|      22|     6|     1');
      FListaSQL.Add('     3|      23|     6|     1');
      FListaSQL.Add('     4|      24|     6|     0');
      FListaSQL.Add('     1|      25|     7|     0');
      FListaSQL.Add('     2|      26|     7|     0');
      FListaSQL.Add('     3|      27|     7|     0');
      FListaSQL.Add('     4|      28|     7|     1');
      FListaSQL.Add('     1|      29|     8|     1');
      FListaSQL.Add('     2|      30|     8|     1');
      FListaSQL.Add('     3|      31|     8|     1');
      FListaSQL.Add('     4|      32|     8|     0');
      FListaSQL.Add('     1|      33|     9|     1');
      FListaSQL.Add('     2|      34|     9|     1');
      FListaSQL.Add('     3|      35|     9|     1');
      FListaSQL.Add('     4|      36|     9|     0');
      FListaSQL.Add('     1|      37|    10|     0');
      FListaSQL.Add('     2|      38|    10|     0');
      FListaSQL.Add('     3|      39|    10|     0');
      FListaSQL.Add('     4|      40|    10|     0');
      FListaSQL.Add('     4|      41|    11|     0');
      FListaSQL.Add('     4|      42|    11|     0');
      FListaSQL.Add('     4|      43|    11|     0');
      FListaSQL.Add('     4|      44|    11|     0');
      FListaSQL.Add('     1|      45|    12|     0');
      FListaSQL.Add('     2|      46|    12|     0');
      FListaSQL.Add('     3|      47|    12|     0');
      FListaSQL.Add('     4|      48|    12|     1');
    end else
    if Uppercase(Tabela) = Uppercase('wopcoes') then
    begin
      if FListaSQL.Count = 0 then
      FListaSQL.Add('Codigo');
      FListaSQL.Add('     1');
    end;
  end;
end;

function TWUsers_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
  TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('') then
  begin
  end else if Uppercase(Tabela) = Uppercase('XXXXX') then
  begin
    //FListaSQL.Add('"Entidades","Cadastro de pessoas f�sicas e jur�dicas (clientes, fornecedores, etc.)"');
    //FListaSQL.Add('"",""');
  end;
end;


function TWUsers_Tabs.CarregaListaFRIndices(Tabela: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('wperfis') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else if Uppercase(Tabela) = Uppercase('wperfisits') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else if Uppercase(Tabela) = Uppercase('wperfjan') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else if Uppercase(Tabela) = Uppercase('wprotocol') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else if Uppercase(Tabela) = Uppercase('waccounts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo'; //C�digo da tabela de usu�rios
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Tipo'; //Apenas para o Syndinet por causa das 3 tabelas de login
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Account';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'IdAccount';
    FLIndices.Add(FRIndices);
    //
  end else if Uppercase(Tabela) = Uppercase('wusers') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Username';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'Password';
    FLIndices.Add(FRIndices);
    //
  end else if Uppercase(Tabela) = Uppercase('wusrgrucab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else if Uppercase(Tabela) = Uppercase('wusrgruits') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else if Uppercase(Tabela) = Uppercase('wtalkmsga') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else if Uppercase(Tabela) = Uppercase('wtalkusra') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Pessoa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('wopcoes') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end;
end;

function TWUsers_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle;
 DMKID_APP: Integer): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('wperfis') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('wperfisits') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Janela';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Libera';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('wperfjan') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descri';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Page';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Caminho';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Geral';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Menu';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);                     //01 - Liberado
      FRCampos.Field      := 'Nivel';    //02 - Boss
      FRCampos.Tipo       := 'int(11)';  //04 - Administrador
      FRCampos.Null       := '';         //08 - Cliente
      FRCampos.Key        := '';         //16 - Usu�rio
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('wprotocol') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tarefa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataExp';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IPCad';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '127.0.0.1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IPAlt';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '127.0.0.1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataFin';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);                       //1 - Sim 0 - N�o
      FRCampos.Field      := 'Finaliz';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Usuario';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Perfil';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //      
    end else
    if Uppercase(Tabela) = Uppercase('waccounts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';        //Preencher apenas no SindyNet
      FRCampos.Tipo       := 'tinyint(1)';  //por causa das 3 tabelas de login
      FRCampos.Null       := 'YES';         //nos demais deixar em branco
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IdAccount';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);                        //Preencher com Facebook.com
      FRCampos.Field      := 'Account';     //O �nico que ter� suporte inicialmente
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PersonalName';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Email';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PNatal';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PictureUrl';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('wusers') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := 'auto_increment';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Username';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Password';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Email';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PersonalName';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ImageUrl';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LoginID';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LoginID_Desktop';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LoginID_Mobile';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Perfil';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';          // 0 ADMINISTRADOR
      FRCampos.Tipo       := 'tinyint(1)';    // 1 BOSS
      FRCampos.Null       := '';              // 2 CLIENTE
      FRCampos.Key        := '';              // 3 USU�RIO
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Status';        // 0 Dispon�vel
      FRCampos.Tipo       := 'tinyint(1)';    // 1 Invis�vel
      FRCampos.Null       := '';              // 2 Ocupado
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TimeZone';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'America/Sao_Paulo';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaLastLogUTC';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaNatal';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sexo';
      FRCampos.Tipo       := 'tinyint(1)'; // 0- N�o Informado
      FRCampos.Null       := '';           // 1- Masculino
      FRCampos.Key        := '';           // 2- Feminino
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Termo_Hash'; //Hash do arquivo publicado SHA1
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Priva_Hash'; //Hash do arquivo publicado SHA1
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Termo_DtaHoraAceite'; //Data Hora UTC do aceite
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'AutorizaUsu'; //Permite usu�rios a solicitarem servi�os pass�veis de cobran�a
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('wusrgrucab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('wusrgruits') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Usuario';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('wtalkmsga') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'bigint(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Conversa';
      FRCampos.Tipo       := 'bigint(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UTC';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';        //0 - Grupo
      FRCampos.Tipo       := 'tinyint(1)';  //1 - Pessoa
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Grupo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pessoa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Texto';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('wtalkusra') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'bigint(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pessoa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Offline';     //0 - False
      FRCampos.Tipo       := 'tinyint(1)';  //1 - True
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Lido';        //0 - False
      FRCampos.Tipo       := 'tinyint(1)';  //1 - True
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('wopcoes') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaSincro';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SincroEmExec';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TWUsers_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      {New(FRCampos);
      FRCampos.Field      := 'FTPConfig';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //}
    end;
  except
    raise;
    Result := False;
  end;
end;

function TWUsers_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  // WEB-PERFI-002 :: Cadastro de Janelas WEB
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-PERFI-002';
  FRJanelas.Nome      := 'FmWPerfJan';
  FRJanelas.Descricao := 'Cadastro de Janelas WEB';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-PERFI-001 :: Cadastro de perifis WEB
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-PERFI-001';
  FRJanelas.Nome      := 'FmWPerfis';
  FRJanelas.Descricao := 'Cadastro de perfis WEB';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-USERS-001 :: Cadastro de usu�rios WEB
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-USERS-001';
  FRJanelas.Nome      := 'FmWUsers';
  FRJanelas.Descricao := 'Cadastro de usu�rios WEB';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-USERS-002 :: Pesquisa de usu�rios WEB
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-USERS-002';
  FRJanelas.Nome      := 'FmWUsersPesq';
  FRJanelas.Descricao := 'Pesquisa de usu�rios WEB';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-PROTO-001 :: Cadastro de Protocolos WEB
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-PROTO-001';
  FRJanelas.Nome      := 'FmWProtocol';
  FRJanelas.Descricao := 'Cadastro de Protocolos WEB';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-PROTO-002 :: Op��es de Protocolos WEB
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-PROTO-002';
  FRJanelas.Nome      := 'FmWPrOpcoes';
  FRJanelas.Descricao := 'Op��es de Protocolos WEB';
  FLJanelas.Add(FRJanelas);
  //  
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.
