unit UnWUsersJan;

interface

uses mySQLDbTables, UMySQLModule, dmkGeral, dmkImage, Forms, Controls, Windows,
  SysUtils, ComCtrls, Grids, DBGrids, UnDmkProcFunc, ExtCtrls, dmkDBGrid, Math,
  UnDmkEnums, UnProjGroup_Consts;

type
  TUnWUsersJan = class(TObject)
  private
  public
    procedure MostraWPerfJan(SoMaster: Boolean);
    procedure MostraWPerfis(Codigo: Integer);
    procedure MostraWUsers(Codigo: Integer);
  end;

var
  UWUsersJan: TUnWUsersJan;

implementation

uses MyListas, Module, DmkDAC_PF, UnMyObjects, MyDBCheck, UnDmkWeb, UnInternalConsts,
  WPerfis, WUsers, WPerfJan, ModuleGeral;

{ TUnWUsersJan }

procedure TUnWUsersJan.MostraWPerfis(Codigo: Integer);
var
  Msg: String;
begin
  if DmkWeb.VerificaSeModuloWebEstaAtivo(CO_DMKID_APP,
    DModG.QrMasterHabilModulos.Value, Dmod.MyDBn, Msg) then
  begin
    if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
    begin
      if DBCheck.CriaFm(TFmWPerfis, FmWPerfis, afmoNegarComAviso) then
      begin
        if Codigo <> 0 then
          FmWPerfis.LocCod(Codigo, Codigo);
        FmWPerfis.ShowModal;
        FmWPerfis.Destroy;
      end;
    end;
  end else
    Geral.MB_Aviso(Msg);
end;

procedure TUnWUsersJan.MostraWUsers(Codigo: Integer);
var
  Msg: String;
begin
  if DmkWeb.VerificaSeModuloWebEstaAtivo(CO_DMKID_APP,
    DModG.QrMasterHabilModulos.Value, Dmod.MyDBn, Msg) then
  begin
    //Usar a vers�o 2.0 onde cadastra direto no site
    (*
    if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
    begin
      DModG.ReopenOpcoesGerl;
      //
      if DBCheck.CriaFm(TFmWUsers, FmWUsers, afmoNegarComAviso) then
      begin
        if Codigo <> 0 then
          FmWUsers.LocCod(Codigo, Codigo);
        FmWUsers.ShowModal;
        if FmWUsers <> nil then
          FmWUsers.Destroy;
      end;
    end;
    *)
    Geral.MB_Aviso('Utilize o bot�o: "Senhas" na aba: "WEB 2.0"!');
  end else
    Geral.MB_Aviso(Msg);
end;

procedure TUnWUsersJan.MostraWPerfJan(SoMaster: Boolean);
var
  Msg: String;
  Modo: TAcessFmModo;
begin
  if DmkWeb.VerificaSeModuloWebEstaAtivo(CO_DMKID_APP,
    DModG.QrMasterHabilModulos.Value, Dmod.MyDBn, Msg) then
  begin
    if SoMaster then
      Modo := afmoSoMaster
    else
      Modo := afmoNegarComAviso;
    //
    if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
    begin
      if DBCheck.CriaFm(TFmWPerfJan, FmWPerfJan, Modo) then
      begin
        FmWPerfJan.ShowModal;
        FmWPerfJan.Destroy;
      end;
    end;
  end else
    Geral.MB_Aviso(Msg);
end;

end.
