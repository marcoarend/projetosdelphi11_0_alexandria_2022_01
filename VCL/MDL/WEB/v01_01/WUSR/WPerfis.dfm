object FmWPerfis: TFmWPerfis
  Left = 368
  Top = 194
  ActiveControl = SbImprime
  Caption = 'WEB-PERFI-001 :: Cadastro de perifis WEB'
  ClientHeight = 444
  ClientWidth = 701
  Color = clBtnFace
  Constraints.MinHeight = 256
  Constraints.MinWidth = 630
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 47
    Width = 701
    Height = 397
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object Label10: TLabel
      Left = 21
      Top = 62
      Width = 31
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Nome:'
    end
    object Label9: TLabel
      Left = 21
      Top = 12
      Width = 36
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'C'#243'digo:'
    end
    object PainelConfirma: TPanel
      Left = 1
      Top = 337
      Width = 699
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 5
      ExplicitTop = 436
      ExplicitWidth = 874
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 10
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 740
        Top = 1
        Width = 133
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 110
          Height = 50
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object EdNome: TdmkEdit
      Left = 21
      Top = 82
      Width = 577
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Nome'
      UpdCampo = 'Nome'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnExit = EdNomeExit
    end
    object CkContinuar: TCheckBox
      Left = 90
      Top = 180
      Width = 138
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Continuar inserindo.'
      TabOrder = 4
    end
    object EdCodigo: TdmkEdit
      Left = 21
      Top = 31
      Width = 87
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Color = clInactiveCaption
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object dmkCkAtivo: TdmkCheckBox
      Left = 21
      Top = 180
      Width = 59
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Ativo'
      TabOrder = 3
      QryCampo = 'Ativo'
      UpdCampo = 'Ativo'
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
    object RgTipo: TdmkRadioGroup
      Left = 21
      Top = 116
      Width = 577
      Height = 56
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Tipo de perfil'
      Columns = 4
      Items.Strings = (
        'Administrador'
        'Boss'
        'Cliente'
        'Usu'#225'rio')
      TabOrder = 2
      UpdType = utYes
      OldValor = 0
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 47
    Width = 701
    Height = 397
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object PainelControle: TPanel
      Left = 1
      Top = 337
      Width = 699
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 436
      ExplicitWidth = 874
      object LaRegistro: TLabel
        Left = 213
        Top = 1
        Width = 26
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = '[N]: 0'
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 212
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 295
        Top = 1
        Width = 578
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object Panel2: TPanel
          Left = 443
          Top = 0
          Width = 134
          Height = 57
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 111
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 5
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 118
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 230
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Exclui banco atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
        end
      end
    end
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 699
      Height = 113
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Enabled = False
      TabOrder = 1
      ExplicitWidth = 874
      object Label2: TLabel
        Left = 7
        Top = 60
        Width = 51
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
      end
      object Label1: TLabel
        Left = 7
        Top = 9
        Width = 36
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 7
        Top = 27
        Width = 89
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Codigo'
        DataSource = DsWPerfis
        TabOrder = 0
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 7
        Top = 78
        Width = 578
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Nome'
        DataSource = DsWPerfis
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object CkAtivo: TDBCheckBox
        Left = 592
        Top = 79
        Width = 59
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ativo'
        DataField = 'Ativo'
        DataSource = DsWPerfis
        TabOrder = 2
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBRGNivel: TDBRadioGroup
        Left = 103
        Top = 9
        Width = 548
        Height = 55
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Tipo de acesso'
        Columns = 4
        DataField = 'Tipo'
        DataSource = DsWPerfis
        Items.Strings = (
          'Administrador'
          'Boss'
          'Cliente'
          'Usu'#225'rio')
        TabOrder = 3
        Values.Strings = (
          '0'
          '1'
          '2'
          '3')
      end
    end
    object PnDados: TPanel
      Left = 1
      Top = 114
      Width = 699
      Height = 192
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 2
      ExplicitWidth = 874
      object Grade: TdmkDBGridDAC
        Left = 1
        Top = 80
        Width = 872
        Height = 111
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'Libera'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Janela'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Page'
            Title.Caption = 'C'#243'digo da janela'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descri'
            Title.Caption = 'Descri'#231#227'o'
            Width = 250
            Visible = True
          end>
        Color = clWindow
        DataSource = DsWPerfisIts
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = GradeCellClick
        EditForceNextYear = False
        Columns = <
          item
            Expanded = False
            FieldName = 'Libera'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Janela'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Page'
            Title.Caption = 'C'#243'digo da janela'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descri'
            Title.Caption = 'Descri'#231#227'o'
            Width = 250
            Visible = True
          end>
      end
      object PnControle: TPanel
        Left = 1
        Top = 1
        Width = 872
        Height = 79
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        TabOrder = 1
        Visible = False
        object BtTudo: TBitBtn
          Tag = 127
          Left = 6
          Top = 6
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Todos'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtTudoClick
        end
        object BtNenhum: TBitBtn
          Tag = 128
          Left = 119
          Top = 6
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Nenhum'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtNenhumClick
        end
        object Panel6: TPanel
          Left = 679
          Top = 1
          Width = 191
          Height = 56
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 2
          object BtRefresh: TBitBtn
            Tag = 18
            Left = 10
            Top = 5
            Width = 172
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Atualiza todos perfis'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtRefreshClick
          end
        end
        object PB1: TProgressBar
          Left = 1
          Top = 57
          Width = 869
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          TabOrder = 3
          Visible = False
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 701
    Height = 47
    Align = alTop
    Alignment = taLeftJustify
    Caption = '                              Perfis'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -30
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object LaTipo: TdmkLabel
      Left = 774
      Top = 1
      Width = 101
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
    end
    object Image1: TImage
      Left = 278
      Top = 1
      Width = 496
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 277
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 4
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsWPerfis: TDataSource
    DataSet = QrWPerfis
    Left = 400
    Top = 8
  end
  object QrWPerfis: TMySQLQuery
    Database = Dmod.MyLocDatabase
    BeforeOpen = QrWPerfisBeforeOpen
    AfterOpen = QrWPerfisAfterOpen
    BeforeClose = QrWPerfisBeforeClose
    AfterScroll = QrWPerfisAfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM wperfis'
      'WHERE Codigo > 0')
    Left = 372
    Top = 8
    object QrWPerfisCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'wperfis.Codigo'
    end
    object QrWPerfisNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'wperfis.Nome'
      Size = 50
    end
    object QrWPerfisLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'wperfis.Lk'
    end
    object QrWPerfisDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'wperfis.DataCad'
    end
    object QrWPerfisDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'wperfis.DataAlt'
    end
    object QrWPerfisUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'wperfis.UserCad'
    end
    object QrWPerfisUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'wperfis.UserAlt'
    end
    object QrWPerfisAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'wperfis.AlterWeb'
    end
    object QrWPerfisAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'wperfis.Ativo'
    end
    object QrWPerfisTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'wperfis.Tipo'
    end
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyLocDatabase
    Left = 668
    Top = 52
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 484
    Top = 8
  end
  object QrWPerfisIts: TMySQLQuery
    Database = Dmod.MyLocDatabase
    BeforeOpen = QrWPerfisBeforeOpen
    AfterOpen = QrWPerfisAfterOpen
    AfterScroll = QrWPerfisItsAfterScroll
    SQL.Strings = (
      'SELECT wit.Controle, wit.Libera, wja.Nome, '
      'wja.Page, wja.Descri'
      'FROM wperfisits wit'
      'LEFT JOIN wperfjan wja ON wja.Codigo = wit.Janela'
      'WHERE wit.Codigo=:P0')
    Left = 428
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrWPerfisItsLibera: TSmallintField
      FieldName = 'Libera'
      Origin = 'wperfisits.Libera'
      MaxValue = 1
    end
    object QrWPerfisItsNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'wperfjan.Nome'
      Size = 10
    end
    object QrWPerfisItsPage: TWideStringField
      FieldName = 'Page'
      Origin = 'wperfjan.Page'
      Size = 10
    end
    object QrWPerfisItsDescri: TWideStringField
      FieldName = 'Descri'
      Origin = 'wperfjan.Descri'
      Size = 255
    end
    object QrWPerfisItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'wperfisits.Controle'
    end
  end
  object DsWPerfisIts: TDataSource
    DataSet = QrWPerfisIts
    Left = 456
    Top = 8
  end
  object QrLoc2: TMySQLQuery
    Database = Dmod.MyLocDatabase
    Left = 640
    Top = 52
  end
  object QrLoc3: TMySQLQuery
    Database = Dmod.MyLocDatabase
    Left = 612
    Top = 52
  end
end
