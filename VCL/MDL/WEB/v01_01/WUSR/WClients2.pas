unit WClients2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, Menus, ComCtrls, dmkDBGrid, dmkEdit, dmkGeral, UnDmkProcFunc,
  dmkImage, UnDmkEnums, DmkDAC_PF, dmkEditCB, dmkDBLookupComboBox;

type
  TFmWClients2 = class(TForm)
    PainelDados: TPanel;
    Dswclients: TDataSource;
    QrWClients: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    PainelData: TPanel;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    DBEdit81: TDBEdit;
    Label80: TLabel;
    EdNomeWeb: TdmkEdit;
    Label8: TLabel;
    PainelUser: TPanel;
    Panel10: TPanel;
    Label27: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Edit001: TdmkEdit;
    EdULogin: TdmkEdit;
    EdUSenha: TdmkEdit;
    PMUsuario: TPopupMenu;
    QrUsers: TmySQLQuery;
    DsUsers: TDataSource;
    EdUser2: TdmkEdit;
    EdPass2: TdmkEdit;
    Label29: TLabel;
    Label30: TLabel;
    Alterausurioatual1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    QrWClientsCodigo: TIntegerField;
    QrWClientsCliente: TIntegerField;
    QrWClientsNCONDOM: TWideStringField;
    QrWClientsCONDTEL: TWideStringField;
    DBEdit001: TDBEdit;
    QrWClientsCNPJ_CPF: TWideStringField;
    Label1: TLabel;
    DBEdit002: TDBEdit;
    dmkDBGUsers: TdmkDBGrid;
    QrUsersCodigo: TIntegerField;
    QrUsersUSER_NOME: TWideStringField;
    QrUsersUnidade: TWideStringField;
    QrUsersAPTO: TIntegerField;
    QrUsersUser_ID: TIntegerField;
    QrUsersCodCliEsp: TIntegerField;
    QrUsersCodigoEsp: TIntegerField;
    QrUsersUsername: TWideStringField;
    QrUsersPassword: TWideStringField;
    QrUsersLoginID: TWideStringField;
    QrUsersLastAcess: TDateTimeField;
    QrUsersTipo: TSmallintField;
    QrUsersLk: TIntegerField;
    QrUsersDataCad: TDateField;
    QrUsersDataAlt: TDateField;
    QrUsersUserCad: TIntegerField;
    QrUsersUserAlt: TIntegerField;
    QrUsersNIVEL: TWideStringField;
    QrExiste: TmySQLQuery;
    QrExistePassword: TWideStringField;
    QrUsersPropriet: TIntegerField;
    QrWClientsNomeWeb: TWideStringField;
    QrWClientsUsername: TWideStringField;
    QrWClientsPassword: TWideStringField;
    QrWClientsUser_ID: TAutoIncField;
    Label10: TLabel;
    DBEdit1: TDBEdit;
    Label12: TLabel;
    DBEdit3: TDBEdit;
    Label13: TLabel;
    DBEdit4: TDBEdit;
    Label14: TLabel;
    DBEdit5: TDBEdit;
    RecriaLoginsesenhas1: TMenuItem;
    Atual1: TMenuItem;
    Selecionados1: TMenuItem;
    Todos1: TMenuItem;
    Label9: TLabel;
    DBEdit7: TDBEdit;
    Label15: TLabel;
    DBEdit8: TDBEdit;
    Label16: TLabel;
    DBEdit9: TDBEdit;
    GroupBox1: TGroupBox;
    dmkEdParc_ValMin: TdmkEdit;
    Label17: TLabel;
    Label18: TLabel;
    dmkEdParc_QtdMax: TdmkEdit;
    QrWClientsParc_ValMin: TFloatField;
    QrWClientsParc_QtdMax: TIntegerField;
    GroupBox2: TGroupBox;
    Label19: TLabel;
    Label20: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    Label7: TLabel;
    EdNomeSindico: TdmkEdit;
    QrWClientsNomeSindico: TWideStringField;
    dmkEdMulta: TdmkEdit;
    dmkEdJuros: TdmkEdit;
    Label21: TLabel;
    Label22: TLabel;
    PnCabeca: TPanel;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel14: TPanel;
    BtSaida: TBitBtn;
    BtCliente: TBitBtn;
    BtUsuario: TBitBtn;
    BtDono: TBitBtn;
    GroupBox3: TGroupBox;
    Panel11: TPanel;
    Panel12: TPanel;
    BitBtn1: TBitBtn;
    BitBtn3: TBitBtn;
    GroupBox4: TGroupBox;
    Panel8: TPanel;
    Panel9: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    RGPwdSite: TRadioGroup;
    CkOcultaBloq: TCheckBox;
    DBRadioGroup1: TDBRadioGroup;
    DBCheckBox1: TDBCheckBox;
    QrWClientsJuros: TFloatField;
    QrWClientsMulta: TFloatField;
    QrWClientsPwdSite: TSmallintField;
    QrWClientsOcultaBloq: TIntegerField;
    QrWClientsCNPJ_TXT: TWideStringField;
    Label23: TLabel;
    DBEdit12: TDBEdit;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    Label3: TLabel;
    EdEMail: TdmkEdit;
    QrUsersEmail: TWideStringField;
    Label4: TLabel;
    dmkEdit1: TdmkEdit;
    Label5: TLabel;
    DBEdit13: TDBEdit;
    QrWClientsEmail: TWideStringField;
    CBPerfil: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    EdPerfil: TdmkEditCB;
    Label11: TLabel;
    QrWPerfil: TmySQLQuery;
    DsWPerfil: TDataSource;
    CBPerfilImv: TdmkDBLookupComboBox;
    EdPerfilImv: TdmkEditCB;
    Label24: TLabel;
    SpeedButton6: TSpeedButton;
    QrWPerfilImv: TmySQLQuery;
    DsWPerfilImv: TDataSource;
    QrWPerfilCodigo: TIntegerField;
    QrWPerfilNome: TWideStringField;
    QrWPerfilImvCodigo: TIntegerField;
    QrWPerfilImvNome: TWideStringField;
    QrWClientsPerfil: TIntegerField;
    QrWClientsPerfilImv: TIntegerField;
    PB1: TProgressBar;
    QrUsersUSUARIO: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtClienteClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrWClientsAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrWClientsAfterScroll(DataSet: TDataSet);
    procedure FormResize(Sender: TObject);
    procedure QrWClientsCalcFields(DataSet: TDataSet);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrWClientsBeforeClose(DataSet: TDataSet);
    procedure BtUsuarioClick(Sender: TObject);
    procedure Alterausurioatual1Click(Sender: TObject);
    procedure BtDonoClick(Sender: TObject);
    procedure Atual1Click(Sender: TObject);
    procedure Selecionados1Click(Sender: TObject);
    procedure Todos1Click(Sender: TObject);
    procedure dmkDBGUsersDblClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure PMUsuarioPopup(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
  private
    FPageIndex: Integer;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    ////
    procedure RecriaLoginsESenhas(Quem: Integer; Quais: TSelType);
    procedure ReopenUsers(APTO: Integer);
    function  GeraNovaSenha(): String;
    procedure EnableBtns();
    procedure MostraWUsers();
    procedure ReopenWPerfil(Query: TmySQLQuery; Tipo: Integer);
  public
    { Public declarations }
    FSeq: Integer;
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmWClients2: TFmWClients2;
const
  FFormatFloat = '00000';

implementation

uses Module, UnMyObjects, Principal, MyDBCheck, WUsers, UnDmkWeb, ModuleGeral,
  UnWUsersJan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmWClients2.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmWClients2.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrWClientsCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmWClients2.DefParams;
begin
  VAR_GOTOTABELA := 'cond';
  VAR_GOTOMYSQLTABLE := QrWClients;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOMySQLDBNAME2 := nil;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT con.Codigo, con.Cliente, wcl.Email, ');
  VAR_SQLx.Add('IF(rec.Tipo=0, rec.RazaoSocial, rec.Nome) NCONDOM, ');
  VAR_SQLx.Add('IF(rec.Tipo=0, rec.CNPJ, rec.CPF) CNPJ_CPF, ');
  VAR_SQLx.Add('IF(rec.Tipo=0, rec.ETe1, rec.PTe2) CONDTEL, ');
  VAR_SQLx.Add('wcl.NomeWeb, wcl.Username, wcl.Password, wcl.User_ID, ');
  VAR_SQLx.Add('wcl.Parc_ValMin, wcl.Parc_QtdMax, wcl.NomeSindico, ');
  VAR_SQLx.Add('wcl.Juros, wcl.Multa, con.PwdSite, con.OcultaBloq, ');
  VAR_SQLx.Add('con.Perfil, con.PerfilImv ');
  VAR_SQLx.Add('FROM cond con ');
  VAR_SQLx.Add('LEFT JOIN entidades rec ON rec.Codigo=con.Cliente ');
  VAR_SQLx.Add('LEFT JOIN wclients  wcl ON wcl.CodCliEsp=con.Codigo ');
  VAR_SQLx.Add('WHERE con.Codigo > 0 ');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND con.Codigo=:P0');
  //
  VAR_SQLa.Add('AND (IF(rec.Tipo=0, rec.RazaoSocial, rec.Nome) LIKE :P0');
  //
end;

procedure TFmWClients2.dmkDBGUsersDblClick(Sender: TObject);
begin
  Geral.MB_Aviso('Unidade: ' + QrUsersUnidade.Value + sLineBreak +
    'Nome: ' + QrUsersUSER_NOME.Value + sLineBreak +
    'Login: ' + QrUsersUsername.Value + sLineBreak +
    'Senha: ' + QrUsersPassword.Value + sLineBreak +
    'E-mail: ' + QrUsersEmail.Value);
end;

procedure TFmWClients2.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
      PainelUser.Visible  := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      //
      EdNomeWeb.Text                := QrWClientsNomeWeb.Value;
      EdUser2.Text                  := QrWClientsUsername.Value;
      EdPass2.Text                  := QrWClientsPassword.Value;
      EdNomeSindico.Text            := QrWClientsNomeSindico.Value;
      dmkEdParc_ValMin.ValueVariant := QrWClientsParc_ValMin.Value;
      dmkEdParc_QtdMax.ValueVariant := QrWClientsParc_QtdMax.Value;
      dmkEdMulta.ValueVariant       := QrWClientsMulta.Value;
      dmkEdJuros.ValueVariant       := QrWClientsJuros.Value;
      RGPwdSite.ItemIndex           := QrWClientsPwdSite.Value;
      CkOcultaBloq.Checked          := Geral.IntToBool(QrWClientsOcultaBloq.Value);
      EdEMail.ValueVariant          := QrWClientsEmail.Value;
      EdPerfil.ValueVariant         := QrWClientsPerfil.Value;
      CBPerfil.KeyValue             := QrWClientsPerfil.Value;
      EdPerfilImv.ValueVariant      := QrWClientsPerfilImv.Value;
      CBPerfilImv.KeyValue          := QrWClientsPerfilImv.Value;
      EdNomeWeb.SetFocus;
    end;
    3:
    begin
      PainelUser.Visible  := True;
      PainelDados.Visible := False;
      EdULogin.SetFocus;
      //
      if SQLType = stUpd then
      begin
        EdULogin.Text := QrUsersUsername.Value;
        EdUSenha.Text := QrUsersPassword.Value;
      end;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmWClients2.MostraWUsers;
var
  Cond, User: Integer;
begin
  if DBCheck.CriaFm(TFmWUsers, FmWUsers, afmoNegarComAviso) then
  begin
    FmWUsers.ShowModal;
    //
    Cond := FmWUsers.FCond;
    User := FmWUsers.FUser_ID;
    //
    if (Cond <> 0) and (User <> 0) then
    begin
      LocCod(Cond, Cond);
      //
      QrUsers.Locate('User_ID', User, []);
    end;
    //
    FmWUsers.Destroy;
  end;
end;

procedure TFmWClients2.PMUsuarioPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrWClients.State <> dsInactive) and (QrWClients.RecordCount > 0);
  Enab2 := (QrUsers.State <> dsInactive) and (QrUsers.RecordCount > 0);
  //
  Alterausurioatual1.Enabled   := Enab and Enab2;
  RecriaLoginsesenhas1.Enabled := Enab and Enab2;
end;

procedure TFmWClients2.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmWClients2.QueryPrincipalAfterOpen;
begin
end;

procedure TFmWClients2.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmWClients2.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmWClients2.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmWClients2.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmWClients2.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmWClients2.SpeedButton5Click(Sender: TObject);
var
  Perfil: Integer;
begin
  VAR_CADASTRO := 0;
  Perfil       := EdPerfil.ValueVariant;
  //
  UWUsersJan.MostraWPerfis(Perfil);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdPerfil, CBPerfil, QrWPerfil, VAR_CADASTRO);
    //
    EdPerfil.SetFocus;
  end;
end;

procedure TFmWClients2.SpeedButton6Click(Sender: TObject);
var
  Perfil: Integer;
begin
  VAR_CADASTRO := 0;
  Perfil       := EdPerfilImv.ValueVariant;
  //
  UWUsersJan.MostraWPerfis(Perfil);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdPerfilImv, CBPerfilImv, QrWPerfilImv, VAR_CADASTRO);
    //
    EdPerfilImv.SetFocus;
  end;
end;

procedure TFmWClients2.BtClienteClick(Sender: TObject);
begin
  if (QrWClients.State <> dsInactive) and (QrWClients.RecordCount > 0) then
  begin
    if QrWClientsUser_ID.Value = 0 then
      MostraEdicao(1, stIns, 0)
    else
      MostraEdicao(1, stUpd, 0);
  end;
end;

procedure TFmWClients2.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrWClientsCodigo.Value;
  Close;
end;

procedure TFmWClients2.BtConfirmaClick(Sender: TObject);
var
  Res: Boolean;
  NomeWeb, Usuario, Senha: String;
  Parc_ValMin, Multa, Juros: Double;
  Codigo, Parc_QtdMax, Perfil, PerfilImv, PwdSite, OcultaBloq, Tip, Cod,
  TipoUsr, IDUsr: Integer;
begin
  NomeWeb     := EdNomeWeb.ValueVariant;
  Parc_ValMin := dmkEdParc_ValMin.ValueVariant;
  Parc_QtdMax := dmkEdParc_QtdMax.ValueVariant;
  Multa       := dmkEdMulta.ValueVariant;
  Juros       := dmkEdJuros.ValueVariant;
  Usuario     := EdUser2.ValueVariant;
  Senha       := EdPass2.ValueVariant;
  Perfil      := EdPerfil.ValueVariant;
  PerfilImv   := EdPerfilImv.ValueVariant;
  PwdSite     := RGPwdSite.ItemIndex;
  OcultaBloq  := Geral.BoolToInt(CkOcultaBloq.Checked);
  //
  if Usuario <> '' then
  begin
    if (Dmod.ValidaUsuarioWeb(Usuario, Tip, Cod) = False) then
    begin
      if (QrWClientsUser_ID.Value <> Cod) and (Tip <> 7) then
      begin
        Geral.MB_Aviso('Este usu�rio n�o est� dispon�vel!');
        EdUser2.SetFocus;
        Exit;
      end;
    end;
    if MyObjects.FIC(Senha = '', EdPass2, 'Defina uma senha!') then Exit;
    if MyObjects.FIC(Perfil = 0, EdPerfil, 'Defina o perfil de senha para o s�ndico!') then Exit;
    if MyObjects.FIC(PerfilImv = 0, EdPerfilImv, 'Defina o perfil de senha para o cond�mino!') then Exit;
  end;
  //
  Res := False;
  //
  if ImgTipo.SQLType = stIns then
  begin
    if Dmod.LocalizaUsuarioWeb(Dmod.QrAux, Dmod.MyDB, Usuario, TipoUsr, IDUsr) then
    begin
      Geral.MB_Aviso('Este usu�rio est� indispon�vel!');
      EdUser2.SetFocus;
      Exit;
    end;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'wclients', True,
    [
      'NomeWeb', 'Username', 'Password', 'CodCliEsp',
      'Parc_ValMin', 'Parc_QtdMax', 'NomeSindico',
      'Multa', 'Juros', 'Email'
    ], [],
    [
      EdNomeWeb.Text, Usuario, Senha, QrWClientsCodigo.Value,
      Parc_ValMin, Parc_QtdMax, EdNomeSindico.Text,
      Multa, Juros, EdEMail.ValueVariant
    ], [], True) then
      Res := True;
  end else
  begin
    if Dmod.LocalizaUsuarioWeb(Dmod.QrAux, Dmod.MyDB, Usuario, TipoUsr, IDUsr) then
    begin
      if ((IDUsr <> QrWClientsUser_ID.Value) or ((IDUsr <> QrWClientsUser_ID.Value) and (TipoUsr <> 7))) then
      begin
        Geral.MB_Aviso('Este usu�rio est� indispon�vel!');
        EdUser2.SetFocus;
        Exit;
      end;
    end;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'wclients', False,
    [
      'NomeWeb', 'Username', 'Password', 'CodCliEsp',
      'Parc_ValMin', 'Parc_QtdMax', 'NomeSindico',
      'Multa', 'Juros', 'Email'
    ], ['User_ID'],
    [
      EdNomeWeb.Text, Usuario, Senha, QrWClientsCodigo.Value,
      Parc_ValMin, Parc_QtdMax, EdNomeSindico.Text,
      Multa, Juros, EdEMail.ValueVariant
    ], [QrWClientsUser_ID.Value], True) then
      Res := True;
  end;
  if Res then
  begin
    Codigo := QrWClientsCodigo.Value;
    //
    if Codigo <> 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cond', False,
        ['PwdSite', 'OcultaBloq', 'Perfil', 'PerfilImv'], ['Codigo'],
        [PwdSite, OcultaBloq, Perfil, PerfilImv], [Codigo], True);
    end else
      Geral.MB_Aviso('Falha ao atualizar dados do condom�nio!');
    //
    LocCod(QrWClientsCodigo.Value, QrWClientsCodigo.Value);
    MostraEdicao(0, stLok, 0);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmWClients2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FSeq := 0;
  Panel10.Align      := alClient;
  PainelEdita.Align  := alClient;
  PainelDados.Align  := alClient;
  PainelEdit.Align   := alClient;
  PageControl1.Align := alClient;
  PainelUser.Align   := alClient;
  PageControl1.ActivePageIndex := 0;
  //
  ReopenWPerfil(QrWPerfil, 7); //S�ndico
  ReopenWPerfil(QrWPerfilImv, 1); //Propriet�rio
  //
  CriaOForm;
end;

procedure TFmWClients2.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrWClientsCodigo.Value,LaRegistro.Caption);
end;

procedure TFmWClients2.SbQueryClick(Sender: TObject);
begin
  MostraWUsers();
end;

procedure TFmWClients2.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmWClients2.SbNomeClick(Sender: TObject);
begin
  MostraWUsers();
end;

procedure TFmWClients2.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmWClients2.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  VAR_MARCA := QrWClientsCodigo.Value;
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmWClients2.QrWClientsAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  if QrWClients.RecordCount > 0 then
    BtUsuario.Enabled := True
  else
    BtUsuario.Enabled := False;
end;

procedure TFmWClients2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //
  if FSeq = 1 then
    MostraEdicao(1, stIns, 0);
end;

procedure TFmWClients2.QrWClientsAfterScroll(DataSet: TDataSet);
begin
  ReopenUsers(0);
end;

procedure TFmWClients2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWClients2.QrWClientsCalcFields(DataSet: TDataSet);
begin
  QrWClientsCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrWClientsCNPJ_CPF.Value);
end;

procedure TFmWClients2.BitBtn1Click(Sender: TObject);
var
  Res: Boolean;
  Cond, CondImov, Tip, Cod, TipoUsr, IDUsr: Integer;
  Usuario, Senha, Email: String;
begin
  Res := False;
  //
  Usuario  := EdULogin.ValueVariant;
  Senha    := EdUSenha.ValueVariant;
  Email    := EdEMail.ValueVariant;
  Cond     := QrWClientsCodigo.Value;
  CondImov := QrUsersAPTO.Value;
  //
  if MyObjects.FIC(Cond = 0, nil, 'Condom�nio n�o informado!') then Exit;
  if MyObjects.FIC(CondImov = 0, nil, 'UH n�o definida!') then Exit;
  if MyObjects.FIC(Usuario = '', EdULogin, 'Usu�rio n�o definido!') then Exit;
  if MyObjects.FIC(Senha = '', EdUSenha, 'Senha n�o definida!') then Exit;
  //
  if (Dmod.ValidaUsuarioWeb(Usuario, Tip, Cod) = False) then
  begin
    if (QrUsersUser_ID.Value <> Cod) and (Tip <> 1) then
    begin
      Geral.MB_Aviso('Este usu�rio n�o est� dispon�vel!');
      EdULogin.SetFocus;
      Exit;
    end;
  end;
  //
  if ImgTipo.SQLType = stIns then
  begin
    if Dmod.LocalizaUsuarioWeb(Dmod.QrAux, Dmod.MyDB, Usuario, TipoUsr, IDUsr) then
    begin
      Geral.MB_Aviso('Este usu�rio est� indispon�vel!');
      EdULogin.SetFocus;
      Exit;
    end;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'users', True,
      ['Tipo', 'Username', 'Password', 'CodCliEsp', 'CodigoEsp', 'Email'], [],
      [1, Usuario, Senha, Cond, CondImov, Email], [], True)
    then
      Res := True;
  end else
  begin
    if Dmod.LocalizaUsuarioWeb(Dmod.QrAux, Dmod.MyDB, Usuario, TipoUsr, IDUsr) then
    begin
      if ((IDUsr <> QrUsersUser_ID.Value) or ((IDUsr <> QrUsersUser_ID.Value) and (TipoUsr <> 1))) then
      begin
        Geral.MB_Aviso('Este usu�rio est� indispon�vel!');
        EdULogin.SetFocus;
        Exit;
      end;
    end;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'users', False,
      ['Tipo', 'Username', 'Password', 'CodCliEsp', 'CodigoEsp', 'Email'], ['User_ID'],
      [1, Usuario, Senha, Cond, CondImov, Email], [QrUsersUser_ID.Value], True)
    then
      Res := True;
  end;
  if Res then
  begin
    ReopenUsers(QrUsersAPTO.Value);
    MostraEdicao(0, stLok, 0);
  end;
end;

procedure TFmWClients2.BitBtn3Click(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmWClients2.ReopenUsers(APTO: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrUsers, Dmod.MyDB, [
    'SELECT ent.Codigo, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) USER_NOME, ',
    'IF(imv.Usuario>0, imv.Usuario, imv.Propriet) + 0.000 USUARIO, ',
    'imv.Propriet, imv.Unidade, imv.Conta APTO, usu.* ',
    'FROM condimov imv ',
    'LEFT JOIN entidades ent ON IF(imv.Usuario>0, imv.Usuario, imv.Propriet)=ent.Codigo ',
    'LEFT JOIN users usu ON usu.CodigoEsp=imv.Conta AND (usu.Tipo=1 OR usu.Tipo IS NULL) ',
    'WHERE imv.Codigo=' + Geral.FF0(QrWClientsCodigo.Value),
    '']);
  if Apto <> 0 then
    QrUsers.Locate('APTO', APTO, []);
end;

procedure TFmWClients2.ReopenWPerfil(Query: TmySQLQuery; Tipo: Integer);
var
  Tip: Integer;
begin
  Tip := FmPrincipal.ConverteWPerfilSyndicToWPerfil(Tipo);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDBn, [
    'SELECT Codigo, Nome ',
    'FROM wperfis ',
    'WHERE Tipo =' + Geral.FF0(Tip),
    '']);
end;

procedure TFmWClients2.QrWClientsBeforeClose(DataSet: TDataSet);
begin
  BtUsuario.Enabled := False;
end;

procedure TFmWClients2.BtUsuarioClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMUsuario, BtUsuario);
end;

procedure TFmWClients2.Alterausurioatual1Click(Sender: TObject);
var
  PerfilImv: Integer;
begin
  PerfilImv := QrWClientsPerfilImv.Value;
  //
  if MyObjects.FIC(PerfilImv = 0, nil, 'Perfil de senha cond�mino n�o definido!') then Exit;
  //
  if QrUsersUser_ID.Value = 0 then
    MostraEdicao(3, stIns, 0)
  else
    MostraEdicao(3, stUpd, 0);
end;

procedure TFmWClients2.RecriaLoginsESenhas(Quem: Integer; Quais: TSelType);
  procedure Users;
  var
    Nivel, Tip, Cod: Integer;
    UserName, Password: String;
    ImgTipo: TSQLType;
  begin
    UserName := 'uh' + Geral.FF0(QrUsersAPTO.Value);
    Password := GeraNovaSenha;
    //
    if (Dmod.ValidaUsuarioWeb(UserName, Tip, Cod) = False) then
    begin
      if (QrUsersUser_ID.Value <> Cod) and (Tip <> 1) then
      begin
        Geral.MB_Aviso('Falha ao incluir usu�rio!' + sLineBreak +
          'Motivo: Este usu�rio n�o est� dispon�vel!');
        Exit;
      end;
    end;
    //
    if QrUsersUser_ID.Value = 0 then
    begin
      ImgTipo := stIns;
      Nivel  := 1;
    end else begin
      ImgTipo := stUpd;
      Nivel  := QrUsersTipo.Value;
    end;
    //
    if ImgTipo = stIns then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo, 'users', True,
      [
        'Tipo', 'Username', 'Password', 'CodCliEsp', 'CodigoEsp'
      ], [],
      [
        Nivel, UserName, Password, QrWClientsCodigo.Value, QrUsersAPTO.Value
      ], [], True);
    end else
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo, 'users', False,
      [
        'Tipo', 'Username', 'Password', 'CodCliEsp', 'CodigoEsp'
      ], ['User_ID'],
      [
        Nivel, UserName, Password, QrWClientsCodigo.Value, QrUsersAPTO.Value
      ], [QrUsersUser_ID.Value], True);
    end;
  end;
var
  Apto, Itens, i, Propriet, PerfilImv: Integer;
  sQuem, sQuais: String;
begin
  PerfilImv := QrWClientsPerfilImv.Value;
  //
  if MyObjects.FIC(PerfilImv = 0, nil, 'Perfil de senha cond�mino n�o definido!') then Exit;
  //
  case Quem of
    1:
    begin
      sQuem := 'usu�rio';
      Itens := dmkDBGUsers.SelectedRows.Count;
    end;
    else
    begin
      sQuem := '?';
      Itens := 0;
    end;
  end;
  case Quais of
    istAtual: sQuais := 'do loguin e senha';
    istSelecionados:
    begin
      if itens < 2 then
        sQuais := 'do loguin e senha'
      else
        sQuais := 'dos loguins e senhas';
    end;
    istTodos: sQuais := 'de todos loguins e senhas';
  end;
  if Itens > 1 then
    sQuem := ' dos ' + sQuem + 's'
   else
    sQuem := ' do ' + sQuem;

  if Geral.MB_Pergunta('Confirma a recria��o ' + sQuais + sQuem +
    ' do condom�nio atual?') = ID_YES then
  begin
    if Quem = 1 then
    begin
      Apto := QrUsersAPTO.Value;
      if Quais = istTodos then
      begin
        Screen.Cursor := crHourGlass;
        try
          QrUsers.DisableControls;
          dmkDBGUsers.Enabled := False;
          PB1.Position        := 0;
          PB1.Max             := QrUsers.RecordCount;
          //
          QrUsers.First;
          while not QrUsers.Eof do
          begin
            Users;
            //
            PB1.Position := PB1.Position + 1;
            PB1.Update;
            Application.ProcessMessages;
            //
            QrUsers.Next;
          end;
        finally
          QrUsers.EnableControls;
          dmkDBGUsers.Enabled := True;
          PB1.Position        := 0;
          //
          Screen.Cursor := crDefault;
        end;
      end else if Quais = istSelecionados then
      begin
        if Itens < 2 then
        begin
          Users;
        end else
        begin
          Screen.Cursor := crHourGlass;
          try
            QrUsers.DisableControls;
            dmkDBGUsers.Enabled := False;
            PB1.Position        := 0;
            PB1.Max             := dmkDBGUsers.SelectedRows.Count;
            //
            with dmkDBGUsers.DataSource.DataSet do
            begin
              for i:= 0 to dmkDBGUsers.SelectedRows.Count-1 do
              begin
                GotoBookmark(pointer(dmkDBGUsers.SelectedRows.Items[i]));
                //
                PB1.Position := PB1.Position + 1;
                PB1.Update;
                Application.ProcessMessages;
                //
                Users;
              end;
            end;
          finally
            QrUsers.EnableControls;
            dmkDBGUsers.Enabled := True;
            PB1.Position        := 0;
            //
            Screen.Cursor := crDefault;
          end;
        end;
      end else
        Users;
      ReopenUsers(Apto);
    end;
  end;
end;

function TFmWClients2.GeraNovaSenha(): String;
var
  Senha: String;
  Nova: Boolean;
begin
  Nova := False;
  while not Nova do
  begin
    Senha := MLAGeral.GeraPassword(True, False, True, 8);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrExiste, Dmod.MyDB, [
      'SELECT Password ',
      'FROM users ',
      'WHERE Password="' + Senha + '"',
      '']);
    //
    Nova := QrExiste.RecordCount = 0;
  end;
  Result := Senha;
end;

procedure TFmWClients2.EnableBtns();
begin
  BtUsuario.Enabled := PageControl1.ActivePageIndex = 0;
  BtDono.Enabled    := PageControl1.ActivePageIndex = 1;
end;

procedure TFmWClients2.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmWClients2.BtDonoClick(Sender: TObject);
begin
  FmPrincipal.MostraCondGri(0);
end;

procedure TFmWClients2.Atual1Click(Sender: TObject);
begin
  RecriaLoginsESenhas(1, istAtual);
end;

procedure TFmWClients2.Selecionados1Click(Sender: TObject);
begin
  RecriaLoginsESenhas(1, istSelecionados);
end;

procedure TFmWClients2.Todos1Click(Sender: TObject);
begin
  RecriaLoginsESenhas(1, istTodos);
end;

end.

