object FmWUsersPesq: TFmWUsersPesq
  Left = 339
  Top = 185
  Caption = 'WEB-USERS-002 :: Pesquisa de usu'#225'rios WEB'
  ClientHeight = 464
  ClientWidth = 895
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 895
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 807
      Top = 0
      Width = 88
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 48
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
      object ImgWEB: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 759
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 332
        Height = 32
        Caption = 'Pesquisa de usu'#225'rios WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 332
        Height = 32
        Caption = 'Pesquisa de usu'#225'rios WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 332
        Height = 32
        Caption = 'Pesquisa de usu'#225'rios WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 394
    Width = 895
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 749
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 747
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label16: TLabel
        Left = 260
        Top = 7
        Width = 129
        Height = 13
        Caption = 'Total de itens pesquisados:'
      end
      object BtPesquisa: TBitBtn
        Tag = 22
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesquisaClick
      end
      object BtAbrir: TBitBtn
        Tag = 14
        Left = 135
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Abrir'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtAbrirClick
      end
      object EdTotItens: TdmkEdit
        Left = 260
        Top = 23
        Width = 130
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 895
    Height = 346
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 895
      Height = 302
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 895
        Height = 150
        Align = alTop
        TabOrder = 0
        object LaPerfil: TLabel
          Left = 453
          Top = 12
          Width = 26
          Height = 13
          Caption = 'Perfil:'
        end
        object LaEntidade: TLabel
          Left = 453
          Top = 60
          Width = 45
          Height = 13
          Caption = 'Entidade:'
        end
        object Label10: TLabel
          Left = 17
          Top = 60
          Width = 97
          Height = 13
          Caption = 'Nome para exibi'#231#227'o:'
        end
        object Label6: TLabel
          Left = 18
          Top = 103
          Width = 31
          Height = 13
          Caption = 'E-mail:'
        end
        object Label3: TLabel
          Left = 234
          Top = 60
          Width = 39
          Height = 13
          Caption = 'Usu'#225'rio:'
        end
        object EdPerfil: TdmkEditCB
          Left = 453
          Top = 28
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBPerfil
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBPerfil: TdmkDBLookupComboBox
          Left = 535
          Top = 28
          Width = 345
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsWPerfis
          TabOrder = 2
          dmkEditCB = EdPerfil
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdEntidade: TdmkEditCB
          Left = 453
          Top = 76
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEntidade
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBEntidade: TdmkDBLookupComboBox
          Left = 535
          Top = 76
          Width = 345
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsEntidades
          TabOrder = 6
          dmkEditCB = EdEntidade
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdNome: TdmkEdit
          Left = 17
          Top = 77
          Width = 210
          Height = 21
          MaxLength = 32
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdEmail: TdmkEdit
          Left = 17
          Top = 119
          Width = 427
          Height = 21
          MaxLength = 100
          TabOrder = 7
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdLogin: TdmkEdit
          Left = 234
          Top = 77
          Width = 210
          Height = 21
          MaxLength = 32
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object RgTipo: TdmkRadioGroup
          Left = 17
          Top = 12
          Width = 427
          Height = 37
          Caption = 'Tipo de perfil'
          Columns = 5
          Items.Strings = (
            'Administrador'
            'Boss'
            'Cliente'
            'Usu'#225'rio'
            'Todos')
          TabOrder = 0
          UpdType = utYes
          OldValor = 0
        end
        object RGSituacao: TdmkRadioGroup
          Left = 453
          Top = 103
          Width = 427
          Height = 37
          Caption = 'Situa'#231#227'o'
          Columns = 3
          Items.Strings = (
            'Ativos'
            'Inativos'
            'Ambos')
          TabOrder = 8
          UpdType = utYes
          OldValor = 0
        end
      end
      object dmkDBGrid1: TdmkDBGrid
        Left = 0
        Top = 150
        Width = 895
        Height = 152
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'Ativo'
            Width = 55
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PersonalName'
            Title.Caption = 'Nome'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ENTNOME'
            Title.Caption = 'Entidade'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEPEF'
            Title.Caption = 'Perfil'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMETIP'
            Title.Caption = 'Tipo de perfil'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Username'
            Title.Caption = 'Usu'#225'rio'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Email'
            Title.Caption = 'E-mail'
            Width = 200
            Visible = True
          end>
        Color = clWindow
        DataSource = DsWUsers
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = dmkDBGrid1DblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'Ativo'
            Width = 55
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PersonalName'
            Title.Caption = 'Nome'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ENTNOME'
            Title.Caption = 'Entidade'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEPEF'
            Title.Caption = 'Perfil'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMETIP'
            Title.Caption = 'Tipo de perfil'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Username'
            Title.Caption = 'Usu'#225'rio'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Email'
            Title.Caption = 'E-mail'
            Width = 200
            Visible = True
          end>
      end
    end
    object GBAvisos1: TGroupBox
      Left = 0
      Top = 302
      Width = 895
      Height = 44
      Align = alBottom
      Caption = ' Avisos: '
      TabOrder = 1
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 891
        Height = 27
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 304
          Height = 16
          Caption = 'D'#234' um duplo clique na grade para localizar o registro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 304
          Height = 16
          Caption = 'D'#234' um duplo clique na grade para localizar o registro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
  end
  object QrWPerfis: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome, Tipo'
      'FROM wperfis'
      'ORDER BY Nome')
    Left = 464
    Top = 260
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Origin = 'wperfis.Codigo'
      Required = True
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Origin = 'wperfis.Nome'
      Required = True
      Size = 50
    end
    object QrWPerfisTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'wperfis.Tipo'
    end
  end
  object DsWPerfis: TDataSource
    DataSet = QrWPerfis
    Left = 492
    Top = 260
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome'
      'FROM  entidades ent'
      'ORDER BY Nome')
    Left = 520
    Top = 260
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrEntidadesNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 548
    Top = 260
  end
  object QrWUsers: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT wus.*, wpe.Nome NOMEPEF, '
      'AES_DECRYPT(Password, "") SENHA,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) CLINOME,'
      
        'IF(wus.Entidade = 0, "", IF (enb.Tipo=0, enb.RazaoSocial, enb.No' +
        'me)) ENTNOME,'
      'CASE wus.Tipo'
      'WHEN 0 THEN "Administrador"'
      'WHEN 1 THEN "Boss"'
      'WHEN 2 THEN "Cliente"'
      'WHEN 3 THEN "Usu'#225'rio" END NOMETIP'
      'FROM wusers wus'
      'LEFT JOIN wperfis wpe ON wpe.Codigo = wus.Perfil'
      'LEFT JOIN clientes cli ON cli.Codigo = wus.Cliente'
      'LEFT JOIN entidades ent ON ent.Codigo = cli.Cliente'
      'LEFT JOIN entidades enb ON enb.Codigo = wus.Entidade'
      'WHERE wus.Codigo > 0')
    Left = 576
    Top = 260
    object QrWUsersCodigo: TAutoIncField
      FieldName = 'Codigo'
      Origin = 'wusers.Codigo'
    end
    object QrWUsersUsername: TWideStringField
      FieldName = 'Username'
      Origin = 'wusers.Username'
      Required = True
      Size = 32
    end
    object QrWUsersPassword: TWideStringField
      FieldName = 'Password'
      Origin = 'wusers.Password'
      Required = True
      Size = 32
    end
    object QrWUsersPersonalName: TWideStringField
      FieldName = 'PersonalName'
      Origin = 'wusers.PersonalName'
      Required = True
      Size = 32
    end
    object QrWUsersLoginID: TWideStringField
      FieldName = 'LoginID'
      Origin = 'wusers.LoginID'
      Size = 32
    end
    object QrWUsersEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'wusers.Entidade'
      Required = True
    end
    object QrWUsersTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'wusers.Tipo'
      Required = True
    end
    object QrWUsersLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'wusers.Lk'
    end
    object QrWUsersDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'wusers.DataCad'
    end
    object QrWUsersDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'wusers.DataAlt'
    end
    object QrWUsersUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'wusers.UserCad'
    end
    object QrWUsersUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'wusers.UserAlt'
    end
    object QrWUsersAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'wusers.AlterWeb'
      Required = True
    end
    object QrWUsersAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'wusers.Ativo'
      Required = True
      MaxValue = 1
    end
    object QrWUsersSENHA: TWideStringField
      FieldName = 'SENHA'
      Size = 32
    end
    object QrWUsersEmail: TWideStringField
      FieldName = 'Email'
      Origin = 'wusers.Email'
      Required = True
      Size = 100
    end
    object QrWUsersPerfil: TIntegerField
      FieldName = 'Perfil'
      Origin = 'wusers.Perfil'
    end
    object QrWUsersCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'wusers.Cliente'
    end
    object QrWUsersNOMEPEF: TWideStringField
      FieldName = 'NOMEPEF'
      Origin = 'wperfis.Nome'
      Size = 50
    end
    object QrWUsersCLINOME: TWideStringField
      FieldName = 'CLINOME'
      Size = 100
    end
    object QrWUsersENTNOME: TWideStringField
      FieldName = 'ENTNOME'
      Size = 100
    end
    object QrWUsersStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrWUsersNOMETIP: TWideStringField
      FieldName = 'NOMETIP'
      Size = 13
    end
  end
  object DsWUsers: TDataSource
    DataSet = QrWUsers
    Left = 604
    Top = 260
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 740
    Top = 16
  end
  object frxWEB_USERS_002_001: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39664.693294085700000000
    ReportOptions.LastChange = 40205.637279965270000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 100
    Top = 248
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsWUsers
        DataSetName = 'frxDsWUsers'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 69.921296459999990000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 593.386210000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 487.559370000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LISTA DE USU'#193'RIOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 699.213050000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000010000
          Top = 56.692949999999990000
          Width = 132.283476770000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 56.692949999999990000
          Width = 132.283464570000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Usu'#225'rio')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Top = 56.692949999999990000
          Width = 207.874015750000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'E-mail')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134199999999900000
          Top = 56.692949999999990000
          Width = 132.283464570000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Perfil')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Top = 56.692949999999990000
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Ativo')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Top = 56.692949999999990000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ID')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 347.716760000000000000
        Width = 699.213050000000000000
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Width = 427.086890000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 151.181200000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsWUsers."ENTNOME"'
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Width = 699.213013390000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Entidade: [frxDsWUsers."ENTNOME"]')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 287.244280000000000000
        Width = 699.213050000000000000
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 226.771800000000000000
        Width = 699.213050000000000000
        DataSet = frxDsWUsers
        DataSetName = 'frxDsWUsers'
        RowCount = 0
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000010000
          Width = 132.283513390000000000
          Height = 13.228346460000000000
          DataField = 'PersonalName'
          DataSet = frxDsWUsers
          DataSetName = 'frxDsWUsers'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsWUsers."PersonalName"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Width = 132.283464570000000000
          Height = 13.228346460000000000
          DataField = 'Username'
          DataSet = frxDsWUsers
          DataSetName = 'frxDsWUsers'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsWUsers."Username"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Width = 207.874040160000000000
          Height = 13.228346460000000000
          DataField = 'Email'
          DataSet = frxDsWUsers
          DataSetName = 'frxDsWUsers'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsWUsers."Email"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134199999999900000
          Width = 132.283464570000000000
          Height = 13.228346460000000000
          DataField = 'NOMEPEF'
          DataSet = frxDsWUsers
          DataSetName = 'frxDsWUsers'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsWUsers."NOMEPEF"]')
          ParentFont = False
        end
        object CheckBox1: TfrxCheckBoxView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'Ativo'
          DataSet = frxDsWUsers
          DataSetName = 'frxDsWUsers'
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          DataField = 'Codigo'
          DataSet = frxDsWUsers
          DataSetName = 'frxDsWUsers'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsWUsers."Codigo"]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 188.976500000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsWUsers."NOMETIP"'
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Width = 699.213013390000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Tipo: [frxDsWUsers."NOMETIP"]')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 264.567100000000000000
        Width = 699.213050000000000000
      end
    end
  end
  object frxDsWUsers: TfrxDBDataset
    UserName = 'frxDsWUsers'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Username=Username'
      'Password=Password'
      'PersonalName=PersonalName'
      'LoginID=LoginID'
      'Entidade=Entidade'
      'Tipo=Tipo'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'SENHA=SENHA'
      'Email=Email'
      'Perfil=Perfil'
      'Cliente=Cliente'
      'NOMEPEF=NOMEPEF'
      'CLINOME=CLINOME'
      'ENTNOME=ENTNOME'
      'Status=Status'
      'NOMETIP=NOMETIP')
    DataSet = QrWUsers
    BCDToCurrency = False
    DataSetOptions = []
    Left = 128
    Top = 248
  end
end
