unit UnWUsers;

interface

uses System.Classes, mySQLDbTables, UMySQLModule, dmkGeral, ModuleGeral,
  Forms, Controls, Windows, SysUtils, ComCtrls, Grids, DBGrids, ExtCtrls,
  UnDmkProcFunc, dmkImage, dmkDBGrid, Math, UnDmkEnums;

type
  TUnWUsers = class(TObject)
  private
    function LocalizaUsuario(QueryLoc: TmySQLQuery; Database: TmySQLDatabase;
               Usuario: String): Integer;
  public
    function InsereWUsers(SQLTipo: TSQLType; QueryUpd, QueryLoc: TmySQLQuery;
               Database: TmySQLDatabase; Codigo, Cliente, Entidade, Perfil,
               Tipo, Sexo, Ativo: Integer; Crypt, Usuario, Senha, SenhaConf, Email,
               PersName: String; DtaNatal: TDate): Integer;
    //function  ConfiguraNiveis(CodigoApp: Integer; SemRestricao: Boolean = True): TStringList;
  end;

var
  UWUsers: TUnWUsers;

implementation

uses Module, DmkDAC_PF, UnMyObjects, MyDBCheck, UnDmkWeb, UnInternalConsts,
  UnGrl_Consts;

{ TUnWUsers }

(* 2017-09-01 => Movido para UnGrlUsuarios MultiOS
function TUnWUsers.ConfiguraNiveis(CodigoApp: Integer; SemRestricao: Boolean = True): TStringList;
var
  Niveis: TStringList;
begin
  Niveis := TStringList.Create;
  try
    //Os n�veis s�o espec�ficos de cada aplicativo
    //
    if CodigoApp in [17, 24] then //DControl, Bugstrol
    begin
      if SemRestricao then
        Niveis.Add('Liberado');
      //
      Niveis.Add('Admininstrador');
      Niveis.Add('Parceiro');
      Niveis.Add('Cliente');
      Niveis.Add('Usu�rio');
    end
    else if CodigoApp = 4 then //Syndi2
    begin
      if SemRestricao then
        Niveis.Add('Nenhum');
      //
      Niveis.Add('Admininstrador');
      Niveis.Add('Grupo');
      Niveis.Add('S�ndico');
      Niveis.Add('Cond�mino');
    end
    else
    begin
      if SemRestricao then
        Niveis.Add('N�vel 0');
      //
      Niveis.Add('N�vel 1');
      Niveis.Add('N�vel 2');
      Niveis.Add('N�vel 3');
      Niveis.Add('N�vel 4');
    end;
  finally
    Result := Niveis;
  end;
end;
*)

function TUnWUsers.InsereWUsers(SQLTipo: TSQLType; QueryUpd,
  QueryLoc: TmySQLQuery; Database: TmySQLDatabase; Codigo, Cliente, Entidade,
  Perfil, Tipo, Sexo, Ativo: Integer; Crypt, Usuario, Senha, SenhaConf, Email,
  PersName: String; DtaNatal: TDate): Integer;
var
  Crypto, DtaNatal_Txt: String;
  Cod, UsrDuplic: Integer;
begin
  Result := 0;
  //
  //Adicionar a vari�vel CO_RandStrWeb01 na internet caso exista
  Crypto    := Crypt + CO_RandStrWeb01;
  UsrDuplic := LocalizaUsuario(QueryLoc, Database, Usuario);
  //
  if MyObjects.FIC(Perfil = 0, nil, 'Perfil n�o definido!') then Exit;
  if MyObjects.FIC(Length(Usuario) = 0, nil, 'Usu�rio n�o definido!') then Exit;
  if MyObjects.FIC((UsrDuplic <> 0) and (UsrDuplic <> Codigo), nil, 'Usu�rio indispon�vel!') then Exit;
  if MyObjects.FIC(Length(Senha) = 0, nil, 'Senha n�o definida!') then Exit;
  //
  if Senha <> '' then
  begin
    if MyObjects.FIC(Senha <> SenhaConf, nil,
      'O campo "Senha" e o campo "Confirmar senha" devem conter o mesmo texto!') then Exit;
  end;
  //
  if DtaNatal = 0 then
    DtaNatal_Txt := '0000-00-00'
  else
    DtaNatal_Txt := Geral.FDT(DtaNatal, 01);
  //
  try
    if SQLTipo = stIns then
      Cod := UMyMod.BuscaNovoCodigo_Int(QueryUpd, 'wusers', 'Codigo', [], [], stIns, 0, siPositivo, nil)
    else
      Cod := Codigo;
    //
    QueryUpd.SQL.Clear;
    if SQLTipo = stIns then
      QueryUpd.SQL.Add('INSERT INTO wusers SET ')
    else
      QueryUpd.SQL.Add('UPDATE wusers SET ');
    QueryUpd.SQL.Add('Entidade=:P0, PersonalName=:P1, Username=:P2, ');
    QueryUpd.SQL.Add('Password=AES_ENCRYPT(:P3, :P4), Email=:P5, ');
    QueryUpd.SQL.Add('Cliente=:P6, Perfil=:P7, Tipo=:P8, Sexo=:P9, ');
    QueryUpd.SQL.Add('DtaNatal=:P10, Ativo=:P11, ');
    //
    if SQLTipo = stIns then
      QueryUpd.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
    else
      QueryUpd.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
    //
    QueryUpd.Params[00].AsInteger := Entidade;
    QueryUpd.Params[01].AsString  := PersName;
    QueryUpd.Params[02].AsString  := Usuario;
    QueryUpd.Params[03].AsString  := Senha;
    QueryUpd.Params[04].AsString  := Crypto;
    QueryUpd.Params[05].AsString  := Email;
    QueryUpd.Params[06].AsInteger := Cliente;
    QueryUpd.Params[07].AsInteger := Perfil;
    QueryUpd.Params[08].AsInteger := Tipo;
    QueryUpd.Params[09].AsInteger := Sexo;
    QueryUpd.Params[10].AsString  := DtaNatal_Txt;
    QueryUpd.Params[11].AsInteger := Ativo;
    //
    QueryUpd.Params[12].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    QueryUpd.Params[13].AsInteger := VAR_USUARIO;
    QueryUpd.Params[14].AsInteger := Cod;
    QueryUpd.ExecSQL;
    //
    Result := Cod
  except
    Result := 0;
  end;
end;

function TUnWUsers.LocalizaUsuario(QueryLoc: TmySQLQuery; Database: TmySQLDatabase; Usuario: String): Integer;
begin
  Result := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, Database, [
    'SELECT Codigo ',
    'FROM wusers ',
    'WHERE Username="' + Usuario + '"',
    '']);
  if QueryLoc.RecordCount > 0 then
    Result := QueryLoc.FieldByName('Codigo').AsInteger;
end;

end.
