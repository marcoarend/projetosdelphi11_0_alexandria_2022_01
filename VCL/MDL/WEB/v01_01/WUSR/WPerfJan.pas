unit WPerfJan;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2,
  UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkCheckBox, frxClass, frxDBSet, dmkRadioGroup,
  dmkCheckGroup, UnDmkProcFunc, frxChBox, UnDmkEnums, UnProjGroup_Consts;

type
  TFmWPerfJan = class(TForm)
    PainelDados: TPanel;
    DsWPerfJan: TDataSource;
    QrWPerfJan: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PnPesq: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TStaticText;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    Label2: TLabel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label9: TLabel;
    QrWPerfJanCodigo: TIntegerField;
    QrWPerfJanNome: TWideStringField;
    QrWPerfJanPage: TWideStringField;
    QrWPerfJanLk: TIntegerField;
    QrWPerfJanDataCad: TDateField;
    QrWPerfJanDataAlt: TDateField;
    QrWPerfJanUserCad: TIntegerField;
    QrWPerfJanUserAlt: TIntegerField;
    QrWPerfJanAlterWeb: TSmallintField;
    QrWPerfJanAtivo: TSmallintField;
    QrWPerfJanDescri: TWideStringField;
    QrWPerfJanGeral: TSmallintField;
    QrWPerfJanMenu: TSmallintField;
    EdPage: TdmkEdit;
    Label3: TLabel;
    Label4: TLabel;
    EdDescri: TdmkEdit;
    CkContinuar: TCheckBox;
    Label5: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    Label6: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    DBCheckBox3: TDBCheckBox;
    CkAtivo: TdmkCheckBox;
    frxWEB_PERFI_002_001: TfrxReport;
    frxDsWPerfJanCamAll: TfrxDBDataset;
    QrWPerfJanCamAll: TmySQLQuery;
    RGGeral: TdmkRadioGroup;
    RGMenu: TdmkRadioGroup;
    DBRadioGroup1: TDBRadioGroup;
    DBRadioGroup2: TDBRadioGroup;
    EdCaminho: TdmkEdit;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    QrWPerfJanCaminho: TWideStringField;
    Label17: TLabel;
    dmkDBEdit3: TdmkDBEdit;
    QrWPerfJanCamAllCaminho: TWideStringField;
    QrWPerfJanAll: TmySQLQuery;
    StringField1: TWideStringField;
    frxDsWPerfJanAll: TfrxDBDataset;
    QrWPerfJanAllCodigo: TIntegerField;
    QrWPerfJanAllNome: TWideStringField;
    QrWPerfJanAllPage: TWideStringField;
    QrWPerfJanAllLk: TIntegerField;
    QrWPerfJanAllDataCad: TDateField;
    QrWPerfJanAllDataAlt: TDateField;
    QrWPerfJanAllUserCad: TIntegerField;
    QrWPerfJanAllUserAlt: TIntegerField;
    QrWPerfJanAllAlterWeb: TSmallintField;
    QrWPerfJanAllAtivo: TSmallintField;
    QrWPerfJanAllDescri: TWideStringField;
    QrWPerfJanAllGeral: TSmallintField;
    QrWPerfJanAllMenu: TSmallintField;
    QrWPerfJanAllNivel: TIntegerField;
    Label16: TLabel;
    CGNiveis: TdmkCheckGroup;
    DBCGNivel: TdmkDBCheckGroup;
    QrWPerfJanNivel: TIntegerField;
    frxCheckBoxObject1: TfrxCheckBoxObject;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrWPerfJanAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrWPerfJanBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxWEB_PERFI_002_001GetValue(const VarName: string; var Value: Variant);
    procedure QrWPerfJanCamAllAfterScroll(DataSet: TDataSet);
    procedure QrWPerfJanCamAllBeforeClose(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //Procedures do form
    function PesquisaPage(): Integer;
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure ReopenWPerfJanAll();
  public
    { Public declarations }
  end;

var
  FmWPerfJan: TFmWPerfJan;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, Curinga, WUsers, UnDmkWeb, MyListas,
  UnGrlUsuarios;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmWPerfJan.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmWPerfJan.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrWPerfJanCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmWPerfJan.DefParams;
begin
  VAR_GOTOTABELA := 'wperfjan';
  VAR_GOTOMYSQLTABLE := QrWPerfJan;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDBn;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM wperfjan');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P1');
  //
end;

procedure TFmWPerfJan.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant  := FormatFloat(FFormatFloat, Codigo);
        EdNome.ValueVariant    := '';
        EdPage.ValueVariant    := '';
        EdDescri.ValueVariant  := '';
        EdCaminho.ValueVariant := '';
        RGGeral.ItemIndex      := 0;
        RGMenu.ItemIndex       := 0;
        CGNiveis.Value         := 1;
        CkAtivo.Checked        := True;
        CkContinuar.Checked    := False;
        CkContinuar.Visible    := True;
      end else begin
        EdCodigo.ValueVariant  := QrWPerfJanCodigo.Value;
        EdNome.ValueVariant    := QrWPerfJanNome.Value;
        EdPage.ValueVariant    := QrWPerfJanPage.Value;
        EdDescri.ValueVariant  := QrWPerfJanDescri.Value;
        EdCaminho.ValueVariant := QrWPerfJanCaminho.Value;
        RGGeral.ItemIndex      := QrWPerfJanGeral.Value;
        RGMenu.ItemIndex       := QrWPerfJanMenu.Value;
        CGNiveis.Value         := QrWPerfJanNivel.Value;
        CkAtivo.Checked        := Geral.IntToBool(QrWPerfJanAtivo.Value);
        CkContinuar.Checked    := False;
        CkContinuar.Visible    := False;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  LaTipo.SQLType := SQLType;
  GOTOy.BotoesSb(LaTipo.SQLType);
end;

function TFmWPerfJan.PesquisaPage: Integer;
var
  Page: String;
begin
  Page   := '';
  Result := 0;
  //
  if InputQuery('Procura e Lista', 'Digite a p�gina:', Page) then
  begin
    DmodG.QrSB.Close;
    DModG.QrSB.Database := Dmod.MyDBn;
    DModG.QrSB.SQL.Clear;
    DModG.QrSB.SQL.Add('SELECT Codigo, Codigo CodUsu, Page Nome');
    DModG.QrSB.SQL.Add('FROM wperfjan');
    DModG.QrSB.SQL.Add('WHERE Page LIKE "%' + Page + '%"');
    DModG.QrSB.Open;
    //
    MyObjects.FormShow(TFmCuringa, FmCuringa);
    //
    Result := VAR_CODIGO;    
  end;
end;

procedure TFmWPerfJan.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmWPerfJan.QueryPrincipalAfterOpen;
begin
end;

procedure TFmWPerfJan.ReopenWPerfJanAll;
var
  Cam: String;
begin
  Cam := QrWPerfJanCamAllCaminho.Value;
  //
  QrWPerfJanAll.Close;
  QrWPerfJanAll.SQL.Clear;
  QrWPerfJanAll.SQL.Add('SELECT *');
  QrWPerfJanAll.SQL.Add('FROM wperfjan');
  if Length(Cam) = 0 then
    QrWPerfJanAll.SQL.Add('WHERE Caminho IS NULL')
  else
    QrWPerfJanAll.SQL.Add('WHERE Caminho=:P0');
  QrWPerfJanAll.SQL.Add('ORDER BY Nome');
  if Length(Cam) > 0 then
    QrWPerfJanAll.Params[0].AsString := Cam;
  QrWPerfJanAll.Open;
end;

procedure TFmWPerfJan.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmWPerfJan.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmWPerfJan.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmWPerfJan.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmWPerfJan.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmWPerfJan.BtAlteraClick(Sender: TObject);
begin
  MostraEdicao(1, stUpd, 0);
end;

procedure TFmWPerfJan.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrWPerfJanCodigo.Value;
  Close;
end;

procedure TFmWPerfJan.BtConfirmaClick(Sender: TObject);
var
  Codigo, RGeral, Menu, Ativo, Nivel: Integer;
  Nome, Page, Descri, Caminho: String;
begin
  Nome    := EdNome.ValueVariant;
  Page    := EdPage.ValueVariant;
  Descri  := EdDescri.ValueVariant;
  Caminho := EdCaminho.ValueVariant;
  RGeral  := RGGeral.ItemIndex;
  Menu    := RGMenu.ItemIndex;
  Nivel   := CGNiveis.Value;
  Ativo   := Geral.BoolToInt(CkAtivo.Checked);
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina um nome!') then Exit;
  if MyObjects.FIC(Length(Page) = 0, EdPage, 'Defina a p�gina!') then Exit;
  if MyObjects.FIC(Length(Descri) = 0, EdDescri, 'Defina uma descri��o!') then Exit;
  //
  if LaTipo.SQLType = stIns then
    Codigo := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpdN, 'wperfjan',
    'Codigo', [], [], stIns, 0, siPositivo, nil)
  else
    Codigo := QrWPerfJanCodigo.Value;
  //
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpdN, LaTipo.Caption, 'wperfjan', False,
  [
    'Nome', 'Page', 'Descri', 'Caminho',
    'Geral', 'Menu', 'Nivel', 'Ativo'
  ], ['Codigo'], [
    Nome, Page, Descri, Caminho,
    RGeral, Menu, Nivel, Ativo
  ], [Codigo]) then
  begin
    if CkContinuar.Checked then
    begin
      Geral.MB_Info('Dados cadastrados com sucesso!');
      LocCod(Codigo, Codigo);
      LaTipo.SQLType := stIns;
      MostraEdicao(1, stIns, 0);
    end else
    begin
      LocCod(Codigo, Codigo);
      MostraEdicao(0, stLok, 0);
    end;
  end;
end;

procedure TFmWPerfJan.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmWPerfJan.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmWPerfJan.FormCreate(Sender: TObject);
var
  Compo: TComponent;
  Query: TmySQLQuery;
  I: Integer;
begin
  for I := 0 to ComponentCount - 1 do
  begin
    Compo := Components[I];
    if Compo is TmySQLQuery then
    begin
        Query := TmySQLQuery(Compo);
        Query.Database := Dmod.MyDBn;
    end;
  end;
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  //
  DBCGNivel.Items := GrlUsuarios.ConfiguraNiveis(CO_DMKID_APP);
  CGNiveis.Items  := GrlUsuarios.ConfiguraNiveis(CO_DMKID_APP);
  //
  CriaOForm;
end;

procedure TFmWPerfJan.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrWPerfJanCodigo.Value, LaRegistro.Caption);
end;

procedure TFmWPerfJan.SbQueryClick(Sender: TObject);
begin
  LocCod(QrWPerfJanCodigo.Value, PesquisaPage());
end;

procedure TFmWPerfJan.SbImprimeClick(Sender: TObject);
begin
  QrWPerfJanCamAll.Open;
  //
  MyObjects.frxMostra(frxWEB_PERFI_002_001, 'Lista de Janelas WEB');
  //
  QrWPerfJanCamAll.Close;
end;

procedure TFmWPerfJan.SbNomeClick(Sender: TObject);
begin
  LocCod(QrWPerfJanCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'wperfjan', Dmod.MyDBn, CO_VAZIO));
end;

procedure TFmWPerfJan.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.SQLType);
end;

procedure TFmWPerfJan.QrWPerfJanAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmWPerfJan.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmWPerfJan.FormResize(Sender: TObject);
begin
  // M L A G e r a l .LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmWPerfJan.frxWEB_PERFI_002_001GetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VAR_TOTREG') = 0 then
    Value := QrWPerfJanAll.RecordCount;
end;

procedure TFmWPerfJan.QrWPerfJanBeforeOpen(DataSet: TDataSet);
begin
  QrWPerfJanCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmWPerfJan.QrWPerfJanCamAllAfterScroll(DataSet: TDataSet);
begin
  ReopenWPerfJanAll;
end;

procedure TFmWPerfJan.QrWPerfJanCamAllBeforeClose(DataSet: TDataSet);
begin
  QrWPerfJanAll.Close;
end;

end.

