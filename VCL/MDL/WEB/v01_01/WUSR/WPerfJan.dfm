object FmWPerfJan: TFmWPerfJan
  Left = 368
  Top = 194
  Caption = 'WEB-PERFI-002 :: Cadastro de Janelas WEB'
  ClientHeight = 422
  ClientWidth = 702
  Color = clBtnFace
  Constraints.MinHeight = 256
  Constraints.MinWidth = 630
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 47
    Width = 702
    Height = 375
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 315
      Width = 700
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      ExplicitTop = 409
      ExplicitWidth = 875
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 10
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 741
        Top = 1
        Width = 133
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 110
          Height = 50
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 700
      Height = 378
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 0
      ExplicitWidth = 875
      object Label7: TLabel
        Left = 5
        Top = 5
        Width = 14
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label9: TLabel
        Left = 79
        Top = 5
        Width = 31
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Nome:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 544
        Top = 5
        Width = 36
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'P'#225'gina:'
        FocusControl = DBEdNome
      end
      object Label4: TLabel
        Left = 5
        Top = 59
        Width = 51
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label8: TLabel
        Left = 5
        Top = 118
        Width = 44
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Caminho:'
        FocusControl = DBEdNome
      end
      object Label10: TLabel
        Left = 287
        Top = 52
        Width = 250
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Caption = 'Nome que ser'#225' mostrado no menu do site'
        FocusControl = DBEdNome
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label11: TLabel
        Left = 607
        Top = 52
        Width = 122
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Caption = 'Nome interno no site'
        FocusControl = DBEdNome
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TLabel
        Left = 614
        Top = 107
        Width = 115
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Caption = 'Apenas informativo'
        FocusControl = DBEdNome
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label13: TLabel
        Left = 413
        Top = 166
        Width = 316
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Caption = 'Caminho no servidor. Exemplo: DiretorioA/DiretorioB/'
        FocusControl = DBEdNome
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label14: TLabel
        Left = 158
        Top = 194
        Width = 314
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Permite visualizar o arquivo sem estar logado no site'
        FocusControl = DBEdNome
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label15: TLabel
        Left = 158
        Top = 247
        Width = 374
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Permite visualizar o arquivo no menu do site ap'#243's estar logado'
        FocusControl = DBEdNome
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label16: TLabel
        Left = 356
        Top = 334
        Width = 386
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Permite visualizar o arquivo de acordo com o n'#237'vel de permiss'#227'o'
        FocusControl = DBEdNome
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object EdCodigo: TdmkEdit
        Left = 5
        Top = 25
        Width = 69
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 79
        Top = 25
        Width = 458
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        MaxLength = 30
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = True
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdPage: TdmkEdit
        Left = 544
        Top = 25
        Width = 185
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        MaxLength = 15
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = True
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdDescri: TdmkEdit
        Left = 5
        Top = 80
        Width = 724
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        MaxLength = 255
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = True
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CkContinuar: TCheckBox
        Left = 76
        Top = 341
        Width = 142
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Continuar inserindo.'
        TabOrder = 9
      end
      object CkAtivo: TdmkCheckBox
        Left = 12
        Top = 341
        Width = 62
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ativo'
        TabOrder = 8
        QryCampo = 'Ativo'
        UpdCampo = 'Ativo'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object RGGeral: TdmkRadioGroup
        Left = 5
        Top = 172
        Width = 148
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#227'o restrito?'
        Columns = 2
        Items.Strings = (
          'N'#227'o'
          'Sim')
        TabOrder = 5
        QryCampo = 'Geral'
        UpdCampo = 'Geral'
        UpdType = utYes
        OldValor = 0
      end
      object RGMenu: TdmkRadioGroup
        Left = 5
        Top = 225
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Aparecer no menu?'
        Columns = 2
        Items.Strings = (
          'N'#227'o'
          'Sim')
        TabOrder = 6
        QryCampo = 'Menu'
        UpdCampo = 'Menu'
        UpdType = utYes
        OldValor = 0
      end
      object EdCaminho: TdmkEdit
        Left = 5
        Top = 139
        Width = 724
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        MaxLength = 255
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = True
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CGNiveis: TdmkCheckGroup
        Left = 5
        Top = 277
        Width = 724
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#237'veis de permiss'#227'o'
        Columns = 5
        Items.Strings = (
          'N'#237'vel 0'
          'N'#237'vel 1'
          'N'#237'vel 2'
          'N'#237'vel 3'
          'N'#237'vel 4')
        TabOrder = 7
        UpdType = utYes
        Value = 0
        OldValor = 0
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 47
    Width = 702
    Height = 375
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 700
      Height = 240
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Enabled = False
      TabOrder = 1
      ExplicitWidth = 875
      object Label1: TLabel
        Left = 5
        Top = 5
        Width = 14
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 79
        Top = 5
        Width = 31
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Nome:'
        FocusControl = DBEdNome
      end
      object Label5: TLabel
        Left = 453
        Top = 5
        Width = 36
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'P'#225'gina:'
        FocusControl = dmkDBEdit1
      end
      object Label6: TLabel
        Left = 5
        Top = 59
        Width = 51
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        FocusControl = dmkDBEdit2
      end
      object Label17: TLabel
        Left = 5
        Top = 118
        Width = 44
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Caminho:'
        FocusControl = dmkDBEdit3
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 5
        Top = 25
        Width = 69
        Height = 21
        Hint = 'N'#186' do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsWPerfJan
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 79
        Top = 25
        Width = 369
        Height = 21
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsWPerfJan
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 453
        Top = 25
        Width = 185
        Height = 21
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'Page'
        DataSource = DsWPerfJan
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 5
        Top = 81
        Width = 633
        Height = 21
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'Descri'
        DataSource = DsWPerfJan
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBCheckBox3: TDBCheckBox
        Left = 287
        Top = 190
        Width = 63
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ativo'
        DataField = 'Ativo'
        DataSource = DsWPerfJan
        TabOrder = 7
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 5
        Top = 174
        Width = 135
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#227'o restrito'
        Columns = 2
        DataField = 'Geral'
        DataSource = DsWPerfJan
        Items.Strings = (
          'N'#227'o'
          'Sim')
        TabOrder = 5
        Values.Strings = (
          '0'
          '1')
      end
      object DBRadioGroup2: TDBRadioGroup
        Left = 144
        Top = 174
        Width = 135
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Aparecer no menu'
        Columns = 2
        DataField = 'Menu'
        DataSource = DsWPerfJan
        Items.Strings = (
          'N'#227'o'
          'Sim')
        TabOrder = 6
        Values.Strings = (
          '0'
          '1')
      end
      object dmkDBEdit3: TdmkDBEdit
        Left = 5
        Top = 139
        Width = 633
        Height = 21
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'Caminho'
        DataSource = DsWPerfJan
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBCGNivel: TdmkDBCheckGroup
        Left = 645
        Top = 5
        Width = 210
        Height = 160
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#237'veis de permiss'#227'o'
        DataField = 'Nivel'
        DataSource = DsWPerfJan
        Items.Strings = (
          'N'#237'vel 0'
          'N'#237'vel 1'
          'N'#237'vel 2'
          'N'#237'vel 3'
          'N'#237'vel 4')
        ParentBackground = False
        TabOrder = 8
      end
    end
    object PainelControle: TPanel
      Left = 1
      Top = 315
      Width = 700
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 409
      ExplicitWidth = 875
      object LaRegistro: TStaticText
        Left = 213
        Top = 1
        Width = 30
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelKind = bkTile
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 212
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 297
        Top = 1
        Width = 577
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtExclui: TBitBtn
          Tag = 12
          Left = 231
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 118
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 5
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 443
          Top = 0
          Width = 134
          Height = 57
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 111
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 702
    Height = 47
    Align = alTop
    Alignment = taLeftJustify
    Caption = '                              Cadastro de Janelas WEB'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -30
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 775
      Top = 1
      Width = 101
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
    end
    object Image1: TImage
      Left = 278
      Top = 1
      Width = 497
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
    object PnPesq: TPanel
      Left = 1
      Top = 1
      Width = 277
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 4
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsWPerfJan: TDataSource
    DataSet = QrWPerfJan
    Left = 520
    Top = 236
  end
  object QrWPerfJan: TMySQLQuery
    Database = Dmod.MyDB
    AllowSequenced = False
    BeforeOpen = QrWPerfJanBeforeOpen
    AfterOpen = QrWPerfJanAfterOpen
    SQL.Strings = (
      'SELECT *'
      'FROM wperfjan')
    Left = 492
    Top = 236
    object QrWPerfJanCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'wperfjan.Codigo'
    end
    object QrWPerfJanNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'wperfjan.Nome'
      Size = 30
    end
    object QrWPerfJanPage: TWideStringField
      DisplayWidth = 15
      FieldName = 'Page'
      Origin = 'wperfjan.Page'
      Size = 15
    end
    object QrWPerfJanLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'wperfjan.Lk'
    end
    object QrWPerfJanDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'wperfjan.DataCad'
    end
    object QrWPerfJanDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'wperfjan.DataAlt'
    end
    object QrWPerfJanUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'wperfjan.UserCad'
    end
    object QrWPerfJanUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'wperfjan.UserAlt'
    end
    object QrWPerfJanAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'wperfjan.AlterWeb'
    end
    object QrWPerfJanAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'wperfjan.Ativo'
    end
    object QrWPerfJanDescri: TWideStringField
      FieldName = 'Descri'
      Origin = 'wperfjan.Descri'
      Size = 255
    end
    object QrWPerfJanGeral: TSmallintField
      FieldName = 'Geral'
      Origin = 'wperfjan.Geral'
    end
    object QrWPerfJanMenu: TSmallintField
      FieldName = 'Menu'
      Origin = 'wperfjan.Menu'
    end
    object QrWPerfJanCaminho: TWideStringField
      FieldName = 'Caminho'
      Origin = 'wperfjan.Caminho'
      Size = 50
    end
    object QrWPerfJanNivel: TIntegerField
      FieldName = 'Nivel'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 548
    Top = 236
  end
  object frxWEB_PERFI_002_001: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39664.693294085700000000
    ReportOptions.LastChange = 40205.637279965270000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxWEB_PERFI_002_001GetValue
    Left = 508
    Top = 8
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsWPerfJanAll
        DataSetName = 'frxDsWPerfJanAll'
      end
      item
        DataSet = frxDsWPerfJanCamAll
        DataSetName = 'frxDsWPerfJanCamAll'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 56.692950000000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 593.386210000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 487.559370000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'JANELAS WEB')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 699.213050000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.566936460000000000
        Top = 136.063080000000000000
        Width = 699.213050000000000000
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 13.338590000000000000
          Width = 94.488250000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Top = 13.338590000000000000
          Width = 94.488250000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'P'#225'gina')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 13.338590000000000000
          Width = 385.512060000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 13.338590000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Libera.')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 616.063390000000000000
          Top = 13.338590000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Menu')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 657.638220000000000000
          Top = 13.338590000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ativo')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Width = 699.213013390000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Caminho')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 260.787570000000000000
        Width = 699.213050000000000000
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 185.196970000000000000
        Width = 699.213050000000000000
        DataSet = frxDsWPerfJanCamAll
        DataSetName = 'frxDsWPerfJanCamAll'
        RowCount = 0
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Width = 699.213013390000000000
          Height = 13.228346460000000000
          DataField = 'Caminho'
          DataSet = frxDsWPerfJanCamAll
          DataSetName = 'frxDsWPerfJanCamAll'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsWPerfJanCamAll."Caminho"]')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 222.992270000000000000
        Width = 699.213050000000000000
        DataSet = frxDsWPerfJanAll
        DataSetName = 'frxDsWPerfJanAll'
        RowCount = 0
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Width = 94.488250000000000000
          Height = 13.228346460000000000
          DataField = 'Nome'
          DataSet = frxDsWPerfJanAll
          DataSetName = 'frxDsWPerfJanAll'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsWPerfJanAll."Nome"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Width = 94.488250000000000000
          Height = 13.228346460000000000
          DataField = 'Page'
          DataSet = frxDsWPerfJanAll
          DataSetName = 'frxDsWPerfJanAll'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsWPerfJanAll."Page"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Width = 385.512060000000000000
          Height = 13.228346460000000000
          DataField = 'Descri'
          DataSet = frxDsWPerfJanAll
          DataSetName = 'frxDsWPerfJanAll'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsWPerfJanAll."Descri"]')
          ParentFont = False
        end
        object CheckBox1: TfrxCheckBoxView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'Geral'
          DataSet = frxDsWPerfJanAll
          DataSetName = 'frxDsWPerfJanAll'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object CheckBox2: TfrxCheckBoxView
          AllowVectorExport = True
          Left = 616.063390000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'Menu'
          DataSet = frxDsWPerfJanAll
          DataSetName = 'frxDsWPerfJanAll'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
        object CheckBox4: TfrxCheckBoxView
          AllowVectorExport = True
          Left = 657.638220000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          CheckColor = clBlack
          CheckStyle = csCheck
          DataField = 'Ativo'
          DataSet = frxDsWPerfJanAll
          DataSetName = 'frxDsWPerfJanAll'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 321.260050000000000000
        Width = 699.213050000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Width = 427.086890000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsWPerfJanCamAll: TfrxDBDataset
    UserName = 'frxDsWPerfJanCamAll'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Caminho=Caminho')
    DataSet = QrWPerfJanCamAll
    BCDToCurrency = False
    DataSetOptions = []
    Left = 536
    Top = 8
  end
  object QrWPerfJanCamAll: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrWPerfJanCamAllBeforeClose
    AfterScroll = QrWPerfJanCamAllAfterScroll
    SQL.Strings = (
      'SELECT Caminho'
      'FROM wperfjan'
      'GROUP BY Caminho'
      'ORDER BY Caminho')
    Left = 564
    Top = 8
    object QrWPerfJanCamAllCaminho: TWideStringField
      FieldName = 'Caminho'
      Origin = 'wperfjan.Caminho'
      Size = 50
    end
  end
  object QrWPerfJanAll: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM wperfjan'
      'WHERE Caminho=:P0'
      'ORDER BY Nome')
    Left = 620
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object StringField1: TWideStringField
      FieldName = 'Caminho'
      Origin = 'wperfjan.Caminho'
      Size = 50
    end
    object QrWPerfJanAllCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'wperfjan.Codigo'
    end
    object QrWPerfJanAllNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'wperfjan.Nome'
      Size = 30
    end
    object QrWPerfJanAllPage: TWideStringField
      FieldName = 'Page'
      Origin = 'wperfjan.Page'
      Size = 10
    end
    object QrWPerfJanAllLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'wperfjan.Lk'
    end
    object QrWPerfJanAllDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'wperfjan.DataCad'
    end
    object QrWPerfJanAllDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'wperfjan.DataAlt'
    end
    object QrWPerfJanAllUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'wperfjan.UserCad'
    end
    object QrWPerfJanAllUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'wperfjan.UserAlt'
    end
    object QrWPerfJanAllAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'wperfjan.AlterWeb'
    end
    object QrWPerfJanAllAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'wperfjan.Ativo'
    end
    object QrWPerfJanAllDescri: TWideStringField
      FieldName = 'Descri'
      Origin = 'wperfjan.Descri'
      Size = 255
    end
    object QrWPerfJanAllGeral: TSmallintField
      FieldName = 'Geral'
      Origin = 'wperfjan.Geral'
    end
    object QrWPerfJanAllMenu: TSmallintField
      FieldName = 'Menu'
      Origin = 'wperfjan.Menu'
    end
    object QrWPerfJanAllNivel: TIntegerField
      FieldName = 'Nivel'
    end
  end
  object frxDsWPerfJanAll: TfrxDBDataset
    UserName = 'frxDsWPerfJanAll'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Caminho=Caminho'
      'Codigo=Codigo'
      'Nome=Nome'
      'Page=Page'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'Descri=Descri'
      'Geral=Geral'
      'Menu=Menu')
    DataSet = QrWPerfJanAll
    BCDToCurrency = False
    DataSetOptions = []
    Left = 592
    Top = 8
  end
  object frxCheckBoxObject1: TfrxCheckBoxObject
    Left = 576
    Top = 236
  end
end
