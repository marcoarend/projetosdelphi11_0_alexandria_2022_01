object FmWUsers: TFmWUsers
  Left = 368
  Top = 194
  Caption = 'WEB-USERS-001 :: Cadastro de usu'#225'rios WEB'
  ClientHeight = 441
  ClientWidth = 908
  Color = clBtnFace
  Constraints.MinHeight = 256
  Constraints.MinWidth = 630
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 94
    Width = 908
    Height = 347
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 908
      Height = 297
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      ExplicitWidth = 1135
      object Label1: TLabel
        Left = 6
        Top = 7
        Width = 36
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
      end
      object Label2: TLabel
        Left = 6
        Top = 114
        Width = 45
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Entidade:'
      end
      object Label11: TLabel
        Left = 6
        Top = 167
        Width = 39
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Usu'#225'rio:'
      end
      object Label12: TLabel
        Left = 279
        Top = 167
        Width = 34
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Senha:'
      end
      object Label13: TLabel
        Left = 553
        Top = 167
        Width = 28
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Email:'
      end
      object Label8: TLabel
        Left = 553
        Top = 114
        Width = 97
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Nome para exibi'#231#227'o:'
      end
      object Label15: TLabel
        Left = 553
        Top = 59
        Width = 26
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Perfil:'
      end
      object LaEsqueciSenha: TLabel
        Left = 386
        Top = 36
        Width = 238
        Height = 16
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Redefinir senha do usu'#225'rio selecionado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnClick = LaEsqueciSenhaClick
        OnMouseEnter = LaEsqueciSenhaMouseEnter
        OnMouseLeave = LaEsqueciSenhaMouseLeave
      end
      object Label7: TLabel
        Left = 101
        Top = 10
        Width = 45
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro:'
      end
      object Label14: TLabel
        Left = 244
        Top = 10
        Width = 42
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Alterado:'
      end
      object Label16: TLabel
        Left = 6
        Top = 224
        Width = 27
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Sexo:'
      end
      object Label17: TLabel
        Left = 279
        Top = 224
        Width = 59
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Nascimento:'
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 6
        Top = 31
        Width = 89
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Codigo'
        DataSource = DsWUsers
        TabOrder = 0
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBRgTipo: TDBRadioGroup
        Left = 6
        Top = 59
        Width = 539
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Tipo de perfil'
        Columns = 4
        DataField = 'Tipo'
        DataSource = DsWUsers
        Items.Strings = (
          'N'#237'vel 0'
          'N'#237'vel 1'
          'N'#237'vel 2'
          'N'#237'vel 3')
        TabOrder = 3
        Values.Strings = (
          '0'
          '1'
          '2'
          '3')
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 6
        Top = 134
        Width = 539
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'ENTNOME'
        DataSource = DsWUsers
        TabOrder = 5
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit4: TdmkDBEdit
        Left = 6
        Top = 188
        Width = 266
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Username'
        DataSource = DsWUsers
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit5: TdmkDBEdit
        Left = 279
        Top = 188
        Width = 266
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'SENHA'
        DataSource = DsWUsers
        Font.Charset = SYMBOL_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Wingdings'
        Font.Style = []
        ParentFont = False
        PasswordChar = '|'
        TabOrder = 8
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBCheckBox1: TDBCheckBox
        Left = 553
        Top = 247
        Width = 65
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ativo'
        DataField = 'Ativo'
        DataSource = DsWUsers
        TabOrder = 12
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object dmkDBEdit6: TdmkDBEdit
        Left = 553
        Top = 188
        Width = 530
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Email'
        DataSource = DsWUsers
        TabOrder = 9
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit3: TdmkDBEdit
        Left = 553
        Top = 134
        Width = 530
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'PersonalName'
        DataSource = DsWUsers
        TabOrder = 6
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit7: TdmkDBEdit
        Left = 553
        Top = 81
        Width = 530
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NOMEPEF'
        DataSource = DsWUsers
        TabOrder = 4
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit8: TdmkDBEdit
        Left = 101
        Top = 31
        Width = 135
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'DataCad_TXT'
        DataSource = DsWUsers
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit9: TdmkDBEdit
        Left = 244
        Top = 31
        Width = 135
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'DataAlt_TXT'
        DataSource = DsWUsers
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit10: TdmkDBEdit
        Left = 6
        Top = 245
        Width = 266
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Sexo_TXT'
        DataSource = DsWUsers
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 10
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit11: TdmkDBEdit
        Left = 279
        Top = 245
        Width = 266
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'DtaNatal_TXT'
        DataSource = DsWUsers
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 11
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 268
      Width = 908
      Height = 79
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 354
      ExplicitWidth = 1135
      object Panel5: TPanel
        Left = 2
        Top = 18
        Width = 212
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 214
        Top = 18
        Width = 30
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 492
        Top = 18
        Width = 641
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 310
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 158
          Top = 5
          Width = 147
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 2
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 478
          Top = 0
          Width = 163
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 148
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 94
    Width = 908
    Height = 347
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 908
      Height = 283
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      ExplicitWidth = 1135
      object Label9: TLabel
        Left = 21
        Top = 12
        Width = 36
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
      end
      object LaPerfil: TLabel
        Left = 555
        Top = 64
        Width = 26
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Perfil:'
      end
      object Label10: TLabel
        Left = 554
        Top = 114
        Width = 97
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Nome para exibi'#231#227'o:'
      end
      object LaEntidade: TLabel
        Left = 21
        Top = 116
        Width = 45
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Entidade:'
      end
      object Label3: TLabel
        Left = 21
        Top = 169
        Width = 60
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Usu'#225'rio: [F4]'
      end
      object Label4: TLabel
        Left = 288
        Top = 169
        Width = 34
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Senha:'
      end
      object Label5: TLabel
        Left = 555
        Top = 169
        Width = 79
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Confirmar senha:'
      end
      object Label6: TLabel
        Left = 22
        Top = 223
        Width = 52
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'E-mail: [F4]'
      end
      object Label24: TLabel
        Left = 966
        Top = 167
        Width = 59
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Nascimento:'
      end
      object EdCodigo: TdmkEdit
        Left = 21
        Top = 31
        Width = 87
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Color = clInactiveCaption
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBackground
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object RgTipo: TdmkRadioGroup
        Left = 21
        Top = 64
        Width = 525
        Height = 46
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Tipo de perfil'
        Columns = 4
        Items.Strings = (
          'N'#237'vel 0'
          'N'#237'vel 1'
          'N'#237'vel 2'
          'N'#237'vel 3')
        TabOrder = 1
        OnClick = RgTipoClick
        UpdType = utYes
        OldValor = 0
      end
      object EdPerfil: TdmkEditCB
        Left = 555
        Top = 84
        Width = 99
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPerfil
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPerfil: TdmkDBLookupComboBox
        Left = 656
        Top = 84
        Width = 448
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsWPerfis
        TabOrder = 3
        dmkEditCB = EdPerfil
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdNome: TdmkEdit
        Left = 554
        Top = 135
        Width = 550
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CBEntidade: TdmkDBLookupComboBox
        Left = 122
        Top = 135
        Width = 424
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsEntidades
        TabOrder = 5
        dmkEditCB = EdEntidade
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdEntidade: TdmkEditCB
        Left = 21
        Top = 135
        Width = 98
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEntidade
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdLogin: TdmkEdit
        Left = 21
        Top = 190
        Width = 258
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = True
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnExit = EdLoginExit
        OnKeyDown = EdLoginKeyDown
      end
      object EdSenha1: TdmkEdit
        Left = 287
        Top = 190
        Width = 258
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = True
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdSenha2: TdmkEdit
        Left = 555
        Top = 190
        Width = 259
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 9
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = True
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnExit = EdSenha2Exit
      end
      object EdEmail: TdmkEdit
        Left = 21
        Top = 242
        Width = 525
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 12
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = True
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnKeyDown = EdEmailKeyDown
      end
      object CkAtivo: TdmkCheckBox
        Left = 555
        Top = 245
        Width = 55
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ativo'
        TabOrder = 13
        QryCampo = 'Ativo'
        UpdCampo = 'Ativo'
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
      object CkContinuar: TCheckBox
        Left = 623
        Top = 245
        Width = 149
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Continuar inserindo.'
        TabOrder = 14
      end
      object RGSexo: TdmkRadioGroup
        Left = 821
        Top = 169
        Width = 139
        Height = 46
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Sexo: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'NI'
          'M'
          'F')
        TabOrder = 10
        OnClick = RGSexoClick
        OnExit = RGSexoExit
        QryCampo = 'Sexo'
        UpdCampo = 'Sexo'
        UpdType = utYes
        OldValor = 0
      end
      object TPDtaNatal: TdmkEditDateTimePicker
        Left = 966
        Top = 188
        Width = 138
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Date = 40338.000000000000000000
        Time = 0.632372372681857100
        TabOrder = 11
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtaNatal'
        UpdCampo = 'DtaNatal'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 270
      Width = 908
      Height = 77
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      ExplicitTop = 356
      ExplicitWidth = 1135
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 15
        Top = 21
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 963
        Top = 18
        Width = 170
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 147
          Height = 50
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 908
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 266
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 10
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 266
      Top = 0
      Width = 760
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 1
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 186
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Senhas Web'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 186
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Senhas Web'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 186
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Senhas Web'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GB_R: TGroupBox
      Left = 1026
      Top = 0
      Width = 109
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 2
      object ImgTipo: TdmkImage
        Left = 59
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
      object ImgWEB: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 51
    Width = 908
    Height = 43
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1131
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 672
    Top = 16
  end
  object QrWUsers: TMySQLQuery
    Database = DModG.DBDmk
    BeforeOpen = QrWUsersBeforeOpen
    AfterOpen = QrWUsersAfterOpen
    SQL.Strings = (
      'SELECT wus.*, wpe.Nome NOMEPEF, '
      'AES_DECRYPT(Password, "") SENHA,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) CLINOME,'
      
        'IF(wus.Entidade = 0, "", IF (enb.Tipo=0, enb.RazaoSocial, enb.No' +
        'me)) ENTNOME'
      'FROM wusers wus'
      'LEFT JOIN wperfis wpe ON wpe.Codigo = wus.Perfil'
      'LEFT JOIN clientes cli ON cli.Codigo = wus.Cliente'
      'LEFT JOIN entidades ent ON ent.Codigo = cli.Cliente'
      'LEFT JOIN entidades enb ON enb.Codigo = wus.Entidade'
      'WHERE wus.Codigo > 0')
    Left = 420
    Top = 16
    object QrWUsersCodigo: TAutoIncField
      FieldName = 'Codigo'
      Origin = 'wusers.Codigo'
    end
    object QrWUsersUsername: TWideStringField
      FieldName = 'Username'
      Origin = 'wusers.Username'
      Required = True
      Size = 32
    end
    object QrWUsersPassword: TWideStringField
      FieldName = 'Password'
      Origin = 'wusers.Password'
      Required = True
      Size = 32
    end
    object QrWUsersPersonalName: TWideStringField
      FieldName = 'PersonalName'
      Origin = 'wusers.PersonalName'
      Required = True
      Size = 32
    end
    object QrWUsersLoginID: TWideStringField
      FieldName = 'LoginID'
      Origin = 'wusers.LoginID'
      Size = 32
    end
    object QrWUsersEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'wusers.Entidade'
      Required = True
    end
    object QrWUsersTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'wusers.Tipo'
      Required = True
    end
    object QrWUsersLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'wusers.Lk'
    end
    object QrWUsersDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'wusers.DataCad'
    end
    object QrWUsersDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'wusers.DataAlt'
    end
    object QrWUsersUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'wusers.UserCad'
    end
    object QrWUsersUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'wusers.UserAlt'
    end
    object QrWUsersAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'wusers.AlterWeb'
      Required = True
    end
    object QrWUsersAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'wusers.Ativo'
      Required = True
    end
    object QrWUsersSENHA: TWideStringField
      FieldName = 'SENHA'
      Size = 32
    end
    object QrWUsersEmail: TWideStringField
      FieldName = 'Email'
      Origin = 'wusers.Email'
      Required = True
      Size = 100
    end
    object QrWUsersPerfil: TIntegerField
      FieldName = 'Perfil'
      Origin = 'wusers.Perfil'
    end
    object QrWUsersCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'wusers.Cliente'
    end
    object QrWUsersNOMEPEF: TWideStringField
      FieldName = 'NOMEPEF'
      Origin = 'wperfis.Nome'
      Size = 50
    end
    object QrWUsersCLINOME: TWideStringField
      FieldName = 'CLINOME'
      Size = 100
    end
    object QrWUsersENTNOME: TWideStringField
      FieldName = 'ENTNOME'
      Size = 100
    end
    object QrWUsersDataCad_TXT: TWideStringField
      FieldName = 'DataCad_TXT'
      Size = 10
    end
    object QrWUsersDataAlt_TXT: TWideStringField
      FieldName = 'DataAlt_TXT'
      Size = 10
    end
    object QrWUsersDtaNatal: TDateField
      FieldName = 'DtaNatal'
    end
    object QrWUsersDtaNatal_TXT: TWideStringField
      FieldName = 'DtaNatal_TXT'
      Size = 10
    end
    object QrWUsersSexo_TXT: TWideStringField
      FieldName = 'Sexo_TXT'
      Size = 15
    end
    object QrWUsersSexo: TSmallintField
      FieldName = 'Sexo'
    end
  end
  object DsWUsers: TDataSource
    DataSet = QrWUsers
    Left = 448
    Top = 16
  end
  object QrWControl: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Crypt'
      'FROM wcontrol')
    Left = 588
    Top = 16
    object QrWControlCrypt: TWideStringField
      FieldName = 'Crypt'
      Size = 30
    end
  end
  object DsClientes: TDataSource
    Left = 616
    Top = 16
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 560
    Top = 16
  end
  object QrEntidades: TMySQLQuery
    Database = DModG.DBDmk
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome,'
      'IF (ent.Tipo=0, ent.EEmail, PEmail) Email'
      'FROM  entidades ent'
      'ORDER BY Nome')
    Left = 532
    Top = 16
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrEntidadesNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrEntidadesCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrEntidadesEmail: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
  end
  object DsWPerfis: TDataSource
    DataSet = QrWPerfis
    Left = 504
    Top = 16
  end
  object QrWPerfis: TMySQLQuery
    Database = DModG.DBDmk
    SQL.Strings = (
      'SELECT Codigo, Nome, Tipo'
      'FROM wperfis'
      'WHERE Tipo=:P0'
      'ORDER BY Nome')
    Left = 476
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Origin = 'wperfis.Codigo'
      Required = True
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Origin = 'wperfis.Nome'
      Required = True
      Size = 50
    end
    object QrWPerfisTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'wperfis.Tipo'
    end
  end
  object QrLoc: TMySQLQuery
    Database = DModG.DBDmk
    Left = 644
    Top = 16
  end
end
