unit WUsersPesq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, dmkRadioGroup, dmkDBGrid, Variants, dmkPermissoes, UnDmkEnums,
  frxClass, frxDBSet, UnDmkProcFunc, UnProjGroup_Consts;

type
  TFmWUsersPesq = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtPesquisa: TBitBtn;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    QrWPerfis: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    QrWPerfisTipo: TSmallintField;
    DsWPerfis: TDataSource;
    LaPerfil: TLabel;
    EdPerfil: TdmkEditCB;
    CBPerfil: TdmkDBLookupComboBox;
    LaEntidade: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    Label10: TLabel;
    EdNome: TdmkEdit;
    Label6: TLabel;
    EdEmail: TdmkEdit;
    EdLogin: TdmkEdit;
    Label3: TLabel;
    RgTipo: TdmkRadioGroup;
    dmkDBGrid1: TdmkDBGrid;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNome: TWideStringField;
    DsEntidades: TDataSource;
    QrWUsers: TmySQLQuery;
    QrWUsersCodigo: TAutoIncField;
    QrWUsersUsername: TWideStringField;
    QrWUsersPassword: TWideStringField;
    QrWUsersPersonalName: TWideStringField;
    QrWUsersLoginID: TWideStringField;
    QrWUsersEntidade: TIntegerField;
    QrWUsersTipo: TSmallintField;
    QrWUsersLk: TIntegerField;
    QrWUsersDataCad: TDateField;
    QrWUsersDataAlt: TDateField;
    QrWUsersUserCad: TIntegerField;
    QrWUsersUserAlt: TIntegerField;
    QrWUsersAlterWeb: TSmallintField;
    QrWUsersAtivo: TSmallintField;
    QrWUsersSENHA: TWideStringField;
    QrWUsersEmail: TWideStringField;
    QrWUsersPerfil: TIntegerField;
    QrWUsersCliente: TIntegerField;
    QrWUsersNOMEPEF: TWideStringField;
    QrWUsersCLINOME: TWideStringField;
    QrWUsersENTNOME: TWideStringField;
    DsWUsers: TDataSource;
    QrWUsersStatus: TSmallintField;
    QrWUsersNOMETIP: TWideStringField;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    RGSituacao: TdmkRadioGroup;
    BtAbrir: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    ImgWEB: TdmkImage;
    SbImprime: TBitBtn;
    frxWEB_USERS_002_001: TfrxReport;
    frxDsWUsers: TfrxDBDataset;
    Label16: TLabel;
    EdTotItens: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure dmkDBGrid1DblClick(Sender: TObject);
    procedure BtAbrirClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    { Private declarations }
    procedure LocalizaRegistro(Codigo: Integer);
  public
    { Public declarations }
    procedure PesquisaUsuario(Tipo, Perfil, Entidade, Situacao: Integer;
              Nome, Usuario, Email: String);
  end;

  var
  FmWUsersPesq: TFmWUsersPesq;

implementation

uses UnMyObjects, Module, WUsers, MyDBCheck, UnDmkWeb, MyListas;

{$R *.DFM}

procedure TFmWUsersPesq.BtAbrirClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrWUsersCodigo.Value;
  //
  LocalizaRegistro(Codigo);
end;

procedure TFmWUsersPesq.BtPesquisaClick(Sender: TObject);
var
  Tipo, Perfil, Entidade, Situacao: Integer;
  Nome, Usuario, Email: String;
begin
  PesquisaUsuario(RgTipo.ItemIndex, EdPerfil.ValueVariant,
    EdEntidade.ValueVariant, RGSituacao.ItemIndex, EdNome.ValueVariant,
    EdLogin.ValueVariant, EdEmail.ValueVariant);
end;

procedure TFmWUsersPesq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWUsersPesq.dmkDBGrid1DblClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrWUsersCodigo.Value;
  //
  LocalizaRegistro(Codigo);
end;

procedure TFmWUsersPesq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
  //
  ImgTipo.SQLType := stPsq;
end;

procedure TFmWUsersPesq.FormCreate(Sender: TObject);
var
  Compo: TComponent;
  Query: TmySQLQuery;
  I: Integer;
begin
  for I := 0 to FmWUsersPesq.ComponentCount - 1 do
  begin
    Compo := FmWUsersPesq.Components[I];
    if Compo is TmySQLQuery then
    begin
      Query := TmySQLQuery(Compo);
      Query.Database := Dmod.MyDBn;
    end;
  end;
  QrWPerfis.Close;
  QrWPerfis.Open;
  //
  QrEntidades.Close;
  QrEntidades.Open;
  //
  RgTipo.ItemIndex        := 4;
  EdPerfil.ValueVariant   := 0;
  CBPerfil.KeyValue       := Null;
  EdNome.ValueVariant     := '';
  EdLogin.ValueVariant    := '';
  EdEntidade.ValueVariant := 0;
  CBEntidade.KeyValue     := Null;
  EdEmail.ValueVariant    := '';
  RGSituacao.ItemIndex    := 2;
  EdTotItens.ValueVariant := 0;
  //
  QrWUsers.Close;
end;

procedure TFmWUsersPesq.PesquisaUsuario(Tipo, Perfil, Entidade, Situacao: Integer;
  Nome, Usuario, Email: String);
begin
  QrWUsers.Close;
  QrWUsers.Database := Dmod.MyDBn;
  QrWUsers.SQL.Clear;
  QrWUsers.SQL.Add('SELECT wus.*, wpe.Nome NOMEPEF,');
  QrWUsers.SQL.Add('AES_DECRYPT(Password, "") SENHA,');
  QrWUsers.SQL.Add('IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) CLINOME,');
  QrWUsers.SQL.Add('IF (enb.Tipo=0, enb.RazaoSocial, enb.Nome) ENTNOME,');
  QrWUsers.SQL.Add('CASE wus.Tipo');
  QrWUsers.SQL.Add('WHEN 0 THEN "Administrador"');
  QrWUsers.SQL.Add('WHEN 1 THEN "Boss"');
  QrWUsers.SQL.Add('WHEN 2 THEN "Cliente"');
  QrWUsers.SQL.Add('WHEN 3 THEN "Usu�rio" END NOMETIP');
  QrWUsers.SQL.Add('FROM wusers wus');
  QrWUsers.SQL.Add('LEFT JOIN wperfis wpe ON wpe.Codigo = wus.Perfil');
  //Tabela clientes � espec�fica do DControl cada aplicativo pode ter a sua
  if CO_DMKID_APP = 17 then //DControl
  begin
    QrWUsers.SQL.Add('LEFT JOIN clientes cli ON cli.Codigo = wus.Cliente');
    QrWUsers.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo = cli.Cliente');
  end else
  begin
    QrWUsers.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo = wus.Cliente');
    QrWUsers.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo = cli.Codigo');
  end;
  QrWUsers.SQL.Add('LEFT JOIN entidades enb ON enb.Codigo = wus.Entidade');
  QrWUsers.SQL.Add('WHERE wus.Codigo > 0');
  if Tipo <> 4 then
    QrWUsers.SQL.Add('AND wus.Tipo=' + FormatFloat('0', Tipo));
  if Perfil <> 0 then
    QrWUsers.SQL.Add('AND wus.Perfil=' + FormatFloat('0', Perfil));
  if Length(Nome) > 0 then
    QrWUsers.SQL.Add('AND wus.PersonalName LIKE "%' + Nome + '%"');
  if Length(Usuario) > 0 then
    QrWUsers.SQL.Add('AND wus.Username LIKE "%' + Usuario + '%"');
  if Entidade <> 0 then
    QrWUsers.SQL.Add('AND wus.Entidade=' + FormatFloat('0', Entidade));
  if Length(Email) > 0 then
    QrWUsers.SQL.Add('AND wus.Email LIKE "%' + Email + '%"');
  if Situacao <> 2 then
  begin
    case Situacao of
      0: QrWUsers.SQL.Add('AND wus.Ativo=1');
      1: QrWUsers.SQL.Add('AND wus.Ativo=0')
    end;
  end;
  QrWUsers.SQL.Add('ORDER BY ENTNOME, NOMETIP');
  QrWUsers.Open;
  //
  EdTotItens.ValueVariant := QrWUsers.RecordCount;
end;

procedure TFmWUsersPesq.SbImprimeClick(Sender: TObject);
var
  Query: TmySQLQuery;
  Controle: Integer;
begin
  if (QrWUsers.State <> dsInactive) and (QrWUsers.RecordCount > 0) then
  begin
    if dmkDBGrid1.DataSource.DataSet is TmySQLQuery then
      Query := TmySQLQuery(dmkDBGrid1.DataSource.DataSet)
    else
      Query := nil;
    if Query <> nil then
      Query.SortFieldNames := '';
    //
    MyObjects.frxMostra(frxWEB_USERS_002_001, 'Lista de Usu�rios WEB');
  end else
    Geral.MB_Aviso('N�o existe nenhum resultado a ser impresso!');
end;

procedure TFmWUsersPesq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWUsersPesq.LocalizaRegistro(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmWUsers, FmWUsers, afmoNegarComAviso) then
  begin
    FmWUsers.LocCod(Codigo, Codigo);
    FmWUsers.ShowModal;
    FmWUsers.Destroy;
  end;
end;

end.
