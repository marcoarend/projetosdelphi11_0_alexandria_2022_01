unit WProtocol;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkCheckBox,
  dmkDBLookupComboBox, dmkEditCB, ComCtrls, Variants, Menus, Grids, DBGrids,
  dmkDBGrid, UnDmkProcFunc, UnDmkEnums;

type
  TFmWProtocol = class(TForm)
    PainelDados: TPanel;
    DsWProtocol: TDataSource;
    QrWProtocol: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TStaticText;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtProtocolo: TBitBtn;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    QrWProtocolCodigo: TIntegerField;
    Label10: TLabel;
    EdTarefa: TdmkEditCB;
    CBTarefa: TdmkDBLookupComboBox;
    Label3: TLabel;
    EdPerfil: TdmkEditCB;
    CBPerfil: TdmkDBLookupComboBox;
    TPDataExp: TDateTimePicker;
    Label18: TLabel;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNome: TWideStringField;
    DsEntidades: TDataSource;
    QrTarefas: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsTarefas: TDataSource;
    QrWProtocolTarefa: TIntegerField;
    QrWProtocolDataHora: TDateTimeField;
    QrWProtocolLk: TIntegerField;
    QrWProtocolDataCad: TDateField;
    QrWProtocolDataAlt: TDateField;
    QrWProtocolUserCad: TIntegerField;
    QrWProtocolUserAlt: TIntegerField;
    QrWProtocolAlterWeb: TSmallintField;
    QrWProtocolAtivo: TSmallintField;
    QrWProtocolUsuario: TIntegerField;
    QrWProtocolDataExp: TDateField;
    QrWProtocolDataFin: TDateTimeField;
    QrWProtocolFinaliz: TSmallintField;
    QrWProtocolEntidade: TIntegerField;
    QrWProtocolPROTNOME: TWideStringField;
    QrWProtocolPersonalName: TWideStringField;
    QrWProtocolNOMEENT: TWideStringField;
    QrWProtocolDATAEXP_TXT: TWideStringField;
    QrWProtocolDATAHORA_TXT: TWideStringField;
    QrWProtocolDATAHORAFIN_TXT: TWideStringField;
    PMProtocolo: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    N1: TMenuItem;
    Encerra1: TMenuItem;
    QrWProtocolIPCad: TWideStringField;
    QrWProtocolIPAlt: TWideStringField;
    QrWProtocolCliente: TIntegerField;
    QrWProtocolPerfil: TIntegerField;
    Label13: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    Label14: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    QrPerfis: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsPerfis: TDataSource;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNome: TWideStringField;
    DsClientes: TDataSource;
    QrWProtocolNOMEPERFIL: TWideStringField;
    QrWProtocolNOMECLI: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PainelData: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    dmkDBEdit3: TdmkDBEdit;
    dmkDBEdit4: TdmkDBEdit;
    dmkDBEdit5: TdmkDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    dmkDBEdit6: TdmkDBEdit;
    dmkDBEdit7: TdmkDBEdit;
    dmkDBEdit8: TdmkDBEdit;
    dmkDBEdit9: TdmkDBEdit;
    Panel4: TPanel;
    Label17: TLabel;
    EdPesID: TdmkEdit;
    Label19: TLabel;
    EdPesTarefa: TdmkEditCB;
    CBPesTarefa: TdmkDBLookupComboBox;
    Label20: TLabel;
    EdPesPerfil: TdmkEditCB;
    CBPesPerfil: TdmkDBLookupComboBox;
    Label21: TLabel;
    EdPesEntidade: TdmkEditCB;
    CBPesEntidade: TdmkDBLookupComboBox;
    CBPesCliente: TdmkDBLookupComboBox;
    EdPesCliente: TdmkEditCB;
    Label22: TLabel;
    GBPesAbertura: TGroupBox;
    CkPesAbeIni: TCheckBox;
    TPPesAbeIni: TDateTimePicker;
    CkPesAbeFim: TCheckBox;
    TPPesAbeFim: TDateTimePicker;
    GBPesEncerr: TGroupBox;
    CkPesEncerrIni: TCheckBox;
    TpPesEncerrIni: TDateTimePicker;
    CkPesEncerrFim: TCheckBox;
    TPPesEncerrFim: TDateTimePicker;
    RGPesStatus: TRadioGroup;
    Label23: TLabel;
    EdPesUsuario: TdmkEditCB;
    CBPesUsuario: TdmkDBLookupComboBox;
    QrPesTarefas: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    DsPesTarefas: TDataSource;
    QrPesPerfis: TmySQLQuery;
    IntegerField4: TIntegerField;
    StringField4: TWideStringField;
    DsPesPerfis: TDataSource;
    QrPesEntidades: TmySQLQuery;
    IntegerField5: TIntegerField;
    StringField5: TWideStringField;
    DsPesEntidades: TDataSource;
    QrPesClientes: TmySQLQuery;
    IntegerField6: TIntegerField;
    StringField6: TWideStringField;
    DsPesClientes: TDataSource;
    QrPesUsers: TmySQLQuery;
    QrPesUsersCodigo: TAutoIncField;
    QrPesUsersPersonalName: TWideStringField;
    DsPesUsers: TDataSource;
    Panel6: TPanel;
    Panel9: TPanel;
    Panel10: TPanel;
    BtPesq: TBitBtn;
    BtLimpar: TBitBtn;
    QrPesWProtocol: TmySQLQuery;
    DsPesWProtocol: TDataSource;
    QrPesWProtocolCodigo: TIntegerField;
    QrPesWProtocolTarefa: TIntegerField;
    QrPesWProtocolDataExp: TDateField;
    QrPesWProtocolIPCad: TWideStringField;
    QrPesWProtocolIPAlt: TWideStringField;
    QrPesWProtocolDataHora: TDateTimeField;
    QrPesWProtocolDataFin: TDateTimeField;
    QrPesWProtocolFinaliz: TSmallintField;
    QrPesWProtocolUsuario: TIntegerField;
    QrPesWProtocolCliente: TIntegerField;
    QrPesWProtocolEntidade: TIntegerField;
    QrPesWProtocolPerfil: TIntegerField;
    QrPesWProtocolLk: TIntegerField;
    QrPesWProtocolDataCad: TDateField;
    QrPesWProtocolDataAlt: TDateField;
    QrPesWProtocolUserCad: TIntegerField;
    QrPesWProtocolUserAlt: TIntegerField;
    QrPesWProtocolAlterWeb: TSmallintField;
    QrPesWProtocolAtivo: TSmallintField;
    QrPesWProtocolPROTNOME: TWideStringField;
    QrPesWProtocolNOMEPERFIL: TWideStringField;
    QrPesWProtocolPersonalName: TWideStringField;
    QrPesWProtocolNOMEENT: TWideStringField;
    QrPesWProtocolNOMECLI: TWideStringField;
    QrPesWProtocolDATAEXP_TXT: TWideStringField;
    QrPesWProtocolDATAHORA_TXT: TWideStringField;
    QrPesWProtocolDATAHORAFIN_TXT: TWideStringField;
    DBGItens: TdmkDBGrid;
    QrPesWProtocolFINALIX_TXT: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrWProtocolAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrWProtocolBeforeOpen(DataSet: TDataSet);
    procedure QrWProtocolCalcFields(DataSet: TDataSet);
    procedure Altera1Click(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure BtProtocoloClick(Sender: TObject);
    procedure PMProtocoloPopup(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure Encerra1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure BtLimparClick(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure QrPesWProtocolCalcFields(DataSet: TDataSet);
    procedure DBGItensDblClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure LimpaCamposPesq();
    procedure ReopenClientes();
    procedure ReopenPesClientes();
  public
    { Public declarations }
  end;

var
  FmWProtocol: TFmWProtocol;
const
  FFormatFloat = '00000';

implementation

uses Module, ModuleGeral, MyDBCheck, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmWProtocol.LimpaCamposPesq;
begin
  EdPesID.ValueVariant       := 0;
  EdPesTarefa.ValueVariant   := 0;
  CBPesTarefa.KeyValue       := Null;
  EdPesPerfil.ValueVariant   := 0;
  CBPesPerfil.KeyValue       := Null;
  EdPesEntidade.ValueVariant := 0;
  CBPesEntidade.KeyValue     := Null;
  EdPesCliente.ValueVariant  := 0;
  CBPesCliente.KeyValue      := Null;
  EdPesUsuario.ValueVariant  := 0;
  CBPesUsuario.KeyValue      := Null;
  RGPesStatus.ItemIndex      := -1;
  CkPesAbeIni.Checked        := False;
  TPPesAbeIni.Date           := Date;
  CkPesAbeFim.Checked        := False;
  TPPesAbeFim.Date           := Date;
  CkPesEncerrIni.Checked     := False;
  TpPesEncerrIni.Date        := Date;
  CkPesEncerrFim.Checked     := False;
  TPPesEncerrFim.Date        := Date;
end;

procedure TFmWProtocol.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmWProtocol.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrWProtocolCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmWProtocol.DefParams;
begin
  VAR_GOTOTABELA := 'wprotocol';
  VAR_GOTOMYSQLTABLE := QrWProtocol;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDBn;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wpr.*, pro.Nome PROTNOME, per.Nome NOMEPERFIL,');
  VAR_SQLx.Add('wus.PersonalName, IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,');
  VAR_SQLx.Add('IF (enc.Tipo=0, enc.RazaoSocial, enc.Nome) NOMECLI');
  VAR_SQLx.Add('FROM wprotocol wpr');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo = wpr.Entidade');
  VAR_SQLx.Add('LEFT JOIN protocolos pro ON pro.Codigo = wpr.Tarefa');
  VAR_SQLx.Add('LEFT JOIN wusers wus ON wus.Codigo = wpr.Usuario');
  VAR_SQLx.Add('LEFT JOIN wperfis per ON per.Codigo = wpr.Perfil');
  //Tabela clientes � espec�fica do DControl cada aplicativo pode ter a sua
  if UpperCase(Application.Title) = 'DCONTROL' then
  begin
    VAR_SQLx.Add('LEFT JOIN clientes cli ON cli.Codigo = wpr.Cliente');
  end else
  begin
    VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo = wpr.Cliente');
  end;
  VAR_SQLx.Add('LEFT JOIN entidades enc ON enc.Codigo = cli.Cliente');
  VAR_SQLx.Add('WHERE wpr.Codigo > 0');
  //
  VAR_SQL1.Add('AND wpr.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmWProtocol.Encerra1Click(Sender: TObject);
var
  Codigo: Integer;
  DataHora: String;
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  Codigo := QrWProtocolCodigo.Value;
  //
  Dmod.QrUpdN.Close;
  Dmod.QrUpdN.SQL.Clear;
  Dmod.QrUpdN.SQL.Add('UPDATE wprotocol SET DataFin=:P0, Finaliz=:P1 ');
  Dmod.QrUpdN.SQL.Add('WHERE Codigo=:P2');
  //
  if QrWProtocolFinaliz.Value = 0 then
  begin
    DataHora := DateTimeToStr(DModG.ObtemAgora());
    if InputQuery('Data / hora', 'Informe a data e a hora:', DataHora) then
    begin
      Dmod.QrUpdN.Params[0].AsDateTime := StrToDateTime(Trim(DataHora));
      Dmod.QrUpdN.Params[1].AsInteger  := 1;
    end;
  end else
  begin
    Dmod.QrUpdN.Params[0].AsDateTime := 0;
    Dmod.QrUpdN.Params[1].AsInteger  := 0;
  end;
  //
  Dmod.QrUpdN.Params[2].AsInteger  := Codigo;
  Dmod.QrUpdN.ExecSQL;
  //
  LocCod(Codigo, Codigo);
  //
  Application.ProcessMessages;
end;

procedure TFmWProtocol.Exclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if MyObjects.FIC(QrWProtocolFinaliz.Value = 1, nil,
    'Exclus�o abortada! Motivo protocolo j� finalizado!') then Exit;
  if Geral.MensagemBox('Confirma a exclus�o do registro?', 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES
  then begin
    Codigo := QrWProtocolCodigo.Value;
    //
    Dmod.QrUpdn.SQL.Clear;
    Dmod.QrUpdn.SQL.Add('DELETE FROM wprotocol WHERE Codigo=:P0');
    Dmod.QrUpdn.Params[00].AsInteger := Codigo;
    Dmod.QrUpdn.ExecSQL;
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmWProtocol.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant   := FormatFloat(FFormatFloat, Codigo);
        EdTarefa.ValueVariant   := 0;
        CBTarefa.KeyValue       := Null;
        TPDataExp.Date          := Date + Dmod.QrWControlDiasExp.Value;
        EdEntidade.ValueVariant := 0;
        CBEntidade.KeyValue     := Null;
        //
      end else begin
        EdCodigo.ValueVariant   := QrWProtocolCodigo.Value;
        EdTarefa.ValueVariant   := QrWProtocolTarefa.Value;
        CBTarefa.KeyValue       := QrWProtocolTarefa.Value;
        TPDataExp.Date          := QrWProtocolDataExp.Value;
        EdEntidade.ValueVariant := QrWProtocolEntidade.Value;
        CBEntidade.KeyValue     := QrWProtocolEntidade.Value;
        //
      end;
      EdTarefa.SetFocus;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.SQLType := SQLType;
  GOTOy.BotoesSb(LaTipo.SQLType);
end;

procedure TFmWProtocol.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePageIndex = 1 then
  begin
    Screen.Cursor := crHourGlass;
    //
    QrPesTarefas.Close;
    QrPesTarefas.Open;
    QrPesPerfis.Close;
    QrPesPerfis.Open;
    QrPesEntidades.Close;
    QrPesEntidades.Open;
    QrPesUsers.Close;
    QrPesUsers.Open;
    ReopenPesClientes;
    //
    LimpaCamposPesq;
    //
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmWProtocol.PMProtocoloPopup(Sender: TObject);
var
  Enab1, Enab2: Boolean;
begin
  Enab1 := (QrWProtocol.State <> dsInactive) and (QrWProtocol.RecordCount > 0);
  Enab2 := QrWProtocolFinaliz.Value = 0;
  //
  case QrWProtocolFinaliz.Value of
    0: Encerra1.Caption := 'Encerrar';
    1: Encerra1.Caption := 'Desfaz encerramento';
  end;
  //
  Altera1.Enabled  := Enab1 and Enab2;
  Exclui1.Enabled  := Enab1 and Enab2;
  Encerra1.Enabled := Enab1;
end;

procedure TFmWProtocol.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmWProtocol.QueryPrincipalAfterOpen;
begin
end;

procedure TFmWProtocol.ReopenClientes;
begin
  QrClientes.Close;
  QrClientes.SQL.Clear;
  if UpperCase(Application.Title) = 'DCONTROL' then
  begin
    QrClientes.SQL.Add('SELECT cli.Codigo,');
    QrClientes.SQL.Add('IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome');
    QrClientes.SQL.Add('FROM  clientes cli');
    QrClientes.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo = cli.Cliente');
    QrClientes.SQL.Add('ORDER BY Nome');
  end else
  begin
    QrClientes.SQL.Add('SELECT Codigo,');
    QrClientes.SQL.Add('IF (Tipo=0, RazaoSocial, Nome) Nome');
    QrClientes.SQL.Add('FROM  entidades');
    QrClientes.SQL.Add('ORDER BY Nome');
  end;
  QrClientes.Open;
end;

procedure TFmWProtocol.ReopenPesClientes;
begin
  QrPesClientes.Close;
  QrPesClientes.SQL.Clear;
  if UpperCase(Application.Title) = 'DCONTROL' then
  begin
    QrPesClientes.SQL.Add('SELECT cli.Codigo,');
    QrPesClientes.SQL.Add('IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome');
    QrPesClientes.SQL.Add('FROM  clientes cli');
    QrPesClientes.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo = cli.Cliente');
    QrPesClientes.SQL.Add('ORDER BY Nome');
  end else
  begin
    QrPesClientes.SQL.Add('SELECT Codigo,');
    QrPesClientes.SQL.Add('IF (Tipo=0, RazaoSocial, Nome) Nome');
    QrPesClientes.SQL.Add('FROM  entidades');
    QrPesClientes.SQL.Add('ORDER BY Nome');
  end;
  QrPesClientes.SQL.Add('');
  QrPesClientes.Open;
end;

procedure TFmWProtocol.DBGItensDblClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrPesWProtocolCodigo.Value;
  //
  LocCod(Codigo, Codigo);
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmWProtocol.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmWProtocol.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmWProtocol.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmWProtocol.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmWProtocol.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmWProtocol.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrWProtocolCodigo.Value;
  Close;
end;

procedure TFmWProtocol.Altera1Click(Sender: TObject);
begin
  MostraEdicao(1, stUpd, QrWProtocolCodigo.Value);
end;

procedure TFmWProtocol.BtConfirmaClick(Sender: TObject);
var
  Codigo, Tarefa, Perfil, Cliente, Entidade: Integer;
  DataHora: TDateTime;
  DataExp: TDate;
begin
  Tarefa   := EdTarefa.ValueVariant;
  Perfil   := EdPerfil.ValueVariant;
  Cliente  := EdCliente.ValueVariant;
  Entidade := EdEntidade.ValueVariant;
  DataExp  := TPDataExp.Date;
  //
  if MyObjects.FIC(Tarefa = 0, EdTarefa, 'Tarefa n�o definida!') then Exit;
  if MyObjects.FIC(DataExp = 0, TPDataExp, 'Data para expira��o n�o definida!') then Exit;
  if MyObjects.FIC(Perfil = 0, EdPerfil, 'Perfil n�o definido!') then Exit;
  //
  if LaTipo.SQLType = stIns then
  begin
    DataHora := DModG.ObtemAgora;
    Codigo   := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpdN, 'wprotocol',
    'Codigo', [], [], stIns, 0, siPositivo, nil);
  end else
  begin
    DataHora := QrWProtocolDataHora.Value;
    Codigo   := QrWProtocolCodigo.Value;
  end;
  //
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpdN, LaTipo.Caption, 'wprotocol', False,
  [
    'Tarefa', 'DataHora', 'DataExp', 'Perfil', 'Cliente', 'Entidade'         
  ], ['Codigo'],
  [
    Tarefa, DataHora, Geral.FDT(DataExp, 1), Perfil, Cliente, Entidade
  ], [Codigo])
  then begin
    LocCod(Codigo, Codigo);
    MostraEdicao(0, stLok, 0);
  end;
end;

procedure TFmWProtocol.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmWProtocol.BtLimparClick(Sender: TObject);
begin
  LimpaCamposPesq;
end;

procedure TFmWProtocol.BtPesqClick(Sender: TObject);
var
  ID, Tarefa, Perfil, Entidade, Cliente, Usuario, Status: Integer;
begin
  ID       := EdPesID.ValueVariant;
  Tarefa   := EdPesTarefa.ValueVariant;
  Perfil   := EdPesPerfil.ValueVariant;
  Entidade := EdPesEntidade.ValueVariant;
  Cliente  := EdPesCliente.ValueVariant;
  Usuario  := EdPesUsuario.ValueVariant;
  Status   := RGPesStatus.ItemIndex;
  //
  QrPesWProtocol.Close;
  QrPesWProtocol.SQL.Clear;
  QrPesWProtocol.SQL.Add('SELECT wpr.*, pro.Nome PROTNOME, per.Nome NOMEPERFIL,');
  QrPesWProtocol.SQL.Add('wus.PersonalName, IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,');
  QrPesWProtocol.SQL.Add('IF (enc.Tipo=0, enc.RazaoSocial, enc.Nome) NOMECLI');
  QrPesWProtocol.SQL.Add('FROM wprotocol wpr');
  QrPesWProtocol.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo = wpr.Entidade');
  QrPesWProtocol.SQL.Add('LEFT JOIN protocolos pro ON pro.Codigo = wpr.Tarefa');
  QrPesWProtocol.SQL.Add('LEFT JOIN wusers wus ON wus.Codigo = wpr.Usuario');
  QrPesWProtocol.SQL.Add('LEFT JOIN wperfis per ON per.Codigo = wpr.Perfil');
  //Tabela clientes � espec�fica do DControl cada aplicativo pode ter a sua
  if UpperCase(Application.Title) = 'DCONTROL' then
  begin
    QrPesWProtocol.SQL.Add('LEFT JOIN clientes cli ON cli.Codigo = wpr.Cliente');
  end else
  begin
    QrPesWProtocol.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo = wpr.Cliente');
  end;
  QrPesWProtocol.SQL.Add('LEFT JOIN entidades enc ON enc.Codigo = cli.Cliente');
  QrPesWProtocol.SQL.Add('WHERE wpr.Codigo <> 0');
  if ID > 0 then
    QrPesWProtocol.SQL.Add('AND wpr.Codigo = ' + FormatFloat('0', ID));
  if Tarefa > 0 then
    QrPesWProtocol.SQL.Add('AND wpr.Tarefa = ' + FormatFloat('0', Tarefa));
  if Perfil > 0 then
    QrPesWProtocol.SQL.Add('AND wpr.Perfil = ' + FormatFloat('0', Perfil));
  if Entidade > 0 then
    QrPesWProtocol.SQL.Add('AND wpr.Entidade = ' + FormatFloat('0', Entidade));
  if Cliente > 0 then
    QrPesWProtocol.SQL.Add('AND wpr.Cliente = ' + FormatFloat('0',  Cliente));
  if Usuario > 0  then
    QrPesWProtocol.SQL.Add('AND wpr.Usuario = ' + FormatFloat('0', Usuario));
  if Status in [0, 1] then
    QrPesWProtocol.SQL.Add('AND wpr.Finaliz = ' + FormatFloat('0', Status));
  QrPesWProtocol.SQL.Add(dmkPF.SQL_Periodo('AND DATE_FORMAT(DataHora, "%Y-%m-%d") ',
    TPPesAbeIni.Date, TPPesAbeFim.Date, CkPesAbeIni.Checked, CkPesAbeFim.Checked));
  QrPesWProtocol.SQL.Add(dmkPF.SQL_Periodo('AND DATE_FORMAT(DataFin, "%Y-%m-%d") ',
    TpPesEncerrIni.Date, TPPesEncerrFim.Date, CkPesEncerrIni.Checked,
    CkPesEncerrFim.Checked));
  QrPesWProtocol.Open;
end;

procedure TFmWProtocol.BtProtocoloClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMProtocolo, BtProtocolo);
end;

procedure TFmWProtocol.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  PainelEdit.Align   := alClient;
  PageControl1.Align := alClient;
  CriaOForm;
  //
  Dmod.QrWControl.Open;
  QrTarefas.Open;
  QrPerfis.Open;
  QrEntidades.Open;
  //
  ReopenClientes;
end;

procedure TFmWProtocol.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.SQLType);
end;

procedure TFmWProtocol.QrPesWProtocolCalcFields(DataSet: TDataSet);
begin
  QrPesWProtocolDATAEXP_TXT.Value     := Geral.FDT(QrPesWProtocolDataExp.Value, 2);
  QrPesWProtocolDATAHORA_TXT.Value    := Geral.FDT(QrPesWProtocolDataHora.Value, 0);
  QrPesWProtocolDATAHORAFIN_TXT.Value := Geral.FDT(QrPesWProtocolDataFin.Value, 0);
  //
  if QrPesWProtocolFinaliz.Value = 0 then
    QrPesWProtocolFINALIX_TXT.Value := 'N�O'
  else
    QrPesWProtocolFINALIX_TXT.Value := 'SIM';
end;

procedure TFmWProtocol.QrWProtocolAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmWProtocol.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmWProtocol.FormResize(Sender: TObject);
begin
  //MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmWProtocol.Inclui1Click(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmWProtocol.QrWProtocolBeforeOpen(DataSet: TDataSet);
begin
  QrWProtocolCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmWProtocol.QrWProtocolCalcFields(DataSet: TDataSet);
begin
  QrWProtocolDATAEXP_TXT.Value     := Geral.FDT(QrWProtocolDataExp.Value, 2);
  QrWProtocolDATAHORA_TXT.Value    := Geral.FDT(QrWProtocolDataHora.Value, 0);
  QrWProtocolDATAHORAFIN_TXT.Value := Geral.FDT(QrWProtocolDataFin.Value, 0);
end;

end.
