unit WLogin;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkEdit,
  dmkGeral, dmkMemo, ShellAPI, dmkImage, Registry, UnDmkEnums;

type
  TFmWLogin = class(TForm)
    PnLogin: TPanel;
    Label1: TLabel;
    EdSenha: TEdit;
    Label2: TLabel;
    LaEsqueciSenha: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    BtOK: TBitBtn;
    LaEsquecer: TLabel;
    CBLogin: TComboBox;
    ImgWEB: TdmkImage;
    ImgTipo: TdmkImage;
    CkLembrar: TCheckBox;
    PnTermos: TPanel;
    LaUsuario: TLabel;
    Label3: TLabel;
    LaTermo: TLabel;
    Label4: TLabel;
    LaPrivacidade: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure LaEsqueciSenhaMouseEnter(Sender: TObject);
    procedure LaEsqueciSenhaMouseLeave(Sender: TObject);
    procedure LaEsqueciSenhaClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LaEsquecerClick(Sender: TObject);
    procedure LaEsquecerMouseEnter(Sender: TObject);
    procedure LaEsquecerMouseLeave(Sender: TObject);
    procedure CBLoginChange(Sender: TObject);
    procedure LaTermoMouseEnter(Sender: TObject);
    procedure LaTermoMouseLeave(Sender: TObject);
    procedure LaPrivacidadeMouseEnter(Sender: TObject);
    procedure LaPrivacidadeMouseLeave(Sender: TObject);
    procedure LaPrivacidadeClick(Sender: TObject);
    procedure LaTermoClick(Sender: TObject);
  private
    { Private declarations }
    FUsuario, FSenha: String;
    procedure AtualizaListaUsuarios();
    procedure MostraEdicao(Termo: Boolean);
  public
    { Public declarations }
  end;

  var
  FmWLogin: TFmWLogin;

implementation

uses UnInternalConsts, {$IfNDef NO_USE_MYSQLMODULE} Module, ModuleGeral,
  DmkDAC_PF, {$EndIf} UnDmkWeb, UnMyObjects, UnGrl_Vars;

{$R *.DFM}

procedure TFmWLogin.AtualizaListaUsuarios();
begin
  CBLogin.Items.Clear;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT DISTINCT Usuario ',
    'FROM emailconta ',
    'WHERE TipoConta=0 ',
    '']);
  if Dmod.QrAux.RecordCount > 0 then
  begin
    Dmod.QrAux.First;
    while not Dmod.QrAux.Eof do
    begin
      CBLogin.Items.Add(Dmod.QrAux.FieldByName('Usuario').AsString);
      //
      Dmod.QrAux.Next;
    end;
  end;
end;

procedure TFmWLogin.BtOKClick(Sender: TObject);
var
  CfgDmk: Integer;
  UsuarioNovo, Usuarios, Usuario, Senha, MsgLogin, Usr, Sen, Msg: String;
begin
  if PnLogin.Visible = True then
  begin
    Usuario := CBLogin.Text;
    Senha   := EdSenha.Text;
    //
    MsgLogin := DmkWeb.LoginUser(Usuario, Senha);
    //
    if VAR_WEB_CONECTADO <> 100 then
    begin
      Geral.MB_Aviso(MsgLogin);
      CBLogin.SetFocus;
    end else
    begin
      {$IfNDef NO_USE_MYSQLMODULE}
      if (CkLembrar.Checked) and (CkLembrar.Visible = True) then
      begin
        DmkWeb.ObtemDadosContaDmk(Dmod.QrAux, Dmod.MyDB, VAR_USUARIO, CfgDmk, Usr, Sen);
        //
        if CfgDmk <> 0 then
          DmkWeb.InsereContaDmk(stUpd, Dmod.QrUpd, Dmod.QrAux, Dmod.MyDB, Usuario, Senha, VAR_USUARIO, CfgDmk)
        else
          DmkWeb.InsereContaDmk(stIns, Dmod.QrUpd, Dmod.QrAux, Dmod.MyDB, Usuario, Senha, VAR_USUARIO, 0);
      end;
      {$EndIf}
      if VAR_WEB_HASHTERMO = '' then
        MostraEdicao(True)
      else
        Close;
    end;
  end else
  begin
    if DmkWeb.Termo_AtualizaHash(31, Msg) then //DmkWeb = 31
    begin
      if DmkWeb.Termo_ConfiguraHashTermo(31, Msg) then //DmkWeb = 31
        Close
      else
        Geral.MB_Aviso(Msg);
    end else
      Geral.MB_Aviso(Msg);
  end;
end;

procedure TFmWLogin.BtSaidaClick(Sender: TObject);
begin
  if PnLogin.Visible then
  begin
    VAR_WEB_CONECTADO := 200;
    VAR_WEB_USR_ID    := 0;
    VAR_WEB_USR_TIPO  := -1;
    //
    Close;
  end else
  begin
    DmkWeb.DesconectarUsuarioWEB;
    Close;
  end;
end;

procedure TFmWLogin.CBLoginChange(Sender: TObject);
begin
  if CBLogin.Text = FUsuario then
  begin
    EdSenha.Text      := FSenha;
    CkLembrar.Checked := True;
  end else
  begin
    EdSenha.Text      := '';
    CkLembrar.Checked := False;
  end;
end;

procedure TFmWLogin.FormActivate(Sender: TObject);
var
  CodigoCfg: Integer;
  Usuario, Senha, Usuarios: String;
begin
  MyObjects.CorIniComponente();
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
  //
  //Colocado no onActivate por causa do SoMai�sculas
  CBLogin.CharCase := ecNormal;
  EdSenha.CharCase := ecNormal;
  //
  {$IfNDef NO_USE_MYSQLMODULE}
    AtualizaListaUsuarios();
    DmkWeb.ObtemDadosContaDmk(Dmod.QrAux, Dmod.MyDB, VAR_USUARIO, CodigoCfg,
      Usuario, Senha);
  {$Else}
    CBLogin.Items.Clear;
    //
    Usuario := '';
    Senha   := '';
  {$EndIf}
  FUsuario := Usuario;
  FSenha   := Senha;
  //
  CBLogin.Text      := FUsuario;
  EdSenha.Text      := FSenha;
  CkLembrar.Checked := True;
end;

procedure TFmWLogin.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  {$IfNDef NO_USE_MYSQLMODULE}
    CkLembrar.Visible  := (Dmod.MyDB.Connected) and (VAR_USUARIO <> 0);
    LaEsquecer.Visible := (Dmod.MyDB.Connected) and (VAR_USUARIO <> 0);
    //
    if (Dmod.MyDB.Connected) and (VAR_USUARIO <> 0) then
    begin
      DModG.ReopenOpcoesGerl;
      //
      LaEsqueciSenha.Visible := True;
    end else
      LaEsqueciSenha.Visible := False;
  {$Else}
    CkLembrar.Visible      := False;
    LaEsquecer.Visible     := False;
    LaEsqueciSenha.Visible := False;
  {$EndIf}
  //
  MostraEdicao(False);
end;

procedure TFmWLogin.FormDestroy(Sender: TObject);
begin
  FmWLogin := nil;
end;

procedure TFmWLogin.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWLogin.FormShow(Sender: TObject);
begin
  CBLogin.SetFocus;
end;

procedure TFmWLogin.LaEsquecerClick(Sender: TObject);
begin
  {$IfNDef NO_USE_MYSQLMODULE}
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      DELETE_FROM + ' emailconta WHERE TipoConta=0 ',
      'AND PermisNiv=' + Geral.FF0(VAR_USUARIO), 
      '']);
    FUsuario     := '';
    FSenha       := '';
    CBLogin.Text := '';
    EdSenha.Text := '';
    //
    if Geral.MB_Pergunta('Deseja esquercer tamb�m todos os usu�rios?') = ID_YES then
    begin
      Geral.WriteAppKeyMULTI_SZ('WebUsers', Application.Title + CO_WEB_REGEDITCAM,  '');
      CBLogin.Items.Clear;
    end;
    //
    CBLogin.SetFocus;
  {$EndIf}
end;

procedure TFmWLogin.LaEsquecerMouseEnter(Sender: TObject);
begin
  LaEsquecer.Font.Color := clBlue;
  LaEsquecer.Font.Style := [fsUnderline];
end;

procedure TFmWLogin.LaEsquecerMouseLeave(Sender: TObject);
begin
  LaEsquecer.Font.Color := clBlue;
  LaEsquecer.Font.Style := [];
end;

procedure TFmWLogin.LaEsqueciSenhaClick(Sender: TObject);
var
  Link: String;
begin
  {$IfNDef NO_USE_MYSQLMODULE}
    //Habilitado apenas para o DControl
    if LaEsqueciSenha.Visible then
      Link := DmkWeb.ObtemLinkLostPass(17, 'http://www.dermatek.net.br', '', CBLogin.Text)
    else
      Link := '';
  {$Else}
    Link := '';
  {$EndIf}
  if Link <> '' then
    ShellExecute(Application.Handle, nil, PChar(Link), nil, nil, sw_hide);
end;

procedure TFmWLogin.LaEsqueciSenhaMouseEnter(Sender: TObject);
begin
  LaEsqueciSenha.Font.Color := clBlue;
  LaEsqueciSenha.Font.Style := [fsUnderline];
end;

procedure TFmWLogin.LaEsqueciSenhaMouseLeave(Sender: TObject);
begin
  LaEsqueciSenha.Font.Color := clBlue;
  LaEsqueciSenha.Font.Style := [];
end;

procedure TFmWLogin.LaPrivacidadeClick(Sender: TObject);
var
  Link: String;
begin
  Link := DmkWeb.ObtemLinkTermo(VAR_WEB_TERMOPRIVA);
  //
  ShellExecute(Application.Handle, nil, PChar(Link), nil, nil, sw_hide);
end;

procedure TFmWLogin.LaPrivacidadeMouseEnter(Sender: TObject);
begin
  LaPrivacidade.Font.Color := clBlue;
  LaPrivacidade.Font.Style := [fsUnderline];
end;

procedure TFmWLogin.LaPrivacidadeMouseLeave(Sender: TObject);
begin
  LaPrivacidade.Font.Color := clBlue;
  LaPrivacidade.Font.Style := [];
end;

procedure TFmWLogin.LaTermoClick(Sender: TObject);
var
  Link: String;
begin
  Link := DmkWeb.ObtemLinkTermo(VAR_WEB_TERMOUSO);
  //
  ShellExecute(Application.Handle, nil, PChar(Link), nil, nil, sw_hide);
end;

procedure TFmWLogin.LaTermoMouseEnter(Sender: TObject);
begin
  LaTermo.Font.Color := clBlue;
  LaTermo.Font.Style := [fsUnderline];
end;

procedure TFmWLogin.LaTermoMouseLeave(Sender: TObject);
begin
  LaTermo.Font.Color := clBlue;
  LaTermo.Font.Style := [];
end;

procedure TFmWLogin.MostraEdicao(Termo: Boolean);
begin
  if not Termo then
  begin
    PnLogin.Visible  := True;
    PnTermos.Visible := False;
  end else
  begin
    LaUsuario.Caption := 'Ol� ' + VAR_WEB_USR_NOME + '!';
    //
    PnLogin.Visible  := False;
    PnTermos.Visible := True;
  end;
end;

end.
