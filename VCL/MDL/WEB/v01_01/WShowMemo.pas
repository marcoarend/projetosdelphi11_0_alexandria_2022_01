unit WShowMemo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBGridZTO, mySQLDbTables;

type
  TFmWShowMemo = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    Splitter1: TSplitter;
    QrPsq: TmySQLQuery;
    DsPsq: TDataSource;
    QrMemo: TmySQLQuery;
    Panel5: TPanel;
    Memo1: TMemo;
    Panel6: TPanel;
    CkQuotes: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrPsqAfterScroll(DataSet: TDataSet);
    procedure CkQuotesClick(Sender: TObject);
  private
    { Private declarations }
    procedure MostraTexto();
  public
    { Public declarations }
    FTabela, FFldCodi, FFldNome, FFldMemo: String;
    //
    procedure ReopenTabela();
  end;

  var
  FmWShowMemo: TFmWShowMemo;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnDmkProcFunc;

{$R *.DFM}

procedure TFmWShowMemo.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWShowMemo.CkQuotesClick(Sender: TObject);
begin
  MostraTexto();
end;

procedure TFmWShowMemo.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmWShowMemo.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmWShowMemo.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWShowMemo.MostraTexto;
var
  Texto: String;
begin
  if QrMemo.State <> dsInactive then
  begin
    Texto := QrMemo.FieldByName(FFldMemo).AsString;
    if CkQuotes.Checked then
      Texto := DmkPF.XMLDecode(Texto);
    Memo1.Text := Texto;
  end;
end;

procedure TFmWShowMemo.QrPsqAfterScroll(DataSet: TDataSet);
var
  Codigo: Integer;
  Texto: String;
begin
  Codigo := QrPsq.FieldByName(FFldCodi).AsInteger;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrMemo, Dmod.MyDBn, [
  'SELECT ' + FFldMemo,
  'FROM ' + FTabela,
  'WHERE ' + FFldCodi + '=' + Geral.FF0(Codigo),
  '']);
  //
  MostraTexto();
end;

procedure TFmWShowMemo.ReopenTabela();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsq, Dmod.MyDBn, [
  'SELECT ' + FFldCodi + ', ' + FFldNome,
  'FROM ' + FTabela,
  'WHERE ' + FFldMemo + ' <> ""',
  '']);
end;

end.
