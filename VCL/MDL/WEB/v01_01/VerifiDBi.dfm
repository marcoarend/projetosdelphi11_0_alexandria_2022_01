object FmVerifiDBi: TFmVerifiDBi
  Left = 382
  Top = 202
  Caption = 'FER-VRFBD-004 :: Verifica'#231#227'o de Banco de Dados WEB'
  ClientHeight = 577
  ClientWidth = 690
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnHide = FormHide
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 47
    Width = 690
    Height = 418
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object ProgressBar1: TProgressBar
      Left = 0
      Top = 402
      Width = 690
      Height = 16
      Align = alBottom
      TabOrder = 0
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 690
      Height = 59
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 690
        Height = 59
        Align = alClient
        Caption = ' Verifica'#231#245'es: '
        TabOrder = 0
        object CkEstrutura: TCheckBox
          Left = 170
          Top = 16
          Width = 158
          Height = 17
          Caption = 'Estrutura do banco de dados.'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object CkPergunta: TCheckBox
          Left = 12
          Top = 16
          Width = 153
          Height = 17
          Caption = 'Pergunta antes de executar.'
          TabOrder = 1
        end
        object CkEstrutLoc: TCheckBox
          Left = 331
          Top = 16
          Width = 192
          Height = 17
          Caption = 'Estrutura do banco de dados local.'
          TabOrder = 2
        end
        object CkRegObrigat: TCheckBox
          Left = 528
          Top = 16
          Width = 154
          Height = 17
          Caption = 'Recria registros obrigat'#243'rios.'
          Enabled = False
          TabOrder = 3
        end
        object CkEntidade0: TCheckBox
          Left = 12
          Top = 35
          Width = 153
          Height = 17
          Caption = 'Exclui entidade zero.'
          TabOrder = 4
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 162
      Width = 458
      Height = 240
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = 'Avisos'
        object Memo1: TMemo
          Left = 0
          Top = 0
          Width = 451
          Height = 215
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -14
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
        end
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 59
      Width = 690
      Height = 103
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object LaAvisoP1: TLabel
        Left = 13
        Top = 78
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAvisoP2: TLabel
        Left = 12
        Top = 78
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object GBAvisos1: TGroupBox
        Left = 0
        Top = 0
        Width = 690
        Height = 72
        Align = alTop
        Caption = ' Avisos: '
        TabOrder = 0
        object Panel13: TPanel
          Left = 2
          Top = 14
          Width = 686
          Height = 56
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 562
            Height = 56
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object LaAvisoB1: TLabel
              Left = 13
              Top = 18
              Width = 120
              Height = 16
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clSilver
              Font.Height = -14
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAvisoB2: TLabel
              Left = 12
              Top = 17
              Width = 120
              Height = 16
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -14
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAvisoG1: TLabel
              Left = 13
              Top = 34
              Width = 120
              Height = 16
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clSilver
              Font.Height = -14
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAvisoG2: TLabel
              Left = 12
              Top = 33
              Width = 120
              Height = 16
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGreen
              Font.Height = -14
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAvisoR1: TLabel
              Left = 13
              Top = 2
              Width = 120
              Height = 16
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clSilver
              Font.Height = -14
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object LaAvisoR2: TLabel
              Left = 12
              Top = 1
              Width = 120
              Height = 16
              Caption = '..............................'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -14
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
          end
          object Panel14: TPanel
            Left = 673
            Top = 0
            Width = 13
            Height = 56
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
          end
          object Panel6: TPanel
            Left = 606
            Top = 0
            Width = 67
            Height = 56
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 2
            object LaTempoI: TLabel
              Left = 0
              Top = 0
              Width = 67
              Height = 14
              Align = alTop
              Alignment = taRightJustify
              AutoSize = False
              Caption = '...'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object LaTempoF: TLabel
              Left = 0
              Top = 14
              Width = 67
              Height = 13
              Align = alTop
              Alignment = taRightJustify
              AutoSize = False
              Caption = '...'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clGreen
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object LaTempoT: TLabel
              Left = 0
              Top = 27
              Width = 67
              Height = 15
              Align = alTop
              Alignment = taRightJustify
              AutoSize = False
              Caption = '...'
              Color = clBtnFace
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = [fsBold]
              ParentColor = False
              ParentFont = False
              Transparent = True
            end
          end
          object Panel8: TPanel
            Left = 562
            Top = 0
            Width = 44
            Height = 56
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 3
            object Label4: TLabel
              Left = 0
              Top = 0
              Width = 43
              Height = 14
              Align = alTop
              AutoSize = False
              Caption = 'In'#237'cio:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
            end
            object Label5: TLabel
              Left = 0
              Top = 14
              Width = 43
              Height = 13
              Align = alTop
              AutoSize = False
              Caption = 'T'#233'rmino:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
            end
            object Label6: TLabel
              Left = 0
              Top = 27
              Width = 43
              Height = 15
              Align = alTop
              AutoSize = False
              Caption = 'Tempo:'
              Color = clBtnFace
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Arial'
              Font.Style = []
              ParentColor = False
              ParentFont = False
              Transparent = True
            end
          end
        end
      end
    end
    object PageControl2: TPageControl
      Left = 458
      Top = 162
      Width = 232
      Height = 240
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      ActivePage = TabSheet2
      Align = alRight
      TabOrder = 4
      object TabSheet2: TTabSheet
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = 'Tabelas desnecess'#225'rias'
        object Panel10: TPanel
          Left = 0
          Top = 179
          Width = 226
          Height = 36
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 0
          object BtTabsNoNeed: TButton
            Left = 6
            Top = 6
            Width = 24
            Height = 24
            Caption = '...'
            TabOrder = 0
            OnClick = BtTabsNoNeedClick
          end
        end
        object ClTabsNoNeed: TCheckListBox
          Left = 0
          Top = 0
          Width = 226
          Height = 179
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ItemHeight = 13
          ParentFont = False
          TabOrder = 1
        end
      end
      object TabSheet3: TTabSheet
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = 'Campos desnecess'#225'rios'
        ImageIndex = 1
        object ClFldsNoNeed: TCheckListBox
          Left = 0
          Top = 0
          Width = 226
          Height = 178
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ItemHeight = 13
          ParentFont = False
          TabOrder = 0
        end
        object Panel9: TPanel
          Left = 0
          Top = 175
          Width = 224
          Height = 37
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitTop = 178
          ExplicitWidth = 226
          object BtClFldsNoNeed: TButton
            Left = 6
            Top = 6
            Width = 24
            Height = 24
            Caption = '...'
            TabOrder = 0
            OnClick = BtClFldsNoNeedClick
          end
        end
      end
      object TabSheet4: TTabSheet
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = #205'ndices desnecess'#225'rios'
        ImageIndex = 2
        object ClIdxsNoNeed: TCheckListBox
          Left = 0
          Top = 0
          Width = 226
          Height = 179
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ItemHeight = 13
          ParentFont = False
          TabOrder = 0
        end
        object Panel17: TPanel
          Left = 0
          Top = 176
          Width = 224
          Height = 36
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitTop = 179
          ExplicitWidth = 226
          object BtClIdxsNoNeed: TButton
            Left = 6
            Top = 6
            Width = 24
            Height = 24
            Caption = '...'
            TabOrder = 0
            OnClick = BtClIdxsNoNeedClick
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 690
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 642
      Top = 0
      Width = 48
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 595
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 426
        Height = 31
        Caption = 'Verifica'#231#227'o de Banco de Dados WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 426
        Height = 31
        Caption = 'Verifica'#231#227'o de Banco de Dados WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 426
        Height = 31
        Caption = 'Verifica'#231#227'o de Banco de Dados WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 465
    Width = 690
    Height = 43
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel1: TPanel
      Left = 2
      Top = 14
      Width = 686
      Height = 28
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 508
    Width = 690
    Height = 69
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 546
      Top = 14
      Width = 142
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSair: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSairClick
      end
    end
    object Panel15: TPanel
      Left = 2
      Top = 14
      Width = 544
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 125
        Left = 10
        Top = 3
        Width = 110
        Height = 39
        Cursor = crHandPoint
        Caption = '&Atualiza'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 140
    Top = 120
  end
  object PMTabsNoNeed: TPopupMenu
    OnPopup = PMTabsNoNeedPopup
    Left = 544
    Top = 248
    object Excluitabelas1: TMenuItem
      Caption = '&Exclui tabela(s)'
      OnClick = Excluitabelas1Click
    end
    object Listatabelas1: TMenuItem
      Caption = '&Lista tabela(s)'
      OnClick = Listatabelas1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Marcartodos2: TMenuItem
      Caption = '&Marcar todos'
      OnClick = Marcartodos2Click
    end
    object Desmarcartodos2: TMenuItem
      Caption = '&Desmarcar todos'
      OnClick = Desmarcartodos2Click
    end
  end
  object PMClFldsNoNeed: TPopupMenu
    OnPopup = PMClFldsNoNeedPopup
    Left = 528
    Top = 376
    object Excluicampos1: TMenuItem
      Caption = '&Exclui campo(s)'
      OnClick = Excluicampos1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Marcartodos1: TMenuItem
      Caption = '&Marcar todos'
      OnClick = Marcartodos1Click
    end
    object Desmarcartodos1: TMenuItem
      Caption = '&Desmarcar todos'
      OnClick = Desmarcartodos1Click
    end
  end
  object PMClIdxsNoNeed: TPopupMenu
    OnPopup = PMClIdxsNoNeedPopup
    Left = 540
    Top = 528
    object ExcluirIndices1: TMenuItem
      Caption = '&Exclui '#237'ndice(s)'
      OnClick = ExcluirIndices1Click
    end
    object MenuItem2: TMenuItem
      Caption = '-'
    end
    object MarcarTodos3: TMenuItem
      Caption = '&Marcar todos'
      OnClick = MarcarTodos3Click
    end
    object DesmarcarTodos3: TMenuItem
      Caption = '&Desmarcar todos'
      OnClick = DesmarcarTodos3Click
    end
  end
end
