object FmWLogin: TFmWLogin
  Left = 339
  Top = 185
  Caption = 'WEB-LOGIN-001 :: Login WEB'
  ClientHeight = 317
  ClientWidth = 331
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnTermos: TPanel
    Left = 0
    Top = 48
    Width = 331
    Height = 155
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 4
    object LaUsuario: TLabel
      Left = 10
      Top = 10
      Width = 95
      Height = 20
      Caption = 'Ol'#225' usu'#225'rio X'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 10
      Top = 40
      Width = 311
      Height = 20
      AutoSize = False
      Caption = 'Ao clicar no bot'#227'o Aceitar voc'#234' concorda com os'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object LaTermo: TLabel
      Left = 10
      Top = 70
      Width = 91
      Height = 16
      Cursor = crHandPoint
      Caption = 'Termos de uso'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = LaTermoClick
      OnMouseEnter = LaTermoMouseEnter
      OnMouseLeave = LaTermoMouseLeave
    end
    object Label4: TLabel
      Left = 10
      Top = 100
      Width = 75
      Height = 20
      AutoSize = False
      Caption = 'e nossa'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object LaPrivacidade: TLabel
      Left = 10
      Top = 130
      Width = 139
      Height = 16
      Cursor = crHandPoint
      Caption = 'Politica de Privacidade'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = LaPrivacidadeClick
      OnMouseEnter = LaPrivacidadeMouseEnter
      OnMouseLeave = LaPrivacidadeMouseLeave
    end
  end
  object PnLogin: TPanel
    Left = 0
    Top = 48
    Width = 331
    Height = 155
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 10
      Top = 8
      Width = 29
      Height = 13
      Caption = 'Login:'
    end
    object Label2: TLabel
      Left = 10
      Top = 64
      Width = 34
      Height = 13
      Caption = 'Senha:'
    end
    object LaEsqueciSenha: TLabel
      Left = 220
      Top = 112
      Width = 101
      Height = 13
      Cursor = crHandPoint
      Alignment = taRightJustify
      Caption = 'Esqueci minha senha'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = LaEsqueciSenhaClick
      OnMouseEnter = LaEsqueciSenhaMouseEnter
      OnMouseLeave = LaEsqueciSenhaMouseLeave
    end
    object LaEsquecer: TLabel
      Left = 258
      Top = 130
      Width = 63
      Height = 13
      Cursor = crHandPoint
      Alignment = taRightJustify
      Caption = 'Limpar senha'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      OnClick = LaEsquecerClick
      OnMouseEnter = LaEsquecerMouseEnter
      OnMouseLeave = LaEsquecerMouseLeave
    end
    object EdSenha: TEdit
      Left = 10
      Top = 80
      Width = 311
      Height = 21
      AutoSize = False
      Font.Charset = SYMBOL_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Wingdings'
      Font.Style = []
      ParentFont = False
      PasswordChar = 'l'
      TabOrder = 1
    end
    object CBLogin: TComboBox
      Left = 10
      Top = 25
      Width = 311
      Height = 24
      AutoDropDown = True
      TabOrder = 0
      OnChange = CBLoginChange
    end
    object CkLembrar: TCheckBox
      Left = 10
      Top = 108
      Width = 130
      Height = 17
      Caption = 'Lembrar minha senha'
      TabOrder = 2
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 331
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 243
      Top = 0
      Width = 88
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgWEB: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
      object ImgTipo: TdmkImage
        Left = 48
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 195
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 138
        Height = 32
        Caption = 'Login WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 138
        Height = 32
        Caption = 'Login WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 138
        Height = 32
        Caption = 'Login WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 203
    Width = 331
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 327
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 247
    Width = 331
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 197
      Top = 15
      Width = 132
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 6
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 195
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
end
