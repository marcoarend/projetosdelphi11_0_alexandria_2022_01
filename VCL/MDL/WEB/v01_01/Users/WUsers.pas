unit WUsers;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkCheckBox, dmkDBLookupComboBox, dmkEditCB,
  DmkDAC_PF, Variants, ShellAPI;

type
  TFmWUsers = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrWUsers: TmySQLQuery;
    QrWUsersCodigo: TAutoIncField;
    QrWUsersUsername: TWideStringField;
    QrWUsersPassword: TWideStringField;
    QrWUsersPersonalName: TWideStringField;
    QrWUsersLoginID: TWideStringField;
    QrWUsersEntidade: TIntegerField;
    QrWUsersTipo: TSmallintField;
    QrWUsersLk: TIntegerField;
    QrWUsersDataCad: TDateField;
    QrWUsersDataAlt: TDateField;
    QrWUsersUserCad: TIntegerField;
    QrWUsersUserAlt: TIntegerField;
    QrWUsersAlterWeb: TSmallintField;
    QrWUsersAtivo: TSmallintField;
    QrWUsersSENHA: TWideStringField;
    QrWUsersEmail: TWideStringField;
    QrWUsersPerfil: TIntegerField;
    QrWUsersCliente: TIntegerField;
    QrWUsersNOMEPEF: TWideStringField;
    QrWUsersCLINOME: TWideStringField;
    QrWUsersENTNOME: TWideStringField;
    DsWUsers: TDataSource;
    QrWControl: TmySQLQuery;
    QrWControlCrypt: TWideStringField;
    DsClientes: TDataSource;
    DsEntidades: TDataSource;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNome: TWideStringField;
    QrEntidadesCliente: TIntegerField;
    DsWPerfis: TDataSource;
    QrWPerfis: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    QrWPerfisTipo: TSmallintField;
    QrLoc: TmySQLQuery;
    Label1: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    DBRgTipo: TDBRadioGroup;
    Label2: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    Label11: TLabel;
    dmkDBEdit4: TdmkDBEdit;
    dmkDBEdit5: TdmkDBEdit;
    Label12: TLabel;
    DBCheckBox1: TDBCheckBox;
    dmkDBEdit6: TdmkDBEdit;
    Label13: TLabel;
    dmkDBEdit3: TdmkDBEdit;
    Label8: TLabel;
    dmkDBEdit7: TdmkDBEdit;
    Label15: TLabel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    RgTipo: TdmkRadioGroup;
    EdPerfil: TdmkEditCB;
    LaPerfil: TLabel;
    CBPerfil: TdmkDBLookupComboBox;
    EdNome: TdmkEdit;
    Label10: TLabel;
    CBEntidade: TdmkDBLookupComboBox;
    EdEntidade: TdmkEditCB;
    LaEntidade: TLabel;
    Label3: TLabel;
    EdLogin: TdmkEdit;
    EdSenha1: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    EdSenha2: TdmkEdit;
    EdEmail: TdmkEdit;
    Label6: TLabel;
    CkAtivo: TdmkCheckBox;
    CkContinuar: TCheckBox;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    ImgWEB: TdmkImage;
    LaEsqueciSenha: TLabel;
    QrEntidadesEmail: TWideStringField;
    QrWUsersDataCad_TXT: TWideStringField;
    QrWUsersDataAlt_TXT: TWideStringField;
    Label7: TLabel;
    dmkDBEdit8: TdmkDBEdit;
    dmkDBEdit9: TdmkDBEdit;
    Label14: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrWUsersAfterOpen(DataSet: TDataSet);
    procedure QrWUsersBeforeOpen(DataSet: TDataSet);
    procedure RgTipoClick(Sender: TObject);
    procedure EdLoginExit(Sender: TObject);
    procedure EdSenha2Exit(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure LaEsqueciSenhaMouseEnter(Sender: TObject);
    procedure LaEsqueciSenhaMouseLeave(Sender: TObject);
    procedure LaEsqueciSenhaClick(Sender: TObject);
    procedure EdEmailKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdLoginKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    procedure QueryPrincipalAfterOpen;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraEdicao(Mostra: Boolean; Status: TSQLType; Codigo: Integer);
    procedure ReopenWPerfis(Tipo: Integer);
    procedure ReopenEntidades(Tipo: Integer);
    procedure MostraWUsersPesq();
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmWUsers: TFmWUsers;

const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, MyListas, WUsersPesq, UnDmkWeb,
  ModuleGeral, UnWUsers, UnGrl_Consts;

{$R *.DFM}

/// //////////////////////////////////////////////////////////////////////////////////
procedure TFmWUsers.LaEsqueciSenhaClick(Sender: TObject);
var
  Link: String;
begin
  Link := DmkWeb.ObtemLinkLostPass(CO_DMKID_APP,
    DModG.QrOpcoesGerl.FieldByName('Web_MyURL').AsString,
    DModG.QrOpcoesGerl.FieldByName('WebId').AsString, QrWUsersUsername.Value);
  //
  ShellExecute(Application.Handle, nil, PChar(Link), nil, nil, sw_hide);
end;

procedure TFmWUsers.LaEsqueciSenhaMouseEnter(Sender: TObject);
begin
  LaEsqueciSenha.Font.Color := clBlue;
  LaEsqueciSenha.Font.Style := [fsUnderline];
end;

procedure TFmWUsers.LaEsqueciSenhaMouseLeave(Sender: TObject);
begin
  LaEsqueciSenha.Font.Color := clBlue;
  LaEsqueciSenha.Font.Style := [];
end;

procedure TFmWUsers.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmWUsers.MostraEdicao(Mostra: Boolean; Status: TSQLType;
  Codigo: Integer);
begin
  if Mostra then
  begin
    PnDados.Visible := False;
    PnEdita.Visible := True;
    //
    if Status = stIns then
    begin
      EdCodigo.ValueVariant   := FormatFloat('000', Codigo);
      RgTipo.ItemIndex        := -1;
      EdPerfil.ValueVariant   := 0;
      CBPerfil.KeyValue       := Null;
      EdEntidade.ValueVariant := 0;
      CBEntidade.KeyValue     := Null;
      EdEmail.ValueVariant    := '';
      EdNome.ValueVariant     := '';
      EdLogin.ValueVariant    := '';
      EdSenha1.ValueVariant   := '';
      EdSenha2.ValueVariant   := '';
      CkAtivo.Checked         := True;
      CkContinuar.Checked     := True;
      CkContinuar.Visible     := True;
    end
    else
    begin
      EdCodigo.ValueVariant   := QrWUsersCodigo.Value;
      RgTipo.ItemIndex        := QrWUsersTipo.Value;
      EdPerfil.ValueVariant   := QrWUsersPerfil.Value;
      CBPerfil.KeyValue       := QrWUsersPerfil.Value;
      EdEntidade.ValueVariant := QrWUsersEntidade.Value;
      CBEntidade.KeyValue     := QrWUsersEntidade.Value;
      EdEmail.ValueVariant    := QrWUsersEmail.Value;
      EdNome.ValueVariant     := QrWUsersPersonalName.Value;
      EdLogin.ValueVariant    := QrWUsersUsername.Value;
      EdSenha1.Text           := QrWUsersSENHA.Value;
      EdSenha2.Text           := QrWUsersSENHA.Value;
      CkAtivo.Checked         := Geral.IntToBool(QrWUsersAtivo.Value);
      CkContinuar.Checked     := False;
      CkContinuar.Visible     := False;
    end;
    RgTipo.SetFocus;
  end
  else
  begin
    PnDados.Visible := True;
    PnEdita.Visible := False;
  end;
  ImgTipo.SQLType := Status;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmWUsers.MostraWUsersPesq;
begin
  if DBCheck.CriaFm(TFmWUsersPesq, FmWUsersPesq, afmoNegarComAviso) then
  begin
    Close;
    //
    FmWUsersPesq.ShowModal;
    FmWUsersPesq.Destroy;
  end;
end;

procedure TFmWUsers.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrWUsersCodigo.Value,
    LaRegistro.Caption[2]);
end;
/// //////////////////////////////////////////////////////////////////////////////////

procedure TFmWUsers.DefParams;
var
  Crypt: String;
begin
  Crypt := QrWControlCrypt.Value + CO_RandStrWeb01;
  //
  VAR_GOTOTABELA := 'wusers';
  VAR_GOTOMYSQLTABLE := QrWUsers;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDBn;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT wus.*, wpe.Nome NOMEPEF,');
  VAR_SQLx.Add
    ('IF(wus.DataCad <= "1899-12-30", "", DATE_FORMAT(wus.DataCad, "%d/%m/%Y")) DataCad_TXT,');
  VAR_SQLx.Add('IF(wus.DataAlt <= "1899-12-30", "", DATE_FORMAT(wus.DataAlt, "%d/%m/%Y")) DataAlt_TXT,');
  VAR_SQLx.Add('AES_DECRYPT(Password, "' + Crypt + '") SENHA,');
  VAR_SQLx.Add('IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) CLINOME,');
  VAR_SQLx.Add
    ('IF(wus.Entidade = 0, "", IF (enb.Tipo=0, enb.RazaoSocial, enb.Nome)) ENTNOME');
  VAR_SQLx.Add('FROM wusers wus');
  VAR_SQLx.Add('LEFT JOIN wperfis wpe ON wpe.Codigo = wus.Perfil');
  // Tabela clientes � espec�fica do DControl cada aplicativo pode ter a sua
  if CO_DMKID_APP = 17 then // DControl
  begin
    VAR_SQLx.Add('LEFT JOIN clientes cli ON cli.Codigo = wus.Cliente');
    VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo = cli.Cliente');
  end
  else
  begin
    VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo = wus.Cliente');
    VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo = cli.Codigo');
  end;
  VAR_SQLx.Add('LEFT JOIN entidades enb ON enb.Codigo = wus.Entidade');
  VAR_SQLx.Add('WHERE wus.Codigo > 0');
  //
  VAR_SQL1.Add('AND wus.Codigo=:P0');
  //
  VAR_SQLa.Add('AND wus.Nome Like :P0');
  //
end;

procedure TFmWUsers.EdEmailKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdEmail.ValueVariant := QrEntidadesEmail.Value;
end;

procedure TFmWUsers.EdLoginExit(Sender: TObject);
var
  Codigo: Integer;
begin
  if ImgTipo.SQLType = stIns then
    Codigo := 0
  else
    Codigo := QrWUsersCodigo.Value;
  //
  if (EdLogin.ValueVariant <> '') then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn,
      ['SELECT Username ', 'FROM wusers ', 'WHERE Username LIKE "' +
      EdLogin.ValueVariant + '"', 'AND Codigo <> ' + Geral.FF0(Codigo), '']);
    if QrLoc.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Este usu�rio j� foi cadastrado!');
      EdLogin.SetFocus;
      Exit;
    end;
  end;
end;

procedure TFmWUsers.EdLoginKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    EdLogin.ValueVariant := QrEntidadesEmail.Value;
end;

procedure TFmWUsers.EdSenha2Exit(Sender: TObject);
begin
  if (EdSenha1.ValueVariant <> EdSenha2.ValueVariant) then
  begin
    Geral.MB_Aviso
      ('O campo Senha e o campo Confirmar senha devem conter o mesmo texto!');
    //
    EdSenha1.ValueVariant := '';
    EdSenha2.ValueVariant := '';
    EdSenha1.SetFocus;
    Exit;
  end;
end;

procedure TFmWUsers.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmWUsers.DefineONomeDoForm;
begin
end;

/// ////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmWUsers.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmWUsers.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmWUsers.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmWUsers.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmWUsers.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWUsers.BtAlteraClick(Sender: TObject);
begin
  if (QrWUsers.State <> dsInactive) and (QrWUsers.RecordCount > 0) then
  begin
    MostraEdicao(True, stUpd, 0);
  end;
end;

procedure TFmWUsers.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrWUsersCodigo.Value;
  Close;
end;

procedure TFmWUsers.BtConfirmaClick(Sender: TObject);
var
  Crypt, UserName, Password, PassConf, Email, PersName: String;
  Codigo, NewCodigo, Entidade, Cliente, Perfil, Tipo, Ativo: Integer;
begin
  NewCodigo := 0;
  Crypt := QrWControlCrypt.Value;
  Entidade := EdEntidade.ValueVariant;
  UserName := EdLogin.ValueVariant;
  Password := EdSenha1.ValueVariant;
  PassConf := EdSenha2.ValueVariant;
  Email := EdEmail.ValueVariant;
  PersName := EdNome.ValueVariant;
  Perfil := EdPerfil.ValueVariant;
  Tipo := RgTipo.ItemIndex;
  Ativo := Geral.BoolToInt(CkAtivo.Checked);
  //
  if Tipo in [0, 1] then
    if not DBCheck.LiberaPelaSenhaBoss then
      Exit;
  //
  if MyObjects.FIC(Entidade = 0, EdEntidade, 'Entidade n�o definida!') then
    Exit;
  if MyObjects.FIC(Length(PersName) = 0, EdNome, 'Nome n�o definido!') then
    Exit;
  if MyObjects.FIC(Length(Email) = 0, EdEmail, 'E-mail n�o definido!') then
    Exit;
  //
  if Tipo in [0, 1] then
    Cliente := 0
  else
  begin
    Cliente := QrEntidadesCliente.Value;
    if MyObjects.FIC(Cliente = 0, nil, 'Cliente n�o definido!') then
      Exit;
  end;
  //
  if ImgTipo.SQLType = stIns then
    Codigo := 0
  else
    Codigo := QrWUsersCodigo.Value;
  //
  NewCodigo := UWUsers.InsereWUsers(ImgTipo.SQLType, Dmod.QrUpdN, QrLoc,
    Dmod.MyDBn, Codigo, Cliente, Entidade, Perfil, Tipo, Ativo, Crypt, UserName,
    Password, PassConf, Email, PersName);
  //
  if NewCodigo <> 0 then
  begin
    LocCod(NewCodigo, NewCodigo);
    //
    if CkContinuar.Checked then
    begin
      Geral.MensagemBox('Senha cadastrada com sucesso!', 'Aviso',
        MB_OK + MB_ICONWARNING);
      MostraEdicao(True, stIns, 0);
    end
    else
    begin
      LocCod(Codigo, Codigo);
      MostraEdicao(False, stLok, 0);
    end;
  end
  else
    Geral.MB_Aviso('Falha ao incluir usu�rio!');
end;

procedure TFmWUsers.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(False, stLok, 0);
end;

procedure TFmWUsers.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(True, stIns, 0);
end;

procedure TFmWUsers.FormCreate(Sender: TObject);
var
  Compo: TComponent;
  Query: TmySQLQuery;
  I: Integer;
begin
  for I := 0 to FmWUsers.ComponentCount - 1 do
  begin
    Compo := FmWUsers.Components[I];
    if Compo is TmySQLQuery then
    begin
      Query := TmySQLQuery(Compo);
      Query.Database := Dmod.MyDBn;
    end;
  end;
  DModG.ReopenOpcoesGerl;
  UMyMod.AbreQuery(QrWControl, Dmod.MyDBn);
  //
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmWUsers.FormDestroy(Sender: TObject);
begin
  FmWUsers := nil;
end;

procedure TFmWUsers.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrWUsersCodigo.Value, LaRegistro.Caption);
end;

procedure TFmWUsers.SbImprimeClick(Sender: TObject);
begin
  MostraWUsersPesq();
end;

procedure TFmWUsers.SbNomeClick(Sender: TObject);
begin
  MostraWUsersPesq();
  // LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmWUsers.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  // LaRegistro.Caption := GOTOy.CodUsu(QrCadastro_SimplesCodigo.Value, LaRegistro.Caption);
end;

procedure TFmWUsers.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmWUsers.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmWUsers.SbQueryClick(Sender: TObject);
begin
  MostraWUsersPesq();
  // LocCod(QrWUsersCodigo.Value, CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'wusers', Dmod.MyDBn, CO_VAZIO));
end;

procedure TFmWUsers.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWUsers.FormShow(Sender: TObject);
begin
  RgTipo.Items.Clear;
  RgTipo.Items.AddStrings(DmkWeb.ConfiguraNiveis(CO_DMKID_APP, False));
  //
  DBRgTipo.Items.Clear;
  DBRgTipo.Items.AddStrings(DmkWeb.ConfiguraNiveis(CO_DMKID_APP, False));
end;

procedure TFmWUsers.QrWUsersAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmWUsers.QrWUsersBeforeOpen(DataSet: TDataSet);
begin
  QrWUsersCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmWUsers.QueryPrincipalAfterOpen;
begin
end;

procedure TFmWUsers.ReopenEntidades(Tipo: Integer);
begin
  if (CO_DMKID_APP = 17) and (Tipo in [2, 3]) then // DControl
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEntidades, Dmod.MyDB,
      ['SELECT cli.Codigo Cliente, ent.Codigo, ',
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome, ',
      'IF (ent.Tipo=0, ent.EEmail, PEmail) Email ', 'FROM clientes cli ',
      'LEFT JOIN entidades ent ON ent.Codigo = cli.Cliente ',
      'ORDER BY Nome ', '']);
  end
  else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEntidades, Dmod.MyDB,
      ['SELECT ent.Codigo Cliente, ent.Codigo, ',
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome, ',
      'IF (ent.Tipo=0, ent.EEmail, PEmail) Email ', 'FROM  entidades ent ',
      'ORDER BY Nome ', '']);
  end;
end;

procedure TFmWUsers.ReopenWPerfis(Tipo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWPerfis, Dmod.MyDBn,
    ['SELECT Codigo, Nome, Tipo ', 'FROM wperfis ',
    'WHERE Tipo=' + Geral.FF0(Tipo), 'ORDER BY Nome ', '']);
end;

procedure TFmWUsers.RgTipoClick(Sender: TObject);
var
  Tipo: Integer;
  Enab1: Boolean;
begin
  case RgTipo.ItemIndex of
    0:
      ReopenWPerfis(0);
    1:
      ReopenWPerfis(1);
    2:
      ReopenWPerfis(2);
    3:
      ReopenWPerfis(3);
  end;
  Tipo := RgTipo.ItemIndex;
  //
  ReopenEntidades(Tipo);
  //
  Enab1 := Tipo in [0, 1, 2, 3];
  //
  LaEntidade.Visible := Enab1;
  EdEntidade.Visible := Enab1;
  CBEntidade.Visible := Enab1;
  //
  BtConfirma.Enabled := True;
end;

end.
