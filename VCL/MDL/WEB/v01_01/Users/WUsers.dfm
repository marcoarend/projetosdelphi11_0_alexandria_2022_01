object FmWUsers: TFmWUsers
  Left = 368
  Top = 194
  Caption = 'WEB-USERS-001 :: Cadastro de usu'#225'rios WEB'
  ClientHeight = 418
  ClientWidth = 891
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 891
    Height = 322
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 891
      Height = 230
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label9: TLabel
        Left = 17
        Top = 10
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object LaPerfil: TLabel
        Left = 451
        Top = 52
        Width = 26
        Height = 13
        Caption = 'Perfil:'
      end
      object Label10: TLabel
        Left = 450
        Top = 93
        Width = 97
        Height = 13
        Caption = 'Nome para exibi'#231#227'o:'
      end
      object LaEntidade: TLabel
        Left = 17
        Top = 94
        Width = 45
        Height = 13
        Caption = 'Entidade:'
      end
      object Label3: TLabel
        Left = 17
        Top = 137
        Width = 60
        Height = 13
        Caption = 'Usu'#225'rio: [F4]'
      end
      object Label4: TLabel
        Left = 234
        Top = 137
        Width = 34
        Height = 13
        Caption = 'Senha:'
      end
      object Label5: TLabel
        Left = 451
        Top = 137
        Width = 79
        Height = 13
        Caption = 'Confirmar senha:'
      end
      object Label6: TLabel
        Left = 18
        Top = 181
        Width = 52
        Height = 13
        Caption = 'E-mail: [F4]'
      end
      object EdCodigo: TdmkEdit
        Left = 17
        Top = 25
        Width = 71
        Height = 21
        Alignment = taRightJustify
        Color = clInactiveCaption
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBackground
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object RgTipo: TdmkRadioGroup
        Left = 17
        Top = 52
        Width = 427
        Height = 37
        Caption = 'Tipo de perfil'
        Columns = 4
        Items.Strings = (
          'N'#237'vel 0'
          'N'#237'vel 1'
          'N'#237'vel 2'
          'N'#237'vel 3')
        TabOrder = 1
        OnClick = RgTipoClick
        UpdType = utYes
        OldValor = 0
      end
      object EdPerfil: TdmkEditCB
        Left = 451
        Top = 68
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPerfil
        IgnoraDBLookupComboBox = False
      end
      object CBPerfil: TdmkDBLookupComboBox
        Left = 533
        Top = 68
        Width = 345
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsWPerfis
        TabOrder = 3
        dmkEditCB = EdPerfil
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdNome: TdmkEdit
        Left = 450
        Top = 110
        Width = 428
        Height = 21
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CBEntidade: TdmkDBLookupComboBox
        Left = 99
        Top = 110
        Width = 345
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsEntidades
        TabOrder = 5
        dmkEditCB = EdEntidade
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdEntidade: TdmkEditCB
        Left = 17
        Top = 110
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEntidade
        IgnoraDBLookupComboBox = False
      end
      object EdLogin: TdmkEdit
        Left = 17
        Top = 154
        Width = 210
        Height = 21
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = True
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnExit = EdLoginExit
        OnKeyDown = EdLoginKeyDown
      end
      object EdSenha1: TdmkEdit
        Left = 233
        Top = 154
        Width = 210
        Height = 21
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = True
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdSenha2: TdmkEdit
        Left = 451
        Top = 154
        Width = 210
        Height = 21
        TabOrder = 9
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = True
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnExit = EdSenha2Exit
      end
      object EdEmail: TdmkEdit
        Left = 17
        Top = 197
        Width = 427
        Height = 21
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = True
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnKeyDown = EdEmailKeyDown
      end
      object CkAtivo: TdmkCheckBox
        Left = 451
        Top = 199
        Width = 45
        Height = 17
        Caption = 'Ativo'
        TabOrder = 11
        QryCampo = 'Ativo'
        UpdCampo = 'Ativo'
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
      object CkContinuar: TCheckBox
        Left = 506
        Top = 199
        Width = 121
        Height = 17
        Caption = 'Continuar inserindo.'
        TabOrder = 12
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 259
      Width = 891
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 751
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 891
    Height = 322
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 891
      Height = 205
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 5
        Top = 6
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label2: TLabel
        Left = 5
        Top = 93
        Width = 45
        Height = 13
        Caption = 'Entidade:'
      end
      object Label11: TLabel
        Left = 5
        Top = 136
        Width = 39
        Height = 13
        Caption = 'Usu'#225'rio:'
      end
      object Label12: TLabel
        Left = 227
        Top = 136
        Width = 34
        Height = 13
        Caption = 'Senha:'
      end
      object Label13: TLabel
        Left = 449
        Top = 136
        Width = 28
        Height = 13
        Caption = 'Email:'
      end
      object Label8: TLabel
        Left = 449
        Top = 93
        Width = 97
        Height = 13
        Caption = 'Nome para exibi'#231#227'o:'
      end
      object Label15: TLabel
        Left = 449
        Top = 48
        Width = 26
        Height = 13
        Caption = 'Perfil:'
      end
      object LaEsqueciSenha: TLabel
        Left = 314
        Top = 29
        Width = 186
        Height = 13
        Cursor = crHandPoint
        Caption = 'Redefinir senha do usu'#225'rio selecionado'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        OnClick = LaEsqueciSenhaClick
        OnMouseEnter = LaEsqueciSenhaMouseEnter
        OnMouseLeave = LaEsqueciSenhaMouseLeave
      end
      object Label7: TLabel
        Left = 82
        Top = 8
        Width = 45
        Height = 13
        Caption = 'Cadastro:'
      end
      object Label14: TLabel
        Left = 198
        Top = 8
        Width = 42
        Height = 13
        Caption = 'Alterado:'
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 5
        Top = 25
        Width = 72
        Height = 21
        DataField = 'Codigo'
        DataSource = DsWUsers
        TabOrder = 0
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBRgTipo: TDBRadioGroup
        Left = 5
        Top = 48
        Width = 438
        Height = 40
        Caption = 'Tipo de perfil'
        Columns = 4
        DataField = 'Tipo'
        DataSource = DsWUsers
        Items.Strings = (
          'N'#237'vel 0'
          'N'#237'vel 1'
          'N'#237'vel 2'
          'N'#237'vel 3')
        ParentBackground = True
        TabOrder = 3
        Values.Strings = (
          '0'
          '1'
          '2'
          '3')
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 5
        Top = 109
        Width = 438
        Height = 21
        DataField = 'ENTNOME'
        DataSource = DsWUsers
        TabOrder = 5
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit4: TdmkDBEdit
        Left = 5
        Top = 153
        Width = 216
        Height = 21
        DataField = 'Username'
        DataSource = DsWUsers
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit5: TdmkDBEdit
        Left = 227
        Top = 153
        Width = 216
        Height = 20
        DataField = 'SENHA'
        DataSource = DsWUsers
        Font.Charset = SYMBOL_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Wingdings'
        Font.Style = []
        ParentFont = False
        PasswordChar = '|'
        TabOrder = 8
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBCheckBox1: TDBCheckBox
        Left = 5
        Top = 180
        Width = 53
        Height = 17
        Caption = 'Ativo'
        DataField = 'Ativo'
        DataSource = DsWUsers
        TabOrder = 10
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object dmkDBEdit6: TdmkDBEdit
        Left = 449
        Top = 153
        Width = 431
        Height = 21
        DataField = 'Email'
        DataSource = DsWUsers
        TabOrder = 9
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit3: TdmkDBEdit
        Left = 449
        Top = 109
        Width = 431
        Height = 21
        DataField = 'PersonalName'
        DataSource = DsWUsers
        TabOrder = 6
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit7: TdmkDBEdit
        Left = 449
        Top = 66
        Width = 431
        Height = 21
        DataField = 'NOMEPEF'
        DataSource = DsWUsers
        TabOrder = 4
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit8: TdmkDBEdit
        Left = 82
        Top = 25
        Width = 110
        Height = 21
        DataField = 'DataCad_TXT'
        DataSource = DsWUsers
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit9: TdmkDBEdit
        Left = 198
        Top = 25
        Width = 110
        Height = 21
        DataField = 'DataAlt_TXT'
        DataSource = DsWUsers
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 258
      Width = 891
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 194
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 368
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 891
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 587
      Height = 52
      Align = alClient
      TabOrder = 1
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 158
        Height = 32
        Caption = 'Senhas Web'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 158
        Height = 32
        Caption = 'Senhas Web'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 158
        Height = 32
        Caption = 'Senhas Web'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GB_R: TGroupBox
      Left = 803
      Top = 0
      Width = 88
      Height = 52
      Align = alRight
      TabOrder = 2
      object ImgTipo: TdmkImage
        Left = 48
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
      object ImgWEB: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 891
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 887
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 672
    Top = 16
  end
  object QrWUsers: TmySQLQuery
    Database = DModG.DBDmk
    BeforeOpen = QrWUsersBeforeOpen
    AfterOpen = QrWUsersAfterOpen
    SQL.Strings = (
      'SELECT wus.*, wpe.Nome NOMEPEF, '
      'AES_DECRYPT(Password, "") SENHA,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) CLINOME,'
      
        'IF(wus.Entidade = 0, "", IF (enb.Tipo=0, enb.RazaoSocial, enb.No' +
        'me)) ENTNOME'
      'FROM wusers wus'
      'LEFT JOIN wperfis wpe ON wpe.Codigo = wus.Perfil'
      'LEFT JOIN clientes cli ON cli.Codigo = wus.Cliente'
      'LEFT JOIN entidades ent ON ent.Codigo = cli.Cliente'
      'LEFT JOIN entidades enb ON enb.Codigo = wus.Entidade'
      'WHERE wus.Codigo > 0')
    Left = 420
    Top = 16
    object QrWUsersCodigo: TAutoIncField
      FieldName = 'Codigo'
      Origin = 'wusers.Codigo'
    end
    object QrWUsersUsername: TWideStringField
      FieldName = 'Username'
      Origin = 'wusers.Username'
      Required = True
      Size = 32
    end
    object QrWUsersPassword: TWideStringField
      FieldName = 'Password'
      Origin = 'wusers.Password'
      Required = True
      Size = 32
    end
    object QrWUsersPersonalName: TWideStringField
      FieldName = 'PersonalName'
      Origin = 'wusers.PersonalName'
      Required = True
      Size = 32
    end
    object QrWUsersLoginID: TWideStringField
      FieldName = 'LoginID'
      Origin = 'wusers.LoginID'
      Size = 32
    end
    object QrWUsersEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'wusers.Entidade'
      Required = True
    end
    object QrWUsersTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'wusers.Tipo'
      Required = True
    end
    object QrWUsersLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'wusers.Lk'
    end
    object QrWUsersDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'wusers.DataCad'
    end
    object QrWUsersDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'wusers.DataAlt'
    end
    object QrWUsersUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'wusers.UserCad'
    end
    object QrWUsersUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'wusers.UserAlt'
    end
    object QrWUsersAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'wusers.AlterWeb'
      Required = True
    end
    object QrWUsersAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'wusers.Ativo'
      Required = True
    end
    object QrWUsersSENHA: TWideStringField
      FieldName = 'SENHA'
      Size = 32
    end
    object QrWUsersEmail: TWideStringField
      FieldName = 'Email'
      Origin = 'wusers.Email'
      Required = True
      Size = 100
    end
    object QrWUsersPerfil: TIntegerField
      FieldName = 'Perfil'
      Origin = 'wusers.Perfil'
    end
    object QrWUsersCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'wusers.Cliente'
    end
    object QrWUsersNOMEPEF: TWideStringField
      FieldName = 'NOMEPEF'
      Origin = 'wperfis.Nome'
      Size = 50
    end
    object QrWUsersCLINOME: TWideStringField
      FieldName = 'CLINOME'
      Size = 100
    end
    object QrWUsersENTNOME: TWideStringField
      FieldName = 'ENTNOME'
      Size = 100
    end
    object QrWUsersDataCad_TXT: TWideStringField
      FieldName = 'DataCad_TXT'
      Size = 10
    end
    object QrWUsersDataAlt_TXT: TWideStringField
      FieldName = 'DataAlt_TXT'
      Size = 10
    end
  end
  object DsWUsers: TDataSource
    DataSet = QrWUsers
    Left = 448
    Top = 16
  end
  object QrWControl: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Crypt'
      'FROM wcontrol')
    Left = 588
    Top = 16
    object QrWControlCrypt: TWideStringField
      FieldName = 'Crypt'
      Size = 30
    end
  end
  object DsClientes: TDataSource
    Left = 616
    Top = 16
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 560
    Top = 16
  end
  object QrEntidades: TmySQLQuery
    Database = DModG.DBDmk
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome,'
      'IF (ent.Tipo=0, ent.EEmail, PEmail) Email'
      'FROM  entidades ent'
      'ORDER BY Nome')
    Left = 532
    Top = 16
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrEntidadesNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrEntidadesCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrEntidadesEmail: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
  end
  object DsWPerfis: TDataSource
    DataSet = QrWPerfis
    Left = 504
    Top = 16
  end
  object QrWPerfis: TmySQLQuery
    Database = DModG.DBDmk
    SQL.Strings = (
      'SELECT Codigo, Nome, Tipo'
      'FROM wperfis'
      'WHERE Tipo=:P0'
      'ORDER BY Nome')
    Left = 476
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Origin = 'wperfis.Codigo'
      Required = True
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Origin = 'wperfis.Nome'
      Required = True
      Size = 50
    end
    object QrWPerfisTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'wperfis.Tipo'
    end
  end
  object QrLoc: TmySQLQuery
    Database = DModG.DBDmk
    Left = 644
    Top = 16
  end
end
