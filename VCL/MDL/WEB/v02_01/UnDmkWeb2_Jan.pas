unit UnDmkWeb2_Jan;

interface

uses
  Winapi.Windows, Winapi.ShellAPI, System.SysUtils, System.Classes, Vcl.Menus,
  Vcl.Forms, Vcl.ComCtrls, Data.DB, UnDmkEnums, mySQLDbTables, UnGrl_Vars,
  UnProjGroup_Consts;

type
  TUnDmkWeb2_Jan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraUsuarios(DMKID_APP: Integer);
    procedure MostraWOpcoes(DBn: TmySQLDatabase);
    procedure MostraVerifiDBi(DBn: TmySQLDatabase; QryWebParams: TMySQLQuery;
              Verifi: Boolean; Conectado: Boolean = False);
    procedure ConfiguraDBn(DB, DBn: TmySQLDatabase; QryWebParams: TmySQLQuery);
    function  MostraWSincro(DMKID_APP, VersaoApp: Integer; Modulos: String;
              QryWebParams: TMySQLQuery; DB, DBn: TmySQLDatabase; TabelasSelUp,
              TabelasSelDown: array of String; VERIFI_DB_CANCEL: Boolean): Boolean;
  end;

var
  DmkWeb2_Jan: TUnDmkWeb2_Jan;


implementation

uses ModuleGeral, MyDBCheck, DmkGeral, DmkDAC_PF, UMySQLModule, WOpcoes2,
  UnDmkWeb2, MyListas, VerifiDBi, UMySQLDB, UnInternalConsts, WSincro3,
  UnDmkProcFunc;

{ TUnDmkWeb2_Jan }

procedure TUnDmkWeb2_Jan.MostraUsuarios(DMKID_APP: Integer);
var
  Link: String;
begin
  if DMKID_APP = 17 then //DControl
    Link := 'http://www.dermatek.com.br/dcontrol/usuarios'
  else
    Link := '';
  //
  if Link <> '' then
    ShellExecute(Application.Handle, nil, PChar(Link), nil, nil, sw_hide)
  else
    Geral.MB_Aviso('Geral URL n�o definida para este aplicativo!');
end;

procedure TUnDmkWeb2_Jan.ConfiguraDBn(DB, DBn: TmySQLDatabase; QryWebParams: TmySQLQuery);
var
  Versao: Integer;
  Verifica: Boolean;
begin
  if DmodG.SoUmaEmpresaLogada(False) then
  begin
    Versao   := USQLDB.ObtemVersaoAppDB(DB, verappWebFile, DmodG.QrFiliLogCodigo.Value);
    Verifica := Versao < CO_VERSAO;
    //
    // Conex�o na base de dados na url
    // S� pode ser ap�s abrir a tabela controle!
    // ver se est� ativado s� ent�o fazer !
    try
      if DmkWeb2.ConexaoRemota(DBn, QryWebParams, 2) then
      begin
        if QryWebParams.FieldByName('Web_MySQL').AsInteger = 2 then
        begin
          DBn.Connect;
          if DBn.Connected and Verifica then
            MostraVerifiDBi(DBn, QryWebParams, True, True);
        end;
      end;
    except
      Geral.MensagemBox('N�o foi poss�vel se conectar � base de dados remota!',
        'Aviso', MB_OK+MB_ICONWARNING);
    end;
  end;
end;

procedure TUnDmkWeb2_Jan.MostraVerifiDBi(DBn: TmySQLDatabase;
  QryWebParams: TMySQLQuery; Verifi: Boolean; Conectado: Boolean = False);
var
  Empresa: Integer;
begin
  if not DmodG.SoUmaEmpresaLogada(True) then Exit;
  //
  if (Conectado = True) or (DmkWeb2.ConexaoRemota(DBn, QryWebParams, 3)) then
  begin
    if not VAR_VERIFI_DB_CANCEL then
    begin
      Empresa := DmodG.QrFiliLogCodigo.Value;
      //
      if Empresa <> 0 then
      begin
        Application.CreateForm(TFmVerifiDBi, FmVerifiDBi);
        with FmVerifiDbi do
        begin
          FEmpresa := DModG.QrFiliLogCodigo.Value;
          FVerifi  := Verifi;
          ShowModal;
          FVerifi := False;
          Destroy;
        end;
      end else
        Geral.MB_Aviso('Nenhuma empresa foi definida!');
    end;
  end;
end;

procedure TUnDmkWeb2_Jan.MostraWOpcoes(DBn: TmySQLDatabase);
var
  Empresa: Integer;
  Msg: String;
begin
  if not DmodG.SoUmaEmpresaLogada(True) then Exit;
  //
  //Deve ser liberado pois nesta janela � configurado o banco de dados.
  if DmkWeb2.VerificaSeModuloWebEstaAtivo(CO_DMKID_APP,
    DModG.QrMasterHabilModulos.Value, Msg) then
  begin
    Empresa := DmodG.QrFiliLogCodigo.Value;
    //
    if Empresa <> 0 then
    begin
      DmodG.ReopenWebParams(Empresa);
      //
      if DBCheck.CriaFm(TFmWOpcoes2, FmWOpcoes2, afmoNegarComAviso) then
      begin
        FmWOpcoes2.FEmpresa := Empresa;
        FmWOpcoes2.ShowModal;
        FmWOpcoes2.Destroy;
      end;
    end else
      Geral.MB_Aviso('Nenhuma empresa foi definida!');
  end else
    Geral.MB_Aviso(Msg);
end;

function TUnDmkWeb2_Jan.MostraWSincro(DMKID_APP, VersaoApp: Integer;
  Modulos: String; QryWebParams: TMySQLQuery; DB, DBn: TmySQLDatabase;
  TabelasSelUp, TabelasSelDown: array of String; VERIFI_DB_CANCEL: Boolean): Boolean;
var
  Qry: TmySQLQuery;
  i: Integer;
  Msg, Tabela: String;
  TbsSelUp, TbSelDown: TStringList;
  DtaUltimaSincro, DtaSincro: TDateTime;
begin
  Result := False;
  //
  UMyMod.AbreQuery(QryWebParams, DB);
  //
  DtaSincro := dmkPF.DateTime_UTCToMyTimeZone(QryWebParams.FieldByName('DtaSincro').AsDateTime);
  //
  if DmkWeb2.VerificaSeModuloWebEstaAtivo(DMKID_APP, Modulos, Msg) then
  begin
    if Geral.IMV(QryWebParams.FieldByName('Web_DBVersao').AsString) < VersaoApp then
    begin
      if Geral.MB_Pergunta('A vers�o do banco de dados WEB est� desatualizada!' + sLineBreak +
        'Deseja realizar uma verifica��o de banco dados agora?') = ID_YES then
      begin
        MostraVerifiDBi(DBn, QryWebParams, VERIFI_DB_CANCEL);
        Exit;
      end else
        Exit;
    end;
    if DmkWeb2.VerificaSeExisteSincroEmExecucao(DBn, DtaUltimaSincro) then
    begin
      if Geral.MB_Pergunta('Existe um sincroniso em execu��o! ' + sLineBreak +
       'Deseja inciar outro?') <> ID_YES
      then
        Exit;
    end;
    if DmkWeb2.ConexaoRemota(DBn, QryWebParams, 1) then
    begin
      if DBCheck.CriaFm(TFmWSincro3, FmWSincro3, afmoNegarComAviso) then
      begin
        TbsSelUp := TStringList.Create;
        try
          for i := 0 to Length(TabelasSelUp) - 1 do
          begin
            Tabela := TabelasSelUp[i];

            if Tabela <> '' then
              TbsSelUp.Add(Tabela);
          end;
        finally
          FmWSincro3.FTbSelUp := TbsSelUp;
        end;
        //
        TbSelDown := TStringList.Create;
        try
          for i := 0 to Length(TabelasSelDown) - 1 do
          begin
            Tabela := TabelasSelDown[i];

            if Tabela <> '' then
              TbSelDown.Add(Tabela);
          end;
        finally
          FmWSincro3.FTbSelDown := TbSelDown;
        end;
        //
        FmWSincro3.FDtaUltimaSincro := DtaSincro;
        FmWSincro3.ShowModal;
        //
        Result := FmWSincro3.FExecutou;
        //
        FmWSincro3.Destroy;
      end;
    end else
      Geral.MB_Aviso('O sincronismo deve ser feito somente no servidor!' +
        sLineBreak + '�ltimo sincronismo em: ' + Geral.FDT(DtaSincro, 0));
  end else
    Geral.MB_Aviso(Msg);
end;

end.
