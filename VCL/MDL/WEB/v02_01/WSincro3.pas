unit WSincro3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, dmkDBGridZTO, DmkDAC_PF, Xml.xmldom, Datasnap.Provider,
  Datasnap.DBClient, Xml.XMLDoc, Xmlxform;

type
  TFmWSincro3 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    GridPanel1: TGridPanel;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    QrWebSincroUp: TmySQLQuery;
    DsWebSincroUp: TDataSource;
    Panel5: TPanel;
    BtUpTodos: TBitBtn;
    BtUpNenhum: TBitBtn;
    dmkDBGZTOUp: TdmkDBGridZTO;
    dmkDBGZTODown: TdmkDBGridZTO;
    Panel6: TPanel;
    BtDownTodos: TBitBtn;
    BtDownNenhum: TBitBtn;
    ClientDataSet1: TClientDataSet;
    DataSource1: TDataSource;
    XMLTransformProvider1: TXMLTransformProvider;
    QrWebSincroDown: TmySQLQuery;
    DsWebSincroDown: TDataSource;
    QrWebSincroUpCodigo: TIntegerField;
    QrWebSincroUpNome: TWideStringField;
    QrWebSincroUpBancoDados: TWideStringField;
    QrWebSincroUpTabela: TWideStringField;
    QrWebSincroUpOrigem: TSmallintField;
    QrWebSincroUpCategoria: TWideStringField;
    QrWebSincroDownCodigo: TIntegerField;
    QrWebSincroDownNome: TWideStringField;
    QrWebSincroDownBancoDados: TWideStringField;
    QrWebSincroDownTabela: TWideStringField;
    QrWebSincroDownOrigem: TSmallintField;
    QrWebSincroDownCategoria: TWideStringField;
    LaAvisos: TLabel;
    Label4: TLabel;
    EdDtaUltimaSincro: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtUpTodosClick(Sender: TObject);
    procedure BtUpNenhumClick(Sender: TObject);
    procedure BtDownTodosClick(Sender: TObject);
    procedure BtDownNenhumClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
  private
    { Private declarations }
    procedure ReopenWebSincro(Query: TMySQLQuery; Origem: Integer);
    procedure SelecionaTabelas(Tabelas: TStringList; Query: TMySQLQuery; Grade: TDBGrid);
    //procedure ImportaDadosWeb();
  public
    { Public declarations }
    FDtaUltimaSincro: TDateTime;
    FTbSelUp, FTbSelDown: TStringList;
    FExecutou: Boolean;
  end;

  var
  FmWSincro3: TFmWSincro3;

implementation

uses UnMyObjects, UnGrlUsuarios, Module, ModuleGeral, UMySQLDB, UnDmkProcFunc,
  UnGrl_REST;

{$R *.DFM}

procedure TFmWSincro3.BtDownTodosClick(Sender: TObject);
begin
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(dmkDBGZTODown), True);
end;

procedure TFmWSincro3.BtOKClick(Sender: TObject);

  procedure ObtemListaDbTabelas(Query: TMySQLQuery; Grade: TDBGrid;
    var LstDBs, LstTabelas: TStringList);

    procedure AddRegistros();
    var
      DB, Tabela: String;
      Index: Integer;
    begin
      DB     := Query.FieldByName('BancoDados').AsString;
      Tabela := DB + '.' + Query.FieldByName('Tabela').AsString;
      //
      if not LstDBs.Find(DB, Index) then
        LstDBs.Add(DB);
      //
      if not LstTabelas.Find(Tabela, Index) then
        LstTabelas.Add(Tabela);
    end;

  var
    i: Integer;
  begin
    LstDBs     := TStringList.Create;
    LstTabelas := TStringList.Create;
    try
      if Grade <> nil then //Selecionados
      begin
        try
          Grade.Enabled := False;
          //
          if Query.RecordCount > 0 then
          begin
            if Grade.SelectedRows.Count > 1 then
            begin
              with Grade.DataSource.DataSet do
              begin
                for i:= 0 to Grade.SelectedRows.Count - 1 do
                begin
                  GotoBookmark(Grade.SelectedRows.Items[i]);
                  AddRegistros();
                end;
              end;
            end else
            begin
              AddRegistros();
            end;
          end;
        finally
          Grade.Enabled := True;
        end;
      end else
      begin //Todos
        Query.First;
        while not Query.EOF do
        begin
          AddRegistros();
          //
          Query.Next;
        end;
      end;
    finally
      LstDBs.Sort;
      LstTabelas.Sort;
    end;
  end;

  function ObtemTabelas(DB: String; LstTabelas: TStringList): String;
  var
    I: Integer;
    Tabs: TStringList;
    TbDb, TbTab, Res: String;
  begin
    Result := '';
    Res    := '';
    //
    for I := 0 to LstTabelas.Count - 1 do
    begin
      Tabs  := Geral.Explode(LstTabelas[I], '.');
      //
      if Tabs.Count = 2 then
      begin
        TbDb  := Tabs[0];
        TbTab := Tabs[1];
      end else
      if Tabs.Count = 1 then
      begin
        TbDb  := '';
        TbTab := Tabs[0];
      end else
      begin
        TbDb  := '';
        TbTab := '';
      end;
      //
      if TbTab <> '' then
      begin
        if LowerCase(DB) = LowerCase(TbDb) then
        begin
          Res := Res + ' ' + TbTab;
        end;
      end;
    end;
    //
    if Res <> '' then
      Result := ' --tables' + Res;
  end;

const
  CO_NomeOri = 'local_web.sql';
var
  Continua, SincTotal: Boolean;
  AgoraUTC: TDateTime;
  I, Empresa, Ori_Porta, Des_Porta: Integer;
  PathMySQL, Destino, Tabelas, Ori_Host, Ori_DB, Ori_DBNome, Ori_User, Ori_Pass,
  Des_Host, Des_DB, Des_User, Des_Pass, Msg: String;
  LstDBsUp, LstDBsDown, LstTabelasUp, LstTabelasDown: TStringList;
  DBOri, DBDes: TmySQLDataBase;
begin
  if (Trim(PathMySQL) = '') then
  begin
    PathMySQL := USQLDB.ObtemPathMySQLBin(Dmod.MyDB);
    PathMySQL := dmkPF.ValidaDiretorio(PathMySQL, True);
  end;
  //
  if MyObjects.FIC(PathMySQL = '', nil, 'Falha ao definir "PathMySQL"!') then Exit;
  //
  BtOk.Enabled          := False;
  LaAvisos.Caption      := '';
  dmkDBGZTOUp.Enabled   := False;
  dmkDBGZTODown.Enabled := False;
  LstDBsUp              := TStringList.Create;
  LstDBsDown            := TStringList.Create;
  LstTabelasUp          := TStringList.Create;
  LstTabelasDown        := TStringList.Create;
  DBOri                 := TmySQLDataBase.Create(Self);
  DBDes                 := TmySQLDataBase.Create(Self);
  try
    Continua := False;
    Destino  := CO_DIR_RAIZ_DMK + '\Sincro';
    AgoraUTC := DModG.ObtemAgora(True);
    //
    //Upload
    ObtemListaDbTabelas(QrWebSincroUp, TDBGrid(dmkDBGZTOUp), LstDBsUp, LstTabelasUp);
    //
    Empresa    := DmodG.QrWebParams.FieldByName('Empresa').AsInteger;
    Ori_Host   := Dmod.MyDB.Host;
    Ori_DBNome := Dmod.MyDB.DatabaseName;
    Ori_User   := Dmod.MyDB.UserName;
    Ori_Pass   := Dmod.MyDB.UserPassword;
    Ori_Porta  := Dmod.MyDB.Port;
    //
    Des_Host  := Dmod.MyDBn.Host;
    Des_DB    := Dmod.MyDBn.DatabaseName;
    Des_User  := Dmod.MyDBn.UserName;
    Des_Pass  := Dmod.MyDBn.UserPassword;
    Des_Porta := Dmod.MyDBn.Port;
    //
    for I := 0 to LstDBsUp.Count - 1 do
    begin
      Ori_DB  := LstDBsUp[I];
      Tabelas := ObtemTabelas(LstDBsUp[I], LstTabelasUp);
      //
      if Ori_DB = '' then
        Ori_DB := Ori_DBNome;
      //
      if Tabelas <> '' then
      begin
        if not dmkPF.ExecutavelEstaRodando('mysqldump.exe') then //O MySQLDump n�o pode estar rodando
        begin
          if (LstDBsUp.Count - 1) = I then
            Continua := True;
          //
          SincTotal := QrWebSincroUp.RecordCount = LstTabelasUp.Count;
          //
          if not USQLDB.Sincro_ExecutaUnidirecional(DBOri, DBDes, Empresa,
            PathMySQL, Destino, CO_NomeOri, Ori_Host, Ori_DB, Ori_User,
            Ori_Pass, Ori_Porta, Des_Host, Des_DB, Des_User, Des_Pass,
            Des_Porta, Tabelas, SincTotal, AgoraUTC, nil, Msg, LaAvisos)
          then
            Continua := False;
          //
          LaAvisos.Caption := Msg;
        end else
        begin
          Geral.MB_Aviso('O "mysqldump.exe" j� est� sendo executado!' +
            sLineBreak + 'Tente novamente mais tarde!');
          Exit;
        end;
      end;
    end;
    //
    if Continua then
    begin
      //Download
      ObtemListaDbTabelas(QrWebSincroDown, TDBGrid(dmkDBGZTODown), LstDBsDown, LstTabelasDown);
      //
      Ori_Host   := Dmod.MyDBn.Host;
      Ori_DBNome := Dmod.MyDBn.DatabaseName;
      Ori_User   := Dmod.MyDBn.UserName;
      Ori_Pass   := Dmod.MyDBn.UserPassword;
      Ori_Porta  := Dmod.MyDBn.Port;
      //
      Des_Host  := Dmod.MyDB.Host;
      Des_DB    := Dmod.MyDB.DatabaseName;
      Des_User  := Dmod.MyDB.UserName;
      Des_Pass  := Dmod.MyDB.UserPassword;
      Des_Porta := Dmod.MyDB.Port;
      //
      for I := 0 to LstDBsDown.Count - 1 do
      begin
        Ori_DB  := LstDBsDown[I];
        Tabelas := ObtemTabelas(LstDBsDown[I], LstTabelasDown);
        //
        if Ori_DB = '' then
          Ori_DB := Ori_DBNome;
        //
        if Tabelas <> '' then
        begin
          if not dmkPF.ExecutavelEstaRodando('mysqldump.exe') then //O MySQLDump n�o pode estar rodando
          begin
            if (LstDBsDown.Count - 1) = I then
              Continua := True;
            //
            SincTotal := QrWebSincroDown.RecordCount = LstTabelasDown.Count;
            //
            if not USQLDB.Sincro_ExecutaUnidirecional(DBOri, DBDes, Empresa,
              PathMySQL, Destino, CO_NomeOri, Ori_Host, Ori_DB, Ori_User,
              Ori_Pass, Ori_Porta, Des_Host, Des_DB, Des_User, Des_Pass,
              Des_Porta, Tabelas, SincTotal, AgoraUTC, nil, Msg, LaAvisos)
            then
              Continua := False;
            //
            LaAvisos.Caption := Msg;
          end else
          begin
            Geral.MB_Aviso('O "mysqldump.exe" j� est� sendo executado!' +
              sLineBreak + 'Tente novamente mais tarde!');
            Exit;
          end;
        end;
      end;
      USQLDB.Sincro_AtualizaDados(Dmod.MyDB, AgoraUTC, Empresa, SincTotal, True);
      USQLDB.Sincro_AtualizaDados(Dmod.MyDBn, AgoraUTC, Empresa, SincTotal, True);
      //
      Geral.MB_Aviso('Sincronismo finalizado!');
      //
      FExecutou := True;
    end;
  finally
    LstDBsUp.Free;
    LstDBsDown.Free;
    LstTabelasUp.Free;
    LstTabelasDown.Free;
    DBOri.Free;
    DBDes.Free;
    //
    dmkDBGZTOUp.Enabled   := False;
    dmkDBGZTODown.Enabled := False;
    LaAvisos.Caption      := '';
    BtOk.Enabled          := True;
  end;
end;

procedure TFmWSincro3.BtDownNenhumClick(Sender: TObject);
begin
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(dmkDBGZTODown), False);
end;

procedure TFmWSincro3.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWSincro3.BtUpNenhumClick(Sender: TObject);
begin
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(dmkDBGZTOUp), False);
end;

procedure TFmWSincro3.BtUpTodosClick(Sender: TObject);
begin
  MyObjects.SetaTodosItensBookmark(Self, TDBGrid(dmkDBGZTOUp), True);
end;

procedure TFmWSincro3.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmWSincro3.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FExecutou       := False;
  //
  ReopenWebSincro(QrWebSincroUp, 0);
  ReopenWebSincro(QrWebSincroDown, 1);
end;

procedure TFmWSincro3.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWSincro3.FormShow(Sender: TObject);
begin
  EdDtaUltimaSincro.ValueVariant := FDtaUltimaSincro;
  //
  SelecionaTabelas(FTbSelUp, QrWebSincroUp, TDBGrid(dmkDBGZTOUp));
  SelecionaTabelas(FTbSelDown, QrWebSincroDown, TDBGrid(dmkDBGZTODown));
end;

procedure TFmWSincro3.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
begin

end;

(*
procedure TFmWSincro3.ImportaDadosWeb;
var
  Res: String;
  Par: THttpParam;
  Txt: TResourceStream;
  XTR: TXMLDocument;
begin
  if GrlUsuarios.Efetuar_Login(Self, stDesktop, Dmod.MyDB, Dmod.QrUpd,
    Dmod.QrAux, VAR_USUARIO) then
  begin
    Par.Nome  := 'id_cliente';
    Par.Valor := '26657d5ff9020d2abefe558796b99584';
    ///
    if Grl_REST.Rest_Dermatek_Post('faturas', 'faturas_vencidas_visualiza', trtXML, [Par], Res) = 200 then
    begin
      Geral.MB_Aviso(Res);
      Txt := TResourceStream.Create(HInstance, 'Resource_1', RT_RCDATA);
      try
        XTR := TXMLDocument.Create(nil);
        //XTR.LoadFromFile('C:\Users\LATITUDE E6520\Desktop\TesteXML\TesteDelphi.xtr');
        XTR.LoadFromStream(Txt);
        //
        XMLTransformProvider1.TransformRead.TransformationDocument := XTR.DOMDocument;
        XMLTransformProvider1.TransformRead.SourceXml := Trim(Res);
      finally
        Txt.Free;
        ClientDataSet1.Active := True;
      end;
    end;
  end;
end;
*)

procedure TFmWSincro3.ReopenWebSincro(Query: TMySQLQuery; Origem: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT * ',
    'FROM web_sincro ',
    'WHERE Origem=' + Geral.FF0(Origem),
    'ORDER BY Categoria, Nome, Tabela ',
    '']);
end;

procedure TFmWSincro3.SelecionaTabelas(Tabelas: TStringList; Query: TMySQLQuery;
  Grade: TDBGrid);
var
  I, Index: Integer;
  Bookmark: TBookmark;
begin
  Screen.Cursor := crHourGlass;
  Grade.Enabled := False;
  //
  Query.DisableControls;
  //
  Tabelas.Sorted := True;
  BookMark       := Query.Bookmark;
  try
    Query.First;
    //
    while not Query.EOF do
    begin
      if Tabelas.Count > 0 then
      begin
        if Tabelas.Find(Query.FieldByName('Tabela').AsString, Index) then
          Grade.SelectedRows.CurrentRowSelected := True;
      end else
        Grade.SelectedRows.CurrentRowSelected := True;
      //
      Query.Next;
    end;
  finally
    Grade.Enabled := True;
    //
    Query.EnableControls;
    //
    Query.Bookmark := Bookmark;
    Screen.Cursor  := crDefault;
  end;
end;

end.
