object FmWSincro3: TFmWSincro3
  Left = 339
  Top = 185
  Caption = 'WEB-SINCR-003 :: Sincronismo Dados WEB [3]'
  ClientHeight = 774
  ClientWidth = 999
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  OnStartDock = FormStartDock
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 999
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 940
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 881
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 412
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Sincronismo Dados WEB [3]'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 412
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Sincronismo Dados WEB [3]'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 412
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Sincronismo Dados WEB [3]'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 999
    Height = 575
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 999
      Height = 575
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 999
        Height = 575
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object LaAvisos: TLabel
          Left = 2
          Top = 18
          Width = 16
          Height = 20
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Alignment = taCenter
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -17
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object GridPanel1: TGridPanel
          Left = 2
          Top = 38
          Width = 995
          Height = 535
          Align = alClient
          ColumnCollection = <
            item
              Value = 100.000000000000000000
            end>
          ControlCollection = <
            item
              Column = 0
              Control = GroupBox2
              Row = 0
            end
            item
              Column = 0
              Control = GroupBox3
              Row = 1
            end>
          RowCollection = <
            item
              Value = 50.000000000000000000
            end
            item
              Value = 50.000000000000000000
            end>
          TabOrder = 0
          object GroupBox2: TGroupBox
            Left = 1
            Top = 1
            Width = 993
            Height = 266
            Align = alClient
            Caption = 'Upload'
            TabOrder = 0
            object Panel5: TPanel
              Left = 2
              Top = 18
              Width = 60
              Height = 246
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object BtUpTodos: TBitBtn
                Tag = 127
                Left = 5
                Top = 4
                Width = 49
                Height = 50
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                NumGlyphs = 2
                TabOrder = 0
                OnClick = BtUpTodosClick
              end
              object BtUpNenhum: TBitBtn
                Tag = 128
                Left = 5
                Top = 58
                Width = 49
                Height = 50
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                NumGlyphs = 2
                TabOrder = 1
                OnClick = BtUpNenhumClick
              end
            end
            object dmkDBGZTOUp: TdmkDBGridZTO
              Left = 62
              Top = 18
              Width = 929
              Height = 246
              Align = alClient
              DataSource = DsWebSincroUp
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
              TabOrder = 1
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -14
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Categoria'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 300
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Tabela'
                  Width = 150
                  Visible = True
                end>
            end
          end
          object GroupBox3: TGroupBox
            Left = 1
            Top = 267
            Width = 993
            Height = 267
            Align = alClient
            Caption = 'Download'
            TabOrder = 1
            object dmkDBGZTODown: TdmkDBGridZTO
              Left = 62
              Top = 18
              Width = 929
              Height = 247
              Align = alClient
              DataSource = DsWebSincroDown
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -14
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Categoria'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 300
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Tabela'
                  Width = 150
                  Visible = True
                end>
            end
            object Panel6: TPanel
              Left = 2
              Top = 18
              Width = 60
              Height = 247
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object BtDownTodos: TBitBtn
                Tag = 127
                Left = 5
                Top = 5
                Width = 49
                Height = 50
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                NumGlyphs = 2
                TabOrder = 0
                OnClick = BtDownTodosClick
              end
              object BtDownNenhum: TBitBtn
                Tag = 128
                Left = 5
                Top = 58
                Width = 49
                Height = 50
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                NumGlyphs = 2
                TabOrder = 1
                OnClick = BtDownNenhumClick
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 634
    Width = 999
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 995
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -18
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -18
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 688
    Width = 999
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 820
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 818
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label4: TLabel
        Left = 163
        Top = 4
        Width = 135
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = #218'ltmo sincronismo em:'
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Sincroni&zar'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object EdDtaUltimaSincro: TdmkEdit
        Left = 163
        Top = 27
        Width = 169
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfLong
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 432
    Top = 371
  end
  object QrWebSincroUp: TMySQLQuery
    Database = Dmod.MyDB
    Left = 183
    Top = 170
    object QrWebSincroUpCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWebSincroUpNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrWebSincroUpBancoDados: TWideStringField
      FieldName = 'BancoDados'
    end
    object QrWebSincroUpTabela: TWideStringField
      FieldName = 'Tabela'
    end
    object QrWebSincroUpOrigem: TSmallintField
      FieldName = 'Origem'
    end
    object QrWebSincroUpCategoria: TWideStringField
      FieldName = 'Categoria'
      Size = 50
    end
  end
  object DsWebSincroUp: TDataSource
    DataSet = QrWebSincroUp
    Left = 212
    Top = 170
  end
  object ClientDataSet1: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'XMLTransformProvider1'
    Left = 600
    Top = 176
  end
  object DataSource1: TDataSource
    DataSet = ClientDataSet1
    Left = 656
    Top = 176
  end
  object XMLTransformProvider1: TXMLTransformProvider
    CacheData = True
    Left = 520
    Top = 176
  end
  object QrWebSincroDown: TMySQLQuery
    Database = Dmod.MyDB
    Left = 191
    Top = 426
    object QrWebSincroDownCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWebSincroDownNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrWebSincroDownBancoDados: TWideStringField
      FieldName = 'BancoDados'
    end
    object QrWebSincroDownTabela: TWideStringField
      FieldName = 'Tabela'
    end
    object QrWebSincroDownOrigem: TSmallintField
      FieldName = 'Origem'
    end
    object QrWebSincroDownCategoria: TWideStringField
      FieldName = 'Categoria'
      Size = 50
    end
  end
  object DsWebSincroDown: TDataSource
    DataSet = QrWebSincroDown
    Left = 220
    Top = 426
  end
end
