unit WOpcoes2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkDBLookupComboBox,
  dmkEditCB, mySQLDbTables, DmkDAC_PF, Variants, dmkCheckBox, dmkMemo,
  UnProjGroup_Consts;

type
  TFmWOpcoes2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PageControl1: TPageControl;
    TSConexoes: TTabSheet;
    GBDataBase: TGroupBox;
    Label32: TLabel;
    EdWeb_Host: TdmkEdit;
    Label33: TLabel;
    EdWeb_User: TdmkEdit;
    Label34: TLabel;
    EdWeb_Pwd: TdmkEdit;
    Label35: TLabel;
    EdWeb_DB: TdmkEdit;
    Label4: TLabel;
    EdWeb_MyURL: TdmkEdit;
    RGWeb_MySQL: TRadioGroup;
    BtImporta: TBitBtn;
    BtTestar: TBitBtn;
    GBFTP: TGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    EdWeb_FTPh: TdmkEdit;
    EdWeb_FTPu: TdmkEdit;
    EdWeb_Raiz: TdmkEdit;
    EdWeb_FTPs: TdmkEdit;
    BitBtn2: TBitBtn;
    BitBtn1: TBitBtn;
    CkWeb_FTPpassivo: TCheckBox;
    DsAppDirWeb: TDataSource;
    QrAppDirWeb: TmySQLQuery;
    IntegerField1: TIntegerField;
    QrAppDirWebNome: TWideStringField;
    Label18: TLabel;
    EdWeb_Porta: TdmkEdit;
    Label19: TLabel;
    BtImportaWeb: TBitBtn;
    GroupBox2: TGroupBox;
    LAWeb_Id: TLabel;
    EdWeb_Id: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtImportaClick(Sender: TObject);
    procedure BtTestarClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtImportaWebClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao();
  public
    { Public declarations }
    FEmpresa: Integer;
  end;

  var
  FmWOpcoes2: TFmWOpcoes2;

implementation

uses UnMyObjects, Module, UnFTP, UMySQLModule, MyListas, ModuleGeral, UMySQLDB,
  UnDmkWeb, MyDBCheck, UnDmkWeb2, UnGrl_Consts;

{$R *.DFM}

procedure TFmWOpcoes2.BitBtn1Click(Sender: TObject);
const
  Avisa = True;
begin
  UFTP.TestaConexaoFTP(Self, EdWeb_FTPh.Text, EdWeb_FTPu.Text, EdWeb_FTPs.Text,
    CkWeb_FTPpassivo.Checked, Avisa);
end;

procedure TFmWOpcoes2.BitBtn2Click(Sender: TObject);
var
  Nome, URL, Host, WebRaiz, Usuario, Senha: String;
  Passivo: Integer;
begin
  UFTP.ImportaConfigFTP(Self, Nome, URL, Host, WebRaiz, Usuario, Senha, Passivo);
  //
  EdWeb_FTPh.ValueVariant  := Host;
  EdWeb_FTPu.ValueVariant  := Usuario;
  EdWeb_FTPs.Text          := Senha;
  EdWeb_Raiz.ValueVariant  := WebRaiz;
  CkWeb_FTPpassivo.Checked := Geral.IntToBool(Passivo);
end;

procedure TFmWOpcoes2.BtImportaClick(Sender: TObject);
begin
  MyObjects.ImportaConfigWeb(Self, nil, EdWeb_Host, EdWeb_DB, EdWeb_Porta,
    EdWeb_User, EdWeb_Pwd);
end;

procedure TFmWOpcoes2.BtImportaWebClick(Sender: TObject);
(*
var
  Cfg: TWebConfig;
*)
begin
  Geral.MB_Aviso('Mudar para n�o precisar informar a chave de seguran�a!');
  (*
  Cfg := DmkWeb2.ImportaConfiguracoesWeb(Self, Dmod.MyDB, Dmod.QrUpd,
          Dmod.QrAux, EdWebId);
  //
  if Cfg.db_host <> '' then
  begin
    EdWeb_Host.ValueVariant  := Cfg.db_host;
    EdWeb_User.ValueVariant  := Cfg.db_usuario;
    EdWeb_Pwd.ValueVariant   := Cfg.db_senha;
    EdWeb_DB.ValueVariant    := Cfg.db_banco_dados;
    EdWeb_Porta.ValueVariant := Cfg.db_porta;
    //
    EdWeb_MyURL.ValueVariant := Cfg.url_app;
    //
    EdWebId.ValueVariant := Cfg.chave;
    //
    EdWeb_FTPh.ValueVariant  := Cfg.arq_host;
    EdWeb_FTPu.ValueVariant  := Cfg.arq_usuario;
    EdWeb_FTPs.ValueVariant  := Cfg.arq_senha;
    EdWeb_Raiz.ValueVariant  := Cfg.arq_raiz;
    CkWeb_FTPpassivo.Checked := Geral.IntToBool(Geral.IMV(Cfg.arq_passivo));
    //
    RGWeb_MySQL.ItemIndex := 2;
    //
    Geral.MB_Aviso('Configura��o importada com sucesso!');
  end;
  *)
end;

procedure TFmWOpcoes2.BtOKClick(Sender: TObject);
var
  SQLTipo: TSQLType;
  Web_Host, Web_User, Web_Pwd, Web_DB, Web_MyURL, Web_FTPh, Web_FTPu,
  Web_FTPs, Web_Raiz, Web_Id: String;
  Web_Porta, Web_MySQL, Web_FTPpassivo, Empresa: Integer;
begin
  if FEmpresa = 0 then
  begin
    Geral.MB_Erro('Empresa n�o definida!');
    Exit;
  end;
  //
  Web_Host  := EdWeb_Host.ValueVariant;
  Web_User  := EdWeb_User.ValueVariant;
  Web_Pwd   := EdWeb_Pwd.ValueVariant;
  Web_DB    := EdWeb_DB.ValueVariant;
  Web_Porta := edWeb_Porta.ValueVariant;
  Web_MyURL := EdWeb_MyURL.ValueVariant;
  Web_MySQL := RGWeb_MySQL.ItemIndex;
  //
  Web_FTPh       := EdWeb_FTPh.ValueVariant;
  Web_FTPu       := EdWeb_FTPu.ValueVariant;
  Web_FTPs       := EdWeb_FTPs.ValueVariant;
  Web_Raiz       := EdWeb_Raiz.ValueVariant;
  Web_FTPpassivo := Geral.BoolToInt(CkWeb_FTPpassivo.Checked);
  //
  Web_Id := EdWeb_Id.ValueVariant;
  //
  SQLTipo := stUpd;
  Empresa := Geral.IMV(USQLDB.ObtemValorCampo(Dmod.MyDB, FEmpresa, 'Empresa',
               'Empresa', 'web_params'));
  //
  if Empresa = 0 then
  begin
    SQLTipo := stIns;
    Empresa := FEmpresa;
  end;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTipo, 'web_params', False,
    ['Web_Host', 'Web_User', 'Web_Pwd',
    'Web_DB', 'Web_Porta', 'Web_MySQL',
    'Web_MyURL', 'Web_FTPh', 'Web_FTPu',
    'Web_FTPs', 'Web_Raiz', 'Web_FTPpassivo',
    'Web_Id'], ['Empresa'],
    [Web_Host, Web_User, 'AES_ENCRYPT("' + Web_Pwd + '", "' + CO_RandStrWeb01 + '")',
    Web_DB, Web_Porta, Web_MySQL,
    Web_MyURL, Web_FTPh, Web_FTPu,
    'AES_ENCRYPT("' + Web_FTPs + '", "' + CO_RandStrWeb01 + '")', Web_Raiz, Web_FTPpassivo,
    Web_Id], [Empresa], False) then
  begin
    Geral.MB_Aviso('Para que as altera��es tenham efeito o aplicativo dever� ser reiniciado!');
    Close;
  end;
end;

procedure TFmWOpcoes2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWOpcoes2.BtTestarClick(Sender: TObject);
const
  Avisa = True;
begin
  UnDmkDAC_PF.TestarConexaoMySQL(Self, EdWeb_Host.Text, EdWeb_User.Text,
    EdWeb_Pwd.Text, EdWeb_DB.Text, 3306, Avisa);
end;

procedure TFmWOpcoes2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmWOpcoes2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FEmpresa        := 0;
  //
  PageControl1.ActivePageIndex := 0;
  //
  MostraEdicao();
  //
  GBFTP.Visible := CO_DMKID_APP <> 17; //DControl
end;

procedure TFmWOpcoes2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWOpcoes2.FormShow(Sender: TObject);
begin
  DmodG.ReopenWebParams(FEmpresa);
end;

procedure TFmWOpcoes2.MostraEdicao();
begin
  //Conex�es
  EdWeb_Host.ValueVariant  := DmodG.QrWebParams.FieldByName('Web_Host').AsString;
  EdWeb_User.ValueVariant  := DmodG.QrWebParams.FieldByName('Web_User').AsString;
  EdWeb_Pwd.ValueVariant   := DmodG.QrWebParams.FieldByName('Web_Pwd_STR').AsString;
  EdWeb_DB.ValueVariant    := DmodG.QrWebParams.FieldByName('Web_DB').AsString;
  EdWeb_Porta.ValueVariant := DModG.QrWebParams.FieldByName('Web_Porta').AsInteger;
  EdWeb_MyURL.ValueVariant := DmodG.QrWebParams.FieldByName('Web_MyURL').AsString;
  RGWeb_MySQL.ItemIndex    := DmodG.QrWebParams.FieldByName('Web_MySQL').AsInteger;
  //
  EdWeb_Id.ValueVariant    := DmodG.QrWebParams.FieldByName('Web_Id').AsString;
  //
  EdWeb_FTPh.ValueVariant  := DmodG.QrWebParams.FieldByName('Web_FTPh').AsString;
  EdWeb_FTPu.ValueVariant  := DmodG.QrWebParams.FieldByName('Web_FTPu').AsString;
  EdWeb_FTPs.ValueVariant  := DmodG.QrWebParams.FieldByName('Web_FTPs_STR').AsString;
  EdWeb_Raiz.ValueVariant  := DmodG.QrWebParams.FieldByName('Web_Raiz').AsString;
  CkWeb_FTPpassivo.Checked := Geral.IntToBool(DmodG.QrWebParams.FieldByName('Web_FTPpassivo').AsInteger);
end;

end.
