unit UnDmkWeb2;

interface

uses
  Winapi.Windows, System.SysUtils, System.Classes, Data.DB, Vcl.Menus,
  Vcl.ComCtrls, Vcl.Forms, Vcl.Dialogs, Xml.XMLIntf, Xml.XMLDoc, Winapi.WinInet,
  mySQLDbTables, dmkEdit, UnProjGroup_Consts;

type
  TWebConfig = record
    db_host: String;
    db_usuario: String;
    db_senha: String;
    db_banco_dados: String;
    db_porta: String;
    arq_host: String;
    arq_usuario: String;
    arq_senha: String;
    arq_raiz: String;
    arq_passivo: String;
    chave: String;
    url_app: String;
  end;
  TUnDmkWeb2 = class(TObject)
  private
    { Private declarations }
    function  ObtemSegredoWebApp(): String;
    function  RemoteConnection(): Boolean; // TemInternet()
  public
    { Public declarations }
    function  ImportaConfiguracoesWeb(Form: TForm; DB: TmySQLDatabase; QryUpd,
              QryAux: TmySQLQuery; EditWeb_Id: TdmkEdit): TWebConfig;
    function  VerificaSeModuloWebEstaAtivo(DMKID_APP: Integer;
              HabilModulos: String; var Msg: String): Boolean;
    function  ConexaoRemota(DBn: TmySQLDatabase; QryWebParams: TmySQLQuery;
              Aviso: Integer): Boolean;
    function  VerificaSeExisteSincroEmExecucao(Database: TmySQLDatabase;
              var DtaUltimaSincro: TDateTime): Boolean;
  end;

var
  DmkWeb2: TUnDmkWeb2;

implementation

uses MyDBCheck, UnDmkProcFunc, UnGrlUsuarios, UnInternalConsts, dmkGeral,
  UnGrl_REST, MyListas, UnMyObjects, DmkDAC_PF, UnDmkEnums, UnGrl_Vars,
  UnGrl_Geral;

function TUnDmkWeb2.ObtemSegredoWebApp(): String;
begin
  if CO_DMKID_APP = 17 then //DControl
    Result := '0o8ksw844kkwcc4cww880woo40scc0os'
  else if CO_DMKID_APP = 29 then //Infrajob
    Result := 'ws4gsk088w0g408448ws4s8scskksocs'
  else
    Result := '';
end;

function TUnDmkWeb2.RemoteConnection: Boolean;
var
  flags: DWORD;
begin
  if InternetGetConnectedState(@flags, 0) then
  begin
    if not InternetCheckConnection('http://www.dermatek.net.br/', 1, 0) then
    begin
      Result := False;
    end else
      Result := True;
  end else
    Result := False;
end;

function TUnDmkWeb2.ConexaoRemota(DBn: TmySQLDatabase; QryWebParams: TmySQLQuery;
  Aviso: Integer): Boolean;

  function ConectarAoSite(var IP: String; DBn: TmySQLDatabase;
    QryWebParams: TmySQLQuery): Integer;
  const
    Msg = 'Falha ao estabelecer conex�o ao MySQL no meu site!';
  var
    Continua: Boolean;
    BD, PW, ID: String;
  begin
    UnDmkDAC_PF.AbreQueryApenas(QryWebParams);
    //
    Result := -2;
    if QryWebParams.FieldByName('Web_MySQL').AsInteger > 0 then
    begin
      if not DBn.Connected then
      begin
        IP := QryWebParams.FieldByName('Web_Host').AsString;
        ID := QryWebParams.FieldByName('Web_User').AsString;
        PW := QryWebParams.FieldByName('Web_Pwd_STR').AsString;
        BD := QryWebParams.FieldByName('Web_DB').AsString;
        //
        if MyObjects.FIC(IP = '', nil, Msg) then Exit;
        if MyObjects.FIC(ID = '', nil, Msg) then Exit;
        if MyObjects.FIC(PW = '', nil, Msg) then Exit;
        if MyObjects.FIC(BD = '', nil, Msg) then Exit;
        //
        Continua := (LowerCase(IP) = 'localhost') or (IP = '127.0.0.1');
        //
        if not Continua then
          Continua := RemoteConnection(); //Tem internet
        //
        if Continua then
        begin
          try
            UnDmkDAC_PF.ConectaMyDB_DAC(DBn, BD, IP, VAR_PORTA,
              ID, PW, True, True, True);
            //
            Result := Geral.BoolToInt(DBn.Connected);
          except
            Result := 0;
          end;
        end else
          Result := 0;
      end else
        Result := 1;
    end else
      Result := -3;
  end;
var
  IP, Txt: String;
  rtrn: Integer;
begin
  rtrn := -3;
  try
    rtrn := ConectarAoSite(IP, DBn, QryWebParams);
  finally
    if rtrn = 0 then
    begin
      case Aviso of
          0: Txt := '';// nada
          1: Txt := 'N�o h� conex�o com o servidor ' + IP + '!';
          2: Txt := 'N�o foi poss�vel se conectar ao servidor ' + IP + '!';
        else Txt := 'O aplicativo n�o est� conectado ao servidor WEB!';
      end;
      if Txt <> '' then
        Geral.MB_Aviso(Txt);
    end else
    if rtrn = -2 then
      Geral.MB_Aviso('A base de dados WEB n�o est� configurada!');
  end;
  if rtrn < 0 then rtrn := 0;
  Result := Geral.IntToBool(rtrn);
end;

function TUnDmkWeb2.ImportaConfiguracoesWeb(Form: TForm; DB: TmySQLDatabase;
  QryUpd, QryAux: TmySQLQuery; EditWeb_Id: TdmkEdit): TWebConfig;
var
  ResCod, Tot, i: Integer;
  Res, App, XMLNode, XMLVal: String;
  Param: THttpParam;
  XMLArqRes: IXMLDocument;
  Cfg: TWebConfig;
begin
  if MyObjects.FIC(EditWeb_Id.ValueVariant = '', EditWeb_Id, 'Informe a chave de seguran�a!') then Exit;
  //
  VAR_WEB_ID := EditWeb_Id.ValueVariant;
  App        := ObtemSegredoWebApp();
  //
  if App <> '' then
  begin
    if GrlUsuarios.Efetuar_Login(Form, stDesktop, DB, QryUpd, QryAux, VAR_USUARIO) then
    begin
      Param.Nome         := 'web_app';
      Param.Valor        := App;
      Param.ACharse      := '';
      Param.AContentType := '';
      //
      ResCod := Grl_REST.Rest_Dermatek_Post('oauth', 'obtem_config_db', trtXML, Param, Res);
      //
      if ResCod = 200 then
      begin
        XMLArqRes := TXMLDocument.Create(nil);
        XMLArqRes.Active := False;
        try
          XMLArqRes.LoadFromXML(Res);
          XMLArqRes.Encoding := 'ISO-10646-UCS-2';
          //
          for i := 0 to XMLArqRes.DocumentElement.ChildNodes.Count - 1 do
          begin
            with XMLArqRes.DocumentElement.ChildNodes[i] do
            begin
              Tot := ChildNodes.Count;
              //
              if Tot > 0 then
              begin
                XMLNode := NodeName;
                XMLVal  := Text;
                //
                if LowerCase(XMLNode) = 'db_host' then
                  Cfg.db_host := XMLVal;
                if LowerCase(XMLNode) = 'db_usuario' then
                  Cfg.db_usuario := XMLVal;
                if LowerCase(XMLNode) = 'db_senha' then
                  Cfg.db_senha := XMLVal;
                if LowerCase(XMLNode) = 'db_banco_dados' then
                  Cfg.db_banco_dados := XMLVal;
                if LowerCase(XMLNode) = 'db_porta' then
                  Cfg.db_porta := XMLVal;
                if LowerCase(XMLNode) = 'arq_host' then
                  Cfg.arq_host := XMLVal;
                if LowerCase(XMLNode) = 'arq_usuario' then
                  Cfg.arq_usuario := XMLVal;
                if LowerCase(XMLNode) = 'arq_senha' then
                  Cfg.arq_senha := XMLVal;
                if LowerCase(XMLNode) = 'arq_raiz' then
                  Cfg.arq_raiz := XMLVal;
                if LowerCase(XMLNode) = 'arq_passivo' then
                  Cfg.arq_passivo := XMLVal;
                if LowerCase(XMLNode) = 'chave' then
                  Cfg.chave := XMLVal;
                if LowerCase(XMLNode) = 'url_app' then
                  Cfg.url_app := XMLVal;
              end;
            end;
          end;
        finally
          Result := Cfg;
        end;
      end else
      begin
        if Res <> '' then
        begin
          XMLArqRes := TXMLDocument.Create(nil);
          XMLArqRes.Active := False;
          try
            XMLArqRes.LoadFromXML(Res);
            XMLArqRes.Encoding := 'ISO-10646-UCS-2';
            //
            for i := 0 to XMLArqRes.DocumentElement.ChildNodes.Count - 1 do
            begin
              with XMLArqRes.DocumentElement.ChildNodes[i] do
              begin
                if LowerCase(NodeName) = 'item' then
                  Geral.MB_Aviso(Text);
                if LowerCase(NodeName) = 'error' then
                  Geral.MB_Aviso(Text);
              end;
            end;
          finally
            ;
          end;
        end else
          Geral.MB_Aviso('Falha ao obter configura��o!');
      end;
    end;
  end else
    Geral.MB_Aviso('Este aplicativo n�o suporte para esta fun��o!');
end;

function TUnDmkWeb2.VerificaSeExisteSincroEmExecucao(Database: TmySQLDatabase;
  var DtaUltimaSincro: TDateTime): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  Qry    := TmySQLQuery.Create(TDataModule(Database.Owner));
  //
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DataBase, [
      'SELECT DtaSincro, SincroEmExec ',
      'FROM web_opcoes ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      DtaUltimaSincro := Qry.FieldByName('DtaSincro').AsDateTime;
      Result          := Geral.IntToBool(Qry.FieldByName('SincroEmExec').AsInteger);
    end;
  finally
    Qry.Free;
  end;
end;

function TUnDmkWeb2.VerificaSeModuloWebEstaAtivo(DMKID_APP: Integer;
  HabilModulos: String; var Msg: String): Boolean;
begin
  Msg    := '';
  Result := False;
  //
  if not Grl_Geral.LiberaModulo(DMKID_APP, Grl_Geral.ObtemSiglaModulo(mdlappWEB), HabilModulos) then //DControl
  begin
    Msg := 'Voc� n�o est� habilitado para utilizar o m�dulo web!' + sLineBreak +
             'Solicite a libera��o junto a Dermatek!';
  end else
    Result := True;
end;

end.
