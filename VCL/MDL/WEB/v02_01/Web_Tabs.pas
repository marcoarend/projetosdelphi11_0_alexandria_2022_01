unit Web_Tabs;

////////////////////////////////////////////////////////////////////////////////
// Formas na pasta C:\Projetos\Delphi 2007\Outros\Web\Users
////////////////////////////////////////////////////////////////////////////////

{ Colocar no MyListas:

Uses Web_Tabs;


//
function TMyListas.CriaListaTabelas:
  WGerl_Tb.CarregaListaTabelas(FTabelas);
//
function TMyListas.CriaListaSQL:
  WGerl_Tb.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:
  WGerl_Tb.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
  WGerl_Tb.CarregaListaFRIndices(Tabela, FRIndices, FLIndices);
//
function TMyListas.CriaListaCampos:
  WGerl_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos);
  WGerl_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  WGerl_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}
interface

uses
  System.Generics.Collections, System.Classes, Windows, Messages, SysUtils,
  Graphics, Controls, Dialogs, DB, (*DBTables,*) UnMyLinguas, Forms, mysqlDBTables,
  dmkGeral, UnDmkEnums, UnGrl_Vars;

type
  TWeb_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(DatabaseName: String; Lista: TList<TTabelas>;
             DMKID_APP: Integer): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; DMKID_APP: Integer;
             var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Database: TmySQLDatabase; Tabela: String;
             FListaSQL: TStringList; DMKID_APP: Integer): Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>): Boolean;
  end;

//const

var
  Web_Tb: TWeb_Tabs;

implementation

uses UMySQLModule, UnInternalConsts;

function TWeb_Tabs.CarregaListaTabelas(DatabaseName: String;
  Lista: TList<TTabelas>; DMKID_APP: Integer): Boolean;
begin
  try
    Result := True;
    //
    if DatabaseName = VAR_DBWEB then
    begin
      MyLinguas.AdTbLst(Lista, False, Lowercase('web_opcoes'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('usuarios'), '');
      //
      if DMKID_APP = 17 then //DControl
      begin
        //API
        MyLinguas.AdTbLst(Lista, False, LowerCase('api_access'), '');
        MyLinguas.AdTbLst(Lista, False, LowerCase('api_cab'), '');
        MyLinguas.AdTbLst(Lista, False, LowerCase('api_keys'), '');
        MyLinguas.AdTbLst(Lista, False, LowerCase('api_logs'), '');
        MyLinguas.AdTbLst(Lista, False, LowerCase('api_profile'), '');
        //Usu�rios
        MyLinguas.AdTbLst(Lista, False, LowerCase('usr_users'), '');
        MyLinguas.AdTbLst(Lista, False, LowerCase('usr_usersits'), '');
        MyLinguas.AdTbLst(Lista, False, LowerCase('usr_opcoes'), '');
        //
        MyLinguas.AdTbLst(Lista, False, LowerCase('web_sessions'), '');
      end;
    end else
    begin
      MyLinguas.AdTbLst(Lista, False, Lowercase('usuarios'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('web_params'), '');
      //
      if DMKID_APP = 17 then //DControl
      begin
        MyLinguas.AdTbLst(Lista, False, Lowercase('web_sincro'), '');
      end;
    end;
  except
    raise;
    Result := False;
  end;
end;

function TWeb_Tabs.CarregaListaSQL(Database: TmySQLDatabase; Tabela: String;
  FListaSQL: TStringList; DMKID_APP: Integer): Boolean;
var
  I: Integer;
  Cod, Tab: String;
  Qry: TmySQLQuery;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('api_access') then
  begin
    if FListaSQL.Count = 0 then
    FListaSQL.Add('Id|All_access|    Controller|Profile');
    FListaSQL.Add('-1|         1|            ""|     -1');
    FListaSQL.Add(' 0|         0|   "api/oauth"|      0');
    FListaSQL.Add(' 0|         0|"api/liberado"|      1');
  end else
  if Uppercase(Tabela) = Uppercase('api_cab') then
  begin
    if FListaSQL.Count = 0 then
    FListaSQL.Add('Id|           Controller|                                                              Description|     GroupAPI|Module|Private');
    FListaSQL.Add(' 1|           "api/apps"|                                       "Gerenciamento de aplicativos WEB"|"Aplicativos"|"VERS"|      0');
    FListaSQL.Add(' 2|       "api/arquivos"|                                              "Gerenciamento de arquivos"|   "Arquivos"| "ARQ"|      0');
    FListaSQL.Add(' 3|     "api/calendario"|"Gerenciamento de itens relacionados ao calend�rio. Agenda, Tarefas, ..."| "Calend�rio"|"CALE"|      0');
    FListaSQL.Add(' 4|      "api/entidades"|                                             "Gerenciamento de entidades"|  "Entidades"| "ENT"|      0');
    FListaSQL.Add(' 5|          "api/oauth"|                                                "Gerenciamento de tokens"|   "Usu�rios"| "USR"|      1');
    FListaSQL.Add(' 6|         "api/perfis"|                                  "Gerenciamento de perfis de senhas WEB"|   "Usu�rios"| "USR"|      0');
    FListaSQL.Add(' 7|         "api/textos"|                                                "Gerenciamento de textos"|     "Textos"| "TXT"|      0');
    FListaSQL.Add(' 8|          "api/users"|                                          "Gerenciamento de usu�rios WEB"|   "Usu�rios"| "USR"|      0');
    FListaSQL.Add(' 9|         "api/ajudas"|                                                 "Visualiza��o de ajudas"|    "Suporte"|"HELP"|      0');
    FListaSQL.Add('10|           "api/nfse"|                                                "Gerenciamento de NFS-es"|      "NFS-e"|"NFSE"|      0');
    FListaSQL.Add('11|        "api/faturas"|                                               "Gerenciamento de Faturas"|    "Faturas"| "BLQ"|      0');
    FListaSQL.Add('12|"api/faturas_imprime"|                                                   "Impress�o de Faturas"|    "Faturas"| "BLQ"|      0');
    FListaSQL.Add('13|       "api/liberado"|                             "Servi�os dispon�veis para acesso sem senha"|   "Usu�rios"| "USR"|      1');
    FListaSQL.Add('14|       "api/helpdesk"|                                                      "Suporte Help Desk"|    "Suporte"|"HELP"|      0');
    FListaSQL.Add('15|     "api/financeiro"|                                               "Gerenciamento financeiro"| "Financeiro"| "FIN"|      0');
    FListaSQL.Add('16|       "api/clientes"|                                              "Gerenciamento de clientes"|   "Clientes"| "CLI"|      0');
  end else
  if Uppercase(Tabela) = Uppercase('api_profile') then
  begin
    if FListaSQL.Count = 0 then
    FListaSQL.Add('Id|  Name|Level');
    FListaSQL.Add('-1|"Boss"|    0');
    FListaSQL.Add(' 0|    ""|    0');
  end else
  if Uppercase(Tabela) = Uppercase('web_opcoes') then
  begin
    if FListaSQL.Count = 0 then
    FListaSQL.Add('Codigo');
    FListaSQL.Add('     1');
  end else
  if Uppercase(Tabela) = Uppercase('usr_opcoes') then
  begin
    if FListaSQL.Count = 0 then
    FListaSQL.Add('Codigo');
    FListaSQL.Add('     1');
  end else
  if Uppercase(Tabela) = Uppercase('usr_users') then
  begin
    if FListaSQL.Count = 0 then //Senha padr�o: Administrador => 123mudar
    FListaSQL.Add('Codigo|           Nome|        Usuario|             Senha');
    FListaSQL.Add('     1|"Administrador"|"Administrador"|"�NeU�`�M��͘_v"');
  end else
  if Uppercase(Tabela) = Uppercase('usr_usersits') then
  begin
    if FListaSQL.Count = 0 then
    FListaSQL.Add('Codigo|Cliente_Id|Entidade|Perfil|Tipo');
    FListaSQL.Add('     1|       -11|     -11|    -1|   0');
  end else
  if Uppercase(Tabela) = Uppercase('web_sincro') then
  begin
    if DMKID_APP = 17 then //DControl
    begin
      if FListaSQL.Count = 0 then
      FListaSQL.Add('Codigo|    Categoria|                                Nome|                            BancoDados|       Tabela|Origem');
      FListaSQL.Add('     1|     "Ajudas"|                            "Ajudas"|                                    ""|    "helpcab"|     0');
      FListaSQL.Add('     2|     "Ajudas"|                   "Ajudas - Op��es"|                                    ""|    "helpopc"|     0');
      FListaSQL.Add('     3|     "Ajudas"|                  "Ajudas - T�picos"|                                    ""|  "helptopic"|     0');
      FListaSQL.Add('     4|     "Ajudas"|                      "Ajudas - FAQ"|                                    ""|    "helpfaq"|     0');
      FListaSQL.Add('     5|     "Ajudas"|               "Ajudas - Restri��es"|                                    ""|  "helprestr"|     0');
      FListaSQL.Add('     6|     "Ajudas"|      "Ajudas - Tipos de Restri��es"|                                    ""| "helprestip"|     0');
      FListaSQL.Add('     7|    "Boletos"|         "Arrecada��es de bloquetos"|                                    ""|    "arreits"|     0');
      FListaSQL.Add('     8|"Aplicativos"|             "Cadastros aplicativos"|                                    ""|"aplicativos"|     0');
      FListaSQL.Add('     9|"Aplicativos"|   "Cadastros aplicativos / m�dulos"|                                    ""| "aplicmodul"|     0');
      FListaSQL.Add('    10|   "Clientes"|  "Cadastros clientes / aplicativos"|                                    ""|   "cliaplic"|     0');
      FListaSQL.Add('    11|   "Clientes"|      "Cadastros clientes / m�dulos"|                                    ""| "cliaplicmo"|     0');
      FListaSQL.Add('    12|   "Clientes"|             "Cadastros de clientes"|                                    ""|   "clientes"|     0');
      FListaSQL.Add('    13|     "Outros"|             "Cadastros de empresas"|                                    ""| "enticliint"|     0');
      FListaSQL.Add('    14|     "Outros"|             "Cadastros de feriados"|                                    ""|   "feriados"|     0');
      FListaSQL.Add('    15|  "Entidades"|               "Cadastros entidades"|                                    ""|  "entidades"|     0');
      FListaSQL.Add('    16|  "Entidades"|    "Cadastros entidades / Contatos"|                                    ""| "enticontat"|     0');
      FListaSQL.Add('    17|  "Entidades"|"Cadastros entidades / Cont. Atrel."|                                    ""| "enticonent"|     0');
      FListaSQL.Add('    18|  "Entidades"|     "Cadastros entidades / E-mails"|                                    ""|   "entimail"|     0');
      FListaSQL.Add('    19|  "Entidades"|   "Cadastros entidades / Telefones"|                                    ""|    "entitel"|     0');
      FListaSQL.Add('    20| "Financeiro"|             "Carteiras financeiras"|                                    ""|  "carteiras"|     0');
      FListaSQL.Add('    21|     "Outros"|  "Configura��o de E-mails e Contas"|                                    ""| "emailconta"|     0');
      FListaSQL.Add('    22|    "Boletos"|                "Configura��es CNAB"|                                    ""|   "cnab_cfg"|     0');
      FListaSQL.Add('    23|     "Outros"|               "Configura��es MySQL"|                                    ""|  "mysqlconf"|     0');
      FListaSQL.Add('    24|  "Entidades"|              "Lista de logradouros"|                                    ""|"listalograd"|     0');
      FListaSQL.Add('    25|"Aplicativos"|                           "Modulos"|                                    ""|    "modulos"|     0');
      FListaSQL.Add('    26|  "Entidades"|                        "Munic�pios"|"' + LowerCase(VAR_AllID_DB_NOME) + '"| "dtb_munici"|     0');
      FListaSQL.Add('    27|      "NFS-e"|                             "NFS-e"|                                    ""| "nfsenfscab"|     0');
      FListaSQL.Add('    28|    "Boletos"|                 "Op��es de faturas"|                                    ""|  "bloopcoes"|     0');
      FListaSQL.Add('    29|    "Boletos"|             "Or�amentos de faturas"|                                    ""|       "prev"|     0');
      FListaSQL.Add('    30|  "Entidades"|                            "Pa�ses"|"' + LowerCase(VAR_AllID_DB_NOME) + '"| "bacen_pais"|     0');
      FListaSQL.Add('    31|     "Outros"|                       "Pr� e-mails"|                                    ""|   "preemail"|     0');
      FListaSQL.Add('    32|     "Outros"|               "Pre e-mails - Texto"|                                    ""|   "preemmsg"|     0');
      FListaSQL.Add('    33| "Protocolos"|                        "Protocolos"|                                    ""| "protocolos"|     0');
      FListaSQL.Add('    34| "Protocolos"|                  "Protocolos itens"|                                    ""| "protpakits"|     0');
      FListaSQL.Add('    35|     "Outros"|               "Tarifas de Servi�os"|                                    ""|  "srvtarifa"|     0');
      FListaSQL.Add('    36|  "Entidades"|      "Tipos de telefones e e-mails"|                                    ""| "entitipcto"|     0');
      FListaSQL.Add('    37|  "Entidades"|                               "UFs"|                                    ""|        "ufs"|     0');
      FListaSQL.Add('    38|      "NFS-e"|                         "NFS-e DPS"|                                    ""| "nfsedpscab"|     0');
      //
      Qry := TmySQLQuery.Create(TDataModule(Database.Owner));
      try
        I := 0;
        //
        Qry.Database := Database;
        Qry.SQL.Clear;
        Qry.SQL.Add('SHOW TABLES');
        Qry.SQL.Add('FROM ' + LowerCase(TMeuDB));
        Qry.SQL.Add('WHERE Tables_in_' + LowerCase(TMeuDB) + ' LIKE "lct%a"');
        Qry.SQL.Add('   OR Tables_in_' + LowerCase(TMeuDB) + ' LIKE "lct%b"');
        Qry.SQL.Add('   OR Tables_in_' + LowerCase(TMeuDB) + ' LIKE "lct%d"');
        Qry.Open;
        Qry.First;
        while not Qry.Eof do
        begin
          I   := I + 1;
          Cod := Geral.FF0(0 - I);
          Tab := Qry.Fields[0].AsString;
          //
          FListaSQL.Add(Cod + '|"Financeiro"|"Lan�amentos financeiros"|""|"' + Tab + '"|0');
          //
          Qry.Next;
        end;
        Qry.Close;
      finally
        Qry.Free;
      end;
    end;
  end;
end;

function TWeb_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
  TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('') then
  begin
    //
  end else if Uppercase(Tabela) = Uppercase('XXXXX') then
  begin
    //FListaSQL.Add('"Entidades","Cadastro de pessoas f�sicas e jur�dicas (clientes, fornecedores, etc.)"');
    //FListaSQL.Add('"",""');
  end;
end;


function TWeb_Tabs.CarregaListaFRIndices(Tabela: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  //
  if Uppercase(Tabela) = Uppercase('api_access') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Id';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Profile';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Controller';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('api_cab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Id';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controller';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('api_keys') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Id';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'ApiKey';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Client_Id';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'User';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'OAuth';
    FLIndices.Add(FRIndices);
    //
    (* DEPRECADO => Em implementa��o ap�s remover excluir da web
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'App';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 5;
    FRIndices.Column_name   := 'Device';
    FLIndices.Add(FRIndices);
    //
    *)
  end else
  if Uppercase(Tabela) = Uppercase('api_logs') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Id';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('api_profile') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Id';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('web_opcoes') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('web_params') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Empresa';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('usr_opcoes') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('usuarios') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Cliente_Id';
    FLIndices.Add(FRIndices);
    //
    (* DEPRECADO => Em implementa��o ap�s remover excluir da web
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 3;
    FRIndices.Column_name   := 'App';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 4;
    FRIndices.Column_name   := 'Device';
    FLIndices.Add(FRIndices);
    //
    *)
  end else
  if Uppercase(Tabela) = Uppercase('usr_users') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Usuario';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('usr_usersits') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Cliente_Id';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('web_sessions') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Id';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'IP_Address';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 1;
    FRIndices.Key_name      := 'usr_sessions_Timestamp';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Timestamp';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('web_sincro') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'BancoDados';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'Tabela';
    FLIndices.Add(FRIndices);
    //
  end;
end;

function TWeb_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; DMKID_APP: Integer;
 var TemControle: TTemControle): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('api_access') then
    begin
      TemControle := TemControle;
      //
      New(FRCampos);
      FRCampos.Field      := 'Id';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Profile';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'All_access';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controller';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Date_created';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Date_modified';
      FRCampos.Tipo       := 'timestamp';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'CURRENT_TIMESTAMP';
      FRCampos.Extra      := 'ON UPDATE CURRENT_TIMESTAMP';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('api_cab') then
    begin
      TemControle := TemControle;
      //
      New(FRCampos);
      FRCampos.Field      := 'Id';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controller';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Description';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GroupAPI';
      FRCampos.Tipo       := 'varchar(40)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Module';
      FRCampos.Tipo       := 'varchar(4)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Private';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('api_keys') then
    begin
      TemControle := TemControle;
      //
      New(FRCampos);
      FRCampos.Field      := 'Id';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ApiKey';
      FRCampos.Tipo       := 'varchar(40)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Client_Id';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Client_Str';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'User';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'OAuth';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Profile';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ignore_Limits';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Is_Private_Key';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ip_Addresses';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Date_Created';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LoginID';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      (* DEPRECADO => Em implementa��o ap�s remover excluir da web
      New(FRCampos);
      FRCampos.Field      := 'App';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'App_Id';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Device';  //        0 => Desktop
      FRCampos.Tipo       := 'int(11)'; //        1 => Web
      FRCampos.Null       := 'YES';        //002 / 999 => Device ID
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      *)
    end else
    if Uppercase(Tabela) = Uppercase('api_logs') then
    begin
      TemControle := TemControle;
      //
      New(FRCampos);
      FRCampos.Field      := 'Id';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := 'auto_increment';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Uri';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Method';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Params';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Api_Key';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ip_Address';
      FRCampos.Tipo       := 'varchar(45)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Time';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RTime';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Authorized';
      FRCampos.Tipo       := 'varchar(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Response_Code';
      FRCampos.Tipo       := 'tinyint(3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('api_profile') then
    begin
      TemControle := TemControle;
      //
      New(FRCampos);
      FRCampos.Field      := 'Id';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Name';
      FRCampos.Tipo       := 'varchar(40)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Client_Id';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Level';      // 0 ADMINISTRADOR
      FRCampos.Tipo       := 'tinyint(1)'; // 1 BOSS
      FRCampos.Null       := '';           // 2 CLIENTE
      FRCampos.Key        := '';           // 3 USU�RIO
      FRCampos.Default    := '-1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('web_opcoes') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaSincro';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SincroEmExec';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('usr_opcoes') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ErrQtd';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DiasExp';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProInsPas'; //Tarefa de protocolo para cria��o de senha
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ProAltPas'; //Tarefa de protocolo para altera��o de senha
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TermoUsu'; //Termo de uso
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TermoPri'; //Termo de privacidade
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('usuarios') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo'; // C�digo do usu�rio Dermatek
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente_Id'; // C�digo do cliente (entidade) no banco de dados Dermatek
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      (* DEPRECADO => Em implementa��o ap�s remover excluir da web
      New(FRCampos);
      FRCampos.Field      := 'App';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'App_Id';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Device';  //        0 => Desktop
      FRCampos.Tipo       := 'int(11)'; //        1 => Web
      FRCampos.Null       := 'YES';        //002 / 999 => Device ID
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      *)
      New(FRCampos);
      FRCampos.Field      := 'Entidade'; // C�digo da entidade
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente_Str'; // String do cliente (entidade) no banco de dados Dermatek
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Token';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Login_Id';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'EntNome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaNatal';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sexo';
      FRCampos.Tipo       := 'tinyint(1)'; // 0- N�o Informado
      FRCampos.Null       := '';           // 1- Masculino
      FRCampos.Key        := '';           // 2- Feminino
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Usuario';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Email';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Telefone';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TimeZone';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'America/Sao_Paulo';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TimeZoneDifUTC';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '-03:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Perfil'; // Perfil de senha
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo'; //N�vel de permiss�o do usu�rio (Informado no perfil)
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '-1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Confirmado'; //Informa se a conta foi confirmada pelo usu�rio
      FRCampos.Tipo       := 'tinyint(1)'; //Aceitou os termos
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsrLocal'; // C�digo do usu�rio local (para aplicativos desktop)
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      if DMKID_APP = 19 then //Academy
      begin
        New(FRCampos);
        FRCampos.Field      := 'Matricula';
        FRCampos.Tipo       := 'varchar(20)';
        FRCampos.Null       := 'YES';
        FRCampos.Key        := '';
        FRCampos.Default    := '';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
        New(FRCampos);
        FRCampos.Field      := 'Categoria';
        FRCampos.Tipo       := 'int(11)';
        FRCampos.Null       := '';
        FRCampos.Key        := '';
        FRCampos.Default    := '0';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
        New(FRCampos);
        FRCampos.Field      := 'Observacao';
        FRCampos.Tipo       := 'varchar(255)';
        FRCampos.Null       := 'YES';
        FRCampos.Key        := '';
        FRCampos.Default    := '';
        FRCampos.Extra      := '';
        FLCampos.Add(FRCampos);
        //
      end;
    end else
    if Uppercase(Tabela) = Uppercase('usr_users') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaNatal';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sexo';
      FRCampos.Tipo       := 'tinyint(1)'; // 0- N�o Informado
      FRCampos.Null       := '';           // 1- Masculino
      FRCampos.Key        := '';           // 2- Feminino
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Usuario';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Senha';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Email';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Telefone';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TimeZone';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := 'America/Sao_Paulo';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TimeZoneDifUTC';
      FRCampos.Tipo       := 'varchar(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '-03:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaLastLogUTC';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LoginID';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('usr_usersits') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cliente_Id'; // C�digo da entidade DB Dermatek
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entidade'; // C�digo da entidade DB cliente
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Perfil'; // Perfil de senha DB cliente
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo'; //N�vel de permiss�o do usu�rio (Informado no perfil) DB cliente
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '-1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('web_sessions') then
    begin
      TemControle := TemControle;
      //
      New(FRCampos);
      FRCampos.Field      := 'Id';
      FRCampos.Tipo       := 'varchar(128)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IP_Address';
      FRCampos.Tipo       := 'varchar(45)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Timestamp';
      FRCampos.Tipo       := 'int(10)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Data';
      FRCampos.Tipo       := 'blob';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('web_sincro') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Categoria';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'BancoDados';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'MUL';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tabela';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Origem';     //0 => Local p/ Web
      FRCampos.Tipo       := 'tinyint(1)'; //1 => Web p/ Local
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TWeb_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      New(FRCampos);
      FRCampos.Field      := 'DtaSincro'; //UTC
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('web_params') then
    begin
      New(FRCampos);
      FRCampos.Field      := 'Empresa';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_Host';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_User';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_Pwd';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_DB';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_Porta';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '3306';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_MyURL';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_MySQL';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_FTPh';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_FTPu';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_FTPs';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_Raiz';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_FTPpassivo';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_DBVersao';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_Id';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DtaSincro';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TWeb_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  // WEB-OPCOE-001 :: Op��es WEB
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-OPCOE-001';
  FRJanelas.Nome      := 'FmWOpcoes3';
  FRJanelas.Descricao := 'Op��es WEB';
  FRJanelas.Modulo    := 'WEB';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-SINCR-003 :: Sincronismo Dados WEB [3]
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-SINCR-003';
  FRJanelas.Nome      := 'FmWSincro3';
  FRJanelas.Descricao := 'Sincronismo Dados WEB [3]';
  FRJanelas.Modulo    := 'WEB';
  FLJanelas.Add(FRJanelas);
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.
