unit NFCeEmiss_0400;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.OleCtrls, SHDocVw, Vcl.Mask, dmkDBLookupComboBox, dmkEditCB,
  dmkRadioGroup, NFCe_PF, dmkCheckBox;

type
  TFmNFCeEmiss_0400 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrImprime: TMySQLQuery;
    QrImprimeCO_SerieNF: TIntegerField;
    QrImprimeSequencial: TIntegerField;
    QrImprimeIncSeqAuto: TSmallintField;
    QrImprimeCtrl_nfs: TIntegerField;
    QrImprimeMaxSeqLib: TIntegerField;
    QrImprimeTipoImpressao: TIntegerField;
    QrImprimeSerieNF_Normal: TIntegerField;
    DsImprime: TDataSource;
    QrParamsEmp: TMySQLQuery;
    QrParamsEmpAssocModNF: TIntegerField;
    QrParamsEmpAssociada: TIntegerField;
    QrParamsEmpCRT: TSmallintField;
    QrParamsEmpCSOSN: TIntegerField;
    QrParamsEmppCredSNAlq: TFloatField;
    QrParamsEmppCredSNMez: TIntegerField;
    QrParamsEmppCredSN_Cfg: TIntegerField;
    QrParamsEmpNFeInfCpl: TIntegerField;
    QrParamsEmpNFeInfCpl_TXT: TWideMemoField;
    QrFatPedNFs_A: TMySQLQuery;
    QrParamsEmpNFetpEmis: TIntegerField;
    QrCTG: TMySQLQuery;
    QrCTGORDEM: TLargeintField;
    QrCTGtpEmis: TIntegerField;
    QrCTGdhEntrada: TDateTimeField;
    QrCTGNome: TWideStringField;
    QrNFeCabA: TMySQLQuery;
    QrNFeCabAStatus: TSmallintField;
    QrNFeCabAinfProt_cStat: TIntegerField;
    QrNFeCabAinfCanc_cStat: TIntegerField;
    QrNFeCabAinfCanc_xMotivo: TWideStringField;
    QrNFeCabAinfProt_xMotivo: TWideStringField;
    QrNFeCabAcStat: TIntegerField;
    QrNFeCabAxMotivo: TWideStringField;
    QrNFeCabAcStat_xMotivo: TWideStringField;
    QrNFeCabAide_cNF: TIntegerField;
    QrNFeCabAIDCtrl: TIntegerField;
    QrNFeCabAId: TWideStringField;
    QrNFeCabAFatID: TIntegerField;
    QrNFeCabAFatNum: TIntegerField;
    QrNFeCabAEmpresa: TIntegerField;
    QrFatPedNFs_B: TMySQLQuery;
    QrFatPedNFs_BFilial: TIntegerField;
    QrFatPedNFs_BIDCtrl: TIntegerField;
    QrFatPedNFs_BTipo: TSmallintField;
    QrFatPedNFs_BOriCodi: TIntegerField;
    QrFatPedNFs_BEmpresa: TIntegerField;
    QrFatPedNFs_BNumeroNF: TIntegerField;
    QrFatPedNFs_BIncSeqAuto: TSmallintField;
    QrFatPedNFs_BAlterWeb: TSmallintField;
    QrFatPedNFs_BAtivo: TSmallintField;
    QrFatPedNFs_BSerieNFCod: TIntegerField;
    QrFatPedNFs_BSerieNFTxt: TWideStringField;
    QrFatPedNFs_BCO_ENT_EMP: TIntegerField;
    QrFatPedNFs_BDataCad: TDateField;
    QrFatPedNFs_BDataAlt: TDateField;
    QrFatPedNFs_BDataAlt_TXT: TWideStringField;
    QrFatPedNFs_BFreteVal: TFloatField;
    QrFatPedNFs_BSeguro: TFloatField;
    QrFatPedNFs_BOutros: TFloatField;
    QrFatPedNFs_BPlacaUF: TWideStringField;
    QrFatPedNFs_BPlacaNr: TWideStringField;
    QrFatPedNFs_BEspecie: TWideStringField;
    QrFatPedNFs_BMarca: TWideStringField;
    QrFatPedNFs_BNumero: TWideStringField;
    QrFatPedNFs_BkgBruto: TFloatField;
    QrFatPedNFs_BkgLiqui: TFloatField;
    QrFatPedNFs_BQuantidade: TWideStringField;
    QrFatPedNFs_BObservacao: TWideStringField;
    QrFatPedNFs_BCFOP1: TWideStringField;
    QrFatPedNFs_BDtEmissNF: TDateField;
    QrFatPedNFs_BDtEntraSai: TDateField;
    QrFatPedNFs_BStatus: TIntegerField;
    QrFatPedNFs_BinfProt_cStat: TIntegerField;
    QrFatPedNFs_BinfAdic_infCpl: TWideMemoField;
    QrFatPedNFs_BinfCanc_cStat: TIntegerField;
    QrFatPedNFs_BDTEMISSNF_TXT: TWideStringField;
    QrFatPedNFs_BDTENTRASAI_TXT: TWideStringField;
    QrFatPedNFs_BRNTC: TWideStringField;
    QrFatPedNFs_BUFembarq: TWideStringField;
    QrFatPedNFs_BxLocEmbarq: TWideStringField;
    QrFatPedNFs_Bide_tpNF: TSmallintField;
    QrFatPedNFs_BHrEntraSai: TTimeField;
    QrFatPedNFs_Bide_dhCont: TDateTimeField;
    QrFatPedNFs_Bide_xJust: TWideStringField;
    QrFatPedNFs_Bemit_CRT: TSmallintField;
    QrFatPedNFs_Bdest_email: TWideStringField;
    QrFatPedNFs_Bvagao: TWideStringField;
    QrFatPedNFs_Bbalsa: TWideStringField;
    QrFatPedNFs_BCabA_FatID: TIntegerField;
    QrFatPedNFs_BCompra_XNEmp: TWideStringField;
    QrFatPedNFs_BCompra_XPed: TWideStringField;
    QrFatPedNFs_BCompra_XCont: TWideStringField;
    QrFatPedNFs_BdhEmiTZD: TFloatField;
    QrFatPedNFs_BdhSaiEntTZD: TFloatField;
    QrFatPedNFs_BHrEmi: TTimeField;
    QrFatPedNFs_BLoteEnv: TIntegerField;
    QrFatPedCab: TMySQLQuery;
    QrFatPedCabMedDDSimpl: TFloatField;
    QrFatPedCabCodigo: TIntegerField;
    QrFatPedCabCodUsu: TIntegerField;
    QrFatPedCabEncerrou: TDateTimeField;
    QrFatPedCabNO_EMP: TWideStringField;
    QrFatPedCabCODMUNICI: TFloatField;
    QrFatPedCabUF_TXT_emp: TWideStringField;
    QrFatPedCabUF_TXT_cli: TWideStringField;
    QrFatPedCabNO_CLI: TWideStringField;
    QrFatPedCabinfAdFisco: TWideStringField;
    QrFatPedCabFinanceiro: TSmallintField;
    QrFatPedCabTotalFatur: TFloatField;
    QrFatPedCabtpNF: TSmallintField;
    QrFatPedCabDTAEMISS_TXT: TWideStringField;
    QrFatPedCabDTAENTRA_TXT: TWideStringField;
    QrFatPedCabDTAINCLU_TXT: TWideStringField;
    QrFatPedCabDTAPREVI_TXT: TWideStringField;
    QrFatPedCabDtaEmiss: TDateField;
    QrFatPedCabDtaEntra: TDateField;
    QrFatPedCabDtaInclu: TDateField;
    QrFatPedCabDtaPrevi: TDateField;
    QrFatPedCabFretePor: TIntegerField;
    QrFatPedCabModeloNF: TFloatField;
    QrFatPedCabRegrFiscal: TFloatField;
    QrFatPedCabCartEmis: TFloatField;
    QrFatPedCabTabelaPrc: TFloatField;
    QrFatPedCabCondicaoPG: TFloatField;
    QrFatPedCabTransporta: TFloatField;
    QrFatPedCabEmpresa: TFloatField;
    QrFatPedCabCliente: TFloatField;
    QrFatPedCabidDest: TSmallintField;
    QrFatPedCabindFinal: TSmallintField;
    QrFatPedCabindPres: TSmallintField;
    QrFatPedCabindSinc: TSmallintField;
    QrFatPedCabTpCalcTrib: TSmallintField;
    QrFatPedCabPedidoCli: TWideStringField;
    QrFatPedCabfinNFe: TSmallintField;
    QrFatPedCabId: TIntegerField;
    QrFatPedCabRetiradaUsa: TSmallintField;
    QrFatPedCabRetiradaEnti: TIntegerField;
    QrFatPedCabEntregaUsa: TSmallintField;
    QrFatPedCabEntregaEnti: TIntegerField;
    QrFatPedCabL_Ativo: TSmallintField;
    QrFisRegCad: TMySQLQuery;
    QrFisRegCadCodigo: TIntegerField;
    QrFisRegCadCodUsu: TIntegerField;
    QrFisRegCadNome: TWideStringField;
    QrFisRegCadModeloNF: TIntegerField;
    QrFisRegCadNO_MODELO_NF: TWideStringField;
    QrFisRegCadFinanceiro: TSmallintField;
    DsFisRegCad: TDataSource;
    Panel3: TPanel;
    Panel2: TPanel;
    LaSerieNF: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    LaNumeroNF: TLabel;
    Label10: TLabel;
    SpeedButton21: TSpeedButton;
    Label26: TLabel;
    EdSerieNF: TdmkEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    EdNumeroNF: TdmkEdit;
    EddhEmiTZD: TdmkEdit;
    EddhEmiVerao: TdmkEdit;
    Panel5: TPanel;
    EdValFrete: TdmkEdit;
    Label1: TLabel;
    GroupBox6: TGroupBox;
    SbRegrFiscal: TSpeedButton;
    Label58: TLabel;
    Label208: TLabel;
    RG_idDest: TdmkRadioGroup;
    RG_indFinal: TdmkRadioGroup;
    EdindPres: TdmkEdit;
    EdindPres_TXT: TEdit;
    Edide_finNFe_TXT: TdmkEdit;
    Edide_finNFe: TdmkEdit;
    Label25: TLabel;
    EdRegrFiscal: TdmkEditCB;
    CBRegrFiscal: TdmkDBLookupComboBox;
    Label29: TLabel;
    EdFretePor: TdmkEditCB;
    CBFretePor: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEdit;
    Label2: TLabel;
    Label30: TLabel;
    EdTransporta: TdmkEditCB;
    CBTransporta: TdmkDBLookupComboBox;
    SbTransporta: TSpeedButton;
    PnNfeCabYA: TPanel;
    Panel8: TPanel;
    Label3: TLabel;
    Label14: TLabel;
    Label4: TLabel;
    EdvPag: TdmkEdit;
    EdvTroco: TdmkEdit;
    EdtPag: TdmkEdit;
    EdtPag_TXT: TdmkEdit;
    PnCard: TPanel;
    Label5: TLabel;
    Label17: TLabel;
    Label28: TLabel;
    Label6: TLabel;
    EdCNPJ: TdmkEdit;
    EdtpIntegra: TdmkEdit;
    EdtpIntegra_TXT: TdmkEdit;
    EdtBand: TdmkEdit;
    EdtBand_TXT: TdmkEdit;
    EdcAut: TdmkEdit;
    QrPsq: TMySQLQuery;
    CkEditaYA: TCheckBox;
    RGtpEmis: TdmkRadioGroup;
    EdCNPJCPFAvulso: TdmkEdit;
    LaCNPJCPFAvulso: TLabel;
    LaRazaoNomeAvulso: TLabel;
    EdRazaoNomeAvulso: TdmkEdit;
    QrFatPedCabInfIntermedEnti: TFloatField;
    Panel6: TPanel;
    Panel7: TPanel;
    Ckide_indIntermed: TdmkCheckBox;
    PnIntermediador: TPanel;
    Label7: TLabel;
    SbInfIntermedEnti: TSpeedButton;
    EdInfIntermedEnti: TdmkEditCB;
    CBInfIntermedEnti: TdmkDBLookupComboBox;
    CkEmiteAvulso: TdmkCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrFatPedNFs_AAfterScroll(DataSet: TDataSet);
    procedure QrFatPedNFs_BAfterScroll(DataSet: TDataSet);
    procedure SbRegrFiscalClick(Sender: TObject);
    procedure EdindPresChange(Sender: TObject);
    procedure EdindPresKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edide_finNFeChange(Sender: TObject);
    procedure Edide_finNFeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdValFreteChange(Sender: TObject);
    procedure SbTransportaClick(Sender: TObject);
    procedure EdtPagChange(Sender: TObject);
    procedure EdtPagKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdtpIntegraChange(Sender: TObject);
    procedure EdtpIntegraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtBandChange(Sender: TObject);
    procedure EdtBandKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbInfIntermedEntiClick(Sender: TObject);
    procedure Ckide_indIntermedClick(Sender: TObject);
    procedure CkEmiteAvulsoClick(Sender: TObject);
    procedure EdCNPJCPFAvulsoChange(Sender: TObject);
  private
    { Private declarations }
    F_indPres, F_finNFe: MyArrayLista;
    F_tPag, F_tpIntegra, F_tBand: MyArrayLista;
    FAgora: TDateTime;
    //
    function  IncluiNFs(): Boolean;
    function  AlteraNFAtual(): Boolean;
    function  Encerra2(var Cancelou: Boolean): Boolean;
    function  InsereNFeCabYA(): Boolean;
    procedure DefineTZD_UTC();

  public
    { Public declarations }
    FConfirmou: Boolean;
    //
{`POIU
    FFormChamou: TFormChamou;
    FThisFatID: Integer;
}
    FIDCtrl: Integer;
{
    // Configura��es cfe chamada:
    // F_Tipo=1 >> NF-e
    // F_Tipo=2 >> NFC-e
    // F_Tipo=102 >> CMPTOut(Blue Derm)
}
    F_Tipo, F_OriCodi, F_Empresa, F_ModeloNF, F_Cliente, F_EMP_FILIAL, F_AFP_Sit,
    (*F_Associada, F_ASS_FILIAL,*) F_EMP_CtaFaturas, (*F_ASS_CtaFaturas,*) F_CartEmis,
    F_CondicaoPG, F_EMP_FaturaDta, F_EMP_IDDuplicata, F_EMP_FaturaSeq,
    F_EMP_FaturaNum, F_TIPOCART, F_Represen, (*F_ASS_IDDuplicata, F_ASS_FaturaSeq,*)
    F_Financeiro(*, F_ASS_FaturaNum*): Integer;
    F_EMP_TpDuplicata, F_EMP_FaturaSep, F_EMP_TxtFaturas, (*F_ASS_FaturaSep,*)
    (*F_ASS_TpDuplicata, F_ASS_TxtFaturas,*) FTabela: String;
    F_AFP_Per: Double;
    F_Abertura: TDateTime;
    FEditaNFeCabYA_Depois: Boolean;
    //
    // FatPedNFs_XXXX
    FEMP_FILIAL, FCU_Pedido, FCliente, FEmpresa, FPediVda, FEntidadeTipo: Integer;
    FUFCli: String;
    F_CtbCadMoF, F_CtbPlaCta, F_tpNF: Integer;
    ///
    //FOEstoqueJaFoiBaixado: Boolean;  //  2020-10-24
    //
    FIDVolume, FTipo, FFatPedCab: Integer;
    function Confirma(): Boolean;
    procedure ReopenFatPedNFs_A((*QuaisFiliais, FilialLoc: Integer*));
    procedure ReopenParamsEmp(Empresa: Integer);
    // FatPedNFs_XXXX
    procedure ReopenFatPedCab(Id: Integer);
    function  ReopenFatPedNFs_B(Filial: Integer): Boolean;
    procedure ReopenNFeCabA();
    function  Step1_GeraNFCe(const Recria, ApenasGeraXML, CalculaAutomatico:
              Boolean; var ide_tpEmis: Integer): Boolean;
    function  Step2_Pagamentos(): Boolean;
    procedure ConfiguracoesIniciais();
    function  VerificaSeHaPagamentos(): Boolean;
    procedure ReopenCTG(Empresa: Integer);
    procedure ReopenImprime();
  end;

  var
  FmNFCeEmiss_0400: TFmNFCeEmiss_0400;

  const
  FThisFatID = VAR_FATID_0002; // Usar outro??? VAR_FATID_0011 ????
  F_Tipo1 = 2; // VAR_FATID_0002 e VAR_FATID_0001 ????

implementation

uses UnMyObjects, ModuleGeral, Module, ModuleNFe_0000, UMySQLModule, DmkDAC_PF,
  UnDmkProcFunc, NFe_PF, NFeCabA_0000, MyDBCheck, ModPediVda;

{$R *.DFM}

function TFmNFCeEmiss_0400.AlteraNFAtual: Boolean;
var
  Empresa, SerieNFCod, NumeroNF, IncSeqAuto, UF: Integer;
  {FreteVal, Seguro, Outros,} kgBruto, kgLiqui, dhEmiTZD, dhSaiEntTZD: Double;
  SerieNFTxt, PlacaUF, PlacaNr, Especie, Marca, Numero, Observacao, Quantidade,
  CFOP1, DtEmissNF, DtEntraSai, HrEmi, HrEntraSai, vagao, balsa, dest_email,
  Compra_XNEmp, Compra_XPed, Compra_XCont: String;
  //
  GenCtbD, GenCtbC: Integer;
begin
  if (DModG.QrCtrlGeralUsarFinCtb.Value = 1) and (F_Financeiro > 0) then
  begin
    GenCtbD := 0;
    GenCtbC := 0;
    Geral.MB_Info(
    'Contas cont�beis ficar�o zeradas nos lan�amentos financeiros!' + SLineBreak +
    'Para implementa��o, solicite � Dermatek!');
  end;
  Result := False;
  //usar QrFatPedNFs deste form!!!
  ReopenFatPedNFs_A((*1,0*));
  Empresa     := QrFatPedNFs_A.FieldByName('CO_ENT_EMP').AsInteger;
  SerieNFCod  := QrImprimeCO_SerieNF.Value;
  SerieNFTxt  := EdSerieNF.ValueVariant;
  IncSeqAuto  := QrImprimeIncSeqAuto.Value;
  NumeroNF    := EdNumeroNF.ValueVariant;
{
  FreteVal    := EdFreteVal.ValueVariant;
  Seguro      := EdSeguro.ValueVariant;
  Outros      := EdOutros.ValueVariant;
}
{`POIU
  kgBruto     := EdkgBruto.ValueVariant;
  kgLiqui     := EdkgLiqui.ValueVariant;
  PlacaUF     := EdPlacaUF.ValueVariant;
  PlacaNr     := EdPlacaNr.ValueVariant;
  Especie     := EdEspecie.ValueVariant;
  Marca       := EdMarca.ValueVariant;
  Numero      := EdNumero.ValueVariant;
  Quantidade  := EdQuantidade.ValueVariant;
  Observacao  := EdObservacao.ValueVariant;
  CFOP1       := '';//EdCFOP1.Text;
  DtEmissNF   := Geral.FDT(TPDtEmissNF.Date, 1);
  DtEntraSai  := Geral.FDT(TPDtEntraSai.Date, 1);
  // 2.00
  HrEntraSai  := EdHrEntraSai.Text;
  vagao       := Edvagao.Text;
  balsa       := Edbalsa.Text;
  dest_email  := Edvagao.Text;
  // fim 2.00
  Compra_XNEmp := EdCompra_XNEmp.Text;
  Compra_XPed  := EdCompra_XPed.Text;
  Compra_XCont := EdCompra_XCont.Text;
}
  // NFe 3.10
  HrEmi        := Geral.FDT(DmodG.ObtemAgora(), 100);
  dhEmiTZD     := EddhEmiTZD.ValueVariant;
{
  dhSaiEntTZD  := EddhSaiEntTZD.ValueVariant;
}
  kgBruto     := 0.000;
  kgLiqui     := 0.000;
  PlacaUF     := '';
  PlacaNr     := '';
  Especie     := '';
  Marca       := '';
  Numero      := '';
  Quantidade  := '';
  Observacao  := '';
  CFOP1       := '';
  DtEmissNF   := Geral.FDT(FAgora, 1);
  DtEntraSai  := DtEmissNF;
  // 2.00
  //HrEntraSai  := '00:00:00';
  HrEntraSai  := Geral.FDT(FAgora, 100);
  vagao       := '';
  balsa       := '';
  dest_email  := '';
  // fim 2.00
  Compra_XNEmp := '';
  Compra_XPed  := '';
  Compra_XCont := '';
  // NFe 3.10
  //HrEmi        := '00:00:00';
  HrEmi        := Geral.FDT(FAgora, 100);
  dhEmiTZD     := 0.000000;
  dhSaiEntTZD  := 0.000000;
  //
  if IncSeqAuto = 0 then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE paramsnfcs SET Sequencial=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Dmod.QrUpd.SQL.Add('AND Sequencial<:P2');
    Dmod.QrUpd.Params[00].AsInteger := NumeroNF;
    Dmod.QrUpd.Params[01].AsInteger := QrImprimeCtrl_nfs.Value;
    Dmod.QrUpd.Params[02].AsInteger := NumeroNF;
    Dmod.QrUpd.ExecSQL;
  end;
  if NumeroNF < 1 then
  begin
    Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
      FormatFloat('000000', NumeroNF));
    Exit;
  end;
  //ReopenQrImprime;
  if NumeroNF > QrImprimeMaxSeqLib.Value then
  begin
    Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
      FormatFloat('000000', NumeroNF));
    Exit;
  end;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovnfsa', False, [
    'Tipo', 'OriCodi', 'Empresa', 'NumeroNF', 'IncSeqAuto', 'SerieNFCod',
    'SerieNFTxt', {'FreteVal', 'Seguro', 'Outros',} 'PlacaUF', 'PlacaNr',
    'Especie', 'Marca', 'Numero', 'kgBruto', 'kgLiqui', 'Observacao',
    'Quantidade', 'CFOP1', 'DtEmissNF', 'DtEntraSai', 'infAdic_infCpl',
    'RNTC', 'UFEmbarq', 'xLocEmbarq',
    'HrEntraSai', 'vagao', 'balsa', 'dest_email',
    'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont',
    'dhEmiTZD', 'dhSaiEntTZD', 'HrEmi',
    'GenCtbD', 'GenCtbC'
  ], ['IDCtrl'], [
    F_Tipo1, F_OriCodi, Empresa, NumeroNF, IncSeqAuto, SerieNFCod,
    SerieNFTxt, {FreteVal, Seguro, Outros,} PlacaUF, PlacaNr,
    Especie, Marca, Numero, kgBruto, kgLiqui, Observacao,
    Quantidade, CFOP1, DtEmissNF, DtEntraSai, (*MeinfAdic_infCpl.Text*)'',
    (*EdRNTC.Text*)'', (*EdUFEmbarq.Text*)'', (*EdxLocEmbarq.Text*)'',
    HrEntraSai, vagao, balsa, dest_email,
    Compra_XNEmp, Compra_XPed, Compra_XCont,
    dhEmiTZD, dhSaiEntTZD, HrEmi,
    GenCtbD, GenCtbC
  ], [FIDCtrl], True) then
  begin
    Result := True;
  end;
end;

procedure TFmNFCeEmiss_0400.BtOKClick(Sender: TObject);
var
  Codigo, RegrFiscal, idDest, indFinal, indPres, indSinc, finNFe, modFrete,
  FretePor, Transporta, ide_tpEmis, Empresa: Integer;
  Frete_V: Double;
  SQLType: TSQLType;
  CNPJCPFAvulso, RazaoNomeAvulso: String;
  EmiteAvulso: Boolean;
  InfIntermedEnti: Integer;
  GenCtbD, GenCtbC: Integer;
begin
  if (DModG.QrCtrlGeralUsarFinCtb.Value = 1) and (F_Financeiro > 0) then
  begin
    GenCtbD := 0;
    GenCtbC := 0;
    Geral.MB_Info(
    'Contas cont�beis ficar�o zeradas nos lan�amentos financeiros!' + SLineBreak +
    'Para implementa��o, solicite � Dermatek!');
  end;
  Empresa       := EdEmPresa.ValueVariant;
  CNPJCPFAvulso := Geral.SoNumero_TT(EdCNPJCPFAvulso.Text);
  RazaoNomeAvulso := EdRazaoNomeAvulso.Text;
  EmiteAvulso := CkEmiteAvulso.Checked;
  //
  ide_tpEmis := Geral.IMV(RGtpEmis.Items[RGtpEmis.ItemIndex][1]);

  ReopenCTG(Empresa);
  //
  if ide_tpEmis <> 1 then
  begin
    if QrCTG.RecordCount = 0 then
    begin
      Geral.MB_Aviso('Empresa (' + Geral.FF0(Empresa) +
      ') em conting�ncia, mas sem configura��o de conting�ncia!' + sLineBreak +
      'Refa�a a configura��o de conting�ncia!');
      Exit;
    end;
  end else
  begin
    if QrCTG.RecordCount <> 0 then
    begin
      Geral.MB_Aviso('Empresa (' + Geral.FF0(Empresa) +
      ') em conting�ncia! Para emitir em modo normal NFCe encerre a conting�ncia! ' + sLineBreak +
      'A configura��o pode ser acessada na janela principal guia NF-e)' + sLineBreak +
      'Antes de encerrar a conting�ncia � necess�rio autorizar as NFC-es emitidas em conting�ncia enviando ao fisco!');
      Exit;
    end;
  end;
  //
  if PnNfeCabYA.Visible then
    if InsereNfeCabYA() = False then Exit;
  FEditaNFeCabYA_Depois := (PnNfeCabYA.Visible = False) or (CkEditaYA.Checked);
  //
  SQLType := stUpd;
  //
  if MyObjects.FIC(EdRegrFiscal.ValueVariant = 0, EdRegrFiscal,
    'Informe a movimenta��o (fiscal)!') then Exit;

  //if DmPediVda.QrParamsEmpPediVdaNElertas.Value = 0 then
  begin
    if MyObjects.FIC(RG_indFinal.ItemIndex <> 1, RG_indFinal,
      'Opera��o deve ser com o consumidor final!') then Exit;

    //{$IfNDef SemNFe_0000}
      if MyObjects.FIC(EdindPres.Text = '', EdindPres,
        'Informe o indicador de presen�a do comprador!') then Exit;
      indPres := Geral.IMV(EdindPres.Text);
      if MyObjects.FIC(not (indPres in [0,1,2,3,4,9]), EdindPres,
        'Informe o indicador de presen�a do comprador!') then Exit;

      if MyObjects.FIC(Edide_finNFe.Text = '', Edide_finNFe,
        'Informe a finalidade de emiss�o da NF-e!') then Exit;
      finNFe := Geral.IMV(Edide_finNFe.Text);
      if MyObjects.FIC(not (finNFe in [1,2,3,4]), Edide_finNFe,
        'Informe a finalidade de emiss�o da NF-e!') then Exit;
    //{$EndIf}
  end;

{
  if DmPediVda.QrFisRegCadFinanceiro.Value > 0 then
  begin
    if MyObjects.FIC(EdCartEmis.ValueVariant = 0, EdCartEmis,
      'Informe a carteira!') then Exit;
  end;
  // 2019-03-25 Erro! informando filial como vari�vel mas o certo � o c�digo da entidade!
  //DmPediVda.ReopenParamsEmp(EdEmpresa.ValueVariant, True); /
  DmPediVda.ReopenParamsEmp(DModG.QrEmpresasCodigo.Value, True);
  // Fim 2019-03-25
  case DmPediVda.QrParamsEmpFatSemPrcL.Value of
    0: if MyObjects.FIC(EdTabelaPrc.ValueVariant = 0, EdTabelaPrc,
    'Informe a tabela de pre�os!') then Exit;
    1: if MyObjects.FIC(EdTabelaPrc.ValueVariant = 0, EdTabelaPrc,
      'ATEN��O! N�o foi informada a tabela de pre�os!') then (*Exit*);
  end;
  //if MyObjects.FIC(EdFretePor.ValueVariant = 0, EdFretePor,
    //'Informe a o respons�vel pelo frete!') then Exit;
  if MyObjects.FIC((EdFretePor.ValueVariant > 1) and (DmPediVda.QrParamsEmpversao.Value < 2),
    EdFretePor, 'Tipo de frete inv�lido na vers�o atual da NFe! Selecione 0 ou 1') then Exit;
  //ErroFatura := FaturaParcialIncorreta(Txt);
  //if MyObjects.FIC(ErroFatura, CkAFP_Sit,
    //'Fatura parcial n�o permitida!' + sLineBreak + Txt) then Exit;
  //Valida regra fiscal com os dados do cliente
}
  if not UMyMod.ObtemCodigoDeCodUsu(EdRegrFiscal, RegrFiscal,
    'Informe a regra fiscal!', 'Codigo', 'CodUsu') then Exit;

{
  if DmPediVda.QrParamsEmpPediVdaNElertas.Value = 0 then
  begin
    if MyObjects.FIC(DmPediVda.QrClientesDOCENT.Value = '', EdCliente,
      'O Destinat�rio n�o possui CNPJ / CPF cadastrado!') then
    begin
      if Geral.MB_Pergunta('Deseja editar agora?' + sLineBreak +
        'AVISO: Verifique se os demais campos est�o devidamente preenchidos!') = ID_YES then
      begin
        MostraFormEntidade2(EdCliente.ValueVariant, DmPediVda.QrClientes,
          EdCliente, CBCliente);
      end;
      Exit;
    end;
    // NFe 3.10
    if MyObjects.FIC(RG_idDest.ItemIndex <= 0, RG_idDest,
      'Informe o Local de Destino da Opera��o!') then Exit;
    // NFe 3.10
    if not Entities.ValidaIndicadorDeIEEntidade(DmPediVda.QrClientesIE.Value,
      DmPediVda.QrClientesindIEDest.Value, DmPediVda.QrClientesTipo.Value, False,
      Mensagem) then
    begin
      Geral.MB_Aviso(Mensagem);
      Exit;
    end;
  end;
  if CkEntregaUsa.Checked = False then
  begin
    EdEntregaEnti.ValueVariant := 0;
    CBEntregaEnti.KeyValue := 0;
  end;
  if CkRetiradaUsa.Checked = False then
  begin
    EdRetiradaEnti.ValueVariant := 0;
    CBRetiradaEnti.KeyValue := 0;
  end;
}











  RegrFiscal := QrFisRegCadCodigo.Value;
  idDest     := RG_idDest.ItemIndex;
  indFinal   := RG_indFinal.ItemIndex;
  indPres    := Geral.IMV(EdindPres.Text);
  finNFe     := Edide_finNFe.ValueVariant;
  indSinc    := 1; // Sincrono
  Transporta := EdTransporta.ValueVariant;
  InfIntermedEnti := EdInfIntermedEnti.ValueVariant;
  Frete_V    := EdValFrete.ValueVariant;
  //
  // Erro 753
  // NFC-e com Frete e n�o � entrega a domic�lio
  //(tag:modFrete<>9 e indPres<>4)
  if not DModG.DefineFretePor(EdFretePor.ValueVariant, FretePor, modFrete) then
    Exit;
  if MyObjects.FIC((modFrete <> 9) and (indPres <> 4), EdFretePor,
  'NFC-e com Frete e n�o � entrega a domic�lio!') then Exit;
  //
  // Status = 786
  // Motivo = NFC-e de entrega a domicilio sem dados do Transportador
  // indPres = 4 => NFC-e em opera��o com entrega a domic�lio;
  if MyObjects.FIC((Transporta = 0) and (indPres = 4), EdFretePor,
  'Entrega a domicilio sem dados do Transportador!') then Exit;

  //
  Codigo     := FPediVda;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pedivda', False, [
  (*'CodUsu', 'Empresa', 'Cliente',
  'DtaEmiss', 'DtaEntra', 'DtaInclu',
  'DtaPrevi', 'Represen', 'Prioridade',
  'CondicaoPG', 'Moeda', 'Situacao',
  'TabelaPrc', 'MotivoSit', 'LoteProd',
  'PedidoCli',*) 'FretePor', 'Transporta',
  (*'Redespacho',*)
  'RegrFiscal',
  (*'DesoAces_V',
  'DesoAces_P',*) 'Frete_V', (*'Frete_P',
  'Seguro_V', 'Seguro_P', 'TotalQtd',
  'Total_Vlr', 'Total_Des', 'Total_Tot',
  'Observa', 'AFP_Sit', 'AFP_Per',
  'ComisFat', 'ComisRec', 'CartEmis',
  'QuantP', 'ValLiq', 'Tipo',
  'EntSai', 'CondPag', 'CentroCust',
  'ItemCust', 'UsaReferen',*)
  'idDest',
  'indFinal', 'indPres', 'IndSinc',
  'finNFe'
  (*, 'RetiradaUsa', 'RetiradaEnti',
  'EntregaUsa', 'EntregaEnti'*),
  'CNPJCPFAvulso', 'RazaoNomeAvulso',
  'EmiteAvulso',
  'InfIntermedEnti'], [
  'Codigo'], [
  (*CodUsu, Empresa, Cliente,
  DtaEmiss, DtaEntra, DtaInclu,
  DtaPrevi, Represen, Prioridade,
  CondicaoPG, Moeda, Situacao,
  TabelaPrc, MotivoSit, LoteProd,
  PedidoCli,*) FretePor, Transporta,
  (*Redespacho,*)
  RegrFiscal,
  (*DesoAces_V,
  DesoAces_P,*) Frete_V, (*Frete_P,
  Seguro_V, Seguro_P, TotalQtd,
  Total_Vlr, Total_Des, Total_Tot,
  Observa, AFP_Sit, AFP_Per,
  ComisFat, ComisRec, CartEmis,
  QuantP, ValLiq, Tipo,
  EntSai, CondPag, CentroCust,
  ItemCust, UsaReferen,*)
  idDest,
  indFinal, indPres, IndSinc,
  finNFe
  (*, RetiradaUsa, RetiradaEnti,
  EntregaUsa, EntregaEnti*),
  CNPJCPFAvulso, RazaoNomeAvulso,
  EmiteAvulso,
  InfIntermedEnti], [
  Codigo], True) then
  begin
    if Confirma() then
      Close
      ;
  end;
end;

procedure TFmNFCeEmiss_0400.BtSaidaClick(Sender: TObject);
begin
  FConfirmou := False;
  Close;
end;

procedure TFmNFCeEmiss_0400.CkEmiteAvulsoClick(Sender: TObject);
begin
  LaCNPJCPFAvulso.Enabled := CkEmiteAvulso.Checked = True;
  EdCNPJCPFAvulso.Enabled := CkEmiteAvulso.Checked = True;
  LaRazaoNomeAvulso.Enabled := CkEmiteAvulso.Checked = True;
  EdRazaoNomeAvulso.Enabled := CkEmiteAvulso.Checked = True;
end;

procedure TFmNFCeEmiss_0400.Ckide_indIntermedClick(Sender: TObject);
begin
  PnIntermediador.Visible := Ckide_indIntermed.Checked;
end;

procedure TFmNFCeEmiss_0400.ConfiguracoesIniciais();
begin
{$IfNDef SemNFe_0000}
  (*
  if ImgTipo.SQLType = stIns then
  begin
  Sempre atualiza para evitar erros
  *)
    //UnNFe_PF.Configura_idDest(DmPediVda.QrFatPedCabUFCli.Value

    // ini 2020-11-19
    //---------NFCe s� pode ser emitida em opera��es internas!------------------
    //UnNFCe_PF.Configura_idDest(EdEmpresa, RG_idDest, FUFCli);
    RG_idDest.ItemIndex := 1; // Operacao interna
    // Fim 2020-11-19

    //UnNFCe_PF.Configura_indFinal(FEntidadeTipo, RG_indFinal);
    RG_indFinal.ItemIndex     := 1; // Consumidor final
    EdindPres.ValueVariant    := 1; // Presencial
    Edide_finNFe.ValueVariant := 1; // Normal
    EdtPag.ValueVariant       := 1; // Dinheiro
    EdtpIntegra.ValueVariant  := 2; // Pagamento n�o integrado com o sistema de automa��o da empresa (Ex.: equipamento POS);

  (*
  end;
  *)
{$EndIf}
  //
  PnNfeCabYA.Visible := VerificaSeHaPagamentos() = False;
end;

function TFmNFCeEmiss_0400.Confirma(): Boolean;
var
  Continua: Boolean;
begin
  //ver horario de verao!
  FConfirmou := True;
  //
  ReopenParamsEmp(F_Empresa);
{`POIU
  if F_Tipo = VAR_FATID_0002 then
  begin
    //
    if QrParamsEmpCRT.Value = 1 then
    begin
      QrCSOSN.Close;
      QrCSOSN.Params[00].AsInteger := F_Tipo;
      QrCSOSN.Params[01].AsInteger := F_OriCodi;
      QrCSOSN.Params[02].AsInteger := F_EMpresa;
      UnDmkDAC_PF.AbreQuery(QrCSOSN, Dmod.MyDB);
      //
      if QrCSOSNItens.Value > 0 then
      begin
        Geral.MB_Aviso(//'Encerramento n�o realizado!' + sLineBreak +
          'Esta empresa � obridada pelo seu CST = 1 a informar o CSOSN ' +
          sLineBreak + 'para cada produto da NF. Existem ' + FormatFloat('0',
          QrCSOSNItens.Value) + ' item(ns) sem esta informa��o!');
        //
        Exit;
      end;
    end;
  end;
}
  if ImgTipo.SQLType = stIns then
    Continua := IncluiNFs()
  else
    Continua := AlteraNFAtual();
  //
  if Continua = False then
  begin
    DmNFe_0000.UpdSerieNFDesfeita(FTabela, F_OriCodi, EdSerieNF.ValueVariant,
      EdNumeroNF.ValueVariant);
    Close;
  end else
  begin
    ReopenFatPedCab((*QrFatPedCabId.Value*)0);
    Result := True;
  end;
end;

procedure TFmNFCeEmiss_0400.DefineTZD_UTC();
var
  Data: TDateTime;
  TZD_UTC: Double;
  hVerao: Boolean;
  SimNao: String;
  Cor: Integer;
begin
  // NFE 3.10
  FAgora := DModG.ObtemAgora();
  //Data := TPDtEmissNF.Date;
  Data := FAgora;
  hVerao := DModG.EstahNoHorarioDeVerao_e_TZD_JahCorrigido(Data, TZD_UTC);
  SimNao := dmkPF.EscolhaDe2Str(hVerao, 'SIM', 'N�O');
  Cor    := dmkPF.EscolhaDe2Int(hVerao, clRed, clBlue);
  EddhEmiVerao.Text := SimNao;
  EddhEmiVerao.Font.Color := Cor;
  EddhEmiTZD.Text := dmkPF.TZD_UTC_FloatToSignedStr(TZD_UTC);
{
  if TPDtEntraSai.Date <> TPDtEmissNF.Date then
  begin
    Data   := TPDtEntraSai.Date;
    hVerao := DModG.EstahNoHorarioDeVerao_e_TZD_JahCorrigido(Data, TZD_UTC);
    SimNao := dmkPF.EscolhaDe2Str(hVerao, 'SIM', 'N�O');
    Cor    := dmkPF.EscolhaDe2Int(hVerao, clRed, clBlue);
  end;
  EddhSaiEntTZD.Text := dmkPF.TZD_UTC_FloatToSignedStr(TZD_UTC);
  EddhSaiEntVerao.Text := SimNao;
  EddhSaiENtVerao.Font.Color := Cor;
}
end;

procedure TFmNFCeEmiss_0400.EdCNPJCPFAvulsoChange(Sender: TObject);
begin
  if (EdCNPJCPFAvulso.Text <> EmptyStr) and
  (EdRazaoNomeAvulso.Text = EmptyStr) then
    EdRazaoNomeAvulso.Text := 'Consumidor';
end;

procedure TFmNFCeEmiss_0400.Edide_finNFeChange(Sender: TObject);
begin
  Edide_finNFe_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_finNFe,  Edide_finNFe.ValueVariant);
end;

procedure TFmNFCeEmiss_0400.Edide_finNFeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    Edide_finNFe.Text := Geral.SelecionaItem(F_finNFe, 0,
      'SEL-LISTA-000 :: Finalidade de emiss�o da NF-e',
      TitCols, Screen.Width)
  end;
end;

procedure TFmNFCeEmiss_0400.EdindPresChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdindPres.Text, F_indPres, Texto, 0, 1);
  EdindPres_TXT.Text := Texto;
end;

procedure TFmNFCeEmiss_0400.EdindPresKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdindPres.Text := Geral.SelecionaItem(F_indPres, 0,
    'SEL-LISTA-000 :: Indicador de presen�a do comprador no estabelecimento comercial no momento da opera��o',
    TitCols, Screen.Width)
  end;
end;

procedure TFmNFCeEmiss_0400.EdtBandChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdtBand.Text, F_tBand, Texto, 0, 1);
  EdtBand_TXT.Text := Texto;
end;

procedure TFmNFCeEmiss_0400.EdtBandKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdtBand.Text := Geral.SelecionaItem(F_tBand, 0,
    'SEL-LISTA-000 :: Bandeira da operadora de cart�o',
    TitCols, Screen.Width)
  end;
end;

procedure TFmNFCeEmiss_0400.EdtPagChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdtPag.Text, F_tPag, Texto, 0, 1);
  EdtPag_TXT.Text := Texto;
  //
{
  if EdtPag.Text = '90' then
    EdvPag.ValueVariant := 0
  else
    EdvPag.ValueVariant := FvPag;
}
end;

procedure TFmNFCeEmiss_0400.EdtPagKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdtPag.Text := Geral.SelecionaItem(F_tPag, 0,
    'SEL-LISTA-000 :: Meio de Pagamento',
    TitCols, Screen.Width)
  end;
end;

procedure TFmNFCeEmiss_0400.EdtpIntegraChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdtpIntegra.Text, F_tpIntegra, Texto, 0, 1);
  EdtpIntegra_TXT.Text := Texto;
end;

procedure TFmNFCeEmiss_0400.EdtpIntegraKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdtpIntegra.Text := Geral.SelecionaItem(F_tpIntegra, 0,
    'SEL-LISTA-000 :: Tipo de Integra��o para pagamento',
    TitCols, Screen.Width)
  end;
end;

procedure TFmNFCeEmiss_0400.EdValFreteChange(Sender: TObject);
begin
  if (EdValFrete.ValueVariant > 0) then
  begin
    if (EdFretePor.ValueVariant = 9) then
    begin
      EdFretePor.ValueVariant := 0;
      CBFretePor.KeyValue     := 0;
      //
    end;
    EdIndPres.ValueVariant := 4;
    //
    if EdTransporta.ValueVariant = 0 then
    begin
      EdTransporta.ValueVariant := EdEmpresa.ValueVariant;
      CBTransporta.KeyValue     := EdEmpresa.ValueVariant;
    end;
  end;
end;

function TFmNFCeEmiss_0400.Encerra2(var Cancelou: Boolean): Boolean;
var
  DataFat, DataEnc: TDateTime;
  Agora: String;
  Filial: Integer;
  T1, T2, F1, F2, V1, V2, P1, P2: Double;
  FreteVal, Seguro, Outros: Double;
begin
  Result := False;
  Cancelou := False;
  //;
  Filial   := 0;
  FreteVal := 0;
  Seguro   := 0;
  Outros   := 0;
  //ShowMessage(IntToStr(F_Tipo));
  Result := False;
  if not (F_Tipo in ([1,2,102,103])) then
  begin
    Geral.MB_Erro('Tipo n�o definido para encerramento! AVISE A DERMATEK!');
    Exit;
  end;
  // Encerra Faturamento
  //if Geral.MB_Pergunta('Confirma o encerramento do faturamento?') = ID_YES then
  begin
    try
      Screen.Cursor := crHourGlass;
      DataEnc := DModG.ObtemAgora();
      Agora := Geral.FDT(DataEnc, 105);
      (*
      case F_EMP_FaturaDta of
        0:
        begin
          MyObjects.CriaForm_AcessoTotal(TFmGetData, FmGetData);
          FmGetData.TPData.Date := Date;
          FmGetData.OcultaJurosEMulta();
          FmGetData.ShowModal;
          FmGetData.Destroy;
          DataFat := VAR_GETDATA;
        end;
        1: DataFat := F_Abertura;
        2: DataFat := DataEnc;
        else DataFat := 0;
      end;
      *)
      (*2:*) DataFat := DataEnc;
      if DataFat = 0 then
      begin
        Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
        'Data de faturamento n�o definida!');
        Exit;
      end else
      begin
        // Alterar data de emiss�o da NF
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'stqmovnfsa', False, [
          'DtEmissNF'], ['Tipo', 'OriCodi'
        ], [Geral.FDT(DataFat, 1)], [F_Tipo1, F_OriCodi], True);
      end;
      //
      {
      Filial := DmFatura.ObtemFilialDeEntidade(F_Empresa);
      //
      if not UnNFe_PF.InsUpdFaturasNFe(Filial, F_Empresa, FThisFatID, F_Tipo,
        F_OriCodi, F_EMP_CtaFaturas, F_Associada, F_ASS_CtaFaturas, F_ASS_FILIAL,
        F_Financeiro, F_CartEmis, F_CondicaoPG, F_AFP_Sit, F_EMP_FaturaNum,
        F_EMP_IDDuplicata, EdNumeroNF.ValueVariant, F_EMP_FaturaSeq, F_TIPOCART,
        F_Represen, F_ASS_IDDuplicata, F_ASS_FaturaSeq, F_Cliente,
        F_AFP_Per, FreteVal, Seguro, 0, Outros, F_EMP_TpDuplicata,
        F_EMP_FaturaSep, F_EMP_TxtFaturas, F_ASS_FaturaSep,
        F_ASS_TpDuplicata, F_ASS_TxtFaturas, DataFat, QrPrzT, QrSumT, QrPrzX,
        QrSumX, QrNF_X) then Exit;
      //
      // Ativa itens no movimento
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa SET Ativo=1');
      Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0 AND OriCodi=:P1 ');
      Dmod.QrUpd.Params[00].AsInteger := F_Tipo;
      Dmod.QrUpd.Params[01].AsInteger := F_OriCodi;
      Dmod.QrUpd.ExecSQL;
      }
      //
      // Encerra definivamente faturamento
      case F_Tipo of
        1, 2: // NF-e, NFC-e
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE fatpedcab SET ');
          Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
          Dmod.QrUpd.Params[00].AsString  := Agora;
          Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
          Dmod.QrUpd.Params[02].AsInteger := F_OriCodi;
          Dmod.QrUpd.ExecSQL;
        end;
        102: //CMPTOut
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE cmptout SET ');
          Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
          Dmod.QrUpd.Params[00].AsString  := Agora;
          Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
          Dmod.QrUpd.Params[02].AsInteger := F_OriCodi;
          Dmod.QrUpd.ExecSQL;
        end;
        103: //NFeMPInn
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE nfempinn SET ');
          Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0, DataFat=:P1 ');
          Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2 ');
          Dmod.QrUpd.Params[00].AsString  := Agora;
          Dmod.QrUpd.Params[01].AsString  := Geral.FDT(DataFat, 105);
          Dmod.QrUpd.Params[02].AsInteger := F_OriCodi;
          Dmod.QrUpd.ExecSQL;
        end;
        else
          Geral.MB_Erro('Encerramento sem finaliza��o! AVISE A DERMATEK!');
      end;
      //
      Result := True;
    finally
      Screen.Cursor := crDefault;
    end;
(*
  end else
  begin
    Cancelou := True;
    Exit;
*)
  end;
end;

procedure TFmNFCeEmiss_0400.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFCeEmiss_0400.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FConfirmou := False;
  DefineTZD_UTC();
  CBFretePor.ListSource := DmPediVda.DsFretePor;
  UnDmkDAC_PF.AbreQuery(QrFisRegCad, Dmod.MyDB);
  if DmPediVda.QrTransportas.State = dsInactive then
    DmPediVda.ReopenTransportas();
  CBTransporta.ListSource := DmPediVda.DsTransportas;
  if DmPediVda.QrInfIntermedEnti.State = dsInactive then
    DmPediVda.ReopenInfIntermedEnti();
  CBInfIntermedEnti.ListSource := DmPediVda.DsInfIntermedEnti;
{$IfNDef SemNFe_0000}
  F_indPres   := UnNFe_PF.ListaIndicadorDePresencaComprador();
  F_finNFe    := UnNFe_PF.ListaFinalidadeDeEmiss�oDaNFe(True);
  F_tPag      := UnNFe_PF.ListaMeiosDePagamento();
  F_tpIntegra := UnNFe_PF.ListaTiposDeIntegracaoParaPagamento();
  F_tBand     := UnNFe_PF.ListaBandeirasDaOperadoraDeCartao();
{$Else}
  F_indPres   := 0;
  F_finNFe    := 0;
  F_tPag      := 0;
  F_tpIntegra := 0;
  F_tBand     := 0;
{$EndIf}
end;

procedure TFmNFCeEmiss_0400.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmNFCeEmiss_0400.IncluiNFs(): Boolean;
var
  Tentativa: Integer;
  //
  procedure ExcluiNF();
  begin
    // excluir nfe stqmovnfsa para n�o gerar erro quando tentar de novo
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM stqmovnfsa ');
    Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0 AND OriCodi=:P1');
    Dmod.QrUpd.Params[00].AsInteger := F_Tipo1;
    Dmod.QrUpd.Params[01].AsInteger := F_OriCodi;
    Dmod.QrUpd.ExecSQL;
    //
{
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM nfecabya ');
    Dmod.QrUpd.SQL.Add('WHERE FatID=:P0 AND FatNum=:P1 AND Empresa=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FThisFatID;
    Dmod.QrUpd.Params[01].AsInteger := FFatPedCab;
    Dmod.QrUpd.Params[02].AsInteger := FEmpresa;
    Dmod.QrUpd.ExecSQL;
}
    //
    if Tentativa = 1 then
      Geral.MB_Erro('Erro no encerramento!' + sLineBreak +
      'Os dados foram exclu�dos e o processo ser� repetido novamente!' + sLineBreak +
      'Caso n�o ocorra, AVISE A DERMATEK.')
    else
      Close;
  end;
var
  IDCtrl, Empresa, SerieNFCod, NumeroNF, IncSeqAuto, UF, emit_CRT: Integer;
  {FreteVal, Seguro, Outros,} kgBruto, kgLiqui, dhEmiTZD, dhSaiEntTZD: Double;
  SerieNFTxt, PlacaUF, PlacaNr, Especie, Marca, Numero, Observacao, Quantidade,
  CFOP1, DtEmissNF, DtEntraSai, HrEntraSai, vagao, balsa, dest_email,
  Compra_XNEmp, Compra_XPed, Compra_XCont, HrEmi: String;
  Incluir, Encerrou, Cancelou: Boolean;
  GenCtbD, GenCtbC: Integer;

  procedure TentaIncluir(var Conseguiu: Boolean);
  begin
    Conseguiu := False;
    FAgora := DModG.ObtemAgora();
    //
    //usar QrFatPedNFs deste form!!!
    Encerrou    := False;
    Empresa     := QrFatPedNFs_A.FieldByName('CO_ENT_EMP').AsInteger;
    SerieNFCod  := QrImprimeCO_SerieNF.Value;
    SerieNFTxt  := EdSerieNF.ValueVariant;
    IncSeqAuto  := QrImprimeIncSeqAuto.Value;
    NumeroNF    := EdNumeroNF.ValueVariant;
    {
    FreteVal    := EdFreteVal.ValueVariant;
    Seguro      := EdSeguro.ValueVariant;
    Outros      := EdOutros.ValueVariant;
    }
  {`POIU
    kgBruto     := EdkgBruto.ValueVariant;
    kgLiqui     := EdkgLiqui.ValueVariant;
    PlacaUF     := EdPlacaUF.ValueVariant;
    PlacaNr     := EdPlacaNr.ValueVariant;
    Especie     := EdEspecie.ValueVariant;
    Marca       := EdMarca.ValueVariant;
    Numero      := EdNumero.ValueVariant;
    Quantidade  := EdQuantidade.ValueVariant;
    Observacao  := EdObservacao.ValueVariant;
    CFOP1       := '';//EdCFOP1.Text;
    DtEmissNF   := Geral.FDT(TPDtEmissNF.Date, 1);
    DtEntraSai  := Geral.FDT(TPDtEntraSai.Date, 1);

    // 2.00
    HrEntraSai  := EdHrEntraSai.Text;
    vagao       := Edvagao.Text;
    balsa       := Edbalsa.Text;
    dest_email  := Eddest_email.Text;
    emit_CRT    := RGCRT.ItemIndex;
    if MyObjects.FIC(emit_CRT < 1, RGCRT, 'Informe o CRT!') then Exit;
    // fim 2.00
    Compra_XNEmp := EdCompra_XNEmp.Text;
    Compra_XPed  := EdCompra_XPed.Text;
    Compra_XCont := EdCompra_XCont.Text;
  }
    //
  {
    if Trim(PlacaUF) <> '' then
    begin
      UF := MLAGeral.GetCodigoUF_da_SiglaUF(Geral.SoLetra_TT(PlacaUF));
      if UF = 0 then
      begin
        if Geral.MB_Pergunta('A UF ' + PlacaUF + ' n�o � reconhecida ' +
          'pelo aplicativo como uma UF v�lida! Deseja continuar assim mesmo?') <> ID_YES
        then
          Exit;
      end;
    end;
    PlacaNr := Trim(PlacaNr);
    //
    if PlacaNr <> '' then
    begin
      if not MLAGeral.PlacaDetranValida(PlacaNr, False) then
      begin
        if Geral.MB_Pergunta('A placa ' + PlacaNr + ' n�o � reconhecida ' +
          'pelo aplicativo como uma placa v�lida! Deseja continuar assim mesmo?') <> ID_YES
        then
          Exit;
      end;
    end;
  }
    emit_CRT    := QrParamsEmpCRT.Value;
    //
    kgBruto     := 0.000;
    kgLiqui     := 0.000;
    PlacaUF     := '';
    PlacaNr     := '';
    Especie     := '';
    Marca       := '';
    Numero      := '';
    Quantidade  := '';
    Observacao  := '';
    CFOP1       := '';
    DtEmissNF   := Geral.FDT(FAgora, 1);
    DtEntraSai  := DtEmissNF;
    // 2.00
    //HrEntraSai  := '00:00:00';
    HrEntraSai  := Geral.FDT(FAgora, 100);
    vagao       := '';
    balsa       := '';
    dest_email  := '';
    // fim 2.00
    Compra_XNEmp := '';
    Compra_XPed  := '';
    Compra_XCont := '';
    // NFe 3.10
    HrEmi        := Geral.FDT(FAgora, 100);
    dhEmiTZD     := EddhEmiTZD.ValueVariant;
    dhSaiEntTZD  := dhEmiTZD;
    //
    //
    if IncSeqAuto = 0 then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE paramsnfcs SET Sequencial=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
      Dmod.QrUpd.SQL.Add('AND Sequencial<:P2');
      Dmod.QrUpd.Params[00].AsInteger := NumeroNF;
      Dmod.QrUpd.Params[01].AsInteger := QrImprimeCtrl_nfs.Value;
      Dmod.QrUpd.Params[02].AsInteger := NumeroNF;
      Dmod.QrUpd.ExecSQL;
    end;
    if NumeroNF < 1 then
    begin
      Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
        FormatFloat('000000', NumeroNF));
      Exit;
    end;
    //
    if NumeroNF > QrImprimeMaxSeqLib.Value then
    begin
      Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
        FormatFloat('000000', NumeroNF));
      Exit;
    end;
    IDCtrl  := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
    try
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovnfsa', False, [
        'Tipo', 'OriCodi', 'Empresa', 'NumeroNF', 'IncSeqAuto', 'SerieNFCod',
        'SerieNFTxt', {'FreteVal', 'Seguro', 'Outros',} 'PlacaUF', 'PlacaNr',
        'Especie', 'Marca', 'Numero', 'kgBruto', 'kgLiqui', 'Observacao',
        'Quantidade', 'CFOP1', 'DtEmissNF', 'DtEntraSai', 'infAdic_infCpl',
        'RNTC', 'UFEmbarq', 'xLocEmbarq',
        'HrEntraSai', 'vagao', 'balsa', 'dest_email', 'emit_CRT',
        'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont',
        'dhEmiTZD', 'dhSaiEntTZD', 'HrEmi',
        'GenCtbD', 'GenCtbC'
      ], ['IDCtrl'], [
        F_Tipo1, F_OriCodi, Empresa, NumeroNF, IncSeqAuto, SerieNFCod,
        SerieNFTxt, {FreteVal, Seguro, Outros,} PlacaUF, PlacaNr,
        Especie, Marca, Numero, kgBruto, kgLiqui, Observacao,
        Quantidade, CFOP1, DtEmissNF, DtEntraSai, (*MeinfAdic_infCpl.Text*)'',
        (*EdRNTC.Text*)'', (*EdUFEmbarq.Text*)'', (*EdxLocEmbarq.Text*)'',
        HrEntraSai, vagao, balsa, dest_email, emit_CRT,
        Compra_XNEmp, Compra_XPed, Compra_XCont,
        dhEmiTZD, dhSaiEntTZD, HrEmi,
        GenCtbD, GenCtbC
      ], [IDCtrl], True) then
      begin
  {
        ReopenFatPedNFs((*2,0*));
        if QrFatPedNFs.RecordCount > 0 then
        begin
          while not QrFatPedNFs.Eof do
          begin
            Incluir := True;
            //
            if not DmNFe_0000.Obtem_Serie_e_NumNF_Novo(
            (*QrFatPedCabSerieDesfe.Value*)-1,
            (*QrFatPedCabNFDesfeita.Value*)0,
            QrImprimeCO_SerieNF.Value,
            QrImprimeCtrl_nfs.Value,
            QrFatPedNFs.FieldByName('CO_ENT_EMP').AsInteger,
            QrFatPedNFs.FieldByName('Filial').AsInteger,
            QrImprimeMaxSeqLib.Value,
            EdSerieNF, EdNumeroNF(*),
            SerieNFTxt, NumeroNF*)) then
            begin
              // ????
            end;
  (*
            SerieNFTxt := IntToStr(EdSerieNF.ValueVariant);
            NumeroNF   := DModG.BuscaProximoCodigoInt('paramsnfcs', 'Sequencial',
            'WHERE Controle=' + dmkPF.FFP(QrImprimeCtrl_nfs.Value, 0), 0,
            QrImprimeMaxSeqLib.Value, 'S�rie: ' + SerieNFTxt + sLineBreak +
            'Filial: ' + dmkPF.FFP(QrFatPedNFsFilial.Value, 0));
  *)
            //
            Empresa     := QrFatPedNFs.FieldByName('CO_ENT_EMP').AsInteger;
            SerieNFCod  := QrImprimeCO_SerieNF.Value;
            IncSeqAuto  := QrImprimeIncSeqAuto.Value;
            //NumeroNF    := EdNumeroNF.ValueVariant;
            (*
            FreteVal    := 0;
            Seguro      := 0;
            Outros      := 0;
            *)
            kgBruto     := 0;
            kgLiqui     := 0;
            PlacaUF     := '';
            PlacaNr     := '';
            Especie     := '';
            Marca       := '';
            Numero      := '';
            Quantidade  := '';
            Observacao  := '';
            CFOP1       := '';
            //
            if IncSeqAuto = 0 then
            begin
              Dmod.QrUpd.SQL.Clear;
              Dmod.QrUpd.SQL.Add('UPDATE paramsnfcs SET Sequencial=:P0');
              Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
              Dmod.QrUpd.SQL.Add('AND Sequencial<:P2');
              Dmod.QrUpd.Params[00].AsInteger := NumeroNF;
              Dmod.QrUpd.Params[01].AsInteger := QrImprimeCtrl_nfs.Value;
              Dmod.QrUpd.Params[02].AsInteger := NumeroNF;
              Dmod.QrUpd.ExecSQL;
            end;
            if NumeroNF < 1 then
            begin
              Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
                FormatFloat('000000', NumeroNF));
              Incluir := False;
            end;
            if Incluir and (NumeroNF > QrImprimeMaxSeqLib.Value) then
            begin
              Geral.MB_Aviso('N�mero inv�lido para a Nota Fiscal:' + sLineBreak +
                FormatFloat('000000', NumeroNF));
              Incluir := False;
            end;
            if Incluir then
            begin
              try
                IDCtrl  := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
                if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovnfsa', False, [
                'Tipo', 'OriCodi', 'Empresa', 'NumeroNF', 'IncSeqAuto', 'SerieNFCod',
                'SerieNFTxt', (*'FreteVal', 'Seguro', 'Outros',*) 'PlacaUF', 'PlacaNr',
                'Especie', 'Marca', 'Numero', 'kgBruto', 'kgLiqui', 'Observacao',
                'Quantidade', 'CFOP1', 'infAdic_infCpl', 'RNTC',
                'UFEmbarq', 'xLocEmbarq',
                'HrEntraSai', 'vagao', 'balsa', 'dest_email',
                'Compra_XNEmp', 'Compra_XPed', 'Compra_XCont',
                'dhEmiTZD', 'dhSaiEntTZD', 'HrEmi'
                ], ['IDCtrl'], [
                F_Tipo, F_OriCodi, Empresa, NumeroNF, IncSeqAuto, SerieNFCod,
                SerieNFTxt, (*FreteVal, Seguro, Outros,*) PlacaUF, PlacaNr,
                Especie, Marca, Numero, kgBruto, kgLiqui, Observacao,
                Quantidade, CFOP1, (*MeinfAdic_infCpl.Text,*)'', (*EdRNTC.Text,*)'',
                (*EdUFEmbarq.Text,*)'', (*EdxLocEmbarq.Text,*)'',
                HrEntraSai, vagao, balsa, dest_email,
                Compra_XNEmp, Compra_XPed, Compra_XCont,
                dhEmiTZD, dhSaiEntTZD, HrEmi
                ], [IDCtrl], True) then
                begin
                //
                end;
              except
                ExcluiNF();
                raise;
              end;
              //
            end else
              ExcluiNF();
            QrFatPedNFs.Next;
          end;
        end;
  }
        //
        //if FFormChamou = fcFmFatPedCab then
        begin
          Cancelou := False;
          Encerrou := Encerra2(Cancelou);
        end;
        if not Cancelou then
        begin
          if Encerrou then
          begin
            ReopenFatPedNFs_A((*1,0*));
            Result := True;
          end;
        end else
          Exit;
      end;
      Conseguiu := True;
    except
      if Tentativa = 1 then ExcluiNF()
      else
        raise;
    end;
  end;
var
 _Conseguiu: Boolean;
begin
  if (DModG.QrCtrlGeralUsarFinCtb.Value = 1) and (F_Financeiro > 0) then
  begin
    GenCtbD := 0;
    GenCtbC := 0;
    Geral.MB_Info(
    'Contas cont�beis ficar�o zeradas nos lan�amentos financeiros!' + SLineBreak +
    'Para implementa��o, solicite � Dermatek!');
  end;
  Tentativa := 1;
  TentaIncluir(_Conseguiu);
  if _Conseguiu = False then
  begin
    Tentativa := 2;
    TentaIncluir(_Conseguiu);
    Result := _Conseguiu;
  end;
  Result := _Conseguiu;
end;

function TFmNFCeEmiss_0400.InsereNFeCabYA(): Boolean;
var
  FatID, FatNum, Empresa, Controle, tPag, tpIntegra, tBand: Integer;
  vPag, vTroco: Double;
  CNPJ, cAut: String;
begin
  Result := False;
  //
  FatID    := FThisFatID;
  FatNum   := FFatPedCab;
  Empresa  := FEmpresa;
  //
  //Controle := EdControle.ValueVariant;
  Controle := 0;
  //
  tPag      := Geral.IMV(EdtPag.ValueVariant);
  vPag      := EdvPag.ValueVariant;
  vTroco    := EdvTroco.ValueVariant;
  tpIntegra := Geral.IMV(EdtpIntegra.ValueVariant);
  CNPJ      := EdCNPJ.ValueVariant;
  tBand     := Geral.IMV(EdtBand.ValueVariant);
  cAut      := EdcAut.ValueVariant;
  //
  if MyObjects.FIC(FatID = 0, nil, 'FatID n�o informado!') then Exit;
  if MyObjects.FIC(FatNum = 0, nil, 'FatNum n�o informado!') then Exit;
  if MyObjects.FIC(Empresa = 0, nil, 'Empresa n�o informada!') then Exit;
  if MyObjects.FIC(tPag = 0, EdtPag, 'Informe o meio de pagamento!') then Exit;
  if MyObjects.FIC(tpIntegra = 0,  EdtpIntegra, 'Tipo de Integra��o para pagamento!') then Exit;
  //
  if ImgTipo.SQLType = stIns then
    //EdControle.ValueVariant :=
    Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfecaby', '', 0);
  //
  //Controle := EdControle.ValueVariant;
  //
  Result :=  UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfecabya', False,
    ['tPag', 'vPag', 'vTroco', 'tpIntegra', 'CNPJ', 'tBand', 'cAut'],
    ['FatID', 'FatNum', 'Empresa', 'Controle'],
    [tPag, vPag, vTroco, tpIntegra, CNPJ, tBand, cAut],
    [FatID, FatNum, Empresa, Controle], True);(* then
  begin
    DmNFe_0000.TotaisNFe(FatID, FatNum, Empresa, FmNFeCabA_0000.LaAviso1,
      FmNFeCabA_0000.LaAviso2, FmNFeCabA_0000.REWarning);
    FmNFeCabA_0000.LocCod(FmNFeCabA_0000.QrNFeCabAIDCtrl.Value,
      FmNFeCabA_0000.QrNFeCabAIDCtrl.Value);
    //
    Close;
  end;*)
end;

procedure TFmNFCeEmiss_0400.QrFatPedNFs_AAfterScroll(DataSet: TDataSet);
begin
  ReopenImprime();
end;

procedure TFmNFCeEmiss_0400.QrFatPedNFs_BAfterScroll(DataSet: TDataSet);
begin
  ReopenImprime();
  ReopenNFeCabA();
end;

procedure TFmNFCeEmiss_0400.ReopenCTG(Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCTG, Dmod.MyDB, [
  'SELECT IF(Empresa=0, 1, 0) ORDEM,   ',
  'tpEmis, dhEntrada, Nome ',
  'FROM nfecntngnc ',
  'WHERE dhSaida < 2 ',
  'AND Empresa=' + FormatFloat('0', Empresa),
  'AND tpEmis=' + FormatFloat('0', 9), // Conting�ncia off-line NFCe
  'ORDER BY ORDEM, dhEntrada DESC ',
  '']);
end;

procedure TFmNFCeEmiss_0400.ReopenFatPedCab(Id: Integer);
//var
  //Empresa: Integer;
begin
  QrFatPedCab.Close;
{
  if EdFilial.ValueVariant = 0 then
    Exit;
  //
  Empresa := QrFiliaisCodigo.Value;
}
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFatPedCab, Dmod.MyDB, [
    'SELECT val.Id, ppc.MedDDSimpl, fpc.Codigo, fpc.CodUsu, fpc.Encerrou, ',
    'IF(emp.Tipo=0,emp.RazaoSocial,emp.Nome) NO_EMP, ',
    'IF(emp.Tipo=0,emp.ECodMunici,emp.PCodMunici) + 0.000 CODMUNICI, ',
    'ufe.Nome UF_TXT_emp, ufc.Nome UF_TXT_cli, ',
    'IF(cli.Tipo=0,cli.RazaoSocial,cli.Nome) NO_CLI, ',
    'frc.ModeloNF + 0.000 ModeloNF, frc.infAdFisco, frc.Financeiro,  ',
    'SUM(val.Total) TotalFatur, frc.TipoMov tpNF,  frc.TpCalcTrib, ',
    'pvd.DtaEmiss, pvd.DtaEntra, pvd.DtaInclu, pvd.DtaPrevi,  ',
    'pvd.RegrFiscal + 0.000 RegrFiscal, pvd.CartEmis + 0.000  ',
    'CartEmis , pvd.TabelaPrc + 0.000 TabelaPrc,  ',
    'pvd.CondicaoPG + 0.000 CondicaoPG, pvd.FretePor,  ',
    'pvd.Transporta + 0.000 Transporta, pvd.Empresa + 0.000  ',
    'Empresa, pvd.Cliente + 0.000 Cliente, pvd.PedidoCli, ',
    // NFe 3.10
    'pvd.idDest, pvd.indFinal, pvd.indPres, pvd.finNFe, pvd.indSinc, ',
    // NFe 4.00 NT 2018/5
    'pvd.RetiradaUsa, pvd.RetiradaEnti, pvd.EntregaUsa, pvd.EntregaEnti, ',
    'pvd.InfIntermedEnti + 0.000 InfIntermedEnti, ',
    'cli.L_Ativo ',
    // Fim NFe 4.00 NT 2018/5
    //
    'FROM stqmovvala val  ',
    'LEFT JOIN fatpedcab fpc ON fpc.Codigo = val.OriCodi  ',
    'LEFT JOIN pedivda pvd ON pvd.Codigo=fpc.Pedido ',
    'LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal ',
    'LEFT JOIN entidades emp ON emp.Codigo=pvd.Empresa ',
    'LEFT JOIN entidades cli ON cli.Codigo=pvd.Cliente ',
    'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPg ',
    'LEFT JOIN ufs ufe ON ufe.Codigo=IF(emp.Tipo=0,emp.EUF,emp.PUF) ',
    'LEFT JOIN ufs ufc ON ufc.Codigo=IF(cli.Tipo=0,cli.EUF,cli.PUF) ',
    'WHERE fpc.Encerrou > 0 ',
    'AND pvd.Empresa=' + FormatFloat('0', FEmpresa),
    Geral.ATS_If(FCU_Pedido <> 0,
      ['AND pvd.CodUsu=' + FormatFloat('0', FCU_Pedido)]),
    Geral.ATS_If(FCliente <> 0,
      ['AND pvd.Cliente=' + FormatFloat('0', FCliente)]),
    //
    'GROUP BY val.OriCodi ',
    'ORDER BY Encerrou DESC ',
    '']);
  if Id <> 0 then
    QrFatPedCab.Locate('Id', Id, []);
end;

procedure TFmNFCeEmiss_0400.ReopenFatPedNFs_A((*QuaisFiliais, FilialLoc: Integer*));
(*
var
  Txt: String;
begin
  if QuaisFiliais = 1 then
    Txt := '='
  else
    Txt := '<>';
  //
*)
begin
  QrFatPedNFs_A.Close;
  QrFatPedNFs_A.SQL.Clear;
  QrFatPedNFs_A.SQL.Add('SELECT DISTINCT ent.Filial, ent.Codigo CO_ENT_EMP,');
  QrFatPedNFs_A.SQL.Add('IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_EMP, smna.*');
  QrFatPedNFs_A.SQL.Add('FROM stqmovvala smva');
  QrFatPedNFs_A.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=smva.Empresa');
  QrFatPedNFs_A.SQL.Add('LEFT JOIN stqmovnfsa smna ON smna.Empresa=smva.Empresa');
  QrFatPedNFs_A.SQL.Add('          AND smva.Tipo=' + FormatFloat('0', F_Tipo1));
  QrFatPedNFs_A.SQL.Add('          AND smna.OriCodi=' + FormatFloat('0', F_OriCodi));
  QrFatPedNFs_A.SQL.Add('WHERE smva.Tipo=' + FormatFloat('0', F_Tipo1));
  QrFatPedNFs_A.SQL.Add('AND smva.OriCodi=' + FormatFloat('0', F_OriCodi));
  //QrFatPedNFs_A.SQL.Add('AND ent.Codigo' + Txt +  FormatFloat('0', F_Empresa));
  QrFatPedNFs_A.SQL.Add('AND ent.Codigo=' + FormatFloat('0', F_Empresa));
  QrFatPedNFs_A.SQL.Add('ORDER BY ent.Filial');
  QrFatPedNFs_A.SQL.Add('');
  //
  UnDmkDAC_PF.AbreQuery(QrFatPedNFs_A, Dmod.MyDB);
  //
  //QrFatPedNFs.Locate('Filial', FilialLoc, []);
end;

function TFmNFCeEmiss_0400.ReopenFatPedNFs_B(Filial: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFatPedNFs_B, Dmod.MyDB, [
  'SELECT nfe.FatID CabA_FatID, nfe.Status, nfe.infProt_cStat, ',
  'nfe.infCanc_cStat, nfe.LoteEnv, ',
  'ent.Filial, ent.Codigo CO_ENT_EMP, nfe.ide_tpNF, smna.* ',
  'FROM stqmovnfsa smna ',
  'LEFT JOIN entidades ent ON ent.Codigo=smna.Empresa ',
  'LEFT JOIN nfecaba nfe ON nfe.IDCtrl=smna.IDCtrl ',
  'WHERE smna.Tipo=' + Geral.FF0(F_Tipo1),//Geral.FF0(FThisFatID),
  'AND smna.OriCodi=' + Geral.FF0(QrFatPedCabCodigo.Value),
  'AND smna.Empresa=' + Geral.FF0(Trunc(QrFatPedCabEmpresa.Value)),
  'ORDER BY ent.Filial ',
  '']);
  //
  Result := QrFatPedNFs_B.Locate('Filial', Filial, []);
end;

procedure TFmNFCeEmiss_0400.ReopenNFeCabA();
begin
  QrNFeCabA.Close;
  QrNFeCabA.Params[00].AsInteger := FThisFatID;
  QrNFeCabA.Params[01].AsInteger := QrFatPedCabCodigo.Value;
  QrNFeCabA.Params[02].AsInteger := FEmpresa; //QrFatPedNFs_AEmpresa.Value;
  QrNFeCabA.Params[03].AsInteger := QrFatPedNFs_BNumeroNF.Value;
  UMyMod.AbreQuery(QrNFeCabA, Dmod.MyDB, 'TFmNFCeEmiss_0400.ReopenNFeCabA()');
end;

procedure TFmNFCeEmiss_0400.ReopenParamsEmp(Empresa: Integer);
begin
  QrParamsEmp.Close;
  QrParamsEmp.Params[0].AsInteger := Empresa;
  UnDmkDAC_PF.AbreQuery(QrParamsEmp, Dmod.MyDB);
end;

procedure TFmNFCeEmiss_0400.ReopenImprime();
begin
  QrImprime.Close;
{
  if QrFatPedNFs.FieldByName('CO_ENT_EMP').AsInteger = F_Empresa then
  begin
}



(*
    QrImprime.Params[00].AsInteger := F_Empresa;
    QrImprime.Params[01].AsInteger := F_ModeloNF;
    UnDmkDAC_PF.AbreQuery(QrImprime, Dmod.MyDB);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrImprime, Dmod.MyDB, [
  'SELECT nfs.SerieNF CO_SerieNF, imp.TipoImpressao, ',
  'nfs.SerieNF SerieNF_Normal, nfs.Sequencial, nfs.IncSeqAuto,  ',
  'nfs.Controle  Ctrl_nfs, nfs.MaxSeqLib ',
  'FROM imprimeempr ime ',
  'LEFT JOIN imprime imp ON imp.Codigo=ime.Codigo ',
  'LEFT JOIN paramsnfcs nfs ON nfs.Controle=ime.ParamsNFCs ',
  'WHERE ime.Empresa=' + Geral.FF0(F_Empresa),
  'AND ime.Codigo=' + Geral.FF0(F_ModeloNF),
  '']);
  //Geral.MB_Teste(QrImprime.SQL.Text);



{
  end else begin
    ReopenParamsEmp(F_Empresa);
    //
    if QrParamsEmpAssocModNF.Value > 0 then
    begin
      QrImprime.Params[00].AsInteger := QrParamsEmpAssociada.Value;
      QrImprime.Params[01].AsInteger := QrParamsEmpAssocModNF.Value;
      UnDmkDAC_PF.AbreQuery(QrImprime, Dmod.MyDB);
    end;
  end;
}
end;

procedure TFmNFCeEmiss_0400.SbInfIntermedEntiClick(Sender: TObject);
begin
  DmPediVda.MostraFormEntidade2(EdInfIntermedEnti.ValueVariant, DmPediVda.QrInfIntermedEnti,
    EdInfIntermedEnti, CBInfIntermedEnti);
end;

procedure TFmNFCeEmiss_0400.SbRegrFiscalClick(Sender: TObject);
begin
// Fazer?
end;

procedure TFmNFCeEmiss_0400.SbTransportaClick(Sender: TObject);
begin
  DmPediVda.MostraFormEntidade2(EdTransporta.ValueVariant, DmPediVda.QrTransportas,
    EdTransporta, CBTransporta);
end;

function TFmNFCeEmiss_0400.Step1_GeraNFCe(const Recria, ApenasGeraXML,
  CalculaAutomatico: Boolean; var ide_tpEmis: Integer): Boolean;
//function T F m F a t P e d N F s _ 0 4 0 0.GerarNFe(Recria, ApenasGeraXML, CalculaAutomatico: Boolean): Boolean;
const
  sProcName = 'TFmNFCeEmiss_0400.Step1_GeraNFCe()';
  EditCabGA = False;
var
  ide_serie: Variant;
  ide_dEmi, ide_dSaiEnt: TDateTime;
  FatNum, Empresa, Cliente, ide_nNF, ide_tpNF,
  modFrete, Transporta, InfIntermedEnti, NFeStatus, FretePor: Integer;
  //
  IDCtrl: Integer;
  //
  CodTXT, EmpTXT, infAdic_infAdFisco, infAdic_infCpl,
  VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
  Exporta_UFEmbarq, Exporta_xLocEmbarq,
  SQL_ITS_ITS, SQL_ITS_TOT, SQL_FAT_ITS, SQL_PGT_ITS, SQL_FAT_TOT, SQL_VOLUMES,
  SQL_CUSTOMZ: String;
  //
  FreteVal, Seguro, Outros: Double;
  GravaCampos, RegrFiscal, CartEmiss, TabelaPrc, CondicaoPg, cNF_Atual,
  Financeiro: Integer;
  //
  UF_TXT_Emp, UF_TXT_Cli: String;
  //
  //2.00
  ide_hSaiEnt: TTime;
  ide_dhCont: TDateTime;
  emit_CRT: Integer;
  ide_xJust, dest_email, Vagao, Balsa: String;
  // fim 2.00
  Compra_XNEmp, Compra_XPed, Compra_XCont: String;
  // NFe 3.10
  idDest, indFinal, indPres, finNFe: Integer;
  ide_hEmi: TTime;
  ide_dhEmiTZD, ide_dhSaiEntTZD: Double;
  // Fim NFe 3.10
  // NFe 4.00 NT 2018/5
  RetiradaUsa, RetiradaEnti, EntregaUsa, EntregaEnti, L_Ativo: Integer;
  // Fim NFe 4.00 NT 2018/5
  // NFCe
  CNPJCPFAvulso, RazaoNomeAvulso: String;
  EmiteAvulso: Boolean;
begin
  CNPJCPFAvulso := Geral.SoNumero_TT(EdCNPJCPFAvulso.Text);
  RazaoNomeAvulso := EdRazaoNomeAvulso.Text;
  ReopenFatPedNFs_B(QrFatPedNFs_BFilial.Value);
  EmiteAvulso := CkEmiteAvulso.Checked;
  //
  Result     := False;
  FatNum     := QrFatPedCabCodigo.Value;
  Empresa    := QrFatPedNFs_BEmpresa.Value;
  IDCtrl     := QrFatPedNFs_BIDCtrl.Value;
  FreteVal   := QrFatPedNFs_BFreteVal.Value;
  Seguro     := QrFatPedNFs_BSeguro.Value;
  Outros     := QrFatPedNFs_BOutros.Value;
  RegrFiscal := Trunc(QrFatPedCabRegrFiscal.Value);
  CartEmiss  := Trunc(QrFatPedCabCartEmis.Value);
  TabelaPrc  := Trunc(QrFatPedCabTabelaPrc.Value);
  CondicaoPg := TrunC(QrFatPedCabCondicaoPG.Value);
  Financeiro := QrFatPedCabFinanceiro.Value;
  // NFe 3.10
  idDest     := QrFatPedCabidDest.Value;
  indFinal   := QrFatPedCabindFinal.Value;
  indPres    := QrFatPedCabindPres.Value;
  finNFe     := QrFatPedCabfinNFe.Value;
  // NFe 4.00 NT 2018/5
  RetiradaUsa  := QrFatPedCabRetiradaUsa.Value;
  RetiradaEnti := QrFatPedCabRetiradaEnti.Value;
  EntregaUsa   := QrFatPedCabEntregaUsa.Value;
  EntregaEnti  := QrFatPedCabEntregaEnti.Value;
  L_Ativo      := QrFatPedCabL_Ativo.Value;
  // Fim NFe 4.00 NT 2018/5
  //
  if not DmodG.QrFiliLog.Locate('Codigo', Empresa, []) then
  begin
    Geral.MB_Aviso('Empresa (' + Geral.FF0(Empresa) +
      ') diferente da logada (' + VAR_LIB_EMPRESAS +
      ')! � necess�rio logoff!');
    Exit;
  end;
  //
  //FEMP_FILIAL, FCU_Pedido, FCliente, FEmpresa
  //if Geral.IMV(EdFilial.Text) = 0 then
  if FEMP_FILIAL = 0 then
  begin
    Geral.MB_Aviso('Empresa n�o definida!');
    Exit;
  end;
  //if MyObjects.FIC(EdCliente.ValueVariant = 0, EdCliente, 'Cliente n�o definido!') then
  if MyObjects.FIC(FCliente = 0, nil, 'Cliente n�o definido!') then
    Exit;
  if not DModG.DefineFretePor(QrFatPedCabFretePor.Value, FretePor, modFrete) then
    Exit;
  //
  Cliente     := FCliente; //QrClientesCodigo.Value;
  Transporta  := Trunc(QrFatPedCabTransporta.Value);
  InfIntermedEnti := Trunc(QrFatPedCabInfIntermedEnti.Value);
  ide_Serie   := QrFatPedNFs_BSerieNFTxt.Value;
  ide_nNF     := QrFatPedNFs_BNumeroNF.Value;
  ide_dEmi    := QrFatPedNFs_BDtEmissNF.Value;
  ide_dSaiEnt := QrFatPedNFs_BDtEntraSai.Value;
  ide_tpNF    := QrFatPedCabtpNF.Value; // 1 = Sa�da
  // NFe 3.10
  ide_dhEmiTZD    := QrFatPedNFs_BdhEmiTZD.Value;
  ide_dhSaiEntTZD := QrFatPedNFs_BdhSaiEntTZD.Value;
  if QrFatPedNFs_B.FieldByName('HrEmi').AsString = EMptyStr then
    ide_hEmi      := 0
  else
    ide_hEmi        := QrFatPedNFs_BHrEmi.Value;
  //
  // Nao Resolveu o erro!
   //ide_hEmi        := QrFatPedNFs_B.FieldByName('HrEmi').AsFloat;

  //ide_tpImp   :=  1-Retrato/ 2-Paisagem
  // 2020-11-01
  // Fazer aqui conting�ncia off-line!
  //ide_tpEmis  := 1;
  //ide_tpEmis  := QrParamsEmpNFetpEmis.Value;
  ide_tpEmis := Geral.IMV(RGtpEmis.Items[RGtpEmis.ItemIndex][1]);

  case ide_tpEmis of
    1:
    begin
      ide_dhCont  := 0;
      ide_xJust   := '';
    end;
    9: // Conting�ncia off-line NFC-e
    begin
      ReopenCTG(Empresa);
      //
      if QrCTG.RecordCount = 0 then
      begin
        Geral.MB_Aviso('Empresa (' + Geral.FF0(Empresa) +
        ') em conting�ncia, mas sem configura��o de conting�ncia!' + sLineBreak +
        'Refa�a a configura��o de conting�ncia!');
        Exit;
      end else
      begin
        ide_dhCont  := QrCTGdhEntrada.Value;
        ide_xJust   := QrCTGNome.Value;
      end;
    end;
    else Geral.MB_Erro('ide_tpEmis n�o implementado em ' + sProcName);
  end;

  //
(*
  if ide_tpEmis <> 1 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrCTG, Dmod.MyDB, [
    'SELECT IF(Empresa=0, 1, 0) ORDEM,   ',
    'tpEmis, dhEntrada, Nome ',
    'FROM nfecntngnc ',
    'WHERE dhSaida < 2 ',
    'AND Empresa = 0 ',
    'OR Empresa=' + FormatFloat('0', Empresa),
    'ORDER BY ORDEM, dhEntrada DESC ',
    '']);
    if QrCTG.RecordCount = 0 then
    begin
      Geral.MB_Aviso('Empresa (' + Geral.FF0(Empresa) +
      ') em conting�ncia, mas sem configura��o de conting�ncia!' + sLineBreak +
      'Refa�a a configura��o de conting�ncia!');
      Exit;
    end else
    begin
      ide_dhCont  := QrCTGdhEntrada.Value;
      ide_xJust   := QrCTGNome.Value;
    end;
  end else
  begin
    ide_dhCont  := 0;
    ide_xJust   := '';
  end;
   1 � Normal � emiss�o normal;
   2 � Conting�ncia FS � emiss�o em conting�ncia com impress�o do DANFE em Formul�rio de Seguran�a;
   3 � Conting�ncia SCAN � emiss�o em conting�ncia no Sistema de Conting�ncia do Ambiente Nacional � SCAN;
   4 � Conting�ncia DPEC - emiss�o em conting�ncia com envio da Declara��o Pr�via de Emiss�o em Conting�ncia � DPEC;
   5 � Conting�ncia FS-DA - emiss�o em conting�ncia com impress�o do DANFE em Formul�rio de Seguran�a para Impress�o de Documento Auxiliar de Documento Fiscal Eletr�nico (FS-DA).
   6 = Conting�ncia SVC-AN (SEFAZ Virtual de Conting�ncia do AN);
   7 = Conting�ncia SVC-RS (SEFAZ Virtual de Conting�ncia do RS);
   9 = Conting�ncia off-line da NFC-e (as demais op��es de conting�ncia s�o v�lidas tamb�m para a NFC-e).
       Para a NFC-e somente est�o dispon�veis e s�o v�lidas as op��es de conting�ncia 5 e 9.
*)

  infAdic_infAdFisco := QrFatPedCabinfAdFisco.Value;
  infAdic_infCpl     := QrFatPedNFs_BinfAdic_infCpl.Value;
  VeicTransp_Placa   := QrFatPedNFs_BPlacaNr.Value;
  VeicTransp_UF      := QrFatPedNFs_BPlacaUF.Value;
  VeicTransp_RNTC    := QrFatPedNFs_BRNTC.Value;
  Exporta_UFEmbarq   := QrFatPedNFs_BUFEmbarq.Value;
  Exporta_XLocEmbarq := QrFatPedNFs_BxLocEmbarq.Value;
  //
  CodTXT      := dmkPF.FFP(QrFatPedCabCodigo.Value, 0);
  EmpTXT      := dmkPF.FFP(QrFatPedCabEmpresa.Value, 0);
  UF_TXT_emp  := QrFatPedCabUF_TXT_emp.Value;
  UF_TXT_cli  := QrFatPedCabUF_TXT_cli.Value;
  //SQL_ITS_ITS := DmNFe_0000.SQL_ITS_ITS_Prod1_Padrao(FThisFatID, FatNum, Empresa);
  SQL_ITS_ITS := DmNFe_0000.SQL_ITS_ITS_Prod1_Padrao(F_Tipo1, FatNum, Empresa);
{
  'SELECT med.Sigla NO_UNIDADE, smva.Empresa, smva.Preco,          'sLineBreak +
  'smva.ICMS_Per, smva.IPI_Per, smva.GraGruX CU_PRODUTO,           'sLineBreak +
  'smva.ID CONTROLE, smva.InfAdCuztm, smva.PercCustom,             'sLineBreak +
  'SUM(smva.Total) Total, SUM(smva.ICMS_Val) ICMS_Val,             'sLineBreak +
  'SUM(smva.IPI_Val) IPI_Val, SUM(smva.Qtde) Qtde, smva.CFOP,      'sLineBreak +
  'gg1.CodUsu CU_GRUPO, gg1.Nome NO_GRUPO,                         'sLineBreak +
  'gg1.CST_A, gg1.CST_B,                                           'sLineBreak +
  'CONCAT(gg1.CST_A, LPAD(gg1.CST_B, 2, "0"))  CST_T,              'sLineBreak +
  'gg1.NCM, ncm.Letras,  gg1.IPI_CST, gg1.IPI_cEnq,                'sLineBreak +
  'ggc.GraCorCad, gcc.CodUsu CU_COR, gcc.Nome NO_COR,              'sLineBreak +
  'gti.Nome NO_TAM, ggx.GraGru1 CO_GRUPO, 0 Desc_Per,              'sLineBreak +
  'gg1.InfAdProd, smva.MedidaC, smva.MedidaL,                      'sLineBreak +
  'smva.MedidaA,smva.MedidaE,                                      'sLineBreak +
  'mor.Medida1, mor.Medida2, mor.Medida3, mor.Medida4,             'sLineBreak +
  'mor.Sigla1, mor.Sigla2, mor.Sigla3, mor.Sigla4,                 'sLineBreak +
  'gta.PrintTam, gcc.PrintCor                                      'sLineBreak +
  'FROM stqmovvala smva                                            'sLineBreak +
  'LEFT JOIN gragrux ggx ON ggx.Controle=smva.GraGruX              'sLineBreak +
  'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1               'sLineBreak +
  'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC             'sLineBreak +
  'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad             'sLineBreak +
  'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI             'sLineBreak +
  'LEFT JOIN gratamcad gta ON gta.Codigo=gti.Codigo                'sLineBreak +
  'LEFT JOIN ncms ncm ON ncm.ncm=gg1.ncm                           'sLineBreak +
  'LEFT JOIN unidmed med ON med.Codigo=gg1.UnidMed                 'sLineBreak +
  'LEFT JOIN medordem mor ON mor.Codigo=smva.MedOrdem              'sLineBreak +
  'WHERE smva.Tipo = 1                                             'sLineBreak +
  'AND smva.OriCodi=' + CodTXT +                                    sLineBreak +
  'AND smva.Empresa=' + EmpTXT +                                    sLineBreak +
  'GROUP BY smva.Preco, smva.ICMS_Per, smva.IPI_Per, smva.GraGruX  ';
  //
  {
  SQL_CUSTOMZ :=
  'SELECT gg1.SiglaCustm                                           'sLineBreak +
  'FROM pedivdacuz pvc                                             'sLineBreak +
  'LEFT JOIN gragrux   ggx ON ggx.Controle=pvc.GraGruX             'sLineBreak +
  'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1               'sLineBreak +
  'WHERE pvc.Controle=:P0                                          'sLineBreak +
  'AND gg1.SiglaCustm <> "";
  }
  //SQL_ITS_TOT := DmNFe_0000.SQL_ITS_TOT_Prod1_Padrao(FThisFatID, FatNum, Empresa);
  SQL_ITS_TOT := DmNFe_0000.SQL_ITS_TOT_Prod1_Padrao(F_Tipo1, FatNum, Empresa);
  {
  'SELECT SUM(smva.Total) Total, SUM(smva.ICMS_Val) ICMS_Val,      'sLineBreak +
  'SUM(smva.IPI_Val) IPI_Val, SUM(smva.Qtde) Qtde,                 'sLineBreak +
  'COUNT(DISTINCT OriCnta) + 0.000 Volumes                         'sLineBreak +
  'FROM stqmovvala smva                                            'sLineBreak +
  'WHERE smva.Tipo = 1                                             'sLineBreak +
  'AND smva.OriCodi=' + CodTXT +                                    sLineBreak +
  'AND smva.Empresa=' + EmpTXT;
  }
  //
  //SQL_FAT_ITS := DmNFe_0000.SQL_FAT_ITS_Prod1_Padrao(FThisFatID, FatNum, Empresa);
  SQL_FAT_ITS := DmNFe_0000.SQL_FAT_ITS_Prod1_Padrao(F_Tipo1, FatNum, Empresa);
  {
  'SELECT FatID, FatNum, Vencimento, Credito Valor,                'sLineBreak +
  'FatParcela, Duplicata                                           'sLineBreak +
  'FROM lan ctos                                                    'sLineBreak +
  'WHERE FatID = 1                                                 'sLineBreak +
  'AND FatNum=' + CodTXT +                                          sLineBreak +
  'AND CliInt=' + EmpTXT;
  }
  //SQL_FAT_TOT :=  DmNFe_0000.SQL_FAT_TOT_Prod1_Padrao(FThisFatID, FatNum, Empresa);
  SQL_FAT_TOT :=  DmNFe_0000.SQL_FAT_TOT_Prod1_Padrao(F_Tipo1, FatNum, Empresa);
  //SQL_VOLUMES :=  DmNFe_0000.SQL_VOLUMES_Prod1_Padrao(FThisFatID, FatNum, Empresa);
  SQL_VOLUMES :=  DmNFe_0000.SQL_VOLUMES_Prod1_Padrao(F_Tipo1, FatNum, Empresa);
  {
  'SELECT Quantidade qVol, Especie esp, Marca marca,               'sLineBreak +
  'Numero nVol, kgLiqui pesoL, kgBruto pesoB                       'sLineBreak +
  'FROM stqmovnfsa                                                 'sLineBreak +
  'WHERE Tipo = 1                                                  'sLineBreak +
  'AND OriCodi=' + CodTXT +                                         sLineBreak +
  'AND Empresa=' + EmpTXT;
  }
(*
  NFeStatus := QrNFeCabAStatus.Value;
  if DBCheck.CriaFm(TFmNFeSteps_ 0 1 1 0 , FmNFeSteps_ 0 1 1 0 , afmoNegarComAviso) then
  begin
    FmNFeSteps_ 0 1 1 0 .FSource_IDCtrl := 'stqmovnfsa';
    FmNFeSteps_ 0 1 1 0 .Show;
    //
*)
    {
    GravaCampos := Geral.MB_Pergunta('Deseja salvar demonstrativo ' +
    ' no banco de dados para verifica��o de dados em relat�rio? ' + sLineBreak +
    'Este demostrativo n�o � obrigat�rio e sua grava��o pode ser demorada!' +
    sLineBreak + 'Deseja salvar assim mesmo?');
    }
    cNF_Atual := QrNFeCabAide_cNF.Value;
    GravaCampos := ID_NO;
(*
    if GravaCampos <> ID_CANCEL then
      FmNFeSteps_xxxx.Cria NFe Normal(Recria, NFeStatus, FThisFatID, FatNum, Empresa,
        IDCtrl, Cliente, FretePor, modFrete, Transporta,
        RegrFiscal, FreteVal, Seguro, Outros, ide_Serie,
        ide_nNF, ide_dEmi, ide_dSaiEnt, ide_tpNF, ide_tpEmis,
        infAdic_infAdFisco, infAdic_infCpl,
        VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
        Exporta_UFEmbarq, Exporta_xLocEmbarq,
        SQL_ITS_ITS, SQL_ITS_TOT, SQL_FAT_ITS, SQL_FAT_TOT, SQL_VOLUMES,
        SQL_CUSTOMZ, UF_TXT_Emp, UF_TXT_Cli, GravaCampos, cNF_Atual, Financeiro)
    else
      Geral.MB_Aviso('Gera��o total da NF-e cancelada pelo usu�rio!');
    //
    FmNFeSteps_XXXX.Destroy;
    //
*)
  // 2.00
  ide_hSaiEnt := QrFatPedNFs_BHrEntraSai.Value;
  emit_CRT    := QrFatPedNFs_Bemit_CRT.Value;
  dest_email  := QrFatPedNFs_Bdest_EMail.Value;
  Vagao       := QrFatPedNFs_BVagao.Value;
  Balsa       := QrFatPedNFs_BBalsa.Value;
  // fim 2.00
  Compra_XNEmp := QrFatPedNFs_BCompra_XNEmp.Value;
  Compra_XPed  := QrFatPedNFs_BCompra_XPed.Value;
  Compra_XCont := QrFatPedNFs_BCompra_XCont.Value;
  //
  NFeStatus := 0;  // T� certo???
  if UnNFe_PF.CriaNFe_vXX_XX(Recria, NFeStatus, FThisFatID, FatNum, Empresa,
       IDCtrl, Cliente, FretePor, modFrete, Transporta, 0,
       RegrFiscal, CartEmiss, TabelaPrc, CondicaoPg, FreteVal, Seguro, Outros,
       ide_Serie, ide_nNF, ide_dEmi, ide_dSaiEnt, ide_tpNF, ide_tpEmis,
       infAdic_infAdFisco, infAdic_infCpl,
       VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
       Exporta_UFEmbarq, Exporta_xLocEmbarq,
       SQL_ITS_ITS, SQL_ITS_TOT, SQL_FAT_ITS, SQL_FAT_TOT, SQL_VOLUMES,
       SQL_CUSTOMZ, UF_TXT_Emp, UF_TXT_Cli, GravaCampos, cNF_Atual, Financeiro,
       ide_hSaiEnt, ide_dhCont, ide_xJust, emit_CRT, dest_email, Vagao, Balsa,
       Compra_XNEmp, Compra_XPed, Compra_XCont,
       // NFe 3.10
       idDest, ide_hEmi, ide_dhEmiTZD, ide_dhSaiEntTZD, indFinal, indPres, finNFe,
       // NFe 4.00 NT 2018/5
       RetiradaUsa, RetiradaEnti, EntregaUsa, EntregaEnti, L_Ativo,
       // Fim NFe 4.00 NT 2018/5
       //
       ApenasGeraXML, CalculaAutomatico,
       CNPJCPFAvulso, RazaoNomeAvulso, EmiteAvulso,
       // Quiet
       EditCabGA, InfIntermedEnti)
  then
    Result := ReopenFatPedNFs_B(QrFatPedNFs_BFilial.Value)
  else
    Result := False;
(*
  end;
*)
  (*
  DESATIVADO EM: 05/04/2017
  MOTIVO: Estava excluindo as altera��es nas faturas e ao recriar estava zarando as altera��es

  if ApenasGeraXML then
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      DmNFe_0000.AtualizaLctoFaturaNFe(FThisFatID, FatNum, Empresa);
    finally
      Screen.Cursor := crDefault
    end;
  end;
  *)
end;


function TFmNFCeEmiss_0400.Step2_Pagamentos(): Boolean;
var
  ide_tpEmis: Integer;
begin
  if DBCheck.CriaFm(TFmNFeCabA_0000, FmNFeCabA_0000, afmoNegarComAviso) then
  begin
    if not FmNFeCabA_0000.LocCod(QrNFeCabAIDCtrl.Value, QrNFeCabAIDCtrl.Value) then
      Geral.MB_Erro('ERRO!, N�o foi poss�vel localizar o ID ' +
        FormatFloat('0', QrNFeCabAIDCtrl.Value) + ' localize manualmente!');
    FmNFeCabA_0000.FMostraSubForm := TSubForm.tsfDadosPagamento;
    FmNFeCabA_0000.ShowModal;
    FmNFeCabA_0000.Destroy;
    //
    Screen.Cursor := crHourGlass;
    try
      Step1_GeraNFCe(True, True, True, ide_tpEmis);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

function TFmNFCeEmiss_0400.VerificaSeHaPagamentos(): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsq, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM nfecabya ',
  'WHERE FatID=' + Geral.FF0(FThisFatID),
  'AND FatNum=' + Geral.FF0(FFatPedCab),
  'AND Empresa=' + Geral.FF0(FEmpresa),
  '']);
  Result := (QrPsq.RecordCount > 0) and (QrPsq.FieldByName('Controle').AsInteger <> 0);
end;

end.
