unit NFCe_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, ComCtrls, Registry, Printers,
  CommCtrl, Consts, Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral,
  UnDmkEnums, UnMsgInt, mySQLDbTables, DB,(* DbTables,*) DmkCoding, dmkEdit,
  dmkRadioGroup, dmkMemo, NFe_PF,
  //(*SHA-1*)DCPsha1, DCPTwoFish, (* fim SHA-1*)  ver se precisa!
  dmkCheckGroup, dmkPageControl, UnAppEnums;

type
  TNFCe_PF = class(TObject)
  private
    { Private declarations }
    procedure AvisoNaoImplemVerNFCe(Versao: Integer);
    procedure MostraFormNFCeEdit_0400(FatSemEstq, FatPedCab, PediVda, IDVolume,
              Tipo, RegrFiscal: Integer; Tabela: String; ValFrete: Double;
              UFCli: String; EntidadeTipo: Integer; ValorTotal: Double;
              OEstoqueJaFoiBaixado: Boolean);
    function  VersaoNFCeEmUso(): Integer;

    procedure ReopenFPC1(FatPedCab: Integer);



  public
    { Public declarations }
    procedure Configura_idDest(EdEmpresa: TdmkEdit; RG_idDest: TdmkRadioGroup;
              UF_RmDs: String);
    procedure Configura_indFinal(Entidade_Tipo: Integer; RG_indFinal: TdmkRadioGroup);
    procedure EncerraFaturamento(FatSemEstq, FatPedCab, PediVda, IDVolume, OriTipo,
              RegrFiscal: Integer; Tabela: String; ValFrete: Double; UFCli: String;
              EntidadeTipo: Integer; ValorTotal: Double; OEstoqueJaFoiBaixado: Boolean);
    function  DefineVarsFormNFCeEmiss_XXXX(): Boolean;
    function  HashQrCode(tpEmis: Integer; ChaveNFe: String; VersaoQrCode, tpAmb:
              Integer; CSCPos, CSC: String; Data: TDateTime; Valor: Double;
              DigestValue: String): String;
    function  ObtemDadosCSC(var CSC, CSCpos: String): Boolean;
    function  MostraFormNFCeLEnU(Form: TForm; ide_tpEmis, StatusAchieved: Integer): Boolean;

  end;

var
  UnNFCe_PF: TNFCe_PF;
//const
  //CO_MSG_VERSAO_NFE2 = 'A vers�o: 2.00 da NF-e n�o est� dispon�vel para este aplicativo!';

implementation
uses
  ModuleGeral, MyDBCheck, ModProd, ModPediVda, DmkDAC_PF, UnDmkProcFunc, Module,
  UMySQLModule, unMyObjects, UnFinanceiro, NFaInfCpl, UnDmkWeb,
  NFeInut_0000, ModuleNFe_0000{,
  (*NFeValidaXML_0000,*) NFeValidaXML_0001,
  NFe_Pesq_0000, NFeCabA_0000,
  NFeXMLGerencia, NFeCabGA,
  NFeEveRLoE, NFeExportaXML_B, NFeNewVer, NFeExportaXML, NFeLoad_Inn,
  NFeCntngnc, NFeLoad_Web, NFeJust,
  NFeLoad_Dir, NFeLoad_Arq, NFeInfCpl,
  NFe_Pesq_0000_ImpLista, NFe_Pesq_0000_ImpFat, NFeXMLData, NFe_LocEPesq,
  ///////////////////////////////////// NFe ////////////////////////////////////
  NFeCabXReb_0000, NFeCabXVol_0000, NFeCabXLac_0000,
  NFeCabZCon_0000, NFeCabZFis_0000, NFeCabZPro_0000,
  NFeDesDowC_0100,
  //NFeValidaXML_0110,
  //
  NFeSteps_0400, FatPedNFs_0400, NFaEdit_0400, NFeItsIDI_0400,
  NFeItsIDIa_0400, NFeLEnU_0400, NFeLEnc_0400, NFeConsulta_0400,
  NFeItsI03_0400, NFeGeraXML_0400,
  //
  ///////////////////////////////////// DFe ////////////////////////////////////
  NFeDistDFeInt_0100, NFeCnfDowC_0100,
  ////////////////////////////////// SPED EFD //////////////////////////////////
  NFeEFD_C170, NFeEFD_C170_Cab, CFOPCFOP},
  //////////////////////////////////////////////////////////////////////////////
  ///
  NFCeEmiss_0400, NFCeLEnU_0400;

{ TNFCe_PF }

procedure TNFCe_PF.AvisoNaoImplemVerNFCe(Versao: Integer);
begin
  Geral.MB_Aviso('Versao de NFC-e n�o implementada: ' +
  Geral.FFT_Dot(Versao / 100, 2, siPositivo));
end;

procedure TNFCe_PF.Configura_idDest(EdEmpresa: TdmkEdit;
  RG_idDest: TdmkRadioGroup; UF_RmDs: String);
var
  Empresa, RemDest: Integer;
  UF_Emit(*, UF_RmDs*): String;
begin
  if EdEmpresa.ValueVariant <> 0 then
  begin
    Empresa := DModG.QrEmpresasCodigo.Value;
    UF_Emit := DModG.ObtemNomeUFDeEntidade(Empresa);
    //UF_RmDs := DmPediVda.QrClientesNOMEUF.Value;
    //
    if (UF_Emit <> '') and (UF_RmDs <> '') then
    begin
      if UF_RmDs = 'EX' then
        RG_idDest.ItemIndex := 3
      else
      if UF_RmDs = UF_Emit then
        RG_idDest.ItemIndex := 1
      else
        RG_idDest.ItemIndex := 2;
    end;
  end;
end;

procedure TNFCe_PF.Configura_indFinal(Entidade_Tipo: Integer;
  RG_indFinal: TdmkRadioGroup);
var
  Tipo: Integer;
begin
  Tipo := Entidade_Tipo; //DmPediVda.QrClientesTipo.Value;
  //
  if Tipo = 0 then
    RG_indFinal.ItemIndex := 0
  else
    RG_indFinal.ItemIndex := 1;
end;

function TNFCe_PF.DefineVarsFormNFCeEmiss_XXXX: Boolean;
var
  VersaoNFCe: Integer;
begin
  Result    := False;
  VersaoNFCe := VersaoNFCeEmUso();
  //
  case VersaoNFCe of
    400:
    begin
      // Configura��o do tipo
      FmNFCeEmiss_0400.F_Tipo            := VAR_FATID_0002; // ??? VAR_FATID_0001 / ;
      FmNFCeEmiss_0400.F_OriCodi         := DmPediVda.QrFatPedCabCodigo        .Value;
      // Integer
      FmNFCeEmiss_0400.F_Empresa         := DmPediVda.QrFatPedCabEmpresa       .Value;
      FmNFCeEmiss_0400.EdEmpresa.ValueVariant := DmPediVda.QrFatPedCabEmpresa.Value;
      FmNFCeEmiss_0400.F_ModeloNF        := DmPediVda.QrFatPedCabModeloNF      .Value;
      FmNFCeEmiss_0400.F_Cliente         := DmPediVda.QrFatPedCabCliente       .Value;
      FmNFCeEmiss_0400.F_EMP_FILIAL      := DmPediVda.QrFatPedCabEMP_FILIAL    .Value;
      FmNFCeEmiss_0400.F_AFP_Sit         := DmPediVda.QrFatPedCabAFP_Sit       .Value;
      //FmNFCeEmiss_0400.F_Associada       := DmPediVda.QrFatPedCabAssociada     .Value;
      //FmNFCeEmiss_0400.F_ASS_FILIAL      := DmPediVda.QrFatPedCabASS_FILIAL    .Value;
      //FmNFCeEmiss_0400.F_EMP_CtaFaturas  := DmPediVda.QrFatPedCabEMP_CtaProdVen.Value;
      //FmNFCeEmiss_0400.F_ASS_CtaFaturas  := DmPediVda.QrFatPedCabASS_CtaProdVen.Value;
      FmNFCeEmiss_0400.F_CartEmis        := DmPediVda.QrFatPedCabCartEmis      .Value;
      FmNFCeEmiss_0400.F_CondicaoPG      := DmPediVda.QrFatPedCabCondicaoPG    .Value;
      FmNFCeEmiss_0400.F_Financeiro      := DmPediVda.QrFatPedCabFinanceiro    .Value; // define na regra fiscal
      FmNFCeEmiss_0400.F_CtbCadMoF       := DmPediVda.QrFatPedCabCtbCadMoF     .Value;
      FmNFCeEmiss_0400.F_CtbPlaCta       := DmPediVda.QrFatPedCabCtbPlaCta     .Value;
      FmNFCeEmiss_0400.F_tpNF            := DmPediVda.QrFatPedCabtpNF          .Value;
      FmNFCeEmiss_0400.F_EMP_FaturaDta   := DmPediVda.QrFatPedCabEMP_FaturaDta .Value;
      FmNFCeEmiss_0400.F_EMP_IDDuplicata := DmPediVda.QrFatPedCabCodUsu        .Value;
      FmNFCeEmiss_0400.F_EMP_FaturaSeq   := DmPediVda.QrFatPedCabEMP_FaturaSeq .Value;
      FmNFCeEmiss_0400.F_EMP_FaturaNum   := DmPediVda.QrFatPedCabEMP_FaturaNum .Value;
      FmNFCeEmiss_0400.F_TIPOCART        := DmPediVda.QrFatPedCabTIPOCART      .Value;
      FmNFCeEmiss_0400.F_Represen        := DmPediVda.QrFatPedCabRepresen      .Value;
      //FmNFCeEmiss_0400.F_ASS_IDDuplicata := DmPediVda.QrFatPedCabCodUsu        .Value;
      //FmNFCeEmiss_0400.F_ASS_FaturaSeq   := DmPediVda.QrFatPedCabASS_FaturaSeq .Value;
      //FmNFCeEmiss_0400.F_ASS_FaturaNum   := DmPediVda.QrFatPedCabASS_FaturaNum .Value;
      // String
      FmNFCeEmiss_0400.F_EMP_FaturaSep   := DmPediVda.QrFatPedCabEMP_FaturaSep .Value;
      FmNFCeEmiss_0400.F_EMP_TxtFaturas  := DmPediVda.QrFatPedCabEMP_TxtProdVen.Value;
      FmNFCeEmiss_0400.F_EMP_TpDuplicata := DmPediVda.QrFatPedCabEMP_DupProdVen.Value;
      //FmNFCeEmiss_0400.F_ASS_FaturaSep   := DmPediVda.QrFatPedCabASS_FaturaSep .Value;
      //FmNFCeEmiss_0400.F_ASS_TxtFaturas  := DmPediVda.QrFatPedCabASS_TxtProdVen.Value;
      //FmNFCeEmiss_0400.F_ASS_TpDuplicata := DmPediVda.QrFatPedCabASS_DupProdVen.Value;
      // Double
      FmNFCeEmiss_0400.F_AFP_Per         := DmPediVda.QrFatPedCabAFP_Per       .Value;
      // TDateTime
      FmNFCeEmiss_0400.F_Abertura        := DmPediVda.QrFatPedCabAbertura      .Value;
      //
      Result := True;
    end
    else
    begin
       Result := False;
       AvisoNaoImplemVerNFCe(VersaoNFCe);
    end;
  end;
end;

procedure TNFCe_PF.EncerraFaturamento(FatSemEstq, FatPedCab, PediVda, IDVolume,
  OriTipo,
  RegrFiscal: Integer; Tabela: String; ValFrete: Double; UFCli: String;
  EntidadeTipo: Integer; ValorTotal: Double; OEstoqueJaFoiBaixado: Boolean);
var
  VersaoNFCe: Integer;
begin
  if not OEstoqueJaFoiBaixado then // 2020-10-24
  begin
    if not DmProd.VerificaEstoqueTodoFaturamento(FatSemEstq, FatPedCab, IDVolume,
      OriTipo, RegrFiscal)
    then
      Exit;
  end;
  //
  ReopenFPC1(FatPedCab);
  //
  VersaoNFCe := VersaoNFCeEmUso();
  case VersaoNFCe of
    0400:
    begin
      MostraFormNFCeEdit_0400(FatSemEstq, FatPedCab, PediVda, IDVolume, OriTipo,
        RegrFiscal, Tabela, ValFrete, UFCli, EntidadeTipo, ValorTotal, OEstoqueJaFoiBaixado);
    end else
      AvisoNaoImplemVerNFCe(VersaoNFCe);
  end;
end;

function TNFCe_PF.HashQrCode(tpEmis: Integer; ChaveNFe: String; VersaoQrCode,
  tpAmb: Integer; CSCPos, CSC: String; Data: TDateTime; Valor: Double;
  DigestValue: String): String;
  //
  function String2Hex(const Buffer: AnsiString): string;
  begin
    SetLength(Result, Length(Buffer) * 2);
    BinToHex(PAnsiChar(Buffer), PChar(Result), Length(Buffer));
  end;
var
  Passo1, Passo2, Passo3, sDia, sValor, DigValHex: String;
  _CSCPos: String;
begin
  _CSCPos := Geral.FF0(Geral.IMV(CSCPos));
  Result := '';
  if (tpEmis = 9) then
  begin
    //
    //Geral.MB_Info('Fazer HashQrCode de Contigencia=9');
(*
Posi��o Descri��o do Par�metro Bytes Orienta��es de preenchimento
====================================================================
1� Chave de Acesso da NFC-e 44* Informar a chave de acesso da NFC-e
2� Vers�o do QR Code 1* Para esta vers�o de documento, preencher o com �2�.
3� Identifica��o do Ambiente (1 � Produ��o, 2 � Homologa��o) 1* Informar valor do campo B24 do leiaute NFC-e
4� Dia da data de emiss�o 2* Informar o dia da data de emiss�o, que consta no campo B09 do leiaute NFC-e. O valor dever� ter exatamente dois d�gitos.
5� Valor Total da NFC-e 15 Informar valor do campo W16 do leiaute NFC-e. O valor deve ser informados com ponto (�.�) como separador decimal;
  n�o informar separador de milhar ou sinais.
6� DigestValue da NFC-e 56* Corresponde ao algoritmo SHA1 sobre o arquivo XML da NFC-e, convertido para formato hexadecimal.
   Ao se efetuar a assinatura digital da NFC-e emitida em conting�ncia off-line, o campo digestvalue constante da XML Signature deve obrigatoriamente
   ser id�ntico ao encontrado quando da gera��o do digestvalue para a montagem QR Code.
7� Identificador do CSC (C�digo de Seguran�a do Contribuinte no Banco de Dados da SEFAZ) 1-6 Deve ser informado sem os �0� (zeros) n�o significativos. A identifica��o do CSC corresponde a ordem do CSC no banco de dados da SEFAZ, n�o confundir com o pr�prio CSC.
8� C�digo Hash dos Par�metros 40* Ver gera��o do Hash do QR Code na emiss�o conting�ncia offline na se��o 4.3.2 deste documento.
*)
    sDia      := FormatDateTime('dd', Data);
    sValor    := Geral.FFT_Dot(Valor, 2, siPositivo);
    DigValHex := String2Hex(DigestValue);
    //
    Passo1 := (*1*)ChaveNFe                + '|' +
              (*2*)Geral.FF0(VersaoQrCode) + '|' +
              (*3*)Geral.FF0(tpAmb)        + '|' +
              (*4*)sDia                    + '|' +
              (*5*)sValor                  + '|' +
              (*6*)DigValHex               + '|' +
              (*7*)_CSCPos
              (*8*)
               ;
    Passo2 := Passo1 + CSC;
    //Passo3
    UndmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT SHA1("' + Passo2 + '") HashQrCode',
    '']);
    Passo3 := Dmod.QrAux.FieldByName('HashQrCode').AsString;

    //
    Result := Passo1 + '|' + Uppercase(Passo3);

  end else
  begin
    Passo1 := ChaveNFe + '|' +
              Geral.FF0(VersaoQrCode) + '|' +
              Geral.FF0(tpAmb) + '|' +
              _CSCPos;
    Passo2 := Passo1 + CSC;
    //Passo3
    UndmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT SHA1("' + Passo2 + '") HashQrCode',
    '']);
    Passo3 := Dmod.QrAux.FieldByName('HashQrCode').AsString;

    //"28170800156225000131650110000151341562040824|2|1|1SEU-CODIGO-CSC-CONTRIBUINTE-36-CARACTERES") HashQrCode'
    //
    //
    Result := Passo1 + '|' + Uppercase(Passo3);
  end;
end;

procedure TNFCe_PF.MostraFormNFCeEdit_0400(FatSemEstq, FatPedCab, PediVda, IDVolume,
  Tipo, RegrFiscal: Integer; Tabela: String; ValFrete: Double; UFCli: String;
  EntidadeTipo: Integer; ValorTotal: Double; OEstoqueJaFoiBaixado: Boolean);

  // FatPedCab: Integer; Tabela: String; OEstoqueJaFoiBaixado: Boolean

const
  CO_EhNFCe = True;
var
  Especie: String;
  Quantidade, Cliente: Integer;
  Recria, ApenasGeraXML, CalculaAutomatico: Boolean;
  ide_tpEmis: Integer;
begin
  if DBCheck.CriaFm((*TFmNFaEdit_0400*)TFmNFCeEmiss_0400, (*FmNFaEdit_0400*)FmNFCeEmiss_0400, afmoNegarComAviso) then
  begin
    Cliente := DmPediVda.QrFPC1Cliente.Value;
    //
    FmNFCeEmiss_0400.FEMP_FILIAL       := DmPediVda.QrFPC1EMP_FILIAL.Value;
    FmNFCeEmiss_0400.FCliente          := Cliente;
    //
    FmNFCeEmiss_0400.CkEmiteAvulso.Checked := Cliente = -2;
(*
    FmNFCeEmiss_0400.LaCNPJCPFAvulso.Enabled := Cliente = -2;
    FmNFCeEmiss_0400.EdCNPJCPFAvulso.Enabled := Cliente = -2;
    FmNFCeEmiss_0400.LaRazaoNomeAvulso.Enabled := Cliente = -2;
    FmNFCeEmiss_0400.EdRazaoNomeAvulso.Enabled := Cliente = -2;
*)
    FmNFCeEmiss_0400.FCU_Pedido        := DmPediVda.QrFPC1CU_PediVda.Value;
    FmNFCeEmiss_0400.FEmpresa          := DmPediVda.QrFPC1Empresa.Value;
    FmNFCeEmiss_0400.FFatPedCab        := FatPedCab;
    //
    DmPediVda.ReopenFatPedCab(DmPediVda.QrFPC1Codigo.Value, True, CO_EhNFCe);
    //
    (*FmNFaEdit_0400*)FmNFCeEmiss_0400.ReopenFatPedNFs_A((*1,0*));
    FmNFCeEmiss_0400.ReopenImprime();
    //
    try
      if not DmNFe_0000.Obtem_Serie_e_NumNF_Novo_NFCe(
        DmPediVda.QrFPC1SerieDesfe.Value,
        DmPediVda.QrFPC1NFDesfeita.Value,
        (*FmNFaEdit_0400*)FmNFCeEmiss_0400.QrImprimeSerieNF_Normal.Value,
        (*FmNFaEdit_0400*)FmNFCeEmiss_0400.QrImprimeCtrl_nfs.Value,
        DmPediVda.QrFPC1Empresa.Value,
        (*FmNFaEdit_0400*)FmNFCeEmiss_0400.QrFatPedNFs_A.FieldByName('Filial').AsInteger,
        (*FmNFaEdit_0400*)FmNFCeEmiss_0400.QrImprimeMaxSeqLib.Value,
        (*FmNFaEdit_0400*)FmNFCeEmiss_0400.EdSerieNF, (*FmNFaEdit_0400*)FmNFCeEmiss_0400.EdNumeroNF) then
      begin
        Screen.Cursor := crDefault;
        Exit;
      end;
      Especie    := '';
      Quantidade := 0;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(DmPediVda.QrVolumes, Dmod.MyDB, [
      'SELECT COUNT(fpv.Ativo) Volumes, med.Nome NO_UnidMed ',
      'FROM fatpedvol fpv ',
      'LEFT JOIN unidmed med ON med.Codigo=fpv.UnidMed ',
      'WHERE fpv.Codigo=' + Geral.FF0(DmPediVda.QrFPC1Codigo.Value),
      'AND fpv.Ativo=1 ',
      'GROUP BY med.Codigo ',
      '']);
      DmPediVda.QrVolumes.First;
      while not DmPediVda.QrVolumes.Eof do
      begin
        Quantidade := Quantidade + DmPediVda.QrVolumesVolumes.Value;
        if Especie <> '' then
          Especie := Especie + ' + ';
        Especie := Especie + ' ' + DmPediVda.QrVolumesNO_UnidMed.Value;
        //
        DmPediVda.QrVolumes.Next;
      end;
      FmNFCeEmiss_0400.FTabela                    := Tabela;
      //
      FmNFCeEmiss_0400.FIDVolume                  := IDVolume;
      FmNFCeEmiss_0400.FTipo                      := Tipo;
      FmNFCeEmiss_0400.FPediVda                   := PediVda;
      FmNFCeEmiss_0400.FUFCli                     := UFCli;
      FmNFCeEmiss_0400.FEntidadeTipo              := EntidadeTipo;
      FmNFCeEmiss_0400.EdRegrFiscal.ValueVariant  := RegrFiscal;
      FmNFCeEmiss_0400.CBRegrFiscal.KeyValue      := RegrFiscal;
      FmNFCeEmiss_0400.EdValFrete.ValueVariant    := ValFrete;
      if ValFrete > 0.01 then
      begin
        FmNFCeEmiss_0400.EdFretePor.ValueVariant := 0;
        FmNFCeEmiss_0400.CBFretePor.KeyValue     := 0;
      end
      else
      begin
        FmNFCeEmiss_0400.EdFretePor.ValueVariant := 9;
        FmNFCeEmiss_0400.CBFretePor.KeyValue     := 9;
      end;
      FmNFCeEmiss_0400.EdvPag.ValueVariant    := ValorTotal;
      //
{`POIU
      (*FmNFaEdit_0400*)FmNFCeEmiss_0400.EdQuantidade.ValueVariant := FloatToStr(Quantidade);
      (*FmNFaEdit_0400*)FmNFCeEmiss_0400.EdEspecie.ValueVariant    := Especie;
}
      //
      (*FmNFaEdit_0400*)FmNFCeEmiss_0400.EdSerieNF.Enabled  := ((*FmNFaEdit_0400*)FmNFCeEmiss_0400.QrImprimeIncSeqAuto.Value = 0);
      (*FmNFaEdit_0400*)FmNFCeEmiss_0400.EdNumeroNF.Enabled := ((*FmNFaEdit_0400*)FmNFCeEmiss_0400.QrImprimeIncSeqAuto.Value = 0);
      //
      // NF-e 2.00
{
      if (DModG.QrPrmsEmpNFeCRT.Value = 3) and
      (DModG.QrPrmsEmpNFeNFeNT2013_003LTT.Value < 2) then
        (*FmNFaEdit_0400*)FmNFCeEmiss_0400.PageControl1.ActivePageIndex := 0
      else
        (*FmNFaEdit_0400*)FmNFCeEmiss_0400.PageControl1.ActivePageIndex := 1;
      //
      (*FmNFaEdit_0400*)FmNFCeEmiss_0400.Eddest_email.Text         := DmodG.ObtemPrimeiroEMail_NFe(DmPediVda.QrFPC1Empresa.Value, DmPediVda.QrFPC1Cliente.Value);
      (*FmNFaEdit_0400*)FmNFCeEmiss_0400.RGCRT.ItemIndex           := DModG.QrPrmsEmpNFeCRT.Value;
      (*FmNFaEdit_0400*)FmNFCeEmiss_0400.EdVagao.Text              := '';
      (*FmNFaEdit_0400*)FmNFCeEmiss_0400.EdBalsa.Text              := '';
      //
      (*FmNFaEdit_0400*)FmNFCeEmiss_0400.EdCompra_XNEmp.Text := '';
      (*FmNFaEdit_0400*)FmNFCeEmiss_0400.EdCompra_XPed.Text  := DmPediVda.QrFPC1PedidoCli.Value;
      (*FmNFaEdit_0400*)FmNFCeEmiss_0400.EdCompra_XCont.Text := '';
      //
      (*FmNFaEdit_0400*)FmNFCeEmiss_0400.ReopenStqMovValX(0);
}
      (*FmNFaEdit_0400*)FmNFCeEmiss_0400.ImgTipo.SQLType := stIns;
      //(*FmNFaEdit_0400*)FmNFCeEmiss_0400.ShowModal;





      FmNFCeEmiss_0400.ConfiguracoesIniciais();
      //(*FmNFaEdit_0400*)FmNFCeEmiss_0400.Confirma();
      (*FmNFaEdit_0400*)FmNFCeEmiss_0400.ShowModal;

      if FmNFCeEmiss_0400.FConfirmou then
      begin




  {
      finally
        (*FmNFaEdit_0400*)FmNFCeEmiss_0400.Destroy;
      end;
      // Reabrir de novo!
  }
        ReopenFPC1(FatPedCab);
        //
        if DmPediVda.QrFPC1Encerrou.Value > 0 then
        begin
    {
          MostraFormFatPedNFs(DmPediVda.QrFPC1EMP_FILIAL.Value,
            DmPediVda.QrFPC1Cliente.Value, DmPediVda.QrFPC1CU_PediVda.Value, True);
            :>>
        if DBCheck.CriaFm(TF m F a t P e d N F s _ 0 4 0 0 , F m F a t P e d N F s _ 0 4 0 0 , afmoNegarComAviso) then
        begin
          F m F a t P e d N F s _ 0 4 0 0 .EdFilial.ValueVariant  := EMP_FILIAL;
          F m F a t P e d N F s _ 0 4 0 0 .CBFilial.KeyValue      := EMP_FILIAL;
          F m F a t P e d N F s _ 0 4 0 0 .EdCliente.ValueVariant := Cliente;
          F m F a t P e d N F s _ 0 4 0 0 .CBCliente.KeyValue     := Cliente;
          F m F a t P e d N F s _ 0 4 0 0 .EdPedido.ValueVariant  := CU_PediVda;
          F m F a t P e d N F s _ 0 4 0 0 .FNaoCriouXML           := ForcaCriarXML;
          F m F a t P e d N F s _ 0 4 0 0 .ShowModal;
          F m F a t P e d N F s _ 0 4 0 0 .Destroy;
        end
  }
          Recria            := True;
          ApenasGeraXML     := False;
          CalculaAutomatico := True;
          //
          //Retornar Aqui tpEmis para ver se eh normal ou contingencia off-line!
          FmNFCeEmiss_0400.Step1_GeraNFCe(Recria, ApenasGeraXML,
            CalculaAutomatico, ide_tpEmis);
          if FmNFCeEmiss_0400.FEditaNFeCabYA_Depois then
            FmNFCeEmiss_0400.Step2_Pagamentos();
          //
          //FmNFCeEmiss_0400.Step1_
          //
          MostraFormNFCeLEnU(FmNFCeEmiss_0400, ide_tpEmis, 0);
        end;
      end;
    finally
      (*FmNFaEdit_0400*)FmNFCeEmiss_0400.Destroy;
    end;
  end;
end;

function TNFCe_PF.MostraFormNFCeLEnU(Form: TForm; ide_tpEmis, StatusAchieved: Integer): Boolean;
const
  sProcName = 'TNFCe_PF.MostraFormNFCeLEnU()';
  //
  procedure Mensagem();
  begin
    Geral.MB_Erro('ide_tpEmis n�o implementado em ' + sProcName +
    sLineBreak + 'ide_tpEmis: ' + Geral.FF0(ide_tpEmis) +
    sLineBreak + 'StatusAchieved: ' + Geral.FF0(StatusAchieved));
  end;
var
  VersaoNFCe: Integer;
begin
  VersaoNFCe := VersaoNFCeEmUso();
  //
  //FNaoCriouXML := False;
  //
  Result := False;
  case VersaoNFCe of
    400:
    if DBCheck.CriaFm(TFmNFCeLEnU_0400, FmNFCeLEnU_0400, afmoNegarComAviso) then
    begin
      FmNFCeLEnU_0400.Fide_tpEmis := ide_tpEmis;
      FmNFCeLEnU_0400.Show;
      case StatusAchieved of
        0: // Nenhum ainda
        begin
          case ide_tpEmis of
            1: Result := FmNFCeLEnU_0400.EnviarNFe();
            9: Result := FmNFCeLEnU_0400.SohImprimeEMudaStatusParaPrecisaEnviar();
            else Mensagem();
          end;
        end;
        35: // Enviar NFce off-line impressa
        begin
          case ide_tpEmis of
            9: Result := FmNFCeLEnU_0400.AutorizarNFCeImpressaOffLine(Form);
            else Mensagem();
          end;
        end;
        else Mensagem();
      end;
      if Result then
        FmNFCeLEnU_0400.Destroy;
    end;
    else AvisoNaoImplemVerNFCe(VersaoNFCe);
  end;
end;

function TNFCe_PF.ObtemDadosCSC(var CSC, CSCpos: String): Boolean;
var
  Empresa: Integer;
begin
  Empresa := Geral.IMV(VAR_LIB_EMPRESAS);
  //
  DModG.ReopenParamsEmp(Empresa);
  //
  CSC := DModG.QrPrmsEmpNFeCSC.Value;
  CSCpos := DModG.QrPrmsEmpNFeCSCpos.Value;
  Result := (CSC <> EmptyStr) and (CSCpos <> EmptyStr);
end;

procedure TNFCe_PF.ReopenFPC1(FatPedCab: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(DmPediVda.QrFPC1, Dmod.MyDB, [
    'SELECT pvd.CodUsu CU_PediVda, pvd.Empresa,  ',
    'pvd.TabelaPrc, pvd.CondicaoPG, pvd.RegrFiscal, ',
    'pvd.AFP_Sit, pvd.AFP_Per, pvd.Cliente, ',
    'pvd.Codigo ID_Pedido, pvd.PedidoCli,  ',
    'ppc.JurosMes, frc.Nome NOMEFISREGCAD, ',
    'ppc.MedDDReal, ppc.MedDDSimpl, ',
    'par.TipMediaDD, par.Associada, par.FatSemEstq, ',
    ' ',
    'par.CtaProdVen EMP_CtaProdVen, ',
    'par.FaturaSeq EMP_FaturaSeq, ',
    'par.FaturaSep EMP_FaturaSep, ',
    'par.FaturaDta EMP_FaturaDta, ',
    'par.TxtProdVen EMP_TxtProdVen, ',
    'emp.Filial EMP_FILIAL, ',
    'ufe.Codigo EMP_UF, ',
    ' ',
    'ass.CtaProdVen ASS_CtaProdVen, ',
    'ass.FaturaSeq ASS_FaturaSeq, ',
    'ass.FaturaSep ASS_FaturaSep, ',
    'ass.FaturaDta ASS_FaturaDta, ',
    'ass.TxtProdVen ASS_TxtProdVen, ',
    'ufa.Nome ASS_NO_UF, ',
    'ufa.Codigo ASS_CO_UF, ',
    'ase.Filial ASS_FILIAL, ',
    ' ',
    'tpc.Nome NO_TabelaPrc, fpc.* ',
    'FROM fatpedcab fpc ',
    'LEFT JOIN pedivda    pvd ON pvd.Codigo=fpc.Pedido ',
    'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPG ',
    'LEFT JOIN tabeprccab tpc ON tpc.Codigo=pvd.TabelaPrc ',
    'LEFT JOIN paramsemp  par ON par.Codigo=pvd.Empresa ',
    'LEFT JOIN paramsemp  ass ON ass.Codigo=par.Associada ',
    'LEFT JOIN entidades  ase ON ase.Codigo=ass.Codigo ',
    'LEFT JOIN entidades  emp ON emp.Codigo=par.Codigo ',
    'LEFT JOIN ufs        ufe ON ufe.Codigo=IF(emp.Tipo=0, emp.EUF, emp.PUF) ',
    'LEFT JOIN ufs        ufa ON ufa.Codigo=IF(ase.Tipo=0, ase.EUF, ase.PUF) ',
    'LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal ',
    'WHERE fpc.Codigo=' + Geral.FF0(FatPedCab),
    '']);
end;

function TNFCe_PF.VersaoNFCeEmUso: Integer;
var
  Empresa: Integer;
begin
  Empresa := Geral.IMV(VAR_LIB_EMPRESAS);
  //
  DModG.ReopenParamsEmp(Empresa);
  //
  Result := Trunc((DModG.QrPrmsEmpNFeversao.Value *100) + 0.5);
end;

end.
