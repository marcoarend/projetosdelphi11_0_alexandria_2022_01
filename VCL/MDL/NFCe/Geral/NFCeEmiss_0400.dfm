object FmNFCeEmiss_0400: TFmNFCeEmiss_0400
  Left = 339
  Top = 185
  Caption = 'NFC-eEMIS-001 :: NFC-e - Emiss'#227'o de Cupom NFC-e 4.00'
  ClientHeight = 606
  ClientWidth = 736
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 736
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 688
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 640
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 386
        Height = 32
        Caption = 'Emiss'#227'o de Cupom NFC-e 4.00'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 386
        Height = 32
        Caption = 'Emiss'#227'o de Cupom NFC-e 4.00'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 386
        Height = 32
        Caption = 'Emiss'#227'o de Cupom NFC-e 4.00'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 492
    Width = 736
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 732
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 536
    Width = 736
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 590
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 588
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = 1
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 48
    Width = 736
    Height = 444
    Align = alClient
    TabOrder = 0
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 734
      Height = 43
      Align = alTop
      BevelOuter = bvLowered
      TabOrder = 0
      object LaSerieNF: TLabel
        Left = 10
        Top = 4
        Width = 44
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'S'#233'rie NF:'
      end
      object Label15: TLabel
        Left = 58
        Top = 4
        Width = 61
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = #218'lt. NF emit.:'
        FocusControl = DBEdit2
      end
      object Label16: TLabel
        Left = 133
        Top = 4
        Width = 45
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#186' Limite:'
        FocusControl = DBEdit3
      end
      object LaNumeroNF: TLabel
        Left = 209
        Top = 4
        Width = 71
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#186' Nota Fiscal:'
      end
      object Label10: TLabel
        Left = 284
        Top = 4
        Width = 50
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'TZD UTC:'
      end
      object SpeedButton21: TSpeedButton
        Left = 337
        Top = 20
        Width = 21
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '?'
      end
      object Label26: TLabel
        Left = 363
        Top = 4
        Width = 67
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Hor'#225'rio ver'#227'o:'
      end
      object Label2: TLabel
        Left = 438
        Top = 4
        Width = 44
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Empresa:'
      end
      object EdSerieNF: TdmkEdit
        Left = 10
        Top = 20
        Width = 45
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object DBEdit2: TDBEdit
        Left = 58
        Top = 20
        Width = 72
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Sequencial'
        DataSource = DsImprime
        ReadOnly = True
        TabOrder = 1
      end
      object DBEdit3: TDBEdit
        Left = 133
        Top = 20
        Width = 72
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'MaxSeqLib'
        DataSource = DsImprime
        ReadOnly = True
        TabOrder = 2
      end
      object EdNumeroNF: TdmkEdit
        Left = 209
        Top = 20
        Width = 72
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EddhEmiTZD: TdmkEdit
        Left = 284
        Top = 20
        Width = 52
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfTZD_UTC
        Texto = '+00:00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EddhEmiVerao: TdmkEdit
        Left = 363
        Top = 20
        Width = 70
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEmpresa: TdmkEdit
        Left = 438
        Top = 20
        Width = 45
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object Panel5: TPanel
      Left = 1
      Top = 44
      Width = 734
      Height = 48
      Align = alTop
      BevelInner = bvLowered
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 12
        Top = 6
        Width = 36
        Height = 13
        Caption = '$ Frete:'
      end
      object Label29: TLabel
        Left = 152
        Top = 6
        Width = 45
        Height = 13
        Caption = 'Frete por:'
      end
      object Label30: TLabel
        Left = 12
        Top = 27
        Width = 75
        Height = 13
        Caption = 'Transportadora:'
      end
      object SbTransporta: TSpeedButton
        Left = 704
        Top = 24
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbTransportaClick
      end
      object EdValFrete: TdmkEdit
        Left = 52
        Top = 2
        Width = 97
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdValFreteChange
      end
      object EdFretePor: TdmkEditCB
        Left = 204
        Top = 2
        Width = 21
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'FretePor'
        UpdCampo = 'FretePor'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBFretePor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBFretePor: TdmkDBLookupComboBox
        Left = 224
        Top = 2
        Width = 349
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        TabOrder = 2
        dmkEditCB = EdFretePor
        QryCampo = 'FretePor'
        UpdType = utNil
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdTransporta: TdmkEditCB
        Left = 96
        Top = 24
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Transporta'
        UpdCampo = 'Transporta'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTransporta
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTransporta: TdmkDBLookupComboBox
        Left = 156
        Top = 24
        Width = 545
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENT'
        TabOrder = 4
        dmkEditCB = EdTransporta
        QryCampo = 'Transporta'
        UpdType = utNil
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
    end
    object GroupBox6: TGroupBox
      Left = 1
      Top = 92
      Width = 734
      Height = 197
      Align = alTop
      Caption = ' Fiscal:'
      TabOrder = 2
      object SbRegrFiscal: TSpeedButton
        Left = 704
        Top = 55
        Width = 21
        Height = 21
        Caption = '...'
        Visible = False
        OnClick = SbRegrFiscalClick
      end
      object Label58: TLabel
        Left = 9
        Top = 117
        Width = 465
        Height = 13
        Caption = 
          'Indicador de presen'#231'a do comprador no estabelecimento comercial ' +
          'no momento da opera'#231#227'o: [F4]'
      end
      object Label208: TLabel
        Left = 570
        Top = 116
        Width = 134
        Height = 13
        Caption = 'Finalidade emis'#227'o NF-e: [F4]'
        FocusControl = Edide_finNFe
      end
      object Label25: TLabel
        Left = 12
        Top = 59
        Width = 162
        Height = 13
        Caption = 'Regra fiscal para emiss'#227'o de NFe:'
      end
      object LaCNPJCPFAvulso: TLabel
        Left = 106
        Top = 156
        Width = 95
        Height = 13
        Caption = 'CPF / CNPJ avulso:'
        Enabled = False
        FocusControl = EdCNPJCPFAvulso
      end
      object LaRazaoNomeAvulso: TLabel
        Left = 222
        Top = 156
        Width = 139
        Height = 13
        Caption = 'Raz'#227'o Social / Nome avulso:'
        Enabled = False
        FocusControl = EdRazaoNomeAvulso
      end
      object RG_idDest: TdmkRadioGroup
        Left = 13
        Top = 76
        Width = 712
        Height = 39
        Caption = ' Local de Destino da Opera'#231#227'o:'
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          '0 - N'#227'o definido'
          '1 - Opera'#231#227'o interna'
          '2 - Opera'#231#227'o interestadual'
          '3 - Opera'#231#227'o com exterior')
        TabOrder = 4
        TabStop = True
        QryCampo = 'idDest'
        UpdCampo = 'idDest'
        UpdType = utYes
        OldValor = 0
      end
      object RG_indFinal: TdmkRadioGroup
        Left = 12
        Top = 15
        Width = 245
        Height = 37
        Caption = ' Opera'#231#227'o com consumidor: '
        Columns = 2
        ItemIndex = 1
        Items.Strings = (
          '0 - Normal'
          '1 - Consumidor final')
        TabOrder = 0
        QryCampo = 'indFinal'
        UpdCampo = 'indFinal'
        UpdType = utYes
        OldValor = 0
      end
      object EdindPres: TdmkEdit
        Left = 10
        Top = 132
        Width = 32
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'indPres'
        UpdCampo = 'indPres'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdindPresChange
        OnKeyDown = EdindPresKeyDown
      end
      object EdindPres_TXT: TEdit
        Left = 43
        Top = 132
        Width = 522
        Height = 21
        TabStop = False
        ReadOnly = True
        TabOrder = 6
      end
      object Edide_finNFe_TXT: TdmkEdit
        Left = 591
        Top = 132
        Width = 128
        Height = 21
        TabStop = False
        ReadOnly = True
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object Edide_finNFe: TdmkEdit
        Left = 570
        Top = 132
        Width = 21
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '4'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'finNFe'
        UpdCampo = 'finNFe'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = Edide_finNFeChange
        OnKeyDown = Edide_finNFeKeyDown
      end
      object EdRegrFiscal: TdmkEditCB
        Left = 180
        Top = 55
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBRegrFiscal
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBRegrFiscal: TdmkDBLookupComboBox
        Left = 237
        Top = 55
        Width = 465
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsFisRegCad
        TabOrder = 3
        dmkEditCB = EdRegrFiscal
        UpdType = utNil
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object RGtpEmis: TdmkRadioGroup
        Left = 260
        Top = 15
        Width = 465
        Height = 37
        Caption = ' Tipo de Emiss'#227'o da NF-e : '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          '1=Emiss'#227'o normal (n'#227'o em conting'#234'ncia)'
          '9=Conting'#234'ncia off-line da NFC-e '#9)
        TabOrder = 1
        QryCampo = 'indFinal'
        UpdCampo = 'indFinal'
        UpdType = utYes
        OldValor = 0
      end
      object EdCNPJCPFAvulso: TdmkEdit
        Left = 106
        Top = 172
        Width = 113
        Height = 21
        Enabled = False
        TabOrder = 9
        FormatType = dmktfString
        MskType = fmtCPFJ
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdCNPJCPFAvulsoChange
      end
      object EdRazaoNomeAvulso: TdmkEdit
        Left = 222
        Top = 172
        Width = 495
        Height = 21
        Enabled = False
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtCPFJ
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CkEmiteAvulso: TdmkCheckBox
        Left = 12
        Top = 172
        Width = 85
        Height = 17
        Caption = 'NFC-e Avulsa:'
        TabOrder = 11
        OnClick = CkEmiteAvulsoClick
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
    end
    object PnNfeCabYA: TPanel
      Left = 1
      Top = 289
      Width = 734
      Height = 120
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 734
        Height = 40
        Align = alTop
        ParentBackground = False
        TabOrder = 0
        object Label3: TLabel
          Left = 404
          Top = 0
          Width = 67
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Valor do Pgto:'
        end
        object Label14: TLabel
          Left = 500
          Top = 0
          Width = 69
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Valor do troco:'
        end
        object Label4: TLabel
          Left = 10
          Top = 0
          Width = 118
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Meio de pagamento: [F4]'
          FocusControl = EdtPag
        end
        object EdvPag: TdmkEdit
          Left = 404
          Top = 16
          Width = 92
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'vPag'
          UpdCampo = 'vPag'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdvTroco: TdmkEdit
          Left = 500
          Top = 16
          Width = 92
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'vTroco'
          UpdCampo = 'vTroco'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdtPag: TdmkEdit
          Left = 10
          Top = 16
          Width = 26
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ValMax = '4'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'tPag'
          UpdCampo = 'tPag'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdtPagChange
          OnKeyDown = EdtPagKeyDown
        end
        object EdtPag_TXT: TdmkEdit
          Left = 36
          Top = 16
          Width = 365
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          ReadOnly = True
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object CkEditaYA: TCheckBox
          Left = 608
          Top = 16
          Width = 109
          Height = 17
          Caption = 'Inserir mais de um.'
          TabOrder = 4
        end
      end
      object PnCard: TPanel
        Left = 0
        Top = 40
        Width = 734
        Height = 80
        Align = alTop
        ParentBackground = False
        TabOrder = 1
        object Label5: TLabel
          Left = 545
          Top = 0
          Width = 165
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'CNPJ da Credenciadora de cart'#227'o:'
        end
        object Label17: TLabel
          Left = 10
          Top = 0
          Width = 194
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Tipo de Integra'#231#227'o para pagamento: [F4]'
        end
        object Label28: TLabel
          Left = 10
          Top = 40
          Width = 180
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Bandeira da operadora de cart'#227'o: [F4]'
        end
        object Label6: TLabel
          Left = 514
          Top = 40
          Width = 163
          Height = 13
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'N'#186' de autoriz. da opera'#231#227'o cart'#227'o:'
        end
        object EdCNPJ: TdmkEdit
          Left = 545
          Top = 16
          Width = 177
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtCPFJ_NFe
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'CNPJ'
          UpdCampo = 'CNPJ'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdtpIntegra: TdmkEdit
          Left = 10
          Top = 16
          Width = 40
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          CharCase = ecUpperCase
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'tpIntegra'
          UpdCampo = 'tpIntegra'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdtpIntegraChange
          OnKeyDown = EdtpIntegraKeyDown
        end
        object EdtpIntegra_TXT: TdmkEdit
          Left = 50
          Top = 16
          Width = 491
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          ReadOnly = True
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdtBand: TdmkEdit
          Left = 10
          Top = 56
          Width = 40
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          CharCase = ecUpperCase
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'tBand'
          UpdCampo = 'tBand'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdtBandChange
          OnKeyDown = EdtBandKeyDown
        end
        object EdtBand_TXT: TdmkEdit
          Left = 50
          Top = 56
          Width = 459
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          ReadOnly = True
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdcAut: TdmkEdit
          Left = 514
          Top = 56
          Width = 207
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          MaxLength = 60
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'cAut'
          UpdCampo = 'cAut'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
    end
    object Panel6: TPanel
      Left = 1
      Top = 418
      Width = 734
      Height = 25
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 4
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 101
        Height = 25
        Align = alLeft
        TabOrder = 0
        object Ckide_indIntermed: TdmkCheckBox
          Left = 12
          Top = 4
          Width = 89
          Height = 17
          Caption = 'Intermediado:'
          TabOrder = 0
          OnClick = Ckide_indIntermedClick
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
      end
      object PnIntermediador: TPanel
        Left = 101
        Top = 0
        Width = 633
        Height = 25
        Align = alClient
        TabOrder = 1
        Visible = False
        object Label7: TLabel
          Left = 4
          Top = 5
          Width = 67
          Height = 13
          Caption = 'Intermediador:'
        end
        object SbInfIntermedEnti: TSpeedButton
          Left = 600
          Top = 2
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SbInfIntermedEntiClick
        end
        object EdInfIntermedEnti: TdmkEditCB
          Left = 76
          Top = 2
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'InfIntermedEnti'
          UpdCampo = 'InfIntermedEnti'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBInfIntermedEnti
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBInfIntermedEnti: TdmkDBLookupComboBox
          Left = 132
          Top = 2
          Width = 465
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENT'
          TabOrder = 1
          dmkEditCB = EdInfIntermedEnti
          QryCampo = 'InfIntermedEnti'
          UpdType = utNil
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 12
    Top = 3
  end
  object QrImprime: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nfs.SerieNF CO_SerieNF, imp.TipoImpressao,'
      'nfs.SerieNF SerieNF_Normal, nfs.Sequencial, nfs.IncSeqAuto, '
      'nfs.Controle  Ctrl_nfs, nfs.MaxSeqLib'
      'FROM imprimeempr ime'
      'LEFT JOIN imprime imp ON imp.Codigo=ime.Codigo'
      'LEFT JOIN paramsnfs nfs ON nfs.Controle=ime.ParamsNFs'
      'WHERE ime.Empresa=:P0'
      'AND ime.Codigo=:P1')
    Left = 184
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrImprimeCO_SerieNF: TIntegerField
      FieldName = 'CO_SerieNF'
      Origin = 'imprime.SerieNF'
      Required = True
    end
    object QrImprimeSequencial: TIntegerField
      FieldName = 'Sequencial'
      Origin = 'paramsnfs.Sequencial'
      DisplayFormat = '000000;-000000; '
    end
    object QrImprimeIncSeqAuto: TSmallintField
      FieldName = 'IncSeqAuto'
      Origin = 'paramsnfs.IncSeqAuto'
    end
    object QrImprimeCtrl_nfs: TIntegerField
      FieldName = 'Ctrl_nfs'
      Origin = 'paramsnfs.Controle'
      Required = True
    end
    object QrImprimeMaxSeqLib: TIntegerField
      FieldName = 'MaxSeqLib'
      Origin = 'paramsnfs.MaxSeqLib'
      DisplayFormat = '000000;-000000; '
    end
    object QrImprimeTipoImpressao: TIntegerField
      FieldName = 'TipoImpressao'
      Origin = 'imprime.TipoImpressao'
      Required = True
    end
    object QrImprimeSerieNF_Normal: TIntegerField
      FieldName = 'SerieNF_Normal'
    end
  end
  object DsImprime: TDataSource
    DataSet = QrImprime
    Left = 184
    Top = 144
  end
  object QrParamsEmp: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT emp.Associada, emp.AssocModNF, emp.CRT, emp.CSOSN,'
      
        'emp.pCredSNAlq, emp.pCredSNMez, emp.NFeInfCpl, cpl.Texto NFeInfC' +
        'pl_TXT,'
      'emp.nfeTPEmis'
      'FROM paramsemp emp'
      'LEFT JOIN nfeinfcpl cpl ON cpl.Codigo = emp.NFeInfCpl'
      'WHERE emp.Codigo=:P0')
    Left = 232
    Top = 120
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrParamsEmpAssocModNF: TIntegerField
      FieldName = 'AssocModNF'
    end
    object QrParamsEmpAssociada: TIntegerField
      FieldName = 'Associada'
    end
    object QrParamsEmpCRT: TSmallintField
      FieldName = 'CRT'
    end
    object QrParamsEmpCSOSN: TIntegerField
      FieldName = 'CSOSN'
    end
    object QrParamsEmppCredSNAlq: TFloatField
      FieldName = 'pCredSNAlq'
    end
    object QrParamsEmppCredSNMez: TIntegerField
      FieldName = 'pCredSNMez'
    end
    object QrParamsEmppCredSN_Cfg: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'pCredSN_Cfg'
      Calculated = True
    end
    object QrParamsEmpNFeInfCpl: TIntegerField
      FieldName = 'NFeInfCpl'
    end
    object QrParamsEmpNFeInfCpl_TXT: TWideMemoField
      FieldName = 'NFeInfCpl_TXT'
      BlobType = ftWideMemo
    end
    object QrParamsEmpNFetpEmis: TIntegerField
      FieldName = 'NFetpEmis'
    end
  end
  object QrFatPedNFs_A: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrFatPedNFs_AAfterScroll
    SQL.Strings = (
      
        'SELECT nfe.FatID CabA_FatID, nfe.Status, nfe.infProt_cStat, nfe.' +
        'infCanc_cStat, '
      'ent.Filial, ent.Codigo CO_ENT_EMP, nfe.ide_tpNF, smna.*'
      ''
      'ide_dhEmiTZD, ide_dhSaiEntTZD'
      ''
      ''
      'FROM stqmovnfsa smna '
      'LEFT JOIN entidades ent ON ent.Codigo=smna.Empresa'
      'LEFT JOIN nfecaba nfe ON nfe.IDCtrl=smna.IDCtrl'
      'WHERE smna.Tipo=:P0'
      'AND smna.OriCodi=:P1'
      'AND smna.Empresa=:P2'
      'ORDER BY ent.Filial')
    Left = 232
    Top = 164
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrCTG: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(Empresa=0, 1, 0) ORDEM,  '
      'tpEmis, dhEntrada, Nome'
      'FROM nfecntngnc'
      'WHERE dhSaida < 2'
      'ORDER BY ORDEM, dhEntrada DESC')
    Left = 344
    Top = 120
    object QrCTGORDEM: TLargeintField
      FieldName = 'ORDEM'
      Required = True
    end
    object QrCTGtpEmis: TIntegerField
      FieldName = 'tpEmis'
    end
    object QrCTGdhEntrada: TDateTimeField
      FieldName = 'dhEntrada'
    end
    object QrCTGNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object QrNFeCabA: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfecaba'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND ide_nNF=:P3')
    Left = 344
    Top = 168
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFeCabAStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrNFeCabAinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrNFeCabAinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
    end
    object QrNFeCabAinfCanc_xMotivo: TWideStringField
      FieldName = 'infCanc_xMotivo'
      Size = 255
    end
    object QrNFeCabAinfProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
    object QrNFeCabAcStat: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'cStat'
      Calculated = True
    end
    object QrNFeCabAxMotivo: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'xMotivo'
      Size = 255
      Calculated = True
    end
    object QrNFeCabAcStat_xMotivo: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'cStat_xMotivo'
      Size = 255
      Calculated = True
    end
    object QrNFeCabAide_cNF: TIntegerField
      FieldName = 'ide_cNF'
    end
    object QrNFeCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrNFeCabAId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrNFeCabAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object QrFatPedNFs_B: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrFatPedNFs_BAfterScroll
    SQL.Strings = (
      
        'SELECT nfe.FatID CabA_FatID, nfe.Status, nfe.infProt_cStat, nfe.' +
        'infCanc_cStat, '
      'ent.Filial, ent.Codigo CO_ENT_EMP, nfe.ide_tpNF, smna.*'
      ''
      'ide_dhEmiTZD, ide_dhSaiEntTZD'
      ''
      ''
      'FROM stqmovnfsa smna '
      'LEFT JOIN entidades ent ON ent.Codigo=smna.Empresa'
      'LEFT JOIN nfecaba nfe ON nfe.IDCtrl=smna.IDCtrl'
      'WHERE smna.Tipo=:P0'
      'AND smna.OriCodi=:P1'
      'AND smna.Empresa=:P2'
      'ORDER BY ent.Filial')
    Left = 288
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrFatPedNFs_BFilial: TIntegerField
      FieldName = 'Filial'
      Origin = 'entidades.Filial'
      DisplayFormat = '000'
    end
    object QrFatPedNFs_BIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'stqmovnfsa.IDCtrl'
      Required = True
    end
    object QrFatPedNFs_BTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'stqmovnfsa.Tipo'
    end
    object QrFatPedNFs_BOriCodi: TIntegerField
      FieldName = 'OriCodi'
      Origin = 'stqmovnfsa.OriCodi'
    end
    object QrFatPedNFs_BEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'stqmovnfsa.Empresa'
    end
    object QrFatPedNFs_BNumeroNF: TIntegerField
      FieldName = 'NumeroNF'
      Origin = 'stqmovnfsa.NumeroNF'
      DisplayFormat = '000000;-000000; '
    end
    object QrFatPedNFs_BIncSeqAuto: TSmallintField
      FieldName = 'IncSeqAuto'
      Origin = 'stqmovnfsa.IncSeqAuto'
    end
    object QrFatPedNFs_BAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'stqmovnfsa.AlterWeb'
    end
    object QrFatPedNFs_BAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'stqmovnfsa.Ativo'
    end
    object QrFatPedNFs_BSerieNFCod: TIntegerField
      FieldName = 'SerieNFCod'
      Origin = 'stqmovnfsa.SerieNFCod'
    end
    object QrFatPedNFs_BSerieNFTxt: TWideStringField
      FieldName = 'SerieNFTxt'
      Origin = 'stqmovnfsa.SerieNFTxt'
      Size = 5
    end
    object QrFatPedNFs_BCO_ENT_EMP: TIntegerField
      FieldName = 'CO_ENT_EMP'
      Origin = 'entidades.Codigo'
    end
    object QrFatPedNFs_BDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'stqmovnfsa.DataCad'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFatPedNFs_BDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'stqmovnfsa.DataAlt'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFatPedNFs_BDataAlt_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataAlt_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedNFs_BFreteVal: TFloatField
      FieldName = 'FreteVal'
      Origin = 'stqmovnfsa.FreteVal'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFatPedNFs_BSeguro: TFloatField
      FieldName = 'Seguro'
      Origin = 'stqmovnfsa.Seguro'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFatPedNFs_BOutros: TFloatField
      FieldName = 'Outros'
      Origin = 'stqmovnfsa.Outros'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrFatPedNFs_BPlacaUF: TWideStringField
      FieldName = 'PlacaUF'
      Origin = 'stqmovnfsa.PlacaUF'
      Size = 2
    end
    object QrFatPedNFs_BPlacaNr: TWideStringField
      FieldName = 'PlacaNr'
      Origin = 'stqmovnfsa.PlacaNr'
      Size = 8
    end
    object QrFatPedNFs_BEspecie: TWideStringField
      FieldName = 'Especie'
      Origin = 'stqmovnfsa.Especie'
      Size = 30
    end
    object QrFatPedNFs_BMarca: TWideStringField
      FieldName = 'Marca'
      Origin = 'stqmovnfsa.Marca'
      Size = 30
    end
    object QrFatPedNFs_BNumero: TWideStringField
      FieldName = 'Numero'
      Origin = 'stqmovnfsa.Numero'
      Size = 30
    end
    object QrFatPedNFs_BkgBruto: TFloatField
      FieldName = 'kgBruto'
      Origin = 'stqmovnfsa.kgBruto'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrFatPedNFs_BkgLiqui: TFloatField
      FieldName = 'kgLiqui'
      Origin = 'stqmovnfsa.kgLiqui'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrFatPedNFs_BQuantidade: TWideStringField
      FieldName = 'Quantidade'
      Origin = 'stqmovnfsa.Quantidade'
      Size = 30
    end
    object QrFatPedNFs_BObservacao: TWideStringField
      FieldName = 'Observacao'
      Origin = 'stqmovnfsa.Observacao'
      Size = 255
    end
    object QrFatPedNFs_BCFOP1: TWideStringField
      FieldName = 'CFOP1'
      Origin = 'stqmovnfsa.CFOP1'
      Size = 6
    end
    object QrFatPedNFs_BDtEmissNF: TDateField
      FieldName = 'DtEmissNF'
      Origin = 'stqmovnfsa.DtEmissNF'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFatPedNFs_BDtEntraSai: TDateField
      FieldName = 'DtEntraSai'
      Origin = 'stqmovnfsa.DtEntraSai'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFatPedNFs_BStatus: TIntegerField
      FieldName = 'Status'
      Origin = 'nfecaba.Status'
    end
    object QrFatPedNFs_BinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
      Origin = 'nfecaba.infProt_cStat'
    end
    object QrFatPedNFs_BinfAdic_infCpl: TWideMemoField
      FieldName = 'infAdic_infCpl'
      Origin = 'stqmovnfsa.infAdic_infCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFatPedNFs_BinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
      Origin = 'nfecaba.infCanc_cStat'
    end
    object QrFatPedNFs_BDTEMISSNF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTEMISSNF_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedNFs_BDTENTRASAI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTENTRASAI_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedNFs_BRNTC: TWideStringField
      FieldName = 'RNTC'
      Origin = 'stqmovnfsa.RNTC'
    end
    object QrFatPedNFs_BUFembarq: TWideStringField
      FieldName = 'UFembarq'
      Origin = 'stqmovnfsa.UFembarq'
      Size = 2
    end
    object QrFatPedNFs_BxLocEmbarq: TWideStringField
      FieldName = 'xLocEmbarq'
      Origin = 'stqmovnfsa.xLocEmbarq'
      Size = 60
    end
    object QrFatPedNFs_Bide_tpNF: TSmallintField
      FieldName = 'ide_tpNF'
      Origin = 'nfecaba.ide_tpNF'
    end
    object QrFatPedNFs_BHrEntraSai: TTimeField
      FieldName = 'HrEntraSai'
      Origin = 'stqmovnfsa.HrEntraSai'
    end
    object QrFatPedNFs_Bide_dhCont: TDateTimeField
      FieldName = 'ide_dhCont'
      Origin = 'stqmovnfsa.ide_dhCont'
    end
    object QrFatPedNFs_Bide_xJust: TWideStringField
      FieldName = 'ide_xJust'
      Origin = 'stqmovnfsa.ide_xJust'
      Size = 255
    end
    object QrFatPedNFs_Bemit_CRT: TSmallintField
      FieldName = 'emit_CRT'
      Origin = 'stqmovnfsa.emit_CRT'
    end
    object QrFatPedNFs_Bdest_email: TWideStringField
      FieldName = 'dest_email'
      Origin = 'stqmovnfsa.dest_email'
      Size = 60
    end
    object QrFatPedNFs_Bvagao: TWideStringField
      FieldName = 'vagao'
      Origin = 'stqmovnfsa.vagao'
    end
    object QrFatPedNFs_Bbalsa: TWideStringField
      FieldName = 'balsa'
      Origin = 'stqmovnfsa.balsa'
    end
    object QrFatPedNFs_BCabA_FatID: TIntegerField
      FieldName = 'CabA_FatID'
      Origin = 'nfecaba.FatID'
      Required = True
    end
    object QrFatPedNFs_BCompra_XNEmp: TWideStringField
      FieldName = 'Compra_XNEmp'
      Origin = 'stqmovnfsa.Compra_XNEmp'
      Size = 17
    end
    object QrFatPedNFs_BCompra_XPed: TWideStringField
      FieldName = 'Compra_XPed'
      Origin = 'stqmovnfsa.Compra_XPed'
      Size = 60
    end
    object QrFatPedNFs_BCompra_XCont: TWideStringField
      FieldName = 'Compra_XCont'
      Origin = 'stqmovnfsa.Compra_XCont'
      Size = 60
    end
    object QrFatPedNFs_BdhEmiTZD: TFloatField
      FieldName = 'dhEmiTZD'
    end
    object QrFatPedNFs_BdhSaiEntTZD: TFloatField
      FieldName = 'dhSaiEntTZD'
    end
    object QrFatPedNFs_BHrEmi: TTimeField
      FieldName = 'HrEmi'
    end
    object QrFatPedNFs_BLoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
  end
  object QrFatPedCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ppc.MedDDSimpl, fpc.Codigo, fpc.CodUsu, fpc.Encerrou,'
      'IF(emp.Tipo=0,emp.RazaoSocial,emp.Nome) NO_EMP,'
      'IF(emp.Tipo=0,emp.ECodMunici,emp.PCodMunici) + 0.000 CODMUNICI,'
      'ufe.Nome UF_TXT_emp, ufc.Nome UF_TXT_cli,'
      'IF(cli.Tipo=0,cli.RazaoSocial,cli.Nome) NO_CLI,'
      'frc.ModeloNF, frc.infAdFisco, frc.Financeiro,'
      'frc.TipoMov tpNF, nfe.ICMSTot_vProd, pvd.*'
      'FROM fatpedcab fpc'
      
        'LEFT JOIN nfecaba nfe ON nfe.FatNum = fpc.Codigo AND  nfe.FatID ' +
        '= 1'
      'LEFT JOIN pedivda pvd ON pvd.Codigo=fpc.Pedido'
      'LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal'
      'LEFT JOIN entidades emp ON emp.Codigo=pvd.Empresa'
      'LEFT JOIN entidades cli ON cli.Codigo=pvd.Cliente'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPg'
      'LEFT JOIN ufs ufe ON ufe.Codigo=IF(emp.Tipo=0,emp.EUF,emp.PUF)'
      'LEFT JOIN ufs ufc ON ufc.Codigo=IF(cli.Tipo=0,cli.EUF,cli.PUF)'
      'WHERE fpc.Encerrou > 0'
      'ORDER BY Encerrou DESC')
    Left = 288
    Top = 144
    object QrFatPedCabMedDDSimpl: TFloatField
      FieldName = 'MedDDSimpl'
    end
    object QrFatPedCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFatPedCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrFatPedCabEncerrou: TDateTimeField
      FieldName = 'Encerrou'
      Required = True
    end
    object QrFatPedCabNO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 100
    end
    object QrFatPedCabCODMUNICI: TFloatField
      FieldName = 'CODMUNICI'
    end
    object QrFatPedCabUF_TXT_emp: TWideStringField
      FieldName = 'UF_TXT_emp'
      Required = True
      Size = 2
    end
    object QrFatPedCabUF_TXT_cli: TWideStringField
      FieldName = 'UF_TXT_cli'
      Required = True
      Size = 2
    end
    object QrFatPedCabNO_CLI: TWideStringField
      FieldName = 'NO_CLI'
      Size = 100
    end
    object QrFatPedCabinfAdFisco: TWideStringField
      FieldName = 'infAdFisco'
      Size = 255
    end
    object QrFatPedCabFinanceiro: TSmallintField
      FieldName = 'Financeiro'
    end
    object QrFatPedCabTotalFatur: TFloatField
      FieldName = 'TotalFatur'
      DisplayFormat = '#,###,##0.00'
    end
    object QrFatPedCabtpNF: TSmallintField
      FieldName = 'tpNF'
    end
    object QrFatPedCabDTAEMISS_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTAEMISS_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedCabDTAENTRA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTAENTRA_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedCabDTAINCLU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTAINCLU_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedCabDTAPREVI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTAPREVI_TXT'
      Size = 10
      Calculated = True
    end
    object QrFatPedCabDtaEmiss: TDateField
      FieldName = 'DtaEmiss'
    end
    object QrFatPedCabDtaEntra: TDateField
      FieldName = 'DtaEntra'
    end
    object QrFatPedCabDtaInclu: TDateField
      FieldName = 'DtaInclu'
    end
    object QrFatPedCabDtaPrevi: TDateField
      FieldName = 'DtaPrevi'
    end
    object QrFatPedCabFretePor: TIntegerField
      FieldName = 'FretePor'
    end
    object QrFatPedCabModeloNF: TFloatField
      FieldName = 'ModeloNF'
    end
    object QrFatPedCabRegrFiscal: TFloatField
      FieldName = 'RegrFiscal'
    end
    object QrFatPedCabCartEmis: TFloatField
      FieldName = 'CartEmis'
    end
    object QrFatPedCabTabelaPrc: TFloatField
      FieldName = 'TabelaPrc'
    end
    object QrFatPedCabCondicaoPG: TFloatField
      FieldName = 'CondicaoPG'
    end
    object QrFatPedCabTransporta: TFloatField
      FieldName = 'Transporta'
    end
    object QrFatPedCabEmpresa: TFloatField
      FieldName = 'Empresa'
    end
    object QrFatPedCabCliente: TFloatField
      FieldName = 'Cliente'
    end
    object QrFatPedCabidDest: TSmallintField
      FieldName = 'idDest'
    end
    object QrFatPedCabindFinal: TSmallintField
      FieldName = 'indFinal'
    end
    object QrFatPedCabindPres: TSmallintField
      FieldName = 'indPres'
    end
    object QrFatPedCabindSinc: TSmallintField
      FieldName = 'indSinc'
    end
    object QrFatPedCabTpCalcTrib: TSmallintField
      FieldName = 'TpCalcTrib'
    end
    object QrFatPedCabPedidoCli: TWideStringField
      FieldName = 'PedidoCli'
    end
    object QrFatPedCabfinNFe: TSmallintField
      FieldName = 'finNFe'
    end
    object QrFatPedCabId: TIntegerField
      FieldName = 'Id'
    end
    object QrFatPedCabRetiradaUsa: TSmallintField
      FieldName = 'RetiradaUsa'
    end
    object QrFatPedCabRetiradaEnti: TIntegerField
      FieldName = 'RetiradaEnti'
    end
    object QrFatPedCabEntregaUsa: TSmallintField
      FieldName = 'EntregaUsa'
    end
    object QrFatPedCabEntregaEnti: TIntegerField
      FieldName = 'EntregaEnti'
    end
    object QrFatPedCabL_Ativo: TSmallintField
      FieldName = 'L_Ativo'
    end
    object QrFatPedCabInfIntermedEnti: TFloatField
      FieldName = 'InfIntermedEnti'
    end
  end
  object QrFisRegCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frc.Codigo, frc.CodUsu, frc.ModeloNF,'
      'frc.Nome, frc.Financeiro, imp.Nome NO_MODELO_NF'
      'FROM fisregcad frc'
      'LEFT JOIN imprime imp ON imp.Codigo=frc.ModeloNF'
      'ORDER BY Nome')
    Left = 464
    Top = 4
    object QrFisRegCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFisRegCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrFisRegCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrFisRegCadModeloNF: TIntegerField
      FieldName = 'ModeloNF'
      Required = True
    end
    object QrFisRegCadNO_MODELO_NF: TWideStringField
      FieldName = 'NO_MODELO_NF'
      Size = 100
    end
    object QrFisRegCadFinanceiro: TSmallintField
      FieldName = 'Financeiro'
    end
  end
  object DsFisRegCad: TDataSource
    DataSet = QrFisRegCad
    Left = 464
    Top = 49
  end
  object QrPsq: TMySQLQuery
    Database = Dmod.MyDB
    Left = 297
    Top = 217
  end
end
