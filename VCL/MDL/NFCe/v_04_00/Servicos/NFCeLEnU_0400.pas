unit NFCeLEnU_0400;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, DmkDAC_PF, UnDmkEnums, UnGrl_Consts, frxClass, frxDBSet,
  UnBematech_PF, declaracoesBematech, TypInfo, Variants;

type
  TFmNFCeLEnU_0400 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    QrNFeLEnC: TmySQLQuery;
    QrNFeLEnCCodigo: TIntegerField;
    QrNFeLEnCCodUsu: TIntegerField;
    QrNFeLEnCNome: TWideStringField;
    QrNFeLEnCEmpresa: TIntegerField;
    QrNFeLEnCversao: TFloatField;
    QrNFeLEnCtpAmb: TSmallintField;
    QrNFeLEnCverAplic: TWideStringField;
    QrNFeLEnCcStat: TIntegerField;
    QrNFeLEnCxMotivo: TWideStringField;
    QrNFeLEnCcUF: TSmallintField;
    QrNFeLEnCnRec: TWideStringField;
    QrNFeLEnCdhRecbto: TDateTimeField;
    QrNFeLEnCtMed: TIntegerField;
    QrNFeLEnCcMsg: TWideStringField;
    QrNFeLEnCxMsg: TWideStringField;
    QrNFeLEnCindSinc: TSmallintField;
    DsNFeLEnC: TDataSource;
    QrNFeCabA: TmySQLQuery;
    QrNFeCabAStatus: TIntegerField;
    QrNFeCabAMotivo: TWideStringField;
    DsNFeCabA: TDataSource;
    QrNFeCabAIDCtrl: TIntegerField;
    Panel5: TPanel;
    Panel6: TPanel;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    CheckBox7: TCheckBox;
    Panel7: TPanel;
    EdFatNum: TdmkEdit;
    EdEmpresa: TdmkEdit;
    Label13: TLabel;
    Label10: TLabel;
    EdFatID: TdmkEdit;
    Label3: TLabel;
    Panel8: TPanel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label2: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label4: TLabel;
    EdIDCtrl: TdmkEdit;
    frxNFCe_01_Autorizada_UniversLight_Condensed: TfrxReport;
    QrA: TMySQLQuery;
    QrAFatID: TIntegerField;
    QrAFatNum: TIntegerField;
    QrAEmpresa: TIntegerField;
    QrALoteEnv: TIntegerField;
    QrAversao: TFloatField;
    QrAId: TWideStringField;
    QrAide_cUF: TSmallintField;
    QrAide_cNF: TIntegerField;
    QrAide_natOp: TWideStringField;
    QrAide_indPag: TSmallintField;
    QrAide_mod: TSmallintField;
    QrAide_serie: TIntegerField;
    QrAide_nNF: TIntegerField;
    QrAide_dEmi: TDateField;
    QrAide_dSaiEnt: TDateField;
    QrAide_tpNF: TSmallintField;
    QrAide_cMunFG: TIntegerField;
    QrAide_tpImp: TSmallintField;
    QrAide_tpEmis: TSmallintField;
    QrAide_cDV: TSmallintField;
    QrAide_tpAmb: TSmallintField;
    QrAide_finNFe: TSmallintField;
    QrAide_procEmi: TSmallintField;
    QrAide_verProc: TWideStringField;
    QrAemit_CNPJ: TWideStringField;
    QrAemit_CPF: TWideStringField;
    QrAemit_xNome: TWideStringField;
    QrAemit_xFant: TWideStringField;
    QrAemit_xLgr: TWideStringField;
    QrAemit_nro: TWideStringField;
    QrAemit_xCpl: TWideStringField;
    QrAemit_xBairro: TWideStringField;
    QrAemit_cMun: TIntegerField;
    QrAemit_xMun: TWideStringField;
    QrAemit_UF: TWideStringField;
    QrAemit_CEP: TIntegerField;
    QrAemit_cPais: TIntegerField;
    QrAemit_xPais: TWideStringField;
    QrAemit_fone: TWideStringField;
    QrAemit_IE: TWideStringField;
    QrAemit_IEST: TWideStringField;
    QrAemit_IM: TWideStringField;
    QrAemit_CNAE: TWideStringField;
    QrAdest_CNPJ: TWideStringField;
    QrAdest_CPF: TWideStringField;
    QrAdest_xNome: TWideStringField;
    QrAdest_xLgr: TWideStringField;
    QrAdest_nro: TWideStringField;
    QrAdest_xCpl: TWideStringField;
    QrAdest_xBairro: TWideStringField;
    QrAdest_cMun: TIntegerField;
    QrAdest_xMun: TWideStringField;
    QrAdest_UF: TWideStringField;
    QrAdest_CEP: TWideStringField;
    QrAdest_cPais: TIntegerField;
    QrAdest_xPais: TWideStringField;
    QrAdest_fone: TWideStringField;
    QrAdest_IE: TWideStringField;
    QrAdest_ISUF: TWideStringField;
    QrAICMSTot_vBC: TFloatField;
    QrAICMSTot_vICMS: TFloatField;
    QrAICMSTot_vBCST: TFloatField;
    QrAICMSTot_vST: TFloatField;
    QrAICMSTot_vProd: TFloatField;
    QrAICMSTot_vFrete: TFloatField;
    QrAICMSTot_vSeg: TFloatField;
    QrAICMSTot_vDesc: TFloatField;
    QrAICMSTot_vII: TFloatField;
    QrAICMSTot_vIPI: TFloatField;
    QrAICMSTot_vPIS: TFloatField;
    QrAICMSTot_vCOFINS: TFloatField;
    QrAICMSTot_vOutro: TFloatField;
    QrAICMSTot_vNF: TFloatField;
    QrAISSQNtot_vServ: TFloatField;
    QrAISSQNtot_vBC: TFloatField;
    QrAISSQNtot_vISS: TFloatField;
    QrAISSQNtot_vPIS: TFloatField;
    QrAISSQNtot_vCOFINS: TFloatField;
    QrARetTrib_vRetPIS: TFloatField;
    QrARetTrib_vRetCOFINS: TFloatField;
    QrARetTrib_vRetCSLL: TFloatField;
    QrARetTrib_vBCIRRF: TFloatField;
    QrARetTrib_vIRRF: TFloatField;
    QrARetTrib_vBCRetPrev: TFloatField;
    QrARetTrib_vRetPrev: TFloatField;
    QrAModFrete: TSmallintField;
    QrATransporta_CNPJ: TWideStringField;
    QrATransporta_CPF: TWideStringField;
    QrATransporta_XNome: TWideStringField;
    QrATransporta_IE: TWideStringField;
    QrATransporta_XEnder: TWideStringField;
    QrATransporta_XMun: TWideStringField;
    QrATransporta_UF: TWideStringField;
    QrARetTransp_vServ: TFloatField;
    QrARetTransp_vBCRet: TFloatField;
    QrARetTransp_PICMSRet: TFloatField;
    QrARetTransp_vICMSRet: TFloatField;
    QrARetTransp_CFOP: TWideStringField;
    QrARetTransp_CMunFG: TWideStringField;
    QrAVeicTransp_Placa: TWideStringField;
    QrAVeicTransp_UF: TWideStringField;
    QrAVeicTransp_RNTC: TWideStringField;
    QrACobr_Fat_NFat: TWideStringField;
    QrACobr_Fat_vOrig: TFloatField;
    QrACobr_Fat_vDesc: TFloatField;
    QrACobr_Fat_vLiq: TFloatField;
    QrAInfAdic_InfCpl: TWideMemoField;
    QrAExporta_UFEmbarq: TWideStringField;
    QrAExporta_XLocEmbarq: TWideStringField;
    QrACompra_XNEmp: TWideStringField;
    QrACompra_XPed: TWideStringField;
    QrACompra_XCont: TWideStringField;
    QrAStatus: TIntegerField;
    QrAinfProt_Id: TWideStringField;
    QrAinfProt_tpAmb: TSmallintField;
    QrAinfProt_verAplic: TWideStringField;
    QrAinfProt_dhRecbto: TDateTimeField;
    QrAinfProt_nProt: TWideStringField;
    QrAinfProt_digVal: TWideStringField;
    QrAinfProt_cStat: TIntegerField;
    QrAinfProt_xMotivo: TWideStringField;
    QrA_Ativo_: TSmallintField;
    QrALk: TIntegerField;
    QrADataCad: TDateField;
    QrADataAlt: TDateField;
    QrAUserCad: TIntegerField;
    QrAUserAlt: TIntegerField;
    QrAAlterWeb: TSmallintField;
    QrAAtivo: TSmallintField;
    QrAIDCtrl: TIntegerField;
    QrAinfCanc_Id: TWideStringField;
    QrAinfCanc_tpAmb: TSmallintField;
    QrAinfCanc_verAplic: TWideStringField;
    QrAinfCanc_dhRecbto: TDateTimeField;
    QrAinfCanc_nProt: TWideStringField;
    QrAinfCanc_digVal: TWideStringField;
    QrAinfCanc_cStat: TIntegerField;
    QrAinfCanc_xMotivo: TWideStringField;
    QrAinfCanc_cJust: TIntegerField;
    QrAinfCanc_xJust: TWideStringField;
    QrAID_TXT: TWideStringField;
    QrAEMIT_ENDERECO: TWideStringField;
    QrAEMIT_FONE_TXT: TWideStringField;
    QrAEMIT_IE_TXT: TWideStringField;
    QrAEMIT_IEST_TXT: TWideStringField;
    QrAEMIT_CNPJ_TXT: TWideStringField;
    QrADEST_CNPJ_CPF_TXT: TWideStringField;
    QrADEST_ENDERECO: TWideStringField;
    QrADEST_CEP_TXT: TWideStringField;
    QrADEST_FONE_TXT: TWideStringField;
    QrADEST_IE_TXT: TWideStringField;
    QrATRANSPORTA_CNPJ_CPF_TXT: TWideStringField;
    QrATRANSPORTA_IE_TXT: TWideStringField;
    QrADOC_SEM_VLR_JUR: TWideStringField;
    QrADOC_SEM_VLR_FIS: TWideStringField;
    QrAide_DSaiEnt_Txt: TWideStringField;
    QrAInfAdic_InfAdFisco: TWideMemoField;
    QrAide_hSaiEnt: TTimeField;
    QrAide_dhCont: TDateTimeField;
    QrAide_xJust: TWideStringField;
    QrAemit_CRT: TSmallintField;
    QrAdest_email: TWideStringField;
    QrAVagao: TWideStringField;
    QrABalsa: TWideStringField;
    QrAMODFRETE_TXT: TWideStringField;
    QrADEST_XMUN_TXT: TWideStringField;
    QrANFeNT2013_003LTT: TSmallintField;
    QrAInfCpl_totTrib: TWideStringField;
    QrAvTotTrib: TFloatField;
    frxDsA: TfrxDBDataset;
    QrI: TMySQLQuery;
    QrIprod_cProd: TWideStringField;
    QrIprod_cEAN: TWideStringField;
    QrIprod_xProd: TWideStringField;
    QrIprod_NCM: TWideStringField;
    QrIprod_EXTIPI: TWideStringField;
    QrIprod_genero: TSmallintField;
    QrIprod_CFOP: TIntegerField;
    QrIprod_uCom: TWideStringField;
    QrIprod_qCom: TFloatField;
    QrIprod_vUnCom: TFloatField;
    QrIprod_vProd: TFloatField;
    QrIprod_cEANTrib: TWideStringField;
    QrIprod_uTrib: TWideStringField;
    QrIprod_qTrib: TFloatField;
    QrIprod_vUnTrib: TFloatField;
    QrIprod_vFrete: TFloatField;
    QrIprod_vSeg: TFloatField;
    QrIprod_vDesc: TFloatField;
    QrITem_IPI: TSmallintField;
    QrInItem: TIntegerField;
    QrIFracio: TSmallintField;
    QrIprod_qCom_TXT: TWideStringField;
    frxDsI: TfrxDBDataset;
    QrN: TMySQLQuery;
    QrNnItem: TIntegerField;
    QrNICMS_Orig: TSmallintField;
    QrNICMS_CST: TSmallintField;
    QrNICMS_modBC: TSmallintField;
    QrNICMS_pRedBC: TFloatField;
    QrNICMS_vBC: TFloatField;
    QrNICMS_pICMS: TFloatField;
    QrNICMS_vICMS: TFloatField;
    QrNICMS_modBCST: TSmallintField;
    QrNICMS_pMVAST: TFloatField;
    QrNICMS_pRedBCST: TFloatField;
    QrNICMS_vBCST: TFloatField;
    QrNICMS_pICMSST: TFloatField;
    QrNICMS_vICMSST: TFloatField;
    QrNICMS_CSOSN: TIntegerField;
    frxDsN: TfrxDBDataset;
    QrV: TMySQLQuery;
    QrVnItem: TIntegerField;
    QrVInfAdProd: TWideMemoField;
    frxDsV: TfrxDBDataset;
    QrO: TMySQLQuery;
    QrOnItem: TIntegerField;
    QrOIPI_clEnq: TWideStringField;
    QrOIPI_CNPJProd: TWideStringField;
    QrOIPI_cSelo: TWideStringField;
    QrOIPI_qSelo: TFloatField;
    QrOIPI_cEnq: TWideStringField;
    QrOIPI_CST: TSmallintField;
    QrOIPI_vBC: TFloatField;
    QrOIPI_qUnid: TFloatField;
    QrOIPI_vUnid: TFloatField;
    QrOIPI_pIPI: TFloatField;
    QrOIPI_vIPI: TFloatField;
    frxDsO: TfrxDBDataset;
    QrM: TMySQLQuery;
    QrMFatID: TIntegerField;
    QrMFatNum: TIntegerField;
    QrMEmpresa: TIntegerField;
    QrMnItem: TIntegerField;
    QrMiTotTrib: TSmallintField;
    QrMvTotTrib: TFloatField;
    frxDsM: TfrxDBDataset;
    QrYA: TMySQLQuery;
    QrYAFatID: TIntegerField;
    QrYAFatNum: TIntegerField;
    QrYAEmpresa: TIntegerField;
    QrYAControle: TIntegerField;
    QrYAtPag: TSmallintField;
    QrYAvPag: TFloatField;
    QrYAtpIntegra: TSmallintField;
    QrYACNPJ: TWideStringField;
    QrYAtBand: TSmallintField;
    QrYAcAut: TWideStringField;
    QrYAvTroco: TFloatField;
    frxDsYA: TfrxDBDataset;
    QrAURL_Consulta: TWideStringField;
    QrAAllTxtLink: TWideStringField;
    QrADEST_CNPJ_CPF_TIT: TWideStringField;
    QrAide_hEmi: TTimeField;
    QrAide_dhEmiTZD: TFloatField;
    QrAide_dhSaiEntTZD: TFloatField;
    QrAide_idDest: TSmallintField;
    QrAide_indFinal: TSmallintField;
    QrAide_indPres: TSmallintField;
    QrAide_dhContTZD: TFloatField;
    QrAEstrangDef: TSmallintField;
    QrAdest_idEstrangeiro: TWideStringField;
    QrAdest_indIEDest: TSmallintField;
    QrAdest_IM: TWideStringField;
    QrAICMSTot_vICMSDeson: TFloatField;
    QrAICMSTot_vFCPUFDest: TFloatField;
    QrAICMSTot_vICMSUFDest: TFloatField;
    QrAICMSTot_vICMSUFRemet: TFloatField;
    QrAICMSTot_vFCP: TFloatField;
    QrAICMSTot_vFCPST: TFloatField;
    QrAICMSTot_vFCPSTRet: TFloatField;
    QrAICMSTot_vIPIDevol: TFloatField;
    QrAISSQNtot_dCompet: TDateField;
    QrAISSQNtot_vDeducao: TFloatField;
    QrAISSQNtot_vOutro: TFloatField;
    QrAISSQNtot_vDescIncond: TFloatField;
    QrAISSQNtot_vDescCond: TFloatField;
    QrAISSQNtot_vISSRet: TFloatField;
    QrAISSQNtot_cRegTrib: TSmallintField;
    QrAExporta_XLocDespacho: TWideStringField;
    QrAprotNFe_versao: TFloatField;
    QrAinfProt_dhRecbtoTZD: TFloatField;
    QrAretCancNFe_versao: TFloatField;
    QrAinfCanc_dhRecbtoTZD: TFloatField;
    QrAFisRegCad: TIntegerField;
    QrACartEmiss: TIntegerField;
    QrATabelaPrc: TIntegerField;
    QrACondicaoPg: TIntegerField;
    QrAFreteExtra: TFloatField;
    QrASegurExtra: TFloatField;
    QrAICMSRec_pRedBC: TFloatField;
    QrAICMSRec_vBC: TFloatField;
    QrAICMSRec_pAliq: TFloatField;
    QrAICMSRec_vICMS: TFloatField;
    QrAIPIRec_pRedBC: TFloatField;
    QrAIPIRec_vBC: TFloatField;
    QrAIPIRec_pAliq: TFloatField;
    QrAIPIRec_vIPI: TFloatField;
    QrAPISRec_pRedBC: TFloatField;
    QrAPISRec_vBC: TFloatField;
    QrAPISRec_pAliq: TFloatField;
    QrAPISRec_vPIS: TFloatField;
    QrACOFINSRec_pRedBC: TFloatField;
    QrACOFINSRec_vBC: TFloatField;
    QrACOFINSRec_pAliq: TFloatField;
    QrACOFINSRec_vCOFINS: TFloatField;
    QrADataFiscal: TDateField;
    QrASINTEGRA_ExpDeclNum: TWideStringField;
    QrASINTEGRA_ExpDeclDta: TDateField;
    QrASINTEGRA_ExpNat: TWideStringField;
    QrASINTEGRA_ExpRegNum: TWideStringField;
    QrASINTEGRA_ExpRegDta: TDateField;
    QrASINTEGRA_ExpConhNum: TWideStringField;
    QrASINTEGRA_ExpConhDta: TDateField;
    QrASINTEGRA_ExpConhTip: TWideStringField;
    QrASINTEGRA_ExpPais: TWideStringField;
    QrASINTEGRA_ExpAverDta: TDateField;
    QrACodInfoEmit: TIntegerField;
    QrACodInfoDest: TIntegerField;
    QrACriAForca: TSmallintField;
    QrACodInfoTrsp: TIntegerField;
    QrACodInfoCliI: TIntegerField;
    QrAOrdemServ: TIntegerField;
    QrASituacao: TSmallintField;
    QrAAntigo: TWideStringField;
    QrANFG_Serie: TWideStringField;
    QrANF_ICMSAlq: TFloatField;
    QrANF_CFOP: TWideStringField;
    QrAImportado: TSmallintField;
    QrANFG_SubSerie: TWideStringField;
    QrANFG_ValIsen: TFloatField;
    QrANFG_NaoTrib: TFloatField;
    QrANFG_Outros: TFloatField;
    QrACOD_MOD: TWideStringField;
    QrACOD_SIT: TSmallintField;
    QrAVL_ABAT_NT: TFloatField;
    QrAEFD_INN_AnoMes: TIntegerField;
    QrAEFD_INN_Empresa: TIntegerField;
    QrAEFD_INN_LinArq: TIntegerField;
    QrAICMSRec_vBCST: TFloatField;
    QrAICMSRec_vICMSST: TFloatField;
    QrAEFD_EXP_REG: TWideStringField;
    QrAICMSRec_pAliqST: TFloatField;
    QrAeveMDe_Id: TWideStringField;
    QrAeveMDe_tpAmb: TSmallintField;
    QrAeveMDe_verAplic: TWideStringField;
    QrAeveMDe_cOrgao: TSmallintField;
    QrAeveMDe_cStat: TIntegerField;
    QrAeveMDe_xMotivo: TWideStringField;
    QrAeveMDe_chNFe: TWideStringField;
    QrAeveMDe_tpEvento: TIntegerField;
    QrAeveMDe_xEvento: TWideStringField;
    QrAeveMDe_nSeqEvento: TSmallintField;
    QrAeveMDe_CNPJDest: TWideStringField;
    QrAeveMDe_CPFDest: TWideStringField;
    QrAeveMDe_emailDest: TWideStringField;
    QrAeveMDe_dhRegEvento: TDateTimeField;
    QrAeveMDe_TZD_UTC: TFloatField;
    QrAeveMDe_nProt: TWideStringField;
    QrAcSitNFe: TSmallintField;
    QrAcSitConf: TSmallintField;
    QrAvBasTrib: TFloatField;
    QrApTotTrib: TFloatField;
    QrAinfCCe_verAplic: TWideStringField;
    QrAinfCCe_cOrgao: TSmallintField;
    QrAinfCCe_tpAmb: TSmallintField;
    QrAinfCCe_CNPJ: TWideStringField;
    QrAinfCCe_CPF: TWideStringField;
    QrAinfCCe_chNFe: TWideStringField;
    QrAinfCCe_dhEvento: TDateTimeField;
    QrAinfCCe_dhEventoTZD: TFloatField;
    QrAinfCCe_tpEvento: TIntegerField;
    QrAinfCCe_nSeqEvento: TIntegerField;
    QrAinfCCe_verEvento: TFloatField;
    QrAinfCCe_xCorrecao: TWideMemoField;
    QrAinfCCe_cStat: TIntegerField;
    QrAinfCCe_dhRegEvento: TDateTimeField;
    QrAinfCCe_dhRegEventoTZD: TFloatField;
    QrAinfCCe_nProt: TWideStringField;
    QrAinfCCe_nCondUso: TIntegerField;
    QrAAWServerID: TIntegerField;
    QrAAWStatSinc: TSmallintField;
    QrAURL_QRCode: TWideStringField;
    frxNFCe_02_Off_Line_Univers_Light_Condensed: TfrxReport;
    QrNFeCabAFatID: TIntegerField;
    QrNFeCabAFatNum: TIntegerField;
    QrNFeCabAEmpresa: TIntegerField;
    frxNFCe_01_Autorizada: TfrxReport;
    frxNFCe_02_Off_Line: TfrxReport;
    ListBox1: TListBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrNFeLEnCBeforeClose(DataSet: TDataSet);
    procedure QrNFeLEnCBeforeOpen(DataSet: TDataSet);
    procedure DBEdit3Change(Sender: TObject);
    procedure DBEdit1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdFatIDRedefinido(Sender: TObject);
    procedure EdFatNumRedefinido(Sender: TObject);
    procedure EdEmpresaRedefinido(Sender: TObject);
    procedure QrACalcFields(DataSet: TDataSet);
    procedure EdIDCtrlRedefinido(Sender: TObject);
    procedure QrIAfterScroll(DataSet: TDataSet);
    procedure frxNFCe_01_Autorizada_UniversLight_CondensedGetValue(const VarName: string; var Value: Variant);
    procedure QrNFeCabAAfterOpen(DataSet: TDataSet);
    procedure frxReportAfterPrintReport(Sender: TObject);
  private
    { Private declarations }
    FFatID, FFatNum, FEmpresa: Integer;
    F_tPag, F_tpIntegra, F_tBand: MyArrayLista;
    //
    procedure DefineFrx(Report: TfrxReport; OQueFaz: TfrxImpComo);
    function  Interrompe(Passo: Integer): Boolean;
    procedure ReopenNFeCabA();
    procedure TraduzMensagem(CodErro: Integer);
    //
    procedure ImprimeDireto(const frxReport: TfrxReport; var AchouImpressora,
              Imprimiu: Boolean);
  public
    { Public declarations }
    FIDCtrl, Fide_tpEmis: Integer;
    FDANFEImpresso: Boolean;
    FImprimiu: Boolean;
    //
    function  AutorizarNFCeImpressaOffLine(Form: TForm): Boolean;
    function  EnviarNFe(): Boolean;
    procedure Imprime_Autorizada(OQueFaz: TfrxImpComo);
    procedure Imprime_Off_Line(OQueFaz: TfrxImpComo);
    procedure ReabreNFeLEnC(Codigo: Integer);
    function  SohImprimeEMudaStatusParaPrecisaEnviar(): Boolean;
    //
  end;

  var
  FmNFCeLEnU_0400: TFmNFCeLEnU_0400;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule,
  ModuleNFe_0000, NFeSteps_0400, MyDBCheck, NFe_Pesq_0000, NFCeEmiss_0400,
  MeuFrx, UnDmkProcFunc, NFe_PF, Printers, DmkACBrNFeSteps_0400, UnGrl_Vars;

{$R *.DFM}

function TFmNFCeLEnU_0400.AutorizarNFCeImpressaOffLine(Form: TForm): Boolean;
const
  Modelo = CO_MODELO_NFE_65;
  sProcName = 'TFmNFCeLEnU_0400.AutorizarNFCeImpressaOffLine()';
var
  CodUsu, Codigo, Empresa, LoteEnv, FatID, FatNum, Status, infProt_cStat,
  indSinc: Integer;
  infProt_xMotivo: String;
  Sincronia: TXXeIndSinc;
  sMsg: String;
begin
  Result := False;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando');
  if TFmNFe_Pesq_0000(Form).QrNFeCabAIDCtrl.Value > 0 then
  begin
    (* O apenas gera � feito automatico na NF-e 3.10
    if not TFmNFe_Pesq_0000(Form).GerarNFe(True, True, True) then
      if Interrompe(201) then Exit;
    *)
  end else
  begin
    //Fazer? if not TFmNFe_Pesq_0000(Form).GerarNFe(True, False, True) then
    if Interrompe(201) then Exit;
  end;
  CheckBox1.Checked := True;
  FIDCtrl := TFmNFe_Pesq_0000(Form).QrNFeCabAIDCtrl.Value;

  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  //

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando Lote de Envio');
  Empresa := TFmNFe_Pesq_0000(Form).QrNFeCabAEmpresa.Value;
  CodUsu := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'nfelenc', 'CodUsu', [], [],
    stIns, 0, siPositivo, nil);
  Codigo := DModG.BuscaProximoCodigoInt('nfectrl', 'nfelenc', '', 0, 999999999, '');
  //indSinc := TFmNFe_Pesq_0000(Form).QrNFeCabAide_indSinc.Value;
  indSinc := 1; // Sincrono!
  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfelenc', False, [
  'CodUsu', 'Empresa', 'indSinc'], ['Codigo'], [
  CodUsu, Empresa, indSinc], [Codigo], True) then
    if Interrompe(202) then Exit;
  CheckBox2.Checked := True;

  //

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Adicionando NF-e ao Lote criado');
  LoteEnv := Codigo;
  ReabreNFeLEnC(LoteEnv);
  FatID := TFmNFe_Pesq_0000(Form).QrNFeCabAFatID.Value;
  FatNum := TFmNFe_Pesq_0000(Form).QrNFeCabAFatNum.Value;
  Status  := DmNFe_0000.stepNFeAdedLote();
  infProt_cStat   := 0; // Limpar variaveis (ver historico)
  infProt_xMotivo := '';
  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
  'LoteEnv', 'Status', 'infProt_cStat', 'infProt_xMotivo'],
  ['FatID', 'FatNum', 'Empresa'], [
  LoteEnv, Status, infProt_cStat, infProt_xMotivo],
  [FatID, FatNum, Empresa], True) then
    if Interrompe(203) then Exit;
  CheckBox3.Checked := True;

  //
  Sincronia := TXXeIndSinc(indSinc);
  //

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviando Lote ao Fisco');
  // TFormaGerenXXe = (fgxxeCAPICOM=0, fgxxeCMaisMais=1, fgxxeACBr=2);
  case TFormaGerenXXe(VAR_DFeAppCode) of
    (*0*)fgxxeCAPICOM:
    begin
      if DBCheck.CriaFm(TFmNFeSteps_0400, FmNFeSteps_0400, afmoNegarComAviso) then
      begin
        FmNFeSteps_0400.PnLoteEnv.Visible := True;
        FmNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerEnvLot.Value;
        FmNFeSteps_0400.Show;
        //
        FmNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvEnvioLoteNFe);// 1 - Envia lote ao fisco
        FmNFeSteps_0400.RGIndSinc.ItemIndex := Integer(Sincronia);
        FmNFeSteps_0400.PreparaEnvioDeLoteNFe(LoteEnv, Empresa, Sincronia, Modelo);
        // Ver o que fazer!
        FmNFeSteps_0400.FFormChamou      := 'FmNFCeLEnU_0400';
        //
        FmNFeSteps_0400.BtOKClick(Self);
        //
        CheckBox4.Checked := True;
        ReabreNFeLEnC(LoteEnv);
        if not (QrNFeLEnCcStat.Value in ([103, 104, 105])) then
        begin
          if Interrompe(204) then Exit
          else
            FmNFeSteps_0400.Destroy;
        end;
        //
        if Sincronia = TXXeIndSinc.nisAssincrono then
        begin
          //
          ReabreNFeLEnC(LoteEnv);
          // Usar Ck???.Checked se der muitos bugs
          while QrNFeLEnCcStat.Value = 105 do
          begin
            Sleep(5000);
            FmNFeSteps_0400.BtOKClick(Self);
          end;
          // Esperar Timer1
          while FmNFeSteps_0400.FSegundos < FmNFeSteps_0400.FSecWait do
          begin
            FmNFeSteps_0400.LaWait.Update;
            Application.ProcessMessages;
          end;
        end;
        FmNFeSteps_0400.Destroy;
      end else if
        Interrompe(205) then Exit;
      //CheckBox4.Checked := True;

        //

      if Sincronia = TXXeIndSinc.nisAssincrono then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Consultando Lote no Fisco');
        if DBCheck.CriaFm(TFmNFeSteps_0400, FmNFeSteps_0400, afmoNegarComAviso) then
        begin
          FmNFeSteps_0400.PnLoteEnv.Visible := True;
          FmNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerConLot.Value;
          FmNFeSteps_0400.Show;
          //
          FmNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvConsultarLoteNfeEnviado); // 2 - Verifica lote no fisco
          FmNFeSteps_0400.PreparaConsultaLote(QrNFeLEnCCodigo.Value, QrNFeLEnCEmpresa.Value,
            QrNFeLEnCnRec.Value);
          FmNFeSteps_0400.FFormChamou      := 'FmNFCeLEnU_0400';
          //
          FmNFeSteps_0400.BtOKClick(Self);
          //
          FmNFeSteps_0400.Destroy;
          if QrNFeLEnCcStat.Value <> 104 then
            if Interrompe(207) then Exit;
        end else if
          Interrompe(206) then Exit;
        CheckBox5.Checked := True;
        //
        FIDCtrl := TFmNFe_Pesq_0000(Form).QrNFeCabAIDCtrl.Value;
      end else
      begin
        // Consulta sincrona!
        CheckBox5.Checked := True;
        ReopenNFeCabA();
        FIDCtrl := QrNFeCabAIDCtrl.Value;
      end;
      //
    end;
    //(*1*)fgxxeCMaisMais:
    (*2*)fgxxeACBr:
    begin
      //Result := EnviarNFe_ACBr(Modelo);
      // DmNFe_0000.ACBrNFe1.Enviar(LoteEnv, ImprimirACBr, SincronoACBr, ZipadoACBr);
      // FTextoArq := DmNFe_0000.ACBrNFe1.WebServices.Retorno.RetornoWS;
      //
      if DBCheck.CriaFm(TFmDmkACBrNFeSteps_0400, FmDmkACBrNFeSteps_0400, afmoNegarComAviso) then
      begin
        FmDmkACBrNFeSteps_0400.PnLoteEnv.Visible := True;
        FmDmkACBrNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerEnvLot.Value;
        FmDmkACBrNFeSteps_0400.Show;
        //
        FmDmkACBrNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvEnvioLoteNFe);// 1 - Envia lote ao fisco
        FmDmkACBrNFeSteps_0400.RGIndSinc.ItemIndex := Integer(Sincronia);
        FmDmkACBrNFeSteps_0400.PreparaEnvioDeLoteNFe(LoteEnv, Empresa, Sincronia, Modelo);
        // Ver o que fazer!
        FmDmkACBrNFeSteps_0400.FFormChamou      := 'FmNFCeLEnU_0400';
        //
        FmDmkACBrNFeSteps_0400.BtOKClick(Self);
        //
        CheckBox4.Checked := True;
        ReabreNFeLEnC(LoteEnv);
        if not (QrNFeLEnCcStat.Value in ([103, 104, 105])) then
        begin
          if Interrompe(204) then Exit
          else
            FmDmkACBrNFeSteps_0400.Destroy;
        end;
        //
        if Sincronia = TXXeIndSinc.nisAssincrono then
        begin
          //
          ReabreNFeLEnC(LoteEnv);
          // Usar Ck???.Checked se der muitos bugs
          while QrNFeLEnCcStat.Value = 105 do
          begin
            Sleep(5000);
            FmDmkACBrNFeSteps_0400.BtOKClick(Self);
          end;
          // Esperar Timer1
          while FmDmkACBrNFeSteps_0400.FSegundos < FmDmkACBrNFeSteps_0400.FSecWait do
          begin
            FmDmkACBrNFeSteps_0400.LaWait.Update;
            Application.ProcessMessages;
          end;
        end;
        FmDmkACBrNFeSteps_0400.Destroy;
      end else if
        Interrompe(205) then Exit;
      //CheckBox4.Checked := True;

        //

      if Sincronia = TXXeIndSinc.nisAssincrono then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Consultando Lote no Fisco');
        if DBCheck.CriaFm(TFmDmkACBrNFeSteps_0400, FmDmkACBrNFeSteps_0400, afmoNegarComAviso) then
        begin
          FmDmkACBrNFeSteps_0400.PnLoteEnv.Visible := True;
          FmDmkACBrNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerConLot.Value;
          FmDmkACBrNFeSteps_0400.Show;
          //
          FmDmkACBrNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvConsultarLoteNfeEnviado); // 2 - Verifica lote no fisco
          FmDmkACBrNFeSteps_0400.PreparaConsultaLote(QrNFeLEnCCodigo.Value, QrNFeLEnCEmpresa.Value,
            QrNFeLEnCnRec.Value);
          FmDmkACBrNFeSteps_0400.FFormChamou      := 'FmNFCeLEnU_0400';
          //
          FmDmkACBrNFeSteps_0400.BtOKClick(Self);
          //
          FmDmkACBrNFeSteps_0400.Destroy;
          if QrNFeLEnCcStat.Value <> 104 then
            if Interrompe(207) then Exit;
        end else if
          Interrompe(206) then Exit;
        CheckBox5.Checked := True;
        //
        FIDCtrl := TFmNFe_Pesq_0000(Form).QrNFeCabAIDCtrl.Value;
      end else
      begin
        // Consulta sincrona!
        CheckBox5.Checked := True;
        ReopenNFeCabA();
        FIDCtrl := QrNFeCabAIDCtrl.Value;
      end;
      //
    end;
    (*?*)else
    begin
      sMsg := GetEnumName(TypeInfo(TFormaGerenXXe), Integer(VarType(VAR_DFeAppCode)));
      Geral.MB_Erro('Forma de gerenciamento de DFe (' + sMsg + ') n�o definido em ' + sProcName);
    end;
  end;

{
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Localizando NF-e');
  //

  FIDCtrl := TFmNFe_Pesq_0000(Form).QrNFeCabAIDCtrl.Value;
  if FIDCtrl <> 0 then
  begin
    EdIDCtrl.ValueVariant := FIDCtrl;
    //DBCheck.CriaFm(TTFmNFe_Pesq_0000(Form), TFmNFe_Pesq_0000(Form), afmoNegarComAviso) then
    if EdIDCtrl.ValueVariant = FIDCtrl then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo dados da NF-e');
      ReopenNFeCabA();
      if (QrNFeCabA.State <> dsInactive) then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando unicidade da NF-e');
        if (QrNFeCabA.RecordCount = 1) then
        begin
          if (QrNFeCabAStatus.Value = 100) then
          //if BtImprime.Enabled then
          begin
            ? DefineFrx(TFmNFe_Pesq_0000(Form).frxA4A_002, ficMostra);
            ? DefineFrx(frxNFCe_01_Autorizada, TfrxImpComo.ficMostra);
            CheckBox6.Checked := FDANFEImpresso;
            //
            // Enviar email - FAZER ?????
            //TFmNFe_Pesq_0000(Form).BtEnviaClick(Self);
            //
            //CheckBox7.Checked := TFmNFe_Pesq_0000(Form).FMailEnviado;
            //
            MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
          end else
          begin
            if Interrompe(212) then Exit;
          end;
        end else
        begin
          if Interrompe(211) then Exit;
        end;
      end else
      begin
        if Interrompe(210) then Exit;
      end;
    end else if
      Interrompe(209) then Exit;
    CheckBox5.Checked := True;
    //
    //
  end else
    if Interrompe(208) then Exit;
}
  //
  Result := True;
end;

procedure TFmNFCeLEnU_0400.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFCeLEnU_0400.DBEdit1Change(Sender: TObject);
var
  Cor: TColor;
begin
  if QrNFeLEnC.State <> dsInactive then
  begin
    case QrNFeLEnCcStat.Value of
      0..99: Cor := clBlack;
      103: Cor := clGreen;
      104: Cor := clBlue;
      105: Cor := clPurple;
      else Cor := clRed;
    end;
    DBEdit1.Font.Color := Cor;
    DBEdit2.Font.Color := Cor;
  end;
  TraduzMensagem(QrNFeLEnCcStat.Value);
end;

procedure TFmNFCeLEnU_0400.DBEdit3Change(Sender: TObject);
var
  Cor: TColor;
begin
  if QrNFeCabA.State <> dsInactive then
  begin
    case QrNFeCabAStatus.Value of
      0..99: Cor := clBlack;
      100: Cor := clBlue;
      101..199: Cor := clPurple;
      else Cor := clRed;
    end;
    DBEdit3.Font.Color := Cor;
    DBEdit4.Font.Color := Cor;
  end;
  TraduzMensagem(QrNFeLEnCcStat.Value);
end;

procedure TFmNFCeLEnU_0400.DefineFrx(Report: TfrxReport; OQueFaz: TfrxImpComo);
  procedure PreparaFrx(MostraMeuFrx: Boolean);
  var
    I, Modelo, Retorno: Integer;
    AchouDireto, ImprimiuDireto: Boolean;
    sIP, sCmdtx: AnsiString;
  begin
    ImprimiuDireto := False;
    Report.PreviewOptions.ZoomMode := zmPageWidth;
    MyObjects.frxPrepara(Report, 'NFCe' + QrAId.Value);
    {
    for I := 0 to Report.PreviewPages.Count -1 do
    begin
      Report.PreviewPages.Page[I].PaperSize := DMPAPER_A4;
      Report.PreviewPages.ModifyPage(I, Report.PreviewPages.Page[I]);
    end;
    }
    FmMeuFrx.BtEdit.Enabled := False;
    if MostraMeuFrx = False then
    begin
      ImprimeDireto(FmMeuFrx.PvVer.Report, AchouDireto, ImprimiuDireto);
    end;
    if MostraMeuFrx or (ImprimiuDireto = False) then
    begin
      FmMeuFrx.ShowModal;
    end;
    FmMeuFrx.Destroy;
    FDANFEImpresso := True;
    //
    // Se � para cortar o papel...
    if (DModG.QrPrmsEmpNFeAutoCutNFCe.Value = 1) and (FImprimiu) then
    begin
      try
        (*Bematech_PF.*)ConfiguraModeloImpressora(Modelo);
        //COMANDO DE ABERTURA DA PORTA DE COMUNICA��O
        Modelo := Bematech_PF.ModeloImpressoraTermica(DmodG.QrPrmsEmpNFeTipoPrintNFCe.Value);
        if Modelo > -1 then
        begin
          sIP := DmodG.QrPrmsEmpNFeIPPrintNFCe.Value;
          Retorno := IniciaPorta(AnsiString(sIP));
          if Retorno = 1 then
          begin
            // Corte parcial do papel na impressora
            sCmdtx := #27+#109;
            Retorno := ComandoTX(sCmdtx, Length(sCmdtx));
            if Retorno <> 1 then
              Geral.MB_Erro('N�o foi poss�vel fazer o corte parcial do papel na impressora!' + sLineBreak +
              DmodG.QrPrmsEMpNFeNomePrintNFCe.Value);
          end else
            Geral.MB_Erro('N�o foi poss�vel conectar na impressora!' + sLineBreak +
            DmodG.QrPrmsEMpNFeNomePrintNFCe.Value + sLineBreak +
            'IP: ' + sIP);
        end;
      finally
        FechaPorta;
      end;
    end;
  end;
  //Page: TfrxReportPage;
begin
  FDANFEImpresso := False;
  //
  Screen.Cursor := crHourGlass;
  MyObjects.frxDefineDataSets(Report, [
    frxDsA,
    frxDsI,
    frxDsM,
    frxDsN,
    frxDsO,
    //
    frxDsV,
    frxDsYA
  ]);
  //
  DModG.ReopenParamsEmp(DmodG.QrEmpresasCodigo.Value);
  //
  QrA.Close;
  QrA.Database := Dmod.MyDB;
  QrA.Params[00].AsInteger := FFatID;
  QrA.Params[01].AsInteger := FFatNum;
  QrA.Params[02].AsInteger := FEmpresa;
  UnDmkDAC_PF.AbreQuery(QrA, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
(*
  QrF.Close;
  QrF.Database := Dmod.MyDB;
  QrF.Params[00].AsInteger := FFatID;
  QrF.Params[01].AsInteger := FFatNum;
  QrF.Params[02].AsInteger := FEmpresa;
  UnDmkDAC_PF.AbreQuery(QrF, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  QrG.Close;
  QrG.Database := Dmod.MyDB;
  QrG.Params[00].AsInteger := FFatID;
  QrG.Params[01].AsInteger := FFatNum;
  QrG.Params[02].AsInteger := FEmpresa;
  UnDmkDAC_PF.AbreQuery(QrG, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
*)
  QrI.Close;
  QrI.Database := Dmod.MyDB;
  QrI.Params[00].AsInteger := FFatID;
  QrI.Params[01].AsInteger := FFatNum;
  QrI.Params[02].AsInteger := FEmpresa;
  UnDmkDAC_PF.AbreQuery(QrI, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
(*
  QrY.Close;
  QrY.Database := Dmod.MyDB;
  QrY.Params[00].AsInteger := FFatID;
  QrY.Params[01].AsInteger := FFatNum;
  QrY.Params[02].AsInteger := FEmpresa;
  UnDmkDAC_PF.AbreQuery(QrY, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  QrXVol.Close;
  QrXVol.Database := Dmod.MyDB;
  QrXVol.Params[00].AsInteger := FFatID;
  QrXVol.Params[01].AsInteger := FFatNum;
  QrXVol.Params[02].AsInteger := FEmpresa;
  UnDmkDAC_PF.AbreQuery(QrXVol, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
*)
  QrYA.Close;
  QrYA.Database := Dmod.MyDB;
  QrYA.Params[00].AsInteger := FFatID;
  QrYA.Params[01].AsInteger := FFatNum;
  QrYA.Params[02].AsInteger := FEmpresa;
  UnDmkDAC_PF.AbreQuery(QrYA, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  case OQueFaz of
    ficMostra: //MyObjects.frxMostra(Report, 'NFe' + QrAId);
    begin
      //FmMeuFrx.PvVer.Print;
      PreparaFrx(True);
    end;
    ficImprime:
    begin
      //ImprimeDireto();
      PreparaFrx(False);
    end;
    {ficSalva}
    ficNone: ;// N�o faz nada!
    ficExporta: MyObjects.frxPrepara(Report, 'NFe' + QrAId.Value);
    else Geral.MensagemBox('A��o n�o definida para frx da DANFE!',
    'ERRO', MB_OK+MB_ICONERROR);
  end;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmNFCeLEnU_0400.TraduzMensagem(CodErro: Integer);
begin
  case CodErro of
    232: Geral.MB_Aviso('Destinat�rio com IE n�o informado!' + slinebreak +
      'A UF iformou que o destinat�rio possui IE ativa na UF!');
  end;
end;

procedure TFmNFCeLEnU_0400.EdEmpresaRedefinido(Sender: TObject);
begin
  FEmpresa := EdEMpresa.ValueVariant;
end;

procedure TFmNFCeLEnU_0400.EdFatIDRedefinido(Sender: TObject);
begin
  FFatID := EdFatID.ValueVariant;
end;

procedure TFmNFCeLEnU_0400.EdFatNumRedefinido(Sender: TObject);
begin
  FFatNum := EdFatNum.ValueVariant;
end;

procedure TFmNFCeLEnU_0400.EdIDCtrlRedefinido(Sender: TObject);
begin
  FIDCtrl := EdIDCtrl.ValueVariant;
end;

function TFmNFCeLEnU_0400.EnviarNFe(): Boolean;
const
  Modelo = CO_MODELO_NFE_65;
  sProcName = 'TFmNFCeLEnU_0400.EnviarNFe()';
var
  CodUsu, Codigo, Empresa, LoteEnv, FatID, FatNum, Status, infProt_cStat,
  indSinc: Integer;
  infProt_xMotivo: String;
  Sincronia: TXXeIndSinc;
  sMsg: String;
begin
  Result := False;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando XML da NF-e');
  if FmNFCeEmiss_0400.QrNFeCabAIDCtrl.Value > 0 then
  begin
    (* O apenas gera � feito automatico na NF-e 3.10
    if not FmNFCeEmiss_0400.GerarNFe(True, True, True) then
      if Interrompe(1) then Exit;
    *)
  end else
  begin
    //Fazer? if not FmNFCeEmiss_0400.GerarNFe(True, False, True) then
    if Interrompe(1) then Exit;
  end;
  CheckBox1.Checked := True;
  FIDCtrl := FmNFCeEmiss_0400.QrNFeCabAIDCtrl.Value;

  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  //

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando Lote de Envio');
  Empresa := FmNFCeEmiss_0400.QrFatPedNFs_BEmpresa.Value;
  CodUsu := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'nfelenc', 'CodUsu', [], [],
    stIns, 0, siPositivo, nil);
  Codigo := DModG.BuscaProximoCodigoInt('nfectrl', 'nfelenc', '', 0, 999999999, '');
  indSinc := FmNFCeEmiss_0400.QrFatPedCabindSinc.Value;
  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfelenc', False, [
  'CodUsu', 'Empresa', 'indSinc'], ['Codigo'], [
  CodUsu, Empresa, indSinc], [Codigo], True) then
    if Interrompe(2) then Exit;
  CheckBox2.Checked := True;

  //

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Adicionando NF-e ao Lote criado');
  LoteEnv := Codigo;
  ReabreNFeLEnC(LoteEnv);
  FatID := FmNFCeEmiss_0400.QrFatPedNFs_BCabA_FatID.Value;
  FatNum := FmNFCeEmiss_0400.QrFatPedNFs_BOriCodi.Value;
  Status  := DmNFe_0000.stepNFeAdedLote();
  infProt_cStat   := 0; // Limpar variaveis (ver historico)
  infProt_xMotivo := '';
  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
  'LoteEnv', 'Status', 'infProt_cStat', 'infProt_xMotivo'],
  ['FatID', 'FatNum', 'Empresa'], [
  LoteEnv, Status, infProt_cStat, infProt_xMotivo],
  [FatID, FatNum, Empresa], True) then
    if Interrompe(3) then Exit;
  CheckBox3.Checked := True;

  //
  Sincronia := TXXeIndSinc(indSinc);
  //

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviando Lote ao Fisco');
  // TFormaGerenXXe = (fgxxeCAPICOM=0, fgxxeCMaisMais=1, fgxxeACBr=2);
  case TFormaGerenXXe(VAR_DFeAppCode) of
    (*0*)fgxxeCAPICOM:
    begin
      if DBCheck.CriaFm(TFmNFeSteps_0400, FmNFeSteps_0400, afmoNegarComAviso) then
      begin
        FmNFeSteps_0400.PnLoteEnv.Visible := True;
        FmNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerEnvLot.Value;
        FmNFeSteps_0400.Show;
        //
        FmNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvEnvioLoteNFe);// 1 - Envia lote ao fisco
        FmNFeSteps_0400.RGIndSinc.ItemIndex := Integer(Sincronia);
        FmNFeSteps_0400.PreparaEnvioDeLoteNFe(LoteEnv, Empresa, Sincronia, Modelo);
        // Ver o que fazer!
        FmNFeSteps_0400.FFormChamou      := 'FmNFCeLEnU_0400';
        //
        FmNFeSteps_0400.BtOKClick(Self);
        //
        CheckBox4.Checked := True;
        ReabreNFeLEnC(LoteEnv);
        if not (QrNFeLEnCcStat.Value in ([103, 104, 105])) then
        begin
          if Interrompe(4) then Exit
          else
            FmNFeSteps_0400.Destroy;
        end;
        //
        if Sincronia = TXXeIndSinc.nisAssincrono then
        begin
          //
          ReabreNFeLEnC(LoteEnv);
          // Usar Ck???.Checked se der muitos bugs
          while QrNFeLEnCcStat.Value = 105 do
          begin
            Sleep(5000);
            FmNFeSteps_0400.BtOKClick(Self);
          end;
          // Esperar Timer1
          while FmNFeSteps_0400.FSegundos < FmNFeSteps_0400.FSecWait do
          begin
            FmNFeSteps_0400.LaWait.Update;
            Application.ProcessMessages;
          end;
        end;
        FmNFeSteps_0400.Destroy;
      end else if
        Interrompe(5) then Exit;
      //CheckBox4.Checked := True;

        //

      if Sincronia = TXXeIndSinc.nisAssincrono then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Consultando Lote no Fisco');
        if DBCheck.CriaFm(TFmNFeSteps_0400, FmNFeSteps_0400, afmoNegarComAviso) then
        begin
          FmNFeSteps_0400.PnLoteEnv.Visible := True;
          FmNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerConLot.Value;
          FmNFeSteps_0400.Show;
          //
          FmNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvConsultarLoteNfeEnviado); // 2 - Verifica lote no fisco
          FmNFeSteps_0400.PreparaConsultaLote(QrNFeLEnCCodigo.Value, QrNFeLEnCEmpresa.Value,
            QrNFeLEnCnRec.Value);
          FmNFeSteps_0400.FFormChamou      := 'FmNFCeLEnU_0400';
          //
          FmNFeSteps_0400.BtOKClick(Self);
          //
          FmNFeSteps_0400.Destroy;
          if QrNFeLEnCcStat.Value <> 104 then
            if Interrompe(7) then Exit;
        end else if
          Interrompe(6) then Exit;
        CheckBox5.Checked := True;
        //
        FIDCtrl := FmNFCeEmiss_0400.QrNFeCabAIDCtrl.Value;
      end else
      begin
        // Consulta sincrona!
        CheckBox5.Checked := True;
        ReopenNFeCabA();
        FIDCtrl := QrNFeCabAIDCtrl.Value;
      end;
      //
    end;
    //(*1*)fgxxeCMaisMais:
    (*2*)fgxxeACBr:
    begin
      //Result := EnviarNFe_ACBr(Modelo);
      // DmNFe_0000.ACBrNFe1.Enviar(LoteEnv, ImprimirACBr, SincronoACBr, ZipadoACBr);
      // FTextoArq := DmNFe_0000.ACBrNFe1.WebServices.Retorno.RetornoWS;
      //
       if DBCheck.CriaFm(TFmDmkACBrNFeSteps_0400, FmDmkACBrNFeSteps_0400, afmoNegarComAviso) then
      begin
        FmDmkACBrNFeSteps_0400.PnLoteEnv.Visible := True;
        FmDmkACBrNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerEnvLot.Value;
        FmDmkACBrNFeSteps_0400.Show;
        //
        FmDmkACBrNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvEnvioLoteNFe);// 1 - Envia lote ao fisco
        FmDmkACBrNFeSteps_0400.RGIndSinc.ItemIndex := Integer(Sincronia);
        FmDmkACBrNFeSteps_0400.PreparaEnvioDeLoteNFe(LoteEnv, Empresa, Sincronia, Modelo);
        // Ver o que fazer!
        FmDmkACBrNFeSteps_0400.FFormChamou      := 'FmNFCeLEnU_0400';
        //
        FmDmkACBrNFeSteps_0400.BtOKClick(Self);
        //
        CheckBox4.Checked := True;
        ReabreNFeLEnC(LoteEnv);
        if not (QrNFeLEnCcStat.Value in ([103, 104, 105])) then
        begin
          if Interrompe(4) then Exit
          else
            FmDmkACBrNFeSteps_0400.Destroy;
        end;
        //
        if Sincronia = TXXeIndSinc.nisAssincrono then
        begin
          //
          ReabreNFeLEnC(LoteEnv);
          // Usar Ck???.Checked se der muitos bugs
          while QrNFeLEnCcStat.Value = 105 do
          begin
            Sleep(5000);
            FmDmkACBrNFeSteps_0400.BtOKClick(Self);
          end;
          // Esperar Timer1
          while FmDmkACBrNFeSteps_0400.FSegundos < FmDmkACBrNFeSteps_0400.FSecWait do
          begin
            FmDmkACBrNFeSteps_0400.LaWait.Update;
            Application.ProcessMessages;
          end;
        end;
        FmDmkACBrNFeSteps_0400.Destroy;
      end else if
        Interrompe(5) then Exit;
      //CheckBox4.Checked := True;

        //

      if Sincronia = TXXeIndSinc.nisAssincrono then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Consultando Lote no Fisco');
        if DBCheck.CriaFm(TFmDmkACBrNFeSteps_0400, FmDmkACBrNFeSteps_0400, afmoNegarComAviso) then
        begin
          FmDmkACBrNFeSteps_0400.PnLoteEnv.Visible := True;
          FmDmkACBrNFeSteps_0400.EdVersaoAcao.ValueVariant := DModG.QrPrmsEmpNFeNFeVerConLot.Value;
          FmDmkACBrNFeSteps_0400.Show;
          //
          FmDmkACBrNFeSteps_0400.RGAcao.ItemIndex := Integer(TNFeServicoStep.nfesrvConsultarLoteNfeEnviado); // 2 - Verifica lote no fisco
          FmDmkACBrNFeSteps_0400.PreparaConsultaLote(QrNFeLEnCCodigo.Value, QrNFeLEnCEmpresa.Value,
            QrNFeLEnCnRec.Value);
          FmDmkACBrNFeSteps_0400.FFormChamou      := 'FmNFCeLEnU_0400';
          //
          FmDmkACBrNFeSteps_0400.BtOKClick(Self);
          //
          FmDmkACBrNFeSteps_0400.Destroy;
          if QrNFeLEnCcStat.Value <> 104 then
            if Interrompe(7) then Exit;
        end else if
          Interrompe(6) then Exit;
        CheckBox5.Checked := True;
        //
        FIDCtrl := FmNFCeEmiss_0400.QrNFeCabAIDCtrl.Value;
      end else
      begin
        // Consulta sincrona!
        CheckBox5.Checked := True;
        ReopenNFeCabA();
        FIDCtrl := QrNFeCabAIDCtrl.Value;
      end;
      //
    end;
    (*?*)else
    begin
      sMsg := GetEnumName(TypeInfo(TFormaGerenXXe), Integer(VarType(VAR_DFeAppCode)));
      Geral.MB_Erro('Forma de gerenciamento de DFe (' + sMsg + ') n�o definido em ' + sProcName);
    end;
  end;


  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Localizando NF-e');
  //

  FIDCtrl := FmNFCeEmiss_0400.QrNFeCabAIDCtrl.Value;
  if FIDCtrl <> 0 then
  begin
    EdIDCtrl.ValueVariant := FIDCtrl;
    //DBCheck.CriaFm(TFmNFe_Pesq_0000, FmNFe_Pesq_0000, afmoNegarComAviso) then
    if EdIDCtrl.ValueVariant = FIDCtrl then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo dados da NF-e');
      ReopenNFeCabA();
      if (QrNFeCabA.State <> dsInactive) then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando unicidade da NF-e');
        if (QrNFeCabA.RecordCount = 1) then
        begin
          if (QrNFeCabAStatus.Value = 100) then
          //if BtImprime.Enabled then
          begin
            //DefineFrx(FmNFe_Pesq_0000.frxA4A_002, ficMostra);
            DefineFrx(frxNFCe_01_Autorizada, TfrxImpComo.ficImprime);
            CheckBox6.Checked := FDANFEImpresso;
            //
            // Enviar email - FAZER ?????
            //FmNFe_Pesq_0000.BtEnviaClick(Self);
            //
            //CheckBox7.Checked := FmNFe_Pesq_0000.FMailEnviado;
            //
            MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
          end else
          begin
            if Interrompe(12) then Exit;
          end;
        end else
        begin
          if Interrompe(11) then Exit;
        end;
      end else
      begin
        if Interrompe(10) then Exit;
      end;
    end else if
      Interrompe(9) then Exit;
    CheckBox5.Checked := True;
    //
    //
  end else
    if Interrompe(8) then Exit;
  //
  Result := True;
end;
{
    if DBCheck.CriaFm(TFmNFe_Pesq_0000, FmNFe_Pesq_0000, afmoNegarComAviso) then
    begin
      //
      FmNFe_Pesq_0000.ReopenNFeCabA(FIDCtrl, True);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo dados da NF-e');
      if (FmNFe_Pesq_0000.QrNFeCabA.State <> dsInactive) then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando unicidade da NF-e');
        if (FmNFe_Pesq_0000.QrNFeCabA.RecordCount = 1) then
        begin
          if (FmNFe_Pesq_0000.QrNFeCabAStatus.Value = 100) then
          //if FmNFe_Pesq_0000.BtImprime.Enabled then
          begin
            //FmNFe_Pesq_0000.DefineFrx(FmNFe_Pesq_0000.frxA4A_002, ficMostra);
            FmNFe_Pesq_0000.DefineQual_frxNFe(ficMostra);
            CheckBox6.Checked := FmNFe_Pesq_0000.FDANFEImpresso;
            //
            FmNFe_Pesq_0000.BtEnviaClick(Self);
            //
            FmNFe_Pesq_0000.Destroy;
            CheckBox7.Checked := FmNFe_Pesq_0000.FMailEnviado;
            //
            MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
          end else begin
            FmNFe_Pesq_0000.Destroy;
            if Interrompe(12) then Exit;
          end;
        end else
        begin
          FmNFe_Pesq_0000.Destroy;
          if Interrompe(11) then Exit;
        end;
      end else
      begin
        FmNFe_Pesq_0000.Destroy;
        if Interrompe(10) then Exit;
      end;
    end else if
      Interrompe(9) then Exit;
    CheckBox5.Checked := True;
    //
    //
  end else
    if Interrompe(8) then Exit;
  //
  Result := True;
end;
}


procedure TFmNFCeLEnU_0400.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmNFCeLEnU_0400.FormCreate(Sender: TObject);
begin
  FIDCtrl := 0;
  F_tPag      := UnNFe_PF.ListaMeiosDePagamento();
end;

procedure TFmNFCeLEnU_0400.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFCeLEnU_0400.frxNFCe_01_Autorizada_UniversLight_CondensedGetValue(const VarName: string;
  var Value: Variant);
var
  Texto: String;
begin
  if VarName = 'VARF_QTDE_ITENS' then
    Value := QrI.RecordCount
  else
  if VarName = 'VARF_tPAG_TXT' then
  begin
    Geral.DescricaoDeArrStrStr(Geral.FF0(QrYAtPag.Value), F_tPag, Texto, 0, 1);
    Value := Texto;
  end else
  if VarName = 'VARF_URL_Consulta_NFCe' then
    Value := QrAURL_Consulta.Value
  else
  if VarName = 'VARF_CHAVE_NFCe_FORMATADA' then
    Value := Geral.FormataChaveNFe(QrAiD.Value, TComoFmtChNFe.cfcnDANFE)
  else
  if VarName = 'LogoNFeExiste' then
    //Value := FileExists(DModG.QrPrmsEmpNFePathLogoNF.Value)
    Value := FileExists(Dmod.QrControleLogoNF.Value)
  else
  if VarName = 'LogoNFePath' then
    //Value := DModG.QrPrmsEmpNFePathLogoNF.Value
    Value := Dmod.QrControleLogoNF.Value
  else
  (*
  if VarName = 'VARF_VIA_DE_QUEM' then
  begin
    /
    Value :=
  end else
  *)
  //...
end;

procedure TFmNFCeLEnU_0400.frxReportAfterPrintReport(Sender: TObject);
begin
  if DModG.QrPrmsEmpNFeAutoCutNFCe.Value = 1 then
    FImprimiu := Geral.MB_Pergunta('A NFCe foi impressa?') = ID_Yes;
end;

procedure TFmNFCeLEnU_0400.ImprimeDireto(const frxReport: TfrxReport; var
  AchouImpressora, Imprimiu: Boolean);
var
  Printer: TPrinter;
  Index, I: Integer;
  NomePrintNFCe: String;
begin
  FImprimiu := False;
  //NomeImpressora := 'MP-4200 TH';
  NomePrintNFCe := DmodG.QrPrmsEMpNFeNomePrintNFCe.Value;
  //
  Index := 0;
  AchouImpressora := False;
  Printer := TPrinter.Create;
  //Printer.Printers;
  ListBox1.Items := printer.Printers;//if it is necessary possible to take list a printer
  for I := 0 to ListBox1.Items.Count - 1 do
  begin
    if AnsiUppercase(ListBox1.Items[I]) = AnsiUppercase(NomePrintNFCe) then
    begin
      Index := I;
      AchouImpressora := True;
      Imprimiu        := True;
    end;
  end;
  if AchouImpressora then
  begin
    FmMeuFrx.PvVer.Report.PrintOptions.ShowDialog := False;
    FmMeuFrx.PvVer.Report.PrintOptions.Printer := NomePrintNFCe;
    FmMeuFrx.PvVer.Report.OnAfterPrintReport := frxReportAfterPrintReport;
    //Imprimiu :=
    FmMeuFrx.PvVer.Print;
  end else
  begin
(*
    FmMeuFrx.PvVer.Report.PrintOptions.ShowDialog := True;
    FmMeuFrx.PvVer.Report.PrintOptions.Printer := 'Default';
    FmMeuFrx.PvVer.Print;
*)
  end;
end;

procedure TFmNFCeLEnU_0400.Imprime_Autorizada(OQueFaz: TfrxImpComo);
begin
  ReopenNFeCabA();
  DefineFrx(frxNFCe_01_Autorizada, OQueFaz);
end;

procedure TFmNFCeLEnU_0400.Imprime_Off_Line(OQueFaz: TfrxImpComo);
begin
  ReopenNFeCabA();
  DefineFrx(frxNFCe_02_Off_Line, OQueFaz);
end;

function TFmNFCeLEnU_0400.Interrompe(Passo: Integer): Boolean;
var
  Aviso: String;
begin
  Result := True;
  Aviso := 'ABORTADO NO PASSO ' + FormatFloat('0', Passo) +
    ' POR ERRO EM: ' + LaAviso1.Caption;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Aviso);
  GBRodaPe.Visible := True;
  ReopenNFeCabA();
end;

procedure TFmNFCeLEnU_0400.QrACalcFields(DataSet: TDataSet);
begin
  QrAId_TXT.Value := DmNFe_0000.FormataID_NFe(QrAId.Value);
  //
  QrAEMIT_ENDERECO.Value := AnsiUppercase(QrAemit_xLgr.Value + ', ' + Geral.FormataNumeroDeRua(
    QrAemit_xLgr.Value,
    QrAemit_nro.Value, True) + ' ' + QrAemit_xCpl.Value + ' ' +
    QrAemit_xBairro.Value + ', ' + QrAemit_xMun.Value + ' - ' +
    QrAemit_UF.Value + ' ' +  'CEP: ' + Geral.FormataCEP_TT(IntToStr(
    QrAemit_CEP.Value))
(*
 CNPJ do Emitente � formatado com a m�scara 99.999.999/9999-99 (ID: C02, tag: CNPJ);
 Raz�o Social do Emitente (ID: C03, tag: xNome);
 Endere�o Completo do Emitente sem a indica��o do pa�s
    ( + '  ' + QrAemit_xPais.Value
*)
    );

  //
  QrAEMIT_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrAemit_fone.Value);
  //
  QrAEMIT_IE_TXT.Value := Geral.Formata_IE(QrAemit_IE.Value, QrAemit_UF.Value, '??');
  QrAEMIT_IEST_TXT.Value := Geral.Formata_IE(QrAemit_IEST.Value, QrAemit_UF.Value, '??');
  //
  QrAEMIT_CNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrAemit_CNPJ.Value);
  //
    //
    ////// DESTINAT�RIO
    //
  //
  if QrACodInfoDest.Value = -2 then  // Consumidor
  begin
    QrADEST_CNPJ_CPF_TIT.Value := 'CONSUMIDOR ';
    QrADEST_CNPJ_CPF_TXT.Value := ' N�O IDENTIFICADO';
  end else
  if QrAdest_CNPJ.Value <> '' then
  begin
    QrADEST_CNPJ_CPF_TIT.Value := 'CONSUMIDOR - CNPJ: ';
    QrADEST_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAdest_CNPJ.Value);
  end else
  if QrAdest_CPF.Value <> '' then
  begin
    QrADEST_CNPJ_CPF_TIT.Value := 'CONSUMIDOR - CPF: ';
    QrADEST_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAdest_CPF.Value);
  end else
  begin
    QrADEST_CNPJ_CPF_TIT.Value := 'CONSUMIDOR - Id. Estrangeiro: ';
    QrADEST_CNPJ_CPF_TXT.Value := QrAdest_idEstrangeiro.Value;
  end;
  //
(*
  if QrAdest_CNPJ.Value <> '' then
    QrADEST_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAdest_CNPJ.Value)
  else
    QrADEST_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAdest_CPF.Value);
*)
  //
  QrADEST_ENDERECO.Value := QrAdest_xLgr.Value + ', ' + Geral.FormataNumeroDeRua(
    QrAdest_xLgr.Value, QrAdest_nro.Value, True) + ' ' + QrAdest_xCpl.Value;
  //
  // 2011-08-25
  if DModG.QrPrmsEmpNFeNFeShowURL.Value <> ''  then
    QrAEMIT_ENDERECO.Value := QrAEMIT_ENDERECO.Value + sLineBreak +
      DModG.QrPrmsEmpNFeNFeShowURL.Value;
  if DModG.QrPrmsEmpNFeNFeMaiusc.Value = 1  then
  begin
    QrADEST_ENDERECO.Value := AnsiUpperCase(QrADEST_ENDERECO.Value);
    QrADEST_XMUN_TXT.Value := AnsiUpperCase(QrAdest_xMun.Value);
  end else
  begin
    QrADEST_XMUN_TXT.Value := QrAdest_xMun.Value;
  end;
  // Fim 2011-08-25
  //
  QrADEST_CEP_TXT.Value := Geral.FormataCEP_TT(QrAdest_CEP.Value);
  QrADEST_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrAdest_fone.Value);
  //
  QrADEST_IE_TXT.Value := Geral.Formata_IE(QrAdest_IE.Value, QrAdest_UF.Value, '??');
  //
  //
    //
    ////// TRANSPORTADORA - REBOQUE???
    //
  //
  if Geral.SoNumero1a9_TT(QrAtransporta_CNPJ.Value) <> '' then
    QrATRANSPORTA_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAtransporta_CNPJ.Value)
  else
    QrATRANSPORTA_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAtransporta_CPF.Value);
  //
  {
  QrATRANSPORTA_ENDERECO.Value := QrAtransporta_xLgr.Value + ', ' + Geral.FormataNumeroDeRua(
    QrAtransporta_nro.Value, True) + ' ' + QrAtransporta_xCpl.Value;
  //
  QrATRANSPORTA_CEP_TXT.Value := Geral.FormataCEP_TT(QrAtransporta_CEP.Value);
  QrATRANSPORTA_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrAtransporta_fone.Value);
  //
  }
  QrATRANSPORTA_IE_TXT.Value := Geral.Formata_IE(QrAtransporta_IE.Value, QrAtransporta_UF.Value, '??');
  //
  if QrAide_tpAmb.Value = 2 then
  begin
    QrADOC_SEM_VLR_JUR.Value :=
      'NF-e emitida em ambiente de homologa��o. N�O POSSUI VALIDADE JUR�DICA';
    QrADOC_SEM_VLR_FIS.Value := 'SEM VALOR FISCAL';
  end else begin
    QrADOC_SEM_VLR_JUR.Value := '';
    QrADOC_SEM_VLR_FIS.Value := QrAinfProt_nProt.Value + ' ' + Geral.FDT(QrAinfProt_dhRecbto.Value, 00);
  end;
  QrAide_DSaiEnt_Txt.Value := dmkPF.FDT_NULO(QrAide_DSaiEnt.Value, 2);
  //
  //
{ p�g 12 da Nota T�cnica 2010/004 de junho/2010
0 � Emitente;
1 � Dest/Rem;
2 � Terceiros;
9 � Sem Frete;
}
  case QrAModFrete.Value of
    0: QrAMODFRETE_TXT.Value := '0 � Emitente';
    1: QrAMODFRETE_TXT.Value := '1 � Dest/Rem';
    2: QrAMODFRETE_TXT.Value := '2 � Terceiros';
    9: QrAMODFRETE_TXT.Value := '9 � Sem Frete';
    else QrAMODFRETE_TXT.Value := FormatFloat('0', QrAModFrete.Value) + ' - ? ? ? ? ';
  end;
end;

procedure TFmNFCeLEnU_0400.QrIAfterScroll(DataSet: TDataSet);
begin
  QrM.Close;
  QrM.Database := Dmod.MyDB;
  QrM.Params[00].AsInteger := FFatID;
  QrM.Params[01].AsInteger := FFatNum;
  QrM.Params[02].AsInteger := FEmpresa;
  QrM.Params[03].AsInteger := QrInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrM, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  QrN.Close;
  QrN.Database := Dmod.MyDB;
  QrN.Params[00].AsInteger := FFatID;
  QrN.Params[01].AsInteger := FFatNum;
  QrN.Params[02].AsInteger := FEmpresa;
  QrN.Params[03].AsInteger := QrInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrN, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  QrO.Close;
  QrO.Database := Dmod.MyDB;
  QrO.Params[00].AsInteger := FFatID;
  QrO.Params[01].AsInteger := FFatNum;
  QrO.Params[02].AsInteger := FEmpresa;
  QrO.Params[03].AsInteger := QrInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrO, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
  QrV.Close;
  QrV.Database := Dmod.MyDB;
  QrV.Params[00].AsInteger := FFatID;
  QrV.Params[01].AsInteger := FFatNum;
  QrV.Params[02].AsInteger := FEmpresa;
  QrV.Params[03].AsInteger := QrInItem.Value;
  UnDmkDAC_PF.AbreQuery(QrV, Dmod.MyDB); // 2022-02-20 - Antigo . O p e n ; 
  //
end;

procedure TFmNFCeLEnU_0400.QrNFeCabAAfterOpen(DataSet: TDataSet);
begin
  EdFatID.ValueVariant   := QrNFeCabAFatID.Value;
  EdFatNum.ValueVariant  := QrNFeCabAFatNum.Value;
  EdEmpresa.ValueVariant := QrNFeCabAEmpresa.Value;
  //
end;

procedure TFmNFCeLEnU_0400.QrNFeLEnCBeforeClose(DataSet: TDataSet);
begin
  QrNFeCabA.Close;
end;

procedure TFmNFCeLEnU_0400.QrNFeLEnCBeforeOpen(DataSet: TDataSet);
begin
  ReopenNFeCabA();
end;

procedure TFmNFCeLEnU_0400.ReopenNFeCabA();
begin
  if FIDCtrl <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrNFeCabA, Dmod.MyDB, [
    'SELECT Status, IDCtrl, FatID, FatNum, Empresa, ',
    'IF(Status=infProt_cStat, infProt_xMotivo,  ',
    'IF(Status=infCanc_cStat, infCanc_xMotivo, "")) Motivo  ',
    'FROM nfecaba  ',
    'WHERE IDCtrl=' + FormatFloat('0', FIDCtrl),
    '']);
  end;
end;

function TFmNFCeLEnU_0400.SohImprimeEMudaStatusParaPrecisaEnviar: Boolean;
(*const
  Modelo = CO_MODELO_NFE_65;
*)
var
  //CodUsu, Codigo, LoteEnv, indSinc,
  FatID, FatNum, Empresa, Status, infProt_cStat: Integer;
  infProt_xMotivo: String;
  //Sincronia: TXXeIndSinc;
begin
  //tpEmis = 9 >> Conting�ncia NFCe off-line!
  Result := False;
  FDANFEImpresso := False;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando XML da NF-e');
  if FmNFCeEmiss_0400.QrNFeCabAIDCtrl.Value > 0 then
  begin
    (* O apenas gera � feito automatico na NF-e 3.10
    if not FmNFCeEmiss_0400.GerarNFe(True, True, True) then
      if Interrompe(101) then Exit;
    *)
  end else
  begin
    //Fazer? if not FmNFCeEmiss_0400.GerarNFe(True, False, True) then
    if Interrompe(101) then Exit;
  end;
  CheckBox1.Checked := True;
  FIDCtrl := FmNFCeEmiss_0400.QrNFeCabAIDCtrl.Value;

  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  //
  Empresa := FmNFCeEmiss_0400.QrFatPedNFs_BEmpresa.Value;
  FatID := FmNFCeEmiss_0400.QrFatPedNFs_BCabA_FatID.Value;
  FatNum := FmNFCeEmiss_0400.QrFatPedNFs_BOriCodi.Value;

(* NFCe off-line: N�o ser� criado lote agora, s� quando enviar!
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando Lote de Envio');
  CodUsu := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'nfelenc', 'CodUsu', [], [],
    stIns, 0, siPositivo, nil);
  Codigo := DModG.BuscaProximoCodigoInt('nfectrl', 'nfelenc', '', 0, 999999999, '');
  indSinc := FmNFCeEmiss_0400.QrFatPedCabindSinc.Value;
  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfelenc', False, [
  'CodUsu', 'Empresa', 'indSinc'], ['Codigo'], [
  CodUsu, Empresa, indSinc], [Codigo], True) then
    if Interrompe(102) then Exit;
  CheckBox2.Checked := True;

  //

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Adicionando NF-e ao Lote criado');
  LoteEnv := Codigo;
  ReabreNFeLEnC(LoteEnv);
*)
  Status  := DmNFe_0000.stepNFCeOffLine();

  infProt_cStat   := Status; // Limpar variaveis (ver historico)
  infProt_xMotivo := '';
  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
  (*'LoteEnv',*) 'Status', 'infProt_cStat', 'infProt_xMotivo'],
  ['FatID', 'FatNum', 'Empresa'], [
  (*LoteEnv,*) Status, infProt_cStat, infProt_xMotivo],
  [FatID, FatNum, Empresa], True) then
    if Interrompe(103) then Exit;
  CheckBox3.Checked := True;
  //
  // Imprimir off-line!
  FIDCtrl := FmNFCeEmiss_0400.QrNFeCabAIDCtrl.Value;
  if FIDCtrl <> 0 then
  begin
    EdIDCtrl.ValueVariant := FIDCtrl;
    //DBCheck.CriaFm(TFmNFe_Pesq_0000, FmNFe_Pesq_0000, afmoNegarComAviso) then
    if EdIDCtrl.ValueVariant = FIDCtrl then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo dados da NF-e');
      ReopenNFeCabA();
      if (QrNFeCabA.State <> dsInactive) then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando unicidade da NF-e');
        if (QrNFeCabA.RecordCount = 1) then
        begin
          if (QrNFeCabAStatus.Value = 35) then
          //if BtImprime.Enabled then
          begin
            //DefineFrx(FmNFe_Pesq_0000.frxA4A_002, ficMostra);
            DefineFrx(frxNFCe_02_Off_Line, TfrxImpComo.ficImprime);
            CheckBox6.Checked := FDANFEImpresso;
            //
            // N�o Enviar email por ser off-line?
            //FmNFe_Pesq_0000.BtEnviaClick(Self);
            //
            //CheckBox7.Checked := FmNFe_Pesq_0000.FMailEnviado;
            //
            MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
          end else
          begin
            if Interrompe(112) then Exit;
          end;
        end else
        begin
          if Interrompe(111) then Exit;
        end;
      end else
      begin
        if Interrompe(110) then Exit;
      end;
    end else if
      Interrompe(109) then Exit;
    CheckBox5.Checked := True;
    //
    //
  end else
    if Interrompe(108) then Exit;
  //
  Result := True;
end;

procedure TFmNFCeLEnU_0400.ReabreNFeLEnC(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFeLEnC, Dmod.MyDB, [
  'SELECT *  ',
  'FROM nfelenc ',
  'WHERE Codigo=' + FormatFloat('0', Codigo),
  '']);
end;

end.
