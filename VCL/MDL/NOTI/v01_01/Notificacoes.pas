unit Notificacoes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, UnDmkProcFunc, dmkPermissoes,
  dmkCompoStore, mySQLDbTables, DmkDAC_PF, Vcl.Imaging.pngimage, Vcl.ImgList,
  UnDmkWeb, Vcl.Menus;

type
  TFmNotificacoes = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    BtRefresh: TBitBtn;
    BtMenu: TBitBtn;
    CSTabSheetChamou: TdmkCompoStore;
    dmkPermissoes1: TdmkPermissoes;
    QrNotifi: TmySQLQuery;
    LVItens: TListView;
    CkAutoLoad: TCheckBox;
    PMMenu: TPopupMenu;
    Abrirselecionado1: TMenuItem;
    Excluirselecionado1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure CkAutoLoadClick(Sender: TObject);
    procedure Abrirselecionado1Click(Sender: TObject);
    procedure Excluirselecionado1Click(Sender: TObject);
    procedure BtMenuClick(Sender: TObject);
    procedure PMMenuPopup(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenNotifi();
  public
    { Public declarations }
    FPanelNotifi: TPanel;
    FTimerNotifi: TTimer;
  end;

  var
  FmNotificacoes: TFmNotificacoes;

implementation

uses UnMyObjects, Module, ModuleGeral, MyGlyfs, Principal,
  {$IfNDef sCNTR} UnContratUnit, {$EndIf}
  {$IfNDef sNFSe} NFSe_0000_Module, {$EndIf}
  {$IfNDef SBLQ} UnBloquetos, {$EndIf}
  UnitNotificacoes, UnitNotificacoesEdit;

{$R *.DFM}

procedure TFmNotificacoes.Abrirselecionado1Click(Sender: TObject);
var
  Codigo: Integer;
  DataHora: TDateTime;
  Item: TListItem;
begin
  Item := LVItens.Selected;
  //
  if Item <> nil then
  begin
    Codigo   := Item.StateIndex;
    DataHora := DmodG.ObtemAgora;
    //
    case TNotifi(Codigo) of
      nftCntrAVencer:
      begin
        {$IfNDef sCNTR}
        ContratUnit.ContratosAVencer(DataHora, True);
        {$EndIf}
      end;
      nftNFSeMenVencid:
      begin
        {$IfNDef sNFSe}
        DmNFSe_0000.EmissoesMensaisVencidas(DataHora, True);
        {$EndIf}
      end;
      nftBlqVenAVen:
      begin
        {$IfNDef SBLQ}
        UBloquetos.ArrecadacoesVencidas_e_AVencer(DataHora, True);
        {$EndIf}
      end;
      else
      begin
        FmPrincipal.NotificacoesJanelas(TNotifi(Codigo), DataHora);
      end;
    end;
  end;
end;

procedure TFmNotificacoes.BtMenuClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMenu, BtMenu);
end;

procedure TFmNotificacoes.BtRefreshClick(Sender: TObject);
begin
  ReopenNotifi();
end;

procedure TFmNotificacoes.BtSaidaClick(Sender: TObject);
begin
  if (FPanelNotifi <> nil) and (FTimerNotifi <> nil) then
    UnNotificacoes.AtualizaNotifi(Dmod.QrNotifi, FPanelNotifi, FTimerNotifi, True);
  //
  if TFmNotificacoes(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmNotificacoes.CkAutoLoadClick(Sender: TObject);
var
  NAutoLoad: Integer;
begin
  NAutoLoad := Geral.BoolToInt(CkAutoLoad.Checked);
  //
  Geral.WriteAppKeyLM(CO_REG_Noti_NaoAutoCarregar, Application.Title, NAutoLoad, ktInteger);
end;

procedure TFmNotificacoes.Excluirselecionado1Click(Sender: TObject);
var
  Item: TListItem;
  Codigo: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Item := LVItens.Selected;
    //
    if Item <> nil then
    begin
      Codigo := Item.StateIndex;
      //
      if Codigo <> 0 then
        UnNotificacoesEdit.ExcluiNotificacao(Dmod.MyDB, Codigo);
      //
      LVItens.DeleteSelected;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmNotificacoes.FormActivate(Sender: TObject);
begin
  if TFmNotificacoes(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmNotificacoes.FormCreate(Sender: TObject);
var
  NAutoLoad: Integer;
begin
  ImgTipo.SQLType := stLok;
  //
  LVItens.PopupMenu := PMMenu;
  FPanelNotifi      := nil;
  FTimerNotifi      := nil;
  //
  UnNotificacoes.ConfiguraListView(LVItens);
  //
  NAutoLoad := Geral.ReadAppKeyLM(CO_REG_Noti_NaoAutoCarregar, Application.Title, ktInteger, 0);
  //
  CkAutoLoad.Checked := Geral.IntToBool(NAutoLoad);
end;

procedure TFmNotificacoes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNotificacoes.FormShow(Sender: TObject);
begin
  {$IfNDef cSkinRank} //Berlin
  {$IfNDef cAlphaSkin} //Berlin
    FmMyGlyfs.DefineGlyfs(TForm(Sender));
  {$EndIf}
  {$EndIf}
  {$IfDef cSkinRank} //Berlin
    if FmPrincipal.Sd1.Active then
      FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
  {$EndIf}
  {$IfDef cAlphaSkin} //Berlin
    if FmPrincipal.sSkinManager1.Active then
      FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
  {$EndIf}
  //
  LVItens.Groups.BeginUpdate;
  try
    UnNotificacoes.InsereGrupos(LVItens);
  finally
    LVItens.Groups.EndUpdate;
  end;
  LVItens.Items.BeginUpdate;
  //
  ReopenNotifi();
end;

procedure TFmNotificacoes.PMMenuPopup(Sender: TObject);
var
  Item: TListItem;
  Enab: Boolean;
begin
  Item := LVItens.Selected;
  //
  Enab := Item <> nil;
  //
  Abrirselecionado1.Enabled   := Enab;
  Excluirselecionado1.Enabled := Enab;
end;

procedure TFmNotificacoes.ReopenNotifi;
var
  Codigo, Grupo: Integer;
  Texto: String;
  DataHora: TDateTime;
  Reabre: Boolean;
begin
  try
    //LVItens.Groups.Clear;
    LVItens.Items.Clear;
    //
    Reabre := False;
    //
    UnNotificacoes.ReopenNotifi(QrNotifi);
    //
    Reabre := UnNotificacoes.ConfiguraNotificacaoPublica(QrNotifi);
    //
    if Reabre = True then
      UnDmkDAC_PF.AbreQuery(QrNotifi, Dmod.MyDB);
    //
    if QrNotifi.RecordCount > 0 then
    begin
      QrNotifi.First;
      //
      while not QrNotifi.EOF do
      begin
        Codigo   := QrNotifi.FieldByName('Codigo').AsInteger;
        Grupo    := QrNotifi.FieldByName('Grupo').AsInteger;
        Texto    := QrNotifi.FieldByName('Nome').AsString;
        DataHora := QrNotifi.FieldByName('DataHoraLocal').AsDateTime;
        //
        UnNotificacoes.InsereItem(LVItens, TGrupoNotifi(Grupo), Codigo, Texto, DataHora);
        //
        QrNotifi.Next;
      end;
    end;
  finally
    LVItens.Items.EndUpdate;
  end;
end;

end.
