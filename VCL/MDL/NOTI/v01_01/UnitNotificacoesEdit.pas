unit UnitNotificacoesEdit;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  ComCtrls, dmkGeral, Forms, Dialogs, UnMsgInt, UnInternalConsts, Db, DbCtrls,
  Variants, mySQLDbTables, UnDmkEnums, UnDmkProcFunc, DmkDAC_PF, UMySQLDB;

type
  TTipoNotifi  = (tpnLocal=0, tpnWeb=1);
  TGrupoNotifi = (gnOutros=0, gnEspecificos=1, gnContratos=2, gnNFSe=3, gnBoletos=4);
  TNotifi      = (ntfBugsOSsFilhas=-1, ntfBugsOSsGarantias=-2, nftCntrAVencer=-3,
                 nftNFSeMenVencid=-4, nftBlqVenAVen=-5, nftBackupAuto=-6,
                 nftSincronismoAuto=-7);
  TUnitNotificacoesEdit = class(TObject)
  private
    { Private declarations }
    function  ObtemGrupoDeTipoNotifi(TipoNotifi: TNotifi): TGrupoNotifi;
    function  ObtemTextoNotificacaoPublica(TipoNotifi: TNotifi; Texto: String = ''): String;
  public
    { Public declarations }
    function  ExcluiNotificacao(DataBase: TmySQLDatabase; Codigo: Integer): Boolean;
    function  CriaNotificacaoPublica(DataBase: TmySQLDatabase; TipoNotifi: TNotifi;
              DataHoraUTC: TDateTime; Texto: String = ''): Boolean;
  end;

var
  UnNotificacoesEdit: TUnitNotificacoesEdit;

implementation

uses UnMyObjects;

{ TUnitNotificacoesEdit }

function TUnitNotificacoesEdit.ExcluiNotificacao(DataBase: TmySQLDatabase;
  Codigo: Integer): Boolean;
begin
  if (Codigo < 0) and (Codigo <> -6) then //Notifica��o de backup pode excluir
  begin
    Result := True;
    Exit;
  end;
  //
  if USQLDB.ExcluiRegistroInt1('', 'notifi', 'Codigo', Codigo, DataBase) = ID_YES then
    Result := True
  else
    Result := False;
end;

function TUnitNotificacoesEdit.ObtemGrupoDeTipoNotifi(TipoNotifi: TNotifi): TGrupoNotifi;
begin
  case TipoNotifi of
    ntfBugsOSsFilhas, ntfBugsOSsGarantias:
      Result := gnEspecificos;
    nftCntrAVencer:
      Result := gnContratos;
    nftNFSeMenVencid:
      Result := gnNFSe;
    nftBlqVenAVen:
      Result := gnBoletos;
    nftBackupAuto:
      Result := gnEspecificos;
    nftSincronismoAuto:
      Result := gnEspecificos;
  end;
end;

function TUnitNotificacoesEdit.ObtemTextoNotificacaoPublica(
  TipoNotifi: TNotifi; Texto: String = ''): String;
begin
  case TipoNotifi of
    ntfBugsOSsFilhas:
      Result := 'Existem OSs de monitoramento de f�rmulas filhas para serem geradas';
    ntfBugsOSsGarantias:
      Result := 'Existem garantias vencidas e a vencer';
    nftCntrAVencer:
      Result := 'Existem contratos a renovar';
    nftNFSeMenVencid:
      Result := 'Existem emiss�es mensais de NFS-e vencidas';
    nftBlqVenAVen:
      Result := 'Existem itens de arrecada��es vencidas';
    nftBackupAuto:
    begin
      if Texto <> '' then
        Result := Texto
      else
        Result := 'Backup realizado com sucesso!';
    end;
    nftSincronismoAuto:
    begin
      if Texto <> '' then
        Result := Texto
      else
        Result := 'Sincronismo realizado com sucesso!';
    end;
  end;
end;

function TUnitNotificacoesEdit.CriaNotificacaoPublica(DataBase: TmySQLDatabase;
  TipoNotifi: TNotifi; DataHoraUTC: TDateTime; Texto: String = ''): Boolean;
var
  Codigo, Grupo, Tipo: Integer;
  DtaHoraUTC, Txt: String;
  Qry, QryUpd: TmySQLQuery;
  SQLType: TSQLType;
begin
  Result     := False;
  Codigo     := Integer(TipoNotifi);
  Grupo      := Integer(ObtemGrupoDeTipoNotifi(TipoNotifi));
  Tipo       := Integer(tpnLocal);
  DtaHoraUTC := Geral.FDT(DataHoraUTC, 105);
  Txt        := ObtemTextoNotificacaoPublica(TipoNotifi, Texto);
  Qry        := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  QryUpd     := TmySQLQuery.Create(TDataModule(DataBase.Owner));
  //
  if MyObjects.FIC(Txt = '', nil, 'Texto n�o definido!') then Exit;
  //
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DataBase, [
      'SELECT Codigo ',
      'FROM notifi ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
    if Qry.RecordCount > 0 then
      SQLType := stUpd
    else
      SQLType := stIns;
    //
    QryUpd.Database := DataBase;
    //
    Result := USQLDB.SQLInsUpd(QryUpd, SQLType, 'notifi', False,
                ['Nome', 'Grupo', 'Tipo', 'DataHoraUTC'], ['Codigo'],
                [Txt, Grupo, Tipo, DtaHoraUTC], [Codigo], True);
  finally
    Qry.Free;
    QryUpd.Free;
  end;
end;

end.
