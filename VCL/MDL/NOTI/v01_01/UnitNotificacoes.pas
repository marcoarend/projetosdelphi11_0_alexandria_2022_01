unit UnitNotificacoes;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  ComCtrls, dmkGeral, Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db,
  DbCtrls, Variants, mySQLDbTables, UnDmkEnums, UnDmkProcFunc, DmkDAC_PF,
  UMySQLModule, dmkPageControl, UnitNotificacoesEdit;

type
  TUnitNotificacoes = class(TObject)
  private
    { Private declarations }
    function  ObtemGrupoID(Grupo: TGrupoNotifi): Integer;
    function  ObtemGrupoNome(Grupo: TGrupoNotifi): String;
    //procedure InsereGrupo(ListView: TListView; Grupo: TGrupoNotifi);
  public
    { Public declarations }
    function  ConfiguraNotificacaoPublica(QueryNotifi: TmySQLQuery): Boolean;
    procedure InsereGrupos(ListView: TListView);
    procedure ConfiguraListView(ListView: TListView);
    procedure ConfiguraPageControl(PageControl: TPageControl; PanelNotifi: TPanel);
    procedure InsereItem(ListView: TListView; Grupo: TGrupoNotifi; Id: Integer;
              Texto: String; DataHora: TDateTime);
    procedure MostraFmNotificacoes(InOwner: TWincontrol;
              AdvToolBarPager: TdmkPageControl; PanelNotifi: TPanel = nil;
              TimerNotifi: TTimer = nil);
    procedure ConfiguracoesIniciaisNotifi(TimerNotifi: TTimer;
              QueryNotifi: TmySQLQuery; PanelNotifi: TPanel; ImageNotifi: TImage);
    procedure AtualizaNotifi(QueryNotifi: TmySQLQuery; PanelNotifi: TPanel;
              TimerNotifi: TTimer; Atualiza: Boolean = False);
    procedure ReopenNotifi(QueryNotifi: TmySQLQuery);
  end;

var
  UnNotificacoes: TUnitNotificacoes;
  //
  VAR_NOTI_UltimaVerifi: TDateTime;
  VAR_NOTI_Mostra: Boolean;
const
  CO_REG_Noti_NaoAutoCarregar = 'Noti_NaoAutoCarregar';
  CO_REG_Noti_Verifi = 'Noti_Verifi';
  CO_REG_Noti_VerifiAll = 'Noti_VerifiAll';

implementation

uses Module, ModuleGeral, UnMyObjects, MyGlyfs, Principal,
  {$IfNDef sCNTR} UnContratUnit, {$EndIf}
  {$IfNDef sNFSe} NFSe_0000_Module, {$EndIf}
  {$IfNDef SBLQ} UnBloquetos, {$EndIf}
  Notificacoes;

{ TUnitNotificacoes }

procedure TUnitNotificacoes.AtualizaNotifi(QueryNotifi: TmySQLQuery;
  PanelNotifi: TPanel; TimerNotifi: TTimer; Atualiza: Boolean = False);

  procedure ConfiguraBotao(TemNotifi: Boolean);
  begin
    if TemNotifi then
    begin
      VAR_NOTI_Mostra     := True;
      PanelNotifi.Visible := True
    end else
    begin
      VAR_NOTI_Mostra     := False;
      PanelNotifi.Visible := False;
    end;
  end;

  procedure AtualizaNotificacoes(Agora: TDateTime);
  var
    UltimaVerifiTot: TDateTime;
  begin
    ReopenNotifi(QueryNotifi);
    //
    if QueryNotifi.RecordCount > 0 then
    begin
      //Sem notificações
      ConfiguraBotao(True);
    end else
    begin
      //Notificações do aplicativo 1 vez por dia
      UltimaVerifiTot := Geral.ReadAppKeyLM(CO_REG_Noti_VerifiAll, Application.Title, ktDate, Agora - 24);

      if Geral.FDT(UltimaVerifiTot, 01) <> Geral.FDT(Agora, 01) then
      begin
        if ConfiguraNotificacaoPublica(QueryNotifi) then
          UnDmkDAC_PF.AbreQuery(QueryNotifi, Dmod.MyDB);
        //
        ConfiguraBotao(QueryNotifi.RecordCount > 0);
        //
      end else
        ConfiguraBotao(False);
    end;
    Geral.WriteAppKeyLM(CO_REG_Noti_Verifi, Application.Title, Agora, ktDateTime);
    Geral.WriteAppKeyLM(CO_REG_Noti_VerifiAll, Application.Title, Agora, ktDate);
  end;

const
 IntervaloAtualiz = 1; //Minutos
 TempoInativo = 5; //Segundos
var
  Agora, UltimaVerifi: TDateTime;
  Inativo: Word;
  NAutoLoad, Intervalo: Integer;
begin
  NAutoLoad := Geral.ReadAppKeyLM(CO_REG_Noti_NaoAutoCarregar, Application.Title, ktInteger, 0);
  //
  if NAutoLoad = 0 then
  begin
    if TimerNotifi <> nil then
      TimerNotifi.Enabled := False;
    //
    if Atualiza = False then
    begin
      Inativo := dmkPF.ObtemTempoInativoDoAplicativo;
      //
      if Inativo > TempoInativo then
      begin
        Agora        := Now;
        UltimaVerifi := Geral.ReadAppKeyLM(CO_REG_Noti_Verifi, Application.Title, ktDateTime, Agora);
        Intervalo    := Trunc(Agora - UltimaVerifi);
        //
        if IntervaloAtualiz <= Intervalo then
          AtualizaNotificacoes(Agora);
      end;
    end else
      AtualizaNotificacoes(Now);
    //
    if TimerNotifi <> nil then
      TimerNotifi.Enabled := True;
  end;
end;

procedure TUnitNotificacoes.ConfiguracoesIniciaisNotifi(TimerNotifi: TTimer;
  QueryNotifi: TmySQLQuery; PanelNotifi: TPanel; ImageNotifi: TImage);
begin
  TimerNotifi.Enabled     := False;
  ImageNotifi.Transparent := True;
  //
  VAR_NOTI_Mostra := False;
  //
  FmMyGlyfs.Lista_32x64A.GetBitmap(227, ImageNotifi.Picture.Bitmap);
  //
  AtualizaNotifi(QueryNotifi, PanelNotifi, nil, True);
  //
  TimerNotifi.InterVal := 60000;
  TimerNotifi.Enabled  := True;
end;

procedure TUnitNotificacoes.ConfiguraListView(ListView: TListView);
var
  Item: TListColumn;
begin
  ListView.Columns.Clear;
  ListView.Groups.Clear;
  ListView.Items.Clear;
  //
  Item := ListView.Columns.Add;
  Item.Caption  := 'Data / hora';
  Item.Width    := 150;
  //
  Item := ListView.Columns.Add;
  Item.Caption  := 'Descrição';
  Item.Width    := 600;
  //
  ListView.HideSelection     := True;
  ListView.ViewStyle         := vsReport;
  ListView.ShowColumnHeaders := False;
  ListView.GroupView         := True;
  ListView.RowSelect         := True;
  ListView.ReadOnly          := True;
  ListView.Font.Size         := 10;
  ListView.SortType          := stData;
end;

function TUnitNotificacoes.ConfiguraNotificacaoPublica(QueryNotifi: TmySQLQuery): Boolean;
var
  DataHora: TDateTime;
begin
  DataHora := DModG.ObtemAgora();
  Result   := False;
  //
  {$IfNDef sCNTR}
  if not QueryNotifi.Locate('Codigo', Integer(gnContratos), []) then
  begin
    if ContratUnit.ContratosAVencer(DataHora, False) <> 0 then
      UnNotificacoesEdit.CriaNotificacaoPublica(Dmod.MyDB, nftCntrAVencer, DmodG.ObtemAgora(True));
    Result := True;
  end;
  {$EndIf}
  {$IfNDef sNFSe}
  if not QueryNotifi.Locate('Codigo', Integer(nftNFSeMenVencid), []) then
  begin
    if DmNFSe_0000.EmissoesMensaisVencidas(DataHora, False) <> 0 then
      UnNotificacoesEdit.CriaNotificacaoPublica(Dmod.MyDB, nftNFSeMenVencid, DmodG.ObtemAgora(True));
    Result := True;
  end;
  {$EndIf}
  {$IfNDef sBLQ}
  if not QueryNotifi.Locate('Codigo', Integer(nftBlqVenAVen), []) then
  begin
    if UBloquetos.ArrecadacoesVencidas_e_AVencer(DataHora, False) <> 0 then
      UnNotificacoesEdit.CriaNotificacaoPublica(Dmod.MyDB, nftBlqVenAVen, DmodG.ObtemAgora(True));
    Result := True;
  end;
  {$EndIf}
  //Ver no Bugstrol como usar
  Result := FmPrincipal.NotificacoesVerifica(QueryNotifi, DataHora);
end;

procedure TUnitNotificacoes.ConfiguraPageControl(PageControl: TPageControl;
  PanelNotifi: TPanel);
begin
  if PageControl.ActivePageIndex = 0 then
    PanelNotifi.Visible := VAR_NOTI_Mostra
  else
    PanelNotifi.Visible := False;
end;

(*
procedure TUnitNotificacoes.InsereGrupo(ListView: TListView;
  Grupo: TGrupoNotifi);
var
  Item: TListGroup;
  Id: Integer;
  Texto: String;
begin
  Id    := ObtemGrupoID(Grupo);
  Texto := ObtemGrupoNome(Grupo);
  //
  Item := ListView.Groups.Add;
  Item.GroupID := Id;
  Item.Header  := Texto;
  Item.State   := [lgsNormal, lgsSelected, lgsCollapsible];
end;
*)

procedure TUnitNotificacoes.InsereGrupos(ListView: TListView);

  procedure CriaGrupo(Grupo: TGrupoNotifi);
  var
    Item: TListGroup;
    Id: Integer;
    Texto: String;
  begin
    Id    := ObtemGrupoID(Grupo);
    Texto := ObtemGrupoNome(Grupo);
    //
    Item := ListView.Groups.Add;
    Item.GroupID := Id;
    Item.Header  := Texto;
    Item.State   := [lgsNormal, lgsSelected, lgsCollapsible];
  end;

begin
  CriaGrupo(gnEspecificos);
  CriaGrupo(gnContratos);
  CriaGrupo(gnNFSe);
  CriaGrupo(gnBoletos);
end;

procedure TUnitNotificacoes.InsereItem(ListView: TListView; Grupo: TGrupoNotifi;
  Id: Integer; Texto: String; DataHora: TDateTime);
var
  Item: TListItem;
  I, GrupoId: Integer;
  Continua: Boolean;
begin
  Continua := True;
  GrupoId  := ObtemGrupoID(Grupo);
  //
  (*
  if ListView.Groups.FindItemID(GrupoId) = nil then
    InsereGrupo(ListView, Grupo);
  *)
  //
  if ListView.Items.Count > 0 then
  begin
    for I := 0 to ListView.Items.Count -1 do
    begin
      if Id = ListView.Items[I].StateIndex then
        Continua := False;
    end;
  end;
  if Continua = True then
  begin
    Item := ListView.Items.Add;
    Item.GroupID    := GrupoId;
    Item.Caption    := Geral.FDT(DataHora, 107);
    Item.StateIndex := Id;
    Item.SubItems.Add(Texto);
  end;
end;

procedure TUnitNotificacoes.MostraFmNotificacoes(InOwner: TWincontrol;
  AdvToolBarPager: TdmkPageControl; PanelNotifi: TPanel = nil;
  TimerNotifi: TTimer = nil);
var
  Form: TForm;
begin
  Form := MyObjects.FormTDICria(TFmNotificacoes, InOwner, AdvToolBarPager, False, True);
  //
  if Form <> nil then
  begin
    TFmNotificacoes(Form).FPanelNotifi := PanelNotifi;
    TFmNotificacoes(Form).FTimerNotifi := TimerNotifi;
  end;
end;

function TUnitNotificacoes.ObtemGrupoID(Grupo: TGrupoNotifi): Integer;
begin
  case Grupo of
    gnOutros:
      Result := 0;
    gnEspecificos:
      Result := 1;
    gnContratos:
      Result := 2;
    gnNFSe:
      Result := 3;
    gnBoletos:
      Result := 4;
  end;
end;

function TUnitNotificacoes.ObtemGrupoNome(Grupo: TGrupoNotifi): String;
begin
  case Grupo of
    gnOutros:
      Result := 'Outros';
    gnEspecificos:
      Result := Application.Title;
    gnContratos:
      Result := 'Contratos';
    gnNFSe:
      Result := 'NFS-e';
    gnBoletos:
      Result := 'Boletos';
  end;
end;

procedure TUnitNotificacoes.ReopenNotifi(QueryNotifi: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QueryNotifi, Dmod.MyDB, [
    'SELECT *, ',
    'CONVERT_TZ(DataHoraUTC, "+00:00", @@session.time_zone) AS DataHoraLocal',
    'FROM notifi ',
    'WHERE Tipo = 0 ',
    'ORDER BY DataHoraUTC ',
    '']);
end;

end.
