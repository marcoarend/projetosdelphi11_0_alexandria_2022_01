unit MDFeLayout_0100;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkEdit, Grids, ComObj, ComCtrls,
  Variants, dmkGeral, DB, mySQLDbTables, DBGrids, dmkDBGrid, DBCtrls, Mask,
  Menus, UnDmkEnums, UnXXe_PF, dmkImage;

type
  TFmMDFeLayout_0100 = class(TForm)
    PainelConfirma: TPanel;
    BtAbre: TBitBtn;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Grade1: TStringGrid;
    BtCarrega: TBitBtn;
    DBGrid1: TDBGrid;
    QrMDFeLayI: TmySQLQuery;
    DsMDFeLayI: TDataSource;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel3: TPanel;
    Label27: TLabel;
    SpeedButton8: TSpeedButton;
    EdArq: TdmkEdit;
    BitBtn3: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    TabSheet3: TTabSheet;
    Panel4: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBMemo2: TDBMemo;
    PMAltera: TPopupMenu;
    Somenteoitemselecionado1: TMenuItem;
    odositensapartirdoatual1: TMenuItem;
    DBMemo1: TDBMemo;
    BtOrdena: TBitBtn;
    QrMDFeLayIVersao: TFloatField;
    QrMDFeLayIGrupo: TIntegerField;
    QrMDFeLayICodigo: TIntegerField;
    QrMDFeLayICampo: TWideStringField;
    QrMDFeLayINivel: TIntegerField;
    QrMDFeLayIDescricao: TWideStringField;
    QrMDFeLayIElemento: TWideStringField;
    QrMDFeLayITipo: TWideStringField;
    QrMDFeLayIOcorMin: TSmallintField;
    QrMDFeLayIOcorMax: TIntegerField;
    QrMDFeLayITamMin: TSmallintField;
    QrMDFeLayITamMax: TIntegerField;
    QrMDFeLayITamVar: TWideStringField;
    QrMDFeLayIObservacao: TWideMemoField;
    QrMDFeLayIInfoVazio: TSmallintField;
    QrMDFeLayIDominio: TWideStringField;
    QrMDFeLayIExpReg: TWideStringField;
    QrMDFeLayIFormatStr: TWideStringField;
    QrMDFeLayIAlterWeb: TSmallintField;
    QrMDFeLayIAtivo: TSmallintField;
    QrMDFeLayIDeciCasas: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure BtAbreClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtCarregaClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure Somenteoitemselecionado1Click(Sender: TObject);
    procedure odositensapartirdoatual1Click(Sender: TObject);
    procedure BtOrdenaClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenMDFeLayI(Grupo, Codigo: Integer);
    procedure CarregaXLS();
    procedure Registro(SQLType: TSQLType);
  public
    { Public declarations }
    FContinua: Boolean;
  end;

  var
  FmMDFeLayout_0100: TFmMDFeLayout_0100;

implementation

uses DmkDAC_PF, UnMyObjects, Module, UMySQLModule, MyDBCheck, MDFeLayout_Edit;

const
  FTits = 11;
  FTitulos: array[0..FTits-1] of String = (
{00}                            '#',
{01}                            'Campo',
{02}                            'Nivel',
{03}                            'Descri��o',
{04}                            'Ele',
{05}                            'Tipo',
{06}                            'Ocorr',
{07}                            'Tama',
{08}                            'Dominio',
{09}                            'ExpReg',
{10}                            'Observa��o');

{$R *.DFM}

procedure TFmMDFeLayout_0100.BitBtn1Click(Sender: TObject);
begin
  MLAGeral.SalvaStringGridToFile(Grade1, 'C:\Dermatek\StringGrids\MDFeLayout.txt');
end;

procedure TFmMDFeLayout_0100.BitBtn2Click(Sender: TObject);
begin
  MLAGeral.LeArquivoToStringGrid(Grade1, 'C:\Dermatek\StringGrids\MDFeLayout.txt');
  BtCarrega.Enabled := True;
end;

procedure TFmMDFeLayout_0100.BitBtn3Click(Sender: TObject);
const
  DestFile = 'C:\Dermatek\Manual_de_Integracao_Contribuinte_MDFe.xls';
var
  Fonte: String;
begin
  Fonte := EdArq.Text;
  if FileExists(DestFile) then DeleteFile(DestFile);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Aguarde... Baixando arquivo!');
  Application.ProcessMessages;
  if MLAGeral.DownloadFile(Fonte, DestFile) then
  begin
    if MyObjects.Xls_To_StringGrid(Grade1, EdArq.Text, PB1, LaAviso1, LaAviso2) then
    begin
      BtCarrega.Enabled := True;
      CarregaXLS();
    end;
  end
  else
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Erro durante o download de "' +Fonte + '"');
end;

procedure TFmMDFeLayout_0100.BtAbreClick(Sender: TObject);
begin
  MyObjects.Xls_To_StringGrid(Grade1, EdArq.Text, PB1, LaAviso1, LaAviso2);
  BtCarrega.Enabled := True;
end;

procedure TFmMDFeLayout_0100.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmMDFeLayout_0100.BtCarregaClick(Sender: TObject);
begin
  CarregaXLS();
end;

procedure TFmMDFeLayout_0100.BtIncluiClick(Sender: TObject);
begin
  Registro(stIns);
end;

procedure TFmMDFeLayout_0100.BtOrdenaClick(Sender: TObject);
(*
var
  CodigoN: Double;
  CodigoX, Codigo, ID: String;
*)
begin
(*
  QrMDFeLayI.First;
  while not QrMDFeLayI.Eof do
  begin
    ID     := QrMDFeLayIID.Value;
    Codigo := QrMDFeLayICodigo.Value;
    MLAGeral.SeparaNumeroDaDireita_TT_Dot2(Codigo, CodigoN, CodigoX);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfelayi', False, [
    'CodigoN', 'CodigoX'], [
    'Codigo', 'ID'], [
    CodigoN, CodigoX], [
    Codigo, ID], False);
    //
    QrMDFeLayI.Next;
  end;
  ReopenMDFeLayI(0, 0);
*)
end;

procedure TFmMDFeLayout_0100.CarregaXLS();
const
  Versao = '2.00';
var
  LeftZeros, InfoVazio: Byte;
  R, P, N: Integer;
  Nome, Nivel, Campo, Descricao, Elemento, Tipo, Tamanho, Ocorrencia,
  Codigo, Observacao, TamVar, CodTxt, MinTxt, MaxTxt, Dominio, ExpReg,
  FormatStr, Grupo: String;
  OcorMin, OcorMax, TamMin, TamMax, DeciCasas, Ordem: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Ordem := 0;
    PB1.Position := 0;
    PB1.Max := Grade1.RowCount;
    //  Le dados da Grade1 e grava no BD
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'DELETE FROM mdfelayi ',
    'WHERE Versao = ' + Versao,
    '']);
    Dmod.QrUpd.ExecSQL;
    //
    for R := 1 to Grade1.RowCount -1 do
    begin
      PB1.Position := PB1.Position + 1;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Item ' + Codigo);
      Grupo  := Trim(Grade1.Cells[01,R]);
      CodTxt := Trim(Grade1.Cells[02,R]);
      if (Grupo <> '') and (CodTxt <> '') then
      begin
        if CodTxt[1] in (['0'..'9']) then
          Codigo := CodTxt
        else
          Codigo := '';
        if Codigo <> '' then
        begin
          //# 	Campo 	N�vel 	Descri��o 	Ele 	Tipo 	Ocorr. 	Tamanho 	Dom�nio 	Exp.Reg. 	Observa��es
          Campo      := Grade1.Cells[03,R];
          Nivel      := Grade1.Cells[04,R];
          Descricao  := Grade1.Cells[05,R];
          Elemento   := Grade1.Cells[06,R];
          Tipo       := Grade1.Cells[07,R];
          Ocorrencia := Grade1.Cells[08,R];
          Tamanho    := Grade1.Cells[09,R];
          Dominio    := Grade1.Cells[10,R];
          ExpReg     := Grade1.Cells[11,R];
          Observacao := Grade1.Cells[12,R];
          //
          P := pos('-', Ocorrencia);
          if P > 0 then
          begin
            MinTxt   := Trim(Copy(Ocorrencia, 1, p - 1));
            OcorMin  := Geral.IMV(MinTxt);
            MaxTxt   := Trim(Copy(Ocorrencia, p + 1));
            if Uppercase(MaxTxt) = 'N' then
              OcorMax  := High(Integer)
            else
              OcorMax  := Geral.IMV(MaxTxt);
          end else begin
            OcorMin    := Geral.IMV(Ocorrencia);
            OcorMax    := OcorMin;
          end;
          //
          XXe_PF.SoNumeroDeTamanhoDeCampoDeXXe(Tamanho, Tamanho);
          //if not k then
          XXe_PF.SeparaIntervalosDeTamanhosDeXXe(Tamanho, Tamanho, TamVar, FormatStr);
          P := pos(',', FormatStr);
          if P > 0 then
          begin
            MinTxt  := Trim(Copy(FormatStr, 1, p - 1));
            TamMin  := Geral.IMV(MinTxt);
            TamMax  := TamMin;
            //
            DeciCasas := Geral.IMV(Trim(Copy(FormatStr, P + 1)));
          end;
          //
          Tamanho := Trim(Tamanho);
          //T := Length(Tamanho);
          P := pos('-', Tamanho);
          if P > 0 then
          begin
            MinTxt  := Trim(Copy(Tamanho, 1, p - 1));
            TamMin  := Geral.IMV(MinTxt);
            //
            MaxTxt  := Trim(Copy(Tamanho, P + 1));
            if Uppercase(MaxTxt) = 'N' then
              TamMax  := High(Integer)
            else
              TamMax  := Geral.IMV(MaxTxt);
            //
            //TamVar  := '';
          end else begin
            TamMin  := Geral.IMV(Tamanho);
            TamMax  := TamMin;
            //TamVar  := '';
          end;
          if pos('CNPJ', Campo) > 0 then LeftZeros := 1 else
          if pos('CPF',  Campo) > 0 then LeftZeros := 1 else
          if pos('CEP',  Campo) > 0 then LeftZeros := 1 else
          if pos('CST',  Campo) > 0 then LeftZeros := 1 else
          if pos('NCM',  Campo) > 0 then LeftZeros := 1 else
          if pos('EAN',  Campo) > 0 then LeftZeros := 1 else
          if Campo = 'cNF'          then LeftZeros := 1 else
                                         LeftZeros := 0;
          if pos('EAN',  Campo) > 0 then InfoVazio := 1 else
                                         InfoVazio := 0;
          Ordem := Ordem + 1;
          //if
          UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'mdfelayi', False, [
          'Campo', 'Nivel', 'Descricao',
          'Elemento', 'Tipo', 'OcorMin',
          'OcorMax', 'TamMin', 'TamMax',
          'TamVar', 'Observacao', 'InfoVazio',
          'Dominio', 'ExpReg', 'FormatStr',
          'DeciCasas'], [
          'Versao', 'Grupo', 'Codigo'], [
          Campo, Nivel, Descricao,
          Elemento, Tipo, OcorMin,
          OcorMax, TamMin, TamMax,
          TamVar, Observacao, InfoVazio,
          Dominio, ExpReg, FormatStr,
          DeciCasas], [
          Versao, Grupo, Codigo], False);
        end;
      end
    end;
    PB1.Position := PB1.Max;
    ReopenMDFeLayI(0, 0);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmMDFeLayout_0100.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMDFeLayout_0100.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMDFeLayout_0100.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  FContinua := False;
  Grade1.ColCount := FTits;
  for I := 0 to FTits - 1 do
    Grade1.Cells[I, 0] := FTitulos[I];
  ReopenMDFeLayI(0, 0);
end;

procedure TFmMDFeLayout_0100.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMDFeLayout_0100.odositensapartirdoatual1Click(Sender: TObject);
begin
  FContinua := True;
  while not QrMDFeLayI.Eof do
  begin
    if FContinua then
    begin
      Registro(stUpd);
      QrMDFeLayI.Next;
    end else Exit;
  end;
end;

procedure TFmMDFeLayout_0100.Registro(SQLType: TSQLType);
var
  Codigo, Grupo: Integer;
begin
  if DBCheck.CriaFm(TFmMDFeLayout_Edit, FmMDFeLayout_Edit, afmoSoAdmin) then
  begin
    with FmMDFeLayout_Edit do
    begin
      if SQLType = stUpd then
      begin
        EdDeciCasas.ValueVariant  := QrMDFeLayIDeciCasas .Value;
        EdOcorMax.ValueVariant    := QrMDFeLayIOcorMax   .Value;
        EdTamMin.ValueVariant     := QrMDFeLayITamMin    .Value;
        EdTamMax.ValueVariant     := QrMDFeLayITamMax    .Value;
        EdTamVar.ValueVariant     := QrMDFeLayITamVar    .Value;
        CkInfoVazio.Checked       := MLAGeral.ITB(QrMDFeLayIInfoVazio .Value);
        EdGrupo.ValueVariant      := QrMDFeLayIGrupo     .Value;
        EdCampo.ValueVariant      := QrMDFeLayICampo     .Value;
        EdDescricao.ValueVariant  := QrMDFeLayIDescricao .Value;
        EdElemento.ValueVariant   := QrMDFeLayIElemento  .Value;
        EdTipo.ValueVariant       := QrMDFeLayITipo      .Value;
        EdOcorMin.ValueVariant    := QrMDFeLayIOcorMin   .Value;
        MeObservacao.Text         := QrMDFeLayIObservacao.Value;
        EdCodigo.ValueVariant     := QrMDFeLayICodigo    .Value;
        EdFormatStr.ValueVariant  := QrMDFeLayIFormatStr .Value;
        EdNivel.ValueVariant      := QrMDFeLayINivel     .Value;
        EdDominio.ValueVariant    := QrMDFeLayIDominio   .Value;
        EdExpReg.ValueVariant     := QrMDFeLayIExpReg    .Value;
        //
        FCodigo                   := QrMDFeLayICodigo    .Value;
        FGrupo                    := QrMDFeLayIGrupo     .Value;
        FVersao                   := QrMDFeLayIVersao    .Value;
      end else begin
        FCodigo                   := 0;
        FGrupo                    := 0;
        FVersao                   := 0.00;
      end;
      ImgTipo.SQLType := SQLType;
      ShowModal;
      FContinua := FmMDFeLayout_Edit.FContinua;
      Codigo := EdCodigo.ValueVariant;
      Grupo  := EdGrupo.ValueVariant;
      Destroy;
    end;
    ReopenMDFeLayI(Grupo, Codigo);
  end;
end;

procedure TFmMDFeLayout_0100.ReopenMDFeLayI(Grupo, Codigo: Integer);
begin
  UndmkDAC_PF.AbreMySQLQuery0(QrMDFeLayI, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfelayi ',
  'ORDER BY Versao, Grupo, Codigo ',
  '']);
  //
  QrMDFeLayI.Locate('Grupo;Codigo', VarArrayOf([Grupo, Codigo]), []);
end;

procedure TFmMDFeLayout_0100.Somenteoitemselecionado1Click(Sender: TObject);
begin
  Registro(stUpd);
end;

procedure TFmMDFeLayout_0100.SpeedButton8Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(EdArq.Text);
  Arquivo := ExtractFileName(EdArq.Text);
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo', '', [], Arquivo) then
    EdArq.Text := Arquivo;
end;

end.
