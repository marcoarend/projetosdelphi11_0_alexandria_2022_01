unit MDFeGeraXML_0100a;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, Variants, Math,
  IBCustomDataSet, IBQuery, Clipbrd, DB, DBClient, dmkGeral, UrlMon,
  InvokeRegistry, SOAPHTTPClient, CAPICOM_TLB, SOAPHTTPTrans, JwaWinCrypt,
  WinInet, XMLDoc, xmldom, XMLIntf, msxmldom, MSXML2_TLB, dmkImage, dmkEdit,
  // Consulta status servi�o
  consStatServMDFe_v100,
  // Modal Rodoviario
  mdfeModalRodoviario_v100,
  // Consulta situa��o MDF-e
  consSitMDFe_v100,
  // Pede inutiliza��o MDF-e
  //inutMDFe_v200,
  // Gera MDFe
  mdfe_v100,
  // Envia lote de MDF-es
  // ???
  // Consulta Lote de MDF-es
  consReciMDFe_v100,
  // Cancela MDF-e
  //cancN F e_v200,
  // Consulta cadastro contribuinte
  //consCad_v200,
  //Consulta MDFes N�o Encerradas
  consMDFeNaoEnc_v100,
  // Download N F es Confirmadas (destinadas)
  //downloadN F e_v100,
  //
  // Distribui��o de DFe de Interesse
  //distDFeInt_v100,
  //
  XSBuiltIns, StrUtils, ComObj, SoapConst, mySQLDbTables,
  UnDmkEnums, UnMDFeListas;

const
  NO_SEM_VALOR = 'MDF-E EMITIDO EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL';
  FVersaoModal_Rodoviario = 2.00;
{
  //FVersaoWS           = '3.10'; // 0310
  N F e_AllModelos      = '55'; // SQL 55,?,?
  N F e_CodAutorizaTxt  = '100';
  N F e_CodCanceladTxt  = '101';
  N F e_CodInutilizTxt  = '102';
  N F e_CodDenegadoTxt  = '110,301,302';
  // XML
}
  sXML_Version        = '1.0';
  sXML_Encoding       = 'UTF-8';
  ENCODING_UTF8 = '?xml version="1.0" encoding="UTF-8"?';
  ENCODING_UTF8_STD = '?xml version="1.0" encoding="UTF-8" standalone="no"?';
{
  NAME_SPACE = 'xmlns="http://www.portalfiscal.inf.br/mdfe"';
  NAME_SPACE_MDFE = 'xmlns="http://www.portalfiscal.inf.br/mdfe"';
  sCampoNulo = '#NULL#';
}

type
{
  TEnumeracao = (enumProcessoEmissao, enumtpEmis, enumFinN F e, enumTpNF,
    enumIndPag, enumOrigemMercadoria, enumCSTICMS);
  // Copiado de ACBr
  TStatusACBrN F e = ( stIdle, stN F eStatusServico, stN F eRecepcao, stN F eRetRecepcao, stN F eConsulta, stN F eCancelamento, stN F eInutilizacao, stN F eRecibo, stN F eCadastro, stN F eEmail, stN F eEnvDPEC, stN F eConsultaDPEC, stN F eCCe );
  TStatusACBrMDFe = ( stMDFeIdle, stMDFeStatusServico, stMDFeRecepcao, stMDFeRetRecepcao, stMDFeConsulta, stMDFeCancelamento, stMDFeInutilizacao, stMDFeRecibo, stMDFeCadastro, stMDFeEmail, stMDFeCCe );
  (* IMPORTANTE - Sempre que alterar um Tipo efetuar a atualiza��o das fun��es de convers�o correspondentes *)
  TLayOut = (LayN f eRecepcao,LayN f eRetRecepcao,LayN f eCancelamento,LayN f eInutilizacao,LayN f eConsulta,LayN f eStatusServico,LayN f eCadastro, LayN f eEnvDPEC, LayN f eConsultaDPEC,
             LayMDFeRecepcao,LayMDFeRetRecepcao,LayMDFeCancelamento,LayMDFeInutilizacao,LayMDFeConsultaCT,LayMDFeStatusServico,LayMDFeCadastro,LayN F eCCe);

  TpcnSchema = (TsPL005c, TsPL006);
  // Incluido o tlConsStatServMDFe para MDFe por possuir uma vers�o diferente da N F e
  TpcnTipoLayout = (tlAtuCadEmiDFe, tlCadEmiDFe, tlCancN F e, tlConsCad, tlConsReciN F e, tlConsSitN F e, tlConsStatServ,
    tlInutN F e, tlN F e, tlProcN F e, tlProcInutN F e, tlRetAtuCadEmiDFe, tlRetCancN F e, tlRetConsCad, tlRetConsReciN F e,
    tlRetConsStatServ, tlRetConsSitN F e, tlRetEnvN F e, tlRetInutN F e, tlEnvN F e, tlProcCancN F e,
                    tlCancMDFe, tlConsReciMDFe, tlConsSitMDFe,
    tlInutMDFe, tlMDFe, tlProcMDFe, tlProcInutMDFe, tlRetCancMDFe, tlRetConsReciMDFe,
    tlRetConsSitMDFe, tlRetEnvMDFe, tlRetInutMDFe, tlEnvMDFe, tlProcCancMDFe, tlEnvDPEC, tlConsDPEC, tlConsStatServMDFe, tlCCeN F e, tlEnvCCeN F e, tlRetEnvCCeN F e);

  // Tipo tcDe6 incluido por Italo em 30/09/2010 (usado no MDFe campo 435: vTar = valor da tarifa do modal Dutovi�rio)
  TpcnTipoCampo = (tcStr, tcInt, tcDat, tcDatHor, tcEsp, tcDe2, tcDe3, tcDe4, tcDe10, tcHor, tcDe6 ); // tcEsp = String: somente numeros;
  TpcnFormatoGravacao = (fgXML, fgTXT);
  TpcnTagAssinatura = (taSempre, taNunca, taSomenteSeAssinada, taSomenteParaNaoAssinada);

  TpcnIndicadorPagamento = (ipVista, ipPrazo, ipOutras);
  TpcnTipoN F e = (tnEntrada, tnSaida);
  TpcnTipoImpressao = (tiRetrato, tiPaisagem);
  TpcnTipoEmissao = (teNormal, teContingencia, teSCAN, teDPEC, teFSDA);
  TpcnTipoAmbiente = (taProducao, taHomologacao);
  TpcnSituacaoEmissor = (seHomologacao, seProducao);
  TpcnFinalidadeN F e = (fnNormal, fnComplementar, fnAjuste);
  TpcnProcessoEmissao = (peAplicativoContribuinte, peAvulsaFisco, peAvulsaContribuinte, peContribuinteAplicativoFisco);
  TpcnTipoOperacao = (toVendaConcessionaria, toFaturamentoDireto, toVendaDireta, toOutros);
  TpcnCondicaoVeiculo = (cvAcabado, cvInacabado, cvSemiAcabado);
  TpcnTipoArma = (taUsoPermitido, taUsoRestrito);
  TpcnOrigemMercadoria = (oeNacional, oeEstrangeiraImportacaoDireta, oeEstrangeiraAdquiridaBrasil);
  TpcnCSTIcms = (cst00, cst10, cst20, cst30, cst40, cst41, cst45, cst50, cst51, cst60, cst70, cst80, cst81, cst90, cstPart10, cstPart90, cstRep41, cstVazio); //80 e 81 apenas para MDFe
  TpcnCSOSNIcms = (csosnVazio,csosn101, csosn102, csosn103, csosn201, csosn202, csosn203, csosn300, csosn400, csosn500,csosn900 );
  TpcnDeterminacaoBaseIcms = (dbiMargemValorAgregado, dbiPauta, dbiPrecoTabelado, dbiValorOperacao);
  TpcnDeterminacaoBaseIcmsST = (dbisPrecoTabelado, dbisListaNegativa, dbisListaPositiva, dbisListaNeutra, dbisMargemValorAgregado, dbisPauta);
  TpcnMotivoDesoneracaoICMS = (mdiTaxi, mdiDeficienteFisico, mdiProdutorAgropecuario, mdiFrotistaLocadora, mdiDiplomaticoConsular, mdiAmazoniaLivreComercio, mdiSuframa, mdiOutros );
  TpcnCstIpi = (ipi00, ipi49, ipi50, ipi99, ipi01, ipi02, ipi03, ipi04, ipi05, ipi51, ipi52, ipi53, ipi54, ipi55);
  TpcnCstPis = (pis01, pis02, pis03, pis04, pis06, pis07, pis08, pis09, pis49, pis50, pis51, pis52, pis53, pis54, pis55, pis56, pis60, pis61, pis62, pis63, pis64, pis65, pis66, pis67, pis70, pis71, pis72, pis73, pis74, pis75, pis98, pis99);
  TpcnCstCofins = (cof01, cof02, cof03, cof04, cof06, cof07, cof08, cof09, cof49, cof50, cof51, cof52, cof53, cof54, cof55, cof56, cof60, cof61, cof62, cof63, cof64, cof65, cof66, cof67, cof70, cof71, cof72, cof73, cof74, cof75, cof98, cof99);
  TpcnModalidadeFrete = (mfContaEmitente, mfContaDestinatario, mfContaTerceiros, mfSemFrete);
  TpcnIndicadorProcesso = (ipSEFAZ, ipJusticaFederal, ipJusticaEstadual, ipSecexRFB, ipOutros);
  TpcnCRT = (crtSimplesNacional, crtSimplesExcessoReceita, crtRegimeNormal);
  TpcnIndicadorTotal = (itSomaTotalN F e, itNaoSomaTotalN F e );

  TpmdfeFormaPagamento = (fpPago, fpAPagar, fpOutros);
  TpmdfeTipoMDFe = (tcNormal, tcComplemento, tcAnulacao, tcSubstituto);
  TpmdfeModal = (mdRodoviario, mdAereo, mdAquaviario, mdFerroviario, mdDutoviario);
  TpmdfeTipoServico = (tsNormal, tsSubcontratacao, tsRedespacho, tsIntermediario);
  TpmdfeRetira = (rtSim, rtNao);
  TpmdfeTomador = ( tmRemetente, tmExpedidor, tmRecebedor, tmDestinatario, tmOutros);
  TpmdfeRspSeg = (rsRemetente, rsExpedidor, rsRecebedor, rsDestinatario, rsEmitenteMDFe, rsTomadorServico);
  TpmdfeLotacao = (ltNao, ltSim);
  TpmdfeProp = (tpTACAgregado, tpTACIndependente, tpOutros);
  TpmdfeMask = (msk4x2, msk7x2, msk9x2, msk10x2, msk13x2, msk15x2, msk6x3, mskAliq);
  UnidMed = (uM3,uKG, uTON, uUNIDADE, uLITROS);
  TpcnECFModRef = (ECFModRefVazio, ECFModRef2B,ECFModRef2C,ECFModRef2D);
  TpcnISSQNcSitTrib  = ( ISSQNcSitTribVazio , ISSQNcSitTribNORMAL, ISSQNcSitTribRETIDA, ISSQNcSitTribSUBSTITUTA,ISSQNcSitTribISENTA);

  // Incluido por Italo em 31/09/2010
  TpmdfeDirecao = (drNorte, drLeste, drSul, drOeste);
  TpmdfeTipoNavegacao = (tnInterior, tnCabotagem);
  // Incluido por Italo em 19/11/2010
  TpmdfeTipoTrafego = (ttProprio, ttMutuo, ttRodoferroviario, ttRodoviario);
  // Incluido por Italo em 24/01/2011
  TpmdfeTipoDataPeriodo = (tdSemData, tdNaData, tdAteData, tdApartirData, tdNoPeriodo);
  TpmdfeTipoHorarioIntervalo = (thSemHorario, thNoHorario, thAteHorario, thApartirHorario, thNoIntervalo);
  TpmdfeTipoDocumento = (tdDeclaracao, tdOutros);
  TpmdfeTipoDocumentoAnterior = (daCTRC, daCTAC, daACT, daNF7, daNF27, daCAN, daCTMC, daATRE, daDTA, daCAI, daCCPI, daCA, daTIF, daOutros);
  TpmdfeRspPagPedagio = (rpEmitente, rpRemetente, rpExpedidor, rpRecebedor, rpDestinatario, rpTomadorServico);
  TpmdfeTipoDispositivo = (tdCartaoMagnetico, tdTAG, tdTicket);
  TpmdfeTipoPropriedade = (tpProprio, tpTerceiro);
  TpmdfeTipoVeiculo = (tvTracao, tvReboque);
  TpmdfeTipoRodado = (trNaoAplicavel, trTruck, trToco, trCavaloMecanico, trVAN, trUtilitario, trOutros);
  TpmdfeTipoCarroceria = (tcNaoAplicavel, tcAberta, tcFechada, tcGraneleira, tcPortaContainer, tcSider);
  // Incluido por Italo em 28/04/2011
  TPosRecibo = (prCabecalho, prRodape);
  // Fim ACBr
}

  TFmMDFeGeraXML_0100a = class(TForm)
    Panel1: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
  private
    { Private declarations }
    //
    procedure ConfiguraReqResp(ReqResp: THTTPReqResp);
    function  CriarDocumentoMDFe(FatID, FatNum, Empresa: Integer;
              var XMLGerado_Arq, XMLGerado_Dir: String; LaAviso1, LaAviso2: TLabel;
              GravaCampos: Integer): Boolean;
    function  MontaID_Inutilizacao(const cUF, Ano, emitCNPJ, Modelo, Serie, nMDFIni,
              nMDFFim: String; var Id: String): Boolean;
    procedure OnBeforePost(const HTTPReqResp: THTTPReqResp;
              Data: Pointer);
    function  SeparaDados(Texto: String; Chave: String; MantemChave:
              Boolean = False): String;
    function  TipoXML(NoStandAlone: Boolean): String;
    // Montagem do MDFe
    function  GerarXMLdoMDFe(FatID, FatNum, Empresa: Integer; const mdfeID: String;
              GravaCampos: Integer): Boolean;
    function  GerarXMLmodalRodo(FatID, FatNum, Empresa: Integer;
              GravaCampos: Integer): Boolean;
    function  Def(const Grupo, Codigo: Integer; const Source: Variant;
              var Dest: String): Boolean;
    function  Def_UTC(const Grupo, Codigo: Integer; const Source1, Source2,
              Source3: Variant; var Dest: String): Boolean;
  public
    { Public declarations }
    function  GerarArquivoXMLdoMDFe(FatID, FatNum, Empresa: Integer;
              LaAviso1, LaAviso2: TLabel; GravaCampos: Integer): Boolean;
    function  GerarLoteMDFeNovo(Lote, Empresa: Integer; out PathLote: String;
              out XML_Lote: String; LaAviso1, LaAviso2: TLabel; Sincronia:
              TXXeIndSinc): Boolean;
    function  NomeAcao(Acao: TTipoConsumoWS_MDFe): String;
    function  ObtemWebServer2(UFServico: String; Ambiente, CodigoUF: Integer;
              Acao: TTipoConsumoWS_MDFe; sAviso: String): Boolean;
    function  VersaoWS(Acao: TTipoConsumoWS_MDFe; Formata: Boolean = True): String;
    function  WS_MDFeInutilizacaoMDFe(UFServico: String; Ambiente, CodigoUF, Ano: Byte;
              Id, CNPJ, Mod_, Serie, NMDFIni, NMDFFin: String;
              XJust, NumeroSerial: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio:
              TMemo; EdWS: TEdit; var ID_Res: String): String;
    function  WS_EventoCancelamentoMDFe(UFServico: String; Ambiente,
              CodigoUF: Byte; (*NumeroSerial: String; Lote: Integer; LaAviso1,
              LaAviso2: TLabel*)
              XML_Evento: String; TipoConsumo: TTipoConsumoWS_MDFe;  RETxtEnvio:
              TMemo; EdWS: TEdit): String;
    function  WS_EventoEncerramentoMDFe(UFServico: String; Ambiente, CodigoUF:
              Byte; XML_Evento: String; TipoConsumo: TTipoConsumoWS_MDFe;
              RETxtEnvio: TMemo; EdWS: TEdit): String;
    function  WS_EventoIncCondutor(UFServico: String; Ambiente, CodigoUF:
              Byte; XML_Evento: String; TipoConsumo: TTipoConsumoWS_MDFe;
              RETxtEnvio: TMemo; EdWS: TEdit): String;
    function  WS_MDFeConsultaMDF(UFServico: String; Ambiente, CodigoUF: Byte;
              ChaveMDFe: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio:
              TMemo; EdWS: TEdit): String;
    function  WS_MDFeRecepcaoLote(UFServico: String; Ambiente, CodigoUF: Byte;
              NumeroSerial: String; Lote: Integer; LaAviso1, LaAviso2: TLabel;
              RETxtEnvio: TMemo; EdWS: TEdit; Sincronia: TXXeIndSinc): String;
    function  WS_MDFeRetRecepcao(UFServico: String; Ambiente, CodigoUF: Byte;
              Recibo: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio: TMemo;
              EdWS: TEdit): String;
    function  WS_MDFeStatusServico(UFServico: String; Ambiente,
              CodigoUF: Byte; Certificado: String; LaAviso1, LaAviso2: TLabel;
              RETxtEnvio: TMemo; EdWS: TEdit): String;
    function  WS_MDFeConsultaNaoEncerrados(UFServico: String; Ambiente,
              CodigoUF: Byte; CNPJ, Certificado: String; LaAviso1, LaAviso2: TLabel;
              RETxtEnvio: TMemo; EdWS: TEdit): String;
    function  XML_MDFeInutMDFe(var Id: String; TpAmb, CUF: Byte; Ano: Integer;
              CNPJ, Mod_, Serie, NMDFIni, NMDFFin, XJust: String): String;
    function  XML_ConsNaoEnc(TpAmb: Integer; CNPJ: String): String;
    function  XML_ConsReciMDFe(TpAmb: Integer; NRec: String): String;
    function  XML_ConsSitMDFe(TpAmb: Integer; ChMDFe, VersaoAcao: String): String;
    function  XML_ConsStatServ(TpAmb, CUF: Integer): String;

  end;
  var
  FmMDFeGeraXML_0100a: TFmMDFeGeraXML_0100a;

implementation

uses UnMyObjects, Module, UnInternalConsts, UMySQLModule, ModuleGeral,
  ModuleMDFe_0000, UnXXe_PF, MDFeSteps_0100a,
  //N F e XMLGerencia,
  UnMDFe_PF,
  //Recibo,
  DmkDAC_PF, UnDmkProcFunc, Recibo;

var
  FGravaCampo: Integer;
  FLaAviso1, FLaAviso2: TLabel;
  FdocXML: TXMLDocument;
{
  FCabecTxt,}
  FAssinTxt: String;
  //FWSDL,
  FDadosTxt, FURL: String;
{
  //  ACBr
  CertStore     : IStore3;
  CertStoreMem  : IStore3;
  PrivateKey    : IPrivateKey;
  Certs         : ICertificates2;}
  Cert          : ICertificate2;
{
  NumCertCarregado : String;
  //
  NomeArquivo: String;
  CaminhoArquivo: String;
  strTpAmb: string;
}
  SerieMDF: String;
  NumeroMDF: String;
  CDV: String;
  modalXML: String;
  cXML: IXMLTMDFe;
  cInfMunCarrega: IXMLInfMunCarrega;
  cInfPercurso: IXMLInfPercurso;
  cInfMunDescarga: IXMLInfMunDescarga;
  cInfCTe: IXMLInfCTe;
  cInfNFe: IXMLInfNFe;
  cInfMDFeTransp: IXMLInfMDFeTransp;
  cLacres: IXMLLacres;
  cAutXML: IXMLAutXML;
  //
  cRodo: IXMLRodo;
  cRodo_veicTracao_condutor: IXMLRodo_veicTracao_condutor;
  cRodo_veicReboque: IXMLRodo_veicReboque;
  cRodo_valePed_disp: IXMLRodo_valePed_disp;
{
  cRodo_occ: IXMLRodo_occ;
  cRodo_valePed: IXMLRodo_valePed;
  cRodo_veic: IXMLRodo_veic;
  cRodo_veic_prop: IXMLRodo_veic_prop;
  cRodo_lacRodo: IXMLRodo_lacRodo;
  cRodo_moto: IXMLRodo_moto;
}
  arqXML: TXMLDocument;
  (* Objetos do Documento XML... *)
{
  cObsCont: IXMLObsCont;
  cObsFisco: IXMLObsFisco;
  cComp: IXMLComp;
  cInfQ: IXMLInfQ;
  cInfNF: IXMLInfNF;
  cInfNFe: IXMLInfNFe;
  cInfOutros: IXMLInfOutros;
  cPeri: IXMLPeri;
  cSeg: IXMLSeg;
  cDup: IXMLDup;
  cAutXml: IXMLAutXml;
}
  //
  rodoXML: TXMLDocument;
  //
  FFatID,
  FFatNum,
  FEmpresa,
  FOrdem: Integer;



{$R *.DFM}

{ TFmMDFeGeraXML_0100a }

procedure TFmMDFeGeraXML_0100a.ConfiguraReqResp(ReqResp: THTTPReqResp);
begin
  ReqResp.OnBeforePost := OnBeforePost;
end;

function TFmMDFeGeraXML_0100a.CriarDocumentoMDFe(FatID, FatNum, Empresa: Integer;
  var XMLGerado_Arq, XMLGerado_Dir: String; LaAviso1, LaAviso2: TLabel;
  GravaCampos: Integer): Boolean;
var
  strChaveAcesso: String;
  TextoXML: String;
begin
  Result := False;
  FLaAviso1 := LaAviso1;
  FLaAviso2 := LaAviso2;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM mdfexmli WHERE FatId=:P0 AND FatNum=:P1 AND Empresa=:P2');
  Dmod.QrUpd.Params[00].AsInteger := FatID;
  Dmod.QrUpd.Params[01].AsInteger := FatNum;
  Dmod.QrUpd.Params[02].AsInteger := Empresa;
  Dmod.QrUpd.ExecSQL;
  //
  FFatID   := FatID;
  FFatNum  := FatNum;
  FEmpresa := Empresa;
  FOrdem   := 0;
  //
  DmMDFe_0000.ReopenMDFeCabA(FatID, FatNum, Empresa);
  //
(* Criando o Documento XML e Gravando cabe�alho... *)
  arqXML := TXMLDocument.Create(nil);
  arqXML.Active := False;
  arqXML.FileName := '';
  cXML := GetMDFe(arqXML);
  arqXML.Version := '1.0';
  arqXML.Encoding := 'UTF-8';
  (*
  arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
  arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
  arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/mdfe/enviMDFe_v1.12.xsd';
  *)

  SerieMDF := FormatFloat('000', DmMDFe_0000.QrMDFeCabAide_Serie.Value);
  NumeroMDF := FormatFloat('00000000', DmMDFe_0000.QrMDFeCabAide_cMDF.Value);

  (* Montar a Chave de Acesso da MDFe de acordo com as informa��es do Registro...*)
  (* cUF=??,dEmi=...*)
  strChaveAcesso := DmMDFe_0000.QrMDFeCabAId.Value;

  CDV := Copy(strChaveAcesso, Length(strChaveAcesso), Length(strChaveAcesso));
  if GerarXMLdoMDFe(FatID, FatNum, Empresa, 'MDFe' + strChaveAcesso, GravaCampos) then
  begin
    DmMDFe_0000.ReopenEmpresa(Empresa);
    //
    //modalXML := rodoXml.XML.Text;
    modalXML := Geral.Substitui(modalXML, ' xmlns="http://www.portalfiscal.inf.br/mdfe"', '');
    modalXml := Geral.Substitui(modalXML, slineBreak, '');
    //
    TextoXML := arqXML.XML.Text;
    //Geral.MB_Info(TextoXML);
    TextoXML := Geral.Substitui(TextoXML, '<_></_>', modalXML);
    DmMDFe_0000.SalvaXML(MDFE_EXT_MDFE_XML, strChaveAcesso, TextoXML, nil, False);
    //
    Result := True;
  end;
  arqXML := nil;
end;

function TFmMDFeGeraXML_0100a.Def(const Grupo, Codigo: Integer;
  const Source: Variant; var Dest: String): Boolean;
const
  sFmtDtHr = 'YYYY-MM-DDTHH:NN:SS';
var
  Continua, Avisa: Boolean;
  Tipo, FormatStr: String;
  TipoVar: TVarType;
begin
  Result := False;
  Dest   := '';
  if DmMDFe_0000.QrMDFeLayI.Locate('Grupo;Codigo', VarArrayOf([Grupo, Codigo]), []) then
  begin
    if DmMDFe_0000.QrMDFeLayIOcorMin.Value > 0 then
    begin
(*    Quntidade na N F e
      if (Codigo = '255') and (ID = 'O11') and (Source = 0) then
        Continua := False else
      if (Codigo = '256') and (ID = 'O12') and (Source = 0) then
        Continua := False else
*)
        Continua := True;
    end else
      Continua := XXe_PF.VarTypeValido(Source, Geral.FF0(Grupo) + '.' + Geral.FF0(Codigo));
    if Continua then
    begin
      Result := True;
      //
      TipoVar   := VarType(Source);
      Tipo      := Uppercase(DmMDFe_0000.QrMDFeLayITipo.Value);
      FormatStr := Uppercase(DmMDFe_0000.QrMDFeLayIFormatStr.Value);
      if (Tipo = 'C') and (TipoVar = varDate) then
        Tipo := 'D';
      //
      // n�mero
      if Tipo = 'N' then
      begin
        // Texto
        if (TipoVar = varString) or (TipoVar = varUString) then
          Dest := Source
        //Double
        else if DmMDFe_0000.QrMDFeLayIDeciCasas.Value > 0 then
          Dest := XXe_PF.DecimalPonto(FloatToStrF(Source, ffFixed, 15,
            DmMDFe_0000.QrMDFeLayIDeciCasas.Value))
        else begin
        //integer
          (*if DmMDFe_0000.QrMDFeLayILeftZeros.Value = 1 then
            Dest := XXe_PF.DecimalPonto(Geral.TFD(FloatToStr(Source),
            DmMDFe_0000.QrMDFeLayITamMax.Value, siPositivo))
          else*)
            Dest := XXe_PF.DecimalPonto(FloatToStr(Source));
            while Length(Dest) < DmMDFe_0000.QrMDFeLayITamMin.Value do
              Dest := '0' + Dest;
        end;
      end else
      // data
      if Tipo = 'D' then
      begin
        if FormatStr = sFmtDtHr then
        begin
          Dest := FormatDateTime('yyyy-mm-dd"T"hh:nn:ss', Source);
        end else
        begin
          if DmMDFe_0000.QrMDFeLayIFormatStr.Value <> 'YYYY-MM-DD' then
            Geral.MB_Aviso('Este � apenas um aviso: "' +
            DmMDFe_0000.QrMDFeLayIFormatStr.Value + '". Informe a DERMATEK!' +
            sLineBreak + 'Grupo: ' + Geral.FF0(Grupo) +
            sLineBreak + 'C�digo: ' + Geral.FF0(Codigo) + sLineBreak);
          Dest := FormatDateTime('yyyy-mm-dd', Source);
        end;
      end else
      // hora
      if Uppercase(DmMDFe_0000.QrMDFeLayITipo.Value) = 'H' then
        Dest := FormatDateTime('hh:nn:ss', Source)
      else
      // texto
      if Uppercase(DmMDFe_0000.QrMDFeLayITipo.Value) = 'C' then
        Dest := Source
      else
      // desconhecido
      begin
        Dest := Source;
        Geral.MB_Aviso(DmMDFe_0000.MensagemDeID_MDFe(Grupo, Codigo, Dest,
        'Tipo de formata��o desconhecida:', 1));
      end;
      //
      Dest := Trim(XXe_PF.ValidaTexto_XML(Dest, Geral.FF0(Grupo), Geral.FF0(Codigo)));
      if (Dest = '') and (DmMDFe_0000.QrMDFeLayIOcorMin.Value > 0) then
      begin
        // verificar se � vazio obrigat�rio
        if DmMDFe_0000.QrMDFeLayIInfoVazio.Value = 0 then
        begin
          Avisa := True;
(*
          if (Codigo = '78') and (ID = 'E17') and
          ((DmMDFe_0000.QrMDFeCabAdest_CNPJ.Value = '') or
          (Geral.SoNumero1a9_TT(DmMDFe_0000.QrMDFeCabAdest_CNPJ.Value) = '')) then
            Avisa := False;
*)
          if Avisa then
            Geral.MB_Aviso(DmMDFe_0000.MensagemDeID_MDFe(Grupo, Codigo, Dest,
            'Valor n�o definido:', 2));
        end;
      end;
      if FGravaCampo = ID_YES then
      begin
        FOrdem := FOrdem + 1;
        //
        MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Inserindo valor de XML no banco de dados.'
        + IntToStr(FOrdem));
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'mdfexmli', False, [
        'Codigo', 'ID', 'Valor'], [
        'FatID', 'FatNum', 'Empresa', 'Ordem'], [
        Grupo, Codigo, Dest], [
        FFatID, FFatNum, FEmpresa, FOrdem], False);
      end;
    end;
  end else Geral.MB_Erro(DmMDFe_0000.MensagemDeID_MDFe(Grupo, Codigo, Dest,
  'N�o foi poss�vel definir o valor', 3));
end;

function TFmMDFeGeraXML_0100a.Def_UTC(const Grupo, Codigo: Integer;
  const Source1, Source2, Source3: Variant; var Dest: String): Boolean;
var
  Continua, Avisa: Boolean;
begin
  Geral.MB_Aviso('Parei aqui! Falta fazer. 3');
{
  //versao := ERf(0, 2, QrOpcoesMDFeMDFeversao.Value, 2);
  Result := False;
  Dest   := '';
  if DmMDFe_0000.QrMDFeLayI.Locate('Grupo;Codigo', VarArrayOf([Grupo, Codigo]), []) then
  begin
    if DmMDFe_0000.QrMDFeLayIOcorMin.Value > 0 then
    begin
(*    Quntidade na N F e
      if (Codigo = '255') and (ID = 'O11') and (Source = 0) then
        Continua := False else
      if (Codigo = '256') and (ID = 'O12') and (Source = 0) then
        Continua := False else
*)
        Continua := True;
    end else
      Continua := XXe_PF.VarTypeValido(Source1, Geral.FF0(Grupo) + '.' + Geral.FF0(Codigo));
    if Continua then
    begin
      Result := True;
      // n�mero
      if Uppercase(DmMDFe_0000.QrMDFeLayITipo.Value) = 'N' then
      begin
        // Texto
        (*if (Codigo = '17') and (ID = 'B13') then
          Dest := Source
         Double
        else*) if DmMDFe_0000.QrMDFeLayIDeciCasas.Value > 0 then
          Dest := XXe_PF.DecimalPonto(FloatToStrF(Source1, ffFixed, 15,
            DmMDFe_0000.QrMDFeLayIDeciCasas.Value))
        else begin
        //integer
          (*if DmMDFe_0000.QrMDFeLayILeftZeros.Value = 1 then
            Dest := XXe_PF.DecimalPonto(Geral.TFD(FloatToStr(Source),
            DmMDFe_0000.QrMDFeLayITamMax.Value, siPositivo))
          else*)
            Dest := XXe_PF.DecimalPonto(FloatToStr(Source1));
        end;
      end else
      // data
      if Uppercase(DmMDFe_0000.QrMDFeLayITipo.Value) = 'D' then
      begin
        if DmMDFe_0000.QrMDFeLayIFormatStr.Value = 'YYYY-MM-DD''T''HH:NN:SS' then
        begin
          Dest := FormatDateTime('yyyy-mm-dd"T"hh:nn:ss', Source1 + Source2);
        end else
        if DmMDFe_0000.QrMDFeLayIFormatStr.Value = 'YYYY-MM-DD''T''HH:NN:SSTZD' then
        begin
          Dest := FormatDateTime('yyyy-mm-dd"T"hh:nn:ss', Source1 + Source2) +
                  dmkPF.TZD_UTC_FloatToSignedStr(Source3);
        end else
        begin
          if DmMDFe_0000.QrMDFeLayIFormatStr.Value <> 'YYYY-MM-DD' then
            Geral.MB_Aviso('Este � apenas um aviso: "' +
            DmMDFe_0000.QrMDFeLayIFormatStr.Value + '". Informe a DERMATEK!');
          Dest := FormatDateTime('yyyy-mm-dd', Source1);
        end;
      end else
      // hora
      if Uppercase(DmMDFe_0000.QrMDFeLayITipo.Value) = 'H' then
        Dest := FormatDateTime('hh:nn:ss', Source1)
      else
      // texto
      if Uppercase(DmMDFe_0000.QrMDFeLayITipo.Value) = 'C' then
        Dest := Source1
      else
      // desconhecido
      begin
        Dest := Source1;
        Geral.MB_Aviso(DmMDFe_0000.MensagemDeID_MDFe(Grupo, Codigo, Dest,
        'Tipo de formata��o desconhecida:', 1));
      end;
      //
      Dest := Trim(XXe_PF.ValidaTexto_XML(Dest, Geral.FF0(Grupo), Geral.FF0(Codigo)));
      if (Dest = '') and (DmMDFe_0000.QrMDFeLayIOcorMin.Value > 0) then
      begin
        // verificar se � vazio obrigat�rio
        if DmMDFe_0000.QrMDFeLayIInfoVazio.Value = 0 then
        begin
          Avisa := True;
(*
          if (Codigo = '78') and (ID = 'E17') and
          ((DmMDFe_0000.QrMDFeCabAdest_CNPJ.Value = '') or
          (Geral.SoNumero1a9_TT(DmMDFe_0000.QrMDFeCabAdest_CNPJ.Value) = '')) then
            Avisa := False;
*)
          if Avisa then
            Geral.MB_Aviso(DmMDFe_0000.MensagemDeID_MDFe(Grupo, Codigo, Dest,
            'Valor n�o definido:', 2));
        end;
      end;
      if FGravaCampo = ID_YES then
      begin
        FOrdem := FOrdem + 1;
        //
        MyObjects.Informa2(FLaAviso1, FLaAviso2, True, 'Inserindo valor de XML no banco de dados.'
        + IntToStr(FOrdem));
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'mdfexmli', False, [
        'Codigo', 'ID', 'Valor'], [
        'FatID', 'FatNum', 'Empresa', 'Ordem'], [
        Grupo, Codigo, Dest], [
        FFatID, FFatNum, FEmpresa, FOrdem], False);
      end;
    end;
  end else Geral.MB_Erro(DmMDFe_0000.MensagemDeID_MDFe(Grupo, Codigo, Dest,
  'N�o foi poss�vel definir o valor', 3));
}
end;

function TFmMDFeGeraXML_0100a.GerarArquivoXMLdoMDFe(FatID, FatNum,
  Empresa: Integer; LaAviso1, LaAviso2: TLabel; GravaCampos: Integer): Boolean;
var
  Continua: Boolean;
  XMLGerado_Arq, XMLGerado_Dir, XMLAssinado_Dir: String;
  Status: Integer;
begin
  Status := DmMDFe_0000.ReopenMDFeCabA(FatID, FatNum, Empresa);
  //Status := DmMDFe_0000.QrMDFeCabAStatus.Value;
  //
  Continua := True;
  if (Continua) or (Status = Integer(mdfemystatusMDFeDados)) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Montando arquivo XML');
    Continua := CriarDocumentoMDFe(FatID, FatNum, Empresa,
      XMLGerado_Arq, XMLGerado_Dir, LaAviso1, LaAviso2, GravaCampos);
    if Continua then
      Continua := DmMDFe_0000.StepMDFeCab(FatID, FatNum, Empresa,
        mdfemystatusMDFeGerada, LaAviso1, LaAviso2);
  end;
  if Continua or (Status = Integer(mdfemystatusMDFeGerada)) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando dados para assinatura do arquivo XML');
    // Assinar MDF-e
    //Continua := False;
    if XMLGerado_Arq = '' then
    begin
      Status := DmMDFe_0000.ReopenMDFeCabA(FatID, FatNum, Empresa);
      if Status = Integer(mdfemystatusMDFeGerada) then
      begin
        DmMDFe_0000.ReopenEmpresa(Empresa);
        //
        XMLGerado_Arq := DmMDFe_0000.QrMDFeCabAId.Value + MDFE_EXT_MDFE_XML;
        XMLGerado_Dir := DmMDFe_0000.QrFilialDirMDFeGer.Value;
      end;
    end;
    if not Geral.VerificaDir(XMLGerado_Dir, '\', 'XML gerado', True) then Exit;
    if XMLGerado_Arq = '' then
      Geral.MB_Aviso('Nome do arquivo da MDF-e gerada indefinido!')
    else
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Assinando arquivo XML');
      Continua := DmMDFe_0000.AssinarArquivoXML(XMLGerado_Arq, XMLGerado_Dir,
        Empresa, 0, DmMDFe_0000.QrMDFeCabAIDCtrl.Value, XMLAssinado_Dir);
    if Continua then
    begin
      //Continua :=
      DmMDFe_0000.StepMDFeCab(FatID, FatNum, Empresa,
        mdfemystatusMDFeAssinada, LaAviso1, LaAviso2);
      Result := True;
    end;
  end;
end;

function TFmMDFeGeraXML_0100a.GerarLoteMDFeNovo(Lote, Empresa: Integer;
  out PathLote, XML_Lote: String; LaAviso1, LaAviso2: TLabel;
  Sincronia: TXXeIndSinc): Boolean;
var
  fArquivoTexto: TextFile;
  MeuXMLAssinado: PChar;
  buf, pathSaida : String;
  mTexto, mTexto2: TStringList;
  XMLAssinado_Dir, XMLAssinado_Arq, XMLArq, LoteStr: String;
  I, IndSinc: Integer;
  XML_STR: String;
  VersaoDados: String;
begin
  Result := False;
  mTexto2 := TStringList.Create;
  mTexto2.Clear;

  DmMDFe_0000.QrMDFeLEnC.Close;
  DmMDFe_0000.QrMDFeLEnC.Params[00].AsInteger := Lote;
  UMyMod.AbreQuery(DmMDFe_0000.QrMDFeLEnC, Dmod.MyDB, 'TFmMDFeGeraXML_0100a.GerarLoteMDFeNovo()');
  //
  if DmMDFe_0000.QrMDFeLEnC.RecordCount = 0 then
  begin
    Result := False;
    Application.MessageBox('N�o existem MDF-e para serem enviadas!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  LoteStr := XXe_PF.StrZero(Lote, 9, 0);
  //
  DmMDFe_0000.ReopenEmpresa(Empresa);
  XMLAssinado_Dir := DmMDFe_0000.QrFilialDirMDFeAss.Value;
  if not Geral.VerificaDir(XMLAssinado_Dir, '\', 'XML assinado', True) then Exit;
  //
  DmMDFe_0000.ObtemDirXML(MDFE_EXT_ENV_LOT_XML, pathSaida, False);
  if not Geral.VerificaDir(pathSaida, '\', 'Lotes de envio de XML', True) then Exit;
  PathLote := pathSaida + LoteStr + MDFE_EXT_ENV_LOT_XML;
  AssignFile(fArquivoTexto, (PathLote));
  Rewrite(fArquivoTexto);
  case Sincronia of
    nisAssincrono: indSInc := 0;
    nisSincrono:   indSinc := 1;
    else Geral.MB_Erro('Sincronia n�o definida em "TFmMDFeGeraXML_0100a.GerarLoteMDFeNovo()"');
  end;
    begin

  end;

  VersaoDados := Geral.FFT_Dot(DmMDFe_0000.QropcoesMDFeMDFeVerEnvLot.Value, 2, siNegativo);

  XML_Lote := '<?xml version="1.0" encoding="UTF-8"?>' +
  '<enviMDFe xmlns="http://www.portalfiscal.inf.br/mdfe" versao="'+
  VersaoDados + '">' +
  '<idLote>' + LoteStr + '</idLote>' +
  //'<indSinc>' + Geral.FF0(indSinc) + '</indSinc>' +
  '';
//repetir essa parte do codigo quando quiser anexar varios arquivos...
  DmMDFe_0000.QrMDFeLEnC.First;
  while not DmMDFe_0000.QrMDFeLEnC.Eof do
  begin
    XMLAssinado_Arq := DmMDFe_0000.QrMDFeLEnCId.Value;
    XMLArq := XMLAssinado_Dir + XMLAssinado_Arq + MDFE_EXT_MDFE_XML;
    if FileExists(XMLArq) then
    begin
      buf := '';
      mTexto := TStringList.Create;
      mTexto.Clear;
      mTexto.LoadFromFile(XMLArq);
      MeuXMLAssinado := PChar(mTexto.Text);
      buf := Copy(MeuXMLAssinado, Pos('<MDFe', MeuXMLAssinado), Length(MeuXMLAssinado));
      XML_Lote := XML_Lote + buf;
      mTexto.Free;
    end;
    DmMDFe_0000.QrMDFeLEnC.Next;
  end;
  XML_Lote := XML_Lote + '</enviMDFe>';
  //
  XML_STR := '';
  for i := 1 to Length(XML_Lote) do
  begin
    if not (XML_Lote[I] in ([#10,#13])) then
      XML_STR := XML_STR + XML_Lote[I];
  end;
  Write(fArquivoTexto, XML_STR);
  CloseFile(fArquivoTexto);
  //
  Result := True;
end;

function TFmMDFeGeraXML_0100a.GerarXMLdoMDFe(FatID, FatNum, Empresa: Integer;
  const mdfeID: String; GravaCampos: Integer): Boolean;
var
  K, h, j, nItem, Controle: Integer;
  Valor, IM, xNome, infAdProd: String;
  DataHora: TDateTime;
  AnyNode: IXMLNode;
  Item: Integer;
begin
  modalXML := '';
  //
  Result := False;
  FGravaCampo := GravaCampos;
  DmMDFe_0000.ReopenMDFeLayI();
(*1*) // Informa��es da TAG InfMDFe...
  // Vers�o do layout
  if Def(0, 2, DmMDFe_0000.QrMDFeCabAversao.Value, Valor) then
    cXML.InfMDFe.Versao := Valor;
  //if Def(0, 3, mdfeID, Valor) then
  cXML.InfMDFe.Id := mdfeID;
  (*4*) // Informa��es da TAG InfMDFe.Ide...
  // C�digo da UF do emitente do Documento Fiscal. Utilizar a Tabela do IBGE
  if Def(0, 5, DmMDFe_0000.QrMDFeCabAide_cUF.Value, Valor) then
    cXML.InfMDFe.Ide.CUF := Valor;
  //C�digo num�rico que comp�e a Chave de Acesso. N�mero aleat�rio gerado pelo emitente para cada MDF-e
  if Def(0, 6, DmMDFe_0000.QrMDFeCabAide_tpAmb.Value, Valor) then
    cXML.InfMDFe.Ide.TpAmb := Valor;
  if Def(0, 7, DmMDFe_0000.QrMDFeCabAide_tpEmit.Value, Valor) then
    cXML.InfMDFe.Ide.TpEmit := Valor;
  //C�digo do modelo do Documento Fiscal. Utilizar 55 para identifica��o da MDF-e, emitida em substitui��o ao modelo 1 e 1A.
  if Def(0, 8, DmMDFe_0000.QrMDFeCabAide_mod.Value, Valor) then
    cXML.InfMDFe.Ide.Mod_ := Valor;//'58';
  //S�rie do Documento Fiscal
  if Def(0, 9, DmMDFe_0000.QrMDFeCabAide_serie.Value, Valor) then
    cXML.InfMDFe.Ide.Serie := Valor;
  //N�mero do Documento Fiscal
  if Def(0, 10, DmMDFe_0000.QrMDFeCabAide_nMDF.Value, Valor) then
    cXML.InfMDFe.Ide.NMDF := Valor;
  if Def(0, 11, DmMDFe_0000.QrMDFeCabAide_cMDF.Value, Valor) then
    cXML.InfMDFe.Ide.CMDF := Valor;
  if Def(0, 12, DmMDFe_0000.QrMDFeCabAide_cDV.Value, Valor) then
    cXML.InfMDFe.Ide.CDV := Valor;
  if Def(0, 13, DmMDFe_0000.QrMDFeCabAide_Modal.Value, Valor) then
    cXML.InfMDFe.Ide.Modal := Valor;
  //Data e hora no formato : aaaa-mm-ddThh:mm:ss
  //if Def_UTC(0, 14,
  DataHora := DmMDFe_0000.QrMDFeCabAide_dEmi.Value +
              DmMDFe_0000.QrMDFeCabAide_hEmi.Value;
  if Def(0, 14, DataHora, Valor) then
    cXML.InfMDFe.Ide.DhEmi := Valor;
  if Def(0, 15, DmMDFe_0000.QrMDFeCabAide_tpEmis.Value, Valor) then
    cXML.InfMDFe.Ide.TpEmis := Valor;
  if Def(0, 16, DmMDFe_0000.QrMDFeCabAide_procEmi.Value, Valor) then
    cXML.InfMDFe.Ide.ProcEmi := Valor;
  if Def(0, 17, DmMDFe_0000.QrMDFeCabAide_verProc.Value, Valor) then
    cXML.InfMDFe.Ide.VerProc := Valor;
  if Def(0, 18, DmMDFe_0000.QrMDFeCabAide_UFIni.Value, Valor) then
    cXML.InfMDFe.Ide.UFIni := Valor;
  if Def(0, 19, DmMDFe_0000.QrMDFeCabAide_UFFim.Value, Valor) then
    cXML.InfMDFe.Ide.UFFim := Valor;
  //
  //
  (*20*) // infMunCarrega - Informa��es dos Munic�pios de Carregamento
  DmMDFe_0000.ReopenMDFeit2InfMunCarrega(FatID, FatNum, Empresa);
  while not DmMDFe_0000.QrMDFeit2InfMunCarrega.Eof do
  begin
    cInfMunCarrega := cXML.InfMDFe.Ide.InfMunCarrega.Add;
    //
    if Def(0, 21, DmMDFe_0000.QrMDFeit2InfMunCarregacMunCarrega.Value, Valor) then
      cInfMunCarrega.CMunCarrega := Valor;
    if Def(0, 22, DmMDFe_0000.QrMDFeit2InfMunCarregaxMunCarrega.Value, Valor) then
      cInfMunCarrega.XMunCarrega := Valor;
    //
    DmMDFe_0000.QrMDFeit2InfMunCarrega.Next;
  end;
  //
  //
  (*23*) // infPercurso - Informa��es do Percurso do MDF-e
  DmMDFe_0000.ReopenMDFeit2InfPercurso(FatID, FatNum, Empresa);
  while not DmMDFe_0000.QrMDFeit2InfPercurso.Eof do
  begin
    cInfPercurso := cXML.InfMDFe.Ide.InfPercurso.Add;
    //
    if Def(0, 24, DmMDFe_0000.QrMDFeit2InfPercursoUFPer.Value, Valor) then
      cInfPercurso.UFPer := Valor;
    //
    DmMDFe_0000.QrMDFeit2InfPercurso.Next;
  end;
  DataHora := DmMDFe_0000.QrMDFeCabAide_dIniViagem.Value +
              DmMDFe_0000.QrMDFeCabAide_hIniViagem.Value;
  if Def(0, 25, DataHora, Valor) then
    cXML.InfMDFe.Ide.DhIniViagem := Valor;
  (*26*)// Emit - Identifica��o do Emitente do Manifesto
  if Def(0, 27, Geral.SoNumero_TT(DmMDFe_0000.QrMDFeCabAemit_CNPJ.Value), Valor) then
    cXML.InfMDFe.Emit.CNPJ := Valor;
  if Def(0, 28, Geral.SoNumero_TT(DmMDFe_0000.QrMDFeCabAemit_IE.Value), Valor) then
    cXML.InfMDFe.Emit.IE := Valor;
  if Def(0, 29, DmMDFe_0000.QrMDFeCabAemit_xNome.Value, Valor) then
    cXML.InfMDFe.Emit.XNome := Valor;
  if Def(0, 30, DmMDFe_0000.QrMDFeCabAemit_xFant.Value, Valor) then
    cXML.InfMDFe.Emit.XFant := Valor;
  (*31*) // enderEmit - Endere�o do emitente
  if Def(0, 32, DmMDFe_0000.QrMDFeCabAemit_xLgr.Value, Valor) then
    cXML.InfMDFe.Emit.EnderEmit.XLgr := Valor;
  if Def(0, 33, DmMDFe_0000.QrMDFeCabAemit_nro.Value, Valor) then
    cXML.InfMDFe.Emit.EnderEmit.Nro := Valor;
  if Def(0, 34, DmMDFe_0000.QrMDFeCabAemit_xCpl.Value, Valor) then
    cXML.InfMDFe.Emit.EnderEmit.XCpl := Valor;
  if Def(0, 35, DmMDFe_0000.QrMDFeCabAemit_xBairro.Value, Valor) then
    cXML.InfMDFe.Emit.EnderEmit.XBairro := Valor;
  if Def(0, 36, DmMDFe_0000.QrMDFeCabAemit_cMun.Value, Valor) then
    cXML.InfMDFe.Emit.EnderEmit.CMun := Valor;
  if Def(0, 37, DmMDFe_0000.QrMDFeCabAemit_xMun.Value, Valor) then
    cXML.InfMDFe.Emit.EnderEmit.XMun := Valor;
  if Def(0, 38, DmMDFe_0000.QrMDFeCabAemit_CEP.Value, Valor) then
    cXML.InfMDFe.Emit.EnderEmit.CEP := Valor;
  if Def(0, 39, DmMDFe_0000.QrMDFeCabAemit_UF.Value, Valor) then
    cXML.InfMDFe.Emit.EnderEmit.UF := Valor;
  if Def(0, 40, DmMDFe_0000.QrMDFeCabAemit_fone.Value, Valor) then
    cXML.InfMDFe.Emit.EnderEmit.Fone := Valor;
  if Def(0, 41, DmMDFe_0000.QrMDFeCabAemit_email.Value, Valor) then
    cXML.InfMDFe.Emit.EnderEmit.Email := Valor;
  (*42*) // infModal - Informa��es do modal
  if Def(0, 43, DmMDFe_0000.QrMDFeCabAversaoModal.Value, Valor) then
    cXML.InfMDFe.InfModal.VersaoModal := Valor;
  (*44*) // xs:any
  if TMDFeModal(DmMDFe_0000.QrMDFeCabAide_Modal.Value) = mdfemodalRodoviario then
  begin
    AnyNode := cXML.InfMDFe.InfModal.AddChild('_');
    AnyNode.Text := '';
    //
////////////////////////////////////////////////////////////////////////////////
/// Layout Modal Rodovi�rio ////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
    (*44*)GerarXMLmodalRodo(FatID, FatNum, Empresa, GravaCampos);
///
////////////////////////////////////////////////////////////////////////////////
  end else
  begin
    Geral.MB_Aviso('Modal selecionado n�o implementado! Solicite � DERMATEK');
  (*
  (mdfemodalIndefinido=0, =1, mdfemodalAereo=2,
  mdfemodalAquaviario=3, mdfemodalFerroviario=4,
  mdfemodalDutoviario=5, mdfemodalMultimodal=6);
  *)
  end;
  (*45*) // ifDoc - Informa��es dos Documentos fiscais vinculados ao manifesto
    (*46*) // infMundescarga - Informa��es dos Munic�pios de descarregamento
  DmMDFe_0000.ReopenMDFeit2InfMunDescarga(FatID, FatNum, Empresa);
  while not DmMDFe_0000.QrMDFeit2InfMunDescarga.Eof do
  begin
    cInfMunDescarga := cXML.InfMDFe.InfDoc.Add;
    Controle        := DmMDFe_0000.QrMDFeit2InfMunDescargaControle.Value;
    Item            := DmMDFe_0000.QrMDFeit2InfMunDescarga.RecNo - 1;
    //
    if Def(0, 47, DmMDFe_0000.QrMDFeit2InfMunDescargacMunDescarga.Value, Valor) then
      cInfMunDescarga.CMunDescarga := Valor;
    if Def(0, 48, DmMDFe_0000.QrMDFeit2InfMunDescargaxMunDescarga.Value, Valor) then
      cInfMunDescarga.XMunDescarga := Valor;
    //
    (*49*) // infCTe - Conhecimentos de Tranporte - usar este grupo quando for prestador de servi�o de transporte
    DmMDFe_0000.ReopenMDFeit3InfCTe(FatID, FatNum, Empresa, Controle);
    while not DmMDFe_0000.QrMDFeit3InfCTe.Eof do
    begin
      cInfCTe := cXML.InfMDFe.InfDoc.InfMunDescarga[Item].InfCte.Add;
      //
      if Def(0, 50, DmMDFe_0000.QrMDFeit3InfCTechCTe.Value, Valor) then
        cInfCTe.ChCTe:= Valor;
      if Def(0, 51, DmMDFe_0000.QrMDFeit3InfCTeSegCodBarra.Value, Valor) then
        cInfCTe.SegCodBarra := Valor;
      //
////////////////////////////////////////////////////////////////////////////////
    (*52*)// infUnidTransp - Tabela Separada
        (*53*)
        (*..*)
        (*63*)
////////////////////////////////////////////////////////////////////////////////
      //
      //
      DmMDFe_0000.QrMDFeit3InfCTe.Next;
      //
    end;
    (*64*) // infNFe - Nota Fiscal Eletronica
    DmMDFe_0000.ReopenMDFeit3InfNFe(FatID, FatNum, Empresa, Controle);
    while not DmMDFe_0000.QrMDFeit3InfNFe.Eof do
    begin
      cInfNFe := cXML.InfMDFe.InfDoc.InfMunDescarga[Item].InfNFe.Add;
      //
      if Def(0, 65, DmMDFe_0000.QrMDFeit3InfNFechNFe.Value, Valor) then
        cInfNFe.ChNFe:= Valor;
      if Def(0, 66, DmMDFe_0000.QrMDFeit3InfNFeSegCodBarra.Value, Valor) then
        cInfNFe.SegCodBarra := Valor;
      //
////////////////////////////////////////////////////////////////////////////////
    (*67*)// infUnidTransp - Tabela Separada
        (*68*)
        (*..*)
        (*78*)
////////////////////////////////////////////////////////////////////////////////
      //
      //
      DmMDFe_0000.QrMDFeit3InfNFe.Next;
      //
    end;
    (*79*) // infMDFeTransp - Manifesto Eletr�nico de Documentos Fiscais.
           // Somente para modal Aquavi�rio (vide regras MOC)
    DmMDFe_0000.ReopenMDFeit3InfMDFe(FatID, FatNum, Empresa, Controle);
    while not DmMDFe_0000.QrMDFeit3InfMDFe.Eof do
    begin
      cInfMDFeTransp := cXML.InfMDFe.InfDoc.InfMunDescarga[Item].InfMDFeTransp.Add;
      //
      if Def(0, 80, DmMDFe_0000.QrMDFeit3InfMDFechMDFe.Value, Valor) then
        cInfMDFeTransp.ChMDFe:= Valor;
      //
////////////////////////////////////////////////////////////////////////////////
    (*81*)// infUnidTransp - Tabela Separada
        (*82*)
        (*..*)
        (*92*)
////////////////////////////////////////////////////////////////////////////////
      //
      //
      DmMDFe_0000.QrMDFeit3InfMDFe.Next;
      //
    end;
    //
    //
    DmMDFe_0000.QrMDFeit2InfMunDescarga.Next;
  end;
  //
  //
  (*93*) // tot - Totalizadores da carga transportada e seus documentos fiscais
  if Def(0, 94, DmMDFe_0000.QrMDFeCabATot_qCTe.Value, Valor) then
    cXML.InfMDFe.Tot.QCTe := Valor;
  if Def(0, 95, DmMDFe_0000.QrMDFeCabATot_qNFe.Value, Valor) then
    cXML.InfMDFe.Tot.QNFe := Valor;
(* Nao existe no manual!!!
  if Def(0, 96, DmMDFe_0000.QrMDFeCabATot_.Value, Valor) then
    cXML.InfMDFe.Tot. := Valor;
*)
  if Def(0, 97, DmMDFe_0000.QrMDFeCabATot_qMDFe.Value, Valor) then
    cXML.InfMDFe.Tot.QMDFe := Valor;
  if Def(0, 98, DmMDFe_0000.QrMDFeCabATot_vCarga.Value, Valor) then
    cXML.InfMDFe.Tot.VCarga := Valor;
  if Def(0, 99, DmMDFe_0000.QrMDFeCabATot_cUnid.Value, Valor) then
    cXML.InfMDFe.Tot.CUnid := Valor;
  if Def(0, 100, DmMDFe_0000.QrMDFeCabATot_qCarga.Value, Valor) then
    cXML.InfMDFe.Tot.QCarga := Valor;
  //
  //
  (*101*) // lacres - Lacres da MDF-e
  DmMDFe_0000.ReopenMDFeIt1Lacres(FatID, FatNum, Empresa);
  while not DmMDFe_0000.QrMDFeIt1Lacres.Eof do
  begin
    cLacres := cXML.InfMDFe.Lacres.Add;
    //
    if Def(0, 102, DmMDFe_0000.QrMDFeIt1LacresnLacre.Value, Valor) then
      cLacres.NLacre := Valor;
    //
    DmMDFe_0000.QrMDFeIt1Lacres.Next;
  end;
  //
  //
  (*103*) // autXML - Autorizados para download do XML do DF-e
  DmMDFe_0000.ReopenMDFeIt1AutXML(FatID, FatNum, Empresa);
  while not DmMDFe_0000.QrMDFeIt1AutXML.Eof do
  begin
    cAutXML := cXML.InfMDFe.AutXML.Add;
    //
    if Geral.SoNumero_TT(DmMDFe_0000.QrMDFeIt1AutXmlCNPJ.Value) <> '' then
      if Def(0, 104, DmMDFe_0000.QrMDFeIt1AutXmlCNPJ.Value, Valor) then
        cAutXML.CNPJ := Valor
    else
      if Def(0, 105, DmMDFe_0000.QrMDFeIt1AutXmlCPF.Value, Valor) then
        cAutXML.CPF := Valor;
    //
    DmMDFe_0000.QrMDFeIt1AutXML.Next;
  end;
  //
  (*106*) // infAdic - Informa��es Adicionais
  if Def(0, 107, DmMDFe_0000.QrMDFeCabAinfAdFisco.Value, Valor) then
    cXML.InfMDFe.InfAdic.InfAdFisco := Valor;
  if Def(0, 108, DmMDFe_0000.QrMDFeCabAinfCpl.Value, Valor) then
    cXML.InfMDFe.InfAdic.InfCpl := Valor;
  (*109*) // -- - ds:Signature
  //
  Result := True;
end;

function TFmMDFeGeraXML_0100a.GerarXMLmodalRodo(FatID, FatNum, Empresa,
  GravaCampos: Integer): Boolean;
var
  Valor: String;
  DataHora: TDateTime;
  AnyNode: IXMLNode;
  Controle: Integer;
begin
  rodoXML := TXMLDocument.Create(nil);
  rodoXML.Active := False;
  rodoXML.FileName := '';
  //rodoXML.DocumentElement.Attributes['xmlns'] := '';
  cRodo := GetRodo(rodoXML);
  //rodoXML.Version := '1.0';
  //rodoXML.Encoding := 'UTF-8';
  DmMDFe_0000.ReopenMDFeMoRodoCab(FatID, FatNum, Empresa);
  if Def(1, 2, DmMDFe_0000.QrMDFeMoRodoCabRNTRC.Value, Valor) then
    cRodo.RNTRC := Valor;
  //
  DmMDFe_0000.ReopenMDFeMoRodoCIOT(FatID, FatNum, Empresa);
  DmMDFe_0000.QrMDFeMoRodoCIOT.First;
  while not DmMDFe_0000.QrMDFeMoRodoCIOT.Eof do
  begin
    //Item := DmMDFe_0000.QrMDFeMoRodoCIOT.RecNo;
    //cRodo.CIOT.Add;
      //
(*
    if Def(1, 3, DmMDFe_0000.QrMDFeMoRodoCIOTCIOT.Value, Valor) then
      cRodo.CIOT[Item] := Valor;
*)
    if Def(1, 3, DmMDFe_0000.QrMDFeMoRodoCIOTCIOT.Value, Valor) then
    begin
      AnyNode := cRodo.AddChild('CIOT');
      AnyNode.Text := Valor;
    end;
    //
    //
    DmMDFe_0000.QrMDFeMoRodoCIOT.Next;
  end;
  (*4*) // veicTracao - Dados do Ve�culo com a Tra��o
  if Def(1, 5, DmMDFe_0000.QrMDFeMoRodoCabveicTracao_cInt.Value, Valor) then
    cRodo.VeicTracao.CInt := Valor;
  if Def(1, 6, DmMDFe_0000.QrMDFeMoRodoCabveicTracao_placa.Value, Valor) then
    cRodo.VeicTracao.Placa := Valor;
  if Def(1, 7, DmMDFe_0000.QrMDFeMoRodoCabveicTracao_RENAVAM.Value, Valor) then
    cRodo.VeicTracao.RENAVAM := Valor;
  if Def(1, 8, DmMDFe_0000.QrMDFeMoRodoCabveicTracao_tara.Value, Valor) then
    cRodo.VeicTracao.Tara := Valor;
  if Def(1, 9, DmMDFe_0000.QrMDFeMoRodoCabveicTracao_capKg.Value, Valor) then
    cRodo.VeicTracao.CapKG := Valor;
  if Def(1, 10, DmMDFe_0000.QrMDFeMoRodoCabveicTracao_capM3.Value, Valor) then
    cRodo.VeicTracao.CapM3 := Valor;
 (*11*) // prop
  DmMDFe_0000.ReopenMDFeMoRodoPropT(FatID, FatNum, Empresa);
  if DmMDFe_0000.QrMDFeMoRodoPropT.RecordCount > 0 then
  begin
    if Geral.SoNumero_TT(DmMDFe_0000.QrMDFeMoRodoPropTCPF.Value) <> '' then
    begin
      if Def(0, 12, DmMDFe_0000.QrMDFeMoRodoPropTCPF.Value, Valor) then
        cRodo.VeicTracao.Prop.CPF := Valor;
    end else
    if Def(0, 13, DmMDFe_0000.QrMDFeMoRodoPropTCNPJ.Value, Valor) then
      cRodo.VeicTracao.Prop.CNPJ := Valor;
    //
    if Def(1, 14, DmMDFe_0000.QrMDFeMoRodoPropTRNTRC.Value, Valor) then
      cRodo.VeicTracao.Prop.RNTRC := Valor;
    if Def(1, 15, DmMDFe_0000.QrMDFeMoRodoPropTxNome.Value, Valor) then
      cRodo.VeicTracao.Prop.XNome := Valor;
    if Geral.SoNumero_TT(DmMDFe_0000.QrMDFeMoRodoPropTCNPJ.Value) <> '' then
    begin
      if Def(1, 16, DmMDFe_0000.QrMDFeMoRodoPropTIE.Value, Valor) then
        cRodo.VeicTracao.Prop.IE := Valor;
    end else
        cRodo.VeicTracao.Prop.IE := '';
    //
    if Def(1, 17, DmMDFe_0000.QrMDFeMoRodoPropTUF.Value, Valor) then
      cRodo.VeicTracao.Prop.UF := Valor;
    if Def(1, 18, DmMDFe_0000.QrMDFeMoRodoPropTtpProp.Value, Valor) then
      cRodo.VeicTracao.Prop.TpProp := Valor;
  end;
  (*19*) // Condutor
  DmMDFe_0000.ReopenMDFeMoRodoCondutor(FatID, FatNum, Empresa);
  DmMDFe_0000.QrMDFeMoRodoCondutor.First;
  while not DmMDFe_0000.QrMDFeMoRodoCondutor.Eof do
  begin
    cRodo_veicTracao_condutor := cRodo.VeicTracao.Condutor.Add;
    //
    if Def(1, 20, DmMDFe_0000.QrMDFeMoRodoCondutorxNome.Value, Valor) then
      cRodo_veicTracao_condutor.XNome := Valor;
    if Def(1, 21, DmMDFe_0000.QrMDFeMoRodoCondutorCPF.Value, Valor) then
      cRodo_veicTracao_condutor.CPF := Valor;
    //
    DmMDFe_0000.QrMDFeMoRodoCondutor.Next;
  end;
  //
  if Def(1, 22, DmMDFe_0000.QrMDFeMoRodoCabveicTracao_tpRod.Value, Valor) then
    cRodo.VeicTracao.TpRod := Valor;
  if Def(1, 23, DmMDFe_0000.QrMDFeMoRodoCabveicTracao_tpCar.Value, Valor) then
    cRodo.VeicTracao.TpCar := Valor;
  if Def(1, 24, DmMDFe_0000.QrMDFeMoRodoCabveicTracao_UF.Value, Valor) then
    cRodo.VeicTracao.UF := Valor;
  (*25*) // veicReboque - Dados dos reboques
  DmMDFe_0000.ReopenMDFeMoRodoVeicRebo(FatID, FatNum, Empresa);
  DmMDFe_0000.QrMDFeMoRodoVeicRebo.First;
  while not DmMDFe_0000.QrMDFeMoRodoVeicRebo.Eof do
  begin
    cRodo_veicReboque := cRodo.VeicReboque.Add;
    //
    if Def(1, 26, DmMDFe_0000.QrMDFeMoRodoVeicRebocInt.Value, Valor) then
      cRodo_veicReboque.CInt := Valor;
    if Def(1, 27, DmMDFe_0000.QrMDFeMoRodoVeicReboplaca.Value, Valor) then
      cRodo_veicReboque.Placa := Valor;
    if Def(1, 28, DmMDFe_0000.QrMDFeMoRodoVeicReboRENAVAM.Value, Valor) then
      cRodo_veicReboque.RENAVAM := Valor;
    if Def(1, 29, DmMDFe_0000.QrMDFeMoRodoVeicRebotara.Value, Valor) then
      cRodo_veicReboque.Tara := Valor;
    if Def(1, 30, DmMDFe_0000.QrMDFeMoRodoVeicRebocapKG.Value, Valor) then
      cRodo_veicReboque.CapKG := Valor;
    if Def(1, 31, DmMDFe_0000.QrMDFeMoRodoVeicRebocapM3.Value, Valor) then
      cRodo_veicReboque.CapM3 := Valor;
    (*32*) // prop
    Controle := DmMDFe_0000.QrMDFeMoRodoVeicReboControle.Value;
    DmMDFe_0000.ReopenMDFeMoRodoPropR(FatID, FatNum, Empresa, Controle);
    if DmMDFe_0000.QrMDFeMoRodoPropR.RecordCount > 0 then
    begin
      if Geral.SoNumero_TT(DmMDFe_0000.QrMDFeMoRodoPropRCPF.Value) <> '' then
      begin
        if Def(0, 33, DmMDFe_0000.QrMDFeMoRodoPropRCPF.Value, Valor) then
          cRodo_veicReboque.Prop.CPF := Valor;
      end else
      if Def(0, 34, DmMDFe_0000.QrMDFeMoRodoPropRCNPJ.Value, Valor) then
        cRodo_veicReboque.Prop.CNPJ := Valor;
      //
      if Def(1, 35, DmMDFe_0000.QrMDFeMoRodoPropRRNTRC.Value, Valor) then
        cRodo_veicReboque.Prop.RNTRC := Valor;
      if Def(1, 36, DmMDFe_0000.QrMDFeMoRodoPropRxNome.Value, Valor) then
        cRodo_veicReboque.Prop.XNome := Valor;
      if Def(1, 37, DmMDFe_0000.QrMDFeMoRodoPropRIE.Value, Valor) then
        cRodo_veicReboque.Prop.IE := Valor;
      if Def(1, 38, DmMDFe_0000.QrMDFeMoRodoPropRUF.Value, Valor) then
        cRodo_veicReboque.Prop.UF := Valor;
      if Def(1, 39, DmMDFe_0000.QrMDFeMoRodoPropRtpProp.Value, Valor) then
        cRodo_veicReboque.Prop.TpProp := Valor;
    end;
    //
    if Def(1, 40, DmMDFe_0000.QrMDFeMoRodoVeicRebotpCar.Value, Valor) then
      cRodo_veicReboque.TpCar := Valor;
    if Def(1, 41, DmMDFe_0000.QrMDFeMoRodoVeicReboUF.Value, Valor) then
      cRodo_veicReboque.UF := Valor;
    //
    DmMDFe_0000.QrMDFeMoRodoVeicRebo.Next;
  end;
  //
  (*42*) // valePed - Informa��es de vale ped�gio
  (*43*) // disp
  DmMDFe_0000.ReopenMDFeMoRodoVPed(FatID, FatNum, Empresa);
  DmMDFe_0000.QrMDFeMoRodoVPed.First;
  while not DmMDFe_0000.QrMDFeMoRodoVPed.Eof do
  begin
    cRodo_valePed_disp := cRodo.ValePed.Add();
    //
    if Def(1, 44, DmMDFe_0000.QrMDFeMoRodoVPedCNPJForn.Value, Valor) then
      cRodo_valePed_disp.CNPJForn := Valor;
    if Def(1, 45, DmMDFe_0000.QrMDFeMoRodoVPedCNPJPg.Value, Valor) then
      cRodo_valePed_disp.CNPJPg := Valor;
    if Def(1, 46, DmMDFe_0000.QrMDFeMoRodoVPednCompra.Value, Valor) then
      cRodo_valePed_disp.nCompra := Valor;
    //
    DmMDFe_0000.QrMDFeMoRodoVPed.Next;
  end;
  if Def(1, 47, DmMDFe_0000.QrMDFeMoRodoCabcodAgPorto.Value, Valor) then
    cRodo.CodAgPorto := Valor;
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  modalXML := rodoXML.XML.Text;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

function TFmMDFeGeraXML_0100a.MontaID_Inutilizacao(const cUF, Ano, emitCNPJ,
  Modelo, Serie, nMDFIni, nMDFFim: String; var Id: String): Boolean;
var
  K: Integer;
begin
  Id := 'ID' + cUF + (*Ano +*) emitCNPJ + Modelo +
    XXe_PF.StrZero(StrToInt(Serie),3,0) +
    XXe_PF.StrZero(StrToInt(nMDFIni),9,0) +
    XXe_PF.StrZero(StrToInt(nMDFFim),9,0);
  K := Length(Id);
  if K = 41 then
    Result := True
  else begin
    Result := False;
    Geral.MB_Erro('ID de inutiliza��o com tamanho inv�lido: "' +
    Id + '". Deveria ter 41 carateres e tem ' + IntToStr(K) + '.');
  end;
end;

function TFmMDFeGeraXML_0100a.NomeAcao(Acao: TTipoConsumoWS_MDFe): String;
begin
  Result := sTipoConsumoWS_MDFe[Integer(Acao)];
end;

function TFmMDFeGeraXML_0100a.ObtemWebServer2(UFServico: String; Ambiente,
  CodigoUF: Integer; Acao: TTipoConsumoWS_MDFe; sAviso: String): Boolean;

  function TextoAmbiente(Ambiente: Byte): String;
  begin
    case Ambiente of
        1: Result := 'Produ��o';
        2: Result := 'Homologa��o';
      else Result := '[Desconhecido]';
    end;
  end;

  function TextoAcao(Acao: TTipoConsumoWS_MDFe): String;
  begin
    Result := sNomeConsumoWS_MDFe[Integer(Acao)];
  end;

  function TipoConsumoWs2Acao(TipoConsumoWS: TTipoConsumoWS_MDFe): Byte;
  begin
    Result := Integer(TipoConsumoWS);
  end;

var
  cUF, A: Integer;
  Versao, x: String;
begin
  FURL := MDFe_PF.ObtemURLWebServerMDFe(UFServico, NomeAcao(Acao), VersaoWS(Acao, True), Ambiente);
  A    := TipoConsumoWs2Acao(Acao);
  cUF  := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(UFServico);
  x    := Uppercase(UFServico + ' '+ NomeAcao(Acao) + ' ' + VersaoWS(Acao, True));
  //
  if FURL = '' then
  begin
    sAviso := 'N�o foi poss�vel definir o endere�o do Web Service! ' + sLineBreak  +
    x + sLineBreak +
    'UF SEFAZ: ' + Geral.FF0(CodigoUF) + ' = ' + Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(CodigoUF)
    + sLineBreak  + 'UF Servi�o: ' + Geral.FF0(cUF) + ' = ' + UFServico
    + sLineBreak  + 'Ambiente: ' + Geral.FF0(Ambiente) + ' = ' + TextoAmbiente(
    Ambiente) + sLineBreak  + 'A��o: ' + Geral.FF0(A) + ' = ' + TextoAcao(Acao)
    + slineBreak + 'x = "' + x + '"' + sLineBreak +
    sLineBreak  + 'AVISE A DERMATEK' + sLineBreak;
    Result := False;
    Geral.MB_Aviso(sAviso);
    Exit;
  end else begin
    Result := True;
    sAviso := FURL;
  end;
end;

procedure TFmMDFeGeraXML_0100a.OnBeforePost(const HTTPReqResp: THTTPReqResp;
  Data: Pointer);
var
  Store        : IStore;
  Certs        : ICertificates;
  Cert         : ICertificate2;
  CertContext  : ICertContext;
  PCertContext : PCCERT_CONTEXT;
  i : Integer;
  ContentHeader: string;
begin
  Store := CoStore.Create;
  //Reposit�rios de Certifcados da M�quina

  Store.Open( CAPICOM_CURRENT_USER_STORE, 'MY',    CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED );
  //Abre a lista de certificados

  Certs := Store.Certificates ;
  //Aloca todos os certificados instalados na m�quia

  i := 0;
  //loop de procura ao certificado requerido pelo n�mero serial
  while i < Certs.Count do
   begin
     Cert := IInterface( Certs.Item[ i+1 ] ) as ICertificate2;
     //Cria objeto para acesso a leitura do certificado
     if UpperCase(Cert.SerialNumber ) = UpperCase(CO_MDFE_CERTIFICADO_DIGITAL_SERIAL_NUMBER) then
     //se o n�mero do serial for igual ao que queremos utilizar
      begin
        //carrega informa��es do certificado
        CertContext := Cert as ICertContext;
        CertContext.Get_CertContext( Integer( PCertContext ) );

        //if not (InternetSetOption(Data, 84, PCertContext, Sizeof(CERT_CONTEXT))) then
        if not InternetSetOption(Data, INTERNET_OPTION_CLIENT_CERT_CONTEXT, PCertContext, sizeof(CertContext)*5) then
        begin
          Geral.MB_Aviso('Falha ao selecionar o certificado.');
        end;

        ContentHeader := Format(ContentTypeTemplate, ['application/soap+xml; charset=utf-8']);
        HttpAddRequestHeaders(Data, PChar(ContentHeader), Length(ContentHeader), HTTP_ADDREQ_FLAG_REPLACE);

        i := Certs.Count;
        //encerra o loop
      end;
     i := i + 1;
  end;
end;

function TFmMDFeGeraXML_0100a.SeparaDados(Texto: String; Chave: String;
  MantemChave: Boolean): String;
var
  PosIni, PosFim : Integer;
begin
  if MantemChave then
   begin
     PosIni := Pos(Chave,Texto)-1;
     PosFim := Pos('/'+Chave,Texto)+length(Chave)+3;

     if (PosIni = 0) or (PosFim = 0) then
      begin
        PosIni := Pos('ns2:'+Chave,Texto)-1;
        PosFim := Pos('/ns2:'+Chave,Texto)+length(Chave)+3;
      end;
   end
  else
   begin
     PosIni := Pos(Chave,Texto)+Pos('>',copy(Texto,Pos(Chave,Texto),length(Texto)));
     PosFim := Pos('/'+Chave,Texto);

     if (PosIni = 0) or (PosFim = 0) then
      begin
        PosIni := Pos('ns2:'+Chave,Texto)+Pos('>',copy(Texto,Pos('ns2:'+Chave,Texto),length(Texto)));
        PosFim := Pos('/ns2:'+Chave,Texto);
      end;
   end;
  Result := copy(Texto,PosIni,PosFim-(PosIni+1));
  if Result = '' then
    Geral.MB_Erro(Texto);
end;

function TFmMDFeGeraXML_0100a.TipoXML(NoStandAlone: Boolean): String;
begin
  Result :=
    '<?xml version="' + sXML_Version + '" encoding="' + sXML_Encoding + '"';
  if NoStandAlone then Result := Result + ' standalone="no"';
  Result := Result +  '?>';
end;

function TFmMDFeGeraXML_0100a.VersaoWS(Acao: TTipoConsumoWS_MDFe;
  Formata: Boolean): String;
begin
  Result := sVersaoConsumoWS_MDFe[Integer(Acao)];
end;

function TFmMDFeGeraXML_0100a.WS_MDFeConsultaMDF(UFServico: String; Ambiente,
  CodigoUF: Byte; ChaveMDFe: String; LaAviso1, LaAviso2: TLabel;
  RETxtEnvio: TMemo; EdWS: TEdit): String;
const
  TipoConsumo = tcwsmdfeConsultaMDFe;
var
  sAviso, Texto, ProcedimentoEnv, ProcedimentoRet: String;
  Acao: TStringList;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  VersaoDados: String;
begin
  Screen.Cursor := crHourGlass;

  VersaoDados := Geral.FFT_Dot(DmMDFe_0000.QropcoesMDFeMDFeVerConMDFe.Value, 2, siNegativo);

  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso) then Exit;
  //
  FDadosTxt := XML_ConsSitMDFe(Ambiente, ChaveMDFe, VersaoDados);
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;


  ProcedimentoEnv := 'MDFeConsulta';
  ProcedimentoRet := 'mdfeConsultaMDFResult';

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<mdfeCabecMsg xmlns="http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</mdfeCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<mdfeDadosMsg xmlns="http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</mdfeDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

     ReqResp := THTTPReqResp.Create(nil);
     ConfiguraReqResp( ReqResp );
     ReqResp.URL := FURL;
     ReqResp.UseUTF8InHeader := True;
     ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv;
  try
       RETxtEnvio.Text :=  Acao.Text;
       EdWS.Text := FURL;
       ReqResp.Execute(Acao.Text, Stream);
       StrStream := TStringStream.Create('');
       StrStream.CopyFrom(Stream, 0);
       Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
       //
       StrStream.Free;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmMDFeGeraXML_0100a.WS_MDFeConsultaNaoEncerrados(UFServico: String;
  Ambiente, CodigoUF: Byte; CNPJ, Certificado: String; LaAviso1, LaAviso2: TLabel;
  RETxtEnvio: TMemo; EdWS: TEdit): String;
const
  TipoConsumo = tcwsmdfeConsultaNaoEncerrados;
var
  Texto : String;
  Acao  : TStringList ;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  sAviso, VersaoDados, ProcedimentoEnv, ProcedimentoRet: String;
begin
  Screen.Cursor := crHourGlass;
  //
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso) then Exit;
  //
  FDadosTxt := XML_ConsNaoEnc(Ambiente, CNPJ);
  FDadosTxt := StringReplace( FDadosTxt, '<' + ENCODING_UTF8_STD + '>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<' + ENCODING_UTF8 + '>', '', [rfReplaceAll] ) ;
  VersaoDados := Geral.FFT_Dot(DmMDFe_0000.QropcoesMDFeMDFeVerNaoEnc.Value, 2, siNegativo);
  //
  ProcedimentoEnv := 'MDFeConsNaoEnc';
  ProcedimentoRet := 'mdfeConsNaoEncResult';
  //

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;
  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<mdfeCabecMsg xmlns="http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</mdfeCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<mdfeDadosMsg xmlns="http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</mdfeDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

     ReqResp := THTTPReqResp.Create(nil);
     ConfiguraReqResp( ReqResp );
     ReqResp.URL := FURL;
     ReqResp.UseUTF8InHeader := True;

     ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv;
  try
    try
         RETxtEnvio.Text :=  Acao.Text;
         EdWS.Text := FURL;
         ReqResp.Execute(Acao.Text, Stream);
         StrStream := TStringStream.Create('');
         StrStream.CopyFrom(Stream, 0);
         Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
         //
         StrStream.Free;
    except on E: Exception do
      begin
       raise Exception.Create('WebService Consulta N�o Encerrados:' + sLineBreak +
                              '- Inativo ou Inoperante tente novamente.' + sLineBreak +
                              '- '+E.Message);
      end;
    end;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmMDFeGeraXML_0100a.WS_MDFeInutilizacaoMDFe(UFServico: String; Ambiente,
  CodigoUF, Ano: Byte; Id, CNPJ, Mod_, Serie, NMDFIni, NMDFFin, XJust,
  NumeroSerial: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio: TMemo;
  EdWS: TEdit; var ID_Res: String): String;
const
  TipoConsumo = tcwsmdfePediInutilizacao;
var
  sAviso: String;
var
  Texto, VersaoDados, ProcedimentoEnv, ProcedimentoRet: String;
  Acao: TStringList ;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
begin
  Geral.MB_Aviso('Parei aqui! Falta fazer. 9');
{
  Screen.Cursor := crHourGlass;
  //
  //if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso) then Exit;
  //
  FDadosTxt := XML_MDFeInutMDFe(Id, Ambiente, CodigoUF, Ano, CNPJ, Mod_, Serie, NMDFIni, NMDFFin, XJust);
  ID_Res := Id;
  //DmMDFe_0000.ReopenEmpresa(Empresa);
  DmMDFe_0000.SalvaXML(MDFE_EXT_PED_INU_XML, ID_Res, FDadosTxt, RETxtEnvio, False);

  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;
  if XXe_PF.ObtemCertificado(NumeroSerial, Cert) then
  begin
    if MDFe_PF.AssinarMSXML(FDadosTxt, Cert, FAssinTxt) then
    begin
      Acao := TStringList.Create;
      Stream := TMemoryStream.Create;

      VersaoDados := Geral.FFT_Dot(DmMDFe_0000.QropcoesMDFeMDFeVerInuNum.Value, 2, siNegativo);

      ProcedimentoEnv := 'mdfeInutilizacaoMDF';
      ProcedimentoRet := 'mdfeInutilizacaoMDFResult';

      Texto := '<?xml version="1.0" encoding="utf-8"?>';
      Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
      Texto := Texto +   '<soap12:Header>';
      Texto := Texto +     '<mdfeCabecMsg xmlns="http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv + '">';
      Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
      Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
      Texto := Texto +     '</mdfeCabecMsg>';
      Texto := Texto +   '</soap12:Header>';
      Texto := Texto +   '<soap12:Body>';
      Texto := Texto +     '<mdfeDadosMsg xmlns="http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv + '">';
      Texto := Texto + FAssinTxt;
      Texto := Texto +     '</mdfeDadosMsg>';
      Texto := Texto +   '</soap12:Body>';
      Texto := Texto +'</soap12:Envelope>';

      Acao.Text := Texto;
      Acao.SaveToStream(Stream);

      ReqResp := THTTPReqResp.Create(nil);
      ConfiguraReqResp( ReqResp );
      ReqResp.URL := FURL;
      ReqResp.UseUTF8InHeader := True;
      ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv;
      try
        RETxtEnvio.Text :=  Acao.Text;
        EdWS.Text := FURL;
        ReqResp.Execute(Acao.Text, Stream);
        StrStream := TStringStream.Create('');
        StrStream.CopyFrom(Stream, 0);
        Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
       //
       StrStream.Free;
      finally
        Acao.Free;
        Stream.Free;
      end;
    end;
  end;
}
end;

function TFmMDFeGeraXML_0100a.WS_MDFeRecepcaoLote(UFServico: String; Ambiente,
  CodigoUF: Byte; NumeroSerial: String; Lote: Integer; LaAviso1,
  LaAviso2: TLabel; RETxtEnvio: TMemo; EdWS: TEdit;
  Sincronia: TXXeIndSinc): String;
var
  sAviso: String;
  //
  Texto : String;
  Acao  : TStringList ;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  I: Integer;
  XML_STR: String;
  //
  VersaoDados, ProcedimentoEnv, ProcedimentoRet: String;
  TipoConsumo_Mdfe: TTipoConsumoWS_MDFe;
begin
  Screen.Cursor := crHourGlass;
  //
  TipoConsumo_Mdfe := tcwsmdfeRecepcao;
  //VersaoDados :=  verEnviMDFeNovo_Versao;
  //
  //if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo_Mdfe, sAviso) then Exit;
  //
  FDadosTxt := FmMDFeSteps_0100a.FXML_LoteMDFe;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  VersaoDados := Geral.FFT_Dot(DmMDFe_0000.QropcoesMDFeMDFeVerEnvLot.Value, 2, siNegativo);
  //ProcedimentoEnv := 'MDFeAutorizacao';
  //ProcedimentoRet := 'mdfeAutorizacaoLoteResult';
  ProcedimentoEnv := 'MDFeRecepcao';
  ProcedimentoRet := 'mdfeRecepcaoLoteResult';
  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<mdfeCabecMsg xmlns="http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</mdfeCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<mdfeDadosMsg xmlns="http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</mdfeDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';


  XML_STR := '';
  for i := 1 to Length(Texto) do
  begin
    if Ord(Texto[I]) > 31 then
      XML_STR := XML_STR + Texto[I]
  end;
  Texto := XML_STR;
  //
  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;
  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv;
  try
    RETxtEnvio.Text :=  Acao.Text;
    EdWS.Text := FURL;
    ReqResp.Execute(Acao.Text, Stream);

    StrStream := TStringStream.Create('');
    StrStream.CopyFrom(Stream, 0);

    Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
    //
    StrStream.Free;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmMDFeGeraXML_0100a.WS_MDFeRetRecepcao(UFServico: String; Ambiente,
  CodigoUF: Byte; Recibo: String; LaAviso1, LaAviso2: TLabel; RETxtEnvio: TMemo;
  EdWS: TEdit): String;
var
  sAviso, VersaoDados, Texto, ProcedimentoEnv, ProcedimentoRet: String;
  Acao: TStringList;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  TipoConsumo_Mdfe: TTipoConsumoWS_MDFe;
begin
  TipoConsumo_Mdfe := tcwsmdfeRetRecepcao;
  //
  Screen.Cursor := crHourGlass;
  //if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso, LaAviso1, LaAviso2) then Exit;
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo_Mdfe, sAviso) then Exit;
  //
  FDadosTxt := XML_ConsReciMDFe(Ambiente, Recibo);
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  VersaoDados := Geral.FFT_Dot(DmMDFe_0000.QropcoesMDFeMDFeVerConLot.Value, 2, siNegativo);
  //
  ProcedimentoEnv := 'MDFeRetRecepcao';
  //ProcedimentoRet abaixo!

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<mdfeCabecMsg xmlns="http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</mdfeCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<mdfeDadosMsg xmlns="http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</mdfeDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;
  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv;
  try
    RETxtEnvio.Text :=  Acao.Text;
    EdWS.Text := FURL;
    ReqResp.Execute(Acao.Text, Stream);
    StrStream := TStringStream.Create('');
    StrStream.CopyFrom(Stream, 0);
    if pos('mdfeRetRecepcaoResult', StrStream.DataString) > 0 then
      ProcedimentoRet := 'mdfeRetRecepcaoResult'
    else
      ProcedimentoRet := 'mdfeRetRecepcaoLoteResult';
    //
    Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
    //
    StrStream.Free;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmMDFeGeraXML_0100a.WS_MDFeStatusServico(UFServico: String; Ambiente,
  CodigoUF: Byte; Certificado: String; LaAviso1, LaAviso2: TLabel;
  RETxtEnvio: TMemo; EdWS: TEdit): String;
const
  TipoConsumo = tcwsmdfeStatusServico;
var
  Texto : String;
  Acao  : TStringList ;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  sAviso, VersaoDados, ProcedimentoEnv, ProcedimentoRet: String;
begin
  Screen.Cursor := crHourGlass;
  //
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso) then Exit;
  //
  FDadosTxt := XML_ConsStatServ(Ambiente, CodigoUF);
  FDadosTxt := StringReplace( FDadosTxt, '<' + ENCODING_UTF8_STD + '>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<' + ENCODING_UTF8 + '>', '', [rfReplaceAll] ) ;
  VersaoDados := Geral.FFT_Dot(DmMDFe_0000.QropcoesMDFeMDFeVerStaSer.Value, 2, siNegativo);
  //
  ProcedimentoEnv := 'MDFeStatusServico';
  ProcedimentoRet := 'mdfeStatusServicoMDFResult';
  //

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;
  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<mdfeCabecMsg xmlns="http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  //Texto := Texto +       '<versaoDados>' + verConsStatServ_Versao + '</versaoDados>';
  Texto := Texto +       '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +     '</mdfeCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<mdfeDadosMsg xmlns="http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</mdfeDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

     ReqResp := THTTPReqResp.Create(nil);
     ConfiguraReqResp( ReqResp );
     ReqResp.URL := FURL;
     ReqResp.UseUTF8InHeader := True;

     ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv;
  try
    try
         RETxtEnvio.Text :=  Acao.Text;
         EdWS.Text := FURL;
         ReqResp.Execute(Acao.Text, Stream);
         StrStream := TStringStream.Create('');
         StrStream.CopyFrom(Stream, 0);
         //Result := ??? SeparaDados(StrStream.DataString,'mdfeStatusServicoNF2Result');
         Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
         //
         StrStream.Free;
    except on E: Exception do
      begin
       raise Exception.Create('WebService Consulta Status servi�o:' + sLineBreak +
                              '- Inativo ou Inoperante tente novamente.' + sLineBreak +
                              '- '+E.Message);
      end;
    end;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmMDFeGeraXML_0100a.WS_EventoCancelamentoMDFe(UFServico: String;
  Ambiente, CodigoUF: Byte; (*NumeroSerial: String; Lote: Integer; LaAviso1,
  LaAviso2: TLabel; RETxtEnvio: TMemo; EdWS: TEdit*)XML_Evento: String;
  TipoConsumo: TTipoConsumoWS_MDFe;  RETxtEnvio: TMemo; EdWS: TEdit): String;
var
  Texto : String;
  Acao: TStringList;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  sAviso, VersaoDados, ProcedimentoEnv, ProcedimentoRet: String;
  //I: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso) then Exit;
  //
  FDadosTxt := XML_Evento;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, #13, '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, #10, '', [rfReplaceAll] ) ;
  //
  ProcedimentoEnv := 'MDFeRecepcaoEvento';
  ProcedimentoRet := 'mdfeRecepcaoEventoResult';
  //
  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  VersaoDados := Geral.FFT_Dot(DmMDFe_0000.QropcoesMDFeMDFeVerLotEve.Value, 2, siNegativo);

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +    '<soap12:Header>';
  Texto := Texto +      '<mdfeCabecMsg xmlns="http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +        '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +        '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +      '</mdfeCabecMsg>';
  Texto := Texto +    '</soap12:Header>';
  Texto := Texto +    '<soap12:Body>';
  Texto := Texto +      '<mdfeDadosMsg xmlns="http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv + '">' + FDadosTxt + '</mdfeDadosMsg>';
  Texto := Texto +    '</soap12:Body>';
  Texto := Texto +  '</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;

  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv;
  try
    try
      RETxtEnvio.Text :=  Acao.Text;
      EdWS.Text := FURL;
      ReqResp.Execute(Acao.Text, Stream);
      StrStream := TStringStream.Create('');
      StrStream.CopyFrom(Stream, 0);
      Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
      //
      StrStream.Free;
    except on E: Exception do
      begin
       raise Exception.Create('WebService Recep��o Evento:' + sLineBreak  +
       '- Inativo ou Inoperante tente novamente.' + sLineBreak  + '- ' + E.Message);
      end;
    end;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmMDFeGeraXML_0100a.WS_EventoEncerramentoMDFe(UFServico: String;
  Ambiente, CodigoUF: Byte; XML_Evento: String;
  TipoConsumo: TTipoConsumoWS_MDFe; RETxtEnvio: TMemo; EdWS: TEdit): String;
var
  Texto : String;
  Acao: TStringList;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  sAviso, VersaoDados, ProcedimentoEnv, ProcedimentoRet: String;
  //I: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso) then Exit;
  //
  FDadosTxt := XML_Evento;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, #13, '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, #10, '', [rfReplaceAll] ) ;
  //
  ProcedimentoEnv := 'MDFeRecepcaoEvento';
  ProcedimentoRet := 'mdfeRecepcaoEventoResult';
  //
  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  VersaoDados := Geral.FFT_Dot(DmMDFe_0000.QropcoesMDFeMDFeVerLotEve.Value, 2, siNegativo);

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +    '<soap12:Header>';
  Texto := Texto +      '<mdfeCabecMsg xmlns="http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +        '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +        '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +      '</mdfeCabecMsg>';
  Texto := Texto +    '</soap12:Header>';
  Texto := Texto +    '<soap12:Body>';
  Texto := Texto +      '<mdfeDadosMsg xmlns="http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv + '">' + FDadosTxt + '</mdfeDadosMsg>';
  Texto := Texto +    '</soap12:Body>';
  Texto := Texto +  '</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;

  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv;
  try
    try
      RETxtEnvio.Text :=  Acao.Text;
      EdWS.Text := FURL;
      ReqResp.Execute(Acao.Text, Stream);
      StrStream := TStringStream.Create('');
      StrStream.CopyFrom(Stream, 0);
      Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
      //
      StrStream.Free;
    except on E: Exception do
      begin
       raise Exception.Create('WebService Recep��o Evento:' + sLineBreak  +
       '- Inativo ou Inoperante tente novamente.' + sLineBreak  + '- ' + E.Message);
      end;
    end;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmMDFeGeraXML_0100a.WS_EventoIncCondutor(UFServico: String; Ambiente,
  CodigoUF: Byte; XML_Evento: String; TipoConsumo: TTipoConsumoWS_MDFe;
  RETxtEnvio: TMemo; EdWS: TEdit): String;
var
  Texto : String;
  Acao: TStringList;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  sAviso, VersaoDados, ProcedimentoEnv, ProcedimentoRet: String;
  //I: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  if not ObtemWebServer2(UFServico, Ambiente, CodigoUF, TipoConsumo, sAviso) then Exit;
  //
  FDadosTxt := XML_Evento;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, #13, '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, #10, '', [rfReplaceAll] ) ;
  //
  ProcedimentoEnv := 'MDFeRecepcaoEvento';
  ProcedimentoRet := 'mdfeRecepcaoEventoResult';
  //
  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  VersaoDados := Geral.FFT_Dot(DmMDFe_0000.QropcoesMDFeMDFeVerLotEve.Value, 2, siNegativo);

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +    '<soap12:Header>';
  Texto := Texto +      '<mdfeCabecMsg xmlns="http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv + '">';
  Texto := Texto +        '<versaoDados>' + VersaoDados + '</versaoDados>';
  Texto := Texto +        '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +      '</mdfeCabecMsg>';
  Texto := Texto +    '</soap12:Header>';
  Texto := Texto +    '<soap12:Body>';
  Texto := Texto +      '<mdfeDadosMsg xmlns="http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv + '">' + FDadosTxt + '</mdfeDadosMsg>';
  Texto := Texto +    '</soap12:Body>';
  Texto := Texto +  '</soap12:Envelope>';

  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;

  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/mdfe/wsdl/' + ProcedimentoEnv;
  try
    try
      RETxtEnvio.Text :=  Acao.Text;
      EdWS.Text := FURL;
      ReqResp.Execute(Acao.Text, Stream);
      StrStream := TStringStream.Create('');
      StrStream.CopyFrom(Stream, 0);
      Result := SeparaDados(StrStream.DataString, ProcedimentoRet);
      //
      StrStream.Free;
    except on E: Exception do
      begin
       raise Exception.Create('WebService Recep��o Evento:' + sLineBreak  +
       '- Inativo ou Inoperante tente novamente.' + sLineBreak  + '- ' + E.Message);
      end;
    end;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;

function TFmMDFeGeraXML_0100a.XML_ConsNaoEnc(TpAmb: Integer; CNPJ: String): String;
var
  XML_ConsNaoEnc_Str: IXMLTConsMDFeNaoEnc;
  Versao: String;
begin
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active    := False;
    FdocXML.FileName  := '';
    XML_ConsNaoEnc_Str := GetconsMDFeNaoEnc(FdocXML);
    FdocXML.Version   := sXML_Version;
    FdocXML.Encoding  := sXML_Encoding;

    Versao := Geral.FFT_Dot(DmMDFe_0000.QropcoesMDFeMDFeVerNaoEnc.Value, 2, siNegativo);

    //XML_ConsNaoEnc_Str.Versao      := verConsNaoEnc_Versao;
    XML_ConsNaoEnc_Str.Versao      := Versao;
    XML_ConsNaoEnc_Str.TpAmb       := FormatFloat('0', TpAmb);
    // Nao tem UF no MDFe !!!
    //XML_ConsNaoEnc_Str.        := FormatFloat('00', CUF);
    XML_ConsNaoEnc_Str.XServ       := 'CONSULTAR N�O ENCERRADOS';
    XML_ConsNaoEnc_Str.CNPJ       := Geral.SoNumero_TT(CNPJ);
    //
    Result := TipoXML(False) + XML_ConsNaoEnc_Str.XML;
  finally
    FdocXML := nil;
  end;
end;

function TFmMDFeGeraXML_0100a.XML_ConsReciMDFe(TpAmb: Integer;
  NRec: String): String;
var
  XML_ConsReciMDFe_Str: IXMLTConsReciMDFe;
  VersaoDados: String;
begin
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active     := False;
    FdocXML.FileName   := '';
    XML_ConsReciMDFe_Str := GetConsReciMDFe(FdocXML);
    FdocXML.Version    := sXML_Version;
    FdocXML.Encoding   := sXML_Encoding;
    //
    VersaoDados := Geral.FFT_Dot(DmMDFe_0000.QropcoesMDFeMDFeVerConLot.Value, 2, siNegativo);

    //XML_ConsReciMDFe_Str.Versao      := verConsReciMDFe_Versao;
    XML_ConsReciMDFe_Str.Versao      := VersaoDados;
    //XML_ConsReciMDFe_Str.Versao      := Versao;
    XML_ConsReciMDFe_Str.TpAmb       := FormatFloat('0', TpAmb);
    XML_ConsReciMDFe_Str.NRec        := NRec;
    //
    Result := TipoXML(False) + XML_ConsReciMDFe_Str.XML;
  finally
    FdocXML := nil;
  end;
end;

function TFmMDFeGeraXML_0100a.XML_ConsSitMDFe(TpAmb: Integer; ChMDFe,
  VersaoAcao: String): String;
var
  XML_ConsSitMDFe_Str: IXMLTConsSitMDFe;
begin
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active     := False;
    FdocXML.FileName   := '';
    XML_ConsSitMDFe_Str := GetConsSitMDFe(FdocXML);
    FdocXML.Version    := sXML_Version;
    FdocXML.Encoding   := sXML_Encoding;
    {
    FdocXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
    FdocXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
    FdocXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/cte/enviMDFe_v1.12.xsd';
    }
    XML_ConsSitMDFe_Str.Versao      := VersaoAcao;
    XML_ConsSitMDFe_Str.TpAmb       := FormatFloat('0', TpAmb);
    XML_ConsSitMDFe_Str.XServ       := 'CONSULTAR';
    XML_ConsSitMDFe_Str.ChMDFe       := ChMDFe;
    //
    Result := TipoXML(False) + XML_ConsSitMDFe_Str.XML;
  finally
    FdocXML := nil;
  end;
end;

function TFmMDFeGeraXML_0100a.XML_ConsStatServ(TpAmb, CUF: Integer): String;
var
  XML_ConsStatServ_Str: IXMLTConsStatServ;
  Versao: String;
begin
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active    := False;
    FdocXML.FileName  := '';
    XML_ConsStatServ_Str := GetconsStatServMDFe(FdocXML);
    FdocXML.Version   := sXML_Version;
    FdocXML.Encoding  := sXML_Encoding;

    Versao := Geral.FFT_Dot(DmMDFe_0000.QropcoesMDFeMDFeVerStaSer.Value, 2, siNegativo);

    //XML_ConsStatServ_Str.Versao      := verConsStatServ_Versao;
    XML_ConsStatServ_Str.Versao      := Versao;
    XML_ConsStatServ_Str.TpAmb       := FormatFloat('0', TpAmb);
    // Nao tem UF no MDFe !!!
    //XML_ConsStatServ_Str.        := FormatFloat('00', CUF);
    XML_ConsStatServ_Str.XServ       := 'STATUS';
    //
    Result := TipoXML(False) + XML_ConsStatServ_Str.XML;
  finally
    FdocXML := nil;
  end;
end;

function TFmMDFeGeraXML_0100a.XML_MDFeInutMDFe(var Id: String; TpAmb, CUF: Byte;
  Ano: Integer; CNPJ, Mod_, Serie, NMDFIni, NMDFFin, XJust: String): String;
{
var
  XML_MDFeInutMDFe_Str: IXMLTInutMDFe;
  xUF, xAno, Versao: String;
}
begin
  Geral.MB_Aviso('Parei aqui! Falta fazer. 16');
{
  xUF  := FormatFloat('00', CUF);
  xAno := FormatFloat('00', Ano);
  MontaID_Inutilizacao(xUF, xAno, CNPJ, Mod_, Serie, nMDFIni, nMDFFin, Id);
  FdocXML := TXMLDocument.Create(nil);
  try
    FdocXML.Active     := False;
    FdocXML.FileName   := '';
    XML_MDFeInutMDFe_Str := GetInutMDFe(FdocXML);
    FdocXML.Version    := sXML_Version;
    FdocXML.Encoding   := sXML_Encoding;

    Versao := Geral.FFT_Dot(DmMDFe_0000.QropcoesMDFeMDFeVerInuNum.Value, 2, siNegativo);

    //XML_MDFeInutMDFe_Str.Versao         := verMDFeInutMDFe_Versao;
    XML_MDFeInutMDFe_Str.Versao         := Versao;
    XML_MDFeInutMDFe_Str.InfInut.Id     := Id;
    XML_MDFeInutMDFe_Str.InfInut.TpAmb  := FormatFloat('0', TpAmb);
    XML_MDFeInutMDFe_Str.InfInut.XServ  := 'INUTILIZAR';
    XML_MDFeInutMDFe_Str.InfInut.CUF    := FormatFloat('00', CUF);
    XML_MDFeInutMDFe_Str.InfInut.Ano    := Ano;
    XML_MDFeInutMDFe_Str.InfInut.CNPJ   := CNPJ;
    XML_MDFeInutMDFe_Str.InfInut.Mod_   := Mod_;
    XML_MDFeInutMDFe_Str.InfInut.Serie  := Serie;
    XML_MDFeInutMDFe_Str.InfInut.NMDFIni := NMDFIni;
    XML_MDFeInutMDFe_Str.InfInut.NMDFFin := NMDFFin;
    XML_MDFeInutMDFe_Str.InfInut.XJust  := XJust;
    Result := TipoXML(False) + XML_MDFeInutMDFe_Str.XML;
  finally
    FdocXML := nil;
  end;
}
end;

end.
