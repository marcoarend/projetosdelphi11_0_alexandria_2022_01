unit MDFeMDFaEdit_0100a;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEditDateTimePicker,
  dmkEdit, mySQLDBTables;

type
  TFmMDFeMDFaEdit_0100a = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    Label23: TLabel;
    TPDataFat: TdmkEditDateTimePicker;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    EdCodigo: TdmkEdit;
    EdSerieMDFe: TdmkEdit;
    Label1: TLabel;
    EdNumeroMDFe: TdmkEdit;
    Label2: TLabel;
    EdEmpresa: TdmkEdit;
    Label4: TLabel;
    EdHoraFat: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FQrFrtMnfCab: TmySQLQuery;
  end;

  var
  FmMDFeMDFaEdit_0100a: TFmMDFeMDFaEdit_0100a;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule, DmkDAC_PF, ModuleMDFe_0000,
  UnMDFe_PF, ModuleFin, MDFeGeraXML_0100a;

{$R *.DFM}

procedure TFmMDFeMDFaEdit_0100a.BtOKClick(Sender: TObject);
const
  Financeiro   = 1; // Credito (2=Debito)
  Represen     = 0;
  JurosMes     = 0.00;
  GravaCampos  = ID_NO;
var
  Encerrou: String;
  FatID, FatNum, Codigo, Status, Empresa, SerieMDFe, NumeroMDFe: Integer;
  SQLType: TSQLType;
  vRec: Double;
  Data: TDateTime;
  Hora: TTime;
begin
  FatID   := VAR_FATID_6001;
  FatNum  := EdCodigo.ValueVariant;
  Empresa := EdEmpresa.ValueVariant;
  //
(*
  CtaFretPrest := DmMDFe_0000.QrOpcoesMDFeCtaFretPrest.Value;
  FaturaNum    := DModG.QrParamsEmpFaturaNum.Value;
  FaturaSeq    := DModG.QrParamsEmpFaturaSeq.Value;
  FaturaSep    := DModG.QrParamsEmpFaturaSep.Value;
  TpDuplicata  := DmMDFe_0000.QrOpcoesMDFeDupFretPrest.Value;
  TxtFaturas   := DmMDFe_0000.QrOpcoesMDFeTxtFretPrest.Value;
  //
  CartEmiss    := FQrFrtMnfCab.FieldByName('CartEmiss').AsInteger;
  CondicaoPG   := FQrFrtMnfCab.FieldByName('CondicaoPG').AsInteger;
  Cliente      := FQrFrtMnfCab.FieldByName('Tomador').AsInteger;
  //
  IDDuplicata  := EdCodigo.ValueVariant;
  //
  TipoCart     := DModFin.ObtemTipoDeCarteira(CartEmiss);
  //
*)
  NumeroMDFe    := EdNumeroMDFe.ValueVariant;
  SerieMDFe     := EdSerieMDFe.ValueVariant;
  //
  if not MDFe_PF.InsUpdFaturasMDFe(Empresa, FatID, FatNum,
  TPDataFat.Date, SerieMDFe, NumeroMDFe) then
    Exit;
  //
  Data := Trunc(TPDataFat.Date);
  Hora := EdHoraFat.ValueVariant;
  if DmMDFe_0000.InsereDadosDeFaturaNasTabelasMDFe(FatID, FatNum, Empresa, SerieMDFe,
  NumeroMDFe, Data, Hora, LaAviso1, LaAviso2) then
  begin
    SQLType        := ImgTipo.SQLType;
    Codigo         := EdCodigo.ValueVariant;
    Encerrou       := Geral.FDT(DModG.ObtemAgora(), 109);
    Status         := Integer(mdfemystatusMDFeDados);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'frtmnfcab', False, [
    'Encerrou', 'Status'], [
    'Codigo'], [
    Encerrou, Status], [
    Codigo], True) then
    begin
      if FmMDFeGeraXML_0100a.GerarArquivoXMLdoMDFe(FatID, FatNum, Empresa, LaAviso1,
      LaAviso2, GravaCampos) then
      begin
        DmMDFE_0000.StepMDFeCab(FatID, FatNum, Empresa, mdfemystatusMDFeAssinada, LaAviso1, LaAviso2);
        UnDmkDAC_PF.AbreQuery(FQrFrtMnfCab, FQrFrtMnfCab.DataBase);
        Close;
      end;
    end;
  end;
end;

procedure TFmMDFeMDFaEdit_0100a.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMDFeMDFaEdit_0100a.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMDFeMDFaEdit_0100a.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stUpd;
  //
  Agora := DModG.ObtemAgora();
  TPDataFat.Date := Trunc(Agora);
  EdHoraFat.ValueVariant := Agora;
end;

procedure TFmMDFeMDFaEdit_0100a.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
