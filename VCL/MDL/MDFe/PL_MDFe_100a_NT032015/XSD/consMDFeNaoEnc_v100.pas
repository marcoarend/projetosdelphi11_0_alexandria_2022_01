
{******************************************************************************************************************}
{                                                                                                                  }
{                                                 XML Data Binding                                                 }
{                                                                                                                  }
{         Generated on: 04/01/2016 16:06:07                                                                        }
{       Generated from: C:\_Compilers\DELPHI_XE2\VCL\MDL\MDFe\PL_MDFe_100a_NT032015\XSD\consMDFeNaoEnc_v1.00.xsd   }
{   Settings stored in: C:\_Compilers\DELPHI_XE2\VCL\MDL\MDFe\PL_MDFe_100a_NT032015\XSD\consMDFeNaoEnc_v1.00.xdb   }
{                                                                                                                  }
{******************************************************************************************************************}

unit consMDFeNaoEnc_v100;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTConsMDFeNaoEnc = interface;
  IXMLTRetConsMDFeNaoEnc = interface;
  IXMLInfMDFe = interface;
  IXMLInfMDFeList = interface;

{ IXMLTConsMDFeNaoEnc }

  IXMLTConsMDFeNaoEnc = interface(IXMLNode)
    ['{4A707E2E-BFCD-4766-89F5-0243398158C2}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_XServ: UnicodeString;
    function Get_CNPJ: UnicodeString;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_XServ(Value: UnicodeString);
    procedure Set_CNPJ(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property XServ: UnicodeString read Get_XServ write Set_XServ;
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
  end;

{ IXMLTRetConsMDFeNaoEnc }

  IXMLTRetConsMDFeNaoEnc = interface(IXMLNode)
    ['{563DD93E-9DAC-4852-A51B-485CBE10E50C}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_InfMDFe: IXMLInfMDFeList;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property VerAplic: UnicodeString read Get_VerAplic write Set_VerAplic;
    property CStat: UnicodeString read Get_CStat write Set_CStat;
    property XMotivo: UnicodeString read Get_XMotivo write Set_XMotivo;
    property CUF: UnicodeString read Get_CUF write Set_CUF;
    property InfMDFe: IXMLInfMDFeList read Get_InfMDFe;
  end;

{ IXMLInfMDFe }

  IXMLInfMDFe = interface(IXMLNode)
    ['{C7B1929E-76E0-4FF0-B807-D3213E6439D9}']
    { Property Accessors }
    function Get_ChMDFe: UnicodeString;
    function Get_NProt: UnicodeString;
    procedure Set_ChMDFe(Value: UnicodeString);
    procedure Set_NProt(Value: UnicodeString);
    { Methods & Properties }
    property ChMDFe: UnicodeString read Get_ChMDFe write Set_ChMDFe;
    property NProt: UnicodeString read Get_NProt write Set_NProt;
  end;

{ IXMLInfMDFeList }

  IXMLInfMDFeList = interface(IXMLNodeCollection)
    ['{FBFF8B50-835F-47B3-A7A5-63422D7C8716}']
    { Methods & Properties }
    function Add: IXMLInfMDFe;
    function Insert(const Index: Integer): IXMLInfMDFe;

    function Get_Item(Index: Integer): IXMLInfMDFe;
    property Items[Index: Integer]: IXMLInfMDFe read Get_Item; default;
  end;

{ Forward Decls }

  TXMLTConsMDFeNaoEnc = class;
  TXMLTRetConsMDFeNaoEnc = class;
  TXMLInfMDFe = class;
  TXMLInfMDFeList = class;

{ TXMLTConsMDFeNaoEnc }

  TXMLTConsMDFeNaoEnc = class(TXMLNode, IXMLTConsMDFeNaoEnc)
  protected
    { IXMLTConsMDFeNaoEnc }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_XServ: UnicodeString;
    function Get_CNPJ: UnicodeString;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_XServ(Value: UnicodeString);
    procedure Set_CNPJ(Value: UnicodeString);
  end;

{ TXMLTRetConsMDFeNaoEnc }

  TXMLTRetConsMDFeNaoEnc = class(TXMLNode, IXMLTRetConsMDFeNaoEnc)
  private
    FInfMDFe: IXMLInfMDFeList;
  protected
    { IXMLTRetConsMDFeNaoEnc }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_InfMDFe: IXMLInfMDFeList;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfMDFe }

  TXMLInfMDFe = class(TXMLNode, IXMLInfMDFe)
  protected
    { IXMLInfMDFe }
    function Get_ChMDFe: UnicodeString;
    function Get_NProt: UnicodeString;
    procedure Set_ChMDFe(Value: UnicodeString);
    procedure Set_NProt(Value: UnicodeString);
  end;

{ TXMLInfMDFeList }

  TXMLInfMDFeList = class(TXMLNodeCollection, IXMLInfMDFeList)
  protected
    { IXMLInfMDFeList }
    function Add: IXMLInfMDFe;
    function Insert(const Index: Integer): IXMLInfMDFe;

    function Get_Item(Index: Integer): IXMLInfMDFe;
  end;

{ Global Functions }

function GetconsMDFeNaoEnc(Doc: IXMLDocument): IXMLTConsMDFeNaoEnc;
function LoadconsMDFeNaoEnc(const FileName: string): IXMLTConsMDFeNaoEnc;
function NewconsMDFeNaoEnc: IXMLTConsMDFeNaoEnc;

const
  TargetNamespace = 'http://www.portalfiscal.inf.br/mdfe';

implementation

{ Global Functions }

function GetconsMDFeNaoEnc(Doc: IXMLDocument): IXMLTConsMDFeNaoEnc;
begin
  Result := Doc.GetDocBinding('consMDFeNaoEnc', TXMLTConsMDFeNaoEnc, TargetNamespace) as IXMLTConsMDFeNaoEnc;
end;

function LoadconsMDFeNaoEnc(const FileName: string): IXMLTConsMDFeNaoEnc;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('consMDFeNaoEnc', TXMLTConsMDFeNaoEnc, TargetNamespace) as IXMLTConsMDFeNaoEnc;
end;

function NewconsMDFeNaoEnc: IXMLTConsMDFeNaoEnc;
begin
  Result := NewXMLDocument.GetDocBinding('consMDFeNaoEnc', TXMLTConsMDFeNaoEnc, TargetNamespace) as IXMLTConsMDFeNaoEnc;
end;

{ TXMLTConsMDFeNaoEnc }

function TXMLTConsMDFeNaoEnc.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTConsMDFeNaoEnc.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTConsMDFeNaoEnc.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTConsMDFeNaoEnc.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTConsMDFeNaoEnc.Get_XServ: UnicodeString;
begin
  Result := ChildNodes['xServ'].Text;
end;

procedure TXMLTConsMDFeNaoEnc.Set_XServ(Value: UnicodeString);
begin
  ChildNodes['xServ'].NodeValue := Value;
end;

function TXMLTConsMDFeNaoEnc.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLTConsMDFeNaoEnc.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

{ TXMLTRetConsMDFeNaoEnc }

procedure TXMLTRetConsMDFeNaoEnc.AfterConstruction;
begin
  RegisterChildNode('infMDFe', TXMLInfMDFe);
  FInfMDFe := CreateCollection(TXMLInfMDFeList, IXMLInfMDFe, 'infMDFe') as IXMLInfMDFeList;
  inherited;
end;

function TXMLTRetConsMDFeNaoEnc.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTRetConsMDFeNaoEnc.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTRetConsMDFeNaoEnc.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTRetConsMDFeNaoEnc.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTRetConsMDFeNaoEnc.Get_VerAplic: UnicodeString;
begin
  Result := ChildNodes['verAplic'].Text;
end;

procedure TXMLTRetConsMDFeNaoEnc.Set_VerAplic(Value: UnicodeString);
begin
  ChildNodes['verAplic'].NodeValue := Value;
end;

function TXMLTRetConsMDFeNaoEnc.Get_CStat: UnicodeString;
begin
  Result := ChildNodes['cStat'].Text;
end;

procedure TXMLTRetConsMDFeNaoEnc.Set_CStat(Value: UnicodeString);
begin
  ChildNodes['cStat'].NodeValue := Value;
end;

function TXMLTRetConsMDFeNaoEnc.Get_XMotivo: UnicodeString;
begin
  Result := ChildNodes['xMotivo'].Text;
end;

procedure TXMLTRetConsMDFeNaoEnc.Set_XMotivo(Value: UnicodeString);
begin
  ChildNodes['xMotivo'].NodeValue := Value;
end;

function TXMLTRetConsMDFeNaoEnc.Get_CUF: UnicodeString;
begin
  Result := ChildNodes['cUF'].Text;
end;

procedure TXMLTRetConsMDFeNaoEnc.Set_CUF(Value: UnicodeString);
begin
  ChildNodes['cUF'].NodeValue := Value;
end;

function TXMLTRetConsMDFeNaoEnc.Get_InfMDFe: IXMLInfMDFeList;
begin
  Result := FInfMDFe;
end;

{ TXMLInfMDFe }

function TXMLInfMDFe.Get_ChMDFe: UnicodeString;
begin
  Result := ChildNodes['chMDFe'].Text;
end;

procedure TXMLInfMDFe.Set_ChMDFe(Value: UnicodeString);
begin
  ChildNodes['chMDFe'].NodeValue := Value;
end;

function TXMLInfMDFe.Get_NProt: UnicodeString;
begin
  Result := ChildNodes['nProt'].Text;
end;

procedure TXMLInfMDFe.Set_NProt(Value: UnicodeString);
begin
  ChildNodes['nProt'].NodeValue := Value;
end;

{ TXMLInfMDFeList }

function TXMLInfMDFeList.Add: IXMLInfMDFe;
begin
  Result := AddItem(-1) as IXMLInfMDFe;
end;

function TXMLInfMDFeList.Insert(const Index: Integer): IXMLInfMDFe;
begin
  Result := AddItem(Index) as IXMLInfMDFe;
end;

function TXMLInfMDFeList.Get_Item(Index: Integer): IXMLInfMDFe;
begin
  Result := List[Index] as IXMLInfMDFe;
end;

end.