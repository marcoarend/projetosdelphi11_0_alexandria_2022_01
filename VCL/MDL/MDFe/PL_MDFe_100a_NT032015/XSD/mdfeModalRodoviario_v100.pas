
{***********************************************************************************************************************}
{                                                                                                                       }
{                                                   XML Data Binding                                                    }
{                                                                                                                       }
{         Generated on: 01/01/2016 22:14:41                                                                             }
{       Generated from: C:\_Compilers\DELPHI_XE2\VCL\MDL\MDFe\PL_MDFe_100a_NT032015\XSD\mdfeModalRodoviario_v1.00.xsd   }
{   Settings stored in: C:\_Compilers\DELPHI_XE2\VCL\MDL\MDFe\PL_MDFe_100a_NT032015\XSD\mdfeModalRodoviario_v1.00.xdb   }
{                                                                                                                       }
{***********************************************************************************************************************}

unit mdfeModalRodoviario_v100;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLRodo = interface;
  IXMLRodo_veicTracao = interface;
  IXMLRodo_veicTracao_prop = interface;
  IXMLRodo_veicTracao_condutor = interface;
  IXMLRodo_veicTracao_condutorList = interface;
  IXMLRodo_veicReboque = interface;
  IXMLRodo_veicReboqueList = interface;
  IXMLRodo_veicReboque_prop = interface;
  IXMLRodo_valePed = interface;
  IXMLRodo_valePed_disp = interface;
  IXMLRodo_CIOTList = interface;

{ IXMLRodo }

  IXMLRodo = interface(IXMLNode)
    ['{A8AD27C3-5449-470C-8EC2-4483767234A5}']
    { Property Accessors }
    function Get_RNTRC: UnicodeString;
    function Get_CIOT: IXMLRodo_CIOTList;
    function Get_VeicTracao: IXMLRodo_veicTracao;
    function Get_VeicReboque: IXMLRodo_veicReboqueList;
    function Get_ValePed: IXMLRodo_valePed;
    function Get_CodAgPorto: UnicodeString;
    procedure Set_RNTRC(Value: UnicodeString);
    procedure Set_CodAgPorto(Value: UnicodeString);
    { Methods & Properties }
    property RNTRC: UnicodeString read Get_RNTRC write Set_RNTRC;
    property CIOT: IXMLRodo_CIOTList read Get_CIOT;
    property VeicTracao: IXMLRodo_veicTracao read Get_VeicTracao;
    property VeicReboque: IXMLRodo_veicReboqueList read Get_VeicReboque;
    property ValePed: IXMLRodo_valePed read Get_ValePed;
    property CodAgPorto: UnicodeString read Get_CodAgPorto write Set_CodAgPorto;
  end;

{ IXMLRodo_veicTracao }

  IXMLRodo_veicTracao = interface(IXMLNode)
    ['{86456CFC-25D6-4F59-828E-F631E5D7324A}']
    { Property Accessors }
    function Get_CInt: UnicodeString;
    function Get_Placa: UnicodeString;
    function Get_RENAVAM: UnicodeString;
    function Get_Tara: UnicodeString;
    function Get_CapKG: UnicodeString;
    function Get_CapM3: UnicodeString;
    function Get_Prop: IXMLRodo_veicTracao_prop;
    function Get_Condutor: IXMLRodo_veicTracao_condutorList;
    function Get_TpRod: UnicodeString;
    function Get_TpCar: UnicodeString;
    function Get_UF: UnicodeString;
    procedure Set_CInt(Value: UnicodeString);
    procedure Set_Placa(Value: UnicodeString);
    procedure Set_RENAVAM(Value: UnicodeString);
    procedure Set_Tara(Value: UnicodeString);
    procedure Set_CapKG(Value: UnicodeString);
    procedure Set_CapM3(Value: UnicodeString);
    procedure Set_TpRod(Value: UnicodeString);
    procedure Set_TpCar(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    { Methods & Properties }
    property CInt: UnicodeString read Get_CInt write Set_CInt;
    property Placa: UnicodeString read Get_Placa write Set_Placa;
    property RENAVAM: UnicodeString read Get_RENAVAM write Set_RENAVAM;
    property Tara: UnicodeString read Get_Tara write Set_Tara;
    property CapKG: UnicodeString read Get_CapKG write Set_CapKG;
    property CapM3: UnicodeString read Get_CapM3 write Set_CapM3;
    property Prop: IXMLRodo_veicTracao_prop read Get_Prop;
    property Condutor: IXMLRodo_veicTracao_condutorList read Get_Condutor;
    property TpRod: UnicodeString read Get_TpRod write Set_TpRod;
    property TpCar: UnicodeString read Get_TpCar write Set_TpCar;
    property UF: UnicodeString read Get_UF write Set_UF;
  end;

{ IXMLRodo_veicTracao_prop }

  IXMLRodo_veicTracao_prop = interface(IXMLNode)
    ['{A7C10D01-46FC-4C93-8280-B899D16FE7E5}']
    { Property Accessors }
    function Get_CPF: UnicodeString;
    function Get_CNPJ: UnicodeString;
    function Get_RNTRC: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_TpProp: UnicodeString;
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_RNTRC(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_TpProp(Value: UnicodeString);
    { Methods & Properties }
    property CPF: UnicodeString read Get_CPF write Set_CPF;
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property RNTRC: UnicodeString read Get_RNTRC write Set_RNTRC;
    property XNome: UnicodeString read Get_XNome write Set_XNome;
    property IE: UnicodeString read Get_IE write Set_IE;
    property UF: UnicodeString read Get_UF write Set_UF;
    property TpProp: UnicodeString read Get_TpProp write Set_TpProp;
  end;

{ IXMLRodo_veicTracao_condutor }

  IXMLRodo_veicTracao_condutor = interface(IXMLNode)
    ['{B9030CD5-3125-42CD-BE58-07C8A2D92E39}']
    { Property Accessors }
    function Get_XNome: UnicodeString;
    function Get_CPF: UnicodeString;
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    { Methods & Properties }
    property XNome: UnicodeString read Get_XNome write Set_XNome;
    property CPF: UnicodeString read Get_CPF write Set_CPF;
  end;

{ IXMLRodo_veicTracao_condutorList }

  IXMLRodo_veicTracao_condutorList = interface(IXMLNodeCollection)
    ['{08BA6347-105F-40DE-AF93-82ACF20106C7}']
    { Methods & Properties }
    function Add: IXMLRodo_veicTracao_condutor;
    function Insert(const Index: Integer): IXMLRodo_veicTracao_condutor;

    function Get_Item(Index: Integer): IXMLRodo_veicTracao_condutor;
    property Items[Index: Integer]: IXMLRodo_veicTracao_condutor read Get_Item; default;
  end;

{ IXMLRodo_veicReboque }

  IXMLRodo_veicReboque = interface(IXMLNode)
    ['{4A5712CC-BDDD-4094-9ABB-8C444E35835D}']
    { Property Accessors }
    function Get_CInt: UnicodeString;
    function Get_Placa: UnicodeString;
    function Get_RENAVAM: UnicodeString;
    function Get_Tara: UnicodeString;
    function Get_CapKG: UnicodeString;
    function Get_CapM3: UnicodeString;
    function Get_Prop: IXMLRodo_veicReboque_prop;
    function Get_TpCar: UnicodeString;
    function Get_UF: UnicodeString;
    procedure Set_CInt(Value: UnicodeString);
    procedure Set_Placa(Value: UnicodeString);
    procedure Set_RENAVAM(Value: UnicodeString);
    procedure Set_Tara(Value: UnicodeString);
    procedure Set_CapKG(Value: UnicodeString);
    procedure Set_CapM3(Value: UnicodeString);
    procedure Set_TpCar(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    { Methods & Properties }
    property CInt: UnicodeString read Get_CInt write Set_CInt;
    property Placa: UnicodeString read Get_Placa write Set_Placa;
    property RENAVAM: UnicodeString read Get_RENAVAM write Set_RENAVAM;
    property Tara: UnicodeString read Get_Tara write Set_Tara;
    property CapKG: UnicodeString read Get_CapKG write Set_CapKG;
    property CapM3: UnicodeString read Get_CapM3 write Set_CapM3;
    property Prop: IXMLRodo_veicReboque_prop read Get_Prop;
    property TpCar: UnicodeString read Get_TpCar write Set_TpCar;
    property UF: UnicodeString read Get_UF write Set_UF;
  end;

{ IXMLRodo_veicReboqueList }

  IXMLRodo_veicReboqueList = interface(IXMLNodeCollection)
    ['{3FE79598-B705-4ACE-9FB4-5B324835C15F}']
    { Methods & Properties }
    function Add: IXMLRodo_veicReboque;
    function Insert(const Index: Integer): IXMLRodo_veicReboque;

    function Get_Item(Index: Integer): IXMLRodo_veicReboque;
    property Items[Index: Integer]: IXMLRodo_veicReboque read Get_Item; default;
  end;

{ IXMLRodo_veicReboque_prop }

  IXMLRodo_veicReboque_prop = interface(IXMLNode)
    ['{FA3C813E-75CD-499B-B41D-BD562EAA13C9}']
    { Property Accessors }
    function Get_CPF: UnicodeString;
    function Get_CNPJ: UnicodeString;
    function Get_RNTRC: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_TpProp: UnicodeString;
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_RNTRC(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_TpProp(Value: UnicodeString);
    { Methods & Properties }
    property CPF: UnicodeString read Get_CPF write Set_CPF;
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property RNTRC: UnicodeString read Get_RNTRC write Set_RNTRC;
    property XNome: UnicodeString read Get_XNome write Set_XNome;
    property IE: UnicodeString read Get_IE write Set_IE;
    property UF: UnicodeString read Get_UF write Set_UF;
    property TpProp: UnicodeString read Get_TpProp write Set_TpProp;
  end;

{ IXMLRodo_valePed }

  IXMLRodo_valePed = interface(IXMLNodeCollection)
    ['{E4E01823-EF3D-4A9D-814E-020DB4B5014D}']
    { Property Accessors }
    function Get_Disp(Index: Integer): IXMLRodo_valePed_disp;
    { Methods & Properties }
    function Add: IXMLRodo_valePed_disp;
    function Insert(const Index: Integer): IXMLRodo_valePed_disp;
    property Disp[Index: Integer]: IXMLRodo_valePed_disp read Get_Disp; default;
  end;

{ IXMLRodo_valePed_disp }

  IXMLRodo_valePed_disp = interface(IXMLNode)
    ['{B019B873-B5A2-49F7-B47A-9EFB467B3437}']
    { Property Accessors }
    function Get_CNPJForn: UnicodeString;
    function Get_CNPJPg: UnicodeString;
    function Get_NCompra: UnicodeString;
    procedure Set_CNPJForn(Value: UnicodeString);
    procedure Set_CNPJPg(Value: UnicodeString);
    procedure Set_NCompra(Value: UnicodeString);
    { Methods & Properties }
    property CNPJForn: UnicodeString read Get_CNPJForn write Set_CNPJForn;
    property CNPJPg: UnicodeString read Get_CNPJPg write Set_CNPJPg;
    property NCompra: UnicodeString read Get_NCompra write Set_NCompra;
  end;

{ IXMLRodo_CIOTList }

  IXMLRodo_CIOTList = interface(IXMLNodeCollection)
    ['{A38D0C1D-8C1A-4AE3-929B-802E1E3DC34F}']
    { Methods & Properties }
    function Add(const Value: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const Value: UnicodeString): IXMLNode;

    function Get_Item(Index: Integer): UnicodeString;
    property Items[Index: Integer]: UnicodeString read Get_Item; default;
  end;

{ Forward Decls }

  TXMLRodo = class;
  TXMLRodo_veicTracao = class;
  TXMLRodo_veicTracao_prop = class;
  TXMLRodo_veicTracao_condutor = class;
  TXMLRodo_veicTracao_condutorList = class;
  TXMLRodo_veicReboque = class;
  TXMLRodo_veicReboqueList = class;
  TXMLRodo_veicReboque_prop = class;
  TXMLRodo_valePed = class;
  TXMLRodo_valePed_disp = class;
  TXMLRodo_CIOTList = class;

{ TXMLRodo }

  TXMLRodo = class(TXMLNode, IXMLRodo)
  private
    FCIOT: IXMLRodo_CIOTList;
    FVeicReboque: IXMLRodo_veicReboqueList;
  protected
    { IXMLRodo }
    function Get_RNTRC: UnicodeString;
    function Get_CIOT: IXMLRodo_CIOTList;
    function Get_VeicTracao: IXMLRodo_veicTracao;
    function Get_VeicReboque: IXMLRodo_veicReboqueList;
    function Get_ValePed: IXMLRodo_valePed;
    function Get_CodAgPorto: UnicodeString;
    procedure Set_RNTRC(Value: UnicodeString);
    procedure Set_CodAgPorto(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRodo_veicTracao }

  TXMLRodo_veicTracao = class(TXMLNode, IXMLRodo_veicTracao)
  private
    FCondutor: IXMLRodo_veicTracao_condutorList;
  protected
    { IXMLRodo_veicTracao }
    function Get_CInt: UnicodeString;
    function Get_Placa: UnicodeString;
    function Get_RENAVAM: UnicodeString;
    function Get_Tara: UnicodeString;
    function Get_CapKG: UnicodeString;
    function Get_CapM3: UnicodeString;
    function Get_Prop: IXMLRodo_veicTracao_prop;
    function Get_Condutor: IXMLRodo_veicTracao_condutorList;
    function Get_TpRod: UnicodeString;
    function Get_TpCar: UnicodeString;
    function Get_UF: UnicodeString;
    procedure Set_CInt(Value: UnicodeString);
    procedure Set_Placa(Value: UnicodeString);
    procedure Set_RENAVAM(Value: UnicodeString);
    procedure Set_Tara(Value: UnicodeString);
    procedure Set_CapKG(Value: UnicodeString);
    procedure Set_CapM3(Value: UnicodeString);
    procedure Set_TpRod(Value: UnicodeString);
    procedure Set_TpCar(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRodo_veicTracao_prop }

  TXMLRodo_veicTracao_prop = class(TXMLNode, IXMLRodo_veicTracao_prop)
  protected
    { IXMLRodo_veicTracao_prop }
    function Get_CPF: UnicodeString;
    function Get_CNPJ: UnicodeString;
    function Get_RNTRC: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_TpProp: UnicodeString;
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_RNTRC(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_TpProp(Value: UnicodeString);
  end;

{ TXMLRodo_veicTracao_condutor }

  TXMLRodo_veicTracao_condutor = class(TXMLNode, IXMLRodo_veicTracao_condutor)
  protected
    { IXMLRodo_veicTracao_condutor }
    function Get_XNome: UnicodeString;
    function Get_CPF: UnicodeString;
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
  end;

{ TXMLRodo_veicTracao_condutorList }

  TXMLRodo_veicTracao_condutorList = class(TXMLNodeCollection, IXMLRodo_veicTracao_condutorList)
  protected
    { IXMLRodo_veicTracao_condutorList }
    function Add: IXMLRodo_veicTracao_condutor;
    function Insert(const Index: Integer): IXMLRodo_veicTracao_condutor;

    function Get_Item(Index: Integer): IXMLRodo_veicTracao_condutor;
  end;

{ TXMLRodo_veicReboque }

  TXMLRodo_veicReboque = class(TXMLNode, IXMLRodo_veicReboque)
  protected
    { IXMLRodo_veicReboque }
    function Get_CInt: UnicodeString;
    function Get_Placa: UnicodeString;
    function Get_RENAVAM: UnicodeString;
    function Get_Tara: UnicodeString;
    function Get_CapKG: UnicodeString;
    function Get_CapM3: UnicodeString;
    function Get_Prop: IXMLRodo_veicReboque_prop;
    function Get_TpCar: UnicodeString;
    function Get_UF: UnicodeString;
    procedure Set_CInt(Value: UnicodeString);
    procedure Set_Placa(Value: UnicodeString);
    procedure Set_RENAVAM(Value: UnicodeString);
    procedure Set_Tara(Value: UnicodeString);
    procedure Set_CapKG(Value: UnicodeString);
    procedure Set_CapM3(Value: UnicodeString);
    procedure Set_TpCar(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRodo_veicReboqueList }

  TXMLRodo_veicReboqueList = class(TXMLNodeCollection, IXMLRodo_veicReboqueList)
  protected
    { IXMLRodo_veicReboqueList }
    function Add: IXMLRodo_veicReboque;
    function Insert(const Index: Integer): IXMLRodo_veicReboque;

    function Get_Item(Index: Integer): IXMLRodo_veicReboque;
  end;

{ TXMLRodo_veicReboque_prop }

  TXMLRodo_veicReboque_prop = class(TXMLNode, IXMLRodo_veicReboque_prop)
  protected
    { IXMLRodo_veicReboque_prop }
    function Get_CPF: UnicodeString;
    function Get_CNPJ: UnicodeString;
    function Get_RNTRC: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_TpProp: UnicodeString;
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_RNTRC(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_TpProp(Value: UnicodeString);
  end;

{ TXMLRodo_valePed }

  TXMLRodo_valePed = class(TXMLNodeCollection, IXMLRodo_valePed)
  protected
    { IXMLRodo_valePed }
    function Get_Disp(Index: Integer): IXMLRodo_valePed_disp;
    function Add: IXMLRodo_valePed_disp;
    function Insert(const Index: Integer): IXMLRodo_valePed_disp;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRodo_valePed_disp }

  TXMLRodo_valePed_disp = class(TXMLNode, IXMLRodo_valePed_disp)
  protected
    { IXMLRodo_valePed_disp }
    function Get_CNPJForn: UnicodeString;
    function Get_CNPJPg: UnicodeString;
    function Get_NCompra: UnicodeString;
    procedure Set_CNPJForn(Value: UnicodeString);
    procedure Set_CNPJPg(Value: UnicodeString);
    procedure Set_NCompra(Value: UnicodeString);
  end;

{ TXMLRodo_CIOTList }

  TXMLRodo_CIOTList = class(TXMLNodeCollection, IXMLRodo_CIOTList)
  protected
    { IXMLRodo_CIOTList }
    function Add(const Value: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const Value: UnicodeString): IXMLNode;

    function Get_Item(Index: Integer): UnicodeString;
  end;

{ Global Functions }

function Getrodo(Doc: IXMLDocument): IXMLRodo;
function Loadrodo(const FileName: string): IXMLRodo;
function Newrodo: IXMLRodo;

const
  TargetNamespace = 'http://www.portalfiscal.inf.br/mdfe';

implementation

{ Global Functions }

function Getrodo(Doc: IXMLDocument): IXMLRodo;
begin
  Result := Doc.GetDocBinding('rodo', TXMLRodo, TargetNamespace) as IXMLRodo;
end;

function Loadrodo(const FileName: string): IXMLRodo;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('rodo', TXMLRodo, TargetNamespace) as IXMLRodo;
end;

function Newrodo: IXMLRodo;
begin
  Result := NewXMLDocument.GetDocBinding('rodo', TXMLRodo, TargetNamespace) as IXMLRodo;
end;

{ TXMLRodo }

procedure TXMLRodo.AfterConstruction;
begin
  RegisterChildNode('veicTracao', TXMLRodo_veicTracao);
  RegisterChildNode('veicReboque', TXMLRodo_veicReboque);
  RegisterChildNode('valePed', TXMLRodo_valePed);
  FCIOT := CreateCollection(TXMLRodo_CIOTList, IXMLNode, 'CIOT') as IXMLRodo_CIOTList;
  FVeicReboque := CreateCollection(TXMLRodo_veicReboqueList, IXMLRodo_veicReboque, 'veicReboque') as IXMLRodo_veicReboqueList;
  inherited;
end;

function TXMLRodo.Get_RNTRC: UnicodeString;
begin
  Result := ChildNodes['RNTRC'].Text;
end;

procedure TXMLRodo.Set_RNTRC(Value: UnicodeString);
begin
  ChildNodes['RNTRC'].NodeValue := Value;
end;

function TXMLRodo.Get_CIOT: IXMLRodo_CIOTList;
begin
  Result := FCIOT;
end;

function TXMLRodo.Get_VeicTracao: IXMLRodo_veicTracao;
begin
  Result := ChildNodes['veicTracao'] as IXMLRodo_veicTracao;
end;

function TXMLRodo.Get_VeicReboque: IXMLRodo_veicReboqueList;
begin
  Result := FVeicReboque;
end;

function TXMLRodo.Get_ValePed: IXMLRodo_valePed;
begin
  Result := ChildNodes['valePed'] as IXMLRodo_valePed;
end;

function TXMLRodo.Get_CodAgPorto: UnicodeString;
begin
  Result := ChildNodes['codAgPorto'].Text;
end;

procedure TXMLRodo.Set_CodAgPorto(Value: UnicodeString);
begin
  ChildNodes['codAgPorto'].NodeValue := Value;
end;

{ TXMLRodo_veicTracao }

procedure TXMLRodo_veicTracao.AfterConstruction;
begin
  RegisterChildNode('prop', TXMLRodo_veicTracao_prop);
  RegisterChildNode('condutor', TXMLRodo_veicTracao_condutor);
  FCondutor := CreateCollection(TXMLRodo_veicTracao_condutorList, IXMLRodo_veicTracao_condutor, 'condutor') as IXMLRodo_veicTracao_condutorList;
  inherited;
end;

function TXMLRodo_veicTracao.Get_CInt: UnicodeString;
begin
  Result := ChildNodes['cInt'].Text;
end;

procedure TXMLRodo_veicTracao.Set_CInt(Value: UnicodeString);
begin
  ChildNodes['cInt'].NodeValue := Value;
end;

function TXMLRodo_veicTracao.Get_Placa: UnicodeString;
begin
  Result := ChildNodes['placa'].Text;
end;

procedure TXMLRodo_veicTracao.Set_Placa(Value: UnicodeString);
begin
  ChildNodes['placa'].NodeValue := Value;
end;

function TXMLRodo_veicTracao.Get_RENAVAM: UnicodeString;
begin
  Result := ChildNodes['RENAVAM'].Text;
end;

procedure TXMLRodo_veicTracao.Set_RENAVAM(Value: UnicodeString);
begin
  ChildNodes['RENAVAM'].NodeValue := Value;
end;

function TXMLRodo_veicTracao.Get_Tara: UnicodeString;
begin
  Result := ChildNodes['tara'].Text;
end;

procedure TXMLRodo_veicTracao.Set_Tara(Value: UnicodeString);
begin
  ChildNodes['tara'].NodeValue := Value;
end;

function TXMLRodo_veicTracao.Get_CapKG: UnicodeString;
begin
  Result := ChildNodes['capKG'].Text;
end;

procedure TXMLRodo_veicTracao.Set_CapKG(Value: UnicodeString);
begin
  ChildNodes['capKG'].NodeValue := Value;
end;

function TXMLRodo_veicTracao.Get_CapM3: UnicodeString;
begin
  Result := ChildNodes['capM3'].Text;
end;

procedure TXMLRodo_veicTracao.Set_CapM3(Value: UnicodeString);
begin
  ChildNodes['capM3'].NodeValue := Value;
end;

function TXMLRodo_veicTracao.Get_Prop: IXMLRodo_veicTracao_prop;
begin
  Result := ChildNodes['prop'] as IXMLRodo_veicTracao_prop;
end;

function TXMLRodo_veicTracao.Get_Condutor: IXMLRodo_veicTracao_condutorList;
begin
  Result := FCondutor;
end;

function TXMLRodo_veicTracao.Get_TpRod: UnicodeString;
begin
  Result := ChildNodes['tpRod'].Text;
end;

procedure TXMLRodo_veicTracao.Set_TpRod(Value: UnicodeString);
begin
  ChildNodes['tpRod'].NodeValue := Value;
end;

function TXMLRodo_veicTracao.Get_TpCar: UnicodeString;
begin
  Result := ChildNodes['tpCar'].Text;
end;

procedure TXMLRodo_veicTracao.Set_TpCar(Value: UnicodeString);
begin
  ChildNodes['tpCar'].NodeValue := Value;
end;

function TXMLRodo_veicTracao.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLRodo_veicTracao.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

{ TXMLRodo_veicTracao_prop }

function TXMLRodo_veicTracao_prop.Get_CPF: UnicodeString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLRodo_veicTracao_prop.Set_CPF(Value: UnicodeString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

function TXMLRodo_veicTracao_prop.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLRodo_veicTracao_prop.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLRodo_veicTracao_prop.Get_RNTRC: UnicodeString;
begin
  Result := ChildNodes['RNTRC'].Text;
end;

procedure TXMLRodo_veicTracao_prop.Set_RNTRC(Value: UnicodeString);
begin
  ChildNodes['RNTRC'].NodeValue := Value;
end;

function TXMLRodo_veicTracao_prop.Get_XNome: UnicodeString;
begin
  Result := ChildNodes['xNome'].Text;
end;

procedure TXMLRodo_veicTracao_prop.Set_XNome(Value: UnicodeString);
begin
  ChildNodes['xNome'].NodeValue := Value;
end;

function TXMLRodo_veicTracao_prop.Get_IE: UnicodeString;
begin
  Result := ChildNodes['IE'].Text;
end;

procedure TXMLRodo_veicTracao_prop.Set_IE(Value: UnicodeString);
begin
  ChildNodes['IE'].NodeValue := Value;
end;

function TXMLRodo_veicTracao_prop.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLRodo_veicTracao_prop.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

function TXMLRodo_veicTracao_prop.Get_TpProp: UnicodeString;
begin
  Result := ChildNodes['tpProp'].Text;
end;

procedure TXMLRodo_veicTracao_prop.Set_TpProp(Value: UnicodeString);
begin
  ChildNodes['tpProp'].NodeValue := Value;
end;

{ TXMLRodo_veicTracao_condutor }

function TXMLRodo_veicTracao_condutor.Get_XNome: UnicodeString;
begin
  Result := ChildNodes['xNome'].Text;
end;

procedure TXMLRodo_veicTracao_condutor.Set_XNome(Value: UnicodeString);
begin
  ChildNodes['xNome'].NodeValue := Value;
end;

function TXMLRodo_veicTracao_condutor.Get_CPF: UnicodeString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLRodo_veicTracao_condutor.Set_CPF(Value: UnicodeString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

{ TXMLRodo_veicTracao_condutorList }

function TXMLRodo_veicTracao_condutorList.Add: IXMLRodo_veicTracao_condutor;
begin
  Result := AddItem(-1) as IXMLRodo_veicTracao_condutor;
end;

function TXMLRodo_veicTracao_condutorList.Insert(const Index: Integer): IXMLRodo_veicTracao_condutor;
begin
  Result := AddItem(Index) as IXMLRodo_veicTracao_condutor;
end;

function TXMLRodo_veicTracao_condutorList.Get_Item(Index: Integer): IXMLRodo_veicTracao_condutor;
begin
  Result := List[Index] as IXMLRodo_veicTracao_condutor;
end;

{ TXMLRodo_veicReboque }

procedure TXMLRodo_veicReboque.AfterConstruction;
begin
  RegisterChildNode('prop', TXMLRodo_veicReboque_prop);
  inherited;
end;

function TXMLRodo_veicReboque.Get_CInt: UnicodeString;
begin
  Result := ChildNodes['cInt'].Text;
end;

procedure TXMLRodo_veicReboque.Set_CInt(Value: UnicodeString);
begin
  ChildNodes['cInt'].NodeValue := Value;
end;

function TXMLRodo_veicReboque.Get_Placa: UnicodeString;
begin
  Result := ChildNodes['placa'].Text;
end;

procedure TXMLRodo_veicReboque.Set_Placa(Value: UnicodeString);
begin
  ChildNodes['placa'].NodeValue := Value;
end;

function TXMLRodo_veicReboque.Get_RENAVAM: UnicodeString;
begin
  Result := ChildNodes['RENAVAM'].Text;
end;

procedure TXMLRodo_veicReboque.Set_RENAVAM(Value: UnicodeString);
begin
  ChildNodes['RENAVAM'].NodeValue := Value;
end;

function TXMLRodo_veicReboque.Get_Tara: UnicodeString;
begin
  Result := ChildNodes['tara'].Text;
end;

procedure TXMLRodo_veicReboque.Set_Tara(Value: UnicodeString);
begin
  ChildNodes['tara'].NodeValue := Value;
end;

function TXMLRodo_veicReboque.Get_CapKG: UnicodeString;
begin
  Result := ChildNodes['capKG'].Text;
end;

procedure TXMLRodo_veicReboque.Set_CapKG(Value: UnicodeString);
begin
  ChildNodes['capKG'].NodeValue := Value;
end;

function TXMLRodo_veicReboque.Get_CapM3: UnicodeString;
begin
  Result := ChildNodes['capM3'].Text;
end;

procedure TXMLRodo_veicReboque.Set_CapM3(Value: UnicodeString);
begin
  ChildNodes['capM3'].NodeValue := Value;
end;

function TXMLRodo_veicReboque.Get_Prop: IXMLRodo_veicReboque_prop;
begin
  Result := ChildNodes['prop'] as IXMLRodo_veicReboque_prop;
end;

function TXMLRodo_veicReboque.Get_TpCar: UnicodeString;
begin
  Result := ChildNodes['tpCar'].Text;
end;

procedure TXMLRodo_veicReboque.Set_TpCar(Value: UnicodeString);
begin
  ChildNodes['tpCar'].NodeValue := Value;
end;

function TXMLRodo_veicReboque.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLRodo_veicReboque.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

{ TXMLRodo_veicReboqueList }

function TXMLRodo_veicReboqueList.Add: IXMLRodo_veicReboque;
begin
  Result := AddItem(-1) as IXMLRodo_veicReboque;
end;

function TXMLRodo_veicReboqueList.Insert(const Index: Integer): IXMLRodo_veicReboque;
begin
  Result := AddItem(Index) as IXMLRodo_veicReboque;
end;

function TXMLRodo_veicReboqueList.Get_Item(Index: Integer): IXMLRodo_veicReboque;
begin
  Result := List[Index] as IXMLRodo_veicReboque;
end;

{ TXMLRodo_veicReboque_prop }

function TXMLRodo_veicReboque_prop.Get_CPF: UnicodeString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLRodo_veicReboque_prop.Set_CPF(Value: UnicodeString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

function TXMLRodo_veicReboque_prop.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLRodo_veicReboque_prop.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLRodo_veicReboque_prop.Get_RNTRC: UnicodeString;
begin
  Result := ChildNodes['RNTRC'].Text;
end;

procedure TXMLRodo_veicReboque_prop.Set_RNTRC(Value: UnicodeString);
begin
  ChildNodes['RNTRC'].NodeValue := Value;
end;

function TXMLRodo_veicReboque_prop.Get_XNome: UnicodeString;
begin
  Result := ChildNodes['xNome'].Text;
end;

procedure TXMLRodo_veicReboque_prop.Set_XNome(Value: UnicodeString);
begin
  ChildNodes['xNome'].NodeValue := Value;
end;

function TXMLRodo_veicReboque_prop.Get_IE: UnicodeString;
begin
  Result := ChildNodes['IE'].Text;
end;

procedure TXMLRodo_veicReboque_prop.Set_IE(Value: UnicodeString);
begin
  ChildNodes['IE'].NodeValue := Value;
end;

function TXMLRodo_veicReboque_prop.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLRodo_veicReboque_prop.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

function TXMLRodo_veicReboque_prop.Get_TpProp: UnicodeString;
begin
  Result := ChildNodes['tpProp'].Text;
end;

procedure TXMLRodo_veicReboque_prop.Set_TpProp(Value: UnicodeString);
begin
  ChildNodes['tpProp'].NodeValue := Value;
end;

{ TXMLRodo_valePed }

procedure TXMLRodo_valePed.AfterConstruction;
begin
  RegisterChildNode('disp', TXMLRodo_valePed_disp);
  ItemTag := 'disp';
  ItemInterface := IXMLRodo_valePed_disp;
  inherited;
end;

function TXMLRodo_valePed.Get_Disp(Index: Integer): IXMLRodo_valePed_disp;
begin
  Result := List[Index] as IXMLRodo_valePed_disp;
end;

function TXMLRodo_valePed.Add: IXMLRodo_valePed_disp;
begin
  Result := AddItem(-1) as IXMLRodo_valePed_disp;
end;

function TXMLRodo_valePed.Insert(const Index: Integer): IXMLRodo_valePed_disp;
begin
  Result := AddItem(Index) as IXMLRodo_valePed_disp;
end;

{ TXMLRodo_valePed_disp }

function TXMLRodo_valePed_disp.Get_CNPJForn: UnicodeString;
begin
  Result := ChildNodes['CNPJForn'].Text;
end;

procedure TXMLRodo_valePed_disp.Set_CNPJForn(Value: UnicodeString);
begin
  ChildNodes['CNPJForn'].NodeValue := Value;
end;

function TXMLRodo_valePed_disp.Get_CNPJPg: UnicodeString;
begin
  Result := ChildNodes['CNPJPg'].Text;
end;

procedure TXMLRodo_valePed_disp.Set_CNPJPg(Value: UnicodeString);
begin
  ChildNodes['CNPJPg'].NodeValue := Value;
end;

function TXMLRodo_valePed_disp.Get_NCompra: UnicodeString;
begin
  Result := ChildNodes['nCompra'].Text;
end;

procedure TXMLRodo_valePed_disp.Set_NCompra(Value: UnicodeString);
begin
  ChildNodes['nCompra'].NodeValue := Value;
end;

{ TXMLRodo_CIOTList }

function TXMLRodo_CIOTList.Add(const Value: UnicodeString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := Value;
end;

function TXMLRodo_CIOTList.Insert(const Index: Integer; const Value: UnicodeString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := Value;
end;

function TXMLRodo_CIOTList.Get_Item(Index: Integer): UnicodeString;
begin
  Result := List[Index].NodeValue;
end;

end.