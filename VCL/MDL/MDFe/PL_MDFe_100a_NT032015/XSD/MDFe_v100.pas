
{********************************************************************************************************}
{                                                                                                        }
{                                            XML Data Binding                                            }
{                                                                                                        }
{         Generated on: 01/01/2016 18:35:10                                                              }
{       Generated from: C:\_Compilers\DELPHI_XE2\VCL\MDL\MDFe\PL_MDFe_100a_NT032015\XSD\mdfe_v1.00.xsd   }
{   Settings stored in: C:\_Compilers\DELPHI_XE2\VCL\MDL\MDFe\PL_MDFe_100a_NT032015\XSD\mdfe_v1.00.xdb   }
{                                                                                                        }
{********************************************************************************************************}

unit mdfe_v100;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTMDFe = interface;
  IXMLInfMDFe = interface;
  IXMLIde = interface;
  IXMLInfMunCarrega = interface;
  IXMLInfMunCarregaList = interface;
  IXMLInfPercurso = interface;
  IXMLInfPercursoList = interface;
  IXMLEmit = interface;
  IXMLTEndeEmi = interface;
  IXMLInfModal = interface;
  IXMLInfDoc = interface;
  IXMLInfMunDescarga = interface;
  IXMLInfCTe = interface;
  IXMLInfCTeList = interface;
  IXMLTUnidadeTransp = interface;
  IXMLTUnidadeTranspList = interface;
  IXMLLacUnidTransp = interface;
  IXMLLacUnidTranspList = interface;
  IXMLTUnidCarga = interface;
  IXMLTUnidCargaList = interface;
  IXMLLacUnidCarga = interface;
  IXMLLacUnidCargaList = interface;
  IXMLInfNFe = interface;
  IXMLInfNFeList = interface;
  IXMLInfMDFeTransp = interface;
  IXMLInfMDFeTranspList = interface;
  IXMLTot = interface;
  IXMLLacres = interface;
  IXMLLacresList = interface;
  IXMLAutXML = interface;
  IXMLAutXMLList = interface;
  IXMLInfAdic = interface;
  IXMLSignatureType_ds = interface;
  IXMLSignedInfoType_ds = interface;
  IXMLCanonicalizationMethod_ds = interface;
  IXMLSignatureMethod_ds = interface;
  IXMLReferenceType_ds = interface;
  IXMLTransformsType_ds = interface;
  IXMLTransformType_ds = interface;
  IXMLDigestMethod_ds = interface;
  IXMLSignatureValueType_ds = interface;
  IXMLKeyInfoType_ds = interface;
  IXMLX509DataType_ds = interface;
  IXMLTEnviMDFe = interface;
  IXMLTRetEnviMDFe = interface;
  IXMLInfRec = interface;
  IXMLTEndereco = interface;
  IXMLTEndernac = interface;
  IXMLTEnderFer = interface;
  IXMLTEndOrg = interface;
  IXMLTLocal = interface;
  IXMLTEndReEnt = interface;
  IXMLTNFeNF = interface;
  IXMLInfNF = interface;
  IXMLEmi = interface;
  IXMLDest = interface;

{ IXMLTMDFe }

  IXMLTMDFe = interface(IXMLNode)
    ['{8F63D807-D16B-4EEF-A4B9-087EC0B923D9}']
    { Property Accessors }
    function Get_InfMDFe: IXMLInfMDFe;
    function Get_Signature: IXMLSignatureType_ds;
    { Methods & Properties }
    property InfMDFe: IXMLInfMDFe read Get_InfMDFe;
    property Signature: IXMLSignatureType_ds read Get_Signature;
  end;

{ IXMLInfMDFe }

  IXMLInfMDFe = interface(IXMLNode)
    ['{C45139C1-B1A8-4E5D-A742-1DF01AA7DBC7}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_Id: UnicodeString;
    function Get_Ide: IXMLIde;
    function Get_Emit: IXMLEmit;
    function Get_InfModal: IXMLInfModal;
    function Get_InfDoc: IXMLInfDoc;
    function Get_Tot: IXMLTot;
    function Get_Lacres: IXMLLacresList;
    function Get_AutXML: IXMLAutXMLList;
    function Get_InfAdic: IXMLInfAdic;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property Id: UnicodeString read Get_Id write Set_Id;
    property Ide: IXMLIde read Get_Ide;
    property Emit: IXMLEmit read Get_Emit;
    property InfModal: IXMLInfModal read Get_InfModal;
    property InfDoc: IXMLInfDoc read Get_InfDoc;
    property Tot: IXMLTot read Get_Tot;
    property Lacres: IXMLLacresList read Get_Lacres;
    property AutXML: IXMLAutXMLList read Get_AutXML;
    property InfAdic: IXMLInfAdic read Get_InfAdic;
  end;

{ IXMLIde }

  IXMLIde = interface(IXMLNode)
    ['{1703BA54-DEB1-44E0-966D-7D9A2D0364EC}']
    { Property Accessors }
    function Get_CUF: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_TpEmit: UnicodeString;
    function Get_Mod_: UnicodeString;
    function Get_Serie: UnicodeString;
    function Get_NMDF: UnicodeString;
    function Get_CMDF: UnicodeString;
    function Get_CDV: UnicodeString;
    function Get_Modal: UnicodeString;
    function Get_DhEmi: UnicodeString;
    function Get_TpEmis: UnicodeString;
    function Get_ProcEmi: UnicodeString;
    function Get_VerProc: UnicodeString;
    function Get_UFIni: UnicodeString;
    function Get_UFFim: UnicodeString;
    function Get_InfMunCarrega: IXMLInfMunCarregaList;
    function Get_InfPercurso: IXMLInfPercursoList;
    function Get_DhIniViagem: UnicodeString;
    procedure Set_CUF(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_TpEmit(Value: UnicodeString);
    procedure Set_Mod_(Value: UnicodeString);
    procedure Set_Serie(Value: UnicodeString);
    procedure Set_NMDF(Value: UnicodeString);
    procedure Set_CMDF(Value: UnicodeString);
    procedure Set_CDV(Value: UnicodeString);
    procedure Set_Modal(Value: UnicodeString);
    procedure Set_DhEmi(Value: UnicodeString);
    procedure Set_TpEmis(Value: UnicodeString);
    procedure Set_ProcEmi(Value: UnicodeString);
    procedure Set_VerProc(Value: UnicodeString);
    procedure Set_UFIni(Value: UnicodeString);
    procedure Set_UFFim(Value: UnicodeString);
    procedure Set_DhIniViagem(Value: UnicodeString);
    { Methods & Properties }
    property CUF: UnicodeString read Get_CUF write Set_CUF;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property TpEmit: UnicodeString read Get_TpEmit write Set_TpEmit;
    property Mod_: UnicodeString read Get_Mod_ write Set_Mod_;
    property Serie: UnicodeString read Get_Serie write Set_Serie;
    property NMDF: UnicodeString read Get_NMDF write Set_NMDF;
    property CMDF: UnicodeString read Get_CMDF write Set_CMDF;
    property CDV: UnicodeString read Get_CDV write Set_CDV;
    property Modal: UnicodeString read Get_Modal write Set_Modal;
    property DhEmi: UnicodeString read Get_DhEmi write Set_DhEmi;
    property TpEmis: UnicodeString read Get_TpEmis write Set_TpEmis;
    property ProcEmi: UnicodeString read Get_ProcEmi write Set_ProcEmi;
    property VerProc: UnicodeString read Get_VerProc write Set_VerProc;
    property UFIni: UnicodeString read Get_UFIni write Set_UFIni;
    property UFFim: UnicodeString read Get_UFFim write Set_UFFim;
    property InfMunCarrega: IXMLInfMunCarregaList read Get_InfMunCarrega;
    property InfPercurso: IXMLInfPercursoList read Get_InfPercurso;
    property DhIniViagem: UnicodeString read Get_DhIniViagem write Set_DhIniViagem;
  end;

{ IXMLInfMunCarrega }

  IXMLInfMunCarrega = interface(IXMLNode)
    ['{23633683-4D62-40C1-A1D0-89274AC4CAD6}']
    { Property Accessors }
    function Get_CMunCarrega: UnicodeString;
    function Get_XMunCarrega: UnicodeString;
    procedure Set_CMunCarrega(Value: UnicodeString);
    procedure Set_XMunCarrega(Value: UnicodeString);
    { Methods & Properties }
    property CMunCarrega: UnicodeString read Get_CMunCarrega write Set_CMunCarrega;
    property XMunCarrega: UnicodeString read Get_XMunCarrega write Set_XMunCarrega;
  end;

{ IXMLInfMunCarregaList }

  IXMLInfMunCarregaList = interface(IXMLNodeCollection)
    ['{E4BE205C-6C26-4F81-9AC1-5A0F5836E3A9}']
    { Methods & Properties }
    function Add: IXMLInfMunCarrega;
    function Insert(const Index: Integer): IXMLInfMunCarrega;

    function Get_Item(Index: Integer): IXMLInfMunCarrega;
    property Items[Index: Integer]: IXMLInfMunCarrega read Get_Item; default;
  end;

{ IXMLInfPercurso }

  IXMLInfPercurso = interface(IXMLNode)
    ['{2C344C56-A8A3-4F22-A409-0515CC70F401}']
    { Property Accessors }
    function Get_UFPer: UnicodeString;
    procedure Set_UFPer(Value: UnicodeString);
    { Methods & Properties }
    property UFPer: UnicodeString read Get_UFPer write Set_UFPer;
  end;

{ IXMLInfPercursoList }

  IXMLInfPercursoList = interface(IXMLNodeCollection)
    ['{79AD8F7D-282C-4D52-84FA-B316EF80AC09}']
    { Methods & Properties }
    function Add: IXMLInfPercurso;
    function Insert(const Index: Integer): IXMLInfPercurso;

    function Get_Item(Index: Integer): IXMLInfPercurso;
    property Items[Index: Integer]: IXMLInfPercurso read Get_Item; default;
  end;

{ IXMLEmit }

  IXMLEmit = interface(IXMLNode)
    ['{AA74E455-769F-4051-9374-139F8FBBDA10}']
    { Property Accessors }
    function Get_CNPJ: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_XFant: UnicodeString;
    function Get_EnderEmit: IXMLTEndeEmi;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_XFant(Value: UnicodeString);
    { Methods & Properties }
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property IE: UnicodeString read Get_IE write Set_IE;
    property XNome: UnicodeString read Get_XNome write Set_XNome;
    property XFant: UnicodeString read Get_XFant write Set_XFant;
    property EnderEmit: IXMLTEndeEmi read Get_EnderEmit;
  end;

{ IXMLTEndeEmi }

  IXMLTEndeEmi = interface(IXMLNode)
    ['{B572857A-A824-41E4-B831-CE75C31856FB}']
    { Property Accessors }
    function Get_XLgr: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_XCpl: UnicodeString;
    function Get_XBairro: UnicodeString;
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_CEP: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_Fone: UnicodeString;
    function Get_Email: UnicodeString;
    procedure Set_XLgr(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_XCpl(Value: UnicodeString);
    procedure Set_XBairro(Value: UnicodeString);
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_CEP(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_Fone(Value: UnicodeString);
    procedure Set_Email(Value: UnicodeString);
    { Methods & Properties }
    property XLgr: UnicodeString read Get_XLgr write Set_XLgr;
    property Nro: UnicodeString read Get_Nro write Set_Nro;
    property XCpl: UnicodeString read Get_XCpl write Set_XCpl;
    property XBairro: UnicodeString read Get_XBairro write Set_XBairro;
    property CMun: UnicodeString read Get_CMun write Set_CMun;
    property XMun: UnicodeString read Get_XMun write Set_XMun;
    property CEP: UnicodeString read Get_CEP write Set_CEP;
    property UF: UnicodeString read Get_UF write Set_UF;
    property Fone: UnicodeString read Get_Fone write Set_Fone;
    property Email: UnicodeString read Get_Email write Set_Email;
  end;

{ IXMLInfModal }

  IXMLInfModal = interface(IXMLNode)
    ['{1448AEE5-933E-4215-9DAF-901AFA2A135E}']
    { Property Accessors }
    function Get_VersaoModal: UnicodeString;
    procedure Set_VersaoModal(Value: UnicodeString);
    { Methods & Properties }
    property VersaoModal: UnicodeString read Get_VersaoModal write Set_VersaoModal;
  end;

{ IXMLInfDoc }

  IXMLInfDoc = interface(IXMLNodeCollection)
    ['{08FBCD63-BCA1-4934-BD64-904A68B7C152}']
    { Property Accessors }
    function Get_InfMunDescarga(Index: Integer): IXMLInfMunDescarga;
    { Methods & Properties }
    function Add: IXMLInfMunDescarga;
    function Insert(const Index: Integer): IXMLInfMunDescarga;
    property InfMunDescarga[Index: Integer]: IXMLInfMunDescarga read Get_InfMunDescarga; default;
  end;

{ IXMLInfMunDescarga }

  IXMLInfMunDescarga = interface(IXMLNode)
    ['{1BF0E761-FFC6-4E23-AB12-F73B3986A809}']
    { Property Accessors }
    function Get_CMunDescarga: UnicodeString;
    function Get_XMunDescarga: UnicodeString;
    function Get_InfCTe: IXMLInfCTeList;
    function Get_InfNFe: IXMLInfNFeList;
    function Get_InfMDFeTransp: IXMLInfMDFeTranspList;
    procedure Set_CMunDescarga(Value: UnicodeString);
    procedure Set_XMunDescarga(Value: UnicodeString);
    { Methods & Properties }
    property CMunDescarga: UnicodeString read Get_CMunDescarga write Set_CMunDescarga;
    property XMunDescarga: UnicodeString read Get_XMunDescarga write Set_XMunDescarga;
    property InfCTe: IXMLInfCTeList read Get_InfCTe;
    property InfNFe: IXMLInfNFeList read Get_InfNFe;
    property InfMDFeTransp: IXMLInfMDFeTranspList read Get_InfMDFeTransp;
  end;

{ IXMLInfCTe }

  IXMLInfCTe = interface(IXMLNode)
    ['{A6E42B94-5ECD-46E4-9BB3-2A952E142C12}']
    { Property Accessors }
    function Get_ChCTe: UnicodeString;
    function Get_SegCodBarra: UnicodeString;
    function Get_InfUnidTransp: IXMLTUnidadeTranspList;
    procedure Set_ChCTe(Value: UnicodeString);
    procedure Set_SegCodBarra(Value: UnicodeString);
    { Methods & Properties }
    property ChCTe: UnicodeString read Get_ChCTe write Set_ChCTe;
    property SegCodBarra: UnicodeString read Get_SegCodBarra write Set_SegCodBarra;
    property InfUnidTransp: IXMLTUnidadeTranspList read Get_InfUnidTransp;
  end;

{ IXMLInfCTeList }

  IXMLInfCTeList = interface(IXMLNodeCollection)
    ['{89378815-5409-4A05-90A0-E1E1DE5A6CD6}']
    { Methods & Properties }
    function Add: IXMLInfCTe;
    function Insert(const Index: Integer): IXMLInfCTe;

    function Get_Item(Index: Integer): IXMLInfCTe;
    property Items[Index: Integer]: IXMLInfCTe read Get_Item; default;
  end;

{ IXMLTUnidadeTransp }

  IXMLTUnidadeTransp = interface(IXMLNode)
    ['{984F5155-38C4-4444-B03F-5A92CB942033}']
    { Property Accessors }
    function Get_TpUnidTransp: UnicodeString;
    function Get_IdUnidTransp: UnicodeString;
    function Get_LacUnidTransp: IXMLLacUnidTranspList;
    function Get_InfUnidCarga: IXMLTUnidCargaList;
    function Get_QtdRat: UnicodeString;
    procedure Set_TpUnidTransp(Value: UnicodeString);
    procedure Set_IdUnidTransp(Value: UnicodeString);
    procedure Set_QtdRat(Value: UnicodeString);
    { Methods & Properties }
    property TpUnidTransp: UnicodeString read Get_TpUnidTransp write Set_TpUnidTransp;
    property IdUnidTransp: UnicodeString read Get_IdUnidTransp write Set_IdUnidTransp;
    property LacUnidTransp: IXMLLacUnidTranspList read Get_LacUnidTransp;
    property InfUnidCarga: IXMLTUnidCargaList read Get_InfUnidCarga;
    property QtdRat: UnicodeString read Get_QtdRat write Set_QtdRat;
  end;

{ IXMLTUnidadeTranspList }

  IXMLTUnidadeTranspList = interface(IXMLNodeCollection)
    ['{C1334940-685F-45D1-85FE-F5DB99C45109}']
    { Methods & Properties }
    function Add: IXMLTUnidadeTransp;
    function Insert(const Index: Integer): IXMLTUnidadeTransp;

    function Get_Item(Index: Integer): IXMLTUnidadeTransp;
    property Items[Index: Integer]: IXMLTUnidadeTransp read Get_Item; default;
  end;

{ IXMLLacUnidTransp }

  IXMLLacUnidTransp = interface(IXMLNode)
    ['{4ACD7163-9F37-4CB2-9384-4639FBED57AA}']
    { Property Accessors }
    function Get_NLacre: UnicodeString;
    procedure Set_NLacre(Value: UnicodeString);
    { Methods & Properties }
    property NLacre: UnicodeString read Get_NLacre write Set_NLacre;
  end;

{ IXMLLacUnidTranspList }

  IXMLLacUnidTranspList = interface(IXMLNodeCollection)
    ['{856D8DA2-C667-43FE-BF28-6D642CD40F33}']
    { Methods & Properties }
    function Add: IXMLLacUnidTransp;
    function Insert(const Index: Integer): IXMLLacUnidTransp;

    function Get_Item(Index: Integer): IXMLLacUnidTransp;
    property Items[Index: Integer]: IXMLLacUnidTransp read Get_Item; default;
  end;

{ IXMLTUnidCarga }

  IXMLTUnidCarga = interface(IXMLNode)
    ['{C645F464-9936-4AFD-B7CA-1BA3D53C1931}']
    { Property Accessors }
    function Get_TpUnidCarga: UnicodeString;
    function Get_IdUnidCarga: UnicodeString;
    function Get_LacUnidCarga: IXMLLacUnidCargaList;
    function Get_QtdRat: UnicodeString;
    procedure Set_TpUnidCarga(Value: UnicodeString);
    procedure Set_IdUnidCarga(Value: UnicodeString);
    procedure Set_QtdRat(Value: UnicodeString);
    { Methods & Properties }
    property TpUnidCarga: UnicodeString read Get_TpUnidCarga write Set_TpUnidCarga;
    property IdUnidCarga: UnicodeString read Get_IdUnidCarga write Set_IdUnidCarga;
    property LacUnidCarga: IXMLLacUnidCargaList read Get_LacUnidCarga;
    property QtdRat: UnicodeString read Get_QtdRat write Set_QtdRat;
  end;

{ IXMLTUnidCargaList }

  IXMLTUnidCargaList = interface(IXMLNodeCollection)
    ['{805564FF-09C5-499A-9967-21FE7660281A}']
    { Methods & Properties }
    function Add: IXMLTUnidCarga;
    function Insert(const Index: Integer): IXMLTUnidCarga;

    function Get_Item(Index: Integer): IXMLTUnidCarga;
    property Items[Index: Integer]: IXMLTUnidCarga read Get_Item; default;
  end;

{ IXMLLacUnidCarga }

  IXMLLacUnidCarga = interface(IXMLNode)
    ['{007CBA1F-4AFC-4CAD-B747-731D95A7F40B}']
    { Property Accessors }
    function Get_NLacre: UnicodeString;
    procedure Set_NLacre(Value: UnicodeString);
    { Methods & Properties }
    property NLacre: UnicodeString read Get_NLacre write Set_NLacre;
  end;

{ IXMLLacUnidCargaList }

  IXMLLacUnidCargaList = interface(IXMLNodeCollection)
    ['{5649CB88-C52C-439E-A135-E802791F2008}']
    { Methods & Properties }
    function Add: IXMLLacUnidCarga;
    function Insert(const Index: Integer): IXMLLacUnidCarga;

    function Get_Item(Index: Integer): IXMLLacUnidCarga;
    property Items[Index: Integer]: IXMLLacUnidCarga read Get_Item; default;
  end;

{ IXMLInfNFe }

  IXMLInfNFe = interface(IXMLNode)
    ['{427E182F-983D-4323-8B7E-2F3F05151275}']
    { Property Accessors }
    function Get_ChNFe: UnicodeString;
    function Get_SegCodBarra: UnicodeString;
    function Get_InfUnidTransp: IXMLTUnidadeTranspList;
    procedure Set_ChNFe(Value: UnicodeString);
    procedure Set_SegCodBarra(Value: UnicodeString);
    { Methods & Properties }
    property ChNFe: UnicodeString read Get_ChNFe write Set_ChNFe;
    property SegCodBarra: UnicodeString read Get_SegCodBarra write Set_SegCodBarra;
    property InfUnidTransp: IXMLTUnidadeTranspList read Get_InfUnidTransp;
  end;

{ IXMLInfNFeList }

  IXMLInfNFeList = interface(IXMLNodeCollection)
    ['{5C0F451D-2904-4A55-B91A-1B4B444F4BF9}']
    { Methods & Properties }
    function Add: IXMLInfNFe;
    function Insert(const Index: Integer): IXMLInfNFe;

    function Get_Item(Index: Integer): IXMLInfNFe;
    property Items[Index: Integer]: IXMLInfNFe read Get_Item; default;
  end;

{ IXMLInfMDFeTransp }

  IXMLInfMDFeTransp = interface(IXMLNode)
    ['{289B8242-9571-4CB9-B12B-8CCB86C1C44A}']
    { Property Accessors }
    function Get_ChMDFe: UnicodeString;
    function Get_InfUnidTransp: IXMLTUnidadeTranspList;
    procedure Set_ChMDFe(Value: UnicodeString);
    { Methods & Properties }
    property ChMDFe: UnicodeString read Get_ChMDFe write Set_ChMDFe;
    property InfUnidTransp: IXMLTUnidadeTranspList read Get_InfUnidTransp;
  end;

{ IXMLInfMDFeTranspList }

  IXMLInfMDFeTranspList = interface(IXMLNodeCollection)
    ['{169975DB-8100-44A9-8EDF-81DE35CE6BD7}']
    { Methods & Properties }
    function Add: IXMLInfMDFeTransp;
    function Insert(const Index: Integer): IXMLInfMDFeTransp;

    function Get_Item(Index: Integer): IXMLInfMDFeTransp;
    property Items[Index: Integer]: IXMLInfMDFeTransp read Get_Item; default;
  end;

{ IXMLTot }

  IXMLTot = interface(IXMLNode)
    ['{79B570F2-CA50-4DCC-BF8B-52B22B060DFE}']
    { Property Accessors }
    function Get_QCTe: UnicodeString;
    function Get_QNFe: UnicodeString;
    function Get_QMDFe: UnicodeString;
    function Get_VCarga: UnicodeString;
    function Get_CUnid: UnicodeString;
    function Get_QCarga: UnicodeString;
    procedure Set_QCTe(Value: UnicodeString);
    procedure Set_QNFe(Value: UnicodeString);
    procedure Set_QMDFe(Value: UnicodeString);
    procedure Set_VCarga(Value: UnicodeString);
    procedure Set_CUnid(Value: UnicodeString);
    procedure Set_QCarga(Value: UnicodeString);
    { Methods & Properties }
    property QCTe: UnicodeString read Get_QCTe write Set_QCTe;
    property QNFe: UnicodeString read Get_QNFe write Set_QNFe;
    property QMDFe: UnicodeString read Get_QMDFe write Set_QMDFe;
    property VCarga: UnicodeString read Get_VCarga write Set_VCarga;
    property CUnid: UnicodeString read Get_CUnid write Set_CUnid;
    property QCarga: UnicodeString read Get_QCarga write Set_QCarga;
  end;

{ IXMLLacres }

  IXMLLacres = interface(IXMLNode)
    ['{8D1EEF00-097E-4BC8-A0CD-8013E4B28235}']
    { Property Accessors }
    function Get_NLacre: UnicodeString;
    procedure Set_NLacre(Value: UnicodeString);
    { Methods & Properties }
    property NLacre: UnicodeString read Get_NLacre write Set_NLacre;
  end;

{ IXMLLacresList }

  IXMLLacresList = interface(IXMLNodeCollection)
    ['{8DA03EE0-C0F5-4395-A16B-2D7ECF180F46}']
    { Methods & Properties }
    function Add: IXMLLacres;
    function Insert(const Index: Integer): IXMLLacres;

    function Get_Item(Index: Integer): IXMLLacres;
    property Items[Index: Integer]: IXMLLacres read Get_Item; default;
  end;

{ IXMLAutXML }

  IXMLAutXML = interface(IXMLNode)
    ['{55E6A08D-B2C3-4BDD-AEBF-8626890EEE16}']
    { Property Accessors }
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    { Methods & Properties }
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property CPF: UnicodeString read Get_CPF write Set_CPF;
  end;

{ IXMLAutXMLList }

  IXMLAutXMLList = interface(IXMLNodeCollection)
    ['{942E0945-3FB2-461B-88E7-99D3A52451A6}']
    { Methods & Properties }
    function Add: IXMLAutXML;
    function Insert(const Index: Integer): IXMLAutXML;

    function Get_Item(Index: Integer): IXMLAutXML;
    property Items[Index: Integer]: IXMLAutXML read Get_Item; default;
  end;

{ IXMLInfAdic }

  IXMLInfAdic = interface(IXMLNode)
    ['{DA4B2DEB-EACC-4B5C-9817-1119D9888C68}']
    { Property Accessors }
    function Get_InfAdFisco: UnicodeString;
    function Get_InfCpl: UnicodeString;
    procedure Set_InfAdFisco(Value: UnicodeString);
    procedure Set_InfCpl(Value: UnicodeString);
    { Methods & Properties }
    property InfAdFisco: UnicodeString read Get_InfAdFisco write Set_InfAdFisco;
    property InfCpl: UnicodeString read Get_InfCpl write Set_InfCpl;
  end;

{ IXMLSignatureType_ds }

  IXMLSignatureType_ds = interface(IXMLNode)
    ['{CA58E49D-50C5-4D94-8DAF-5901F67BD9E8}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_SignedInfo: IXMLSignedInfoType_ds;
    function Get_SignatureValue: IXMLSignatureValueType_ds;
    function Get_KeyInfo: IXMLKeyInfoType_ds;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property SignedInfo: IXMLSignedInfoType_ds read Get_SignedInfo;
    property SignatureValue: IXMLSignatureValueType_ds read Get_SignatureValue;
    property KeyInfo: IXMLKeyInfoType_ds read Get_KeyInfo;
  end;

{ IXMLSignedInfoType_ds }

  IXMLSignedInfoType_ds = interface(IXMLNode)
    ['{2A2174D6-831C-4FE7-8683-E19FF214B8F3}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_CanonicalizationMethod: IXMLCanonicalizationMethod_ds;
    function Get_SignatureMethod: IXMLSignatureMethod_ds;
    function Get_Reference: IXMLReferenceType_ds;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property CanonicalizationMethod: IXMLCanonicalizationMethod_ds read Get_CanonicalizationMethod;
    property SignatureMethod: IXMLSignatureMethod_ds read Get_SignatureMethod;
    property Reference: IXMLReferenceType_ds read Get_Reference;
  end;

{ IXMLCanonicalizationMethod_ds }

  IXMLCanonicalizationMethod_ds = interface(IXMLNode)
    ['{D07E6609-8F70-4467-9B53-347BDD325BA8}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
  end;

{ IXMLSignatureMethod_ds }

  IXMLSignatureMethod_ds = interface(IXMLNode)
    ['{9601FA5D-D81C-47F9-B6E2-3093247DED78}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
  end;

{ IXMLReferenceType_ds }

  IXMLReferenceType_ds = interface(IXMLNode)
    ['{AA46C995-25AC-4233-B830-7D895138E378}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_URI: UnicodeString;
    function Get_Type_: UnicodeString;
    function Get_Transforms: IXMLTransformsType_ds;
    function Get_DigestMethod: IXMLDigestMethod_ds;
    function Get_DigestValue: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_URI(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_DigestValue(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property URI: UnicodeString read Get_URI write Set_URI;
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
    property Transforms: IXMLTransformsType_ds read Get_Transforms;
    property DigestMethod: IXMLDigestMethod_ds read Get_DigestMethod;
    property DigestValue: UnicodeString read Get_DigestValue write Set_DigestValue;
  end;

{ IXMLTransformsType_ds }

  IXMLTransformsType_ds = interface(IXMLNodeCollection)
    ['{A31D36F8-D437-4CBC-B9F1-C51EFF9B550F}']
    { Property Accessors }
    function Get_Transform(Index: Integer): IXMLTransformType_ds;
    { Methods & Properties }
    function Add: IXMLTransformType_ds;
    function Insert(const Index: Integer): IXMLTransformType_ds;
    property Transform[Index: Integer]: IXMLTransformType_ds read Get_Transform; default;
  end;

{ IXMLTransformType_ds }

  IXMLTransformType_ds = interface(IXMLNodeCollection)
    ['{19522DB7-18EF-41BC-BA84-A393ACDFFFC3}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    function Get_XPath(Index: Integer): UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    function Add(const XPath: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const XPath: UnicodeString): IXMLNode;
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
    property XPath[Index: Integer]: UnicodeString read Get_XPath; default;
  end;

{ IXMLDigestMethod_ds }

  IXMLDigestMethod_ds = interface(IXMLNode)
    ['{CB064B3E-7BB3-4104-8930-031C2499C021}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
  end;

{ IXMLSignatureValueType_ds }

  IXMLSignatureValueType_ds = interface(IXMLNode)
    ['{B531C07A-3562-4723-9253-47F27D6E85F7}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
  end;

{ IXMLKeyInfoType_ds }

  IXMLKeyInfoType_ds = interface(IXMLNode)
    ['{D324B6DB-7C32-473F-90A2-D7AAF1D9C472}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_X509Data: IXMLX509DataType_ds;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property X509Data: IXMLX509DataType_ds read Get_X509Data;
  end;

{ IXMLX509DataType_ds }

  IXMLX509DataType_ds = interface(IXMLNode)
    ['{0275D40C-7B84-4CBD-B468-DEC2763E47C2}']
    { Property Accessors }
    function Get_X509Certificate: UnicodeString;
    procedure Set_X509Certificate(Value: UnicodeString);
    { Methods & Properties }
    property X509Certificate: UnicodeString read Get_X509Certificate write Set_X509Certificate;
  end;

{ IXMLTEnviMDFe }

  IXMLTEnviMDFe = interface(IXMLNode)
    ['{008FC631-B827-42BD-8F82-E8DB8B82A4B6}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_IdLote: UnicodeString;
    function Get_MDFe: IXMLTMDFe;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_IdLote(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property IdLote: UnicodeString read Get_IdLote write Set_IdLote;
    property MDFe: IXMLTMDFe read Get_MDFe;
  end;

{ IXMLTRetEnviMDFe }

  IXMLTRetEnviMDFe = interface(IXMLNode)
    ['{1B2D09D9-8A79-40FE-8D9D-E9BCC28D938D}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    function Get_InfRec: IXMLInfRec;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property CUF: UnicodeString read Get_CUF write Set_CUF;
    property VerAplic: UnicodeString read Get_VerAplic write Set_VerAplic;
    property CStat: UnicodeString read Get_CStat write Set_CStat;
    property XMotivo: UnicodeString read Get_XMotivo write Set_XMotivo;
    property InfRec: IXMLInfRec read Get_InfRec;
  end;

{ IXMLInfRec }

  IXMLInfRec = interface(IXMLNode)
    ['{70B2C169-B097-4F7E-A910-4756423C3324}']
    { Property Accessors }
    function Get_NRec: UnicodeString;
    function Get_DhRecbto: UnicodeString;
    function Get_TMed: Integer;
    procedure Set_NRec(Value: UnicodeString);
    procedure Set_DhRecbto(Value: UnicodeString);
    procedure Set_TMed(Value: Integer);
    { Methods & Properties }
    property NRec: UnicodeString read Get_NRec write Set_NRec;
    property DhRecbto: UnicodeString read Get_DhRecbto write Set_DhRecbto;
    property TMed: Integer read Get_TMed write Set_TMed;
  end;

{ IXMLTEndereco }

  IXMLTEndereco = interface(IXMLNode)
    ['{882AA773-7E4A-457A-B6E1-681777C1559B}']
    { Property Accessors }
    function Get_XLgr: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_XCpl: UnicodeString;
    function Get_XBairro: UnicodeString;
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_CEP: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_CPais: UnicodeString;
    function Get_XPais: UnicodeString;
    procedure Set_XLgr(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_XCpl(Value: UnicodeString);
    procedure Set_XBairro(Value: UnicodeString);
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_CEP(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_CPais(Value: UnicodeString);
    procedure Set_XPais(Value: UnicodeString);
    { Methods & Properties }
    property XLgr: UnicodeString read Get_XLgr write Set_XLgr;
    property Nro: UnicodeString read Get_Nro write Set_Nro;
    property XCpl: UnicodeString read Get_XCpl write Set_XCpl;
    property XBairro: UnicodeString read Get_XBairro write Set_XBairro;
    property CMun: UnicodeString read Get_CMun write Set_CMun;
    property XMun: UnicodeString read Get_XMun write Set_XMun;
    property CEP: UnicodeString read Get_CEP write Set_CEP;
    property UF: UnicodeString read Get_UF write Set_UF;
    property CPais: UnicodeString read Get_CPais write Set_CPais;
    property XPais: UnicodeString read Get_XPais write Set_XPais;
  end;

{ IXMLTEndernac }

  IXMLTEndernac = interface(IXMLNode)
    ['{93FC5960-1C31-46E9-AB8B-BD24BE2B06D6}']
    { Property Accessors }
    function Get_XLgr: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_XCpl: UnicodeString;
    function Get_XBairro: UnicodeString;
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_CEP: UnicodeString;
    function Get_UF: UnicodeString;
    procedure Set_XLgr(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_XCpl(Value: UnicodeString);
    procedure Set_XBairro(Value: UnicodeString);
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_CEP(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    { Methods & Properties }
    property XLgr: UnicodeString read Get_XLgr write Set_XLgr;
    property Nro: UnicodeString read Get_Nro write Set_Nro;
    property XCpl: UnicodeString read Get_XCpl write Set_XCpl;
    property XBairro: UnicodeString read Get_XBairro write Set_XBairro;
    property CMun: UnicodeString read Get_CMun write Set_CMun;
    property XMun: UnicodeString read Get_XMun write Set_XMun;
    property CEP: UnicodeString read Get_CEP write Set_CEP;
    property UF: UnicodeString read Get_UF write Set_UF;
  end;

{ IXMLTEnderFer }

  IXMLTEnderFer = interface(IXMLNode)
    ['{9FD56645-775D-4F84-B0A8-EA6AF918DD08}']
    { Property Accessors }
    function Get_XLgr: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_XCpl: UnicodeString;
    function Get_XBairro: UnicodeString;
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_CEP: UnicodeString;
    function Get_UF: UnicodeString;
    procedure Set_XLgr(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_XCpl(Value: UnicodeString);
    procedure Set_XBairro(Value: UnicodeString);
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_CEP(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    { Methods & Properties }
    property XLgr: UnicodeString read Get_XLgr write Set_XLgr;
    property Nro: UnicodeString read Get_Nro write Set_Nro;
    property XCpl: UnicodeString read Get_XCpl write Set_XCpl;
    property XBairro: UnicodeString read Get_XBairro write Set_XBairro;
    property CMun: UnicodeString read Get_CMun write Set_CMun;
    property XMun: UnicodeString read Get_XMun write Set_XMun;
    property CEP: UnicodeString read Get_CEP write Set_CEP;
    property UF: UnicodeString read Get_UF write Set_UF;
  end;

{ IXMLTEndOrg }

  IXMLTEndOrg = interface(IXMLNode)
    ['{600E8975-0854-4BB2-B50E-E4B9A599C039}']
    { Property Accessors }
    function Get_XLgr: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_XCpl: UnicodeString;
    function Get_XBairro: UnicodeString;
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_CEP: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_CPais: UnicodeString;
    function Get_XPais: UnicodeString;
    function Get_Fone: UnicodeString;
    procedure Set_XLgr(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_XCpl(Value: UnicodeString);
    procedure Set_XBairro(Value: UnicodeString);
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_CEP(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_CPais(Value: UnicodeString);
    procedure Set_XPais(Value: UnicodeString);
    procedure Set_Fone(Value: UnicodeString);
    { Methods & Properties }
    property XLgr: UnicodeString read Get_XLgr write Set_XLgr;
    property Nro: UnicodeString read Get_Nro write Set_Nro;
    property XCpl: UnicodeString read Get_XCpl write Set_XCpl;
    property XBairro: UnicodeString read Get_XBairro write Set_XBairro;
    property CMun: UnicodeString read Get_CMun write Set_CMun;
    property XMun: UnicodeString read Get_XMun write Set_XMun;
    property CEP: UnicodeString read Get_CEP write Set_CEP;
    property UF: UnicodeString read Get_UF write Set_UF;
    property CPais: UnicodeString read Get_CPais write Set_CPais;
    property XPais: UnicodeString read Get_XPais write Set_XPais;
    property Fone: UnicodeString read Get_Fone write Set_Fone;
  end;

{ IXMLTLocal }

  IXMLTLocal = interface(IXMLNode)
    ['{2F78676D-F75A-4130-A88F-AAE496657C68}']
    { Property Accessors }
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_UF: UnicodeString;
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    { Methods & Properties }
    property CMun: UnicodeString read Get_CMun write Set_CMun;
    property XMun: UnicodeString read Get_XMun write Set_XMun;
    property UF: UnicodeString read Get_UF write Set_UF;
  end;

{ IXMLTEndReEnt }

  IXMLTEndReEnt = interface(IXMLNode)
    ['{D5B6F6B2-2BE8-47DC-A362-86237E0919B3}']
    { Property Accessors }
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_XLgr: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_XCpl: UnicodeString;
    function Get_XBairro: UnicodeString;
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_UF: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_XLgr(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_XCpl(Value: UnicodeString);
    procedure Set_XBairro(Value: UnicodeString);
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    { Methods & Properties }
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property CPF: UnicodeString read Get_CPF write Set_CPF;
    property XNome: UnicodeString read Get_XNome write Set_XNome;
    property XLgr: UnicodeString read Get_XLgr write Set_XLgr;
    property Nro: UnicodeString read Get_Nro write Set_Nro;
    property XCpl: UnicodeString read Get_XCpl write Set_XCpl;
    property XBairro: UnicodeString read Get_XBairro write Set_XBairro;
    property CMun: UnicodeString read Get_CMun write Set_CMun;
    property XMun: UnicodeString read Get_XMun write Set_XMun;
    property UF: UnicodeString read Get_UF write Set_UF;
  end;

{ IXMLTNFeNF }

  IXMLTNFeNF = interface(IXMLNode)
    ['{A4D64E72-FE1A-439B-8229-5DABC92EB282}']
    { Property Accessors }
    function Get_InfNFe: IXMLInfNFe;
    function Get_InfNF: IXMLInfNF;
    { Methods & Properties }
    property InfNFe: IXMLInfNFe read Get_InfNFe;
    property InfNF: IXMLInfNF read Get_InfNF;
  end;

{ IXMLInfNF }

  IXMLInfNF = interface(IXMLNode)
    ['{BDFF3C48-C0F3-4F6D-99D9-6BF71BDFC3F6}']
    { Property Accessors }
    function Get_Emi: IXMLEmi;
    function Get_Dest: IXMLDest;
    function Get_Serie: UnicodeString;
    function Get_NNF: UnicodeString;
    function Get_DEmi: UnicodeString;
    function Get_VNF: UnicodeString;
    function Get_PIN: UnicodeString;
    procedure Set_Serie(Value: UnicodeString);
    procedure Set_NNF(Value: UnicodeString);
    procedure Set_DEmi(Value: UnicodeString);
    procedure Set_VNF(Value: UnicodeString);
    procedure Set_PIN(Value: UnicodeString);
    { Methods & Properties }
    property Emi: IXMLEmi read Get_Emi;
    property Dest: IXMLDest read Get_Dest;
    property Serie: UnicodeString read Get_Serie write Set_Serie;
    property NNF: UnicodeString read Get_NNF write Set_NNF;
    property DEmi: UnicodeString read Get_DEmi write Set_DEmi;
    property VNF: UnicodeString read Get_VNF write Set_VNF;
    property PIN: UnicodeString read Get_PIN write Set_PIN;
  end;

{ IXMLEmi }

  IXMLEmi = interface(IXMLNode)
    ['{BCAAB4A6-A353-471A-AB27-F5CD2C68EB03}']
    { Property Accessors }
    function Get_CNPJ: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_UF: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    { Methods & Properties }
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property XNome: UnicodeString read Get_XNome write Set_XNome;
    property UF: UnicodeString read Get_UF write Set_UF;
  end;

{ IXMLDest }

  IXMLDest = interface(IXMLNode)
    ['{DD83CBAE-EE8A-4310-BA7D-7388B8F39391}']
    { Property Accessors }
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_UF: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    { Methods & Properties }
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property CPF: UnicodeString read Get_CPF write Set_CPF;
    property XNome: UnicodeString read Get_XNome write Set_XNome;
    property UF: UnicodeString read Get_UF write Set_UF;
  end;

{ Forward Decls }

  TXMLTMDFe = class;
  TXMLInfMDFe = class;
  TXMLIde = class;
  TXMLInfMunCarrega = class;
  TXMLInfMunCarregaList = class;
  TXMLInfPercurso = class;
  TXMLInfPercursoList = class;
  TXMLEmit = class;
  TXMLTEndeEmi = class;
  TXMLInfModal = class;
  TXMLInfDoc = class;
  TXMLInfMunDescarga = class;
  TXMLInfCTe = class;
  TXMLInfCTeList = class;
  TXMLTUnidadeTransp = class;
  TXMLTUnidadeTranspList = class;
  TXMLLacUnidTransp = class;
  TXMLLacUnidTranspList = class;
  TXMLTUnidCarga = class;
  TXMLTUnidCargaList = class;
  TXMLLacUnidCarga = class;
  TXMLLacUnidCargaList = class;
  TXMLInfNFe = class;
  TXMLInfNFeList = class;
  TXMLInfMDFeTransp = class;
  TXMLInfMDFeTranspList = class;
  TXMLTot = class;
  TXMLLacres = class;
  TXMLLacresList = class;
  TXMLAutXML = class;
  TXMLAutXMLList = class;
  TXMLInfAdic = class;
  TXMLSignatureType_ds = class;
  TXMLSignedInfoType_ds = class;
  TXMLCanonicalizationMethod_ds = class;
  TXMLSignatureMethod_ds = class;
  TXMLReferenceType_ds = class;
  TXMLTransformsType_ds = class;
  TXMLTransformType_ds = class;
  TXMLDigestMethod_ds = class;
  TXMLSignatureValueType_ds = class;
  TXMLKeyInfoType_ds = class;
  TXMLX509DataType_ds = class;
  TXMLTEnviMDFe = class;
  TXMLTRetEnviMDFe = class;
  TXMLInfRec = class;
  TXMLTEndereco = class;
  TXMLTEndernac = class;
  TXMLTEnderFer = class;
  TXMLTEndOrg = class;
  TXMLTLocal = class;
  TXMLTEndReEnt = class;
  TXMLTNFeNF = class;
  TXMLInfNF = class;
  TXMLEmi = class;
  TXMLDest = class;

{ TXMLTMDFe }

  TXMLTMDFe = class(TXMLNode, IXMLTMDFe)
  protected
    { IXMLTMDFe }
    function Get_InfMDFe: IXMLInfMDFe;
    function Get_Signature: IXMLSignatureType_ds;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfMDFe }

  TXMLInfMDFe = class(TXMLNode, IXMLInfMDFe)
  private
    FLacres: IXMLLacresList;
    FAutXML: IXMLAutXMLList;
  protected
    { IXMLInfMDFe }
    function Get_Versao: UnicodeString;
    function Get_Id: UnicodeString;
    function Get_Ide: IXMLIde;
    function Get_Emit: IXMLEmit;
    function Get_InfModal: IXMLInfModal;
    function Get_InfDoc: IXMLInfDoc;
    function Get_Tot: IXMLTot;
    function Get_Lacres: IXMLLacresList;
    function Get_AutXML: IXMLAutXMLList;
    function Get_InfAdic: IXMLInfAdic;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_Id(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLIde }

  TXMLIde = class(TXMLNode, IXMLIde)
  private
    FInfMunCarrega: IXMLInfMunCarregaList;
    FInfPercurso: IXMLInfPercursoList;
  protected
    { IXMLIde }
    function Get_CUF: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_TpEmit: UnicodeString;
    function Get_Mod_: UnicodeString;
    function Get_Serie: UnicodeString;
    function Get_NMDF: UnicodeString;
    function Get_CMDF: UnicodeString;
    function Get_CDV: UnicodeString;
    function Get_Modal: UnicodeString;
    function Get_DhEmi: UnicodeString;
    function Get_TpEmis: UnicodeString;
    function Get_ProcEmi: UnicodeString;
    function Get_VerProc: UnicodeString;
    function Get_UFIni: UnicodeString;
    function Get_UFFim: UnicodeString;
    function Get_InfMunCarrega: IXMLInfMunCarregaList;
    function Get_InfPercurso: IXMLInfPercursoList;
    function Get_DhIniViagem: UnicodeString;
    procedure Set_CUF(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_TpEmit(Value: UnicodeString);
    procedure Set_Mod_(Value: UnicodeString);
    procedure Set_Serie(Value: UnicodeString);
    procedure Set_NMDF(Value: UnicodeString);
    procedure Set_CMDF(Value: UnicodeString);
    procedure Set_CDV(Value: UnicodeString);
    procedure Set_Modal(Value: UnicodeString);
    procedure Set_DhEmi(Value: UnicodeString);
    procedure Set_TpEmis(Value: UnicodeString);
    procedure Set_ProcEmi(Value: UnicodeString);
    procedure Set_VerProc(Value: UnicodeString);
    procedure Set_UFIni(Value: UnicodeString);
    procedure Set_UFFim(Value: UnicodeString);
    procedure Set_DhIniViagem(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfMunCarrega }

  TXMLInfMunCarrega = class(TXMLNode, IXMLInfMunCarrega)
  protected
    { IXMLInfMunCarrega }
    function Get_CMunCarrega: UnicodeString;
    function Get_XMunCarrega: UnicodeString;
    procedure Set_CMunCarrega(Value: UnicodeString);
    procedure Set_XMunCarrega(Value: UnicodeString);
  end;

{ TXMLInfMunCarregaList }

  TXMLInfMunCarregaList = class(TXMLNodeCollection, IXMLInfMunCarregaList)
  protected
    { IXMLInfMunCarregaList }
    function Add: IXMLInfMunCarrega;
    function Insert(const Index: Integer): IXMLInfMunCarrega;

    function Get_Item(Index: Integer): IXMLInfMunCarrega;
  end;

{ TXMLInfPercurso }

  TXMLInfPercurso = class(TXMLNode, IXMLInfPercurso)
  protected
    { IXMLInfPercurso }
    function Get_UFPer: UnicodeString;
    procedure Set_UFPer(Value: UnicodeString);
  end;

{ TXMLInfPercursoList }

  TXMLInfPercursoList = class(TXMLNodeCollection, IXMLInfPercursoList)
  protected
    { IXMLInfPercursoList }
    function Add: IXMLInfPercurso;
    function Insert(const Index: Integer): IXMLInfPercurso;

    function Get_Item(Index: Integer): IXMLInfPercurso;
  end;

{ TXMLEmit }

  TXMLEmit = class(TXMLNode, IXMLEmit)
  protected
    { IXMLEmit }
    function Get_CNPJ: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_XFant: UnicodeString;
    function Get_EnderEmit: IXMLTEndeEmi;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_XFant(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTEndeEmi }

  TXMLTEndeEmi = class(TXMLNode, IXMLTEndeEmi)
  protected
    { IXMLTEndeEmi }
    function Get_XLgr: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_XCpl: UnicodeString;
    function Get_XBairro: UnicodeString;
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_CEP: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_Fone: UnicodeString;
    function Get_Email: UnicodeString;
    procedure Set_XLgr(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_XCpl(Value: UnicodeString);
    procedure Set_XBairro(Value: UnicodeString);
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_CEP(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_Fone(Value: UnicodeString);
    procedure Set_Email(Value: UnicodeString);
  end;

{ TXMLInfModal }

  TXMLInfModal = class(TXMLNode, IXMLInfModal)
  protected
    { IXMLInfModal }
    function Get_VersaoModal: UnicodeString;
    procedure Set_VersaoModal(Value: UnicodeString);
  end;

{ TXMLInfDoc }

  TXMLInfDoc = class(TXMLNodeCollection, IXMLInfDoc)
  protected
    { IXMLInfDoc }
    function Get_InfMunDescarga(Index: Integer): IXMLInfMunDescarga;
    function Add: IXMLInfMunDescarga;
    function Insert(const Index: Integer): IXMLInfMunDescarga;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfMunDescarga }

  TXMLInfMunDescarga = class(TXMLNode, IXMLInfMunDescarga)
  private
    FInfCTe: IXMLInfCTeList;
    FInfNFe: IXMLInfNFeList;
    FInfMDFeTransp: IXMLInfMDFeTranspList;
  protected
    { IXMLInfMunDescarga }
    function Get_CMunDescarga: UnicodeString;
    function Get_XMunDescarga: UnicodeString;
    function Get_InfCTe: IXMLInfCTeList;
    function Get_InfNFe: IXMLInfNFeList;
    function Get_InfMDFeTransp: IXMLInfMDFeTranspList;
    procedure Set_CMunDescarga(Value: UnicodeString);
    procedure Set_XMunDescarga(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfCTe }

  TXMLInfCTe = class(TXMLNode, IXMLInfCTe)
  private
    FInfUnidTransp: IXMLTUnidadeTranspList;
  protected
    { IXMLInfCTe }
    function Get_ChCTe: UnicodeString;
    function Get_SegCodBarra: UnicodeString;
    function Get_InfUnidTransp: IXMLTUnidadeTranspList;
    procedure Set_ChCTe(Value: UnicodeString);
    procedure Set_SegCodBarra(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfCTeList }

  TXMLInfCTeList = class(TXMLNodeCollection, IXMLInfCTeList)
  protected
    { IXMLInfCTeList }
    function Add: IXMLInfCTe;
    function Insert(const Index: Integer): IXMLInfCTe;

    function Get_Item(Index: Integer): IXMLInfCTe;
  end;

{ TXMLTUnidadeTransp }

  TXMLTUnidadeTransp = class(TXMLNode, IXMLTUnidadeTransp)
  private
    FLacUnidTransp: IXMLLacUnidTranspList;
    FInfUnidCarga: IXMLTUnidCargaList;
  protected
    { IXMLTUnidadeTransp }
    function Get_TpUnidTransp: UnicodeString;
    function Get_IdUnidTransp: UnicodeString;
    function Get_LacUnidTransp: IXMLLacUnidTranspList;
    function Get_InfUnidCarga: IXMLTUnidCargaList;
    function Get_QtdRat: UnicodeString;
    procedure Set_TpUnidTransp(Value: UnicodeString);
    procedure Set_IdUnidTransp(Value: UnicodeString);
    procedure Set_QtdRat(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTUnidadeTranspList }

  TXMLTUnidadeTranspList = class(TXMLNodeCollection, IXMLTUnidadeTranspList)
  protected
    { IXMLTUnidadeTranspList }
    function Add: IXMLTUnidadeTransp;
    function Insert(const Index: Integer): IXMLTUnidadeTransp;

    function Get_Item(Index: Integer): IXMLTUnidadeTransp;
  end;

{ TXMLLacUnidTransp }

  TXMLLacUnidTransp = class(TXMLNode, IXMLLacUnidTransp)
  protected
    { IXMLLacUnidTransp }
    function Get_NLacre: UnicodeString;
    procedure Set_NLacre(Value: UnicodeString);
  end;

{ TXMLLacUnidTranspList }

  TXMLLacUnidTranspList = class(TXMLNodeCollection, IXMLLacUnidTranspList)
  protected
    { IXMLLacUnidTranspList }
    function Add: IXMLLacUnidTransp;
    function Insert(const Index: Integer): IXMLLacUnidTransp;

    function Get_Item(Index: Integer): IXMLLacUnidTransp;
  end;

{ TXMLTUnidCarga }

  TXMLTUnidCarga = class(TXMLNode, IXMLTUnidCarga)
  private
    FLacUnidCarga: IXMLLacUnidCargaList;
  protected
    { IXMLTUnidCarga }
    function Get_TpUnidCarga: UnicodeString;
    function Get_IdUnidCarga: UnicodeString;
    function Get_LacUnidCarga: IXMLLacUnidCargaList;
    function Get_QtdRat: UnicodeString;
    procedure Set_TpUnidCarga(Value: UnicodeString);
    procedure Set_IdUnidCarga(Value: UnicodeString);
    procedure Set_QtdRat(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTUnidCargaList }

  TXMLTUnidCargaList = class(TXMLNodeCollection, IXMLTUnidCargaList)
  protected
    { IXMLTUnidCargaList }
    function Add: IXMLTUnidCarga;
    function Insert(const Index: Integer): IXMLTUnidCarga;

    function Get_Item(Index: Integer): IXMLTUnidCarga;
  end;

{ TXMLLacUnidCarga }

  TXMLLacUnidCarga = class(TXMLNode, IXMLLacUnidCarga)
  protected
    { IXMLLacUnidCarga }
    function Get_NLacre: UnicodeString;
    procedure Set_NLacre(Value: UnicodeString);
  end;

{ TXMLLacUnidCargaList }

  TXMLLacUnidCargaList = class(TXMLNodeCollection, IXMLLacUnidCargaList)
  protected
    { IXMLLacUnidCargaList }
    function Add: IXMLLacUnidCarga;
    function Insert(const Index: Integer): IXMLLacUnidCarga;

    function Get_Item(Index: Integer): IXMLLacUnidCarga;
  end;

{ TXMLInfNFe }

  TXMLInfNFe = class(TXMLNode, IXMLInfNFe)
  private
    FInfUnidTransp: IXMLTUnidadeTranspList;
  protected
    { IXMLInfNFe }
    function Get_ChNFe: UnicodeString;
    function Get_SegCodBarra: UnicodeString;
    function Get_InfUnidTransp: IXMLTUnidadeTranspList;
    procedure Set_ChNFe(Value: UnicodeString);
    procedure Set_SegCodBarra(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfNFeList }

  TXMLInfNFeList = class(TXMLNodeCollection, IXMLInfNFeList)
  protected
    { IXMLInfNFeList }
    function Add: IXMLInfNFe;
    function Insert(const Index: Integer): IXMLInfNFe;

    function Get_Item(Index: Integer): IXMLInfNFe;
  end;

{ TXMLInfMDFeTransp }

  TXMLInfMDFeTransp = class(TXMLNode, IXMLInfMDFeTransp)
  private
    FInfUnidTransp: IXMLTUnidadeTranspList;
  protected
    { IXMLInfMDFeTransp }
    function Get_ChMDFe: UnicodeString;
    function Get_InfUnidTransp: IXMLTUnidadeTranspList;
    procedure Set_ChMDFe(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfMDFeTranspList }

  TXMLInfMDFeTranspList = class(TXMLNodeCollection, IXMLInfMDFeTranspList)
  protected
    { IXMLInfMDFeTranspList }
    function Add: IXMLInfMDFeTransp;
    function Insert(const Index: Integer): IXMLInfMDFeTransp;

    function Get_Item(Index: Integer): IXMLInfMDFeTransp;
  end;

{ TXMLTot }

  TXMLTot = class(TXMLNode, IXMLTot)
  protected
    { IXMLTot }
    function Get_QCTe: UnicodeString;
    function Get_QNFe: UnicodeString;
    function Get_QMDFe: UnicodeString;
    function Get_VCarga: UnicodeString;
    function Get_CUnid: UnicodeString;
    function Get_QCarga: UnicodeString;
    procedure Set_QCTe(Value: UnicodeString);
    procedure Set_QNFe(Value: UnicodeString);
    procedure Set_QMDFe(Value: UnicodeString);
    procedure Set_VCarga(Value: UnicodeString);
    procedure Set_CUnid(Value: UnicodeString);
    procedure Set_QCarga(Value: UnicodeString);
  end;

{ TXMLLacres }

  TXMLLacres = class(TXMLNode, IXMLLacres)
  protected
    { IXMLLacres }
    function Get_NLacre: UnicodeString;
    procedure Set_NLacre(Value: UnicodeString);
  end;

{ TXMLLacresList }

  TXMLLacresList = class(TXMLNodeCollection, IXMLLacresList)
  protected
    { IXMLLacresList }
    function Add: IXMLLacres;
    function Insert(const Index: Integer): IXMLLacres;

    function Get_Item(Index: Integer): IXMLLacres;
  end;

{ TXMLAutXML }

  TXMLAutXML = class(TXMLNode, IXMLAutXML)
  protected
    { IXMLAutXML }
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
  end;

{ TXMLAutXMLList }

  TXMLAutXMLList = class(TXMLNodeCollection, IXMLAutXMLList)
  protected
    { IXMLAutXMLList }
    function Add: IXMLAutXML;
    function Insert(const Index: Integer): IXMLAutXML;

    function Get_Item(Index: Integer): IXMLAutXML;
  end;

{ TXMLInfAdic }

  TXMLInfAdic = class(TXMLNode, IXMLInfAdic)
  protected
    { IXMLInfAdic }
    function Get_InfAdFisco: UnicodeString;
    function Get_InfCpl: UnicodeString;
    procedure Set_InfAdFisco(Value: UnicodeString);
    procedure Set_InfCpl(Value: UnicodeString);
  end;

{ TXMLSignatureType_ds }

  TXMLSignatureType_ds = class(TXMLNode, IXMLSignatureType_ds)
  protected
    { IXMLSignatureType_ds }
    function Get_Id: UnicodeString;
    function Get_SignedInfo: IXMLSignedInfoType_ds;
    function Get_SignatureValue: IXMLSignatureValueType_ds;
    function Get_KeyInfo: IXMLKeyInfoType_ds;
    procedure Set_Id(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSignedInfoType_ds }

  TXMLSignedInfoType_ds = class(TXMLNode, IXMLSignedInfoType_ds)
  protected
    { IXMLSignedInfoType_ds }
    function Get_Id: UnicodeString;
    function Get_CanonicalizationMethod: IXMLCanonicalizationMethod_ds;
    function Get_SignatureMethod: IXMLSignatureMethod_ds;
    function Get_Reference: IXMLReferenceType_ds;
    procedure Set_Id(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLCanonicalizationMethod_ds }

  TXMLCanonicalizationMethod_ds = class(TXMLNode, IXMLCanonicalizationMethod_ds)
  protected
    { IXMLCanonicalizationMethod_ds }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
  end;

{ TXMLSignatureMethod_ds }

  TXMLSignatureMethod_ds = class(TXMLNode, IXMLSignatureMethod_ds)
  protected
    { IXMLSignatureMethod_ds }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
  end;

{ TXMLReferenceType_ds }

  TXMLReferenceType_ds = class(TXMLNode, IXMLReferenceType_ds)
  protected
    { IXMLReferenceType_ds }
    function Get_Id: UnicodeString;
    function Get_URI: UnicodeString;
    function Get_Type_: UnicodeString;
    function Get_Transforms: IXMLTransformsType_ds;
    function Get_DigestMethod: IXMLDigestMethod_ds;
    function Get_DigestValue: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_URI(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_DigestValue(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTransformsType_ds }

  TXMLTransformsType_ds = class(TXMLNodeCollection, IXMLTransformsType_ds)
  protected
    { IXMLTransformsType_ds }
    function Get_Transform(Index: Integer): IXMLTransformType_ds;
    function Add: IXMLTransformType_ds;
    function Insert(const Index: Integer): IXMLTransformType_ds;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTransformType_ds }

  TXMLTransformType_ds = class(TXMLNodeCollection, IXMLTransformType_ds)
  protected
    { IXMLTransformType_ds }
    function Get_Algorithm: UnicodeString;
    function Get_XPath(Index: Integer): UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    function Add(const XPath: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const XPath: UnicodeString): IXMLNode;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDigestMethod_ds }

  TXMLDigestMethod_ds = class(TXMLNode, IXMLDigestMethod_ds)
  protected
    { IXMLDigestMethod_ds }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
  end;

{ TXMLSignatureValueType_ds }

  TXMLSignatureValueType_ds = class(TXMLNode, IXMLSignatureValueType_ds)
  protected
    { IXMLSignatureValueType_ds }
    function Get_Id: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
  end;

{ TXMLKeyInfoType_ds }

  TXMLKeyInfoType_ds = class(TXMLNode, IXMLKeyInfoType_ds)
  protected
    { IXMLKeyInfoType_ds }
    function Get_Id: UnicodeString;
    function Get_X509Data: IXMLX509DataType_ds;
    procedure Set_Id(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLX509DataType_ds }

  TXMLX509DataType_ds = class(TXMLNode, IXMLX509DataType_ds)
  protected
    { IXMLX509DataType_ds }
    function Get_X509Certificate: UnicodeString;
    procedure Set_X509Certificate(Value: UnicodeString);
  end;

{ TXMLTEnviMDFe }

  TXMLTEnviMDFe = class(TXMLNode, IXMLTEnviMDFe)
  protected
    { IXMLTEnviMDFe }
    function Get_Versao: UnicodeString;
    function Get_IdLote: UnicodeString;
    function Get_MDFe: IXMLTMDFe;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_IdLote(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTRetEnviMDFe }

  TXMLTRetEnviMDFe = class(TXMLNode, IXMLTRetEnviMDFe)
  protected
    { IXMLTRetEnviMDFe }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    function Get_InfRec: IXMLInfRec;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfRec }

  TXMLInfRec = class(TXMLNode, IXMLInfRec)
  protected
    { IXMLInfRec }
    function Get_NRec: UnicodeString;
    function Get_DhRecbto: UnicodeString;
    function Get_TMed: Integer;
    procedure Set_NRec(Value: UnicodeString);
    procedure Set_DhRecbto(Value: UnicodeString);
    procedure Set_TMed(Value: Integer);
  end;

{ TXMLTEndereco }

  TXMLTEndereco = class(TXMLNode, IXMLTEndereco)
  protected
    { IXMLTEndereco }
    function Get_XLgr: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_XCpl: UnicodeString;
    function Get_XBairro: UnicodeString;
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_CEP: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_CPais: UnicodeString;
    function Get_XPais: UnicodeString;
    procedure Set_XLgr(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_XCpl(Value: UnicodeString);
    procedure Set_XBairro(Value: UnicodeString);
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_CEP(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_CPais(Value: UnicodeString);
    procedure Set_XPais(Value: UnicodeString);
  end;

{ TXMLTEndernac }

  TXMLTEndernac = class(TXMLNode, IXMLTEndernac)
  protected
    { IXMLTEndernac }
    function Get_XLgr: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_XCpl: UnicodeString;
    function Get_XBairro: UnicodeString;
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_CEP: UnicodeString;
    function Get_UF: UnicodeString;
    procedure Set_XLgr(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_XCpl(Value: UnicodeString);
    procedure Set_XBairro(Value: UnicodeString);
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_CEP(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
  end;

{ TXMLTEnderFer }

  TXMLTEnderFer = class(TXMLNode, IXMLTEnderFer)
  protected
    { IXMLTEnderFer }
    function Get_XLgr: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_XCpl: UnicodeString;
    function Get_XBairro: UnicodeString;
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_CEP: UnicodeString;
    function Get_UF: UnicodeString;
    procedure Set_XLgr(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_XCpl(Value: UnicodeString);
    procedure Set_XBairro(Value: UnicodeString);
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_CEP(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
  end;

{ TXMLTEndOrg }

  TXMLTEndOrg = class(TXMLNode, IXMLTEndOrg)
  protected
    { IXMLTEndOrg }
    function Get_XLgr: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_XCpl: UnicodeString;
    function Get_XBairro: UnicodeString;
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_CEP: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_CPais: UnicodeString;
    function Get_XPais: UnicodeString;
    function Get_Fone: UnicodeString;
    procedure Set_XLgr(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_XCpl(Value: UnicodeString);
    procedure Set_XBairro(Value: UnicodeString);
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_CEP(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_CPais(Value: UnicodeString);
    procedure Set_XPais(Value: UnicodeString);
    procedure Set_Fone(Value: UnicodeString);
  end;

{ TXMLTLocal }

  TXMLTLocal = class(TXMLNode, IXMLTLocal)
  protected
    { IXMLTLocal }
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_UF: UnicodeString;
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
  end;

{ TXMLTEndReEnt }

  TXMLTEndReEnt = class(TXMLNode, IXMLTEndReEnt)
  protected
    { IXMLTEndReEnt }
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_XLgr: UnicodeString;
    function Get_Nro: UnicodeString;
    function Get_XCpl: UnicodeString;
    function Get_XBairro: UnicodeString;
    function Get_CMun: UnicodeString;
    function Get_XMun: UnicodeString;
    function Get_UF: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_XLgr(Value: UnicodeString);
    procedure Set_Nro(Value: UnicodeString);
    procedure Set_XCpl(Value: UnicodeString);
    procedure Set_XBairro(Value: UnicodeString);
    procedure Set_CMun(Value: UnicodeString);
    procedure Set_XMun(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
  end;

{ TXMLTNFeNF }

  TXMLTNFeNF = class(TXMLNode, IXMLTNFeNF)
  protected
    { IXMLTNFeNF }
    function Get_InfNFe: IXMLInfNFe;
    function Get_InfNF: IXMLInfNF;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfNF }

  TXMLInfNF = class(TXMLNode, IXMLInfNF)
  protected
    { IXMLInfNF }
    function Get_Emi: IXMLEmi;
    function Get_Dest: IXMLDest;
    function Get_Serie: UnicodeString;
    function Get_NNF: UnicodeString;
    function Get_DEmi: UnicodeString;
    function Get_VNF: UnicodeString;
    function Get_PIN: UnicodeString;
    procedure Set_Serie(Value: UnicodeString);
    procedure Set_NNF(Value: UnicodeString);
    procedure Set_DEmi(Value: UnicodeString);
    procedure Set_VNF(Value: UnicodeString);
    procedure Set_PIN(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLEmi }

  TXMLEmi = class(TXMLNode, IXMLEmi)
  protected
    { IXMLEmi }
    function Get_CNPJ: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_UF: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
  end;

{ TXMLDest }

  TXMLDest = class(TXMLNode, IXMLDest)
  protected
    { IXMLDest }
    function Get_CNPJ: UnicodeString;
    function Get_CPF: UnicodeString;
    function Get_XNome: UnicodeString;
    function Get_UF: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CPF(Value: UnicodeString);
    procedure Set_XNome(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
  end;

{ Global Functions }

function GetMDFe(Doc: IXMLDocument): IXMLTMDFe;
function LoadMDFe(const FileName: string): IXMLTMDFe;
function NewMDFe: IXMLTMDFe;

const
  TargetNamespace = 'http://www.portalfiscal.inf.br/mdfe';

implementation

{ Global Functions }

function GetMDFe(Doc: IXMLDocument): IXMLTMDFe;
begin
  Result := Doc.GetDocBinding('MDFe', TXMLTMDFe, TargetNamespace) as IXMLTMDFe;
end;

function LoadMDFe(const FileName: string): IXMLTMDFe;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('MDFe', TXMLTMDFe, TargetNamespace) as IXMLTMDFe;
end;

function NewMDFe: IXMLTMDFe;
begin
  Result := NewXMLDocument.GetDocBinding('MDFe', TXMLTMDFe, TargetNamespace) as IXMLTMDFe;
end;


{ TXMLTMDFe }

procedure TXMLTMDFe.AfterConstruction;
begin
  RegisterChildNode('infMDFe', TXMLInfMDFe);
  RegisterChildNode('Signature', TXMLSignatureType_ds);
  inherited;
end;

function TXMLTMDFe.Get_InfMDFe: IXMLInfMDFe;
begin
  Result := ChildNodes['infMDFe'] as IXMLInfMDFe;
end;

function TXMLTMDFe.Get_Signature: IXMLSignatureType_ds;
begin
  Result := ChildNodes['Signature'] as IXMLSignatureType_ds;
end;

{ TXMLInfMDFe }

procedure TXMLInfMDFe.AfterConstruction;
begin
  RegisterChildNode('ide', TXMLIde);
  RegisterChildNode('emit', TXMLEmit);
  RegisterChildNode('infModal', TXMLInfModal);
  RegisterChildNode('infDoc', TXMLInfDoc);
  RegisterChildNode('tot', TXMLTot);
  RegisterChildNode('lacres', TXMLLacres);
  RegisterChildNode('autXML', TXMLAutXML);
  RegisterChildNode('infAdic', TXMLInfAdic);
  FLacres := CreateCollection(TXMLLacresList, IXMLLacres, 'lacres') as IXMLLacresList;
  FAutXML := CreateCollection(TXMLAutXMLList, IXMLAutXML, 'autXML') as IXMLAutXMLList;
  inherited;
end;

function TXMLInfMDFe.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLInfMDFe.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLInfMDFe.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLInfMDFe.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLInfMDFe.Get_Ide: IXMLIde;
begin
  Result := ChildNodes['ide'] as IXMLIde;
end;

function TXMLInfMDFe.Get_Emit: IXMLEmit;
begin
  Result := ChildNodes['emit'] as IXMLEmit;
end;

function TXMLInfMDFe.Get_InfModal: IXMLInfModal;
begin
  Result := ChildNodes['infModal'] as IXMLInfModal;
end;

function TXMLInfMDFe.Get_InfDoc: IXMLInfDoc;
begin
  Result := ChildNodes['infDoc'] as IXMLInfDoc;
end;

function TXMLInfMDFe.Get_Tot: IXMLTot;
begin
  Result := ChildNodes['tot'] as IXMLTot;
end;

function TXMLInfMDFe.Get_Lacres: IXMLLacresList;
begin
  Result := FLacres;
end;

function TXMLInfMDFe.Get_AutXML: IXMLAutXMLList;
begin
  Result := FAutXML;
end;

function TXMLInfMDFe.Get_InfAdic: IXMLInfAdic;
begin
  Result := ChildNodes['infAdic'] as IXMLInfAdic;
end;

{ TXMLIde }

procedure TXMLIde.AfterConstruction;
begin
  RegisterChildNode('infMunCarrega', TXMLInfMunCarrega);
  RegisterChildNode('infPercurso', TXMLInfPercurso);
  FInfMunCarrega := CreateCollection(TXMLInfMunCarregaList, IXMLInfMunCarrega, 'infMunCarrega') as IXMLInfMunCarregaList;
  FInfPercurso := CreateCollection(TXMLInfPercursoList, IXMLInfPercurso, 'infPercurso') as IXMLInfPercursoList;
  inherited;
end;

function TXMLIde.Get_CUF: UnicodeString;
begin
  Result := ChildNodes['cUF'].Text;
end;

procedure TXMLIde.Set_CUF(Value: UnicodeString);
begin
  ChildNodes['cUF'].NodeValue := Value;
end;

function TXMLIde.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLIde.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLIde.Get_TpEmit: UnicodeString;
begin
  Result := ChildNodes['tpEmit'].Text;
end;

procedure TXMLIde.Set_TpEmit(Value: UnicodeString);
begin
  ChildNodes['tpEmit'].NodeValue := Value;
end;

function TXMLIde.Get_Mod_: UnicodeString;
begin
  Result := ChildNodes['mod'].Text;
end;

procedure TXMLIde.Set_Mod_(Value: UnicodeString);
begin
  ChildNodes['mod'].NodeValue := Value;
end;

function TXMLIde.Get_Serie: UnicodeString;
begin
  Result := ChildNodes['serie'].Text;
end;

procedure TXMLIde.Set_Serie(Value: UnicodeString);
begin
  ChildNodes['serie'].NodeValue := Value;
end;

function TXMLIde.Get_NMDF: UnicodeString;
begin
  Result := ChildNodes['nMDF'].Text;
end;

procedure TXMLIde.Set_NMDF(Value: UnicodeString);
begin
  ChildNodes['nMDF'].NodeValue := Value;
end;

function TXMLIde.Get_CMDF: UnicodeString;
begin
  Result := ChildNodes['cMDF'].Text;
end;

procedure TXMLIde.Set_CMDF(Value: UnicodeString);
begin
  ChildNodes['cMDF'].NodeValue := Value;
end;

function TXMLIde.Get_CDV: UnicodeString;
begin
  Result := ChildNodes['cDV'].Text;
end;

procedure TXMLIde.Set_CDV(Value: UnicodeString);
begin
  ChildNodes['cDV'].NodeValue := Value;
end;

function TXMLIde.Get_Modal: UnicodeString;
begin
  Result := ChildNodes['modal'].Text;
end;

procedure TXMLIde.Set_Modal(Value: UnicodeString);
begin
  ChildNodes['modal'].NodeValue := Value;
end;

function TXMLIde.Get_DhEmi: UnicodeString;
begin
  Result := ChildNodes['dhEmi'].Text;
end;

procedure TXMLIde.Set_DhEmi(Value: UnicodeString);
begin
  ChildNodes['dhEmi'].NodeValue := Value;
end;

function TXMLIde.Get_TpEmis: UnicodeString;
begin
  Result := ChildNodes['tpEmis'].Text;
end;

procedure TXMLIde.Set_TpEmis(Value: UnicodeString);
begin
  ChildNodes['tpEmis'].NodeValue := Value;
end;

function TXMLIde.Get_ProcEmi: UnicodeString;
begin
  Result := ChildNodes['procEmi'].Text;
end;

procedure TXMLIde.Set_ProcEmi(Value: UnicodeString);
begin
  ChildNodes['procEmi'].NodeValue := Value;
end;

function TXMLIde.Get_VerProc: UnicodeString;
begin
  Result := ChildNodes['verProc'].Text;
end;

procedure TXMLIde.Set_VerProc(Value: UnicodeString);
begin
  ChildNodes['verProc'].NodeValue := Value;
end;

function TXMLIde.Get_UFIni: UnicodeString;
begin
  Result := ChildNodes['UFIni'].Text;
end;

procedure TXMLIde.Set_UFIni(Value: UnicodeString);
begin
  ChildNodes['UFIni'].NodeValue := Value;
end;

function TXMLIde.Get_UFFim: UnicodeString;
begin
  Result := ChildNodes['UFFim'].Text;
end;

procedure TXMLIde.Set_UFFim(Value: UnicodeString);
begin
  ChildNodes['UFFim'].NodeValue := Value;
end;

function TXMLIde.Get_InfMunCarrega: IXMLInfMunCarregaList;
begin
  Result := FInfMunCarrega;
end;

function TXMLIde.Get_InfPercurso: IXMLInfPercursoList;
begin
  Result := FInfPercurso;
end;

function TXMLIde.Get_DhIniViagem: UnicodeString;
begin
  Result := ChildNodes['dhIniViagem'].Text;
end;

procedure TXMLIde.Set_DhIniViagem(Value: UnicodeString);
begin
  ChildNodes['dhIniViagem'].NodeValue := Value;
end;

{ TXMLInfMunCarrega }

function TXMLInfMunCarrega.Get_CMunCarrega: UnicodeString;
begin
  Result := ChildNodes['cMunCarrega'].Text;
end;

procedure TXMLInfMunCarrega.Set_CMunCarrega(Value: UnicodeString);
begin
  ChildNodes['cMunCarrega'].NodeValue := Value;
end;

function TXMLInfMunCarrega.Get_XMunCarrega: UnicodeString;
begin
  Result := ChildNodes['xMunCarrega'].Text;
end;

procedure TXMLInfMunCarrega.Set_XMunCarrega(Value: UnicodeString);
begin
  ChildNodes['xMunCarrega'].NodeValue := Value;
end;

{ TXMLInfMunCarregaList }

function TXMLInfMunCarregaList.Add: IXMLInfMunCarrega;
begin
  Result := AddItem(-1) as IXMLInfMunCarrega;
end;

function TXMLInfMunCarregaList.Insert(const Index: Integer): IXMLInfMunCarrega;
begin
  Result := AddItem(Index) as IXMLInfMunCarrega;
end;

function TXMLInfMunCarregaList.Get_Item(Index: Integer): IXMLInfMunCarrega;
begin
  Result := List[Index] as IXMLInfMunCarrega;
end;

{ TXMLInfPercurso }

function TXMLInfPercurso.Get_UFPer: UnicodeString;
begin
  Result := ChildNodes['UFPer'].Text;
end;

procedure TXMLInfPercurso.Set_UFPer(Value: UnicodeString);
begin
  ChildNodes['UFPer'].NodeValue := Value;
end;

{ TXMLInfPercursoList }

function TXMLInfPercursoList.Add: IXMLInfPercurso;
begin
  Result := AddItem(-1) as IXMLInfPercurso;
end;

function TXMLInfPercursoList.Insert(const Index: Integer): IXMLInfPercurso;
begin
  Result := AddItem(Index) as IXMLInfPercurso;
end;

function TXMLInfPercursoList.Get_Item(Index: Integer): IXMLInfPercurso;
begin
  Result := List[Index] as IXMLInfPercurso;
end;

{ TXMLEmit }

procedure TXMLEmit.AfterConstruction;
begin
  RegisterChildNode('enderEmit', TXMLTEndeEmi);
  inherited;
end;

function TXMLEmit.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLEmit.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLEmit.Get_IE: UnicodeString;
begin
  Result := ChildNodes['IE'].Text;
end;

procedure TXMLEmit.Set_IE(Value: UnicodeString);
begin
  ChildNodes['IE'].NodeValue := Value;
end;

function TXMLEmit.Get_XNome: UnicodeString;
begin
  Result := ChildNodes['xNome'].Text;
end;

procedure TXMLEmit.Set_XNome(Value: UnicodeString);
begin
  ChildNodes['xNome'].NodeValue := Value;
end;

function TXMLEmit.Get_XFant: UnicodeString;
begin
  Result := ChildNodes['xFant'].Text;
end;

procedure TXMLEmit.Set_XFant(Value: UnicodeString);
begin
  ChildNodes['xFant'].NodeValue := Value;
end;

function TXMLEmit.Get_EnderEmit: IXMLTEndeEmi;
begin
  Result := ChildNodes['enderEmit'] as IXMLTEndeEmi;
end;

{ TXMLTEndeEmi }

function TXMLTEndeEmi.Get_XLgr: UnicodeString;
begin
  Result := ChildNodes['xLgr'].Text;
end;

procedure TXMLTEndeEmi.Set_XLgr(Value: UnicodeString);
begin
  ChildNodes['xLgr'].NodeValue := Value;
end;

function TXMLTEndeEmi.Get_Nro: UnicodeString;
begin
  Result := ChildNodes['nro'].Text;
end;

procedure TXMLTEndeEmi.Set_Nro(Value: UnicodeString);
begin
  ChildNodes['nro'].NodeValue := Value;
end;

function TXMLTEndeEmi.Get_XCpl: UnicodeString;
begin
  Result := ChildNodes['xCpl'].Text;
end;

procedure TXMLTEndeEmi.Set_XCpl(Value: UnicodeString);
begin
  ChildNodes['xCpl'].NodeValue := Value;
end;

function TXMLTEndeEmi.Get_XBairro: UnicodeString;
begin
  Result := ChildNodes['xBairro'].Text;
end;

procedure TXMLTEndeEmi.Set_XBairro(Value: UnicodeString);
begin
  ChildNodes['xBairro'].NodeValue := Value;
end;

function TXMLTEndeEmi.Get_CMun: UnicodeString;
begin
  Result := ChildNodes['cMun'].Text;
end;

procedure TXMLTEndeEmi.Set_CMun(Value: UnicodeString);
begin
  ChildNodes['cMun'].NodeValue := Value;
end;

function TXMLTEndeEmi.Get_XMun: UnicodeString;
begin
  Result := ChildNodes['xMun'].Text;
end;

procedure TXMLTEndeEmi.Set_XMun(Value: UnicodeString);
begin
  ChildNodes['xMun'].NodeValue := Value;
end;

function TXMLTEndeEmi.Get_CEP: UnicodeString;
begin
  Result := ChildNodes['CEP'].Text;
end;

procedure TXMLTEndeEmi.Set_CEP(Value: UnicodeString);
begin
  ChildNodes['CEP'].NodeValue := Value;
end;

function TXMLTEndeEmi.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLTEndeEmi.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

function TXMLTEndeEmi.Get_Fone: UnicodeString;
begin
  Result := ChildNodes['fone'].Text;
end;

procedure TXMLTEndeEmi.Set_Fone(Value: UnicodeString);
begin
  ChildNodes['fone'].NodeValue := Value;
end;

function TXMLTEndeEmi.Get_Email: UnicodeString;
begin
  Result := ChildNodes['email'].Text;
end;

procedure TXMLTEndeEmi.Set_Email(Value: UnicodeString);
begin
  ChildNodes['email'].NodeValue := Value;
end;

{ TXMLInfModal }

function TXMLInfModal.Get_VersaoModal: UnicodeString;
begin
  Result := AttributeNodes['versaoModal'].Text;
end;

procedure TXMLInfModal.Set_VersaoModal(Value: UnicodeString);
begin
  SetAttribute('versaoModal', Value);
end;

{ TXMLInfDoc }

procedure TXMLInfDoc.AfterConstruction;
begin
  RegisterChildNode('infMunDescarga', TXMLInfMunDescarga);
  ItemTag := 'infMunDescarga';
  ItemInterface := IXMLInfMunDescarga;
  inherited;
end;

function TXMLInfDoc.Get_InfMunDescarga(Index: Integer): IXMLInfMunDescarga;
begin
  Result := List[Index] as IXMLInfMunDescarga;
end;

function TXMLInfDoc.Add: IXMLInfMunDescarga;
begin
  Result := AddItem(-1) as IXMLInfMunDescarga;
end;

function TXMLInfDoc.Insert(const Index: Integer): IXMLInfMunDescarga;
begin
  Result := AddItem(Index) as IXMLInfMunDescarga;
end;

{ TXMLInfMunDescarga }

procedure TXMLInfMunDescarga.AfterConstruction;
begin
  RegisterChildNode('infCTe', TXMLInfCTe);
  RegisterChildNode('infNFe', TXMLInfNFe);
  RegisterChildNode('infMDFeTransp', TXMLInfMDFeTransp);
  FInfCTe := CreateCollection(TXMLInfCTeList, IXMLInfCTe, 'infCTe') as IXMLInfCTeList;
  FInfNFe := CreateCollection(TXMLInfNFeList, IXMLInfNFe, 'infNFe') as IXMLInfNFeList;
  FInfMDFeTransp := CreateCollection(TXMLInfMDFeTranspList, IXMLInfMDFeTransp, 'infMDFeTransp') as IXMLInfMDFeTranspList;
  inherited;
end;

function TXMLInfMunDescarga.Get_CMunDescarga: UnicodeString;
begin
  Result := ChildNodes['cMunDescarga'].Text;
end;

procedure TXMLInfMunDescarga.Set_CMunDescarga(Value: UnicodeString);
begin
  ChildNodes['cMunDescarga'].NodeValue := Value;
end;

function TXMLInfMunDescarga.Get_XMunDescarga: UnicodeString;
begin
  Result := ChildNodes['xMunDescarga'].Text;
end;

procedure TXMLInfMunDescarga.Set_XMunDescarga(Value: UnicodeString);
begin
  ChildNodes['xMunDescarga'].NodeValue := Value;
end;

function TXMLInfMunDescarga.Get_InfCTe: IXMLInfCTeList;
begin
  Result := FInfCTe;
end;

function TXMLInfMunDescarga.Get_InfNFe: IXMLInfNFeList;
begin
  Result := FInfNFe;
end;

function TXMLInfMunDescarga.Get_InfMDFeTransp: IXMLInfMDFeTranspList;
begin
  Result := FInfMDFeTransp;
end;

{ TXMLInfCTe }

procedure TXMLInfCTe.AfterConstruction;
begin
  RegisterChildNode('infUnidTransp', TXMLTUnidadeTransp);
  FInfUnidTransp := CreateCollection(TXMLTUnidadeTranspList, IXMLTUnidadeTransp, 'infUnidTransp') as IXMLTUnidadeTranspList;
  inherited;
end;

function TXMLInfCTe.Get_ChCTe: UnicodeString;
begin
  Result := ChildNodes['chCTe'].Text;
end;

procedure TXMLInfCTe.Set_ChCTe(Value: UnicodeString);
begin
  ChildNodes['chCTe'].NodeValue := Value;
end;

function TXMLInfCTe.Get_SegCodBarra: UnicodeString;
begin
  Result := ChildNodes['SegCodBarra'].Text;
end;

procedure TXMLInfCTe.Set_SegCodBarra(Value: UnicodeString);
begin
  ChildNodes['SegCodBarra'].NodeValue := Value;
end;

function TXMLInfCTe.Get_InfUnidTransp: IXMLTUnidadeTranspList;
begin
  Result := FInfUnidTransp;
end;

{ TXMLInfCTeList }

function TXMLInfCTeList.Add: IXMLInfCTe;
begin
  Result := AddItem(-1) as IXMLInfCTe;
end;

function TXMLInfCTeList.Insert(const Index: Integer): IXMLInfCTe;
begin
  Result := AddItem(Index) as IXMLInfCTe;
end;

function TXMLInfCTeList.Get_Item(Index: Integer): IXMLInfCTe;
begin
  Result := List[Index] as IXMLInfCTe;
end;

{ TXMLTUnidadeTransp }

procedure TXMLTUnidadeTransp.AfterConstruction;
begin
  RegisterChildNode('lacUnidTransp', TXMLLacUnidTransp);
  RegisterChildNode('infUnidCarga', TXMLTUnidCarga);
  FLacUnidTransp := CreateCollection(TXMLLacUnidTranspList, IXMLLacUnidTransp, 'lacUnidTransp') as IXMLLacUnidTranspList;
  FInfUnidCarga := CreateCollection(TXMLTUnidCargaList, IXMLTUnidCarga, 'infUnidCarga') as IXMLTUnidCargaList;
  inherited;
end;

function TXMLTUnidadeTransp.Get_TpUnidTransp: UnicodeString;
begin
  Result := ChildNodes['tpUnidTransp'].Text;
end;

procedure TXMLTUnidadeTransp.Set_TpUnidTransp(Value: UnicodeString);
begin
  ChildNodes['tpUnidTransp'].NodeValue := Value;
end;

function TXMLTUnidadeTransp.Get_IdUnidTransp: UnicodeString;
begin
  Result := ChildNodes['idUnidTransp'].Text;
end;

procedure TXMLTUnidadeTransp.Set_IdUnidTransp(Value: UnicodeString);
begin
  ChildNodes['idUnidTransp'].NodeValue := Value;
end;

function TXMLTUnidadeTransp.Get_LacUnidTransp: IXMLLacUnidTranspList;
begin
  Result := FLacUnidTransp;
end;

function TXMLTUnidadeTransp.Get_InfUnidCarga: IXMLTUnidCargaList;
begin
  Result := FInfUnidCarga;
end;

function TXMLTUnidadeTransp.Get_QtdRat: UnicodeString;
begin
  Result := ChildNodes['qtdRat'].Text;
end;

procedure TXMLTUnidadeTransp.Set_QtdRat(Value: UnicodeString);
begin
  ChildNodes['qtdRat'].NodeValue := Value;
end;

{ TXMLTUnidadeTranspList }

function TXMLTUnidadeTranspList.Add: IXMLTUnidadeTransp;
begin
  Result := AddItem(-1) as IXMLTUnidadeTransp;
end;

function TXMLTUnidadeTranspList.Insert(const Index: Integer): IXMLTUnidadeTransp;
begin
  Result := AddItem(Index) as IXMLTUnidadeTransp;
end;

function TXMLTUnidadeTranspList.Get_Item(Index: Integer): IXMLTUnidadeTransp;
begin
  Result := List[Index] as IXMLTUnidadeTransp;
end;

{ TXMLLacUnidTransp }

function TXMLLacUnidTransp.Get_NLacre: UnicodeString;
begin
  Result := ChildNodes['nLacre'].Text;
end;

procedure TXMLLacUnidTransp.Set_NLacre(Value: UnicodeString);
begin
  ChildNodes['nLacre'].NodeValue := Value;
end;

{ TXMLLacUnidTranspList }

function TXMLLacUnidTranspList.Add: IXMLLacUnidTransp;
begin
  Result := AddItem(-1) as IXMLLacUnidTransp;
end;

function TXMLLacUnidTranspList.Insert(const Index: Integer): IXMLLacUnidTransp;
begin
  Result := AddItem(Index) as IXMLLacUnidTransp;
end;

function TXMLLacUnidTranspList.Get_Item(Index: Integer): IXMLLacUnidTransp;
begin
  Result := List[Index] as IXMLLacUnidTransp;
end;

{ TXMLTUnidCarga }

procedure TXMLTUnidCarga.AfterConstruction;
begin
  RegisterChildNode('lacUnidCarga', TXMLLacUnidCarga);
  FLacUnidCarga := CreateCollection(TXMLLacUnidCargaList, IXMLLacUnidCarga, 'lacUnidCarga') as IXMLLacUnidCargaList;
  inherited;
end;

function TXMLTUnidCarga.Get_TpUnidCarga: UnicodeString;
begin
  Result := ChildNodes['tpUnidCarga'].Text;
end;

procedure TXMLTUnidCarga.Set_TpUnidCarga(Value: UnicodeString);
begin
  ChildNodes['tpUnidCarga'].NodeValue := Value;
end;

function TXMLTUnidCarga.Get_IdUnidCarga: UnicodeString;
begin
  Result := ChildNodes['idUnidCarga'].Text;
end;

procedure TXMLTUnidCarga.Set_IdUnidCarga(Value: UnicodeString);
begin
  ChildNodes['idUnidCarga'].NodeValue := Value;
end;

function TXMLTUnidCarga.Get_LacUnidCarga: IXMLLacUnidCargaList;
begin
  Result := FLacUnidCarga;
end;

function TXMLTUnidCarga.Get_QtdRat: UnicodeString;
begin
  Result := ChildNodes['qtdRat'].Text;
end;

procedure TXMLTUnidCarga.Set_QtdRat(Value: UnicodeString);
begin
  ChildNodes['qtdRat'].NodeValue := Value;
end;

{ TXMLTUnidCargaList }

function TXMLTUnidCargaList.Add: IXMLTUnidCarga;
begin
  Result := AddItem(-1) as IXMLTUnidCarga;
end;

function TXMLTUnidCargaList.Insert(const Index: Integer): IXMLTUnidCarga;
begin
  Result := AddItem(Index) as IXMLTUnidCarga;
end;

function TXMLTUnidCargaList.Get_Item(Index: Integer): IXMLTUnidCarga;
begin
  Result := List[Index] as IXMLTUnidCarga;
end;

{ TXMLLacUnidCarga }

function TXMLLacUnidCarga.Get_NLacre: UnicodeString;
begin
  Result := ChildNodes['nLacre'].Text;
end;

procedure TXMLLacUnidCarga.Set_NLacre(Value: UnicodeString);
begin
  ChildNodes['nLacre'].NodeValue := Value;
end;

{ TXMLLacUnidCargaList }

function TXMLLacUnidCargaList.Add: IXMLLacUnidCarga;
begin
  Result := AddItem(-1) as IXMLLacUnidCarga;
end;

function TXMLLacUnidCargaList.Insert(const Index: Integer): IXMLLacUnidCarga;
begin
  Result := AddItem(Index) as IXMLLacUnidCarga;
end;

function TXMLLacUnidCargaList.Get_Item(Index: Integer): IXMLLacUnidCarga;
begin
  Result := List[Index] as IXMLLacUnidCarga;
end;

{ TXMLInfNFe }

procedure TXMLInfNFe.AfterConstruction;
begin
  RegisterChildNode('infUnidTransp', TXMLTUnidadeTransp);
  FInfUnidTransp := CreateCollection(TXMLTUnidadeTranspList, IXMLTUnidadeTransp, 'infUnidTransp') as IXMLTUnidadeTranspList;
  inherited;
end;

function TXMLInfNFe.Get_ChNFe: UnicodeString;
begin
  Result := ChildNodes['chNFe'].Text;
end;

procedure TXMLInfNFe.Set_ChNFe(Value: UnicodeString);
begin
  ChildNodes['chNFe'].NodeValue := Value;
end;

function TXMLInfNFe.Get_SegCodBarra: UnicodeString;
begin
  Result := ChildNodes['SegCodBarra'].Text;
end;

procedure TXMLInfNFe.Set_SegCodBarra(Value: UnicodeString);
begin
  ChildNodes['SegCodBarra'].NodeValue := Value;
end;

function TXMLInfNFe.Get_InfUnidTransp: IXMLTUnidadeTranspList;
begin
  Result := FInfUnidTransp;
end;

{ TXMLInfNFeList }

function TXMLInfNFeList.Add: IXMLInfNFe;
begin
  Result := AddItem(-1) as IXMLInfNFe;
end;

function TXMLInfNFeList.Insert(const Index: Integer): IXMLInfNFe;
begin
  Result := AddItem(Index) as IXMLInfNFe;
end;

function TXMLInfNFeList.Get_Item(Index: Integer): IXMLInfNFe;
begin
  Result := List[Index] as IXMLInfNFe;
end;

{ TXMLInfMDFeTransp }

procedure TXMLInfMDFeTransp.AfterConstruction;
begin
  RegisterChildNode('infUnidTransp', TXMLTUnidadeTransp);
  FInfUnidTransp := CreateCollection(TXMLTUnidadeTranspList, IXMLTUnidadeTransp, 'infUnidTransp') as IXMLTUnidadeTranspList;
  inherited;
end;

function TXMLInfMDFeTransp.Get_ChMDFe: UnicodeString;
begin
  Result := ChildNodes['chMDFe'].Text;
end;

procedure TXMLInfMDFeTransp.Set_ChMDFe(Value: UnicodeString);
begin
  ChildNodes['chMDFe'].NodeValue := Value;
end;

function TXMLInfMDFeTransp.Get_InfUnidTransp: IXMLTUnidadeTranspList;
begin
  Result := FInfUnidTransp;
end;

{ TXMLInfMDFeTranspList }

function TXMLInfMDFeTranspList.Add: IXMLInfMDFeTransp;
begin
  Result := AddItem(-1) as IXMLInfMDFeTransp;
end;

function TXMLInfMDFeTranspList.Insert(const Index: Integer): IXMLInfMDFeTransp;
begin
  Result := AddItem(Index) as IXMLInfMDFeTransp;
end;

function TXMLInfMDFeTranspList.Get_Item(Index: Integer): IXMLInfMDFeTransp;
begin
  Result := List[Index] as IXMLInfMDFeTransp;
end;

{ TXMLTot }

function TXMLTot.Get_QCTe: UnicodeString;
begin
  Result := ChildNodes['qCTe'].Text;
end;

procedure TXMLTot.Set_QCTe(Value: UnicodeString);
begin
  ChildNodes['qCTe'].NodeValue := Value;
end;

function TXMLTot.Get_QNFe: UnicodeString;
begin
  Result := ChildNodes['qNFe'].Text;
end;

procedure TXMLTot.Set_QNFe(Value: UnicodeString);
begin
  ChildNodes['qNFe'].NodeValue := Value;
end;

function TXMLTot.Get_QMDFe: UnicodeString;
begin
  Result := ChildNodes['qMDFe'].Text;
end;

procedure TXMLTot.Set_QMDFe(Value: UnicodeString);
begin
  ChildNodes['qMDFe'].NodeValue := Value;
end;

function TXMLTot.Get_VCarga: UnicodeString;
begin
  Result := ChildNodes['vCarga'].Text;
end;

procedure TXMLTot.Set_VCarga(Value: UnicodeString);
begin
  ChildNodes['vCarga'].NodeValue := Value;
end;

function TXMLTot.Get_CUnid: UnicodeString;
begin
  Result := ChildNodes['cUnid'].Text;
end;

procedure TXMLTot.Set_CUnid(Value: UnicodeString);
begin
  ChildNodes['cUnid'].NodeValue := Value;
end;

function TXMLTot.Get_QCarga: UnicodeString;
begin
  Result := ChildNodes['qCarga'].Text;
end;

procedure TXMLTot.Set_QCarga(Value: UnicodeString);
begin
  ChildNodes['qCarga'].NodeValue := Value;
end;

{ TXMLLacres }

function TXMLLacres.Get_NLacre: UnicodeString;
begin
  Result := ChildNodes['nLacre'].Text;
end;

procedure TXMLLacres.Set_NLacre(Value: UnicodeString);
begin
  ChildNodes['nLacre'].NodeValue := Value;
end;

{ TXMLLacresList }

function TXMLLacresList.Add: IXMLLacres;
begin
  Result := AddItem(-1) as IXMLLacres;
end;

function TXMLLacresList.Insert(const Index: Integer): IXMLLacres;
begin
  Result := AddItem(Index) as IXMLLacres;
end;

function TXMLLacresList.Get_Item(Index: Integer): IXMLLacres;
begin
  Result := List[Index] as IXMLLacres;
end;

{ TXMLAutXML }

function TXMLAutXML.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLAutXML.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLAutXML.Get_CPF: UnicodeString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLAutXML.Set_CPF(Value: UnicodeString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

{ TXMLAutXMLList }

function TXMLAutXMLList.Add: IXMLAutXML;
begin
  Result := AddItem(-1) as IXMLAutXML;
end;

function TXMLAutXMLList.Insert(const Index: Integer): IXMLAutXML;
begin
  Result := AddItem(Index) as IXMLAutXML;
end;

function TXMLAutXMLList.Get_Item(Index: Integer): IXMLAutXML;
begin
  Result := List[Index] as IXMLAutXML;
end;

{ TXMLInfAdic }

function TXMLInfAdic.Get_InfAdFisco: UnicodeString;
begin
  Result := ChildNodes['infAdFisco'].Text;
end;

procedure TXMLInfAdic.Set_InfAdFisco(Value: UnicodeString);
begin
  ChildNodes['infAdFisco'].NodeValue := Value;
end;

function TXMLInfAdic.Get_InfCpl: UnicodeString;
begin
  Result := ChildNodes['infCpl'].Text;
end;

procedure TXMLInfAdic.Set_InfCpl(Value: UnicodeString);
begin
  ChildNodes['infCpl'].NodeValue := Value;
end;

{ TXMLSignatureType_ds }

procedure TXMLSignatureType_ds.AfterConstruction;
begin
  RegisterChildNode('SignedInfo', TXMLSignedInfoType_ds);
  RegisterChildNode('SignatureValue', TXMLSignatureValueType_ds);
  RegisterChildNode('KeyInfo', TXMLKeyInfoType_ds);
  inherited;
end;

function TXMLSignatureType_ds.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLSignatureType_ds.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLSignatureType_ds.Get_SignedInfo: IXMLSignedInfoType_ds;
begin
  Result := ChildNodes['SignedInfo'] as IXMLSignedInfoType_ds;
end;

function TXMLSignatureType_ds.Get_SignatureValue: IXMLSignatureValueType_ds;
begin
  Result := ChildNodes['SignatureValue'] as IXMLSignatureValueType_ds;
end;

function TXMLSignatureType_ds.Get_KeyInfo: IXMLKeyInfoType_ds;
begin
  Result := ChildNodes['KeyInfo'] as IXMLKeyInfoType_ds;
end;

{ TXMLSignedInfoType_ds }

procedure TXMLSignedInfoType_ds.AfterConstruction;
begin
  RegisterChildNode('CanonicalizationMethod', TXMLCanonicalizationMethod_ds);
  RegisterChildNode('SignatureMethod', TXMLSignatureMethod_ds);
  RegisterChildNode('Reference', TXMLReferenceType_ds);
  inherited;
end;

function TXMLSignedInfoType_ds.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLSignedInfoType_ds.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLSignedInfoType_ds.Get_CanonicalizationMethod: IXMLCanonicalizationMethod_ds;
begin
  Result := ChildNodes['CanonicalizationMethod'] as IXMLCanonicalizationMethod_ds;
end;

function TXMLSignedInfoType_ds.Get_SignatureMethod: IXMLSignatureMethod_ds;
begin
  Result := ChildNodes['SignatureMethod'] as IXMLSignatureMethod_ds;
end;

function TXMLSignedInfoType_ds.Get_Reference: IXMLReferenceType_ds;
begin
  Result := ChildNodes['Reference'] as IXMLReferenceType_ds;
end;

{ TXMLCanonicalizationMethod_ds }

function TXMLCanonicalizationMethod_ds.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLCanonicalizationMethod_ds.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

{ TXMLSignatureMethod_ds }

function TXMLSignatureMethod_ds.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLSignatureMethod_ds.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

{ TXMLReferenceType_ds }

procedure TXMLReferenceType_ds.AfterConstruction;
begin
  RegisterChildNode('Transforms', TXMLTransformsType_ds);
  RegisterChildNode('DigestMethod', TXMLDigestMethod_ds);
  inherited;
end;

function TXMLReferenceType_ds.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLReferenceType_ds.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLReferenceType_ds.Get_URI: UnicodeString;
begin
  Result := AttributeNodes['URI'].Text;
end;

procedure TXMLReferenceType_ds.Set_URI(Value: UnicodeString);
begin
  SetAttribute('URI', Value);
end;

function TXMLReferenceType_ds.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLReferenceType_ds.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

function TXMLReferenceType_ds.Get_Transforms: IXMLTransformsType_ds;
begin
  Result := ChildNodes['Transforms'] as IXMLTransformsType_ds;
end;

function TXMLReferenceType_ds.Get_DigestMethod: IXMLDigestMethod_ds;
begin
  Result := ChildNodes['DigestMethod'] as IXMLDigestMethod_ds;
end;

function TXMLReferenceType_ds.Get_DigestValue: UnicodeString;
begin
  Result := ChildNodes['DigestValue'].Text;
end;

procedure TXMLReferenceType_ds.Set_DigestValue(Value: UnicodeString);
begin
  ChildNodes['DigestValue'].NodeValue := Value;
end;

{ TXMLTransformsType_ds }

procedure TXMLTransformsType_ds.AfterConstruction;
begin
  RegisterChildNode('Transform', TXMLTransformType_ds);
  ItemTag := 'Transform';
  ItemInterface := IXMLTransformType_ds;
  inherited;
end;

function TXMLTransformsType_ds.Get_Transform(Index: Integer): IXMLTransformType_ds;
begin
  Result := List[Index] as IXMLTransformType_ds;
end;

function TXMLTransformsType_ds.Add: IXMLTransformType_ds;
begin
  Result := AddItem(-1) as IXMLTransformType_ds;
end;

function TXMLTransformsType_ds.Insert(const Index: Integer): IXMLTransformType_ds;
begin
  Result := AddItem(Index) as IXMLTransformType_ds;
end;

{ TXMLTransformType_ds }

procedure TXMLTransformType_ds.AfterConstruction;
begin
  ItemTag := 'XPath';
  ItemInterface := IXMLNode;
  inherited;
end;

function TXMLTransformType_ds.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLTransformType_ds.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

function TXMLTransformType_ds.Get_XPath(Index: Integer): UnicodeString;
begin
  Result := List[Index].Text;
end;

function TXMLTransformType_ds.Add(const XPath: UnicodeString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := XPath;
end;

function TXMLTransformType_ds.Insert(const Index: Integer; const XPath: UnicodeString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := XPath;
end;

{ TXMLDigestMethod_ds }

function TXMLDigestMethod_ds.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLDigestMethod_ds.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

{ TXMLSignatureValueType_ds }

function TXMLSignatureValueType_ds.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLSignatureValueType_ds.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

{ TXMLKeyInfoType_ds }

procedure TXMLKeyInfoType_ds.AfterConstruction;
begin
  RegisterChildNode('X509Data', TXMLX509DataType_ds);
  inherited;
end;

function TXMLKeyInfoType_ds.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLKeyInfoType_ds.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLKeyInfoType_ds.Get_X509Data: IXMLX509DataType_ds;
begin
  Result := ChildNodes['X509Data'] as IXMLX509DataType_ds;
end;

{ TXMLX509DataType_ds }

function TXMLX509DataType_ds.Get_X509Certificate: UnicodeString;
begin
  Result := ChildNodes['X509Certificate'].Text;
end;

procedure TXMLX509DataType_ds.Set_X509Certificate(Value: UnicodeString);
begin
  ChildNodes['X509Certificate'].NodeValue := Value;
end;

{ TXMLTEnviMDFe }

procedure TXMLTEnviMDFe.AfterConstruction;
begin
  RegisterChildNode('MDFe', TXMLTMDFe);
  inherited;
end;

function TXMLTEnviMDFe.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTEnviMDFe.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTEnviMDFe.Get_IdLote: UnicodeString;
begin
  Result := ChildNodes['idLote'].Text;
end;

procedure TXMLTEnviMDFe.Set_IdLote(Value: UnicodeString);
begin
  ChildNodes['idLote'].NodeValue := Value;
end;

function TXMLTEnviMDFe.Get_MDFe: IXMLTMDFe;
begin
  Result := ChildNodes['MDFe'] as IXMLTMDFe;
end;

{ TXMLTRetEnviMDFe }

procedure TXMLTRetEnviMDFe.AfterConstruction;
begin
  RegisterChildNode('infRec', TXMLInfRec);
  inherited;
end;

function TXMLTRetEnviMDFe.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTRetEnviMDFe.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTRetEnviMDFe.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTRetEnviMDFe.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTRetEnviMDFe.Get_CUF: UnicodeString;
begin
  Result := ChildNodes['cUF'].Text;
end;

procedure TXMLTRetEnviMDFe.Set_CUF(Value: UnicodeString);
begin
  ChildNodes['cUF'].NodeValue := Value;
end;

function TXMLTRetEnviMDFe.Get_VerAplic: UnicodeString;
begin
  Result := ChildNodes['verAplic'].Text;
end;

procedure TXMLTRetEnviMDFe.Set_VerAplic(Value: UnicodeString);
begin
  ChildNodes['verAplic'].NodeValue := Value;
end;

function TXMLTRetEnviMDFe.Get_CStat: UnicodeString;
begin
  Result := ChildNodes['cStat'].Text;
end;

procedure TXMLTRetEnviMDFe.Set_CStat(Value: UnicodeString);
begin
  ChildNodes['cStat'].NodeValue := Value;
end;

function TXMLTRetEnviMDFe.Get_XMotivo: UnicodeString;
begin
  Result := ChildNodes['xMotivo'].Text;
end;

procedure TXMLTRetEnviMDFe.Set_XMotivo(Value: UnicodeString);
begin
  ChildNodes['xMotivo'].NodeValue := Value;
end;

function TXMLTRetEnviMDFe.Get_InfRec: IXMLInfRec;
begin
  Result := ChildNodes['infRec'] as IXMLInfRec;
end;

{ TXMLInfRec }

function TXMLInfRec.Get_NRec: UnicodeString;
begin
  Result := ChildNodes['nRec'].Text;
end;

procedure TXMLInfRec.Set_NRec(Value: UnicodeString);
begin
  ChildNodes['nRec'].NodeValue := Value;
end;

function TXMLInfRec.Get_DhRecbto: UnicodeString;
begin
  Result := ChildNodes['dhRecbto'].Text;
end;

procedure TXMLInfRec.Set_DhRecbto(Value: UnicodeString);
begin
  ChildNodes['dhRecbto'].NodeValue := Value;
end;

function TXMLInfRec.Get_TMed: Integer;
begin
  Result := ChildNodes['tMed'].NodeValue;
end;

procedure TXMLInfRec.Set_TMed(Value: Integer);
begin
  ChildNodes['tMed'].NodeValue := Value;
end;

{ TXMLTEndereco }

function TXMLTEndereco.Get_XLgr: UnicodeString;
begin
  Result := ChildNodes['xLgr'].Text;
end;

procedure TXMLTEndereco.Set_XLgr(Value: UnicodeString);
begin
  ChildNodes['xLgr'].NodeValue := Value;
end;

function TXMLTEndereco.Get_Nro: UnicodeString;
begin
  Result := ChildNodes['nro'].Text;
end;

procedure TXMLTEndereco.Set_Nro(Value: UnicodeString);
begin
  ChildNodes['nro'].NodeValue := Value;
end;

function TXMLTEndereco.Get_XCpl: UnicodeString;
begin
  Result := ChildNodes['xCpl'].Text;
end;

procedure TXMLTEndereco.Set_XCpl(Value: UnicodeString);
begin
  ChildNodes['xCpl'].NodeValue := Value;
end;

function TXMLTEndereco.Get_XBairro: UnicodeString;
begin
  Result := ChildNodes['xBairro'].Text;
end;

procedure TXMLTEndereco.Set_XBairro(Value: UnicodeString);
begin
  ChildNodes['xBairro'].NodeValue := Value;
end;

function TXMLTEndereco.Get_CMun: UnicodeString;
begin
  Result := ChildNodes['cMun'].Text;
end;

procedure TXMLTEndereco.Set_CMun(Value: UnicodeString);
begin
  ChildNodes['cMun'].NodeValue := Value;
end;

function TXMLTEndereco.Get_XMun: UnicodeString;
begin
  Result := ChildNodes['xMun'].Text;
end;

procedure TXMLTEndereco.Set_XMun(Value: UnicodeString);
begin
  ChildNodes['xMun'].NodeValue := Value;
end;

function TXMLTEndereco.Get_CEP: UnicodeString;
begin
  Result := ChildNodes['CEP'].Text;
end;

procedure TXMLTEndereco.Set_CEP(Value: UnicodeString);
begin
  ChildNodes['CEP'].NodeValue := Value;
end;

function TXMLTEndereco.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLTEndereco.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

function TXMLTEndereco.Get_CPais: UnicodeString;
begin
  Result := ChildNodes['cPais'].Text;
end;

procedure TXMLTEndereco.Set_CPais(Value: UnicodeString);
begin
  ChildNodes['cPais'].NodeValue := Value;
end;

function TXMLTEndereco.Get_XPais: UnicodeString;
begin
  Result := ChildNodes['xPais'].Text;
end;

procedure TXMLTEndereco.Set_XPais(Value: UnicodeString);
begin
  ChildNodes['xPais'].NodeValue := Value;
end;

{ TXMLTEndernac }

function TXMLTEndernac.Get_XLgr: UnicodeString;
begin
  Result := ChildNodes['xLgr'].Text;
end;

procedure TXMLTEndernac.Set_XLgr(Value: UnicodeString);
begin
  ChildNodes['xLgr'].NodeValue := Value;
end;

function TXMLTEndernac.Get_Nro: UnicodeString;
begin
  Result := ChildNodes['nro'].Text;
end;

procedure TXMLTEndernac.Set_Nro(Value: UnicodeString);
begin
  ChildNodes['nro'].NodeValue := Value;
end;

function TXMLTEndernac.Get_XCpl: UnicodeString;
begin
  Result := ChildNodes['xCpl'].Text;
end;

procedure TXMLTEndernac.Set_XCpl(Value: UnicodeString);
begin
  ChildNodes['xCpl'].NodeValue := Value;
end;

function TXMLTEndernac.Get_XBairro: UnicodeString;
begin
  Result := ChildNodes['xBairro'].Text;
end;

procedure TXMLTEndernac.Set_XBairro(Value: UnicodeString);
begin
  ChildNodes['xBairro'].NodeValue := Value;
end;

function TXMLTEndernac.Get_CMun: UnicodeString;
begin
  Result := ChildNodes['cMun'].Text;
end;

procedure TXMLTEndernac.Set_CMun(Value: UnicodeString);
begin
  ChildNodes['cMun'].NodeValue := Value;
end;

function TXMLTEndernac.Get_XMun: UnicodeString;
begin
  Result := ChildNodes['xMun'].Text;
end;

procedure TXMLTEndernac.Set_XMun(Value: UnicodeString);
begin
  ChildNodes['xMun'].NodeValue := Value;
end;

function TXMLTEndernac.Get_CEP: UnicodeString;
begin
  Result := ChildNodes['CEP'].Text;
end;

procedure TXMLTEndernac.Set_CEP(Value: UnicodeString);
begin
  ChildNodes['CEP'].NodeValue := Value;
end;

function TXMLTEndernac.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLTEndernac.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

{ TXMLTEnderFer }

function TXMLTEnderFer.Get_XLgr: UnicodeString;
begin
  Result := ChildNodes['xLgr'].Text;
end;

procedure TXMLTEnderFer.Set_XLgr(Value: UnicodeString);
begin
  ChildNodes['xLgr'].NodeValue := Value;
end;

function TXMLTEnderFer.Get_Nro: UnicodeString;
begin
  Result := ChildNodes['nro'].Text;
end;

procedure TXMLTEnderFer.Set_Nro(Value: UnicodeString);
begin
  ChildNodes['nro'].NodeValue := Value;
end;

function TXMLTEnderFer.Get_XCpl: UnicodeString;
begin
  Result := ChildNodes['xCpl'].Text;
end;

procedure TXMLTEnderFer.Set_XCpl(Value: UnicodeString);
begin
  ChildNodes['xCpl'].NodeValue := Value;
end;

function TXMLTEnderFer.Get_XBairro: UnicodeString;
begin
  Result := ChildNodes['xBairro'].Text;
end;

procedure TXMLTEnderFer.Set_XBairro(Value: UnicodeString);
begin
  ChildNodes['xBairro'].NodeValue := Value;
end;

function TXMLTEnderFer.Get_CMun: UnicodeString;
begin
  Result := ChildNodes['cMun'].Text;
end;

procedure TXMLTEnderFer.Set_CMun(Value: UnicodeString);
begin
  ChildNodes['cMun'].NodeValue := Value;
end;

function TXMLTEnderFer.Get_XMun: UnicodeString;
begin
  Result := ChildNodes['xMun'].Text;
end;

procedure TXMLTEnderFer.Set_XMun(Value: UnicodeString);
begin
  ChildNodes['xMun'].NodeValue := Value;
end;

function TXMLTEnderFer.Get_CEP: UnicodeString;
begin
  Result := ChildNodes['CEP'].Text;
end;

procedure TXMLTEnderFer.Set_CEP(Value: UnicodeString);
begin
  ChildNodes['CEP'].NodeValue := Value;
end;

function TXMLTEnderFer.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLTEnderFer.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

{ TXMLTEndOrg }

function TXMLTEndOrg.Get_XLgr: UnicodeString;
begin
  Result := ChildNodes['xLgr'].Text;
end;

procedure TXMLTEndOrg.Set_XLgr(Value: UnicodeString);
begin
  ChildNodes['xLgr'].NodeValue := Value;
end;

function TXMLTEndOrg.Get_Nro: UnicodeString;
begin
  Result := ChildNodes['nro'].Text;
end;

procedure TXMLTEndOrg.Set_Nro(Value: UnicodeString);
begin
  ChildNodes['nro'].NodeValue := Value;
end;

function TXMLTEndOrg.Get_XCpl: UnicodeString;
begin
  Result := ChildNodes['xCpl'].Text;
end;

procedure TXMLTEndOrg.Set_XCpl(Value: UnicodeString);
begin
  ChildNodes['xCpl'].NodeValue := Value;
end;

function TXMLTEndOrg.Get_XBairro: UnicodeString;
begin
  Result := ChildNodes['xBairro'].Text;
end;

procedure TXMLTEndOrg.Set_XBairro(Value: UnicodeString);
begin
  ChildNodes['xBairro'].NodeValue := Value;
end;

function TXMLTEndOrg.Get_CMun: UnicodeString;
begin
  Result := ChildNodes['cMun'].Text;
end;

procedure TXMLTEndOrg.Set_CMun(Value: UnicodeString);
begin
  ChildNodes['cMun'].NodeValue := Value;
end;

function TXMLTEndOrg.Get_XMun: UnicodeString;
begin
  Result := ChildNodes['xMun'].Text;
end;

procedure TXMLTEndOrg.Set_XMun(Value: UnicodeString);
begin
  ChildNodes['xMun'].NodeValue := Value;
end;

function TXMLTEndOrg.Get_CEP: UnicodeString;
begin
  Result := ChildNodes['CEP'].Text;
end;

procedure TXMLTEndOrg.Set_CEP(Value: UnicodeString);
begin
  ChildNodes['CEP'].NodeValue := Value;
end;

function TXMLTEndOrg.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLTEndOrg.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

function TXMLTEndOrg.Get_CPais: UnicodeString;
begin
  Result := ChildNodes['cPais'].Text;
end;

procedure TXMLTEndOrg.Set_CPais(Value: UnicodeString);
begin
  ChildNodes['cPais'].NodeValue := Value;
end;

function TXMLTEndOrg.Get_XPais: UnicodeString;
begin
  Result := ChildNodes['xPais'].Text;
end;

procedure TXMLTEndOrg.Set_XPais(Value: UnicodeString);
begin
  ChildNodes['xPais'].NodeValue := Value;
end;

function TXMLTEndOrg.Get_Fone: UnicodeString;
begin
  Result := ChildNodes['fone'].Text;
end;

procedure TXMLTEndOrg.Set_Fone(Value: UnicodeString);
begin
  ChildNodes['fone'].NodeValue := Value;
end;

{ TXMLTLocal }

function TXMLTLocal.Get_CMun: UnicodeString;
begin
  Result := ChildNodes['cMun'].Text;
end;

procedure TXMLTLocal.Set_CMun(Value: UnicodeString);
begin
  ChildNodes['cMun'].NodeValue := Value;
end;

function TXMLTLocal.Get_XMun: UnicodeString;
begin
  Result := ChildNodes['xMun'].Text;
end;

procedure TXMLTLocal.Set_XMun(Value: UnicodeString);
begin
  ChildNodes['xMun'].NodeValue := Value;
end;

function TXMLTLocal.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLTLocal.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

{ TXMLTEndReEnt }

function TXMLTEndReEnt.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLTEndReEnt.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLTEndReEnt.Get_CPF: UnicodeString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLTEndReEnt.Set_CPF(Value: UnicodeString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

function TXMLTEndReEnt.Get_XNome: UnicodeString;
begin
  Result := ChildNodes['xNome'].Text;
end;

procedure TXMLTEndReEnt.Set_XNome(Value: UnicodeString);
begin
  ChildNodes['xNome'].NodeValue := Value;
end;

function TXMLTEndReEnt.Get_XLgr: UnicodeString;
begin
  Result := ChildNodes['xLgr'].Text;
end;

procedure TXMLTEndReEnt.Set_XLgr(Value: UnicodeString);
begin
  ChildNodes['xLgr'].NodeValue := Value;
end;

function TXMLTEndReEnt.Get_Nro: UnicodeString;
begin
  Result := ChildNodes['nro'].Text;
end;

procedure TXMLTEndReEnt.Set_Nro(Value: UnicodeString);
begin
  ChildNodes['nro'].NodeValue := Value;
end;

function TXMLTEndReEnt.Get_XCpl: UnicodeString;
begin
  Result := ChildNodes['xCpl'].Text;
end;

procedure TXMLTEndReEnt.Set_XCpl(Value: UnicodeString);
begin
  ChildNodes['xCpl'].NodeValue := Value;
end;

function TXMLTEndReEnt.Get_XBairro: UnicodeString;
begin
  Result := ChildNodes['xBairro'].Text;
end;

procedure TXMLTEndReEnt.Set_XBairro(Value: UnicodeString);
begin
  ChildNodes['xBairro'].NodeValue := Value;
end;

function TXMLTEndReEnt.Get_CMun: UnicodeString;
begin
  Result := ChildNodes['cMun'].Text;
end;

procedure TXMLTEndReEnt.Set_CMun(Value: UnicodeString);
begin
  ChildNodes['cMun'].NodeValue := Value;
end;

function TXMLTEndReEnt.Get_XMun: UnicodeString;
begin
  Result := ChildNodes['xMun'].Text;
end;

procedure TXMLTEndReEnt.Set_XMun(Value: UnicodeString);
begin
  ChildNodes['xMun'].NodeValue := Value;
end;

function TXMLTEndReEnt.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLTEndReEnt.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

{ TXMLTNFeNF }

procedure TXMLTNFeNF.AfterConstruction;
begin
  RegisterChildNode('infNFe', TXMLInfNFe);
  RegisterChildNode('infNF', TXMLInfNF);
  inherited;
end;

function TXMLTNFeNF.Get_InfNFe: IXMLInfNFe;
begin
  Result := ChildNodes['infNFe'] as IXMLInfNFe;
end;

function TXMLTNFeNF.Get_InfNF: IXMLInfNF;
begin
  Result := ChildNodes['infNF'] as IXMLInfNF;
end;

{ TXMLInfNF }

procedure TXMLInfNF.AfterConstruction;
begin
  RegisterChildNode('emi', TXMLEmi);
  RegisterChildNode('dest', TXMLDest);
  inherited;
end;

function TXMLInfNF.Get_Emi: IXMLEmi;
begin
  Result := ChildNodes['emi'] as IXMLEmi;
end;

function TXMLInfNF.Get_Dest: IXMLDest;
begin
  Result := ChildNodes['dest'] as IXMLDest;
end;

function TXMLInfNF.Get_Serie: UnicodeString;
begin
  Result := ChildNodes['serie'].Text;
end;

procedure TXMLInfNF.Set_Serie(Value: UnicodeString);
begin
  ChildNodes['serie'].NodeValue := Value;
end;

function TXMLInfNF.Get_NNF: UnicodeString;
begin
  Result := ChildNodes['nNF'].Text;
end;

procedure TXMLInfNF.Set_NNF(Value: UnicodeString);
begin
  ChildNodes['nNF'].NodeValue := Value;
end;

function TXMLInfNF.Get_DEmi: UnicodeString;
begin
  Result := ChildNodes['dEmi'].Text;
end;

procedure TXMLInfNF.Set_DEmi(Value: UnicodeString);
begin
  ChildNodes['dEmi'].NodeValue := Value;
end;

function TXMLInfNF.Get_VNF: UnicodeString;
begin
  Result := ChildNodes['vNF'].Text;
end;

procedure TXMLInfNF.Set_VNF(Value: UnicodeString);
begin
  ChildNodes['vNF'].NodeValue := Value;
end;

function TXMLInfNF.Get_PIN: UnicodeString;
begin
  Result := ChildNodes['PIN'].Text;
end;

procedure TXMLInfNF.Set_PIN(Value: UnicodeString);
begin
  ChildNodes['PIN'].NodeValue := Value;
end;

{ TXMLEmi }

function TXMLEmi.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLEmi.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLEmi.Get_XNome: UnicodeString;
begin
  Result := ChildNodes['xNome'].Text;
end;

procedure TXMLEmi.Set_XNome(Value: UnicodeString);
begin
  ChildNodes['xNome'].NodeValue := Value;
end;

function TXMLEmi.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLEmi.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

{ TXMLDest }

function TXMLDest.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLDest.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLDest.Get_CPF: UnicodeString;
begin
  Result := ChildNodes['CPF'].Text;
end;

procedure TXMLDest.Set_CPF(Value: UnicodeString);
begin
  ChildNodes['CPF'].NodeValue := Value;
end;

function TXMLDest.Get_XNome: UnicodeString;
begin
  Result := ChildNodes['xNome'].Text;
end;

procedure TXMLDest.Set_XNome(Value: UnicodeString);
begin
  ChildNodes['xNome'].NodeValue := Value;
end;

function TXMLDest.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLDest.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

end.