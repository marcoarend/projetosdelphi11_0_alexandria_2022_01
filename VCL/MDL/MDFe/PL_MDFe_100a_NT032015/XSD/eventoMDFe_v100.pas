
{**************************************************************************************************************}
{                                                                                                              }
{                                               XML Data Binding                                               }
{                                                                                                              }
{         Generated on: 06/01/2016 10:52:47                                                                    }
{       Generated from: C:\_Compilers\DELPHI_XE2\VCL\MDL\MDFe\PL_MDFe_100a_NT032015\XSD\eventoMDFe_v1.00.xsd   }
{   Settings stored in: C:\_Compilers\DELPHI_XE2\VCL\MDL\MDFe\PL_MDFe_100a_NT032015\XSD\eventoMDFe_v1.00.xdb   }
{                                                                                                              }
{**************************************************************************************************************}

unit eventoMDFe_v100;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTEvento = interface;
  IXMLInfEvento = interface;
  IXMLDetEvento = interface;
  IXMLSignatureType_ds = interface;
  IXMLSignedInfoType_ds = interface;
  IXMLCanonicalizationMethod_ds = interface;
  IXMLSignatureMethod_ds = interface;
  IXMLReferenceType_ds = interface;
  IXMLTransformsType_ds = interface;
  IXMLTransformType_ds = interface;
  IXMLDigestMethod_ds = interface;
  IXMLSignatureValueType_ds = interface;
  IXMLKeyInfoType_ds = interface;
  IXMLX509DataType_ds = interface;
  IXMLTRetEvento = interface;
  IXMLTProcEvento = interface;

{ IXMLTEvento }

  IXMLTEvento = interface(IXMLNode)
    ['{E4154775-27A7-4B2D-BAD2-F60CD02FB37D}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_InfEvento: IXMLInfEvento;
    function Get_Signature: IXMLSignatureType_ds;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property InfEvento: IXMLInfEvento read Get_InfEvento;
    property Signature: IXMLSignatureType_ds read Get_Signature;
  end;

{ IXMLInfEvento }

  IXMLInfEvento = interface(IXMLNode)
    ['{57CDD8E3-2505-4700-9EBC-B14B1A011D12}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_COrgao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_CNPJ: UnicodeString;
    function Get_ChMDFe: UnicodeString;
    function Get_DhEvento: UnicodeString;
    function Get_TpEvento: UnicodeString;
    function Get_NSeqEvento: UnicodeString;
    function Get_DetEvento: IXMLDetEvento;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_COrgao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_ChMDFe(Value: UnicodeString);
    procedure Set_DhEvento(Value: UnicodeString);
    procedure Set_TpEvento(Value: UnicodeString);
    procedure Set_NSeqEvento(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property COrgao: UnicodeString read Get_COrgao write Set_COrgao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property ChMDFe: UnicodeString read Get_ChMDFe write Set_ChMDFe;
    property DhEvento: UnicodeString read Get_DhEvento write Set_DhEvento;
    property TpEvento: UnicodeString read Get_TpEvento write Set_TpEvento;
    property NSeqEvento: UnicodeString read Get_NSeqEvento write Set_NSeqEvento;
    property DetEvento: IXMLDetEvento read Get_DetEvento;
  end;

{ IXMLDetEvento }

  IXMLDetEvento = interface(IXMLNode)
    ['{DF355A57-91DA-4A1F-8DD8-0901BD72AF44}']
    { Property Accessors }
    function Get_VersaoEvento: UnicodeString;
    procedure Set_VersaoEvento(Value: UnicodeString);
    { Methods & Properties }
    property VersaoEvento: UnicodeString read Get_VersaoEvento write Set_VersaoEvento;
  end;

{ IXMLSignatureType_ds }

  IXMLSignatureType_ds = interface(IXMLNode)
    ['{BE62A0C7-A1EB-443A-BA10-CA083F0646EE}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_SignedInfo: IXMLSignedInfoType_ds;
    function Get_SignatureValue: IXMLSignatureValueType_ds;
    function Get_KeyInfo: IXMLKeyInfoType_ds;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property SignedInfo: IXMLSignedInfoType_ds read Get_SignedInfo;
    property SignatureValue: IXMLSignatureValueType_ds read Get_SignatureValue;
    property KeyInfo: IXMLKeyInfoType_ds read Get_KeyInfo;
  end;

{ IXMLSignedInfoType_ds }

  IXMLSignedInfoType_ds = interface(IXMLNode)
    ['{29B0DE00-88F0-40F4-A6D2-36EDB3C5D929}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_CanonicalizationMethod: IXMLCanonicalizationMethod_ds;
    function Get_SignatureMethod: IXMLSignatureMethod_ds;
    function Get_Reference: IXMLReferenceType_ds;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property CanonicalizationMethod: IXMLCanonicalizationMethod_ds read Get_CanonicalizationMethod;
    property SignatureMethod: IXMLSignatureMethod_ds read Get_SignatureMethod;
    property Reference: IXMLReferenceType_ds read Get_Reference;
  end;

{ IXMLCanonicalizationMethod_ds }

  IXMLCanonicalizationMethod_ds = interface(IXMLNode)
    ['{2B16C994-4999-495C-862A-F3A57BDF08A1}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
  end;

{ IXMLSignatureMethod_ds }

  IXMLSignatureMethod_ds = interface(IXMLNode)
    ['{7C323170-15B9-4709-A4B5-CFD6497E016B}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
  end;

{ IXMLReferenceType_ds }

  IXMLReferenceType_ds = interface(IXMLNode)
    ['{7C852336-DA69-47D3-BE3C-8F25FF37AE68}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_URI: UnicodeString;
    function Get_Type_: UnicodeString;
    function Get_Transforms: IXMLTransformsType_ds;
    function Get_DigestMethod: IXMLDigestMethod_ds;
    function Get_DigestValue: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_URI(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_DigestValue(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property URI: UnicodeString read Get_URI write Set_URI;
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
    property Transforms: IXMLTransformsType_ds read Get_Transforms;
    property DigestMethod: IXMLDigestMethod_ds read Get_DigestMethod;
    property DigestValue: UnicodeString read Get_DigestValue write Set_DigestValue;
  end;

{ IXMLTransformsType_ds }

  IXMLTransformsType_ds = interface(IXMLNodeCollection)
    ['{A15B68EB-0637-48DF-A149-8300E09282DA}']
    { Property Accessors }
    function Get_Transform(Index: Integer): IXMLTransformType_ds;
    { Methods & Properties }
    function Add: IXMLTransformType_ds;
    function Insert(const Index: Integer): IXMLTransformType_ds;
    property Transform[Index: Integer]: IXMLTransformType_ds read Get_Transform; default;
  end;

{ IXMLTransformType_ds }

  IXMLTransformType_ds = interface(IXMLNodeCollection)
    ['{687DD5C7-2B34-4582-BA82-B814C8C9210B}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    function Get_XPath(Index: Integer): UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    function Add(const XPath: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const XPath: UnicodeString): IXMLNode;
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
    property XPath[Index: Integer]: UnicodeString read Get_XPath; default;
  end;

{ IXMLDigestMethod_ds }

  IXMLDigestMethod_ds = interface(IXMLNode)
    ['{99FA9A69-A9E3-4089-8483-8585081DCEF7}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
  end;

{ IXMLSignatureValueType_ds }

  IXMLSignatureValueType_ds = interface(IXMLNode)
    ['{6DED46B1-F372-41EA-8396-FB6FA3D4789B}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
  end;

{ IXMLKeyInfoType_ds }

  IXMLKeyInfoType_ds = interface(IXMLNode)
    ['{BE76438A-F7CD-45EE-9C86-771F6DFE2AD4}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_X509Data: IXMLX509DataType_ds;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property X509Data: IXMLX509DataType_ds read Get_X509Data;
  end;

{ IXMLX509DataType_ds }

  IXMLX509DataType_ds = interface(IXMLNode)
    ['{998FCABA-9BED-4D36-A11E-170D4379B0A9}']
    { Property Accessors }
    function Get_X509Certificate: UnicodeString;
    procedure Set_X509Certificate(Value: UnicodeString);
    { Methods & Properties }
    property X509Certificate: UnicodeString read Get_X509Certificate write Set_X509Certificate;
  end;

{ IXMLTRetEvento }

  IXMLTRetEvento = interface(IXMLNode)
    ['{37E933A2-F961-438A-9C33-C10DEFA853D1}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_InfEvento: IXMLInfEvento;
    function Get_Signature: IXMLSignatureType_ds;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property InfEvento: IXMLInfEvento read Get_InfEvento;
    property Signature: IXMLSignatureType_ds read Get_Signature;
  end;

{ IXMLTProcEvento }

  IXMLTProcEvento = interface(IXMLNode)
    ['{F8FF8F63-DA74-4B97-9643-F243FCBA0C29}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_EventoMDFe: IXMLTEvento;
    function Get_RetEventoMDFe: IXMLTRetEvento;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property EventoMDFe: IXMLTEvento read Get_EventoMDFe;
    property RetEventoMDFe: IXMLTRetEvento read Get_RetEventoMDFe;
  end;

{ Forward Decls }

  TXMLTEvento = class;
  TXMLInfEvento = class;
  TXMLDetEvento = class;
  TXMLSignatureType_ds = class;
  TXMLSignedInfoType_ds = class;
  TXMLCanonicalizationMethod_ds = class;
  TXMLSignatureMethod_ds = class;
  TXMLReferenceType_ds = class;
  TXMLTransformsType_ds = class;
  TXMLTransformType_ds = class;
  TXMLDigestMethod_ds = class;
  TXMLSignatureValueType_ds = class;
  TXMLKeyInfoType_ds = class;
  TXMLX509DataType_ds = class;
  TXMLTRetEvento = class;
  TXMLTProcEvento = class;

{ TXMLTEvento }

  TXMLTEvento = class(TXMLNode, IXMLTEvento)
  protected
    { IXMLTEvento }
    function Get_Versao: UnicodeString;
    function Get_InfEvento: IXMLInfEvento;
    function Get_Signature: IXMLSignatureType_ds;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfEvento }

  TXMLInfEvento = class(TXMLNode, IXMLInfEvento)
  protected
    { IXMLInfEvento }
    function Get_Id: UnicodeString;
    function Get_COrgao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_CNPJ: UnicodeString;
    function Get_ChMDFe: UnicodeString;
    function Get_DhEvento: UnicodeString;
    function Get_TpEvento: UnicodeString;
    function Get_NSeqEvento: UnicodeString;
    function Get_DetEvento: IXMLDetEvento;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_COrgao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_ChMDFe(Value: UnicodeString);
    procedure Set_DhEvento(Value: UnicodeString);
    procedure Set_TpEvento(Value: UnicodeString);
    procedure Set_NSeqEvento(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDetEvento }

  TXMLDetEvento = class(TXMLNode, IXMLDetEvento)
  protected
    { IXMLDetEvento }
    function Get_VersaoEvento: UnicodeString;
    procedure Set_VersaoEvento(Value: UnicodeString);
  end;

{ TXMLSignatureType_ds }

  TXMLSignatureType_ds = class(TXMLNode, IXMLSignatureType_ds)
  protected
    { IXMLSignatureType_ds }
    function Get_Id: UnicodeString;
    function Get_SignedInfo: IXMLSignedInfoType_ds;
    function Get_SignatureValue: IXMLSignatureValueType_ds;
    function Get_KeyInfo: IXMLKeyInfoType_ds;
    procedure Set_Id(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSignedInfoType_ds }

  TXMLSignedInfoType_ds = class(TXMLNode, IXMLSignedInfoType_ds)
  protected
    { IXMLSignedInfoType_ds }
    function Get_Id: UnicodeString;
    function Get_CanonicalizationMethod: IXMLCanonicalizationMethod_ds;
    function Get_SignatureMethod: IXMLSignatureMethod_ds;
    function Get_Reference: IXMLReferenceType_ds;
    procedure Set_Id(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLCanonicalizationMethod_ds }

  TXMLCanonicalizationMethod_ds = class(TXMLNode, IXMLCanonicalizationMethod_ds)
  protected
    { IXMLCanonicalizationMethod_ds }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
  end;

{ TXMLSignatureMethod_ds }

  TXMLSignatureMethod_ds = class(TXMLNode, IXMLSignatureMethod_ds)
  protected
    { IXMLSignatureMethod_ds }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
  end;

{ TXMLReferenceType_ds }

  TXMLReferenceType_ds = class(TXMLNode, IXMLReferenceType_ds)
  protected
    { IXMLReferenceType_ds }
    function Get_Id: UnicodeString;
    function Get_URI: UnicodeString;
    function Get_Type_: UnicodeString;
    function Get_Transforms: IXMLTransformsType_ds;
    function Get_DigestMethod: IXMLDigestMethod_ds;
    function Get_DigestValue: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_URI(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_DigestValue(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTransformsType_ds }

  TXMLTransformsType_ds = class(TXMLNodeCollection, IXMLTransformsType_ds)
  protected
    { IXMLTransformsType_ds }
    function Get_Transform(Index: Integer): IXMLTransformType_ds;
    function Add: IXMLTransformType_ds;
    function Insert(const Index: Integer): IXMLTransformType_ds;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTransformType_ds }

  TXMLTransformType_ds = class(TXMLNodeCollection, IXMLTransformType_ds)
  protected
    { IXMLTransformType_ds }
    function Get_Algorithm: UnicodeString;
    function Get_XPath(Index: Integer): UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    function Add(const XPath: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const XPath: UnicodeString): IXMLNode;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDigestMethod_ds }

  TXMLDigestMethod_ds = class(TXMLNode, IXMLDigestMethod_ds)
  protected
    { IXMLDigestMethod_ds }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
  end;

{ TXMLSignatureValueType_ds }

  TXMLSignatureValueType_ds = class(TXMLNode, IXMLSignatureValueType_ds)
  protected
    { IXMLSignatureValueType_ds }
    function Get_Id: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
  end;

{ TXMLKeyInfoType_ds }

  TXMLKeyInfoType_ds = class(TXMLNode, IXMLKeyInfoType_ds)
  protected
    { IXMLKeyInfoType_ds }
    function Get_Id: UnicodeString;
    function Get_X509Data: IXMLX509DataType_ds;
    procedure Set_Id(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLX509DataType_ds }

  TXMLX509DataType_ds = class(TXMLNode, IXMLX509DataType_ds)
  protected
    { IXMLX509DataType_ds }
    function Get_X509Certificate: UnicodeString;
    procedure Set_X509Certificate(Value: UnicodeString);
  end;

{ TXMLTRetEvento }

  TXMLTRetEvento = class(TXMLNode, IXMLTRetEvento)
  protected
    { IXMLTRetEvento }
    function Get_Versao: UnicodeString;
    function Get_InfEvento: IXMLInfEvento;
    function Get_Signature: IXMLSignatureType_ds;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTProcEvento }

  TXMLTProcEvento = class(TXMLNode, IXMLTProcEvento)
  protected
    { IXMLTProcEvento }
    function Get_Versao: UnicodeString;
    function Get_EventoMDFe: IXMLTEvento;
    function Get_RetEventoMDFe: IXMLTRetEvento;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ Global Functions }

function GetEvento(Doc: IXMLDocument): IXMLTEvento;
function LoadEvento(const FileName: string): IXMLTEvento;
function NewEvento: IXMLTEvento;

const
  TargetNamespace = 'http://www.portalfiscal.inf.br/mdfe';

implementation

{ Global Functions }

function GetEvento(Doc: IXMLDocument): IXMLTEvento;
begin
  Result := Doc.GetDocBinding('eventoMDFe', TXMLTEvento, TargetNamespace) as IXMLTEvento;
end;

function LoadEvento(const FileName: string): IXMLTEvento;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('eventoMDFe', TXMLTEvento, TargetNamespace) as IXMLTEvento;
end;

function NewEvento: IXMLTEvento;
begin
  Result := NewXMLDocument.GetDocBinding('eventoMDFe', TXMLTEvento, TargetNamespace) as IXMLTEvento;
end;

{ TXMLTEvento }

procedure TXMLTEvento.AfterConstruction;
begin
  RegisterChildNode('infEvento', TXMLInfEvento);
  RegisterChildNode('Signature', TXMLSignatureType_ds);
  inherited;
end;

function TXMLTEvento.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTEvento.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTEvento.Get_InfEvento: IXMLInfEvento;
begin
  Result := ChildNodes['infEvento'] as IXMLInfEvento;
end;

function TXMLTEvento.Get_Signature: IXMLSignatureType_ds;
begin
  Result := ChildNodes['Signature'] as IXMLSignatureType_ds;
end;

{ TXMLInfEvento }

procedure TXMLInfEvento.AfterConstruction;
begin
  RegisterChildNode('detEvento', TXMLDetEvento);
  inherited;
end;

function TXMLInfEvento.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLInfEvento.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLInfEvento.Get_COrgao: UnicodeString;
begin
  Result := ChildNodes['cOrgao'].Text;
end;

procedure TXMLInfEvento.Set_COrgao(Value: UnicodeString);
begin
  ChildNodes['cOrgao'].NodeValue := Value;
end;

function TXMLInfEvento.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLInfEvento.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLInfEvento.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLInfEvento.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLInfEvento.Get_ChMDFe: UnicodeString;
begin
  Result := ChildNodes['chMDFe'].Text;
end;

procedure TXMLInfEvento.Set_ChMDFe(Value: UnicodeString);
begin
  ChildNodes['chMDFe'].NodeValue := Value;
end;

function TXMLInfEvento.Get_DhEvento: UnicodeString;
begin
  Result := ChildNodes['dhEvento'].Text;
end;

procedure TXMLInfEvento.Set_DhEvento(Value: UnicodeString);
begin
  ChildNodes['dhEvento'].NodeValue := Value;
end;

function TXMLInfEvento.Get_TpEvento: UnicodeString;
begin
  Result := ChildNodes['tpEvento'].Text;
end;

procedure TXMLInfEvento.Set_TpEvento(Value: UnicodeString);
begin
  ChildNodes['tpEvento'].NodeValue := Value;
end;

function TXMLInfEvento.Get_NSeqEvento: UnicodeString;
begin
  Result := ChildNodes['nSeqEvento'].Text;
end;

procedure TXMLInfEvento.Set_NSeqEvento(Value: UnicodeString);
begin
  ChildNodes['nSeqEvento'].NodeValue := Value;
end;

function TXMLInfEvento.Get_DetEvento: IXMLDetEvento;
begin
  Result := ChildNodes['detEvento'] as IXMLDetEvento;
end;

{ TXMLDetEvento }

function TXMLDetEvento.Get_VersaoEvento: UnicodeString;
begin
  Result := AttributeNodes['versaoEvento'].Text;
end;

procedure TXMLDetEvento.Set_VersaoEvento(Value: UnicodeString);
begin
  SetAttribute('versaoEvento', Value);
end;

{ TXMLSignatureType_ds }

procedure TXMLSignatureType_ds.AfterConstruction;
begin
  RegisterChildNode('SignedInfo', TXMLSignedInfoType_ds);
  RegisterChildNode('SignatureValue', TXMLSignatureValueType_ds);
  RegisterChildNode('KeyInfo', TXMLKeyInfoType_ds);
  inherited;
end;

function TXMLSignatureType_ds.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLSignatureType_ds.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLSignatureType_ds.Get_SignedInfo: IXMLSignedInfoType_ds;
begin
  Result := ChildNodes['SignedInfo'] as IXMLSignedInfoType_ds;
end;

function TXMLSignatureType_ds.Get_SignatureValue: IXMLSignatureValueType_ds;
begin
  Result := ChildNodes['SignatureValue'] as IXMLSignatureValueType_ds;
end;

function TXMLSignatureType_ds.Get_KeyInfo: IXMLKeyInfoType_ds;
begin
  Result := ChildNodes['KeyInfo'] as IXMLKeyInfoType_ds;
end;

{ TXMLSignedInfoType_ds }

procedure TXMLSignedInfoType_ds.AfterConstruction;
begin
  RegisterChildNode('CanonicalizationMethod', TXMLCanonicalizationMethod_ds);
  RegisterChildNode('SignatureMethod', TXMLSignatureMethod_ds);
  RegisterChildNode('Reference', TXMLReferenceType_ds);
  inherited;
end;

function TXMLSignedInfoType_ds.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLSignedInfoType_ds.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLSignedInfoType_ds.Get_CanonicalizationMethod: IXMLCanonicalizationMethod_ds;
begin
  Result := ChildNodes['CanonicalizationMethod'] as IXMLCanonicalizationMethod_ds;
end;

function TXMLSignedInfoType_ds.Get_SignatureMethod: IXMLSignatureMethod_ds;
begin
  Result := ChildNodes['SignatureMethod'] as IXMLSignatureMethod_ds;
end;

function TXMLSignedInfoType_ds.Get_Reference: IXMLReferenceType_ds;
begin
  Result := ChildNodes['Reference'] as IXMLReferenceType_ds;
end;

{ TXMLCanonicalizationMethod_ds }

function TXMLCanonicalizationMethod_ds.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLCanonicalizationMethod_ds.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

{ TXMLSignatureMethod_ds }

function TXMLSignatureMethod_ds.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLSignatureMethod_ds.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

{ TXMLReferenceType_ds }

procedure TXMLReferenceType_ds.AfterConstruction;
begin
  RegisterChildNode('Transforms', TXMLTransformsType_ds);
  RegisterChildNode('DigestMethod', TXMLDigestMethod_ds);
  inherited;
end;

function TXMLReferenceType_ds.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLReferenceType_ds.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLReferenceType_ds.Get_URI: UnicodeString;
begin
  Result := AttributeNodes['URI'].Text;
end;

procedure TXMLReferenceType_ds.Set_URI(Value: UnicodeString);
begin
  SetAttribute('URI', Value);
end;

function TXMLReferenceType_ds.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLReferenceType_ds.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

function TXMLReferenceType_ds.Get_Transforms: IXMLTransformsType_ds;
begin
  Result := ChildNodes['Transforms'] as IXMLTransformsType_ds;
end;

function TXMLReferenceType_ds.Get_DigestMethod: IXMLDigestMethod_ds;
begin
  Result := ChildNodes['DigestMethod'] as IXMLDigestMethod_ds;
end;

function TXMLReferenceType_ds.Get_DigestValue: UnicodeString;
begin
  Result := ChildNodes['DigestValue'].Text;
end;

procedure TXMLReferenceType_ds.Set_DigestValue(Value: UnicodeString);
begin
  ChildNodes['DigestValue'].NodeValue := Value;
end;

{ TXMLTransformsType_ds }

procedure TXMLTransformsType_ds.AfterConstruction;
begin
  RegisterChildNode('Transform', TXMLTransformType_ds);
  ItemTag := 'Transform';
  ItemInterface := IXMLTransformType_ds;
  inherited;
end;

function TXMLTransformsType_ds.Get_Transform(Index: Integer): IXMLTransformType_ds;
begin
  Result := List[Index] as IXMLTransformType_ds;
end;

function TXMLTransformsType_ds.Add: IXMLTransformType_ds;
begin
  Result := AddItem(-1) as IXMLTransformType_ds;
end;

function TXMLTransformsType_ds.Insert(const Index: Integer): IXMLTransformType_ds;
begin
  Result := AddItem(Index) as IXMLTransformType_ds;
end;

{ TXMLTransformType_ds }

procedure TXMLTransformType_ds.AfterConstruction;
begin
  ItemTag := 'XPath';
  ItemInterface := IXMLNode;
  inherited;
end;

function TXMLTransformType_ds.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLTransformType_ds.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

function TXMLTransformType_ds.Get_XPath(Index: Integer): UnicodeString;
begin
  Result := List[Index].Text;
end;

function TXMLTransformType_ds.Add(const XPath: UnicodeString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := XPath;
end;

function TXMLTransformType_ds.Insert(const Index: Integer; const XPath: UnicodeString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := XPath;
end;

{ TXMLDigestMethod_ds }

function TXMLDigestMethod_ds.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLDigestMethod_ds.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

{ TXMLSignatureValueType_ds }

function TXMLSignatureValueType_ds.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLSignatureValueType_ds.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

{ TXMLKeyInfoType_ds }

procedure TXMLKeyInfoType_ds.AfterConstruction;
begin
  RegisterChildNode('X509Data', TXMLX509DataType_ds);
  inherited;
end;

function TXMLKeyInfoType_ds.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLKeyInfoType_ds.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLKeyInfoType_ds.Get_X509Data: IXMLX509DataType_ds;
begin
  Result := ChildNodes['X509Data'] as IXMLX509DataType_ds;
end;

{ TXMLX509DataType_ds }

function TXMLX509DataType_ds.Get_X509Certificate: UnicodeString;
begin
  Result := ChildNodes['X509Certificate'].Text;
end;

procedure TXMLX509DataType_ds.Set_X509Certificate(Value: UnicodeString);
begin
  ChildNodes['X509Certificate'].NodeValue := Value;
end;

{ TXMLTRetEvento }

procedure TXMLTRetEvento.AfterConstruction;
begin
  RegisterChildNode('infEvento', TXMLInfEvento);
  RegisterChildNode('Signature', TXMLSignatureType_ds);
  inherited;
end;

function TXMLTRetEvento.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTRetEvento.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTRetEvento.Get_InfEvento: IXMLInfEvento;
begin
  Result := ChildNodes['infEvento'] as IXMLInfEvento;
end;

function TXMLTRetEvento.Get_Signature: IXMLSignatureType_ds;
begin
  Result := ChildNodes['Signature'] as IXMLSignatureType_ds;
end;

{ TXMLTProcEvento }

procedure TXMLTProcEvento.AfterConstruction;
begin
  RegisterChildNode('eventoMDFe', TXMLTEvento);
  RegisterChildNode('retEventoMDFe', TXMLTRetEvento);
  inherited;
end;

function TXMLTProcEvento.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTProcEvento.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTProcEvento.Get_EventoMDFe: IXMLTEvento;
begin
  Result := ChildNodes['eventoMDFe'] as IXMLTEvento;
end;

function TXMLTProcEvento.Get_RetEventoMDFe: IXMLTRetEvento;
begin
  Result := ChildNodes['retEventoMDFe'] as IXMLTRetEvento;
end;

end.