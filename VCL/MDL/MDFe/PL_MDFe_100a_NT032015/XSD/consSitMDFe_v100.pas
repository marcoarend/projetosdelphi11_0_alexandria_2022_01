
{***************************************************************************************************************}
{                                                                                                               }
{                                               XML Data Binding                                                }
{                                                                                                               }
{         Generated on: 10/01/2016 08:21:07                                                                     }
{       Generated from: C:\_Compilers\DELPHI_XE2\VCL\MDL\MDFe\PL_MDFe_100a_NT032015\XSD\consSitMDFe_v1.00.xsd   }
{   Settings stored in: C:\_Compilers\DELPHI_XE2\VCL\MDL\MDFe\PL_MDFe_100a_NT032015\XSD\consSitMDFe_v1.00.xdb   }
{                                                                                                               }
{***************************************************************************************************************}

unit consSitMDFe_v100;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLTConsSitMDFe = interface;
  IXMLTProtMDFe = interface;
  IXMLInfProt = interface;
  IXMLSignatureType_ = interface;
  IXMLSignedInfoType_ = interface;
  IXMLCanonicalizationMethod_ = interface;
  IXMLSignatureMethod_ = interface;
  IXMLReferenceType_ = interface;
  IXMLTransformsType_ = interface;
  IXMLTransformType_ = interface;
  IXMLDigestMethod_ = interface;
  IXMLSignatureValueType_ = interface;
  IXMLKeyInfoType_ = interface;
  IXMLX509DataType_ = interface;
  IXMLTConsReciMDFe = interface;
  IXMLTRetConsReciMDFe = interface;
  IXMLTEvento = interface;
  IXMLInfEvento = interface;
  IXMLDetEvento = interface;
  IXMLTRetEvento = interface;
  IXMLTProcEvento = interface;
  IXMLTProcEventoList = interface;
  IXMLTRetConsSitMDFe = interface;

{ IXMLTConsSitMDFe }

  IXMLTConsSitMDFe = interface(IXMLNode)
    ['{613779E7-C599-45A7-97B3-3C5357D7074B}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_XServ: UnicodeString;
    function Get_ChMDFe: UnicodeString;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_XServ(Value: UnicodeString);
    procedure Set_ChMDFe(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property XServ: UnicodeString read Get_XServ write Set_XServ;
    property ChMDFe: UnicodeString read Get_ChMDFe write Set_ChMDFe;
  end;

{ IXMLTProtMDFe }

  IXMLTProtMDFe = interface(IXMLNode)
    ['{5D43297F-D6EC-40D7-A011-007499862025}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_InfProt: IXMLInfProt;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property InfProt: IXMLInfProt read Get_InfProt;
    property Signature: IXMLSignatureType_ read Get_Signature;
  end;

{ IXMLInfProt }

  IXMLInfProt = interface(IXMLNode)
    ['{5F55DF57-165A-44EC-AD82-DDF87293C092}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_ChMDFe: UnicodeString;
    function Get_DhRecbto: UnicodeString;
    function Get_NProt: UnicodeString;
    function Get_DigVal: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_ChMDFe(Value: UnicodeString);
    procedure Set_DhRecbto(Value: UnicodeString);
    procedure Set_NProt(Value: UnicodeString);
    procedure Set_DigVal(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property VerAplic: UnicodeString read Get_VerAplic write Set_VerAplic;
    property ChMDFe: UnicodeString read Get_ChMDFe write Set_ChMDFe;
    property DhRecbto: UnicodeString read Get_DhRecbto write Set_DhRecbto;
    property NProt: UnicodeString read Get_NProt write Set_NProt;
    property DigVal: UnicodeString read Get_DigVal write Set_DigVal;
    property CStat: UnicodeString read Get_CStat write Set_CStat;
    property XMotivo: UnicodeString read Get_XMotivo write Set_XMotivo;
  end;

{ IXMLSignatureType_ }

  IXMLSignatureType_ = interface(IXMLNode)
    ['{F218ED97-30FD-4E40-9572-0BBAFDD4AC44}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_SignedInfo: IXMLSignedInfoType_;
    function Get_SignatureValue: IXMLSignatureValueType_;
    function Get_KeyInfo: IXMLKeyInfoType_;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property SignedInfo: IXMLSignedInfoType_ read Get_SignedInfo;
    property SignatureValue: IXMLSignatureValueType_ read Get_SignatureValue;
    property KeyInfo: IXMLKeyInfoType_ read Get_KeyInfo;
  end;

{ IXMLSignedInfoType_ }

  IXMLSignedInfoType_ = interface(IXMLNode)
    ['{550080C1-7579-4BCC-94BE-8A327D681552}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_CanonicalizationMethod: IXMLCanonicalizationMethod_;
    function Get_SignatureMethod: IXMLSignatureMethod_;
    function Get_Reference: IXMLReferenceType_;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property CanonicalizationMethod: IXMLCanonicalizationMethod_ read Get_CanonicalizationMethod;
    property SignatureMethod: IXMLSignatureMethod_ read Get_SignatureMethod;
    property Reference: IXMLReferenceType_ read Get_Reference;
  end;

{ IXMLCanonicalizationMethod_ }

  IXMLCanonicalizationMethod_ = interface(IXMLNode)
    ['{F6BDDE00-EDE1-4EF2-B1A1-31F66802FB41}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
  end;

{ IXMLSignatureMethod_ }

  IXMLSignatureMethod_ = interface(IXMLNode)
    ['{02A20ED7-AB86-41AF-9ADC-87E40B8CA47D}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
  end;

{ IXMLReferenceType_ }

  IXMLReferenceType_ = interface(IXMLNode)
    ['{DAF1ED46-8521-4A51-AEA7-FDAD3629B120}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_URI: UnicodeString;
    function Get_Type_: UnicodeString;
    function Get_Transforms: IXMLTransformsType_;
    function Get_DigestMethod: IXMLDigestMethod_;
    function Get_DigestValue: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_URI(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_DigestValue(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property URI: UnicodeString read Get_URI write Set_URI;
    property Type_: UnicodeString read Get_Type_ write Set_Type_;
    property Transforms: IXMLTransformsType_ read Get_Transforms;
    property DigestMethod: IXMLDigestMethod_ read Get_DigestMethod;
    property DigestValue: UnicodeString read Get_DigestValue write Set_DigestValue;
  end;

{ IXMLTransformsType_ }

  IXMLTransformsType_ = interface(IXMLNodeCollection)
    ['{72CA9B0D-72F2-4229-9C80-6264FFE2CD8D}']
    { Property Accessors }
    function Get_Transform(Index: Integer): IXMLTransformType_;
    { Methods & Properties }
    function Add: IXMLTransformType_;
    function Insert(const Index: Integer): IXMLTransformType_;
    property Transform[Index: Integer]: IXMLTransformType_ read Get_Transform; default;
  end;

{ IXMLTransformType_ }

  IXMLTransformType_ = interface(IXMLNodeCollection)
    ['{11258CAC-97FE-4EFC-BF66-F6A46A85DEDF}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    function Get_XPath(Index: Integer): UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    function Add(const XPath: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const XPath: UnicodeString): IXMLNode;
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
    property XPath[Index: Integer]: UnicodeString read Get_XPath; default;
  end;

{ IXMLDigestMethod_ }

  IXMLDigestMethod_ = interface(IXMLNode)
    ['{4DD67AE2-2EB6-4623-BE7D-64A2B613A60A}']
    { Property Accessors }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    { Methods & Properties }
    property Algorithm: UnicodeString read Get_Algorithm write Set_Algorithm;
  end;

{ IXMLSignatureValueType_ }

  IXMLSignatureValueType_ = interface(IXMLNode)
    ['{29B17237-0AF1-40C8-B2C1-3A10AD578627}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
  end;

{ IXMLKeyInfoType_ }

  IXMLKeyInfoType_ = interface(IXMLNode)
    ['{B536AEBA-9640-4A78-984E-9051299438E6}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_X509Data: IXMLX509DataType_;
    procedure Set_Id(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property X509Data: IXMLX509DataType_ read Get_X509Data;
  end;

{ IXMLX509DataType_ }

  IXMLX509DataType_ = interface(IXMLNode)
    ['{06CB4D96-5580-4A51-909D-58543BB88EA8}']
    { Property Accessors }
    function Get_X509Certificate: UnicodeString;
    procedure Set_X509Certificate(Value: UnicodeString);
    { Methods & Properties }
    property X509Certificate: UnicodeString read Get_X509Certificate write Set_X509Certificate;
  end;

{ IXMLTConsReciMDFe }

  IXMLTConsReciMDFe = interface(IXMLNode)
    ['{3346F92A-A105-472E-B198-488B246DFE5B}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_NRec: UnicodeString;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_NRec(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property NRec: UnicodeString read Get_NRec write Set_NRec;
  end;

{ IXMLTRetConsReciMDFe }

  IXMLTRetConsReciMDFe = interface(IXMLNode)
    ['{D9C1C49C-4667-4F46-8676-CE5EC7D47E7D}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_NRec: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_ProtMDFe: IXMLTProtMDFe;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_NRec(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property VerAplic: UnicodeString read Get_VerAplic write Set_VerAplic;
    property NRec: UnicodeString read Get_NRec write Set_NRec;
    property CStat: UnicodeString read Get_CStat write Set_CStat;
    property XMotivo: UnicodeString read Get_XMotivo write Set_XMotivo;
    property CUF: UnicodeString read Get_CUF write Set_CUF;
    property ProtMDFe: IXMLTProtMDFe read Get_ProtMDFe;
  end;

{ IXMLTEvento }

  IXMLTEvento = interface(IXMLNode)
    ['{C38618AF-D936-4849-BE50-A5BEBEDD52DE}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_InfEvento: IXMLInfEvento;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property InfEvento: IXMLInfEvento read Get_InfEvento;
    property Signature: IXMLSignatureType_ read Get_Signature;
  end;

{ IXMLInfEvento }

  IXMLInfEvento = interface(IXMLNode)
    ['{6689B614-5BE9-47C2-80C6-1B2082AE1359}']
    { Property Accessors }
    function Get_Id: UnicodeString;
    function Get_COrgao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_CNPJ: UnicodeString;
    function Get_ChMDFe: UnicodeString;
    function Get_DhEvento: UnicodeString;
    function Get_TpEvento: UnicodeString;
    function Get_NSeqEvento: UnicodeString;
    function Get_DetEvento: IXMLDetEvento;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_COrgao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_ChMDFe(Value: UnicodeString);
    procedure Set_DhEvento(Value: UnicodeString);
    procedure Set_TpEvento(Value: UnicodeString);
    procedure Set_NSeqEvento(Value: UnicodeString);
    { Methods & Properties }
    property Id: UnicodeString read Get_Id write Set_Id;
    property COrgao: UnicodeString read Get_COrgao write Set_COrgao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property ChMDFe: UnicodeString read Get_ChMDFe write Set_ChMDFe;
    property DhEvento: UnicodeString read Get_DhEvento write Set_DhEvento;
    property TpEvento: UnicodeString read Get_TpEvento write Set_TpEvento;
    property NSeqEvento: UnicodeString read Get_NSeqEvento write Set_NSeqEvento;
    property DetEvento: IXMLDetEvento read Get_DetEvento;
  end;

{ IXMLDetEvento }

  IXMLDetEvento = interface(IXMLNode)
    ['{53AEF855-5030-4164-BD49-03FE07BA45FB}']
    { Property Accessors }
    function Get_VersaoEvento: UnicodeString;
    procedure Set_VersaoEvento(Value: UnicodeString);
    { Methods & Properties }
    property VersaoEvento: UnicodeString read Get_VersaoEvento write Set_VersaoEvento;
  end;

{ IXMLTRetEvento }

  IXMLTRetEvento = interface(IXMLNode)
    ['{D2970D57-0D55-4735-B673-AB1ADD83332E}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_InfEvento: IXMLInfEvento;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property InfEvento: IXMLInfEvento read Get_InfEvento;
    property Signature: IXMLSignatureType_ read Get_Signature;
  end;

{ IXMLTProcEvento }

  IXMLTProcEvento = interface(IXMLNode)
    ['{74CB68B8-0803-4890-B920-6BA4BC278E82}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_EventoMDFe: IXMLTEvento;
    function Get_RetEventoMDFe: IXMLTRetEvento;
    procedure Set_Versao(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property EventoMDFe: IXMLTEvento read Get_EventoMDFe;
    property RetEventoMDFe: IXMLTRetEvento read Get_RetEventoMDFe;
  end;

{ IXMLTProcEventoList }

  IXMLTProcEventoList = interface(IXMLNodeCollection)
    ['{B13C8FD2-10F0-4B6B-80A1-C2657190AE07}']
    { Methods & Properties }
    function Add: IXMLTProcEvento;
    function Insert(const Index: Integer): IXMLTProcEvento;

    function Get_Item(Index: Integer): IXMLTProcEvento;
    property Items[Index: Integer]: IXMLTProcEvento read Get_Item; default;
  end;

{ IXMLTRetConsSitMDFe }

  IXMLTRetConsSitMDFe = interface(IXMLNode)
    ['{8D078B69-08D9-468D-9E7A-8077A56E8DD0}']
    { Property Accessors }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_ProtMDFe: IXMLTProtMDFe;
    function Get_ProcEventoMDFe: IXMLTProcEventoList;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
    { Methods & Properties }
    property Versao: UnicodeString read Get_Versao write Set_Versao;
    property TpAmb: UnicodeString read Get_TpAmb write Set_TpAmb;
    property VerAplic: UnicodeString read Get_VerAplic write Set_VerAplic;
    property CStat: UnicodeString read Get_CStat write Set_CStat;
    property XMotivo: UnicodeString read Get_XMotivo write Set_XMotivo;
    property CUF: UnicodeString read Get_CUF write Set_CUF;
    property ProtMDFe: IXMLTProtMDFe read Get_ProtMDFe;
    property ProcEventoMDFe: IXMLTProcEventoList read Get_ProcEventoMDFe;
  end;

{ Forward Decls }

  TXMLTConsSitMDFe = class;
  TXMLTProtMDFe = class;
  TXMLInfProt = class;
  TXMLSignatureType_ = class;
  TXMLSignedInfoType_ = class;
  TXMLCanonicalizationMethod_ = class;
  TXMLSignatureMethod_ = class;
  TXMLReferenceType_ = class;
  TXMLTransformsType_ = class;
  TXMLTransformType_ = class;
  TXMLDigestMethod_ = class;
  TXMLSignatureValueType_ = class;
  TXMLKeyInfoType_ = class;
  TXMLX509DataType_ = class;
  TXMLTConsReciMDFe = class;
  TXMLTRetConsReciMDFe = class;
  TXMLTEvento = class;
  TXMLInfEvento = class;
  TXMLDetEvento = class;
  TXMLTRetEvento = class;
  TXMLTProcEvento = class;
  TXMLTProcEventoList = class;
  TXMLTRetConsSitMDFe = class;

{ TXMLTConsSitMDFe }

  TXMLTConsSitMDFe = class(TXMLNode, IXMLTConsSitMDFe)
  protected
    { IXMLTConsSitMDFe }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_XServ: UnicodeString;
    function Get_ChMDFe: UnicodeString;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_XServ(Value: UnicodeString);
    procedure Set_ChMDFe(Value: UnicodeString);
  end;

{ TXMLTProtMDFe }

  TXMLTProtMDFe = class(TXMLNode, IXMLTProtMDFe)
  protected
    { IXMLTProtMDFe }
    function Get_Versao: UnicodeString;
    function Get_InfProt: IXMLInfProt;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfProt }

  TXMLInfProt = class(TXMLNode, IXMLInfProt)
  protected
    { IXMLInfProt }
    function Get_Id: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_ChMDFe: UnicodeString;
    function Get_DhRecbto: UnicodeString;
    function Get_NProt: UnicodeString;
    function Get_DigVal: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_ChMDFe(Value: UnicodeString);
    procedure Set_DhRecbto(Value: UnicodeString);
    procedure Set_NProt(Value: UnicodeString);
    procedure Set_DigVal(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
  end;

{ TXMLSignatureType_ }

  TXMLSignatureType_ = class(TXMLNode, IXMLSignatureType_)
  protected
    { IXMLSignatureType_ }
    function Get_Id: UnicodeString;
    function Get_SignedInfo: IXMLSignedInfoType_;
    function Get_SignatureValue: IXMLSignatureValueType_;
    function Get_KeyInfo: IXMLKeyInfoType_;
    procedure Set_Id(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLSignedInfoType_ }

  TXMLSignedInfoType_ = class(TXMLNode, IXMLSignedInfoType_)
  protected
    { IXMLSignedInfoType_ }
    function Get_Id: UnicodeString;
    function Get_CanonicalizationMethod: IXMLCanonicalizationMethod_;
    function Get_SignatureMethod: IXMLSignatureMethod_;
    function Get_Reference: IXMLReferenceType_;
    procedure Set_Id(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLCanonicalizationMethod_ }

  TXMLCanonicalizationMethod_ = class(TXMLNode, IXMLCanonicalizationMethod_)
  protected
    { IXMLCanonicalizationMethod_ }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
  end;

{ TXMLSignatureMethod_ }

  TXMLSignatureMethod_ = class(TXMLNode, IXMLSignatureMethod_)
  protected
    { IXMLSignatureMethod_ }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
  end;

{ TXMLReferenceType_ }

  TXMLReferenceType_ = class(TXMLNode, IXMLReferenceType_)
  protected
    { IXMLReferenceType_ }
    function Get_Id: UnicodeString;
    function Get_URI: UnicodeString;
    function Get_Type_: UnicodeString;
    function Get_Transforms: IXMLTransformsType_;
    function Get_DigestMethod: IXMLDigestMethod_;
    function Get_DigestValue: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_URI(Value: UnicodeString);
    procedure Set_Type_(Value: UnicodeString);
    procedure Set_DigestValue(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTransformsType_ }

  TXMLTransformsType_ = class(TXMLNodeCollection, IXMLTransformsType_)
  protected
    { IXMLTransformsType_ }
    function Get_Transform(Index: Integer): IXMLTransformType_;
    function Add: IXMLTransformType_;
    function Insert(const Index: Integer): IXMLTransformType_;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTransformType_ }

  TXMLTransformType_ = class(TXMLNodeCollection, IXMLTransformType_)
  protected
    { IXMLTransformType_ }
    function Get_Algorithm: UnicodeString;
    function Get_XPath(Index: Integer): UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
    function Add(const XPath: UnicodeString): IXMLNode;
    function Insert(const Index: Integer; const XPath: UnicodeString): IXMLNode;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDigestMethod_ }

  TXMLDigestMethod_ = class(TXMLNode, IXMLDigestMethod_)
  protected
    { IXMLDigestMethod_ }
    function Get_Algorithm: UnicodeString;
    procedure Set_Algorithm(Value: UnicodeString);
  end;

{ TXMLSignatureValueType_ }

  TXMLSignatureValueType_ = class(TXMLNode, IXMLSignatureValueType_)
  protected
    { IXMLSignatureValueType_ }
    function Get_Id: UnicodeString;
    procedure Set_Id(Value: UnicodeString);
  end;

{ TXMLKeyInfoType_ }

  TXMLKeyInfoType_ = class(TXMLNode, IXMLKeyInfoType_)
  protected
    { IXMLKeyInfoType_ }
    function Get_Id: UnicodeString;
    function Get_X509Data: IXMLX509DataType_;
    procedure Set_Id(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLX509DataType_ }

  TXMLX509DataType_ = class(TXMLNode, IXMLX509DataType_)
  protected
    { IXMLX509DataType_ }
    function Get_X509Certificate: UnicodeString;
    procedure Set_X509Certificate(Value: UnicodeString);
  end;

{ TXMLTConsReciMDFe }

  TXMLTConsReciMDFe = class(TXMLNode, IXMLTConsReciMDFe)
  protected
    { IXMLTConsReciMDFe }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_NRec: UnicodeString;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_NRec(Value: UnicodeString);
  end;

{ TXMLTRetConsReciMDFe }

  TXMLTRetConsReciMDFe = class(TXMLNode, IXMLTRetConsReciMDFe)
  protected
    { IXMLTRetConsReciMDFe }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_NRec: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_ProtMDFe: IXMLTProtMDFe;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_NRec(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTEvento }

  TXMLTEvento = class(TXMLNode, IXMLTEvento)
  protected
    { IXMLTEvento }
    function Get_Versao: UnicodeString;
    function Get_InfEvento: IXMLInfEvento;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLInfEvento }

  TXMLInfEvento = class(TXMLNode, IXMLInfEvento)
  protected
    { IXMLInfEvento }
    function Get_Id: UnicodeString;
    function Get_COrgao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_CNPJ: UnicodeString;
    function Get_ChMDFe: UnicodeString;
    function Get_DhEvento: UnicodeString;
    function Get_TpEvento: UnicodeString;
    function Get_NSeqEvento: UnicodeString;
    function Get_DetEvento: IXMLDetEvento;
    procedure Set_Id(Value: UnicodeString);
    procedure Set_COrgao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_ChMDFe(Value: UnicodeString);
    procedure Set_DhEvento(Value: UnicodeString);
    procedure Set_TpEvento(Value: UnicodeString);
    procedure Set_NSeqEvento(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLDetEvento }

  TXMLDetEvento = class(TXMLNode, IXMLDetEvento)
  protected
    { IXMLDetEvento }
    function Get_VersaoEvento: UnicodeString;
    procedure Set_VersaoEvento(Value: UnicodeString);
  end;

{ TXMLTRetEvento }

  TXMLTRetEvento = class(TXMLNode, IXMLTRetEvento)
  protected
    { IXMLTRetEvento }
    function Get_Versao: UnicodeString;
    function Get_InfEvento: IXMLInfEvento;
    function Get_Signature: IXMLSignatureType_;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTProcEvento }

  TXMLTProcEvento = class(TXMLNode, IXMLTProcEvento)
  protected
    { IXMLTProcEvento }
    function Get_Versao: UnicodeString;
    function Get_EventoMDFe: IXMLTEvento;
    function Get_RetEventoMDFe: IXMLTRetEvento;
    procedure Set_Versao(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTProcEventoList }

  TXMLTProcEventoList = class(TXMLNodeCollection, IXMLTProcEventoList)
  protected
    { IXMLTProcEventoList }
    function Add: IXMLTProcEvento;
    function Insert(const Index: Integer): IXMLTProcEvento;

    function Get_Item(Index: Integer): IXMLTProcEvento;
  end;

{ TXMLTRetConsSitMDFe }

  TXMLTRetConsSitMDFe = class(TXMLNode, IXMLTRetConsSitMDFe)
  private
    FProcEventoMDFe: IXMLTProcEventoList;
  protected
    { IXMLTRetConsSitMDFe }
    function Get_Versao: UnicodeString;
    function Get_TpAmb: UnicodeString;
    function Get_VerAplic: UnicodeString;
    function Get_CStat: UnicodeString;
    function Get_XMotivo: UnicodeString;
    function Get_CUF: UnicodeString;
    function Get_ProtMDFe: IXMLTProtMDFe;
    function Get_ProcEventoMDFe: IXMLTProcEventoList;
    procedure Set_Versao(Value: UnicodeString);
    procedure Set_TpAmb(Value: UnicodeString);
    procedure Set_VerAplic(Value: UnicodeString);
    procedure Set_CStat(Value: UnicodeString);
    procedure Set_XMotivo(Value: UnicodeString);
    procedure Set_CUF(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ Global Functions }

function GetconsSitMDFe(Doc: IXMLDocument): IXMLTConsSitMDFe;
function LoadconsSitMDFe(const FileName: string): IXMLTConsSitMDFe;
function NewconsSitMDFe: IXMLTConsSitMDFe;

const
  TargetNamespace = 'http://www.portalfiscal.inf.br/mdfe';

implementation

{ Global Functions }

function GetconsSitMDFe(Doc: IXMLDocument): IXMLTConsSitMDFe;
begin
  Result := Doc.GetDocBinding('consSitMDFe', TXMLTConsSitMDFe, TargetNamespace) as IXMLTConsSitMDFe;
end;

function LoadconsSitMDFe(const FileName: string): IXMLTConsSitMDFe;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('consSitMDFe', TXMLTConsSitMDFe, TargetNamespace) as IXMLTConsSitMDFe;
end;

function NewconsSitMDFe: IXMLTConsSitMDFe;
begin
  Result := NewXMLDocument.GetDocBinding('consSitMDFe', TXMLTConsSitMDFe, TargetNamespace) as IXMLTConsSitMDFe;
end;

{ TXMLTConsSitMDFe }

function TXMLTConsSitMDFe.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTConsSitMDFe.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTConsSitMDFe.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTConsSitMDFe.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTConsSitMDFe.Get_XServ: UnicodeString;
begin
  Result := ChildNodes['xServ'].Text;
end;

procedure TXMLTConsSitMDFe.Set_XServ(Value: UnicodeString);
begin
  ChildNodes['xServ'].NodeValue := Value;
end;

function TXMLTConsSitMDFe.Get_ChMDFe: UnicodeString;
begin
  Result := ChildNodes['chMDFe'].Text;
end;

procedure TXMLTConsSitMDFe.Set_ChMDFe(Value: UnicodeString);
begin
  ChildNodes['chMDFe'].NodeValue := Value;
end;

{ TXMLTProtMDFe }

procedure TXMLTProtMDFe.AfterConstruction;
begin
  RegisterChildNode('infProt', TXMLInfProt);
  RegisterChildNode('Signature', TXMLSignatureType_);
  inherited;
end;

function TXMLTProtMDFe.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTProtMDFe.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTProtMDFe.Get_InfProt: IXMLInfProt;
begin
  Result := ChildNodes['infProt'] as IXMLInfProt;
end;

function TXMLTProtMDFe.Get_Signature: IXMLSignatureType_;
begin
  Result := ChildNodes['Signature'] as IXMLSignatureType_;
end;

{ TXMLInfProt }

function TXMLInfProt.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLInfProt.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLInfProt.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLInfProt.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLInfProt.Get_VerAplic: UnicodeString;
begin
  Result := ChildNodes['verAplic'].Text;
end;

procedure TXMLInfProt.Set_VerAplic(Value: UnicodeString);
begin
  ChildNodes['verAplic'].NodeValue := Value;
end;

function TXMLInfProt.Get_ChMDFe: UnicodeString;
begin
  Result := ChildNodes['chMDFe'].Text;
end;

procedure TXMLInfProt.Set_ChMDFe(Value: UnicodeString);
begin
  ChildNodes['chMDFe'].NodeValue := Value;
end;

function TXMLInfProt.Get_DhRecbto: UnicodeString;
begin
  Result := ChildNodes['dhRecbto'].Text;
end;

procedure TXMLInfProt.Set_DhRecbto(Value: UnicodeString);
begin
  ChildNodes['dhRecbto'].NodeValue := Value;
end;

function TXMLInfProt.Get_NProt: UnicodeString;
begin
  Result := ChildNodes['nProt'].Text;
end;

procedure TXMLInfProt.Set_NProt(Value: UnicodeString);
begin
  ChildNodes['nProt'].NodeValue := Value;
end;

function TXMLInfProt.Get_DigVal: UnicodeString;
begin
  Result := ChildNodes['digVal'].Text;
end;

procedure TXMLInfProt.Set_DigVal(Value: UnicodeString);
begin
  ChildNodes['digVal'].NodeValue := Value;
end;

function TXMLInfProt.Get_CStat: UnicodeString;
begin
  Result := ChildNodes['cStat'].Text;
end;

procedure TXMLInfProt.Set_CStat(Value: UnicodeString);
begin
  ChildNodes['cStat'].NodeValue := Value;
end;

function TXMLInfProt.Get_XMotivo: UnicodeString;
begin
  Result := ChildNodes['xMotivo'].Text;
end;

procedure TXMLInfProt.Set_XMotivo(Value: UnicodeString);
begin
  ChildNodes['xMotivo'].NodeValue := Value;
end;

{ TXMLSignatureType_ }

procedure TXMLSignatureType_.AfterConstruction;
begin
  RegisterChildNode('SignedInfo', TXMLSignedInfoType_);
  RegisterChildNode('SignatureValue', TXMLSignatureValueType_);
  RegisterChildNode('KeyInfo', TXMLKeyInfoType_);
  inherited;
end;

function TXMLSignatureType_.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLSignatureType_.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLSignatureType_.Get_SignedInfo: IXMLSignedInfoType_;
begin
  Result := ChildNodes['SignedInfo'] as IXMLSignedInfoType_;
end;

function TXMLSignatureType_.Get_SignatureValue: IXMLSignatureValueType_;
begin
  Result := ChildNodes['SignatureValue'] as IXMLSignatureValueType_;
end;

function TXMLSignatureType_.Get_KeyInfo: IXMLKeyInfoType_;
begin
  Result := ChildNodes['KeyInfo'] as IXMLKeyInfoType_;
end;

{ TXMLSignedInfoType_ }

procedure TXMLSignedInfoType_.AfterConstruction;
begin
  RegisterChildNode('CanonicalizationMethod', TXMLCanonicalizationMethod_);
  RegisterChildNode('SignatureMethod', TXMLSignatureMethod_);
  RegisterChildNode('Reference', TXMLReferenceType_);
  inherited;
end;

function TXMLSignedInfoType_.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLSignedInfoType_.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLSignedInfoType_.Get_CanonicalizationMethod: IXMLCanonicalizationMethod_;
begin
  Result := ChildNodes['CanonicalizationMethod'] as IXMLCanonicalizationMethod_;
end;

function TXMLSignedInfoType_.Get_SignatureMethod: IXMLSignatureMethod_;
begin
  Result := ChildNodes['SignatureMethod'] as IXMLSignatureMethod_;
end;

function TXMLSignedInfoType_.Get_Reference: IXMLReferenceType_;
begin
  Result := ChildNodes['Reference'] as IXMLReferenceType_;
end;

{ TXMLCanonicalizationMethod_ }

function TXMLCanonicalizationMethod_.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLCanonicalizationMethod_.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

{ TXMLSignatureMethod_ }

function TXMLSignatureMethod_.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLSignatureMethod_.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

{ TXMLReferenceType_ }

procedure TXMLReferenceType_.AfterConstruction;
begin
  RegisterChildNode('Transforms', TXMLTransformsType_);
  RegisterChildNode('DigestMethod', TXMLDigestMethod_);
  inherited;
end;

function TXMLReferenceType_.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLReferenceType_.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLReferenceType_.Get_URI: UnicodeString;
begin
  Result := AttributeNodes['URI'].Text;
end;

procedure TXMLReferenceType_.Set_URI(Value: UnicodeString);
begin
  SetAttribute('URI', Value);
end;

function TXMLReferenceType_.Get_Type_: UnicodeString;
begin
  Result := AttributeNodes['Type'].Text;
end;

procedure TXMLReferenceType_.Set_Type_(Value: UnicodeString);
begin
  SetAttribute('Type', Value);
end;

function TXMLReferenceType_.Get_Transforms: IXMLTransformsType_;
begin
  Result := ChildNodes['Transforms'] as IXMLTransformsType_;
end;

function TXMLReferenceType_.Get_DigestMethod: IXMLDigestMethod_;
begin
  Result := ChildNodes['DigestMethod'] as IXMLDigestMethod_;
end;

function TXMLReferenceType_.Get_DigestValue: UnicodeString;
begin
  Result := ChildNodes['DigestValue'].Text;
end;

procedure TXMLReferenceType_.Set_DigestValue(Value: UnicodeString);
begin
  ChildNodes['DigestValue'].NodeValue := Value;
end;

{ TXMLTransformsType_ }

procedure TXMLTransformsType_.AfterConstruction;
begin
  RegisterChildNode('Transform', TXMLTransformType_);
  ItemTag := 'Transform';
  ItemInterface := IXMLTransformType_;
  inherited;
end;

function TXMLTransformsType_.Get_Transform(Index: Integer): IXMLTransformType_;
begin
  Result := List[Index] as IXMLTransformType_;
end;

function TXMLTransformsType_.Add: IXMLTransformType_;
begin
  Result := AddItem(-1) as IXMLTransformType_;
end;

function TXMLTransformsType_.Insert(const Index: Integer): IXMLTransformType_;
begin
  Result := AddItem(Index) as IXMLTransformType_;
end;

{ TXMLTransformType_ }

procedure TXMLTransformType_.AfterConstruction;
begin
  ItemTag := 'XPath';
  ItemInterface := IXMLNode;
  inherited;
end;

function TXMLTransformType_.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLTransformType_.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

function TXMLTransformType_.Get_XPath(Index: Integer): UnicodeString;
begin
  Result := List[Index].Text;
end;

function TXMLTransformType_.Add(const XPath: UnicodeString): IXMLNode;
begin
  Result := AddItem(-1);
  Result.NodeValue := XPath;
end;

function TXMLTransformType_.Insert(const Index: Integer; const XPath: UnicodeString): IXMLNode;
begin
  Result := AddItem(Index);
  Result.NodeValue := XPath;
end;

{ TXMLDigestMethod_ }

function TXMLDigestMethod_.Get_Algorithm: UnicodeString;
begin
  Result := AttributeNodes['Algorithm'].Text;
end;

procedure TXMLDigestMethod_.Set_Algorithm(Value: UnicodeString);
begin
  SetAttribute('Algorithm', Value);
end;

{ TXMLSignatureValueType_ }

function TXMLSignatureValueType_.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLSignatureValueType_.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

{ TXMLKeyInfoType_ }

procedure TXMLKeyInfoType_.AfterConstruction;
begin
  RegisterChildNode('X509Data', TXMLX509DataType_);
  inherited;
end;

function TXMLKeyInfoType_.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLKeyInfoType_.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLKeyInfoType_.Get_X509Data: IXMLX509DataType_;
begin
  Result := ChildNodes['X509Data'] as IXMLX509DataType_;
end;

{ TXMLX509DataType_ }

function TXMLX509DataType_.Get_X509Certificate: UnicodeString;
begin
  Result := ChildNodes['X509Certificate'].Text;
end;

procedure TXMLX509DataType_.Set_X509Certificate(Value: UnicodeString);
begin
  ChildNodes['X509Certificate'].NodeValue := Value;
end;

{ TXMLTConsReciMDFe }

function TXMLTConsReciMDFe.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTConsReciMDFe.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTConsReciMDFe.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTConsReciMDFe.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTConsReciMDFe.Get_NRec: UnicodeString;
begin
  Result := ChildNodes['nRec'].Text;
end;

procedure TXMLTConsReciMDFe.Set_NRec(Value: UnicodeString);
begin
  ChildNodes['nRec'].NodeValue := Value;
end;

{ TXMLTRetConsReciMDFe }

procedure TXMLTRetConsReciMDFe.AfterConstruction;
begin
  RegisterChildNode('protMDFe', TXMLTProtMDFe);
  inherited;
end;

function TXMLTRetConsReciMDFe.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTRetConsReciMDFe.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTRetConsReciMDFe.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTRetConsReciMDFe.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTRetConsReciMDFe.Get_VerAplic: UnicodeString;
begin
  Result := ChildNodes['verAplic'].Text;
end;

procedure TXMLTRetConsReciMDFe.Set_VerAplic(Value: UnicodeString);
begin
  ChildNodes['verAplic'].NodeValue := Value;
end;

function TXMLTRetConsReciMDFe.Get_NRec: UnicodeString;
begin
  Result := ChildNodes['nRec'].Text;
end;

procedure TXMLTRetConsReciMDFe.Set_NRec(Value: UnicodeString);
begin
  ChildNodes['nRec'].NodeValue := Value;
end;

function TXMLTRetConsReciMDFe.Get_CStat: UnicodeString;
begin
  Result := ChildNodes['cStat'].Text;
end;

procedure TXMLTRetConsReciMDFe.Set_CStat(Value: UnicodeString);
begin
  ChildNodes['cStat'].NodeValue := Value;
end;

function TXMLTRetConsReciMDFe.Get_XMotivo: UnicodeString;
begin
  Result := ChildNodes['xMotivo'].Text;
end;

procedure TXMLTRetConsReciMDFe.Set_XMotivo(Value: UnicodeString);
begin
  ChildNodes['xMotivo'].NodeValue := Value;
end;

function TXMLTRetConsReciMDFe.Get_CUF: UnicodeString;
begin
  Result := ChildNodes['cUF'].Text;
end;

procedure TXMLTRetConsReciMDFe.Set_CUF(Value: UnicodeString);
begin
  ChildNodes['cUF'].NodeValue := Value;
end;

function TXMLTRetConsReciMDFe.Get_ProtMDFe: IXMLTProtMDFe;
begin
  Result := ChildNodes['protMDFe'] as IXMLTProtMDFe;
end;

{ TXMLTEvento }

procedure TXMLTEvento.AfterConstruction;
begin
  RegisterChildNode('infEvento', TXMLInfEvento);
  RegisterChildNode('Signature', TXMLSignatureType_);
  inherited;
end;

function TXMLTEvento.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTEvento.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTEvento.Get_InfEvento: IXMLInfEvento;
begin
  Result := ChildNodes['infEvento'] as IXMLInfEvento;
end;

function TXMLTEvento.Get_Signature: IXMLSignatureType_;
begin
  Result := ChildNodes['Signature'] as IXMLSignatureType_;
end;

{ TXMLInfEvento }

procedure TXMLInfEvento.AfterConstruction;
begin
  RegisterChildNode('detEvento', TXMLDetEvento);
  inherited;
end;

function TXMLInfEvento.Get_Id: UnicodeString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLInfEvento.Set_Id(Value: UnicodeString);
begin
  SetAttribute('Id', Value);
end;

function TXMLInfEvento.Get_COrgao: UnicodeString;
begin
  Result := ChildNodes['cOrgao'].Text;
end;

procedure TXMLInfEvento.Set_COrgao(Value: UnicodeString);
begin
  ChildNodes['cOrgao'].NodeValue := Value;
end;

function TXMLInfEvento.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLInfEvento.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLInfEvento.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLInfEvento.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLInfEvento.Get_ChMDFe: UnicodeString;
begin
  Result := ChildNodes['chMDFe'].Text;
end;

procedure TXMLInfEvento.Set_ChMDFe(Value: UnicodeString);
begin
  ChildNodes['chMDFe'].NodeValue := Value;
end;

function TXMLInfEvento.Get_DhEvento: UnicodeString;
begin
  Result := ChildNodes['dhEvento'].Text;
end;

procedure TXMLInfEvento.Set_DhEvento(Value: UnicodeString);
begin
  ChildNodes['dhEvento'].NodeValue := Value;
end;

function TXMLInfEvento.Get_TpEvento: UnicodeString;
begin
  Result := ChildNodes['tpEvento'].Text;
end;

procedure TXMLInfEvento.Set_TpEvento(Value: UnicodeString);
begin
  ChildNodes['tpEvento'].NodeValue := Value;
end;

function TXMLInfEvento.Get_NSeqEvento: UnicodeString;
begin
  Result := ChildNodes['nSeqEvento'].Text;
end;

procedure TXMLInfEvento.Set_NSeqEvento(Value: UnicodeString);
begin
  ChildNodes['nSeqEvento'].NodeValue := Value;
end;

function TXMLInfEvento.Get_DetEvento: IXMLDetEvento;
begin
  Result := ChildNodes['detEvento'] as IXMLDetEvento;
end;

{ TXMLDetEvento }

function TXMLDetEvento.Get_VersaoEvento: UnicodeString;
begin
  Result := AttributeNodes['versaoEvento'].Text;
end;

procedure TXMLDetEvento.Set_VersaoEvento(Value: UnicodeString);
begin
  SetAttribute('versaoEvento', Value);
end;

{ TXMLTRetEvento }

procedure TXMLTRetEvento.AfterConstruction;
begin
  RegisterChildNode('infEvento', TXMLInfEvento);
  RegisterChildNode('Signature', TXMLSignatureType_);
  inherited;
end;

function TXMLTRetEvento.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTRetEvento.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTRetEvento.Get_InfEvento: IXMLInfEvento;
begin
  Result := ChildNodes['infEvento'] as IXMLInfEvento;
end;

function TXMLTRetEvento.Get_Signature: IXMLSignatureType_;
begin
  Result := ChildNodes['Signature'] as IXMLSignatureType_;
end;

{ TXMLTProcEvento }

procedure TXMLTProcEvento.AfterConstruction;
begin
  RegisterChildNode('eventoMDFe', TXMLTEvento);
  RegisterChildNode('retEventoMDFe', TXMLTRetEvento);
  inherited;
end;

function TXMLTProcEvento.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTProcEvento.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTProcEvento.Get_EventoMDFe: IXMLTEvento;
begin
  Result := ChildNodes['eventoMDFe'] as IXMLTEvento;
end;

function TXMLTProcEvento.Get_RetEventoMDFe: IXMLTRetEvento;
begin
  Result := ChildNodes['retEventoMDFe'] as IXMLTRetEvento;
end;

{ TXMLTProcEventoList }

function TXMLTProcEventoList.Add: IXMLTProcEvento;
begin
  Result := AddItem(-1) as IXMLTProcEvento;
end;

function TXMLTProcEventoList.Insert(const Index: Integer): IXMLTProcEvento;
begin
  Result := AddItem(Index) as IXMLTProcEvento;
end;

function TXMLTProcEventoList.Get_Item(Index: Integer): IXMLTProcEvento;
begin
  Result := List[Index] as IXMLTProcEvento;
end;

{ TXMLTRetConsSitMDFe }

procedure TXMLTRetConsSitMDFe.AfterConstruction;
begin
  RegisterChildNode('protMDFe', TXMLTProtMDFe);
  RegisterChildNode('procEventoMDFe', TXMLTProcEvento);
  FProcEventoMDFe := CreateCollection(TXMLTProcEventoList, IXMLTProcEvento, 'procEventoMDFe') as IXMLTProcEventoList;
  inherited;
end;

function TXMLTRetConsSitMDFe.Get_Versao: UnicodeString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTRetConsSitMDFe.Set_Versao(Value: UnicodeString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTRetConsSitMDFe.Get_TpAmb: UnicodeString;
begin
  Result := ChildNodes['tpAmb'].Text;
end;

procedure TXMLTRetConsSitMDFe.Set_TpAmb(Value: UnicodeString);
begin
  ChildNodes['tpAmb'].NodeValue := Value;
end;

function TXMLTRetConsSitMDFe.Get_VerAplic: UnicodeString;
begin
  Result := ChildNodes['verAplic'].Text;
end;

procedure TXMLTRetConsSitMDFe.Set_VerAplic(Value: UnicodeString);
begin
  ChildNodes['verAplic'].NodeValue := Value;
end;

function TXMLTRetConsSitMDFe.Get_CStat: UnicodeString;
begin
  Result := ChildNodes['cStat'].Text;
end;

procedure TXMLTRetConsSitMDFe.Set_CStat(Value: UnicodeString);
begin
  ChildNodes['cStat'].NodeValue := Value;
end;

function TXMLTRetConsSitMDFe.Get_XMotivo: UnicodeString;
begin
  Result := ChildNodes['xMotivo'].Text;
end;

procedure TXMLTRetConsSitMDFe.Set_XMotivo(Value: UnicodeString);
begin
  ChildNodes['xMotivo'].NodeValue := Value;
end;

function TXMLTRetConsSitMDFe.Get_CUF: UnicodeString;
begin
  Result := ChildNodes['cUF'].Text;
end;

procedure TXMLTRetConsSitMDFe.Set_CUF(Value: UnicodeString);
begin
  ChildNodes['cUF'].NodeValue := Value;
end;

function TXMLTRetConsSitMDFe.Get_ProtMDFe: IXMLTProtMDFe;
begin
  Result := ChildNodes['protMDFe'] as IXMLTProtMDFe;
end;

function TXMLTRetConsSitMDFe.Get_ProcEventoMDFe: IXMLTProcEventoList;
begin
  Result := FProcEventoMDFe;
end;

end.