unit MDFeLEnC_0100a;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkValUsu,
  dmkDBLookupComboBox, dmkEditCB, Menus, Grids, DBGrids, UrlMon, ComCtrls,
  OleCtrls, SHDocVw, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmMDFeLEnC_0100a = class(TForm)
    PnDados: TPanel;
    DsMDFeLEnC: TDataSource;
    QrMDFeLEnC: TmySQLQuery;
    PnEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    Label4: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    VuEmpresa: TdmkValUsu;
    QrMDFeLEnCDataHora_TXT: TWideStringField;
    PMLote: TPopupMenu;
    Incluinovolote1: TMenuItem;
    Alteraloteatual1: TMenuItem;
    Excluiloteatual1: TMenuItem;
    PMMDFe: TPopupMenu;
    IncluiMDFeaoloteatual1: TMenuItem;
    DsMDFeLEnI: TDataSource;
    QrMDFeLEnI: TmySQLQuery;
    QrMDFeCabA: TmySQLQuery;
    QrMDFeCabAEmpresa: TIntegerField;
    QrMDFeCabAFatID: TIntegerField;
    QrMDFeCabAFatNum: TIntegerField;
    QrMDFeCabAide_nMDF: TIntegerField;
    QrMDFeCabAide_dEmi: TDateField;
    QrMDFeCabAid: TWideStringField;
    QrMDFeCabAide_cMDF: TIntegerField;
    QrMDFeCabAide_serie: TIntegerField;
    QrMDFeCabAStatus: TSmallintField;
    DsMDFeCabA: TDataSource;
    N1: TMenuItem;
    Envialoteaofisco1: TMenuItem;
    QrMDFeLEnCCodigo: TIntegerField;
    QrMDFeLEnCCodUsu: TIntegerField;
    QrMDFeLEnCNome: TWideStringField;
    QrMDFeLEnCEmpresa: TIntegerField;
    QrMDFeLEnCversao: TFloatField;
    QrMDFeLEnCtpAmb: TSmallintField;
    QrMDFeLEnCverAplic: TWideStringField;
    QrMDFeLEnCcStat: TIntegerField;
    QrMDFeLEnCxMotivo: TWideStringField;
    QrMDFeLEnCcUF: TSmallintField;
    QrMDFeLEnCnRec: TWideStringField;
    QrMDFeLEnCdhRecbto: TDateTimeField;
    QrMDFeLEnCtMed: TIntegerField;
    QrMDFeLEnCFilial: TIntegerField;
    QrMDFeLEnCNO_Empresa: TWideStringField;
    QrMDFeLEnCNO_Ambiente: TWideStringField;
    Verificalotenofisco1: TMenuItem;
    QrMDFeCabAinfProt_cStat: TIntegerField;
    QrMDFeCabAinfProt_xMotivo: TWideStringField;
    N2: TMenuItem;
    Lerarquivo1: TMenuItem;
    Enviodelote1: TMenuItem;
    Consultadelote1: TMenuItem;
    PainelData: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    GroupBox1: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    GroupBox2: TGroupBox;
    Label16: TLabel;
    Label10: TLabel;
    Label17: TLabel;
    DBEdit13: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdCodigo: TDBEdit;
    QrMDFeLEnM: TmySQLQuery;
    DsMDFeLEnM: TDataSource;
    DBGrid2: TDBGrid;
    QrMDFeLEnMControle: TIntegerField;
    QrMDFeLEnMversao: TFloatField;
    QrMDFeLEnMtpAmb: TSmallintField;
    QrMDFeLEnMverAplic: TWideStringField;
    QrMDFeLEnMcStat: TIntegerField;
    QrMDFeLEnMxMotivo: TWideStringField;
    QrMDFeLEnMcUF: TSmallintField;
    QrMDFeLEnMnRec: TWideStringField;
    QrMDFeLEnMdhRecbto: TDateTimeField;
    QrMDFeLEnMtMed: TIntegerField;
    RemoveMDFesselecionadas1: TMenuItem;
    N3: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    WB_XML: TWebBrowser;
    PMXML: TPopupMenu;
    AbrirXMLnonavegadordestajanela1: TMenuItem;
    SelecionaroXML1: TMenuItem;
    Lote1: TMenuItem;
    Assinada1: TMenuItem;
    Gerada2: TMenuItem;
    AbrirXMLdaNMDFenonavegadorpadro1: TMenuItem;
    Assina1: TMenuItem;
    Gerada1: TMenuItem;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtLote: TBitBtn;
    BtMDFe: TBitBtn;
    BtXML: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    RetornodoLotedeenvio1: TMenuItem;
    RGIndSinc: TdmkRadioGroup;
    DBRadioGroup1: TDBRadioGroup;
    QrMDFeLEnCIndSinc: TSmallintField;
    N4: TMenuItem;
    ValidarXMLdaMDFe1: TMenuItem;
    Assinada2: TMenuItem;
    Lotedeenvio1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrMDFeLEnCAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrMDFeLEnCBeforeOpen(DataSet: TDataSet);
    procedure BtLoteClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure QrMDFeLEnCCalcFields(DataSet: TDataSet);
    procedure Alteraloteatual1Click(Sender: TObject);
    procedure Incluinovolote1Click(Sender: TObject);
    procedure PMLotePopup(Sender: TObject);
    procedure PMMDFePopup(Sender: TObject);
    procedure IncluiMDFeaoloteatual1Click(Sender: TObject);
    procedure BtMDFeClick(Sender: TObject);
    procedure QrMDFeLEnCAfterScroll(DataSet: TDataSet);
    procedure Envialoteaofisco1Click(Sender: TObject);
    procedure Verificalotenofisco1Click(Sender: TObject);
    procedure Enviodelote1Click(Sender: TObject);
    procedure Consultadelote1Click(Sender: TObject);
    procedure RemoveMDFesselecionadas1Click(Sender: TObject);
    procedure Gerada1Click(Sender: TObject);
    procedure Assina1Click(Sender: TObject);
    procedure Gerada2Click(Sender: TObject);
    procedure Assinada1Click(Sender: TObject);
    procedure Lote1Click(Sender: TObject);
    procedure SelecionaroXML1Click(Sender: TObject);
    procedure BtXMLClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure RetornodoLotedeenvio1Click(Sender: TObject);
    procedure PMXMLPopup(Sender: TObject);
    procedure Assinada2Click(Sender: TObject);
    procedure Lotedeenvio1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure EnviaLoteAoFisco(SohLer: Boolean);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenMDFeLEnI();
    procedure ReopenMDFeLEnM();
  end;

var
  FmMDFeLEnC_0100a: TFmMDFeLEnC_0100a;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, MyDBCheck, MDFeLEnI_0100a, ModuleMDFe_0000,
  MDFeSteps_0100a, UnMDFe_PF, UnXXe_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmMDFeLEnC_0100a.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmMDFeLEnC_0100a.Lote1Click(Sender: TObject);
var
  LoteStr: String;
begin
  LoteStr := FormatFloat('000000000', QrMDFeLEnCCodigo.Value);
  DmMDFe_0000.AbreXML_NoTWebBrowser(QrMDFeLEnCEmpresa.Value, MDFE_EXT_ENV_LOT_XML,
    LoteStr, True, WB_XML);
end;

procedure TFmMDFeLEnC_0100a.Lotedeenvio1Click(Sender: TObject);
var
  LoteStr, Arquivo: String;
begin
  LoteStr := FormatFloat('000000000', QrMDFeLEnCCodigo.Value);
  Arquivo := DmMDFe_0000.Obtem_Arquivo_XML_(QrMDFeLEnCEmpresa.Value,
               MDFE_EXT_ENV_LOT_XML, LoteStr, True);
  //
  XXe_PF.ValidaXML_XXe(Arquivo);
end;

procedure TFmMDFeLEnC_0100a.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrMDFeLEncCodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmMDFeLEnC_0100a.Verificalotenofisco1Click(Sender: TObject);
begin
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  //
  if DBCheck.CriaFm(TFmMDFeSteps_0100a, FmMDFeSteps_0100a, afmoNegarComAviso) then
  begin
    //FmMDFeSteps_0100a.PnLoteEnv.Visible := True;
    FmMDFeSteps_0100a.EdVersaoAcao.ValueVariant := DmMDFe_0000.QrOpcoesMDFeMDFeVerConLot.Value;
    FmMDFeSteps_0100a.Show;
    //
    FmMDFeSteps_0100a.RGAcao.ItemIndex := Integer(TMDFeServicoStep.mdfesrvConsultarLoteMDFeEnviado);  // 2 - Verifica lote no fisco
    FmMDFeSteps_0100a.PreparaConsultaLote(QrMDFeLEnCCodigo.Value, QrMDFeLEnCEmpresa.Value,
      QrMDFeLEnCnRec.Value);
    FmMDFeSteps_0100a.FQry      := QrMDFeLEnC;
    //
    //FmMDFeSteps_0100a.Destroy;
    //LocCod(QrMDFeLEnCCodigo.Value, QrMDFeLEnCCodigo.Value);
  end;
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmMDFeLEnC_0100a.DefParams;
begin
  VAR_GOTOTABELA := 'MDFeLEnc';
  VAR_GOTOMYSQLTABLE := QrMDFeLEnc;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT lot.*, ent.Filial, ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_Empresa');
  VAR_SQLx.Add('FROM mdfelenc lot');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=lot.Empresa');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE lot.Codigo>-1000');
  VAR_SQLx.Add('AND lot.Empresa IN (' + VAR_LIB_EMPRESAS + ')');
  //
  VAR_SQL1.Add('AND lot.Codigo=:P0');
  //
  VAR_SQL2.Add('AND lot.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND lot.Nome Like :P0');
  //
end;

procedure TFmMDFeLEnC_0100a.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'mdfelenc', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmMDFeLEnC_0100a.EnviaLoteAoFisco(SohLer: Boolean);
var
  IndSinc: TXXeIndSinc;
  Lote, Empresa: Integer;
begin
  IndSinc := TXXeIndSinc(QrMDFeLEnCindSinc.Value);
  Lote    := QrMDFeLEnCCodigo.Value;
  Empresa := QrMDFeLEnCEmpresa.Value;
  MDFe_PF.EnviaLoteAoFisco(IndSinc, Lote, Empresa, SohLer, QrMDFeLEnC);
  //LocCod(Lote, Lote);
end;

procedure TFmMDFeLEnC_0100a.Envialoteaofisco1Click(Sender: TObject);
begin
  EnviaLoteAoFisco(False);
end;

procedure TFmMDFeLEnC_0100a.Enviodelote1Click(Sender: TObject);
begin
  EnviaLoteAoFisco(True);
end;

procedure TFmMDFeLEnC_0100a.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PnDados.Visible := True;
      PnEdita.Visible := False;
    end;
    1:
    begin
      PnEdita.Visible := True;
      PnDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmMDFeLEnC_0100a.PMLotePopup(Sender: TObject);
var
  HabilitaA, HabilitaB, HabilitaC: Boolean;
begin
  HabilitaA := (QrMDFeLEnc.State = dsBrowse) and (QrMDFeLEnc.RecordCount > 0);
  HabilitaC := (QrMDFeCabA.State = dsBrowse) and (QrMDFeCabA.RecordCount > 0);
  Alteraloteatual1.Enabled := HabilitaA;
  // Leituras de arquivos
  LerArquivo1.Enabled      := HabilitaA;
  Enviodelote1.Enabled     := HabilitaA;
  Consultadelote1.Enabled  := HabilitaA;//? and (QrMDFeLEnc.);
  //
  HabilitaB := HabilitaA and (not (QrMDFeLEnCcStat.Value in ([103,104,105]))) and HabilitaC;
  Envialoteaofisco1.Enabled := HabilitaB;
  HabilitaB := HabilitaA and (QrMDFeLEnCcStat.Value > 102);
  Verificalotenofisco1.Enabled := HabilitaB;

  (*
  Envialoteaofisco1.Enabled    := True;
  Verificalotenofisco1.Enabled := True;
  (*desmarcar em testes*)
end;

procedure TFmMDFeLEnC_0100a.PMMDFePopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrMDFeLEnc.State <> dsInactive) and (QrMDFeLEnc.RecordCount > 0) and (QrMDFeLEnCcStat.Value <> 103);
  //
  Alteraloteatual1.Enabled        := Habilita;
  RemoveMDFesselecionadas1.Enabled := Habilita;
end;

procedure TFmMDFeLEnC_0100a.PMXMLPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrMDFeLEnC.State <> dsInactive) and (QrMDFeLEnC.RecordCount > 0);
  Enab2 := (QrMDFeCabA.State <> dsInactive) and (QrMDFeCabA.RecordCount > 0);
  //
  AbrirXMLnonavegadordestajanela1.Enabled := Enab and Enab2;
  AbrirXMLdaNMDFenonavegadorpadro1.Enabled := Enab and Enab2;
  ValidarXMLdaMDFe1.Enabled                := Enab and Enab2;
end;

procedure TFmMDFeLEnC_0100a.Consultadelote1Click(Sender: TObject);
begin
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  //
  if DBCheck.CriaFm(TFmMDFeSteps_0100a, FmMDFeSteps_0100a, afmoNegarComAviso) then
  begin
    //FmMDFeSteps_0100a.PnLoteEnv.Visible := True;
    FmMDFeSteps_0100a.EdVersaoAcao.ValueVariant := DmMDFe_0000.QrOpcoesMDFeMDFeVerConLot.Value;
    FmMDFeSteps_0100a.Show;
    //
    FmMDFeSteps_0100a.RGAcao.ItemIndex := Integer(TMDFeServicoStep.mdfesrvConsultarLoteMDFeEnviado);  // 2 - verifica lote no fisco
    FmMDFeSteps_0100a.CkSoLer.Checked  := True;
    FmMDFeSteps_0100a.PreparaConsultaLote(QrMDFeLEnCCodigo.Value,
      QrMDFeLEnCEmpresa.Value, QrMDFeLEnCnRec.Value);
    //FmMDFeSteps_0100a.FFormChamou      := 'FmMDFeLEnc_0100a';
    FmMDFeSteps_0100a.FQry      := QrMDFeLEnC;
    //
    //
    //FmMDFeSteps_0100a.Destroy;
    //
    //LocCod(QrMDFeLEnCCodigo.Value, QrMDFeLEnCCodigo.Value);
  end;
end;

procedure TFmMDFeLEnC_0100a.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmMDFeLEnC_0100a.QueryPrincipalAfterOpen;
begin
end;

procedure TFmMDFeLEnC_0100a.RemoveMDFesselecionadas1Click(Sender: TObject);
var
  Status: Integer;
begin
  QrMDFeLEnM.First;
  if (QrMDFeCabAStatus.Value < 100) or (not (QrMDFeLEnCcStat.Value in ([103,104,105])))
  or ((QrMDFeLEnMcStat.Value in ([103,104,105])) and (QrMDFeCabAStatus.Value > 200)) then
  begin
    if Geral.MB_Pergunta('Deseja realmente tirar a MDF-e deste lote?') = mrYes then
    begin
      if (QrMDFeCabAStatus.Value = 50)
      or ((QrMDFeCabAStatus.Value = 103) and (QrMDFeLEnMcStat.Value > 200)) then
        Status := 30
      else
        Status := QrMDFeCabAStatus.Value;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE mdfecaba SET LoteEnv = 0, Status=:P0 ');
      Dmod.QrUpd.SQL.Add('WHERE FatID=:P1 AND FatNum=:P2 AND Empresa=:P3');
      Dmod.QrUpd.Params[00].AsInteger := Status;
      Dmod.QrUpd.Params[01].AsInteger := QrMDFeCabAFatID.Value;
      Dmod.QrUpd.Params[02].AsInteger := QrMDFeCabAFatNum.Value;
      Dmod.QrUpd.Params[03].AsInteger := QrMDFeCabAEmpresa.Value;
      Dmod.QrUpd.ExecSQL;
      ReopenMDFeLEnI();
    end;
  end else
    Geral.MB_Aviso('A��o n�o permitida!');

end;

procedure TFmMDFeLEnC_0100a.ReopenMDFeLEnI;
begin
  QrMDFeCabA.Close;
  QrMDFeCabA.Params[0].AsInteger := QrMDFeLEnCCodigo.Value;
  QrMDFeCabA.Open;
end;

procedure TFmMDFeLEnC_0100a.ReopenMDFeLEnM;
begin
  QrMDFeLEnM.Close;
  QrMDFeLEnM.Params[0].AsInteger := QrMDFeLEnCCodigo.Value;
  QrMDFeLEnM.Open;
end;

procedure TFmMDFeLEnC_0100a.RetornodoLotedeenvio1Click(Sender: TObject);
var
  LoteStr: String;
begin
  LoteStr := FormatFloat('000000000', QrMDFeLEnCCodigo.Value);
  DmMDFe_0000.AbreXML_NoTWebBrowser(QrMDFeLEnCEmpresa.Value, MDFE_EXT_REC_XML,
    LoteStr, True, WB_XML);
end;

procedure TFmMDFeLEnC_0100a.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmMDFeLEnC_0100a.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmMDFeLEnC_0100a.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmMDFeLEnC_0100a.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmMDFeLEnC_0100a.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmMDFeLEnC_0100a.Alteraloteatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrMDFeLEnc, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'mdfelenc');
end;

procedure TFmMDFeLEnC_0100a.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrMDFeLEncCodigo.Value;
  Close;
end;

procedure TFmMDFeLEnC_0100a.BtXMLClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMXML, BtXML);
end;

procedure TFmMDFeLEnC_0100a.BtMDFeClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMMDFe, BtMDFe);
end;

procedure TFmMDFeLEnC_0100a.Assina1Click(Sender: TObject);
begin
  DmMDFe_0000.AbreXML_NoNavegadorPadrao(QrMDFeLEnCEmpresa.Value, MDFE_EXT_MDFE_XML,
    QrMDFeCabAid.Value, True);
end;

procedure TFmMDFeLEnC_0100a.Assinada1Click(Sender: TObject);
begin
  DmMDFe_0000.AbreXML_NoTWebBrowser(QrMDFeLEnCEmpresa.Value, MDFE_EXT_MDFE_XML,
    QrMDFeCabAid.Value, True, WB_XML);
end;

procedure TFmMDFeLEnC_0100a.Assinada2Click(Sender: TObject);
var
  Arquivo: String;
begin
  Arquivo := DmMDFe_0000.Obtem_Arquivo_XML_(QrMDFeLEnCEmpresa.Value,
                MDFE_EXT_MDFE_XML, QrMDFeCabAid.Value, True);
  //
  XXe_PF.ValidaXML_XXe(Arquivo);
end;

procedure TFmMDFeLEnC_0100a.BtConfirmaClick(Sender: TObject);
var
  Nome(*, verAplic, xMotivo, nRec, dhRecbto, cMsg, xMsg*): String;
  Codigo, CodUsu, Empresa, (*tpAmb, cStat, cUF, tMed,*) IndSinc: Integer;
  //versao, dhRecbtoTZD: Double;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  (*Empresa*)    DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
(*  versao         := ;
  tpAmb          := ;
  verAplic       := ;
  cStat          := ;
  xMotivo        := ;
  cUF            := ;
  nRec           := ;
  dhRecbto       := ;
  tMed           := ;
  cMsg           := ;
  xMsg           := ;
  dhRecbtoTZD    := ;*)
  IndSinc        := RGIndSinc.ItemIndex;

  //
  if ImgTipo.SQLType = stUpd then
    Codigo := EdCodigo.ValueVariant
  else
    Codigo := DModG.BuscaProximoCodigoInt('mdfectrl', 'mdfelenc', '', 0, 999999999, '');
  CodUsu := Codigo;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'mdfelenc', False, [
  'CodUsu', 'Nome', 'Empresa',
  (*'versao', 'tpAmb', 'verAplic',
  'cStat', 'xMotivo', 'cUF',
  'nRec', 'dhRecbto', 'tMed',
  'cMsg', 'xMsg', 'dhRecbtoTZD',*)
  'IndSinc'], [
  'Codigo'], [
  CodUsu, Nome, Empresa,
  (*versao, tpAmb, verAplic,
  cStat, xMotivo, cUF,
  nRec, dhRecbto, tMed,
  cMsg, xMsg, dhRecbtoTZD,*)
  IndSinc], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmMDFeLEnC_0100a.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'MDFeLEnc', 'Codigo');
  MostraEdicao(0, stLok, 0);
end;

procedure TFmMDFeLEnC_0100a.BtLoteClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMLote, BtLote);
end;

procedure TFmMDFeLEnC_0100a.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  PainelEdit.Align   := alClient;
  PageControl1.Align := alClient;
  PageControl1.ActivePageIndex := 0;
  CriaOForm;
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmMDFeLEnC_0100a.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrMDFeLEncCodigo.Value, LaRegistro.Caption);
end;

procedure TFmMDFeLEnC_0100a.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmMDFeLEnC_0100a.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmMDFeLEnC_0100a.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrMDFeLEncCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmMDFeLEnC_0100a.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmMDFeLEnC_0100a.QrMDFeLEnCAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmMDFeLEnC_0100a.QrMDFeLEnCAfterScroll(DataSet: TDataSet);
begin
  ReopenMDFeLEnI();
  ReopenMDFeLEnM();
end;

procedure TFmMDFeLEnC_0100a.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMDFeLEnC_0100a.SbQueryClick(Sender: TObject);
begin
  LocCod(QrMDFeLEncCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'MDFeLEnc', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmMDFeLEnC_0100a.SelecionaroXML1Click(Sender: TObject);
var
  Arquivo: String;
begin
  if MyObjects.FileOpenDialog(Self, 'C:\Dermatek\MDFE', '', 'Informe o aqruivo XML',
  '', [], Arquivo) then
    WB_XML.Navigate(Arquivo);
end;

procedure TFmMDFeLEnC_0100a.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMDFeLEnC_0100a.Gerada1Click(Sender: TObject);
begin
  DmMDFe_0000.AbreXML_NoNavegadorPadrao(QrMDFeLEnCEmpresa.Value, MDFE_EXT_MDFE_XML,
    QrMDFeCabAid.Value, False);
end;

procedure TFmMDFeLEnC_0100a.Gerada2Click(Sender: TObject);
begin
  DmMDFe_0000.AbreXML_NoTWebBrowser(QrMDFeLEnCEmpresa.Value, MDFE_EXT_MDFE_XML,
    QrMDFeCabAid.Value, False, WB_XML);
end;

procedure TFmMDFeLEnC_0100a.IncluiMDFeaoloteatual1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMDFeLEnI_0100a, FmMDFeLEnI_0100a, afmoNegarComAviso) then
  begin
    FmMDFeLEnI_0100a.QrMDFeCabA.Close;
    FmMDFeLEnI_0100a.QrMDFeCabA.Params[00].AsInteger := QrMDFeLEncEmpresa.Value;
    FmMDFeLEnI_0100a.QrMDFeCabA.Params[01].AsInteger := Integer(mdfemystatusMDFeAssinada);
    FmMDFeLEnI_0100a.QrMDFeCabA.Open;
    //
    FmMDFeLEnI_0100a.ShowModal;
    FmMDFeLEnI_0100a.Destroy;
    //
    //LocCod(QrMDFeLEnCCodigo.Value, QrMDFeLEnCCodigo.Value);
  end;
end;

procedure TFmMDFeLEnC_0100a.Incluinovolote1Click(Sender: TObject);
var
  Filial: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrMDFeLEnc, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'mdfelenc');
  UMyMod.ObtemValorDoCampoXDeIndex_Int(VAR_LIB_EMPRESA_SEL, 'Filial', 'Codigo',
    DmodG.QrEmpresas, EdEmpresa, CBEmpresa, Filial);
end;

procedure TFmMDFeLEnC_0100a.QrMDFeLEnCBeforeOpen(DataSet: TDataSet);
begin
  QrMDFeLEncCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmMDFeLEnC_0100a.QrMDFeLEnCCalcFields(DataSet: TDataSet);
begin
  QrMDFeLEncDataHora_TXT.Value := dmkPF.FDT_NULO(QrMDFeLEnCdhRecbto.Value, 0);
  case QrMDFeLEnCtpAmb.Value of
    1: QrMDFeLEnCNO_Ambiente.Value := 'Produ��o';
    2: QrMDFeLEnCNO_Ambiente.Value := 'Homologa��o';
    else QrMDFeLEnCNO_Ambiente.Value := '';
  end;
end;

end.

