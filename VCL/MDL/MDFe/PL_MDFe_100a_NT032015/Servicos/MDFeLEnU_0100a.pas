unit MDFeLEnU_0100a;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, DmkDAC_PF, UnDmkEnums;

type
  TFmMDFeLEnU_0100a = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    QrMDFeLEnC: TmySQLQuery;
    QrMDFeLEnCCodigo: TIntegerField;
    QrMDFeLEnCCodUsu: TIntegerField;
    QrMDFeLEnCNome: TWideStringField;
    QrMDFeLEnCEmpresa: TIntegerField;
    QrMDFeLEnCversao: TFloatField;
    QrMDFeLEnCtpAmb: TSmallintField;
    QrMDFeLEnCverAplic: TWideStringField;
    QrMDFeLEnCcStat: TIntegerField;
    QrMDFeLEnCxMotivo: TWideStringField;
    QrMDFeLEnCcUF: TSmallintField;
    QrMDFeLEnCnRec: TWideStringField;
    QrMDFeLEnCdhRecbto: TDateTimeField;
    QrMDFeLEnCtMed: TIntegerField;
    QrMDFeLEnCcMsg: TWideStringField;
    QrMDFeLEnCxMsg: TWideStringField;
    QrMDFeLEnCindSinc: TSmallintField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DsMDFeLEnC: TDataSource;
    DBEdit2: TDBEdit;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    Label2: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    QrMDFeCabA: TmySQLQuery;
    QrMDFeCabAStatus: TIntegerField;
    QrMDFeCabAMotivo: TWideStringField;
    DsMDFeCabA: TDataSource;
    CheckBox7: TCheckBox;
    QrMDFeCabAIDCtrl: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrMDFeLEnCBeforeClose(DataSet: TDataSet);
    procedure QrMDFeLEnCBeforeOpen(DataSet: TDataSet);
    procedure DBEdit3Change(Sender: TObject);
    procedure DBEdit1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure TraduzMensagem(CodErro: Integer);
  public
    { Public declarations }
    FIDCtrl: Integer;
    //
    function EnviarCTe(): Boolean;
    procedure ReabreCTeLEnC(Codigo: Integer);
    procedure ReopenCTeCabA();
    function Interrompe(Passo: Integer): Boolean;
  end;

  var
  FmMDFeLEnU_0100a: TFmMDFeLEnU_0100a;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule,
  ModuleCTe_0000, CTeSteps_0200a, MyDBCheck, (*CTe_Pesq_0000,*) FrtFatCab;

{$R *.DFM}

procedure TFmMDFeLEnU_0100a.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMDFeLEnU_0100a.DBEdit1Change(Sender: TObject);
var
  Cor: TColor;
begin
  if QrMDFeLEnC.State <> dsInactive then
  begin
    case QrMDFeLEnCcStat.Value of
      0..99: Cor := clBlack;
      103: Cor := clGreen;
      104: Cor := clBlue;
      105: Cor := clPurple;
      else Cor := clRed;
    end;
    DBEdit1.Font.Color := Cor;
    DBEdit2.Font.Color := Cor;
  end;
  TraduzMensagem(QrMDFeLEnCcStat.Value);
end;

procedure TFmMDFeLEnU_0100a.DBEdit3Change(Sender: TObject);
var
  Cor: TColor;
begin
  if QrMDFeCabA.State <> dsInactive then
  begin
    case QrMDFeCabAStatus.Value of
      0..99: Cor := clBlack;
      100: Cor := clBlue;
      101..199: Cor := clPurple;
      else Cor := clRed;
    end;
    DBEdit3.Font.Color := Cor;
    DBEdit4.Font.Color := Cor;
  end;
  TraduzMensagem(QrMDFeLEnCcStat.Value);
end;

procedure TFmMDFeLEnU_0100a.TraduzMensagem(CodErro: Integer);
begin
  case CodErro of
    232: Geral.MB_Aviso('Destinat�rio com IE n�o informado!' + slinebreak +
      'A UF iformou que o destinat�rio possui IE ativa na UF!');
  end;
end;

function TFmMDFeLEnU_0100a.EnviarCTe(): Boolean;
var
  CodUsu, Codigo, Empresa, LoteEnv, FatID, FatNum, Status, infProt_cStat,
  indSinc: Integer;
  infProt_xMotivo: String;
  Sincronia: TXXeIndSinc;
begin
 { TODO 0 -cCTe : Envio de CTe unica!}
  Geral.MB_Info('Falta implementar o envio!');
{

  Result := False;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando XML da MDF-e');
  if FmFrtFatCab.QrMDFeCabAIDCtrl.Value > 0 then
  begin
    (* O apenas gera � feito automatico na MDF-e 3.10
    if not FmFrtFatCab.GerarCTe(True, True, True) then
      if Interrompe(1) then Exit;
    *)
  end else
  begin
    if not FmFrtFatCab.GerarCTe(True, False, True) then
      if Interrompe(1) then Exit;
  end;
  CheckBox1.Checked := True;
  FIDCtrl := FmFrtFatCab.QrMDFeCabAIDCtrl.Value;

  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  //

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando Lote de Envio');
  Empresa := FmFrtFatCab.QrFatPedNFsEmpresa.Value;
  CodUsu := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'mdfelenc', 'CodUsu', [], [],
    stIns, 0, siPositivo, nil);
  Codigo := DModG.BuscaProximoCodigoInt('mdfectrl', 'mdfelenc', '', 0, 999999999, '');
  indSinc := FmFrtFatCab.QrFatPedCabindSinc.Value;
  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'mdfelenc', False, [
  'CodUsu', 'Empresa', 'indSinc'], ['Codigo'], [
  CodUsu, Empresa, indSinc], [Codigo], True) then
    if Interrompe(2) then Exit;
  CheckBox2.Checked := True;

  //

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Adicionando MDF-e ao Lote criado');
  LoteEnv := Codigo;
  ReabreCTeLEnC(LoteEnv);
  FatID := FmFrtFatCab.QrFatPedNFsCabA_FatID.Value;
  FatNum := FmFrtFatCab.QrFatPedNFsOriCodi.Value;
  Status  := DmCTe_0000.stepCTeAdedLote();
  infProt_cStat   := 0; // Limpar variaveis (ver historico)
  infProt_xMotivo := '';
  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfecaba', False, [
  'LoteEnv', 'Status', 'infProt_cStat', 'infProt_xMotivo'],
  ['FatID', 'FatNum', 'Empresa'], [
  LoteEnv, Status, infProt_cStat, infProt_xMotivo],
  [FatID, FatNum, Empresa], True) then
    if Interrompe(3) then Exit;
  CheckBox3.Checked := True;

  //
  Sincronia := TXXeIndSinc(indSinc);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviando Lote ao Fisco');
  if DBCheck.CriaFm(TFmCTeSteps_0200a, FmCTeSteps_0200a, afmoNegarComAviso) then
  begin
    FmCTeSteps_0200a.PnLoteEnv.Visible := True;
    FmCTeSteps_0200a.EdVersaoAcao.ValueVariant := DModG.QrParamsEmpCTeVerEnvLot.Value;
    FmCTeSteps_0200a.Show;
    //
    FmCTeSteps_0200a.RGAcao.ItemIndex := Integer(TCTeServicoStep.mdfesrvEnvioLoteCTe);// 1 - Envia lote ao fisco
    FmCTeSteps_0200a.RGIndSinc.ItemIndex := Integer(Sincronia);
    FmCTeSteps_0200a.PreparaEnvioDeLoteCTe(LoteEnv, Empresa, Sincronia);
    // Ver o que fazer!
    FmCTeSteps_0200a.FFormChamou      := 'FmCTeLEnU_0200a';
    //
    FmCTeSteps_0200a.BtOKClick(Self);
    //
    CheckBox4.Checked := True;
    if not (QrMDFeLEnCcStat.Value in ([103, 104, 105])) then
    begin
      FmCTeSteps_0200a.Destroy;
      if Interrompe(4) then Exit;
    end;
    //
    if Sincronia = TXXeIndSinc.nisAssincrono then
    begin
      //
      // Usar Ck???.Checked se der muitos bugs
      while QrMDFeLEnCcStat.Value = 105 do
      begin
        Sleep(5000);
        FmCTeSteps_0200a.BtOKClick(Self);
      end;
      // Esperar Timer1
      while FmCTeSteps_0200a.FSegundos < FmCTeSteps_0200a.FSecWait do
      begin
        FmCTeSteps_0200a.LaWait.Update;
        Application.ProcessMessages;
      end;
    end;
    FmCTeSteps_0200a.Destroy;
  end else if
    Interrompe(5) then Exit;
  //CheckBox4.Checked := True;

    //

  if Sincronia = TXXeIndSinc.nisAssincrono then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Consultando Lote no Fisco');
    if DBCheck.CriaFm(TFmCTeSteps_0200a, FmCTeSteps_0200a, afmoNegarComAviso) then
    begin
      FmCTeSteps_0200a.PnLoteEnv.Visible := True;
      FmCTeSteps_0200a.EdVersaoAcao.ValueVariant := DModG.QrParamsEmpCTeVerConLot.Value;
      FmCTeSteps_0200a.Show;
      //
      FmCTeSteps_0200a.RGAcao.ItemIndex := Integer(TCTeServicoStep.mdfesrvConsultarLoteCteEnviado); // 2 - Verifica lote no fisco
      FmCTeSteps_0200a.PreparaConsultaLote(QrMDFeLEnCCodigo.Value, QrMDFeLEnCEmpresa.Value,
        QrMDFeLEnCnRec.Value);
      FmCTeSteps_0200a.FFormChamou      := 'FmCTeLEnU_200a';
      //
      FmCTeSteps_0200a.BtOKClick(Self);
      //
      FmCTeSteps_0200a.Destroy;
      if QrMDFeLEnCcStat.Value <> 104 then
        if Interrompe(7) then Exit;
    end else if
      Interrompe(6) then Exit;
    CheckBox5.Checked := True;
    //
    FIDCtrl := FmFrtFatCab.QrMDFeCabAIDCtrl.Value;
  end else
  begin
    // Consulta sincrona!
    CheckBox5.Checked := True;
    ReopenCTeCabA();
    FIDCtrl := QrMDFeCabAIDCtrl.Value;
  end;
  //

  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Localizando MDF-e');
  //

  FIDCtrl := FmFrtFatCab.QrMDFeCabAIDCtrl.Value;
  if FIDCtrl <> 0 then
  begin
    if DBCheck.CriaFm(TFmCTe_Pesq_0000, FmCTe_Pesq_0000, afmoNegarComAviso) then
    begin
      //
      FmCTe_Pesq_0000.ReopenCTeCabA(FIDCtrl, True);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo dados da MDF-e');
      if (FmCTe_Pesq_0000.QrMDFeCabA.State <> dsInactive) then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando unicidade da MDF-e');
        if (FmCTe_Pesq_0000.QrMDFeCabA.RecordCount = 1) then
        begin
          if (FmCTe_Pesq_0000.QrMDFeCabAStatus.Value = 100) then
          //if FmCTe_Pesq_0000.BtImprime.Enabled then
          begin
            //FmCTe_Pesq_0000.DefineFrx(FmCTe_Pesq_0000.frxA4A_002, ficMostra);
            FmCTe_Pesq_0000.DefineQual_frxCTe(ficMostra);
            CheckBox6.Checked := FmCTe_Pesq_0000.FDAMDFEImpresso;
            //
            FmCTe_Pesq_0000.BtEnviaClick(Self);
            //
            FmCTe_Pesq_0000.Destroy;
            CheckBox7.Checked := FmCTe_Pesq_0000.FMailEnviado;
            //
            MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
          end else begin
            FmCTe_Pesq_0000.Destroy;
            if Interrompe(12) then Exit;
          end;
        end else
        begin
          FmCTe_Pesq_0000.Destroy;
          if Interrompe(11) then Exit;
        end;
      end else
      begin
        FmCTe_Pesq_0000.Destroy;
        if Interrompe(10) then Exit;
      end;
    end else if
      Interrompe(9) then Exit;
    CheckBox5.Checked := True;
    //
    Result := True;
    //
  end else
    if Interrompe(8) then Exit;
}
end;


procedure TFmMDFeLEnU_0100a.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmMDFeLEnU_0100a.FormCreate(Sender: TObject);
begin
  FIDCtrl := 0;
end;

procedure TFmMDFeLEnU_0100a.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmMDFeLEnU_0100a.Interrompe(Passo: Integer): Boolean;
var
  Aviso: String;
begin
  Result := True;
  Aviso := 'ABORTADO NO PASSO ' + FormatFloat('0', Passo) +
    ' POR ERRO EM: ' + LaAviso1.Caption;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Aviso);
  GBRodaPe.Visible := True;
  ReopenCTeCabA();
end;

procedure TFmMDFeLEnU_0100a.QrMDFeLEnCBeforeClose(DataSet: TDataSet);
begin
  QrMDFeCabA.Close;
end;

procedure TFmMDFeLEnU_0100a.QrMDFeLEnCBeforeOpen(DataSet: TDataSet);
begin
  ReopenCTeCabA();
end;

procedure TFmMDFeLEnU_0100a.ReopenCTeCabA();
begin
  if FIDCtrl <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeCabA, Dmod.MyDB, [
    'SELECT Status, IDCtrl, ',
    'IF(Status=infProt_cStat, infProt_xMotivo,  ',
    'IF(Status=infCanc_cStat, infCanc_xMotivo, "")) Motivo  ',
    'FROM mdfecaba  ',
    'WHERE IDCtrl=' + FormatFloat('0', FIDCtrl),
    '']);
  end;
end;

procedure TFmMDFeLEnU_0100a.ReabreCTeLEnC(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeLEnC, Dmod.MyDB, [
  'SELECT *  ',
  'FROM mdfelenc ',
  'WHERE Codigo=' + FormatFloat('0', Codigo),
  '']);
end;

end.
