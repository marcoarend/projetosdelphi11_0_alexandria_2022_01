unit MDFeLEnI_0100a;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, mySQLDbTables, Grids, DBGrids,
  dmkImage, UnDmkEnums;

type
  TFmMDFeLEnI_0100a = class(TForm)
    Panel1: TPanel;
    QrMDFeCabA: TmySQLQuery;
    QrMDFeCabAFatID: TIntegerField;
    QrMDFeCabAFatNum: TIntegerField;
    QrMDFeCabAide_nCT: TIntegerField;
    QrMDFeCabAide_dEmi: TDateField;
    QrMDFeCabAid: TWideStringField;
    QrMDFeCabAide_cCT: TIntegerField;
    QrMDFeCabAide_serie: TIntegerField;
    QrMDFeCabAStatus: TSmallintField;
    DBGrid1: TDBGrid;
    DsMDFeCabA: TDataSource;
    QrMDFeCabAEmpresa: TIntegerField;
    QrMDFeCabAinfProt_cStat: TIntegerField;
    QrMDFeCabAinfProt_xMotivo: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmMDFeLEnI_0100a: TFmMDFeLEnI_0100a;

implementation

uses UnMyObjects, MDFeLEnC_0100a, Module, UMySQLModule, dmkGeral, ModuleMDFe_0000;

{$R *.DFM}

procedure TFmMDFeLEnI_0100a.BtConfirmaClick(Sender: TObject);
  procedure AdicionaMDFeAoLote();
  var
    LoteEnv, FatID, FatNum, Empresa, Status: Integer;
    infProt_cStat, infProt_xMotivo: String;
  begin
    LoteEnv := FmMDFeLEnC_0100a.QrMDFeLEnCCodigo.Value;
    FatID   := QrMDFeCabAFatID.Value;
    FatNum  := QrMDFeCabAFatNum.Value;
    Empresa := QrMDFeCabAEmpresa.Value;
    Status  := Integer(mdfemystatusMDFeAddedLote);
    infProt_cStat   := '0'; // Limpar variaveis (ver historico)
    infProt_xMotivo := '';
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfecaba', False, [
    'LoteEnv', 'Status', 'infProt_cStat', 'infProt_xMotivo'], ['FatID', 'FatNum', 'Empresa'], [
    LoteEnv, Status, infProt_cStat, infProt_xMotivo], [FatID, FatNum, Empresa], True);
  end;
var
  I, N: Integer;
begin
  N := DBGrid1.SelectedRows.Count;
  if N + FmMDFeLEnC_0100a.QrMDFeCabA.RecordCount > 1 then
  begin
    Geral.MensagemBox('Quantidade extrapola o m�ximo de 1 MDF-e permitida por lote!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if N > 1 then
  begin
    if Application.MessageBox(PChar('Confirma a adi��o das ' + IntToStr(N) +
    ' MDF-e selecionadas?'), 'Pergunta', MB_ICONQUESTION+MB_YESNOCANCEL) =
    ID_YES then
    begin
      with DBGrid1.DataSource.DataSet do
      for I := 0 to N - 1 do
      begin
        GotoBookmark(pointer(DBGrid1.SelectedRows.Items[I]));
        AdicionaMDFeAoLote();
      end;
    end;
  end else AdicionaMDFeAoLote();
  FmMDFeLEnC_0100a.ReopenMDFeLEnI();
  Close;
end;

procedure TFmMDFeLEnI_0100a.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMDFeLEnI_0100a.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMDFeLEnI_0100a.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FmMDFeLEnC_0100a.LocCod(FmMDFeLEnC_0100a.QrMDFeLEnCCodigo.Value, FmMDFeLEnC_0100a.QrMDFeLEnCCodigo.Value);
end;

procedure TFmMDFeLEnI_0100a.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmMDFeLEnI_0100a.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
