object FmMDFeLEnC_0100a: TFmMDFeLEnC_0100a
  Left = 368
  Top = 194
  Caption = 'MDF-LOTES-001 :: Lotes de MDF-e'
  ClientHeight = 617
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 521
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 92
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label8: TLabel
        Left = 68
        Top = 4
        Width = 57
        Height = 13
        Caption = 'C'#243'digo: [F4]'
      end
      object Label9: TLabel
        Left = 152
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label4: TLabel
        Left = 8
        Top = 48
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 152
        Top = 20
        Width = 633
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 68
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 64
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 65
        Top = 64
        Width = 720
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 4
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 457
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 860
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
        end
      end
    end
    object RGIndSinc: TdmkRadioGroup
      Left = 8
      Top = 94
      Width = 204
      Height = 39
      Caption = ' Envio da MDF-e ao Fisco:'
      Columns = 2
      Enabled = False
      ItemIndex = 0
      Items.Strings = (
        'Ass'#237'ncrono'
        'S'#237'ncrono')
      TabOrder = 2
      QryCampo = 'IndSinc'
      UpdCampo = 'IndSinc'
      UpdType = utYes
      OldValor = 0
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 521
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 192
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 793
        Height = 192
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 4
          Width = 14
          Height = 13
          Caption = 'ID:'
        end
        object Label2: TLabel
          Left = 152
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label3: TLabel
          Left = 68
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdNome
        end
        object Label5: TLabel
          Left = 8
          Top = 44
          Width = 20
          Height = 13
          Caption = 'Filial'
          FocusControl = DBEdit2
        end
        object DBEdNome: TDBEdit
          Left = 68
          Top = 20
          Width = 80
          Height = 21
          DataField = 'CodUsu'
          DataSource = DsMDFeLEnC
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 8
          Top = 60
          Width = 56
          Height = 21
          DataField = 'Filial'
          DataSource = DsMDFeLEnC
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 68
          Top = 60
          Width = 509
          Height = 21
          DataField = 'NO_Empresa'
          DataSource = DsMDFeLEnC
          TabOrder = 2
        end
        object GroupBox1: TGroupBox
          Left = 8
          Top = 84
          Width = 493
          Height = 101
          Caption = ' Resposta do envio do XML: '
          TabOrder = 3
          object Label11: TLabel
            Left = 8
            Top = 20
            Width = 85
            Height = 13
            Caption = 'Vers'#227'o do leiaute:'
            FocusControl = DBEdit6
          end
          object Label12: TLabel
            Left = 108
            Top = 20
            Width = 85
            Height = 13
            Caption = 'Tipo de ambiente:'
            FocusControl = DBEdit7
          end
          object Label13: TLabel
            Left = 276
            Top = 20
            Width = 171
            Height = 13
            Caption = 'Vers'#227'o do aplic. que recebeu o lote:'
            FocusControl = DBEdit8
          end
          object Label14: TLabel
            Left = 8
            Top = 60
            Width = 48
            Height = 13
            Caption = 'Resposta:'
            FocusControl = DBEdit9
          end
          object Label15: TLabel
            Left = 456
            Top = 20
            Width = 17
            Height = 13
            Caption = 'UF:'
            FocusControl = DBEdit12
          end
          object DBEdit6: TDBEdit
            Left = 8
            Top = 36
            Width = 97
            Height = 21
            DataField = 'versao'
            DataSource = DsMDFeLEnC
            TabOrder = 0
          end
          object DBEdit7: TDBEdit
            Left = 108
            Top = 36
            Width = 21
            Height = 21
            DataField = 'tpAmb'
            DataSource = DsMDFeLEnC
            TabOrder = 1
          end
          object DBEdit8: TDBEdit
            Left = 276
            Top = 36
            Width = 177
            Height = 21
            DataField = 'verAplic'
            DataSource = DsMDFeLEnC
            TabOrder = 2
          end
          object DBEdit9: TDBEdit
            Left = 8
            Top = 76
            Width = 33
            Height = 21
            DataField = 'cStat'
            DataSource = DsMDFeLEnC
            TabOrder = 3
          end
          object DBEdit10: TDBEdit
            Left = 44
            Top = 76
            Width = 441
            Height = 21
            DataField = 'xMotivo'
            DataSource = DsMDFeLEnC
            TabOrder = 4
          end
          object DBEdit11: TDBEdit
            Left = 128
            Top = 36
            Width = 145
            Height = 21
            DataField = 'NO_Ambiente'
            DataSource = DsMDFeLEnC
            TabOrder = 5
          end
          object DBEdit12: TDBEdit
            Left = 456
            Top = 36
            Width = 29
            Height = 21
            DataField = 'cUF'
            DataSource = DsMDFeLEnC
            TabOrder = 6
          end
        end
        object GroupBox2: TGroupBox
          Left = 504
          Top = 85
          Width = 281
          Height = 101
          Caption = ' Recibo de aceite do lote: '
          TabOrder = 4
          object Label16: TLabel
            Left = 8
            Top = 16
            Width = 87
            Height = 13
            Caption = 'N'#250'mero do recibo:'
            FocusControl = DBEdit13
          end
          object Label10: TLabel
            Left = 8
            Top = 56
            Width = 58
            Height = 13
            Caption = 'Data / hora:'
            FocusControl = DBEdit5
          end
          object Label17: TLabel
            Left = 184
            Top = 56
            Width = 84
            Height = 13
            Caption = 't m'#233'd. resp. (seg):'
            FocusControl = DBEdit14
          end
          object DBEdit13: TDBEdit
            Left = 8
            Top = 32
            Width = 265
            Height = 21
            DataField = 'nRec'
            DataSource = DsMDFeLEnC
            TabOrder = 0
          end
          object DBEdit5: TDBEdit
            Left = 8
            Top = 72
            Width = 173
            Height = 21
            DataField = 'DataHora_TXT'
            DataSource = DsMDFeLEnC
            TabOrder = 1
          end
          object DBEdit14: TDBEdit
            Left = 184
            Top = 72
            Width = 89
            Height = 21
            DataField = 'tMed'
            DataSource = DsMDFeLEnC
            TabOrder = 2
          end
        end
        object DBEdit4: TDBEdit
          Left = 152
          Top = 20
          Width = 633
          Height = 21
          DataField = 'Nome'
          DataSource = DsMDFeLEnC
          TabOrder = 5
        end
        object DBEdCodigo: TDBEdit
          Left = 8
          Top = 20
          Width = 57
          Height = 21
          DataField = 'Codigo'
          DataSource = DsMDFeLEnC
          TabOrder = 6
        end
        object DBRadioGroup1: TDBRadioGroup
          Left = 581
          Top = 42
          Width = 204
          Height = 39
          Caption = ' Envio da MDF-e ao Fisco:'
          Columns = 2
          DataField = 'indSinc'
          DataSource = DsMDFeLEnC
          Items.Strings = (
            'Ass'#237'ncrono'
            'S'#237'ncrono')
          ParentBackground = True
          TabOrder = 7
          Values.Strings = (
            '0'
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9')
        end
      end
      object DBGrid2: TDBGrid
        Left = 793
        Top = 0
        Width = 215
        Height = 192
        Align = alClient
        DataSource = DsMDFeLEnM
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 192
      Width = 1008
      Height = 193
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' Informa'#231#245'es das notas do lote '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 165
          Align = alClient
          DataSource = DsMDFeCabA
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'ide_serie'
              Title.Caption = 'S'#233'rie'
              Width = 33
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ide_nMDF'
              Title.Caption = 'Manifesto'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'id'
              Title.Caption = 'Chave de acesso da MDF-e'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Status'
              Title.Caption = 'Status MDF-e'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'infProt_cStat'
              Title.Caption = 'Status Prot.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'infProt_xMotivo'
              Title.Caption = 'Motivo Status Protocolo MDF-e'
              Width = 660
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' XML do arquivo carregado '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object WB_XML: TWebBrowser
          Left = 0
          Top = 0
          Width = 1000
          Height = 165
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 998
          ControlData = {
            4C0000005A6700000E1100000000000000000000000000000000000000000000
            000000004C000000000000000000000001000000E0D057007335CF11AE690800
            2B2E126208000000000000004C0000000114020000000000C000000000000046
            8000000000000000000000000000000000000000000000000000000000000000
            00000000000000000100000000000000000000000000000000000000}
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 457
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 432
        Top = 15
        Width = 574
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 441
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtLote: TBitBtn
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Lote'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtLoteClick
        end
        object BtMDFe: TBitBtn
          Left = 126
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&MDF-e'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtMDFeClick
        end
        object BtXML: TBitBtn
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&XML'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtXMLClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 195
        Height = 32
        Caption = 'Lotes de MDF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 195
        Height = 32
        Caption = 'Lotes de MDF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 195
        Height = 32
        Caption = 'Lotes de MDF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsMDFeLEnC: TDataSource
    DataSet = QrMDFeLEnC
    Left = 40
    Top = 65524
  end
  object QrMDFeLEnC: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrMDFeLEnCBeforeOpen
    AfterOpen = QrMDFeLEnCAfterOpen
    AfterScroll = QrMDFeLEnCAfterScroll
    OnCalcFields = QrMDFeLEnCCalcFields
    SQL.Strings = (
      
        'SELECT lot.*, ent.Filial, IF(ent.Tipo=0, ent.RazaoSocial, ent.No' +
        'me) NO_Empresa'
      'FROM mdfelenc lot'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Empresa')
    Left = 12
    Top = 65524
    object QrMDFeLEnCDataHora_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DataHora_TXT'
      Calculated = True
    end
    object QrMDFeLEnCCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'mdfelenc.Codigo'
    end
    object QrMDFeLEnCCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'mdfelenc.CodUsu'
    end
    object QrMDFeLEnCNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'mdfelenc.Nome'
      Size = 30
    end
    object QrMDFeLEnCEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'mdfelenc.Empresa'
    end
    object QrMDFeLEnCversao: TFloatField
      FieldName = 'versao'
      Origin = 'mdfelenc.versao'
      DisplayFormat = '0.00'
    end
    object QrMDFeLEnCtpAmb: TSmallintField
      FieldName = 'tpAmb'
      Origin = 'mdfelenc.tpAmb'
    end
    object QrMDFeLEnCverAplic: TWideStringField
      FieldName = 'verAplic'
      Origin = 'mdfelenc.verAplic'
    end
    object QrMDFeLEnCcStat: TIntegerField
      FieldName = 'cStat'
      Origin = 'mdfelenc.cStat'
    end
    object QrMDFeLEnCxMotivo: TWideStringField
      FieldName = 'xMotivo'
      Origin = 'mdfelenc.xMotivo'
      Size = 255
    end
    object QrMDFeLEnCcUF: TSmallintField
      FieldName = 'cUF'
      Origin = 'mdfelenc.cUF'
    end
    object QrMDFeLEnCnRec: TWideStringField
      FieldName = 'nRec'
      Origin = 'mdfelenc.nRec'
      Size = 15
    end
    object QrMDFeLEnCdhRecbto: TDateTimeField
      FieldName = 'dhRecbto'
      Origin = 'mdfelenc.dhRecbto'
    end
    object QrMDFeLEnCtMed: TIntegerField
      FieldName = 'tMed'
      Origin = 'mdfelenc.tMed'
    end
    object QrMDFeLEnCFilial: TIntegerField
      FieldName = 'Filial'
      Origin = 'entidades.Filial'
    end
    object QrMDFeLEnCNO_Empresa: TWideStringField
      FieldName = 'NO_Empresa'
      Size = 100
    end
    object QrMDFeLEnCNO_Ambiente: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_Ambiente'
      Calculated = True
    end
    object QrMDFeLEnCIndSinc: TSmallintField
      FieldName = 'IndSinc'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanUpd01 = BtXML
    Left = 68
    Top = 65524
  end
  object VuEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    Panel = PnEdita
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 100
    Top = 65524
  end
  object PMLote: TPopupMenu
    OnPopup = PMLotePopup
    Left = 568
    Top = 476
    object Incluinovolote1: TMenuItem
      Caption = '&Inclui novo lote'
      OnClick = Incluinovolote1Click
    end
    object Alteraloteatual1: TMenuItem
      Caption = '&Altera lote atual'
      OnClick = Alteraloteatual1Click
    end
    object Excluiloteatual1: TMenuItem
      Caption = '&Exclui lote atual'
      Enabled = False
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Envialoteaofisco1: TMenuItem
      Caption = '&Envia lote ao fisco'
      Enabled = False
      OnClick = Envialoteaofisco1Click
    end
    object Verificalotenofisco1: TMenuItem
      Caption = 'Verifica lote no fisco'
      Enabled = False
      OnClick = Verificalotenofisco1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Lerarquivo1: TMenuItem
      Caption = '&Ler arquivo'
      object Enviodelote1: TMenuItem
        Caption = '&Processamento de lote (s'#237'ncrono ou ass'#237'ncrono)'
        OnClick = Enviodelote1Click
      end
      object Consultadelote1: TMenuItem
        Caption = '&Consulta de lote'
        OnClick = Consultadelote1Click
      end
    end
  end
  object PMMDFe: TPopupMenu
    OnPopup = PMMDFePopup
    Left = 664
    Top = 476
    object IncluiMDFeaoloteatual1: TMenuItem
      Caption = '&Inclui MDFe ao lote atual'
      OnClick = IncluiMDFeaoloteatual1Click
    end
    object RemoveMDFesselecionadas1: TMenuItem
      Caption = '&Remove MDF-e(s) selecionada(s)'
      OnClick = RemoveMDFesselecionadas1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
  end
  object DsMDFeLEnI: TDataSource
    DataSet = QrMDFeLEnI
    Left = 484
    Top = 56
  end
  object QrMDFeLEnI: TmySQLQuery
    Database = Dmod.MyDB
    Left = 484
    Top = 8
  end
  object QrMDFeCabA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatID, FatNum, Empresa, ide_nMDF, ide_dEmi,'
      'id, ide_cMDF, ide_serie, Status, '
      'infProt_cStat, infProt_xMotivo'
      'FROM mdfecaba'
      'WHERE LoteEnv=:P0'
      '')
    Left = 428
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMDFeCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeCabAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeCabAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeCabAide_nMDF: TIntegerField
      FieldName = 'ide_nMDF'
      DisplayFormat = '000000000'
    end
    object QrMDFeCabAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrMDFeCabAid: TWideStringField
      FieldName = 'id'
      Size = 44
    end
    object QrMDFeCabAide_cMDF: TIntegerField
      FieldName = 'ide_cMDF'
    end
    object QrMDFeCabAide_serie: TIntegerField
      FieldName = 'ide_serie'
      DisplayFormat = '000'
    end
    object QrMDFeCabAStatus: TSmallintField
      FieldName = 'Status'
      DisplayFormat = '000'
    end
    object QrMDFeCabAinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrMDFeCabAinfProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
  end
  object DsMDFeCabA: TDataSource
    DataSet = QrMDFeCabA
    Left = 428
    Top = 56
  end
  object QrMDFeLEnM: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mdfelenm'
      'WHERE Codigo=:P0'
      'ORDER BY Controle DESC'
      '')
    Left = 544
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMDFeLEnMcStat: TIntegerField
      FieldName = 'cStat'
    end
    object QrMDFeLEnMxMotivo: TWideStringField
      FieldName = 'xMotivo'
      Size = 255
    end
    object QrMDFeLEnMnRec: TWideStringField
      FieldName = 'nRec'
      Size = 15
    end
    object QrMDFeLEnMdhRecbto: TDateTimeField
      FieldName = 'dhRecbto'
    end
    object QrMDFeLEnMControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMDFeLEnMversao: TFloatField
      FieldName = 'versao'
    end
    object QrMDFeLEnMtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrMDFeLEnMverAplic: TWideStringField
      FieldName = 'verAplic'
    end
    object QrMDFeLEnMcUF: TSmallintField
      FieldName = 'cUF'
    end
    object QrMDFeLEnMtMed: TIntegerField
      FieldName = 'tMed'
    end
  end
  object DsMDFeLEnM: TDataSource
    DataSet = QrMDFeLEnM
    Left = 544
    Top = 56
  end
  object PMXML: TPopupMenu
    OnPopup = PMXMLPopup
    Left = 744
    Top = 472
    object AbrirXMLnonavegadordestajanela1: TMenuItem
      Caption = 'Abrir XML no navegador desta janela'
      object Gerada2: TMenuItem
        Caption = '&Gerado'
        OnClick = Gerada2Click
      end
      object Assinada1: TMenuItem
        Caption = '&Assinado'
        OnClick = Assinada1Click
      end
      object Lote1: TMenuItem
        Caption = '&Lote de envio'
        OnClick = Lote1Click
      end
      object RetornodoLotedeenvio1: TMenuItem
        Caption = '&Retorno do Lote de envio'
        OnClick = RetornodoLotedeenvio1Click
      end
      object SelecionaroXML1: TMenuItem
        Caption = '&Selecionar o XML'
        OnClick = SelecionaroXML1Click
      end
    end
    object AbrirXMLdaNMDFenonavegadorpadro1: TMenuItem
      Caption = 'Abrir XML do MDFe no navegador padr'#227'o'
      object Gerada1: TMenuItem
        Caption = '&Gerado'
        OnClick = Gerada1Click
      end
      object Assina1: TMenuItem
        Caption = '&Assinado'
        OnClick = Assina1Click
      end
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object ValidarXMLdaMDFe1: TMenuItem
      Caption = 'Validar XML da MDFe'
      object Assinada2: TMenuItem
        Caption = '&Assinado'
        OnClick = Assinada2Click
      end
      object Lotedeenvio1: TMenuItem
        Caption = '&Lote de envio'
        OnClick = Lotedeenvio1Click
      end
    end
  end
end
