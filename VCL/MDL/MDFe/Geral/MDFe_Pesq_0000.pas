unit MDFe_Pesq_0000;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkLabel, dmkEdit, dmkEditCB,
  DBCtrls, dmkDBLookupComboBox, DB, mySQLDbTables, ComCtrls,
  dmkEditDateTimePicker, Grids, DBGrids, dmkDBGrid, Variants, Menus, dmkMemo,
  frxClass, frxDBSet, dmkGeral, frxBarcode, UnDmkProcFunc, dmkImage,
  dmkCheckGroup, frxExportPDF, UnDmkEnums, dmkCompoStore, UnMDFeListas;

type
  THackDBGrid = class(TdmkDBGrid);
  TFmMDFe_Pesq_0000 = class(TForm)
    PnDados: TPanel;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNO_Enti: TWideStringField;
    DsClientes: TDataSource;
    dmkDBGrid1: TdmkDBGrid;
    QrClientesTipo: TSmallintField;
    QrClientesCNPJ: TWideStringField;
    QrClientesCPF: TWideStringField;
    PMLeArq: TPopupMenu;
    Cancelamento1: TMenuItem;
    PMImprime: TPopupMenu;
    CampospreenchidosnoXMLdaMDFe1: TMenuItem;
    QrMDFeCabA: TmySQLQuery;
    DsMDFeCabA: TDataSource;
    frxDsMDFeCabA: TfrxDBDataset;
    QrMDFeCabAMsg: TmySQLQuery;
    QrMDFeCabAMsgFatID: TIntegerField;
    QrMDFeCabAMsgFatNum: TIntegerField;
    QrMDFeCabAMsgEmpresa: TIntegerField;
    QrMDFeCabAMsgControle: TIntegerField;
    QrMDFeCabAMsgSolicit: TIntegerField;
    QrMDFeCabAMsgId: TWideStringField;
    QrMDFeCabAMsgtpAmb: TSmallintField;
    QrMDFeCabAMsgverAplic: TWideStringField;
    QrMDFeCabAMsgdhRecbto: TDateTimeField;
    QrMDFeCabAMsgnProt: TWideStringField;
    QrMDFeCabAMsgdigVal: TWideStringField;
    QrMDFeCabAMsgcStat: TIntegerField;
    QrMDFeCabAMsgxMotivo: TWideStringField;
    DsMDFeCabAMsg: TDataSource;
    frxDsA: TfrxDBDataset;
    QrA: TmySQLQuery;
    QrMDFeXMLI: TmySQLQuery;
    QrMDFeXMLICodigo: TWideStringField;
    QrMDFeXMLIID: TWideStringField;
    QrMDFeXMLIValor: TWideStringField;
    QrMDFeXMLIPai: TWideStringField;
    QrMDFeXMLIDescricao: TWideStringField;
    QrMDFeXMLICampo: TWideStringField;
    frxDsMDFeXMLI: TfrxDBDataset;
    frxCampos: TfrxReport;
    QrArq: TmySQLQuery;
    QrArqFatID: TIntegerField;
    QrArqFatNum: TIntegerField;
    QrArqEmpresa: TIntegerField;
    QrArqIDCtrl: TIntegerField;
    QrArqXML_MDFe: TWideMemoField;
    QrArqXML_Aut: TWideMemoField;
    QrArqXML_Can: TWideMemoField;
    QrArqLk: TIntegerField;
    QrArqDataCad: TDateField;
    QrArqDataAlt: TDateField;
    QrArqUserCad: TIntegerField;
    QrArqUserAlt: TIntegerField;
    QrArqAlterWeb: TSmallintField;
    QrArqAtivo: TSmallintField;
    PMArq: TPopupMenu;
    MDFe1: TMenuItem;
    Autorizao1: TMenuItem;
    Cancelamento2: TMenuItem;
    Panel3: TPanel;
    N1: TMenuItem;
    DiretriodoarquivoXML1: TMenuItem;
    frxListaMFFes: TfrxReport;
    N2: TMenuItem;
    Listadasnotaspesquisadas1: TMenuItem;
    QrMDFeCabANOME_tpEmis: TWideStringField;
    QrMDFe_100: TmySQLQuery;
    frxDsMDFe_100: TfrxDBDataset;
    QrMDFe_XXX: TmySQLQuery;
    frxDsMDFe_XXX: TfrxDBDataset;
    QrMDFe_XXXOrdem: TIntegerField;
    QrMDFe_XXXcStat: TIntegerField;
    QrMDFe_XXXide_nNF: TIntegerField;
    QrMDFe_XXXide_serie: TIntegerField;
    QrMDFe_XXXide_AAMM_AA: TWideStringField;
    QrMDFe_XXXide_AAMM_MM: TWideStringField;
    QrMDFe_XXXide_dEmi: TDateField;
    QrMDFe_XXXNOME_tpEmis: TWideStringField;
    QrMDFe_XXXNOME_tpNF: TWideStringField;
    QrMDFe_XXXStatus: TIntegerField;
    QrMDFe_XXXMotivo: TWideStringField;
    QrMDFe_100ide_nNF: TIntegerField;
    QrMDFe_100ide_serie: TIntegerField;
    QrMDFe_100ide_dEmi: TDateField;
    QrMDFe_100ICMSTot_vProd: TFloatField;
    QrMDFe_100ICMSTot_vST: TFloatField;
    QrMDFe_100ICMSTot_vFrete: TFloatField;
    QrMDFe_100ICMSTot_vSeg: TFloatField;
    QrMDFe_100ICMSTot_vIPI: TFloatField;
    QrMDFe_100ICMSTot_vOutro: TFloatField;
    QrMDFe_100ICMSTot_vDesc: TFloatField;
    QrMDFe_100ICMSTot_vNF: TFloatField;
    QrMDFe_100ICMSTot_vBC: TFloatField;
    QrMDFe_100ICMSTot_vICMS: TFloatField;
    QrMDFe_100NOME_tpEmis: TWideStringField;
    QrMDFe_100NOME_tpNF: TWideStringField;
    QrMDFe_100Ordem: TIntegerField;
    QrMDFe_XXXNOME_ORDEM: TWideStringField;
    QrMDFe_101: TmySQLQuery;
    frxDsMDFe_101: TfrxDBDataset;
    QrMDFe_101Ordem: TIntegerField;
    QrMDFe_101ide_nNF: TIntegerField;
    QrMDFe_101ide_serie: TIntegerField;
    QrMDFe_101ide_AAMM_AA: TWideStringField;
    QrMDFe_101ide_AAMM_MM: TWideStringField;
    QrMDFe_101ide_dEmi: TDateField;
    QrMDFe_101NOME_tpEmis: TWideStringField;
    QrMDFe_101NOME_tpNF: TWideStringField;
    QrMDFe_101infCanc_dhRecbto: TDateTimeField;
    QrMDFe_101infCanc_nProt: TWideStringField;
    QrMDFe_101Motivo: TWideStringField;
    QrMDFe_101Id: TWideStringField;
    QrMDFe_101Id_TXT: TWideStringField;
    PreviewdaMDFe1: TMenuItem;
    QrMDFe_100ICMSTot_vPIS: TFloatField;
    QrMDFe_100ICMSTot_vCOFINS: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    BtImprime: TBitBtn;
    BtEnvia: TBitBtn;
    BtEvento: TBitBtn;
    BtInutiliza: TBitBtn;
    BtLeArq: TBitBtn;
    BtArq: TBitBtn;
    BtConsulta: TBitBtn;
    BtExclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    frxA4A_002: TfrxReport;
    Splitter2: TSplitter;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGEventos: TDBGrid;
    TabSheet2: TTabSheet;
    DBGMensagens: TDBGrid;
    QrMDFeEveRRet: TmySQLQuery;
    DsMDFeEveRRet: TDataSource;
    QrMDFeEveRRetControle: TIntegerField;
    QrMDFeEveRRetSubCtrl: TIntegerField;
    QrMDFeEveRRetret_versao: TFloatField;
    QrMDFeEveRRetret_Id: TWideStringField;
    QrMDFeEveRRetret_tpAmb: TSmallintField;
    QrMDFeEveRRetret_verAplic: TWideStringField;
    QrMDFeEveRRetret_cOrgao: TSmallintField;
    QrMDFeEveRRetret_cStat: TIntegerField;
    QrMDFeEveRRetret_xMotivo: TWideStringField;
    QrMDFeEveRRetret_chMDFe: TWideStringField;
    QrMDFeEveRRetret_tpEvento: TIntegerField;
    QrMDFeEveRRetret_xEvento: TWideStringField;
    QrMDFeEveRRetret_nSeqEvento: TIntegerField;
    QrMDFeEveRRetret_CNPJDest: TWideStringField;
    QrMDFeEveRRetret_CPFDest: TWideStringField;
    QrMDFeEveRRetret_emailDest: TWideStringField;
    QrMDFeEveRRetret_dhRegEvento: TDateTimeField;
    QrMDFeEveRRetret_TZD_UTC: TFloatField;
    QrMDFeEveRRetret_nProt: TWideStringField;
    QrMDFeEveRRetNO_EVENTO: TWideStringField;
    Panel5: TPanel;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    BtReabre: TBitBtn;
    Label1: TLabel;
    TPDataI: TdmkEditDateTimePicker;
    Label2: TLabel;
    TPDataF: TdmkEditDateTimePicker;
    Ck100e101: TCheckBox;
    Panel6: TPanel;
    dmkLabel1: TdmkLabel;
    EdFilial: TdmkEditCB;
    CBFilial: TdmkDBLookupComboBox;
    Label14: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    RGAmbiente: TRadioGroup;
    RGQuemEmit: TRadioGroup;
    CGcSitEnc: TdmkCheckGroup;
    Panel7: TPanel;
    frxA4A_003: TfrxReport;
    frxPDFExport: TfrxPDFExport;
    CSTabSheetChamou: TdmkCompoStore;
    N3: TMenuItem;
    ImprimeSomenteadmin1: TMenuItem;
    frxListaMDFesB: TfrxReport;
    QrMDFe_100ide_NatOP: TWideStringField;
    QrMDFe_100qVol: TFloatField;
    QrMDFe_100esp: TWideStringField;
    QrMDFe_100marca: TWideStringField;
    QrMDFe_100nVol: TWideStringField;
    QrMDFe_100PesoL: TFloatField;
    QrMDFe_100PesoB: TFloatField;
    N4: TMenuItem;
    este1: TMenuItem;
    porNatOp1: TMenuItem;
    porCFOP1: TMenuItem;
    PB1: TProgressBar;
    N5: TMenuItem;
    ExportaporNatOptotaisMDFe1: TMenuItem;
    ExportaporCFOPtotaiseitensMDFe1: TMenuItem;
    frxA4A_001Rodo: TfrxReport;
    QrMDFeCabANO_Emi: TWideStringField;
    QrMDFeCabAFatID: TIntegerField;
    QrMDFeCabAFatNum: TIntegerField;
    QrMDFeCabAEmpresa: TIntegerField;
    QrMDFeCabALoteEnv: TIntegerField;
    QrMDFeCabAId: TWideStringField;
    QrMDFeCabAide_serie: TIntegerField;
    QrMDFeCabAide_nMDF: TIntegerField;
    QrMDFeCabAide_dEmi: TDateField;
    QrMDFeCabAcStat: TFloatField;
    QrMDFeCabAnProt: TWideStringField;
    QrMDFeCabAxMotivo: TWideStringField;
    QrMDFeCabAdhRecbto: TWideStringField;
    QrMDFeCabAIDCtrl: TIntegerField;
    QrMDFeCabAversao: TFloatField;
    QrMDFeCabAide_tpEmis: TSmallintField;
    QrMDFeCabAinfCanc_xJust: TWideStringField;
    QrMDFeCabAStatus: TIntegerField;
    QrMDFeCabAcSitMDFe: TSmallintField;
    QrMDFeCabAcSitEnc: TSmallintField;
    QrMDFeCabANO_cSitEnc: TWideStringField;
    QrMDFeCabAinfCanc_dhRecbto: TDateTimeField;
    QrMDFeCabAinfCanc_nProt: TWideStringField;
    QrAFatID: TIntegerField;
    QrAFatNum: TIntegerField;
    QrAEmpresa: TIntegerField;
    QrAIDCtrl: TIntegerField;
    QrALoteEnv: TIntegerField;
    QrAversao: TFloatField;
    QrAId: TWideStringField;
    QrAide_cUF: TSmallintField;
    QrAide_tpAmb: TSmallintField;
    QrAide_tpEmit: TSmallintField;
    QrAide_mod: TSmallintField;
    QrAide_serie: TIntegerField;
    QrAide_nMDF: TIntegerField;
    QrAide_cMDF: TIntegerField;
    QrAide_cDV: TSmallintField;
    QrAide_Modal: TSmallintField;
    QrAide_dEmi: TDateField;
    QrAide_hEmi: TTimeField;
    QrAide_dhEmiTZD: TFloatField;
    QrAide_tpEmis: TSmallintField;
    QrAide_procEmi: TSmallintField;
    QrAide_verProc: TWideStringField;
    QrAide_UFIni: TWideStringField;
    QrAide_UFFim: TWideStringField;
    QrAemit_CNPJ: TWideStringField;
    QrAemit_IE: TWideStringField;
    QrAemit_xNome: TWideStringField;
    QrAemit_xFant: TWideStringField;
    QrAemit_xLgr: TWideStringField;
    QrAemit_nro: TWideStringField;
    QrAemit_xCpl: TWideStringField;
    QrAemit_xBairro: TWideStringField;
    QrAemit_cMun: TIntegerField;
    QrAemit_xMun: TWideStringField;
    QrAemit_CEP: TIntegerField;
    QrAemit_UF: TWideStringField;
    QrAemit_fone: TWideStringField;
    QrAemit_email: TWideStringField;
    QrAversaoModal: TFloatField;
    QrAStatus: TIntegerField;
    QrAprotMDFe_versao: TFloatField;
    QrAinfProt_Id: TWideStringField;
    QrAinfProt_tpAmb: TSmallintField;
    QrAinfProt_verAplic: TWideStringField;
    QrAinfProt_dhRecbto: TDateTimeField;
    QrAinfProt_dhRecbtoTZD: TFloatField;
    QrAinfProt_nProt: TWideStringField;
    QrAinfProt_digVal: TWideStringField;
    QrAinfProt_cStat: TIntegerField;
    QrAinfProt_xMotivo: TWideStringField;
    QrAretCancMDFe_versao: TFloatField;
    QrAinfCanc_Id: TWideStringField;
    QrAinfCanc_tpAmb: TSmallintField;
    QrAinfCanc_verAplic: TWideStringField;
    QrAinfCanc_dhRecbto: TDateTimeField;
    QrAinfCanc_dhRecbtoTZD: TFloatField;
    QrAinfCanc_nProt: TWideStringField;
    QrAinfCanc_digVal: TWideStringField;
    QrAinfCanc_cStat: TIntegerField;
    QrAinfCanc_xMotivo: TWideStringField;
    QrAinfCanc_cJust: TIntegerField;
    QrAinfCanc_xJust: TWideStringField;
    QrAFisRegCad: TIntegerField;
    QrACartEmiss: TIntegerField;
    QrATabelaPrc: TIntegerField;
    QrACondicaoPg: TIntegerField;
    QrACodInfoEmite: TIntegerField;
    QrACriAForca: TSmallintField;
    QrAcSitMDFe: TSmallintField;
    QrAcSitEnc: TSmallintField;
    QrALk: TIntegerField;
    QrADataCad: TDateField;
    QrADataAlt: TDateField;
    QrAUserCad: TIntegerField;
    QrAUserAlt: TIntegerField;
    QrAAlterWeb: TSmallintField;
    QrAAtivo: TSmallintField;
    QrAide_dIniViagem: TDateField;
    QrAide_hIniViagem: TTimeField;
    QrAide_dhIniViagemTZD: TFloatField;
    QrATot_qCTe: TIntegerField;
    QrATot_qNFe: TIntegerField;
    QrATot_qMDFe: TIntegerField;
    QrATot_vCarga: TFloatField;
    QrATot_cUnid: TSmallintField;
    QrATot_qCarga: TFloatField;
    QrAinfAdFisco: TWideMemoField;
    QrAinfCpl: TWideMemoField;
    QrOpcoesMDFe: TmySQLQuery;
    QrOpcoesMDFeMDFeversao: TFloatField;
    QrOpcoesMDFeMDFeide_mod: TSmallintField;
    QrOpcoesMDFeSimplesFed: TSmallintField;
    QrOpcoesMDFeDirMDFeGer: TWideStringField;
    QrOpcoesMDFeDirMDFeAss: TWideStringField;
    QrOpcoesMDFeDirMDFeEnvLot: TWideStringField;
    QrOpcoesMDFeDirMDFeRec: TWideStringField;
    QrOpcoesMDFeDirMDFePedRec: TWideStringField;
    QrOpcoesMDFeDirMDFeProRec: TWideStringField;
    QrOpcoesMDFeDirMDFeDen: TWideStringField;
    QrOpcoesMDFeDirMDFePedCan: TWideStringField;
    QrOpcoesMDFeDirMDFeCan: TWideStringField;
    QrOpcoesMDFeDirMDFePedInu: TWideStringField;
    QrOpcoesMDFeDirMDFeInu: TWideStringField;
    QrOpcoesMDFeDirMDFePedSit: TWideStringField;
    QrOpcoesMDFeDirMDFeSit: TWideStringField;
    QrOpcoesMDFeDirMDFePedSta: TWideStringField;
    QrOpcoesMDFeDirMDFeSta: TWideStringField;
    QrOpcoesMDFeMDFeUF_WebServ: TWideStringField;
    QrOpcoesMDFeMDFeUF_Servico: TWideStringField;
    QrOpcoesMDFePathLogoMDFe: TWideStringField;
    QrOpcoesMDFeCtaFretPrest: TIntegerField;
    QrOpcoesMDFeTxtFretPrest: TWideStringField;
    QrOpcoesMDFeDupFretPrest: TWideStringField;
    QrOpcoesMDFeMDFeSerNum: TWideStringField;
    QrOpcoesMDFeMDFeSerVal: TDateField;
    QrOpcoesMDFeMDFeSerAvi: TSmallintField;
    QrOpcoesMDFeDirDAMDFes: TWideStringField;
    QrOpcoesMDFeDirMDFeProt: TWideStringField;
    QrOpcoesMDFeMDFePreMailAut: TIntegerField;
    QrOpcoesMDFeMDFePreMailCan: TIntegerField;
    QrOpcoesMDFeMDFePreMailEveCCe: TIntegerField;
    QrOpcoesMDFeMDFeVerStaSer: TFloatField;
    QrOpcoesMDFeMDFeVerEnvLot: TFloatField;
    QrOpcoesMDFeMDFeVerConLot: TFloatField;
    QrOpcoesMDFeMDFeVerCanMDFe: TFloatField;
    QrOpcoesMDFeMDFeVerInuNum: TFloatField;
    QrOpcoesMDFeMDFeVerConMDFe: TFloatField;
    QrOpcoesMDFeMDFeVerLotEve: TFloatField;
    QrOpcoesMDFeMDFeide_tpImp: TSmallintField;
    QrOpcoesMDFeMDFeide_tpAmb: TSmallintField;
    QrOpcoesMDFeMDFeAppCode: TSmallintField;
    QrOpcoesMDFeMDFeAssDigMode: TSmallintField;
    QrOpcoesMDFeMDFeEntiTipCto: TIntegerField;
    QrOpcoesMDFeMDFeEntiTipCt1: TIntegerField;
    QrOpcoesMDFeMyEmailMDFe: TWideStringField;
    QrOpcoesMDFeDirMDFeSchema: TWideStringField;
    QrOpcoesMDFeDirMDFeRWeb: TWideStringField;
    QrOpcoesMDFeDirMDFeEveEnvLot: TWideStringField;
    QrOpcoesMDFeDirMDFeEveRetLot: TWideStringField;
    QrOpcoesMDFeDirMDFeEvePedEnc: TWideStringField;
    QrOpcoesMDFeDirMDFeEveRetEnc: TWideStringField;
    QrOpcoesMDFeDirMDFeEveProcEnc: TWideStringField;
    QrOpcoesMDFeDirMDFeEvePedCan: TWideStringField;
    QrOpcoesMDFeDirMDFeEveRetCan: TWideStringField;
    QrOpcoesMDFeDirMDFeRetMDFeDes: TWideStringField;
    QrOpcoesMDFeMDFeMDFeVerConDes: TFloatField;
    QrOpcoesMDFeDirMDFeEvePedMDe: TWideStringField;
    QrOpcoesMDFeDirMDFeEveRetMDe: TWideStringField;
    QrOpcoesMDFeDirDowMDFeDes: TWideStringField;
    QrOpcoesMDFeDirDowMDFeMDFe: TWideStringField;
    QrOpcoesMDFeMDFeInfCpl: TIntegerField;
    QrOpcoesMDFeMDFeVerConsCad: TFloatField;
    QrOpcoesMDFeMDFeVerDistDFeInt: TFloatField;
    QrOpcoesMDFeMDFeVerDowMDFe: TFloatField;
    QrOpcoesMDFeDirDowMDFeCnf: TWideStringField;
    QrOpcoesMDFeMDFeItsLin: TSmallintField;
    QrOpcoesMDFeMDFeFTRazao: TSmallintField;
    QrOpcoesMDFeMDFeFTEnder: TSmallintField;
    QrOpcoesMDFeMDFeFTFones: TSmallintField;
    QrOpcoesMDFeMDFeMaiusc: TSmallintField;
    QrOpcoesMDFeMDFeShowURL: TWideStringField;
    QrOpcoesMDFeMDFetpEmis: TSmallintField;
    QrOpcoesMDFeMDFeSCAN_Ser: TIntegerField;
    QrOpcoesMDFeMDFeSCAN_nMDF: TIntegerField;
    QrOpcoesMDFeMDFe_indFinalCpl: TSmallintField;
    QrOpcoesMDFeTZD_UTC_Auto_TermoAceite: TIntegerField;
    QrOpcoesMDFeTZD_UTC_Auto_DataHoraAceite: TDateTimeField;
    QrOpcoesMDFeTZD_UTC_Auto_UsuarioAceite: TIntegerField;
    QrOpcoesMDFeTZD_UTC_Auto: TSmallintField;
    QrOpcoesMDFeTZD_UTC: TFloatField;
    QrOpcoesMDFehVeraoAsk: TDateField;
    QrOpcoesMDFehVeraoIni: TDateField;
    QrOpcoesMDFehVeraoFim: TDateField;
    QrOpcoesMDFeParamsMDFe: TIntegerField;
    QrOpcoesMDFeCodigo: TIntegerField;
    QrMDFeCabAide_Modal: TSmallintField;
    QrRodoCab: TmySQLQuery;
    QrRodoCabFatID: TIntegerField;
    QrRodoCabFatNum: TIntegerField;
    QrRodoCabEmpresa: TIntegerField;
    QrRodoCabControle: TIntegerField;
    QrRodoCabRNTRC: TWideStringField;
    QrRodoCabveicTracao_cInt: TIntegerField;
    QrRodoCabveicTracao_placa: TWideStringField;
    QrRodoCabveicTracao_RENAVAM: TWideStringField;
    QrRodoCabveicTracao_tara: TIntegerField;
    QrRodoCabveicTracao_capKg: TIntegerField;
    QrRodoCabveicTracao_capM3: TIntegerField;
    QrRodoCabLk: TIntegerField;
    QrRodoCabDataCad: TDateField;
    QrRodoCabDataAlt: TDateField;
    QrRodoCabUserCad: TIntegerField;
    QrRodoCabUserAlt: TIntegerField;
    QrRodoCabAlterWeb: TSmallintField;
    QrRodoCabAtivo: TSmallintField;
    QrRodoCabcodAgPorto: TWideStringField;
    QrRodoCabveicTracao_tpRod: TSmallintField;
    QrRodoCabveicTracao_tpCar: TSmallintField;
    QrRodoCabveicTracao_UF: TWideStringField;
    frxDsRodoCab: TfrxDBDataset;
    QrRodoCIOT: TmySQLQuery;
    QrRodoCIOTCIOT: TLargeintField;
    QrRodoCondutor: TmySQLQuery;
    QrRodoCondutorxNome: TWideStringField;
    QrRodoCondutorCPF: TWideStringField;
    QrRodoVPed: TmySQLQuery;
    QrRodoVPedCNPJForn: TWideStringField;
    QrRodoVPedCNPJPg: TWideStringField;
    QrRodoVPednCompra: TWideStringField;
    QrAID_TXT: TWideStringField;
    QrAEMIT_ENDERECO: TWideStringField;
    QrAEMIT_FONE_TXT: TWideStringField;
    QrAEMIT_IE_TXT: TWideStringField;
    QrAEMIT_CNPJ_TXT: TWideStringField;
    QrADOC_SEM_VLR_JUR: TWideStringField;
    QrADOC_SEM_VLR_FIS: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdFilialExit(Sender: TObject);
    procedure EdFilialChange(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure EdClienteExit(Sender: TObject);
    procedure TPDataIChange(Sender: TObject);
    procedure TPDataIClick(Sender: TObject);
    procedure Cancelamento1Click(Sender: TObject);
    procedure QrClientesBeforeClose(DataSet: TDataSet);
    procedure BtLeArqClick(Sender: TObject);
    procedure dmkDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtConsultaClick(Sender: TObject);
    procedure BtInutilizaClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure CampospreenchidosnoXMLdaMDFe1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrACalcFields(DataSet: TDataSet);
    procedure QrMDFeCabAAfterOpen(DataSet: TDataSet);
    procedure QrMDFeCabAAfterScroll(DataSet: TDataSet);
    procedure QrMDFeCabABeforeClose(DataSet: TDataSet);
    procedure QrIAfterScroll(DataSet: TDataSet);
    procedure QrYAfterOpen(DataSet: TDataSet);
    procedure QrXVolAfterOpen(DataSet: TDataSet);
    procedure BtArqClick(Sender: TObject);
    procedure PMArqPopup(Sender: TObject);
    procedure MDFe1Click(Sender: TObject);
    procedure Autorizao1Click(Sender: TObject);
    procedure Cancelamento2Click(Sender: TObject);
    procedure BtEnviaClick(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure CkEmitClick(Sender: TObject);
    procedure RGAmbienteClick(Sender: TObject);
    procedure DiretriodoarquivoXML1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
    procedure RGOrdem2Click(Sender: TObject);
    procedure Listadasnotaspesquisadas1Click(Sender: TObject);
    procedure QrMDFeCabACalcFields(DataSet: TDataSet);
    procedure QrMDFe_XXXCalcFields(DataSet: TDataSet);
    procedure frxListaMFFesGetValue(const VarName: string; var Value: Variant);
    procedure QrMDFe_101CalcFields(DataSet: TDataSet);
    procedure PreviewdaMDFe1Click(Sender: TObject);
    procedure BtEventoClick(Sender: TObject);
    procedure QrMDFeEveRRetCalcFields(DataSet: TDataSet);
    procedure PMEventosPopup(Sender: TObject);
    procedure CorrigeManifestao1Click(Sender: TObject);
    procedure CGcSitEncClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dmkDBGrid1DblClick(Sender: TObject);
    procedure ImprimeSomenteadmin1Click(Sender: TObject);
    procedure porCFOP1Click(Sender: TObject);
    procedure porNatOp1Click(Sender: TObject);
    procedure ExportaporNatOptotaisMDFe1Click(Sender: TObject);
    procedure ExportaporCFOPtotaiseitensMDFe1Click(Sender: TObject);
    procedure frxA4A_001RodoLotGetValue(const VarName: string; var Value: Variant);
    procedure frxA4A_001RodoGetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
    // F N F e YIts: String;
    FLote, FEmpresa, FIDCtrl: Integer;
(*
    VARF_SEG_NOME_01, VARF_SEG_NOME_02,
    VARF_SEG_RESP_01, VARF_SEG_RESP_02,
    VARF_SEG_APOL_01, VARF_SEG_APOL_02,
    VARF_SEG_AVER_01, VARF_SEG_AVER_02,
    VARF_UMED_QTDE_01, VARF_UMED_QTDE_02, VARF_UMED_QTDE_03, VARF_UMED_QTDE_04,
    VARF_UMED_QTDE_05, VARF_UMED_QTDE_06, VARF_UMED_QTDE_07, VARF_UMED_QTDE_08,
    VARF_CMPVL_NOM_01, VARF_CMPVL_VLR_01, VARF_CMPVL_NOM_02, VARF_CMPVL_VLR_02,
    VARF_CMPVL_NOM_03, VARF_CMPVL_VLR_03, VARF_CMPVL_NOM_04, VARF_CMPVL_VLR_04,
    VARF_CMPVL_NOM_05, VARF_CMPVL_VLR_05, VARF_CMPVL_NOM_06, VARF_CMPVL_VLR_06,
    VARF_CMPVL_NOM_07, VARF_CMPVL_VLR_07, VARF_CMPVL_NOM_08, VARF_CMPVL_VLR_08,
    VARF_CMPVL_NOM_09, VARF_CMPVL_VLR_09, VARF_CMPVL_NOM_10, VARF_CMPVL_VLR_10,
    VARF_CMPVL_NOM_11, VARF_CMPVL_VLR_11, VARF_CMPVL_NOM_12, VARF_CMPVL_VLR_12,
    VARF_CMPVL_NOM_13, VARF_CMPVL_VLR_13, VARF_CMPVL_NOM_14, VARF_CMPVL_VLR_14,
    VARF_CMPVL_NOM_15, VARF_CMPVL_VLR_15, VARF_CMPVL_NOM_16, VARF_CMPVL_VLR_16,
    VARF_DOCUM_TIPO_01, VARF_DOCUM_DOCU_01, VARF_DOCUM_SRNR_01,
    VARF_DOCUM_TIPO_02, VARF_DOCUM_DOCU_02, VARF_DOCUM_SRNR_02,
    VARF_DOCUM_TIPO_03, VARF_DOCUM_DOCU_03, VARF_DOCUM_SRNR_03,
    VARF_DOCUM_TIPO_04, VARF_DOCUM_DOCU_04, VARF_DOCUM_SRNR_04,
    VARF_DOCUM_TIPO_05, VARF_DOCUM_DOCU_05, VARF_DOCUM_SRNR_05,
    VARF_DOCUM_TIPO_06, VARF_DOCUM_DOCU_06, VARF_DOCUM_SRNR_06,
    VARF_DOCUM_TIPO_07, VARF_DOCUM_DOCU_07, VARF_DOCUM_SRNR_07,
    VARF_DOCUM_TIPO_08, VARF_DOCUM_DOCU_08, VARF_DOCUM_SRNR_08,
    VARF_DOCUM_TIPO_09, VARF_DOCUM_DOCU_09, VARF_DOCUM_SRNR_09,
    VARF_DOCUM_TIPO_10, VARF_DOCUM_DOCU_10, VARF_DOCUM_SRNR_10,
    VARF_DOCUM_TIPO_11, VARF_DOCUM_DOCU_11, VARF_DOCUM_SRNR_11,
    VARF_DOCUM_TIPO_12, VARF_DOCUM_DOCU_12, VARF_DOCUM_SRNR_12,
    VARF_DOCUM_TIPO_13, VARF_DOCUM_DOCU_13, VARF_DOCUM_SRNR_13,
    VARF_DOCUM_TIPO_14, VARF_DOCUM_DOCU_14, VARF_DOCUM_SRNR_14,
    VARF_DOCUM_TIPO_15, VARF_DOCUM_DOCU_15, VARF_DOCUM_SRNR_15,
    VARF_DOCUM_TIPO_16, VARF_DOCUM_DOCU_16, VARF_DOCUM_SRNR_16,
    VARF_DOCUM_TIPO_17, VARF_DOCUM_DOCU_17, VARF_DOCUM_SRNR_17,
    VARF_DOCUM_TIPO_18, VARF_DOCUM_DOCU_18, VARF_DOCUM_SRNR_18,
    VARF_DOCUM_TIPO_19, VARF_DOCUM_DOCU_19, VARF_DOCUM_SRNR_19,
    VARF_DOCUM_TIPO_20, VARF_DOCUM_DOCU_20, VARF_DOCUM_SRNR_20,
    VARF_TPVEIC_TIPO_01, VARF_TPVEIC_PLACA_01, VARF_TPVEIC_UF_01, VARF_TPVEIC_RNTRC_01,
    VARF_TPVEIC_TIPO_02, VARF_TPVEIC_PLACA_02, VARF_TPVEIC_UF_02, VARF_TPVEIC_RNTRC_02,
    VARF_TPVEIC_TIPO_03, VARF_TPVEIC_PLACA_03, VARF_TPVEIC_UF_03, VARF_TPVEIC_RNTRC_03,
    VARF_TPVEIC_TIPO_04, VARF_TPVEIC_PLACA_04, VARF_TPVEIC_UF_04, VARF_TPVEIC_RNTRC_04,
    VARF_VPEDAG_CNPJR_01, VARF_VPEDAG_CNPJF_01, VARF_VPEDAG_NCOMP_01,
    VARF_VPEDAG_CNPJR_02, VARF_VPEDAG_CNPJF_02, VARF_VPEDAG_NCOMP_02,
    VARF_VPEDAG_CNPJR_03, VARF_VPEDAG_CNPJF_03, VARF_VPEDAG_NCOMP_03,
    VARF_VPEDAG_CNPJR_04, VARF_VPEDAG_CNPJF_04, VARF_VPEDAG_NCOMP_04,
    VARF_LACRODO_LACRES, VARF_OBS_CONT, VARF_OBS_FISCO,
    VARF_MOTO_NOME_01,VARF_MOTO_NOME_02, VARF_MOTO_CPF_01, VARF_MOTO_CPF_02,
*)
    VARF_VPED_RESP_CNPJ_01, VARF_VPED_RESP_CNPJ_02,
    VARF_VPED_PAGA_CNPJ_01, VARF_VPED_PAGA_CNPJ_02,
    VARF_VPED_NUM_COMPR_01, VARF_VPED_NUM_COMPR_02,
    VARF_CONDUTOR_CPF_01, VARF_CONDUTOR_CPF_02, VARF_CONDUTOR_CPF_03,
    VARF_CONDUTOR_CPF_04, VARF_CONDUTOR_CPF_05, VARF_CONDUTOR_CPF_06,
    VARF_CONDUTOR_CPF_07, VARF_CONDUTOR_CPF_08, VARF_CONDUTOR_CPF_09,
    VARF_CONDUTOR_NOME_01, VARF_CONDUTOR_NOME_02, VARF_CONDUTOR_NOME_03,
    VARF_CONDUTOR_NOME_04, VARF_CONDUTOR_NOME_05, VARF_CONDUTOR_NOME_06,
    VARF_CONDUTOR_NOME_07, VARF_CONDUTOR_NOME_08, VARF_CONDUTOR_NOME_09,
    FChaveMDFe, FProtocolo: String;
    // CabY
    FFaturas: array[0..14] of array[0..2] of String;
    // CabXVol
    Fesp, Fmarca, FnVol: String;
    FpesoL, FpesoB, FqVol: Double;
    // Impress�o de lista de MDFes
    F_MDFe_100, F_MDFe_101, F_MDFe_XXX: String;
    //
    FSo_O_ID: Boolean;
    //
    procedure DefineFrx(var Report: TfrxReport; const OQueFaz: TfrxImpComo);
    procedure DefineVarsDePreparacao();
    procedure ImprimeMDFe(Preview: Boolean);
    procedure ImprimeListaNova(PorCFOP, Exporta: Boolean);
    procedure ObtemdadosInfDoc(var Tipo, Docu, SrNr: String);
  public
    { Public declarations }
    FDACTEImpresso, FMailEnviado: Boolean;
    //
    procedure FechaMDFeCabA();
    procedure ReopenMDFeCabA(IDCtrl: Integer; So_O_ID: Boolean);
    procedure ReopenMDFeCabAMsg();
    procedure ReopenMDFeEveRRet();
    (*
    procedure ReopenMDFeIt3InfQ();
    procedure ReopenMDFeIt2Seg();
    procedure ReopenMDFeIt2Comp();
    procedure ReopenMDFeIt2InfDoc();
    procedure ReopenMDFeMoRodoCab();
    procedure ReopenMDFeMoRodoVeic();
    procedure ReopenMDFeMoRodoProp();
    procedure ReopenMDFeMoRodoMoto();
    procedure ReopenMDFeMoRodoLacr();
    procedure ReopenMDFeMoRodoVPed();
    procedure ReopenMDFeIt2ObsCont();
    procedure ReopenMDFeIt2ObsFisco();
    *)
    //
    function  DefineQual_frxMDFe(OQueFaz: TfrxImpComo): TfrxReport;
  end;

  var
  FmMDFe_Pesq_0000: TFmMDFe_Pesq_0000;

implementation

uses UnMyObjects, ModuleGeral, UnInternalConsts, MyDBCheck, UCreate,
  (*MDFeInut_0000a,*) UMySQLModule, Module, ModuleMDFe_0000, MDFeSteps_0100a,
  (*MDFeEveRCab,*) MeuFrx, DmkDAC_PF, UnMailEnv,
  MyGlyfs, Principal, UnMDFe_PF, UnXXe_PF, MDFeEveRCab;

{$R *.DFM}

procedure TFmMDFe_Pesq_0000.BtConsultaClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  //
  DefineVarsDePreparacao();
  MDFe_PF.MostraFormStepsMDFe_ConsultaMDFe(FEmpresa, FIDCtrl, FChaveMDFe, nil);
  //
  ReopenMDFeCabA(FIDCtrl, False);
end;

procedure TFmMDFe_Pesq_0000.BtImprimeClick(Sender: TObject);
begin
  ImprimeMDFe(False);
end;

procedure TFmMDFe_Pesq_0000.DefineFrx(var Report: TfrxReport; const OQueFaz: TfrxImpComo);
var
  I: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  DModG.ReopenParamsEmp(DmodG.QrEmpresasCodigo.Value);
  UnDmkDAC_PF.AbreMySQLQuery0(QrA, Dmod.MyDB, [
  'SELECT * ' ,
  'FROM mdfecaba' ,
  'WHERE FatID=' + Geral.FF0(QrMDFeCabAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrMDFeCabAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrMDFeCabAEmpresa.Value),
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrRodoCab, Dmod.MyDB, [
  'SELECT * ' ,
  'FROM mdfemorodocab' ,
  'WHERE FatID=' + Geral.FF0(QrMDFeCabAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrMDFeCabAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrMDFeCabAEmpresa.Value),
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrRodoCIOT, Dmod.MyDB, [
  'SELECT * ' ,
  'FROM mdfemorodociot' ,
  'WHERE FatID=' + Geral.FF0(QrMDFeCabAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrMDFeCabAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrMDFeCabAEmpresa.Value),
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrRodoCondutor, Dmod.MyDB, [
  'SELECT * ' ,
  'FROM mdfemorodocondutor' ,
  'WHERE FatID=' + Geral.FF0(QrMDFeCabAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrMDFeCabAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrMDFeCabAEmpresa.Value),
  '']);
  //
  VARF_CONDUTOR_CPF_01 := ''; VARF_CONDUTOR_CPF_02 := ''; VARF_CONDUTOR_CPF_03 := '';
  VARF_CONDUTOR_CPF_04 := ''; VARF_CONDUTOR_CPF_05 := ''; VARF_CONDUTOR_CPF_06 := '';
  VARF_CONDUTOR_CPF_07 := ''; VARF_CONDUTOR_CPF_08 := ''; VARF_CONDUTOR_CPF_09 := '';
  VARF_CONDUTOR_NOME_01 := ''; VARF_CONDUTOR_NOME_02 := ''; VARF_CONDUTOR_NOME_03 := '';
  VARF_CONDUTOR_NOME_04 := ''; VARF_CONDUTOR_NOME_05 := ''; VARF_CONDUTOR_NOME_06 := '';
  VARF_CONDUTOR_NOME_07 := ''; VARF_CONDUTOR_NOME_08 := ''; VARF_CONDUTOR_NOME_09 := '';
  QrRodoCondutor.First;
  while not QrRodoCondutor.Eof do
  begin
    if QrRodoCondutor.RecNo = 1 then
    begin
      VARF_CONDUTOR_CPF_01  := QrRodoCondutorCPF.Value;
      VARF_CONDUTOR_Nome_01 := QrRodoCondutorxNome.Value;
    end;
    if QrRodoCondutor.RecNo = 2 then
    begin
      VARF_CONDUTOR_CPF_02  := QrRodoCondutorCPF.Value;
      VARF_CONDUTOR_Nome_02 := QrRodoCondutorxNome.Value;
    end;
    if QrRodoCondutor.RecNo = 3 then
    begin
      VARF_CONDUTOR_CPF_03  := QrRodoCondutorCPF.Value;
      VARF_CONDUTOR_Nome_03 := QrRodoCondutorxNome.Value;
    end;
    if QrRodoCondutor.RecNo = 4 then
    begin
      VARF_CONDUTOR_CPF_04  := QrRodoCondutorCPF.Value;
      VARF_CONDUTOR_Nome_04 := QrRodoCondutorxNome.Value;
    end;
    if QrRodoCondutor.RecNo = 5 then
    begin
      VARF_CONDUTOR_CPF_05  := QrRodoCondutorCPF.Value;
      VARF_CONDUTOR_Nome_05 := QrRodoCondutorxNome.Value;
    end;
    if QrRodoCondutor.RecNo = 6 then
    begin
      VARF_CONDUTOR_CPF_06  := QrRodoCondutorCPF.Value;
      VARF_CONDUTOR_Nome_06 := QrRodoCondutorxNome.Value;
    end;
    if QrRodoCondutor.RecNo = 7 then
    begin
      VARF_CONDUTOR_CPF_07  := QrRodoCondutorCPF.Value;
      VARF_CONDUTOR_Nome_07 := QrRodoCondutorxNome.Value;
    end;
    QrRodoCondutor.Next;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrRodoVPed, Dmod.MyDB, [
  'SELECT * ' ,
  'FROM mdfemorodovped' ,
  'WHERE FatID=' + Geral.FF0(QrMDFeCabAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrMDFeCabAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrMDFeCabAEmpresa.Value),
  '']);
  //
  VARF_VPED_RESP_CNPJ_01 := '';
  VARF_VPED_PAGA_CNPJ_01 := '';
  VARF_VPED_NUM_COMPR_01 := '';
  VARF_VPED_PAGA_CNPJ_02 := '';
  VARF_VPED_RESP_CNPJ_02 := '';
  VARF_VPED_NUM_COMPR_02 := '';
  while not QrRodoVPed.Eof do
  begin
    if QrRodoCondutor.RecNo = 1 then
    begin
      VARF_VPED_RESP_CNPJ_01 := QrRodoVPedCNPJForn.Value;
      VARF_VPED_PAGA_CNPJ_01 := QrRodoVPedCNPJPg.Value;
      VARF_VPED_NUM_COMPR_01 := QrRodoVPednCompra.Value;
    end;
    //
    QrRodoVPed.Next;
  end;

(*
  ReopenMDFeIt3InfQ();
  VARF_UMED_QTDE_01 := ''; VARF_UMED_QTDE_02 := '';
  VARF_UMED_QTDE_03 := ''; VARF_UMED_QTDE_04 := '';
  VARF_UMED_QTDE_05 := ''; VARF_UMED_QTDE_06 := '';
  VARF_UMED_QTDE_07 := ''; VARF_UMED_QTDE_08 := '';
  Qr3InfQ.First;
  while not Qr3InfQ.Eof do
  begin
    if Qr3InfQ.RecNo = 1 then
      VARF_UMED_QTDE_01 := FloatToStr(Qr3InfQqCarga.Value) + Qr3InfQtpMed.Value;
    if Qr3InfQ.RecNo = 2 then
      VARF_UMED_QTDE_02 := FloatToStr(Qr3InfQqCarga.Value) + Qr3InfQtpMed.Value;
    if Qr3InfQ.RecNo = 3 then
      VARF_UMED_QTDE_03 := FloatToStr(Qr3InfQqCarga.Value) + Qr3InfQtpMed.Value;
    if Qr3InfQ.RecNo = 4 then
      VARF_UMED_QTDE_04 := FloatToStr(Qr3InfQqCarga.Value) + Qr3InfQtpMed.Value;
    if Qr3InfQ.RecNo = 5 then
      VARF_UMED_QTDE_05 := FloatToStr(Qr3InfQqCarga.Value) + Qr3InfQtpMed.Value;
    if Qr3InfQ.RecNo = 6 then
      VARF_UMED_QTDE_06 := FloatToStr(Qr3InfQqCarga.Value) + Qr3InfQtpMed.Value;
    if Qr3InfQ.RecNo = 7 then
      VARF_UMED_QTDE_07 := FloatToStr(Qr3InfQqCarga.Value) + Qr3InfQtpMed.Value;
    if Qr3InfQ.RecNo = 8 then
      VARF_UMED_QTDE_08 := FloatToStr(Qr3InfQqCarga.Value) + Qr3InfQtpMed.Value;
    //
    Qr3InfQ.Next;
  end;
  ReopenMDFeIt2Seg();
  VARF_SEG_NOME_01 := '';
  VARF_SEG_RESP_01 := '';
  VARF_SEG_APOL_01 := '';
  VARF_SEG_AVER_01 := '';
  VARF_SEG_NOME_02 := '';
  VARF_SEG_RESP_02 := '';
  VARF_SEG_APOL_02 := '';
  VARF_SEG_AVER_02 := '';
  while not Qr2Seg.Eof do
  begin
    if Qr2Seg.RecNo = 1 then
    begin
      VARF_SEG_NOME_01 := Qr2SegxSeg.Value;
      VARF_SEG_RESP_01 := sMDFeRespSeg[Qr2SegrespSeg.Value];
      VARF_SEG_APOL_01 := Qr2SegnApol.Value;
      VARF_SEG_AVER_01 := Qr2SegnAver.Value;
    end;
    if Qr2Seg.RecNo = 2 then
    begin
      VARF_SEG_NOME_02 := Qr2SegxSeg.Value;
      VARF_SEG_RESP_02 := sMDFeRespSeg[Qr2SegrespSeg.Value];
      VARF_SEG_APOL_02 := Qr2SegnApol.Value;
      VARF_SEG_AVER_02 := Qr2SegnAver.Value;
    end;
    //
    Qr2Seg.Next;
  end;
///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
  ReopenMDFeIt2Comp();
  VARF_CMPVL_NOM_01 := ''; VARF_CMPVL_VLR_01 := '';
  VARF_CMPVL_NOM_02 := ''; VARF_CMPVL_VLR_02 := '';
  VARF_CMPVL_NOM_03 := ''; VARF_CMPVL_VLR_03 := '';
  VARF_CMPVL_NOM_04 := ''; VARF_CMPVL_VLR_04 := '';
  VARF_CMPVL_NOM_05 := ''; VARF_CMPVL_VLR_05 := '';
  VARF_CMPVL_NOM_06 := ''; VARF_CMPVL_VLR_06 := '';
  VARF_CMPVL_NOM_07 := ''; VARF_CMPVL_VLR_07 := '';
  VARF_CMPVL_NOM_08 := ''; VARF_CMPVL_VLR_08 := '';
  VARF_CMPVL_NOM_09 := ''; VARF_CMPVL_VLR_09 := '';
  VARF_CMPVL_NOM_10 := ''; VARF_CMPVL_VLR_10 := '';
  VARF_CMPVL_NOM_11 := ''; VARF_CMPVL_VLR_11 := '';
  VARF_CMPVL_NOM_12 := ''; VARF_CMPVL_VLR_12 := '';
  VARF_CMPVL_NOM_13 := ''; VARF_CMPVL_VLR_13 := '';
  VARF_CMPVL_NOM_14 := ''; VARF_CMPVL_VLR_14 := '';
  VARF_CMPVL_NOM_15 := ''; VARF_CMPVL_VLR_15 := '';
  VARF_CMPVL_NOM_16 := ''; VARF_CMPVL_VLR_16 := '';
  while not Qr2Comp.Eof do
  begin
    if Qr2Comp.RecNo = 1 then
    begin
      VARF_CMPVL_NOM_01 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_01 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 2 then
    begin
      VARF_CMPVL_NOM_02 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_02 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 3 then
    begin
      VARF_CMPVL_NOM_03 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_03 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 4 then
    begin
      VARF_CMPVL_NOM_04 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_04 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 5 then
    begin
      VARF_CMPVL_NOM_05 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_05 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 6 then
    begin
      VARF_CMPVL_NOM_06 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_06 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 7 then
    begin
      VARF_CMPVL_NOM_07 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_07 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 8 then
    begin
      VARF_CMPVL_NOM_08 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_08 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 9 then
    begin
      VARF_CMPVL_NOM_09 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_09 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 10 then
    begin
      VARF_CMPVL_NOM_10 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_10 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 11 then
    begin
      VARF_CMPVL_NOM_11 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_11 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 12 then
    begin
      VARF_CMPVL_NOM_12 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_12 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 13 then
    begin
      VARF_CMPVL_NOM_13 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_13 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 14 then
    begin
      VARF_CMPVL_NOM_14 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_14 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 15 then
    begin
      VARF_CMPVL_NOM_15 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_15 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    if Qr2Comp.RecNo = 16 then
    begin
      VARF_CMPVL_NOM_16 := Qr2CompxNome.Value;
      VARF_CMPVL_VLR_16 := Geral.FFT(Qr2CompvComp.Value, 2, siPositivo);
    end else
    ;
    //
    Qr2Comp.Next;
  end;
///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
  ReopenMDFeIt2InfDoc();
  VARF_DOCUM_TIPO_01 := ''; VARF_DOCUM_DOCU_01 := ''; VARF_DOCUM_SRNR_01 := '';
  VARF_DOCUM_TIPO_02 := ''; VARF_DOCUM_DOCU_02 := ''; VARF_DOCUM_SRNR_02 := '';
  VARF_DOCUM_TIPO_03 := ''; VARF_DOCUM_DOCU_03 := ''; VARF_DOCUM_SRNR_03 := '';
  VARF_DOCUM_TIPO_04 := ''; VARF_DOCUM_DOCU_04 := ''; VARF_DOCUM_SRNR_04 := '';
  VARF_DOCUM_TIPO_05 := ''; VARF_DOCUM_DOCU_05 := ''; VARF_DOCUM_SRNR_05 := '';
  VARF_DOCUM_TIPO_06 := ''; VARF_DOCUM_DOCU_06 := ''; VARF_DOCUM_SRNR_06 := '';
  VARF_DOCUM_TIPO_07 := ''; VARF_DOCUM_DOCU_07 := ''; VARF_DOCUM_SRNR_07 := '';
  VARF_DOCUM_TIPO_08 := ''; VARF_DOCUM_DOCU_08 := ''; VARF_DOCUM_SRNR_08 := '';
  VARF_DOCUM_TIPO_09 := ''; VARF_DOCUM_DOCU_09 := ''; VARF_DOCUM_SRNR_09 := '';
  VARF_DOCUM_TIPO_10 := ''; VARF_DOCUM_DOCU_10 := ''; VARF_DOCUM_SRNR_10 := '';
  VARF_DOCUM_TIPO_11 := ''; VARF_DOCUM_DOCU_11 := ''; VARF_DOCUM_SRNR_11 := '';
  VARF_DOCUM_TIPO_12 := ''; VARF_DOCUM_DOCU_12 := ''; VARF_DOCUM_SRNR_12 := '';
  VARF_DOCUM_TIPO_13 := ''; VARF_DOCUM_DOCU_13 := ''; VARF_DOCUM_SRNR_13 := '';
  VARF_DOCUM_TIPO_14 := ''; VARF_DOCUM_DOCU_14 := ''; VARF_DOCUM_SRNR_14 := '';
  VARF_DOCUM_TIPO_15 := ''; VARF_DOCUM_DOCU_15 := ''; VARF_DOCUM_SRNR_15 := '';
  VARF_DOCUM_TIPO_16 := ''; VARF_DOCUM_DOCU_16 := ''; VARF_DOCUM_SRNR_16 := '';
  VARF_DOCUM_TIPO_17 := ''; VARF_DOCUM_DOCU_17 := ''; VARF_DOCUM_SRNR_17 := '';
  VARF_DOCUM_TIPO_18 := ''; VARF_DOCUM_DOCU_18 := ''; VARF_DOCUM_SRNR_18 := '';
  VARF_DOCUM_TIPO_19 := ''; VARF_DOCUM_DOCU_19 := ''; VARF_DOCUM_SRNR_19 := '';
  VARF_DOCUM_TIPO_20 := ''; VARF_DOCUM_DOCU_20 := ''; VARF_DOCUM_SRNR_20 := '';
  //
  while not Qr2InfDoc.Eof do
  begin
    if Qr2InfDoc.RecNo = 1 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_01, VARF_DOCUM_DOCU_01, VARF_DOCUM_SRNR_01)
    else
    if Qr2InfDoc.RecNo = 2 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_02, VARF_DOCUM_DOCU_02, VARF_DOCUM_SRNR_02)
    else
    if Qr2InfDoc.RecNo = 3 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_03, VARF_DOCUM_DOCU_03, VARF_DOCUM_SRNR_03)
    else
    if Qr2InfDoc.RecNo = 4 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_04, VARF_DOCUM_DOCU_04, VARF_DOCUM_SRNR_04)
    else
    if Qr2InfDoc.RecNo = 5 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_05, VARF_DOCUM_DOCU_05, VARF_DOCUM_SRNR_05)
    else
    if Qr2InfDoc.RecNo = 6 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_06, VARF_DOCUM_DOCU_06, VARF_DOCUM_SRNR_06)
    else
    if Qr2InfDoc.RecNo = 7 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_07, VARF_DOCUM_DOCU_07, VARF_DOCUM_SRNR_07)
    else
    if Qr2InfDoc.RecNo = 8 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_08, VARF_DOCUM_DOCU_08, VARF_DOCUM_SRNR_08)
    else
    if Qr2InfDoc.RecNo = 9 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_09, VARF_DOCUM_DOCU_09, VARF_DOCUM_SRNR_09)
    else
    if Qr2InfDoc.RecNo = 10 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_10, VARF_DOCUM_DOCU_10, VARF_DOCUM_SRNR_10)
    else
    if Qr2InfDoc.RecNo = 11 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_11, VARF_DOCUM_DOCU_11, VARF_DOCUM_SRNR_11)
    else
    if Qr2InfDoc.RecNo = 12 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_12, VARF_DOCUM_DOCU_12, VARF_DOCUM_SRNR_12)
    else
    if Qr2InfDoc.RecNo = 13 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_13, VARF_DOCUM_DOCU_13, VARF_DOCUM_SRNR_13)
    else
    if Qr2InfDoc.RecNo = 14 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_14, VARF_DOCUM_DOCU_14, VARF_DOCUM_SRNR_14)
    else
    if Qr2InfDoc.RecNo = 15 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_15, VARF_DOCUM_DOCU_15, VARF_DOCUM_SRNR_15)
    else
    if Qr2InfDoc.RecNo = 16 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_16, VARF_DOCUM_DOCU_16, VARF_DOCUM_SRNR_16)
    else
    if Qr2InfDoc.RecNo = 17 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_17, VARF_DOCUM_DOCU_17, VARF_DOCUM_SRNR_17)
    else
    if Qr2InfDoc.RecNo = 18 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_18, VARF_DOCUM_DOCU_18, VARF_DOCUM_SRNR_18)
    else
    if Qr2InfDoc.RecNo = 19 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_19, VARF_DOCUM_DOCU_19, VARF_DOCUM_SRNR_19)
    else
    if Qr2InfDoc.RecNo = 20 then
      ObtemdadosInfDoc(VARF_DOCUM_TIPO_20, VARF_DOCUM_DOCU_20, VARF_DOCUM_SRNR_20)
    ;
    //
    Qr2InfDoc.Next;
  end;
///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
  ReopenMDFeMoRodoCab();
///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
  ReopenMDFeMoRodoVeic();
  VARF_TPVEIC_TIPO_01 := ''; VARF_TPVEIC_PLACA_01 := ''; VARF_TPVEIC_UF_01 := ''; VARF_TPVEIC_RNTRC_01 := '';
  VARF_TPVEIC_TIPO_02 := ''; VARF_TPVEIC_PLACA_02 := ''; VARF_TPVEIC_UF_02 := ''; VARF_TPVEIC_RNTRC_02 := '';
  VARF_TPVEIC_TIPO_03 := ''; VARF_TPVEIC_PLACA_03 := ''; VARF_TPVEIC_UF_03 := ''; VARF_TPVEIC_RNTRC_03 := '';
  VARF_TPVEIC_TIPO_04 := ''; VARF_TPVEIC_PLACA_04 := ''; VARF_TPVEIC_UF_04 := ''; VARF_TPVEIC_RNTRC_04 := '';
  while not QrRodoVeic.Eof do
  begin
    if QrRodoVeic.RecNo = 1 then
    begin
      VARF_TPVEIC_TIPO_01  := sMDFeTpVeic[QrRodoVeictpVeic.Value];
      VARF_TPVEIC_PLACA_01 := QrRodoVeicplaca.Value;
      VARF_TPVEIC_UF_01    := QrRodoVeicUF.Value;
      if QrRodoVeictpProp.Value = 'T' then
        VARF_TPVEIC_RNTRC_01 := QrRodoPropRNTRC.Value
      else
        VARF_TPVEIC_RNTRC_01 := QrRodoCabRNTRC.Value;
    end;
    if QrRodoVeic.RecNo = 2 then
    begin
      VARF_TPVEIC_TIPO_02  := sMDFeTpVeic[QrRodoVeictpVeic.Value];
      VARF_TPVEIC_PLACA_02 := QrRodoVeicplaca.Value;
      VARF_TPVEIC_UF_02    := QrRodoVeicUF.Value;
      if QrRodoVeictpProp.Value = 'T' then
        VARF_TPVEIC_RNTRC_02 := QrRodoPropRNTRC.Value
      else
        VARF_TPVEIC_RNTRC_02 := QrRodoCabRNTRC.Value;
    end;
    if QrRodoVeic.RecNo = 3 then
    begin
      VARF_TPVEIC_TIPO_03  := sMDFeTpVeic[QrRodoVeictpVeic.Value];
      VARF_TPVEIC_PLACA_03 := QrRodoVeicplaca.Value;
      VARF_TPVEIC_UF_03    := QrRodoVeicUF.Value;
      if QrRodoVeictpProp.Value = 'T' then
        VARF_TPVEIC_RNTRC_03 := QrRodoPropRNTRC.Value
      else
        VARF_TPVEIC_RNTRC_03 := QrRodoCabRNTRC.Value;
    end;
    if QrRodoVeic.RecNo = 4 then
    begin
      VARF_TPVEIC_TIPO_04  := sMDFeTpVeic[QrRodoVeictpVeic.Value];
      VARF_TPVEIC_PLACA_04 := QrRodoVeicplaca.Value;
      VARF_TPVEIC_UF_04    := QrRodoVeicUF.Value;
      if QrRodoVeictpProp.Value = 'T' then
        VARF_TPVEIC_RNTRC_04 := QrRodoPropRNTRC.Value
      else
        VARF_TPVEIC_RNTRC_04 := QrRodoCabRNTRC.Value;
    end;
    QrRodoVeic.Next;
  end;
///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
  ReopenMDFeMoRodoVPed();
  VARF_VPEDAG_CNPJR_01 := ''; VARF_VPEDAG_CNPJF_01 := ''; VARF_VPEDAG_NCOMP_01 := '';
  VARF_VPEDAG_CNPJR_02 := ''; VARF_VPEDAG_CNPJF_02 := ''; VARF_VPEDAG_NCOMP_02 := '';
  VARF_VPEDAG_CNPJR_03 := ''; VARF_VPEDAG_CNPJF_03 := ''; VARF_VPEDAG_NCOMP_03 := '';
  VARF_VPEDAG_CNPJR_04 := ''; VARF_VPEDAG_CNPJF_04 := ''; VARF_VPEDAG_NCOMP_04 := '';
  while not QrRodoVPed.Eof do
  begin
    if QrRodoVPed.RecNo = 1 then
    begin
      VARF_VPEDAG_CNPJR_01 := Geral.FormataCNPJ_TT_NFe(QrRodoVPedCNPJPg.Value);
      VARF_VPEDAG_CNPJF_01 := Geral.FormataCNPJ_TT_NFe(QrRodoVPedCNPJForn.Value);
      VARF_VPEDAG_NCOMP_01 := QrRodoVPednCompra.Value;
    end;
    if QrRodoVPed.RecNo = 2 then
    begin
      VARF_VPEDAG_CNPJR_02 := Geral.FormataCNPJ_TT_NFe(QrRodoVPedCNPJPg.Value);
      VARF_VPEDAG_CNPJF_02 := Geral.FormataCNPJ_TT_NFe(QrRodoVPedCNPJForn.Value);
      VARF_VPEDAG_NCOMP_02 := QrRodoVPednCompra.Value;
    end;
    if QrRodoVPed.RecNo = 3 then
    begin
      VARF_VPEDAG_CNPJR_03 := Geral.FormataCNPJ_TT_NFe(QrRodoVPedCNPJPg.Value);
      VARF_VPEDAG_CNPJF_03 := Geral.FormataCNPJ_TT_NFe(QrRodoVPedCNPJForn.Value);
      VARF_VPEDAG_NCOMP_03 := QrRodoVPednCompra.Value;
    end;
    if QrRodoVPed.RecNo = 4 then
    begin
      VARF_VPEDAG_CNPJR_04 := Geral.FormataCNPJ_TT(QrRodoVPedCNPJPg.Value);
      VARF_VPEDAG_CNPJF_04 := Geral.FormataCNPJ_TT(QrRodoVPedCNPJForn.Value);
      VARF_VPEDAG_NCOMP_04 := QrRodoVPednCompra.Value;
    end;
    QrRodoVPed.Next;
  end;
///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
  ReopenMDFeMoRodoMoto();
  VARF_MOTO_NOME_01 := ''; VARF_MOTO_CPF_01 := '';
  VARF_MOTO_NOME_02 := ''; VARF_MOTO_CPF_02 := '';
  while not QrRodoMoto.Eof do
  begin
    if QrRodoMoto.RecNo = 1 then
    begin
      VARF_MOTO_NOME_01 := QrRodoMotoxNome.Value;
      VARF_MOTO_CPF_01  := Geral.FormataCNPJ_TT(QrRodoMotoCPF.Value);
    end;
    QrRodoMoto.Next;
    if QrRodoMoto.RecNo = 2 then
    begin
      VARF_MOTO_NOME_02 := QrRodoMotoxNome.Value;
      VARF_MOTO_CPF_02  := Geral.FormataCNPJ_TT(QrRodoMotoCPF.Value);
    end;
    QrRodoMoto.Next;
  end;
///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
  ReopenMDFeMoRodoLacr();
  VARF_LACRODO_LACRES := '';
  while not QrRodoLacr.Eof do
  begin
    if VARF_LACRODO_LACRES <> '' then
      VARF_LACRODO_LACRES := VARF_LACRODO_LACRES + ', ';
    VARF_LACRODO_LACRES := VARF_LACRODO_LACRES + QrRodoLacrnLacre.Value;
    //
    QrRodoLacr.Next;
  end;
  if VARF_LACRODO_LACRES <> '' then
    VARF_LACRODO_LACRES := VARF_LACRODO_LACRES + '.';
///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
  ReopenMDFeIt2ObsCont();
  VARF_OBS_CONT := '';
  while not Qr2ObsCont.Eof do
  begin
    if VARF_OBS_CONT <> '' then
      VARF_OBS_CONT := VARF_OBS_CONT + '. ';
    VARF_OBS_CONT := VARF_OBS_CONT + Qr2ObsContxTexto.Value;
    //
    Qr2ObsCont.Next;
  end;
  if VARF_OBS_CONT <> '' then
    VARF_OBS_CONT := VARF_OBS_CONT + '. ';
///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
  ReopenMDFeIt2ObsFisco();
  VARF_OBS_FISCO := '';
  while not Qr2ObsFisco.Eof do
  begin
    if VARF_OBS_FISCO <> '' then
      VARF_OBS_FISCO := VARF_OBS_FISCO + ', ';
    VARF_OBS_FISCO := VARF_OBS_FISCO + Qr2ObsFiscoxTexto.Value;
    //
    Qr2ObsFisco.Next;
  end;
  if VARF_OBS_FISCO <> '' then
    VARF_OBS_FISCO := VARF_OBS_FISCO + '. ';
///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///
*)
  Report := frxA4A_001Rodo;
  case OQueFaz of
    ficMostra: //MyObjects.frxMostra(Report, 'MDFe' + QrAId.Value);
    begin
      Report.PreviewOptions.ZoomMode := zmPageWidth;
      MyObjects.frxPrepara(Report, 'MDFe' + QrAId.Value);
      for I := 0 to Report.PreviewPages.Count -1 do
      begin
        Report.PreviewPages.Page[I].PaperSize := DMPAPER_A4;
        Report.PreviewPages.ModifyPage(I, Report.PreviewPages.Page[I]);
      end;
      FmMeuFrx.ShowModal;
      FmMeuFrx.Destroy;
      FDACTEImpresso := True;
    end;
    {ficImprime:
    ficSalva}
    ficNone: ;// N�o faz nada!
    ficExporta: MyObjects.frxPrepara(Report, 'MDFe' + QrAId.Value);
    else Geral.MB_Erro('A��o n�o definida para frx da DACTE!');
  end;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmMDFe_Pesq_0000.BtInutilizaClick(Sender: TObject);
begin
{ TODO -cCTe : Inutilizacao }
{
  if DBCheck.CriaFm(TFmMDFeInut_0000a, FmMDFeInut_0000a, afmoNegarComAviso) then
  begin
    FmMDFeInut_0000a.ShowModal;
    FmMDFeInut_0000a.Destroy;
  end;
}
end;

procedure TFmMDFe_Pesq_0000.Autorizao1Click(Sender: TObject);
begin
  Geral.MensagemBox(QrArqXML_Aut.Value, 'XML Autoriza��o', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmMDFe_Pesq_0000.BtReabreClick(Sender: TObject);
var
  IDCtrl: Integer;
begin
  IDCtrl := 0;
  if (QrMDFeCabA.State <> dsInactive) then IDCtrl := QrMDFeCabAIDCtrl.Value;
  ReopenMDFeCabA(IDCtrl, False);
end;

procedure TFmMDFe_Pesq_0000.BtEnviaClick(Sender: TObject);
{ TODO -cCTe : Envia MDFe }
{
  procedure ExportaXMLeDACTE(const CNPJ_CPF: String; var DiretDACTE, DiretXML: String);
  var
    Id, ArqDACTE, ArqXML, DirMDFeXML, DirDACTEs, XML_Distribuicao: String;
    Frx: TfrxReport;
    ExportouDACTE, ExportouXML: Boolean;
  begin
    ExportouDACTE := False;
    ExportouXML   := False;
    DiretDACTE    := '';
    DiretXML      := '';
    Id            := Geral.SoNumero_TT(QrMDFeCabAId.Value);
    DirMDFeXML     := DmMDFe_0000.QrFilialDirMDFeProt.Value;
    DirDACTEs     := DmMDFe_0000.QrFilialDirDACTEs.Value;
    //
    if DirMDFeXML = '' then
      DirMDFeXML := 'C:\Dermatek\MDFe\Clientes';
    if DirDACTEs = '' then
      DirDACTEs := 'C:\Dermatek\MDFe\Clientes';
    //
    Geral.VerificaDir(DirMDFeXML, '\', '', True);
    DirMDFeXML := DirMDFeXML + CNPJ_CPF;
    Geral.VerificaDir(DirDACTEs, '\', '', True);
    DirDACTEs := DirDACTEs + CNPJ_CPF;
    //
    // DACTE
    //
    if QrMDFeCabAcStat.Value = 100 then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Gerando arquivo PDF do DACTE');
      //
      Frx      := DefineQual_frxMDFe(ficNone);
      ArqDACTE := DirDACTEs;
      //
      Geral.VerificaDir(ArqDACTE, '\', 'Diret�rio tempor�rio do DACTE', True);
      //
      ArqDACTE := ArqDACTE + Id + '.pdf';
      //
      try
        frxPDFExport.FileName := ArqDACTE;
        MyObjects.frxPrepara(Frx, 'MDFe' + ID);
        Frx.Export(frxPDFExport);
        //
        ExportouDACTE := True;
      except
        ExportouDACTE := False;
      end;
    end;
    //
    // XML
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Gerando arquivo XML da MDFe');
    //
    ArqXML := DirMDFeXML;
    //
    Geral.VerificaDir(ArqXML, '\', 'Diret�rio tempor�rio do XML de MDFe', True);
    //
    ArqXML := ArqXML + ID + '.xml';
    //
    DmMDFe_0000.QrArq.Close;
    DmMDFe_0000.QrArq.Params[0].AsInteger := QrMDFeCabAIDCtrl.Value;
    DmMDFe_0000.QrArq.Open;
    //
    if MDFe_PF.XML_DistribuiMDFe(Id, Trunc(QrMDFeCabAcStat.Value),
      DmMDFe_0000.QrArqXML_MDFe.Value, DmMDFe_0000.QrArqXML_Aut.Value,
      DmMDFe_0000.QrArqXML_Can.Value, True, XML_Distribuicao,
      QrMDFeCabAversao.Value, 'na base de dados')
    then
      ExportouXML := MDFe_PF.SalvaXML(ArqXML, XML_Distribuicao);
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //
    if ExportouXML then
      DiretXML := ArqXML;
    if ExportouDACTE then
      DiretDACTE := ArqDACTE;
  end;
}
var
  CNPJ_CPF, DACTE, XML: String;
  Entidade: Integer;
begin
  Geral.MB_Info('O Manifesto n�o � um documento compartilh�vel com terceiros!');
{
  DmMDFe_0000.ReopenOpcoesMDFe(QrOpcoesMDFe, DModG.QrEmpresasCodigo.Value, True);
  //
  if QrMDFeCabAdest_CNPJ.Value <> '' then
    CNPJ_CPF := QrMDFeCabAdest_CNPJ.Value
  else
    CNPJ_CPF := QrMDFeCabAdest_CPF.Value;
  //
  ExportaXMLeDACTE(CNPJ_CPF, DACTE, XML);
  //
  CNPJ_CPF := Geral.SoNumero_TT(CNPJ_CPF);
  //
  DModG.ObtemEntidadeDeCNPJCFP(CNPJ_CPF, Entidade);
  //
  if MyObjects.FIC(DACTE = '', nil, 'Falha ao exportar DACTE!' + sLineBreak + 'O e-mail n�o poder� ser enviado!') then Exit;
  if MyObjects.FIC(XML = '', nil, 'Falha ao exportar XML!' + sLineBreak + 'O e-mail n�o poder� ser enviado!') then Exit;
  //
  if UMailEnv.Monta_e_Envia_Mail([Entidade, DModG.QrEmpresasCodigo.Value,
    QrMDFeCabAcStat.Value], meMDFe, [DACTE, XML], [QrMDFeCabAdest_xNome.Value,
    QrMDFeCabAId.Value], True) then
  begin
    FMailEnviado := True;
    Geral.MB_Aviso('E-mail enviado com sucesso!');
  end;
}
end;

procedure TFmMDFe_Pesq_0000.BtEventoClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMDFeEveRCab, FmMDFeEveRCab, afmoNegarComAviso) then
  begin
    FmMDFeEveRCab.ReopenMDFeCabA(
      QrMDFeCabAFatID.Value, QrMDFeCabAFatNum.Value, QrMDFeCabAEmpresa.Value);
    FmMDFeEveRCab.ShowModal;
    FmMDFeEveRCab.Destroy;
  end;
end;

procedure TFmMDFe_Pesq_0000.BtExcluiClick(Sender: TObject);
{ TODO -cCTe : Exclusao MDFe }
{
  function VerificaSeNumeroFoiInutilizado(MDFeNum: Integer): Boolean;
  begin
    //True  = Libera Exclus�o
    //False = Bloqueia Exclus�o
    Result := False;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAux, Dmod.MyDB, [
      'SELECT nNFIni, nNFFim ',
      'FROM mdfeinut ',
      'WHERE ' + Geral.FF0(MDFeNum) + ' BETWEEN nNFIni AND nNFFim ',
      '']);
    if DModG.QrAux.RecordCount > 0 then
      Result := True;    
  end;
}
var
  Status, FatID, FatNum, Empresa, ide_nNF: Integer;
  ChaveMDFe: String;
begin
{
  if (QrMDFeCabA.State = dsInactive) or (QrMDFeCabA.RecordCount = 0) then Exit;
  //
  Status   := Trunc(QrMDFeCabAcStat.Value);
  FatID    := QrMDFeCabAFatID.Value;
  FatNum   := QrMDFeCabAFatNum.Value;
  Empresa  := QrMDFeCabAEmpresa.Value;
  ChaveMDFe := QrMDFeCabAId.Value;
  //
  if Geral.MensagemBox('Deseja realmente excluir a MDFe:' + sLineBreak +
  'S�rie: ' + FormatFloat('000', QrMDFeCabAide_serie.Value) + sLineBreak +
  'N�: ' + FormatFloat('000', QrMDFeCabAide_nNF.Value) + sLineBreak + sLineBreak +
  'ESTE PROCEDIMENTO N�O PODER� SER REVERTIDO!',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    if VerificaSeNumeroFoiInutilizado(QrMDFeCabAide_nNF.Value) then
    begin
      if DmMDFe_0000.CancelaFaturamento(ChaveMDFe) then //Primeiro exclui o faturamento e estoque depois a nota
      begin
        DmMDFe_0000.ExcluiMDFe(Status, FatID, FatNum, Empresa, False);
        //
        QrMDFeCabA.Next;
        ide_nNF := UMyMod.ProximoRegistro(QrMDFeCabA, 'ide_nNF', QrMDFeCabAide_nNF.Value);
        //
        BtReabreClick(Self);
        QrMDFeCabA.Locate('ide_nNF', ide_nNF, []);
        Geral.MensagemBox('MDFe exclu�da com sucesso!',
        'Informa��o', MB_OK+MB_ICONINFORMATION);
      end;
    end else
      Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak +
        'Motivo: Este n�mero n�o foi inutilizado!' + sLineBreak +
        'Inutilize este n�mero e tente novamente!');
  end;
}
end;

procedure TFmMDFe_Pesq_0000.BtArqClick(Sender: TObject);
begin
  QrArq.Close;
  QrArq.Params[0].AsInteger := QrMDFeCabAIDCtrl.Value;
  QrArq.Open;
  //
  MyObjects.MostraPopUpDeBotao(PMArq, BtArq);
end;

procedure TFmMDFe_Pesq_0000.BtLeArqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLeArq, BtLeArq);
end;

procedure TFmMDFe_Pesq_0000.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTROX := QrMDFeCabAId.Value;
  //
  if TFmMDFe_Pesq_0000(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmMDFe_Pesq_0000.CampospreenchidosnoXMLdaMDFe1Click(Sender: TObject);
begin
  QrMDFeXMLI.Close;
  QrMDFeXMLI.Params[00].AsInteger := QrMDFeCabAFatID.Value;
  QrMDFeXMLI.Params[01].AsInteger := QrMDFeCabAFatNum.Value;
  QrMDFeXMLI.Params[02].AsInteger := QrMDFeCabAEmpresa.Value;
  QrMDFeXMLI.Open;
  //
  MyObjects.frxMostra(frxCampos, 'Campos preenchidos no XML');
end;

procedure TFmMDFe_Pesq_0000.Cancelamento1Click(Sender: TObject);
const
  SohLer = True;
begin
  //MDFe_PF.MostraFormStepsMDFe_CancelaMDFe(SohLer);
end;

procedure TFmMDFe_Pesq_0000.Cancelamento2Click(Sender: TObject);
begin
  Geral.MensagemBox(QrArqXML_Can.Value, 'XML Cancelamento', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmMDFe_Pesq_0000.CGcSitEncClick(Sender: TObject);
begin
  FechaMDFeCabA;
end;

procedure TFmMDFe_Pesq_0000.CkEmitClick(Sender: TObject);
begin
  FechaMDFeCabA;
end;

procedure TFmMDFe_Pesq_0000.CorrigeManifestao1Click(Sender: TObject);
var
  IDCtrl, cSitEnc: Integer;
begin
{ TODO -cCTe : Manifest MDFe }
{
  case QrMDFeEveRRetret_tpEvento.Value of
    MDFe_CodEventoMDeConfirmacao, // = 210200;
    MDFe_CodEventoMDeCiencia    , // = 210210;
    MDFe_CodEventoMDeDesconhece , // = 210220;
    MDFe_CodEventoMDeNaoRealizou: // = 210240;
    begin
      if (QrMDFeCabAcSitEnc.Value = MDFe_CodEventoSitConfNaoConsultada)
      or (QrMDFeCabAcSitEnc.Value = MDFe_CodEventoSitConfMDeSemManifestacao) then
      begin
        if QrMDFeEveRRetret_cStat.Value = 135 then
        begin
          IDCtrl   := QrMDFeCabAIDCtrl.Value;
          cSitEnc := MDFe_PF.Obtem_DeManifestacao_de_tpEvento_cSitEnc(
            QrMDFeEveRRetret_tpEvento.Value);
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfecaba', False, [
          (*'cSitMDFe',*) 'cSitEnc'
          ], [
          'IDCtrl'
          ], [
          (*cSitMDFe,*) cSitEnc
          ], [
          IDCtrl], True) then
            ReopenMDFeCabA(IDCtrl, FSo_O_ID);
        end
        else Geral.MB_Aviso('O evento selecionado n�o foi vinculado na sefaz!');
      end
      else Geral.MB_Aviso('O evento selecionado n�o permite altera��o!');
    end
    else Geral.MB_Aviso('N�o h� implementa��o para o evento selecionado!');
  end;
}
end;

procedure TFmMDFe_Pesq_0000.DefineVarsDePreparacao();
begin
  FLote      := QrMDFeCabALoteEnv.Value;
  FEmpresa   := QrMDFeCabAEmpresa.Value;
  FChaveMDFe  := QrMDFeCabAId.Value;
  FProtocolo := QrMDFeCabAnProt.Value;
  FIDCtrl    := QrMDFeCabAIDCtrl.Value;
end;

function TFmMDFe_Pesq_0000.DefineQual_frxMDFe(OQueFaz: TfrxImpComo): TfrxReport;
var
  Report: TfrxReport;
begin
  if TMDFeModal(QrMDFeCabAide_Modal.Value) = mdfemodalRodoviario then
  begin
    FDACTEImpresso := False;
    DefineFrx(Report, OQueFaz);
    MyObjects.frxDefineDataSets(Report, [
    frxDsA,
    frxDsRodoCab
    ]);
    Result := Report;
  end;
end;

procedure TFmMDFe_Pesq_0000.DiretriodoarquivoXML1Click(Sender: TObject);
var
  Dir, Arq, Ext, Arquivo: String;
begin
  if not DmMDFe_0000.ReopenEmpresa(DModG.QrEmpresasCodigo.Value) then Exit;
  //
  Arq := QrMDFeCabAId.Value;
  Ext := CTE_EXT_CTE_XML;
  //
  if DmMDFe_0000.ObtemDirXML(Ext, Dir, True) then
  begin
    Arquivo := Dir + Arq + Ext;
    Geral.MensagemBox(Arquivo, 'Caminho do arquivo XML', MB_OK+MB_ICONWARNING);
  end; 
end;

procedure TFmMDFe_Pesq_0000.dmkDBGrid1DblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := Uppercase(dmkDBGrid1.Columns[THackDBGrid(dmkDBGrid1).Col -1].FieldName);
  //
  if Campo = Uppercase('Id') then
  begin
    if (QrMDFeCabA.State <> dsInactive) and (QrMDFeCabA.RecordCount > 0) then
      Geral.MB_Aviso('Chave de acesso da MDF-e:' + sLineBreak + QrMDFeCabAId.Value);
  end;
end;

procedure TFmMDFe_Pesq_0000.dmkDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  DmMDFe_0000.ColoreStatusMDFeEmDBGrid(TDBGrid(dmkDBGrid1), Rect, DataCol, Column,
  State, Trunc(QrMDFeCabAcStat.Value));
  //
  DmMDFe_0000.ColoreSitConfMDFeEmDBGrid(TDBGrid(dmkDBGrid1), Rect, DataCol, Column,
  State, QrMDFeCabAcSitEnc.Value);
end;

procedure TFmMDFe_Pesq_0000.EdClienteChange(Sender: TObject);
begin
  if not EdFilial.Focused then
    FechaMDFeCabA;
end;

procedure TFmMDFe_Pesq_0000.EdClienteExit(Sender: TObject);
begin
  FechaMDFeCabA;
end;

procedure TFmMDFe_Pesq_0000.EdFilialChange(Sender: TObject);
begin
  if not EdFilial.Focused then
    FechaMDFeCabA;
end;

procedure TFmMDFe_Pesq_0000.EdFilialExit(Sender: TObject);
begin
  FechaMDFeCabA;
end;

procedure TFmMDFe_Pesq_0000.ExportaporCFOPtotaiseitensMDFe1Click(Sender: TObject);
begin
  ImprimeListaNova(True, True);
end;

procedure TFmMDFe_Pesq_0000.ExportaporNatOptotaisMDFe1Click(Sender: TObject);
begin
  ImprimeListaNova(False, True);
end;

procedure TFmMDFe_Pesq_0000.FormActivate(Sender: TObject);
begin
{ *****
  if TFmMDFe_Pesq_0000(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
  //Corrige lan�amentos financeiros de notas com desconto
  UMyMod.AbreQuery(DModG.QrCtrlGeral, Dmod.MyDB);
  if DModG.QrCtrlGeralAtualizouLctsMDFe.AsInteger = 0 then
    DmMDFe_0000.CorrigeLctosFaturaMDFes(PB1);
  if DModG.QrCtrlGeralAtualizouMDFeCan.AsInteger = 0 then
    DmMDFe_0000.AtualizaTodasNotasCanceladas(PB1);
}
end;

procedure TFmMDFe_Pesq_0000.FormCreate(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  ImgTipo.SQLType := stPsq;
  FDACTEImpresso := False;
  FMailEnviado := False;
  PageControl1.ActivePageIndex := 0;
  DModG.ReopenEmpresas(VAR_USUARIO, 0, EdFilial, CBFilial);
  QrClientes.Open;
  TPDataI.Date := Date - 10; // m�x permitido pelo fisco
  TPDataF.Date := Date;
  //
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  CBFilial.ListSource := DModG.DsEmpresas;
  RGAmbiente.ItemIndex := DModG.QrParamsEmpide_tpAmb.Value;
  //
  ReopenMDFeCabA(0, False);
  //
  //QrMDFeYIts.Database := DModG.MyPID_DB;
  //
  CGcSitEnc.SetMaxValue();
  //
  Screen.Cursor := crDefault;
end;

procedure TFmMDFe_Pesq_0000.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMDFe_Pesq_0000.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
end;

procedure TFmMDFe_Pesq_0000.frxA4A_001RodoGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'LogoMDFeExiste' then Value := FileExists(QrOpcoesMDFePathLogoMDFe.Value) else
  if VarName = 'LogoMDFePath' then Value := QrOpcoesMDFePathLogoMDFe.Value else
  if VarName = 'VARF_Tot_qCarga' then
  begin
    case TMDFeCargaCUnid(QrATot_cUnid.Value) of
      mdfeccuKG : Value := QrATot_qCarga.Value;
      mdfeccuTON: Value := QrATot_qCarga.Value * 1000;
      (*mdfeccuIndef=0,*)
      else
      begin
        Geral.MB_Aviso('Unidade "Tot_cUnid" n�o implementada!');
        Value := '????';
      end;
    end;
  end;
  if VarName = 'VARF_CIOT' then Value := QrRodoCIOTCIOT.Value else
  if VarName = 'VARF_CONDUTOR_CPF_01' then Value := VARF_CONDUTOR_CPF_01 else
  if VarName = 'VARF_CONDUTOR_NOME_01' then Value := VARF_CONDUTOR_NOME_01 else
  if VarName = 'VARF_CONDUTOR_CPF_02' then Value := VARF_CONDUTOR_CPF_02 else
  if VarName = 'VARF_CONDUTOR_NOME_02' then Value := VARF_CONDUTOR_NOME_02 else
  if VarName = 'VARF_CONDUTOR_CPF_03' then Value := VARF_CONDUTOR_CPF_03 else
  if VarName = 'VARF_CONDUTOR_NOME_03' then Value := VARF_CONDUTOR_NOME_03 else
  if VarName = 'VARF_CONDUTOR_CPF_04' then Value := VARF_CONDUTOR_CPF_04 else
  if VarName = 'VARF_CONDUTOR_NOME_04' then Value := VARF_CONDUTOR_NOME_04 else
  if VarName = 'VARF_CONDUTOR_CPF_05' then Value := VARF_CONDUTOR_CPF_05 else
  if VarName = 'VARF_CONDUTOR_NOME_05' then Value := VARF_CONDUTOR_NOME_05 else
  if VarName = 'VARF_CONDUTOR_CPF_06' then Value := VARF_CONDUTOR_CPF_06 else
  if VarName = 'VARF_CONDUTOR_NOME_06' then Value := VARF_CONDUTOR_NOME_06 else
  if VarName = 'VARF_CONDUTOR_CPF_07' then Value := VARF_CONDUTOR_CPF_07 else
  if VarName = 'VARF_CONDUTOR_NOME_07' then Value := VARF_CONDUTOR_NOME_07 else
  if VarName = 'VARF_VPED_RESP_CNPJ_01' then Value := VARF_VPED_RESP_CNPJ_01 else
  if VarName = 'VARF_VPED_RESP_CNPJ_02' then Value := VARF_VPED_RESP_CNPJ_02 else
  if VarName = 'VARF_VPED_PAGA_CNPJ_01' then Value := VARF_VPED_PAGA_CNPJ_01 else
  if VarName = 'VARF_VPED_PAGA_CNPJ_02' then Value := VARF_VPED_PAGA_CNPJ_02 else
  if VarName = 'VARF_VPED_NUM_COMPR_01' then Value := VARF_VPED_NUM_COMPR_01 else
  if VarName = 'VARF_VPED_NUM_COMPR_02' then Value := VARF_VPED_NUM_COMPR_02 else
  //if VarName = 'VARF_VEIC_PLACA' then Value := ? else
  //if VarName = 'VARF_VEIC_RNTRC' then Value := ? else
  ;
end;

procedure TFmMDFe_Pesq_0000.frxA4A_001RodoLotGetValue(const VarName: string;
  var Value: Variant);
begin
{
  if VarName = 'LogoMDFeExiste' then Value := FileExists(QrOpcoesMDFePathLogoMDFe.Value) else
  if VarName = 'LogoMDFePath' then Value := QrOpcoesMDFePathLogoMDFe.Value else
  if VarName = 'VARF_TPCTE_TXT' then Value := sMDFetpMDFe[QrAide_tpMDFe.Value];
  if VarName = 'VARF_TPSERV_TXT' then Value := sMDFetpserv[QrAide_tpServ.Value];
  if VarName = 'VARF_TOMA99_TXT' then Value := sMDFeToma[QrAtoma99_toma.Value];
  if VarName = 'VARF_FORPAG_TXT' then Value := sMDFeforpag[QrAide_forPag.Value];
  //
  if VarName = 'VARF_UMED_QTDE_01' then Value := VARF_UMED_QTDE_01;
  if VarName = 'VARF_UMED_QTDE_02' then Value := VARF_UMED_QTDE_02;
  if VarName = 'VARF_UMED_QTDE_03' then Value := VARF_UMED_QTDE_03;
  if VarName = 'VARF_UMED_QTDE_04' then Value := VARF_UMED_QTDE_04;
  if VarName = 'VARF_UMED_QTDE_05' then Value := VARF_UMED_QTDE_05;
  if VarName = 'VARF_UMED_QTDE_06' then Value := VARF_UMED_QTDE_06;
  if VarName = 'VARF_UMED_QTDE_07' then Value := VARF_UMED_QTDE_07;
  if VarName = 'VARF_UMED_QTDE_08' then Value := VARF_UMED_QTDE_08;
  //
  if VarName = 'VARF_SEG_NOME_01' then Value := VARF_SEG_NOME_01;
  if VarName = 'VARF_SEG_RESP_01' then Value := VARF_SEG_RESP_01;
  if VarName = 'VARF_SEG_APOL_01' then Value := VARF_SEG_APOL_01;
  if VarName = 'VARF_SEG_AVER_01' then Value := VARF_SEG_AVER_01;

  if VarName = 'VARF_SEG_NOME_02' then Value := VARF_SEG_NOME_02;
  if VarName = 'VARF_SEG_RESP_02' then Value := VARF_SEG_RESP_02;
  if VarName = 'VARF_SEG_APOL_02' then Value := VARF_SEG_APOL_02;
  if VarName = 'VARF_SEG_AVER_02' then Value := VARF_SEG_AVER_02;
  //
  if VarName = 'VARF_CMPVL_NOM_01' then Value := VARF_CMPVL_NOM_01;
  if VarName = 'VARF_CMPVL_VLR_01' then Value := VARF_CMPVL_VLR_01;
  if VarName = 'VARF_CMPVL_NOM_02' then Value := VARF_CMPVL_NOM_02;
  if VarName = 'VARF_CMPVL_VLR_02' then Value := VARF_CMPVL_VLR_02;
  if VarName = 'VARF_CMPVL_NOM_03' then Value := VARF_CMPVL_NOM_03;
  if VarName = 'VARF_CMPVL_VLR_03' then Value := VARF_CMPVL_VLR_03;
  if VarName = 'VARF_CMPVL_NOM_04' then Value := VARF_CMPVL_NOM_04;
  if VarName = 'VARF_CMPVL_VLR_04' then Value := VARF_CMPVL_VLR_04;
  if VarName = 'VARF_CMPVL_NOM_05' then Value := VARF_CMPVL_NOM_05;
  if VarName = 'VARF_CMPVL_VLR_05' then Value := VARF_CMPVL_VLR_05;
  if VarName = 'VARF_CMPVL_NOM_06' then Value := VARF_CMPVL_NOM_06;
  if VarName = 'VARF_CMPVL_VLR_06' then Value := VARF_CMPVL_VLR_06;
  if VarName = 'VARF_CMPVL_NOM_07' then Value := VARF_CMPVL_NOM_07;
  if VarName = 'VARF_CMPVL_VLR_07' then Value := VARF_CMPVL_VLR_07;
  if VarName = 'VARF_CMPVL_NOM_08' then Value := VARF_CMPVL_NOM_08;
  if VarName = 'VARF_CMPVL_VLR_08' then Value := VARF_CMPVL_VLR_08;
  if VarName = 'VARF_CMPVL_NOM_09' then Value := VARF_CMPVL_NOM_09;
  if VarName = 'VARF_CMPVL_VLR_09' then Value := VARF_CMPVL_VLR_09;
  if VarName = 'VARF_CMPVL_NOM_10' then Value := VARF_CMPVL_NOM_10;
  if VarName = 'VARF_CMPVL_VLR_10' then Value := VARF_CMPVL_VLR_10;
  if VarName = 'VARF_CMPVL_NOM_11' then Value := VARF_CMPVL_NOM_11;
  if VarName = 'VARF_CMPVL_VLR_11' then Value := VARF_CMPVL_VLR_11;
  if VarName = 'VARF_CMPVL_NOM_12' then Value := VARF_CMPVL_NOM_12;
  if VarName = 'VARF_CMPVL_VLR_12' then Value := VARF_CMPVL_VLR_12;
  if VarName = 'VARF_CMPVL_NOM_13' then Value := VARF_CMPVL_NOM_13;
  if VarName = 'VARF_CMPVL_VLR_13' then Value := VARF_CMPVL_VLR_13;
  if VarName = 'VARF_CMPVL_NOM_14' then Value := VARF_CMPVL_NOM_14;
  if VarName = 'VARF_CMPVL_VLR_14' then Value := VARF_CMPVL_VLR_14;
  if VarName = 'VARF_CMPVL_NOM_15' then Value := VARF_CMPVL_NOM_15;
  if VarName = 'VARF_CMPVL_VLR_15' then Value := VARF_CMPVL_VLR_15;
  if VarName = 'VARF_CMPVL_NOM_16' then Value := VARF_CMPVL_NOM_16;
  if VarName = 'VARF_CMPVL_VLR_16' then Value := VARF_CMPVL_VLR_16;
  //
  if VarName = 'VARF_CST_TXT' then Value := sMDFeMyCST[QrAMDFeMyCST.Value];
  //
  if VarName = 'VARF_DOCUM_TIPO_01' then Value := VARF_DOCUM_TIPO_01;
  if VarName = 'VARF_DOCUM_DOCU_01' then Value := VARF_DOCUM_DOCU_01;
  if VarName = 'VARF_DOCUM_SRNR_01' then Value := VARF_DOCUM_SRNR_01;
  if VarName = 'VARF_DOCUM_TIPO_02' then Value := VARF_DOCUM_TIPO_02;
  if VarName = 'VARF_DOCUM_DOCU_02' then Value := VARF_DOCUM_DOCU_02;
  if VarName = 'VARF_DOCUM_SRNR_02' then Value := VARF_DOCUM_SRNR_02;
  if VarName = 'VARF_DOCUM_TIPO_03' then Value := VARF_DOCUM_TIPO_03;
  if VarName = 'VARF_DOCUM_DOCU_03' then Value := VARF_DOCUM_DOCU_03;
  if VarName = 'VARF_DOCUM_SRNR_03' then Value := VARF_DOCUM_SRNR_03;
  if VarName = 'VARF_DOCUM_TIPO_04' then Value := VARF_DOCUM_TIPO_04;
  if VarName = 'VARF_DOCUM_DOCU_04' then Value := VARF_DOCUM_DOCU_04;
  if VarName = 'VARF_DOCUM_SRNR_04' then Value := VARF_DOCUM_SRNR_04;
  if VarName = 'VARF_DOCUM_TIPO_05' then Value := VARF_DOCUM_TIPO_05;
  if VarName = 'VARF_DOCUM_DOCU_05' then Value := VARF_DOCUM_DOCU_05;
  if VarName = 'VARF_DOCUM_SRNR_05' then Value := VARF_DOCUM_SRNR_05;
  if VarName = 'VARF_DOCUM_TIPO_06' then Value := VARF_DOCUM_TIPO_06;
  if VarName = 'VARF_DOCUM_DOCU_06' then Value := VARF_DOCUM_DOCU_06;
  if VarName = 'VARF_DOCUM_SRNR_06' then Value := VARF_DOCUM_SRNR_06;
  if VarName = 'VARF_DOCUM_TIPO_07' then Value := VARF_DOCUM_TIPO_07;
  if VarName = 'VARF_DOCUM_DOCU_07' then Value := VARF_DOCUM_DOCU_07;
  if VarName = 'VARF_DOCUM_SRNR_07' then Value := VARF_DOCUM_SRNR_07;
  if VarName = 'VARF_DOCUM_TIPO_08' then Value := VARF_DOCUM_TIPO_08;
  if VarName = 'VARF_DOCUM_DOCU_08' then Value := VARF_DOCUM_DOCU_08;
  if VarName = 'VARF_DOCUM_SRNR_08' then Value := VARF_DOCUM_SRNR_08;
  if VarName = 'VARF_DOCUM_TIPO_09' then Value := VARF_DOCUM_TIPO_09;
  if VarName = 'VARF_DOCUM_DOCU_09' then Value := VARF_DOCUM_DOCU_09;
  if VarName = 'VARF_DOCUM_SRNR_09' then Value := VARF_DOCUM_SRNR_09;
  if VarName = 'VARF_DOCUM_TIPO_10' then Value := VARF_DOCUM_TIPO_10;
  if VarName = 'VARF_DOCUM_DOCU_10' then Value := VARF_DOCUM_DOCU_10;
  if VarName = 'VARF_DOCUM_SRNR_10' then Value := VARF_DOCUM_SRNR_10;
  if VarName = 'VARF_DOCUM_TIPO_11' then Value := VARF_DOCUM_TIPO_11;
  if VarName = 'VARF_DOCUM_DOCU_11' then Value := VARF_DOCUM_DOCU_11;
  if VarName = 'VARF_DOCUM_SRNR_11' then Value := VARF_DOCUM_SRNR_11;
  if VarName = 'VARF_DOCUM_TIPO_12' then Value := VARF_DOCUM_TIPO_12;
  if VarName = 'VARF_DOCUM_DOCU_12' then Value := VARF_DOCUM_DOCU_12;
  if VarName = 'VARF_DOCUM_SRNR_12' then Value := VARF_DOCUM_SRNR_12;
  if VarName = 'VARF_DOCUM_TIPO_13' then Value := VARF_DOCUM_TIPO_13;
  if VarName = 'VARF_DOCUM_DOCU_13' then Value := VARF_DOCUM_DOCU_13;
  if VarName = 'VARF_DOCUM_SRNR_13' then Value := VARF_DOCUM_SRNR_13;
  if VarName = 'VARF_DOCUM_TIPO_14' then Value := VARF_DOCUM_TIPO_14;
  if VarName = 'VARF_DOCUM_DOCU_14' then Value := VARF_DOCUM_DOCU_14;
  if VarName = 'VARF_DOCUM_SRNR_14' then Value := VARF_DOCUM_SRNR_14;
  if VarName = 'VARF_DOCUM_TIPO_15' then Value := VARF_DOCUM_TIPO_15;
  if VarName = 'VARF_DOCUM_DOCU_15' then Value := VARF_DOCUM_DOCU_15;
  if VarName = 'VARF_DOCUM_SRNR_15' then Value := VARF_DOCUM_SRNR_15;
  if VarName = 'VARF_DOCUM_TIPO_16' then Value := VARF_DOCUM_TIPO_16;
  if VarName = 'VARF_DOCUM_DOCU_16' then Value := VARF_DOCUM_DOCU_16;
  if VarName = 'VARF_DOCUM_SRNR_16' then Value := VARF_DOCUM_SRNR_16;
  if VarName = 'VARF_DOCUM_TIPO_17' then Value := VARF_DOCUM_TIPO_17;
  if VarName = 'VARF_DOCUM_DOCU_17' then Value := VARF_DOCUM_DOCU_17;
  if VarName = 'VARF_DOCUM_SRNR_17' then Value := VARF_DOCUM_SRNR_17;
  if VarName = 'VARF_DOCUM_TIPO_18' then Value := VARF_DOCUM_TIPO_18;
  if VarName = 'VARF_DOCUM_DOCU_18' then Value := VARF_DOCUM_DOCU_18;
  if VarName = 'VARF_DOCUM_SRNR_18' then Value := VARF_DOCUM_SRNR_18;
  if VarName = 'VARF_DOCUM_TIPO_19' then Value := VARF_DOCUM_TIPO_19;
  if VarName = 'VARF_DOCUM_DOCU_19' then Value := VARF_DOCUM_DOCU_19;
  if VarName = 'VARF_DOCUM_SRNR_19' then Value := VARF_DOCUM_SRNR_19;
  if VarName = 'VARF_DOCUM_TIPO_20' then Value := VARF_DOCUM_TIPO_20;
  if VarName = 'VARF_DOCUM_DOCU_20' then Value := VARF_DOCUM_DOCU_20;
  if VarName = 'VARF_DOCUM_SRNR_20' then Value := VARF_DOCUM_SRNR_20;
  //
  if VarName = 'VARF_RODOCAD_RNTRC' then Value := QrRodoCabRNTRC.Value;
  if VarName = 'VARF_RODOCAD_CIOT'  then Value := QrRodoCabCIOT.Value;
  if VarName = 'VARF_RODOCAD_LOTA'  then Value := sMDFeRodoLota[QrRodoCablota.Value];
  if VarName = 'VARF_RODOCAD_DTPRV' then Value := Geral.FDT(QrRodoCabdPrev.Value, 2);
  //
  if VarName = 'VARF_TPVEIC_TIPO_01' then Value := VARF_TPVEIC_TIPO_01;
  if VarName = 'VARF_TPVEIC_PLACA_01' then Value := VARF_TPVEIC_PLACA_01;
  if VarName = 'VARF_TPVEIC_UF_01' then Value := VARF_TPVEIC_UF_01;
  if VarName = 'VARF_TPVEIC_RNTRC_01' then Value := VARF_TPVEIC_RNTRC_01;
  if VarName = 'VARF_TPVEIC_TIPO_02' then Value := VARF_TPVEIC_TIPO_02;
  if VarName = 'VARF_TPVEIC_PLACA_02' then Value := VARF_TPVEIC_PLACA_02;
  if VarName = 'VARF_TPVEIC_UF_02' then Value := VARF_TPVEIC_UF_02;
  if VarName = 'VARF_TPVEIC_RNTRC_02' then Value := VARF_TPVEIC_RNTRC_02;
  if VarName = 'VARF_TPVEIC_TIPO_03' then Value := VARF_TPVEIC_TIPO_03;
  if VarName = 'VARF_TPVEIC_PLACA_03' then Value := VARF_TPVEIC_PLACA_03;
  if VarName = 'VARF_TPVEIC_UF_03' then Value := VARF_TPVEIC_UF_03;
  if VarName = 'VARF_TPVEIC_RNTRC_03' then Value := VARF_TPVEIC_RNTRC_03;
  if VarName = 'VARF_TPVEIC_TIPO_04' then Value := VARF_TPVEIC_TIPO_04;
  if VarName = 'VARF_TPVEIC_PLACA_04' then Value := VARF_TPVEIC_PLACA_04;
  if VarName = 'VARF_TPVEIC_UF_04' then Value := VARF_TPVEIC_UF_04;
  if VarName = 'VARF_TPVEIC_RNTRC_04' then Value := VARF_TPVEIC_RNTRC_04;
  //
  if VarName = 'VARF_VPEDAG_CNPJR_01' then Value := VARF_VPEDAG_CNPJR_01;
  if VarName = 'VARF_VPEDAG_CNPJF_01' then Value := VARF_VPEDAG_CNPJF_01;
  if VarName = 'VARF_VPEDAG_NCOMP_01' then Value := VARF_VPEDAG_NCOMP_01;
  if VarName = 'VARF_VPEDAG_CNPJR_02' then Value := VARF_VPEDAG_CNPJR_02;
  if VarName = 'VARF_VPEDAG_CNPJF_02' then Value := VARF_VPEDAG_CNPJF_02;
  if VarName = 'VARF_VPEDAG_NCOMP_02' then Value := VARF_VPEDAG_NCOMP_02;
  if VarName = 'VARF_VPEDAG_CNPJR_03' then Value := VARF_VPEDAG_CNPJR_03;
  if VarName = 'VARF_VPEDAG_CNPJF_03' then Value := VARF_VPEDAG_CNPJF_03;
  if VarName = 'VARF_VPEDAG_NCOMP_03' then Value := VARF_VPEDAG_NCOMP_03;
  if VarName = 'VARF_VPEDAG_CNPJR_04' then Value := VARF_VPEDAG_CNPJR_04;
  if VarName = 'VARF_VPEDAG_CNPJF_04' then Value := VARF_VPEDAG_CNPJF_04;
  if VarName = 'VARF_VPEDAG_NCOMP_04' then Value := VARF_VPEDAG_NCOMP_04;
  //
  if VarName = 'VARF_MOTO_NOME_01' then Value := VARF_MOTO_NOME_01;
  if VarName = 'VARF_MOTO_NOME_02' then Value := VARF_MOTO_NOME_02;
  if VarName = 'VARF_MOTO_CPF_01' then Value := VARF_MOTO_CPF_01;
  if VarName = 'VARF_MOTO_CPF_02' then Value := VARF_MOTO_CPF_02;
  //
  if VarName = 'VARF_LACRODO_LACRES' then Value := VARF_LACRODO_LACRES;
  //
  if VarName = 'VARF_OBS_CONT' then Value := VARF_OBS_CONT;
  if VarName = 'VARF_OBS_FISCO' then Value := VARF_OBS_FISCO;
  //
}
end;

procedure TFmMDFe_Pesq_0000.frxListaMFFesGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := CBFilial.Text
  else
  if VarName = 'VARF_TERCEIRO' then
    Value := dmkPF.ParValueCodTxt('', CBCliente.Text, EdCliente.ValueVariant)
  else
  if VarName = 'VARF_PERIODO' then
    Value := dmkPF.PeriodoImp2(
    TPDataI.Date, TPDataF.Date, True, True, '', '', '')
  else
  if VarName = 'VARF_STATUS' then
  begin
    if Ck100e101.Checked then
      Value := 'AUTORIZADAS E CANCELADAS'
    else
      Value := 'QUALQUER';
  end
  else
  if VarName = 'VARF_APENAS_EMP' then
  begin
    case RGQuemEmit.ItemIndex of
      0: Value := 'Notas Fiscais Emitidas';
      1: Value := 'Notas Fiscais Recebidas';
      2: Value := 'Notas Fiscais Emitidas e Recebidas';
      else Value := 'Notas Fiscais ????';
    end;
  end
  else
  if VarName = 'VARF_AMBIENTE' then
  begin
    case RGAmbiente.ItemIndex of
      0: Value := 'AMBOS';
      1: Value := 'PRODU��O';
      2: Value := 'HOMOLOGA��O';
    end;
  end
end;

procedure TFmMDFe_Pesq_0000.ImprimeListaNova(PorCFOP, Exporta: Boolean);
begin
{ *****
  MDFe_PF.MostraFormMDFe_Pesq_0000_ImpLista(PorCFOP,
  EdFilial.ValueVariant, TPDataI.Date, TPDataF.Date, RGQuemEmit.ItemIndex,
  RGAmbiente.ItemIndex, EdCliente.ValueVariant, QrClientesTipo.Value,
  RGOrdem1.ItemIndex, RGOrdem2.ItemIndex, Ck100e101.Checked,
  QrClientesCNPJ.Value, QrClientesCPF.Value, CGcSitEnc, CkideNatOp.Checked,
  CBFilial.Text, CBCliente.Text, Exporta);
}
end;

procedure TFmMDFe_Pesq_0000.ImprimeMDFe(Preview: Boolean);
var
  I: Integer;
  Report: TfrxReport;
  Memo: TfrxMemoView;
begin
  //Report := frxA4A_002;
    Report := DefineQual_frxMDFe(ficNone);
  try
    for I := 0 to Report.ComponentCount - 1 do
    begin
      if Report.Components[I] is TfrxMemoView then
      begin
        Memo := TfrxMemoView(Report.Components[I]);
        case Memo.Tag of
          0: Memo.Visible := not Preview;
          1: ; // Deixa como est�
          2: Memo.Visible := Preview;
        end;
      end
      else if (Report.Components[I] is TfrxLineView) and (Preview) then
        TfrxLineView(Report.Components[I]).Visible := False
      else if (Report.Components[I] is TfrxBarCodeView) and (Preview) then
        TfrxBarCodeView(Report.Components[I]).Visible := False
    end;
    DefineFrx(Report, ficMostra);
  finally
    //Report.Free;
  end;
end;

procedure TFmMDFe_Pesq_0000.ImprimeSomenteadmin1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  BtImprimeClick(Self);
end;

procedure TFmMDFe_Pesq_0000.Listadasnotaspesquisadas1Click(Sender: TObject);
var
  ide_nNF, ide_serie, cStat, Ordem, Status: Integer;
  ide_dEmi: String;
  ICMSTot_vProd, ICMSTot_vST, ICMSTot_vFrete,
  ICMSTot_vSeg, ICMSTot_vIPI, ICMSTot_vOutro,
  ICMSTot_vDesc, ICMSTot_vNF, ICMSTot_vBC,
  ICMSTot_vICMS, ICMSTot_vPIS, ICMSTot_vCOFINS: Double;
  NOME_tpEmis, NOME_tpNF, Motivo,
  ide_AAMM_AA, ide_AAMM_MM, Id, infCanc_dhRecbto, infCanc_nProt, Ordem100: String;
  //
  esp, marca, nVol: String;
  qVol, PesoL, PesoB: Double;
begin
{ TODO : Incluir Notas inutilizadas }
{ *****
  Screen.Cursor := crHourGlass;
  try
    F_MDFe_100 := GradeCriar.RecriaTempTableNovo(ntrttMDFe_100, DmodG.QrUpdPID1, False);
    F_MDFe_101 := GradeCriar.RecriaTempTableNovo(ntrttMDFe_101, DmodG.QrUpdPID1, False);
    F_MDFe_XXX := GradeCriar.RecriaTempTableNovo(ntrttMDFe_XXX, DmodG.QrUpdPID1, False);
    //
    QrMDFeCabA.First;
    while not QrMDFeCabA.Eof do
    begin
      // 0..999
      cStat            := Trunc(QrMDFeCabAcStat.Value);
      ide_nNF          := QrMDFeCabAide_nNF.Value;
      ide_serie        := QrMDFeCabAide_serie.Value;
      ide_dEmi         := Geral.FDT(QrMDFeCabAide_dEmi.Value, 1);
      // 100
      ICMSTot_vProd    := QrMDFeCabAICMSTot_vProd.Value;
      ICMSTot_vST      := QrMDFeCabAICMSTot_vST.Value;
      ICMSTot_vFrete   := QrMDFeCabAICMSTot_vFrete.Value;
      ICMSTot_vSeg     := QrMDFeCabAICMSTot_vSeg.Value;
      ICMSTot_vIPI     := QrMDFeCabAICMSTot_vIPI.Value;
      ICMSTot_vOutro   := QrMDFeCabAICMSTot_vOutro.Value;
      ICMSTot_vDesc    := QrMDFeCabAICMSTot_vDesc.Value;
      ICMSTot_vNF      := QrMDFeCabAICMSTot_vNF.Value;
      ICMSTot_vBC      := QrMDFeCabAICMSTot_vBC.Value;
      ICMSTot_vICMS    := QrMDFeCabAICMSTot_vICMS.Value;
      ICMSTot_vPIS     := QrMDFeCabAICMSTot_vPIS.Value;
      ICMSTot_vCOFINS  := QrMDFeCabAICMSTot_vCOFINS.Value;
      NOME_tpEmis      := QrMDFeCabANOME_tpEmis.Value;
      NOME_tpNF        := QrMDFeCabANOME_tpNF.Value;
      ide_natOp        := QrMDFeCabAide_natOp.Value;
      // 101
      ide_AAMM_AA      := Geral.FDT(QrMDFeCabAide_dEmi.Value, 21);
      ide_AAMM_MM      := Geral.FDT(QrMDFeCabAide_dEmi.Value, 22);
      Motivo           := QrMDFeCabAinfCanc_xJust.Value;
      Id               := QrMDFeCabAId.Value;
      infCanc_dhRecbto := Geral.FDT(QrMDFeCabAinfCanc_dhRecbto.Value, 9);
      infCanc_nProt    := QrMDFeCabAinfCanc_nProt.Value;
      // Outros
      Status           := QrMDFeCabAStatus.Value;
      //
      // 2015-10-17
      UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeCabXVol, Dmod.MyDB, [
      'SELECT SUM(qVol) qVol, esp, marca,  ',
      'nVol, SUM(PesoL) PesoL, SUM(PesoB) PesoB  ',
      'FROM mdfecabxvol ',
      'WHERE FatID=' + Geral.FF0(QrMDFeCabAFatID.Value),
      'AND FatNum=' + Geral.FF0(QrMDFeCabAFatNum.Value),
      'AND Empresa=' + Geral.FF0(QrMDFeCabAEmpresa.Value),
      '']);
      qVol             := QrMDFeCabXVolqVol.Value;
      esp              := QrMDFeCabXVolesp.Value;
      marca            := QrMDFeCabXVolmarca.Value;
      nVol             := QrMDFeCabXVolnVol.Value;
      PesoL            := QrMDFeCabXVolPesoL.Value;
      PesoB            := QrMDFeCabXVolPesoB.Value;
      // Fim 2015-10-17
      case cStat of
        100:
        begin
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, F_MDFe_100, True, [
          'Ordem',
          'ide_nNF', 'ide_serie', 'ide_dEmi',
          'ICMSTot_vProd', 'ICMSTot_vST', 'ICMSTot_vFrete',
          'ICMSTot_vSeg', 'ICMSTot_vIPI', 'ICMSTot_vOutro',
          'ICMSTot_vDesc', 'ICMSTot_vNF', 'ICMSTot_vBC',
          'ICMSTot_vICMS', 'ICMSTot_vPIS', 'ICMSTot_vCOFINS',
          'NOME_tpEmis', 'NOME_tpNF', 'ide_natOp',
          'qVol', 'esp', 'marca',
          'nVol', 'PesoL', 'PesoB'], [
          ], [
          0,
          ide_nNF, ide_serie, ide_dEmi,
          ICMSTot_vProd, ICMSTot_vST, ICMSTot_vFrete,
          ICMSTot_vSeg, ICMSTot_vIPI, ICMSTot_vOutro,
          ICMSTot_vDesc, ICMSTot_vNF, ICMSTot_vBC,
          ICMSTot_vICMS, ICMSTot_vPIS, ICMSTot_vCOFINS,
          NOME_tpEmis, NOME_tpNF, ide_natOp,
          qVol, esp, marca,
          nVol, PesoL, PesoB
          ], [
          ], False);
        end;
        101:
        begin
          Ordem := 0;
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, F_MDFe_101, False, [
          'Ordem', 'ide_nNF',
          'ide_serie', 'ide_AAMM_AA', 'ide_AAMM_MM',
          'ide_dEmi', 'NOME_tpEmis', 'NOME_tpNF',
          'Id', 'infCanc_dhRecbto', 'infCanc_nProt', 'Motivo'], [
          ], [
          Ordem, ide_nNF,
          ide_serie, ide_AAMM_AA, ide_AAMM_MM,
          ide_dEmi, NOME_tpEmis, NOME_tpNF,
          Id, infCanc_dhRecbto, infCanc_nProt, Motivo], [
          ], False);
        end;
        else begin
          if Status < 100 then
            Ordem := 2
          else
            Ordem := 1;
          Motivo := MDFe_PF.Texto_StatusMDFe(Status, QrMDFeCabAversao.Value);
          //
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, F_MDFe_XXX, False, [
          'Ordem', 'cStat', 'ide_nNF',
          'ide_serie', 'ide_AAMM_AA', 'ide_AAMM_MM',
          'ide_dEmi', 'NOME_tpEmis', 'NOME_tpNF',
          'Status', 'Motivo'], [
          ], [
          Ordem, cStat, ide_nNF,
          ide_serie, ide_AAMM_AA, ide_AAMM_MM,
          ide_dEmi, NOME_tpEmis, NOME_tpNF,
          Status, Motivo], [
          ], False);
        end;
      end;
      //
      QrMDFeCabA.Next;
    end;
    //
    if CkideNatOp.Checked then
      Ordem100 := 'ORDER BY ide_NatOp, ide_nNF'
    else
      Ordem100 := 'ORDER BY ide_nNF';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrMDFe_100, DModG.MyPID_DB, [
    'SELECT * FROM mdfe_100 ',
    Ordem100,
    '']);
    //
    QrMDFe_101.Close;
    QrMDFe_101.Database := DModG.MyPID_DB;
    QrMDFe_101.Open;
    //
    QrMDFe_XXX.Database := DModG.MyPID_DB;
    if CkideNatOp.Checked then
      MyObjects.frxMostra(frxListaMDFesB, 'Lista de MDF-e(s)')
    else
      MyObjects.frxMostra(frxListaMDFes, 'Lista de MDF-e(s)');
  finally
    Screen.Cursor := crDefault;
  end;
}
end;

procedure TFmMDFe_Pesq_0000.MDFe1Click(Sender: TObject);
begin
  Geral.MensagemBox(QrArqXML_MDFe.Value, 'XML MDFe', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmMDFe_Pesq_0000.ObtemdadosInfDoc(var Tipo, Docu, SrNr: String);
begin
{ *****
  case TMDFeMyTpDocInf(Qr2InfDocMyTpDocInf.Value) of
    mdfemtdiNFe:
    begin
      Tipo := 'NFe';
      Docu := Qr2InfDocInfNFe_chave.Value;
      SrNr := '';
    end;
    mdfemtdiNFsOld:
    begin
      Tipo := Geral.FF0(Qr2InfDocInfNF_mod.Value);
      Docu := QrAREMET_CNPJ_CPF_TXT.Value;
      SrNr := Qr2InfDocInfNF_serie.Value + ' / ' + Qr2InfDocInfNF_nDoc.Value;
    end;
    mdfemtdiOutrosDoc:
    begin
      Tipo := Geral.FF0(Qr2InfDocInfOutros_tpDoc.Value);
      Docu := Qr2InfDocInfOutros_descOutros.Value;
      SrNr := Qr2InfDocInfOutros_nDoc.Value;
    end;
    else
    begin
      Tipo := '';
      Docu := '';
      SrNr := '';
    end;
  end;
}
end;

procedure TFmMDFe_Pesq_0000.PMArqPopup(Sender: TObject);
begin
  MDFe1         .Enabled := QrArqXML_MDFe.Value <> '';
  Autorizao1   .Enabled := QrArqXML_Aut.Value <> '';
  Cancelamento2.Enabled := QrArqXML_Can.Value <> '';
end;

procedure TFmMDFe_Pesq_0000.PMEventosPopup(Sender: TObject);
begin
{ *****
  CorrigeManifestao1.Enabled :=
    (QrMDFeCabA.State <> DsInactive) and (QrMDFeCabA.RecordCount > 0) and (
      (QrMDFeCabAcSitEnc.Value = MDFe_CodEventoSitConfNaoConsultada) or
      (QrMDFeCabAcSitEnc.Value = MDFe_CodEventoSitConfMDeSemManifestacao)
    );
}
end;

procedure TFmMDFe_Pesq_0000.porCFOP1Click(Sender: TObject);
begin
  ImprimeListaNova(True, False);
end;

procedure TFmMDFe_Pesq_0000.porNatOp1Click(Sender: TObject);
begin
  ImprimeListaNova(False, False);
end;

procedure TFmMDFe_Pesq_0000.PreviewdaMDFe1Click(Sender: TObject);
begin
  ImprimeMDFe(True);
end;

procedure TFmMDFe_Pesq_0000.QrACalcFields(DataSet: TDataSet);
begin
  QrAId_TXT.Value := XXe_PF.FormataID_XXe(QrAId.Value);
  //
  QrAEMIT_ENDERECO.Value := QrAemit_xLgr.Value + ', ' + Geral.FormataNumeroDeRua(
    QrAemit_xLgr.Value,
    QrAemit_nro.Value, True) + ' ' + QrAemit_xCpl.Value + sLineBreak +
    QrAemit_xBairro.Value + sLineBreak + QrAemit_xMun.Value + ' - ' +
    QrAemit_UF.Value + sLineBreak +  'CEP: ' + Geral.FormataCEP_TT(IntToStr(
    QrAemit_CEP.Value))(* + '  ' + QrAemit_xPais.Value*);
  QrAEMIT_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrAemit_fone.Value);
  //
  QrAEMIT_IE_TXT.Value := Geral.Formata_IE(QrAemit_IE.Value, QrAemit_UF.Value, '??');
  //if DModG.QrParamsEmpMDFeShowURL.Value <> ''  then    /
  if QrOpcoesMDFeMDFeShowURL.Value <> ''  then
    QrAEMIT_ENDERECO.Value := QrAEMIT_ENDERECO.Value + sLineBreak +
      QrOpcoesMDFeMDFeShowURL.Value;
  QrAEMIT_CNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrAemit_CNPJ.Value);
  //
  if QrAide_tpAmb.Value = 2 then
  begin
    QrADOC_SEM_VLR_JUR.Value :=
      'MDF-e emitida em ambiente de homologa��o. N�O POSSUI VALIDADE JUR�DICA';
    QrADOC_SEM_VLR_FIS.Value := 'SEM VALOR FISCAL';
  end else begin
    QrADOC_SEM_VLR_JUR.Value := '';
    QrADOC_SEM_VLR_FIS.Value := QrAinfProt_nProt.Value + ' ' + Geral.FDT(QrAinfProt_dhRecbto.Value, 00);
  end;
{*****
  //
  //
    //
    ////// REMETENTE
    //
  //
  if QrACodInfoRemet.Value <> 0 then
  begin
    QrAREMET_NOME.Value := QrArem_xNome.Value;
    if QrARem_CNPJ.Value <> '' then
      QrAREMET_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrARem_CNPJ.Value)
    else
      QrAREMET_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrARem_CPF.Value);
    QrAREMET_ENDERECO.Value := QrARem_xLgr.Value + ', ' + Geral.FormataNumeroDeRua(
      QrARem_xLgr.Value, QrARem_nro.Value, True) + ' ' + QrARem_xCpl.Value;
    //if DModG.QrParamsEmpMDFeMaiusc.Value = 1  then
    if QrOpcoesMDFeMDFeMaiusc.Value = 1 then
    begin
      QrAREMET_ENDERECO.Value := AnsiUpperCase(QrAREMET_ENDERECO.Value);
      QrAREMET_XMUN_TXT.Value := AnsiUpperCase(QrARem_xMun.Value);
      QrAREMET_XPAIS_TXT.Value := AnsiUpperCase(QrARem_xPais.Value);
    end else
    begin
      QrAREMET_XMUN_TXT.Value := QrARem_xMun.Value;
      QrAREMET_XPAIS_TXT.Value := QrARem_xPais.Value;
    end;
    QrAREMET_CEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(QrARem_CEP.Value));
    QrAREMET_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrARem_fone.Value);
    QrAREMET_IE_TXT.Value := Geral.Formata_IE(QrARem_IE.Value, QrARem_UF.Value, '??');
    //
    //
  end else
  begin
    QrAREMET_NOME.Value         := '';
    QrAREMET_CNPJ_CPF_TXT.Value := '';
    QrAREMET_ENDERECO.Value     := '';
    QrAREMET_CEP_TXT.Value      := '';
    QrAREMET_FONE_TXT.Value     := '';
    QrAREMET_IE_TXT.Value       := '';
    QrAREMET_XMUN_TXT.Value     := '';
    QrAREMET_XPAIS_TXT.Value    := '';
  end;
  //
    //
    ////// DESTINAT�RIO
    //
  //
    QrADEST_NOME.Value := QrAdest_xNome.Value;
  if QrAdest_CNPJ.Value <> '' then
    QrADEST_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAdest_CNPJ.Value)
  else
    QrADEST_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAdest_CPF.Value);
  QrADEST_ENDERECO.Value := QrAdest_xLgr.Value + ', ' + Geral.FormataNumeroDeRua(
    QrAdest_xLgr.Value, QrAdest_nro.Value, True) + ' ' + QrAdest_xCpl.Value;
  //if DModG.QrParamsEmpMDFeMaiusc.Value = 1  then  /
  if QrOpcoesMDFeMDFeMaiusc.Value = 1  then
  begin
    QrADEST_ENDERECO.Value := AnsiUpperCase(QrADEST_ENDERECO.Value);
    QrADEST_XMUN_TXT.Value := AnsiUpperCase(QrAdest_xMun.Value);
    QrADEST_XPAIS_TXT.Value := AnsiUpperCase(QrADest_xPais.Value);
  end else
  begin
    QrADEST_XMUN_TXT.Value := QrAdest_xMun.Value;
    QrADEST_XPAIS_TXT.Value := QrADest_xPais.Value;
  end;
  QrADEST_CEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(QrAdest_CEP.Value));
  QrADEST_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrAdest_fone.Value);
  //
  QrADEST_IE_TXT.Value := Geral.Formata_IE(QrAdest_IE.Value, QrAdest_UF.Value, '??');
  //
  //
    //
    ////// EXPEDIDOR
    //
  //
  if QrACodInfoExped.Value <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qr1Exped, Dmod.MyDB, [
    'SELECT * ',
    'FROM mdfecab1exped ',
    'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
    'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
    'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
    '']);
    QrAEXPED_NOME.Value := Qr1ExpedxNome.Value;
    if Qr1ExpedCNPJ.Value <> '' then
      QrAEXPED_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(Qr1ExpedCNPJ.Value)
    else
      QrAEXPED_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(Qr1ExpedCPF.Value);
    QrAEXPED_ENDERECO.Value := Qr1ExpedxLgr.Value + ', ' + Geral.FormataNumeroDeRua(
      Qr1ExpedxLgr.Value, Qr1Expednro.Value, True) + ' ' + Qr1ExpedxCpl.Value;
    //if DModG.QrParamsEmpMDFeMaiusc.Value = 1  then
    if QrOpcoesMDFeMDFeMaiusc.Value = 1  then
    begin
      QrAEXPED_ENDERECO.Value := AnsiUpperCase(QrAEXPED_ENDERECO.Value);
      QrAEXPED_XMUN_TXT.Value := AnsiUpperCase(Qr1ExpedxMun.Value);
      QrAEXPED_XPAIS_TXT.Value := AnsiUpperCase(Qr1ExpedxPais.Value);
    end else
    begin
      QrAEXPED_XMUN_TXT.Value := Qr1ExpedxMun.Value;
      QrAEXPED_XPAIS_TXT.Value := Qr1ExpedxPais.Value;
    end;
    QrAEXPED_CEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(Qr1ExpedCEP.Value));
    QrAEXPED_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(Qr1Expedfone.Value);
    QrAEXPED_IE_TXT.Value := Geral.Formata_IE(Qr1ExpedIE.Value, Qr1ExpedUF.Value, '??');
    QrAEXPED_UF.Value := Qr1ExpedUF.Value;
    //
    //
  end else
  begin
    QrAEXPED_NOME.Value         := '';
    QrAEXPED_CNPJ_CPF_TXT.Value := '';
    QrAEXPED_ENDERECO.Value     := '';
    QrAEXPED_CEP_TXT.Value      := '';
    QrAEXPED_FONE_TXT.Value     := '';
    QrAEXPED_IE_TXT.Value       := '';
    QrAEXPED_XMUN_TXT.Value     := '';
    QrAEXPED_XPAIS_TXT.Value    := '';
  end;
  //
  //
    //
    ////// RECEBEDOR
    //
  //
  if QrACodInfoReceb.Value <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qr1Receb, Dmod.MyDB, [
    'SELECT * ',
    'FROM mdfecab1receb ',
    'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
    'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
    'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
    '']);
    QrARECEB_NOME.Value         := Qr1RecebxNome.Value;
    if Qr1RecebCNPJ.Value <> '' then
      QrARECEB_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(Qr1RecebCNPJ.Value)
    else
      QrARECEB_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(Qr1RecebCPF.Value);
    QrARECEB_ENDERECO.Value := Qr1RecebxLgr.Value + ', ' + Geral.FormataNumeroDeRua(
      Qr1RecebxLgr.Value, Qr1Recebnro.Value, True) + ' ' + Qr1RecebxCpl.Value;
    //if DModG.QrParamsEmpMDFeMaiusc.Value = 1  then
    //if DModG.QrParamsEmpMDFeMaiusc.Value = 1  then
    if QrOpcoesMDFeMDFeMaiusc.Value = 1  then
    begin
      QrARECEB_ENDERECO.Value := AnsiUpperCase(QrARECEB_ENDERECO.Value);
      QrARECEB_XMUN_TXT.Value := AnsiUpperCase(Qr1RecebxMun.Value);
      QrARECEB_XPAIS_TXT.Value := AnsiUpperCase(Qr1RecebxPais.Value);
    end else
    begin
      QrARECEB_XMUN_TXT.Value := Qr1RecebxMun.Value;
      QrARECEB_XPAIS_TXT.Value := Qr1RecebxPais.Value;
    end;
    QrARECEB_CEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(Qr1RecebCEP.Value));
    QrARECEB_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(Qr1Recebfone.Value);
    QrARECEB_IE_TXT.Value := Geral.Formata_IE(Qr1RecebIE.Value, Qr1RecebUF.Value, '??');
    QrARECEB_UF.Value := Qr1RecebUF.Value;
    //
    //
  end else
  begin
    QrARECEB_NOME.Value         := '';
    QrARECEB_CNPJ_CPF_TXT.Value := '';
    QrARECEB_ENDERECO.Value     := '';
    QrARECEB_CEP_TXT.Value      := '';
    QrARECEB_FONE_TXT.Value     := '';
    QrARECEB_IE_TXT.Value       := '';
    QrARECEB_XMUN_TXT.Value     := '';
    QrAEXPED_XPAIS_TXT.Value    := '';
  end;
  //
  //
    //
    ////// OUTRO (TOMA4)
    //
  //
  if QrACodInfoToma4.Value <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qr2Toma4, Dmod.MyDB, [
    'SELECT * ',
    'FROM mdfecab2toma04 ',
    'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
    'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
    'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
    '']);
    QrATOMA4_NOME.Value         := QrAtoma04_xNome.Value;
    if QrAtoma04_CNPJ.Value <> '' then
      QrATOMA4_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAtoma04_CNPJ.Value)
    else
      QrATOMA4_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAtoma04_CPF.Value);
    QrATOMA4_ENDERECO.Value := Qr2Toma4enderToma_xLgr.Value + ', ' + Geral.FormataNumeroDeRua(
      Qr2Toma4enderToma_xLgr.Value, Qr2Toma4enderToma_nro.Value, True) + ' ' + Qr2Toma4enderToma_xCpl.Value;
    //if DModG.QrParamsEmpMDFeMaiusc.Value = 1  then
    if QrOpcoesMDFeMDFeMaiusc.Value = 1  then
    begin
      QrATOMA4_ENDERECO.Value := AnsiUpperCase(QrATOMA4_ENDERECO.Value);
      QrATOMA4_XMUN_TXT.Value := AnsiUpperCase(Qr2Toma4enderToma_xMun.Value);
      QrATOMA4_XPAIS_TXT.Value := AnsiUpperCase(Qr2Toma4enderToma_xPais.Value);
    end else
    begin
      QrATOMA4_XMUN_TXT.Value := Qr2Toma4enderToma_xMun.Value;
      QrATOMA4_XPAIS_TXT.Value := Qr2Toma4enderToma_xPais.Value;
    end;
    QrATOMA4_CEP_TXT.Value := Geral.FormataCEP_TT(IntToStr(Qr2Toma4enderToma_CEP.Value));
    QrATOMA4_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrAToma04_fone.Value);
    QrATOMA4_IE_TXT.Value := Geral.Formata_IE(QrAtoma04_IE.Value, Qr2Toma4enderToma_UF.Value, '??');
    QrATOMA4_UF.Value := Qr2Toma4enderToma_UF.Value;
    //
    //
  end else
  begin
    QrATOMA4_NOME.Value         := '';
    QrATOMA4_CNPJ_CPF_TXT.Value := '';
    QrATOMA4_ENDERECO.Value     := '';
    QrATOMA4_CEP_TXT.Value      := '';
    QrATOMA4_FONE_TXT.Value     := '';
    QrATOMA4_IE_TXT.Value       := '';
    QrATOMA4_XMUN_TXT.Value     := '';
    QrATOMA4_XPAIS_TXT.Value    := '';
  end;
    //
    //
  //
    //
    ////// TOMADOR
    //
  //
  case TMDFeToma(QrAtoma99_toma.Value) of
    mdfetomaRemetente:
    begin
      QrATOMAD_NOME.Value         := QrAREMET_NOME.Value;
      QrATOMAD_CNPJ_CPF_TXT.Value := QrAREMET_CNPJ_CPF_TXT.Value;
      QrATOMAD_ENDERECO.Value     := QrAREMET_ENDERECO.Value    ;
      QrATOMAD_CEP_TXT.Value      := QrAREMET_CEP_TXT.Value     ;
      QrATOMAD_FONE_TXT.Value     := QrAREMET_FONE_TXT.Value    ;
      QrATOMAD_IE_TXT.Value       := QrAREMET_IE_TXT.Value      ;
      QrATOMAD_XMUN_TXT.Value     := QrAREMET_XMUN_TXT.Value    ;
      QrATOMAD_XPAIS_TXT.Value    := QrAREMET_XPAIS_TXT.Value   ;
      QrATOMAD_UF.Value           := QrArem_UF.Value;
    end;
    mdfetomaExpedidor:
    begin
      QrATOMAD_NOME.Value         := QrAEXPED_NOME.Value;
      QrATOMAD_CNPJ_CPF_TXT.Value := QrAEXPED_CNPJ_CPF_TXT.Value;
      QrATOMAD_ENDERECO.Value     := QrAEXPED_ENDERECO.Value    ;
      QrATOMAD_CEP_TXT.Value      := QrAEXPED_CEP_TXT.Value     ;
      QrATOMAD_FONE_TXT.Value     := QrAEXPED_FONE_TXT.Value    ;
      QrATOMAD_IE_TXT.Value       := QrAEXPED_IE_TXT.Value      ;
      QrATOMAD_XMUN_TXT.Value     := QrAEXPED_XMUN_TXT.Value    ;
      QrATOMAD_XPAIS_TXT.Value    := QrAEXPED_XPAIS_TXT.Value   ;
      QrATOMAD_UF.Value           := QrAEXPED_UF.Value;
    end;
    mdfetomaRecebedor:
    begin
      QrATOMAD_NOME.Value         := QrARECEB_NOME.Value;
      QrATOMAD_CNPJ_CPF_TXT.Value := QrARECEB_CNPJ_CPF_TXT.Value;
      QrATOMAD_ENDERECO.Value     := QrARECEB_ENDERECO.Value    ;
      QrATOMAD_CEP_TXT.Value      := QrARECEB_CEP_TXT.Value     ;
      QrATOMAD_FONE_TXT.Value     := QrARECEB_FONE_TXT.Value    ;
      QrATOMAD_IE_TXT.Value       := QrARECEB_IE_TXT.Value      ;
      QrATOMAD_XMUN_TXT.Value     := QrARECEB_XMUN_TXT.Value    ;
      QrATOMAD_XPAIS_TXT.Value    := QrARECEB_XPAIS_TXT.Value   ;
      QrATOMAD_UF.Value           := QrARECEB_UF.Value;
    end;
    mdfetomaDestinatario:
    begin
      QrATOMAD_NOME.Value         := QrADEST_NOME.Value;
      QrATOMAD_CNPJ_CPF_TXT.Value := QrADEST_CNPJ_CPF_TXT.Value;
      QrATOMAD_ENDERECO.Value     := QrADEST_ENDERECO.Value    ;
      QrATOMAD_CEP_TXT.Value      := QrADEST_CEP_TXT.Value     ;
      QrATOMAD_FONE_TXT.Value     := QrADEST_FONE_TXT.Value    ;
      QrATOMAD_IE_TXT.Value       := QrADEST_IE_TXT.Value      ;
      QrATOMAD_XMUN_TXT.Value     := QrADEST_XMUN_TXT.Value    ;
      QrATOMAD_XPAIS_TXT.Value    := QrADEST_XPAIS_TXT.Value   ;
      QrATOMAD_UF.Value           := QrAdest_UF.Value;
    end;
    mdfetomaOutros:
    begin
      QrATOMAD_NOME.Value         := QrATOMA4_NOME.Value;
      QrATOMAD_CNPJ_CPF_TXT.Value := QrATOMA4_CNPJ_CPF_TXT.Value;
      QrATOMAD_ENDERECO.Value     := QrATOMA4_ENDERECO.Value    ;
      QrATOMAD_CEP_TXT.Value      := QrATOMA4_CEP_TXT.Value     ;
      QrATOMAD_FONE_TXT.Value     := QrATOMA4_FONE_TXT.Value    ;
      QrATOMAD_IE_TXT.Value       := QrATOMA4_IE_TXT.Value      ;
      QrATOMAD_XMUN_TXT.Value     := QrATOMA4_XMUN_TXT.Value    ;
      QrATOMAD_XPAIS_TXT.Value    := QrATOMA4_XPAIS_TXT.Value   ;
      QrATOMAD_UF.Value           := QrATOMA4_UF.Value;
    end;
  end;
  //
  //
}
end;

procedure TFmMDFe_Pesq_0000.QrClientesBeforeClose(DataSet: TDataSet);
begin
  QrMDFeCabAMsg.Close;
  BtLeArq.Enabled     := False;
  BtImprime.Enabled   := False;
  BtEnvia.Enabled     := False;
  BtEvento.Enabled    := False;
  BtInutiliza.Enabled := False;
  BtExclui.Enabled    := False;
end;

procedure TFmMDFe_Pesq_0000.QrIAfterScroll(DataSet: TDataSet);
begin
{
  QrM.Close;
  QrM.Params[00].AsInteger := QrMDFeCabAFatID.Value;
  QrM.Params[01].AsInteger := QrMDFeCabAFatNum.Value;
  QrM.Params[02].AsInteger := QrMDFeCabAEmpresa.Value;
  QrM.Params[03].AsInteger := QrInItem.Value;
  QrM.Open;
  //
  QrN.Close;
  QrN.Params[00].AsInteger := QrMDFeCabAFatID.Value;
  QrN.Params[01].AsInteger := QrMDFeCabAFatNum.Value;
  QrN.Params[02].AsInteger := QrMDFeCabAEmpresa.Value;
  QrN.Params[03].AsInteger := QrInItem.Value;
  QrN.Open;
  //
  QrO.Close;
  QrO.Params[00].AsInteger := QrMDFeCabAFatID.Value;
  QrO.Params[01].AsInteger := QrMDFeCabAFatNum.Value;
  QrO.Params[02].AsInteger := QrMDFeCabAEmpresa.Value;
  QrO.Params[03].AsInteger := QrInItem.Value;
  QrO.Open;
  //
  QrV.Close;
  QrV.Params[00].AsInteger := QrMDFeCabAFatID.Value;
  QrV.Params[01].AsInteger := QrMDFeCabAFatNum.Value;
  QrV.Params[02].AsInteger := QrMDFeCabAEmpresa.Value;
  QrV.Params[03].AsInteger := QrInItem.Value;
  QrV.Open;
  //
}
end;

procedure TFmMDFe_Pesq_0000.QrMDFeCabAAfterOpen(DataSet: TDataSet);
begin
  PnDados.Visible    := True;
  BtConsulta.Enabled := QrMDFeCabA.RecordCount > 0;
  SbImprime.Enabled  := QrMDFeCabA.RecordCount > 0;
  SbNovo.Enabled     := QrMDFeCabA.RecordCount > 0;
  if QrMDFeCabA.recordCount > 0 then
    if not DmMDFe_0000.ReopenOpcoesMDFe(QrOpcoesMDFe,
      QrMDFeCabAEmpresa.Value, True) then Close;
end;

procedure TFmMDFe_Pesq_0000.QrMDFeCabAAfterScroll(DataSet: TDataSet);
begin
  BtLeArq.Enabled     := True;
  BtImprime.Enabled   := False;
  BtEnvia.Enabled     := False;
  BtEvento.Enabled    := False;
  BtInutiliza.Enabled := False;
  BtExclui.Enabled    := False;
  case Trunc(QrMDFeCabAcStat.Value) of
    0..99, 201..203, 205..999:  //No status 204 Rejei��o: Duplicidade de MDF-e n�o deixar excluir
    begin
      BtExclui.Enabled := True;
    end;
    100, 132:
    begin
      BtImprime.Enabled := True;
      BtEvento.Enabled  := True;
      BtEnvia.Enabled   := True;
    end;
    101:
    begin
      BtEnvia.Enabled   := True;
      BtEvento.Enabled  := True;
    end;
    //100: BtImprime.Enabled := True;
  end;
  ReopenMDFeEveRRet();
  ReopenMDFeCabAMsg();
end;

procedure TFmMDFe_Pesq_0000.QrMDFeCabABeforeClose(DataSet: TDataSet);
begin
  PnDados.Visible    := False;
  BtConsulta.Enabled := False;
  SbImprime.Enabled  := False;
  SbNovo.Enabled     := False;
  QrMDFeCabAMsg.Close;
  QrMDFeEveRRet.Close;
end;

procedure TFmMDFe_Pesq_0000.QrMDFeCabACalcFields(DataSet: TDataSet);
begin
  case QrMDFeCabAide_tpEmis.Value of
    1: QrMDFeCabANome_tpEmis.Value := 'Normal';
    2: QrMDFeCabANome_tpEmis.Value := 'Conting�ncia FS';
    3: QrMDFeCabANome_tpEmis.Value := 'Conting�ncia SCAN';
    4: QrMDFeCabANome_tpEmis.Value := 'Conting�ncia DEPEC';
    5: QrMDFeCabANome_tpEmis.Value := 'Conting�ncia FS-DA';
    else QrMDFeCabANome_tpEmis.Value := '? ? ? ?';
  end;
end;

procedure TFmMDFe_Pesq_0000.QrMDFeEveRRetCalcFields(DataSet: TDataSet);
begin
{ *****
  if QrMDFeEveRRetret_xEvento.Value <> '' then
    QrMDFeEveRRetNO_EVENTO.Value := QrMDFeEveRRetret_xEvento.Value
  else
    QrMDFeEveRRetNO_EVENTO.Value :=
      MDFe_PF.Obtem_DeEvento_Nome(QrMDFeEveRRetret_tpEvento.Value);
}
end;

procedure TFmMDFe_Pesq_0000.QrMDFe_101CalcFields(DataSet: TDataSet);
begin
{ *****
  QrMDFe_101Id_TXT.Value := XXe_PF.FormataID_XXe(QrMDFe_101Id.Value);
}
end;

procedure TFmMDFe_Pesq_0000.QrMDFe_XXXCalcFields(DataSet: TDataSet);
begin
  case QrMDFe_XXXOrdem.Value of
    0: QrMDFe_XXXNOME_ORDEM.Value := 'CANCELADA';
    1: QrMDFe_XXXNOME_ORDEM.Value := 'REJEITADA';
    2: QrMDFe_XXXNOME_ORDEM.Value := 'N�O ENVIADA';
    else QrMDFe_XXXNOME_ORDEM.Value := '? ? ? ? ?';
  end;
end;

procedure TFmMDFe_Pesq_0000.QrXVolAfterOpen(DataSet: TDataSet);
begin
{
  Fesp    := '';
  Fmarca  := '';
  FnVol   := '';
  //
  FpesoL  := 0;
  FpesoB  := 0;
  FqVol   := 0;
  //
  while not QrXVol.Eof do
  begin
    if (QrXVolesp.Value <> '') and (Fesp <> '') then Fesp := Fesp + '/';
    Fesp    := Fesp    + QrXVolesp.Value;
    if (QrXVolmarca.Value <> '') and (Fmarca <> '') then Fmarca := Fmarca + '/';
    Fmarca  := Fmarca  + QrXVolmarca.Value;
    if (QrXVolnVol.Value <> '') and (FnVol <> '') then FnVol := FnVol + '/';
    FnVol   := FnVol   + QrXVolnVol.Value;
    //         //
    FpesoL  := FpesoL  + QrXVolpesoL.Value;
    FpesoB  := FpesoB  + QrXVolpesoB.Value;
    FqVol   := FqVol   + QrXVolqVol.Value;
    //
    QrXVol.Next;
  end;
}
end;

procedure TFmMDFe_Pesq_0000.QrYAfterOpen(DataSet: TDataSet);
  function CorrigeDataVazio(Data: String): String;
  begin
    if Data = '' then
      Result := '0000-00-00'
    else
      Result := Data;
  end;
  function CorrigeNumeroVazio(Numero: String): String;
  begin
    if Numero = '' then
      Result := '0'
    else
      Result := Numero;
  end;
  function ProximoSeq(var nSeq: Integer): String;
  begin
    nSeq := nSeq + 1;
    Result := FormatFloat('0', nSeq);
  end;
const
  Fields = 03; // (nDup, dVencv, Dup) + 1 Seq  = 4 no total
  //MaxRow = 02;
  MaxCol = 14; // 5 grupos de 3 campos > 0..14 = 15 itens
  //
var
  Seq, MaxRow, Linha, Col, Row, ColsInRow: Integer;
  Faturas: array of array of String;
  Seq1, nDup1, dVenc1, vDup1,
  Seq2, nDup2, dVenc2, vDup2,
  Seq3, nDup3, dVenc3, vDup3,
  Seq4, nDup4, dVenc4, vDup4,
  Seq5, nDup5, dVenc5, vDup5: String;
begin
{
  ColsInRow := (MaxCol + 1) div Fields;
  // Aumentar para arredondar para cima
  MaxRow := QrY.RecordCount + ColsInRow - 1;
  // Total de linhas
  MaxRow := (MaxRow div ColsInRow) - 1; // > 0..?

  //
  //  Limpar vari�veis
  SetLength(Faturas, MaxCol + 1, MaxRow + 1);
  for Col := 0 to MaxCol do
    for Row := 0 to MaxRow do
      FFaturas[Col,Row] := '';

//begin
  //

  Col := 0;
  Row := 0;
  QrY.First;
  while not QrY.Eof do
  begin
    Faturas[Col, Row] :=  QrYnDup.Value;
    Col := Col + 1;
    Faturas[Col, Row] :=  Geral.FDT(QrYdVenc.Value, 1);
    Col := Col + 1;
    Faturas[Col, Row] :=  dmkPF.FFP(QrYvDup.Value, 2);
    //
    if Col = MaxCol then
    begin
      Col := 0;
      Row := Row + 1;
    end else
      Col := Col + 1;
    //
    QrY.Next;
  end;

  //

  F N F eYIts := UCriar.RecriaTempTable(' N F e YIts', DmodG.QrUpdPID1, False);

  Seq := 0;
  if MaxRow > -1 then
  begin
    for Row := 0 to MaxRow do
    begin
      Linha  := Row + 1;
      Seq1   := CorrigeNumeroVazio(ProximoSeq(Seq));
      nDup1  := Faturas[00, Row];
      dVenc1 := CorrigeDataVazio(Faturas[01, Row]);
      vDup1  := CorrigeNumeroVazio(Faturas[02, Row]);

      Seq2   := CorrigeNumeroVazio(ProximoSeq(Seq));
      nDup2  := Faturas[03, Row];
      dVenc2 := CorrigeDataVazio(Faturas[04, Row]);
      vDup2  := CorrigeNumeroVazio(Faturas[05, Row]);

      Seq3   := CorrigeNumeroVazio(ProximoSeq(Seq));
      nDup3  := Faturas[06, Row];
      dVenc3 := CorrigeDataVazio(Faturas[07, Row]);
      vDup3  := CorrigeNumeroVazio(Faturas[08, Row]);

      Seq4   := CorrigeNumeroVazio(ProximoSeq(Seq));
      nDup4  := Faturas[09, Row];
      dVenc4 := CorrigeDataVazio(Faturas[10, Row]);
      vDup4  := CorrigeNumeroVazio(Faturas[11, Row]);

      Seq5   := CorrigeNumeroVazio(ProximoSeq(Seq));
      nDup5  := Faturas[12, Row];
      dVenc5 := CorrigeDataVazio(Faturas[13, Row]);
      vDup5  := CorrigeNumeroVazio(Faturas[14, Row]);
      //
      //for Col := 0 to MaxCol do
      //if
      UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'nfeyits', False, [
        'Seq1', 'nDup1', 'dVenc1', 'vDup1',
        'Seq2', 'nDup2', 'dVenc2', 'vDup2',
        'Seq3', 'nDup3', 'dVenc3', 'vDup3',
        'Seq4', 'nDup4', 'dVenc4', 'vDup4',
        'Seq5', 'nDup5', 'dVenc5', 'vDup5',
        'Linha'], [
      ], [
        Seq1, nDup1, dVenc1, vDup1,
        Seq2, nDup2, dVenc2, vDup2,
        Seq3, nDup3, dVenc3, vDup3,
        Seq4, nDup4, dVenc4, vDup4,
        Seq5, nDup5, dVenc5, vDup5,
        Linha], [
      ], False);
    end;
  end else begin
    Seq1    := CorrigeNumeroVazio('');
    nDup1   := '';
    dVenc1  := CorrigeDataVazio('');
    vDup1   := CorrigeNumeroVazio('');
    Seq2    := CorrigeNumeroVazio('');
    nDup2   := '';
    dVenc2  := CorrigeDataVazio('');
    vDup2   := CorrigeNumeroVazio('');
    Seq3    := CorrigeNumeroVazio('');
    nDup3   := '';
    dVenc3  := CorrigeDataVazio('');
    vDup3   := CorrigeNumeroVazio('');
    Seq4    := CorrigeNumeroVazio('');
    nDup4   := '';
    dVenc4  := CorrigeDataVazio('');
    vDup4   := CorrigeNumeroVazio('');
    Seq5    := CorrigeNumeroVazio('');
    nDup5   := '';
    dVenc5  := CorrigeDataVazio('');
    vDup5   := CorrigeNumeroVazio('');
    Linha   := 1;
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'nfeyits', False, [
      'Seq1', 'nDup1', 'dVenc1', 'vDup1',
      'Seq2', 'nDup2', 'dVenc2', 'vDup2',
      'Seq3', 'nDup3', 'dVenc3', 'vDup3',
      'Seq4', 'nDup4', 'dVenc4', 'vDup4',
      'Seq5', 'nDup5', 'dVenc5', 'vDup5',
      'Linha'], [
    ], [
      Seq1, nDup1, dVenc1, vDup1,
      Seq2, nDup2, dVenc2, vDup2,
      Seq3, nDup3, dVenc3, vDup3,
      Seq4, nDup4, dVenc4, vDup4,
      Seq5, nDup5, dVenc5, vDup5,
      Linha], [
    ], False);
  end;
  QrMDFeYIts.Close;
  QrMDFeYIts.Open;
}
end;

procedure TFmMDFe_Pesq_0000.FechaMDFeCabA();
begin
  QrMDFeCabA.Close;
end;

procedure TFmMDFe_Pesq_0000.RGOrdem1Click(Sender: TObject);
begin
  FechaMDFeCabA;
end;

procedure TFmMDFe_Pesq_0000.RGOrdem2Click(Sender: TObject);
begin
  FechaMDFeCabA;
end;

procedure TFmMDFe_Pesq_0000.ReopenMDFeCabA(IDCtrl: Integer; So_O_ID: Boolean);
var
  Empresa, DOC1, Ord2, SitConfs: String;
begin
  Screen.Cursor := crHourGlass;
  FSo_o_ID := So_O_ID;
  if (EdFilial.ValueVariant <> Null) and (EdFilial.ValueVariant <> 0) then
    Empresa := FormatFloat('0', DModG.QrEmpresasCodigo.Value)
  else Empresa := '';
  //
  QrMDFeCabA.Close;
  if Empresa <> '' then
  begin
    QrMDFeCabA.SQL.Clear;
    QrMDFeCabA.SQL.Add('SELECT IF(emi.Tipo=0, emi.RazaoSocial, emi.Nome) NO_Emi,');
    QrMDFeCabA.SQL.Add('nfa.FatID, nfa.FatNum, nfa.Empresa, nfa.LoteEnv, nfa.Id,');
    QrMDFeCabA.SQL.Add('ide_serie, ide_nMDF, ide_dEmi, ide_Modal, ');
    QrMDFeCabA.SQL.Add('Status + 0.000 cStat,');
    QrMDFeCabA.SQL.Add('IF(Status=101, infCanc_nProt, ');
    QrMDFeCabA.SQL.Add('IF(Status=132, infEnc_nProt, infProt_nProt)) nProt,');
    QrMDFeCabA.SQL.Add('IF(Status=101, infCanc_xMotivo, ');
    QrMDFeCabA.SQL.Add('IF(Status=132, infEnc_xMotivo, infProt_xMotivo)) xMotivo,');
    QrMDFeCabA.SQL.Add('IF(Status=101, ');
    QrMDFeCabA.SQL.Add('  DATE_FORMAT(infCanc_dhRecbto, "%d/%m/%Y %H:%i:%S"), ');
    QrMDFeCabA.SQL.Add('IF(Status=132, ');
    QrMDFeCabA.SQL.Add('  DATE_FORMAT(infEnc_dhRegEvento, "%d/%m/%Y %H:%i:%S"), ');
    QrMDFeCabA.SQL.Add('  DATE_FORMAT(infProt_dhRecbto, "%d/%m/%Y %H:%i:%S"))) dhRecbto,');
    QrMDFeCabA.SQL.Add('IDCtrl, versao, ');
    QrMDFeCabA.SQL.Add('ide_tpEmis, infCanc_xJust, Status, ');
    QrMDFeCabA.SQL.Add('cSitMDFe, cSitEnc,');
    QrMDFeCabA.SQL.Add(MDFe_PF.TxtNo_SitEnc());
    QrMDFeCabA.SQL.Add('nfa.infCanc_dhRecbto, nfa.infCanc_nProt, ');
    QrMDFeCabA.SQL.Add('nfa.ide_Modal');
    QrMDFeCabA.SQL.Add('FROM mdfecaba nfa');
    QrMDFeCabA.SQL.Add('LEFT JOIN entidades emi ON emi.Codigo=nfa.CodInfoEmite');
    QrMDFeCabA.SQL.Add('');
    if FSo_O_ID then
    begin
      QrMDFeCabA.SQL.Add('WHERE IDCtrl=' + FormatFloat('0', IDCtrl));
      QrMDFeCabA.Open;
    end else begin
      QrMDFeCabA.SQL.Add(dmkPF.SQL_Periodo('WHERE ide_dEmi ', TPDataI.Date, TPDataF.Date, True, True));
      QrMDFeCabA.SQL.Add('AND nfa.Empresa = ' + Empresa);
      case RGQuemEmit.ItemIndex of
        0: QrMDFeCabA.SQL.Add('AND nfa.emit_CNPJ="' + DModG.QrEmpresasCNPJ_CPF.Value + '"');
        1: QrMDFeCabA.SQL.Add('AND (nfa.emit_CNPJ<>"' + DModG.QrEmpresasCNPJ_CPF.Value + '" OR nfa.emit_CNPJ IS NULL)');
      end;
      if Ck100e101.Checked  then
        QrMDFeCabA.SQL.Add('AND (IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat)) in (100,101)');
      case RGAmbiente.ItemIndex of
        0: ; // Tudo
        1: QrMDFeCabA.SQL.Add('AND nfa.ide_tpAmb=1');
        2: QrMDFeCabA.SQL.Add('AND nfa.ide_tpAmb=2');
      end;
      SitConfs := CGcSitEnc.GetIndexesChecked(True, -1);
      if SitConfs <> '' then
        QrMDFeCabA.SQL.Add('AND nfa.cSitEnc IN (' + SitConfs + ')');
      //
      case RGOrdem2.ItemIndex of
        0: Ord2 := '';
        1: Ord2 := 'DESC';
      end;
      case RGOrdem1.ItemIndex of
        0: QrMDFeCabA.SQL.Add('ORDER BY nfa.ide_dEmi ' + Ord2 + ', nfa.ide_nMDF ' + Ord2);
        1: QrMDFeCabA.SQL.Add('ORDER BY nfa.ide_nMDF ' + Ord2 + ', nfa.ide_dEmi ' + Ord2 );
        2: QrMDFeCabA.SQL.Add('ORDER BY cStat ' + Ord2 + ', nfa.ide_nMDF ' + Ord2);
        3: QrMDFeCabA.SQL.Add('ORDER BY cStat ' + Ord2 + ', nfa.ide_dEmi ' + Ord2);
      end;
      UnDmkDAC_PF.AbreQuery(QrMDFeCabA, Dmod.MyDB);
      //Geral.MB_SQL(Self, QrMDFeCabA);
      QrMDFeCabA.Locate('IDCtrl', IDCtrl, []);
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmMDFe_Pesq_0000.ReopenMDFeCabAMsg;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeCabAMsg, Dmod.MyDB, [
  'SELECT * ' ,
  'FROM mdfecabamsg ' ,
  'WHERE FatID=' + Geral.FF0(QrMDFeCabAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrMDFeCabAFatID.Value),
  'AND Empresa=' + Geral.FF0(QrMDFeCabAFatID.Value),
  'ORDER BY Controle DESC ',
  '']);
(*
  QrMDFeCabAMsg.Close;
  QrMDFeCabAMsg.Params[00].AsInteger := QrMDFeCabAFatID.Value;
  QrMDFeCabAMsg.Params[01].AsInteger := QrMDFeCabAFatNum.Value;
  QrMDFeCabAMsg.Params[02].AsInteger := QrMDFeCabAEmpresa.Value;
  QrMDFeCabAMsg.Open;
*)
end;

procedure TFmMDFe_Pesq_0000.ReopenMDFeEveRRet();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeEveRRet, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfeeverret',
  'WHERE ret_chMDFe="' + QrMDFeCabAId.Value + '" ',
  'ORDER BY ret_dhRegEvento DESC',
  '']);
end;

{
procedure TFmMDFe_Pesq_0000.ReopenMDFeIt2Comp;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr2Comp, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfeit2comp ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  '']);
end;

procedure TFmMDFe_Pesq_0000.ReopenMDFeIt2InfDoc();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr2InfDoc, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfeit2infdoc ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  '']);
end;

procedure TFmMDFe_Pesq_0000.ReopenMDFeIt2ObsCont();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr2ObsCont, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfeit2obscont ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  '']);
end;

procedure TFmMDFe_Pesq_0000.ReopenMDFeIt2ObsFisco();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr2ObsFisco, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfeit2obsfisco ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  '']);
end;

procedure TFmMDFe_Pesq_0000.ReopenMDFeIt2Seg();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr2Seg, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfeit2seg ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  '']);
end;

procedure TFmMDFe_Pesq_0000.ReopenMDFeIt3InfQ();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr3InfQ, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfeit3infq ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  '']);
end;

procedure TFmMDFe_Pesq_0000.ReopenMDFeMoRodoCab();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRodoCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfemorodocab ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  '']);
end;

procedure TFmMDFe_Pesq_0000.ReopenMDFeMoRodoLacr();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRodoLacr, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfemorodolacr ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  '']);
end;

procedure TFmMDFe_Pesq_0000.ReopenMDFeMoRodoMoto();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRodoMoto, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfemorodomoto ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  '']);
end;

procedure TFmMDFe_Pesq_0000.ReopenMDFeMoRodoProp();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRodoProp, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfemorodoprop ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  'AND Controle=' + Geral.FF0(QrRodoVeicControle.Value),
  'AND Conta=' + Geral.FF0(QrRodoVeicConta.Value),
  '']);
end;

procedure TFmMDFe_Pesq_0000.ReopenMDFeMoRodoVeic();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRodoVeic, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfemorodoveic ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  '']);
end;

procedure TFmMDFe_Pesq_0000.ReopenMDFeMoRodoVPed();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRodoVPed, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfemorodovped ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  '']);
end;
}

procedure TFmMDFe_Pesq_0000.RGAmbienteClick(Sender: TObject);
begin
  FechaMDFeCabA;
end;

procedure TFmMDFe_Pesq_0000.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmMDFe_Pesq_0000.TPDataIChange(Sender: TObject);
begin
  FechaMDFeCabA;
end;

procedure TFmMDFe_Pesq_0000.TPDataIClick(Sender: TObject);
begin
  FechaMDFeCabA;
end;

end.

