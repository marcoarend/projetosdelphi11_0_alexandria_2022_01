unit MDFeSteps_0100a;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, Vcl.OleCtrls,
  OmniXML, OmniXMLUtils, Variants,
  UnDmkProcFunc, UnMDFe_PF,
  SHDocVw, dmkPageControl, dmkRadioGroup, mySQLDbTables, dmkDBLookupComboBox,
  dmkEditCB, dmkEditDateTimePicker;

type
  TTipoTagXML = (
     ttx_Id                ,
     ttx_idLote            ,
     ttx_versao            ,
     ttx_tpAmb             ,
     ttx_verAplic          ,
     ttx_cOrgao            ,
     ttx_cStat             ,
     ttx_xMotivo           ,
     ttx_cUF               ,
     ttx_dhRecbto          ,
     ttx_chMDFe             ,
     ttx_nProt             ,
     ttx_digVal            ,
     ttx_ano               ,
     ttx_CNPJ              ,
     ttx_mod               ,
     ttx_serie             ,
     ttx_nCTIni            ,
     ttx_nCTFin            ,
     ttx_nRec              ,
     ttx_tMed              ,
     ttx_tpEvento          ,
     ttx_xEvento           ,
     ttx_CNPJDest          ,
     ttx_CPFDest           ,
     ttx_emailDest         ,
     ttx_nSeqEvento        ,
     ttx_dhRegEvento       ,
     ttx_dhResp            ,
     ttx_indCont           ,
     ttx_ultNSU            ,
     ttx_maxNSU            ,
     ttx_NSU               ,
     ttx_CPF               ,
     ttx_xNome             ,
     ttx_IE                ,
     ttx_dEmi              ,
     ttx_tpCT              ,
     ttx_vCT               ,
     ttx_cSitMDFe          ,
     ttx_cSitEnc           ,
     ttx_dhEvento          ,
     ttx_descEvento        ,
     //ttx_x Correcao         ,
     ttx_cJust             ,
     ttx_xJust             ,
     ttx_verEvento         ,
     ttx_schema            ,
     ttx_docZip            ,
     ttx_dhEmi             ,
     //
     ttx_versaoEvento      ,
     ttx_dtEnc             ,
     ttx_cMun              ,
     ttx_
     );

  TFmMDFeSteps_0100a = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    PnConfirma: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PnConfig1: TPanel;
    Panel6: TPanel;
    Label5: TLabel;
    Label2: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    EdSerialNumber: TdmkEdit;
    EdUF_Servico: TdmkEdit;
    CBUF: TComboBox;
    EdEmpresa: TdmkEdit;
    Panel7: TPanel;
    RGAmbiente: TRadioGroup;
    Panel11: TPanel;
    Label19: TLabel;
    EdWebService: TEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    RETxtEnvio: TMemo;
    TabSheet5: TTabSheet;
    WBEnvio: TWebBrowser;
    TabSheet2: TTabSheet;
    RETxtRetorno: TMemo;
    TabSheet3: TTabSheet;
    WBResposta: TWebBrowser;
    TabSheet4: TTabSheet;
    MeInfo: TMemo;
    TabSheet6: TTabSheet;
    MeChaves: TMemo;
    Panel1: TPanel;
    EdEmitCNPJ: TdmkEdit;
    Label14: TLabel;
    Label20: TLabel;
    EdVersaoAcao: TdmkEdit;
    RGAcao: TRadioGroup;
    LaExpiraCertDigital: TLabel;
    PCAcao: TdmkPageControl;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    Panel5: TPanel;
    Label4: TLabel;
    EdLote: TdmkEdit;
    Label6: TLabel;
    EdRecibo: TdmkEdit;
    CkSoLer: TCheckBox;
    RGIndSinc: TdmkRadioGroup;
    REWarning: TRichEdit;
    Timer1: TTimer;
    PnAbrirXML: TPanel;
    BtAbrir: TButton;
    Button1: TButton;
    QrMDFeCabA1: TmySQLQuery;
    QrMDFeCabA1FatID: TIntegerField;
    QrMDFeCabA1FatNum: TIntegerField;
    QrMDFeCabA1Empresa: TIntegerField;
    QrMDFeCabA1IDCtrl: TIntegerField;
    TabSheet9: TTabSheet;
    TabSheet10: TTabSheet;
    PnInutiliza: TPanel;
    Label15: TLabel;
    GroupBox2: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    EdnCTIni: TdmkEdit;
    EdnCTFim: TdmkEdit;
    Panel9: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    EdModeloInu: TdmkEdit;
    EdSerieInu: TdmkEdit;
    EdAno: TdmkEdit;
    PnJustificativa: TPanel;
    Label7: TLabel;
    SpeedButton2: TSpeedButton;
    EdMDFeJustInu: TdmkEditCB;
    CBMDFeJustInu: TdmkDBLookupComboBox;
    QrMDFeJustInu: TmySQLQuery;
    QrMDFeJustInuCodigo: TIntegerField;
    QrMDFeJustInuNome: TWideStringField;
    QrMDFeJustInuCodUsu: TIntegerField;
    QrMDFeJustInuAplicacao: TIntegerField;
    DsMDFeJustInu: TDataSource;
    QrMDFeJustCan: TmySQLQuery;
    QrMDFeJustCanCodigo: TIntegerField;
    QrMDFeJustCanNome: TWideStringField;
    QrMDFeJustCanCodUsu: TIntegerField;
    QrMDFeJustCanAplicacao: TIntegerField;
    DsMDFeJustCan: TDataSource;
    Panel8: TPanel;
    Label3: TLabel;
    SpeedButton1: TSpeedButton;
    EdMDFeJustCan: TdmkEditCB;
    CBMDFeJustCan: TdmkDBLookupComboBox;
    Panel10: TPanel;
    Panel12: TPanel;
    Label22: TLabel;
    Label23: TLabel;
    EdModeloCan: TdmkEdit;
    EdSerieCan: TdmkEdit;
    Label8: TLabel;
    EdnMDFCan: TdmkEdit;
    Label1: TLabel;
    EdnProtCan: TdmkEdit;
    Label9: TLabel;
    EdchMDFeCan: TdmkEdit;
    Label16: TLabel;
    EdnSeqEventoCan: TdmkEdit;
    TabSheet11: TTabSheet;
    TabSheet12: TTabSheet;
    TabSheet13: TTabSheet;
    TabSheet14: TTabSheet;
    TabSheet15: TTabSheet;
    Panel13: TPanel;
    EdnProtEnc: TdmkEdit;
    Label21: TLabel;
    Label24: TLabel;
    EdchMDFeEnc: TdmkEdit;
    Label26: TLabel;
    EdcUFEnc: TdmkEdit;
    Label105: TLabel;
    EdcMunEnc: TdmkEditCB;
    Label36: TLabel;
    TPdtEnc: TdmkEditDateTimePicker;
    EdxMunEnc: TdmkEdit;
    Panel14: TPanel;
    Label25: TLabel;
    EdnProtIdC: TdmkEdit;
    Label27: TLabel;
    EdchMDFeIdC: TdmkEdit;
    EdxNomeIdC: TdmkEdit;
    Label28: TLabel;
    EdCPFIdc: TdmkEdit;
    Label29: TLabel;
    Panel15: TPanel;
    Label33: TLabel;
    Label30: TLabel;
    EdChaveMDFeMDFe: TdmkEdit;
    EdIDCtrlMDFe: TdmkEdit;
    QrCabA: TmySQLQuery;
    QrCabAIDCtrl: TIntegerField;
    QrCabAinfProt_ID: TWideStringField;
    QrCabAinfProt_nProt: TWideStringField;
    QrMDFeCabA2: TmySQLQuery;
    QrMDFeCabA2FatID: TIntegerField;
    QrMDFeCabA2FatNum: TIntegerField;
    QrMDFeCabA2Empresa: TIntegerField;
    QrMDFeCabA2Id: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure RETxtEnvioChange(Sender: TObject);
    procedure RETxtRetornoChange(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure BtAbrirClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure RGAcaoClick(Sender: TObject);
  private
    { Private declarations }
    FCodigoUF_Int, FAmbiente_Int: Integer;
    FAmbiente_Txt, FCodigoUF_Txt, FWSDL, FURL: String;
    xmlDoc: IXMLDocument;
    xmlNode, xmlSub, xmlChild1, xmlChild2, xmlChild3: IXMLNode;
    FPathLoteMDFe: String;
    FSegundos, FSecWait: Integer;
    FNaoExecutaLeitura: Boolean;
    FSiglas_WS: MyArrayLista;
    xmlList(*, xmlSubs*): IXMLNodeList;
    //
    function  AbreArquivoXML(Arq, Ext: String; Assinado: Boolean): Boolean;
    function  DefinechMDFe(const EdChaveMDFe: TdmkEdit; var chMDFe: String): Boolean;
{
    function  DefinenCTIni(var nCTIni: String): Boolean;
    function  DefinenCTFin(var nCTFim: String): Boolean;
    function  DefineEmitCNPJ(var EmitCNPJ: String): Boolean;
}
    function  DefineEmpresa(var Empresa: Integer): Boolean;
    function  DefineIDCtrl(const EdIDCtrl: TdmkEdit; var IDCtrl: Integer): Boolean;
    function  DefineLote(var Lote: Integer): Boolean;
{
    function  DefineModelo(const ModInt: Integer; var ModTxt: String): Boolean;
    function  DefineSerie(const SerVar: Variant; var SerTxt: String): Boolean;
}
    function  DefineXMLDoc(): Boolean;
    //
    procedure ExecutaConsultaLoteMDFe();
    procedure ExecutaConsultaMDFe();
    procedure ExecutaConsultaNaoEncerrados();
    procedure ExecutaEnvioDeLoteMDFe(Sincronia: TXXeIndSinc);
    procedure ExecutaEvento_Cancelamanto();
    procedure ExecutaEvento_IncCondutor();
    procedure ExecutaEvento_PedidoDeEncerramento();
{
    procedure ExecutaInutilizaNumerosMDFe();
}
    //
    procedure HabilitaBotoes(Visivel: Boolean = True);
    function  LeNoXML(No: IXMLNode; Tipo: TTipoNoXML; Tag: TTipoTagXML; AvisaVersao: Boolean = True): String;
    //
    procedure LerTextoConsultaLoteMDFe();
    procedure LerTextoConsultaMDFe();
    procedure LerTextoConsultaNaoEncerrados();
    function  LerTextoEnvioLoteMDFe(): Boolean;
{
    procedure LerTextoInutilizaNumerosMDFe();
}
    procedure LerTextoStatusServico();
    function  LerTextoEvento(): Boolean;
    procedure LerTextoEvento_Cancelamento();
    procedure LerTextoEvento_IncCondutor();
    procedure LerTextoEvento_PedidoDeEncerramento();
    procedure LerXML_procEventoMDFe(Lista: IXMLNodeList; digVal, cStat,
              dhRecbto: String);
    //
    procedure MostraTextoRetorno(Texto: String);
    function  ObtemNomeDaTag(Tag: TTipoTagXML): String;
    function  ObtemDescricaoDaTag(Tag: TTipoTagXML): String;
    procedure ReopenMDFeJust(QrMDFeJust: TmySQLQuery; Aplicacao: Byte);
    function  TextoArqDefinido(Texto: String): Boolean;
    procedure VerificaCertificadoDigital(Empresa: Integer);
    procedure VerificaStatusServico();

  public
    { Public declarations }
    FXML_Evento, FTextoArq: String;
    FXML_LoteMDFe, FXML_LoteEvento: String;
    FQry: TmySQLQuery;
    //
    procedure PreparaConsultaLote(Lote, Empresa: Integer; Recibo: String);
    procedure PreparaConsultaMDFe(Empresa, IDCtrl: Integer; ChaveMDFe: String);
    procedure PreparaConsultaNaoEncerrados(Empresa: Integer);
    function  PreparaEnvioDeLoteMDFe(Lote, Empresa: Integer; Sincronia:
              TXXeIndSinc): Boolean;
    procedure PreparaEventoIncCondutor(Empresa: Integer; chMDFe, nProt, xNome,
              CPF: String; XML_Evento: String);
    procedure PreparaEncerramentoDeMDFe(Empresa: Integer; chMDFe, xMun: String;
              nProt: String; dtEnc: TDateTime; cUF, cMun: Integer;
              XML_Evento: String);
{
    procedure PreparaInutilizaNumerosMDFe(Empresa, Lote, Ano, Modelo, Serie,
              nCTIni, nCTFim, Justif: Integer);
}
    procedure PreparaCancelamentoDeMDFe(Empresa, Serie, nMDF, nSeqEvento,
              Justif: Integer; Modelo, nProtCan, chMDFe, XML_Evento: String);
{
    procedure PreparaStepGenerico(Empresa: Integer);
}
    procedure PreparaVerificacaoStatusServico(Empresa: Integer);
  end;

  var
  FmMDFeSteps_0100a: TFmMDFeSteps_0100a;

var
  FverXML_versao: String;

implementation

uses UnMyObjects, Module, UnXXe_PF, ModuleMDFe_0000, UMySQLModule, dmkDAC_PF,
  MDFeGeraXML_0100a, ModuleGeral(*, MDFeLEnC_0200a, MDFeLEnU_0200a, MDFEInut_0000,
  MDFeEveRCab*);

{$R *.DFM}

const
  FXML_Load_Failure = 'Falha ao carregar o XML!';
  CO_Texto_Opt_Sel = 'Clique em "OK" para ler arquivo selecionado, ou em "Abrir" para selecionar um arquivo!';
  CO_Texto_Clk_Sel = 'Configure a forma de consulta e clique em "OK"!';
  CO_Texto_Env_Sel = 'Configure a forma de envio e clique em "OK" para enviar o lote ao fisco!';

function TFmMDFeSteps_0100a.AbreArquivoXML(Arq, Ext: String;
  Assinado: Boolean): Boolean;
var
  Dir, Arquivo: String;
begin
  Result := False;
  FTextoArq := '';
  if not DmMDFe_0000.ObtemDirXML(Ext, Dir, Assinado) then
    Exit;
  Arquivo := Dir + Arq + Ext;
  if FileExists(Arquivo) then
  begin
    if dmkPF.CarregaArquivo(Arquivo, FTextoArq) then
    begin
      MostraTextoRetorno(FTextoArq);
      Result := FTextoArq <> '';
      if Result then HabilitaBotoes();
    end;
  end else Geral.MB_Erro('Arquivo n�o localizado "' + Arquivo + '"!');
end;

procedure TFmMDFeSteps_0100a.BtAbrirClick(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  Geral.MB_Aviso('Parei Aqui!');
{***
  IniDir := ExtractFileDir('C:\Dermatek\MDFe\');
  Arquivo := '';
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo XML', '', [], Arquivo) then
  begin
    //AbreArquivoSelecionado(Arquivo);
    //Result := False;
    if dmkPF.CarregaArquivo(Arquivo, FTextoArq) then
    begin
      MostraTextoRetorno(FTextoArq);
      if FTextoArq <> '' then
        HabilitaBotoes();
      //Result := true;
    end;
  end;
}
end;

procedure TFmMDFeSteps_0100a.BtOKClick(Sender: TObject);
begin
  CO_MDFE_CERTIFICADO_DIGITAL_SERIAL_NUMBER := EdSerialNumber.Text;
  REWarning.Text     := '';
  RETxtEnvio.Text    := '';
  MeInfo.Text        := '';
  MostraTextoRetorno('');
  PageControl1.ActivePageIndex := 0;
  Update;
  Application.ProcessMessages;
  //
  FCodigoUF_Int  := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(CBUF.Text);
  FAmbiente_Int  := RGAmbiente.ItemIndex;
  if FAmbiente_Int = 0 then
  begin
    Geral.MB_Aviso('Defina o ambiente!');
    Exit;
  end;
  FAmbiente_Txt  := IntToStr(FAmbiente_Int);
  FCodigoUF_Txt  := IntToStr(FCodigoUF_Int);
  FWSDL          := '';
  FURL           := '';
  if FCodigoUF_Int = 0 then
  begin
    Geral.MB_Aviso('Defina a UF!');
    Exit;
  end;
  //
  BtOK.Enabled := False;
  case RGAcao.ItemIndex of
    0: VerificaStatusServico();
    1:
    begin
      if CkSoLer.Checked then
        LerTextoEnvioLoteMDFe((*nisSincrono*))
      else
        ExecutaEnvioDeLoteMDFe(TXXeIndSinc(RGIndSinc.ItemIndex));
    end;
    2:
    begin
      if CkSoLer.Checked then
        LerTextoConsultaLoteMDFe()
      else
        ExecutaConsultaLoteMDFe();
    end;
    3:
    begin
      if CkSoLer.Checked then
        LerTextoEvento_Cancelamento()
      else
        ExecutaEvento_Cancelamanto();
    end;
{
    4:
    begin
      if CkSoLer.Checked then
        LerTextoInutilizaNumerosMDFe()
      else
        ExecutaInutilizaNumerosMDFe();
    end;
}
    5:
    begin
      if CkSoLer.Checked then
        LerTextoConsultaMDFe()
      else
        ExecutaConsultaMDFe();
    end;
    6:
    begin
      if CkSoLer.Checked then
        LerTextoEvento_IncCondutor()
      else
        ExecutaEvento_IncCondutor()
    end;
{
    7: ConsultaCadastroContribuinte();
}
    8: //Evento de Encerramento de MDFe
    begin
      if CkSoLer.Checked then
        LerTextoEvento_PedidoDeEncerramento()
      else
        ExecutaEvento_PedidoDeEncerramento();
    end;
    9:
    begin
      if CkSoLer.Checked then
        LerTextoConsultaNaoEncerrados()
      else
        ExecutaConsultaNaoEncerrados();
    end;
{
    10:
    begin
      if CkSoLer.Checked then
        LerTextoDownloadMDFeConfirmadas()
      else
        ExecutaDownloadMDFeConfirmadas();
    end;
    11:
    begin
      if CkSoLer.Checked then
        LerTextoConsultaDistribuicaoDFeInteresse()
      else
        ExecutaConsultaDistribuicaoDFeInteresse();
    end;
}
    else Geral.MB_Aviso('A a��o "' + RGAcao.Items[RGAcao.ItemIndex] +
    '" n�o est� implementada! AVISE A DERMATEK!');
  end;
  if Trim(REWarning.Text) <> '' then
    dmkPF.LeTexto_Permanente(REWarning.Text, 'Aviso do Web Service');
end;

procedure TFmMDFeSteps_0100a.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMDFeSteps_0100a.Button1Click(Sender: TObject);
begin
  dmkPF.LeTexto_Permanente(FTextoArq, FXML_Load_Failure);
end;

{
function TFmMDFeSteps_0100a.DefineEmitCNPJ(var EmitCNPJ: String): Boolean;
var
  K: Integer;
begin
  EmitCNPJ := Geral.SoNumero_TT(EdEmitCNPJ.Text);
  k := Length(EmitCNPJ);
  if K = 14 then Result := True
  else begin
    Result := False;
    Geral.MB_Erro('CNPJ da empresa emitente com tamanho incorreto!');
  end;
end;
}

function TFmMDFeSteps_0100a.DefinechMDFe(const EdChaveMDFe: TdmkEdit;
  var chMDFe: String): Boolean;
var
  K: Integer;
begin
  Result := False;
  chMDFe  := EdChaveMDFe.Text;
  K := Length(chMDFe);
  if K <> 44 then
    Geral.MB_Erro('Tamanho da chave difere de 44: tamanho = ' + IntToStr(K))
  else if Geral.SoNumero1a9_TT(chMDFe) = '' then
    Geral.MB_Erro('Chave n�o definida!')
  else
    Result := True;
end;

function TFmMDFeSteps_0100a.DefineEmpresa(var Empresa: Integer): Boolean;
begin
  Empresa := EdEmpresa.ValueVariant;
  if Empresa <> 0 then Result := True
  else begin
    Result := False;
    Geral.MB_Erro('Empresa n�o definida!');
  end;
end;

function TFmMDFeSteps_0100a.DefineIDCtrl(const EdIDCtrl: TdmkEdit;
  var IDCtrl: Integer): Boolean;
begin
  IDCtrl := EdIDCtrl.ValueVariant;
  if IDCtrl <> 0 then Result := True
  else begin
    Result := False;
    Geral.MB_Erro(
    'IDCtrl n�o definido! A��o/consulta n�o ser� inclu�da no hist�rico do MDFe!');
  end;
end;

function TFmMDFeSteps_0100a.DefineLote(var Lote: Integer): Boolean;
begin
  Lote := EdLote.ValueVariant;
  if Lote <> 0 then Result := True
  else begin
    Result := False;
    Geral.MB_Erro('Lote n�o definido!');
  end;
end;

{
function TFmMDFeSteps_0100a.DefineModelo(const ModInt: Integer; var ModTxt: String): Boolean;
begin
  ModTxt := FormatFloat('00', ModInt);
  if ModInt <> 57 then
  begin
    Result := False;
    Geral.MB_Erro('Modelo de MDF-e n�o implementado: ' + ModTxt);
  end else Result := True;
end;

function TFmMDFeSteps_0100a.DefinenCTFin(var nCTFim: String): Boolean;
begin
  if (EdnCTFim.ValueVariant = null) or (EdnCTFim.ValueVariant < 0) then
  begin
    Result := False;
    Geral.MB_Erro('Numera��o final de MDF-e inv�lida!');
  end else begin
    nCTFim := FormatFloat('0', EdnCTFim.ValueVariant);
    Result := True;
  end;
end;

function TFmMDFeSteps_0100a.DefinenCTIni(var nCTIni: String): Boolean;
begin
  if (EdnCTIni.ValueVariant = null) or (EdnCTIni.ValueVariant < 0) then
  begin
    Result := False;
    Geral.MB_Erro('Numera��o inicial de MDF-e inv�lida!');
  end else begin
    nCTIni := FormatFloat('0', EdnCTIni.ValueVariant);
    Result := True;
  end;
end;

function TFmMDFeSteps_0100a.DefineSerie(const SerVar: Variant; var SerTxt: String): Boolean;
begin
  if (SerVar = null) or
  (SerVar < 0) or
  (SerVar > 899) then
  begin
    Result := False;
    Geral.MB_Aviso('N�mero de s�rie inv�lido!');
  end else
  begin
    SerTxt  := FormatFloat('0', SerVar);
    Result := True;
  end;
end;
}

function TFmMDFeSteps_0100a.DefineXMLDoc(): Boolean;
begin
  xmlDoc := TXMLDocument.Create;
  if not XMLLoadFromAnsiString(xmlDoc, FTextoArq) then
  begin
    Result := False;
    dmkPF.LeTexto_Permanente(FTextoArq, FXML_Load_Failure);
  end else Result := True;
end;

procedure TFmMDFeSteps_0100a.EdEmpresaChange(Sender: TObject);
begin
  DmMDFe_0000.ReopenOpcoesMDFe(DmMDFe_0000.QrOpcoesMDFe, EdEmpresa.ValueVariant, True);
  RGAmbiente.ItemIndex := DmMDFe_0000.QrOpcoesMDFeMDFeide_tpAmb.Value;
end;

procedure TFmMDFeSteps_0100a.ExecutaEvento_Cancelamanto();
const
  TipoConsumo = tcwsmdfeEveCancelamento;
var
  ID_Res, Id, xJust, Modelo, Serie, nCTIni, nCTFin, EmitCNPJ, LoteStr: String;
  Empresa, Lote, K: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando dados a serem enviados ao servidor do fisco');
  Screen.Cursor := CrHourGlass;
  try
    FTextoArq := FmMDFeGeraXML_0100a.WS_EventoCancelamentoMDFe(EdUF_Servico.Text,
      FAmbiente_Int, FCodigoUF_Int,  (*NumeroSerial: String; Lote: Integer; LaAviso1,
      LaAviso2: TLabel; RETxtEnvio: TMemo; EdWS: TEdit*)FXML_Evento, TipoConsumo,
      RETxtEnvio, EdWebService);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebido com Sucesso!');
    //
    // Salva arquivo
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Salvando resposta');
    //
    ID := EdchMDFeCan.Text;
    //
    DmMDFe_0000.SalvaXML(MDFE_EXT_EVE_RET_CAN_XML, EdchMDFeCan.Text, FTextoArq, RETxtRetorno, False);
    //
    LerTextoEvento_Cancelamento();
    //
    if FQry <> nil then
      UnDmkDAC_PF.AbreQuery(FQry, FQry.Database);
    //
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MB_Erro('Erro na chamada do WS...' +  sLineBreak  + FTextoArq);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Erros');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmMDFeSteps_0100a.ExecutaConsultaLoteMDFe();
var
  Recibo, LoteStr: String;
  Empresa, Lote: Integer;
begin
  Recibo := EdRecibo.Text;
  if (Recibo <> '')  then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando consulta');
    if not DefineEmpresa(Empresa) then Exit;
    if not DefineLote(Lote) then Exit;
    DmMDFe_0000.ReopenEmpresa(Empresa);
    FTextoArq :='';
    Screen.Cursor := CrHourGlass;
    try
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Consultando o servidor do fisco');
      FTextoArq := FmMDFeGeraXML_0100a.WS_MDFeRetRecepcao(EdUF_Servico.Text, FAmbiente_Int,
        FCodigoUF_Int, EdRecibo.Text, LaAviso1, LaAviso2, RETxtEnvio, EdWebService);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Sucesso!');
      MostraTextoRetorno(FTextoArq);
      if Pos('Erros:', FTextoArq) > 0 then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2,False, 'O fisco acusou erros na resposta!');
        Geral.MB_Erro('Resposta recebida com Erros!');
      end else
      begin
        DmMDFe_0000.ReopenEmpresa(Empresa);
        //
        LoteStr := DmMDFe_0000.FormataLoteMDFe(Lote);
        DmMDFe_0000.SalvaXML(MDFE_EXT_PRO_REC_XML, LoteStr, FTextoArq, RETxtRetorno, False);
        //
        LerTextoConsultaLoteMDFe();
        try
          if FQry <> nil then
            UnDmkDAC_PF.AbreQuery(FQry, FQry.Database)
          else
            Geral.MB_Aviso(
            'Tabela n�o definida para localizar o lote de MDF-e(s) n�mero ' +
            EdLote.Text + '!');
        except
          Geral.MB_Aviso('N�o foi poss�vel localizar o lote de MDF-e(s) n�mero ' +
          EdLote.Text + '!');
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end
  else
    Geral.MB_Aviso('Recibo n�o informado para consulta...');
end;

procedure TFmMDFeSteps_0100a.ExecutaConsultaMDFe();
var
  Empresa, IDCtrl: Integer;
  chMDFe: String;
  Id, Dir, Aviso: String;
begin
  if not DefineEmpresa(Empresa) then Exit;
  if not DefinechMDFe(EdChaveMDFeMDFe, chMDFe) then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    FTextoArq := FmMDFeGeraXML_0100a.WS_MDFeConsultaMDF(EdUF_Servico.Text,
      FAmbiente_Int, FCodigoUF_Int, chMDFe, LaAviso1, LaAviso2,
      RETxtEnvio, EdWebService);
    if FTextoArq = '' then
      Exit;
    MostraTextoRetorno(FTextoArq);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Resposta recebida com Sucesso!');
    //
    DmMDFe_0000.SalvaXML(MDFE_EXT_SIT_XML, chMDFe, FTextoArq, RETxtRetorno, False);
    //
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MB_Erro('Erro na chamada do WS...' +  sLineBreak  + FTextoArq);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Erros!');
    end;
    if not FNaoExecutaLeitura then
    begin
      LerTextoConsultaMDFe();
      //
      QrCabA.Close;
      QrCabA.Params[00].AsInteger := EdEmpresa.ValueVariant;
      QrCabA.Params[01].AsString  := chMDFe;
      QrCabA.Open;
      IDCtrl := QrCabAIDCtrl.Value;
      if IDCtrl > 0 then
      begin
        Id    := QrCabAinfProt_ID.Value;
        if Trim(Id) = '' then
          Id := QrCabAinfProt_nProt.Value;
        Dir   := DmMDFe_0000.QrOpcoesMDFeDirMDFeSit.Value;
        Aviso := '';
        DmMDFe_0000.AtualizaXML_No_BD_ConsultaMDFe(chMDFe, Id, IDCtrl, Dir, Aviso);
        if Aviso <> '' then Geral.MB_Aviso(
        'Os arquivos abaixo n�o foram localizados:' + sLineBreak + Aviso);
      end;
    end else
    begin
      LerTextoConsultaMDFe();
      Close;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmMDFeSteps_0100a.ExecutaConsultaNaoEncerrados();
var
  CodigoUF_Int: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    CodigoUF_Int  := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(CBUF.Text);
    //
    FTextoArq := FmMDFeGeraXML_0100a.WS_MDFeConsultaNaoEncerrados(EdUF_Servico.Text,
      RGAmbiente.ItemIndex, CodigoUF_Int, EdEmitCNPJ.Text, EdSerialNumber.Text,
      LaAviso1, LaAviso2, RETxtEnvio, EdWebService);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com sucesso!');
    MostraTextoRetorno(FTextoArq);
    LerTextoConsultaNaoEncerrados();
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MB_Erro('Erro na chamada do WS...' +  sLineBreak  + FTextoArq);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Erros');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmMDFeSteps_0100a.ExecutaEnvioDeLoteMDFe(Sincronia: TXXeIndSinc);
var
  retWS, rtfDadosMsg, LoteStr: String;
  Lote, Empresa: Integer;
begin
  FTextoArq := '';
  if not DefineLote(Lote) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  //
  if not CkSoLer.Checked then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviando lote ao fisco');
    if not FileExists(FPathLoteMDFe) then
    begin
      //Continua := False;
      Geral.MB_Erro('O lote "' + FPathLoteMDFe + '" n�o foi localizado!');
      Exit;
    end;
    if dmkPF.CarregaArquivo(FPathLoteMDFe, rtfDadosMsg) then
    begin
      if rtfDadosMsg = '' then
      begin
        Geral.MB_Erro('O lote de MDFe "' + FPathLoteMDFe +
        '" foi carregado mas est� vazio!');
        Exit;
      end;
      retWS :='';
      FTextoArq :='';
      Screen.Cursor := crHourGlass;
      try
        FTextoArq := FmMDFeGeraXML_0100a.WS_MDFeRecepcaoLote(EdUF_Servico.Text,
        FAmbiente_Int, FCodigoUF_Int, EdSerialNumber.Text, Lote, LaAviso1, LaAviso2, RETxtEnvio,
        EdWebService, Sincronia);
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Sucesso!');
        MostraTextoRetorno(FTextoArq);
        //
        DmMDFe_0000.ReopenEmpresa(Empresa);
        LoteStr := FormatFloat('000000000', Lote);
        DmMDFe_0000.SalvaXML(MDFE_EXT_REC_XML, LoteStr, FTextoArq, RETxtRetorno, False);
        //
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
  if FTextoArq <> '' then
    if LerTextoEnvioLoteMDFe((*Sincronia*)) then
      Timer1.Enabled := Sincronia = TXXeIndSinc.nisAssincrono;
end;

procedure TFmMDFeSteps_0100a.ExecutaEvento_IncCondutor;
const
  TipoConsumo = tcwsmdfeEveIncCondutor;
var
  //ID_Res, xJust, Modelo, Serie, LoteStr,
  Id: String;
  Empresa, Lote, K: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando dados a serem enviados ao servidor do fisco');
  Screen.Cursor := CrHourGlass;
  try
    FTextoArq := FmMDFeGeraXML_0100a.WS_EventoIncCondutor(EdUF_Servico.Text,
      FAmbiente_Int, FCodigoUF_Int, FXML_Evento, TipoConsumo,
      RETxtEnvio, EdWebService);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebido com Sucesso!');
    //
    // Salva arquivo
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Salvando resposta');
    //
    ID := EdchMDFeIdC.Text;
    //
    DmMDFe_0000.SalvaXML(MDFE_EXT_EVE_RET_IDC_XML, EdchMDFeIdC.Text, FTextoArq, RETxtRetorno, False);
    //
    LerTextoEvento_IncCondutor();
    //
    if FQry <> nil then
      UnDmkDAC_PF.AbreQuery(FQry, FQry.Database);
    //
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MB_Erro('Erro na chamada do WS...' +  sLineBreak  + FTextoArq);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Erros');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmMDFeSteps_0100a.ExecutaEvento_PedidoDeEncerramento;
const
  TipoConsumo = tcwsmdfeEveEncerramento;
var
  //ID_Res, xJust, Modelo, Serie, LoteStr,
  Id: String;
  Empresa, Lote, K: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando dados a serem enviados ao servidor do fisco');
  Screen.Cursor := CrHourGlass;
  try
    FTextoArq := FmMDFeGeraXML_0100a.WS_EventoEncerramentoMDFe(EdUF_Servico.Text,
      FAmbiente_Int, FCodigoUF_Int, FXML_Evento, TipoConsumo,
      RETxtEnvio, EdWebService);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebido com Sucesso!');
    //
    // Salva arquivo
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Salvando resposta');
    //
    ID := EdchMDFeEnc.Text;
    //
    DmMDFe_0000.SalvaXML(MDFE_EXT_EVE_RET_ENC_XML, EdchMDFeEnc.Text, FTextoArq, RETxtRetorno, False);
    //
    LerTextoEvento_PedidoDeEncerramento();
    //
    if FQry <> nil then
      UnDmkDAC_PF.AbreQuery(FQry, FQry.Database);
    //
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MB_Erro('Erro na chamada do WS...' +  sLineBreak  + FTextoArq);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Erros');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

{
procedure TFmMDFeSteps_0100a.ExecutaInutilizaNumerosMDFe();
var
  ID_Res, Id, xJust, Modelo, Serie, nCTIni, nCTFin, EmitCNPJ, LoteStr: String;
  Empresa, Lote, K: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando dados a serem enviados ao servidor do fisco');
  if not DefineEmpresa(Empresa) then Exit;
  if not DefineLote(Lote) then Exit;
  if not DefineModelo(EdModeloInu.ValueVariant, Modelo) then Exit;
  if not DefineSerie(EdSerieInu.ValueVariant, Serie) then Exit;
  if not DefinenCTIni(nCTIni) then Exit;
  if not DefinenCTFin(nCTFin) then Exit;
  if not DefineEmitCNPJ(EmitCNPJ) then Exit;
  //
  xJust := Trim(XXe_PF.ValidaTexto_XML(CBMDFeJustInu.Text, 'xJust', 'xJust'));
  K := Length(xJust);
  if K < 15 then
  begin
    Geral.MB_Aviso('A justificativa deve ter pelo menos 15 caracteres!' +
    sLineBreak + 'O texto "' + xJust + '" tem apenas ' + IntToStr(K)+'.');
    Exit;
  end;
  xJust := Geral.TFD(FloatToStr(QrMDFeJustInuCodigo.Value), 10, siPositivo) + ' - ' + xJust;
  Screen.Cursor := CrHourGlass;
  try
    FTextoArq := FmMDFeGeraXML_0100a.WS_MDFeInutilizacaoMDFe(EdUF_Servico.Text,
      FAmbiente_Int, FCodigoUF_Int, EdAno.ValueVariant, Id, emitCNPJ, Modelo,
      Serie, nCTIni, nCTFin, XJust, EdSerialNumber.Text, LaAviso1, LaAviso2, RETxtEnvio,
      EdWebService, ID_Res);
    //
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebido com Sucesso!');
    //
    // Salva arquivo
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Salvando resposta');
    //
    //cUF := IntToStr(Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(FSiglaUF));
    DmMDFe_0000.MontaID_Inutilizacao(FCodigoUF_Txt, EdAno.Text, emitCNPJ, Modelo, Serie,
      nCTIni, nCTFin, Id);
    //
    LoteStr := Id;// + '_' + FormatFloat('000000000', Lote);
    DmMDFe_0000.SalvaXML(MDFE_EXT_INU_XML, LoteStr, FTextoArq, RETxtRetorno, False);
    //
    LerTextoInutilizaNumerosMDFe();
    //
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MB_Erro('Erro na chamada do WS...' +  sLineBreak  + FTextoArq);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Erros');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;
}

procedure TFmMDFeSteps_0100a.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMDFeSteps_0100a.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
(*
  FultNSU  := 0;
  FNSU     := 0;
  //
  //Self.Height := 730;
*)
  //
  FTextoArq :='';
  FNaoExecutaLeitura := False;
  FSecWait := 15;
  Timer1.Enabled := False;
  FSegundos := 0;
  //
  PageControl1.ActivePageIndex := 0;
  FSiglas_WS  := Geral.SiglasWebService();
  //FFormChamou := '';
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Configurando conforme solicita��o');
  if ((VAR_LOGIN = 'MASTER') and (VAR_SENHA = CO_MASTER)) then
  begin
    EdLote.Enabled     := True;
    EdEmpresa.Enabled  := True;
    EdRecibo.Enabled   := True;
    //
    BtAbrir.Enabled    := True;
  end;
  //
end;

procedure TFmMDFeSteps_0100a.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMDFeSteps_0100a.HabilitaBotoes(Visivel: Boolean);
begin
  PnConfirma.Visible := Visivel;
end;

function TFmMDFeSteps_0100a.LeNoXML(No: IXMLNode; Tipo: TTipoNoXML;
  Tag: TTipoTagXML; AvisaVersao: Boolean): String;
var
  Texto: String;
begin
  Result := '';
  case Tipo of
    tnxTextStr: Result := GetNodeTextStr(No, ObtemNomeDaTag(Tag), '');
    tnxAttrStr: Result := GetNodeAttrStr(No, ObtemNomeDaTag(Tag), '');
    else Result := '???' + ObtemNomeDaTag(Tag) + '???';
  end;
  //
  if (Tag = ttx_Versao) and (Result <> FverXML_versao) then
    if AvisaVersao then
      Geral.MB_Aviso('Vers�o do XML difere do esperado!' +
      sLineBreak + 'Vers�o informada: ' + Result + sLineBreak +
      'Verifique a vers�o do servi�o no cadastro das op��es da filial para a vers�o: '
      + Result);
  if (Tag = ttx_dhRecbto) then XXe_PF.Ajusta_dh_XXe(Result);
  if Result <> '' then
  begin
    case Tag of
      ttx_tpAmb: Texto := Result + ' - ' + XXe_PF.ObtemNomeAmbiente(Result);
      ttx_tMed : Texto := Result + ' segundos';
      ttx_cUF  : Texto := Result + ' - ' +
                 Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(StrToInt(Result));
      else Texto := Result;
    end;
  end;
  //
  MeInfo.Lines.Add(ObtemDescricaoDaTag(Tag) + ' = ' + Texto);
end;

procedure TFmMDFeSteps_0100a.LerTextoEvento_Cancelamento();
begin
  LerTextoEvento();
end;

procedure TFmMDFeSteps_0100a.LerTextoConsultaLoteMDFe();
var
  Codigo, Controle, FatID, FatNum, Empresa, Status: Integer;
  versao, tpAmb, verAplic, cStat, xMotivo, cUF, nRec, dhRecbto, tMed,
  infProt_Id, infProt_chMDFe, infProt_nProt, infProt_digVal, infProt_tpAmb,
  infProt_verAplic, infProt_dhRecbto, infProt_cStat, infProt_xMotivo: String;
  dhRecbtoTZD, infProt_dhRecbtoTZD: Double;
begin
  FverXML_versao := Geral.FFT_Dot(DmMDFe_0000.QropcoesMDFeMDFeVerConLot.Value, 2, siNegativo);
  dhRecbto       := '0000-00-00';
  dhRecbtoTZD    := 0;
  tMed           := '0';
  //
  if not DefineLote(Codigo) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � recibo de lote de envio
    xmlNode := xmlDoc.SelectSingleNode('/retConsReciMDFe');
    if assigned(xmlNode) then
    begin
      Pagecontrol1.ActivePageIndex := 4;
      versao   := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      //
      tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
      verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      nRec     := LeNoXML(xmlNode, tnxTextStr, ttx_nRec);
      cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
      xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
      //
      Status := Integer(mdfemystatusLoteEnvConsulta);
      Controle := DModG.BuscaProximoCodigoInt('mdfectrl', 'mdfelenm', '', 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'mdfelenm', False, [
        'Codigo', 'versao', 'tpAmb',
        'verAplic', 'cStat', 'xMotivo',
        'cUF', 'nRec', 'dhRecbto', 'tMed',
        'Status', 'dhRecbtoTZD'
      ], ['Controle'], [
        Codigo, versao, tpAmb,
        verAplic, cStat, xMotivo,
        cUF, nRec, dhRecbto, tMed,
        Status, dhRecbtoTZD
      ], [Controle], True) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfelenc', False, [
          'versao', 'tpAmb', 'verAplic',
          'cStat', 'xMotivo', 'cUF',
          'nRec', 'dhRecbto', 'tMed',
          'dhRecbtoTZD'
        ], ['Codigo'], [
          versao, tpAmb, verAplic,
          cStat, xMotivo, cUF,
          nRec, dhRecbto, tMed,
          dhRecbtoTZD
        ], [Codigo], True) then
        begin
          xmlList := xmlDoc.SelectNodes('/retConsReciMDFe/protMDFe/infProt');
          if xmlList.Length > 0 then
          begin
            while xmlList.Length > 0 do
            begin
              //
              infProt_Id       := LeNoXML(xmlList.Item[0], tnxAttrStr, ttx_Id);
              infProt_tpAmb    := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_tpAmb);
              infProt_verAplic := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_verAplic);
              infProt_chMDFe    := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_chMDFe);
              infProt_dhRecbto := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_dhRecbto);
              infProt_nProt    := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_nProt);
              infProt_digVal   := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_digVal);
              infProt_cStat    := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_cStat);
              infProt_xMotivo  := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_xMotivo);
              //
              XXe_PF.Ajusta_dh_XXe_UTC(infProt_dhRecbto, infProt_dhRecbtoTZD);
              //
              if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfecaba', False, [
                'Status', 'infProt_Id', 'infProt_tpAmb',
                'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
                'infProt_digVal', 'infProt_cStat', 'infProt_xMotivo',
                'protMDFe_versao', 'infProt_dhRecbtoTZD'
              ], ['ID', 'LoteEnv'], [
                infProt_cStat, infProt_Id, infProt_tpAmb,
                infProt_verAplic, infProt_dhRecbto, infProt_nProt,
                infProt_digVal, infProt_cStat, infProt_xMotivo,
                versao, infProt_dhRecbtoTZD
              ], [infProt_chMDFe, Codigo], True) then
              begin
                DmMDFe_0000.DefineSituacaoEncerramento(infProt_chMDFe);
                // hist�rico da NF
                QrMDFeCabA1.Close;
                QrMDFeCabA1.Params[00].AsString  := infProt_chMDFe;
                QrMDFeCabA1.Params[01].AsInteger := Codigo;
                QrMDFeCabA1.Open;
                if QrMDFeCabA1.RecordCount > 0 then
                begin
                  FatID       := QrMDFeCabA1FatID.Value;
                  FatNum      := QrMDFeCabA1FatNum.Value;
                  Empresa     := QrMDFeCabA1Empresa.Value;
                  //
                  Controle := DModG.BuscaProximoCodigoInt(
                    'mdfectrl', 'mdfecabamsg', '', 0);
                  //
                  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'mdfecabamsg', False, [
                  'FatID', 'FatNum', 'Empresa', 'Solicit',
                  'Id', 'tpAmb', 'verAplic',
                  'dhRecbto', 'nProt', 'digVal',
                  'cStat', 'xMotivo', '_Ativo_',
                  'dhRecbtoTZD'], [
                  'Controle'], [
                  FatID, FatNum, Empresa, 100(*autoriza��o*),
                  infProt_Id, infProt_tpAmb, infProt_verAplic,
                  infProt_dhRecbto, infProt_nProt, infProt_digVal,
                  infProt_cStat, infProt_xMotivo, 1,
                  infProt_dhRecbtoTZD], [
                  Controle], True);
                end else Geral.MB_Aviso('O Manifesto de chave "' +
                infProt_chMDFe +
                '" n�o foi localizada e ficara sem o hist�rico desta consulta!');
              end;
              //
              xmlList.Remove(xmlList.Item[0]);
              //
            end;
          end
        end;
      end;
      //
      DmMDFe_0000.AtualizaXML_No_BD_Tudo(False);
      //
    end else Geral.MB_Aviso('Arquivo XML n�o conhecido ou n�o implementado! [6]');
  end;
end;

procedure TFmMDFeSteps_0100a.LerTextoConsultaMDFe();
var
  IDCtrl, Controle, FatID, FatNum, Empresa: Integer;
  tpAmb, verAplic, cStat, xMotivo, cUF, dhRecbto, Id, nProt,
  chMDFe, digVal, cJust, xJust, _Stat, _Motivo, tpEvento, dhEvento: String;
  //
  Status, Evento, nCondUso, infCCe_CCeConta: Integer;
  infProt_dhRecbtoTZD, dhRecbtoTZD, infCanc_dhRecbtoTZD, dhEventoTZD: Double;
  //
  cOrgao, dhRegEvento, xEvento, nSeqEvento, verEvento, CNPJ, CPF: String;
  //
  infCCe_verAplic, infCCe_dhRegEvento, infCCe_nProt,
  infCCe_CNPJ, infCCe_CPF, infCCe_chMDFe, infCCe_dhEvento: String;
  infCCe_cOrgao, infCCe_tpAmb, infCCe_tpEvento, infCCe_nSeqEvento, infCCe_cStat,
  infCCe_nCondUso: Integer;
  infCCe_verEvento, infCCe_dhRegEventoTZD, infCCe_dhEventoTZD: Double;
  //
  _CNPJ, _CPF: String;
  _dhEvento: TDateTime;
  _dhEventoTZD, _verEvento: Double;
  MDFeEveRCCeConta: Integer;
  //
  versao: String;
begin
  FverXML_versao := Geral.FFT_Dot(DmMDFe_0000.QrOpcoesMDFeMDFeVerConMDFe.Value, 2, siNegativo);
  if not DefineEmpresa(Empresa) then Exit;
  if not DefinechMDFe(EdChaveMDFeMDFe, chMDFe) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    if DefineIDCtrl(EdIDCtrlMDFe, IDCtrl) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeCabA2, Dmod.MyDB, [
      'SELECT * ',
      'FROM mdfecaba ',
      'WHERE IDCtrl=' + Geral.FF0(IDCtrl),
      '']);
      if QrMDFeCabA2.RecordCount > 0 then
      begin
        // Verifica se � recibo de consulta de MDFe
        xmlNode := xmlDoc.SelectSingleNode('/retConsSitMDFe');
        if not assigned(xmlNode) then
          Geral.MB_Aviso(
          'Arquivo XML n�o possui informa��es de Autoriza��o ou Cancelamento de MDF-e!')
        else
        begin
          tpAmb    := '';
          verAplic := '';
          cStat    := '';
          xMotivo  := '';
          cUF      := '';
          chMDFe    := '';
          dhRecbto := '';
          nProt    := '';
          digVal   := '';
          cJust    := '';
          xJust    := '';
          //
          PageControl1.ActivePageIndex := 4;
          //Id       := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
          tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
          verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
          cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
          xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
          cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
          if (Geral.IMV(cStat) in ([100, 101, 110, 132]))
          or (Geral.IMV(cStat) = 609) then
          begin
            xmlNode := xmlDoc.SelectSingleNode('/retConsSitMDFe/protMDFe');
            if assigned(xmlNode) then
            begin
              versao := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
              xmlNode := xmlDoc.SelectSingleNode('/retConsSitMDFe/protMDFe/infProt');
              if assigned(xmlNode) then
              begin
                //colocar aqui info de 100
                Pagecontrol1.ActivePageIndex := 4;
                Id       := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
                tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
                verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
                chMDFe    := LeNoXML(xmlNode, tnxTextStr, ttx_chMDFe);
                dhRecbto := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
                nProt    := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
                digVal   := LeNoXML(xmlNode, tnxTextStr, ttx_digVal);
                _Stat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
                _Motivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
                //
                if (Geral.IMV(cStat) in ([100, 101, 110, 132]))
                or (Geral.IMV(cStat) = 609) then
                begin
                  if (chMDFe <> QrMDFeCabA2Id.Value) then
                  begin
                    Geral.MB_Erro('Chave da MDF-e n�o comdfere: ' + sLineBreak +
                    'No XML: ' + chMDFe + sLineBreak +
                    'No BD: ' + QrMDFeCabA2Id.Value);
                    //
                    Exit;
                  end else
                  begin
                    if cStat = '609' then
                      cStat := '132';
                    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfecaba', False, [
                      'Status'], ['ID'], [cStat], [chMDFe], True);
                  end;
                  if Geral.IMV(_Stat) = 100 then
                  begin
                    //
                    XXe_PF.Ajusta_dh_XXe_UTC(dhRecbto, infProt_dhRecbtoTZD);
                    //
                    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfecaba', False, [
                      'infProt_Id', 'infProt_tpAmb',
                      'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
                      'infProt_digVal', 'infProt_cStat', 'infProt_xMotivo',
                      'infProt_dhRecbtoTZD'
                    ], ['ID'], [
                      Id, tpAmb,
                      verAplic, dhRecbto, nProt,
                      digVal, _Stat, _Motivo,
                      infProt_dhRecbtoTZD
                    ], [chMDFe], True);
                    DmMDFe_0000.DefineSituacaoEncerramento(chMDFe);
                  end;
                  //
                end;
              end;
            end;
{
            // Cancelamento Nao Tem!!!
            xmlNode := xmlDoc.SelectSingleNode('/retConsSitMDFe/retCancMDFe/infCanc');
            if assigned(xmlNode) then
            begin
              //colocar aqui info de 101
              Pagecontrol1.ActivePageIndex := 4;
              Id       := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
              tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
              verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
              _Stat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
              _Motivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
              cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
              //
              if Geral.IMV(_Stat) = 101 then
              begin
                //tMed     := LeNoXML(xmlNode, tnxTextStr, ttx_tMed);
                chMDFe    := LeNoXML(xmlNode, tnxTextStr, ttx_chMDFe);
                dhRecbto := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
                nProt    := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
                digVal   := LeNoXML(xmlNode, tnxTextStr, ttx_digVal);
                cJust    := LeNoXML(xmlNode, tnxTextStr, ttx_cJust);
                xJust    := LeNoXML(xmlNode, tnxTextStr, ttx_xJust);
                //
                XXe_PF.Ajusta_dh_XXe_UTC(dhRecbto, infProt_dhRecbtoTZD);
                //
                UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfecaba', False, [
                  'nfCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
                  'nfCanc_dhRecbto', 'infCanc_nProt', 'infCanc_digVal',
                  'nfCanc_cStat', 'infCanc_xMotivo', 'infCanc_cJust',
                  'nfCanc_xJust', 'infCanc_dhRecbtoTZD'
                ], ['ID'], [
                  Id, tpAmb, verAplic,
                  dhRecbto, nProt, digVal,
                  _Stat, _Motivo, cJust,
                  xJust, infProt_dhRecbtoTZD
                ], [chMDFe], True);
              end;
            end;
}

////////////////////////////////////////////////////////////////////////////////
            // E V E N T O S
            xmlList := xmlDoc.SelectNodes('/retConsSitMDFe/procEventoMDFe');
            LerXML_procEventoMDFe(xmlList, digVal, cStat, dhRecbto);
          end else
            Geral.MB_Erro('C�digo de retorno ' + cStat + ' - ' +
            MDFe_PF.Texto_StatusMDFe(Geral.IMV(cStat), 0) + sLineBreak +
            'N�o esperado na consulta!');
        end;
      end else Geral.MB_Aviso('A Nota Fiscal de chave "' + chMDFe +
      '" n�o foi localizada e ficar� sem defini��o de Autoriza��o ou Cancelamento DESTA CONSULTA!');
      if Geral.IMV(cStat) > 0 then
      begin
        if DefineIDCtrl(EdIDCtrlMDFe, IDCtrl) then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeCabA2, Dmod.MyDB, [
          'SELECT * ',
          'FROM mdfecaba ',
          'WHERE IDCtrl=' + Geral.FF0(IDCtrl),
          '']);
            //
          if QrMDFeCabA2.RecordCount > 0 then
          begin
            // hist�rico da NF
            FatID   := QrMDFeCabA2FatID.Value;
            FatNum  := QrMDFeCabA2FatNum.Value;
            Empresa := QrMDFeCabA2Empresa.Value;
            dhRecbto    := '0000-00-00 00:00:00';
            dhRecbtoTZD := infProt_dhRecbtoTZD;
            //
            Controle := DModG.BuscaProximoCodigoInt(
              'mdfectrl', 'mdfecabamsg', '', 0);
            //
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'mdfecabamsg', False, [
            'FatID', 'FatNum', 'Empresa', 'Solicit',
            'Id', 'tpAmb', 'verAplic',
            'dhRecbto', 'nProt', 'digVal',
            'cStat', 'xMotivo', '_Ativo_',
            'dhRecbtoTZD'], [
            'Controle'], [
            FatID, FatNum, Empresa, 100(*homologa��o*),
            Id, tpAmb, verAplic,
            dhRecbto, nProt, digVal,
            cStat, xMotivo, 1,
            dhRecbtoTZD], [
            Controle], True) then
            begin
              // Mostrar
              //Para evitar erros quando aberto em aba FmMDFe_Pesq_0000.PageControl1.ActivePageIndex := 1;
            end;
          end;
          //end;
        end else Geral.MB_Aviso(
        'O Status retornou zerado e a Nota Fiscal de chave "' + chMDFe +
        '" ficar� sem o hist�rico desta consulta!');
        //
      end else Geral.MB_Aviso(
      'Arquivo XML n�o possui informa��es de Autoriza��o ou Cancelamento de MDF-e!');
    end else Geral.MB_Aviso('Arquivo XML n�o conhecido ou n�o implementado! [7]');
  end;
  try
    (*
    Para evitar erros quando aberto em aba
    if IDCtrl <> 0 then
      FmMDFe_Pesq_0000.ReopenMDFeCabA(IDCtrl, False);
    *)
  except
    //
  end;
  //Close;
end;

procedure TFmMDFeSteps_0100a.LerTextoConsultaNaoEncerrados();
var
  infProt_chMDFe: String;
begin
  FverXML_versao := Geral.FFT_Dot(DmMDFe_0000.QropcoesMDFeMDFeVerNaoEnc.Value, 2, siNegativo);
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � status de servi�o
    xmlNode := xmlDoc.SelectSingleNode('/retConsMDFeNaoEnc');
    if assigned(xmlNode) then
    begin
      LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
      LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
      LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
      LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
      //
      xmlList := xmlDoc.SelectNodes('/retConsMDFeNaoEnc/infMDFe');
      if xmlList.Length > 0 then
      begin
        while xmlList.Length > 0 do
        begin
          //
          infProt_chMDFe    := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_chMDFe);
          MeInfo.Lines.Add(ObtemDescricaoDaTag(ttx_chMDFe) + ' = ' + infProt_chMDFe);
          //
          xmlList.Remove(xmlList.Item[0]);
          //
        end;
      end;
      Pagecontrol1.ActivePageIndex := 4;
    end else Geral.MB_Aviso(
    'Arquivo XML n�o conhecido ou n�o implementado! [4]' + sLineBreak +
    'Texto XML:' + sLineBreak +
    FTextoArq);
  end;
end;

function TFmMDFeSteps_0100a.LerTextoEnvioLoteMDFe: Boolean;
  function CorrigeDataStringVazio(Data: String): String;
  begin
    if Data = '' then
      Result := '0000-00-00'
    else
      Result := Data;
  end;
var
  Status, Codigo, Controle, Empresa: Integer;
  versao, tpAmb, verAplic, cStat, xMotivo, cUF, nRec, dhRecbto, tMed: String;
  //
  FatID, FatNum: Integer;
  infProt_Id, infProt_chMDFe, infProt_nProt, infProt_digVal, infProt_tpAmb,
  infProt_verAplic, infProt_dhRecbto, infProt_cStat, infProt_xMotivo: String;
  infProt_dhRecbtoTZD, dhRecbtoTZD: Double;
  Sincronia: TXXeIndSinc;
  Dir, Aviso: String;
  IDCtrl: Integer;
begin
  FverXML_versao := Geral.FFT_Dot(DmMDFe_0000.QropcoesMDFeMDFeVerEnvLot.Value, 2, siNegativo);
  Result := False;
  if not DefineLote(Codigo) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � envio de lote
    xmlNode := xmlDoc.SelectSingleNode('/retEnviMDFe');
    if assigned(xmlNode) then
    begin
      Pagecontrol1.ActivePageIndex := 4;
(*
Arquivo XML n�o conhecido ou n�o implementado:
<retEnviMDFe versao='2.00' xmlns='http://www.portalfiscal.inf.br/mdfe'>
  <tpAmb>2</tpAmb>
  <cUF>41</cUF>
  <verAplic>PR-v2_1_21</verAplic>
  <cStat>103</cStat>
  <xMotivo>Lote recebido com sucesso</xMotivo>
  <infRec>
    <nRec>411000000672763</nRec>
    <dhRecbto>2015-12-19T20:15:34</dhRecbto>
    <tMed>1</tMed>
  </infRec>
</retEnviMDFe>*)
      versao   := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      //
      tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
      verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
      xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
      //
      xmlNode := xmlDoc.SelectSingleNode('/retEnviMDFe/protMDFe');
      if assigned(xmlNode) then
      begin
        Sincronia := nisSincrono;
        Geral.MB_Aviso('Envio de lote s�ncrono n�ao implementado!');
{
(*==============================================================================
      MDFe Sincrona
================================================================================
- <protMDFe versao="3.10">
- <infProt Id="ID141140001620054">
  <tpAmb>2</tpAmb>
  <verAplic>PR-v3_2_1</verAplic>
  <chMDFe>41141002717861000110550010000045811360413650</chMDFe>
  <dhRecbto>2014-10-22T00:53:41-02:00</dhRecbto>
  <nProt>141140001620054</nProt>
  <digVal>gzIavxRcIKOO6WbOu2M+HwEH0Cg=</digVal>
  <cStat>100</cStat>
  <xMotivo>Autorizado o uso da MDF-e</xMotivo>
  </infProt>
  </protMDFe>
  </retEnviMDFe>
*)
        xmlNode := xmlDoc.SelectSingleNode('/retEnviMDFe/protMDFe/infProt');
        if assigned(xmlNode) then
        begin
          infProt_Id       := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
          infProt_tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
          infProt_verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
          infProt_chMDFe    := LeNoXML(xmlNode, tnxTextStr, ttx_chMDFe);
          infProt_dhRecbto := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
          infProt_nProt    := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
          infProt_digVal   := LeNoXML(xmlNode, tnxTextStr, ttx_digVal);
          infProt_cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
          infProt_xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
          //
          XXe_PF.Ajusta_dh_XXe_UTC(infProt_dhRecbto, infProt_dhRecbtoTZD);
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfecaba', False, [
            'Status', 'infProt_Id', 'infProt_tpAmb',
            'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
            'infProt_digVal', 'infProt_cStat', 'infProt_xMotivo',
            'protMDFe_versao', 'infProt_dhRecbtoTZD'
          ], ['ID', 'LoteEnv'], [
            infProt_cStat, infProt_Id, infProt_tpAmb,
            infProt_verAplic, infProt_dhRecbto, infProt_nProt,
            infProt_digVal, infProt_cStat, infProt_xMotivo,
            versao, infProt_dhRecbtoTZD
          ], [infProt_chMDFe, Codigo], True) then
          begin
            Result := True;
            // hist�rico da NF
            QrMDFeCabA1.Close;
            QrMDFeCabA1.Params[00].AsString  := infProt_chMDFe;
            QrMDFeCabA1.Params[01].AsInteger := Codigo;
            QrMDFeCabA1.Open;
            if QrMDFeCabA1.RecordCount > 0 then
            begin
              FatID   := QrMDFeCabA1FatID.Value;
              FatNum  := QrMDFeCabA1FatNum.Value;
              Empresa := QrMDFeCabA1Empresa.Value;
              //
              Controle := DModG.BuscaProximoCodigoInt(
                'mdfectrl', 'mdfecabamsg', '', 0);
              //
              UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'mdfecabamsg', False, [
              'FatID', 'FatNum', 'Empresa', 'Solicit',
              'Id', 'tpAmb', 'verAplic',
              'dhRecbto', 'nProt', 'digVal',
              'cStat', 'xMotivo', 'dhRecbtoTZD',
              '_Ativo_'], [
              'Controle'], [
              FatID, FatNum, Empresa, 100(*autoriza��o*),
              infProt_Id, infProt_tpAmb, infProt_verAplic,
              infProt_dhRecbto, infProt_nProt, infProt_digVal,
              infProt_cStat, infProt_xMotivo, infProt_dhRecbtoTZD,
              1], [
              Controle], True);
            end else Geral.MB_Aviso('O Manifesto de chave "' +
            infProt_chMDFe +
            '" n�o foi localizada e ficara sem o hist�rico desta consulta!');
            //
            Dir := DModG.QrParamsEmpDirRec.Value;
            Aviso := '';
            IDCtrl := QrMDFeCabA1IDCtrl.Value;
            //DmMDFe_0000.AtualizaXML_No_BD_Aut_Sinc(IDCtrl, Codigo, infProt_Id, Dir, Aviso);
            DmMDFe_0000.AtualizaXML_No_BD_Aut_Sinc(IDCtrl, Codigo, infProt_chMDFe, Dir, Aviso);
            //
            //DmMDFe_0000.AtualizaXML_No_BD_MDFe(QrMDFeCabA1IDCtrl.Value, Dir, Aviso);
            //DmMDFe_0000.AtualizaXML_No_BD_Tudo(False);
            //
           if Aviso <> '' then Geral.MB_Aviso(
            'Os arquivos abaixo n�o foram localizados:' + sLineBreak + Aviso);
          end;
          // MDFeLEnM
          // Conforme Manual MDFe 3.10:
          // C. Processamento S�ncrono
          //No caso de processamento s�ncrono do Lote de MDF-e, as valida��es da
          //MDF-e ser�o feitas na sequ�ncia, sem a gera��o de um N�mero de Recibo.
          nRec      := 'MDFe s�ncrona';
          dhRecbto := infProt_dhRecbto;
          tMed      := '0';
          //
        end else Geral.MB_Erro('Protocolo de MDFe sem inform��es!');
(*==============================================================================
      FIM  MDFe 3.10  - MDFe Sincrona
==============================================================================*)
}
     end else
      begin
        Sincronia := nisAssincrono;
        xmlNode := xmlDoc.SelectSingleNode('/retEnviMDFe/infRec');
        if assigned(xmlNode) then
        begin
          nRec      := LeNoXML(xmlNode, tnxTextStr, ttx_nRec);
          dhRecbto  := CorrigeDataStringVazio(LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto));
          tMed      := LeNoXML(xmlNode, tnxTextStr, ttx_tMed);
        end else begin
          nRec      := '';
          dhRecbto  := CorrigeDataStringVazio('');
          tMed      := '0';
        end;
      end;
      //
      XXe_PF.Ajusta_dh_XXe_UTC(dhRecbto, dhRecbtoTZD);
      //
      Controle := DModG.BuscaProximoCodigoInt('mdfectrl', 'mdfelenm', '', 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'mdfelenm', False, [
        'Codigo', 'versao', 'tpAmb',
        'verAplic', 'cStat', 'xMotivo',
        'cUF', 'nRec', 'dhRecbto', 'tMed',
        'Status', 'dhRecbtoTZD'
      ], ['Controle'], [
        Codigo, versao, tpAmb,
        verAplic, cStat, xMotivo,
        cUF, nRec, dhRecbto, tMed,
        Integer(mdfemystatusLoteEnvEnviado), dhRecbtoTZD
      ], [Controle], True) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfelenc', False, [
          'versao', 'tpAmb', 'verAplic',
          'cStat', 'xMotivo', 'cUF',
          'nRec', 'dhRecbto', 'tMed'
        ], ['Codigo'], [
          versao, tpAmb, verAplic,
          cStat, xMotivo, cUF,
          nRec, dhRecbto, tMed
        ], [Codigo], True) then
        begin
          // CUIDADO!!!!  Somente se for lote Assincrono!!
          // Se fizer no sincrono a MDFe mesmo autorizada fimca Status = 40 !!!
          if Sincronia = nisAssincrono then
          begin
            Status := Geral.IMV(cStat);
            //
            if Status <> 103 then
              Status := Integer(mdfemystatusLoteRejeitado);
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfecaba', False, [
              'Status'], ['LoteEnv', 'Empresa'], [
              Status], [Codigo, Empresa], True);
          end;
          Result := True;
        end;
      end;
    end else
      Geral.MB_Aviso('Arquivo XML n�o conhecido ou n�o implementado:' +
      sLineBreak + FTextoArq);
  end;
  //
  if FQry <> nil then
    UnDmkDAC_PF.AbreQuery(FQry, FQry.Database)
  else
    Geral.MB_Aviso(
    'Tabela n�o definida para localizar o lote de MDF-e(s) n�mero ' +
    EdLote.Text + '!');
end;

procedure TFmMDFeSteps_0100a.LerTextoEvento_IncCondutor;
begin
  LerTextoEvento();
end;

procedure TFmMDFeSteps_0100a.LerTextoEvento_PedidoDeEncerramento();
begin
  LerTextoEvento();
end;

function TFmMDFeSteps_0100a.LerTextoEvento(): Boolean;
var
  cJust, Status, Codigo, Controle, Empresa: Integer;
  //idLote, tpAmb, verAplic, cOrgao, cStat, xMotivo: String;
  versao, infEvento_Id, infEvento_tpAmb, infEvento_verAplic, infEvento_cOrgao,
  infEvento_cStat, infEvento_xMotivo, infEvento_chMDFe, infEvento_tpEvento,
  infEvento_xEvento, infEvento_CNPJDest, infEvento_CPFDest, infEvento_emailDest,
  infEvento_nSeqEvento, infEvento_dhRegEvento, infEvento_nProt, XML_retEve,
  infEvento_versao: String;
  //
var
  infEnc_verAplic, infEnc_CNPJ, (*infEnc_CPF,*) infEnc_chMDFe, (*infEnc_dhEvento,*)
  infEnc_dtEnc, infEnc_dhRegEvento, infEnc_nProt, infEnc_Id: String;
  infEnc_cOrgao, infEnc_tpAmb, infEnc_tpEvento, infEnc_nSeqEvento, infEnc_cUF,
  infEnc_cMun, infEnc_cStat: Integer;
  (*infEnc_dhEventoTZD,*) infEnc_verEvento(*, infEnc_dhRegEventoTZD*): Double;
    //
  infIdC_verAplic, infIdC_CNPJ, infIdC_chMDFe, infIdC_xNome, infIdC_CPF,
  infIdC_dhRegEvento, infIdC_nProt, infIdC_Id: String;
  infIdC_cOrgao, infIdC_tpAmb, infIdC_tpEvento, infIdC_nSeqEvento,
  infIdC_cStat: Integer;
  infIdC_verEvento: Double;
  //
  infCanc_Id, infCanc_tpAmb, infCanc_verAplic, infCanc_dhRecbto,
  infCanc_nProt, infCanc_digVal, infCanc_xMotivo,
  infCanc_xJust, retCancMDFe_versao: String;
  ret_TZD_UTC, eveMDFe_TZD_UTC: Double;
  infCanc_cJust, tpEvento, nSeqEvento, SubCtrl, infCanc_cStat,
  cSitEnc: Integer;
  //cSitMDFe: Integer;
  Id, xJust: String;
  SQLType: TSQLType;
  infCanc_dhRecbtoTZD: Double;
  //
  CNPJ, CPF, nProt, xNome: String;
  dhEvento, dtEnc: TDateTime;
  dhEventoTZD, verEvento: Double;
  nCondUso, cUF, cMun: Integer;
begin
  //FverXML_versao := verEnviEvento_Versao;
  FverXML_versao := Geral.FFT_Dot(DmMDFe_0000.QropcoesMDFeMDFeVerLotEve.Value, 2, siNegativo);
  Result := False;
  //if not DefineLote(Codigo) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � envio de lote
    //xmlNode := xmlDoc.SelectSingleNode('/retEnvEvento');
    // Verifica se � evento de MDFe
    xmlNode := xmlDoc.SelectSingleNode('/retEventoMDFe');
    if assigned(xmlNode) then
    begin
(*
<?xml version="1.0"?>
-<retEventoMDFe xmlns="http://www.portalfiscal.inf.br/mdfe" versao="2.00">
  -<infEvento>
    <tpAmb>2</tpAmb>
    <verAplic>PR-v2_1_21</verAplic>
    <cOrgao>41</cOrgao>
    <cStat>135</cStat>
    <xMotivo>Evento registrado e vinculado a MDF-e</xMotivo>
    <chMDFe>41151202717861000110570000000000161704437136</chMDFe>
    <tpEvento>110111</tpEvento>
    <xEvento>Cancelamento</xEvento>
    <nSeqEvento>1</nSeqEvento>
    <dhRegEvento>2015-12-26T15:09:50</dhRegEvento>
    <nProt>141150000218474</nProt>
  </infEvento>
</retEventoMDFe>
*)
      //Pagecontrol1.ActivePageIndex := 4;
      versao   := LeNoXML(xmlNode, tnxAttrStr, ttx_versao, False);
      //Geral.MB_Info('***versao***:' + Versao);
      //
(*
      idLote   := LeNoXML(xmlNode, tnxTextStr, ttx_idLote);
      tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
      verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      cOrgao   := LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao);
      cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
      xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      //
      ///xmlNode := xmlDoc.SelectSingleNode('/retEnvEvento/retEvento');
      Controle := DModG.BuscaProximoCodigoInt('mdfectrl', 'mdfeeverlor', '', 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'mdfeeverlor', False, [
        'Codigo', 'versao', 'tpAmb',
        'verAplic', 'cOrgao',
        'cStat', 'xMotivo'
      ], ['Controle'], [
        Codigo, versao, tpAmb,
        verAplic, cOrgao,
        cStat, xMotivo
      ], [Controle], True) then
*)
      begin
(*        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfeeverloe', False, [
          'versao', 'tpAmb', 'verAplic',
          'cOrgao', 'cStat', 'xMotivo'
        ], ['Codigo'], [
          versao, tpAmb, verAplic,
          cOrgao, cStat, xMotivo
        ], [Codigo], True) then
*)
        begin
(*
          xmlList := xmlDoc.SelectNodes('/retEnvEvento/retEvento');
          //xmlList := xmlDoc.SelectNodes('/retEnvEvento/retEvento/infEvento');
          if xmlList.Length > 0 then
*)
          begin
(*
            //I := -1;
            while xmlList.Length > 0 do
*)
            //begin
(*
              //I := I + 1;
              infEvento_versao := LeNoXML(xmlList.Item[0], tnxAttrStr, ttx_versao, False);
              XML_retEve            := xmlList.Item[0].XML;
              //
*)
              //xmlNode := xmlList.Item[0].SelectSingleNode('/retEnvEvento/retEvento/infEvento');
              //xmlNode := xmlList.Item[0].FirstChild;
              //xmlList.Item[0].SelectSingleNode('/retEnvEvento/retEvento/infEvento', xmlNode);
              xmlNode := xmlDoc.SelectSingleNode('/retEventoMDFe/infEvento');
              if assigned(xmlNode) then
              begin
(*
<?xml version="1.0"?>
-<retEventoMDFe xmlns="http://www.portalfiscal.inf.br/mdfe" versao="2.00">
  -<infEvento>
    <tpAmb>2</tpAmb>
    <verAplic>PR-v2_1_21</verAplic>
    <cOrgao>41</cOrgao>
    <cStat>135</cStat>
    <xMotivo>Evento registrado e vinculado a MDF-e</xMotivo>
    <chMDFe>41151202717861000110570000000000161704437136</chMDFe>
    <tpEvento>110111</tpEvento>
    <xEvento>Cancelamento</xEvento>
    <nSeqEvento>1</nSeqEvento>
    <dhRegEvento>2015-12-26T15:09:50</dhRegEvento>
    <nProt>141150000218474</nProt>
  </infEvento>
</retEventoMDFe>
*)
                infEvento_Id          := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
                //
                infEvento_tpAmb       := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
                infEvento_verAplic    := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
                infEvento_cOrgao      := LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao);
                infEvento_cStat       := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
                infEvento_xMotivo     := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
                infEvento_chMDFe       := LeNoXML(xmlNode, tnxTextStr, ttx_chMDFe);
                infEvento_tpEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
                infEvento_xEvento     := LeNoXML(xmlNode, tnxTextStr, ttx_xEvento);
                //infEvento_CNPJDest    := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJDest);
                //infEvento_CPFDest     := LeNoXML(xmlNode, tnxTextStr, ttx_CPFDest);
                //infEvento_emailDest   := LeNoXML(xmlNode, tnxTextStr, ttx_emailDest);
                infEvento_nSeqEvento  := LeNoXML(xmlNode, tnxTextStr, ttx_nSeqEvento);
                infEvento_dhRegEvento := LeNoXML(xmlNode, tnxTextStr, ttx_dhRegEvento);
                infEvento_nProt       := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
                //
              //
              XXe_PF.Ajusta_dh_XXe_UTC(infEvento_dhRegEvento, ret_TZD_UTC);
              //
              Status := Geral.IMV(infEvento_cStat);

              tpEvento := Geral.IMV(infEvento_tpEvento);
              nSeqEvento := Geral.IMV(infEvento_nSeqEvento);
              //N�O LOCALIZA DIREITO
              Controle := DmMDFe_0000.EventoObtemCtrl(Codigo, tpEvento,
                nSeqEvento, infEvento_chMDFe);
              //
              if Controle <> 0 then
              begin
                infEvento_versao := versao;
                XML_retEve       := FTextoArq;
                //
                if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfeevercab', False, [
                'XML_retEve',
                'Status', 'ret_versao', 'ret_Id',
                'ret_tpAmb', 'ret_verAplic', 'ret_cOrgao',
                'ret_cStat', 'ret_xMotivo', 'ret_chMDFe',
                'ret_tpEvento', 'ret_xEvento', 'ret_nSeqEvento',
                'ret_CNPJDest', 'ret_CPFDest', 'ret_emailDest',
                'ret_dhRegEvento', 'ret_TZD_UTC', 'ret_nProt'], [
                (*'FatID', 'FatNum', 'Empresa',*) 'Controle'], [
                Geral.WideStringToSQLString(XML_retEve), Status,
                (*ret_versao*)infEvento_versao, (*ret_Id*)infEvento_Id,
                (*ret_tpAmb*)infEvento_tpAmb, (*ret_verAplic*)infEvento_verAplic,
                (*ret_cOrgao*)infEvento_cOrgao, (*ret_cStat*)infEvento_cStat,
                (*ret_xMotivo*)infEvento_xMotivo, (*ret_chMDFe*)infEvento_chMDFe,
                (*ret_tpEvento*)infEvento_tpEvento, (*ret_xEvento*)infEvento_xEvento,
                (*ret_nSeqEvento*)infEvento_nSeqEvento, (*ret_CNPJDest*)infEvento_CNPJDest,
                (*ret_CPFDest*)infEvento_CPFDest, (*ret_emailDest*)infEvento_emailDest,
                (*ret_dhRegEvento*)infEvento_dhRegEvento, ret_TZD_UTC,
                (*ret_nProt*)infEvento_nProt], [
                (*FatID, FatNum, Empresa,*) Controle], True) then
                begin
                  // hist�rico
                  //SubCtrl := DModG.BuscaProximoCodigoInt('mdfectrl', 'mdfeeverret', '', 0);
                  DmMDFe_0000.EventoObtemSub(Controle, infEvento_Id, SubCtrl, SQLType);

                  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'mdfeeverret', False, [
                  'Controle', 'ret_versao', 'ret_Id', 'ret_tpAmb',
                  'ret_verAplic', 'ret_cOrgao', 'ret_cStat',
                  'ret_xMotivo', 'ret_chMDFe', 'ret_tpEvento',
                  'ret_xEvento', 'ret_nSeqEvento', 'ret_CNPJDest',
                  'ret_CPFDest', 'ret_emailDest', 'ret_dhRegEvento',
                  'ret_TZD_UTC', 'ret_nProt'], [
                  'SubCtrl'], [
                  Controle, (*ret_versao*)infEvento_versao, (*ret_Id*)infEvento_Id,
                  (*ret_tpAmb*)infEvento_tpAmb, (*ret_verAplic*)infEvento_verAplic,
                  (*ret_cOrgao*)infEvento_cOrgao, (*ret_cStat*)infEvento_cStat,
                  (*ret_xMotivo*)infEvento_xMotivo, (*ret_chMDFe*)infEvento_chMDFe,
                  (*ret_tpEvento*)infEvento_tpEvento, (*ret_xEvento*)infEvento_xEvento,
                  (*ret_nSeqEvento*)infEvento_nSeqEvento, (*ret_CNPJDest*)infEvento_CNPJDest,
                  (*ret_CPFDest*)infEvento_CPFDest, (*ret_emailDest*)infEvento_emailDest,
                  (*ret_dhRegEvento*)infEvento_dhRegEvento, ret_TZD_UTC,
                  (*ret_nProt*)infEvento_nProt], [
                  SubCtrl], True) then
                  begin
                    case Geral.IMV(infEvento_tpEvento) of
                      CO_COD_evexxe110111Can: // Cancelamento
                      begin
                        //
                        if infEvento_cStat = '135' then
                        begin
                          if not DmMDFe_0000.LocalizaMDFeInfoEveCan(
                            infEvento_chMDFe, Id, xJust, cJust)
                          then
                            Geral.MB_Erro('Falha ao localizar dados do cancelamento');
                          //
                          Status              := 101;
                          infCanc_Id          := Id;
                          infCanc_tpAmb       := infEvento_tpAmb;
                          infCanc_verAplic    := infEvento_verAplic;
                          infCanc_dhRecbto    := infEvento_dhRegEvento;
                          infCanc_dhRecbtoTZD := ret_TZD_UTC;
                          infCanc_nProt       := infEvento_nProt;
                          infCanc_digVal      := '';//;N�o vem
                          infCanc_cStat       := 101;
                          infCanc_xMotivo     := 'Cancelamento de MDF-e homologado';
                          infCanc_cJust       := cJust;
                          infCanc_xJust       := xJust;
                          retCancMDFe_versao   := infEvento_versao;
                          //
                          Id                  := infEvento_chMDFe;
                          //
                          //if not DmMDFe_0000.CancelaFaturamento(Id) then
                            //Geral.MB_Aviso('Falha ao atualizar o faturamento!');
                          //
                          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfecaba', False, [
                            'Status', 'infCanc_Id', 'infCanc_tpAmb',
                            'infCanc_verAplic', 'infCanc_dhRecbto', 'infCanc_nProt',
                            'infCanc_digVal', 'infCanc_cStat', 'infCanc_xMotivo',
                            'infCanc_cJust', 'infCanc_xJust', 'retCancMDFe_versao',
                            'infCanc_dhRecbtoTZD'
                          ], ['Id'], [
                            Status, infCanc_Id, infCanc_tpAmb,
                            infCanc_verAplic, infCanc_dhRecbto, infCanc_nProt,
                            infCanc_digVal, infCanc_cStat, infCanc_xMotivo,
                            infCanc_cJust, infCanc_xJust, retCancMDFe_versao,
                            infCanc_dhRecbtoTZD
                          ], [infEvento_chMDFe], True) then
                            DmMDFe_0000.DefineSituacaoEncerramento(infEvento_chMDFe);
                        end;
                      end;
                      CO_COD_evexxe110112Enc: // Encerramento.
                      begin
                        //
                        if infEvento_cStat = '135' then
                        begin
                          if not DmMDFe_0000.LocalizaMDFeInfoEveEnc(
                            infEvento_chMDFe, nProt, dtEnc, cUF, cMun, CNPJ)
                          then
                            Geral.MB_Erro('Falha ao localizar dados do encerramento');
                          //
{
<retEventoMDFe xmlns="http://www.portalfiscal.inf.br/mdfe" versao="1.00">
  <infEvento Id="ID941160000000034">
    <tpAmb>2</tpAmb>
    <verAplic>RS20140703103031</verAplic>
    <cOrgao>41</cOrgao>
    <cStat>135</cStat>
    <xMotivo>Evento registrado e vinculado ao MDF-e</xMotivo>
    <chMDFe>41160102717861000110580090000000031572157512</chMDFe>
    <tpEvento>110112</tpEvento>
    <xEvento>Encerramento</xEvento>
    <nSeqEvento>1</nSeqEvento>
    <dhRegEvento>2016-01-06T11:44:05</dhRegEvento>
    <nProt>941160000000034</nProt>
  </infEvento>
</retEventoMDFe>
}
                          Status                := 132;
                          infEnc_Id             := infEvento_Id;
                          infEnc_tpAmb          := Geral.IMV(infEvento_tpAmb);
                          infEnc_verAplic       := infEvento_verAplic;
                          infEnc_cOrgao         := Geral.IMV(infEvento_cOrgao);
                          infEnc_CNPJ           := CNPJ;
                          infEnc_chMDFe          := infEvento_chMDFe;
                          //infEnc_dhEvento       := Geral.FDT(dhEvento, 1);
                          //infEnc_dhEventoTZD    := dhEventoTZD;
                          infEnc_tpEvento       := tpEvento;
                          infEnc_nSeqEvento     := nSeqEvento;
                          infEnc_verEvento      := Geral.DMV_Dot(infEvento_versao);
                          infEnc_cStat          := Geral.IMV(infEvento_cStat);
                          infEnc_dhRegEvento    := infEvento_dhRegEvento;
                          //infEnc_dhRegEventoTZD := ret_TZD_UTC;
                          infEnc_nProt          := infEvento_nProt;
                          //
                          //infEnc_CPF            := CPF;
                          infEnc_dtEnc          := Geral.FDT(dtEnc, 1);
                          infEnc_cUF            := cUF;
                          infEnc_cMun           := cMun;
                          //
                          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfecaba', False, [
                          'infEnc_Id', 'Status',
                          'infEnc_verAplic', 'infEnc_cOrgao', 'infEnc_tpAmb',
                          'infEnc_CNPJ', (*'infEnc_CPF',*) 'infEnc_chMDFe',
                          (*'infEnc_dhEvento',*) (*'infEnc_dhEventoTZD',*) 'infEnc_tpEvento',
                          'infEnc_nSeqEvento', 'infEnc_verEvento', 'infEnc_dtEnc',
                          'infEnc_cUF', 'infEnc_cMun', 'infEnc_cStat',
                          'infEnc_dhRegEvento', (*'infEnc_dhRegEventoTZD',*) 'infEnc_nProt'], [
                          'ID'], [
                          infEnc_Id, Status,
                          infEnc_verAplic, infEnc_cOrgao, infEnc_tpAmb,
                          infEnc_CNPJ, (*infEnc_CPF,*) infEnc_chMDFe,
                          (*infEnc_dhEvento,*) (*infEnc_dhEventoTZD,*) infEnc_tpEvento,
                          infEnc_nSeqEvento, infEnc_verEvento, infEnc_dtEnc,
                          infEnc_cUF, infEnc_cMun, infEnc_cStat,
                          infEnc_dhRegEvento, (*infEnc_dhRegEventoTZD,*) infEnc_nProt], [
                          infEvento_chMDFe], True) then
                            DmMDFe_0000.DefineSituacaoEncerramento(infEvento_chMDFe);
                        end;
                      end;
                      CO_COD_evexxe110114IncCondutor: // Inclusao de Condutor
                      begin
                        //
                        if infEvento_cStat = '135' then
                        begin
                          if not DmMDFe_0000.LocalizaMDFeInfoEveIdC(
                            infEvento_chMDFe, CNPJ, xNome, CPF)
                          then
                            Geral.MB_Erro('Falha ao localizar dados do encerramento');
                          //
                          infIdC_Id             := infEvento_Id;
                          infIdC_tpAmb          := Geral.IMV(infEvento_tpAmb);
                          infIdC_verAplic       := infEvento_verAplic;
                          infIdC_cOrgao         := Geral.IMV(infEvento_cOrgao);
                          infIdC_CNPJ           := CNPJ;
                          infIdC_chMDFe          := infEvento_chMDFe;
                          //infIdC_dhEvento       := Geral.FDT(dhEvento, 1);
                          //infIdC_dhEventoTZD    := dhEventoTZD;
                          infIdC_tpEvento       := tpEvento;
                          infIdC_nSeqEvento     := nSeqEvento;
                          infIdC_verEvento      := Geral.DMV_Dot(infEvento_versao);
                          infIdC_cStat          := Geral.IMV(infEvento_cStat);
                          infIdC_dhRegEvento    := infEvento_dhRegEvento;
                          //infIdC_dhRegEventoTZD := ret_TZD_UTC;
                          infIdC_nProt          := infEvento_nProt;
                          //
                          //infIdC_CPF            := CPF;
                          infIdC_xNome          := xNome;
                          infIdC_CPF            := CPF;
                          //
                          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfecaba', False, [
                          'infIdC_verAplic', 'infIdC_cOrgao', 'infIdC_tpAmb',
                          'infIdC_CNPJ', 'infIdC_chMDFe', 'infIdC_tpEvento',
                          'infIdC_nSeqEvento', 'infIdC_verEvento', 'infIdC_xNome',
                          'infIdC_CPF', 'infIdC_cStat', 'infIdC_dhRegEvento',
                          'infIdC_nProt', 'infIdC_Id'], [
                          'ID'], [
                          infIdC_verAplic, infIdC_cOrgao, infIdC_tpAmb,
                          infIdC_CNPJ, infIdC_chMDFe, infIdC_tpEvento,
                          infIdC_nSeqEvento, infIdC_verEvento, infIdC_xNome,
                          infIdC_CPF, infIdC_cStat, infIdC_dhRegEvento,
                          infIdC_nProt, infIdC_Id], [
                          infEvento_chMDFe], True);
                        end;
                      end;
                      else
                      begin
                        Geral.MB_Aviso('Tipo de evento n�o implementado na fun��o:' +
                        sLineBreak + 'TFmMDFeSteps_0200a.LerTextoEnvioLoteEvento()');
                        Result := False;
                        Exit;
                      end;
                    end;
                  end;
                end;
              end;
              //
              //xmlList.Remove(xmlList.Item[0]);
              //
            end;
          end
        end;

      end;
      // N�o precisa! J� Faz direto nas SQL acima!
      DmMDFe_0000.AtualizaXML_No_BD_Tudo(False);
      //
    end else Geral.MB_Aviso('Arquivo XML n�o conhecido ou n�o implementado! [2]');
  end;
end;

{
procedure TFmMDFeSteps_0100a.LerTextoInutilizaNumerosMDFe;
var
  Controle, Empresa: Integer;
  versao, tpAmb, verAplic, cStat, xMotivo, cUF, dhRecbto, Id, nProt, xJust, Ano,
  CNPJ, Modelo, Serie, nCTIni, nCTFim: String;
  //
  Lote: Integer;
  dhRecbtoTZD: Double;
begin
  //FverXML_versao := verMDFeInutMDFe_Versao;
  FverXML_versao := Geral.FFT_Dot(DmMDFe_0000.QropcoesMDFeMDFeVerInuNum.Value, 2, siNegativo);
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � recibo de inutiliza��o de n�meros de NF
    xmlNode := xmlDoc.SelectSingleNode('/retInutMDFe');
    if assigned(xmlNode) then
    begin
      versao   := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      //
      tpAmb    := '';
      verAplic := '';
      cStat    := '';
      xMotivo  := '';
      cUF      := '';
      xmlNode := xmlDoc.SelectSingleNode('/retInutMDFe/infInut');
      if assigned(xmlNode) then
      begin
        Id       := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
        tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
        verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
        cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
        xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
        cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
        if not DefineLote(Lote) then
        begin
          MDFe_PF.DesmontaID_Inutilizacao(
            Id, cUF, Ano, CNPJ, Modelo, Serie, nCTIni, nCTFim);
          DmMDFe_0000.QrMDFeInut.Close;
          DmMDFe_0000.QrMDFeInut.Params[00].AsInteger := Empresa;
          DmMDFe_0000.QrMDFeInut.Params[01].AsString := cUF;
          DmMDFe_0000.QrMDFeInut.Params[02].AsString := ano;
          DmMDFe_0000.QrMDFeInut.Params[03].AsString := CNPJ;
          DmMDFe_0000.QrMDFeInut.Params[04].AsString := modelo;
          DmMDFe_0000.QrMDFeInut.Params[05].AsString := Serie;
          DmMDFe_0000.QrMDFeInut.Params[06].AsString := nCTIni;
          DmMDFe_0000.QrMDFeInut.Params[07].AsString := nCTFim;
          DmMDFe_0000.QrMDFeInut.Open;
          Lote := DmMDFe_0000.QrMDFeInutCodigo.Value;
          if Lote = 0 then
          begin
            Geral.MB_Erro('N�o foi poss�vel descobrir o Lote pelo ID!');
            //
            Exit;
          end else
            Geral.MB_Aviso('O Lote foi encontrado pelo ID!');
        end;
        if cStat = '102' then
        begin
          ano      := LeNoXML(xmlNode, tnxTextStr, ttx_ano);
          CNPJ     := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJ);
          modelo   := LeNoXML(xmlNode, tnxTextStr, ttx_mod);
          serie    := LeNoXML(xmlNode, tnxTextStr, ttx_serie);
          nCTIni   := LeNoXML(xmlNode, tnxTextStr, ttx_nCTIni);
          nCTFim   := LeNoXML(xmlNode, tnxTextStr, ttx_nCTFin);
          dhRecbto := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
          nProt    := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
          //
          XXe_PF.Ajusta_dh_XXe_UTC(dhRecbto, dhRecbtoTZD);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfeinut', False, [
          'versao', 'Id', 'tpAmb',
          'cUF', 'ano', 'CNPJ',
          //ver aqui o que fazer
          'modelo', 'Serie',
          'nCTIni', 'nCTFim', 'xJust',
          'cStat', 'xMotivo', 'dhRecbto',
          'nProt', 'dhRecbtoTZD'], ['Codigo'], [
          versao, Id, tpAmb,
          cUF, ano, CNPJ,
          modelo, Serie,
          nCTIni, nCTFim, xJust,
          cStat, xMotivo, dhRecbto,
          nProt, dhRecbtoTZD], [Lote], True);
        end;
        Controle := DModG.BuscaProximoCodigoInt('mdfectrl', 'mdfeinutmsg', '', 0);
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'mdfeinutmsg', False, [
        'Codigo', 'versao', 'Id',
        'tpAmb', 'verAplic', 'cStat',
        'xMotivo', 'cUF', '_Ativo_'], [
        'Controle'], [
        Lote, versao, Id,
        tpAmb, verAplic, cStat,
        xMotivo, cUF, 1], [
        Controle], True);
      end else Geral.MB_Aviso(
      'Arquivo XML n�o possui informa��es de Inutiliza��o de numera��o de MDF-e!');
    end else Geral.MB_Aviso(
    'Arquivo XML n�o conhecido ou n�o implementado! [3]' +
    sLineBreak + FTextoArq);
    //
    DmMDFe_0000.AtualizaXML_No_BD_Tudo(False);
    //
  end;
  FmMDFeInut_0000.LocCod(Lote, Lote);
  //Close;
end;
}

procedure TFmMDFeSteps_0100a.LerTextoStatusServico();
(*
<retConsStatServMDFe versao='2.00' xmlns='http://www.portalfiscal.inf.br/mdfe'>
  <tpAmb>2</tpAmb>
  <verAplic>PR-v2_1_21</verAplic>
  <cStat>107</cStat>
  <xMotivo>Servico em Operacao</xMotivo>
  <cUF>41</cUF>
  <dhRecbto>2015-12-19T11:49:44</dhRecbto>
  <tMed>0</tMed>
</retConsStatServMDFe>*)
begin
  FverXML_versao := Geral.FFT_Dot(DmMDFe_0000.QropcoesMDFeMDFeVerStaSer.Value, 2, siNegativo);
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � status de servi�o
    xmlNode := xmlDoc.SelectSingleNode('/retConsStatServMDFe');
    if assigned(xmlNode) then
    begin
      LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
      LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
      LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
      LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
      LeNoXML(xmlNode, tnxTextStr, ttx_tMed);
      //
      Pagecontrol1.ActivePageIndex := 4;
    end else Geral.MB_Aviso(
    'Arquivo XML n�o conhecido ou n�o implementado! [4]' + sLineBreak +
    'Texto XML:' + sLineBreak +
    FTextoArq);
  end;
end;

procedure TFmMDFeSteps_0100a.LerXML_procEventoMDFe(Lista: IXMLNodeList; digVal,
  cStat, dhRecbto: String);
var
  IDCtrl, Controle, Conta, FatID, FatNum, Empresa: Integer;
  // Parte generica - envio
  EventoMDFe_versao,
  infEvento_Id,
  infEvento_cOrgao,
  infEvento_tpAmb,
  infEvento_CNPJ,
  infEvento_chMDFe,
  infEvento_dhEvento,
  infEvento_tpEvento,
  infEvento_nSeqEvento,
  infEvento_versaoEvento,
  // Parte generica - retorno
  retEvento_versao,
  infEvento_verAplic,
  infEvento_cStat,
  infEvento_xMotivo,
  infEvento_xEvento,
  infEvento_dhRegEvento,
  infEvento_nProt,
  infEvento_descEvento,
  // Especifica de Cancelamento
  infCanc_xJust,
  // Encerramento
  infEnc_dtEnc,
  infEnc_cUF,
  infEnc_cMun,
  // Inclusao de condutor
  infIdC_xNome,
  infIdC_CPF,
  //

  //
  cUF, //Id,
  cJust, _Stat, _Motivo, infEnc_xMotivo, infIdC_xMotivo: String;
  //
  Status, Evento, nCondUso: Integer;
  infProt_dhRecbtoTZD, dhRecbtoTZD, infCanc_dhRecbtoTZD, dhEventoTZD: Double;
  //




  // Copiado da N.F.e mas nao existe:
  infEvento_CPF: String;
  //
  _CNPJ, _CPF: String;
  _dhEvento: TDateTime;
  _dhEventoTZD, _verEvento: Double;
  versao, descEvento, grupoAlterado, CampoAlterado, ValorAlterado,
  nroItemAlterado: String;
  //
begin
  if Lista.Length > 0 then
  begin
    while Lista.Length > 0 do
    begin
      EventoMDFe_versao              := '';
      infEvento_Id                   := '';
      infEvento_cOrgao               := '';
      infEvento_tpAmb                := '';
      infEvento_CNPJ                 := '';
      infEvento_chMDFe               := '';
      infEvento_dhEvento             := '';
      infEvento_tpEvento             := '';
      infEvento_nSeqEvento           := '';
      infEvento_versaoEvento         := '';
      // Parte generica - retorno
      retEvento_versao               := '';
      infEvento_verAplic             := '';
      infEvento_cStat                := '';
      infEvento_xMotivo              := '';
      infEvento_xEvento              := '';
      infEvento_dhRegEvento          := '';
      infEvento_nProt                := '';
      infEvento_descEvento           := '';
      // Especifica de Cancelamento
      infCanc_xJust                  := '';
      // Encerramento
      infEnc_dtEnc                   := '';
      infEnc_cUF                     := '';
      infEnc_cMun                    := '';
      // Inclusao de condutor
      infIdC_xNome                   := '';
      infIdC_CPF                     := '';

      //
      //xmlSub  := Lista.Item[0].FirstChild;
      xmlSub  := nil;
      xmlSub  := Lista.Item[0];
      // Dados do envio ao Fisco...
      // ... dados gerais
      // Erro Aqui! Pega sempre o primeiro!
      //xmlNode := xmlSub.SelectSingleNode('/retConsSitMDFe/procEventoMDFe/evento/infEvento');
      // Nao pega nada!
      //xmlNode := xmlSub.SelectSingleNode('procEventoMDFe/infEvento');
      xmlNode := xmlSub.SelectSingleNode('eventoMDFe');
      if assigned(xmlNode) then
      begin
        eventoMDFe_versao  := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
        //
        xmlNode := xmlSub.SelectSingleNode('eventoMDFe/infEvento');
        if assigned(xmlNode) then
        begin
          infEvento_Id           := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
          infEvento_cOrgao       := LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao);
          infEvento_tpAmb        := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
          infEvento_CNPJ         := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJ);
          infEvento_CPF          := LeNoXML(xmlNode, tnxTextStr, ttx_CPF);
          infEvento_chMDFe       := LeNoXML(xmlNode, tnxTextStr, ttx_chMDFe);
          infEvento_dhEvento     := LeNoXML(xmlNode, tnxTextStr, ttx_dhEvento);
          infEvento_tpEvento     := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
          infEvento_nSeqEvento   := LeNoXML(xmlNode, tnxTextStr, ttx_nseqEvento);
          infEvento_versaoEvento := LeNoXML(xmlNode, tnxAttrStr, ttx_versaoEvento);
          //
          XXe_PF.Ajusta_dh_XXe_UTC(infEvento_dhEvento, dhEventoTZD);
          //
          // ... dados especificos de cada evento
          //xmlNode := xmlSub.SelectSingleNode('/retConsSitMDFe/procEventoMDFe/eventoMDFe/infEvento/detEvento');
          xmlChild1 := xmlNode.SelectSingleNode('detEvento');
          if assigned(xmlChild1) then
          begin
            infEvento_versaoEvento := LeNoXML(xmlChild1, tnxAttrStr, ttx_versaoEvento);
////////////////////////////////////////////////////////////////////////////////
            // Especifica de Cancelamento
(*
<detEvento versaoEvento="2.00">
  <evCancMDFe>
    <descEvento>Cancelamento</descEvento>
    <nProt>141150000218490</nProt>
    <xJust>TESTE DE CANCELAMENTO</xJust>
  </evCancMDFe>
</detEvento>
*)
            xmlChild2 := xmlNode.SelectSingleNode('detEvento/evCancMDFe');
            if assigned(xmlChild2) then
            begin
              //infCanc_descEvento := LeNoXML(xmlChild2, tnxTextStr, ttx_descEvento);
              //infCanc_nProt      := LeNoXML(xmlChild2, tnxTextStr, ttx_nProt);
              infCanc_xJust      := LeNoXML(xmlChild2, tnxTextStr, ttx_xJust);
            end;
// Fim cancelamento

// Encerramento
(*
<detEvento versaoEvento="1.00">
  <evEncMDFe xmlns="http://www.portalfiscal.inf.br/mdfe">
    <descEvento>Encerramento</descEvento>
    <nProt>941160000000035</nProt>
    <dtEnc>2016-01-06</dtEnc>
    <cUF>43</cUF>
    <cMun>4313409</cMun>
  </evEncMDFe>
</detEvento>
*)
            xmlChild2 := xmlNode.SelectSingleNode('detEvento/evEncMDFe');
            if assigned(xmlChild2) then
            begin
              //infEnc_descEvento  := LeNoXML(xmlChild2, tnxTextStr, ttx_descEvento);
              //infEnc_nProt       := LeNoXML(xmlChild2, tnxTextStr, ttx_nProt);
              infEnc_dtEnc       := LeNoXML(xmlChild2, tnxTextStr, ttx_dtEnc);
              infEnc_cUF         := LeNoXML(xmlChild2, tnxTextStr, ttx_cUF);
              infEnc_cMun        := LeNoXML(xmlChild2, tnxTextStr, ttx_cMun);
            end;
// Fim encerramento

// Inclusao de condutor
(*
<detEvento versaoEvento="1.00">
  <evIncCondutorMDFe xmlns="http://www.portalfiscal.inf.br/mdfe">
    <descEvento>Inclusao Condutor</descEvento>
      <condutor>
      <xNome>Cleide Merli Arend</xNome>
      <CPF>02985988926</CPF>
    </condutor>
  </evIncCondutorMDFe>
</detEvento>
*)
            xmlChild2 := xmlNode.SelectSingleNode('detEvento/evIncCondutorMDFe');
            if assigned(xmlChild2) then
            begin
              //infEnc_descEvento  := LeNoXML(xmlChild2, tnxTextStr, ttx_descEvento);
              xmlChild3 := xmlChild2.SelectSingleNode('condutor');
              if assigned(xmlChild2) then
              begin
                infIdC_xNome       := LeNoXML(xmlChild3, tnxTextStr, ttx_xNome);
                infIdC_CPF         := LeNoXML(xmlChild3, tnxTextStr, ttx_CPF);
              end;
            end;
// Fim Inclusao de condutor
          //
          end;
        end;
        // Dados do retorno da consulta no Fisco
        // erro aqui
        //xmlNode := xmlSub.SelectSingleNode('/retConsSitMDFe/procEventoMDFe/retEvento/infEvento');
        xmlNode := xmlSub.SelectSingleNode('retEventoMDFe/infEvento');
        if assigned(xmlNode) then
        begin
          retEvento_versao := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
          _Stat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
          Evento := Geral.IMV(infEvento_tpEvento);
          if (Geral.IMV(_Stat) in ([135, 136])) then
          begin
            // Parte generica - retorno
            //infEvento_tpAmb       := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
            infEvento_verAplic    := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
            //infEvento_cOrgao      := LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao);
            infEvento_cStat       := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
            infEvento_xMotivo     := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
            //infEvento_chMDFe       := LeNoXML(xmlNode, tnxTextStr, ttx_chMDFe);
            //infEvento_tpEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
            infEvento_xEvento     := LeNoXML(xmlNode, tnxTextStr, ttx_xEvento);
            //infEvento_nSeqEvento  := LeNoXML(xmlNode, tnxTextStr, ttx_nSeqEvento);
            infEvento_dhRegEvento := LeNoXML(xmlNode, tnxTextStr, ttx_dhRegEvento);
            infEvento_nProt       := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
            //
            XXe_PF.Ajusta_dh_XXe_UTC(infEvento_dhRegEvento, infCanc_dhRecbtoTZD);
            //
            //infEvento_cStat := cStat;
            case Evento of
              CO_COD_evexxe110111Can: // = 110111;
              begin
                if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfecaba', False, [
                  'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
                  'infCanc_dhRecbto', 'infCanc_nProt', 'infCanc_digVal',
                  'infCanc_cStat', 'infCanc_xMotivo', (*'infCanc_cJust',*)
                  'infCanc_xJust', 'infCanc_dhRecbtoTZD',
                  'infCanc_dhEvento', 'infCanc_dhRegEvento'
                ], ['ID'], [
                  infEvento_Id, infEvento_tpAmb, infEvento_verAplic,
                  dhRecbto, infEvento_nProt, digVal,
                  infEvento_cStat, infEvento_xMotivo, (*cJust,*)
                  infCanc_xJust, infProt_dhRecbtoTZD,
                  infEvento_dhEvento, infEvento_dhRegEvento
                ], [infEvento_chMDFe], True) then
                  DmMDFe_0000.DefineSituacaoEncerramento(infEvento_chMDFe);
              end;
              CO_COD_evexxe110112Enc: // = 110112;
              begin
                  if Geral.IMV(infEvento_cStat) = 135 then
                    infEnc_xMotivo := 'Encerramento homologado'
                  else
                    infEnc_xMotivo := infEvento_xMotivo;
                  //
                  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfecaba', False,[
                  'infEnc_verAplic', 'infEnc_cOrgao', 'infEnc_tpAmb',
                  'infEnc_CNPJ', 'infEnc_chMDFe', 'infEnc_tpEvento',
                  'infEnc_nSeqEvento', 'infEnc_verEvento', 'infEnc_dtEnc',
                  'infEnc_cUF', 'infEnc_cMun', 'infEnc_cStat',
                  'infEnc_dhRegEvento', 'infEnc_nProt', 'infEnc_Id',
                  'infEnc_xMotivo', 'infEnc_dhEvento'
                  ], ['ID'], [
                  infEvento_verAplic, infEvento_cOrgao, infEvento_tpAmb,
                  infEvento_CNPJ, infEvento_chMDFe, infEvento_tpEvento,
                  infEvento_nSeqEvento, infEvento_versaoEvento, infEnc_dtEnc,
                  infEnc_cUF, infEnc_cMun, infEvento_cStat,
                  infEvento_dhRegEvento, infEvento_nProt, infEvento_Id,
                  infEnc_xMotivo, infEvento_dhEvento
                  ], [infEvento_chMDFe], True) then
                    DmMDFe_0000.DefineSituacaoEncerramento(infEvento_chMDFe);
              end;
              CO_COD_evexxe110114IncCondutor: // = 110114;
              begin
                if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfecaba', False, [
                'infIdC_verAplic', 'infIdC_cOrgao', 'infIdC_tpAmb',
                'infIdC_CNPJ', 'infIdC_chMDFe', 'infIdC_tpEvento',
                'infIdC_nSeqEvento', 'infIdC_verEvento', 'infIdC_xNome',
                'infIdC_CPF', 'infIdC_cStat', 'infIdC_dhRegEvento',
                'infIdC_nProt', 'infIdC_Id', 'infIdc_xMotivo',
                'infIdC_dhEvento'
                ], ['ID'], [
                infEvento_verAplic, infEvento_cOrgao, infEvento_tpAmb,
                infEvento_CNPJ, infEvento_chMDFe, infEvento_tpEvento,
                infEvento_nSeqEvento, infEvento_versaoEvento, infIdC_xNome,
                infIdC_CPF, infEvento_cStat, infEvento_dhRegEvento,
                infEvento_nProt, infEvento_Id, infIdC_xMotivo,
                infEvento_dhEvento
                ], [infEvento_chMDFe], True) then;
              end;
              else Geral.MB_Erro('Evento: ' + infEvento_tpEvento +
              ' n�o implementado em "TFmMDFeSteps_0310.LerTextoConsultaMDFe()"');
            end;
          end else
            Geral.MB_Erro('C�digo de retorno de evento ' + _Stat + ' - ' +
            MDFe_PF.Texto_StatusMDFe(Geral.IMV(_Stat), 0) + sLineBreak +
            'N�o esperado para c�digo de retorno de consulta ' + infEvento_cStat +
            MDFe_PF.Texto_StatusMDFe(Geral.IMV(infEvento_cStat), 0));
        end;
      end;
      Lista.Remove(Lista.Item[0]);
    end;
  end;
end;

procedure TFmMDFeSteps_0100a.MostraTextoRetorno(Texto: String);
begin
  RETxtRetorno.Text := Texto;
end;

function TFmMDFeSteps_0100a.ObtemDescricaoDaTag(Tag: TTipoTagXML): String;
begin
  case Tag of
    ttx_Id          : Result := 'ID'                    ;
    ttx_idLote      : Result := 'ID do lote'            ;
    ttx_versao      : Result := 'Vers�o XML'            ;
    ttx_tpAmb       : Result := 'Ambiente'              ;
    ttx_verAplic    : Result := 'Vers�o do aplicativo'  ;
    ttx_cOrgao      : Result := 'Org�o'                 ;
    ttx_cStat       : Result := 'Status'                ;
    ttx_xMotivo     : Result := 'Motivo'                ;
    ttx_cUF         : Result := 'UF'                    ;
    ttx_dhRecbto    : Result := 'Data/hora recibo'      ;
    ttx_chMDFe       : Result := 'Chave MDF-e'            ;
    ttx_nProt       : Result := 'Protocolo'             ;
    ttx_digVal      : Result := 'Valor "digest"'        ;
    ttx_ano         : Result := 'Ano'                   ;
    ttx_CNPJ        : Result := 'CNPJ'                  ;
    ttx_mod         : Result := 'Modelo MDF'             ;
    ttx_serie       : Result := 'S�rie MDF'              ;
    //ttx_nMDFIni      : Result := 'N�mero inicial MDF'     ;
    //ttx_nMDFFin      : Result := 'N�mero final MDF'       ;
    ttx_nRec        : Result := 'N�mero do Recibo'      ;
    ttx_tMed        : Result := 'Tempo m�dio'           ;
    ttx_tpEvento    : Result := 'Tipo de evento'        ;
    ttx_xEvento     : Result := 'Descri��o do evento'   ;
    ttx_CNPJDest    : Result := 'CNPJ do destinat�rio'  ;
    ttx_CPFDest     : Result := 'CPF do destinat�rio'   ;
    ttx_emailDest   : Result := 'E-mail do destinat�rio';
    ttx_nSeqEvento  : Result := 'Sequencial do evento'  ;
    ttx_dhRegEvento : Result := 'Data/hora registro'    ;
    ttx_dhResp      : Result := 'Data/hora msg resposta';
    ttx_indCont     : Result := 'Indicador continua��o' ;
    ttx_ultNSU      : Result := '�ltima NSU pesquisada' ;
    ttx_maxNSU      : Result := 'NSU M�xima encontrada' ;
    ttx_NSU         : Result := 'NSU do docum. fiscal'  ;
    ttx_CPF         : Result := 'CPF'                   ;
    ttx_xNome       : Result := 'Raz�o Social ou Nome'  ;
    ttx_IE          : Result := 'Inscri��o Estadual'    ;
    ttx_dEmi        : Result := 'Data da emiss�o'       ;
    //ttx_tpMDF        : Result := 'Tipo de opera��o MDF-e' ;
    //ttx_vMDF         : Result := 'Valor total da MDF-e'   ;
    ttx_cSitMDFe     : Result := 'Situa��o da MDF-e'    ;
    ttx_cSitEnc     : Result := 'Sit. Encerramento'     ;
    ttx_dhEvento    : Result := 'Data/hora do evento'   ;
    ttx_descEvento  : Result := 'Evento'                ;
    //ttx_x Correcao   : Result := 'x Correcao'             ;
    ttx_cJust       : Result := 'C�digo da justifica��o';
    ttx_xJust       : Result := 'Texto da justifica��o' ;
    ttx_verEvento   : Result := 'Vers�o do evento'      ;
    ttx_schema      : Result := 'Schema XML'            ;
    ttx_docZip      : Result := 'Resumo do documento'   ;
    ttx_dhEmi       : Result := 'Data / hora de emiss�o';
    //
    ttx_versaoEvento: Result := 'Vers�o do evento'      ;
    ttx_dtEnc       : Result := 'Data de encerramento'  ;
    ttx_cMun        : Result := 'Munic�pio'             ;
    //
    else              Result := '? ? ? ? ?';
  end;
end;

function TFmMDFeSteps_0100a.ObtemNomeDaTag(Tag: TTipoTagXML): String;
begin
  case Tag of
    ttx_Id          : Result := 'Id'                ;
    ttx_idLote      : Result := 'idLote'            ;
    ttx_versao      : Result := 'versao'            ;
    ttx_tpAmb       : Result := 'tpAmb'             ;
    ttx_verAplic    : Result := 'verAplic'          ;
    ttx_cOrgao      : Result := 'cOrgao'            ;
    ttx_cStat       : Result := 'cStat'             ;
    ttx_xMotivo     : Result := 'xMotivo'           ;
    ttx_cUF         : Result := 'cUF'               ;
    ttx_dhRecbto    : Result := 'dhRecbto'          ;
    ttx_chMDFe       : Result := 'chMDFe'             ;
    ttx_nProt       : Result := 'nProt'             ;
    ttx_digVal      : Result := 'digVal'            ;
    ttx_ano         : Result := 'ano'               ;
    ttx_CNPJ        : Result := 'CNPJ'              ;
    ttx_mod         : Result := 'mod'               ;
    ttx_serie       : Result := 'serie'             ;
    //ttx_nMDFIni      : Result := 'nMDFIni'            ;
    //ttx_nMDFFin      : Result := 'nMDFFin'            ;
    ttx_nRec        : Result := 'nRec'              ;
    ttx_tMed        : Result := 'tMed'              ;
    ttx_tpEvento    : Result := 'tpEvento'          ;
    ttx_xEvento     : Result := 'xEvento'           ;
    ttx_CNPJDest    : Result := 'CNPJDest'          ;
    ttx_CPFDest     : Result := 'CPFDest'           ;
    ttx_emailDest   : Result := 'emailDest'         ;
    ttx_nSeqEvento  : Result := 'nSeqEvento'        ;
    ttx_dhRegEvento : Result := 'dhRegEvento'       ;
    ttx_dhResp      : Result := 'dhResp'            ;
    ttx_indCont     : Result := 'indCont'           ;
    ttx_ultNSU      : Result := 'ultNSU'            ;
    ttx_maxNSU      : Result := 'maxNSU'            ;
    ttx_NSU         : Result := 'NSU'               ;
    ttx_CPF         : Result := 'CPF'               ;
    ttx_xNome       : Result := 'xNome'             ;
    ttx_IE          : Result := 'IE'                ;
    ttx_dEmi        : Result := 'dEmi'              ;
    //ttx_tpMDF        : Result := 'tpMDF'              ;
    //ttx_vMDF         : Result := 'vMDF'               ;
    ttx_cSitMDFe    : Result := 'cSitMDFe'          ;
    ttx_cSitEnc     : Result := 'cSitEnc'           ;
    ttx_dhEvento    : Result := 'dhEvento'          ;
    ttx_descEvento  : Result := 'descEvento'        ;
    //ttx_x Correcao   : Result := 'x Correcao'         ;
    ttx_cJust       : Result := 'cJust'             ;
    ttx_xJust       : Result := 'xJust'             ;
    ttx_verEvento   : Result := 'verEvento'         ;
    ttx_schema      : Result := 'schema'            ;
    ttx_docZip      : Result := 'docZip'            ;
    ttx_dhEmi       : Result := 'dhEmi'             ;
    //
    ttx_versaoEvento: Result := 'versaoEvento'      ;
    ttx_dtEnc       : Result := 'dtEnc'             ;
    ttx_cMun        : Result := 'cMun'              ;
    //
    else           Result :=      '???';
  end;
end;

procedure TFmMDFeSteps_0100a.PreparaCancelamentoDeMDFe(Empresa, Serie, nMDF,
  nSeqEvento, Justif: Integer; Modelo, nProtCan, chMDFe, XML_Evento: String);
var
  LoteStr, cUF, Id: String;
begin
  ReopenMDFeJust(QrMDFeJustCan, 1);
  EdEmpresa.ValueVariant       := Empresa;
  EdModeloCan.ValueVariant     := Modelo;
  EdSerieCan.ValueVariant      := Serie;
  EdnMDFCan.ValueVariant       := nMDF;
  EdnProtCan.ValueVariant      := nProtCan;
  EdchMDFeCan.ValueVariant     := chMDFe;
  EdnSeqEventoCan.ValueVariant := nSeqEvento;
  EdMDFeJustCan.ValueVariant   := Justif;
  CBMDFeJustCan.KeyValue       := Justif;
  FXML_Evento                  := XML_Evento;
  //
  VerificaCertificadoDigital(Empresa);
  //
  HabilitaBotoes();
end;

procedure TFmMDFeSteps_0100a.PreparaConsultaLote(Lote, Empresa: Integer;
  Recibo: String);
var
  LoteStr: String;
begin
  EdLote.ValueVariant      := Lote;
  EdEmpresa.ValueVariant   := Empresa;
  EdRecibo.ValueVariant    := Recibo;
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();
  //
  if CkSoLer.Checked then
  begin
    LoteStr := DmMDFe_0000.FormataLoteMDFe(Lote);
    if AbreArquivoXML(LoteStr, MDFE_EXT_PRO_REC_XML, False) then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Opt_Sel);
  end else
    MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Clk_Sel);
end;

procedure TFmMDFeSteps_0100a.PreparaConsultaMDFe(Empresa, IDCtrl: Integer;
  ChaveMDFe: String);
begin
  EdEmpresa.ValueVariant   := Empresa;
  EdChaveMDFeMDFe.Text       := ChaveMDFe;
  EdIDCtrlMDFe.ValueVariant := IDCtrl;
  //
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Clk_Sel);
end;

procedure TFmMDFeSteps_0100a.PreparaConsultaNaoEncerrados(Empresa: Integer);
begin
  EdEmpresa.ValueVariant := Empresa;
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Clk_Sel);
end;

procedure TFmMDFeSteps_0100a.PreparaEncerramentoDeMDFe(Empresa: Integer; chMDFe,
  xMun: String; nProt: String; dtEnc: TDateTime; cUF, cMun: Integer;
  XML_Evento: String);
begin
  EdEmpresa.ValueVariant       := Empresa;
  EdnProtEnc.ValueVariant      := nProt;
  EdchMDFeEnc.ValueVariant     := chMDFe;
  //EdnSeqEnc.ValueVariant       := nSeq;
  TpdtEnc.Date                 := dtEnc;
  EdcUFEnc.ValueVariant        := cUF;
  EdcMunEnc.ValueVariant       := cMun;
  EdxMunEnc.ValueVariant       := xMun;
  FXML_Evento                  := XML_Evento;
  //
  VerificaCertificadoDigital(Empresa);
  //
  HabilitaBotoes();
end;

function TFmMDFeSteps_0100a.PreparaEnvioDeLoteMDFe(Lote, Empresa: Integer;
  Sincronia: TXXeIndSinc): Boolean;
var
  LoteStr: String;
  Continua: Boolean;
begin
  FPathLoteMDFe := '';
  EdLote.ValueVariant    := Lote;
  EdEmpresa.ValueVariant := Empresa;
  //
  VerificaCertificadoDigital(Empresa);
  //
  if not CkSoLer.Checked then
  begin
    Continua := FmMDFeGeraXML_0100a.GerarLoteMDFeNovo(Lote, Empresa, FPathLoteMDFe,
      FXML_LoteMDFe, LaAviso1, LaAviso2, Sincronia);
    if Continua then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
      CO_Texto_Env_Sel);
    end else MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Envio cancelado!');
  end else
  begin
    LoteStr := FormatFloat('000000000', Lote);
    if AbreArquivoXML(LoteStr, MDFE_EXT_REC_XML, False) then
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
      CO_Texto_Opt_Sel);
  end;
  HabilitaBotoes();
  Result := True;
end;

procedure TFmMDFeSteps_0100a.PreparaEventoIncCondutor(Empresa: Integer; chMDFe,
  nProt, xNome, CPF, XML_Evento: String);
begin
  EdEmpresa.ValueVariant       := Empresa;
  EdnProtIdC.ValueVariant      := nProt;
  EdchMDFeIdC.ValueVariant     := chMDFe;
  EdxNomeIdC.ValueVariant      := xNome;
  EdCPFIdC.ValueVariant        := CPF;
  FXML_Evento                  := XML_Evento;
  //
  VerificaCertificadoDigital(Empresa);
  //
  HabilitaBotoes();
end;

{
procedure TFmMDFeSteps_0100a.PreparaInutilizaNumerosMDFe(Empresa, Lote, Ano,
  Modelo, Serie, nCTIni, nCTFim, Justif: Integer);
var
  LoteStr, cUF, Id: String;
begin
  ReopenMDFeJust(QrMDFeJustInu, 2);
  EdEmpresa.ValueVariant      := Empresa;
  EdLote.ValueVariant         := Lote;
  EdAno.ValueVariant          := Ano;
  EdModeloInu.ValueVariant    := Modelo;
  EdSerieInu.ValueVariant     := Serie;
  EdnCTIni.ValueVariant       := nCTIni;
  EdnCTFim.ValueVariant       := nCTFim;
  EdMDFeJustInu.ValueVariant   := Justif;
  CBMDFeJustInu.KeyValue       := Justif;
  //
  PnJustificativa.Enabled  := False;
  //
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  if CkSoLer.Checked then
  begin
    cUF := IntToStr(Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(DmMDFe_0000.QrEmpresaNO_UF.Value));
    DmMDFe_0000.MontaID_Inutilizacao(cUF, EdAno.Text, EdEmitCNPJ.Text,
      FormatFloat('00', EdModeloInu.ValueVariant),
      FormatFloat('00', EdSerieInu.ValueVariant),
      FormatFloat('000000000', nCTIni),
      FormatFloat('000000000', nCTFim), Id);
    LoteStr := Id + '_' + FormatFloat('000000000', Lote);
    //
    if AbreArquivoXML(LoteStr, MDFE_EXT_INU_XML, False) then
      MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Opt_Sel);
  end else
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Configure o modo de solicita��o e clique em "OK"!');
  HabilitaBotoes();
end;

procedure TFmMDFeSteps_0100a.PreparaStepGenerico(Empresa: Integer);
begin
  EdEmpresa.ValueVariant := Empresa;
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Clk_Sel);
end;
}

procedure TFmMDFeSteps_0100a.PreparaVerificacaoStatusServico(
  Empresa: Integer);
begin
  EdEmpresa.ValueVariant := Empresa;
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, CO_Texto_Clk_Sel);
end;

procedure TFmMDFeSteps_0100a.ReopenMDFeJust(QrMDFeJust: TmySQLQuery; Aplicacao: Byte);
begin
  QrMDFeJust.Close;
  QrMDFeJust.Params[0].AsInteger := Aplicacao;
  QrMDFeJust.Open;
  //
end;

procedure TFmMDFeSteps_0100a.RETxtEnvioChange(Sender: TObject);
begin
  MyObjects.LoadXML(RETxtEnvio, WBEnvio, PageControl1, 1);
end;

procedure TFmMDFeSteps_0100a.RETxtRetornoChange(Sender: TObject);
begin
  MyObjects.LoadXML(RETxtRetorno, WBResposta, PageControl1, 3);
end;

procedure TFmMDFeSteps_0100a.RGAcaoClick(Sender: TObject);
begin
  PCAcao.ActivePageIndex := RGAcao.ItemIndex;
end;

function TFmMDFeSteps_0100a.TextoArqDefinido(Texto: String): Boolean;
begin
  Result := Texto <> '';
  if not Result then
    Geral.MB_Erro('Texto XML n�o definido!');
end;

procedure TFmMDFeSteps_0100a.Timer1Timer(Sender: TObject);
begin
  FSegundos := FSegundos + 1;
  //LaWait.Visible := True;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Aguarde o tempo m�nimo de resposta ' +
    FormatFloat('0', FSegundos) + ' de ' + FormatFloat('0', FSecWait));
  //
  if LaAviso1.Font.Color = clGreen then
  begin
    LaAviso1.Font.Color := clBlue;
    LaAviso2.Font.Color := clBlue;
  end else
  begin
    LaAviso1.Font.Color := clGreen;
    LaAviso2.Font.Color := clGreen;
  end;
  //
  if FSegundos = FSecWait then
    Close;
end;

procedure TFmMDFeSteps_0100a.VerificaCertificadoDigital(Empresa: Integer);
begin
  DmMDFe_0000.ReopenEmpresa(Empresa);
  EdEmitCNPJ.Text          := DmMDFe_0000.QrEmpresaCNPJ.Value;
  CBUF.Text                := DmMDFe_0000.QrFilialMDFeUF_WebServ.Value;
  //2011-08-25
  //EdUF_Servico.Text        := DmMDFe_0000.QrFilialUF_Servico.Value;
  case DmMDFe_0000.QrFilialMDFetpEmis.Value of
    3(*SCAN*): EdUF_Servico.Text := 'SCAN';
    else EdUF_Servico.Text := DmMDFe_0000.QrFilialMDFeUF_Servico.Value;
  end;
  // Fim 2011-08-25
  EdSerialNumber.Text := DmMDFe_0000.QrFilialMDFeSerNum.Value;
  LaExpiraCertDigital.Caption := '';
  if DmMDFe_0000.QrFilialMDFeSerVal.Value < 2 then
    LaExpiraCertDigital.Caption :=
    'N�o h� data de validade cadastrada para seu certificado digital!'
  else
  if DmMDFe_0000.QrFilialMDFeSerVal.Value < Int(Date) then
    LaExpiraCertDigital.Caption :=
    'Seu certificado digital expirou!'
  else
  if DmMDFe_0000.QrFilialMDFeSerVal.Value <= (Int(Date) +
    DmMDFe_0000.QrFilialMDFeSerAvi.Value) then
    LaExpiraCertDigital.Caption :=
    'Seu certificado digital expira em ' + FormatFloat('0.000',
    DmMDFe_0000.QrFilialMDFeSerVal.Value - Now + 1) + ' dias!';
  LaExpiraCertDigital.Visible := LaExpiraCertDigital.Caption <> '';
end;

procedure TFmMDFeSteps_0100a.VerificaStatusServico();
var
  CodigoUF_Int: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    CodigoUF_Int  := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(CBUF.Text);
    //
    FTextoArq := FmMDFeGeraXML_0100a.WS_MDFeStatusServico(EdUF_Servico.Text,
      RGAmbiente.ItemIndex, CodigoUF_Int, EdSerialNumber.Text,
      LaAviso1, LaAviso2, RETxtEnvio, EdWebService);
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com sucesso!');
    MostraTextoRetorno(FTextoArq);
    LerTextoStatusServico();
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MB_Erro('Erro na chamada do WS...' +  sLineBreak  + FTextoArq);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Resposta recebida com Erros');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

end.
