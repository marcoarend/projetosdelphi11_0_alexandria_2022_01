unit ParamsMDFe;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral,
  Variants, dmkValUsu, dmkRadioGroup, dmkImage, UnDmkEnums;

type
  TFmParamsMDFe = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    QrPesqPrc: TmySQLQuery;
    QrPesqPrcPreco: TFloatField;
    Panel3: TPanel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit1: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdSequencial: TdmkEdit;
    EdSerieMDFe: TdmkEdit;
    Label6: TLabel;
    EdControle: TdmkEdit;
    RGIncSeqAuto: TdmkRadioGroup;
    EdMaxSeqLib: TdmkEdit;
    Label7: TLabel;
    Panel4: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel5: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmParamsMDFe: TFmParamsMDFe;

implementation

uses UnMyObjects, Module, ParamsEmp, UMySQLModule;

{$R *.DFM}

procedure TFmParamsMDFe.BtOKClick(Sender: TObject);
var
  Codigo, Controle, SerieMDFe, Sequencial, MaxSeqLib, IncSeqAuto: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  SerieMDFe      := EdSerieMDFe.ValueVariant;
  Sequencial     := EdSequencial.ValueVariant;
  MaxSeqLib      := EdMaxSeqLib.ValueVariant;
  IncSeqAuto     := RGIncSeqAuto.ItemIndex;
  //
  if SerieMDFe >= 890 then
  begin
    Geral.MB_Aviso('A serie deve ser inferior a 900!' + sLineBreak +
    'Para cadastrar a s�rie para notas em conting�ncia utilize o campo correto!');
    Exit;
  end;
  Controle := UMyMod.BPGS1I32('paramsmdfe', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'paramsmdfe', False, [
  'Codigo', 'SerieMDFe', 'Sequencial',
  'MaxSeqLib', 'IncSeqAuto'], [
  'Controle'], [
  Codigo, SerieMDFe, Sequencial,
  MaxSeqLib, IncSeqAuto], [
  Controle], True) then
  begin
    FmParamsEmp.ReopenParamsMDFe(Controle);
    if CkContinuar.Checked then
    begin
      Geral.MB_Info(DmkEnums.NomeTipoSQL(ImgTipo.SQLType) +
        ' de S�rie de MDF-e realizado com sucesso!');
      ImgTipo.SQLType             := stIns;
      EdSerieMDFe.ValueVariant    := 0;
      EdSequencial.ValueVariant   := 0;
      EdControle.ValueVariant     := 0;
      EdSerieMDFe.SetFocus;
    end else Close;
  end;
end;

procedure TFmParamsMDFe.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmParamsMDFe.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  DBEdCodigo.DataSource := FmParamsEmp.DsParamsEmp;
  DBEdit1.DataSource    := FmParamsEmp.DsParamsEmp;
  DBEdNome.DataSource   := FmParamsEmp.DsParamsEmp;
end;

procedure TFmParamsMDFe.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmParamsMDFe.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
