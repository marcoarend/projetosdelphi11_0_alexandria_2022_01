object FmMDFe_Pesq_0000: TFmMDFe_Pesq_0000
  Left = 339
  Top = 185
  Caption = 'MDF-PESQU-001 :: Pesquisa de MDF-e'
  ClientHeight = 692
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 234
    Width = 1008
    Height = 388
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    Visible = False
    object Splitter2: TSplitter
      Left = 0
      Top = 253
      Width = 1008
      Height = 5
      Cursor = crVSplit
      Align = alBottom
      ExplicitLeft = 40
      ExplicitTop = 268
    end
    object dmkDBGrid1: TdmkDBGrid
      Left = 0
      Top = 0
      Width = 1008
      Height = 253
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'FatID'
          Title.Caption = 'Fat.ID'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ide_serie'
          Title.Caption = 'S'#233'rie'
          Width = 29
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ide_nMDF'
          Title.Caption = 'N'#186' MDF'
          Width = 54
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LoteEnv'
          Title.Caption = 'Lote'
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'cStat'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Title.Caption = 'Status'
          Width = 35
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'xMotivo'
          Title.Caption = 'Motivo do status'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_cSitEnc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Title.Caption = 'Encerramento'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'dhRecbto'
          Title.Caption = 'Data/hora fisco'
          Width = 106
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Id'
          Title.Caption = 'Chave de acesso da MDF-e'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ide_dEmi'
          Title.Caption = 'Emiss'#227'o'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nProt'
          Title.Caption = 'N'#186' do protocolo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ide_tpEmis'
          Title.Caption = 'tp emiss.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatNum'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Emi'
          Title.Caption = 'Emitente'
          Width = 240
          Visible = True
        end>
      Color = clWindow
      DataSource = DsMDFeCabA
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDrawColumnCell = dmkDBGrid1DrawColumnCell
      OnDblClick = dmkDBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'FatID'
          Title.Caption = 'Fat.ID'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ide_serie'
          Title.Caption = 'S'#233'rie'
          Width = 29
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ide_nMDF'
          Title.Caption = 'N'#186' MDF'
          Width = 54
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LoteEnv'
          Title.Caption = 'Lote'
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'cStat'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Title.Caption = 'Status'
          Width = 35
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'xMotivo'
          Title.Caption = 'Motivo do status'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_cSitEnc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Title.Caption = 'Encerramento'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'dhRecbto'
          Title.Caption = 'Data/hora fisco'
          Width = 106
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Id'
          Title.Caption = 'Chave de acesso da MDF-e'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ide_dEmi'
          Title.Caption = 'Emiss'#227'o'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'nProt'
          Title.Caption = 'N'#186' do protocolo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ide_tpEmis'
          Title.Caption = 'tp emiss.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatNum'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Emi'
          Title.Caption = 'Emitente'
          Width = 240
          Visible = True
        end>
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 258
      Width = 1008
      Height = 130
      ActivePage = TabSheet1
      Align = alBottom
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' Eventos enviados ou consultados:'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGEventos: TDBGrid
          Left = 0
          Top = 22
          Width = 1000
          Height = 80
          Align = alClient
          DataSource = DsMDFeEveRRet
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_EVENTO'
              Title.Caption = 'Ocorr'#234'ncia'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ret_cStat'
              Title.Caption = 'Status'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ret_xMotivo'
              Title.Caption = 'Motivo do stuats'
              Width = 485
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ret_nProt'
              Title.Caption = 'Protocolo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ret_dhRegEvento'
              Title.Caption = 'Data / hora'
              Visible = True
            end>
        end
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 22
          Align = alTop
          BevelOuter = bvNone
          Caption = 
            'Click contr'#225'rio do mouse na grade de eventos mostra menu de op'#231#245 +
            'es de eventos.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8388863
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentBackground = False
          ParentFont = False
          TabOrder = 1
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Mensagens recebidas dos web services: '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGMensagens: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 102
          Align = alClient
          DataSource = DsMDFeCabAMsg
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 105
    Width = 1008
    Height = 129
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Panel5: TPanel
      Left = 621
      Top = 0
      Width = 387
      Height = 129
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 200
        Top = 7
        Width = 55
        Height = 13
        Caption = 'Data inicial:'
      end
      object Label2: TLabel
        Left = 200
        Top = 47
        Width = 48
        Height = 13
        Caption = 'Data final:'
      end
      object RGOrdem1: TRadioGroup
        Left = 4
        Top = 1
        Width = 121
        Height = 104
        Caption = ' Ordem: '
        ItemIndex = 0
        Items.Strings = (
          'Data, N'#186' MDF'
          'N'#186' MDF, Data'
          'Status, N'#186' MDF'
          'Status, Data')
        TabOrder = 0
        OnClick = RGOrdem1Click
      end
      object RGOrdem2: TRadioGroup
        Left = 128
        Top = 1
        Width = 65
        Height = 104
        Caption = ' Sentido: '
        ItemIndex = 1
        Items.Strings = (
          'ASC'
          'DES')
        TabOrder = 1
        OnClick = RGOrdem2Click
      end
      object BtReabre: TBitBtn
        Tag = 18
        Left = 288
        Top = 87
        Width = 90
        Height = 40
        Caption = '&Reabre'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtReabreClick
      end
      object TPDataI: TdmkEditDateTimePicker
        Left = 200
        Top = 23
        Width = 112
        Height = 21
        Date = 40017.908086238430000000
        Time = 40017.908086238430000000
        TabOrder = 3
        OnClick = TPDataIClick
        OnChange = TPDataIChange
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object TPDataF: TdmkEditDateTimePicker
        Left = 200
        Top = 63
        Width = 112
        Height = 21
        Date = 40017.908141423620000000
        Time = 40017.908141423620000000
        TabOrder = 4
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object Ck100e101: TCheckBox
        Left = 9
        Top = 109
        Width = 189
        Height = 17
        Caption = 'Somente autorizadas e canceladas.'
        TabOrder = 5
        OnClick = CkEmitClick
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 457
      Height = 129
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      object dmkLabel1: TdmkLabel
        Left = 4
        Top = 4
        Width = 102
        Height = 13
        Caption = 'Empresa (obrigat'#243'rio):'
        UpdType = utYes
        SQLType = stNil
      end
      object Label14: TLabel
        Left = 4
        Top = 44
        Width = 45
        Height = 13
        Caption = 'Entidade:'
        Enabled = False
      end
      object EdFilial: TdmkEditCB
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdFilialChange
        OnExit = EdFilialExit
        DBLookupComboBox = CBFilial
        IgnoraDBLookupComboBox = False
      end
      object CBFilial: TdmkDBLookupComboBox
        Left = 59
        Top = 20
        Width = 390
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdFilial
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCliente: TdmkEditCB
        Left = 4
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdClienteChange
        OnExit = EdClienteExit
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 59
        Top = 60
        Width = 390
        Height = 21
        Enabled = False
        KeyField = 'Codigo'
        ListField = 'NO_Enti'
        ListSource = DsClientes
        TabOrder = 3
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
      object RGAmbiente: TRadioGroup
        Left = 4
        Top = 84
        Width = 189
        Height = 41
        Caption = ' Ambiente: '
        Columns = 3
        ItemIndex = 1
        Items.Strings = (
          'Ambos'
          'Produ.'
          'Homol.')
        TabOrder = 4
        OnClick = RGAmbienteClick
      end
      object RGQuemEmit: TRadioGroup
        Left = 195
        Top = 84
        Width = 254
        Height = 41
        Caption = ' Quem emitiu:  '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Emitidas'
          'Recebidas'
          'Ambas')
        TabOrder = 5
        OnClick = RGAmbienteClick
      end
    end
    object CGcSitEnc: TdmkCheckGroup
      Left = 457
      Top = 0
      Width = 164
      Height = 129
      Align = alLeft
      Caption = ' Encerramento do MDF-e: '
      ItemIndex = 2
      Items.Strings = (
        'Indefinidos'
        'N'#227'o encerrado (100)'
        'Encerrado (132, 609)')
      TabOrder = 2
      OnClick = CGcSitEncClick
      UpdType = utYes
      Value = 4
      OldValor = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 217
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 4
      end
    end
    object GB_M: TGroupBox
      Left = 217
      Top = 0
      Width = 743
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 239
        Height = 32
        Caption = 'Pesquisa de MDF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 239
        Height = 32
        Caption = 'Pesquisa de MDF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 239
        Height = 32
        Caption = 'Pesquisa de MDF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 53
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 19
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
    object PB1: TProgressBar
      Left = 2
      Top = 34
      Width = 1004
      Height = 17
      Align = alBottom
      TabOrder = 1
      Visible = False
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 622
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 4
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtImprimeClick
      end
      object BtEnvia: TBitBtn
        Tag = 244
        Left = 94
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Envia'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtEnviaClick
      end
      object BtEvento: TBitBtn
        Tag = 526
        Left = 184
        Top = 4
        Width = 90
        Height = 40
        Caption = 'E&vento'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtEventoClick
      end
      object BtInutiliza: TBitBtn
        Tag = 455
        Left = 274
        Top = 4
        Width = 90
        Height = 40
        Caption = 'In&utiliza'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtInutilizaClick
      end
      object BtLeArq: TBitBtn
        Tag = 40
        Left = 364
        Top = 4
        Width = 90
        Height = 40
        Caption = '&L'#234' arquivo'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtLeArqClick
      end
      object BtArq: TBitBtn
        Tag = 512
        Left = 454
        Top = 4
        Width = 90
        Height = 40
        Caption = 'L'#234' &XML/BD'
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BtArqClick
      end
      object BtConsulta: TBitBtn
        Tag = 528
        Left = 544
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Consulta'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 6
        OnClick = BtConsultaClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 634
        Top = 4
        Width = 90
        Height = 40
        Caption = 'Exclui'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 7
        OnClick = BtExcluiClick
      end
      object Panel2: TPanel
        Left = 909
        Top = 0
        Width = 95
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 8
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrClientesBeforeClose
    SQL.Strings = (
      'SELECT Codigo, Tipo, CNPJ, CPF,'
      'IF(Tipo=0, RazaoSocial, Nome) NO_Enti'
      'FROM Entidades'
      'WHERE Cliente1="V"'
      'OR Cliente2="V"'
      'OR Cliente3="V"'
      'OR Cliente4="V"'
      'ORDER BY NO_Enti')
    Left = 620
    Top = 4
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNO_Enti: TWideStringField
      FieldName = 'NO_Enti'
      Required = True
      Size = 100
    end
    object QrClientesTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrClientesCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrClientesCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 620
    Top = 52
  end
  object PMLeArq: TPopupMenu
    Left = 400
    Top = 580
    object Cancelamento1: TMenuItem
      Caption = '&Cancelamento'
      Enabled = False
      OnClick = Cancelamento1Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 8
    Top = 8
    object CampospreenchidosnoXMLdaMDFe1: TMenuItem
      Caption = '&Campos preenchidos no XML do MDF-e'
      Visible = False
      OnClick = CampospreenchidosnoXMLdaMDFe1Click
    end
    object N2: TMenuItem
      Caption = '-'
      Visible = False
    end
    object Listadasnotaspesquisadas1: TMenuItem
      Caption = '&Lista das notas pesquisadas'
      Visible = False
      OnClick = Listadasnotaspesquisadas1Click
    end
    object este1: TMenuItem
      Caption = 'Lista das notas pesquisadas (Novo!)'
      Visible = False
      object porNatOp1: TMenuItem
        Caption = 'Imprime por NatOp (totais MDFe)'
        OnClick = porNatOp1Click
      end
      object porCFOP1: TMenuItem
        Caption = 'Imprime por CFOP (totais e itens MDFe)'
        OnClick = porCFOP1Click
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object ExportaporNatOptotaisMDFe1: TMenuItem
        Caption = 'Exporta por NatOp (totais MDFe)'
        OnClick = ExportaporNatOptotaisMDFe1Click
      end
      object ExportaporCFOPtotaiseitensMDFe1: TMenuItem
        Caption = 'Exporta por CFOP (totais e itens MDFe)'
        OnClick = ExportaporCFOPtotaiseitensMDFe1Click
      end
    end
    object PreviewdaMDFe1: TMenuItem
      Caption = 'Preview do MDF-e'
      OnClick = PreviewdaMDFe1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object ImprimeSomenteadmin1: TMenuItem
      Caption = '&Imprime (Somente admin.)'
      OnClick = ImprimeSomenteadmin1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
  end
  object QrMDFeCabA: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrMDFeCabAAfterOpen
    BeforeClose = QrMDFeCabABeforeClose
    AfterScroll = QrMDFeCabAAfterScroll
    OnCalcFields = QrMDFeCabACalcFields
    SQL.Strings = (
      'SELECT IF(emi.Tipo=0, emi.RazaoSocial, emi.Nome) NO_Emi,'
      'nfa.FatID, nfa.FatNum, nfa.Empresa, nfa.LoteEnv, nfa.Id,'
      'ide_serie, ide_nMDF, ide_dEmi,'
      'IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat) + 0.000 cStat,'
      'IF(infCanc_cStat>0, infCanc_nProt, infProt_nProt) nProt,'
      'IF(infCanc_cStat>0, infCanc_xMotivo, infProt_xMotivo) xMotivo,'
      
        'IF(infCanc_cStat>0, DATE_FORMAT(infCanc_dhRecbto, "%d/%m/%Y %H:%' +
        'i:%S"),'
      'DATE_FORMAT(infProt_dhRecbto, "%d/%m/%Y %H:%i:%S")) dhRecbto,'
      'IDCtrl, versao,'
      'ide_tpEmis, infCanc_xJust, Status,'
      'cSitMDFe, cSitConf,'
      
        'ELT(cSitConf+2, "N'#227'o consultada", "Sem manifesta'#231#227'o", "Confirmad' +
        'a",'
      
        '"Desconhecida", "N'#227'o realizada", "Ci'#234'ncia", "? ? ?") NO_cSitConf' +
        ','
      'nfa.infCanc_dhRecbto, nfa.infCanc_nProt'
      'FROM mdfecaba nfa'
      'LEFT JOIN entidades emi ON emi.Codigo=nfa.CodInfoEmite'
      ''
      'WHERE ide_dEmi  BETWEEN "2015-12-10" AND "2015-12-20 23:59:59"'
      'AND nfa.Empresa = -11'
      'AND nfa.emit_CNPJ="02717861000110"'
      'AND nfa.ide_tpAmb=2'
      'AND nfa.cSitConf IN (3)'
      'ORDER BY nfa.ide_dEmi DESC, nfa.ide_nMDF DESC')
    Left = 596
    Top = 268
    object QrMDFeCabANOME_tpEmis: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_tpEmis'
      Size = 30
      Calculated = True
    end
    object QrMDFeCabANO_Emi: TWideStringField
      FieldName = 'NO_Emi'
      Size = 100
    end
    object QrMDFeCabAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeCabAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeCabALoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
    object QrMDFeCabAId: TWideStringField
      FieldName = 'Id'
      Size = 47
    end
    object QrMDFeCabAide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrMDFeCabAide_nMDF: TIntegerField
      FieldName = 'ide_nMDF'
    end
    object QrMDFeCabAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrMDFeCabAcStat: TFloatField
      FieldName = 'cStat'
      Required = True
    end
    object QrMDFeCabAnProt: TWideStringField
      FieldName = 'nProt'
      Size = 15
    end
    object QrMDFeCabAxMotivo: TWideStringField
      FieldName = 'xMotivo'
      Size = 255
    end
    object QrMDFeCabAdhRecbto: TWideStringField
      FieldName = 'dhRecbto'
      Size = 24
    end
    object QrMDFeCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrMDFeCabAversao: TFloatField
      FieldName = 'versao'
    end
    object QrMDFeCabAide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
    end
    object QrMDFeCabAinfCanc_xJust: TWideStringField
      FieldName = 'infCanc_xJust'
      Size = 255
    end
    object QrMDFeCabAStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrMDFeCabAcSitMDFe: TSmallintField
      FieldName = 'cSitMDFe'
    end
    object QrMDFeCabAcSitEnc: TSmallintField
      FieldName = 'cSitEnc'
    end
    object QrMDFeCabANO_cSitEnc: TWideStringField
      FieldName = 'NO_cSitEnc'
      Size = 16
    end
    object QrMDFeCabAinfCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
    end
    object QrMDFeCabAinfCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Size = 15
    end
    object QrMDFeCabAide_Modal: TSmallintField
      FieldName = 'ide_Modal'
    end
  end
  object DsMDFeCabA: TDataSource
    DataSet = QrMDFeCabA
    Left = 688
    Top = 268
  end
  object frxDsMDFeCabA: TfrxDBDataset
    UserName = 'frxDsMDFeCabA'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOME_tpEmis=NOME_tpEmis'
      'NO_Emi=NO_Emi'
      'FatID=FatID'
      'FatNum=FatNum'
      'Empresa=Empresa'
      'LoteEnv=LoteEnv'
      'Id=Id'
      'ide_serie=ide_serie'
      'ide_nMDF=ide_nMDF'
      'ide_dEmi=ide_dEmi'
      'cStat=cStat'
      'nProt=nProt'
      'xMotivo=xMotivo'
      'dhRecbto=dhRecbto'
      'IDCtrl=IDCtrl'
      'versao=versao'
      'ide_tpEmis=ide_tpEmis'
      'infCanc_xJust=infCanc_xJust'
      'Status=Status'
      'cSitMDFe=cSitMDFe'
      'cSitEnc=cSitEnc'
      'NO_cSitEnc=NO_cSitEnc'
      'infCanc_dhRecbto=infCanc_dhRecbto'
      'infCanc_nProt=infCanc_nProt'
      'ide_Modal=ide_Modal')
    DataSet = QrMDFeCabA
    BCDToCurrency = False
    Left = 508
    Top = 268
  end
  object QrMDFeCabAMsg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctecabamsg '
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'ORDER BY Controle DESC')
    Left = 596
    Top = 316
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrMDFeCabAMsgFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeCabAMsgFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeCabAMsgEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeCabAMsgControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMDFeCabAMsgSolicit: TIntegerField
      FieldName = 'Solicit'
    end
    object QrMDFeCabAMsgId: TWideStringField
      FieldName = 'Id'
      Size = 30
    end
    object QrMDFeCabAMsgtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrMDFeCabAMsgverAplic: TWideStringField
      FieldName = 'verAplic'
      Size = 30
    end
    object QrMDFeCabAMsgdhRecbto: TDateTimeField
      FieldName = 'dhRecbto'
    end
    object QrMDFeCabAMsgnProt: TWideStringField
      FieldName = 'nProt'
      Size = 15
    end
    object QrMDFeCabAMsgdigVal: TWideStringField
      FieldName = 'digVal'
      Size = 28
    end
    object QrMDFeCabAMsgcStat: TIntegerField
      FieldName = 'cStat'
    end
    object QrMDFeCabAMsgxMotivo: TWideStringField
      FieldName = 'xMotivo'
      Size = 255
    end
  end
  object DsMDFeCabAMsg: TDataSource
    DataSet = QrMDFeCabAMsg
    Left = 688
    Top = 316
  end
  object frxDsA: TfrxDBDataset
    UserName = 'frxDsA'
    CloseDataSource = False
    FieldAliases.Strings = (
      'FatID=FatID'
      'FatNum=FatNum'
      'Empresa=Empresa'
      'IDCtrl=IDCtrl'
      'LoteEnv=LoteEnv'
      'versao=versao'
      'Id=Id'
      'ide_cUF=ide_cUF'
      'ide_tpAmb=ide_tpAmb'
      'ide_tpEmit=ide_tpEmit'
      'ide_mod=ide_mod'
      'ide_serie=ide_serie'
      'ide_nMDF=ide_nMDF'
      'ide_cMDF=ide_cMDF'
      'ide_cDV=ide_cDV'
      'ide_Modal=ide_Modal'
      'ide_dEmi=ide_dEmi'
      'ide_hEmi=ide_hEmi'
      'ide_dhEmiTZD=ide_dhEmiTZD'
      'ide_tpEmis=ide_tpEmis'
      'ide_procEmi=ide_procEmi'
      'ide_verProc=ide_verProc'
      'ide_UFIni=ide_UFIni'
      'ide_UFFim=ide_UFFim'
      'emit_CNPJ=emit_CNPJ'
      'emit_IE=emit_IE'
      'emit_xNome=emit_xNome'
      'emit_xFant=emit_xFant'
      'emit_xLgr=emit_xLgr'
      'emit_nro=emit_nro'
      'emit_xCpl=emit_xCpl'
      'emit_xBairro=emit_xBairro'
      'emit_cMun=emit_cMun'
      'emit_xMun=emit_xMun'
      'emit_CEP=emit_CEP'
      'emit_UF=emit_UF'
      'emit_fone=emit_fone'
      'emit_email=emit_email'
      'versaoModal=versaoModal'
      'Status=Status'
      'protMDFe_versao=protMDFe_versao'
      'infProt_Id=infProt_Id'
      'infProt_tpAmb=infProt_tpAmb'
      'infProt_verAplic=infProt_verAplic'
      'infProt_dhRecbto=infProt_dhRecbto'
      'infProt_dhRecbtoTZD=infProt_dhRecbtoTZD'
      'infProt_nProt=infProt_nProt'
      'infProt_digVal=infProt_digVal'
      'infProt_cStat=infProt_cStat'
      'infProt_xMotivo=infProt_xMotivo'
      'retCancMDFe_versao=retCancMDFe_versao'
      'infCanc_Id=infCanc_Id'
      'infCanc_tpAmb=infCanc_tpAmb'
      'infCanc_verAplic=infCanc_verAplic'
      'infCanc_dhRecbto=infCanc_dhRecbto'
      'infCanc_dhRecbtoTZD=infCanc_dhRecbtoTZD'
      'infCanc_nProt=infCanc_nProt'
      'infCanc_digVal=infCanc_digVal'
      'infCanc_cStat=infCanc_cStat'
      'infCanc_xMotivo=infCanc_xMotivo'
      'infCanc_cJust=infCanc_cJust'
      'infCanc_xJust=infCanc_xJust'
      'FisRegCad=FisRegCad'
      'CartEmiss=CartEmiss'
      'TabelaPrc=TabelaPrc'
      'CondicaoPg=CondicaoPg'
      'CodInfoEmite=CodInfoEmite'
      'CriAForca=CriAForca'
      'cSitMDFe=cSitMDFe'
      'cSitEnc=cSitEnc'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'ide_dIniViagem=ide_dIniViagem'
      'ide_hIniViagem=ide_hIniViagem'
      'ide_dhIniViagemTZD=ide_dhIniViagemTZD'
      'Tot_qCTe=Tot_qCTe'
      'Tot_qNFe=Tot_qNFe'
      'Tot_qMDFe=Tot_qMDFe'
      'Tot_vCarga=Tot_vCarga'
      'Tot_cUnid=Tot_cUnid'
      'Tot_qCarga=Tot_qCarga'
      'infAdFisco=infAdFisco'
      'infCpl=infCpl'
      'ID_TXT=ID_TXT'
      'EMIT_ENDERECO=EMIT_ENDERECO'
      'EMIT_FONE_TXT=EMIT_FONE_TXT'
      'EMIT_IE_TXT=EMIT_IE_TXT'
      'EMIT_CNPJ_TXT=EMIT_CNPJ_TXT'
      'DOC_SEM_VLR_JUR=DOC_SEM_VLR_JUR'
      'DOC_SEM_VLR_FIS=DOC_SEM_VLR_FIS')
    DataSet = QrA
    BCDToCurrency = False
    Left = 864
    Top = 268
  end
  object QrA: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrACalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM mdfecaba'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 784
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrALoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
    object QrAversao: TFloatField
      FieldName = 'versao'
    end
    object QrAId: TWideStringField
      FieldName = 'Id'
      Size = 47
    end
    object QrAide_cUF: TSmallintField
      FieldName = 'ide_cUF'
    end
    object QrAide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
    end
    object QrAide_tpEmit: TSmallintField
      FieldName = 'ide_tpEmit'
    end
    object QrAide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrAide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrAide_nMDF: TIntegerField
      FieldName = 'ide_nMDF'
    end
    object QrAide_cMDF: TIntegerField
      FieldName = 'ide_cMDF'
    end
    object QrAide_cDV: TSmallintField
      FieldName = 'ide_cDV'
    end
    object QrAide_Modal: TSmallintField
      FieldName = 'ide_Modal'
    end
    object QrAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrAide_hEmi: TTimeField
      FieldName = 'ide_hEmi'
    end
    object QrAide_dhEmiTZD: TFloatField
      FieldName = 'ide_dhEmiTZD'
    end
    object QrAide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
    end
    object QrAide_procEmi: TSmallintField
      FieldName = 'ide_procEmi'
    end
    object QrAide_verProc: TWideStringField
      FieldName = 'ide_verProc'
    end
    object QrAide_UFIni: TWideStringField
      FieldName = 'ide_UFIni'
      Size = 2
    end
    object QrAide_UFFim: TWideStringField
      FieldName = 'ide_UFFim'
      Size = 2
    end
    object QrAemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrAemit_IE: TWideStringField
      FieldName = 'emit_IE'
      Size = 14
    end
    object QrAemit_xNome: TWideStringField
      FieldName = 'emit_xNome'
      Size = 60
    end
    object QrAemit_xFant: TWideStringField
      FieldName = 'emit_xFant'
      Size = 60
    end
    object QrAemit_xLgr: TWideStringField
      FieldName = 'emit_xLgr'
      Size = 60
    end
    object QrAemit_nro: TWideStringField
      FieldName = 'emit_nro'
      Size = 60
    end
    object QrAemit_xCpl: TWideStringField
      FieldName = 'emit_xCpl'
      Size = 60
    end
    object QrAemit_xBairro: TWideStringField
      FieldName = 'emit_xBairro'
      Size = 60
    end
    object QrAemit_cMun: TIntegerField
      FieldName = 'emit_cMun'
    end
    object QrAemit_xMun: TWideStringField
      FieldName = 'emit_xMun'
      Size = 60
    end
    object QrAemit_CEP: TIntegerField
      FieldName = 'emit_CEP'
    end
    object QrAemit_UF: TWideStringField
      FieldName = 'emit_UF'
      Size = 2
    end
    object QrAemit_fone: TWideStringField
      FieldName = 'emit_fone'
      Size = 14
    end
    object QrAemit_email: TWideStringField
      FieldName = 'emit_email'
      Size = 60
    end
    object QrAversaoModal: TFloatField
      FieldName = 'versaoModal'
    end
    object QrAStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrAprotMDFe_versao: TFloatField
      FieldName = 'protMDFe_versao'
    end
    object QrAinfProt_Id: TWideStringField
      FieldName = 'infProt_Id'
      Size = 30
    end
    object QrAinfProt_tpAmb: TSmallintField
      FieldName = 'infProt_tpAmb'
    end
    object QrAinfProt_verAplic: TWideStringField
      FieldName = 'infProt_verAplic'
      Size = 30
    end
    object QrAinfProt_dhRecbto: TDateTimeField
      FieldName = 'infProt_dhRecbto'
    end
    object QrAinfProt_dhRecbtoTZD: TFloatField
      FieldName = 'infProt_dhRecbtoTZD'
    end
    object QrAinfProt_nProt: TWideStringField
      FieldName = 'infProt_nProt'
      Size = 15
    end
    object QrAinfProt_digVal: TWideStringField
      FieldName = 'infProt_digVal'
      Size = 28
    end
    object QrAinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrAinfProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
    object QrAretCancMDFe_versao: TFloatField
      FieldName = 'retCancMDFe_versao'
    end
    object QrAinfCanc_Id: TWideStringField
      FieldName = 'infCanc_Id'
      Size = 54
    end
    object QrAinfCanc_tpAmb: TSmallintField
      FieldName = 'infCanc_tpAmb'
    end
    object QrAinfCanc_verAplic: TWideStringField
      FieldName = 'infCanc_verAplic'
      Size = 30
    end
    object QrAinfCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
    end
    object QrAinfCanc_dhRecbtoTZD: TFloatField
      FieldName = 'infCanc_dhRecbtoTZD'
    end
    object QrAinfCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Size = 15
    end
    object QrAinfCanc_digVal: TWideStringField
      FieldName = 'infCanc_digVal'
      Size = 28
    end
    object QrAinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
    end
    object QrAinfCanc_xMotivo: TWideStringField
      FieldName = 'infCanc_xMotivo'
      Size = 255
    end
    object QrAinfCanc_cJust: TIntegerField
      FieldName = 'infCanc_cJust'
    end
    object QrAinfCanc_xJust: TWideStringField
      FieldName = 'infCanc_xJust'
      Size = 255
    end
    object QrAFisRegCad: TIntegerField
      FieldName = 'FisRegCad'
    end
    object QrACartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
    object QrATabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
    end
    object QrACondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrACodInfoEmite: TIntegerField
      FieldName = 'CodInfoEmite'
    end
    object QrACriAForca: TSmallintField
      FieldName = 'CriAForca'
    end
    object QrAcSitMDFe: TSmallintField
      FieldName = 'cSitMDFe'
    end
    object QrAcSitEnc: TSmallintField
      FieldName = 'cSitEnc'
    end
    object QrALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrAAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrAide_dIniViagem: TDateField
      FieldName = 'ide_dIniViagem'
    end
    object QrAide_hIniViagem: TTimeField
      FieldName = 'ide_hIniViagem'
    end
    object QrAide_dhIniViagemTZD: TFloatField
      FieldName = 'ide_dhIniViagemTZD'
    end
    object QrATot_qCTe: TIntegerField
      FieldName = 'Tot_qCTe'
    end
    object QrATot_qNFe: TIntegerField
      FieldName = 'Tot_qNFe'
    end
    object QrATot_qMDFe: TIntegerField
      FieldName = 'Tot_qMDFe'
    end
    object QrATot_vCarga: TFloatField
      FieldName = 'Tot_vCarga'
    end
    object QrATot_cUnid: TSmallintField
      FieldName = 'Tot_cUnid'
    end
    object QrATot_qCarga: TFloatField
      FieldName = 'Tot_qCarga'
    end
    object QrAinfAdFisco: TWideMemoField
      FieldName = 'infAdFisco'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrAinfCpl: TWideMemoField
      FieldName = 'infCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrAID_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ID_TXT'
      Size = 100
      Calculated = True
    end
    object QrAEMIT_ENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_ENDERECO'
      Size = 255
      Calculated = True
    end
    object QrAEMIT_FONE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_FONE_TXT'
      Size = 100
      Calculated = True
    end
    object QrAEMIT_IE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_IE_TXT'
      Size = 100
      Calculated = True
    end
    object QrAEMIT_CNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMIT_CNPJ_TXT'
      Size = 100
      Calculated = True
    end
    object QrADOC_SEM_VLR_JUR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOC_SEM_VLR_JUR'
      Size = 255
      Calculated = True
    end
    object QrADOC_SEM_VLR_FIS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOC_SEM_VLR_FIS'
      Size = 255
      Calculated = True
    end
  end
  object QrMDFeXMLI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT xml.Codigo, xml.ID, xml.Valor, lay.Pai, lay.Descricao, la' +
        'y.Campo'
      'FROM nfexmli xml'
      'LEFT JOIN nfelayi lay ON lay.ID=xml.ID AND lay.Codigo=xml.Codigo'
      'WHERE xml.FatID=:P0'
      'AND xml.FatNum=:P1'
      'AND xml.Empresa=:P2')
    Left = 356
    Top = 288
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrMDFeXMLICodigo: TWideStringField
      FieldName = 'Codigo'
      Size = 5
    end
    object QrMDFeXMLIID: TWideStringField
      FieldName = 'ID'
      Size = 6
    end
    object QrMDFeXMLIValor: TWideStringField
      FieldName = 'Valor'
      Size = 60
    end
    object QrMDFeXMLIPai: TWideStringField
      FieldName = 'Pai'
      Size = 10
    end
    object QrMDFeXMLIDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrMDFeXMLICampo: TWideStringField
      FieldName = 'Campo'
      Size = 30
    end
  end
  object frxDsMDFeXMLI: TfrxDBDataset
    UserName = 'frxDsMDFeXMLI'
    CloseDataSource = False
    DataSet = QrMDFeXMLI
    BCDToCurrency = False
    Left = 356
    Top = 336
  end
  object frxCampos: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40025.074573773150000000
    ReportOptions.LastChange = 40025.074573773150000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 188
    Top = 284
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsMDFeCabA
        DataSetName = 'frxDsMDFeCabA'
      end
      item
        DataSet = frxDsMDFeXMLI
        DataSetName = 'frxDsMDFeXMLI'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 15.000000000000000000
      object PageHeader9: TfrxPageHeader
        FillType = ftBrush
        Height = 58.582701570000000000
        Top = 79.370130000000000000
        Width = 699.213050000000000000
        object Shape7: TfrxShapeView
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Line24: TfrxLineView
          Top = 18.897650000000000000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo215: TfrxMemoView
          Left = 151.181200000000000000
          Width = 396.850650000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Campos preenchidos no XML')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo293: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            
              'NF N'#186' [frxDsCTeCabA."ide_nNF"]   -   Chave NF-e: [frxDsCTeCabA."' +
              'Id"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Top = 37.795300000000000000
          Width = 18.897650000000000000
          Height = 20.787401570000000000
          DataSet = frxDsMDFeXMLI
          DataSetName = 'frxDsMDFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'd.'
            'ID')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 18.897650000000000000
          Top = 37.795300000000000000
          Width = 52.913420000000000000
          Height = 20.787401570000000000
          DataSet = frxDsMDFeXMLI
          DataSetName = 'frxDsMDFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pai'
            'Campo')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 71.811070000000000000
          Top = 37.795300000000000000
          Width = 627.401980000000000000
          Height = 20.787401570000000000
          DataSet = frxDsMDFeXMLI
          DataSetName = 'frxDsMDFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o do campo'
            'Valor da campo no XML')
          ParentFont = False
        end
        object Memo216: TfrxMemoView
          Left = 7.559060000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 548.031850000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'g. [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData12: TfrxMasterData
        FillType = ftBrush
        Height = 20.787401570000000000
        Top = 200.315090000000000000
        Width = 699.213050000000000000
        DataSet = frxDsMDFeXMLI
        DataSetName = 'frxDsMDFeXMLI'
        RowCount = 0
        object Memo301: TfrxMemoView
          Width = 18.897650000000000000
          Height = 20.787401574803150000
          DataSet = frxDsMDFeXMLI
          DataSetName = 'frxDsMDFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCTeXMLI."Codigo"]'
            '[frxDsCTeXMLI."ID"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 18.897650000000000000
          Width = 52.913420000000000000
          Height = 20.787401570000000000
          DataSet = frxDsMDFeXMLI
          DataSetName = 'frxDsMDFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCTeXMLI."Pai"]'
            '[frxDsCTeXMLI."Campo"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 71.811070000000000000
          Width = 627.401980000000000000
          Height = 20.787401574803150000
          DataSet = frxDsMDFeXMLI
          DataSetName = 'frxDsMDFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCTeXMLI."Descricao"]'
            '[frxDsCTeXMLI."Valor"]')
          ParentFont = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 37.795300000000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Shape1: TfrxShapeView
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo8: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTeCabA."NO_Cli"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
      end
    end
  end
  object QrArq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctearq'
      'WHERE IDCtrl=:P0'
      ''
      '')
    Left = 356
    Top = 384
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrArqFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'nfearq.FatID'
    end
    object QrArqFatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'nfearq.FatNum'
    end
    object QrArqEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfearq.Empresa'
    end
    object QrArqIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'nfearq.IDCtrl'
    end
    object QrArqXML_MDFe: TWideMemoField
      FieldName = 'XML_MDFe'
      Origin = 'nfearq.XML_MDFe'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrArqXML_Aut: TWideMemoField
      FieldName = 'XML_Aut'
      Origin = 'nfearq.XML_Aut'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrArqXML_Can: TWideMemoField
      FieldName = 'XML_Can'
      Origin = 'nfearq.XML_Can'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrArqLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'nfearq.Lk'
    end
    object QrArqDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'nfearq.DataCad'
    end
    object QrArqDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'nfearq.DataAlt'
    end
    object QrArqUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'nfearq.UserCad'
    end
    object QrArqUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'nfearq.UserAlt'
    end
    object QrArqAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'nfearq.AlterWeb'
    end
    object QrArqAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'nfearq.Ativo'
    end
  end
  object PMArq: TPopupMenu
    OnPopup = PMArqPopup
    Left = 448
    Top = 580
    object MDFe1: TMenuItem
      Caption = 'MDF-e'
      Enabled = False
      OnClick = MDFe1Click
    end
    object Autorizao1: TMenuItem
      Caption = '&Autoriza'#231#227'o'
      Enabled = False
      OnClick = Autorizao1Click
    end
    object Cancelamento2: TMenuItem
      Caption = '&Cancelamento'
      Enabled = False
      OnClick = Cancelamento2Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object DiretriodoarquivoXML1: TMenuItem
      Caption = '&Diret'#243'rio do arquivo XML'
      OnClick = DiretriodoarquivoXML1Click
    end
  end
  object frxListaMFFes: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40025.074573773150000000
    ReportOptions.LastChange = 40025.074573773150000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxListaMFFesGetValue
    Left = 100
    Top = 280
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsMDFe_100
        DataSetName = 'frxDsMDFe_100'
      end
      item
        DataSet = frxDsMDFe_101
        DataSetName = 'frxDsMDFe_101'
      end
      item
        DataSet = frxDsMDFe_XXX
        DataSetName = 'frxDsMDFe_XXX'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader9: TfrxPageHeader
        FillType = ftBrush
        Height = 60.472480000000000000
        Top = 79.370130000000000000
        Width = 990.236860000000000000
        object Shape7: TfrxShapeView
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Line24: TfrxLineView
          Top = 18.897650000000000000
          Width = 990.236860000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo293: TfrxMemoView
          Left = 151.181200000000000000
          Width = 687.874460000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo da pesquisa: [VARF_PERIODO]')
          ParentFont = False
        end
        object Memo216: TfrxMemoView
          Left = 7.559060000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 839.055660000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'g. [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 264.566929130000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Status: [VARF_STATUS]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 272.126160000000000000
          Top = 18.897650000000000000
          Width = 445.984540000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_APENAS_EMP]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 718.110700000000000000
          Top = 18.897650000000000000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Ambiente: [VARF_AMBIENTE]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Top = 41.574830000000000000
          Width = 495.118430000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Empresa: [VARF_EMPRESA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 495.118430000000000000
          Top = 41.574830000000000000
          Width = 495.118430000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Terceiro: [VARF_TERCEIRO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData12: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 260.787570000000000000
        Width = 990.236860000000000000
        DataSet = frxDsMDFe_100
        DataSetName = 'frxDsMDFe_100'
        RowCount = 0
        object Memo14: TfrxMemoView
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39', <frxDsCTe_100."ide_nNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Left = 64.252010000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsCTe_100."ide_serie">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 86.929190000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'ide_dEmi'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_100."ide_dEmi"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 132.283550000000000000
          Width = 68.031496062992130000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vProd'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vProd"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 566.929500000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vNF'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 200.315090000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vST'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vST"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 260.787570000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vFrete'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vFrete"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 321.260050000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vSeg'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vSeg"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          Left = 381.732530000000000000
          Width = 64.251968503937010000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vIPI'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vIPI"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 445.984540000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vOutro'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vOutro"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 506.457020000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vDesc'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vDesc"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          Left = 634.961040000000000000
          Width = 68.031500940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vBC'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vBC"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          Left = 702.992580000000000000
          Width = 64.251968500000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vICMS'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vICMS"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo63: TfrxMemoView
          Left = 888.189550000000000000
          Width = 86.929141180000000000
          Height = 13.228346460000000000
          DataField = 'NOME_tpEmis'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_100."NOME_tpEmis"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo64: TfrxMemoView
          Left = 975.118740000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataField = 'NOME_tpNF'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_100."NOME_tpNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo93: TfrxMemoView
          Left = 767.244590000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vPIS'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vPIS"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo96: TfrxMemoView
          Left = 827.717070000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vCOFINS'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vCOFINS"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 37.795300000000000000
        Top = 18.897650000000000000
        Width = 990.236860000000000000
        object Shape1: TfrxShapeView
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo215: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 975.118740000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Lista de Notas Fiscais Eletr'#244'nicas')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Width = 975.118740000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 990.236860000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
      end
      object PageFooter3: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 623.622450000000000000
        Width = 990.236860000000000000
        object Memo120: TfrxMemoView
          Width = 1046.929810000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 35.905526460000000000
        Top = 200.315090000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsCTe_100."Ordem"'
        object Memo1: TfrxMemoView
          Top = 22.677180000000000000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#186' NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 64.252010000000000000
          Top = 22.677180000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Left = 86.929190000000000000
          Top = 22.677180000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 132.283550000000000000
          Top = 22.677180000000000000
          Width = 68.031496062992130000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Produtos')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 566.929500000000000000
          Top = 22.677180000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total NFe')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 200.315090000000000000
          Top = 22.677180000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ICMS ST')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 260.787570000000000000
          Top = 22.677180000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Frete')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Left = 321.260050000000000000
          Top = 22.677180000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Seguro')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 381.732530000000000000
          Top = 22.677180000000000000
          Width = 64.251968503937010000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IPI')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 445.984540000000000000
          Top = 22.677180000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Outras desp.')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 506.457020000000000000
          Top = 22.677180000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Desconto')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 634.961040000000000000
          Top = 22.677180000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'BC ICMS')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          Left = 702.992580000000000000
          Top = 22.677180000000000000
          Width = 64.251968500000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ICMS')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          Top = 3.779530000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'AUTORIZADA')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 888.189550000000000000
          Top = 22.677180000000000000
          Width = 86.929141180000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
          WordWrap = False
        end
        object Memo62: TfrxMemoView
          Left = 975.118740000000000000
          Top = 22.677180000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'E/S')
          ParentFont = False
          WordWrap = False
        end
        object Memo94: TfrxMemoView
          Left = 767.244590000000000000
          Top = 22.677180000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'PIS')
          ParentFont = False
          WordWrap = False
        end
        object Memo97: TfrxMemoView
          Left = 827.717070000000000000
          Top = 22.677180000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'COFINS')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677177560000000000
        Top = 298.582870000000000000
        Width = 990.236860000000000000
        object Memo54: TfrxMemoView
          Left = 566.929500000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          Left = 321.260050000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vSeg">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo56: TfrxMemoView
          Left = 381.732530000000000000
          Width = 64.251968503937010000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vIPI">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          Left = 445.984540000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vOutro">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo58: TfrxMemoView
          Left = 506.457020000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vDesc">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo59: TfrxMemoView
          Left = 634.961040000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vBC">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo60: TfrxMemoView
          Left = 702.992580000000000000
          Width = 64.251968500000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vICMS">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo65: TfrxMemoView
          Width = 132.283501180000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAIS:  ')
          ParentFont = False
          WordWrap = False
        end
        object Memo66: TfrxMemoView
          Left = 888.189550000000000000
          Width = 102.047261180000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
        object Memo67: TfrxMemoView
          Left = 132.283550000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vProd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          Left = 200.315090000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vST">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo69: TfrxMemoView
          Left = 260.787570000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vFrete">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo95: TfrxMemoView
          Left = 767.244590000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vPIS">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo98: TfrxMemoView
          Left = 827.717070000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vCOFINS">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 35.905526460000000000
        Top = 343.937230000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsCTe_101."Ordem"'
        object Memo31: TfrxMemoView
          Left = 177.637910000000000000
          Top = 22.677180000000000000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          Left = 245.669450000000000000
          Top = 22.677180000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'E/S')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          Top = 3.779530000000000000
          Width = 990.236860000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'CANCELADA')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Top = 22.677180000000000000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#186' NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          Left = 64.252010000000000000
          Top = 22.677180000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          Left = 132.283550000000000000
          Top = 22.677180000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          Left = 86.929190000000000000
          Top = 22.677180000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ano')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          Left = 109.606370000000000000
          Top = 22.677180000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          Left = 260.787570000000000000
          Top = 22.677180000000000000
          Width = 226.771653540000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Chave NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          Left = 638.740570000000000000
          Top = 22.677180000000000000
          Width = 351.496260710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Motivo ou descri'#231#227'o do status')
          ParentFont = False
          WordWrap = False
        end
        object Memo89: TfrxMemoView
          Left = 487.559370000000000000
          Top = 22.677180000000000000
          Width = 79.370081180000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'dh Recibo')
          ParentFont = False
          WordWrap = False
        end
        object Memo91: TfrxMemoView
          Left = 566.929500000000000000
          Top = 22.677180000000000000
          Width = 71.811021180000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186' protocolo canc.')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 404.409710000000000000
        Width = 990.236860000000000000
        DataSet = frxDsMDFe_101
        DataSetName = 'frxDsMDFe_101'
        RowCount = 0
        object Memo30: TfrxMemoView
          Left = 177.637910000000000000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_101."NOME_tpEmis"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          Left = 245.669450000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_101."NOME_tpNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39', <frxDsCTe_101."ide_nNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Left = 64.252010000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_XXX
          DataSetName = 'frxDsMDFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_101."ide_serie"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          Left = 132.283550000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_XXX
          DataSetName = 'frxDsMDFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_101."ide_dEmi"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          Left = 86.929190000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_XXX
          DataSetName = 'frxDsMDFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_101."ide_AAMM_AA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo49: TfrxMemoView
          Left = 109.606370000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_XXX
          DataSetName = 'frxDsMDFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_101."ide_AAMM_MM"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo51: TfrxMemoView
          Left = 260.787570000000000000
          Width = 226.771653540000000000
          Height = 13.228346460000000000
          DataField = 'Id_TXT'
          DataSet = frxDsMDFe_101
          DataSetName = 'frxDsMDFe_101'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCTe_101."Id_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo53: TfrxMemoView
          Left = 638.740570000000000000
          Width = 351.496260710000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_XXX
          DataSetName = 'frxDsMDFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCTe_101."Motivo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo90: TfrxMemoView
          Left = 487.559370000000000000
          Width = 79.370081180000000000
          Height = 13.228346460000000000
          DataField = 'infCanc_dhRecbto'
          DataSet = frxDsMDFe_101
          DataSetName = 'frxDsMDFe_101'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCTe_101."infCanc_dhRecbto"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo92: TfrxMemoView
          Left = 566.929500000000000000
          Width = 71.811021180000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_101
          DataSetName = 'frxDsMDFe_101'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCTe_101."infCanc_nProt"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        Height = 35.905526460000000000
        Top = 464.882190000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsCTe_XXX."Ordem"'
        object Memo70: TfrxMemoView
          Left = 177.637910000000000000
          Top = 22.677180000000000000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          Left = 245.669450000000000000
          Top = 22.677180000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'E/S')
          ParentFont = False
          WordWrap = False
        end
        object Memo72: TfrxMemoView
          Top = 3.779530000000000000
          Width = 990.236860000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCTe_XXX."NOME_ORDEM"]')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Top = 22.677180000000000000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#186' NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo74: TfrxMemoView
          Left = 64.252010000000000000
          Top = 22.677180000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
        end
        object Memo75: TfrxMemoView
          Left = 132.283550000000000000
          Top = 22.677180000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo76: TfrxMemoView
          Left = 86.929190000000000000
          Top = 22.677180000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ano')
          ParentFont = False
          WordWrap = False
        end
        object Memo77: TfrxMemoView
          Left = 109.606370000000000000
          Top = 22.677180000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
          WordWrap = False
        end
        object Memo78: TfrxMemoView
          Left = 260.787570000000000000
          Top = 22.677180000000000000
          Width = 30.236240000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Status')
          ParentFont = False
          WordWrap = False
        end
        object Memo79: TfrxMemoView
          Left = 291.023810000000000000
          Top = 22.677180000000000000
          Width = 699.213020710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Motivo ou descri'#231#227'o do status')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 525.354670000000000000
        Width = 990.236860000000000000
        DataSet = frxDsMDFe_XXX
        DataSetName = 'frxDsMDFe_XXX'
        RowCount = 0
        object Memo80: TfrxMemoView
          Left = 177.637910000000000000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_XXX."NOME_tpEmis"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo81: TfrxMemoView
          Left = 245.669450000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_XXX."NOME_tpNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo82: TfrxMemoView
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39', <frxDsCTe_XXX."ide_nNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo83: TfrxMemoView
          Left = 64.252010000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataField = 'ide_serie'
          DataSet = frxDsMDFe_XXX
          DataSetName = 'frxDsMDFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_XXX."ide_serie"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo84: TfrxMemoView
          Left = 132.283550000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'ide_dEmi'
          DataSet = frxDsMDFe_XXX
          DataSetName = 'frxDsMDFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_XXX."ide_dEmi"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo85: TfrxMemoView
          Left = 86.929190000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_XXX
          DataSetName = 'frxDsMDFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_XXX."ide_AAMM_AA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo86: TfrxMemoView
          Left = 109.606370000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataField = 'ide_AAMM_MM'
          DataSet = frxDsMDFe_XXX
          DataSetName = 'frxDsMDFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_XXX."ide_AAMM_MM"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo87: TfrxMemoView
          Left = 260.787570000000000000
          Width = 30.236240000000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_XXX
          DataSetName = 'frxDsMDFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsCTe_XXX."ide_serie">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo88: TfrxMemoView
          Left = 291.023810000000000000
          Width = 699.213020710000000000
          Height = 13.228346460000000000
          DataField = 'Motivo'
          DataSet = frxDsMDFe_XXX
          DataSetName = 'frxDsMDFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCTe_XXX."Motivo"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Top = 442.205010000000000000
        Width = 990.236860000000000000
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Top = 563.149970000000000000
        Width = 990.236860000000000000
      end
    end
  end
  object QrMDFe_100: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM nfe_100'
      'ORDER BY ide_nNF')
    Left = 20
    Top = 376
    object QrMDFe_100Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrMDFe_100ide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrMDFe_100ide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrMDFe_100ide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrMDFe_100ICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
    end
    object QrMDFe_100ICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
    end
    object QrMDFe_100ICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
    end
    object QrMDFe_100ICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
    end
    object QrMDFe_100ICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
    end
    object QrMDFe_100ICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
    end
    object QrMDFe_100ICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
    end
    object QrMDFe_100ICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
    end
    object QrMDFe_100ICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
    end
    object QrMDFe_100ICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
    end
    object QrMDFe_100ICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
    end
    object QrMDFe_100ICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
    end
    object QrMDFe_100NOME_tpEmis: TWideStringField
      FieldName = 'NOME_tpEmis'
      Size = 30
    end
    object QrMDFe_100NOME_tpNF: TWideStringField
      FieldName = 'NOME_tpNF'
      Size = 1
    end
    object QrMDFe_100ide_NatOP: TWideStringField
      FieldName = 'ide_NatOP'
      Size = 60
    end
    object QrMDFe_100PesoB: TFloatField
      FieldName = 'PesoB'
    end
    object QrMDFe_100PesoL: TFloatField
      FieldName = 'PesoL'
    end
    object QrMDFe_100nVol: TWideStringField
      FieldName = 'nVol'
      Size = 60
    end
    object QrMDFe_100marca: TWideStringField
      FieldName = 'marca'
      Size = 60
    end
    object QrMDFe_100esp: TWideStringField
      FieldName = 'esp'
      Size = 60
    end
    object QrMDFe_100qVol: TFloatField
      FieldName = 'qVol'
    end
  end
  object frxDsMDFe_100: TfrxDBDataset
    UserName = 'frxDsMDFe_100'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Ordem=Ordem'
      'ide_nNF=ide_nNF'
      'ide_serie=ide_serie'
      'ide_dEmi=ide_dEmi'
      'ICMSTot_vProd=ICMSTot_vProd'
      'ICMSTot_vST=ICMSTot_vST'
      'ICMSTot_vFrete=ICMSTot_vFrete'
      'ICMSTot_vSeg=ICMSTot_vSeg'
      'ICMSTot_vIPI=ICMSTot_vIPI'
      'ICMSTot_vOutro=ICMSTot_vOutro'
      'ICMSTot_vDesc=ICMSTot_vDesc'
      'ICMSTot_vNF=ICMSTot_vNF'
      'ICMSTot_vBC=ICMSTot_vBC'
      'ICMSTot_vICMS=ICMSTot_vICMS'
      'ICMSTot_vPIS=ICMSTot_vPIS'
      'ICMSTot_vCOFINS=ICMSTot_vCOFINS'
      'NOME_tpEmis=NOME_tpEmis'
      'NOME_tpNF=NOME_tpNF'
      'ide_NatOP=ide_NatOP'
      'PesoB=PesoB'
      'PesoL=PesoL'
      'nVol=nVol'
      'marca=marca'
      'esp=esp'
      'qVol=qVol')
    DataSet = QrMDFe_100
    BCDToCurrency = False
    Left = 20
    Top = 424
  end
  object QrMDFe_XXX: TmySQLQuery
    Database = Dmod.ZZDB
    OnCalcFields = QrMDFe_XXXCalcFields
    SQL.Strings = (
      'SELECT * FROM nfe_xxx'
      'ORDER BY Ordem, ide_nNF;')
    Left = 188
    Top = 376
    object QrMDFe_XXXOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrMDFe_XXXcStat: TIntegerField
      FieldName = 'cStat'
    end
    object QrMDFe_XXXide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrMDFe_XXXide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrMDFe_XXXide_AAMM_AA: TWideStringField
      FieldName = 'ide_AAMM_AA'
      Size = 2
    end
    object QrMDFe_XXXide_AAMM_MM: TWideStringField
      FieldName = 'ide_AAMM_MM'
      Size = 2
    end
    object QrMDFe_XXXide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrMDFe_XXXNOME_tpEmis: TWideStringField
      FieldName = 'NOME_tpEmis'
      Size = 30
    end
    object QrMDFe_XXXNOME_tpNF: TWideStringField
      FieldName = 'NOME_tpNF'
      Size = 1
    end
    object QrMDFe_XXXStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrMDFe_XXXMotivo: TWideStringField
      FieldName = 'Motivo'
      Size = 255
    end
    object QrMDFe_XXXNOME_ORDEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_ORDEM'
      Size = 50
      Calculated = True
    end
  end
  object frxDsMDFe_XXX: TfrxDBDataset
    UserName = 'frxDsMDFe_XXX'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Ordem=Ordem'
      'cStat=cStat'
      'ide_nNF=ide_nNF'
      'ide_serie=ide_serie'
      'ide_AAMM_AA=ide_AAMM_AA'
      'ide_AAMM_MM=ide_AAMM_MM'
      'ide_dEmi=ide_dEmi'
      'NOME_tpEmis=NOME_tpEmis'
      'NOME_tpNF=NOME_tpNF'
      'Status=Status'
      'Motivo=Motivo'
      'NOME_ORDEM=NOME_ORDEM')
    DataSet = QrMDFe_XXX
    BCDToCurrency = False
    Left = 188
    Top = 424
  end
  object QrMDFe_101: TmySQLQuery
    Database = Dmod.ZZDB
    OnCalcFields = QrMDFe_101CalcFields
    SQL.Strings = (
      'SELECT * FROM nfe_101'
      'ORDER BY ide_nNF')
    Left = 100
    Top = 376
    object QrMDFe_101Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrMDFe_101ide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrMDFe_101ide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrMDFe_101ide_AAMM_AA: TWideStringField
      FieldName = 'ide_AAMM_AA'
      Size = 2
    end
    object QrMDFe_101ide_AAMM_MM: TWideStringField
      FieldName = 'ide_AAMM_MM'
      Size = 2
    end
    object QrMDFe_101ide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrMDFe_101NOME_tpEmis: TWideStringField
      FieldName = 'NOME_tpEmis'
      Size = 30
    end
    object QrMDFe_101NOME_tpNF: TWideStringField
      FieldName = 'NOME_tpNF'
      Size = 1
    end
    object QrMDFe_101Id: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrMDFe_101infCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
    end
    object QrMDFe_101infCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Size = 15
    end
    object QrMDFe_101Motivo: TWideStringField
      FieldName = 'Motivo'
      Size = 255
    end
    object QrMDFe_101Id_TXT: TWideStringField
      DisplayWidth = 100
      FieldKind = fkCalculated
      FieldName = 'Id_TXT'
      Size = 100
      Calculated = True
    end
  end
  object frxDsMDFe_101: TfrxDBDataset
    UserName = 'frxDsMDFe_101'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Ordem=Ordem'
      'ide_nNF=ide_nNF'
      'ide_serie=ide_serie'
      'ide_AAMM_AA=ide_AAMM_AA'
      'ide_AAMM_MM=ide_AAMM_MM'
      'ide_dEmi=ide_dEmi'
      'NOME_tpEmis=NOME_tpEmis'
      'NOME_tpNF=NOME_tpNF'
      'Id=Id'
      'infCanc_dhRecbto=infCanc_dhRecbto'
      'infCanc_nProt=infCanc_nProt'
      'Motivo=Motivo'
      'Id_TXT=Id_TXT')
    DataSet = QrMDFe_101
    BCDToCurrency = False
    Left = 100
    Top = 424
  end
  object frxA4A_002: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40022.702970393500000000
    ReportOptions.LastChange = 41548.400113136580000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'procedure MasterData1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      
        '  Child1.Visible := True;                                       ' +
        '           '
      'end;'
      ''
      'procedure Child1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  Child1.Visible := False;  '
      'end;'
      ''
      'procedure MeFlowToHidOnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  if MeFlowToHid.Memo.Text <> '#39#39' then'
      '  begin                '
      '    Page3.Visible := True;'
      '    MeFlowToSee.Memo.Text := MeFlowToHid.Memo.Text;'
      '  end;              '
      'end;'
      ''
      'var'
      '  Altura: Extended;                                      '
      'procedure Me_ICMS_CSTOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <frxDsN."ICMS_CST"> = 0 then'
      
        '    Me_ICMS_CST.Memo.Text := FormatFloat('#39'000'#39', <frxDsN."ICMS_CS' +
        'OSN">)  '
      '  else'
      
        '    Me_ICMS_CST.Memo.Text := FormatFloat('#39'000'#39', <frxDsN."ICMS_CS' +
        'T">);                                                      '
      'end;'
      ''
      'begin'
      '  if <LogoNFeExiste> = True then'
      '  begin              '
      '    Picture1.LoadFromFile(<LogoNFePath>);'
      '    Picture2.LoadFromFile(<LogoNFePath>);'
      '    Picture3.LoadFromFile(<LogoNFePath>);'
      '  end;'
      '  //        '
      '  Altura := <NFeItsLin>;'
      
        '  Line1.Top := Altura;                                          ' +
        '    '
      
        '  Me_prod_cProd.Height := Altura;                               ' +
        '                         '
      
        '  Me_prod_xProd.Height := Altura;                               ' +
        '                         '
      
        '  Me_prod_NCM.Height := Altura;                                 ' +
        '                       '
      
        '  Me_ICMS_CST.Height := Altura;                                 ' +
        '                       '
      
        '  Me_prod_CFOP.Height := Altura;                                ' +
        '                        '
      
        '  Me_prod_uCom.Height := Altura;                                ' +
        '                        '
      
        '  Me_prod_qCom.Height := Altura;                                ' +
        '                        '
      
        '  Me_prod_vUnCom.Height := Altura;                              ' +
        '                          '
      
        '  Me_prod_vProd.Height := Altura;                               ' +
        '                         '
      
        '  Me_ICMS_vBC.Height := Altura;                                 ' +
        '                       '
      
        '  Me_ICMS_vICMS.Height := Altura;                               ' +
        '                         '
      
        '  Me_IPI_vIPI.Height := Altura;                                 ' +
        '                       '
      
        '  Me_ICMS_vICMS.Height := Altura;                               ' +
        '                         '
      '  Me_ICMS_pICMS.Height := Altura;'
      '  Me_IPI_pIPI.Height := Altura;'
      '  DD_prod.Height := Altura;'
      '  //'
      '  MeRazao_1.Font.Size := <NFeFTRazao>;'
      '  MeRazao_2.Font.Size := <NFeFTRazao>;'
      '  MeRazao_3.Font.Size := <NFeFTRazao>;'
      '  MeEnder_1.Font.Size := <NFeFTEnder>;'
      '  MeEnder_2.Font.Size := <NFeFTEnder>;'
      '  MeEnder_3.Font.Size := <NFeFTEnder>;'
      '  MeFones_1.Font.Size := <NFeFTFones>;'
      '  MeFones_2.Font.Size := <NFeFTFones>;'
      '  MeFones_3.Font.Size := <NFeFTFones>;'
      '  //        '
      'end.')
    Left = 264
    Top = 336
    Datasets = <
      item
        DataSet = frxDsA
        DataSetName = 'frxDsA'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      MirrorMargins = True
      LargeDesignHeight = True
      object Memo12: TfrxMemoView
        Tag = 2
        Top = 355.275820000000000000
        Width = 793.701300000000000000
        Height = 559.370440000000000000
        Visible = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 15000804
        Font.Height = -117
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          'SEM VALOR')
        ParentFont = False
        Rotation = 45
        VAlign = vaCenter
      end
      object Memo86: TfrxMemoView
        Left = 30.236240000000000000
        Top = 914.646260000000000000
        Width = 733.228346460000000000
        Height = 15.874015750000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'C'#193'LCULO DO ISSQN')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo87: TfrxMemoView
        Tag = 1
        Left = 30.236240000000000000
        Top = 930.520275750000000000
        Width = 183.307086610000000000
        Height = 32.125984250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'INSCRI'#199#195'O MUNICIPAL')
        ParentFont = False
      end
      object Memo88: TfrxMemoView
        Tag = 1
        Left = 30.236240000000000000
        Top = 939.969094650000000000
        Width = 183.307086610000000000
        Height = 22.677180000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsA."emit_IM"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo106: TfrxMemoView
        Tag = 1
        Left = 213.543307090000000000
        Top = 930.520275750000000000
        Width = 183.307086610000000000
        Height = 32.125984250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'VALOR TOTAL DOS SERVI'#199'OS')
        ParentFont = False
      end
      object Memo107: TfrxMemoView
        Tag = 1
        Left = 213.543307090000000000
        Top = 939.969094650000000000
        Width = 183.307086610000000000
        Height = 22.677180000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          
            '[FormatFloat('#39'#,###,###,##0.00;#,###,###,##0.00; '#39',<frxDsA."ISSQ' +
            'Ntot_vServ">)]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo108: TfrxMemoView
        Tag = 1
        Left = 396.850393700000000000
        Top = 930.520275750000000000
        Width = 183.307086610000000000
        Height = 32.125984250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'BASE DE C'#193'LCULO DO ISSQN')
        ParentFont = False
      end
      object Memo109: TfrxMemoView
        Tag = 1
        Left = 396.850393700000000000
        Top = 939.969094650000000000
        Width = 183.307086610000000000
        Height = 22.677180000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          
            '[FormatFloat('#39'#,###,###,##0.00;#,###,###,##0.00; '#39',<frxDsA."ISSQ' +
            'Ntot_vBC">)]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo110: TfrxMemoView
        Tag = 1
        Left = 579.779530000000000000
        Top = 930.520275750000000000
        Width = 183.307086610000000000
        Height = 32.125984250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'VALOR DO ISSQN')
        ParentFont = False
      end
      object Memo111: TfrxMemoView
        Tag = 1
        Left = 579.779530000000000000
        Top = 939.969094650000000000
        Width = 183.307086610000000000
        Height = 22.677180000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          
            '[FormatFloat('#39'#,###,###,##0.00;#,###,###,##0.00; '#39',<frxDsA."ISSQ' +
            'Ntot_vISS">)]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo112: TfrxMemoView
        Left = 30.236240000000000000
        Top = 962.646260000000000000
        Width = 86.551181100000000000
        Height = 15.874015750000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'DADOS ADICIONAIS')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo113: TfrxMemoView
        Tag = 1
        Left = 30.236240000000000000
        Top = 978.520275750000000000
        Width = 445.984251970000000000
        Height = 116.031496060000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'INFORMA'#199#213'ES COMPLEMENTARES')
        ParentFont = False
      end
      object MeFlowToSor: TfrxMemoView
        Tag = 1
        Left = 30.236240000000000000
        Top = 987.969094650000000000
        Width = 445.984251970000000000
        Height = 106.582677170000000000
        FlowTo = frxA4A_002.MeFlowToHid
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          '[frxDsA."InfAdic_InfAdFisco"]'
          '[frxDsA."InfAdic_InfCpl"]')
        ParentFont = False
      end
      object Memo115: TfrxMemoView
        Tag = 1
        Left = 476.220472440000000000
        Top = 978.520275750000000000
        Width = 287.244094490000000000
        Height = 116.031496060000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'RESERVADO AO FISCO')
        ParentFont = False
      end
      object Memo116: TfrxMemoView
        Tag = 1
        Left = 476.220472440000000000
        Top = 991.748624650000000000
        Width = 287.244094490000000000
        Height = 102.803147170000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haRight
        ParentFont = False
      end
      object MeFlowToHid: TfrxMemoView
        Left = 30.236240000000000000
        Top = 1099.843230000000000000
        Width = 733.228820000000000000
        Height = 7.559060000000000000
        OnAfterData = 'MeFlowToHidOnAfterData'
        OnAfterPrint = 'MeFlowToHidOnAfterPrint'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Width = 0.100000000000000000
        ParentFont = False
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 80.472438500000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo13: TfrxMemoView
          Tag = 2
          Left = 30.236227800000000000
          Top = 16.118107800000000000
          Width = 733.228331810000000000
          Height = 319.370078740000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 15000804
          Font.Height = -128
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '   PREVIEW')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeCanhotoRec: TfrxMemoView
          Left = 30.236240000000000000
          Top = 16.220470000000000000
          Width = 563.149577010000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              'RECEBEMOS DE [frxDsA."emit_xNome"] OS PRODUTOS E/OU SERVI'#199'OS CON' +
              'STANTES DA NOTA FISCAL ELETR'#212'NICA INDICADA AO LADO')
          ParentFont = False
        end
        object MeCanhotoNFe: TfrxMemoView
          Left = 593.385817010000000000
          Top = 16.220470000000000000
          Width = 170.078740160000000000
          Height = 64.251968500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object MeCanhotoDat: TfrxMemoView
          Left = 30.236240000000000000
          Top = 48.346454249999990000
          Width = 154.960629920000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DATA DE RECEBIMENTO')
          ParentFont = False
        end
        object MeCanhotoIde: TfrxMemoView
          Left = 185.196869920000000000
          Top = 48.346454249999990000
          Width = 408.188944650000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IDENTIFICA'#199#195'O E ASSINATURA DO RECEBEDOR')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 593.385817010000000000
          Top = 16.118120000000000000
          Width = 170.078850000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'NF-e')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 593.275981100000000000
          Top = 31.992135750000000000
          Width = 22.677180000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186)
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 615.953161100000000000
          Top = 31.992135750000000000
          Width = 147.401670000000000000
          Height = 15.874015750000000000
          DisplayFormat.FormatStr = '%2.2m'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = 14211288
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nNF">)]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 593.496451100000000000
          Top = 48.244104250000010000
          Width = 37.795300000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 631.291751100000000000
          Top = 48.244104250000010000
          Width = 128.504020000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39',<frxDsA."ide_serie">)]')
          ParentFont = False
        end
        object Memo146: TfrxMemoView
          Left = 32.126011100000000000
          Top = 24.811033380000000000
          Width = 559.370244720000000000
          Height = 21.543307090000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_JUR"]'
            '[frxDsA."DOC_SEM_VLR_FIS"]')
          ParentFont = False
        end
      end
      object Line6: TfrxLineView
        Left = 30.236220470000000000
        Top = 695.433070866142000000
        Height = 219.212598425197000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line7: TfrxLineView
        Left = 68.031496060000000000
        Top = 695.433070866142000000
        Height = 219.212598425197000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line8: TfrxLineView
        Left = 325.039370080000000000
        Top = 695.433070866142000000
        Height = 219.212598425197000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line14: TfrxLineView
        Left = 362.834645669291000000
        Top = 695.433070866142000000
        Height = 219.212598425197000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line15: TfrxLineView
        Left = 381.732283464567000000
        Top = 695.433070866142000000
        Height = 219.212598425197000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line16: TfrxLineView
        Left = 404.409448818898000000
        Top = 695.433070866142000000
        Height = 219.212598425197000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line17: TfrxLineView
        Left = 442.204724409449000000
        Top = 695.433070866142000000
        Height = 219.212598425197000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line18: TfrxLineView
        Left = 491.338900000000000000
        Top = 695.433070866142000000
        Height = 219.212598425197000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line19: TfrxLineView
        Left = 536.692913390000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line20: TfrxLineView
        Left = 582.047244094487900000
        Top = 695.433070866142000000
        Height = 219.212598425197000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line21: TfrxLineView
        Left = 627.401574803150000000
        Top = 695.433070866142000000
        Height = 219.212598425197000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line22: TfrxLineView
        Left = 672.755905511810900000
        Top = 695.433070866142000000
        Height = 219.212598425197000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line24: TfrxLineView
        Left = 718.110236220471900000
        Top = 695.433070866142000000
        Height = 219.212598425197000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line25: TfrxLineView
        Left = 740.787401574802900000
        Top = 695.433070866142000000
        Height = 219.212598425197000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line26: TfrxLineView
        Left = 763.464566929134000000
        Top = 695.433070866142000000
        Height = 219.212598425197000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object PageHeader1: TfrxPageHeader
        Tag = 1
        FillType = ftBrush
        Height = 232.818897640000000000
        Top = 120.944960000000000000
        Width = 793.701300000000000000
        object Memo6: TfrxMemoView
          Left = 30.236240000000000000
          Top = 20.511749999999990000
          Width = 332.598425200000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Tag = 1
          Left = 35.905511810000000000
          Top = 30.236220469999990000
          Width = 128.503937010000000000
          Height = 128.503937010000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo11: TfrxMemoView
          Left = 457.323130000000000000
          Top = 168.566929130000000000
          Width = 306.141654170000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PROTOCOLO DE AUTORIZA'#199#195'O DE USO')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.511749999999990000
          Width = 94.488188980000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 457.322834650000000000
          Top = 20.511749999999990000
          Width = 306.141732280000000000
          Height = 55.937007870000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object BarCode1: TfrxBarCodeView
          Left = 466.771641340000000000
          Top = 30.338589999999990000
          Width = 277.000000000000000000
          Height = 37.795275590000000000
          BarType = bcCode128C
          DataField = 'Id'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Rotation = 0
          ShowText = False
          Text = '00000000000000000000000000000000000000000000'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Memo9: TfrxMemoView
          Left = 457.322834650000000000
          Top = 76.448757870000000000
          Width = 306.141732280000000000
          Height = 35.905514250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'CHAVE DE ACESSO P/ CONSULTA DE AUTENTICIDADE NO PORTAL NACIONAL ' +
              'DA NF-E WWW.NFE.FAZENDA.GOV.BR/PORTAL')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 458.834635900000000000
          Top = 93.488250000000010000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ID_TXT"]')
          ParentFont = False
        end
        object MeRazao_1: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 30.236239999999990000
          Width = 192.756030000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_xNome"]')
          ParentFont = False
        end
        object MeEnder_1: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 68.031539999999990000
          Width = 192.756030000000000000
          Height = 77.480314960000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."EMIT_ENDERECO"]')
          ParentFont = False
        end
        object MeFones_1: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 145.511811020000000000
          Width = 192.756030000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TELEFONE: [frxDsA."EMIT_FONE_TXT"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 168.566929130000000000
          Width = 427.086614170000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NATUREZA DA OPERA'#199#195'O')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 176.259783940000000000
          Width = 426.330708660000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."ide_natOp"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo20: TfrxMemoView
          Left = 457.322834650000000000
          Top = 112.251968500000000000
          Width = 306.141732280000000000
          Height = 56.314960630000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 457.322834650000000000
          Top = 176.125940310000000000
          Width = 306.141732280000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_FIS"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 200.692913390000000000
          Width = 229.039350550000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 200.692913390000000000
          Width = 243.401574800000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL DO SUBSTITUTO TRIBUT'#193'RIO')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Tag = 1
          Left = 502.677165350000000000
          Top = 200.692913390000000000
          Width = 260.787401570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 209.519626460000000000
          Width = 229.039350550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IE_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 209.519626460000000000
          Width = 242.645669290000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IEST_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          Tag = 1
          Left = 503.433061100000000000
          Top = 209.519626460000000000
          Width = 259.275590550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Left = 30.236240000000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo147: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.409399999999990000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DANFE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo192: TfrxMemoView
          Left = 362.834635900000000000
          Top = 40.574830000000010000
          Width = 96.000000000000000000
          Height = 35.905511810000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DOCUMENTO AUXILIAR'
            'DA NOTA FISCAL'
            'ELETR'#212'NICA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo193: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 76.724360629999990000
          Width = 96.000000000000000000
          Height = 31.748031500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            ' 0 - ENTRADA'
            ' 1 - SA'#205'DA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo194: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 108.472392130000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186' [FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nNF">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo195: TfrxMemoView
          Tag = 1
          Left = 432.756191100000000000
          Top = 80.503888190000010000
          Width = 16.629870000000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_tpNF"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo196: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 128.504020000000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#201'RIE:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo197: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 148.157431500000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FOLHA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo198: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 128.504020000000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39',<frxDsA."ide_serie">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo199: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 148.157431500000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Page#]/[TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Left = 697.323291100000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo200: TfrxMemoView
          Left = 460.102364650000000000
          Top = 116.409448820000000000
          Width = 302.362204720000000000
          Height = 51.023634250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_JUR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object BarCode2: TfrxBarCodeView
          Left = 466.771961100000000000
          Top = 120.944960000000000000
          Width = 79.000000000000000000
          Height = 37.795275590000000000
          Visible = False
          BarType = bcCode128C
          Rotation = 0
          ShowText = False
          Text = '12345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 242.000000000000000000
      PaperSize = 256
      LargeDesignHeight = True
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        Tag = 1
        FillType = ftBrush
        Height = 132.284294490000000000
        Top = 355.275820000000000000
        Width = 793.701300000000000000
        OnAfterPrint = 'MasterData1OnAfterPrint'
        RowCount = 1
        object Me_E04_Titu: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 12.094485749999990000
          Width = 419.527559060000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NOME / RAZ'#195'O SOCIAL')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 124.724409450000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESTINAT'#193'RIO / REMETENTE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Me_E02_Titu: TfrxMemoView
          Tag = 1
          Left = 449.763779530000000000
          Top = 12.094485749999990000
          Width = 204.094488190000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ / CPF')
          ParentFont = False
        end
        object Me_B09_Titu: TfrxMemoView
          Tag = 1
          Left = 653.858267720000000000
          Top = 12.094485749999990000
          Width = 109.606299210000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DATA DA EMISS'#195'O')
          ParentFont = False
        end
        object Me_E04_Dado: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 20.165342130000000000
          Width = 419.527559060000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."dest_xNome"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaBottom
        end
        object Me_E02_Dado: TfrxMemoView
          Tag = 1
          Left = 449.763779530000000000
          Top = 20.165342130000000000
          Width = 204.094488190000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_CNPJ_CPF_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Me_B09_Dado: TfrxMemoView
          Tag = 1
          Left = 651.968931100000000000
          Top = 20.165342130000000000
          Width = 110.362204720000000000
          Height = 24.188976380000000000
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_dEmi"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo27: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 44.220469999999970000
          Width = 351.496062990000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ENDERE'#199'O')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 52.291326379999990000
          Width = 351.496062990000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DEST_ENDERECO"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo29: TfrxMemoView
          Tag = 1
          Left = 381.732283460000000000
          Top = 44.220469999999970000
          Width = 185.196850390000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'BAIRRO / DISTRITO')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Tag = 1
          Left = 381.732283460000000000
          Top = 52.291326379999990000
          Width = 185.196850390000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."dest_xBairro"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo31: TfrxMemoView
          Tag = 1
          Left = 566.929133860000000000
          Top = 44.220469999999970000
          Width = 86.929133860000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CEP')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Tag = 1
          Left = 566.929133860000000000
          Top = 52.291326379999990000
          Width = 86.929133860000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."dest_CEP_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo33: TfrxMemoView
          Tag = 1
          Left = 653.858267720000000000
          Top = 44.220469999999970000
          Width = 109.606299210000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DATA DA ENTRADA / SA'#205'DA')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Tag = 1
          Left = 651.968494170000000000
          Top = 52.291326379999990000
          Width = 110.362204720000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_dSaiEnt_Txt"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo15: TfrxMemoView
          Tag = 1
          Left = 653.858267720000000000
          Top = 76.346454250000030000
          Width = 109.606299210000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'HORA DA ENTRADA / SA'#205'DA')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Tag = 1
          Left = 652.346446930000000000
          Top = 84.417310629999970000
          Width = 110.362204720000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordBreak = True
        end
        object Memo35: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 76.346454250000030000
          Width = 268.724409450000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MUNIC'#205'PIO')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 84.417310629999970000
          Width = 268.724409450000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_XMUN_TXT"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo37: TfrxMemoView
          Tag = 1
          Left = 298.960649450000000000
          Top = 76.346454250000030000
          Width = 123.212578900000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FONE / FAX')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Tag = 1
          Left = 298.960649450000000000
          Top = 84.417310629999970000
          Width = 123.212578900000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_FONE_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo39: TfrxMemoView
          Tag = 1
          Left = 422.173228340000000000
          Top = 76.346454250000030000
          Width = 27.590551180000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Tag = 1
          Left = 422.173228340000000000
          Top = 84.417310629999970000
          Width = 27.590551180000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."dest_UF"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo41: TfrxMemoView
          Tag = 1
          Left = 449.763779530000000000
          Top = 76.346454250000030000
          Width = 204.094488190000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Tag = 1
          Left = 449.763779530000000000
          Top = 84.417310629999970000
          Width = 204.094488190000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_IE_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo43: TfrxMemoView
          Tag = 1
          Left = 30.236220470000000000
          Top = 110.118381180000000000
          Width = 124.724409450000000000
          Height = 12.094485750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FATURA / DUPLICATA')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo148: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 122.835194880000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo151: TfrxMemoView
          Tag = 1
          Left = 79.370149530000000000
          Top = 122.835194880000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCIMENTO')
          ParentFont = False
        end
        object Memo154: TfrxMemoView
          Tag = 1
          Left = 128.504039530000000000
          Top = 122.835194880000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Tag = 1
          Left = 177.637929530000000000
          Top = 122.835475590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo157: TfrxMemoView
          Tag = 1
          Left = 226.771839060000000000
          Top = 122.835475590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCIMENTO')
          ParentFont = False
        end
        object Memo160: TfrxMemoView
          Tag = 1
          Left = 275.905729060000000000
          Top = 122.835475590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo163: TfrxMemoView
          Tag = 1
          Left = 325.039599530000000000
          Top = 122.835475590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo166: TfrxMemoView
          Tag = 1
          Left = 374.173509060000000000
          Top = 122.835475590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCIMENTO')
          ParentFont = False
        end
        object Memo169: TfrxMemoView
          Tag = 1
          Left = 423.307399060000000000
          Top = 122.835475590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo172: TfrxMemoView
          Tag = 1
          Left = 472.441269530000000000
          Top = 122.835475590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo175: TfrxMemoView
          Tag = 1
          Left = 521.575179060000000000
          Top = 122.835475590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCIMENTO')
          ParentFont = False
        end
        object Memo178: TfrxMemoView
          Tag = 1
          Left = 570.709069060000000000
          Top = 122.835475590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo181: TfrxMemoView
          Tag = 1
          Left = 619.842939530000000000
          Top = 122.835475590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo184: TfrxMemoView
          Tag = 1
          Left = 668.976849060000000000
          Top = 122.835475590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCIMENTO')
          ParentFont = False
        end
        object Memo187: TfrxMemoView
          Tag = 1
          Left = 718.110739060000000000
          Top = 122.835475590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        Tag = 1
        FillType = ftBrush
        Height = 208.251968510000000000
        Top = 544.252320000000100000
        Width = 793.701300000000000000
        OnAfterPrint = 'MasterData2OnAfterPrint'
        RowCount = 1
        object Memo46: TfrxMemoView
          Tag = 1
          Left = 604.724409450000000000
          Top = 15.874015750000010000
          Width = 158.740167240000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL DOS PRODUTOS')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Tag = 1
          Left = 604.724409450000000000
          Top = 25.322834640000000000
          Width = 158.740167240000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vProd"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo48: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 15.874015750000010000
          Width = 143.622047244095000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'BASE DE C'#193'LCULO DO ICMS')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 25.322834640000000000
          Width = 143.622047240000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vBC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo50: TfrxMemoView
          Tag = 1
          Left = 173.858267720000000000
          Top = 15.874015750000010000
          Width = 143.622047240000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO ICMS')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Tag = 1
          Left = 173.858267720000000000
          Top = 25.322834640000000000
          Width = 143.622047240000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vICMS"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo52: TfrxMemoView
          Tag = 1
          Left = 317.480314960630000000
          Top = 15.874015750000010000
          Width = 143.622047240000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'BASE DE C'#193'LCULO DO ICMS ST')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Tag = 1
          Left = 317.480314960000000000
          Top = 25.322834640000000000
          Width = 143.622047240000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vBCST"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo54: TfrxMemoView
          Tag = 1
          Left = 461.102362204724000000
          Top = 15.874015750000010000
          Width = 143.622047240000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO ICMS SUBSTITUI'#199#195'O')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Tag = 1
          Left = 461.102362200000000000
          Top = 25.322834640000000000
          Width = 143.622047240000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vST"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo56: TfrxMemoView
          Tag = 1
          Left = 514.125986690000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL DO IPI')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Tag = 1
          Left = 514.125986690000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vIPI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo58: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 48.000000000000000000
          Width = 120.944881889764000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO FRETE')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vFrete"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo60: TfrxMemoView
          Tag = 1
          Left = 151.291348350000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO SEGURO')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Tag = 1
          Left = 151.291348350000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vSeg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo62: TfrxMemoView
          Tag = 1
          Left = 272.236227800000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESCONTO')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Tag = 1
          Left = 272.236227800000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vDesc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo64: TfrxMemoView
          Tag = 1
          Left = 393.181107240000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'OUTRAS DESPESAS ACESS'#211'RIAS')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Tag = 1
          Left = 393.181107240000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vOutro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo66: TfrxMemoView
          Tag = 1
          Left = 635.070866140000000000
          Top = 48.000000000000000000
          Width = 128.503937007874000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL DA NOTA')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Tag = 1
          Left = 635.070866140000000000
          Top = 57.448818899999990000
          Width = 128.503941890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vNF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo68: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 80.125984249999990000
          Width = 211.653543310000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TRANSPORTADOR / VOLUMES TRANSPORTADOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Tag = 1
          Left = 631.181085280000000000
          Top = 96.000000000000000000
          Width = 132.283464570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ / CPF')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Tag = 1
          Left = 631.181085280000000000
          Top = 105.448818900000000000
          Width = 132.283464570000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."TRANSPORTA_CNPJ_CPF_TXT"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo71: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 96.000000000000000000
          Width = 306.141732283465000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NOME / RAZ'#195'O SOCIAL')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 105.448818900000000000
          Width = 306.141732280000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."Transporta_XNome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo73: TfrxMemoView
          Tag = 1
          Left = 336.377950310000000000
          Top = 96.000000000000000000
          Width = 94.488188980000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FRETE POR CONTA')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Tag = 1
          Left = 336.377950310000000000
          Top = 105.448818900000000000
          Width = 90.708658980000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."MODFRETE_TXT"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo75: TfrxMemoView
          Tag = 1
          Left = 430.866141732283000000
          Top = 96.000000000000000000
          Width = 94.488188980000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#211'DIGO ANTT')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Tag = 1
          Left = 430.866141730000000000
          Top = 105.448818900000000000
          Width = 94.488188980000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."VeicTransp_RNTC"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo77: TfrxMemoView
          Tag = 1
          Left = 600.944864800000000000
          Top = 96.000000000000000000
          Width = 30.236220472440900000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Tag = 1
          Left = 600.944864800000000000
          Top = 105.448818900000000000
          Width = 30.236220470000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."VeicTransp_UF"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo79: TfrxMemoView
          Tag = 1
          Left = 600.944864800000000000
          Top = 128.125984250000000000
          Width = 30.236220472440900000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          Tag = 1
          Left = 600.944864800000000000
          Top = 137.574803150000000000
          Width = 30.236220470000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."Transporta_UF"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo81: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 128.125984250000000000
          Width = 340.157477870000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ENDERE'#199'O')
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 137.574803150000000000
          Width = 340.157477870000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."Transporta_XEnder"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo83: TfrxMemoView
          Tag = 1
          Left = 370.393698350000000000
          Top = 128.125984250000000000
          Width = 230.551164020000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MUNIC'#205'PIO')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Tag = 1
          Left = 370.393698350000000000
          Top = 137.574803150000000000
          Width = 230.551164020000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."Transporta_XMun"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo89: TfrxMemoView
          Tag = 1
          Left = 631.181085280000000000
          Top = 128.125984250000000000
          Width = 132.283464570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Tag = 1
          Left = 631.181085280000000000
          Top = 137.574803150000000000
          Width = 132.283464570000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."TRANSPORTA_IE_TXT"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo91: TfrxMemoView
          Tag = 1
          Left = 525.354313620000000000
          Top = 160.252373700000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PESO BRUTO')
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Tag = 1
          Left = 525.354313620000000000
          Top = 169.701192600000000000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0.000;-0.000; '#39', <VARF_XVOL_PESOB>)]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo93: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 160.252373700000000000
          Width = 109.606299212598000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'QUANTIDADE')
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 169.701192600000000000
          Width = 109.606299210000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <VARF_XVOL_QVOL>)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo95: TfrxMemoView
          Tag = 1
          Left = 139.842519690000000000
          Top = 160.252373700000000000
          Width = 113.385826771654000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ESP'#201'CIE')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Tag = 1
          Left = 139.842519690000000000
          Top = 169.701192600000000000
          Width = 113.385826770000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_XVOL_ESP]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo97: TfrxMemoView
          Tag = 1
          Left = 253.228346456693000000
          Top = 160.252373700000000000
          Width = 117.165354330000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MARCA')
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          Tag = 1
          Left = 253.228346460000000000
          Top = 169.701192600000000000
          Width = 117.165354330000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_XVOL_MARCA]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo99: TfrxMemoView
          Tag = 1
          Left = 370.393700790000000000
          Top = 160.252373700000000000
          Width = 154.960612830000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Tag = 1
          Left = 370.393700790000000000
          Top = 169.701192600000000000
          Width = 154.960612830000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_XVOL_NVOL]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo101: TfrxMemoView
          Tag = 1
          Left = 646.299212598425000000
          Top = 160.252373700000000000
          Width = 117.165354330709000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PESO L'#205'QUIDO')
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          Tag = 1
          Left = 646.299212600000000000
          Top = 169.701192600000000000
          Width = 117.165354330000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0.000;-0.000; '#39', <VARF_XVOL_PESOL>)]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo103: TfrxMemoView
          Tag = 1
          Left = 525.354330708661000000
          Top = 96.000363699999970000
          Width = 75.590551180000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PLACA DO VE'#205'CULO')
          ParentFont = False
        end
        object Memo104: TfrxMemoView
          Tag = 1
          Left = 525.354330710000000000
          Top = 105.449182600000000000
          Width = 75.590551180000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."VeicTransp_Placa"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo85: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 192.377952760000000000
          Width = 151.181102360000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DADOS DOS PRODUTOS / SERVI'#199'OS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 143.622047244094000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#193'LCULO DO IMPOSTO')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        Tag = 1
        FillType = ftBrush
        Height = 11.338582680000000000
        Top = 510.236550000000000000
        Width = 793.701300000000000000
        DataSet = Form1.frxDsCTeYIts
        DataSetName = 'frxDsCTeYIts'
        RowCount = 0
        object Memo149: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataField = 'nDup1'
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTeYIts."nDup1"]')
          ParentFont = False
        end
        object Memo150: TfrxMemoView
          Tag = 1
          Left = 79.370149530000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataField = 'xVenc1'
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTeYIts."xVenc1"]')
          ParentFont = False
        end
        object Memo152: TfrxMemoView
          Tag = 1
          Left = 128.504039530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.00'#39', <frxDsCTeYIts."vDup1">)]')
          ParentFont = False
        end
        object Memo153: TfrxMemoView
          Tag = 1
          Left = 177.637910000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTeYIts."nDup2"]')
          ParentFont = False
        end
        object Memo155: TfrxMemoView
          Tag = 1
          Left = 226.771819530000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTeYIts."xVenc2"]')
          ParentFont = False
        end
        object Memo156: TfrxMemoView
          Tag = 1
          Left = 275.905709530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.00'#39', <frxDsCTeYIts."vDup2">)]')
          ParentFont = False
        end
        object Memo158: TfrxMemoView
          Tag = 1
          Left = 325.039580000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTeYIts."nDup3"]')
          ParentFont = False
        end
        object Memo159: TfrxMemoView
          Tag = 1
          Left = 374.173489530000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTeYIts."xVenc3"]')
          ParentFont = False
        end
        object Memo161: TfrxMemoView
          Tag = 1
          Left = 423.307379530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.00'#39', <frxDsCTeYIts."vDup3">)]')
          ParentFont = False
        end
        object Memo162: TfrxMemoView
          Tag = 1
          Left = 472.441250000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTeYIts."nDup4"]')
          ParentFont = False
        end
        object Memo164: TfrxMemoView
          Tag = 1
          Left = 521.575159530000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTeYIts."xVenc4"]')
          ParentFont = False
        end
        object Memo165: TfrxMemoView
          Tag = 1
          Left = 570.709049530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.00'#39', <frxDsCTeYIts."vDup4">)]')
          ParentFont = False
        end
        object Memo167: TfrxMemoView
          Tag = 1
          Left = 619.842920000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTeYIts."nDup5"]')
          ParentFont = False
        end
        object Memo168: TfrxMemoView
          Tag = 1
          Left = 668.976829530000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTeYIts."xVenc5"]')
          ParentFont = False
        end
        object Memo170: TfrxMemoView
          Tag = 1
          Left = 718.110719530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.00'#39', <frxDsCTeYIts."vDup5">)]')
          ParentFont = False
        end
      end
      object DD_prod: TfrxDetailData
        Tag = 1
        FillType = ftBrush
        Height = 32.125984250000000000
        Top = 816.378480000000000000
        Width = 793.701300000000000000
        DataSet = Form1.frxDsI
        DataSetName = 'frxDsI'
        RowCount = 0
        object Me_prod_cProd: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 37.795280470000000000
          Height = 32.125984250000000000
          DataField = 'prod_cProd'
          DataSet = Form1.frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_cProd"]')
          ParentFont = False
        end
        object Me_prod_xProd: TfrxMemoView
          Tag = 1
          Left = 68.031520470000000000
          Width = 257.007903310000000000
          Height = 32.125984250000000000
          DataSet = Form1.frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_xProd"] [frxDsV."InfAdProd"]')
          ParentFont = False
        end
        object Me_prod_NCM: TfrxMemoView
          Tag = 1
          Left = 325.039406690000000000
          Width = 37.795275590000000000
          Height = 32.125984250000000000
          DataSet = Form1.frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_NCM"]')
          ParentFont = False
        end
        object Me_ICMS_CST: TfrxMemoView
          Tag = 1
          Left = 362.834687170000000000
          Width = 18.897635350000000000
          Height = 32.125984250000000000
          OnBeforePrint = 'Me_ICMS_CSTOnBeforePrint'
          DataSet = Form1.frxDsN
          DataSetName = 'frxDsN'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsN."ICMS_CST"]')
          ParentFont = False
        end
        object Me_prod_CFOP: TfrxMemoView
          Tag = 1
          Left = 381.732322520000000000
          Width = 22.677158030000000000
          Height = 32.125984250000000000
          DataSet = Form1.frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_CFOP"]')
          ParentFont = False
        end
        object Me_prod_uCom: TfrxMemoView
          Tag = 1
          Left = 404.409434170000000000
          Width = 37.795275590000000000
          Height = 32.125984250000000000
          DataSet = Form1.frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_uCom"]')
          ParentFont = False
        end
        object Me_prod_qCom: TfrxMemoView
          Tag = 1
          Left = 442.204700000000000000
          Width = 49.133865590000000000
          Height = 32.125984250000000000
          DataField = 'prod_qCom'
          DataSet = Form1.frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_qCom"]')
          ParentFont = False
        end
        object Me_prod_vUnCom: TfrxMemoView
          Tag = 1
          Left = 491.338582680000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          DataSet = Form1.frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.000000'#39', <frxDsI."prod_vUnCom">)]')
          ParentFont = False
        end
        object Me_prod_vProd: TfrxMemoView
          Tag = 1
          Left = 536.692898740000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          DataSet = Form1.frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsI."prod_vProd">)]')
          ParentFont = False
        end
        object Me_ICMS_vBC: TfrxMemoView
          Tag = 1
          Left = 582.047231890000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsN."ICMS_vBC">)]')
          ParentFont = False
        end
        object Me_ICMS_vICMS: TfrxMemoView
          Tag = 1
          Left = 627.401565040000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsN."ICMS_vICMS">)]')
          ParentFont = False
        end
        object Me_IPI_vIPI: TfrxMemoView
          Tag = 1
          Left = 672.755893310000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsO."IPI_vIPI">)]')
          ParentFont = False
        end
        object Me_ICMS_pICMS: TfrxMemoView
          Tag = 1
          Left = 718.110216690000000000
          Width = 22.677165350000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0.00;-0.00; '#39', <frxDsN."ICMS_pICMS">)]')
          ParentFont = False
        end
        object Me_IPI_pIPI: TfrxMemoView
          Tag = 1
          Left = 740.787401570000000000
          Width = 22.677165350000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0.00;-0.00; '#39', <frxDsO."IPI_pIPI">)]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Tag = 1
          Left = 30.236240000000000000
          Top = 32.125984249999990000
          Width = 733.228820000000000000
          StretchMode = smMaxHeight
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
      end
      object Header1: TfrxHeader
        Tag = 1
        FillType = ftBrush
        Height = 17.385826770000000000
        Top = 774.803650000000000000
        Width = 793.701300000000000000
        object Memo131: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 37.795280470000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#211'DIGO'
            'PRODUTO')
          ParentFont = False
        end
        object Memo132: TfrxMemoView
          Tag = 1
          Left = 68.031520470000000000
          Width = 257.007903310000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESCRI'#199#195'O DOS PRODUTOS / SERVI'#199'OS')
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          Tag = 1
          Left = 325.039406690000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NCM/SH')
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          Tag = 1
          Left = 362.834687170000000000
          Width = 18.897635350000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CST')
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          Tag = 1
          Left = 381.732322520000000000
          Width = 22.677158030000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CFOP')
          ParentFont = False
        end
        object Memo136: TfrxMemoView
          Tag = 1
          Left = 404.409434170000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UNIDADE')
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          Tag = 1
          Left = 442.204700000000000000
          Width = 49.133865590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'QUANTIDADE')
          ParentFont = False
        end
        object Memo138: TfrxMemoView
          Tag = 1
          Left = 491.338582680000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR UNIT'#193'RIO')
          ParentFont = False
        end
        object Memo139: TfrxMemoView
          Tag = 1
          Left = 536.692898740000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL')
          ParentFont = False
        end
        object Memo140: TfrxMemoView
          Tag = 1
          Left = 582.047231890000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'B.CALC. ICMS')
          ParentFont = False
        end
        object Memo141: TfrxMemoView
          Tag = 1
          Left = 627.401565040000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR ICMS')
          ParentFont = False
        end
        object Memo142: TfrxMemoView
          Tag = 1
          Left = 672.755893310000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR IPI')
          ParentFont = False
        end
        object Memo143: TfrxMemoView
          Tag = 1
          Left = 718.110216690000000000
          Top = 8.692476459999967000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ICMS')
          ParentFont = False
        end
        object Memo144: TfrxMemoView
          Tag = 1
          Left = 740.787401570000000000
          Top = 8.692476459999967000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IPI')
          ParentFont = False
        end
        object Memo145: TfrxMemoView
          Tag = 1
          Left = 718.110216690000000000
          Width = 45.354340470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'AL'#205'QUOTAS')
          ParentFont = False
        end
      end
      object Child1: TfrxChild
        Tag = 1
        FillType = ftBrush
        Height = 17.385826770000000000
        Top = 313.700990000000000000
        Visible = False
        Width = 793.701300000000000000
        OnAfterPrint = 'Child1OnAfterPrint'
        object Memo171: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 37.795280470000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#211'DIGO'
            'PRODUTO')
          ParentFont = False
        end
        object Memo173: TfrxMemoView
          Tag = 1
          Left = 68.031520470000000000
          Width = 257.007903310000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESCRI'#199#195'O DOS PRODUTOS / SERVI'#199'OS')
          ParentFont = False
        end
        object Memo174: TfrxMemoView
          Tag = 1
          Left = 325.039406690000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NCM/SH')
          ParentFont = False
        end
        object Memo176: TfrxMemoView
          Tag = 1
          Left = 362.834687170000000000
          Width = 18.897635350000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CST')
          ParentFont = False
        end
        object Memo177: TfrxMemoView
          Tag = 1
          Left = 381.732322520000000000
          Width = 22.677158030000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CFOP')
          ParentFont = False
        end
        object Memo179: TfrxMemoView
          Tag = 1
          Left = 404.409434170000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UNIDADE')
          ParentFont = False
        end
        object Memo180: TfrxMemoView
          Tag = 1
          Left = 442.204700000000000000
          Width = 49.133865590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'QUANTIDADE')
          ParentFont = False
        end
        object Memo182: TfrxMemoView
          Tag = 1
          Left = 491.338582680000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR UNIT'#193'RIO')
          ParentFont = False
        end
        object Memo183: TfrxMemoView
          Tag = 1
          Left = 536.692898740000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL')
          ParentFont = False
        end
        object Memo185: TfrxMemoView
          Tag = 1
          Left = 582.047231890000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'B.CALC. ICMS')
          ParentFont = False
        end
        object Memo186: TfrxMemoView
          Tag = 1
          Left = 627.401565040000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR ICMS')
          ParentFont = False
        end
        object Memo188: TfrxMemoView
          Tag = 1
          Left = 672.755893310000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR IPI')
          ParentFont = False
        end
        object Memo189: TfrxMemoView
          Tag = 1
          Left = 718.110216690000000000
          Top = 8.692476460000023000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ICMS')
          ParentFont = False
        end
        object Memo190: TfrxMemoView
          Tag = 1
          Left = 740.787401570000000000
          Top = 8.692476460000023000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IPI')
          ParentFont = False
        end
        object Memo191: TfrxMemoView
          Tag = 1
          Left = 718.110216690000000000
          Width = 45.354340470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'AL'#205'QUOTAS')
          ParentFont = False
        end
      end
      object PageHeader2: TfrxPageHeader
        Tag = 1
        FillType = ftBrush
        Height = 232.818897640000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo201: TfrxMemoView
          Left = 30.236240000000000000
          Top = 20.511750000000000000
          Width = 332.598425200000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo202: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.511750000000000000
          Width = 94.488188980000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo203: TfrxMemoView
          Left = 457.322834650000000000
          Top = 20.511750000000000000
          Width = 306.141732280000000000
          Height = 55.937007870000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object BarCode3: TfrxBarCodeView
          Left = 466.771641340000000000
          Top = 30.338590000000010000
          Width = 277.000000000000000000
          Height = 37.795275590000000000
          BarType = bcCode128C
          DataField = 'Id'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Rotation = 0
          ShowText = False
          Text = '00000000000000000000000000000000000000000000'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Memo204: TfrxMemoView
          Left = 457.322834650000000000
          Top = 76.448757870000000000
          Width = 306.141732280000000000
          Height = 35.905514250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'CHAVE DE ACESSO P/ CONSULTA DE AUTENTICIDADE NO PORTAL NACIONAL ' +
              'DA NF-E WWW.NFE.FAZENDA.GOV.BR/PORTAL')
          ParentFont = False
        end
        object Memo205: TfrxMemoView
          Left = 458.834635900000000000
          Top = 93.488250000000000000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ID_TXT"]')
          ParentFont = False
        end
        object MeRazao_2: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 30.236240000000000000
          Width = 192.756030000000000000
          Height = 41.574830000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_xNome"]')
          ParentFont = False
        end
        object MeEnder_2: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 71.811070000000000000
          Width = 192.756030000000000000
          Height = 71.811070000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."EMIT_ENDERECO"]')
          ParentFont = False
        end
        object MeFones_2: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 143.622140000000000000
          Width = 192.756030000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TELEFONE: [frxDsA."EMIT_FONE_TXT"]')
          ParentFont = False
        end
        object Memo210: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 168.566929130000000000
          Width = 427.086614170000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NATUREZA DA OPERA'#199#195'O')
          ParentFont = False
        end
        object Memo211: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 176.259783940000000000
          Width = 426.330708660000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."ide_natOp"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo212: TfrxMemoView
          Left = 457.322834650000000000
          Top = 112.251968503937000000
          Width = 306.141732280000000000
          Height = 56.314960629921300000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo213: TfrxMemoView
          Left = 457.322834650000000000
          Top = 168.566880310000000000
          Width = 306.141732280000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_FIS"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo214: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 200.692913390000000000
          Width = 229.039350550000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo215: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 200.692913390000000000
          Width = 243.401574800000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL DO SUBSTITUTO TRIBUT'#193'RIO')
          ParentFont = False
        end
        object Memo216: TfrxMemoView
          Tag = 1
          Left = 502.677165350000000000
          Top = 200.692913390000000000
          Width = 260.787401570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ')
          ParentFont = False
        end
        object Memo217: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 209.519626460000000000
          Width = 229.039350550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IE_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo218: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 209.519626460000000000
          Width = 242.645669290000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IEST_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo219: TfrxMemoView
          Tag = 1
          Left = 503.433061100000000000
          Top = 209.519626460000000000
          Width = 259.275590550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          Left = 30.236240000000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo250: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.409400000000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DANFE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo251: TfrxMemoView
          Left = 362.834635900000000000
          Top = 40.574830000000000000
          Width = 96.000000000000000000
          Height = 35.905511810000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DOCUMENTO AUXILIAR'
            'DA NOTA FISCAL'
            'ELETR'#212'NICA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo252: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 76.724360630000010000
          Width = 96.000000000000000000
          Height = 31.748031500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            ' 0 - ENTRADA'
            ' 1 - SA'#205'DA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo253: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 108.472392130000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186' [FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nNF">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo254: TfrxMemoView
          Tag = 1
          Left = 432.756191100000000000
          Top = 80.503888190000000000
          Width = 16.629870000000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_tpNF"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo255: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 128.504020000000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#201'RIE:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo256: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 148.157431500000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FOLHA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo257: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 128.504020000000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39',<frxDsA."ide_serie">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo258: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 148.157431500000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Page#]/[TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line10: TfrxLineView
          Left = 697.323291100000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo259: TfrxMemoView
          Left = 460.102364650000000000
          Top = 116.409448820000000000
          Width = 302.362204720000000000
          Height = 51.023634250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_JUR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object BarCode4: TfrxBarCodeView
          Left = 466.771961100000000000
          Top = 120.944960000000000000
          Width = 79.000000000000000000
          Height = 37.795275590000000000
          Visible = False
          BarType = bcCode128C
          Rotation = 0
          ShowText = False
          Text = '12345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Picture2: TfrxPictureView
          Tag = 1
          Left = 35.905511810000000000
          Top = 30.236240000000000000
          Width = 128.503937010000000000
          Height = 128.503937010000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
    end
    object Page3: TfrxReportPage
      Visible = False
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LargeDesignHeight = True
      object MasterData3: TfrxMasterData
        Tag = 1
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 313.700990000000000000
        Width = 793.701300000000000000
        AllowSplit = True
        DataSet = frxDsA
        DataSetName = 'frxDsA'
        RowCount = 0
        object MeFlowToSee: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 18.897650000000000000
          Width = 733.228820000000000000
          Height = 7.559060000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo249: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 7.559060000000000000
          Width = 733.228531970000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INFORMA'#199#213'ES COMPLEMENTARES (COMPLEMENTO DA PRIMEIRA FOLHA)')
          ParentFont = False
        end
      end
      object PageHeader3: TfrxPageHeader
        FillType = ftBrush
        Height = 232.818897640000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo220: TfrxMemoView
          Left = 30.236240000000000000
          Top = 20.511750000000000000
          Width = 332.598425200000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo221: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.511750000000000000
          Width = 94.488188980000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo222: TfrxMemoView
          Left = 457.322834650000000000
          Top = 20.511750000000000000
          Width = 306.141732280000000000
          Height = 55.937007870000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object BarCode5: TfrxBarCodeView
          Left = 466.771641340000000000
          Top = 30.338590000000000000
          Width = 277.000000000000000000
          Height = 37.795275590000000000
          BarType = bcCode128C
          DataField = 'Id'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Rotation = 0
          ShowText = False
          Text = '00000000000000000000000000000000000000000000'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Memo223: TfrxMemoView
          Left = 457.322834650000000000
          Top = 76.448757870000000000
          Width = 306.141732280000000000
          Height = 35.905514250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'CHAVE DE ACESSO P/ CONSULTA DE AUTENTICIDADE NO PORTAL NACIONAL ' +
              'DA NF-E WWW.NFE.FAZENDA.GOV.BR/PORTAL')
          ParentFont = False
        end
        object Memo224: TfrxMemoView
          Left = 458.834635900000000000
          Top = 93.488250000000000000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ID_TXT"]')
          ParentFont = False
        end
        object MeRazao_3: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 30.236240000000000000
          Width = 192.756030000000000000
          Height = 41.574830000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_xNome"]')
          ParentFont = False
        end
        object MeEnder_3: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 71.811070000000000000
          Width = 192.756030000000000000
          Height = 71.811070000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."EMIT_ENDERECO"]')
          ParentFont = False
        end
        object MeFones_3: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 143.622140000000000000
          Width = 192.756030000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TELEFONE: [frxDsA."EMIT_FONE_TXT"]')
          ParentFont = False
        end
        object Memo229: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 168.566929130000000000
          Width = 427.086614170000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NATUREZA DA OPERA'#199#195'O')
          ParentFont = False
        end
        object Memo230: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 176.259783940000000000
          Width = 426.330708660000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."ide_natOp"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo231: TfrxMemoView
          Left = 457.322834650000000000
          Top = 112.251968503937000000
          Width = 306.141732280000000000
          Height = 56.314960629921300000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo232: TfrxMemoView
          Left = 457.322834650000000000
          Top = 168.566880310000000000
          Width = 306.141732280000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_FIS"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo233: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 200.692913390000000000
          Width = 229.039350550000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo234: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 200.692913390000000000
          Width = 243.401574800000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL DO SUBSTITUTO TRIBUT'#193'RIO')
          ParentFont = False
        end
        object Memo235: TfrxMemoView
          Tag = 1
          Left = 502.677165350000000000
          Top = 200.692913390000000000
          Width = 260.787401570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ')
          ParentFont = False
        end
        object Memo236: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 209.519626460000000000
          Width = 229.039350550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IE_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo237: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 209.519626460000000000
          Width = 242.645669290000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IEST_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo238: TfrxMemoView
          Tag = 1
          Left = 503.433061100000000000
          Top = 209.519626460000000000
          Width = 259.275590550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line4: TfrxLineView
          Left = 30.236240000000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo239: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.409400000000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DANFE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo240: TfrxMemoView
          Left = 362.834635900000000000
          Top = 40.574830000000000000
          Width = 96.000000000000000000
          Height = 35.905511810000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DOCUMENTO AUXILIAR'
            'DA NOTA FISCAL'
            'ELETR'#212'NICA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo241: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 76.724360630000000000
          Width = 96.000000000000000000
          Height = 31.748031500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            ' 0 - ENTRADA'
            ' 1 - SA'#205'DA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo242: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 108.472392130000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186' [FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nNF">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo243: TfrxMemoView
          Tag = 1
          Left = 432.756191100000000000
          Top = 80.503888190000000000
          Width = 16.629870000000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_tpNF"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo244: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 128.504020000000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#201'RIE:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo245: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 148.157431500000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FOLHA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo246: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 128.504020000000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39',<frxDsA."ide_serie">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo247: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 148.157431500000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Page#]/[TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line5: TfrxLineView
          Left = 697.323291100000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo248: TfrxMemoView
          Left = 460.102364650000000000
          Top = 116.409448820000000000
          Width = 302.362204720000000000
          Height = 51.023634250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_JUR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object BarCode6: TfrxBarCodeView
          Left = 466.771961100000000000
          Top = 120.944960000000000000
          Width = 79.000000000000000000
          Height = 37.795275590000000000
          Visible = False
          BarType = bcCode128C
          Rotation = 0
          ShowText = False
          Text = '12345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Picture3: TfrxPictureView
          Tag = 1
          Left = 35.905511810000000000
          Top = 30.236240000000000000
          Width = 128.503937010000000000
          Height = 128.503937010000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
    end
  end
  object QrMDFeEveRRet: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrMDFeEveRRetCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM nfeeverret'
      'WHERE ret_chNFe="41130502717861000110550090000000061780170615" '
      'ORDER BY ret_dhRegEvento')
    Left = 596
    Top = 364
    object QrMDFeEveRRetControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMDFeEveRRetSubCtrl: TIntegerField
      FieldName = 'SubCtrl'
    end
    object QrMDFeEveRRetret_versao: TFloatField
      FieldName = 'ret_versao'
    end
    object QrMDFeEveRRetret_Id: TWideStringField
      FieldName = 'ret_Id'
      Size = 67
    end
    object QrMDFeEveRRetret_tpAmb: TSmallintField
      FieldName = 'ret_tpAmb'
    end
    object QrMDFeEveRRetret_verAplic: TWideStringField
      FieldName = 'ret_verAplic'
    end
    object QrMDFeEveRRetret_cOrgao: TSmallintField
      FieldName = 'ret_cOrgao'
    end
    object QrMDFeEveRRetret_cStat: TIntegerField
      FieldName = 'ret_cStat'
    end
    object QrMDFeEveRRetret_xMotivo: TWideStringField
      FieldName = 'ret_xMotivo'
      Size = 255
    end
    object QrMDFeEveRRetret_chMDFe: TWideStringField
      FieldName = 'ret_chMDFe'
      Size = 44
    end
    object QrMDFeEveRRetret_tpEvento: TIntegerField
      FieldName = 'ret_tpEvento'
    end
    object QrMDFeEveRRetret_xEvento: TWideStringField
      FieldName = 'ret_xEvento'
      Size = 60
    end
    object QrMDFeEveRRetret_nSeqEvento: TIntegerField
      FieldName = 'ret_nSeqEvento'
    end
    object QrMDFeEveRRetret_CNPJDest: TWideStringField
      FieldName = 'ret_CNPJDest'
      Size = 18
    end
    object QrMDFeEveRRetret_CPFDest: TWideStringField
      FieldName = 'ret_CPFDest'
      Size = 18
    end
    object QrMDFeEveRRetret_emailDest: TWideStringField
      FieldName = 'ret_emailDest'
      Size = 60
    end
    object QrMDFeEveRRetret_dhRegEvento: TDateTimeField
      FieldName = 'ret_dhRegEvento'
    end
    object QrMDFeEveRRetret_TZD_UTC: TFloatField
      FieldName = 'ret_TZD_UTC'
    end
    object QrMDFeEveRRetret_nProt: TWideStringField
      FieldName = 'ret_nProt'
      Size = 15
    end
    object QrMDFeEveRRetNO_EVENTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_EVENTO'
      Size = 255
      Calculated = True
    end
  end
  object DsMDFeEveRRet: TDataSource
    DataSet = QrMDFeEveRRet
    Left = 688
    Top = 364
  end
  object frxA4A_003: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40022.702970393500000000
    ReportOptions.LastChange = 41318.876849131900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'procedure MasterData1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      
        '  Child1.Visible := True;                                       ' +
        '           '
      'end;'
      ''
      'procedure Child1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  Child1.Visible := False;  '
      'end;'
      ''
      'procedure MeFlowToHidOnAfterPrint(Sender: TfrxComponent);'
      'begin'
      '  if MeFlowToHid.Memo.Text <> '#39#39' then'
      '  begin                '
      '    Page3.Visible := True;'
      '    MeFlowToSee.Memo.Text := MeFlowToHid.Memo.Text;'
      '  end;              '
      'end;'
      ''
      'var'
      '  Altura: Extended;                                      '
      'procedure Me_ICMS_CSTOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <frxDsN."ICMS_CST"> = 0 then'
      
        '    Me_ICMS_CST.Memo.Text := FormatFloat('#39'000'#39', <frxDsN."ICMS_CS' +
        'OSN">)  '
      '  else'
      
        '    Me_ICMS_CST.Memo.Text := FormatFloat('#39'000'#39', <frxDsN."ICMS_CS' +
        'T">);                                                      '
      'end;'
      ''
      'begin'
      '  if <LogoNFeExiste> = True then'
      '  begin              '
      '    Picture1.LoadFromFile(<LogoNFePath>);'
      '    Picture2.LoadFromFile(<LogoNFePath>);'
      '    Picture3.LoadFromFile(<LogoNFePath>);'
      '  end;'
      '  //        '
      '  Altura := <NFeItsLin>;'
      
        '  Line1.Top := Altura;                                          ' +
        '    '
      
        '  Me_prod_cProd.Height := Altura;                               ' +
        '                         '
      
        '  Me_prod_xProd.Height := Altura;                               ' +
        '                         '
      
        '  Me_prod_NCM.Height := Altura;                                 ' +
        '                       '
      
        '  Me_ICMS_CST.Height := Altura;                                 ' +
        '                       '
      
        '  Me_prod_CFOP.Height := Altura;                                ' +
        '                        '
      
        '  Me_prod_uCom.Height := Altura;                                ' +
        '                        '
      
        '  Me_prod_qCom.Height := Altura;                                ' +
        '                        '
      
        '  Me_prod_vUnCom.Height := Altura;                              ' +
        '                          '
      
        '  Me_prod_vProd.Height := Altura;                               ' +
        '                         '
      
        '  Me_ICMS_vBC.Height := Altura;                                 ' +
        '                       '
      
        '  Me_ICMS_vICMS.Height := Altura;                               ' +
        '                         '
      
        '  Me_IPI_vIPI.Height := Altura;                                 ' +
        '                       '
      
        '  Me_ICMS_vICMS.Height := Altura;                               ' +
        '                         '
      '  Me_ICMS_pICMS.Height := Altura;'
      '  Me_IPI_pIPI.Height := Altura;'
      '  DD_prod.Height := Altura;'
      '  //'
      '  MeRazao_1.Font.Size := <NFeFTRazao>;'
      '  MeRazao_2.Font.Size := <NFeFTRazao>;'
      '  MeRazao_3.Font.Size := <NFeFTRazao>;'
      '  MeEnder_1.Font.Size := <NFeFTEnder>;'
      '  MeEnder_2.Font.Size := <NFeFTEnder>;'
      '  MeEnder_3.Font.Size := <NFeFTEnder>;'
      '  MeFones_1.Font.Size := <NFeFTFones>;'
      '  MeFones_2.Font.Size := <NFeFTFones>;'
      '  MeFones_3.Font.Size := <NFeFTFones>;'
      '  //        '
      'end.')
    Left = 264
    Top = 284
    Datasets = <
      item
        DataSet = frxDsA
        DataSetName = 'frxDsA'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      MirrorMargins = True
      LargeDesignHeight = True
      object Memo86: TfrxMemoView
        Left = 30.236240000000000000
        Top = 914.646260000000000000
        Width = 733.228346460000000000
        Height = 15.874015750000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'C'#193'LCULO DO ISSQN')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo87: TfrxMemoView
        Tag = 1
        Left = 30.236240000000000000
        Top = 930.520275750000000000
        Width = 183.307086610000000000
        Height = 32.125984250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'INSCRI'#199#195'O MUNICIPAL')
        ParentFont = False
      end
      object Memo88: TfrxMemoView
        Tag = 1
        Left = 30.236240000000000000
        Top = 939.969094650000000000
        Width = 183.307086610000000000
        Height = 22.677180000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsA."emit_IM"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo106: TfrxMemoView
        Tag = 1
        Left = 213.543307090000000000
        Top = 930.520275750000000000
        Width = 183.307086610000000000
        Height = 32.125984250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'VALOR TOTAL DOS SERVI'#199'OS')
        ParentFont = False
      end
      object Memo107: TfrxMemoView
        Tag = 1
        Left = 213.543307090000000000
        Top = 939.969094650000000000
        Width = 183.307086610000000000
        Height = 22.677180000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          
            '[FormatFloat('#39'#,###,###,##0.00;#,###,###,##0.00; '#39',<frxDsA."ISSQ' +
            'Ntot_vServ">)]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo108: TfrxMemoView
        Tag = 1
        Left = 396.850393700000000000
        Top = 930.520275750000000000
        Width = 183.307086610000000000
        Height = 32.125984250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'BASE DE C'#193'LCULO DO ISSQN')
        ParentFont = False
      end
      object Memo109: TfrxMemoView
        Tag = 1
        Left = 396.850393700000000000
        Top = 939.969094650000000000
        Width = 183.307086610000000000
        Height = 22.677180000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          
            '[FormatFloat('#39'#,###,###,##0.00;#,###,###,##0.00; '#39',<frxDsA."ISSQ' +
            'Ntot_vBC">)]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo110: TfrxMemoView
        Tag = 1
        Left = 579.779530000000000000
        Top = 930.520275750000000000
        Width = 183.307086610000000000
        Height = 32.125984250000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'VALOR DO ISSQN')
        ParentFont = False
      end
      object Memo111: TfrxMemoView
        Tag = 1
        Left = 579.779530000000000000
        Top = 939.969094650000000000
        Width = 183.307086610000000000
        Height = 22.677180000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haRight
        Memo.UTF8W = (
          
            '[FormatFloat('#39'#,###,###,##0.00;#,###,###,##0.00; '#39',<frxDsA."ISSQ' +
            'Ntot_vISS">)]')
        ParentFont = False
        VAlign = vaBottom
      end
      object Memo112: TfrxMemoView
        Left = 30.236240000000000000
        Top = 962.646260000000000000
        Width = 86.551181100000000000
        Height = 15.874015750000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'DADOS ADICIONAIS')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo113: TfrxMemoView
        Tag = 1
        Left = 30.236240000000000000
        Top = 978.520275750000000000
        Width = 445.984251970000000000
        Height = 116.031496060000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'INFORMA'#199#213'ES COMPLEMENTARES')
        ParentFont = False
      end
      object MeFlowToSor: TfrxMemoView
        Tag = 1
        Left = 30.236240000000000000
        Top = 987.969094650000000000
        Width = 445.984251970000000000
        Height = 106.582677170000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          '[frxDsA."InfAdic_InfAdFisco"]'
          '[frxDsA."InfAdic_InfCpl"]'
          '[frxDsA."InfCpl_totTrib"]')
        ParentFont = False
      end
      object Memo115: TfrxMemoView
        Tag = 1
        Left = 476.220472440000000000
        Top = 978.520275750000000000
        Width = 287.244094490000000000
        Height = 116.031496060000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        Memo.UTF8W = (
          'RESERVADO AO FISCO')
        ParentFont = False
      end
      object Memo116: TfrxMemoView
        Tag = 1
        Left = 476.220472440000000000
        Top = 991.748624650000000000
        Width = 287.244094490000000000
        Height = 102.803147170000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Frame.Width = 0.100000000000000000
        HAlign = haRight
        ParentFont = False
      end
      object MeFlowToHid: TfrxMemoView
        Left = 30.236240000000000000
        Top = 1099.843230000000000000
        Width = 733.228820000000000000
        Height = 7.559060000000000000
        OnAfterData = 'MeFlowToHidOnAfterData'
        OnAfterPrint = 'MeFlowToHidOnAfterPrint'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -8
        Font.Name = 'Times New Roman'
        Font.Style = []
        Frame.Width = 0.100000000000000000
        ParentFont = False
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 80.472438500000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo13: TfrxMemoView
          Tag = 2
          Left = 30.236227800000000000
          Top = 16.118107800000000000
          Width = 733.228331810000000000
          Height = 319.370078740000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 15000804
          Font.Height = -128
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '   PREVIEW')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeCanhotoRec: TfrxMemoView
          Left = 30.236240000000000000
          Top = 16.220470000000000000
          Width = 563.149577010000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              'RECEBEMOS DE [frxDsA."emit_xNome"] OS PRODUTOS E/OU SERVI'#199'OS CON' +
              'STANTES DA NOTA FISCAL ELETR'#212'NICA INDICADA AO LADO')
          ParentFont = False
        end
        object MeCanhotoNFe: TfrxMemoView
          Left = 593.385817010000000000
          Top = 16.220470000000000000
          Width = 170.078740160000000000
          Height = 64.251968500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object MeCanhotoDat: TfrxMemoView
          Left = 30.236240000000000000
          Top = 48.346454249999990000
          Width = 154.960629920000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DATA DE RECEBIMENTO')
          ParentFont = False
        end
        object MeCanhotoIde: TfrxMemoView
          Left = 185.196869920000000000
          Top = 48.346454249999990000
          Width = 408.188944650000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'IDENTIFICA'#199#195'O E ASSINATURA DO RECEBEDOR')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 593.385817010000000000
          Top = 16.118120000000000000
          Width = 170.078850000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'NF-e')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 593.275981100000000000
          Top = 31.992135750000000000
          Width = 22.677180000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186)
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 615.953161100000000000
          Top = 31.992135750000000000
          Width = 147.401670000000000000
          Height = 15.874015750000000000
          DisplayFormat.FormatStr = '%2.2m'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = 14211288
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nNF">)]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 593.496451100000000000
          Top = 48.244104250000010000
          Width = 37.795300000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#233'rie:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 631.291751100000000000
          Top = 48.244104250000010000
          Width = 128.504020000000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39',<frxDsA."ide_serie">)]')
          ParentFont = False
        end
        object Memo146: TfrxMemoView
          Left = 32.126011100000000000
          Top = 24.811033380000000000
          Width = 559.370244720000000000
          Height = 21.543307090000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_JUR"]'
            '[frxDsA."DOC_SEM_VLR_FIS"]')
          ParentFont = False
        end
      end
      object Line6: TfrxLineView
        Left = 30.236220470000000000
        Top = 695.433070866142000000
        Height = 219.212598425197000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line7: TfrxLineView
        Left = 68.031496060000000000
        Top = 695.433070866142000000
        Height = 219.212598425197000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line8: TfrxLineView
        Left = 279.685010080000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line14: TfrxLineView
        Left = 317.480285670000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line15: TfrxLineView
        Left = 336.377923460000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line16: TfrxLineView
        Left = 359.055088820000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line17: TfrxLineView
        Left = 396.850364410000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line18: TfrxLineView
        Left = 445.984540000000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line19: TfrxLineView
        Left = 491.338553390000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line20: TfrxLineView
        Left = 536.692884090000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line21: TfrxLineView
        Left = 582.047214800000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line22: TfrxLineView
        Left = 627.401545510000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line24: TfrxLineView
        Left = 672.755876220000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line25: TfrxLineView
        Left = 695.433041570000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line26: TfrxLineView
        Left = 718.110206930000000000
        Top = 695.433070870000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object PageHeader1: TfrxPageHeader
        Tag = 1
        FillType = ftBrush
        Height = 232.818897640000000000
        Top = 120.944960000000000000
        Width = 793.701300000000000000
        object Memo6: TfrxMemoView
          Left = 30.236240000000000000
          Top = 20.511749999999990000
          Width = 332.598425200000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Tag = 1
          Left = 35.905511810000000000
          Top = 30.236220469999990000
          Width = 128.503937010000000000
          Height = 128.503937010000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo11: TfrxMemoView
          Left = 457.323130000000000000
          Top = 168.566929130000000000
          Width = 306.141654170000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PROTOCOLO DE AUTORIZA'#199#195'O DE USO')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.511749999999990000
          Width = 94.488188980000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 457.322834650000000000
          Top = 20.511749999999990000
          Width = 306.141732280000000000
          Height = 55.937007870000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object BarCode1: TfrxBarCodeView
          Left = 466.771641340000000000
          Top = 30.338589999999990000
          Width = 277.000000000000000000
          Height = 37.795275590000000000
          BarType = bcCode128C
          DataField = 'Id'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Rotation = 0
          ShowText = False
          Text = '00000000000000000000000000000000000000000000'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Memo9: TfrxMemoView
          Left = 457.322834650000000000
          Top = 76.448757870000000000
          Width = 306.141732280000000000
          Height = 35.905514250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'CHAVE DE ACESSO P/ CONSULTA DE AUTENTICIDADE NO PORTAL NACIONAL ' +
              'DA NF-E WWW.NFE.FAZENDA.GOV.BR/PORTAL')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 458.834635900000000000
          Top = 93.488250000000010000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ID_TXT"]')
          ParentFont = False
        end
        object MeRazao_1: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 30.236239999999990000
          Width = 192.756030000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_xNome"]')
          ParentFont = False
        end
        object MeEnder_1: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 68.031539999999990000
          Width = 192.756030000000000000
          Height = 77.480314960000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."EMIT_ENDERECO"]')
          ParentFont = False
        end
        object MeFones_1: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 145.511811020000000000
          Width = 192.756030000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TELEFONE: [frxDsA."EMIT_FONE_TXT"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 168.566929130000000000
          Width = 427.086614170000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NATUREZA DA OPERA'#199#195'O')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 176.259783940000000000
          Width = 426.330708660000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."ide_natOp"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo20: TfrxMemoView
          Left = 457.322834650000000000
          Top = 112.251968500000000000
          Width = 306.141732280000000000
          Height = 56.314960630000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 457.322834650000000000
          Top = 176.125940310000000000
          Width = 306.141732280000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_FIS"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 200.692913390000000000
          Width = 229.039350550000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 200.692913390000000000
          Width = 243.401574800000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL DO SUBSTITUTO TRIBUT'#193'RIO')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Tag = 1
          Left = 502.677165350000000000
          Top = 200.692913390000000000
          Width = 260.787401570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 209.519626460000000000
          Width = 229.039350550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IE_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 209.519626460000000000
          Width = 242.645669290000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IEST_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          Tag = 1
          Left = 503.433061100000000000
          Top = 209.519626460000000000
          Width = 259.275590550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Left = 30.236240000000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo147: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.409399999999990000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DANFE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo192: TfrxMemoView
          Left = 362.834635900000000000
          Top = 40.574830000000010000
          Width = 96.000000000000000000
          Height = 35.905511810000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DOCUMENTO AUXILIAR'
            'DA NOTA FISCAL'
            'ELETR'#212'NICA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo193: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 76.724360629999990000
          Width = 96.000000000000000000
          Height = 31.748031500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            ' 0 - ENTRADA'
            ' 1 - SA'#205'DA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo194: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 108.472392130000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186' [FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nNF">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo195: TfrxMemoView
          Tag = 1
          Left = 432.756191100000000000
          Top = 80.503888190000010000
          Width = 16.629870000000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_tpNF"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo196: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 128.504020000000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#201'RIE:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo197: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 148.157431500000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FOLHA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo198: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 128.504020000000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39',<frxDsA."ide_serie">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo199: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 148.157431500000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Page#]/[TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Left = 697.323291100000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo200: TfrxMemoView
          Left = 460.102364650000000000
          Top = 116.409448820000000000
          Width = 302.362204720000000000
          Height = 51.023634250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_JUR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object BarCode2: TfrxBarCodeView
          Left = 466.771961100000000000
          Top = 120.944960000000000000
          Width = 79.000000000000000000
          Height = 37.795275590000000000
          Visible = False
          BarType = bcCode128C
          Rotation = 0
          ShowText = False
          Text = '12345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
      end
      object Memo12: TfrxMemoView
        Tag = 2
        Left = -30.236240000000000000
        Top = 355.275820000000000000
        Width = 793.701300000000000000
        Height = 559.370440000000000000
        Visible = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 15000804
        Font.Height = -117
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        HAlign = haCenter
        Memo.UTF8W = (
          'SEM VALOR')
        ParentFont = False
        Rotation = 45
        VAlign = vaCenter
      end
      object Line11: TfrxLineView
        Left = 763.465060000000000000
        Top = 695.433520000000000000
        Height = 219.212598430000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 242.000000000000000000
      PaperSize = 256
      LargeDesignHeight = True
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        Tag = 1
        FillType = ftBrush
        Height = 132.284294490000000000
        Top = 355.275820000000000000
        Width = 793.701300000000000000
        OnAfterPrint = 'MasterData1OnAfterPrint'
        RowCount = 1
        object Me_E04_Titu: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 12.094485749999990000
          Width = 419.527559060000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NOME / RAZ'#195'O SOCIAL')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 124.724409450000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESTINAT'#193'RIO / REMETENTE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Me_E02_Titu: TfrxMemoView
          Tag = 1
          Left = 449.763779530000000000
          Top = 12.094485749999990000
          Width = 204.094488190000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ / CPF')
          ParentFont = False
        end
        object Me_B09_Titu: TfrxMemoView
          Tag = 1
          Left = 653.858267720000000000
          Top = 12.094485749999990000
          Width = 109.606299210000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DATA DA EMISS'#195'O')
          ParentFont = False
        end
        object Me_E04_Dado: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 20.165342130000000000
          Width = 419.527559060000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."dest_xNome"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaBottom
        end
        object Me_E02_Dado: TfrxMemoView
          Tag = 1
          Left = 449.763779530000000000
          Top = 20.165342130000000000
          Width = 204.094488190000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_CNPJ_CPF_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Me_B09_Dado: TfrxMemoView
          Tag = 1
          Left = 651.968931100000000000
          Top = 20.165342130000000000
          Width = 110.362204720000000000
          Height = 24.188976380000000000
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_dEmi"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo27: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 44.220469999999970000
          Width = 351.496062990000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ENDERE'#199'O')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 52.291326379999990000
          Width = 351.496062990000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DEST_ENDERECO"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo29: TfrxMemoView
          Tag = 1
          Left = 381.732283460000000000
          Top = 44.220469999999970000
          Width = 185.196850390000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'BAIRRO / DISTRITO')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Tag = 1
          Left = 381.732283460000000000
          Top = 52.291326379999990000
          Width = 185.196850390000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."dest_xBairro"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo31: TfrxMemoView
          Tag = 1
          Left = 566.929133860000000000
          Top = 44.220469999999970000
          Width = 86.929133860000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CEP')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Tag = 1
          Left = 566.929133860000000000
          Top = 52.291326379999990000
          Width = 86.929133860000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."dest_CEP_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo33: TfrxMemoView
          Tag = 1
          Left = 653.858267720000000000
          Top = 44.220469999999970000
          Width = 109.606299210000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DATA DA ENTRADA / SA'#205'DA')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Tag = 1
          Left = 651.968494170000000000
          Top = 52.291326379999990000
          Width = 110.362204720000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_dSaiEnt_Txt"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo15: TfrxMemoView
          Tag = 1
          Left = 653.858267720000000000
          Top = 76.346454250000030000
          Width = 109.606299210000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'HORA DA ENTRADA / SA'#205'DA')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Tag = 1
          Left = 652.346446930000000000
          Top = 84.417310629999970000
          Width = 110.362204720000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordBreak = True
        end
        object Memo35: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 76.346454250000030000
          Width = 268.724409450000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MUNIC'#205'PIO')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 84.417310629999970000
          Width = 268.724409450000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_XMUN_TXT"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo37: TfrxMemoView
          Tag = 1
          Left = 298.960649450000000000
          Top = 76.346454250000030000
          Width = 123.212578900000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FONE / FAX')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Tag = 1
          Left = 298.960649450000000000
          Top = 84.417310629999970000
          Width = 123.212578900000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_FONE_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo39: TfrxMemoView
          Tag = 1
          Left = 422.173228340000000000
          Top = 76.346454250000030000
          Width = 27.590551180000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Tag = 1
          Left = 422.173228340000000000
          Top = 84.417310629999970000
          Width = 27.590551180000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."dest_UF"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo41: TfrxMemoView
          Tag = 1
          Left = 449.763779530000000000
          Top = 76.346454250000030000
          Width = 204.094488190000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Tag = 1
          Left = 449.763779530000000000
          Top = 84.417310629999970000
          Width = 204.094488190000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DEST_IE_TXT"]')
          ParentFont = False
          WordBreak = True
          VAlign = vaBottom
        end
        object Memo43: TfrxMemoView
          Tag = 1
          Left = 30.236220470000000000
          Top = 110.118381180000000000
          Width = 124.724409450000000000
          Height = 12.094485750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FATURA / DUPLICATA')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo148: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 122.835194880000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo151: TfrxMemoView
          Tag = 1
          Left = 79.370149530000000000
          Top = 122.835194880000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCIMENTO')
          ParentFont = False
        end
        object Memo154: TfrxMemoView
          Tag = 1
          Left = 128.504039530000000000
          Top = 122.835194880000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Tag = 1
          Left = 177.637929530000000000
          Top = 122.835475590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo157: TfrxMemoView
          Tag = 1
          Left = 226.771839060000000000
          Top = 122.835475590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCIMENTO')
          ParentFont = False
        end
        object Memo160: TfrxMemoView
          Tag = 1
          Left = 275.905729060000000000
          Top = 122.835475590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo163: TfrxMemoView
          Tag = 1
          Left = 325.039599530000000000
          Top = 122.835475590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo166: TfrxMemoView
          Tag = 1
          Left = 374.173509060000000000
          Top = 122.835475590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCIMENTO')
          ParentFont = False
        end
        object Memo169: TfrxMemoView
          Tag = 1
          Left = 423.307399060000000000
          Top = 122.835475590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo172: TfrxMemoView
          Tag = 1
          Left = 472.441269530000000000
          Top = 122.835475590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo175: TfrxMemoView
          Tag = 1
          Left = 521.575179060000000000
          Top = 122.835475590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCIMENTO')
          ParentFont = False
        end
        object Memo178: TfrxMemoView
          Tag = 1
          Left = 570.709069060000000000
          Top = 122.835475590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
        object Memo181: TfrxMemoView
          Tag = 1
          Left = 619.842939530000000000
          Top = 122.835475590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo184: TfrxMemoView
          Tag = 1
          Left = 668.976849060000000000
          Top = 122.835475590000000000
          Width = 49.133858270000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCIMENTO')
          ParentFont = False
        end
        object Memo187: TfrxMemoView
          Tag = 1
          Left = 718.110739060000000000
          Top = 122.835475590000000000
          Width = 45.354330710000000000
          Height = 9.448818900000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        Tag = 1
        FillType = ftBrush
        Height = 208.251968510000000000
        Top = 544.252320000000000000
        Width = 793.701300000000000000
        OnAfterPrint = 'MasterData2OnAfterPrint'
        RowCount = 1
        object Memo46: TfrxMemoView
          Tag = 1
          Left = 634.960649450000000000
          Top = 15.874015750000010000
          Width = 128.503937007874000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL DOS PRODUTOS')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Tag = 1
          Left = 634.960649450000000000
          Top = 25.322834640000000000
          Width = 128.503937007874000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vProd"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo48: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 15.874015750000010000
          Width = 120.944881889764000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'BASE DE C'#193'LCULO DO ICMS')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 25.322834640000000000
          Width = 120.944881889764000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vBC"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo50: TfrxMemoView
          Tag = 1
          Left = 151.181087720000000000
          Top = 15.874015750000010000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO ICMS')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Tag = 1
          Left = 151.181087720000000000
          Top = 25.322834640000000000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vICMS"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo52: TfrxMemoView
          Tag = 1
          Left = 272.125954960000000000
          Top = 15.874015750000010000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'BASE DE C'#193'LCULO DO ICMS ST')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Tag = 1
          Left = 272.125954960000000000
          Top = 25.322834640000000000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vBCST"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo54: TfrxMemoView
          Tag = 1
          Left = 393.070822200000000000
          Top = 15.874015750000010000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO ICMS SUBSTITUI'#199#195'O')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Tag = 1
          Left = 393.070822200000000000
          Top = 25.322834640000000000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vST"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo56: TfrxMemoView
          Tag = 1
          Left = 514.125986690000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL DO IPI')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Tag = 1
          Left = 514.125986690000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vIPI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo58: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 48.000000000000000000
          Width = 120.944881889764000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO FRETE')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vFrete"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo60: TfrxMemoView
          Tag = 1
          Left = 151.291348350000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO SEGURO')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Tag = 1
          Left = 151.291348350000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vSeg"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo62: TfrxMemoView
          Tag = 1
          Left = 272.236227800000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESCONTO')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Tag = 1
          Left = 272.236227800000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vDesc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo64: TfrxMemoView
          Tag = 1
          Left = 393.181107240000000000
          Top = 48.000000000000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'OUTRAS DESPESAS ACESS'#211'RIAS')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Tag = 1
          Left = 393.181107240000000000
          Top = 57.448818899999990000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vOutro"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo66: TfrxMemoView
          Tag = 1
          Left = 635.070866140000000000
          Top = 48.000000000000000000
          Width = 128.503937010000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL DA NOTA')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Tag = 1
          Left = 635.070866140000000000
          Top = 57.448818899999990000
          Width = 128.503941890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."ICMSTot_vNF"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo68: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 80.125984249999990000
          Width = 211.653543310000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TRANSPORTADOR / VOLUMES TRANSPORTADOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Tag = 1
          Left = 631.181085280000000000
          Top = 96.000000000000000000
          Width = 132.283464570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ / CPF')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Tag = 1
          Left = 631.181085280000000000
          Top = 105.448818900000000000
          Width = 132.283464570000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."TRANSPORTA_CNPJ_CPF_TXT"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo71: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 96.000000000000000000
          Width = 306.141732283465000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NOME / RAZ'#195'O SOCIAL')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 105.448818900000000000
          Width = 306.141732280000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."Transporta_XNome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo73: TfrxMemoView
          Tag = 1
          Left = 336.377950310000000000
          Top = 96.000000000000000000
          Width = 94.488188980000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FRETE POR CONTA')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Tag = 1
          Left = 336.377950310000000000
          Top = 105.448818900000000000
          Width = 90.708658980000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."MODFRETE_TXT"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo75: TfrxMemoView
          Tag = 1
          Left = 430.866141732283000000
          Top = 96.000000000000000000
          Width = 94.488188980000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#211'DIGO ANTT')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Tag = 1
          Left = 430.866141730000000000
          Top = 105.448818900000000000
          Width = 94.488188980000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."VeicTransp_RNTC"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo77: TfrxMemoView
          Tag = 1
          Left = 600.944864800000000000
          Top = 96.000000000000000000
          Width = 30.236220472440900000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Tag = 1
          Left = 600.944864800000000000
          Top = 105.448818900000000000
          Width = 30.236220470000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."VeicTransp_UF"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo79: TfrxMemoView
          Tag = 1
          Left = 600.944864800000000000
          Top = 128.125984250000000000
          Width = 30.236220472440900000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          Tag = 1
          Left = 600.944864800000000000
          Top = 137.574803150000000000
          Width = 30.236220470000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."Transporta_UF"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo81: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 128.125984250000000000
          Width = 340.157477870000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ENDERE'#199'O')
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 137.574803150000000000
          Width = 340.157477870000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."Transporta_XEnder"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo83: TfrxMemoView
          Tag = 1
          Left = 370.393698350000000000
          Top = 128.125984250000000000
          Width = 230.551164020000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MUNIC'#205'PIO')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Tag = 1
          Left = 370.393698350000000000
          Top = 137.574803150000000000
          Width = 230.551164020000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."Transporta_XMun"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo89: TfrxMemoView
          Tag = 1
          Left = 631.181085280000000000
          Top = 128.125984250000000000
          Width = 132.283464570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Tag = 1
          Left = 631.181085280000000000
          Top = 137.574803150000000000
          Width = 132.283464570000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."TRANSPORTA_IE_TXT"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo91: TfrxMemoView
          Tag = 1
          Left = 525.354313620000000000
          Top = 160.252373700000000000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PESO BRUTO')
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Tag = 1
          Left = 525.354313620000000000
          Top = 169.701192600000000000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0.000;-0.000; '#39', <VARF_XVOL_PESOB>)]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo93: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 160.252373700000000000
          Width = 109.606299212598000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'QUANTIDADE')
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 169.701192600000000000
          Width = 109.606299210000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <VARF_XVOL_QVOL>)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo95: TfrxMemoView
          Tag = 1
          Left = 139.842519690000000000
          Top = 160.252373700000000000
          Width = 113.385826771654000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ESP'#201'CIE')
          ParentFont = False
        end
        object Memo96: TfrxMemoView
          Tag = 1
          Left = 139.842519690000000000
          Top = 169.701192600000000000
          Width = 113.385826770000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_XVOL_ESP]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo97: TfrxMemoView
          Tag = 1
          Left = 253.228346456693000000
          Top = 160.252373700000000000
          Width = 117.165354330000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MARCA')
          ParentFont = False
        end
        object Memo98: TfrxMemoView
          Tag = 1
          Left = 253.228346460000000000
          Top = 169.701192600000000000
          Width = 117.165354330000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_XVOL_MARCA]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo99: TfrxMemoView
          Tag = 1
          Left = 370.393700790000000000
          Top = 160.252373700000000000
          Width = 154.960612830000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Tag = 1
          Left = 370.393700790000000000
          Top = 169.701192600000000000
          Width = 154.960612830000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_XVOL_NVOL]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo101: TfrxMemoView
          Tag = 1
          Left = 646.299212598425000000
          Top = 160.252373700000000000
          Width = 117.165354330709000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PESO L'#205'QUIDO')
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          Tag = 1
          Left = 646.299212600000000000
          Top = 169.701192600000000000
          Width = 117.165354330000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0.000;-0.000; '#39', <VARF_XVOL_PESOL>)]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo103: TfrxMemoView
          Tag = 1
          Left = 525.354330708661000000
          Top = 96.000363699999970000
          Width = 75.590551180000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PLACA DO VE'#205'CULO')
          ParentFont = False
        end
        object Memo104: TfrxMemoView
          Tag = 1
          Left = 525.354330710000000000
          Top = 105.449182600000000000
          Width = 75.590551180000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."VeicTransp_Placa"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo85: TfrxMemoView
          Tag = 1
          Left = 30.346468900000000000
          Top = 192.377952760000000000
          Width = 151.181102360000000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DADOS DOS PRODUTOS / SERVI'#199'OS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 143.622047244094000000
          Height = 15.874015750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#193'LCULO DO IMPOSTO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo117: TfrxMemoView
          Tag = 1
          Left = 514.016080000000000000
          Top = 15.874015750000010000
          Width = 120.944881890000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR APROXIMADO TRIBUTOS')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          Tag = 1
          Left = 514.016080000000000000
          Top = 25.322834650000000000
          Width = 120.944881890000000000
          Height = 22.677180000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsA."vTotTrib"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object DetailData1: TfrxDetailData
        Tag = 1
        FillType = ftBrush
        Height = 11.338582680000000000
        Top = 510.236550000000000000
        Width = 793.701300000000000000
        DataSet = Form1.frxDsCTeYIts
        DataSetName = 'frxDsCTeYIts'
        RowCount = 0
        object Memo149: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataField = 'nDup1'
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTeYIts."nDup1"]')
          ParentFont = False
        end
        object Memo150: TfrxMemoView
          Tag = 1
          Left = 79.370149530000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataField = 'xVenc1'
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTeYIts."xVenc1"]')
          ParentFont = False
        end
        object Memo152: TfrxMemoView
          Tag = 1
          Left = 128.504039530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.00'#39', <frxDsCTeYIts."vDup1">)]')
          ParentFont = False
        end
        object Memo153: TfrxMemoView
          Tag = 1
          Left = 177.637910000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTeYIts."nDup2"]')
          ParentFont = False
        end
        object Memo155: TfrxMemoView
          Tag = 1
          Left = 226.771819530000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTeYIts."xVenc2"]')
          ParentFont = False
        end
        object Memo156: TfrxMemoView
          Tag = 1
          Left = 275.905709530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.00'#39', <frxDsCTeYIts."vDup2">)]')
          ParentFont = False
        end
        object Memo158: TfrxMemoView
          Tag = 1
          Left = 325.039580000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTeYIts."nDup3"]')
          ParentFont = False
        end
        object Memo159: TfrxMemoView
          Tag = 1
          Left = 374.173489530000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTeYIts."xVenc3"]')
          ParentFont = False
        end
        object Memo161: TfrxMemoView
          Tag = 1
          Left = 423.307379530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.00'#39', <frxDsCTeYIts."vDup3">)]')
          ParentFont = False
        end
        object Memo162: TfrxMemoView
          Tag = 1
          Left = 472.441250000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTeYIts."nDup4"]')
          ParentFont = False
        end
        object Memo164: TfrxMemoView
          Tag = 1
          Left = 521.575159530000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTeYIts."xVenc4"]')
          ParentFont = False
        end
        object Memo165: TfrxMemoView
          Tag = 1
          Left = 570.709049530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.00'#39', <frxDsCTeYIts."vDup4">)]')
          ParentFont = False
        end
        object Memo167: TfrxMemoView
          Tag = 1
          Left = 619.842920000000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTeYIts."nDup5"]')
          ParentFont = False
        end
        object Memo168: TfrxMemoView
          Tag = 1
          Left = 668.976829530000000000
          Width = 49.133858270000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTeYIts."xVenc5"]')
          ParentFont = False
        end
        object Memo170: TfrxMemoView
          Tag = 1
          Left = 718.110719530000000000
          Width = 45.354330710000000000
          Height = 11.338582680000000000
          DataSet = Form1.frxDsCTeYIts
          DataSetName = 'frxDsCTeYIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,###,##0.00'#39', <frxDsCTeYIts."vDup5">)]')
          ParentFont = False
        end
      end
      object DD_prod: TfrxDetailData
        Tag = 1
        FillType = ftBrush
        Height = 32.125984250000000000
        Top = 816.378480000000000000
        Width = 793.701300000000000000
        DataSet = Form1.frxDsI
        DataSetName = 'frxDsI'
        RowCount = 0
        object Me_prod_cProd: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 37.795280470000000000
          Height = 32.125984250000000000
          DataField = 'prod_cProd'
          DataSet = Form1.frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_cProd"]')
          ParentFont = False
        end
        object Me_prod_xProd: TfrxMemoView
          Tag = 1
          Left = 68.031520470000000000
          Width = 211.653543310000000000
          Height = 32.125984250000000000
          DataSet = Form1.frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_xProd"] [frxDsV."InfAdProd"]')
          ParentFont = False
        end
        object Me_prod_NCM: TfrxMemoView
          Tag = 1
          Left = 279.685046690000000000
          Width = 37.795275590000000000
          Height = 32.125984250000000000
          DataSet = Form1.frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_NCM"]')
          ParentFont = False
        end
        object Me_ICMS_CST: TfrxMemoView
          Tag = 1
          Left = 317.480327170000000000
          Width = 18.897635350000000000
          Height = 32.125984250000000000
          OnBeforePrint = 'Me_ICMS_CSTOnBeforePrint'
          DataSet = Form1.frxDsN
          DataSetName = 'frxDsN'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsN."ICMS_CST"]')
          ParentFont = False
        end
        object Me_prod_CFOP: TfrxMemoView
          Tag = 1
          Left = 336.377962520000000000
          Width = 22.677158030000000000
          Height = 32.125984250000000000
          DataSet = Form1.frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_CFOP"]')
          ParentFont = False
        end
        object Me_prod_uCom: TfrxMemoView
          Tag = 1
          Left = 359.055074170000000000
          Width = 37.795275590000000000
          Height = 32.125984250000000000
          DataSet = Form1.frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsI."prod_uCom"]')
          ParentFont = False
        end
        object Me_prod_qCom: TfrxMemoView
          Tag = 1
          Left = 396.850340000000000000
          Width = 49.133865590000000000
          Height = 32.125984250000000000
          DataField = 'prod_qCom'
          DataSet = Form1.frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsI."prod_qCom"]')
          ParentFont = False
        end
        object Me_prod_vUnCom: TfrxMemoView
          Tag = 1
          Left = 445.984222680000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          DataSet = Form1.frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.000000'#39', <frxDsI."prod_vUnCom">)]')
          ParentFont = False
        end
        object Me_prod_vProd: TfrxMemoView
          Tag = 1
          Left = 491.338538740000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          DataSet = Form1.frxDsI
          DataSetName = 'frxDsI'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsI."prod_vProd">)]')
          ParentFont = False
        end
        object Me_ICMS_vBC: TfrxMemoView
          Tag = 1
          Left = 536.692871890000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsN."ICMS_vBC">)]')
          ParentFont = False
        end
        object Me_ICMS_vICMS: TfrxMemoView
          Tag = 1
          Left = 582.047205040000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsN."ICMS_vICMS">)]')
          ParentFont = False
        end
        object Me_IPI_vIPI: TfrxMemoView
          Tag = 1
          Left = 627.401533310000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsO."IPI_vIPI">)]')
          ParentFont = False
        end
        object Me_ICMS_pICMS: TfrxMemoView
          Tag = 1
          Left = 672.755856690000000000
          Width = 22.677165350000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0.00;-0.00; '#39', <frxDsN."ICMS_pICMS">)]')
          ParentFont = False
        end
        object Me_IPI_pIPI: TfrxMemoView
          Tag = 1
          Left = 695.433041570000000000
          Width = 22.677165350000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0.00;-0.00; '#39', <frxDsO."IPI_pIPI">)]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Tag = 1
          Left = 30.236240000000000000
          Top = 32.125984249999990000
          Width = 733.228820000000000000
          StretchMode = smMaxHeight
          Color = clBlack
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Tag = 1
          Left = 718.110700000000000000
          Width = 45.354330710000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'#,###,##0.00'#39', <frxDsM."vTotTrib">)]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        Tag = 1
        FillType = ftBrush
        Height = 17.385826770000000000
        Top = 774.803650000000000000
        Width = 793.701300000000000000
        object Memo131: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 37.795280470000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#211'DIGO'
            'PRODUTO')
          ParentFont = False
        end
        object Memo132: TfrxMemoView
          Tag = 1
          Left = 68.031520470000000000
          Width = 211.653543310000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESCRI'#199#195'O DOS PRODUTOS / SERVI'#199'OS')
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          Tag = 1
          Left = 279.685046690000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NCM/SH')
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          Tag = 1
          Left = 317.480327170000000000
          Width = 18.897635350000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CST')
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          Tag = 1
          Left = 336.377962520000000000
          Width = 22.677158030000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CFOP')
          ParentFont = False
        end
        object Memo136: TfrxMemoView
          Tag = 1
          Left = 359.055074170000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UNIDADE')
          ParentFont = False
        end
        object Memo137: TfrxMemoView
          Tag = 1
          Left = 396.850340000000000000
          Width = 49.133865590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'QUANTIDADE')
          ParentFont = False
        end
        object Memo138: TfrxMemoView
          Tag = 1
          Left = 445.984222680000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR UNIT'#193'RIO')
          ParentFont = False
        end
        object Memo139: TfrxMemoView
          Tag = 1
          Left = 491.338538740000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL')
          ParentFont = False
        end
        object Memo140: TfrxMemoView
          Tag = 1
          Left = 536.692871890000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'B.CALC. ICMS')
          ParentFont = False
        end
        object Memo141: TfrxMemoView
          Tag = 1
          Left = 582.047205040000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR ICMS')
          ParentFont = False
        end
        object Memo142: TfrxMemoView
          Tag = 1
          Left = 627.401533310000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR IPI')
          ParentFont = False
        end
        object Memo143: TfrxMemoView
          Tag = 1
          Left = 672.755856690000000000
          Top = 8.692476459999967000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ICMS')
          ParentFont = False
        end
        object Memo144: TfrxMemoView
          Tag = 1
          Left = 695.433041570000000000
          Top = 8.692476459999967000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IPI')
          ParentFont = False
        end
        object Memo145: TfrxMemoView
          Tag = 1
          Left = 672.755856690000000000
          Width = 45.354340470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'AL'#205'QUOTAS')
          ParentFont = False
        end
        object Memo105: TfrxMemoView
          Tag = 1
          Left = 718.110700000000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VAL APROX'
            'TRIBUTOS')
          ParentFont = False
        end
      end
      object Child1: TfrxChild
        Tag = 1
        FillType = ftBrush
        Height = 17.385826770000000000
        Top = 313.700990000000000000
        Visible = False
        Width = 793.701300000000000000
        OnAfterPrint = 'Child1OnAfterPrint'
        object Memo171: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Width = 37.795280470000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#211'DIGO'
            'PRODUTO')
          ParentFont = False
        end
        object Memo173: TfrxMemoView
          Tag = 1
          Left = 68.031520470000000000
          Width = 257.007903310000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESCRI'#199#195'O DOS PRODUTOS / SERVI'#199'OS')
          ParentFont = False
        end
        object Memo174: TfrxMemoView
          Tag = 1
          Left = 325.039406690000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NCM/SH')
          ParentFont = False
        end
        object Memo176: TfrxMemoView
          Tag = 1
          Left = 362.834687170000000000
          Width = 18.897635350000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CST')
          ParentFont = False
        end
        object Memo177: TfrxMemoView
          Tag = 1
          Left = 381.732322520000000000
          Width = 22.677158030000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CFOP')
          ParentFont = False
        end
        object Memo179: TfrxMemoView
          Tag = 1
          Left = 404.409434170000000000
          Width = 37.795275590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UNIDADE')
          ParentFont = False
        end
        object Memo180: TfrxMemoView
          Tag = 1
          Left = 442.204700000000000000
          Width = 49.133865590000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'QUANTIDADE')
          ParentFont = False
        end
        object Memo182: TfrxMemoView
          Tag = 1
          Left = 491.338582680000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR UNIT'#193'RIO')
          ParentFont = False
        end
        object Memo183: TfrxMemoView
          Tag = 1
          Left = 536.692898740000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL')
          ParentFont = False
        end
        object Memo185: TfrxMemoView
          Tag = 1
          Left = 582.047231890000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'B.CALC. ICMS')
          ParentFont = False
        end
        object Memo186: TfrxMemoView
          Tag = 1
          Left = 627.401565040000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR ICMS')
          ParentFont = False
        end
        object Memo188: TfrxMemoView
          Tag = 1
          Left = 672.755893310000000000
          Width = 45.354330710000000000
          Height = 17.385826770000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR IPI')
          ParentFont = False
        end
        object Memo189: TfrxMemoView
          Tag = 1
          Left = 718.110216690000000000
          Top = 8.692476460000023000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ICMS')
          ParentFont = False
        end
        object Memo190: TfrxMemoView
          Tag = 1
          Left = 740.787401570000000000
          Top = 8.692476460000023000
          Width = 22.677160470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'IPI')
          ParentFont = False
        end
        object Memo191: TfrxMemoView
          Tag = 1
          Left = 718.110216690000000000
          Width = 45.354340470000000000
          Height = 8.692913390000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'AL'#205'QUOTAS')
          ParentFont = False
        end
      end
      object PageHeader2: TfrxPageHeader
        Tag = 1
        FillType = ftBrush
        Height = 232.818897640000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo201: TfrxMemoView
          Left = 30.236240000000000000
          Top = 20.511750000000000000
          Width = 332.598425200000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo202: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.511750000000000000
          Width = 94.488188980000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo203: TfrxMemoView
          Left = 457.322834650000000000
          Top = 20.511750000000000000
          Width = 306.141732280000000000
          Height = 55.937007870000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object BarCode3: TfrxBarCodeView
          Left = 466.771641340000000000
          Top = 30.338590000000010000
          Width = 277.000000000000000000
          Height = 37.795275590000000000
          BarType = bcCode128C
          DataField = 'Id'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Rotation = 0
          ShowText = False
          Text = '00000000000000000000000000000000000000000000'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Memo204: TfrxMemoView
          Left = 457.322834650000000000
          Top = 76.448757870000000000
          Width = 306.141732280000000000
          Height = 35.905514250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'CHAVE DE ACESSO P/ CONSULTA DE AUTENTICIDADE NO PORTAL NACIONAL ' +
              'DA NF-E WWW.NFE.FAZENDA.GOV.BR/PORTAL')
          ParentFont = False
        end
        object Memo205: TfrxMemoView
          Left = 458.834635900000000000
          Top = 93.488250000000000000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ID_TXT"]')
          ParentFont = False
        end
        object MeRazao_2: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 30.236240000000000000
          Width = 192.756030000000000000
          Height = 41.574830000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_xNome"]')
          ParentFont = False
        end
        object MeEnder_2: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 71.811070000000000000
          Width = 192.756030000000000000
          Height = 71.811070000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."EMIT_ENDERECO"]')
          ParentFont = False
        end
        object MeFones_2: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 143.622140000000000000
          Width = 192.756030000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TELEFONE: [frxDsA."EMIT_FONE_TXT"]')
          ParentFont = False
        end
        object Memo210: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 168.566929130000000000
          Width = 427.086614170000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NATUREZA DA OPERA'#199#195'O')
          ParentFont = False
        end
        object Memo211: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 176.259783940000000000
          Width = 426.330708660000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."ide_natOp"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo212: TfrxMemoView
          Left = 457.322834650000000000
          Top = 112.251968503937000000
          Width = 306.141732280000000000
          Height = 56.314960629921300000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo213: TfrxMemoView
          Left = 457.322834650000000000
          Top = 168.566880310000000000
          Width = 306.141732280000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_FIS"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo214: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 200.692913390000000000
          Width = 229.039350550000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo215: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 200.692913390000000000
          Width = 243.401574800000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL DO SUBSTITUTO TRIBUT'#193'RIO')
          ParentFont = False
        end
        object Memo216: TfrxMemoView
          Tag = 1
          Left = 502.677165350000000000
          Top = 200.692913390000000000
          Width = 260.787401570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ')
          ParentFont = False
        end
        object Memo217: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 209.519626460000000000
          Width = 229.039350550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IE_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo218: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 209.519626460000000000
          Width = 242.645669290000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IEST_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo219: TfrxMemoView
          Tag = 1
          Left = 503.433061100000000000
          Top = 209.519626460000000000
          Width = 259.275590550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          Left = 30.236240000000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo250: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.409400000000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DANFE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo251: TfrxMemoView
          Left = 362.834635900000000000
          Top = 40.574830000000000000
          Width = 96.000000000000000000
          Height = 35.905511810000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DOCUMENTO AUXILIAR'
            'DA NOTA FISCAL'
            'ELETR'#212'NICA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo252: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 76.724360630000010000
          Width = 96.000000000000000000
          Height = 31.748031500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            ' 0 - ENTRADA'
            ' 1 - SA'#205'DA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo253: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 108.472392130000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186' [FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nNF">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo254: TfrxMemoView
          Tag = 1
          Left = 432.756191100000000000
          Top = 80.503888190000000000
          Width = 16.629870000000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_tpNF"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo255: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 128.504020000000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#201'RIE:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo256: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 148.157431500000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FOLHA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo257: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 128.504020000000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39',<frxDsA."ide_serie">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo258: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 148.157431500000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Page#]/[TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line10: TfrxLineView
          Left = 697.323291100000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo259: TfrxMemoView
          Left = 460.102364650000000000
          Top = 116.409448820000000000
          Width = 302.362204720000000000
          Height = 51.023634250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_JUR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object BarCode4: TfrxBarCodeView
          Left = 466.771961100000000000
          Top = 120.944960000000000000
          Width = 79.000000000000000000
          Height = 37.795275590000000000
          Visible = False
          BarType = bcCode128C
          Rotation = 0
          ShowText = False
          Text = '12345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Picture2: TfrxPictureView
          Tag = 1
          Left = 35.905511810000000000
          Top = 30.236240000000000000
          Width = 128.503937010000000000
          Height = 128.503937010000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
    end
    object Page3: TfrxReportPage
      Visible = False
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LargeDesignHeight = True
      object MasterData3: TfrxMasterData
        Tag = 1
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 313.700990000000000000
        Width = 793.701300000000000000
        AllowSplit = True
        DataSet = frxDsA
        DataSetName = 'frxDsA'
        RowCount = 0
        object MeFlowToSee: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 18.897650000000000000
          Width = 733.228820000000000000
          Height = 7.559060000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo249: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 7.559059999999988000
          Width = 733.228531970000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INFORMA'#199#213'ES COMPLEMENTARES (COMPLEMENTO DA PRIMEIRA FOLHA)')
          ParentFont = False
        end
      end
      object PageHeader3: TfrxPageHeader
        FillType = ftBrush
        Height = 232.818897640000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo220: TfrxMemoView
          Left = 30.236240000000000000
          Top = 20.511750000000000000
          Width = 332.598425200000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo221: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.511750000000000000
          Width = 94.488188980000000000
          Height = 148.157480310000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo222: TfrxMemoView
          Left = 457.322834650000000000
          Top = 20.511750000000000000
          Width = 306.141732280000000000
          Height = 55.937007870000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object BarCode5: TfrxBarCodeView
          Left = 466.771641340000000000
          Top = 30.338590000000010000
          Width = 277.000000000000000000
          Height = 37.795275590000000000
          BarType = bcCode128C
          DataField = 'Id'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Rotation = 0
          ShowText = False
          Text = '00000000000000000000000000000000000000000000'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Memo223: TfrxMemoView
          Left = 457.322834650000000000
          Top = 76.448757870000000000
          Width = 306.141732280000000000
          Height = 35.905514250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            
              'CHAVE DE ACESSO P/ CONSULTA DE AUTENTICIDADE NO PORTAL NACIONAL ' +
              'DA NF-E WWW.NFE.FAZENDA.GOV.BR/PORTAL')
          ParentFont = False
        end
        object Memo224: TfrxMemoView
          Left = 458.834635900000000000
          Top = 93.488250000000000000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ID_TXT"]')
          ParentFont = False
        end
        object MeRazao_3: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 30.236240000000000000
          Width = 192.756030000000000000
          Height = 41.574830000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_xNome"]')
          ParentFont = False
        end
        object MeEnder_3: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 71.811070000000000000
          Width = 192.756030000000000000
          Height = 71.811070000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."EMIT_ENDERECO"]')
          ParentFont = False
        end
        object MeFones_3: TfrxMemoView
          Tag = 1
          Left = 168.189091100000000000
          Top = 143.622140000000000000
          Width = 192.756030000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TELEFONE: [frxDsA."EMIT_FONE_TXT"]')
          ParentFont = False
        end
        object Memo229: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 168.566929130000000000
          Width = 427.086614170000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NATUREZA DA OPERA'#199#195'O')
          ParentFont = False
        end
        object Memo230: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 176.259783940000000000
          Width = 426.330708660000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."ide_natOp"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo231: TfrxMemoView
          Left = 457.322834650000000000
          Top = 112.251968503937000000
          Width = 306.141732280000000000
          Height = 56.314960629921300000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo232: TfrxMemoView
          Left = 457.322834650000000000
          Top = 168.566880310000000000
          Width = 306.141732280000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_FIS"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo233: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 200.692913390000000000
          Width = 229.039350550000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL')
          ParentFont = False
        end
        object Memo234: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 200.692913390000000000
          Width = 243.401574800000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL DO SUBSTITUTO TRIBUT'#193'RIO')
          ParentFont = False
        end
        object Memo235: TfrxMemoView
          Tag = 1
          Left = 502.677165350000000000
          Top = 200.692913390000000000
          Width = 260.787401570000000000
          Height = 32.125984250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ')
          ParentFont = False
        end
        object Memo236: TfrxMemoView
          Tag = 1
          Left = 30.236240000000000000
          Top = 209.519626460000000000
          Width = 229.039350550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IE_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo237: TfrxMemoView
          Tag = 1
          Left = 259.275590550000000000
          Top = 209.519626460000000000
          Width = 242.645669290000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_IEST_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo238: TfrxMemoView
          Tag = 1
          Left = 503.433061100000000000
          Top = 209.519626460000000000
          Width = 259.275590550000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line4: TfrxLineView
          Left = 30.236240000000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo239: TfrxMemoView
          Left = 362.834635900000000000
          Top = 20.409400000000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DANFE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo240: TfrxMemoView
          Left = 362.834635900000000000
          Top = 40.574830000000000000
          Width = 96.000000000000000000
          Height = 35.905511810000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'DOCUMENTO AUXILIAR'
            'DA NOTA FISCAL'
            'ELETR'#212'NICA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo241: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 76.724360630000010000
          Width = 96.000000000000000000
          Height = 31.748031500000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            ' 0 - ENTRADA'
            ' 1 - SA'#205'DA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo242: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 108.472392130000000000
          Width = 96.000000000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186' [FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nNF">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo243: TfrxMemoView
          Tag = 1
          Left = 432.756191100000000000
          Top = 80.503888190000000000
          Width = 16.629870000000000000
          Height = 24.188976380000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_tpNF"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo244: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 128.504020000000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'S'#201'RIE:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo245: TfrxMemoView
          Tag = 1
          Left = 362.834635900000000000
          Top = 148.157431500000000000
          Width = 39.307050000000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FOLHA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo246: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 128.504020000000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39',<frxDsA."ide_serie">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo247: TfrxMemoView
          Tag = 1
          Left = 402.519951100000000000
          Top = 148.157431500000000000
          Width = 54.803156930000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[Page#]/[TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line5: TfrxLineView
          Left = 697.323291100000000000
          Top = 11.338590000000000000
          Width = 68.031515590000000000
          Color = clBlack
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo248: TfrxMemoView
          Left = 460.102364650000000000
          Top = 116.409448820000000000
          Width = 302.362204720000000000
          Height = 51.023634250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_JUR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object BarCode6: TfrxBarCodeView
          Left = 466.771961100000000000
          Top = 120.944960000000000000
          Width = 79.000000000000000000
          Height = 37.795275590000000000
          Visible = False
          BarType = bcCode128C
          Rotation = 0
          ShowText = False
          Text = '12345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Picture3: TfrxPictureView
          Tag = 1
          Left = 35.905511810000000000
          Top = 30.236240000000000000
          Width = 128.503937010000000000
          Height = 128.503937010000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
    end
  end
  object frxPDFExport: TfrxPDFExport
    ShowDialog = False
    UseFileCache = True
    ShowProgress = False
    OverwritePrompt = False
    DataOnly = False
    EmbeddedFonts = True
    PrintOptimized = True
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'Dermatek'
    Subject = 'FastReport PDF export'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = False
    Left = 188
    Top = 332
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 160
    Top = 11
  end
  object frxListaMDFesB: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40025.074573773150000000
    ReportOptions.LastChange = 40025.074573773150000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxListaMFFesGetValue
    Left = 100
    Top = 328
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsMDFe_100
        DataSetName = 'frxDsMDFe_100'
      end
      item
        DataSet = frxDsMDFe_101
        DataSetName = 'frxDsMDFe_101'
      end
      item
        DataSet = frxDsMDFe_XXX
        DataSetName = 'frxDsMDFe_XXX'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader9: TfrxPageHeader
        FillType = ftBrush
        Height = 60.472480000000000000
        Top = 79.370130000000000000
        Width = 990.236860000000000000
        object Shape7: TfrxShapeView
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Line24: TfrxLineView
          Top = 18.897650000000000000
          Width = 990.236860000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo293: TfrxMemoView
          Left = 151.181200000000000000
          Width = 687.874460000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo da pesquisa: [VARF_PERIODO]')
          ParentFont = False
        end
        object Memo216: TfrxMemoView
          Left = 7.559060000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 839.055660000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'g. [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 264.566929130000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Status: [VARF_STATUS]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 272.126160000000000000
          Top = 18.897650000000000000
          Width = 445.984540000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_APENAS_EMP]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 718.110700000000000000
          Top = 18.897650000000000000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Ambiente: [VARF_AMBIENTE]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Top = 41.574830000000000000
          Width = 495.118430000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Empresa: [VARF_EMPRESA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 495.118430000000000000
          Top = 41.574830000000000000
          Width = 495.118430000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Terceiro: [VARF_TERCEIRO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData12: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 302.362400000000000000
        Width = 990.236860000000000000
        DataSet = frxDsMDFe_100
        DataSetName = 'frxDsMDFe_100'
        RowCount = 0
        object Memo14: TfrxMemoView
          Width = 56.692913385826770000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39', <frxDsCTe_100."ide_nNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          Left = 56.692950000000000000
          Width = 18.897637800000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsCTe_100."ide_serie">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 75.590600000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DataField = 'ide_dEmi'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_100."ide_dEmi"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 117.165430000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vProd'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vProd"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 449.764070000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vNF'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          Left = 177.637910000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vST'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vST"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 222.992270000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vFrete'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vFrete"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 268.346630000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vSeg'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vSeg"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          Left = 313.700990000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vIPI'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vIPI"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 359.055350000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vOutro'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vOutro"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 404.409710000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vDesc'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vDesc"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          Left = 502.677490000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vBC'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vBC"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          Left = 555.590910000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vICMS'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vICMS"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo63: TfrxMemoView
          Left = 918.425790000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          DataField = 'NOME_tpEmis'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_100."NOME_tpEmis"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo64: TfrxMemoView
          Left = 975.118740000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataField = 'NOME_tpNF'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_100."NOME_tpNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo93: TfrxMemoView
          Left = 608.504330000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vPIS'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vPIS"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo96: TfrxMemoView
          Left = 661.417750000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vCOFINS'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."ICMSTot_vCOFINS"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          Left = 714.331170000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataField = 'esp'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCTe_100."esp"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo62: TfrxMemoView
          Left = 767.244590000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'qVol'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."qVol"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo94: TfrxMemoView
          Left = 812.598950000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataField = 'PesoB'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."PesoB"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo97: TfrxMemoView
          Left = 865.512370000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataField = 'PesoL'
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsCTe_100."PesoL"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 37.795300000000000000
        Top = 18.897650000000000000
        Width = 990.236860000000000000
        object Shape1: TfrxShapeView
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo215: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 975.118740000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Lista de Notas Fiscais Eletr'#244'nicas')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Width = 975.118740000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 990.236860000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
      end
      object PageFooter3: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 702.992580000000000000
        Width = 990.236860000000000000
        object Memo120: TfrxMemoView
          Width = 1046.929810000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 200.315090000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsCTe_100."Ordem"'
        object Memo34: TfrxMemoView
          Top = 3.779530000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'AUTORIZADA')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677177560000000000
        Top = 377.953000000000000000
        Width = 990.236860000000000000
        object Memo54: TfrxMemoView
          Left = 449.764070000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          Left = 268.346630000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vSeg">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo56: TfrxMemoView
          Left = 313.700990000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vIPI">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          Left = 359.055350000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vOutro">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo58: TfrxMemoView
          Left = 404.409710000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vDesc">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo59: TfrxMemoView
          Left = 502.677490000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vBC">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo60: TfrxMemoView
          Left = 555.590910000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vICMS">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo65: TfrxMemoView
          Width = 117.165381180000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAIS:  ')
          ParentFont = False
          WordWrap = False
        end
        object Memo66: TfrxMemoView
          Left = 918.425790000000000000
          Width = 71.811021180000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
        object Memo67: TfrxMemoView
          Left = 117.165430000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vProd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          Left = 177.637910000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vST">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo69: TfrxMemoView
          Left = 222.992270000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vFrete">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo95: TfrxMemoView
          Left = 608.504330000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vPIS">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo98: TfrxMemoView
          Left = 661.417750000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vCOFINS">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo117: TfrxMemoView
          Left = 714.331170000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          WordWrap = False
        end
        object Memo118: TfrxMemoView
          Left = 767.244590000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."qVol">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo119: TfrxMemoView
          Left = 812.598950000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."PesoB">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo121: TfrxMemoView
          Left = 865.512370000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."PesoL">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 35.905526460000000000
        Top = 423.307360000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsCTe_101."Ordem"'
        object Memo31: TfrxMemoView
          Left = 177.637910000000000000
          Top = 22.677180000000000000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          Left = 245.669450000000000000
          Top = 22.677180000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'E/S')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          Top = 3.779530000000000000
          Width = 990.236860000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'CANCELADA')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Top = 22.677180000000000000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#186' NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          Left = 64.252010000000000000
          Top = 22.677180000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          Left = 132.283550000000000000
          Top = 22.677180000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          Left = 86.929190000000000000
          Top = 22.677180000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ano')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          Left = 109.606370000000000000
          Top = 22.677180000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          Left = 260.787570000000000000
          Top = 22.677180000000000000
          Width = 226.771653540000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Chave NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          Left = 638.740570000000000000
          Top = 22.677180000000000000
          Width = 351.496260710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Motivo ou descri'#231#227'o do status')
          ParentFont = False
          WordWrap = False
        end
        object Memo89: TfrxMemoView
          Left = 487.559370000000000000
          Top = 22.677180000000000000
          Width = 79.370081180000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'dh Recibo')
          ParentFont = False
          WordWrap = False
        end
        object Memo91: TfrxMemoView
          Left = 566.929500000000000000
          Top = 22.677180000000000000
          Width = 71.811021180000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186' protocolo canc.')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 483.779840000000000000
        Width = 990.236860000000000000
        DataSet = frxDsMDFe_101
        DataSetName = 'frxDsMDFe_101'
        RowCount = 0
        object Memo30: TfrxMemoView
          Left = 177.637910000000000000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_101."NOME_tpEmis"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          Left = 245.669450000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_101."NOME_tpNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39', <frxDsCTe_101."ide_nNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Left = 64.252010000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_XXX
          DataSetName = 'frxDsMDFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_101."ide_serie"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          Left = 132.283550000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_XXX
          DataSetName = 'frxDsMDFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_101."ide_dEmi"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          Left = 86.929190000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_XXX
          DataSetName = 'frxDsMDFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_101."ide_AAMM_AA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo49: TfrxMemoView
          Left = 109.606370000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_XXX
          DataSetName = 'frxDsMDFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_101."ide_AAMM_MM"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo51: TfrxMemoView
          Left = 260.787570000000000000
          Width = 226.771653540000000000
          Height = 13.228346460000000000
          DataField = 'Id_TXT'
          DataSet = frxDsMDFe_101
          DataSetName = 'frxDsMDFe_101'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCTe_101."Id_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo53: TfrxMemoView
          Left = 638.740570000000000000
          Width = 351.496260710000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_XXX
          DataSetName = 'frxDsMDFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCTe_101."Motivo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo90: TfrxMemoView
          Left = 487.559370000000000000
          Width = 79.370081180000000000
          Height = 13.228346460000000000
          DataField = 'infCanc_dhRecbto'
          DataSet = frxDsMDFe_101
          DataSetName = 'frxDsMDFe_101'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCTe_101."infCanc_dhRecbto"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo92: TfrxMemoView
          Left = 566.929500000000000000
          Width = 71.811021180000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_101
          DataSetName = 'frxDsMDFe_101'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCTe_101."infCanc_nProt"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        Height = 35.905526460000000000
        Top = 544.252320000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsCTe_XXX."Ordem"'
        object Memo70: TfrxMemoView
          Left = 177.637910000000000000
          Top = 22.677180000000000000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          Left = 245.669450000000000000
          Top = 22.677180000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'E/S')
          ParentFont = False
          WordWrap = False
        end
        object Memo72: TfrxMemoView
          Top = 3.779530000000000000
          Width = 990.236860000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCTe_XXX."NOME_ORDEM"]')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Top = 22.677180000000000000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#186' NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo74: TfrxMemoView
          Left = 64.252010000000000000
          Top = 22.677180000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
        end
        object Memo75: TfrxMemoView
          Left = 132.283550000000000000
          Top = 22.677180000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo76: TfrxMemoView
          Left = 86.929190000000000000
          Top = 22.677180000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ano')
          ParentFont = False
          WordWrap = False
        end
        object Memo77: TfrxMemoView
          Left = 109.606370000000000000
          Top = 22.677180000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
          WordWrap = False
        end
        object Memo78: TfrxMemoView
          Left = 260.787570000000000000
          Top = 22.677180000000000000
          Width = 30.236240000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Status')
          ParentFont = False
          WordWrap = False
        end
        object Memo79: TfrxMemoView
          Left = 291.023810000000000000
          Top = 22.677180000000000000
          Width = 699.213020710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Motivo ou descri'#231#227'o do status')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 604.724800000000000000
        Width = 990.236860000000000000
        DataSet = frxDsMDFe_XXX
        DataSetName = 'frxDsMDFe_XXX'
        RowCount = 0
        object Memo80: TfrxMemoView
          Left = 177.637910000000000000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_XXX."NOME_tpEmis"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo81: TfrxMemoView
          Left = 245.669450000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_XXX."NOME_tpNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo82: TfrxMemoView
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39', <frxDsCTe_XXX."ide_nNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo83: TfrxMemoView
          Left = 64.252010000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataField = 'ide_serie'
          DataSet = frxDsMDFe_XXX
          DataSetName = 'frxDsMDFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_XXX."ide_serie"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo84: TfrxMemoView
          Left = 132.283550000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'ide_dEmi'
          DataSet = frxDsMDFe_XXX
          DataSetName = 'frxDsMDFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_XXX."ide_dEmi"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo85: TfrxMemoView
          Left = 86.929190000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_XXX
          DataSetName = 'frxDsMDFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_XXX."ide_AAMM_AA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo86: TfrxMemoView
          Left = 109.606370000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataField = 'ide_AAMM_MM'
          DataSet = frxDsMDFe_XXX
          DataSetName = 'frxDsMDFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsCTe_XXX."ide_AAMM_MM"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo87: TfrxMemoView
          Left = 260.787570000000000000
          Width = 30.236240000000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_XXX
          DataSetName = 'frxDsMDFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsCTe_XXX."ide_serie">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo88: TfrxMemoView
          Left = 291.023810000000000000
          Width = 699.213020710000000000
          Height = 13.228346460000000000
          DataField = 'Motivo'
          DataSet = frxDsMDFe_XXX
          DataSetName = 'frxDsMDFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCTe_XXX."Motivo"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Top = 521.575140000000000000
        Width = 990.236860000000000000
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Top = 642.520100000000000000
        Width = 990.236860000000000000
      end
      object GroupHeader4: TfrxGroupHeader
        FillType = ftBrush
        Height = 32.125996460000000000
        Top = 245.669450000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsCTe_100."ide_NatOP"'
        object Memo99: TfrxMemoView
          Top = 18.897650000000000000
          Width = 56.692913385826770000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#186' NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo100: TfrxMemoView
          Left = 56.692950000000000000
          Top = 18.897650000000000000
          Width = 18.897637800000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
        end
        object Memo101: TfrxMemoView
          Left = 75.590600000000000000
          Top = 18.897650000000000000
          Width = 41.574803150000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo102: TfrxMemoView
          Left = 117.165430000000000000
          Top = 18.897650000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Produtos')
          ParentFont = False
          WordWrap = False
        end
        object Memo103: TfrxMemoView
          Left = 449.764070000000000000
          Top = 18.897650000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total NFe')
          ParentFont = False
          WordWrap = False
        end
        object Memo104: TfrxMemoView
          Left = 177.637910000000000000
          Top = 18.897650000000000000
          Width = 45.354330708661420000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ICMS ST')
          ParentFont = False
          WordWrap = False
        end
        object Memo105: TfrxMemoView
          Left = 222.992270000000000000
          Top = 18.897650000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Frete')
          ParentFont = False
          WordWrap = False
        end
        object Memo106: TfrxMemoView
          Left = 268.346630000000000000
          Top = 18.897650000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Seguro')
          ParentFont = False
          WordWrap = False
        end
        object Memo107: TfrxMemoView
          Left = 313.700990000000000000
          Top = 18.897650000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IPI')
          ParentFont = False
          WordWrap = False
        end
        object Memo108: TfrxMemoView
          Left = 359.055350000000000000
          Top = 18.897650000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Outras desp.')
          ParentFont = False
          WordWrap = False
        end
        object Memo109: TfrxMemoView
          Left = 404.409710000000000000
          Top = 18.897650000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Desconto')
          ParentFont = False
          WordWrap = False
        end
        object Memo110: TfrxMemoView
          Left = 502.677490000000000000
          Top = 18.897650000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'BC ICMS')
          ParentFont = False
          WordWrap = False
        end
        object Memo111: TfrxMemoView
          Left = 555.590910000000000000
          Top = 18.897650000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ICMS')
          ParentFont = False
          WordWrap = False
        end
        object Memo112: TfrxMemoView
          Width = 990.236860000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCTe_100."ide_NatOp"]')
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          Left = 918.425790000000000000
          Top = 18.897650000000000000
          Width = 56.692913390000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
          WordWrap = False
        end
        object Memo114: TfrxMemoView
          Left = 975.118740000000000000
          Top = 18.897650000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'E/S')
          ParentFont = False
          WordWrap = False
        end
        object Memo115: TfrxMemoView
          Left = 608.504330000000000000
          Top = 18.897650000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'PIS')
          ParentFont = False
          WordWrap = False
        end
        object Memo116: TfrxMemoView
          Left = 661.417750000000000000
          Top = 18.897650000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'COFINS')
          ParentFont = False
          WordWrap = False
        end
        object Memo122: TfrxMemoView
          Left = 714.331170000000000000
          Top = 18.897650000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Esp'#233'cie vol.')
          ParentFont = False
          WordWrap = False
        end
        object Memo123: TfrxMemoView
          Left = 767.244590000000000000
          Top = 18.897650000000000000
          Width = 45.354330708661420000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde volumes')
          ParentFont = False
          WordWrap = False
        end
        object Memo124: TfrxMemoView
          Left = 812.598950000000000000
          Top = 18.897650000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso bruto')
          ParentFont = False
          WordWrap = False
        end
        object Memo125: TfrxMemoView
          Left = 865.512370000000000000
          Top = 18.897650000000000000
          Width = 52.913385826771650000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Peso l'#237'q.')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 340.157700000000000000
        Width = 990.236860000000000000
        object Memo1: TfrxMemoView
          Left = 449.764070000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 268.346630000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vSeg">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Left = 313.700990000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vIPI">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 359.055350000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vOutro">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 404.409710000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vDesc">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 502.677490000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vBC">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          Left = 555.590910000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vICMS">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Width = 117.165381180000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'SUB TOTAIS:  ')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 117.165430000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vProd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 177.637910000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vST">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 222.992270000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vFrete">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 608.504330000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vPIS">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          Left = 661.417750000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."ICMSTot_vCOFINS">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo126: TfrxMemoView
          Left = 714.331170000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          WordWrap = False
        end
        object Memo127: TfrxMemoView
          Left = 767.244590000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."qVol">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo128: TfrxMemoView
          Left = 812.598950000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."PesoB">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo129: TfrxMemoView
          Left = 865.512370000000000000
          Width = 52.913385830000000000
          Height = 13.228346460000000000
          DataSet = frxDsMDFe_100
          DataSetName = 'frxDsMDFe_100'
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsCTe_100."PesoL">)]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object frxA4A_001Rodo: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40022.702970393500000000
    ReportOptions.LastChange = 41318.876849131900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <LogoMDFeExiste> = True then'
      '  begin              '
      '    Picture1.LoadFromFile(<LogoMDFePath>);'
      '  end;                '
      'end.')
    OnGetValue = frxA4A_001RodoGetValue
    Left = 936
    Top = 268
    Datasets = <
      item
        DataSet = frxDsA
        DataSetName = 'frxDsA'
      end
      item
        DataSet = frxDsRodoCab
        DataSetName = 'frxDsRodoCab'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      MirrorMargins = True
      LargeDesignHeight = True
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 18.897637795275590000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
      end
      object PageHeader1: TfrxPageHeader
        Tag = 1
        FillType = ftBrush
        Height = 1035.591220000000000000
        Top = 60.472480000000000000
        Width = 793.701300000000000000
        object Shape1: TfrxShapeView
          Left = 30.236240000000000000
          Top = 86.929190000000000000
          Width = 733.228605200000000000
          Height = 30.236220470000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo192: TfrxMemoView
          Left = 166.299075900000000000
          Top = 86.929177800000000000
          Width = 593.385995200000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DOCUMENTO AUXILIAR DE MANIFESTO ELETR'#212'NICO DE DOCUMENTOS FISCAIS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo147: TfrxMemoView
          Left = 34.015770000000000000
          Top = 86.929190000000000000
          Width = 132.283335200000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DAMDFE')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape3: TfrxShapeView
          Left = 30.236240000000000000
          Top = 230.551303150000000000
          Width = 733.228524650000000000
          Height = 37.795280470000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo7: TfrxMemoView
          Left = 30.236240000000000000
          Top = 230.551330000000000000
          Width = 120.944881890000000000
          Height = 22.677155590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Modelo')
          ParentFont = False
        end
        object Memo125: TfrxMemoView
          Left = 151.181102360000000000
          Top = 230.551330000000000000
          Width = 45.354330710000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
        end
        object Memo126: TfrxMemoView
          Left = 196.535433070000000000
          Top = 230.551330000000000000
          Width = 151.181102360000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#250'mero')
          ParentFont = False
        end
        object Memo127: TfrxMemoView
          Left = 347.716535430000000000
          Top = 230.551330000000000000
          Width = 79.370059210000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'FL')
          ParentFont = False
        end
        object Memo128: TfrxMemoView
          Left = 427.086890000000000000
          Top = 230.551330000000000000
          Width = 192.755925040000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data e Hora de Emiss'#227'o')
          ParentFont = False
        end
        object Memo129: TfrxMemoView
          Left = 619.842920000000000000
          Top = 230.551330000000000000
          Width = 71.811023620000000000
          Height = 37.795275590551180000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'UF Carreg.')
          ParentFont = False
        end
        object Memo130: TfrxMemoView
          Left = 30.236240000000000000
          Top = 240.000122050000000000
          Width = 120.944881890000000000
          Height = 28.346461570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_mod"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo206: TfrxMemoView
          Left = 151.181102360000000000
          Top = 240.000122050000000000
          Width = 45.354330710000000000
          Height = 28.346456690000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_serie"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo207: TfrxMemoView
          Left = 196.535433070000000000
          Top = 240.000122050000000000
          Width = 151.181102360000000000
          Height = 28.346456690000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39',<frxDsA."ide_nMDF">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo208: TfrxMemoView
          Left = 347.716535430000000000
          Top = 240.000122050000000000
          Width = 79.370059210000000000
          Height = 28.346456690000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[Page#]/[TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo209: TfrxMemoView
          Left = 427.086890000000000000
          Top = 240.000122050000000000
          Width = 192.755925040000000000
          Height = 28.346456690000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_dEmi"] [frxDsA."ide_hEmi"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo225: TfrxMemoView
          Left = 619.842920000000000000
          Top = 240.000122050000000000
          Width = 71.811023620000000000
          Height = 28.346456690000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_UFIni"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape9: TfrxShapeView
          Left = 30.236240000000000000
          Top = 11.338590000000000000
          Width = 733.228820000000000000
          Height = 75.590600000000000000
          Curve = 1
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Picture1: TfrxPictureView
          Tag = 1
          Left = 34.015770000000000000
          Top = 15.118100470000000000
          Width = 68.031496060000000000
          Height = 68.031496060000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object MeRazao_1: TfrxMemoView
          Tag = 1
          Left = 105.826869290000000000
          Top = 11.338590000000000000
          Width = 653.858690000000000000
          Height = 26.456697800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."emit_xNome"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeEnder_1: TfrxMemoView
          Tag = 1
          Left = 105.826869290000000000
          Top = 37.795300000000000000
          Width = 653.858690000000000000
          Height = 34.015752910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CNPJ [frxDsA."EMIT_CNPJ_TXT"]  IE [frxDsA."EMIT_IE_TXT"]'
            '[frxDsA."EMIT_ENDERECO"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end
            item
            end>
        end
        object MeFones_1: TfrxMemoView
          Tag = 1
          Left = 105.826869290000000000
          Top = 71.811033390000000000
          Width = 653.858690000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TELEFONE: [frxDsA."EMIT_FONE_TXT"]')
          ParentFont = False
        end
        object Shape12: TfrxShapeView
          Left = 30.236240000000000000
          Top = 272.126160000000000000
          Width = 733.228820000000000000
          Height = 170.078850000000000000
          Curve = 1
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Shape15: TfrxShapeView
          Left = 30.236240000000000000
          Top = 117.165430000000000000
          Width = 733.228820000000000000
          Height = 109.606370000000000000
          Curve = 1
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo124: TfrxMemoView
          Left = 30.236240000000000000
          Top = 274.015909130000000000
          Width = 733.228605200000000000
          Height = 24.566921810000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Modal Rodovi'#225'rio de Carga')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 34.016065350000000000
          Top = 196.535450160000000000
          Width = 725.669484170000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'PROTOCOLO DE AUTORIZA'#199#195'O DE USO')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 30.236240000000000000
          Top = 166.299249210000000000
          Width = 733.228622280000000000
          Height = 30.236225350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CHAVE DE ACESSO')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 30.236337630000000000
          Top = 176.637910000000000000
          Width = 733.228527090000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ID_TXT"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo21: TfrxMemoView
          Left = 34.015770000000000000
          Top = 204.094505280000000000
          Width = 725.669562280000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."DOC_SEM_VLR_FIS"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object BarCode1: TfrxBarCodeView
          Left = 263.810938190000000000
          Top = 130.393700790000000000
          Width = 277.000000000000000000
          Height = 30.236220470000000000
          BarType = bcCode128C
          DataField = 'Id'
          DataSet = frxDsA
          DataSetName = 'frxDsA'
          Rotation = 0
          ShowText = False
          Text = '00000000000000000000000000000000000000000000'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Memo1: TfrxMemoView
          Left = 30.236240000000000000
          Top = 117.165430000000000000
          Width = 733.228622280000000000
          Height = 11.338575350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CONTROLE DO FISCO')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 691.653990000000000000
          Top = 230.551330000000000000
          Width = 71.811023620000000000
          Height = 37.795275590551180000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'UF Descar.')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 691.653990000000000000
          Top = 240.000122050000000000
          Width = 71.811023620000000000
          Height = 28.346456690000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."ide_UFFim"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 30.236240000000000000
          Top = 298.582870000000000000
          Width = 102.047244090000000000
          Height = 22.677155590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CIOT')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 132.283452360000000000
          Top = 298.582870000000000000
          Width = 120.944881890000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde. CT-e')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 253.228383070000000000
          Top = 298.582870000000000000
          Width = 143.622047240000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde. NF-e')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 396.850425430000000000
          Top = 298.582870000000000000
          Width = 154.960629920000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde. NF')
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          Left = 551.811380000000000000
          Top = 298.582870000000000000
          Width = 211.653540870000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Peso Total (Kg)')
          ParentFont = False
        end
        object Memo120: TfrxMemoView
          Left = 30.236240000000000000
          Top = 308.031662050000000000
          Width = 102.047244090000000000
          Height = 28.346461570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CIOT]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo121: TfrxMemoView
          Left = 132.283452360000000000
          Top = 308.031662050000000000
          Width = 120.944881890000000000
          Height = 28.346456690000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."Tot_qCTe"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo122: TfrxMemoView
          Left = 253.228383070000000000
          Top = 308.031662050000000000
          Width = 143.622047240000000000
          Height = 28.346456690000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsA."Tot_qNFe"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo123: TfrxMemoView
          Left = 396.850425430000000000
          Top = 308.031662050000000000
          Width = 154.960629920000000000
          Height = 28.346456690000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo149: TfrxMemoView
          Left = 551.811380000000000000
          Top = 308.031662050000000000
          Width = 211.653540870000000000
          Height = 28.346456690000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_Tot_qCarga]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo150: TfrxMemoView
          Left = 30.236240000000000000
          Top = 336.378170000000000000
          Width = 430.866141730000000000
          Height = 15.118095590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ve'#237'culo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo182: TfrxMemoView
          Left = 461.102660000000000000
          Top = 336.378170000000000000
          Width = 302.362334090000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Condutor')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo181: TfrxMemoView
          Left = 30.236240000000000000
          Top = 351.496290000000000000
          Width = 207.874015750000000000
          Height = 22.677155590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Placa')
          ParentFont = False
        end
        object Memo183: TfrxMemoView
          Left = 30.236240000000000000
          Top = 360.945082050000000000
          Width = 207.874015750000000000
          Height = 28.346461570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRodoCab."veicTracao_placa"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo184: TfrxMemoView
          Left = 238.110390000000000000
          Top = 351.496290000000000000
          Width = 222.992125980000000000
          Height = 22.677155590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'RNTRC')
          ParentFont = False
        end
        object Memo185: TfrxMemoView
          Left = 238.110390000000000000
          Top = 360.945082050000000000
          Width = 222.992125980000000000
          Height = 28.346461570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRodoCab."RNTRC"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo186: TfrxMemoView
          Left = 461.102660000000000000
          Top = 351.496290000000000000
          Width = 139.842519690000000000
          Height = 90.708695590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CPF')
          ParentFont = False
        end
        object Memo187: TfrxMemoView
          Left = 461.102660000000000000
          Top = 362.834645670000000000
          Width = 139.842519690000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CONDUTOR_CPF_01]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo188: TfrxMemoView
          Left = 600.945270000000000000
          Top = 351.496290000000000000
          Width = 162.519685040000000000
          Height = 90.708695590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo189: TfrxMemoView
          Left = 600.945270000000000000
          Top = 362.834645670000000000
          Width = 162.519685040000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CONDUTOR_NOME_01]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo190: TfrxMemoView
          Left = 30.236240000000000000
          Top = 389.291590000000000000
          Width = 430.866354090000000000
          Height = 11.338565590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vale Ped'#225'gio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo191: TfrxMemoView
          Left = 30.236240000000000000
          Top = 400.630180000000000000
          Width = 154.960629920000000000
          Height = 41.574803150000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Respons'#225'vel CNPJ')
          ParentFont = False
        end
        object Memo193: TfrxMemoView
          Left = 30.236240000000000000
          Top = 411.968503940000000000
          Width = 154.960629920000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_VPED_RESP_CNPJ_01]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo194: TfrxMemoView
          Left = 185.196970000000000000
          Top = 400.630180000000000000
          Width = 139.842519690000000000
          Height = 41.574803149606300000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Fornecedora CNPJ')
          ParentFont = False
        end
        object Memo195: TfrxMemoView
          Left = 185.196970000000000000
          Top = 411.968503940000000000
          Width = 139.842519690000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_VPED_PAGA_CNPJ_01]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo196: TfrxMemoView
          Left = 325.039580000000000000
          Top = 400.630180000000000000
          Width = 136.062992130000000000
          Height = 41.574803150000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#186' Comprovante')
          ParentFont = False
        end
        object Memo197: TfrxMemoView
          Left = 325.039580000000000000
          Top = 411.968503940000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_VPED_NUM_COMPR_01]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo198: TfrxMemoView
          Left = 461.102660000000000000
          Top = 374.173470000000000000
          Width = 139.842519690000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CONDUTOR_CPF_02]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo199: TfrxMemoView
          Left = 600.945270000000000000
          Top = 374.173470000000000000
          Width = 162.519685040000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CONDUTOR_NOME_02]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo200: TfrxMemoView
          Left = 461.102660000000000000
          Top = 385.512060000000000000
          Width = 139.842519690000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CONDUTOR_CPF_03]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo226: TfrxMemoView
          Left = 600.945270000000000000
          Top = 385.512060000000000000
          Width = 162.519685040000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CONDUTOR_NOME_03]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo308: TfrxMemoView
          Left = 461.102660000000000000
          Top = 396.850650000000000000
          Width = 139.842519690000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CONDUTOR_CPF_04]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo309: TfrxMemoView
          Left = 600.945270000000000000
          Top = 396.850650000000000000
          Width = 162.519685040000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CONDUTOR_NOME_04]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo310: TfrxMemoView
          Left = 461.102660000000000000
          Top = 408.189240000000000000
          Width = 139.842519690000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CONDUTOR_CPF_05]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo311: TfrxMemoView
          Left = 600.945270000000000000
          Top = 408.189240000000000000
          Width = 162.519685040000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CONDUTOR_NOME_05]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo312: TfrxMemoView
          Left = 461.102660000000000000
          Top = 419.527830000000000000
          Width = 139.842519690000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CONDUTOR_CPF_06]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo313: TfrxMemoView
          Left = 600.945270000000000000
          Top = 419.527830000000000000
          Width = 162.519685040000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CONDUTOR_NOME_06]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo314: TfrxMemoView
          Left = 461.102660000000000000
          Top = 430.866420000000000000
          Width = 139.842519690000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CONDUTOR_CPF_07]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo315: TfrxMemoView
          Left = 600.945270000000000000
          Top = 430.866420000000000000
          Width = 162.519685040000000000
          Height = 11.338582680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CONDUTOR_NOME_07]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo316: TfrxMemoView
          Left = 30.236240000000000000
          Top = 427.086890000000000000
          Width = 154.960629920000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_VPED_RESP_CNPJ_02]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo326: TfrxMemoView
          Left = 185.196970000000000000
          Top = 427.086890000000000000
          Width = 139.842519690000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_VPED_PAGA_CNPJ_02]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo327: TfrxMemoView
          Left = 325.039580000000000000
          Top = 427.086890000000000000
          Width = 136.062992130000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_VPED_NUM_COMPR_02]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape2: TfrxShapeView
          Left = 30.236240000000000000
          Top = 445.984540000000000000
          Width = 733.228820000000000000
          Height = 589.606680000000000000
          Curve = 1
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo6: TfrxMemoView
          Left = 34.015770000000000000
          Top = 445.984540000000000000
          Width = 725.669291340000000000
          Height = 11.338565590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Observa'#231#227'o')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 34.015770000000000000
          Top = 447.874272050000000000
          Width = 725.669291340000000000
          Height = 587.716901570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '                       [frxDsA."infAdFisco"]'
            '[frxDsA."infCpl"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
      end
    end
  end
  object QrOpcoesMDFe: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT etc0.Nome NO_ETC_0, etc1.Nome NO_ETC_1,  ctr.*'
      'FROM paramsemp ctr'
      'LEFT JOIN entitipcto etc0 ON etc0.Codigo=ctr.EntiTipCto'
      'LEFT JOIN entitipcto etc1 ON etc1.Codigo=ctr.EntiTipCt1'
      'WHERE ctr.Codigo=:P0')
    Left = 24
    Top = 280
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOpcoesMDFeMDFeversao: TFloatField
      FieldName = 'MDFeversao'
    end
    object QrOpcoesMDFeMDFeide_mod: TSmallintField
      FieldName = 'MDFeide_mod'
    end
    object QrOpcoesMDFeSimplesFed: TSmallintField
      FieldName = 'SimplesFed'
    end
    object QrOpcoesMDFeDirMDFeGer: TWideStringField
      FieldName = 'DirMDFeGer'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeAss: TWideStringField
      FieldName = 'DirMDFeAss'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEnvLot: TWideStringField
      FieldName = 'DirMDFeEnvLot'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeRec: TWideStringField
      FieldName = 'DirMDFeRec'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFePedRec: TWideStringField
      FieldName = 'DirMDFePedRec'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeProRec: TWideStringField
      FieldName = 'DirMDFeProRec'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeDen: TWideStringField
      FieldName = 'DirMDFeDen'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFePedCan: TWideStringField
      FieldName = 'DirMDFePedCan'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeCan: TWideStringField
      FieldName = 'DirMDFeCan'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFePedInu: TWideStringField
      FieldName = 'DirMDFePedInu'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeInu: TWideStringField
      FieldName = 'DirMDFeInu'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFePedSit: TWideStringField
      FieldName = 'DirMDFePedSit'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeSit: TWideStringField
      FieldName = 'DirMDFeSit'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFePedSta: TWideStringField
      FieldName = 'DirMDFePedSta'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeSta: TWideStringField
      FieldName = 'DirMDFeSta'
      Size = 255
    end
    object QrOpcoesMDFeMDFeUF_WebServ: TWideStringField
      FieldName = 'MDFeUF_WebServ'
      Size = 2
    end
    object QrOpcoesMDFeMDFeUF_Servico: TWideStringField
      FieldName = 'MDFeUF_Servico'
      Size = 10
    end
    object QrOpcoesMDFePathLogoMDFe: TWideStringField
      FieldName = 'PathLogoMDFe'
      Size = 255
    end
    object QrOpcoesMDFeCtaFretPrest: TIntegerField
      FieldName = 'CtaFretPrest'
    end
    object QrOpcoesMDFeTxtFretPrest: TWideStringField
      FieldName = 'TxtFretPrest'
      Size = 100
    end
    object QrOpcoesMDFeDupFretPrest: TWideStringField
      FieldName = 'DupFretPrest'
      Size = 3
    end
    object QrOpcoesMDFeMDFeSerNum: TWideStringField
      FieldName = 'MDFeSerNum'
      Size = 255
    end
    object QrOpcoesMDFeMDFeSerVal: TDateField
      FieldName = 'MDFeSerVal'
    end
    object QrOpcoesMDFeMDFeSerAvi: TSmallintField
      FieldName = 'MDFeSerAvi'
    end
    object QrOpcoesMDFeDirDAMDFes: TWideStringField
      FieldName = 'DirDAMDFes'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeProt: TWideStringField
      FieldName = 'DirMDFeProt'
      Size = 255
    end
    object QrOpcoesMDFeMDFePreMailAut: TIntegerField
      FieldName = 'MDFePreMailAut'
    end
    object QrOpcoesMDFeMDFePreMailCan: TIntegerField
      FieldName = 'MDFePreMailCan'
    end
    object QrOpcoesMDFeMDFePreMailEveCCe: TIntegerField
      FieldName = 'MDFePreMailEveCCe'
    end
    object QrOpcoesMDFeMDFeVerStaSer: TFloatField
      FieldName = 'MDFeVerStaSer'
    end
    object QrOpcoesMDFeMDFeVerEnvLot: TFloatField
      FieldName = 'MDFeVerEnvLot'
    end
    object QrOpcoesMDFeMDFeVerConLot: TFloatField
      FieldName = 'MDFeVerConLot'
    end
    object QrOpcoesMDFeMDFeVerCanMDFe: TFloatField
      FieldName = 'MDFeVerCanMDFe'
    end
    object QrOpcoesMDFeMDFeVerInuNum: TFloatField
      FieldName = 'MDFeVerInuNum'
    end
    object QrOpcoesMDFeMDFeVerConMDFe: TFloatField
      FieldName = 'MDFeVerConMDFe'
    end
    object QrOpcoesMDFeMDFeVerLotEve: TFloatField
      FieldName = 'MDFeVerLotEve'
    end
    object QrOpcoesMDFeMDFeide_tpImp: TSmallintField
      FieldName = 'MDFeide_tpImp'
    end
    object QrOpcoesMDFeMDFeide_tpAmb: TSmallintField
      FieldName = 'MDFeide_tpAmb'
    end
    object QrOpcoesMDFeMDFeAppCode: TSmallintField
      FieldName = 'MDFeAppCode'
    end
    object QrOpcoesMDFeMDFeAssDigMode: TSmallintField
      FieldName = 'MDFeAssDigMode'
    end
    object QrOpcoesMDFeMDFeEntiTipCto: TIntegerField
      FieldName = 'MDFeEntiTipCto'
    end
    object QrOpcoesMDFeMDFeEntiTipCt1: TIntegerField
      FieldName = 'MDFeEntiTipCt1'
    end
    object QrOpcoesMDFeMyEmailMDFe: TWideStringField
      FieldName = 'MyEmailMDFe'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeSchema: TWideStringField
      FieldName = 'DirMDFeSchema'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeRWeb: TWideStringField
      FieldName = 'DirMDFeRWeb'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEveEnvLot: TWideStringField
      FieldName = 'DirMDFeEveEnvLot'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEveRetLot: TWideStringField
      FieldName = 'DirMDFeEveRetLot'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEvePedEnc: TWideStringField
      FieldName = 'DirMDFeEvePedEnc'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEveRetEnc: TWideStringField
      FieldName = 'DirMDFeEveRetEnc'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEveProcEnc: TWideStringField
      FieldName = 'DirMDFeEveProcEnc'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEvePedCan: TWideStringField
      FieldName = 'DirMDFeEvePedCan'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEveRetCan: TWideStringField
      FieldName = 'DirMDFeEveRetCan'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeRetMDFeDes: TWideStringField
      FieldName = 'DirMDFeRetMDFeDes'
      Size = 255
    end
    object QrOpcoesMDFeMDFeMDFeVerConDes: TFloatField
      FieldName = 'MDFeMDFeVerConDes'
    end
    object QrOpcoesMDFeDirMDFeEvePedMDe: TWideStringField
      FieldName = 'DirMDFeEvePedMDe'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEveRetMDe: TWideStringField
      FieldName = 'DirMDFeEveRetMDe'
      Size = 255
    end
    object QrOpcoesMDFeDirDowMDFeDes: TWideStringField
      FieldName = 'DirDowMDFeDes'
      Size = 255
    end
    object QrOpcoesMDFeDirDowMDFeMDFe: TWideStringField
      FieldName = 'DirDowMDFeMDFe'
      Size = 255
    end
    object QrOpcoesMDFeMDFeInfCpl: TIntegerField
      FieldName = 'MDFeInfCpl'
    end
    object QrOpcoesMDFeMDFeVerConsCad: TFloatField
      FieldName = 'MDFeVerConsCad'
    end
    object QrOpcoesMDFeMDFeVerDistDFeInt: TFloatField
      FieldName = 'MDFeVerDistDFeInt'
    end
    object QrOpcoesMDFeMDFeVerDowMDFe: TFloatField
      FieldName = 'MDFeVerDowMDFe'
    end
    object QrOpcoesMDFeDirDowMDFeCnf: TWideStringField
      FieldName = 'DirDowMDFeCnf'
      Size = 255
    end
    object QrOpcoesMDFeMDFeItsLin: TSmallintField
      FieldName = 'MDFeItsLin'
    end
    object QrOpcoesMDFeMDFeFTRazao: TSmallintField
      FieldName = 'MDFeFTRazao'
    end
    object QrOpcoesMDFeMDFeFTEnder: TSmallintField
      FieldName = 'MDFeFTEnder'
    end
    object QrOpcoesMDFeMDFeFTFones: TSmallintField
      FieldName = 'MDFeFTFones'
    end
    object QrOpcoesMDFeMDFeMaiusc: TSmallintField
      FieldName = 'MDFeMaiusc'
    end
    object QrOpcoesMDFeMDFeShowURL: TWideStringField
      FieldName = 'MDFeShowURL'
      Size = 255
    end
    object QrOpcoesMDFeMDFetpEmis: TSmallintField
      FieldName = 'MDFetpEmis'
    end
    object QrOpcoesMDFeMDFeSCAN_Ser: TIntegerField
      FieldName = 'MDFeSCAN_Ser'
    end
    object QrOpcoesMDFeMDFeSCAN_nMDF: TIntegerField
      FieldName = 'MDFeSCAN_nMDF'
    end
    object QrOpcoesMDFeMDFe_indFinalCpl: TSmallintField
      FieldName = 'MDFe_indFinalCpl'
    end
    object QrOpcoesMDFeTZD_UTC_Auto_TermoAceite: TIntegerField
      FieldName = 'TZD_UTC_Auto_TermoAceite'
    end
    object QrOpcoesMDFeTZD_UTC_Auto_DataHoraAceite: TDateTimeField
      FieldName = 'TZD_UTC_Auto_DataHoraAceite'
    end
    object QrOpcoesMDFeTZD_UTC_Auto_UsuarioAceite: TIntegerField
      FieldName = 'TZD_UTC_Auto_UsuarioAceite'
    end
    object QrOpcoesMDFeTZD_UTC_Auto: TSmallintField
      FieldName = 'TZD_UTC_Auto'
    end
    object QrOpcoesMDFeTZD_UTC: TFloatField
      FieldName = 'TZD_UTC'
    end
    object QrOpcoesMDFehVeraoAsk: TDateField
      FieldName = 'hVeraoAsk'
    end
    object QrOpcoesMDFehVeraoIni: TDateField
      FieldName = 'hVeraoIni'
    end
    object QrOpcoesMDFehVeraoFim: TDateField
      FieldName = 'hVeraoFim'
    end
    object QrOpcoesMDFeParamsMDFe: TIntegerField
      FieldName = 'ParamsMDFe'
    end
    object QrOpcoesMDFeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrRodoCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mdfemorodocab')
    Left = 784
    Top = 316
    object QrRodoCabFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrRodoCabFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrRodoCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrRodoCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrRodoCabRNTRC: TWideStringField
      FieldName = 'RNTRC'
      Size = 8
    end
    object QrRodoCabveicTracao_cInt: TIntegerField
      FieldName = 'veicTracao_cInt'
    end
    object QrRodoCabveicTracao_placa: TWideStringField
      FieldName = 'veicTracao_placa'
      Size = 7
    end
    object QrRodoCabveicTracao_RENAVAM: TWideStringField
      FieldName = 'veicTracao_RENAVAM'
      Size = 11
    end
    object QrRodoCabveicTracao_tara: TIntegerField
      FieldName = 'veicTracao_tara'
    end
    object QrRodoCabveicTracao_capKg: TIntegerField
      FieldName = 'veicTracao_capKg'
    end
    object QrRodoCabveicTracao_capM3: TIntegerField
      FieldName = 'veicTracao_capM3'
    end
    object QrRodoCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrRodoCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrRodoCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrRodoCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrRodoCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrRodoCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrRodoCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrRodoCabcodAgPorto: TWideStringField
      FieldName = 'codAgPorto'
      Size = 16
    end
    object QrRodoCabveicTracao_tpRod: TSmallintField
      FieldName = 'veicTracao_tpRod'
    end
    object QrRodoCabveicTracao_tpCar: TSmallintField
      FieldName = 'veicTracao_tpCar'
    end
    object QrRodoCabveicTracao_UF: TWideStringField
      FieldName = 'veicTracao_UF'
      Size = 2
    end
  end
  object frxDsRodoCab: TfrxDBDataset
    UserName = 'frxDsRodoCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'FatID=FatID'
      'FatNum=FatNum'
      'Empresa=Empresa'
      'Controle=Controle'
      'RNTRC=RNTRC'
      'veicTracao_cInt=veicTracao_cInt'
      'veicTracao_placa=veicTracao_placa'
      'veicTracao_RENAVAM=veicTracao_RENAVAM'
      'veicTracao_tara=veicTracao_tara'
      'veicTracao_capKg=veicTracao_capKg'
      'veicTracao_capM3=veicTracao_capM3'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'codAgPorto=codAgPorto'
      'veicTracao_tpRod=veicTracao_tpRod'
      'veicTracao_tpCar=veicTracao_tpCar'
      'veicTracao_UF=veicTracao_UF')
    DataSet = QrRodoCab
    BCDToCurrency = False
    Left = 864
    Top = 316
  end
  object QrRodoCIOT: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mdfemorodociot')
    Left = 784
    Top = 364
    object QrRodoCIOTCIOT: TLargeintField
      FieldName = 'CIOT'
    end
  end
  object QrRodoCondutor: TmySQLQuery
    Database = Dmod.MyLocDatabase
    Left = 784
    Top = 412
    object QrRodoCondutorxNome: TWideStringField
      FieldName = 'xNome'
      Size = 60
    end
    object QrRodoCondutorCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
  end
  object QrRodoVPed: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM mdfemorodovped')
    Left = 784
    Top = 460
    object QrRodoVPedCNPJForn: TWideStringField
      FieldName = 'CNPJForn'
      Size = 18
    end
    object QrRodoVPedCNPJPg: TWideStringField
      FieldName = 'CNPJPg'
      Size = 18
    end
    object QrRodoVPednCompra: TWideStringField
      FieldName = 'nCompra'
    end
  end
end
