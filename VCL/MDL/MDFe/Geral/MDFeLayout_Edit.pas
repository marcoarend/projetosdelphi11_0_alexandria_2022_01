unit MDFeLayout_Edit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, DB, Mask,
  DBCtrls, dmkEdit, dmkMemo, dmkCheckBox, dmkGeral, UnDmkEnums, dmkImage;

type
  TFmMDFeLayout_Edit = class(TForm)
    Panel1: TPanel;
    EdGrupo: TdmkEdit;
    EdCodigo: TdmkEdit;
    EdNivel: TdmkEdit;
    EdCampo: TdmkEdit;
    EdDescricao: TdmkEdit;
    EdElemento: TdmkEdit;
    EdTipo: TdmkEdit;
    EdOcorMin: TdmkEdit;
    EdOcorMax: TdmkEdit;
    EdTamMin: TdmkEdit;
    EdTamMax: TdmkEdit;
    EdTamVar: TdmkEdit;
    EdDeciCasas: TdmkEdit;
    MeObservacao: TdmkMemo;
    Label15: TLabel;
    Label5: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    CkInfoVazio: TdmkCheckBox;
    EdFormatStr: TdmkEdit;
    Label16: TLabel;
    EdVersao: TdmkEdit;
    Label17: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtConfirma: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    EdDominio: TdmkEdit;
    Label19: TLabel;
    EdExpReg: TdmkEdit;
    Label20: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo, FGrupo: Integer;
    FVersao: Double;
    FContinua: Boolean;
  end;

  var
  FmMDFeLayout_Edit: TFmMDFeLayout_Edit;

implementation

{$R *.DFM}

uses UnMyObjects, UMySQLModule, Module, MDFeLayout_0100;


procedure TFmMDFeLayout_Edit.BtConfirmaClick(Sender: TObject);
var
(*
  LeftZeros: Integer;
  Pai, ID, CodigoX, SubGrupo: String;
  CodigoN: Double;
  //
*)
  Campos: array of String;
  Valores: array of Variant;
var
  Campo, Descricao, Elemento, Tipo, TamVar, Observacao, Dominio, ExpReg,
  FormatStr: String;
  Grupo, Codigo, Nivel, OcorMin, OcorMax, TamMin, TamMax, InfoVazio,
  DeciCasas(*, CartaCorrige*): Integer;
  Versao: Double;
  SQLType: TSQLType;
begin
  Campo        := EdCampo.ValueVariant;
  Descricao    := EdDescricao.ValueVariant;
  Elemento     := EdElemento.ValueVariant;
  Tipo         := EdTipo.ValueVariant;
  TamVar       := EdTamVar.ValueVariant;
  Observacao   := MeObservacao.Text;
  Dominio      := EdDominio.Text;
  ExpReg       := EdExpReg.Text;
  FormatStr    := EdFormatStr.ValueVariant;

  Grupo        := EdGrupo.ValueVariant;
  Codigo       := EdCodigo.ValueVariant;
  Nivel        := EdNivel.ValueVariant;
  OcorMin      := EdOcorMin.ValueVariant;
  OcorMax      := EdOcorMax.ValueVariant;
  TamMin       := EdTamMin.ValueVariant;
  TamMax       := EdTamMax.ValueVariant;
  InfoVazio    := MLAGeral.BTI(CkInfoVazio.Checked);
  DeciCasas    := EdDeciCasas.ValueVariant;
  //CartaCorrige := MLAGeral.BTI(CkInfoVazio.Checked);

  Versao       := EdVersao.ValueVariant;

(*
  Pai          := EdPai.ValueVariant;
  ID           := EdID.ValueVariant;
  SubGrupo     := EdSubGrupo.Text;
  CodigoN      := EdCodigoN.ValueVariant;
  CodigoX      := EdCodigoX.Text;
*)
  //
  if ImgTipo.SQLType = stIns then
  begin
    FCodigo    := Codigo;
    FGrupo     := Grupo;
    FVersao    := Versao;
  end;
  //
  if ImgTipo.SQLType = stUpd then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'mdfelayi', False, [
    'Campo', 'Descricao',
    'Elemento', 'Tipo', 'OcorMin',
    'OcorMax', 'TamMin', 'TamMax',
    'TamVar', 'DeciCasas', 'Observacao',
    'InfoVazio', 'FormatStr',
    'Dominio', 'ExpReg', 'ExpReg',
    'Codigo', 'Grupo', 'Versao'],
    [
    'Codigo', 'Grupo', 'Versao'],
    [
    Campo, Descricao,
    Elemento, Tipo, OcorMin,
    OcorMax, TamMin, TamMax,
    TamVar, DeciCasas, Observacao,
    InfoVazio, FormatStr,
    Dominio, ExpReg, ExpReg,
    Codigo, Grupo, Versao],
    [
    FCodigo, FGrupo, FVersao], False) then
      Close;
  end else
  // stIns
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'mdfelayi', False, [
    'Campo', 'Descricao',
    'Elemento', 'Tipo', 'OcorMin',
    'OcorMax', 'TamMin', 'TamMax',
    'TamVar', 'DeciCasas', 'Observacao',
    'InfoVazio', 'FormatStr',
    'Dominio', 'ExpReg', 'ExpReg'],
    [
    'Codigo', 'Grupo', 'Versao'],
    [
    Campo, Descricao,
    Elemento, Tipo, OcorMin,
    OcorMax, TamMin, TamMax,
    TamVar, DeciCasas, Observacao,
    InfoVazio, FormatStr,
    Dominio, ExpReg],
    [
    FCodigo, Grupo, FVersao], False) then
      Close;
  end;
end;

procedure TFmMDFeLayout_Edit.BtSaidaClick(Sender: TObject);
begin
  FContinua := False;
  Close;
end;

procedure TFmMDFeLayout_Edit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMDFeLayout_Edit.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FContinua := True;
end;

procedure TFmMDFeLayout_Edit.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
