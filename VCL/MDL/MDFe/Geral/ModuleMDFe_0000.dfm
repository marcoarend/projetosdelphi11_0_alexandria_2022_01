object DmMDFe_0000: TDmMDFe_0000
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 844
  Width = 1028
  object QrOpcoesMDFe: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrOpcoesMDFeAfterOpen
    SQL.Strings = (
      'SELECT etc0.Nome NO_ETC_0, etc1.Nome NO_ETC_1,  ctr.*'
      'FROM paramsemp ctr'
      'LEFT JOIN entitipcto etc0 ON etc0.Codigo=ctr.EntiTipCto'
      'LEFT JOIN entitipcto etc1 ON etc1.Codigo=ctr.EntiTipCt1'
      'WHERE ctr.Codigo=:P0')
    Left = 32
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOpcoesMDFeMDFeversao: TFloatField
      FieldName = 'MDFeversao'
    end
    object QrOpcoesMDFeMDFeide_mod: TSmallintField
      FieldName = 'MDFeide_mod'
    end
    object QrOpcoesMDFeSimplesFed: TSmallintField
      FieldName = 'SimplesFed'
    end
    object QrOpcoesMDFeDirMDFeGer: TWideStringField
      FieldName = 'DirMDFeGer'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeAss: TWideStringField
      FieldName = 'DirMDFeAss'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEnvLot: TWideStringField
      FieldName = 'DirMDFeEnvLot'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeRec: TWideStringField
      FieldName = 'DirMDFeRec'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFePedRec: TWideStringField
      FieldName = 'DirMDFePedRec'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeProRec: TWideStringField
      FieldName = 'DirMDFeProRec'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeDen: TWideStringField
      FieldName = 'DirMDFeDen'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFePedCan: TWideStringField
      FieldName = 'DirMDFePedCan'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeCan: TWideStringField
      FieldName = 'DirMDFeCan'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFePedInu: TWideStringField
      FieldName = 'DirMDFePedInu'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeInu: TWideStringField
      FieldName = 'DirMDFeInu'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFePedSit: TWideStringField
      FieldName = 'DirMDFePedSit'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeSit: TWideStringField
      FieldName = 'DirMDFeSit'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFePedSta: TWideStringField
      FieldName = 'DirMDFePedSta'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeSta: TWideStringField
      FieldName = 'DirMDFeSta'
      Size = 255
    end
    object QrOpcoesMDFeMDFeUF_WebServ: TWideStringField
      FieldName = 'MDFeUF_WebServ'
      Size = 2
    end
    object QrOpcoesMDFeMDFeUF_Servico: TWideStringField
      FieldName = 'MDFeUF_Servico'
      Size = 10
    end
    object QrOpcoesMDFePathLogoMDFe: TWideStringField
      FieldName = 'PathLogoMDFe'
      Size = 255
    end
    object QrOpcoesMDFeCtaFretPrest: TIntegerField
      FieldName = 'CtaFretPrest'
    end
    object QrOpcoesMDFeTxtFretPrest: TWideStringField
      FieldName = 'TxtFretPrest'
      Size = 100
    end
    object QrOpcoesMDFeDupFretPrest: TWideStringField
      FieldName = 'DupFretPrest'
      Size = 3
    end
    object QrOpcoesMDFeMDFeSerNum: TWideStringField
      FieldName = 'MDFeSerNum'
      Size = 255
    end
    object QrOpcoesMDFeMDFeSerVal: TDateField
      FieldName = 'MDFeSerVal'
    end
    object QrOpcoesMDFeMDFeSerAvi: TSmallintField
      FieldName = 'MDFeSerAvi'
    end
    object QrOpcoesMDFeDirDAMDFes: TWideStringField
      FieldName = 'DirDAMDFes'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeProt: TWideStringField
      FieldName = 'DirMDFeProt'
      Size = 255
    end
    object QrOpcoesMDFeMDFePreMailAut: TIntegerField
      FieldName = 'MDFePreMailAut'
    end
    object QrOpcoesMDFeMDFePreMailCan: TIntegerField
      FieldName = 'MDFePreMailCan'
    end
    object QrOpcoesMDFeMDFePreMailEveCCe: TIntegerField
      FieldName = 'MDFePreMailEveCCe'
    end
    object QrOpcoesMDFeMDFeVerStaSer: TFloatField
      FieldName = 'MDFeVerStaSer'
    end
    object QrOpcoesMDFeMDFeVerEnvLot: TFloatField
      FieldName = 'MDFeVerEnvLot'
    end
    object QrOpcoesMDFeMDFeVerConLot: TFloatField
      FieldName = 'MDFeVerConLot'
    end
    object QrOpcoesMDFeMDFeVerNaoEnc: TFloatField
      FieldName = 'MDFeVerNaoEnc'
    end
    object QrOpcoesMDFeMDFeVerCanMDFe: TFloatField
      FieldName = 'MDFeVerCanMDFe'
    end
    object QrOpcoesMDFeMDFeVerInuNum: TFloatField
      FieldName = 'MDFeVerInuNum'
    end
    object QrOpcoesMDFeMDFeVerConMDFe: TFloatField
      FieldName = 'MDFeVerConMDFe'
    end
    object QrOpcoesMDFeMDFeVerLotEve: TFloatField
      FieldName = 'MDFeVerLotEve'
    end
    object QrOpcoesMDFeMDFeide_tpImp: TSmallintField
      FieldName = 'MDFeide_tpImp'
    end
    object QrOpcoesMDFeMDFeide_tpAmb: TSmallintField
      FieldName = 'MDFeide_tpAmb'
    end
    object QrOpcoesMDFeMDFeAppCode: TSmallintField
      FieldName = 'MDFeAppCode'
    end
    object QrOpcoesMDFeMDFeAssDigMode: TSmallintField
      FieldName = 'MDFeAssDigMode'
    end
    object QrOpcoesMDFeMDFeEntiTipCto: TIntegerField
      FieldName = 'MDFeEntiTipCto'
    end
    object QrOpcoesMDFeMDFeEntiTipCt1: TIntegerField
      FieldName = 'MDFeEntiTipCt1'
    end
    object QrOpcoesMDFeMyEmailMDFe: TWideStringField
      FieldName = 'MyEmailMDFe'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeSchema: TWideStringField
      FieldName = 'DirMDFeSchema'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeRWeb: TWideStringField
      FieldName = 'DirMDFeRWeb'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEveEnvLot: TWideStringField
      FieldName = 'DirMDFeEveEnvLot'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEveRetLot: TWideStringField
      FieldName = 'DirMDFeEveRetLot'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEvePedEnc: TWideStringField
      FieldName = 'DirMDFeEvePedEnc'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEveRetEnc: TWideStringField
      FieldName = 'DirMDFeEveRetEnc'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEveProcEnc: TWideStringField
      FieldName = 'DirMDFeEveProcEnc'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEvePedCan: TWideStringField
      FieldName = 'DirMDFeEvePedCan'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEveRetCan: TWideStringField
      FieldName = 'DirMDFeEveRetCan'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeRetMDFeDes: TWideStringField
      FieldName = 'DirMDFeRetMDFeDes'
      Size = 255
    end
    object QrOpcoesMDFeMDFeMDFeVerConDes: TFloatField
      FieldName = 'MDFeMDFeVerConDes'
    end
    object QrOpcoesMDFeDirMDFeEvePedMDe: TWideStringField
      FieldName = 'DirMDFeEvePedMDe'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEveRetMDe: TWideStringField
      FieldName = 'DirMDFeEveRetMDe'
      Size = 255
    end
    object QrOpcoesMDFeDirDowMDFeDes: TWideStringField
      FieldName = 'DirDowMDFeDes'
      Size = 255
    end
    object QrOpcoesMDFeDirDowMDFeMDFe: TWideStringField
      FieldName = 'DirDowMDFeMDFe'
      Size = 255
    end
    object QrOpcoesMDFeMDFeInfCpl: TIntegerField
      FieldName = 'MDFeInfCpl'
    end
    object QrOpcoesMDFeMDFeVerConsCad: TFloatField
      FieldName = 'MDFeVerConsCad'
    end
    object QrOpcoesMDFeMDFeVerDistDFeInt: TFloatField
      FieldName = 'MDFeVerDistDFeInt'
    end
    object QrOpcoesMDFeMDFeVerDowMDFe: TFloatField
      FieldName = 'MDFeVerDowMDFe'
    end
    object QrOpcoesMDFeDirDowMDFeCnf: TWideStringField
      FieldName = 'DirDowMDFeCnf'
      Size = 255
    end
    object QrOpcoesMDFeMDFeItsLin: TSmallintField
      FieldName = 'MDFeItsLin'
    end
    object QrOpcoesMDFeMDFeFTRazao: TSmallintField
      FieldName = 'MDFeFTRazao'
    end
    object QrOpcoesMDFeMDFeFTEnder: TSmallintField
      FieldName = 'MDFeFTEnder'
    end
    object QrOpcoesMDFeMDFeFTFones: TSmallintField
      FieldName = 'MDFeFTFones'
    end
    object QrOpcoesMDFeMDFeMaiusc: TSmallintField
      FieldName = 'MDFeMaiusc'
    end
    object QrOpcoesMDFeMDFeShowURL: TWideStringField
      FieldName = 'MDFeShowURL'
      Size = 255
    end
    object QrOpcoesMDFeMDFetpEmis: TSmallintField
      FieldName = 'MDFetpEmis'
    end
    object QrOpcoesMDFeMDFeSCAN_Ser: TIntegerField
      FieldName = 'MDFeSCAN_Ser'
    end
    object QrOpcoesMDFeMDFeSCAN_nMDF: TIntegerField
      FieldName = 'MDFeSCAN_nMDF'
    end
    object QrOpcoesMDFeMDFe_indFinalCpl: TSmallintField
      FieldName = 'MDFe_indFinalCpl'
    end
    object QrOpcoesMDFeTZD_UTC_Auto_TermoAceite: TIntegerField
      FieldName = 'TZD_UTC_Auto_TermoAceite'
    end
    object QrOpcoesMDFeTZD_UTC_Auto_DataHoraAceite: TDateTimeField
      FieldName = 'TZD_UTC_Auto_DataHoraAceite'
    end
    object QrOpcoesMDFeTZD_UTC_Auto_UsuarioAceite: TIntegerField
      FieldName = 'TZD_UTC_Auto_UsuarioAceite'
    end
    object QrOpcoesMDFeTZD_UTC_Auto: TSmallintField
      FieldName = 'TZD_UTC_Auto'
    end
    object QrOpcoesMDFeTZD_UTC: TFloatField
      FieldName = 'TZD_UTC'
    end
    object QrOpcoesMDFehVeraoAsk: TDateField
      FieldName = 'hVeraoAsk'
    end
    object QrOpcoesMDFehVeraoIni: TDateField
      FieldName = 'hVeraoIni'
    end
    object QrOpcoesMDFehVeraoFim: TDateField
      FieldName = 'hVeraoFim'
    end
    object QrOpcoesMDFeParamsMDFe: TIntegerField
      FieldName = 'ParamsMDFe'
    end
    object QrOpcoesMDFeCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
  end
  object QrEmpresa: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, en.Tipo, en.CNPJ, en.CPF, '
      'en.IE, en.IEST, en.RG, en.NIRE, en.CNAE,  uf.DTB DTB_UF,'
      'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_ENT,'
      'IF(en.Tipo=0,en.Fantasia,en.Apelido) FANTASIA,'
      'IF(en.Tipo=0,en.ERua,en.PRua) RUA,'
      'IF(en.Tipo=0,en.ENumero,en.PNumero) + 0.000 Numero,'
      'IF(en.Tipo=0,en.ECompl,en.PCompl) COMPL,'
      'IF(en.Tipo=0,en.EBairro,en.PBairro) BAIRRO,'
      'IF(en.Tipo=0,en.ECEP,en.PCEP) + 0.000 CEP,'
      'IF(en.Tipo=0,en.ETe1,en.PTe1) Te1,'
      'IF(en.Tipo=0,en.EUF,en.PUF) + 0.000 UF,'
      'IF(en.Tipo=0,en.ECodiPais,en.PCodiPais) + 0.000 CodiPais,'
      ''
      'IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) + 0.000 CodMunici,'
      'll.Nome NO_LOGRAD, mu.Nome NO_Munici, uf.Nome NO_UF,'
      'bpa.Nome NO_Pais, en.SUFRAMA, en.Filial'
      ''
      'FROM entidades en'
      
        'LEFT JOIN listalograd ll ON ll.Codigo=IF(en.Tipo=0,en.ELograd,en' +
        '.PLograd) '
      
        'LEFT JOIN dtb_munici mu ON mu.Codigo=IF(en.Tipo=0,en.ECodMunici,' +
        'en.PCodMunici)'
      'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF)'
      
        'LEFT JOIN bacen_pais bpa ON bpa.Codigo=IF(en.Tipo=0,en.ECodiPais' +
        ',en.PCodiPais)'
      'WHERE en.Codigo=:P0'
      ''
      '')
    Left = 32
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmpresaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmpresaTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEmpresaCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrEmpresaCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrEmpresaIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEmpresaRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrEmpresaCNAE: TWideStringField
      FieldName = 'CNAE'
      Size = 7
    end
    object QrEmpresaNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
    object QrEmpresaFANTASIA: TWideStringField
      FieldName = 'FANTASIA'
      Required = True
      Size = 60
    end
    object QrEmpresaRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrEmpresaCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEmpresaBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEmpresaTe1: TWideStringField
      FieldName = 'Te1'
    end
    object QrEmpresaNO_LOGRAD: TWideStringField
      FieldName = 'NO_LOGRAD'
      Size = 10
    end
    object QrEmpresaNO_Munici: TWideStringField
      FieldName = 'NO_Munici'
      Size = 100
    end
    object QrEmpresaNO_UF: TWideStringField
      FieldName = 'NO_UF'
      Required = True
      Size = 2
    end
    object QrEmpresaDTB_UF: TWideStringField
      FieldName = 'DTB_UF'
      Required = True
      Size = 2
    end
    object QrEmpresaNO_Pais: TWideStringField
      FieldName = 'NO_Pais'
      Size = 100
    end
    object QrEmpresaIEST: TWideStringField
      FieldName = 'IEST'
    end
    object QrEmpresaSUFRAMA: TWideStringField
      FieldName = 'SUFRAMA'
      Size = 9
    end
    object QrEmpresaFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrEmpresaNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
    object QrEmpresaNumero: TFloatField
      FieldName = 'Numero'
    end
    object QrEmpresaCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrEmpresaCodiPais: TFloatField
      FieldName = 'CodiPais'
    end
    object QrEmpresaCodMunici: TFloatField
      FieldName = 'CodMunici'
    end
    object QrEmpresaUF: TFloatField
      FieldName = 'UF'
    end
  end
  object QrFilial: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM paramsemp'
      'WHERE Codigo=:P0')
    Left = 32
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFilialMDFeversao: TFloatField
      FieldName = 'MDFeversao'
    end
    object QrFilialMDFeide_mod: TSmallintField
      FieldName = 'MDFeide_mod'
    end
    object QrFilialSimplesFed: TSmallintField
      FieldName = 'SimplesFed'
    end
    object QrFilialDirMDFeGer: TWideStringField
      FieldName = 'DirMDFeGer'
      Size = 255
    end
    object QrFilialDirMDFeAss: TWideStringField
      FieldName = 'DirMDFeAss'
      Size = 255
    end
    object QrFilialDirMDFeEnvLot: TWideStringField
      FieldName = 'DirMDFeEnvLot'
      Size = 255
    end
    object QrFilialDirMDFeRec: TWideStringField
      FieldName = 'DirMDFeRec'
      Size = 255
    end
    object QrFilialDirMDFePedRec: TWideStringField
      FieldName = 'DirMDFePedRec'
      Size = 255
    end
    object QrFilialDirMDFeProRec: TWideStringField
      FieldName = 'DirMDFeProRec'
      Size = 255
    end
    object QrFilialDirMDFeDen: TWideStringField
      FieldName = 'DirMDFeDen'
      Size = 255
    end
    object QrFilialDirMDFePedCan: TWideStringField
      FieldName = 'DirMDFePedCan'
      Size = 255
    end
    object QrFilialDirMDFeCan: TWideStringField
      FieldName = 'DirMDFeCan'
      Size = 255
    end
    object QrFilialDirMDFePedInu: TWideStringField
      FieldName = 'DirMDFePedInu'
      Size = 255
    end
    object QrFilialDirMDFeInu: TWideStringField
      FieldName = 'DirMDFeInu'
      Size = 255
    end
    object QrFilialDirMDFePedSit: TWideStringField
      FieldName = 'DirMDFePedSit'
      Size = 255
    end
    object QrFilialDirMDFeSit: TWideStringField
      FieldName = 'DirMDFeSit'
      Size = 255
    end
    object QrFilialDirMDFePedSta: TWideStringField
      FieldName = 'DirMDFePedSta'
      Size = 255
    end
    object QrFilialDirMDFeSta: TWideStringField
      FieldName = 'DirMDFeSta'
      Size = 255
    end
    object QrFilialMDFeUF_WebServ: TWideStringField
      FieldName = 'MDFeUF_WebServ'
      Size = 2
    end
    object QrFilialMDFeUF_Servico: TWideStringField
      FieldName = 'MDFeUF_Servico'
      Size = 10
    end
    object QrFilialPathLogoMDFe: TWideStringField
      FieldName = 'PathLogoMDFe'
      Size = 255
    end
    object QrFilialCtaFretPrest: TIntegerField
      FieldName = 'CtaFretPrest'
    end
    object QrFilialTxtFretPrest: TWideStringField
      FieldName = 'TxtFretPrest'
      Size = 100
    end
    object QrFilialDupFretPrest: TWideStringField
      FieldName = 'DupFretPrest'
      Size = 3
    end
    object QrFilialMDFeSerNum: TWideStringField
      FieldName = 'MDFeSerNum'
      Size = 255
    end
    object QrFilialMDFeSerVal: TDateField
      FieldName = 'MDFeSerVal'
    end
    object QrFilialMDFeSerAvi: TSmallintField
      FieldName = 'MDFeSerAvi'
    end
    object QrFilialDirDAMDFes: TWideStringField
      FieldName = 'DirDAMDFes'
      Size = 255
    end
    object QrFilialDirMDFeProt: TWideStringField
      FieldName = 'DirMDFeProt'
      Size = 255
    end
    object QrFilialMDFePreMailAut: TIntegerField
      FieldName = 'MDFePreMailAut'
    end
    object QrFilialMDFePreMailCan: TIntegerField
      FieldName = 'MDFePreMailCan'
    end
    object QrFilialMDFePreMailEveCCe: TIntegerField
      FieldName = 'MDFePreMailEveCCe'
    end
    object QrFilialMDFeVerStaSer: TFloatField
      FieldName = 'MDFeVerStaSer'
    end
    object QrFilialMDFeVerEnvLot: TFloatField
      FieldName = 'MDFeVerEnvLot'
    end
    object QrFilialMDFeVerConLot: TFloatField
      FieldName = 'MDFeVerConLot'
    end
    object QrFilialMDFeVerCanMDFe: TFloatField
      FieldName = 'MDFeVerCanMDFe'
    end
    object QrFilialMDFeVerInuNum: TFloatField
      FieldName = 'MDFeVerInuNum'
    end
    object QrFilialMDFeVerConMDFe: TFloatField
      FieldName = 'MDFeVerConMDFe'
    end
    object QrFilialMDFeVerLotEve: TFloatField
      FieldName = 'MDFeVerLotEve'
    end
    object QrFilialMDFeide_tpImp: TSmallintField
      FieldName = 'MDFeide_tpImp'
    end
    object QrFilialMDFeide_tpAmb: TSmallintField
      FieldName = 'MDFeide_tpAmb'
    end
    object QrFilialMDFeAppCode: TSmallintField
      FieldName = 'MDFeAppCode'
    end
    object QrFilialMDFeAssDigMode: TSmallintField
      FieldName = 'MDFeAssDigMode'
    end
    object QrFilialMDFeEntiTipCto: TIntegerField
      FieldName = 'MDFeEntiTipCto'
    end
    object QrFilialMDFeEntiTipCt1: TIntegerField
      FieldName = 'MDFeEntiTipCt1'
    end
    object QrFilialMyEmailMDFe: TWideStringField
      FieldName = 'MyEmailMDFe'
      Size = 255
    end
    object QrFilialDirMDFeSchema: TWideStringField
      FieldName = 'DirMDFeSchema'
      Size = 255
    end
    object QrFilialDirMDFeRWeb: TWideStringField
      FieldName = 'DirMDFeRWeb'
      Size = 255
    end
    object QrFilialDirMDFeEveEnvLot: TWideStringField
      FieldName = 'DirMDFeEveEnvLot'
      Size = 255
    end
    object QrFilialDirMDFeEveRetLot: TWideStringField
      FieldName = 'DirMDFeEveRetLot'
      Size = 255
    end
    object QrFilialDirMDFeEvePedCan: TWideStringField
      FieldName = 'DirMDFeEvePedCan'
      Size = 255
    end
    object QrFilialDirMDFeEveRetCan: TWideStringField
      FieldName = 'DirMDFeEveRetCan'
      Size = 255
    end
    object QrFilialDirMDFeRetMDFeDes: TWideStringField
      FieldName = 'DirMDFeRetMDFeDes'
      Size = 255
    end
    object QrFilialMDFeMDFeVerConDes: TFloatField
      FieldName = 'MDFeMDFeVerConDes'
    end
    object QrFilialDirMDFeEvePedMDe: TWideStringField
      FieldName = 'DirMDFeEvePedMDe'
      Size = 255
    end
    object QrFilialDirMDFeEveRetMDe: TWideStringField
      FieldName = 'DirMDFeEveRetMDe'
      Size = 255
    end
    object QrFilialDirDowMDFeDes: TWideStringField
      FieldName = 'DirDowMDFeDes'
      Size = 255
    end
    object QrFilialDirDowMDFeMDFe: TWideStringField
      FieldName = 'DirDowMDFeMDFe'
      Size = 255
    end
    object QrFilialMDFeInfCpl: TIntegerField
      FieldName = 'MDFeInfCpl'
    end
    object QrFilialMDFeVerConsCad: TFloatField
      FieldName = 'MDFeVerConsCad'
    end
    object QrFilialMDFeVerDistDFeInt: TFloatField
      FieldName = 'MDFeVerDistDFeInt'
    end
    object QrFilialMDFeVerDowMDFe: TFloatField
      FieldName = 'MDFeVerDowMDFe'
    end
    object QrFilialDirDowMDFeCnf: TWideStringField
      FieldName = 'DirDowMDFeCnf'
      Size = 255
    end
    object QrFilialMDFeItsLin: TSmallintField
      FieldName = 'MDFeItsLin'
    end
    object QrFilialMDFeFTRazao: TSmallintField
      FieldName = 'MDFeFTRazao'
    end
    object QrFilialMDFeFTEnder: TSmallintField
      FieldName = 'MDFeFTEnder'
    end
    object QrFilialMDFeFTFones: TSmallintField
      FieldName = 'MDFeFTFones'
    end
    object QrFilialMDFeMaiusc: TSmallintField
      FieldName = 'MDFeMaiusc'
    end
    object QrFilialMDFeShowURL: TWideStringField
      FieldName = 'MDFeShowURL'
      Size = 255
    end
    object QrFilialMDFetpEmis: TSmallintField
      FieldName = 'MDFetpEmis'
    end
    object QrFilialMDFeSCAN_Ser: TIntegerField
      FieldName = 'MDFeSCAN_Ser'
    end
    object QrFilialMDFeSCAN_nMDF: TIntegerField
      FieldName = 'MDFeSCAN_nMDF'
    end
    object QrFilialMDFe_indFinalCpl: TSmallintField
      FieldName = 'MDFe_indFinalCpl'
    end
    object QrFilialTZD_UTC_Auto_TermoAceite: TIntegerField
      FieldName = 'TZD_UTC_Auto_TermoAceite'
    end
    object QrFilialTZD_UTC_Auto_DataHoraAceite: TDateTimeField
      FieldName = 'TZD_UTC_Auto_DataHoraAceite'
    end
    object QrFilialTZD_UTC_Auto_UsuarioAceite: TIntegerField
      FieldName = 'TZD_UTC_Auto_UsuarioAceite'
    end
    object QrFilialTZD_UTC_Auto: TSmallintField
      FieldName = 'TZD_UTC_Auto'
    end
    object QrFilialTZD_UTC: TFloatField
      FieldName = 'TZD_UTC'
    end
    object QrFilialhVeraoAsk: TDateField
      FieldName = 'hVeraoAsk'
    end
    object QrFilialhVeraoIni: TDateField
      FieldName = 'hVeraoIni'
    end
    object QrFilialhVeraoFim: TDateField
      FieldName = 'hVeraoFim'
    end
    object QrFilialParamsMDFe: TIntegerField
      FieldName = 'ParamsMDFe'
    end
    object QrFilialDirMDFeEvePedEnc: TWideStringField
      FieldName = 'DirMDFeEvePedEnc'
      Size = 255
    end
    object QrFilialDirMDFeEveRetEnc: TWideStringField
      FieldName = 'DirMDFeEveRetEnc'
      Size = 255
    end
    object QrFilialDirMDFeEvePedIdC: TWideStringField
      FieldName = 'DirMDFeEvePedIdC'
      Size = 255
    end
    object QrFilialDirMDFeEveRetIdC: TWideStringField
      FieldName = 'DirMDFeEveRetIdC'
      Size = 255
    end
  end
  object QrMDFeCabA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mdfecaba'
      'WHERE FatID=:p0')
    Left = 276
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p0'
        ParamType = ptUnknown
      end>
    object QrMDFeCabAFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeCabAFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeCabAEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrMDFeCabALoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
    object QrMDFeCabAversao: TFloatField
      FieldName = 'versao'
    end
    object QrMDFeCabAId: TWideStringField
      FieldName = 'Id'
      Size = 47
    end
    object QrMDFeCabAide_cUF: TSmallintField
      FieldName = 'ide_cUF'
    end
    object QrMDFeCabAide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
    end
    object QrMDFeCabAide_tpEmit: TSmallintField
      FieldName = 'ide_tpEmit'
    end
    object QrMDFeCabAide_mod: TSmallintField
      FieldName = 'ide_mod'
    end
    object QrMDFeCabAide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrMDFeCabAide_nMDF: TIntegerField
      FieldName = 'ide_nMDF'
    end
    object QrMDFeCabAide_cMDF: TIntegerField
      FieldName = 'ide_cMDF'
    end
    object QrMDFeCabAide_cDV: TSmallintField
      FieldName = 'ide_cDV'
    end
    object QrMDFeCabAide_Modal: TSmallintField
      FieldName = 'ide_Modal'
    end
    object QrMDFeCabAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrMDFeCabAide_hEmi: TTimeField
      FieldName = 'ide_hEmi'
    end
    object QrMDFeCabAide_dhEmiTZD: TFloatField
      FieldName = 'ide_dhEmiTZD'
    end
    object QrMDFeCabAide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
    end
    object QrMDFeCabAide_procEmi: TSmallintField
      FieldName = 'ide_procEmi'
    end
    object QrMDFeCabAide_verProc: TWideStringField
      FieldName = 'ide_verProc'
    end
    object QrMDFeCabAide_UFIni: TWideStringField
      FieldName = 'ide_UFIni'
      Size = 2
    end
    object QrMDFeCabAide_UFFim: TWideStringField
      FieldName = 'ide_UFFim'
      Size = 2
    end
    object QrMDFeCabAide_dIniViagem: TDateField
      FieldName = 'ide_dIniViagem'
    end
    object QrMDFeCabAide_hIniViagem: TTimeField
      FieldName = 'ide_hIniViagem'
    end
    object QrMDFeCabAide_dhIniViagemTZD: TFloatField
      FieldName = 'ide_dhIniViagemTZD'
    end
    object QrMDFeCabAemit_CNPJ: TWideStringField
      FieldName = 'emit_CNPJ'
      Size = 14
    end
    object QrMDFeCabAemit_IE: TWideStringField
      FieldName = 'emit_IE'
      Size = 14
    end
    object QrMDFeCabAemit_xNome: TWideStringField
      FieldName = 'emit_xNome'
      Size = 60
    end
    object QrMDFeCabAemit_xFant: TWideStringField
      FieldName = 'emit_xFant'
      Size = 60
    end
    object QrMDFeCabAemit_xLgr: TWideStringField
      FieldName = 'emit_xLgr'
      Size = 60
    end
    object QrMDFeCabAemit_nro: TWideStringField
      FieldName = 'emit_nro'
      Size = 60
    end
    object QrMDFeCabAemit_xCpl: TWideStringField
      FieldName = 'emit_xCpl'
      Size = 60
    end
    object QrMDFeCabAemit_xBairro: TWideStringField
      FieldName = 'emit_xBairro'
      Size = 60
    end
    object QrMDFeCabAemit_cMun: TIntegerField
      FieldName = 'emit_cMun'
    end
    object QrMDFeCabAemit_xMun: TWideStringField
      FieldName = 'emit_xMun'
      Size = 60
    end
    object QrMDFeCabAemit_CEP: TIntegerField
      FieldName = 'emit_CEP'
    end
    object QrMDFeCabAemit_UF: TWideStringField
      FieldName = 'emit_UF'
      Size = 2
    end
    object QrMDFeCabAemit_fone: TWideStringField
      FieldName = 'emit_fone'
      Size = 14
    end
    object QrMDFeCabAemit_email: TWideStringField
      FieldName = 'emit_email'
      Size = 60
    end
    object QrMDFeCabAversaoModal: TFloatField
      FieldName = 'versaoModal'
    end
    object QrMDFeCabATot_qCTe: TIntegerField
      FieldName = 'Tot_qCTe'
    end
    object QrMDFeCabATot_qNFe: TIntegerField
      FieldName = 'Tot_qNFe'
    end
    object QrMDFeCabATot_qMDFe: TIntegerField
      FieldName = 'Tot_qMDFe'
    end
    object QrMDFeCabATot_vCarga: TFloatField
      FieldName = 'Tot_vCarga'
    end
    object QrMDFeCabATot_cUnid: TSmallintField
      FieldName = 'Tot_cUnid'
    end
    object QrMDFeCabATot_qCarga: TFloatField
      FieldName = 'Tot_qCarga'
    end
    object QrMDFeCabAinfAdFisco: TWideMemoField
      FieldName = 'infAdFisco'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrMDFeCabAinfCpl: TWideMemoField
      FieldName = 'infCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrMDFeCabAStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrMDFeCabAprotMDFe_versao: TFloatField
      FieldName = 'protMDFe_versao'
    end
    object QrMDFeCabAinfProt_Id: TWideStringField
      FieldName = 'infProt_Id'
      Size = 30
    end
    object QrMDFeCabAinfProt_tpAmb: TSmallintField
      FieldName = 'infProt_tpAmb'
    end
    object QrMDFeCabAinfProt_verAplic: TWideStringField
      FieldName = 'infProt_verAplic'
      Size = 30
    end
    object QrMDFeCabAinfProt_dhRecbto: TDateTimeField
      FieldName = 'infProt_dhRecbto'
    end
    object QrMDFeCabAinfProt_dhRecbtoTZD: TFloatField
      FieldName = 'infProt_dhRecbtoTZD'
    end
    object QrMDFeCabAinfProt_nProt: TWideStringField
      FieldName = 'infProt_nProt'
      Size = 15
    end
    object QrMDFeCabAinfProt_digVal: TWideStringField
      FieldName = 'infProt_digVal'
      Size = 28
    end
    object QrMDFeCabAinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrMDFeCabAinfProt_xMotivo: TWideStringField
      FieldName = 'infProt_xMotivo'
      Size = 255
    end
    object QrMDFeCabAretCancMDFe_versao: TFloatField
      FieldName = 'retCancMDFe_versao'
    end
    object QrMDFeCabAinfCanc_Id: TWideStringField
      FieldName = 'infCanc_Id'
      Size = 54
    end
    object QrMDFeCabAinfCanc_tpAmb: TSmallintField
      FieldName = 'infCanc_tpAmb'
    end
    object QrMDFeCabAinfCanc_verAplic: TWideStringField
      FieldName = 'infCanc_verAplic'
      Size = 30
    end
    object QrMDFeCabAinfCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
    end
    object QrMDFeCabAinfCanc_dhRecbtoTZD: TFloatField
      FieldName = 'infCanc_dhRecbtoTZD'
    end
    object QrMDFeCabAinfCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Size = 15
    end
    object QrMDFeCabAinfCanc_digVal: TWideStringField
      FieldName = 'infCanc_digVal'
      Size = 28
    end
    object QrMDFeCabAinfCanc_cStat: TIntegerField
      FieldName = 'infCanc_cStat'
    end
    object QrMDFeCabAinfCanc_xMotivo: TWideStringField
      FieldName = 'infCanc_xMotivo'
      Size = 255
    end
    object QrMDFeCabAinfCanc_cJust: TIntegerField
      FieldName = 'infCanc_cJust'
    end
    object QrMDFeCabAinfCanc_xJust: TWideStringField
      FieldName = 'infCanc_xJust'
      Size = 255
    end
    object QrMDFeCabAFisRegCad: TIntegerField
      FieldName = 'FisRegCad'
    end
    object QrMDFeCabACartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
    object QrMDFeCabATabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
    end
    object QrMDFeCabACondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrMDFeCabACodInfoEmite: TIntegerField
      FieldName = 'CodInfoEmite'
    end
    object QrMDFeCabACriAForca: TSmallintField
      FieldName = 'CriAForca'
    end
    object QrMDFeCabAcSitMDFe: TSmallintField
      FieldName = 'cSitMDFe'
    end
    object QrMDFeCabAcSitEnc: TSmallintField
      FieldName = 'cSitEnc'
    end
    object QrMDFeCabAinfEnc_verAplic: TWideStringField
      FieldName = 'infEnc_verAplic'
      Size = 30
    end
    object QrMDFeCabAinfEnc_cOrgao: TSmallintField
      FieldName = 'infEnc_cOrgao'
    end
    object QrMDFeCabAinfEnc_tpAmb: TSmallintField
      FieldName = 'infEnc_tpAmb'
    end
    object QrMDFeCabAinfEnc_CNPJ: TWideStringField
      FieldName = 'infEnc_CNPJ'
      Size = 18
    end
    object QrMDFeCabAinfEnc_chMDFe: TWideStringField
      FieldName = 'infEnc_chMDFe'
      Size = 44
    end
    object QrMDFeCabAinfEnc_tpEvento: TIntegerField
      FieldName = 'infEnc_tpEvento'
    end
    object QrMDFeCabAinfEnc_nSeqEvento: TIntegerField
      FieldName = 'infEnc_nSeqEvento'
    end
    object QrMDFeCabAinfEnc_verEvento: TFloatField
      FieldName = 'infEnc_verEvento'
    end
    object QrMDFeCabAinfEnc_dtEnc: TDateField
      FieldName = 'infEnc_dtEnc'
    end
    object QrMDFeCabAinfEnc_cUF: TSmallintField
      FieldName = 'infEnc_cUF'
    end
    object QrMDFeCabAinfEnc_cMun: TIntegerField
      FieldName = 'infEnc_cMun'
    end
    object QrMDFeCabAinfEnc_cStat: TIntegerField
      FieldName = 'infEnc_cStat'
    end
    object QrMDFeCabAinfEnc_dhRegEvento: TDateTimeField
      FieldName = 'infEnc_dhRegEvento'
    end
    object QrMDFeCabAinfEnc_nProt: TWideStringField
      FieldName = 'infEnc_nProt'
      Size = 15
    end
    object QrMDFeCabAinfEnc_Id: TWideStringField
      FieldName = 'infEnc_Id'
      Size = 17
    end
  end
  object QrMDFeLayI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT layc.Nome NO_Grupo, layi.* '
      'FROM mdfelayi layi '
      'LEFT JOIN mdfelayc layc ON layc.Grupo=layi.Grupo '
      'WHERE layi.Versao>0')
    Left = 128
    Top = 4
    object QrMDFeLayINO_Grupo: TWideStringField
      FieldName = 'NO_Grupo'
      Size = 100
    end
    object QrMDFeLayIVersao: TFloatField
      FieldName = 'Versao'
    end
    object QrMDFeLayIGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrMDFeLayICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMDFeLayICampo: TWideStringField
      FieldName = 'Campo'
      Size = 30
    end
    object QrMDFeLayINivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrMDFeLayIDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 255
    end
    object QrMDFeLayIElemento: TWideStringField
      FieldName = 'Elemento'
      Size = 10
    end
    object QrMDFeLayITipo: TWideStringField
      FieldName = 'Tipo'
      Size = 1
    end
    object QrMDFeLayIOcorMin: TSmallintField
      FieldName = 'OcorMin'
    end
    object QrMDFeLayIOcorMax: TIntegerField
      FieldName = 'OcorMax'
    end
    object QrMDFeLayITamMin: TSmallintField
      FieldName = 'TamMin'
    end
    object QrMDFeLayITamMax: TIntegerField
      FieldName = 'TamMax'
    end
    object QrMDFeLayITamVar: TWideStringField
      FieldName = 'TamVar'
      Size = 30
    end
    object QrMDFeLayIObservacao: TWideMemoField
      FieldName = 'Observacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrMDFeLayIInfoVazio: TSmallintField
      FieldName = 'InfoVazio'
    end
    object QrMDFeLayIDominio: TWideStringField
      FieldName = 'Dominio'
      Size = 10
    end
    object QrMDFeLayIExpReg: TWideStringField
      FieldName = 'ExpReg'
      Size = 10
    end
    object QrMDFeLayIFormatStr: TWideStringField
      FieldName = 'FormatStr'
      Size = 30
    end
    object QrMDFeLayIDeciCasas: TSmallintField
      FieldName = 'DeciCasas'
    end
    object QrMDFeLayICartaCorrige: TSmallintField
      FieldName = 'CartaCorrige'
    end
    object QrMDFeLayIAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrMDFeLayIAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrFrtMnfCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtmnfcab'
      'WHERE Codigo=:P0')
    Left = 128
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtMnfCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrFrtMnfCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrFrtMnfCabAbertura: TDateTimeField
      FieldName = 'Abertura'
    end
    object QrFrtMnfCabEncerrou: TDateTimeField
      FieldName = 'Encerrou'
    end
    object QrFrtMnfCabDataViagIniPrv: TDateTimeField
      FieldName = 'DataViagIniPrv'
    end
    object QrFrtMnfCabDataViagIniSai: TDateTimeField
      FieldName = 'DataViagIniSai'
    end
    object QrFrtMnfCabDataViagFimPrv: TDateTimeField
      FieldName = 'DataViagFimPrv'
    end
    object QrFrtMnfCabDataViagFimChg: TDateTimeField
      FieldName = 'DataViagFimChg'
    end
    object QrFrtMnfCabFrtRegMnf: TIntegerField
      FieldName = 'FrtRegMnf'
    end
    object QrFrtMnfCabTrnsIts: TIntegerField
      FieldName = 'TrnsIts'
    end
    object QrFrtMnfCabTpEmit: TSmallintField
      FieldName = 'TpEmit'
    end
    object QrFrtMnfCabModal: TSmallintField
      FieldName = 'Modal'
    end
    object QrFrtMnfCabTpEmis: TSmallintField
      FieldName = 'TpEmis'
    end
    object QrFrtMnfCabFrtRegPth: TIntegerField
      FieldName = 'FrtRegPth'
    end
    object QrFrtMnfCabUFIni: TWideStringField
      FieldName = 'UFIni'
      Size = 2
    end
    object QrFrtMnfCabUFFim: TWideStringField
      FieldName = 'UFFim'
      Size = 2
    end
    object QrFrtMnfCabParamsMDFe: TIntegerField
      FieldName = 'ParamsMDFe'
    end
    object QrFrtMnfCabTot_qCTe: TIntegerField
      FieldName = 'Tot_qCTe'
    end
    object QrFrtMnfCabTot_qNFe: TIntegerField
      FieldName = 'Tot_qNFe'
    end
    object QrFrtMnfCabTot_qMDFe: TIntegerField
      FieldName = 'Tot_qMDFe'
    end
    object QrFrtMnfCabTot_vCarga: TFloatField
      FieldName = 'Tot_vCarga'
    end
    object QrFrtMnfCabTot_cUnid: TSmallintField
      FieldName = 'Tot_cUnid'
    end
    object QrFrtMnfCabTot_qCarga: TFloatField
      FieldName = 'Tot_qCarga'
    end
    object QrFrtMnfCabinfAdFisco: TWideMemoField
      FieldName = 'infAdFisco'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFrtMnfCabinfCpl: TWideMemoField
      FieldName = 'infCpl'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrFrtMnfCabSerieDesfe: TIntegerField
      FieldName = 'SerieDesfe'
    end
    object QrFrtMnfCabMDFDesfeita: TIntegerField
      FieldName = 'MDFDesfeita'
    end
    object QrFrtMnfCabCodAgPorto: TWideStringField
      FieldName = 'CodAgPorto'
      Size = 16
    end
  end
  object QrEntiMDFe: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Tipo,'
      'IF(ent.Tipo=0, ent.CNPJ, "") CNPJ,     '
      'IF(ent.Tipo=1, ent.CPF, "") CPF,     '
      'IF(ent.Tipo=0, ent.IE, "") IE,     '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) xNome,     '
      'IF(ent.Tipo=0, ent.Fantasia, ent.Apelido) xFant,     '
      'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) Fone, '
      'IF(ent.Tipo=0, ent.ERua, ent.PRua) xLgr, '
      
        'IF(IF(ent.Tipo=0, ent.ENumero, ent.PNumero) <>0, IF(ent.Tipo=0, ' +
        'ent.ENumero, ent.PNumero), "S/N") Nro, '
      'IF(ent.Tipo=0, ent.ECompl,   ent.PCompl) xCpl, '
      'IF(ent.Tipo=0, ent.EBairro,   ent.PBairro) xBairro, '
      'IF(ent.Tipo=0, ent.ECodMunici, ent.PCodMunici) + 0.000 CMun, '
      'mun.Nome xMun, '
      'IF(ent.Tipo=0,  ent.ECEP, ent.PCEP) + 0.000 CEP, '
      'ufs.Nome UF,'
      'IF(ent.Tipo=0, ent.ECodiPais, ent.PCodiPais) + 0.000 CPais,'
      'bpa.Nome xPais, '
      'IF(ent.Tipo=0, ent.EEmail, ent.PEmail) Email'
      'FROM entidades ent '
      
        'LEFT JOIN loclogiall.dtb_munici mun ON mun.Codigo=IF(ent.Tipo=0,' +
        ' ent.ECodMunici, ent.PCodMunici) '
      
        'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF)' +
        ' '
      
        'LEFT JOIN loclogiall.bacen_pais bpa ON bpa.Codigo=IF(ent.Tipo=0,' +
        ' ent.ECodiPais, ent.PCodiPais) '
      'WHERE ent.Codigo=:P0')
    Left = 32
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiMDFeCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrEntiMDFeCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrEntiMDFeIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEntiMDFexNome: TWideStringField
      FieldName = 'xNome'
      Size = 100
    end
    object QrEntiMDFexFant: TWideStringField
      FieldName = 'xFant'
      Size = 60
    end
    object QrEntiMDFefone: TWideStringField
      FieldName = 'fone'
    end
    object QrEntiMDFeTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEntiMDFexLgr: TWideStringField
      FieldName = 'xLgr'
      Size = 60
    end
    object QrEntiMDFexCpl: TWideStringField
      FieldName = 'xCpl'
      Size = 60
    end
    object QrEntiMDFexBairro: TWideStringField
      FieldName = 'xBairro'
      Size = 60
    end
    object QrEntiMDFeCMun: TFloatField
      FieldName = 'CMun'
      Required = True
    end
    object QrEntiMDFexMun: TWideStringField
      FieldName = 'xMun'
      Size = 100
    end
    object QrEntiMDFeCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrEntiMDFeUF: TWideStringField
      FieldName = 'UF'
      Required = True
      Size = 2
    end
    object QrEntiMDFeCPais: TFloatField
      FieldName = 'CPais'
      Required = True
    end
    object QrEntiMDFexPais: TWideStringField
      FieldName = 'xPais'
      Size = 100
    end
    object QrEntiMDFeEmail: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
    object QrEntiMDFeNro: TWideStringField
      FieldName = 'Nro'
      Size = 60
    end
    object QrEntiMDFeSUFRAMA: TWideStringField
      FieldName = 'SUFRAMA'
      Size = 9
    end
  end
  object QrFrtMnfMunCarrega: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtmnfmuncarrega'
      'WHERE Codigo=:P0')
    Left = 128
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtMnfMunCarregaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfMunCarregaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtMnfMunCarregacMunCarrega: TIntegerField
      FieldName = 'cMunCarrega'
    end
    object QrFrtMnfMunCarregaxMunCarrega: TWideStringField
      FieldName = 'xMunCarrega'
      Size = 60
    end
  end
  object QrFrtMnfInfPercurso: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtmnfinfpercurso'
      'WHERE Codigo=:P0')
    Left = 128
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtMnfInfPercursoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfInfPercursoControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtMnfInfPercursoUFPer: TWideStringField
      FieldName = 'UFPer'
      Size = 2
    end
  end
  object QrFrtMnfLacres: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtmnflacres'
      'WHERE Codigo=:P0')
    Left = 128
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtMnfLacresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfLacresControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtMnfLacresnLacre: TWideStringField
      FieldName = 'nLacre'
      Size = 60
    end
    object QrFrtMnfLacresLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtMnfLacresDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtMnfLacresDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtMnfLacresUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtMnfLacresUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtMnfLacresAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtMnfLacresAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrFrtMnfAutXML: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM frtmnfautxml'
      'WHERE Codigo=:P0')
    Left = 128
    Top = 484
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtMnfAutXMLCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfAutXMLControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtMnfAutXMLAddForma: TSmallintField
      FieldName = 'AddForma'
    end
    object QrFrtMnfAutXMLAddIDCad: TIntegerField
      FieldName = 'AddIDCad'
    end
    object QrFrtMnfAutXMLTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrFrtMnfAutXMLCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrFrtMnfAutXMLCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrFrtMnfAutXMLLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtMnfAutXMLDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtMnfAutXMLDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtMnfAutXMLUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtMnfAutXMLUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtMnfAutXMLAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtMnfAutXMLAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrFrtMnfInfDocCMun: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrFrtMnfInfDocCMunAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT Codigo, cMundescarga'
      'FROM frtmnfinfdoc'
      'WHERE Codigo=:P0'
      '')
    Left = 128
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFrtMnfInfDocCMunCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfInfDocCMuncMundescarga: TIntegerField
      FieldName = 'cMundescarga'
    end
  end
  object QrFrtMnfInfDocXMun: TmySQLQuery
    Database = Dmod.MyDB
    Left = 128
    Top = 244
    object QrFrtMnfInfDocXMunxMundescarga: TWideStringField
      FieldName = 'xMundescarga'
      Size = 60
    end
    object QrFrtMnfInfDocXMunControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrFrtMnfInfDoc55: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM frtmnfinfdoc '
      'WHERE Codigo<0'
      'AND cMunDescarga<0'
      'AND DocMod=0'
      'ORDER BY Controle'
      ' ')
    Left = 128
    Top = 292
    object QrFrtMnfInfDoc55Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfInfDoc55Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtMnfInfDoc55cMunDescarga: TIntegerField
      FieldName = 'cMunDescarga'
    end
    object QrFrtMnfInfDoc55xMunDescarga: TWideStringField
      FieldName = 'xMunDescarga'
      Size = 60
    end
    object QrFrtMnfInfDoc55DocMod: TSmallintField
      FieldName = 'DocMod'
    end
    object QrFrtMnfInfDoc55DocSerie: TIntegerField
      FieldName = 'DocSerie'
    end
    object QrFrtMnfInfDoc55DocNumero: TIntegerField
      FieldName = 'DocNumero'
    end
    object QrFrtMnfInfDoc55DocChave: TWideStringField
      FieldName = 'DocChave'
      Size = 44
    end
    object QrFrtMnfInfDoc55SegCodBarra: TWideStringField
      FieldName = 'SegCodBarra'
      Size = 36
    end
    object QrFrtMnfInfDoc55TrnsIts: TIntegerField
      FieldName = 'TrnsIts'
    end
  end
  object QrFrtMnfInfDoc57: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM frtmnfinfdoc '
      'WHERE Codigo<0'
      'AND cMunDescarga<0'
      'AND DocMod=0'
      'ORDER BY Controle'
      ' ')
    Left = 128
    Top = 340
    object QrFrtMnfInfDoc57Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfInfDoc57Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtMnfInfDoc57cMunDescarga: TIntegerField
      FieldName = 'cMunDescarga'
    end
    object QrFrtMnfInfDoc57xMunDescarga: TWideStringField
      FieldName = 'xMunDescarga'
      Size = 60
    end
    object QrFrtMnfInfDoc57DocMod: TSmallintField
      FieldName = 'DocMod'
    end
    object QrFrtMnfInfDoc57DocSerie: TIntegerField
      FieldName = 'DocSerie'
    end
    object QrFrtMnfInfDoc57DocNumero: TIntegerField
      FieldName = 'DocNumero'
    end
    object QrFrtMnfInfDoc57DocChave: TWideStringField
      FieldName = 'DocChave'
      Size = 44
    end
    object QrFrtMnfInfDoc57SegCodBarra: TWideStringField
      FieldName = 'SegCodBarra'
      Size = 36
    end
    object QrFrtMnfInfDoc57TrnsIts: TIntegerField
      FieldName = 'TrnsIts'
    end
  end
  object QrFrtMnfInfDoc58: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *  '
      'FROM frtmnfinfdoc '
      'WHERE Codigo<0'
      'AND cMunDescarga<0'
      'AND DocMod=0'
      'ORDER BY Controle'
      ' ')
    Left = 128
    Top = 388
    object QrFrtMnfInfDoc58Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfInfDoc58Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtMnfInfDoc58cMunDescarga: TIntegerField
      FieldName = 'cMunDescarga'
    end
    object QrFrtMnfInfDoc58xMunDescarga: TWideStringField
      FieldName = 'xMunDescarga'
      Size = 60
    end
    object QrFrtMnfInfDoc58DocMod: TSmallintField
      FieldName = 'DocMod'
    end
    object QrFrtMnfInfDoc58DocSerie: TIntegerField
      FieldName = 'DocSerie'
    end
    object QrFrtMnfInfDoc58DocNumero: TIntegerField
      FieldName = 'DocNumero'
    end
    object QrFrtMnfInfDoc58DocChave: TWideStringField
      FieldName = 'DocChave'
      Size = 44
    end
    object QrFrtMnfInfDoc58SegCodBarra: TWideStringField
      FieldName = 'SegCodBarra'
      Size = 36
    end
    object QrFrtMnfInfDoc58TrnsIts: TIntegerField
      FieldName = 'TrnsIts'
    end
  end
  object QrTrnsIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cad.RNTRC, cad.tpProp, its.*'
      'FROM trnsits its'
      'LEFT JOIN trnscad cad ON cad.Codigo=its.Codigo'
      'WHERE its.Controle=:P0')
    Left = 32
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTrnsItsRNTRC: TWideStringField
      FieldName = 'RNTRC'
      Size = 8
    end
    object QrTrnsItstpProp: TSmallintField
      FieldName = 'tpProp'
    end
    object QrTrnsItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTrnsItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTrnsItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrTrnsItsRENAVAM: TWideStringField
      FieldName = 'RENAVAM'
      Size = 11
    end
    object QrTrnsItsPlaca: TWideStringField
      FieldName = 'Placa'
      Size = 11
    end
    object QrTrnsItsTaraKg: TIntegerField
      FieldName = 'TaraKg'
    end
    object QrTrnsItsCapKg: TIntegerField
      FieldName = 'CapKg'
    end
    object QrTrnsItsTpVeic: TSmallintField
      FieldName = 'TpVeic'
    end
    object QrTrnsItsTpRod: TSmallintField
      FieldName = 'TpRod'
    end
    object QrTrnsItsTpCar: TSmallintField
      FieldName = 'TpCar'
    end
    object QrTrnsItsCapM3: TIntegerField
      FieldName = 'CapM3'
    end
    object QrTrnsItsUF: TWideStringField
      FieldName = 'UF'
      Required = True
      Size = 2
    end
  end
  object QrTerceiro: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tcb.RNTRC,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      'IF(ent.Tipo=0, "", ent.CPF) CPF,'
      'IF(ent.Tipo=0, ent.CNPJ, "") CNPJ,'
      'IF(ent.Tipo=0, ent.IE, "") IE,'
      'ufs.Nome UF'
      ''
      'FROM entidades ent'
      
        'LEFT JOIN ufs ufs ON ufs.Codigo=(IF(ent.Tipo=0, ent.EUF, ent.PUF' +
        '))'
      'LEFT JOIN trnscad tcb ON tcb.Codigo=ent.Codigo'
      'WHERE ent.Codigo=:P0')
    Left = 32
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerceiroRNTRC: TWideStringField
      FieldName = 'RNTRC'
      Size = 8
    end
    object QrTerceiroTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrTerceiroNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrTerceiroCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrTerceiroCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrTerceiroIE: TWideStringField
      FieldName = 'IE'
    end
    object QrTerceiroUF: TWideStringField
      FieldName = 'UF'
      Required = True
      Size = 2
    end
  end
  object QrTrnsCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM trnscad'
      'WHERE Codigo=:P0')
    Left = 32
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTrnsCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTrnsCadRNTRC: TWideStringField
      FieldName = 'RNTRC'
      Size = 8
    end
    object QrTrnsCadLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTrnsCadDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTrnsCadDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTrnsCadUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTrnsCadUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTrnsCadAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTrnsCadAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTrnsCadTpProp: TSmallintField
      FieldName = 'TpProp'
    end
  end
  object QrFrtMnfMoRodoCondutor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT rco.*'
      'FROM frtmnfmorodocondutor rco'
      'WHERE rco.Codigo=0')
    Left = 128
    Top = 532
    object QrFrtMnfMoRodoCondutorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfMoRodoCondutorControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtMnfMoRodoCondutorEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrFrtMnfMoRodoCondutorxNome: TWideStringField
      FieldName = 'xNome'
      Size = 60
    end
    object QrFrtMnfMoRodoCondutorCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrFrtMnfMoRodoCondutorLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtMnfMoRodoCondutorDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtMnfMoRodoCondutorDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtMnfMoRodoCondutorUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtMnfMoRodoCondutorUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtMnfMoRodoCondutorAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtMnfMoRodoCondutorAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrFrtMnfMoRodoVeicRebo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT rcv.*, its.Nome NO_VEIC, its.Placa'
      'FROM frtmnfmorodoveicrebo rcv'
      'LEFT JOIN trnsits its ON its.Controle=rcv.cInt'
      'WHERE rcv.Codigo>0')
    Left = 128
    Top = 580
    object QrFrtMnfMoRodoVeicReboCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfMoRodoVeicReboControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtMnfMoRodoVeicRebocInt: TIntegerField
      FieldName = 'cInt'
    end
    object QrFrtMnfMoRodoVeicReboLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtMnfMoRodoVeicReboDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtMnfMoRodoVeicReboDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtMnfMoRodoVeicReboUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtMnfMoRodoVeicReboUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtMnfMoRodoVeicReboAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtMnfMoRodoVeicReboAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFrtMnfMoRodoVeicReboNO_VEIC: TWideStringField
      FieldName = 'NO_VEIC'
      Size = 100
    end
    object QrFrtMnfMoRodoVeicReboPlaca: TWideStringField
      FieldName = 'Placa'
      Size = 11
    end
  end
  object QrFrtMnfMoRodoValePedg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT rcr.*'
      'FROM frtmnfmorodovalepedg rcr'
      'WHERE rcr.Codigo>0')
    Left = 128
    Top = 628
    object QrFrtMnfMoRodoValePedgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfMoRodoValePedgControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtMnfMoRodoValePedgCNPJForn: TWideStringField
      FieldName = 'CNPJForn'
      Size = 14
    end
    object QrFrtMnfMoRodoValePedgCNPJPg: TWideStringField
      FieldName = 'CNPJPg'
      Size = 14
    end
    object QrFrtMnfMoRodoValePedgnCompra: TWideStringField
      FieldName = 'nCompra'
    end
    object QrFrtMnfMoRodoValePedgLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFrtMnfMoRodoValePedgDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFrtMnfMoRodoValePedgDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFrtMnfMoRodoValePedgUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFrtMnfMoRodoValePedgUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFrtMnfMoRodoValePedgAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFrtMnfMoRodoValePedgAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object QrMDFeArq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT caba.IDCtrl IDCtrl_CabA, arq.IDCtrl IDCtrl_Arq, '
      'caba.Id, caba.FatID, caba.FatNum, caba.Empresa'
      'FROM ctecaba caba'
      'LEFT JOIN ctearq arq ON arq.IDCtrl=caba.IDCtrl'
      'WHERE caba.IDCtrl=:P0')
    Left = 856
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMDFeArqIDCtrl_CabA: TIntegerField
      FieldName = 'IDCtrl_CabA'
    end
    object QrMDFeArqIDCtrl_Arq: TIntegerField
      FieldName = 'IDCtrl_Arq'
      Required = True
    end
    object QrMDFeArqId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrMDFeArqFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeArqFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeArqEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object QrMDFeit2InfMunCarrega: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mdfeit2infmuncarrega '
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 276
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrMDFeit2InfMunCarregaFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeit2InfMunCarregaFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeit2InfMunCarregaEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeit2InfMunCarregaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMDFeit2InfMunCarregacMunCarrega: TIntegerField
      FieldName = 'cMunCarrega'
    end
    object QrMDFeit2InfMunCarregaxMunCarrega: TWideStringField
      FieldName = 'xMunCarrega'
      Size = 60
    end
  end
  object QrMDFeIt2InfPercurso: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mdfeit2infpercurso '
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 276
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrMDFeIt2InfPercursoFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeIt2InfPercursoFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeIt2InfPercursoEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeIt2InfPercursoControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMDFeIt2InfPercursoUFPer: TWideStringField
      FieldName = 'UFPer'
      Size = 2
    end
  end
  object QrMDFeIt2InfMunDescarga: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mdfeit2infmundescarga'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 276
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrMDFeIt2InfMunDescargaFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeIt2InfMunDescargaFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeIt2InfMunDescargaEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeIt2InfMunDescargaControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrMDFeIt2InfMunDescargacMunDescarga: TIntegerField
      FieldName = 'cMunDescarga'
    end
    object QrMDFeIt2InfMunDescargaxMunDescarga: TWideStringField
      FieldName = 'xMunDescarga'
      Size = 60
    end
  end
  object QrMDFeIt3InfCTe: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mdfeit3infcte'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND Controle=:P3')
    Left = 276
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrMDFeIt3InfCTeFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeIt3InfCTeFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeIt3InfCTeEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeIt3InfCTeControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrMDFeIt3InfCTeConta: TLargeintField
      FieldName = 'Conta'
    end
    object QrMDFeIt3InfCTechCTe: TWideStringField
      FieldName = 'chCTe'
      Size = 44
    end
    object QrMDFeIt3InfCTeSegCodBarra: TWideStringField
      FieldName = 'SegCodBarra'
      Size = 36
    end
  end
  object QrMDFeIt3InfNFe: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mdfeit3infnfe'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND Controle=:P3')
    Left = 276
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrMDFeIt3InfNFeFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeIt3InfNFeFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeIt3InfNFeEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeIt3InfNFeControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrMDFeIt3InfNFeConta: TLargeintField
      FieldName = 'Conta'
    end
    object QrMDFeIt3InfNFechNFe: TWideStringField
      FieldName = 'chNFe'
      Size = 44
    end
    object QrMDFeIt3InfNFeSegCodBarra: TWideStringField
      FieldName = 'SegCodBarra'
      Size = 36
    end
  end
  object QrMDFeIt3InfMDFe: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mdfeit3infmdfe'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND Controle=:P3')
    Left = 276
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrMDFeIt3InfMDFeFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeIt3InfMDFeFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeIt3InfMDFeEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeIt3InfMDFeControle: TLargeintField
      FieldName = 'Controle'
    end
    object QrMDFeIt3InfMDFeConta: TLargeintField
      FieldName = 'Conta'
    end
    object QrMDFeIt3InfMDFechMDFe: TWideStringField
      FieldName = 'chMDFe'
      Size = 44
    end
  end
  object QrMDFeIt1Lacres: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mdfeit1lacres'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 276
    Top = 344
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrMDFeIt1LacresFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeIt1LacresFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeIt1LacresEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeIt1LacresControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMDFeIt1LacresnLacre: TWideStringField
      FieldName = 'nLacre'
    end
  end
  object QrMDFeIt1AutXml: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mdfeit1autxml'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2')
    Left = 276
    Top = 392
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrMDFeIt1AutXmlFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeIt1AutXmlFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeIt1AutXmlEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeIt1AutXmlControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMDFeIt1AutXmlAddForma: TSmallintField
      FieldName = 'AddForma'
    end
    object QrMDFeIt1AutXmlAddIDCad: TIntegerField
      FieldName = 'AddIDCad'
    end
    object QrMDFeIt1AutXmlTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrMDFeIt1AutXmlCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrMDFeIt1AutXmlCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
  end
  object QrMDFeMoRodoCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mdfemorodocab'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 536
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrMDFeMoRodoCabFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeMoRodoCabFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeMoRodoCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeMoRodoCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMDFeMoRodoCabRNTRC: TWideStringField
      FieldName = 'RNTRC'
      Size = 8
    end
    object QrMDFeMoRodoCabveicTracao_cInt: TIntegerField
      FieldName = 'veicTracao_cInt'
    end
    object QrMDFeMoRodoCabveicTracao_placa: TWideStringField
      FieldName = 'veicTracao_placa'
      Size = 7
    end
    object QrMDFeMoRodoCabveicTracao_RENAVAM: TWideStringField
      FieldName = 'veicTracao_RENAVAM'
      Size = 11
    end
    object QrMDFeMoRodoCabveicTracao_tara: TIntegerField
      FieldName = 'veicTracao_tara'
    end
    object QrMDFeMoRodoCabveicTracao_capKg: TIntegerField
      FieldName = 'veicTracao_capKg'
    end
    object QrMDFeMoRodoCabveicTracao_capM3: TIntegerField
      FieldName = 'veicTracao_capM3'
    end
    object QrMDFeMoRodoCabveicTracao_tpRod: TSmallintField
      FieldName = 'veicTracao_tpRod'
    end
    object QrMDFeMoRodoCabveicTracao_tpCar: TSmallintField
      FieldName = 'veicTracao_tpCar'
    end
    object QrMDFeMoRodoCabveicTracao_UF: TWideStringField
      FieldName = 'veicTracao_UF'
      Size = 2
    end
    object QrMDFeMoRodoCabcodAgPorto: TWideStringField
      FieldName = 'codAgPorto'
      Size = 16
    end
  end
  object QrMDFeMoRodoPropT: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mdfemorodopropt'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 536
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrMDFeMoRodoPropTFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeMoRodoPropTFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeMoRodoPropTEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeMoRodoPropTControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMDFeMoRodoPropTConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrMDFeMoRodoPropTCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrMDFeMoRodoPropTCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrMDFeMoRodoPropTRNTRC: TWideStringField
      FieldName = 'RNTRC'
      Size = 8
    end
    object QrMDFeMoRodoPropTxNome: TWideStringField
      FieldName = 'xNome'
      Size = 60
    end
    object QrMDFeMoRodoPropTIE: TWideStringField
      FieldName = 'IE'
      Size = 14
    end
    object QrMDFeMoRodoPropTUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrMDFeMoRodoPropTtpProp: TSmallintField
      FieldName = 'tpProp'
    end
  end
  object QrMDFeMoRodoPropR: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mdfemorodopropr'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 536
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrMDFeMoRodoPropRFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeMoRodoPropRFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeMoRodoPropREmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeMoRodoPropRControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMDFeMoRodoPropRConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrMDFeMoRodoPropRCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrMDFeMoRodoPropRCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrMDFeMoRodoPropRRNTRC: TWideStringField
      FieldName = 'RNTRC'
      Size = 8
    end
    object QrMDFeMoRodoPropRxNome: TWideStringField
      FieldName = 'xNome'
      Size = 60
    end
    object QrMDFeMoRodoPropRIE: TWideStringField
      FieldName = 'IE'
      Size = 14
    end
    object QrMDFeMoRodoPropRUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrMDFeMoRodoPropRtpProp: TSmallintField
      FieldName = 'tpProp'
    end
  end
  object QrMDFeMoRodoCondutor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mdfemorodocondutor'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 536
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrMDFeMoRodoCondutorFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeMoRodoCondutorFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeMoRodoCondutorEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeMoRodoCondutorControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMDFeMoRodoCondutorConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrMDFeMoRodoCondutorxNome: TWideStringField
      FieldName = 'xNome'
    end
    object QrMDFeMoRodoCondutorCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
  end
  object QrMDFeMoRodoVeicRebo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mdfemorodoveic'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 536
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrMDFeMoRodoVeicReboFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeMoRodoVeicReboFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeMoRodoVeicReboEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeMoRodoVeicReboControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMDFeMoRodoVeicReboConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrMDFeMoRodoVeicRebocInt: TIntegerField
      FieldName = 'cInt'
    end
    object QrMDFeMoRodoVeicReboplaca: TWideStringField
      FieldName = 'placa'
      Size = 7
    end
    object QrMDFeMoRodoVeicReboRENAVAM: TWideStringField
      FieldName = 'RENAVAM'
      Size = 11
    end
    object QrMDFeMoRodoVeicRebotara: TIntegerField
      FieldName = 'tara'
    end
    object QrMDFeMoRodoVeicRebocapKG: TIntegerField
      FieldName = 'capKG'
    end
    object QrMDFeMoRodoVeicRebocapM3: TIntegerField
      FieldName = 'capM3'
    end
    object QrMDFeMoRodoVeicRebotpCar: TSmallintField
      FieldName = 'tpCar'
    end
    object QrMDFeMoRodoVeicReboUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
  end
  object QrMDFeMoRodoVPed: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mdfemorodovped'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 536
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrMDFeMoRodoVPedFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeMoRodoVPedFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeMoRodoVPedEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeMoRodoVPedControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMDFeMoRodoVPedConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrMDFeMoRodoVPedCNPJForn: TWideStringField
      FieldName = 'CNPJForn'
      Size = 14
    end
    object QrMDFeMoRodoVPedCNPJPg: TWideStringField
      FieldName = 'CNPJPg'
      Size = 14
    end
    object QrMDFeMoRodoVPednCompra: TWideStringField
      FieldName = 'nCompra'
    end
  end
  object QrMDFeMoRodoCIOT: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mdfemorodociot'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      '')
    Left = 536
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrMDFeMoRodoCIOTFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeMoRodoCIOTFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeMoRodoCIOTEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeMoRodoCIOTControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMDFeMoRodoCIOTConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrMDFeMoRodoCIOTCIOT: TLargeintField
      FieldName = 'CIOT'
    end
  end
  object QrFrtMnfMoRodoCIOT: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT rcr.*'
      'FROM frtmnfmorodociot rcr'
      'WHERE rcr.Codigo>0')
    Left = 128
    Top = 676
    object QrFrtMnfMoRodoCIOTCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFrtMnfMoRodoCIOTControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFrtMnfMoRodoCIOTCIOT: TLargeintField
      FieldName = 'CIOT'
    end
  end
  object QrMDFeLEnC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Id, FatID, FatNum'
      'FROM mdfecaba'
      'WHERE LoteEnv=:P0'
      '')
    Left = 36
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMDFeLEnCId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrMDFeLEnCFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeLEnCFatNum: TIntegerField
      FieldName = 'FatNum'
    end
  end
  object QrAtoArq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT caba.IDCtrl'
      'FROM mdfecaba caba'
      'LEFT JOIN mdfearq arq ON arq.IDCtrl=caba.IDCtrl'
      'WHERE arq.IDCtrl IS NULL'
      'AND Id <> ""'
      'AND ('
      '  emit_CNPJ=:P0'
      '  OR'
      '  emit_CPF=:P1'
      ')')
    Left = 856
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrAtoArqIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
  end
  object QrMDFeAut: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT caba.IDCtrl, caba.LoteEnv, caba.Id'
      'FROM mdfecaba caba'
      'LEFT JOIN mdfearq arq ON arq.IDCtrl=caba.IDCtrl'
      'WHERE arq.IDCtrl IS NOT NULL'
      'AND caba.infProt_cStat=100'
      'AND arq.XML_Aut IS NULL'
      'AND ('
      '  emit_CNPJ=:P0'
      '  OR'
      '  emit_CPF=:P1'
      ')')
    Left = 856
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrMDFeAutIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrMDFeAutLoteEnv: TIntegerField
      FieldName = 'LoteEnv'
    end
    object QrMDFeAutId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
  end
  object QrMDFeCan: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT caba.IDCtrl, caba.Id'
      'FROM mdfecaba caba'
      'LEFT JOIN mdfearq arq ON arq.IDCtrl=caba.IDCtrl'
      'WHERE arq.IDCtrl IS NOT NULL'
      'AND caba.infCanc_cStat=101'
      'AND ('
      '  emit_CNPJ=:P0'
      '  OR'
      '  emit_CPF=:P1'
      ')')
    Left = 856
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrMDFeCanIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrMDFeCanId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
  end
  object QrMDFeInu: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mdfeinut'
      'WHERE XML_Inu IS NULL'
      'AND cStat=102')
    Left = 856
    Top = 192
    object QrMDFeInuCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMDFeInuCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrMDFeInuNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrMDFeInuEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeInuDataC: TDateField
      FieldName = 'DataC'
    end
    object QrMDFeInuversao: TFloatField
      FieldName = 'versao'
    end
    object QrMDFeInuId: TWideStringField
      FieldName = 'Id'
      Size = 30
    end
    object QrMDFeInutpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrMDFeInucUF: TSmallintField
      FieldName = 'cUF'
    end
    object QrMDFeInuano: TSmallintField
      FieldName = 'ano'
    end
    object QrMDFeInuCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrMDFeInumodelo: TSmallintField
      FieldName = 'modelo'
    end
    object QrMDFeInuSerie: TIntegerField
      FieldName = 'Serie'
    end
    object QrMDFeInunMDFIni: TIntegerField
      FieldName = 'nMDFIni'
    end
    object QrMDFeInunMDFFim: TIntegerField
      FieldName = 'nMDFFim'
    end
    object QrMDFeInuJustif: TIntegerField
      FieldName = 'Justif'
    end
    object QrMDFeInuxJust: TWideStringField
      FieldName = 'xJust'
      Size = 255
    end
    object QrMDFeInucStat: TIntegerField
      FieldName = 'cStat'
    end
    object QrMDFeInuxMotivo: TWideStringField
      FieldName = 'xMotivo'
      Size = 255
    end
    object QrMDFeInudhRecbto: TDateTimeField
      FieldName = 'dhRecbto'
    end
    object QrMDFeInunProt: TWideStringField
      FieldName = 'nProt'
      Size = 15
    end
    object QrMDFeInuLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMDFeInuDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMDFeInuDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMDFeInuUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMDFeInuUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMDFeInuAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrMDFeInuAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrMDFeInuXML_Inu: TWideMemoField
      FieldName = 'XML_Inu'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrEveTp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nSeqEvento '
      'FROM mdfeevercab'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND tpEvento=:P3'
      'ORDER BY nSeqEvento DESC'
      ''
      '')
    Left = 856
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrEveTpnSeqEvento: TSmallintField
      FieldName = 'nSeqEvento'
    end
  end
  object QrEveIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM mdfeevercab'
      'WHERE EventoLote=:P0'
      'AND tpEvento=:P1'
      'AND nSeqEvento=:P2')
    Left = 856
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrEveItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrEveSub: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SubCtrl'
      'FROM mdfeeverret'
      'WHERE Controle=:P0'
      'AND ret_Id=:P1')
    Left = 856
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEveSubSubCtrl: TIntegerField
      FieldName = 'SubCtrl'
    end
  end
  object QrMDFeEnc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT caba.IDCtrl, caba.Id'
      'FROM mdfecaba caba'
      'LEFT JOIN mdfearq arq ON arq.IDCtrl=caba.IDCtrl'
      'WHERE arq.IDCtrl IS NOT NULL'
      'AND caba.infEnc_cStat=101'
      'AND ('
      '  emit_CNPJ=:P0'
      '  OR'
      '  emit_CPF=:P1'
      ')')
    Left = 856
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrMDFeEncIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrMDFeEncId: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
  end
  object QrArq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mdfearq'
      'WHERE IDCtrl=:P0'
      ''
      '')
    Left = 856
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrArqFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'nfearq.FatID'
    end
    object QrArqFatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'nfearq.FatNum'
    end
    object QrArqEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfearq.Empresa'
    end
    object QrArqIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'nfearq.IDCtrl'
    end
    object QrArqXML_MDFe: TWideMemoField
      FieldName = 'XML_MDFe'
      Origin = 'nfearq.XML_MDFe'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrArqXML_Aut: TWideMemoField
      FieldName = 'XML_Aut'
      Origin = 'nfearq.XML_Aut'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrArqXML_Can: TWideMemoField
      FieldName = 'XML_Can'
      Origin = 'nfearq.XML_Can'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrArqLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'nfearq.Lk'
    end
    object QrArqDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'nfearq.DataCad'
    end
    object QrArqDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'nfearq.DataAlt'
    end
    object QrArqUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'nfearq.UserCad'
    end
    object QrArqAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'nfearq.AlterWeb'
    end
    object QrArqUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'nfearq.UserAlt'
    end
    object QrArqAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'nfearq.Ativo'
    end
  end
  object QrCEnc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 856
    Top = 484
    object QrCEncinfEnc_cStat: TIntegerField
      FieldName = 'infEnc_cStat'
    end
    object QrCEncinfProt_cStat: TIntegerField
      FieldName = 'infProt_cStat'
    end
    object QrCEncStatus: TIntegerField
      FieldName = 'Status'
    end
  end
end
