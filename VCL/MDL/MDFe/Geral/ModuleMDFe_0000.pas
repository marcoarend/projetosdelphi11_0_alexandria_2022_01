unit ModuleMDFe_0000;

interface

uses
  System.SysUtils, System.Classes, Data.DB, mySQLDbTables,
  StdCtrls, Variants, SHDocVw, UrlMon,
  CAPICOM_TLB,
  dmkGeral, UnInternalConsts, UnDmkEnums, UnXXe_PF,
  Forms, Controls, Types,
  //Dialogs, ComCtrls, InvokeRegistry, dmkEdit,
  //SOAPHTTPClient, SOAPHTTPTrans, JwaWinCrypt, WinInet, DateUtils, OleCtrls,
  Grids, DBGrids, Graphics, UnDmkProcFunc,
  UnMyObjects, UnMDFeListas, UnMDFe_PF;

type
  TDmMDFe_0000 = class(TDataModule)
    QrOpcoesMDFe: TmySQLQuery;
    QrOpcoesMDFeMDFeversao: TFloatField;
    QrOpcoesMDFeMDFeide_mod: TSmallintField;
    QrOpcoesMDFeSimplesFed: TSmallintField;
    QrOpcoesMDFeDirMDFeGer: TWideStringField;
    QrOpcoesMDFeDirMDFeAss: TWideStringField;
    QrOpcoesMDFeDirMDFeEnvLot: TWideStringField;
    QrOpcoesMDFeDirMDFeRec: TWideStringField;
    QrOpcoesMDFeDirMDFePedRec: TWideStringField;
    QrOpcoesMDFeDirMDFeProRec: TWideStringField;
    QrOpcoesMDFeDirMDFeDen: TWideStringField;
    QrOpcoesMDFeDirMDFePedCan: TWideStringField;
    QrOpcoesMDFeDirMDFeCan: TWideStringField;
    QrOpcoesMDFeDirMDFePedInu: TWideStringField;
    QrOpcoesMDFeDirMDFeInu: TWideStringField;
    QrOpcoesMDFeDirMDFePedSit: TWideStringField;
    QrOpcoesMDFeDirMDFeSit: TWideStringField;
    QrOpcoesMDFeDirMDFePedSta: TWideStringField;
    QrOpcoesMDFeDirMDFeSta: TWideStringField;
    QrOpcoesMDFeMDFeUF_WebServ: TWideStringField;
    QrOpcoesMDFeMDFeUF_Servico: TWideStringField;
    QrOpcoesMDFePathLogoMDFe: TWideStringField;
    QrOpcoesMDFeCtaFretPrest: TIntegerField;
    QrOpcoesMDFeTxtFretPrest: TWideStringField;
    QrOpcoesMDFeDupFretPrest: TWideStringField;
    QrOpcoesMDFeMDFeSerNum: TWideStringField;
    QrOpcoesMDFeMDFeSerVal: TDateField;
    QrOpcoesMDFeMDFeSerAvi: TSmallintField;
    QrOpcoesMDFeDirDAMDFes: TWideStringField;
    QrOpcoesMDFeDirMDFeProt: TWideStringField;
    QrOpcoesMDFeMDFePreMailAut: TIntegerField;
    QrOpcoesMDFeMDFePreMailCan: TIntegerField;
    QrOpcoesMDFeMDFePreMailEveCCe: TIntegerField;
    QrOpcoesMDFeMDFeVerStaSer: TFloatField;
    QrOpcoesMDFeMDFeVerEnvLot: TFloatField;
    QrOpcoesMDFeMDFeVerConLot: TFloatField;
    QrOpcoesMDFeMDFeVerCanMDFe: TFloatField;
    QrOpcoesMDFeMDFeVerInuNum: TFloatField;
    QrOpcoesMDFeMDFeVerConMDFe: TFloatField;
    QrOpcoesMDFeMDFeVerLotEve: TFloatField;
    QrOpcoesMDFeMDFeide_tpImp: TSmallintField;
    QrOpcoesMDFeMDFeide_tpAmb: TSmallintField;
    QrOpcoesMDFeMDFeAppCode: TSmallintField;
    QrOpcoesMDFeMDFeAssDigMode: TSmallintField;
    QrOpcoesMDFeMDFeEntiTipCto: TIntegerField;
    QrOpcoesMDFeMDFeEntiTipCt1: TIntegerField;
    QrOpcoesMDFeMyEmailMDFe: TWideStringField;
    QrOpcoesMDFeDirMDFeSchema: TWideStringField;
    QrOpcoesMDFeDirMDFeRWeb: TWideStringField;
    QrOpcoesMDFeDirMDFeEveEnvLot: TWideStringField;
    QrOpcoesMDFeDirMDFeEveRetLot: TWideStringField;
    QrOpcoesMDFeDirMDFeEvePedEnc: TWideStringField;
    QrOpcoesMDFeDirMDFeEveRetEnc: TWideStringField;
    QrOpcoesMDFeDirMDFeEveProcEnc: TWideStringField;
    QrOpcoesMDFeDirMDFeEvePedCan: TWideStringField;
    QrOpcoesMDFeDirMDFeEveRetCan: TWideStringField;
    QrOpcoesMDFeDirMDFeRetMDFeDes: TWideStringField;
    QrOpcoesMDFeMDFeMDFeVerConDes: TFloatField;
    QrOpcoesMDFeDirMDFeEvePedMDe: TWideStringField;
    QrOpcoesMDFeDirMDFeEveRetMDe: TWideStringField;
    QrOpcoesMDFeDirDowMDFeDes: TWideStringField;
    QrOpcoesMDFeDirDowMDFeMDFe: TWideStringField;
    QrOpcoesMDFeMDFeInfCpl: TIntegerField;
    QrOpcoesMDFeMDFeVerConsCad: TFloatField;
    QrOpcoesMDFeMDFeVerDistDFeInt: TFloatField;
    QrOpcoesMDFeMDFeVerDowMDFe: TFloatField;
    QrOpcoesMDFeDirDowMDFeCnf: TWideStringField;
    QrOpcoesMDFeMDFeItsLin: TSmallintField;
    QrOpcoesMDFeMDFeFTRazao: TSmallintField;
    QrOpcoesMDFeMDFeFTEnder: TSmallintField;
    QrOpcoesMDFeMDFeFTFones: TSmallintField;
    QrOpcoesMDFeMDFeMaiusc: TSmallintField;
    QrOpcoesMDFeMDFeShowURL: TWideStringField;
    QrOpcoesMDFeMDFetpEmis: TSmallintField;
    QrOpcoesMDFeMDFeSCAN_Ser: TIntegerField;
    QrOpcoesMDFeMDFeSCAN_nMDF: TIntegerField;
    QrOpcoesMDFeMDFe_indFinalCpl: TSmallintField;
    QrOpcoesMDFeTZD_UTC_Auto_TermoAceite: TIntegerField;
    QrOpcoesMDFeTZD_UTC_Auto_DataHoraAceite: TDateTimeField;
    QrOpcoesMDFeTZD_UTC_Auto_UsuarioAceite: TIntegerField;
    QrOpcoesMDFeTZD_UTC_Auto: TSmallintField;
    QrOpcoesMDFeTZD_UTC: TFloatField;
    QrOpcoesMDFehVeraoAsk: TDateField;
    QrOpcoesMDFehVeraoIni: TDateField;
    QrOpcoesMDFehVeraoFim: TDateField;
    QrOpcoesMDFeParamsMDFe: TIntegerField;
    QrEmpresa: TmySQLQuery;
    QrEmpresaCodigo: TIntegerField;
    QrEmpresaTipo: TSmallintField;
    QrEmpresaCNPJ: TWideStringField;
    QrEmpresaCPF: TWideStringField;
    QrEmpresaIE: TWideStringField;
    QrEmpresaRG: TWideStringField;
    QrEmpresaCNAE: TWideStringField;
    QrEmpresaNO_ENT: TWideStringField;
    QrEmpresaFANTASIA: TWideStringField;
    QrEmpresaRUA: TWideStringField;
    QrEmpresaCOMPL: TWideStringField;
    QrEmpresaBAIRRO: TWideStringField;
    QrEmpresaTe1: TWideStringField;
    QrEmpresaNO_LOGRAD: TWideStringField;
    QrEmpresaNO_Munici: TWideStringField;
    QrEmpresaNO_UF: TWideStringField;
    QrEmpresaDTB_UF: TWideStringField;
    QrEmpresaNO_Pais: TWideStringField;
    QrEmpresaIEST: TWideStringField;
    QrEmpresaSUFRAMA: TWideStringField;
    QrEmpresaFilial: TIntegerField;
    QrEmpresaNIRE: TWideStringField;
    QrEmpresaNumero: TFloatField;
    QrEmpresaCEP: TFloatField;
    QrEmpresaCodiPais: TFloatField;
    QrEmpresaCodMunici: TFloatField;
    QrEmpresaUF: TFloatField;
    QrFilial: TmySQLQuery;
    QrFilialMDFeversao: TFloatField;
    QrFilialMDFeide_mod: TSmallintField;
    QrFilialSimplesFed: TSmallintField;
    QrFilialDirMDFeGer: TWideStringField;
    QrFilialDirMDFeAss: TWideStringField;
    QrFilialDirMDFeEnvLot: TWideStringField;
    QrFilialDirMDFeRec: TWideStringField;
    QrFilialDirMDFePedRec: TWideStringField;
    QrFilialDirMDFeProRec: TWideStringField;
    QrFilialDirMDFeDen: TWideStringField;
    QrFilialDirMDFePedCan: TWideStringField;
    QrFilialDirMDFeCan: TWideStringField;
    QrFilialDirMDFePedInu: TWideStringField;
    QrFilialDirMDFeInu: TWideStringField;
    QrFilialDirMDFePedSit: TWideStringField;
    QrFilialDirMDFeSit: TWideStringField;
    QrFilialDirMDFePedSta: TWideStringField;
    QrFilialDirMDFeSta: TWideStringField;
    QrFilialMDFeUF_WebServ: TWideStringField;
    QrFilialMDFeUF_Servico: TWideStringField;
    QrFilialPathLogoMDFe: TWideStringField;
    QrFilialCtaFretPrest: TIntegerField;
    QrFilialTxtFretPrest: TWideStringField;
    QrFilialDupFretPrest: TWideStringField;
    QrFilialMDFeSerNum: TWideStringField;
    QrFilialMDFeSerVal: TDateField;
    QrFilialMDFeSerAvi: TSmallintField;
    QrFilialDirDAMDFes: TWideStringField;
    QrFilialDirMDFeProt: TWideStringField;
    QrFilialMDFePreMailAut: TIntegerField;
    QrFilialMDFePreMailCan: TIntegerField;
    QrFilialMDFePreMailEveCCe: TIntegerField;
    QrFilialMDFeVerStaSer: TFloatField;
    QrFilialMDFeVerEnvLot: TFloatField;
    QrFilialMDFeVerConLot: TFloatField;
    QrFilialMDFeVerCanMDFe: TFloatField;
    QrFilialMDFeVerInuNum: TFloatField;
    QrFilialMDFeVerConMDFe: TFloatField;
    QrFilialMDFeVerLotEve: TFloatField;
    QrFilialMDFeide_tpImp: TSmallintField;
    QrFilialMDFeide_tpAmb: TSmallintField;
    QrFilialMDFeAppCode: TSmallintField;
    QrFilialMDFeAssDigMode: TSmallintField;
    QrFilialMDFeEntiTipCto: TIntegerField;
    QrFilialMDFeEntiTipCt1: TIntegerField;
    QrFilialMyEmailMDFe: TWideStringField;
    QrFilialDirMDFeSchema: TWideStringField;
    QrFilialDirMDFeRWeb: TWideStringField;
    QrFilialDirMDFeEveEnvLot: TWideStringField;
    QrFilialDirMDFeEveRetLot: TWideStringField;
    QrFilialDirMDFeEvePedEnc: TWideStringField;
    QrFilialDirMDFeEveRetEnc: TWideStringField;
    QrFilialDirMDFeEvePedCan: TWideStringField;
    QrFilialDirMDFeEveRetCan: TWideStringField;
    QrFilialDirMDFeRetMDFeDes: TWideStringField;
    QrFilialMDFeMDFeVerConDes: TFloatField;
    QrFilialDirMDFeEvePedMDe: TWideStringField;
    QrFilialDirMDFeEveRetMDe: TWideStringField;
    QrFilialDirDowMDFeDes: TWideStringField;
    QrFilialDirDowMDFeMDFe: TWideStringField;
    QrFilialMDFeInfCpl: TIntegerField;
    QrFilialMDFeVerConsCad: TFloatField;
    QrFilialMDFeVerDistDFeInt: TFloatField;
    QrFilialMDFeVerDowMDFe: TFloatField;
    QrFilialDirDowMDFeCnf: TWideStringField;
    QrFilialMDFeItsLin: TSmallintField;
    QrFilialMDFeFTRazao: TSmallintField;
    QrFilialMDFeFTEnder: TSmallintField;
    QrFilialMDFeFTFones: TSmallintField;
    QrFilialMDFeMaiusc: TSmallintField;
    QrFilialMDFeShowURL: TWideStringField;
    QrFilialMDFetpEmis: TSmallintField;
    QrFilialMDFeSCAN_Ser: TIntegerField;
    QrFilialMDFeSCAN_nMDF: TIntegerField;
    QrFilialMDFe_indFinalCpl: TSmallintField;
    QrFilialTZD_UTC_Auto_TermoAceite: TIntegerField;
    QrFilialTZD_UTC_Auto_DataHoraAceite: TDateTimeField;
    QrFilialTZD_UTC_Auto_UsuarioAceite: TIntegerField;
    QrFilialTZD_UTC_Auto: TSmallintField;
    QrFilialTZD_UTC: TFloatField;
    QrFilialhVeraoAsk: TDateField;
    QrFilialhVeraoIni: TDateField;
    QrFilialhVeraoFim: TDateField;
    QrFilialParamsMDFe: TIntegerField;
    QrMDFeCabA: TmySQLQuery;
    QrMDFeLayI: TmySQLQuery;
    QrFrtMnfCab: TmySQLQuery;
    QrFrtMnfCabCodigo: TIntegerField;
    QrFrtMnfCabNome: TWideStringField;
    QrFrtMnfCabEmpresa: TIntegerField;
    QrFrtMnfCabAbertura: TDateTimeField;
    QrFrtMnfCabEncerrou: TDateTimeField;
    QrFrtMnfCabDataViagIniPrv: TDateTimeField;
    QrFrtMnfCabDataViagIniSai: TDateTimeField;
    QrFrtMnfCabDataViagFimPrv: TDateTimeField;
    QrFrtMnfCabDataViagFimChg: TDateTimeField;
    QrFrtMnfCabFrtRegMnf: TIntegerField;
    QrFrtMnfCabTrnsIts: TIntegerField;
    QrFrtMnfCabTpEmit: TSmallintField;
    QrFrtMnfCabModal: TSmallintField;
    QrFrtMnfCabTpEmis: TSmallintField;
    QrFrtMnfCabFrtRegPth: TIntegerField;
    QrFrtMnfCabUFIni: TWideStringField;
    QrFrtMnfCabUFFim: TWideStringField;
    QrFrtMnfCabParamsMDFe: TIntegerField;
    QrFrtMnfCabTot_qCTe: TIntegerField;
    QrFrtMnfCabTot_qNFe: TIntegerField;
    QrFrtMnfCabTot_qMDFe: TIntegerField;
    QrFrtMnfCabTot_vCarga: TFloatField;
    QrFrtMnfCabTot_cUnid: TSmallintField;
    QrFrtMnfCabTot_qCarga: TFloatField;
    QrFrtMnfCabinfAdFisco: TWideMemoField;
    QrFrtMnfCabinfCpl: TWideMemoField;
    QrFrtMnfCabSerieDesfe: TIntegerField;
    QrFrtMnfCabMDFDesfeita: TIntegerField;
    QrFrtMnfCabCodAgPorto: TWideStringField;
    QrMDFeCabAFatID: TIntegerField;
    QrMDFeCabAFatNum: TIntegerField;
    QrMDFeCabAEmpresa: TIntegerField;
    QrMDFeCabAIDCtrl: TIntegerField;
    QrMDFeCabALoteEnv: TIntegerField;
    QrMDFeCabAversao: TFloatField;
    QrMDFeCabAId: TWideStringField;
    QrMDFeCabAide_cUF: TSmallintField;
    QrMDFeCabAide_tpAmb: TSmallintField;
    QrMDFeCabAide_tpEmit: TSmallintField;
    QrMDFeCabAide_mod: TSmallintField;
    QrMDFeCabAide_serie: TIntegerField;
    QrMDFeCabAide_nMDF: TIntegerField;
    QrMDFeCabAide_cMDF: TIntegerField;
    QrMDFeCabAide_cDV: TSmallintField;
    QrMDFeCabAide_Modal: TSmallintField;
    QrMDFeCabAide_dEmi: TDateField;
    QrMDFeCabAide_hEmi: TTimeField;
    QrMDFeCabAide_dhEmiTZD: TFloatField;
    QrMDFeCabAide_tpEmis: TSmallintField;
    QrMDFeCabAide_procEmi: TSmallintField;
    QrMDFeCabAide_verProc: TWideStringField;
    QrMDFeCabAide_UFIni: TWideStringField;
    QrMDFeCabAide_UFFim: TWideStringField;
    QrMDFeCabAide_dIniViagem: TDateField;
    QrMDFeCabAide_hIniViagem: TTimeField;
    QrMDFeCabAide_dhIniViagemTZD: TFloatField;
    QrMDFeCabAemit_CNPJ: TWideStringField;
    QrMDFeCabAemit_IE: TWideStringField;
    QrMDFeCabAemit_xNome: TWideStringField;
    QrMDFeCabAemit_xFant: TWideStringField;
    QrMDFeCabAemit_xLgr: TWideStringField;
    QrMDFeCabAemit_nro: TWideStringField;
    QrMDFeCabAemit_xCpl: TWideStringField;
    QrMDFeCabAemit_xBairro: TWideStringField;
    QrMDFeCabAemit_cMun: TIntegerField;
    QrMDFeCabAemit_xMun: TWideStringField;
    QrMDFeCabAemit_CEP: TIntegerField;
    QrMDFeCabAemit_UF: TWideStringField;
    QrMDFeCabAemit_fone: TWideStringField;
    QrMDFeCabAemit_email: TWideStringField;
    QrMDFeCabAversaoModal: TFloatField;
    QrMDFeCabATot_qCTe: TIntegerField;
    QrMDFeCabATot_qNFe: TIntegerField;
    QrMDFeCabATot_qMDFe: TIntegerField;
    QrMDFeCabATot_vCarga: TFloatField;
    QrMDFeCabATot_cUnid: TSmallintField;
    QrMDFeCabATot_qCarga: TFloatField;
    QrMDFeCabAinfAdFisco: TWideMemoField;
    QrMDFeCabAinfCpl: TWideMemoField;
    QrMDFeCabAStatus: TIntegerField;
    QrMDFeCabAprotMDFe_versao: TFloatField;
    QrMDFeCabAinfProt_Id: TWideStringField;
    QrMDFeCabAinfProt_tpAmb: TSmallintField;
    QrMDFeCabAinfProt_verAplic: TWideStringField;
    QrMDFeCabAinfProt_dhRecbto: TDateTimeField;
    QrMDFeCabAinfProt_dhRecbtoTZD: TFloatField;
    QrMDFeCabAinfProt_nProt: TWideStringField;
    QrMDFeCabAinfProt_digVal: TWideStringField;
    QrMDFeCabAinfProt_cStat: TIntegerField;
    QrMDFeCabAinfProt_xMotivo: TWideStringField;
    QrMDFeCabAretCancMDFe_versao: TFloatField;
    QrMDFeCabAinfCanc_Id: TWideStringField;
    QrMDFeCabAinfCanc_tpAmb: TSmallintField;
    QrMDFeCabAinfCanc_verAplic: TWideStringField;
    QrMDFeCabAinfCanc_dhRecbto: TDateTimeField;
    QrMDFeCabAinfCanc_dhRecbtoTZD: TFloatField;
    QrMDFeCabAinfCanc_nProt: TWideStringField;
    QrMDFeCabAinfCanc_digVal: TWideStringField;
    QrMDFeCabAinfCanc_cStat: TIntegerField;
    QrMDFeCabAinfCanc_xMotivo: TWideStringField;
    QrMDFeCabAinfCanc_cJust: TIntegerField;
    QrMDFeCabAinfCanc_xJust: TWideStringField;
    QrMDFeCabAFisRegCad: TIntegerField;
    QrMDFeCabACartEmiss: TIntegerField;
    QrMDFeCabATabelaPrc: TIntegerField;
    QrMDFeCabACondicaoPg: TIntegerField;
    QrMDFeCabACodInfoEmite: TIntegerField;
    QrMDFeCabACriAForca: TSmallintField;
    QrMDFeCabAcSitMDFe: TSmallintField;
    QrMDFeCabAcSitEnc: TSmallintField;
    QrEntiMDFe: TmySQLQuery;
    QrEntiMDFeCNPJ: TWideStringField;
    QrEntiMDFeCPF: TWideStringField;
    QrEntiMDFeIE: TWideStringField;
    QrEntiMDFexNome: TWideStringField;
    QrEntiMDFexFant: TWideStringField;
    QrEntiMDFefone: TWideStringField;
    QrEntiMDFeTipo: TSmallintField;
    QrEntiMDFexLgr: TWideStringField;
    QrEntiMDFexCpl: TWideStringField;
    QrEntiMDFexBairro: TWideStringField;
    QrEntiMDFeCMun: TFloatField;
    QrEntiMDFexMun: TWideStringField;
    QrEntiMDFeCEP: TFloatField;
    QrEntiMDFeUF: TWideStringField;
    QrEntiMDFeCPais: TFloatField;
    QrEntiMDFexPais: TWideStringField;
    QrEntiMDFeEmail: TWideStringField;
    QrEntiMDFeNro: TWideStringField;
    QrEntiMDFeSUFRAMA: TWideStringField;
    QrMDFeLayINO_Grupo: TWideStringField;
    QrMDFeLayIVersao: TFloatField;
    QrMDFeLayIGrupo: TIntegerField;
    QrMDFeLayICodigo: TIntegerField;
    QrMDFeLayICampo: TWideStringField;
    QrMDFeLayINivel: TIntegerField;
    QrMDFeLayIDescricao: TWideStringField;
    QrMDFeLayIElemento: TWideStringField;
    QrMDFeLayITipo: TWideStringField;
    QrMDFeLayIOcorMin: TSmallintField;
    QrMDFeLayIOcorMax: TIntegerField;
    QrMDFeLayITamMin: TSmallintField;
    QrMDFeLayITamMax: TIntegerField;
    QrMDFeLayITamVar: TWideStringField;
    QrMDFeLayIObservacao: TWideMemoField;
    QrMDFeLayIInfoVazio: TSmallintField;
    QrMDFeLayIDominio: TWideStringField;
    QrMDFeLayIExpReg: TWideStringField;
    QrMDFeLayIFormatStr: TWideStringField;
    QrMDFeLayIDeciCasas: TSmallintField;
    QrMDFeLayICartaCorrige: TSmallintField;
    QrMDFeLayIAlterWeb: TSmallintField;
    QrMDFeLayIAtivo: TSmallintField;
    QrFrtMnfMunCarrega: TmySQLQuery;
    QrFrtMnfInfPercurso: TmySQLQuery;
    QrFrtMnfLacres: TmySQLQuery;
    QrFrtMnfAutXML: TmySQLQuery;
    QrFrtMnfAutXMLCodigo: TIntegerField;
    QrFrtMnfAutXMLControle: TIntegerField;
    QrFrtMnfAutXMLAddForma: TSmallintField;
    QrFrtMnfAutXMLAddIDCad: TIntegerField;
    QrFrtMnfAutXMLTipo: TSmallintField;
    QrFrtMnfAutXMLCNPJ: TWideStringField;
    QrFrtMnfAutXMLCPF: TWideStringField;
    QrFrtMnfAutXMLLk: TIntegerField;
    QrFrtMnfAutXMLDataCad: TDateField;
    QrFrtMnfAutXMLDataAlt: TDateField;
    QrFrtMnfAutXMLUserCad: TIntegerField;
    QrFrtMnfAutXMLUserAlt: TIntegerField;
    QrFrtMnfAutXMLAlterWeb: TSmallintField;
    QrFrtMnfAutXMLAtivo: TSmallintField;
    QrFrtMnfLacresCodigo: TIntegerField;
    QrFrtMnfLacresControle: TIntegerField;
    QrFrtMnfLacresnLacre: TWideStringField;
    QrFrtMnfLacresLk: TIntegerField;
    QrFrtMnfLacresDataCad: TDateField;
    QrFrtMnfLacresDataAlt: TDateField;
    QrFrtMnfLacresUserCad: TIntegerField;
    QrFrtMnfLacresUserAlt: TIntegerField;
    QrFrtMnfLacresAlterWeb: TSmallintField;
    QrFrtMnfLacresAtivo: TSmallintField;
    QrFrtMnfInfPercursoCodigo: TIntegerField;
    QrFrtMnfInfPercursoControle: TIntegerField;
    QrFrtMnfInfPercursoUFPer: TWideStringField;
    QrFrtMnfMunCarregaCodigo: TIntegerField;
    QrFrtMnfMunCarregaControle: TIntegerField;
    QrFrtMnfMunCarregacMunCarrega: TIntegerField;
    QrFrtMnfMunCarregaxMunCarrega: TWideStringField;
    QrFrtMnfInfDocCMun: TmySQLQuery;
    QrFrtMnfInfDocXMun: TmySQLQuery;
    QrFrtMnfInfDocCMunCodigo: TIntegerField;
    QrFrtMnfInfDocCMuncMundescarga: TIntegerField;
    QrFrtMnfInfDocXMunxMundescarga: TWideStringField;
    QrFrtMnfInfDoc55: TmySQLQuery;
    QrFrtMnfInfDoc55Codigo: TIntegerField;
    QrFrtMnfInfDoc55Controle: TIntegerField;
    QrFrtMnfInfDoc55cMunDescarga: TIntegerField;
    QrFrtMnfInfDoc55xMunDescarga: TWideStringField;
    QrFrtMnfInfDoc55DocMod: TSmallintField;
    QrFrtMnfInfDoc55DocSerie: TIntegerField;
    QrFrtMnfInfDoc55DocNumero: TIntegerField;
    QrFrtMnfInfDoc55DocChave: TWideStringField;
    QrFrtMnfInfDoc55SegCodBarra: TWideStringField;
    QrFrtMnfInfDoc55TrnsIts: TIntegerField;
    QrFrtMnfInfDoc57: TmySQLQuery;
    QrFrtMnfInfDoc57Codigo: TIntegerField;
    QrFrtMnfInfDoc57Controle: TIntegerField;
    QrFrtMnfInfDoc57cMunDescarga: TIntegerField;
    QrFrtMnfInfDoc57xMunDescarga: TWideStringField;
    QrFrtMnfInfDoc57DocMod: TSmallintField;
    QrFrtMnfInfDoc57DocSerie: TIntegerField;
    QrFrtMnfInfDoc57DocNumero: TIntegerField;
    QrFrtMnfInfDoc57DocChave: TWideStringField;
    QrFrtMnfInfDoc57SegCodBarra: TWideStringField;
    QrFrtMnfInfDoc57TrnsIts: TIntegerField;
    QrFrtMnfInfDoc58: TmySQLQuery;
    QrFrtMnfInfDoc58Codigo: TIntegerField;
    QrFrtMnfInfDoc58Controle: TIntegerField;
    QrFrtMnfInfDoc58cMunDescarga: TIntegerField;
    QrFrtMnfInfDoc58xMunDescarga: TWideStringField;
    QrFrtMnfInfDoc58DocMod: TSmallintField;
    QrFrtMnfInfDoc58DocSerie: TIntegerField;
    QrFrtMnfInfDoc58DocNumero: TIntegerField;
    QrFrtMnfInfDoc58DocChave: TWideStringField;
    QrFrtMnfInfDoc58SegCodBarra: TWideStringField;
    QrFrtMnfInfDoc58TrnsIts: TIntegerField;
    QrFrtMnfInfDocXMunControle: TIntegerField;
    QrTrnsIts: TmySQLQuery;
    QrTrnsItsRNTRC: TWideStringField;
    QrTrnsItstpProp: TSmallintField;
    QrTrnsItsCodigo: TIntegerField;
    QrTrnsItsControle: TIntegerField;
    QrTrnsItsNome: TWideStringField;
    QrTrnsItsRENAVAM: TWideStringField;
    QrTrnsItsPlaca: TWideStringField;
    QrTrnsItsTaraKg: TIntegerField;
    QrTrnsItsCapKg: TIntegerField;
    QrTrnsItsTpVeic: TSmallintField;
    QrTrnsItsTpRod: TSmallintField;
    QrTrnsItsTpCar: TSmallintField;
    QrTrnsItsCapM3: TIntegerField;
    QrTrnsItsUF: TWideStringField;
    QrTerceiro: TmySQLQuery;
    QrTerceiroRNTRC: TWideStringField;
    QrTerceiroNO_ENT: TWideStringField;
    QrTerceiroCPF: TWideStringField;
    QrTerceiroCNPJ: TWideStringField;
    QrTerceiroIE: TWideStringField;
    QrTerceiroUF: TWideStringField;
    QrTrnsCad: TmySQLQuery;
    QrTrnsCadCodigo: TIntegerField;
    QrTrnsCadRNTRC: TWideStringField;
    QrTrnsCadLk: TIntegerField;
    QrTrnsCadDataCad: TDateField;
    QrTrnsCadDataAlt: TDateField;
    QrTrnsCadUserCad: TIntegerField;
    QrTrnsCadUserAlt: TIntegerField;
    QrTrnsCadAlterWeb: TSmallintField;
    QrTrnsCadAtivo: TSmallintField;
    QrTrnsCadTpProp: TSmallintField;
    QrTerceiroTipo: TSmallintField;
    QrFrtMnfMoRodoCondutor: TmySQLQuery;
    QrFrtMnfMoRodoCondutorCodigo: TIntegerField;
    QrFrtMnfMoRodoCondutorControle: TIntegerField;
    QrFrtMnfMoRodoCondutorEntidade: TIntegerField;
    QrFrtMnfMoRodoCondutorxNome: TWideStringField;
    QrFrtMnfMoRodoCondutorCPF: TWideStringField;
    QrFrtMnfMoRodoCondutorLk: TIntegerField;
    QrFrtMnfMoRodoCondutorDataCad: TDateField;
    QrFrtMnfMoRodoCondutorDataAlt: TDateField;
    QrFrtMnfMoRodoCondutorUserCad: TIntegerField;
    QrFrtMnfMoRodoCondutorUserAlt: TIntegerField;
    QrFrtMnfMoRodoCondutorAlterWeb: TSmallintField;
    QrFrtMnfMoRodoCondutorAtivo: TSmallintField;
    QrFrtMnfMoRodoVeicRebo: TmySQLQuery;
    QrFrtMnfMoRodoVeicReboCodigo: TIntegerField;
    QrFrtMnfMoRodoVeicReboControle: TIntegerField;
    QrFrtMnfMoRodoVeicRebocInt: TIntegerField;
    QrFrtMnfMoRodoVeicReboLk: TIntegerField;
    QrFrtMnfMoRodoVeicReboDataCad: TDateField;
    QrFrtMnfMoRodoVeicReboDataAlt: TDateField;
    QrFrtMnfMoRodoVeicReboUserCad: TIntegerField;
    QrFrtMnfMoRodoVeicReboUserAlt: TIntegerField;
    QrFrtMnfMoRodoVeicReboAlterWeb: TSmallintField;
    QrFrtMnfMoRodoVeicReboAtivo: TSmallintField;
    QrFrtMnfMoRodoVeicReboNO_VEIC: TWideStringField;
    QrFrtMnfMoRodoVeicReboPlaca: TWideStringField;
    QrFrtMnfMoRodoValePedg: TmySQLQuery;
    QrFrtMnfMoRodoValePedgCodigo: TIntegerField;
    QrFrtMnfMoRodoValePedgControle: TIntegerField;
    QrFrtMnfMoRodoValePedgCNPJForn: TWideStringField;
    QrFrtMnfMoRodoValePedgCNPJPg: TWideStringField;
    QrFrtMnfMoRodoValePedgnCompra: TWideStringField;
    QrFrtMnfMoRodoValePedgLk: TIntegerField;
    QrFrtMnfMoRodoValePedgDataCad: TDateField;
    QrFrtMnfMoRodoValePedgDataAlt: TDateField;
    QrFrtMnfMoRodoValePedgUserCad: TIntegerField;
    QrFrtMnfMoRodoValePedgUserAlt: TIntegerField;
    QrFrtMnfMoRodoValePedgAlterWeb: TSmallintField;
    QrFrtMnfMoRodoValePedgAtivo: TSmallintField;
    QrMDFeArq: TmySQLQuery;
    QrMDFeArqIDCtrl_CabA: TIntegerField;
    QrMDFeArqIDCtrl_Arq: TIntegerField;
    QrMDFeArqId: TWideStringField;
    QrMDFeArqFatID: TIntegerField;
    QrMDFeArqFatNum: TIntegerField;
    QrMDFeArqEmpresa: TIntegerField;
    QrMDFeit2InfMunCarrega: TmySQLQuery;
    QrMDFeit2InfMunCarregaFatID: TIntegerField;
    QrMDFeit2InfMunCarregaFatNum: TIntegerField;
    QrMDFeit2InfMunCarregaEmpresa: TIntegerField;
    QrMDFeit2InfMunCarregaControle: TIntegerField;
    QrMDFeit2InfMunCarregacMunCarrega: TIntegerField;
    QrMDFeit2InfMunCarregaxMunCarrega: TWideStringField;
    QrMDFeIt2InfPercurso: TmySQLQuery;
    QrMDFeIt2InfPercursoFatID: TIntegerField;
    QrMDFeIt2InfPercursoFatNum: TIntegerField;
    QrMDFeIt2InfPercursoEmpresa: TIntegerField;
    QrMDFeIt2InfPercursoControle: TIntegerField;
    QrMDFeIt2InfPercursoUFPer: TWideStringField;
    QrMDFeIt2InfMunDescarga: TmySQLQuery;
    QrMDFeIt3InfCTe: TmySQLQuery;
    QrMDFeIt3InfNFe: TmySQLQuery;
    QrMDFeIt3InfMDFe: TmySQLQuery;
    QrMDFeIt2InfMunDescargaFatID: TIntegerField;
    QrMDFeIt2InfMunDescargaFatNum: TIntegerField;
    QrMDFeIt2InfMunDescargaEmpresa: TIntegerField;
    QrMDFeIt2InfMunDescargaControle: TLargeintField;
    QrMDFeIt2InfMunDescargacMunDescarga: TIntegerField;
    QrMDFeIt2InfMunDescargaxMunDescarga: TWideStringField;
    QrMDFeIt3InfCTeFatID: TIntegerField;
    QrMDFeIt3InfCTeFatNum: TIntegerField;
    QrMDFeIt3InfCTeEmpresa: TIntegerField;
    QrMDFeIt3InfCTeControle: TLargeintField;
    QrMDFeIt3InfCTeConta: TLargeintField;
    QrMDFeIt3InfCTechCTe: TWideStringField;
    QrMDFeIt3InfCTeSegCodBarra: TWideStringField;
    QrMDFeIt3InfNFeFatID: TIntegerField;
    QrMDFeIt3InfNFeFatNum: TIntegerField;
    QrMDFeIt3InfNFeEmpresa: TIntegerField;
    QrMDFeIt3InfNFeControle: TLargeintField;
    QrMDFeIt3InfNFeConta: TLargeintField;
    QrMDFeIt3InfNFechNFe: TWideStringField;
    QrMDFeIt3InfNFeSegCodBarra: TWideStringField;
    QrMDFeIt3InfMDFeFatID: TIntegerField;
    QrMDFeIt3InfMDFeFatNum: TIntegerField;
    QrMDFeIt3InfMDFeEmpresa: TIntegerField;
    QrMDFeIt3InfMDFeControle: TLargeintField;
    QrMDFeIt3InfMDFeConta: TLargeintField;
    QrMDFeIt3InfMDFechMDFe: TWideStringField;
    QrMDFeIt1Lacres: TmySQLQuery;
    QrMDFeIt1AutXml: TmySQLQuery;
    QrMDFeIt1LacresFatID: TIntegerField;
    QrMDFeIt1LacresFatNum: TIntegerField;
    QrMDFeIt1LacresEmpresa: TIntegerField;
    QrMDFeIt1LacresControle: TIntegerField;
    QrMDFeIt1LacresnLacre: TWideStringField;
    QrMDFeIt1AutXmlFatID: TIntegerField;
    QrMDFeIt1AutXmlFatNum: TIntegerField;
    QrMDFeIt1AutXmlEmpresa: TIntegerField;
    QrMDFeIt1AutXmlControle: TIntegerField;
    QrMDFeIt1AutXmlAddForma: TSmallintField;
    QrMDFeIt1AutXmlAddIDCad: TIntegerField;
    QrMDFeIt1AutXmlTipo: TSmallintField;
    QrMDFeIt1AutXmlCNPJ: TWideStringField;
    QrMDFeIt1AutXmlCPF: TWideStringField;
    QrMDFeMoRodoCab: TmySQLQuery;
    QrMDFeMoRodoCabFatID: TIntegerField;
    QrMDFeMoRodoCabFatNum: TIntegerField;
    QrMDFeMoRodoCabEmpresa: TIntegerField;
    QrMDFeMoRodoCabControle: TIntegerField;
    QrMDFeMoRodoCabRNTRC: TWideStringField;
    QrMDFeMoRodoCabveicTracao_cInt: TIntegerField;
    QrMDFeMoRodoCabveicTracao_placa: TWideStringField;
    QrMDFeMoRodoCabveicTracao_RENAVAM: TWideStringField;
    QrMDFeMoRodoCabveicTracao_tara: TIntegerField;
    QrMDFeMoRodoCabveicTracao_capKg: TIntegerField;
    QrMDFeMoRodoCabveicTracao_capM3: TIntegerField;
    QrMDFeMoRodoCabveicTracao_tpRod: TSmallintField;
    QrMDFeMoRodoCabveicTracao_tpCar: TSmallintField;
    QrMDFeMoRodoCabveicTracao_UF: TWideStringField;
    QrMDFeMoRodoCabcodAgPorto: TWideStringField;
    QrMDFeMoRodoPropT: TmySQLQuery;
    QrMDFeMoRodoPropR: TmySQLQuery;
    QrMDFeMoRodoCondutor: TmySQLQuery;
    QrMDFeMoRodoVeicRebo: TmySQLQuery;
    QrMDFeMoRodoVPed: TmySQLQuery;
    QrMDFeMoRodoVPedFatID: TIntegerField;
    QrMDFeMoRodoVPedFatNum: TIntegerField;
    QrMDFeMoRodoVPedEmpresa: TIntegerField;
    QrMDFeMoRodoVPedControle: TIntegerField;
    QrMDFeMoRodoVPedConta: TIntegerField;
    QrMDFeMoRodoVPedCNPJForn: TWideStringField;
    QrMDFeMoRodoVPedCNPJPg: TWideStringField;
    QrMDFeMoRodoVPednCompra: TWideStringField;
    QrMDFeMoRodoVeicReboFatID: TIntegerField;
    QrMDFeMoRodoVeicReboFatNum: TIntegerField;
    QrMDFeMoRodoVeicReboEmpresa: TIntegerField;
    QrMDFeMoRodoVeicReboControle: TIntegerField;
    QrMDFeMoRodoVeicReboConta: TIntegerField;
    QrMDFeMoRodoVeicRebocInt: TIntegerField;
    QrMDFeMoRodoVeicReboplaca: TWideStringField;
    QrMDFeMoRodoVeicReboRENAVAM: TWideStringField;
    QrMDFeMoRodoVeicRebotara: TIntegerField;
    QrMDFeMoRodoVeicRebocapKG: TIntegerField;
    QrMDFeMoRodoVeicRebocapM3: TIntegerField;
    QrMDFeMoRodoVeicRebotpCar: TSmallintField;
    QrMDFeMoRodoVeicReboUF: TWideStringField;
    QrMDFeMoRodoCondutorFatID: TIntegerField;
    QrMDFeMoRodoCondutorFatNum: TIntegerField;
    QrMDFeMoRodoCondutorEmpresa: TIntegerField;
    QrMDFeMoRodoCondutorControle: TIntegerField;
    QrMDFeMoRodoCondutorConta: TIntegerField;
    QrMDFeMoRodoCondutorxNome: TWideStringField;
    QrMDFeMoRodoCondutorCPF: TWideStringField;
    QrMDFeMoRodoPropRFatID: TIntegerField;
    QrMDFeMoRodoPropRFatNum: TIntegerField;
    QrMDFeMoRodoPropREmpresa: TIntegerField;
    QrMDFeMoRodoPropRControle: TIntegerField;
    QrMDFeMoRodoPropRConta: TIntegerField;
    QrMDFeMoRodoPropRCPF: TWideStringField;
    QrMDFeMoRodoPropRCNPJ: TWideStringField;
    QrMDFeMoRodoPropRRNTRC: TWideStringField;
    QrMDFeMoRodoPropRxNome: TWideStringField;
    QrMDFeMoRodoPropRIE: TWideStringField;
    QrMDFeMoRodoPropRUF: TWideStringField;
    QrMDFeMoRodoPropRtpProp: TSmallintField;
    QrMDFeMoRodoPropTFatID: TIntegerField;
    QrMDFeMoRodoPropTFatNum: TIntegerField;
    QrMDFeMoRodoPropTEmpresa: TIntegerField;
    QrMDFeMoRodoPropTControle: TIntegerField;
    QrMDFeMoRodoPropTConta: TIntegerField;
    QrMDFeMoRodoPropTCPF: TWideStringField;
    QrMDFeMoRodoPropTCNPJ: TWideStringField;
    QrMDFeMoRodoPropTRNTRC: TWideStringField;
    QrMDFeMoRodoPropTxNome: TWideStringField;
    QrMDFeMoRodoPropTIE: TWideStringField;
    QrMDFeMoRodoPropTUF: TWideStringField;
    QrMDFeMoRodoPropTtpProp: TSmallintField;
    QrMDFeMoRodoCIOT: TmySQLQuery;
    QrFrtMnfMoRodoCIOT: TmySQLQuery;
    QrFrtMnfMoRodoCIOTCodigo: TIntegerField;
    QrFrtMnfMoRodoCIOTControle: TIntegerField;
    QrFrtMnfMoRodoCIOTCIOT: TLargeintField;
    QrMDFeMoRodoCIOTFatID: TIntegerField;
    QrMDFeMoRodoCIOTFatNum: TIntegerField;
    QrMDFeMoRodoCIOTEmpresa: TIntegerField;
    QrMDFeMoRodoCIOTControle: TIntegerField;
    QrMDFeMoRodoCIOTConta: TIntegerField;
    QrMDFeMoRodoCIOTCIOT: TLargeintField;
    QrMDFeLEnC: TmySQLQuery;
    QrMDFeLEnCId: TWideStringField;
    QrMDFeLEnCFatID: TIntegerField;
    QrMDFeLEnCFatNum: TIntegerField;
    QrOpcoesMDFeCNPJ_CPF: TWideStringField;
    QrAtoArq: TmySQLQuery;
    QrAtoArqIDCtrl: TIntegerField;
    QrMDFeAut: TmySQLQuery;
    QrMDFeAutIDCtrl: TIntegerField;
    QrMDFeAutLoteEnv: TIntegerField;
    QrMDFeAutId: TWideStringField;
    QrMDFeCan: TmySQLQuery;
    QrMDFeCanIDCtrl: TIntegerField;
    QrMDFeCanId: TWideStringField;
    QrMDFeInu: TmySQLQuery;
    QrMDFeInuCodigo: TIntegerField;
    QrMDFeInuCodUsu: TIntegerField;
    QrMDFeInuNome: TWideStringField;
    QrMDFeInuEmpresa: TIntegerField;
    QrMDFeInuDataC: TDateField;
    QrMDFeInuversao: TFloatField;
    QrMDFeInuId: TWideStringField;
    QrMDFeInutpAmb: TSmallintField;
    QrMDFeInucUF: TSmallintField;
    QrMDFeInuano: TSmallintField;
    QrMDFeInuCNPJ: TWideStringField;
    QrMDFeInumodelo: TSmallintField;
    QrMDFeInuSerie: TIntegerField;
    QrMDFeInunMDFIni: TIntegerField;
    QrMDFeInunMDFFim: TIntegerField;
    QrMDFeInuJustif: TIntegerField;
    QrMDFeInuxJust: TWideStringField;
    QrMDFeInucStat: TIntegerField;
    QrMDFeInuxMotivo: TWideStringField;
    QrMDFeInudhRecbto: TDateTimeField;
    QrMDFeInunProt: TWideStringField;
    QrMDFeInuLk: TIntegerField;
    QrMDFeInuDataCad: TDateField;
    QrMDFeInuDataAlt: TDateField;
    QrMDFeInuUserCad: TIntegerField;
    QrMDFeInuUserAlt: TIntegerField;
    QrMDFeInuAlterWeb: TSmallintField;
    QrMDFeInuAtivo: TSmallintField;
    QrMDFeInuXML_Inu: TWideMemoField;
    QrOpcoesMDFeMDFeVerNaoEnc: TFloatField;
    QrEveTp: TmySQLQuery;
    QrEveTpnSeqEvento: TSmallintField;
    QrFilialDirMDFeEvePedIdC: TWideStringField;
    QrFilialDirMDFeEveRetIdC: TWideStringField;
    QrEveIts: TmySQLQuery;
    QrEveItsControle: TIntegerField;
    QrEveSub: TmySQLQuery;
    QrEveSubSubCtrl: TIntegerField;
    QrMDFeCabAinfEnc_verAplic: TWideStringField;
    QrMDFeCabAinfEnc_cOrgao: TSmallintField;
    QrMDFeCabAinfEnc_tpAmb: TSmallintField;
    QrMDFeCabAinfEnc_CNPJ: TWideStringField;
    QrMDFeCabAinfEnc_chMDFe: TWideStringField;
    QrMDFeCabAinfEnc_tpEvento: TIntegerField;
    QrMDFeCabAinfEnc_nSeqEvento: TIntegerField;
    QrMDFeCabAinfEnc_verEvento: TFloatField;
    QrMDFeCabAinfEnc_dtEnc: TDateField;
    QrMDFeCabAinfEnc_cUF: TSmallintField;
    QrMDFeCabAinfEnc_cMun: TIntegerField;
    QrMDFeCabAinfEnc_cStat: TIntegerField;
    QrMDFeCabAinfEnc_dhRegEvento: TDateTimeField;
    QrMDFeCabAinfEnc_nProt: TWideStringField;
    QrMDFeCabAinfEnc_Id: TWideStringField;
    QrMDFeEnc: TmySQLQuery;
    QrMDFeEncIDCtrl: TIntegerField;
    QrMDFeEncId: TWideStringField;
    QrArq: TmySQLQuery;
    QrArqFatID: TIntegerField;
    QrArqFatNum: TIntegerField;
    QrArqEmpresa: TIntegerField;
    QrArqIDCtrl: TIntegerField;
    QrArqXML_MDFe: TWideMemoField;
    QrArqXML_Aut: TWideMemoField;
    QrArqXML_Can: TWideMemoField;
    QrArqLk: TIntegerField;
    QrArqDataCad: TDateField;
    QrArqDataAlt: TDateField;
    QrArqUserCad: TIntegerField;
    QrArqAlterWeb: TSmallintField;
    QrArqUserAlt: TIntegerField;
    QrArqAtivo: TSmallintField;
    QrCEnc: TmySQLQuery;
    QrCEncinfEnc_cStat: TIntegerField;
    QrCEncinfProt_cStat: TIntegerField;
    QrCEncStatus: TIntegerField;
    procedure QrOpcoesMDFeAfterOpen(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure QrFrtMnfInfDocCMunAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    FMsg: String;
    FLastVerMDFeLayI: Double;
  public
    { Public declarations }
    procedure AbreXML_NoNavegadorPadrao(Empresa: Integer; Ext, Arq: String;
              Assinado: Boolean);
    procedure AbreXML_NoTWebBrowser(Empresa: Integer; Ext, Arq: String;
              Assinado: Boolean; WebBrowser: TWebBrowser);
    function  AssinarArquivoXML(XMLGerado_Arq, XMLGerado_Dir: String; Empresa,
              TipoDoc, IDCtrl: Integer; var XMLAssinado_Dir: String): Boolean;
    function  AtualizaXML_No_BD_Aut(const IDCtrl, LoteEnv: Integer; const Id:
              String; var Dir, Aviso: String): Boolean;
    function  AtualizaXML_No_BD_Can(const IDCtrl: Integer; const Id:
              String; var Dir, Aviso: String): Boolean;
    function  AtualizaXML_No_BD_ConsultaMDFe(const Chave, Id: String;
              const IDCtrl: Integer; var Dir, Aviso: String): Boolean;
    function  AtualizaXML_No_BD_Enc(const IDCtrl: Integer; const Id:
              String; var Dir, Aviso: String): Boolean;
    function  AtualizaXML_No_BD_MDFe(const IDCtrl: Integer; var Dir, Aviso:
              String): Boolean;
    function  AtualizaXML_No_BD_Inu(const Codigo: Integer; var Dir, Aviso:
              String): Boolean;
    function  AtualizaXML_No_BD_Tudo(InfoTermino: Boolean): Boolean;
    procedure ColoreStatusMDFeEmDBGrid(const DBGrid: TDBGrid;
              const Rect: TRect; DataCol: Integer; Column: TColumn; State:
              TGridDrawState; const Status: Integer);
    procedure ColoreSitConfMDFeEmDBGrid(const DBGrid: TDBGrid;
              const Rect: TRect; DataCol: Integer; Column: TColumn; State:
              TGridDrawState; const SitConf: Integer);
    function  DefDomI(Grupo, Codigo, Valor: Integer): Integer;
    function  DefDomX(Grupo, Codigo: Integer; Valor: String): String;
    procedure DefineSituacaoEncerramento(ChaveMDFe: String);
    function  ERf(Grupo, Codigo: Integer; Valor: Double; Casas: Integer): Double;
    function  ERi(Grupo, Codigo, Valor: Integer): Integer;
    function  ERi64(Grupo, Codigo: Integer; Valor: Int64): Int64;
    function  ERx(Grupo, Codigo: Integer; Valor: String): String;
    function  EventoObtemCtrl(EventoLote, tpEvento, nSeqEvento: Integer;
              chMDFe: String): Integer;
    procedure EventoObtemSub(const Controle: Integer; const IdLote: String;
              var SubCtrl: Integer; var SQLType: TSQLType);
    function  Evento_Obtem_nSeqEvento(FatID, FatNum, Empresa, tpEvento: Integer):
              Integer;
    function  ExcluiDadosTabelasMDFe(FatID, FatNum: Integer): Boolean;
    function  FormataLoteMDFe(Lote: Integer): String;
    function  InsereDadosDeFaturaNasTabelasMDFe(FatID, FatNum, Empresa, SerieMDFe,
              NumeroMDFe: Integer; DataEmi: TDateTime; HoraFat: TTime;
              LaAviso1, LaAviso2: TLabel): Boolean;
    function  InsereDadosMDFeCab(const FatID, FatNum, Empresa, IDCtrl_Atual,
              VersaoMDFe, SerieMDFe, NumeroMDFe: Integer; Recria: Boolean;
              DataEmi: TDateTime; HoraFat: TTime; var IDCtrl: Integer): Boolean;
    function  LocalizaMDFeInfoEveCan(const ChMDFe: String; var Id,
              xJust: String; var cJust: Integer): Boolean;
    function  LocalizaMDFeInfoEveEnc(const ChMDFe: String; var nProt: String;
              var dtEnc: TDateTime; var cUF, cMun: Integer; CNPJ: String): Boolean;
    function  LocalizaMDFeInfoEveIdC(const ChMDFe: String; var CNPJ,
              xNome, CPF: String): Boolean;
    function  MensagemDeID_MDFe(Grupo, Codigo: Integer; Valor, Texto:
              String; Seq_Chamou: Integer): String;
    function  Obtem_Arquivo_XML_(Empresa: Integer; Ext, Arq: String; Assinado: Boolean): String;
    function  ObtemDirXML(const Ext: String; var Dir: String; Assinado:
              Boolean): Boolean;

    procedure ReopenEntiMDFe(Entidade: Integer);
    procedure ReopenFrtMnfMunCarrega(Codigo: Integer);
    procedure ReopenFrtMnfInfPercurso(Codigo: Integer);
    //procedure ReopenFrtMnfInfDoc(Codigo: Integer);
    procedure ReopenFrtMnfDocCMun(Codigo: Integer);
    procedure ReopenFrtMnfLacres(Codigo: Integer);
    procedure ReopenFrtMnfAutXML(Codigo: Integer);
    procedure ReopenFrtMnfMoRodoCondutor(Codigo: Integer);
    procedure ReopenFrtMnfMoRodoValePedg(Codigo: Integer);
    procedure ReopenFrtMnfMoRodoVeicRebo(Codigo: Integer);
    procedure ReopenFrtMnfMoRodoCIOT(Codigo: Integer);
    //
    procedure ReopenMDFeArq(IDCtrl: Integer);
    function  ReopenMDFeCabA(FatID, FatNum, Empresa: Integer): Integer;
    procedure ReopenMDFeit2InfMunCarrega(FatID, FatNum, Empresa: Integer);
    procedure ReopenMDFeit2InfPercurso(FatID, FatNum, Empresa: Integer);
    procedure ReopenMDFeit2InfMunDescarga(FatID, FatNum, Empresa: Integer);
    procedure ReopenMDFeit3InfCTe(FatID, FatNum, Empresa: Integer;
              Controle: Int64);
    procedure ReopenMDFeit3InfNFe(FatID, FatNum, Empresa: Integer;
              Controle: Int64);
    procedure ReopenMDFeit3InfMDFe(FatID, FatNum, Empresa: Integer;
              Controle: Int64);
    procedure ReopenMDFeit1Lacres(FatID, FatNum, Empresa: Integer);
    procedure ReopenMDFeit1AutXml(FatID, FatNum, Empresa: Integer);
    procedure ReopenMDFeMoRodoCab(FatID, FatNum, Empresa: Integer);
    procedure ReopenMDFeMoRodoPropT(FatID, FatNum, Empresa: Integer);
    procedure ReopenMDFeMoRodoPropR(FatID, FatNum, Empresa, Controle: Integer);
    procedure ReopenMDFeMoRodoCondutor(FatID, FatNum, Empresa: Integer);
    procedure ReopenMDFeMoRodoVeicRebo(FatID, FatNum, Empresa: Integer);
    procedure ReopenMDFeMoRodoVPed(FatID, FatNum, Empresa: Integer);
    procedure ReopenMDFeMoRodoCIOT(FatID, FatNum, Empresa: Integer);
    //
    procedure ReopenMDFeLayI();
    function  ReopenEmpresa(Empresa: Integer): Boolean;
    function  ReopenOpcoesMDFe(QrOpMDFe: TmySQLQuery; Empresa: Integer; Avisa:
              Boolean): Boolean;
    procedure ReopenTerceiro(Codigo: Integer);
    procedure ReopenTrnsCad(Codigo: Integer);
    procedure ReopenTrnsIts(Controle: Integer);
    function  SalvaXML(Ext, Arq: String; Texto: String; rtfRetWS:
              TMemo; Assinado: Boolean): String;
    function  StepMDFeCab(FatID, FatNum, Empresa: Integer; Status:
              TMDFeMyStatus; LaAviso1, LaAviso2: TLabel): Boolean;
    function  TituloArqXML(const Ext: String; var Titulo, Descri: String):
              Boolean;
    function  VerificaTabelaLayI(): Boolean;
  end;

var
  DmMDFe_0000: TDmMDFe_0000;
  //
  sVersaoConsumoWS_MDFe: array[0..MaxTipoConsumoWS_MDFe] of string;

implementation

uses DmkDAC_PF, Module, UMySQLModule, MyDBCheck, ModuleGeral;

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

{ TDmMDFe_0000 }

procedure TDmMDFe_0000.AbreXML_NoNavegadorPadrao(Empresa: Integer; Ext,
  Arq: String; Assinado: Boolean);
var
  Dir, Arquivo: String;
  PWC: array[0..127] of WideChar;
  Tam: Integer;
begin
  if not ReopenEmpresa(Empresa) then Exit;
  if ObtemDirXML(Ext, Dir, Assinado) then
  begin
    Arquivo := Dir + Arq + Ext;
    Tam := Length(Arquivo) + 1;
    StringToWideChar(Arquivo, PWC, Tam);
    try
      HlinkNavigateString(nil, PWC);
    finally
      //
    end;
  end;
end;

procedure TDmMDFe_0000.AbreXML_NoTWebBrowser(Empresa: Integer; Ext, Arq: String;
  Assinado: Boolean; WebBrowser: TWebBrowser);
var
  Dir, Arquivo: String;
begin
  if not ReopenEmpresa(Empresa) then Exit;
  if ObtemDirXML(Ext, Dir, Assinado) then
  begin
    Arquivo := Dir + Arq + Ext;
    WebBrowser.Navigate(Arquivo);
  end;
end;

function TDmMDFe_0000.AssinarArquivoXML(XMLGerado_Arq, XMLGerado_Dir: String;
  Empresa, TipoDoc, IDCtrl: Integer; var XMLAssinado_Dir: String): Boolean;
var
  fEntrada: file;
  NumRead, i: integer;
  buf: char;
  xmldoc, ref: String;
  Ext, FonteXML: String;
  //
  NumeroSerial: String;
  Cert: ICertificate2;
  Assinada: String;
  Aviso, Dir: String;
begin
  Result := False;
  // Abrir    Arquivo := XMLGerado_Dir + XMLGerado_Arq;
  if not Geral.VerificaDir(XMLGerado_Dir, '\', 'XML gerado', True) then Exit;
  FonteXML := XMLGerado_Dir + XMLGerado_Arq;
  if not FileExists(FonteXML) then
  begin
    Geral.MB_Aviso('Arquivo n�o encontrado: ' + FonteXML);
    Exit;
  end;
  //
  AssignFile(fEntrada, XMLGerado_Dir + XMLGerado_Arq);
  Reset(fEntrada, 1);
  xmlDoc := '';

  for i := 1 to FileSize(fEntrada) do
  begin
    Blockread(fEntrada, buf, 1, NumRead);
    xmlDoc := xmlDoc + buf;
  end;

  CloseFile(fEntrada);

  //

  if xmlDoc <> '' then
  begin
    ReopenEmpresa(Empresa);
    if QrFilialMDFeSerNum.Value <> '' then
    begin
      NumeroSerial := QrFilialMDFeSerNum.Value;
      //
      case TipoDoc of
        0: ref := 'infMDFe';
        //1: ref := 'infCanc';
        //2: ref := 'infInut';
        else
        begin
          Geral.MB_Erro('"ref" indefinido. "TipoDoc" n�o implementado!');
          ref := '???';
        end;
      end;

      case TipoDoc of
        0: Ext := MDFE_EXT_MDFE_XML;
        //1: Ext := MDFE_EXT_CAN_XML;
        //2: Ext := MDFE_EXT_INU_XML;
        else
        begin
          Geral.MB_Erro('"Ext" indefinido. "TipoDoc" n�o implementado!');
          Ext := '???';
        end;
      end;
      if XXe_PF.ObtemCertificado(NumeroSerial, Cert) then
      begin
        Result := MDFe_PF.AssinarMSXML(xmlDoc, Cert, Assinada);
        if Result then
        begin
          SalvaXML(Ext, XMLGerado_Arq,  Assinada, nil, True);
          if TipoDoc = 0 then
          begin
            DModG.ReopenParamsEmp(Empresa);
            Dir := QrOpcoesMDFeDirMDFeAss.Value;
            Aviso := '';
            AtualizaXML_No_BD_MDFe(IDCtrl, Dir, Aviso);
            if Aviso <> '' then Geral.MB_Aviso(
            'O arquivo abaixo n�o foi localizado para ser adicionado ao banco de dados:'
             + sLineBreak + Aviso);
          end;
        end else
          Exit;
      end;// else Result := False;
    end
    else
      Geral.MB_Aviso('Serial do Certificado n�o informado...');
  end
  else
    Geral.MB_Aviso('Documento XML para assinatura n�o informado...');
  Result := True;
end;

function TDmMDFe_0000.AtualizaXML_No_BD_Aut(const IDCtrl, LoteEnv: Integer;
  const Id: String; var Dir, Aviso: String): Boolean;
var
  Arq, DirArq, Buf, Prot: String;
  XML_Aut: String;
  Texto: TStringList;
  XML_LoteEnv: PChar;
  //FatID, FatNum, Empresa,
  Ini, Fim: Integer;
begin
  Arq := FormataLoteMDFe(LoteEnv) + MDFE_EXT_PRO_REC_XML;
  Geral.VerificaDir(Dir, '\', 'Lote de MDFe', True);
  DirArq := Dir + Arq;
  if not FileExists(DirArq) then
  begin
    Aviso := Aviso + DirArq + sLineBreak;
  end else begin
    buf := '';
    Texto := TStringList.Create;
    Texto.Clear;
    Texto.LoadFromFile(DirArq);
    XML_LoteEnv := PChar(Texto.Text);
    buf := Copy(XML_LoteEnv, 1);
    Ini := pos('<protMDFe', buf);
    while Ini > 0 do
    begin
      Prot := '';
      Geral.SeparaPrimeiraOcorrenciaDeTexto('<protMDFe', buf, Prot, buf);
      if Prot <> '' then
      begin
        Fim := pos('</protMDFe>', buf);
        XML_Aut := '<' + Copy(buf, 1, Fim + Length('</protMDFe>') - 1);
        XML_Aut := Geral.WideStringToSQLString(XML_Aut);
        if (pos(Id, XML_Aut) > 0) and (pos('<cStat>100</cStat>', XML_Aut) > 0) then
        begin
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'MDFeArq', False, [
          'XML_Aut'], ['IDCtrl'], [XML_Aut], [IDCtrl], True);
        end;
      end;
      Ini := pos('<protMDFe', buf);
    end;
    Texto.Free;
  end;
  Result := True;
end;

function TDmMDFe_0000.AtualizaXML_No_BD_Can(const IDCtrl: Integer;
  const Id: String; var Dir, Aviso: String): Boolean;
var
  Arq, DirArq, Buf: String;
  XML_Can: String;
  Texto: TStringList;
  XML_All: PChar;
  Ini: Integer;
begin
  //if QrMDFeArqIDCtrl_Arq.Value = 0 then
  //begin
    Arq := Id + MDFE_EXT_EVE_RET_CAN_XML;
    Geral.VerificaDir(Dir, '\', 'MDFe cancelada', True);
    DirArq := Dir + Arq;
    if not FileExists(DirArq) then
    begin
      Aviso := Aviso + DirArq + sLineBreak;
    end else begin
      buf := '';
      Texto := TStringList.Create;
      Texto.Clear;
      Texto.LoadFromFile(DirArq);
      XML_All := PChar(Texto.Text);
      buf          := Copy(XML_All, 1);
      //Ini          := pos('<retCancMDFe', buf);
      Ini          := pos('<retEventoMDFe', buf);
      XML_Can      := Copy(buf, Ini);
      XML_Can      := Geral.WideStringToSQLString(XML_Can);
      //
      //Geral.MensagemBox(XML_Can, 'Ativo', MB_OK+MB_ICONWARNING);
      if XML_Can <> '' then
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfearq', False, [
      'XML_Can'], ['IDCtrl'], [XML_Can ], [ IDCtrl ], True);

      Texto.Free;
    end;
  //end;
  Result := True;
end;

function TDmMDFe_0000.AtualizaXML_No_BD_ConsultaMDFe(const Chave, Id: String;
  const IDCtrl: Integer; var Dir, Aviso: String): Boolean;
var
  Arq, DirArq, Buf, Prot: String;
  XML_Sit: String;
  Texto: TStringList;
  XML_ConsultaMDFe: PChar;
  //FatID, FatNum, Empresa,
  Ini, Fim: Integer;
begin
  Arq := Chave + MDFE_EXT_SIT_XML;
  Geral.VerificaDir(Dir, '\', 'Situa��o de MDFe', True);
  DirArq := Dir + Arq;
  if not FileExists(DirArq) then
  begin
    Aviso := Aviso + DirArq + sLineBreak;
  end else begin
    buf := '';
    Texto := TStringList.Create;
    Texto.Clear;
    Texto.LoadFromFile(DirArq);
    XML_ConsultaMDFe := PChar(Texto.Text);
    buf := Copy(XML_ConsultaMDFe, 1);
    Ini := pos('<protMDFe', buf);
    if Ini > 0 then
    begin
      Prot := '';
      Geral.SeparaPrimeiraOcorrenciaDeTexto('<protMDFe', buf, Prot, buf);
      if Prot <> '' then
      begin
        Fim := pos('</protMDFe>', buf);
        XML_Sit := '<' + Copy(buf, 1, Fim + Length('</protMDFe>') - 1);
        XML_Sit := Geral.WideStringToSQLString(XML_Sit);
        //
        // Autorizado
        if (pos(Id, XML_Sit) > 0) and (pos('<cStat>100</cStat>', XML_Sit) > 0) then
        begin
          QrArq.Close;
          QrArq.Params[00].AsInteger := IDCtrl;
          QrArq.Open;
          if QrArqXML_Aut.Value = '' then
          begin
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'MDFeArq', False, [
            'XML_Aut'], ['IDCtrl'], [XML_Sit], [IDCtrl], True);
          end;
        end;
      end;
    end
    else
    begin
      Ini := pos('<retConsSitMDFe', buf);
      if Ini > 0 then
      begin
        Prot := '';
        Geral.SeparaPrimeiraOcorrenciaDeTexto('<retConsSitMDFe', buf, Prot, buf);
        if Prot <> '' then
        begin
          Fim := pos('</retConsSitMDFe>', buf);
          XML_Sit := '<' + Copy(buf, 1, Fim + Length('</retConsSitMDFe>') - 1);
          XML_Sit := Geral.WideStringToSQLString(XML_Sit);
          //
          // Autorizado
          // neste caso o ID � a pr�pria chave
          if ((pos(Id, XML_Sit) > 0) or (pos(Chave, XML_Sit) > 0)) and (pos('<cStat>100</cStat>', XML_Sit) > 0) then
          begin
            QrArq.Close;
            QrArq.Params[00].AsInteger := IDCtrl;
            QrArq.Open;
            if QrArqXML_Can.Value = '' then
            begin
              UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'MDFeArq', False, [
              'XML_Aut'], ['IDCtrl'], [XML_Sit], [IDCtrl], True);
            end;
          end;
          //
          // Cancelado
          // neste caso o ID � a pr�pria chave
          if ((pos(Id, XML_Sit) > 0) or (pos(Chave, XML_Sit) > 0)) and (pos('<cStat>101</cStat>', XML_Sit) > 0) then
          begin
            QrArq.Close;
            QrArq.Params[00].AsInteger := IDCtrl;
            QrArq.Open;
            if QrArqXML_Can.Value = '' then
            begin
              UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'MDFeArq', False, [
              'XML_Can'], ['IDCtrl'], [XML_Sit], [IDCtrl], True);
            end;
          end;
        end;
      end;
    end;
    //
    Texto.Free;
  end;
  Result := True;
end;

function TDmMDFe_0000.AtualizaXML_No_BD_Enc(const IDCtrl: Integer;
  const Id: String; var Dir, Aviso: String): Boolean;
var
  Arq, DirArq, Buf: String;
  XML_Enc: String;
  Texto: TStringList;
  XML_All: PChar;
  Ini: Integer;
begin
  //if QrMDFeArqIDCtrl_Arq.Value = 0 then
  //begin
    Arq := Id + MDFE_EXT_EVE_RET_ENC_XML;
    Geral.VerificaDir(Dir, '\', 'MDFe encerrada', True);
    DirArq := Dir + Arq;
    if not FileExists(DirArq) then
    begin
      Aviso := Aviso + DirArq + sLineBreak;
    end else begin
      buf := '';
      Texto := TStringList.Create;
      Texto.Clear;
      Texto.LoadFromFile(DirArq);
      XML_All := PChar(Texto.Text);
      buf          := Copy(XML_All, 1);
      //Ini          := pos('<retCancMDFe', buf);
      Ini          := pos('<retEventoMDFe', buf);
      XML_Enc      := Copy(buf, Ini);
      XML_Enc      := Geral.WideStringToSQLString(XML_Enc);
      //
      Geral.MB_Info(XML_Enc);
      if XML_Enc <> '' then
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfearq', False, [
      'XML_Enc'], ['IDCtrl'], [XML_Enc ], [ IDCtrl ], True);

      Texto.Free;
    end;
  //end;
  Result := True;
end;

function TDmMDFe_0000.AtualizaXML_No_BD_Inu(const Codigo: Integer; var Dir,
  Aviso: String): Boolean;
var
  Arq, DirArq, Buf: String;
  Id, XML_Inu: String;
  Texto: TStringList;
  XML_Txt: PChar;
  //FatID, FatNum, Empresa: Integer;
begin
  Geral.MB_Aviso('"TDmMDFe_0000.AtualizaXML_No_BD_Inu()" n�o implementado!');
{
  MontaID_Inutilizacao(FormatFloat('00', QrMDFeInucUF.Value),
  FormatFloat('00', QrMDFeInuano.Value), QrMDFeInuCNPJ.Value,
  FormatFloat('00', QrMDFeInumodelo.Value),
  FormatFloat('0', QrMDFeInuSerie.Value),
  FormatFloat('000000000', QrMDFeInunMDFIni.Value),
  FormatFloat('000000000', QrMDFeInunMDFFim.Value), Id);
  //
  Arq := Id + MDFE_EXT_INU_XML;
  Geral.VerificaDir(Dir, '\', 'MDFe inutilizada', True);
  DirArq := Dir + Arq;
  if not FileExists(DirArq) then
  begin
    Aviso := Aviso + DirArq + sLineBreak;
  end else begin
    buf := '';
    Texto := TStringList.Create;
    Texto.Clear;
    Texto.LoadFromFile(DirArq);
    XML_Txt      := PChar(Texto.Text);
    buf          := Copy(XML_Txt, 1);
    XML_Inu      := Geral.WideStringToSQLString(buf);
    //
    if XML_Inu <> '' then

    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfeinut', False, [
    'XML_Inu'], ['Codigo'], [XML_Inu ], [Codigo], True);
    Texto.Free;
  end;
  Result := true;
}
end;

function TDmMDFe_0000.AtualizaXML_No_BD_MDFe(const IDCtrl: Integer; var Dir,
  Aviso: String): Boolean;
var
  Arq, DirArq, Buf: String;
  XML_MDFe: String;
  Texto: TStringList;
  XML_Assinado: PChar;
  FatID, FatNum, Empresa: Integer;
  //
  SQLType: TSQLType;
begin
  ReopenMDFeArq(IDCtrl);
  //
  Arq := QrMDFeArqId.Value + MDFE_EXT_MDFE_XML;
  Geral.VerificaDir(Dir, '\', 'MDFe assinada', True);
  DirArq := Dir + Arq;
  if not FileExists(DirArq) then
  begin
    Aviso := Aviso + DirArq + sLineBreak;
  end else begin
    buf := '';
    Texto := TStringList.Create;
    Texto.Clear;
    Texto.LoadFromFile(DirArq);
    XML_Assinado := PChar(Texto.Text);
    buf          := Copy(XML_Assinado, 1);
    XML_MDFe      := Geral.WideStringToSQLString(buf);
    //
    FatID        := QrMDFeArqFatID.Value;
    FatNum       := QrMDFeArqFatNum.Value;
    Empresa      := QrMDFeArqEmpresa.Value;
    //
    if QrMDFeArqIDCtrl_Arq.Value = 0 then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM mdfearq WHERE IDCtrl=:P0');
      Dmod.QrUpd.Params[00].AsInteger := IDCtrl;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM mdfearq ');
      Dmod.QrUpd.SQL.Add('WHERE FatID=:P0 AND FatNum=:P1 AND Empresa=:P2');
      Dmod.QrUpd.Params[00].AsInteger := FatID;
      Dmod.QrUpd.Params[01].AsInteger := FatNum;
      Dmod.QrUpd.Params[02].AsInteger := Empresa;
      Dmod.QrUpd.ExecSQL;
      //
      SQLType := stIns;
    end else begin
      SQLType := stUpd;
    end;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'mdfearq', False, [
    'IDCtrl', 'XML_MDFe'], ['FatID', 'FatNum', 'Empresa'], [
     IDCtrl,   XML_MDFe ], [ FatID,   FatNum,   Empresa ], True);
    Texto.Free;
  end;
  Result := True;
end;

function TDmMDFe_0000.AtualizaXML_No_BD_Tudo(InfoTermino: Boolean): Boolean;
var
  Aviso, CNPJ_CPF, Dir: String;
begin
  Screen.Cursor := crHourGlass;
  //
  if VAR_LIB_EMPRESA_SEL = 0 then
  begin
    Geral.MB_Aviso('Empresa logada n�o definida!');
    Result := False;
    Exit;
  end;
  //
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  if not ReopenOpcoesMDFe(QrOpcoesMDFe, VAR_LIB_EMPRESA_SEL, True) then Exit;
  CNPJ_CPF := QrOpcoesMDFeCNPJ_CPF.Value;
  if CNPJ_CPF = '' then
  begin
    Geral.MB_Aviso('CNPJ ou CPF da Empresa logada n�o definido!');
    Result := False;
    Exit;
  end;
  Aviso := '';

  //

{ TODO -cMDFe : ????TODO  }
  // Permitir cliente editar nfearq e dizer que nao quer mais que procure o XML
  Dir := QrOpcoesMDFeDirMDFeAss.Value;
  UnDmkDAC_PF.AbreMySQLQuery0(QrAtoArq, Dmod.MyDB, [
  'SELECT caba.IDCtrl ',
  'FROM mdfecaba caba ',
  'LEFT JOIN mdfearq arq ON arq.IDCtrl=caba.IDCtrl ',
  'WHERE arq.IDCtrl IS NULL ',
  'AND Id <> "" ',
  'AND emit_CNPJ="' + CNPJ_CPF + '" ',
(*
  'AND ( ',
  '  emit_CNPJ="' + CNPJ_CPF + '" ',
  '  OR ',
  '  emit_CPF="' + CNPJ_CPF + '" ',
  ') ',
*)
  '']);
  //
  QrAtoArq.First;
  while not QrAtoArq.Eof do
  begin
    AtualizaXML_No_BD_MDFe(QrAtoArqIDCtrl.Value, Dir, Aviso);
    //
    QrAtoArq.Next;
  end;

  //

  Dir := QrOpcoesMDFeDirMDFeProRec.Value;
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeAut, Dmod.MyDB, [
  'SELECT caba.IDCtrl, caba.LoteEnv, caba.Id' ,
  'FROM mdfecaba caba' ,
  'LEFT JOIN mdfearq arq ON arq.IDCtrl=caba.IDCtrl' ,
  'WHERE arq.IDCtrl IS NOT NULL' ,
  'AND caba.infProt_cStat=100' ,
  'AND arq.XML_Aut IS NULL' ,
  'AND emit_CNPJ="' + CNPJ_CPF + '" ',
  (*'AND (' ,
  '  emit_CNPJ=:P0' ,
  '  OR' ,
  '  emit_CPF=:P1' ,
  ')' ,*)
  '']);
  //
  QrMDFeAut.First;
  while not QrMDFeAut.Eof do
  begin
    AtualizaXML_No_BD_Aut(QrMDFeAutIDCtrl.Value, QrMDFeAutLoteEnv.Value,
      QrMDFeAutId.Value, Dir, Aviso);
    //
    QrMDFeAut.Next;
  end;

  //

  Dir := QrOpcoesMDFeDirMDFeEveRetCan.Value;
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeCan, Dmod.MyDB, [
  'SELECT caba.IDCtrl, caba.Id' ,
  'FROM mdfecaba caba' ,
  'LEFT JOIN mdfearq arq ON arq.IDCtrl=caba.IDCtrl' ,
  'WHERE arq.IDCtrl IS NOT NULL' ,
  'AND caba.infCanc_cStat=101' ,
  'AND emit_CNPJ="' + CNPJ_CPF + '" ',
  'AND arq.XML_Can="" ',
(*
  'AND (' ,
  '  emit_CNPJ=:P0' ,
  '  OR' ,
  '  emit_CPF=:P1' ,
  ')' ,
*)
  '']);
  //Geral.MB_SQL(nil, QrMDFeCan);
  //
  QrMDFeCan.First;
  while not QrMDFeCan.Eof do
  begin
    AtualizaXML_No_BD_Can(QrMDFeCanIDCtrl.Value, QrMDFeCanId.Value, Dir, Aviso);
    //
    QrMDFeCan.Next;
  end;

  //

  Dir := QrOpcoesMDFeDirMDFeEveRetEnc.Value;
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeEnc, Dmod.MyDB, [
  'SELECT caba.IDCtrl, caba.Id' ,
  'FROM mdfecaba caba' ,
  'LEFT JOIN mdfearq arq ON arq.IDCtrl=caba.IDCtrl' ,
  'WHERE arq.IDCtrl IS NOT NULL' ,
  'AND caba.infEnc_cStat=135' ,
  'AND emit_CNPJ="' + CNPJ_CPF + '" ',
  'AND arq.XML_Enc="" ',
(*
  'AND (' ,
  '  emit_CNPJ=:P0' ,
  '  OR' ,
  '  emit_CPF=:P1' ,
  ')' ,
*)
  '']);
  //Geral.MB_SQL(nil, QrMDFeEnc);
  //
  QrMDFeEnc.First;
  while not QrMDFeEnc.Eof do
  begin
    AtualizaXML_No_BD_Enc(QrMDFeEncIDCtrl.Value, QrMDFeEncId.Value, Dir, Aviso);
    //
    QrMDFeEnc.Next;
  end;

  //
(*
  Dir := QrOpcoesMDFeDirMDFeInu.Value;
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeInu, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfeinut ',
  'WHERE XML_Inu IS NULL ',
  'AND cStat=102 ',
  '']);
  //
  QrMDFeInu.First;
  while not QrMDFeInu.Eof do
  begin
    AtualizaXML_No_BD_Inu(QrMDFeInuCodigo.Value, Dir, Aviso);
    //
    QrMDFeInu.Next;
  end;
*)

  //
  if Aviso <> '' then Geral.MB_Aviso(
  'Os arquivos abaixo n�o foram localizados:' + sLineBreak + Aviso);

  if InfoTermino then
    Geral.MB_Aviso('Verifica��o finalizada');

  Screen.Cursor := crDefault;
  Result := true;
end;

procedure TDmMDFe_0000.ColoreSitConfMDFeEmDBGrid(const DBGrid: TDBGrid;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState;
  const SitConf: Integer);
var
  Txt, Bak: TColor;
begin
  if (Column.FieldName = 'NO_cSitEnc') then
  begin
    //Txt := clWindow;
    //Bak := clBlack;
    case SitConf of
      -1:  begin Txt := clYellow ; Bak := clBlack  end;
      0:   begin Txt := clRed    ; Bak := clWindow end;
      1:   begin Txt := clBlue   ; Bak := clWindow end;
      2:   begin Txt := clFuchsia; Bak := clWindow end;
      else begin Txt := clBlack  ; Bak := clRed;   end;
      (*4:   begin Txt := clBlack  ; Bak := clWindow;end;
      5:   begin Txt := $000080FF; Bak := clWindow end; // Laranja intenso
      else begin Txt := clRed    ; Bak := clWindow end;*)
    end;
    MyObjects.DesenhaTextoEmDBGrid(DBGrid, Rect,
      Txt, Bak, Column.Alignment, Column.Field.DisplayText);
  end;
end;

procedure TDmMDFe_0000.ColoreStatusMDFeEmDBGrid(const DBGrid: TDBGrid;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState;
  const Status: Integer);
var
  Txt, Bak: TColor;
begin
  if (Column.FieldName = 'cStat') then
  begin
    Txt := clWindow;
    Bak := clBlack;
    case Status of
      100:      begin Txt := clBlue   ; Bak := clWindow end;
      101..102: begin Txt := clFuchsia; Bak := clWindow end;
      103..131: begin Txt := clBlack  ; Bak := clWindow end;
      //132:      begin Txt := clGreen  ; Bak := clWindow end;
      132:      begin Txt := $0000AA00; Bak := clWindow end;
      133..199: begin Txt := clBlack  ; Bak := clWindow end;
      201..299: begin Txt := clRed    ; Bak := clWindow end;
      301..399: begin Txt := clWindow ; Bak := clRed;   end;
      401..999: begin Txt := clRed    ; Bak := clWindow end;
    end;
    MyObjects.DesenhaTextoEmDBGrid(DBGrid, Rect,
      Txt, Bak, Column.Alignment, Column.Field.DisplayText);
  end
  else
  if (Column.FieldName = 'NO_cSitMDFe') then
  begin
    //Txt := clWindow;
    //Bak := clBlack;
    case Status of
      //-1,0:begin Txt := clBlack  ; Bak := clWindow end;
      1:   begin Txt := clBlue   ; Bak := clWindow end;
      2:   begin Txt := clFuchsia; Bak := clWindow end;
      3:   begin Txt := clWindow ; Bak := clRed;   end;
      else begin Txt := clRed    ; Bak := clWindow end;
    end;
    MyObjects.DesenhaTextoEmDBGrid(DBGrid, Rect,
      Txt, Bak, Column.Alignment, Column.Field.DisplayText);
  end;
end;

procedure TDmMDFe_0000.DataModuleCreate(Sender: TObject);
begin
  FLastVerMDFeLayI := 0;
end;

function TDmMDFe_0000.DefDomI(Grupo, Codigo, Valor: Integer): Integer;
  function Txt: String;
  begin
    Result := Geral.FFN(Valor, QrMDFeLayITamMin.Value);
  end;
var
  Dominio, Msg, Campo: String;
begin
  Result := Valor;
  if QrMDFeLayI.Locate('Grupo;Codigo', VarArrayOf([Grupo, Codigo]), []) then
  begin
    if (QrMDFeLayIOcorMin.Value > 0) then
    begin
      Dominio := Trim(QrMDFeLayIDominio.Value);
      if Dominio <> '' then
      begin
        Campo := QrMDFeLayICampo.Value;
        //if
        MDFe_PF.VDom(Grupo, Codigo, Campo, Txt, Dominio, FMsg)// = False then
        (*begin
          FMsg := FMsg + 'MDF-e item #' + Geral.FF0(ID) + ' campo "' +
           + '": ' + Msg;
        end;
        *)
      end;
    end;
  end else FMsg := FMsg + 'N�o foi poss�vel definir o item #' +
  Geral.FF0(Grupo) + '.' + Geral.FF0(Codigo) + ' pelo layout da MDF-e! [1]';
end;

function TDmMDFe_0000.DefDomX(Grupo, Codigo: Integer; Valor: String): String;
var
  Dominio, Msg, Campo: String;
begin
  Result := Valor;
  if QrMDFeLayI.Locate('Grupo;Codigo', VarArrayOf([Grupo, Codigo]), []) then
  begin
    if (QrMDFeLayIOcorMin.Value > 0) then
    begin
      Dominio := Trim(QrMDFeLayIDominio.Value);
      if Dominio <> '' then
      begin
        Campo := QrMDFeLayICampo.Value;
        MDFe_PF.VDom(Grupo, Codigo, Campo, Valor, Dominio, FMsg);
      end;
    end;
  end else FMsg := FMsg + 'N�o foi poss�vel definir o item #' +
  Geral.FF0(Grupo) + '.' + Geral.FF0(Codigo) + ' pelo layout da MDF-e! [2]';
end;

procedure TDmMDFe_0000.DefineSituacaoEncerramento(ChaveMDFe: String);
var
  cSitEnc: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCEnc, Dmod.MyDB, [
  'SELECT infProt_cStat, infCanc_cStat, infEnc_cStat, Status ',
  'FROM mdfecaba ',
  'WHERE Id = "' + ChaveMDFe + '"',
  '']);
  //Geral.MB_SQL(nil, QrCEnc);
  //
  if (QrCEncinfEnc_cStat.Value = 135)
  or (QrCEncinfEnc_cStat.Value = 609) then
    cSitEnc := 1
  else
  if (QrCEncStatus.Value = 132) then
    cSitEnc := 1
  else
  if (QrCEncStatus.Value = 100) then
    cSitEnc := 0
  else
    cSitEnc := -1;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfecaba', False, [
  'cSitEnc'], [
  'Id'], [
  cSitEnc], [
  ChaveMDFe], True);
end;

function TDmMDFe_0000.ERf(Grupo, Codigo: Integer; Valor: Double;
  Casas: Integer): Double;
begin
  Result := Valor;
  ERx(Grupo, Codigo, Geral.FFT_Dot(Valor, Casas, siNegativo));
end;

function TDmMDFe_0000.ERi(Grupo, Codigo, Valor: Integer): Integer;
begin
  Result := Valor;
  ERx(Grupo, Codigo, Geral.FF0(Valor));
end;

function TDmMDFe_0000.ERi64(Grupo, Codigo: Integer; Valor: Int64): Int64;
begin
  Result := Valor;
  ERx(Grupo, Codigo, IntToStr(Valor));
end;

function TDmMDFe_0000.ERx(Grupo, Codigo: Integer; Valor: String): String;
var
  Campo, ExpressaoRegular: String;
begin
  Result := Valor;
  if QrMDFeLayI.Locate('Grupo;Codigo', VarArrayOf([Grupo, Codigo]), []) then
  begin
    if (QrMDFeLayIOcorMin.Value > 0) then
    begin
      ExpressaoRegular := Trim(QrMDFeLayIExpReg.Value);
      if ExpressaoRegular <> '' then
      begin
        Campo := QrMDFeLayICampo.Value;
        MDFe_PF.VRegExp(Grupo, Codigo, Campo, Valor, ExpressaoRegular, FMsg);
      end;
    end;
  end else FMsg := FMsg + 'N�o foi poss�vel definir o item #' +
  Geral.FF0(Grupo) + '.' + Geral.FF0(Codigo) + ' pelo layout da MDF-e! [4]';
end;

function TDmMDFe_0000.EventoObtemCtrl(EventoLote, tpEvento, nSeqEvento: Integer;
  chMDFe: String): Integer;
var
  Msg: String;
begin
  Result := 0;
  Msg := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrEveIts, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM mdfeevercab ',
  'WHERE EventoLote=' + Geral.FF0(EventoLote),
  'AND tpEvento=' + Geral.FF0(tpEvento),
  'AND nSeqEvento=' + Geral.FF0(nSeqEvento),
  'AND chMDFe="' + chMDFe + '"',
  '']);
  //
  case QrEveIts.RecordCount of
    0: Msg := 'Item de evento n�o localizado!';
    1: Result := QrEveItsControle.Value;
    else Msg := 'Item de evento duplicado!';
  end;
  if Msg <> '' then
    Geral.MB_Erro(Msg);
end;

procedure TDmMDFe_0000.EventoObtemSub(const Controle: Integer;
  const IdLote: String; var SubCtrl: Integer; var SQLType: TSQLType);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEveSub, Dmod.MyDB, [
  'SELECT SubCtrl ',
  'FROM mdfeeverret ',
  'WHERE Controle=' + Geral.FF0(Controle),
  'AND ret_Id="' + IdLote + '"',
  '']);
  //
  if QrEveSub.RecordCount = 0 then
  begin
    SQLType := stIns;
    SubCtrl := DModG.BuscaProximoCodigoInt('mdfectrl', 'mdfeeverret', '', 0)
  end else begin
    SQLType := stUpd;
    SubCtrl := QrEveSubSubCtrl.Value;
  end;
end;

function TDmMDFe_0000.Evento_Obtem_nSeqEvento(FatID, FatNum, Empresa,
  tpEvento: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEveTp, Dmod.MyDB, [
  'SELECT nSeqEvento ',
  'FROM mdfeevercab ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND tpEvento=' + Geral.FF0(tpEvento),
  'ORDER BY nSeqEvento DESC ',
  '']);
  if QrEveTp.RecordCount = 0 then
    Result := 1
  else
    Result := QrEveTpnSeqEvento.Value + 1;
end;

function TDmMDFe_0000.ExcluiDadosTabelasMDFe(FatID, FatNum: Integer): Boolean;
  function SQLDeleteTabela(Tabela:String): String;
  begin
    Result := 'DELETE FROM ' + Lowercase(Tabela) + ' WHERE FatID=' +
    Geral.FF0(FatID) + ' AND FatNum=' + Geral.FF0(FatNum) + ';';
  end;
begin
  Result := UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  SQLDeleteTabela('mdfecaba'),
  //
  SQLDeleteTabela('mdfeit1autxml'),
  SQLDeleteTabela('mdfeit1lacres'),
  SQLDeleteTabela('mdfeit2infmuncarrega'),
  SQLDeleteTabela('mdfeit2infmundescarga'),
  SQLDeleteTabela('mdfeit2infpercurso'),
  SQLDeleteTabela('mdfeit3infcte'),
  SQLDeleteTabela('mdfeit3infmdfe'),
  SQLDeleteTabela('mdfeit3infnfe'),
  //
  SQLDeleteTabela('mdfemorodocab'),
  SQLDeleteTabela('mdfemorodocondutor'),
  SQLDeleteTabela('mdfemorodopropr'),
  SQLDeleteTabela('mdfemorodopropt'),
  SQLDeleteTabela('mdfemorodoveic'),
  SQLDeleteTabela('mdfemorodovped'),
  SQLDeleteTabela('mdfemorodociot'),
  '']);
end;

function TDmMDFe_0000.FormataLoteMDFe(Lote: Integer): String;
begin
  Result :=  FormatFloat('000000000', Lote);
end;

function TDmMDFe_0000.InsereDadosDeFaturaNasTabelasMDFe(FatID, FatNum, Empresa,
  SerieMDFe, NumeroMDFe: Integer; DataEmi: TDateTime; HoraFat: TTime; LaAviso1,
  LaAviso2: TLabel): Boolean;
const
  Recria = False; // ou True???
var
  IDCtrl_Anterior, IDCtrl, VersaoMDFe: Integer;
  Continua: Boolean;
begin
  Result := False;
  //Dados anteriores
  ReopenMDFeCabA(FatID, FatNum, Empresa);
  IDCtrl_Anterior := QrMDFeCabAIDCtrl.Value;
  VersaoMDFe := MDFe_PF.VersaoMDFeEmUso();
  //
  Continua := ExcluiDadosTabelasMDFe(FatID, FatNum);
  //
  if Continua then
    Continua := InsereDadosMDFeCab(FatID, FatNum, Empresa, IDCtrl_Anterior,
      VersaoMDFe, SerieMDFe, NumeroMDFe, Recria, DataEmi, HoraFat, IDCtrl);
  if Continua then
    Continua := StepMDFeCab(FatID, FatNum, Empresa,
    mdfemystatusMDFeDados, LaAviso1, LaAviso2);

  Result := Continua = True;
end;

function TDmMDFe_0000.InsereDadosMDFeCab(const FatID, FatNum, Empresa,
  IDCtrl_Atual, VersaoMDFe, SerieMDFe, NumeroMDFe: Integer; Recria: Boolean;
  DataEmi: TDateTime; HoraFat: TTime; var IDCtrl: Integer): Boolean;
var
  SQLType: TSQLType;
  cUF: Integer;
  DV, ChaveDeAcesso: String;
  //
  Id, ide_dEmi, ide_hEmi, ide_verProc, ide_UFIni, ide_UFFim, ide_dIniViagem,
  ide_hIniViagem, emit_CNPJ, emit_IE, emit_xNome, emit_xFant, emit_xLgr,
  emit_nro, emit_xCpl, emit_xBairro, emit_xMun, emit_UF, emit_fone,
  emit_email, infAdFisco, infCpl: String;
  (*infProt_Id, infProt_verAplic, infProt_dhRecbto, infProt_nProt,
  infProt_digVal, infProt_xMotivo, infCanc_Id, infCanc_verAplic,
  infCanc_dhRecbto, infCanc_nProt, infCanc_digVal, infCanc_xMotivo,
  infCanc_xJust, eve???_Id, eve???_verAplic, eve???_xMotivo, eve???_chMDFe,
  eve???_xEvento, eve???_CNPJDest, eve???_CPFDest, eve???_emailDest,
  eve???_dhRegEvento, eve???_nProt, infCCe_verAplic, infCCe_CNPJ, infCCe_CPF,
  infCCe_chMDFe, infCCe_dhEvento, infCCe_x Correcao, infCCe_dhRegEvento,
  infCCe_nProt: String;*)
  LoteEnv, ide_cUF, ide_tpAmb, ide_tpEmit, ide_mod, ide_serie, ide_nMDF,
  ide_cMDF, ide_cDV, ide_Modal, ide_tpEmis, ide_procEmi, emit_cMun,
  emit_CEP,  Tot_qCTe, Tot_qNFe, Tot_qMDFe, Tot_cUnid: Integer;
  (*infProt_tpAmb, infProt_cStat, infCanc_tpAmb, infCanc_cStat, infCanc_cJust,
  FisRegCad, CartEmiss, TabelaPrc, CondicaoPg,*) CodInfoEmite, CriAForca,
  (*eve???_tpAmb, eve???_cOrgao, eve???_cStat, eve???_tpEvento, eve???_nSeqEvento,
  cSitMDFe, cSitEnc, infCCe_cOrgao, infCCe_tpAmb, infCCe_tpEvento,
  infCCe_nSeqEvento, infCCe_cStat, infCCe_nCondUso: Integer;*)
  versao, ide_dhEmiTZD, ide_dhIniViagemTZD, versaoModal, Tot_vCarga,
  Tot_qCarga: Double;
  (*protMDFe_versao, infProt_dhRecbtoTZD, retCancMDFe_versao,
  infCanc_dhRecbtoTZD, eve???_TZD_UTC, infCCe_dhEventoTZD, infCCe_verEvento,
  infCCe_dhRegEventoTZD: Double;*)
  //
  Controle, Conta: Int64;
  // infMunCarrega
  xMunCarrega: String;
  cMunCarrega: Integer;
  // infPercurso
  UFPer: String;
  // infMundescarga
  xMunDescarga: String;
  cMunDescarga: Integer;
  // infCTe
  chCTe, SegCodBarra: String;
  // infNFe
  chNFe: String;
  // infMDFe
  chMDFe: String;
  // lacres
  nLacre: String;
  // autXML
  CNPJ, CPF: String;
  AddForma, AddIDCad, Tipo: Integer;
  //rodo.cab
  RNTRC, veicTracao_placa, veicTracao_RENAVAM, UFx, codAgPorto: String;
  veicTracao_cInt, veicTracao_tara, veicTracao_capKg, veicTracao_capM3, tpRod,
  tpCar: Integer;
  CIOT: Int64;
  // mdfemorodopropt
  xNome, IE: String;
  tpProp: Integer;
  // mdfemorodoveic
  placa, RENAVAM: String;
  cInt, tara, capKG, capM3: Integer;
  // mdfemorodovped
  CNPJForn, CNPJPg, nCompra: String;
begin
  Result := False;
  if FatID <> VAR_FATID_6001 then
  begin
    Geral.MB_Aviso('"FatID" ' + Geral.FF0(FatID) +
    ' n�o implementado em adi��o de dados de MDF-e nas tabelas!');
    Exit;
  end;
  ReopenMDFeLayI();
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtmnfcab ',
  'WHERE Codigo=' + Geral.FF0(FatNum),
  '']);
  FMsg := '';
  //
  SQLType        := stIns;
  if IDCtrl_Atual = 0 then
    IDCtrl := UMyMod.Busca_IDCtrl_MDFe(SQLType, IDCtrl)
  else
    IDCtrl := IDCtrl_Atual;
  //
  LoteEnv        := 0;
  //
  if not ReopenOpcoesMDFe(QrOpcoesMDFe, Empresa, True) then Exit;
  //
  ReopenEmpresa(Empresa);
  if QrEmpresaCodigo.Value <> Empresa then
    FMsg := FMsg + 'Empresa n�o localizada!';
{ N�o tem impostos!
  // S� simples federal! - Parei aqui
  if QrFilialSimplesFed.Value = 0 then
    Geral.MB_Aviso('C�lculo de impostos em fase de implementa��o!' + sLineBreak +
    'Verifique a exatid�o dos valores e comunique � DERMATEK qualquer erro!');
  //
}
  if Empresa = 0 then
    FMsg := FMsg + 'Empresa inv�lida!';
  //
  //
  cUF := Geral.IMV(QrEmpresaDTB_UF.Value);
  MDFe_PF.MontaChaveDeAcesso(cUF, DataEmi, QrEmpresaCNPJ.Value,
  QrOpcoesMDFeMDFeide_mod.Value, SerieMDFe, NumeroMDFe, ide_cMDF, DV,
  ChaveDeAcesso, QrMDFeCabAide_cMDF.Value, QrOpcoesMDFeMDFetpEmis.Value,
    QrOpcoesMDFeMDFeversao.Value);
  versao := ERf(0, 2, QrOpcoesMDFeMDFeversao.Value, 2);

  (*3*)Id             := ChaveDeAcesso;
  ERx(0, 3, 'MDFe' + Id);
  if Trunc((versao * 100) + 0.5) <> VersaoMDFe then
    FMsg := FMsg + 'Vers�o MDF-e n�o suportada neste aplicativo: ' +
    FloatToStr(Versao);
  // Cabecalho ide => (*4*)
  ide_cUF        := Geral.IMV(QrEmpresaDTB_UF.Value);
  ide_cUF        := DefDomI(0, 5, ide_cUF);
  ide_tpAmb      := DefDomI(0, 6, QrOpcoesMDFeMDFeide_tpAmb.Value);
  ide_tpEmit     := DefDomI(0, 7, QrFrtMnfCabTpEmit.Value);
  ide_mod        := DefDomI(0, 8, QrOpcoesMDFeMDFeide_mod.Value);
  ide_serie      := ERi(0, 9, SerieMDFe);
  ide_nMDF       := ERi(0, 10, NumeroMDFe);
  ide_cMDF       := Geral.IMV(ERx(0, 11, Geral.FFN(ide_cMDF, 9)));
  ide_cDV        := ERi(0, 12,  Geral.IMV(DV));
  ide_Modal      := DefDomI(0, 13, QrFrtMnfCabModal.Value);
  ide_dEmi       := (*14*)Geral.FDT(DataEmi, 1);
  ide_hEmi       := (*14*)Geral.FDT(HoraFat, 100);
  ide_dhEmiTZD   := (*14*)QrOpcoesMDFeTZD_UTC.Value;
  ide_tpEmis     := DefDomI(0, 15, QrOpcoesMDFeMDFetpEmis.Value);
  ide_procEmi    := DefDomI(0, 16, 0); // Emissao com aplicativo do contribuinte
  ide_verProc    := ERx(0, 17, DBCheck.Obtem_verProc);
  ide_UFIni      := DefDomX(0, 18, QrFrtMnfCabUFIni.Value);
  ide_UFFim      := DefDomX(0, 19, QrFrtMnfCabUFFim.Value);
///
///
////////////////////////////////////////////////////////////////////////////////
  (*20*) // infMunCarrega - Tabela Separada
        (*21*)// cMunCarrega
        (*22*)// xMunCarrega
////////////////////////////////////////////////////////////////////////////////
///
///
///
///
////////////////////////////////////////////////////////////////////////////////
  (*23*) // infPercurso - Tabela Separada
        (*24*)// UFPer
////////////////////////////////////////////////////////////////////////////////
///
///
  ide_dIniViagem := (*25*)Geral.FDT(QrFrtMnfCabDataViagIniPrv.Value, 1);
  ide_hIniViagem := (*25*)Geral.FDT(QrFrtMnfCabDataViagIniPrv.Value, 100);
  ide_dhIniViagemTZD   := (*25*)QrOpcoesMDFeTZD_UTC.Value;
////////////////////////////////////////////////////////////////////////////////
///
  (*26*)// Cabecalho emit
  ReopenEntiMDFe(QrFrtMnfCabEmpresa.Value);
  emit_CNPJ      := ERx(0, 27, Geral.SoNumero_TT(QrEntiMDFeCNPJ.Value));
  emit_IE        := ERx(0, 28, Geral.SoNumeroELetra_TT(QrEntiMDFeIE.Value));
  emit_xNome     := ERx(0, 29, QrEntiMDFexNome.Value);
  emit_xFant     := ERx(0, 30, QrEntiMDFexFant.Value);
  (*31*)// Sub Cabecalho enderEmit
  emit_xLgr      := ERx(0, 32, QrEntiMDFexLgr.Value);
  emit_nro       := ERx(0, 33, QrEntiMDFeNro.Value);
  emit_xCpl      := ERx(0, 34, QrEntiMDFexCpl.Value);
  emit_xBairro   := ERx(0, 35, QrEntiMDFexBairro.Value);
  emit_cMun      := ERi(0, 36, Trunc(QrEntiMDFeCMun.Value));
  emit_xMun      := ERx(0, 37, QrEntiMDFexMun.Value);
  emit_CEP       := ERi(0, 38, Trunc(QrEntiMDFeCEP.Value));
  emit_UF        := DefDomX(0, 39, QrEntiMDFeUF.Value);
  emit_fone      := ERx(0, 40, Geral.SoNumero_TT(QrEntiMDFeFone.Value));
  emit_email     := ERx(0, 41, QrEntiMDFeemail.Value);
///
////////////////////////////////////////////////////////////////////////////////
///
  ///
  ///
  ////////////////////////////////////////////////////////////////////////////////
  ///
  ///
    (*42*)// infModal => usar frtMdf??? - Tabela Separada
  versaoModal    := ERf(0, 43, QrOpcoesMDFeMDFeversao.Value, 2);
          (*44*)
  ///
  ///
  ///
  ///
  ////////////////////////////////////////////////////////////////////////////////
  ///
  ///
    (*45*)// infDoc => usar frtMdf??? - Tabela Separada
      (*46*)//
      (*...*)
      (*92*)
  ///
  ///
  Tot_qCTe       := ERi(0, 94, Trunc(QrFrtMnfCabTot_qCTe.Value));
  Tot_qNFe       := ERi(0, 95, Trunc(QrFrtMnfCabTot_qNFe.Value));
  Tot_qMDFe      := ERi(0, 97, Trunc(QrFrtMnfCabTot_qMDFe.Value));
  (*96*) // Nao tem nada no manual!!!
  Tot_vCarga     := ERf(0, 98, QrFrtMnfCabTot_vCarga.Value, 2);
  Tot_cUnid      := DefDomI(0, 99, QrFrtMnfCabTot_cUnid.Value);
  Tot_qCarga     := ERf(0, 100, QrFrtMnfCabTot_qCarga.Value, 4);
///
///
////////////////////////////////////////////////////////////////////////////////
  (*101*)// lacres - Tabela Separada
        (*102*)// nLacre
////////////////////////////////////////////////////////////////////////////////
///
///
///
///
////////////////////////////////////////////////////////////////////////////////
  (*103*)// autXML - Tabela Separada
        (*104*)// CNPJ
        (*105*)// CPF
////////////////////////////////////////////////////////////////////////////////
  (*106*) // infAdic
  infAdFisco     := ERx(0, 107, QrFrtMnfCabinfAdFisco.Value);
  infCpl         := ERx(0, 108, QrFrtMnfCabinfCpl.Value);
///
///
///  (*109*) // ds:Signature
///
///
  CodInfoEmite   := QrFrtMnfCabEmpresa.Value;
  CriAForca      := 0;

  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'mdfecaba', False, [
  'FatID', 'FatNum', 'Empresa',
  'LoteEnv', 'versao',
  'Id', 'ide_cUF', 'ide_tpAmb',
  'ide_tpEmit', 'ide_mod', 'ide_serie',
  'ide_nMDF', 'ide_cMDF', 'ide_cDV',
  'ide_Modal', 'ide_dEmi', 'ide_hEmi',
  'ide_dhEmiTZD', 'ide_tpEmis', 'ide_procEmi',
  'ide_verProc', 'ide_UFIni', 'ide_UFFim',
  'ide_dIniViagem', 'ide_hIniViagem', 'ide_dhIniViagemTZD',
  'emit_CNPJ', 'emit_IE', 'emit_xNome',
  'emit_xFant', 'emit_xLgr', 'emit_nro',
  'emit_xCpl', 'emit_xBairro', 'emit_cMun',
  'emit_xMun', 'emit_CEP', 'emit_UF',
  'emit_fone', 'emit_email', 'versaoModal',
  'Tot_qCTe', 'Tot_qNFe', 'Tot_qMDFe',
  'Tot_cUnid', 'Tot_vCarga', 'Tot_qCarga',
  'infAdFisco', 'infCpl',
  (*'Status', 'protMDFe_versao', 'infProt_Id',
  'infProt_tpAmb', 'infProt_verAplic', 'infProt_dhRecbto',
  'infProt_dhRecbtoTZD', 'infProt_nProt', 'infProt_digVal',
  'infProt_cStat', 'infProt_xMotivo', 'retCancMDFe_versao',
  'infCanc_Id', 'infCanc_tpAmb', 'infCanc_verAplic',
  'infCanc_dhRecbto', 'infCanc_dhRecbtoTZD', 'infCanc_nProt',
  'infCanc_digVal', 'infCanc_cStat', 'infCanc_xMotivo',
  'infCanc_cJust', 'infCanc_xJust', 'FisRegCad',
  'CartEmiss', 'TabelaPrc', 'CondicaoPg',*)
  'CodInfoEmite', 'CriAForca'(*, 'eve???_Id',
  'eve???_tpAmb', 'eve???_verAplic', 'eve???_cOrgao',
  'eve???_cStat', 'eve???_xMotivo', 'eve???_chMDFe',
  'eve???_tpEvento', 'eve???_xEvento', 'eve???_nSeqEvento',
  'eve???_CNPJDest', 'eve???_CPFDest', 'eve???_emailDest',
  'eve???_dhRegEvento', 'eve???_TZD_UTC', 'eve???_nProt',
  'cSitMDFe', 'cSitEnc', 'infCCe_verAplic',
  'infCCe_cOrgao', 'infCCe_tpAmb', 'infCCe_CNPJ',
  'infCCe_CPF', 'infCCe_chMDFe', 'infCCe_dhEvento',
  'infCCe_dhEventoTZD', 'infCCe_tpEvento', 'infCCe_nSeqEvento',
  'infCCe_verEvento', infCCe_x Correcao, 'infCCe_cStat',
  'infCCe_dhRegEvento', 'infCCe_dhRegEventoTZD', 'infCCe_nProt',
  'infCCe_nCondUso'*)], [
  'IDCtrl'], [
  FatID, FatNum, Empresa,
  LoteEnv, versao,
  Id, ide_cUF, ide_tpAmb,
  ide_tpEmit, ide_mod, ide_serie,
  ide_nMDF, ide_cMDF, ide_cDV,
  ide_Modal, ide_dEmi, ide_hEmi,
  ide_dhEmiTZD, ide_tpEmis, ide_procEmi,
  ide_verProc, ide_UFIni, ide_UFFim,
  ide_dIniViagem, ide_hIniViagem, ide_dhIniViagemTZD,
  emit_CNPJ, emit_IE, emit_xNome,
  emit_xFant, emit_xLgr, emit_nro,
  emit_xCpl, emit_xBairro, emit_cMun,
  emit_xMun, emit_CEP, emit_UF,
  emit_fone, emit_email, versaoModal,
  Tot_qCTe, Tot_qNFe, Tot_qMDFe,
  Tot_cUnid, Tot_vCarga, Tot_qCarga,
  infAdFisco, infCpl,
  (*Status, protMDFe_versao, infProt_Id,
  infProt_tpAmb, infProt_verAplic, infProt_dhRecbto,
  infProt_dhRecbtoTZD, infProt_nProt, infProt_digVal,
  infProt_cStat, infProt_xMotivo, retCancMDFe_versao,
  infCanc_Id, infCanc_tpAmb, infCanc_verAplic,
  infCanc_dhRecbto, infCanc_dhRecbtoTZD, infCanc_nProt,
  infCanc_digVal, infCanc_cStat, infCanc_xMotivo,
  infCanc_cJust, infCanc_xJust, FisRegCad,
  CartEmiss, TabelaPrc, CondicaoPg,*)
  CodInfoEmite, CriAForca(*, eve???_Id,
  eve???_tpAmb, eve???_verAplic, eve???_cOrgao,
  eve???_cStat, eve???_xMotivo, eve???_chMDFe,
  eve???_tpEvento, eve???_xEvento, eve???_nSeqEvento,
  eve???_CNPJDest, eve???_CPFDest, eve???_emailDest,
  eve???_dhRegEvento, eve???_TZD_UTC, eve???_nProt,
  cSitMDFe, cSitEnc, infCCe_verAplic,
  infCCe_cOrgao, infCCe_tpAmb, infCCe_CNPJ,
  infCCe_CPF, infCCe_chMDFe, infCCe_dhEvento,
  infCCe_dhEventoTZD, infCCe_tpEvento, infCCe_nSeqEvento,
  infCCe_verEvento, infCCe_x Correcao, infCCe_cStat,
  infCCe_dhRegEvento, infCCe_dhRegEventoTZD, infCCe_nProt,
  infCCe_nCondUso*)], [
  IDCtrl], True) then
  begin
    //Controle := IDCtrl;
    //
///
///
////////////////////////////////////////////////////////////////////////////////
    (*20*) // infMunCarrega - Tabela Separada
    ReopenFrtMnfMunCarrega(FatNum);
    while not QrFrtMnfMunCarrega.Eof do
    begin
      cMunCarrega     := ERi(0, 21, QrFrtMnfMunCarregacMunCarrega.Value);
      xMunCarrega     := ERx(0, 22, QrFrtMnfMunCarregaxMunCarrega.Value);
      //
      Controle         := QrFrtMnfMunCarregaControle.Value;
      //Controle := UMyMod.BPGS1I32('mdfeit2infmuncarrega', 'Controle', '', '', tsPos, SQLType, Controle);
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'mdfeit2infmuncarrega', False, [
      'FatID', 'FatNum', 'Empresa',
      'cMunCarrega', 'xMunCarrega'], [
      'Controle'], [
      FatID, FatNum, Empresa,
      cMunCarrega, xMunCarrega], [
      Controle], True);
      //
      QrFrtMnfMunCarrega.Next;
    end;
////////////////////////////////////////////////////////////////////////////////
///
///
///
///
////////////////////////////////////////////////////////////////////////////////
  (*23*) // infPercurso - Tabela Separada
    ReopenFrtMnfInfPercurso(FatNum);
    while not QrFrtMnfInfPercurso.Eof do
    begin
      UFPer            := DefDomX(0, 24, QrFrtMnfInfPercursoUFPer.Value);
      Controle         := QrFrtMnfInfPercursoControle.Value;
      //Controle := UMyMod.BPGS1I32('mdfeit2infpercurso', 'Controle', '', '', tsPos, SQLType, Controle);
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'mdfeit2infpercurso', False, [
      'FatID', 'FatNum', 'Empresa',
      'UFPer'], [
      'Controle'], [
      FatID, FatNum, Empresa,
      UFPer], [
      Controle], True);
      //
      QrFrtMnfInfPercurso.Next;
    end;
////////////////////////////////////////////////////////////////////////////////
///
///
///
///
////////////////////////////////////////////////////////////////////////////////
    (*45*) // infDod
      (*46*) // infMunDescarga
    ReopenFrtMnfDocCMun(FatNum);
    while not QrFrtMnfInfDocCMun.Eof do
    begin
      cMunDescarga     := ERi(0, 47, QrFrtMnfInfDocCMuncMunDescarga.Value);
      xMunDescarga     := ERx(0, 48, QrFrtMnfInfDocXMunxMunDescarga.Value);
      //
      //Controle := UMyMod.BPGS1I64('mdfeit2infmundescarga', 'Controle', '', '', tsPos, SQLType, Controle);
      Controle         := QrFrtMnfInfDocXMunControle.Value;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'mdfeit2infmundescarga', False, [
      'FatID', 'FatNum', 'Empresa',
      'cMunDescarga', 'xMunDescarga'], [
      'Controle'], [
      FatID, FatNum, Empresa,
      cMunDescarga, xMunDescarga], [
      Controle], True) then
      begin
        // (*49*) // infCTe
        QrFrtMnfInfDoc57.First;
        while not QrFrtMnfInfDoc57.Eof do
        begin
          chCTe          := ERx(0, 50, QrFrtMnfInfDoc57DocChave.Value);
          SegCodBarra    := ERx(0, 51, QrFrtMnfInfDoc57SegCodBarra.Value);
          //
          Conta          := QrFrtMnfInfDoc57Controle.Value;
          //Conta := UMyMod.BPGS1I32('mdfeit3infcte', 'Conta', '', '', tsPos, SQLType, Conta);
          //if
          UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'mdfeit3infcte', False, [
          'FatID', 'FatNum', 'Empresa',
          'Controle', 'chCTe', 'SegCodBarra'], [
          'Conta'], [
          FatID, FatNum, Empresa,
          Controle, chCTe, SegCodBarra], [
          Conta], True);
          //
          QrFrtMnfInfDoc57.Next;
        end;
        //
        // (*64*) // infNFe
        QrFrtMnfInfDoc55.First;
        while not QrFrtMnfInfDoc55.Eof do
        begin
          chNFe          := ERx(0, 65, QrFrtMnfInfDoc55DocChave.Value);
          SegCodBarra    := ERx(0, 66, QrFrtMnfInfDoc55SegCodBarra.Value);
          //
          Conta          := QrFrtMnfInfDoc55Controle.Value;
          //Conta := UMyMod.BPGS1I32('mdfeit3infnfe', 'Conta', '', '', tsPos, SQLType, Conta);
          //if
          UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'mdfeit3infnfe', False, [
          'FatID', 'FatNum', 'Empresa',
          'Controle', 'chNFe', 'SegCodBarra'], [
          'Conta'], [
          FatID, FatNum, Empresa,
          Controle, chNFe, SegCodBarra], [
          Conta], True);
          //
          QrFrtMnfInfDoc55.Next;
        end;
        //
        // (*79*) // infMDFe
        QrFrtMnfInfDoc58.First;
        while not QrFrtMnfInfDoc58.Eof do
        begin
          chMDFe         := ERx(0, 80, QrFrtMnfInfDoc58DocChave.Value);
          //SegCodBarra    := ERx(0, 51, QrFrtMnfInfDoc58SegCodBarra.Value);
          //
          Conta          := QrFrtMnfInfDoc58Controle.Value;
          //Conta := UMyMod.BPGS1I32('mdfeit3infmdfe', 'Conta', '', '', tsPos, SQLType, Conta);
          //if
          UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'mdfeit3infmdfe', False, [
          'FatID', 'FatNum', 'Empresa',
          'Controle', 'chMDFe'(*, 'SegCodBarra'*)], [
          'Conta'], [
          FatID, FatNum, Empresa,
          Controle, chMDFe(*, SegCodBarra*)], [
          Conta], True);
          //
          QrFrtMnfInfDoc58.Next;
        end;
        //
      end;
      //
      QrFrtMnfInfDocCMun.Next;
    end;
////////////////////////////////////////////////////////////////////////////////
///
///
///
///
////////////////////////////////////////////////////////////////////////////////
  (*101*) // lacres - Tabela Separada
    ReopenFrtMnfLacres(FatNum);
    while not QrFrtMnfLacres.Eof do
    begin
      nLacre            := ERx(0, 102, QrFrtMnfLacresnLacre.Value);
      Controle         := QrFrtMnfLacresControle.Value;
      //Controle := UMyMod.BPGS1I32('mdfeit2infpercurso', 'Controle', '', '', tsPos, SQLType, Controle);
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'mdfeit1lacres', False, [
      'FatID', 'FatNum', 'Empresa',
      'nLacre'], [
      'Controle'], [
      FatID, FatNum, Empresa,
      nLacre], [
      Controle], True);
      //
      QrFrtMnfLacres.Next;
    end;
////////////////////////////////////////////////////////////////////////////////
///
///
///
///
////////////////////////////////////////////////////////////////////////////////
    (*103*) // autXML - Autorizados para download do XML
    ReopenFrtMnfAutXml(FatNum);
    QrFrtMnfAutXml.First;
    while not QrFrtMnfAutXml.Eof do
    begin
      //(*****)Controle       := UMyMod.BPGS1I32('mdfeit1autxml', 'Controle', '', '', tsPos, SQLType, 0);
      Controle              := QrFrtMnfAutXmlControle.Value;
      AddForma              := QrFrtMnfAutXmlAddForma.Value;
      AddIDCad              := QrFrtMnfAutXmlAddIDCad.Value;
      Tipo                  := QrFrtMnfAutXmlTipo.Value;
      CNPJ                  := Geral.SoNumero_TT(QrFrtMnfAutXmlCNPJ.Value);
      CPF                   := Geral.SoNumero_TT(QrFrtMnfAutXmlCPF.Value);
      if CNPJ <> '' then
        CNPJ                := ERx(0, 104, CNPJ)
      else
        CPF                 := ERx(0, 105, CPF);
      //
      //Controle := UMyMod.BPGS1I32('mdfeit1autxml', 'Controle', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'mdfeit1autxml', False, [
      'FatID', 'FatNum', 'Empresa',
      'AddForma', 'AddIDCad', 'Tipo',
      'CNPJ', 'CPF'], [
      'Controle'], [
      FatID, FatNum, Empresa,
      AddForma, AddIDCad, Tipo,
      CNPJ, CPF], [
      Controle], True);
      //
      QrFrtMnfAutXml.Next;
    end;
////////////////////////////////////////////////////////////////////////////////
///
///
///
///
////////////////////////////////////////////////////////////////////////////////
    case TMDFeModal(ide_Modal) of
      //mdfemodalIndefinido=0,
      mdfemodalRodoviario:
      begin
        ReopenTrnsCad(QrFrtMnfCabEmpresa.Value);
        (*1*) // rodo
        Controle           := QrFrtMnfCabCodigo.Value;
        RNTRC              := Geral.FF0(Geral.IMV('0' + Geral.SoNumero_TT(QrTrnsCadRNTRC.Value)));
        RNTRC              := ERx(1, 2, RNTRC);
        (*3*) // CIOT - Carta Frete
        ReopenFrtMnfMoRodoCIOT(FatNum);
        while not QrFrtMnfMoRodoCIOT.Eof do
        begin
          Conta          := QrFrtMnfMoRodoCIOTControle.Value;
          CIOT           := ERi64(1, 3, QrFrtMnfMoRodoCIOTCIOT.Value);
          //
          //Conta := UMyMod.BPGS1I32('mdfemorodovped', 'Conta', '', '', tsPos, stIns, Conta);
          UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'mdfemorodociot', False, [
          'FatID', 'FatNum', 'Empresa',
          'Controle', 'CIOT'], [
          'Conta'], [
          FatID, FatNum, Empresa,
          Controle, CIOT], [
          Conta], True);
          //
          QrFrtMnfMoRodoCIOT.Next;
        end;
        (*4*) // veicTracao
        ReopenTrnsIts(QrFrtMnfCabTrnsIts.Value);
        veicTracao_cInt    := ERi(1, 5, QrTrnsItsControle.Value);
        veicTracao_placa   := ERx(1, 6, Geral.SoNumeroELetra_TT(QrTrnsItsPlaca.Value));
        veicTracao_RENAVAM := ERx(1, 7, QrTrnsItsRENAVAM.Value);
        veicTracao_tara    := ERi(1, 8, QrTrnsItsTaraKg.Value);
        veicTracao_capKg   := ERi(1, 9, QrTrnsItsCapKg.Value);
        veicTracao_capM3   := ERi(1, 10, QrTrnsItsCapM3.Value);
        (*11*) // prop
        if (QrTrnsItsCodigo.Value <> 0) and (QrTrnsItsCodigo.Value <> Empresa) then
        begin
          ReopenTerceiro(QrTrnsItsCodigo.Value);
          Conta          := Controle;
          CPF            := '';
          CNPJ           := '';
          if QrTerceiroTipo.Value = 1 then
            CPF          := ERx(1, 12, Geral.SoNumero_TT(QrTerceiroCPF.Value))
          else
            CNPJ         := ERx(1, 13, Geral.SoNumero_TT(QrTerceiroCNPJ.Value));
          RNTRC          := Geral.FF0(Geral.IMV('0' + Geral.SoNumero_TT(QrTrnsItsRNTRC.Value)));
          RNTRC          := ERx(1, 14, RNTRC);
          xNome          := ERx(1, 15, QrTerceiroNO_ENT.Value);
          IE             := ERx(1, 16, QrTerceiroIE.Value);
          UFx            := DefDomX(1, 17, QrTerceiroUF.Value);
          tpProp         := DefDomI(1, 18, QrTrnsItsTpProp.Value);
          //
          //Conta := UMyMod.BPGS1I32('mdfemorodopropt', 'Conta', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
          if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'mdfemorodopropt', False, [
          'FatID', 'FatNum', 'Empresa',
          'Controle', 'CPF', 'CNPJ',
          'RNTRC', 'xNome', 'IE',
          'UF', 'tpProp'], [
          'Conta'], [
          FatID, FatNum, Empresa,
          Controle, CPF, CNPJ,
          RNTRC, xNome, IE,
          UFx, tpProp], [
          Conta], True) then ;
        end;
        (*19*) // Condutor
        ReopenFrtMnfMoRodoCondutor(FatNum);
        QrFrtMnfMoRodoCondutor.First;
        while not QrFrtMnfMoRodoCondutor.Eof do
        begin
          Conta := QrFrtMnfMoRodoCondutorControle.Value;
          xNome := ERx(1, 20, QrFrtMnfMoRodoCondutorxNome.Value);
          CPF   := ERx(1, 21, Geral.SoNumero_TT(QrFrtMnfMoRodoCondutorCPF.Value));
          //
          //Conta := UMyMod.BPGS1I32('mdfemorodocondutor', 'Conta', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
          if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'mdfemorodocondutor', False, [
          'FatID', 'FatNum', 'Empresa',
          'Controle', 'xNome', 'CPF'], [
          'Conta'], [
          FatID, FatNum, Empresa,
          Controle, xNome, CPF], [
          Conta], True) then ;
          //
          QrFrtMnfMoRodoCondutor.Next;
        end;
        tpRod              := DefDomI(1, 22, QrTrnsItsTpRod.Value);
        tpCar              := DefDomI(1, 23, QrTrnsItsTpCar.Value);
        UFx                := DefDomX(1, 24, QrTrnsItsUF.Value);
        codAgPorto         := ERx(1, 47, QrFrtMnfCabCodAgPorto.Value);
        //
        //Controle := UMyMod.BPGS1I32('mdfemorodocab', 'Controle', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
        if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'mdfemorodocab', False, [
        'FatID', 'FatNum', 'Empresa',
        'RNTRC', 'veicTracao_cInt',
        'veicTracao_placa', 'veicTracao_RENAVAM', 'veicTracao_tara',
        'veicTracao_capKg', 'veicTracao_capM3', 'veicTracao_tpRod',
        'veicTracao_tpCar', 'veicTracao_UF', 'codAgPorto'], [
        'Controle'], [
        FatID, FatNum, Empresa,
        RNTRC, veicTracao_cInt,
        veicTracao_placa, veicTracao_RENAVAM, veicTracao_tara,
        veicTracao_capKg, veicTracao_capM3, tpRod,
        tpCar, UFx, codAgPorto], [
        Controle], True) then
        begin
          (*25*) // veicReboque - Dados dos Reboques
          ReopenFrtMnfMoRodoVeicRebo(FatNum);
          while not QrFrtMnfMoRodoVeicRebo.Eof do
          begin
            Conta          := QrFrtMnfMoRodoVeicReboControle.Value;
            cInt           := ERi(1, 26, QrFrtMnfMoRodoVeicReboCInt.Value);
            ReopenTrnsIts(cInt);
            placa          := ERx(1, 27, Geral.SoNumeroELetra_TT(QrTrnsItsplaca.Value));
            RENAVAM        := ERx(1, 28, QrTrnsItsRENAVAM.Value);
            tara           := ERi(1, 29, QrTrnsItsTaraKg.Value);
            capKG          := ERi(1, 30, QrTrnsItscapKG.Value);
            capM3          := ERi(1, 31, QrTrnsItscapM3.Value);
            (*32*) //Prop
            if (QrTrnsItsCodigo.Value <> 0) and (QrTrnsItsCodigo.Value <> Empresa) then
            begin
              ReopenTerceiro(QrTrnsItsCodigo.Value);
              //Conta          := Conta;
              CPF            := '';
              CNPJ           := '';
              if QrTerceiroTipo.Value = 1 then
                CPF          := ERx(1, 33, Geral.SoNumero_TT(QrTerceiroCPF.Value))
              else
                CNPJ         := ERx(1, 34, Geral.SoNumero_TT(QrTerceiroCNPJ.Value));
              RNTRC          := Geral.FF0(Geral.IMV('0' + Geral.SoNumero_TT(QrTrnsItsRNTRC.Value)));
              RNTRC          := ERx(1, 35, RNTRC);
              xNome          := ERx(1, 36, QrTerceiroNO_ENT.Value);
              IE             := ERx(1, 37, QrTerceiroIE.Value);
              UFx            := DefDomX(1, 38, QrTerceiroUF.Value);
              tpProp         := DefDomI(1, 39, QrTrnsItsTpProp.Value);
              //
              //Conta := UMyMod.BPGS1I32('mdfemorodopropr', 'Conta', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
              if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'mdfemorodopropr', False, [
              'FatID', 'FatNum', 'Empresa',
              'Controle', 'CPF', 'CNPJ',
              'RNTRC', 'xNome', 'IE',
              'UF', 'tpProp'], [
              'Conta'], [
              FatID, FatNum, Empresa,
              Controle, CPF, CNPJ,
              RNTRC, xNome, IE,
              UFx, tpProp], [
              Conta], True) then ;
            end;
            tpCar          := DefDomI(1, 40, QrTrnsItstpCar.Value);
            UFx            := DefDomX(1, 41, QrTrnsItsUF.Value);
            //
            //Conta := UMyMod.BPGS1I32('mdfemorodoveic', 'Conta', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
            if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'mdfemorodoveic', False, [
            'FatID', 'FatNum', 'Empresa',
            'Controle', 'cInt', 'placa',
            'RENAVAM', 'tara', 'capKG',
            'capM3', 'tpCar', 'UF'], [
            'Conta'], [
            FatID, FatNum, Empresa,
            Controle, cInt, placa,
            RENAVAM, tara, capKG,
            capM3, tpCar, UFx], [
            Conta], True) then ;
            //
            QrFrtMnfMoRodoVeicRebo.Next;
          end;
          (*42*) // valePed - Vale pedagio
          ReopenFrtMnfMoRodoValePedg(FatNum);
          while not QrFrtMnfMoRodoValePedg.Eof do
          begin
            (*43*) // disp - Informa��es sobre os dispositivos dos Vale Ped�gio
            Conta          := QrFrtMnfMoRodoValePedgControle.Value;
            CNPJForn       := ERx(1, 44, Geral.SoNumero_TT(QrFrtMnfMoRodoValePedgCNPJForn.Value));
            CNPJPg         := ERx(1, 45, Geral.SoNumero_TT(QrFrtMnfMoRodoValePedgCNPJPg.Value));
            nCompra        := ERx(1, 46, QrFrtMnfMoRodoValePedgnCompra.Value);
            //
            //Conta := UMyMod.BPGS1I32('mdfemorodovped', 'Conta', '', '', tsPos, stIns, Conta);
            UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'mdfemorodovped', False, [
            'FatID', 'FatNum', 'Empresa',
            'Controle', 'CNPJForn', 'nCompra',
            'CNPJPg'], [
            'Conta'], [
            FatID, FatNum, Empresa,
            Controle, CNPJForn, nCompra,
            CNPJPg], [
            Conta], True);
            //
            QrFrtMnfMoRodoValePedg.Next;
          end;
        end;
      end;
      //mdfemodalAereo=2,
      //mdfemodalAquaviario=3, mdfemodalFerroviario=4,
      //mdfemodalDutoviario=5, mdfemodalMultimodal=6
      else
      begin
        Result := False;
        Geral.MB_Aviso('Tipo de Modal n�o implementado!');
      end;
    end;
  end;
  if Trim(FMsg) <> '' then
  begin
    Geral.MB_Aviso(FMsg);
  end else
    Result := True;
end;

function TDmMDFe_0000.LocalizaMDFeInfoEveCan(const ChMDFe: String; var Id,
  xJust: String; var cJust: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Id    := '';
  xJust := '';
  cJust := 0;
  Result := False;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT cab.Id, can.nJust, can.xJust',
    'FROM mdfeevercab cab',
    'LEFT JOIN mdfeevercan can ON can.Controle = cab.Controle',
    'WHERE cab.chMDFe="' + ChMDFe + '"',
    'GROUP BY cab.Id',
    '']);
    if Qry.RecordCount > 0 then
    begin
      Id     := Qry.FieldByName('Id').AsString;
      xJust  := Qry.FieldByName('xJust').AsString;
      cJust  := Qry.FieldByName('nJust').AsInteger;
      Result := True;
    end;
  finally
    Qry.Free;
  end;
end;

function TDmMDFe_0000.LocalizaMDFeInfoEveEnc(const ChMDFe: String;
  var nProt: String; var dtEnc: TDateTime; var cUF, cMun: Integer;
  CNPJ: String): Boolean;
var
  Qry: TmySQLQuery;
begin
  nProt  := '';
  dtEnc  := 0;
  cUF    := 0;
  cMun   := 0;
  CNPJ   := '';
  Result := False;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT cab.Id, cab.CNPJ, ',
    'enc.nProt, enc.dtEnc, enc.cUF, enc.cMun ',
    'FROM mdfeevercab cab',
    'LEFT JOIN mdfeeverenc enc ON enc.Controle = cab.Controle',
    'WHERE cab.chMDFe="' + ChMDFe + '"',
    'GROUP BY cab.Id',
    '']);
    if Qry.RecordCount > 0 then
    begin
      CNPJ   := Qry.FieldByName('CNPJ').AsString;
      nProt  := Qry.FieldByName('nProt').AsString;
      dtEnc  := Qry.FieldByName('dtEnc').AsDateTime;
      cUF    := Qry.FieldByName('cUF').AsInteger;
      cMun   := Qry.FieldByName('cMun').AsInteger;
      Result := True;
    end;
  finally
    Qry.Free;
  end;
end;

function TDmMDFe_0000.LocalizaMDFeInfoEveIdC(const ChMDFe: String; var CNPJ,
  xNome, CPF: String): Boolean;
var
  Qry: TmySQLQuery;
begin
  CNPJ   := '';
  XNome  := '';
  CPF    := '';
  Result := False;
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT cab.Id, cab.CNPJ, ',
    'idc.xNome, idc.CPF, idc.Conta ',
    'FROM mdfeevercab cab',
    'LEFT JOIN mdfeeveridc idc ON idc.Controle = cab.Controle',
    'WHERE cab.chMDFe="' + ChMDFe + '"',
    //'GROUP BY cab.Id',
    // Pegar o ultimo
    'ORDER BY idc.Conta DESC',
    '']);
    if Qry.RecordCount > 0 then
    begin
      CNPJ   := Qry.FieldByName('CNPJ').AsString;
      xNome  := Qry.FieldByName('xNome').AsString;
      CPF    := Qry.FieldByName('CPF').AsString;
      Result := True;
    end;
  finally
    Qry.Free;
  end;
end;

function TDmMDFe_0000.MensagemDeID_MDFe(Grupo, Codigo: Integer; Valor,
  Texto: String; Seq_Chamou: Integer): String;
var
  Txt, Obs: String;
begin
  if Valor = '' then
    Txt := '(valor n�o definido)'
  else
    Txt := Valor;
  //
  Result := 'TDmCTe_0000.MensagemDeID_MDFe()'+sLineBreak+
            'Seq Chamada: ' + FormatFloat('0', Seq_Chamou) + sLineBreak +
            'N�o foi poss�vel definir o item abaixo pelo layout da MDF-e:' +
  sLineBreak +
  sLineBreak + 'Campo: ' +QrMDFeLayIDescricao.Value +
  sLineBreak + 'Valor: ' + Txt;
  //
  //if Trim(Texto) <> '' then
  Result := Result + sLineBreak + 'Motivo: ' + Texto;
  //
  Obs := Trim(QrMDFeLayIObservacao.Value);
  if Obs <> '' then
  Result := Result + sLineBreak + 'Observa��es: ' + Obs;
  //
  Result := Result + sLineBreak + sLineBreak +
    'ID MOC # ' + Geral.FF0(Grupo) + '.' + Geral.FF0(Codigo);
end;

function TDmMDFe_0000.ObtemDirXML(const Ext: String; var Dir: String;
  Assinado: Boolean): Boolean;
var
  Titulo, Descri: String;
begin
  //Result := False;
{
7.3. Padr�o de nomes para os arquivos
Visando facilitar o processo de guarda dos arquivos pelos leg�timos interessados, foi criado um
padr�o de nome para os diversos tipos de arquivos utilizados pelo sistema MDF-e. S�o eles:
� MDF-e: O nome do arquivo ser� a chave de acesso completa com extens�o �-
mdfe.xml�;
� Recibo: O nome do arquivo ser� o n�mero do lote com extens�o �-rec.xml�;
� Pedido do Resultado do Processamento do MDF-e: O nome do arquivo ser� o
n�mero do recibo com extens�o �-ped-rec.xml�;
� Resultado do Processamento do MDF-e: O nome do arquivo ser� o n�mero do recibo
com extens�o �-pro-rec.xml�;
� Pedido de Registro de Evento de MDF-e: O nome do arquivo ser� a chave de acesso
completa com extens�o �-ped-eve.xml�;
� Registro de Evento de MDF-e: O nome do arquivo ser� a chave de acesso completa
com extens�o �-env.xml�;
� Pedido de Consulta Situa��o Atual do MDF-e: O nome do arquivo ser� a chave de
acesso completa com extens�o �-ped-sit.xml�;
� Situa��o Atual do MDF-e: O nome do arquivo ser� a chave de acesso completa com
extens�o �-sit.xml�;
Manifesto Eletr�nico de Documentos Fiscais
Manual de Orienta��es do Contribuinte
79
� Pedido de Consulta do Status do Servi�o: O nome do arquivo ser�:
�AAAAMMDDTHHMMSS� do momento da consulta com extens�o �-ped-sta.xml�;
� Status do Servi�o: O nome do arquivo ser�: �AAAAMMDDTHHMMSS� do momento da
consulta com extens�o �-sta.xml�;
O padr�o de nomenclatura tamb�m facilitar� o aplicativo visualizador do MDF-e.
}
  Dir := '';
//� MDF-e: O nome do arquivo ser� a chave de acesso completa com extens�o �- mdfe.xml�;
  if Ext = MDFE_EXT_MDFE_XML  then
  begin
    if Assinado then
      Dir := QrFilialDirMDFeAss.Value
    else
      Dir := QrFilialDirMDFeGer.Value;
// Envio de lote n�o tem no manual!!!
  end else
    if Ext = MDFE_EXT_ENV_LOT_XML then
      Dir := QrFilialDirMDFeEnvLot.Value
//� Recibo: O nome do arquivo ser� o n�mero do lote com extens�o �-rec.xml�;
  else
    if Ext = MDFE_EXT_REC_XML then
      Dir := QrFilialDirMDFeRec.Value
//� Pedido do Resultado do Processamento do MDF-e: O nome do arquivo
//ser� o n�mero do recibo com extens�o �-ped-rec.xml�;
  else
    if Ext = MDFE_EXT_PED_REC_XML then
      Dir := QrFilialDirMDFePedRec.Value
//� Resultado do Processamento do MDF-e: O nome do arquivo ser�
//o n�mero do recibo com extens�o �-pro-rec.xml�;
  else
    if Ext = MDFE_EXT_PRO_REC_XML then
      Dir := QrFilialDirMDFeProRec.Value
//� Pedido de Registro de Evento de MDF-e: O nome do arquivo ser�
//a chave de acesso completa com extens�o �-ped-eve.xml�;
  else
    if Ext = MDFE_EXT_PED_EVE_XML then
      Dir := QrFilialDirMDFeEveEnvLot.Value
//� Registro de Evento de MDF-e: O nome do arquivo ser�
//a chave de acesso completa com extens�o �-env.xml�;
  else
    if Ext = MDFE_EXT_ENV_XML then
      Dir := QrFilialDirMDFeEveRetLot.Value
//� Pedido de Consulta Situa��o Atual do MDF-e: O nome do arquivo ser�
//a chave de acesso completa com extens�o �-ped-sit.xml�;
  else
    if Ext = MDFE_EXT_PED_SIT_XML then
      Dir := QrFilialDirMDFePedSit.Value
//� Situa��o Atual do MDF-e: O nome do arquivo ser�
//a chave de acesso completa com extens�o �-sit.xml�;
  else
    if Ext = MDFE_EXT_SIT_XML then
      Dir := QrFilialDirMDFeSit.Value
//� Pedido de Consulta do Status do Servi�o: O nome do arquivo ser�:
//�AAAAMMDDTHHMMSS� do momento da consulta com extens�o �-ped-sta.xml�;
  else
    if Ext = MDFE_EXT_PED_STA_XML then
      Dir := QrFilialDirMDFePedSta.Value
//� Status do Servi�o: O nome do arquivo ser�:
//�AAAAMMDDTHHMMSS� do momento da consulta com extens�o �-sta.xml�;
  else
    if Ext = MDFE_EXT_STA_XML then
      Dir := QrFilialDirMDFeSta.Value
//O padr�o de nomenclatura tamb�m facilitar� o aplicativo visualizador do MDF-e.

(*
  else
    if Ext = MDFE_EXT_EVE_XML then
      Dir := QrFilialDirMDFeEveRetLot.Value
  else
    if Ext = MDFE_EXT_DEN_XML then
      Dir := QrFilialDirMDFeDen.Value
  else
    if Ext = MDFE_EXT_PED_CAN_XML then
      Dir := QrFilialDirMDFePedCan.Value
  else
    if Ext = MDFE_EXT_CAN_XML then
      Dir := QrFilialDirMDFeCan.Value
  else
    if Ext = MDFE_EXT_EVE_RET_CAN_XML then
      Dir := QrFilialDirMDFeCan.Value
  else
    if Ext = MDFE_EXT_PED_INU_XML then
      Dir := QrFilialDirMDFePedInu.Value
  else
    if Ext = MDFE_EXT_INU_XML then
      Dir := QrFilialDirMDFeInu.Value
  else
    if Ext = MDFE_EXT_MDFE_WEB_XML then
      Dir := QrFilialDirMDFeMDFERWeb.Value
  else
    if Ext = MDFE_EXT_EVE_ENV_CCE_XML then
      Dir := QrFilialDirMDFeEvePedCCe.Value
  else
    if Ext = MDFE_EXT_EVE_RET_CCE_XML then
      Dir := QrFilialDirMDFeEveRetCCe.Value
*)
  else
    if Ext = MDFE_EXT_EVE_ENV_CAN_XML then
      Dir := QrFilialDirMDFeEvePedCan.Value
  else
    if Ext = MDFE_EXT_EVE_RET_CAN_XML then
      Dir := QrFilialDirMDFeEveRetCan.Value
  else
    if Ext = MDFE_EXT_EVE_ENV_ENC_XML then
      Dir := QrFilialDirMDFeEvePedEnc.Value
  else
    if Ext = MDFE_EXT_EVE_RET_ENC_XML then
      Dir := QrFilialDirMDFeEveRetEnc.Value
  else
    if Ext = MDFE_EXT_EVE_ENV_IDC_XML then
      Dir := QrFilialDirMDFeEvePedIdC.Value
  else
    if Ext = MDFE_EXT_EVE_RET_IDC_XML then
      Dir := QrFilialDirMDFeEveRetIdC.Value
(*
  else
    if Ext = MDFE_EXT_RET_MDFE_DES_XML then
      Dir := QrFilialDirMDFeRetMDFEDes.Value
  else
    if Ext = MDFE_EXT_EVE_ENV_MDE_XML then
      Dir := QrFilialDirMDFeEvePedMDe.Value
  else
    if Ext = MDFE_EXT_EVE_RET_MDE_XML then
      Dir := QrFilialDirMDFeEveRetMDe.Value
  else
    if Ext = MDFE_EXT_RET_DOW_MDFE_DES_XML then
      Dir := QrFilialDirMDFeDowMDFEDes.Value
  else
    if Ext = MDFE_EXT_RET_DOW_MDFE_MDFE_XML then
      Dir := QrFilialDirMDFeDowMDFEMDFE.Value
  else
    if Ext = MDFE_EXT_PED_DFE_DIS_INT_XML then
      Dir := QrFilialDirMDFeDistDFeInt.Value
  else
    if Ext = MDFE_EXT_RET_DFE_DIS_INT_XML then
      Dir := QrFilialDirMDFeRetDistDFeInt.Value
  else
    if Ext = MDFE_EXT_RET_DOW_MDFE_CNF_XML then
      Dir := QrFilialDirMDFeDowMDFECnf.Value
  else
    if Ext = MDFE_EXT_RET_CNF_MDFE_MDFE_XML then
      Dir := QrFilialDirMDFeDowMDFEMDFE.Value
*)
  //
  else
  Geral.MB_Erro('Tipo de (final de) nome de arquivo XML desconhecido: ' + Ext);
  //
  if Dir <> '' then
  begin
    TituloArqXML(Ext, Titulo, Descri);
    Result := Geral.VerificaDir(Dir, '\', Titulo + sLineBreak + Descri, True);
  end else
  begin
    Geral.MB_Aviso(
    'Diret�rio XML n�o definido no cadastro da filial para o tipo: ' + Ext);
    Result := False;
  end;
end;

function TDmMDFe_0000.Obtem_Arquivo_XML_(Empresa: Integer; Ext, Arq: String;
  Assinado: Boolean): String;
var
  Dir: String;
begin
  Result := '';
  //
  if not ReopenEmpresa(Empresa) then Exit;
  //
  if ObtemDirXML(Ext, Dir, Assinado) then
    Result := Dir + Arq + Ext;
end;

procedure TDmMDFe_0000.QrFrtMnfInfDocCMunAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfInfDocXMun, Dmod.MyDB, [
  'SELECT DISTINCT Controle, xMundescarga  ',
  'FROM frtmnfinfdoc ',
  'WHERE Codigo=' + Geral.FF0(QrFrtMnfInfDocCMunCodigo.Value),
  'AND cMundescarga=' + Geral.FF0(QrFrtMnfInfDocCMuncMundescarga.Value),
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfInfDoc57, Dmod.MyDB, [
  'SELECT *  ',
  'FROM frtmnfinfdoc ',
  'WHERE Codigo=' + Geral.FF0(QrFrtMnfInfDocCMunCodigo.Value),
  'AND cMundescarga=' + Geral.FF0(QrFrtMnfInfDocCMuncMundescarga.Value),
  'AND DocMod=' + Geral.FF0(Integer(TTipoXXe.tipoxxeCTe)),
  'ORDER BY Controle',
  ' ']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfInfDoc55, Dmod.MyDB, [
  'SELECT *  ',
  'FROM frtmnfinfdoc ',
  'WHERE Codigo=' + Geral.FF0(QrFrtMnfInfDocCMunCodigo.Value),
  'AND cMundescarga=' + Geral.FF0(QrFrtMnfInfDocCMuncMundescarga.Value),
  'AND DocMod=' + Geral.FF0(Integer(TTipoXXe.tipoxxeNFe)),
  'ORDER BY Controle',
  ' ']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfInfDoc58, Dmod.MyDB, [
  'SELECT *  ',
  'FROM frtmnfinfdoc ',
  'WHERE Codigo=' + Geral.FF0(QrFrtMnfInfDocCMunCodigo.Value),
  'AND cMundescarga=' + Geral.FF0(QrFrtMnfInfDocCMuncMundescarga.Value),
  'AND DocMod=' + Geral.FF0(Integer(TTipoXXe.tipoxxeMDFe)),
  'ORDER BY Controle',
  ' ']);
end;

procedure TDmMDFe_0000.QrOpcoesMDFeAfterOpen(DataSet: TDataSet);
const
  Formata = True;
  //
  function Fmt(Val: Double): String;
  begin
    if Formata then
      Result := Geral.FFT_Dot(Val, 2, siNegativo)
    else
      Result := Geral.FFI(Val);
  end;
begin
  VAR_TXT_versmdfeStatusServico         := Fmt(QrOpcoesMDFeMDFeVerStaSer.Value);
  VAR_TXT_versmdfeRecepcao              := Fmt(QrOpcoesMDFeMDFeVerConLot.Value);
  VAR_TXT_versmdfeRetRecepcao           := Fmt(QrOpcoesMDFeMDFeVerConLot.Value);
  VAR_TXT_versmdfeEveCancelamento       := Fmt(QrOpcoesMDFeMDFeVerLotEve.Value);
  VAR_TXT_versmdfePedInutilizacao       := Fmt(QrOpcoesMDFeMDFeVerLotEve.Value);
  VAR_TXT_versmdfeConsultaMDFe          := Fmt(QrOpcoesMDFeMDFeVerConMDFe.Value);
  VAR_TXT_versmdfeEveIncCondutor        := Fmt(QrOpcoesMDFeMDFeVerLotEve.Value);
  VAR_TXT_versmdfe7      := Fmt(0);
  VAR_TXT_versmdfeEveEncerramento       := Fmt(QrOpcoesMDFeMDFeVerLotEve.Value);
  VAR_TXT_versmdfeConsultaNaoEncerrados := Fmt(QrOpcoesMDFeMDFeVerNaoEnc.Value);
  //
  //MaxVersaoConsumoWS_MDFe = Integer(High(TTipoConsumoWS_MDFe));
/// ATEN��O!!! MUDAR ABAIXO O SEQUENCIAL!!
////////////////////////////////////////////////////////////////////////////////
  sVersaoConsumoWS_MDFe[-MaxTipoConsumoWS_MDFe+09] := VAR_TXT_versmdfeStatusServico;
  sVersaoConsumoWS_MDFe[-MaxTipoConsumoWS_MDFe+10] := VAR_TXT_versmdfeRecepcao;
  sVersaoConsumoWS_MDFe[-MaxTipoConsumoWS_MDFe+11] := VAR_TXT_versmdfeRetRecepcao;
  sVersaoConsumoWS_MDFe[-MaxTipoConsumoWS_MDFe+12] := VAR_TXT_versmdfeEveCancelamento;
  sVersaoConsumoWS_MDFe[-MaxTipoConsumoWS_MDFe+13] := VAR_TXT_versmdfePedInutilizacao;
  sVersaoConsumoWS_MDFe[-MaxTipoConsumoWS_MDFe+14] := VAR_TXT_versmdfeConsultaMDFe;
  sVersaoConsumoWS_MDFe[-MaxTipoConsumoWS_MDFe+15] := VAR_TXT_versmdfeEveIncCondutor;
  sVersaoConsumoWS_MDFe[-MaxTipoConsumoWS_MDFe+16] := VAR_TXT_versmdfe7;
  sVersaoConsumoWS_MDFe[-MaxTipoConsumoWS_MDFe+17] := VAR_TXT_versmdfeEveEncerramento;
  sVersaoConsumoWS_MDFe[-MaxTipoConsumoWS_MDFe+18] := VAR_TXT_versmdfeConsultaNaoEncerrados;
  //
end;

function TDmMDFe_0000.ReopenEmpresa(Empresa: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmpresa, Dmod.MyDB, [
  'SELECT en.Codigo, en.Tipo, en.CNPJ, en.CPF, ',
  'en.IE, en.IEST, en.RG, en.NIRE, en.CNAE,  uf.DTB DTB_UF, ',
  'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_ENT, ',
  'IF(en.Tipo=0,en.Fantasia,en.Apelido) FANTASIA, ',
  'IF(en.Tipo=0,en.ERua,en.PRua) RUA, ',
  'IF(en.Tipo=0,en.ENumero,en.PNumero) + 0.000 Numero, ',
  'IF(en.Tipo=0,en.ECompl,en.PCompl) COMPL, ',
  'IF(en.Tipo=0,en.EBairro,en.PBairro) BAIRRO, ',
  'IF(en.Tipo=0,en.ECEP,en.PCEP) + 0.000 CEP, ',
  'IF(en.Tipo=0,en.ETe1,en.PTe1) Te1, ',
  'IF(en.Tipo=0,en.EUF,en.PUF) + 0.000 UF, ',
  'IF(en.Tipo=0,en.ECodiPais,en.PCodiPais) + 0.000 CodiPais, ',

  'IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) + 0.000 CodMunici, ',
  'll.Nome NO_LOGRAD, mu.Nome NO_Munici, uf.Nome NO_UF, ',
  'bpa.Nome NO_Pais, en.SUFRAMA, en.Filial ',

  'FROM entidades en ',
  'LEFT JOIN listalograd ll ON ll.Codigo=IF(en.Tipo=0,en.ELograd,en.PLograd) ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mu ON mu.Codigo=IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) ',
  'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF) ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais bpa ON bpa.Codigo=IF(en.Tipo=0,en.ECodiPais,en.PCodiPais) ',
  'WHERE en.Codigo=' + Geral.FF0(Empresa),
  '']);
  //
  QrFilial.Close;
  QrFilial.Params[0].AsInteger := Empresa;
  UnDmkDAC_PF.AbreQuery(QrFilial, Dmod.MyDB);
  //
  Result := (QrEmpresa.RecordCount > 0) and (QrFilial.RecordCount > 0);
  if not Result then Geral.MB_Aviso(
  'N�o foi poss�vel reabrir as tabelas da empresa ' + IntToStr(Empresa) + '!');
end;

procedure TDmMDFe_0000.ReopenEntiMDFe(Entidade: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntiMDFe, Dmod.MyDB, [
  'SELECT ent.Tipo, ent.SUFRAMA, ',
  'IF(ent.Tipo=0, ent.CNPJ, "") CNPJ,     ',
  'IF(ent.Tipo=1, ent.CPF, "") CPF,     ',
  'IF(ent.Tipo=0, ent.IE, "") IE,     ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) xNome,     ',
  'IF(ent.Tipo=0, ent.Fantasia, ent.Apelido) xFant,     ',
  'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) Fone, ',
  'IF(ent.Tipo=0, ent.ERua, ent.PRua) xLgr, ',
  'IF(IF(ent.Tipo=0, ent.ENumero, ent.PNumero) <>0, ',
  'IF(ent.Tipo=0, ent.ENumero, ent.PNumero), "S/N") Nro, ',
  'IF(ent.Tipo=0, ent.ECompl,   ent.PCompl) xCpl, ',
  'IF(ent.Tipo=0, ent.EBairro,   ent.PBairro) xBairro, ',
  'IF(ent.Tipo=0, ent.ECodMunici, ent.PCodMunici) + 0.000 CMun, ',
  'mun.Nome xMun, ',
  'IF(ent.Tipo=0,  ent.ECEP, ent.PCEP) + 0.000 CEP, ',
  'ufs.Nome UF,',
  'IF(ent.Tipo=0, ent.ECodiPais, ent.PCodiPais) + 0.000 CPais,',
  'bpa.Nome xPais, ',
  'IF(ent.Tipo=0, ent.EEmail, ent.PEmail) Email',
  'FROM entidades ent ',
  'LEFT JOIN loclogiall.dtb_munici mun ',
  ' ON mun.Codigo=IF(ent.Tipo=0, ent.ECodMunici, ent.PCodMunici) ',
  'LEFT JOIN ufs ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PUF) ',
  'LEFT JOIN loclogiall.bacen_pais bpa ',
  ' ON bpa.Codigo=IF(ent.Tipo=0, ent.ECodiPais, ent.PCodiPais) ',
  'WHERE ent.Codigo=' + Geral.FF0(Entidade),
  '']);
end;

procedure TDmMDFe_0000.ReopenFrtMnfAutXML(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfAutXML, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtmnfautxml ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;

(*
procedure TDmMDFe_0000.ReopenFrtMnfInfDoc(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfInfDoc, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtmnfinfdoc ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;
*)

procedure TDmMDFe_0000.ReopenFrtMnfInfPercurso(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfInfPercurso, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtmnfinfpercurso ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmMDFe_0000.ReopenFrtMnfLacres(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfLacres, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtmnflacres ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmMDFe_0000.ReopenFrtMnfMoRodoCIOT(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfMoRodoCIOT, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtmnfmorodociot ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmMDFe_0000.ReopenFrtMnfMoRodoCondutor(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfMoRodoCondutor, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtmnfmorodocondutor ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmMDFe_0000.ReopenFrtMnfMoRodoValePedg(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfMoRodoValePedg, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtmnfmorodovalepedg ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmMDFe_0000.ReopenFrtMnfMoRodoVeicRebo(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfMoRodoVeicRebo, Dmod.MyDB, [
  'SELECT rcv.*, its.Nome NO_VEIC, its.Placa ',
  'FROM frtmnfmorodoveicrebo rcv ',
  'LEFT JOIN trnsits its ON its.Controle=rcv.cInt ',
  'WHERE rcv.Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmMDFe_0000.ReopenFrtMnfMunCarrega(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfMunCarrega, Dmod.MyDB, [
  'SELECT * ',
  'FROM frtmnfmuncarrega ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmMDFe_0000.ReopenFrtMnfDocCMun(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrtMnfInfDocCMun, Dmod.MyDB, [
  'SELECT DISTINCT Codigo, cMundescarga ',
  'FROM frtmnfinfdoc ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmMDFe_0000.ReopenMDFeArq(IDCtrl: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeArq, Dmod.MyDB, [
  'SELECT caba.IDCtrl IDCtrl_CabA, arq.IDCtrl IDCtrl_Arq, ',
  'caba.Id, caba.FatID, caba.FatNum, caba.Empresa ',
  'FROM mdfecaba caba ',
  'LEFT JOIN mdfearq arq ON arq.IDCtrl=caba.IDCtrl ',
  'WHERE caba.IDCtrl=' + Geral.FF0(IDCtrl),
  '']);
end;

function TDmMDFe_0000.ReopenMDFeCabA(FatID, FatNum, Empresa: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeCabA, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfecaba ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
  //
  if QrMDFeCabA.RecordCount = 0 then
    Result := -1
  else
    Result := QrMDFeCabAStatus.Value;
end;

procedure TDmMDFe_0000.ReopenMDFeIt1AutXml(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeIt1AutXml, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfeit1autxml ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

procedure TDmMDFe_0000.ReopenMDFeIt1Lacres(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeIt1Lacres, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfeit1lacres ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

procedure TDmMDFe_0000.ReopenMDFeit2InfMunCarrega(FatID, FatNum,
  Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeit2InfMunCarrega, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfeit2infmuncarrega ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

procedure TDmMDFe_0000.ReopenMDFeit2InfMunDescarga(FatID, FatNum,
  Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeit2InfMunDescarga, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfeit2infmundescarga ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

procedure TDmMDFe_0000.ReopenMDFeit2InfPercurso(FatID, FatNum,
  Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeit2InfPercurso, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfeit2infpercurso ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

procedure TDmMDFe_0000.ReopenMDFeit3InfCTe(FatID, FatNum, Empresa: Integer;
  Controle: Int64);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeit3InfCTe, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfeit3infcte ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND Controle=' + Geral.FI64(Controle),
  '']);
end;

procedure TDmMDFe_0000.ReopenMDFeit3InfMDFe(FatID, FatNum, Empresa: Integer;
  Controle: Int64);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeit3InfMDFe, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfeit3infmdfe ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND Controle=' + Geral.FI64(Controle),
  '']);
end;

procedure TDmMDFe_0000.ReopenMDFeit3InfNFe(FatID, FatNum, Empresa: Integer;
  Controle: Int64);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeit3InfNFe, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfeit3infnfe ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND Controle=' + Geral.FI64(Controle),
  '']);
end;

procedure TDmMDFe_0000.ReopenMDFeLayI;
begin
  if (QrMDFeLayI.State = dsInactive) or
  (FLastVerMDFeLayI <> QrOpcoesMDFeMDFeVersao.Value) then
  begin
    FLastVerMDFeLayI := QrOpcoesMDFeMDFeVersao.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeLayI, Dmod.MyDB, [
    'SELECT layc.Nome NO_Grupo, layi.* ',
    'FROM mdfelayi layi ',
    'LEFT JOIN mdfelayc layc ON layc.Grupo=layi.Grupo ',
    'WHERE layi.Versao=' + Geral.FFT_Dot(FLastVerMDFeLayI, 2, siNegativo),
    '']);
  end;
  VerificaTabelaLayI();
end;

procedure TDmMDFe_0000.ReopenMDFeMoRodoCab(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeMoRodoCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfemorodocab ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

procedure TDmMDFe_0000.ReopenMDFeMoRodoCIOT(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeMoRodoCIOT, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfemorodociot ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

procedure TDmMDFe_0000.ReopenMDFeMoRodoCondutor(FatID, FatNum,
  Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeMoRodoCondutor, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfemorodocondutor ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

procedure TDmMDFe_0000.ReopenMDFeMoRodoPropR(FatID, FatNum, Empresa, Controle:
  Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeMoRodoPropR, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfemorodopropr ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND Controle=' + Geral.FF0(Controle),
  '']);
end;

procedure TDmMDFe_0000.ReopenMDFeMoRodoPropT(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeMoRodoPropT, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfemorodopropt ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

procedure TDmMDFe_0000.ReopenMDFeMoRodoVeicRebo(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeMoRodoVeicRebo, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfemorodoveic ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

procedure TDmMDFe_0000.ReopenMDFeMoRodoVPed(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeMoRodoVPed, Dmod.MyDB, [
  'SELECT * ',
  'FROM mdfemorodovped ',
  'WHERE FatID=' + Geral.FF0(FatID),
  'AND FatNum=' + Geral.FF0(FatNum),
  'AND Empresa=' + Geral.FF0(Empresa),
  '']);
end;

function TDmMDFe_0000.ReopenOpcoesMDFe(QrOpMDFe: TmySQLQuery; Empresa: Integer;
  Avisa: Boolean): Boolean;
begin
  if (Empresa > 0) or (EMPRESA <> VAR_LIB_EMPRESA_SEL) then
    Geral.MB_Aviso('CUIDADO!!! Empresa = ' + IntToStr(Empresa) + ' (Op��es MDF-e)');
  UnDmkDAC_PF.AbreMySQLQuery0(QrOpMDFe, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF, ',
  'pem.*' ,
  'FROM paramsemp pem' ,
  'LEFT JOIN entidades ent ON ent.Codigo=pem.Codigo ',
  'WHERE pem.Codigo=' + Geral.FF0(Empresa),
  '']);
  Result := QrOpMDFe.RecordCount > 0;
  if not Result and Avisa then
    Geral.MB_Aviso('N�o existem op��es de MDF-e cadastradas!');
end;

procedure TDmMDFe_0000.ReopenTerceiro(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTerceiro, Dmod.MyDB, [
  'SELECT tcb.RNTRC, ent.Tipo, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
  'IF(ent.Tipo=0, "", ent.CPF) CPF, ',
  'IF(ent.Tipo=0, ent.CNPJ, "") CNPJ, ',
  'IF(ent.Tipo=0, ent.IE, "") IE, ',
  'ufs.Nome UF, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
  'ent.CPF, ent.CNPJ  ',
  'FROM entidades ent ',
  'LEFT JOIN ufs ufs ON ufs.Codigo=(IF(ent.Tipo=0, ent.EUF, ent.PUF)) ',
  'LEFT JOIN trnscad tcb ON tcb.Codigo=ent.Codigo ',
  'WHERE ent.Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmMDFe_0000.ReopenTrnsCad(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTrnsCad, Dmod.MyDB, [
  'SELECT * ',
  'FROM trnscad ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmMDFe_0000.ReopenTrnsIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTrnsIts, Dmod.MyDB, [
  'SELECT cad.RNTRC, cad.tpProp, its.* ',
  'FROM trnsits its ',
  'LEFT JOIN trnscad cad ON cad.Codigo=its.Codigo ',
  'WHERE its.Controle=' + Geral.FF0(Controle),
  '']);
end;

function TDmMDFe_0000.SalvaXML(Ext, Arq, Texto: String; rtfRetWS: TMemo;
  Assinado: Boolean): String;
var
  Dir, Arquivo, xxx: String;
  TxtXML: TextFile;
  P: Integer;
begin
  if ObtemDirXML(Ext, Dir, Assinado) then
  begin
    P := pos(Ext, Arq);
    if P = 0 then
    begin
      // Usar extens�o correta para xml de MDFe!
      if (Ext = MDFE_EXT_RET_DOW_MDFE_MDFE_XML)
      or (Ext = MDFE_EXT_RET_CNF_MDFE_MDFE_XML) then
        xxx := MDFE_EXT_MDFE_XML
      else
        xxx := Ext;
      Arquivo := Dir + Arq + xxx
    end else
      Arquivo := Dir + Arq; // j� tem Ext ?
    //
    AssignFile(TxtXML, Arquivo);
    Rewrite(TxtXML);
    Write(TxtXML, Texto);
    CloseFile(TxtXML);
    //
    Result := Arquivo;
    if rtfRetWS <> nil then
      rtfRetWS.Text := Texto;
  end else
    Result := '';
end;

function TDmMDFe_0000.StepMDFeCab(FatID, FatNum, Empresa: Integer;
  Status: TMDFeMyStatus; LaAviso1, LaAviso2: TLabel): Boolean;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True,
  'Atualizando status da MDF-e para ' + IntToStr(Integer(Status)));
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfecaba', False, [
  'Status'], [
  'FatID', 'FatNum', 'Empresa'], [
  Integer(Status)], [
  FatID, FatNum, Empresa], True);
end;

function TDmMDFe_0000.TituloArqXML(const Ext: String; var Titulo,
  Descri: String): Boolean;
begin
  Result := True;
  if Ext = MDFE_EXT_MDFE_XML then
  begin
    Titulo := 'MDF-e';
    Descri := 'O nome do arquivo ser� a chave de acesso completa com extens�o �-mdfe.xml�';
  end else
  if Ext = MDFE_EXT_ENV_LOT_XML then
  begin
    Titulo := 'Envio de Lote de MDF-e';
    Descri := 'O nome do arquivo ser� o n�mero do lote com extens�o �-env-lot.xml�';
  end else
  if Ext = MDFE_EXT_REC_XML then
  begin
    Titulo := 'Recibo';
    Descri := 'O nome do arquivo ser� o n�mero do lote com extens�o �-rec.xml�';
  end else
  if Ext = MDFE_EXT_PED_REC_XML then
  begin
    Titulo := 'Pedido do Resultado do Processamento do Lote de MDF-e';
    Descri := 'O nome do arquivo ser� o n�mero do recibo com extens�o �-ped-rec.xml�';
  end else
  if Ext = MDFE_EXT_PRO_REC_XML then
  begin
    Titulo := 'Resultado do Processamento do Lote de MDF-e';
    Descri := 'O nome do arquivo ser� o n�mero do recibo com extens�o �-pro-rec.xml�';
  end else
(*
  if Ext = MDFE_EXT_DEN_XML then
  begin
    Titulo := 'Denega��o de Uso';
    Descri := 'O nome do arquivo ser� a chave de acesso completa com extens�o �-den.xml�';
  end else
  if Ext = MDFE_EXT_PED_CAN_XML then
  begin
    Titulo := 'Pedido de Cancelamento de MDF-e';
    Descri := 'O nome do arquivo ser� a chave de acesso completa com extens�o �-ped-can.xml�';
  end else
  if Ext = MDFE_EXT_CAN_XML then
  begin
    Titulo := 'Cancelamento de MDF-e';
    Descri := 'O nome do arquivo ser� a chave de acesso completa com extens�o �-can.xml�';
  end else
  if Ext = MDFE_EXT_PED_INU_XML then
  begin
    Titulo := 'Pedido de Inutiliza��o de Numera��o';
    Descri := 'O nome do arquivo ser� composto por: UF + Ano de inutiliza��o + CNPJ do emitente + Modelo + S�rie + N�mero Inicial + N�mero Final com extens�o �-ped-inu.xml�';
  end else
  if Ext = MDFE_EXT_INU_XML then
  begin
    Titulo := 'Inutiliza��o de Numera��o';
    Descri := 'O nome do arquivo ser� composto por: Ano de inutiliza��o + CNPJ do emitente + Modelo + S�rie + N�mero Inicial + N�mero Final com extens�o �-inu.xml�';
  end else
*)
  if Ext = MDFE_EXT_PED_SIT_XML then
  begin
    Titulo := 'Pedido de Consulta Situa��o Atual da MDF-e';
    Descri := 'O nome do arquivo ser� a chave de acesso completa com extens�o �-ped-sit.xml�';
  end else
  if Ext = MDFE_EXT_SIT_XML then
  begin
    Titulo := 'Situa��o Atual da MDF-e';
    Descri := 'O nome do arquivo ser� a chave de acesso completa com extens�o �-sit.xml�';
  end else
  if Ext = MDFE_EXT_PED_STA_XML then
  begin
    Titulo := 'Pedido de Consulta do Status do Servi�o';
    Descri := 'O nome do arquivo ser�: �AAAAMMDDTHHMMSS� do momento da consulta com extens�o �-ped-sta.xml�';
  end else
  if Ext = MDFE_EXT_STA_XML then
  begin
    Titulo := 'Status do Servi�o';
    Descri := 'O nome do arquivo ser�: �AAAAMMDDTHHMMSS� do momento da consulta com extens�o �-sta.xml�';
  end else
  begin
    Result := False;
    Titulo := 'Tipo de arquivo XML desconhecido para MDF-e';
    Descri := 'AVISE A DERMATEK!';
  end;
end;

function TDmMDFe_0000.VerificaTabelaLayI: Boolean;
begin
  Result := QrMDFeLayI.RecordCount > 0;
  if not result then
  begin
    QrMDFeLayI.Close;
    Geral.MB_Aviso('Tabela "MDFeLayI" vazia!');
  end;
end;

end.
