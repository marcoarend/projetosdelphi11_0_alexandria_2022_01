unit UnMDFeListas;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls,
  UnInternalConsts2, ComCtrls, dmkGeral, dmkEdit, Variants, UnDmkEnums;

var
  VAR_TXT_versmdfeStatusServico:         string = '1.00';
  VAR_TXT_versmdfeRecepcao:              string = '1.00';
  VAR_TXT_versmdfeRetRecepcao:           string = '1.00';
  VAR_TXT_versmdfeEveCancelamento:       string = '1.00';
  VAR_TXT_versmdfePedInutilizacao:       string = '1.00';
  VAR_TXT_versmdfeConsultaMDFe:          string = '1.00';
  VAR_TXT_versmdfeEveIncCondutor:        string = '1.00';
  VAR_TXT_versmdfe7:       string = '1.00';
  VAR_TXT_versmdfeEveEncerramento:       string = '1.00';
  VAR_TXT_versmdfeConsultaNaoEncerrados: string = '1.00';
const

  CO_TXT_tcwsmdfeStatusServico         = 'MdfeStatusServico';
  CO_TXT_tcwsmdfeRecepcao              = 'MdfeRecepcao';
  CO_TXT_tcwsmdfeRetRecepcao           = 'MdfeRetRecepcao';
  CO_TXT_tcwsmdfeEveCancelamento       = 'MdfeRecepcaoEvento';
  CO_TXT_tcwsmdfe4     = '';
  CO_TXT_tcwsmdfeConsultaMDFe          = 'MdfeConsulta';
  CO_TXT_tcwsmdfeEveIncCondutor        = 'MdfeRecepcaoEvento';
  CO_TXT_tcwsmdfe7     = '';
  CO_TXT_tcwsmdfeEveEncerramento       = 'MdfeRecepcaoEvento';
  CO_TXT_tcwsmdfeConsultaNaoEncerrados = 'MdfeConsNaoEnc';
  MaxTipoConsumoWS_MDFe = Integer(High(TTipoConsumoWS_MDFe));
  sTipoConsumoWS_MDFe: array[0..MaxTipoConsumoWS_MDFe] of string = (
  CO_TXT_tcwsmdfeStatusServico,
  CO_TXT_tcwsmdfeRecepcao,
  CO_TXT_tcwsmdfeRetRecepcao,
  CO_TXT_tcwsmdfeEveCancelamento,
  CO_TXT_tcwsmdfe4,
  CO_TXT_tcwsmdfeConsultaMDFe,
  CO_TXT_tcwsmdfeEveIncCondutor,
  CO_TXT_tcwsmdfe7,
  CO_TXT_tcwsmdfeEveEncerramento,
  CO_TXT_tcwsmdfeConsultaNaoEncerrados
  );
  CO_TXT_txwsmdfeStatusServico         = 'Status Servi�o';
  CO_TXT_txwsmdfeRecepcao              = 'Recep��o de lote de MDF-e';
  CO_TXT_txwsmdfeRetRecepcao           = 'Retorno de lote de MDF-e';
  CO_TXT_txwsmdfeEveCancelamento       = 'Evento cancelamento de MDF-e';
  CO_TXT_txwsmdfe4     = '';
  CO_TXT_txwsmdfeConsultaMDFe          = 'Consulta Status de MDF-e';
  CO_TXT_txwsmdfeEveIncCondutor        = 'Evento de inclus�o de condutor';
  CO_TXT_txwsmdfe7     = '';
  CO_TXT_txwsmdfeEveEncerramento       = 'Evento de encerramento de MDF-e';
  CO_TXT_txwsmdfeConsultaNaoEncerrados = 'Consulta de MDF-es n�o encerrados';
  MaxNomeConsumoWS_MDFe = Integer(High(TTipoConsumoWS_MDFe));
  sNomeConsumoWS_MDFe: array[0..MaxTipoConsumoWS_MDFe] of string = (
  CO_TXT_txwsmdfeStatusServico,
  CO_TXT_txwsmdfeRecepcao,
  CO_TXT_txwsmdfeRetRecepcao,
  CO_TXT_txwsmdfeEveCancelamento,
  CO_TXT_txwsmdfe4,
  CO_TXT_txwsmdfeConsultaMDFe,
  CO_TXT_txwsmdfeEveIncCondutor,
  CO_TXT_txwsmdfe7,
  CO_TXT_txwsmdfeEveEncerramento,
  CO_TXT_txwsmdfeConsultaNaoEncerrados
  );
////////////////////////////////////////////////////////////////////////////////
//veja CO_TXT_verscte...
  MaxVersaoConsumoWS_CTe = Integer(High(TTipoConsumoWS_CTe));
////////////////////////////////////////////////////////////////////////////////

  CO_TXT_mdfetpemitIndef              = 'Indefinido';
  CO_TXT_mdfetpemitPrestadorServico   = 'Prestador de servi�o de transporte';
  CO_TXT_mdfetpemitCargaPropria       = 'Transportador de carga pr�pria';
  MaxTipoEmitenteMDFe = Integer(High(TMDFeTipoEmitente));
  sTipoEmitenteMDFe: array[0..MaxTipoEmitenteMDFe] of string = (
  CO_TXT_mdfetpemitIndef              ,
  CO_TXT_mdfetpemitPrestadorServico   ,
  CO_TXT_mdfetpemitCargaPropria
  );

  CO_TXT_mdfemodalIndefinido     = 'Indefinido';
  CO_TXT_mdfemodalRodoviario     = 'Rodovi�rio';
  CO_TXT_mdfemodalAereo          = 'A�reo';
  CO_TXT_mdfemodalAquaviario     = 'Aquavi�rio';
  CO_TXT_mdfemodalFerroviario    = 'Ferrovi�rio';
  //CO_TXT_mdfemodalDutoviario     = 'Dutovi�rio';
  //CO_TXT_mdfemodalMultimodal     = 'Multimodal';
  MaxMDFeModal = Integer(High(TMDFeModal));
  sMDFeModal: array[0..MaxMDFeModal] of string = (
  CO_TXT_mdfemodalIndefinido     ,
  CO_TXT_mdfemodalRodoviario     ,
  CO_TXT_mdfemodalAereo          ,
  CO_TXT_mdfemodalAquaviario     ,
  CO_TXT_mdfemodalFerroviario    //,
  //CO_TXT_mdfemodalDutoviario     ,
  //CO_TXT_mdfemodalMultimodal
  );

type
  TUnMDFeListas = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    //function  ArrayEnquadramentoLegalIPI(): MyArrayLista;

  end;

var
  MDFeListas: TUnMDFeListas;

implementation

end.
