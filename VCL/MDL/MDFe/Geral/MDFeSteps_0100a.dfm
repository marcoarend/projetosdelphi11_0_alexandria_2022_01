object FmMDFeSteps_0100a: TFmMDFeSteps_0100a
  Left = 339
  Top = 185
  Caption = 'MDF-STEPS-001 :: Passos do MDF-e 1.00a'
  ClientHeight = 691
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 293
        Height = 32
        Caption = 'Passos do MDF-e 1.00a'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 293
        Height = 32
        Caption = 'Passos do MDF-e 1.00a'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 293
        Height = 32
        Caption = 'Passos do MDF-e 1.00a'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 529
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 529
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 529
        Align = alClient
        TabOrder = 0
        object LaExpiraCertDigital: TLabel
          Left = 2
          Top = 167
          Width = 780
          Height = 25
          Align = alTop
          Alignment = taCenter
          Caption = 'Expira'#231#227'o do Certificado Digital'
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -21
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
          ExplicitWidth = 331
        end
        object PnConfig1: TPanel
          Left = 2
          Top = 434
          Width = 780
          Height = 64
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 0
          object Panel6: TPanel
            Left = 113
            Top = 0
            Width = 667
            Height = 64
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label5: TLabel
              Left = 4
              Top = 12
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object Label2: TLabel
              Left = 124
              Top = 13
              Width = 53
              Height = 13
              Caption = 'UF (WS):'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label17: TLabel
              Left = 4
              Top = 40
              Width = 127
              Height = 13
              Caption = 'Serial do Certificado digital:'
            end
            object Label18: TLabel
              Left = 256
              Top = 12
              Width = 74
              Height = 13
              Caption = 'Servi'#231'o [F4]:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object EdSerialNumber: TdmkEdit
              Left = 136
              Top = 36
              Width = 260
              Height = 21
              Enabled = False
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdUF_Servico: TdmkEdit
              Left = 332
              Top = 8
              Width = 64
              Height = 21
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object CBUF: TComboBox
              Left = 180
              Top = 8
              Width = 69
              Height = 21
              CharCase = ecUpperCase
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              Items.Strings = (
                'AC'
                'AL'
                'AM'
                'AP'
                'BA'
                'CE'
                'DF'
                'ES'
                'GO'
                'MA'
                'MG'
                'MS'
                'MT'
                'PA'
                'PB'
                'PE'
                'PI'
                'PR'
                'RJ'
                'RN'
                'RO'
                'RR'
                'RS'
                'SC'
                'SE'
                'SP'
                'TO')
            end
            object EdEmpresa: TdmkEdit
              Left = 52
              Top = 8
              Width = 61
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdEmpresaChange
            end
            object REWarning: TRichEdit
              Left = 400
              Top = 0
              Width = 329
              Height = 64
              Font.Charset = ANSI_CHARSET
              Font.Color = 4227327
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              ReadOnly = True
              TabOrder = 4
            end
          end
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 113
            Height = 64
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object RGAmbiente: TRadioGroup
              Left = 0
              Top = 0
              Width = 113
              Height = 64
              Align = alClient
              Caption = ' Ambiente: '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ItemIndex = 0
              Items.Strings = (
                'Nenhum'
                'Produ'#231#227'o'
                'Homologa'#231#227'o')
              ParentFont = False
              TabOrder = 0
            end
          end
        end
        object Panel11: TPanel
          Left = 2
          Top = 498
          Width = 780
          Height = 29
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object Label19: TLabel
            Left = 8
            Top = 8
            Width = 68
            Height = 13
            Caption = 'Web Service: '
          end
          object Label20: TLabel
            Left = 656
            Top = 8
            Width = 36
            Height = 13
            Caption = 'Vers'#227'o:'
          end
          object EdWebService: TEdit
            Left = 76
            Top = 4
            Width = 573
            Height = 21
            ReadOnly = True
            TabOrder = 0
          end
          object EdVersaoAcao: TdmkEdit
            Left = 696
            Top = 4
            Width = 69
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '2,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 2.000000000000000000
            ValWarn = False
          end
        end
        object PageControl1: TPageControl
          Left = 2
          Top = 306
          Width = 664
          Height = 128
          ActivePage = TabSheet5
          Align = alClient
          TabOrder = 2
          object TabSheet1: TTabSheet
            Caption = ' XML de envio '
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 772
            ExplicitHeight = 0
            object RETxtEnvio: TMemo
              Left = 0
              Top = 0
              Width = 656
              Height = 100
              Align = alClient
              TabOrder = 0
              WordWrap = False
              OnChange = RETxtEnvioChange
              ExplicitWidth = 772
            end
          end
          object TabSheet5: TTabSheet
            Caption = 'XML de envio (Formatado)'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object WBEnvio: TWebBrowser
              Left = 0
              Top = 0
              Width = 656
              Height = 100
              Align = alClient
              TabOrder = 0
              ExplicitWidth = 838
              ControlData = {
                4C000000CD430000560A00000000000000000000000000000000000000000000
                000000004C000000000000000000000001000000E0D057007335CF11AE690800
                2B2E126208000000000000004C0000000114020000000000C000000000000046
                8000000000000000000000000000000000000000000000000000000000000000
                00000000000000000100000000000000000000000000000000000000}
            end
          end
          object TabSheet2: TTabSheet
            Caption = ' XML Retornado (Texto) '
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object RETxtRetorno: TMemo
              Left = 0
              Top = 0
              Width = 838
              Height = 100
              Align = alClient
              TabOrder = 0
              WantReturns = False
              OnChange = RETxtRetornoChange
            end
          end
          object TabSheet3: TTabSheet
            Caption = ' XML Retornado (Formatado) '
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object WBResposta: TWebBrowser
              Left = 0
              Top = 0
              Width = 776
              Height = 67
              Align = alClient
              TabOrder = 0
              ExplicitHeight = 21
              ControlData = {
                4C00000034500000ED0600000000000000000000000000000000000000000000
                000000004C000000000000000000000001000000E0D057007335CF11AE690800
                2B2E126208000000000000004C0000000114020000000000C000000000000046
                8000000000000000000000000000000000000000000000000000000000000000
                00000000000000000100000000000000000000000000000000000000}
            end
          end
          object TabSheet4: TTabSheet
            Caption = ' Informa'#231#245'es do XML de retorno '
            ImageIndex = 4
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object MeInfo: TMemo
              Left = 0
              Top = 0
              Width = 776
              Height = 67
              Align = alClient
              ScrollBars = ssVertical
              TabOrder = 0
              WordWrap = False
            end
          end
          object TabSheet6: TTabSheet
            Caption = 'Chaves'
            ImageIndex = 5
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object MeChaves: TMemo
              Left = 0
              Top = 0
              Width = 838
              Height = 100
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 0
            end
          end
        end
        object Panel1: TPanel
          Left = 2
          Top = 121
          Width = 780
          Height = 46
          Align = alTop
          TabOrder = 3
          object Label14: TLabel
            Left = 8
            Top = 4
            Width = 73
            Height = 13
            Caption = 'CNPJ empresa:'
          end
          object EdEmitCNPJ: TdmkEdit
            Left = 8
            Top = 20
            Width = 112
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 1
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '899'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object RGIndSinc: TdmkRadioGroup
            Left = 591
            Top = 1
            Width = 188
            Height = 44
            Align = alRight
            Caption = ' Envio do CT-e ao Fisco:'
            Columns = 2
            Enabled = False
            ItemIndex = 0
            Items.Strings = (
              'Ass'#237'ncrono'
              'S'#237'ncrono')
            TabOrder = 1
            QryCampo = 'IndSinc'
            UpdCampo = 'IndSinc'
            UpdType = utYes
            OldValor = 0
          end
        end
        object RGAcao: TRadioGroup
          Left = 2
          Top = 15
          Width = 780
          Height = 106
          Align = alTop
          Caption = ' A'#231#227'o a realizar: '
          Columns = 3
          Enabled = False
          ItemIndex = 0
          Items.Strings = (
            'Status do servi'#231'o'
            'Envio de lote de MDF-e ao fisco'
            'Consultar lote enviado'
            'Pedir cancelamento de MDF-e'
            'Pedir inutiliza'#231#227'o de n'#250'mero(s) de MDF-e'
            'Consultar MDF-e'
            'Enviar lote de eventos da MDF-e'
            'Consulta Cadastro Entidade'
            'Pedir encerramento de MDF-e'
            'Consulta MDF-es N'#227'o Encerradas'
            'Download de MDF-e(s)'
            'Consulta Distribui'#231#227'o de MDF-e de Interesse')
          TabOrder = 4
          OnClick = RGAcaoClick
        end
        object PCAcao: TdmkPageControl
          Left = 2
          Top = 192
          Width = 780
          Height = 114
          ActivePage = TabSheet12
          Align = alTop
          TabOrder = 5
          EhAncora = False
          object TabSheet7: TTabSheet
            Caption = 'Status do servi'#231'o'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
          end
          object TabSheet8: TTabSheet
            Caption = 'Envio de lote de MDF-e ao fisco'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 656
              Height = 86
              Align = alClient
              BevelOuter = bvNone
              Enabled = False
              ParentBackground = False
              TabOrder = 0
              object Label4: TLabel
                Left = 4
                Top = 4
                Width = 24
                Height = 13
                Caption = 'Lote:'
              end
              object Label6: TLabel
                Left = 84
                Top = 4
                Width = 37
                Height = 13
                Caption = 'Recibo:'
              end
              object EdLote: TdmkEdit
                Left = 4
                Top = 20
                Width = 77
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 9
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '000000000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdRecibo: TdmkEdit
                Left = 84
                Top = 20
                Width = 204
                Height = 21
                ReadOnly = True
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 9
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
          object TabSheet11: TTabSheet
            Caption = 'Retorno Lote'
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
          end
          object TabSheet9: TTabSheet
            Caption = 'Cancelamento de MDF-e'
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 656
            ExplicitHeight = 0
            object Panel8: TPanel
              Left = 0
              Top = 41
              Width = 772
              Height = 45
              Align = alTop
              BevelOuter = bvNone
              Enabled = False
              ParentBackground = False
              TabOrder = 0
              ExplicitWidth = 656
              object Label3: TLabel
                Left = 4
                Top = 4
                Width = 169
                Height = 13
                Caption = 'Justificativa (m'#237'nimo 15 caracteres):'
              end
              object SpeedButton1: TSpeedButton
                Left = 629
                Top = 20
                Width = 21
                Height = 21
                Caption = '...'
                Enabled = False
              end
              object EdMDFeJustCan: TdmkEditCB
                Left = 4
                Top = 20
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBMDFeJustCan
                IgnoraDBLookupComboBox = False
              end
              object CBMDFeJustCan: TdmkDBLookupComboBox
                Left = 60
                Top = 20
                Width = 569
                Height = 21
                KeyField = 'CodUsu'
                ListField = 'Nome'
                ListSource = DsMDFeJustCan
                TabOrder = 1
                dmkEditCB = EdMDFeJustCan
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
            object Panel10: TPanel
              Left = 0
              Top = 0
              Width = 772
              Height = 41
              Align = alTop
              BevelOuter = bvNone
              Enabled = False
              ParentBackground = False
              TabOrder = 1
              ExplicitWidth = 656
              object Panel12: TPanel
                Left = 0
                Top = 0
                Width = 772
                Height = 41
                Align = alClient
                BevelOuter = bvNone
                Enabled = False
                TabOrder = 0
                ExplicitWidth = 656
                object Label22: TLabel
                  Left = 4
                  Top = 4
                  Width = 38
                  Height = 13
                  Caption = 'Modelo:'
                end
                object Label23: TLabel
                  Left = 48
                  Top = 4
                  Width = 27
                  Height = 13
                  Caption = 'S'#233'rie:'
                end
                object Label8: TLabel
                  Left = 84
                  Top = 4
                  Width = 75
                  Height = 13
                  Caption = 'N'#250'mero MDF-e:'
                end
                object Label1: TLabel
                  Left = 156
                  Top = 4
                  Width = 48
                  Height = 13
                  Caption = 'Protocolo:'
                end
                object Label9: TLabel
                  Left = 260
                  Top = 4
                  Width = 69
                  Height = 13
                  Caption = 'Chave MDF-e:'
                end
                object Label16: TLabel
                  Left = 540
                  Top = 4
                  Width = 22
                  Height = 13
                  Caption = 'Seq:'
                end
                object EdModeloCan: TdmkEdit
                  Left = 4
                  Top = 20
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 2
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '58'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 58
                  ValWarn = False
                end
                object EdSerieCan: TdmkEdit
                  Left = 48
                  Top = 20
                  Width = 33
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 1
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ValMax = '899'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object EdnMDFCan: TdmkEdit
                  Left = 84
                  Top = 20
                  Width = 68
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 1
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ValMax = '899'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object EdnProtCan: TdmkEdit
                  Left = 156
                  Top = 20
                  Width = 102
                  Height = 21
                  ReadOnly = True
                  TabOrder = 3
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 9
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdchMDFeCan: TdmkEdit
                  Left = 260
                  Top = 20
                  Width = 276
                  Height = 21
                  ReadOnly = True
                  TabOrder = 4
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 9
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdnSeqEventoCan: TdmkEdit
                  Left = 540
                  Top = 20
                  Width = 29
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 5
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 1
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ValMax = '899'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
              end
            end
          end
          object TabSheet10: TTabSheet
            Caption = 'Inutiliza'#231#227'o de N'#250'mero'
            ImageIndex = 4
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object PnInutiliza: TPanel
              Left = 0
              Top = 0
              Width = 772
              Height = 41
              Align = alTop
              BevelOuter = bvNone
              Enabled = False
              ParentBackground = False
              TabOrder = 0
              ExplicitWidth = 656
              object Label15: TLabel
                Left = 376
                Top = 0
                Width = 22
                Height = 13
                Caption = 'Ano:'
              end
              object GroupBox2: TGroupBox
                Left = 88
                Top = 0
                Width = 280
                Height = 41
                Align = alLeft
                Caption = ' Intervalo de numera'#231#227'o a ser inutilizado:  '
                TabOrder = 0
                object Label10: TLabel
                  Left = 8
                  Top = 20
                  Width = 70
                  Height = 13
                  Caption = 'N'#186' MDF inicial:'
                end
                object Label11: TLabel
                  Left = 148
                  Top = 20
                  Width = 63
                  Height = 13
                  Caption = 'N'#186' MDF final:'
                end
                object EdnCTIni: TdmkEdit
                  Left = 80
                  Top = 16
                  Width = 65
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ValMax = '999999999'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object EdnCTFim: TdmkEdit
                  Left = 216
                  Top = 16
                  Width = 65
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ValMax = '999999999'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
              end
              object Panel9: TPanel
                Left = 0
                Top = 0
                Width = 88
                Height = 41
                Align = alLeft
                BevelOuter = bvNone
                TabOrder = 1
                object Label12: TLabel
                  Left = 4
                  Top = 4
                  Width = 38
                  Height = 13
                  Caption = 'Modelo:'
                end
                object Label13: TLabel
                  Left = 48
                  Top = 4
                  Width = 27
                  Height = 13
                  Caption = 'S'#233'rie:'
                end
                object EdModeloInu: TdmkEdit
                  Left = 4
                  Top = 20
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 2
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '57'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 57
                  ValWarn = False
                end
                object EdSerieInu: TdmkEdit
                  Left = 48
                  Top = 20
                  Width = 33
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 1
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ValMax = '899'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
              end
              object EdAno: TdmkEdit
                Left = 376
                Top = 16
                Width = 25
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 2
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ValMax = '899'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
            object PnJustificativa: TPanel
              Left = 0
              Top = 41
              Width = 772
              Height = 45
              Align = alTop
              BevelOuter = bvNone
              Enabled = False
              ParentBackground = False
              TabOrder = 1
              ExplicitWidth = 656
              object Label7: TLabel
                Left = 4
                Top = 4
                Width = 169
                Height = 13
                Caption = 'Justificativa (m'#237'nimo 15 caracteres):'
              end
              object SpeedButton2: TSpeedButton
                Left = 629
                Top = 20
                Width = 21
                Height = 21
                Caption = '...'
                Enabled = False
              end
              object EdMDFeJustInu: TdmkEditCB
                Left = 4
                Top = 20
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBMDFeJustInu
                IgnoraDBLookupComboBox = False
              end
              object CBMDFeJustInu: TdmkDBLookupComboBox
                Left = 60
                Top = 20
                Width = 569
                Height = 21
                KeyField = 'CodUsu'
                ListField = 'Nome'
                ListSource = DsMDFeJustInu
                TabOrder = 1
                dmkEditCB = EdMDFeJustInu
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
          end
          object TabSheet12: TTabSheet
            Caption = 'Consulta MDF-e'
            ImageIndex = 5
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel15: TPanel
              Left = 0
              Top = 0
              Width = 772
              Height = 86
              Align = alClient
              BevelOuter = bvNone
              Enabled = False
              ParentBackground = False
              TabOrder = 0
              object Label33: TLabel
                Left = 4
                Top = 4
                Width = 69
                Height = 13
                Caption = 'Chave MDF-e:'
              end
              object Label30: TLabel
                Left = 284
                Top = 4
                Width = 56
                Height = 13
                Caption = 'Controle ID:'
              end
              object EdChaveMDFeMDFe: TdmkEdit
                Left = 4
                Top = 20
                Width = 276
                Height = 21
                ReadOnly = True
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 9
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdIDCtrlMDFe: TdmkEdit
                Left = 284
                Top = 20
                Width = 68
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 1
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ValMax = '899'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
          end
          object TabSheet13: TTabSheet
            Caption = 'TabSheet06'
            ImageIndex = 6
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 656
            ExplicitHeight = 0
            object Panel14: TPanel
              Left = 0
              Top = 0
              Width = 772
              Height = 86
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              ExplicitWidth = 656
              object Label25: TLabel
                Left = 4
                Top = 4
                Width = 48
                Height = 13
                Caption = 'Protocolo:'
              end
              object Label27: TLabel
                Left = 108
                Top = 4
                Width = 69
                Height = 13
                Caption = 'Chave MDF-e:'
              end
              object Label28: TLabel
                Left = 4
                Top = 44
                Width = 91
                Height = 13
                Caption = 'Nome do motorista:'
              end
              object Label29: TLabel
                Left = 272
                Top = 44
                Width = 23
                Height = 13
                Caption = 'CPF:'
              end
              object EdnProtIdC: TdmkEdit
                Left = 4
                Top = 20
                Width = 102
                Height = 21
                ReadOnly = True
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 9
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdchMDFeIdC: TdmkEdit
                Left = 108
                Top = 20
                Width = 276
                Height = 21
                ReadOnly = True
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 9
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdxNomeIdC: TdmkEdit
                Left = 4
                Top = 60
                Width = 265
                Height = 21
                ReadOnly = True
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 9
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdCPFIdc: TdmkEdit
                Left = 272
                Top = 60
                Width = 112
                Height = 21
                ReadOnly = True
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 9
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
          object TabSheet14: TTabSheet
            Caption = 'TabSheet07'
            ImageIndex = 7
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
          end
          object TabSheet15: TTabSheet
            Caption = 'Encerramento MDF-e'
            ImageIndex = 8
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel13: TPanel
              Left = 0
              Top = 0
              Width = 772
              Height = 86
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label21: TLabel
                Left = 4
                Top = 4
                Width = 48
                Height = 13
                Caption = 'Protocolo:'
              end
              object Label24: TLabel
                Left = 108
                Top = 4
                Width = 69
                Height = 13
                Caption = 'Chave MDF-e:'
              end
              object Label26: TLabel
                Left = 5
                Top = 44
                Width = 17
                Height = 13
                Caption = 'UF:'
              end
              object Label105: TLabel
                Left = 36
                Top = 44
                Width = 36
                Height = 13
                Caption = 'Cidade:'
              end
              object Label36: TLabel
                Left = 388
                Top = 4
                Width = 109
                Height = 13
                Caption = 'Data do encerramento:'
              end
              object EdnProtEnc: TdmkEdit
                Left = 4
                Top = 20
                Width = 102
                Height = 21
                ReadOnly = True
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 9
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdchMDFeEnc: TdmkEdit
                Left = 108
                Top = 20
                Width = 276
                Height = 21
                ReadOnly = True
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 9
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdcUFEnc: TdmkEdit
                Left = 5
                Top = 60
                Width = 28
                Height = 21
                Alignment = taRightJustify
                CharCase = ecUpperCase
                ReadOnly = True
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdcMunEnc: TdmkEditCB
                Left = 36
                Top = 60
                Width = 56
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                IgnoraDBLookupComboBox = True
              end
              object TPdtEnc: TdmkEditDateTimePicker
                Left = 387
                Top = 20
                Width = 116
                Height = 21
                Date = 40137.793551006940000000
                Time = 40137.793551006940000000
                TabOrder = 4
                ReadOnly = True
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                UpdType = utYes
              end
              object EdxMunEnc: TdmkEdit
                Left = 96
                Top = 60
                Width = 405
                Height = 21
                ReadOnly = True
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
        end
        object PnAbrirXML: TPanel
          Left = 666
          Top = 306
          Width = 116
          Height = 128
          Align = alRight
          BevelOuter = bvNone
          Enabled = False
          TabOrder = 6
          Visible = False
          object BtAbrir: TButton
            Left = 8
            Top = 4
            Width = 100
            Height = 25
            Caption = 'Abrir arquivo XML'
            Enabled = False
            TabOrder = 0
            OnClick = BtAbrirClick
          end
          object Button1: TButton
            Left = 8
            Top = 32
            Width = 100
            Height = 25
            Caption = 'Aviso'
            TabOrder = 1
            OnClick = Button1Click
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 577
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 621
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object PnConfirma: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      Visible = False
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object CkSoLer: TCheckBox
        Left = 148
        Top = 16
        Width = 189
        Height = 17
        Caption = 'Somente ler arquivo j'#225' gravado.'
        TabOrder = 1
      end
    end
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 16
    Top = 8
  end
  object QrMDFeCabA1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM mdfecaba'
      'WHERE ID=:P0'
      'AND LoteEnv=:P1'
      '')
    Left = 216
    Top = 80
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrMDFeCabA1FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeCabA1FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeCabA1Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeCabA1IDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
  end
  object QrMDFeJustInu: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mdfejust'
      'WHERE :P0 & Aplicacao > 0'
      'ORDER BY Nome')
    Left = 492
    Top = 65528
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMDFeJustInuCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMDFeJustInuNome: TWideStringField
      FieldName = 'Nome'
      Size = 240
    end
    object QrMDFeJustInuCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrMDFeJustInuAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
  end
  object DsMDFeJustInu: TDataSource
    DataSet = QrMDFeJustInu
    Left = 492
    Top = 36
  end
  object QrMDFeJustCan: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM mdfejust'
      'WHERE :P0 & Aplicacao > 0'
      'ORDER BY Nome')
    Left = 588
    Top = 65528
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMDFeJustCanCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMDFeJustCanNome: TWideStringField
      FieldName = 'Nome'
      Size = 240
    end
    object QrMDFeJustCanCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrMDFeJustCanAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
  end
  object DsMDFeJustCan: TDataSource
    DataSet = QrMDFeJustCan
    Left = 588
    Top = 36
  end
  object QrCabA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IDCtrl, infProt_ID, infProt_nProt'
      'FROM mdfecaba '
      'WHERE Empresa=:P0'
      'AND id=:P1')
    Left = 400
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrCabAinfProt_ID: TWideStringField
      FieldName = 'infProt_ID'
      Size = 30
    end
    object QrCabAinfProt_nProt: TWideStringField
      FieldName = 'infProt_nProt'
      Size = 15
    end
  end
  object QrMDFeCabA2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctecaba'
      'WHERE ID=:P0'
      'AND IDCtrl=:P1'
      '')
    Left = 328
    Top = 412
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrMDFeCabA2FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeCabA2FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeCabA2Empresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeCabA2Id: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
  end
end
