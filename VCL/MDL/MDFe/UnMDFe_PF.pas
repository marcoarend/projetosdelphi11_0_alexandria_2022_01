unit UnMDFe_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts2, ComCtrls, Registry, Printers,
  CommCtrl, Consts, Variants, UnInternalConsts, ZCF2, StrUtils, dmkGeral,
  UnDmkEnums, UnMsgInt, mySQLDbTables, DB,(* DbTables,*) DmkCoding, dmkEdit,
  dmkRadioGroup, dmkMemo, AdvToolBar, dmkCheckGroup,
  CAPICOM_TLB,  MSXML2_TLB,
  InvokeRegistry, Rio, SOAPHTTPClient, SOAPHTTPTrans, JwaWinCrypt,
  WinInet, XMLDoc, xmldom, XMLIntf, msxmldom, ComObj,
  UnMyObjects, UnXXe_PF,
  Buttons, UnMLAGeral, Math, UnDmkProcFunc,
  IBCustomDataSet, IBQuery, UrlMon;

type
  TUnMDFe_PF = class(TObject)
  private
    { Private declarations }
    //
  public
    { Public declarations }
    //
    function  AssinarMSXML(XML: String; Certificado:
              ICertificate2; out XMLAssinado: String): Boolean;
    procedure AvisoNaoImplemVerMDFe(Versao: Integer);
    procedure EnviaLoteAoFisco(Sincronia: TXXeIndSinc; Lote, Empresa: Integer;
              SohLer: Boolean; Qry: TmySQLQuery);
    function  Evento_MontaId(tpEvento: Integer; Chave: String; nSeqEvento:
              Integer): String;
    function  Evento_Obtem_DadosEspecificos(const EvePreDef: Integer;
              const EventoMDFe: TEventoXXe; var tpEvento: Integer; var versao: Double;
              var descEvento: String): Boolean;
    function  InsUpdFaturasMDFe(Empresa, FatID, FatNum: Integer;
              Data: TDateTime; SeriMDFe, NumeroMDFe: Integer): Boolean;
    function  MontaChaveDeAcesso(cUF: Integer; Emissao: TDateTime; CNPJ: String;
              Modelo, Serie, NumeroMDF: Integer; var ide_cMDF: Integer;
              var cDV: String; var ChaveDeAcesso: String; const cMDF_Atual,
              ide_tpEmis: Integer; versao: Double): Boolean;
    procedure MostraFormMDFeJust();
    procedure MostraFormMDFeLayout_0100();
    procedure MostraFormMDFeLEnc(Codigo: Integer);
    procedure MostraFormMDFeMDFaEdit_0100a(Empresa, FrtMnfCab: Integer; QrFrtMnfCab:
              TmySQLQuery; SerieMDFe, NumeroMDFe: Integer);
    procedure MostraFormMDFePesq(AbrirEmAba: Boolean; InOwner: TWincontrol;
              AdvToolBarPager: TAdvToolBarPager);
    procedure MostraFormStepsMDFe_Cancelamento(Empresa, Serie, nMDF, nSeqEvento,
              Justif: Integer; Modelo, nProtCan, chMDFe, XML_Evento: String;
              Qry: TmySQLQuery);
    procedure MostraFormStepsMDFe_ConsultaMDFe(Empresa, IDCtrl: Integer;
              ChaveMDFe: String; MeXML: TMemo);
    procedure MostraFormStepsMDFe_Encerramento(Empresa: Integer; nProt, chMDFe,
              xMun: String; dtEnc: TDateTime; cUF, cMun: Integer; XML_Evento:
              String; Qry: TmySQLQuery);
    procedure MostraFormStepsMDFe_IncCondutor(Empresa: Integer; nProt, chMDFe,
              xNome, CPF, XML_Evento: String; Qry: TmySQLQuery);
    procedure MostraFormStepsMDFe_NaoEncerrados();
    procedure MostraFormStepsMDFe_StatusServico();
    function  Obtem_Serie_e_NumMDF(const SerieDesfeita, MDFeDesfeita,
              ParamsMDFe, FatID, FatNum, Empresa: Integer; const Tabela: String;
              var SerieMDFe, NumeroMDFe: Integer): Boolean;
    function  ObtemURLWebServerMDFe(UF, Servico, Versao: String; tpAmb:
              Integer): String;
    function  Texto_StatusMDFe(Status: Integer; VersaoMDFe: Double): String;
    procedure TotaisMDFe(Codigo: Integer; QrFrtFatCab: TmySQLQuery);
    function  TxtNo_SitEnc(): String;
    function  VersaoMDFeEmUso(): Integer;
    function  VDom(const Grupo, Codigo: Integer; const Campo, Valor, Dominio:
              String; var AllMsg: String): Boolean;
    function  VRegExp(const Grupo, Codigo: Integer; const Campo, Valor,
              ExpressaoRegular: String; var AllMsg: String): Boolean;
  end;

var
  MDFe_PF: TUnMDFe_PF;
  CO_MDFE_CERTIFICADO_DIGITAL_SERIAL_NUMBER: String;


const
  // assinatura
  INTERNET_OPTION_CLIENT_CERT_CONTEXT = 84;
  DSIGNS = 'xmlns:ds="http://www.w3.org/2000/09/xmldsig#"';
 { TODO : Colocar aqui os stat da CCe }
  MDFe_AllModelos      = '58';
{
  MDFe_CodAutorizaTxt  = '100';
  MDFe_CodCanceladTxt  = '101';
  MDFe_CodInutilizTxt  = '102';
  MDFe_CodDenegadoTxt  = '110,301,302';
}
  // DesmontarChaveMDFe
  DEMONTA_CHV_INI_SerMDFe = 23;
  DEMONTA_CHV_TAM_SerMDFe = 03;
  DEMONTA_CHV_INI_NumMDFe = 26;
  DEMONTA_CHV_TAM_NumMDFe = 09;

(*
  //MDFe_CodEventoCCe = 110110;
  MDFe_CodEventoCan = 110111;
  MDFe_CodEventoEnc = 110112;
  MDFe_CodEventoIdC = 110114;
*)
 // MDFe_CodEventoMMo = 110160;
  // Apenas compatibilidade por enquanto!!!!
  // Na verdade estes codigos sao da N F e
  MDFe_CodsEveWbserverUserDef = '210200, 210210, 210220, 210240';



implementation

uses MyDBCheck, ModuleMDFe_0000, MDFeSteps_0100a, ModuleGeral, DmkDAC_PF,
  MDFeLayout_0100, Module, UMySQLModule, RegExpr, MDFeMDFaEdit_0100a,
  MDFeLEnc_0100a, MDFe_Pesq_0000, MDFeJust;

var
  //  ACBr
  CertStore     : IStore3;
  CertStoreMem  : IStore3;
  PrivateKey    : IPrivateKey;
  Certs         : ICertificates2;
  Cert          : ICertificate2;
  NumCertCarregado : String;

{ TUnMDFe_PF }

function TUnMDFe_PF.AssinarMSXML(XML: String; Certificado: ICertificate2;
  out XMLAssinado: String): Boolean;
var
 I, J, PosIni, PosFim : Integer;
 URI           : String ;
 Tipo : Integer;

 xmlHeaderAntes, xmlHeaderDepois: String;
 xmldoc  : IXMLDOMDocument3;
 xmldsig : IXMLDigitalSignature;
 dsigKey   : IXMLDSigKey;
 signedKey : IXMLDSigKey;
begin
  Result := False;
  try
  if Pos('<Signature', XML) <= 0 then
  begin
    //I := pos('<infMdfe',XML) ;
    I := pos('<infMDFe',XML) ;
    Tipo := 1;
  end;
  if I = 0  then
  begin
    I := Pos('<infEvento', XML);
    if I > 0 then
      Tipo := 2
    else
    begin
      I := pos('<infInut',XML) ;
      if I > 0 then
        Tipo := 3
      else
      begin
        Geral.MB_Erro('Tag n�o definida em "TUnMDFe_PF.AssinarMSXML()" [1]' +
        slineBreak + XML);
        Exit;
      end;
(*
      I := pos('<infCanc',XML) ;
      if I > 0 then
        Tipo := 2
      else
      begin
        I := pos('<infInut',XML) ;
        if I > 0 then
          Tipo := 3
        else
        begin
          I := Pos('<infEvento', XML);
          if I > 0 then
          begin
            I := Pos('<Evento', XML);
            if I > 0 then
              Tipo := 6
            else
              Tipo := 5 // '<evento'
          end else
            Tipo := 4;
        end;
     end;
*)
    end;
  end;

      I := PosEx('Id=',XML,6) ;
      if I = 0 then
         raise Exception.Create('N�o encontrado in�cio do URI: Id=') ;
      I := PosEx('"',XML,I+2) ;
      if I = 0 then
         raise Exception.Create('N�o encontrado in�cio do URI: aspas inicial') ;
      J := PosEx('"',XML,I+1) ;
      if J = 0 then
         raise Exception.Create('N�o encontrado in�cio do URI: aspas final') ;

      URI := copy(XML,I+1,J-I-1) ;

      if Tipo = 1 then
         XML := copy(XML,1,pos('</MDFe>',XML)-1)
      else if Tipo = 2 then
         XML := copy(XML,1,pos('</eventoMDFe>',XML)-1)
      else if Tipo = 3 then
         XML := copy(XML,1,pos('</inutMDFe>',XML)-1)
{
      else if Tipo = 2 then
         XML := copy(XML,1,pos('</cancMDFe>',XML)-1)
      else if Tipo = 4 then
         XML := copy(XML,1,pos('</envDPEC>',XML)-1)
      else if Tipo = 5 then
         XML := copy(XML,1,pos('</evento>',XML)-1);
(*
      else if Tipo = 6 then
         XML := copy(XML,1,pos('</Evento>',XML)-1)
*)
      }
      else
      begin
        Geral.MB_Erro('Tag n�o definida em "TUnMDFe_PF.AssinarMSXML()" [2]');
        Exit;
      end;

      XML := XML + '<Signature xmlns="http://www.w3.org/2000/09/xmldsig#"><SignedInfo><CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"/><SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1" />';
      XML := XML + '<Reference URI="#'+URI+'">';
      XML := XML + '<Transforms><Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature" /><Transform Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" /></Transforms><DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1" />';
      XML := XML + '<DigestValue></DigestValue></Reference></SignedInfo><SignatureValue></SignatureValue><KeyInfo></KeyInfo></Signature>';

      if Tipo = 1 then
         XML := XML + '</MDFe>'
      else if Tipo = 2 then
         XML := XML + '</eventoMDFe>'
      else if Tipo = 3 then
         XML := XML + '</inutMDFe>'
      {
      else if Tipo = 2 then
         XML := XML + '</cancMDFe>'
      else if Tipo = 4 then
         XML := XML + '</envDPEC>'
      else if Tipo = 5 then
         XML := XML + '</evento>';
      (*
      else if Tipo = 6 then
         XML := XML + '</Evento>';
      *)
      }
      else
      begin
        Geral.MB_Erro('Tag n�o definida em "TUnMDFe_PF.AssinarMSXML()" [3]');
        Exit;
      end;
   //end;

   // Lendo Header antes de assinar //
   xmlHeaderAntes := '' ;
   I := pos('?>',XML) ;
   if I > 0 then
      xmlHeaderAntes := copy(XML,1,I+1) ;

   xmldoc := CoDOMDocument50.Create;

   xmldoc.async              := False;
   xmldoc.validateOnParse    := False;
   xmldoc.preserveWhiteSpace := True;

   xmldsig := CoMXDigitalSignature50.Create;

   if (not xmldoc.loadXML(XML) ) then
   begin
     Geral.MensagemBox('N�o foi poss�vel carregar o arquivo: ' + #13#10 + XML, 'ERRO', MB_OK+MB_ICONERROR);
     raise Exception.Create('N�o foi poss�vel carregar o arquivo: '+XML);
   end;
   xmldoc.setProperty('SelectionNamespaces', DSIGNS);

   xmldsig.signature := xmldoc.selectSingleNode('.//ds:Signature');
   //xmldsig.signature := xmldoc.selectSingleNode('Signature');

   if (xmldsig.signature = nil) then
      raise Exception.Create('� preciso carregar o template antes de assinar.');

   if NumCertCarregado <> Certificado.SerialNumber then
      CertStoreMem := nil;

   if  CertStoreMem = nil then
    begin
      CertStore := CoStore.Create;
      CertStore.Open(CAPICOM_CURRENT_USER_STORE, 'My', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

      CertStoreMem := CoStore.Create;
      CertStoreMem.Open(CAPICOM_MEMORY_STORE, 'Memoria', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

      Certs := CertStore.Certificates as ICertificates2;
      for i:= 1 to Certs.Count do
      begin
        Cert := IInterface(Certs.Item[i]) as ICertificate2;
        if Cert.SerialNumber = Certificado.SerialNumber then
         begin
           CertStoreMem.Add(Cert);
           NumCertCarregado := Certificado.SerialNumber;
         end;
      end;
   end;

   OleCheck(IDispatch(Certificado.PrivateKey).QueryInterface(IPrivateKey,PrivateKey));
   xmldsig.store := CertStoreMem;

   dsigKey := xmldsig.createKeyFromCSP(PrivateKey.ProviderType, PrivateKey.ProviderName, PrivateKey.ContainerName, 0);
   if (dsigKey = nil) then
      raise Exception.Create('Erro ao criar a chave do CSP. (assinatura digital)');

   signedKey := xmldsig.sign(dsigKey, $00000002);
   if (signedKey <> nil) then
    begin
      XMLAssinado := xmldoc.xml;
      XMLAssinado := StringReplace( XMLAssinado, #10, '', [rfReplaceAll] ) ;
      XMLAssinado := StringReplace( XMLAssinado, #13, '', [rfReplaceAll] ) ;
      PosIni := Pos('<SignatureValue>',XMLAssinado)+length('<SignatureValue>');
      XMLAssinado := copy(XMLAssinado,1,PosIni-1)+StringReplace( copy(XMLAssinado,PosIni,length(XMLAssinado)), ' ', '', [rfReplaceAll] ) ;
      PosIni := Pos('<X509Certificate>',XMLAssinado)-1;
      PosFim := dmkPF.PosLast('<X509Certificate>', XMLAssinado);

      XMLAssinado := copy(XMLAssinado,1,PosIni)+copy(XMLAssinado,PosFim,length(XMLAssinado));
    end
   else
     raise Exception.Create('Ocorreu uma falha ao tentar assinar o XML!');
      //raise Exception.Create('Assinatura Falhou.');
   if xmlHeaderAntes <> '' then
   begin
      I := pos('?>',XMLAssinado) ;
      if I > 0 then
       begin
         xmlHeaderDepois := copy(XMLAssinado,1,I+1) ;
         if xmlHeaderAntes <> xmlHeaderDepois then
         begin
           XMLAssinado := StuffString(XMLAssinado,1,length(xmlHeaderDepois),xmlHeaderAntes) ;
         end;
       end
      else
         XMLAssinado := xmlHeaderAntes + XMLAssinado ;
   end ;

   dsigKey   := nil;
   signedKey := nil;
   xmldoc    := nil;
   xmldsig   := nil;
   //
   Result := True;
   except
     Result := False;
   end;
end;

procedure TUnMDFe_PF.AvisoNaoImplemVerMDFe(Versao: Integer);
begin
  Geral.MB_Aviso('Versao de MDFe n�o implementada: ' +
  Geral.FFT_Dot(Versao / 100, 2, siPositivo));
end;

procedure TUnMDFe_PF.EnviaLoteAoFisco(Sincronia: TXXeIndSinc; Lote,
  Empresa: Integer; SohLer: Boolean; Qry: TmySQLQuery);
var
  VersaoAcao: String;
begin
  case Sincronia of
    nisAssincrono: VersaoAcao  := Geral.FFT_Dot(DmMDFe_0000.QrOpcoesMDFeMDFeVerEnvLot.Value, 2, siPositivo);
    nisSincrono: VersaoAcao  := Geral.FFT_Dot(DmMDFe_0000.QrOpcoesMDFeMDFeVerEnvLot.Value, 2, siPositivo);
    else
    begin
      Geral.MB_Erro(
        'Forma de sincronismo n�o implementada em "MDFe_PF.EnviaLoteAoFisco()"');
      Exit;
    end;
  end;
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  //
  if DBCheck.CriaFm(TFmMDFeSteps_0100a, FmMDFeSteps_0100a, afmoNegarComAviso) then
  begin
    //FmMDFeSteps_0100a.PnLoteEnv.Visible := True;
    FmMDFeSteps_0100a.EdVersaoAcao.ValueVariant := DmMDFe_0000.QrOpcoesMDFeMDFeVerEnvLot.Value;
    FmMDFeSteps_0100a.CkSoLer.Checked  := SohLer;
    FmMDFeSteps_0100a.Show;
    //
    FmMDFeSteps_0100a.RGAcao.ItemIndex := Integer(TMDFeServicoStep.mdfesrvEnvioLoteMDFe); // Envia lote ao fisco Assincono ou sincrono
    FmMDFeSteps_0100a.RGIndSinc.ItemIndex := Integer(Sincronia);
    FmMDFeSteps_0100a.PreparaEnvioDeLoteMDFe(Lote, Empresa, Sincronia);
    //FmMDFeSteps_0100a.FFormChamou      := 'FmMDFeLEnc_0100a';
    FmMDFeSteps_0100a.FQry      := Qry;
    //
    //
    //FmMDFeSteps_0100a.Destroy;
    //
  end;
end;

function TUnMDFe_PF.Evento_MontaId(tpEvento: Integer; Chave: String;
  nSeqEvento: Integer): String;
const
  TamStr = 54;
begin
//ID 110110 3511031029073900013955001000000001105112804108 01
  Result := 'ID' + Geral.FFN(tpEvento, 6) + Chave + Geral.FFn(nSeqEvento, 2);
  if Length(Result) <> TamStr then
    Geral.MB_Erro('Tamanho do Id difere de ' + Geral.FF0(TamStr) +
    '! - MDFe_PF.Evento_MontaId');
end;

function TUnMDFe_PF.Evento_Obtem_DadosEspecificos(const EvePreDef: Integer;
  const EventoMDFe: TEventoXXe; var tpEvento: Integer; var versao: Double;
  var descEvento: String): Boolean;
begin
  Result   := True;
  tpEvento := nEventoXXe[Integer(TpEvento)];
  case EventoMDFe of
    evexxe110111Can:
    begin
      descEvento := 'Cancelamento';
      versao := 1;
    end;
    evexxe110112Enc:
    begin
      descEvento := 'Encerramento';
      versao := 1;
    end;
    evexxe110114IncCondutor:
    begin
      descEvento := 'Inclusao Condutor';
      versao := 1;
    end;
    else begin
      Result := False;
      tpEvento := 0;
      versao := 0;
      descEvento := 'Evento n�o definido em MDFe_PF.Evento_Obtem_tpEvento';
      Geral.MensagemBox(descEvento, 'ERRO', MB_OK+MB_ICONERROR);
    end;
  end;
end;

function TUnMDFe_PF.InsUpdFaturasMDFe(Empresa, FatID, FatNum: Integer;
  Data: TDateTime; SeriMDFe, NumeroMDFe: Integer): Boolean;
var
  Filial: Integer;
begin
  try
    Result       := False;
    Filial       := DModG.ObtemFilialDeEntidade(Empresa);
    if NumeroMDFe = 0 then
    begin
      Geral.MB_Aviso('Encerramento n�o realizado!' + sLineBreak +
        'O n�mero do manifesto n�o foi definido para a empresa ' +
        FormatFloat('000', Filial) + ' !');
      Exit;
    end;
    Result := True;
  except
    Result := False;
  end;
end;

function TUnMDFe_PF.MontaChaveDeAcesso(cUF: Integer; Emissao: TDateTime;
  CNPJ: String; Modelo, Serie, NumeroMDF: Integer; var ide_cMDF: Integer;
  var cDV, ChaveDeAcesso: String; const cMDF_Atual, ide_tpEmis: Integer;
  versao: Double): Boolean;
var
  UF_IBGE, AAMM, ModMDFe, Mensagem, SerMDFe, NumMDFe, AleMDFe, tpEmis: String;
begin
  Mensagem := '';
  Result := False;
  UF_IBGE := FormatFloat('00', cUF);
  if (cUF < FMinUF_IBGE) or (cUF > FMaxUF_IBGE) then
    Mensagem := 'C�digo IBGE da UF inv�lido: ' + UF_IBGE;
  if Length(UF_IBGE) <> 2 then
    Mensagem := 'C�digo IBGE da UF inv�lido: ' + UF_IBGE;
  //
  AAMM := FormatDateTime('YYMM', Emissao);
  if Emissao < 2 then
    Mensagem := 'Ano/m�s de emiss�o inv�lido: ' + AAMM;
  //
  if Length(CNPJ) < 14 then
    Mensagem := 'Tamanho inv�lido para o CNPJ!';
  //
  ModMDFe := FormatFloat('00', Modelo);
  if Length(ModMDFe) <> 2 then
    Mensagem := 'Modelo de MDFe inv�lido: ' + ModMDFe;
  //
  SerMDFe := FormatFloat('000', Serie);
  if Length(SerMDFe) <> 3 then
    Mensagem := 'S�rie da MDFe inv�lida: ' + SerMDFe;
  //
  NumMDFe := FormatFloat('000000000', NumeroMDF);
  if Length(NumMDFe) <> 9 then
    Mensagem := 'N�mero de MDFe inv�lido: ' + NumMDFe;
  //
  begin
    // vers�o 2.00
    if (cMDF_Atual <> 0) and (cMDF_Atual < 100000000) then
    begin
      ide_cMDF := cMDF_Atual;
      Geral.MensagemBox('Foi detectado que o c�digo num�rico j� existe para esta MDF-e.'
      + sLineBreak + 'Ser� utilizado o mesmo: ' + FormatFloat('00000000', ide_cMDF),
      'Aviso', MB_OK+MB_ICONWARNING);
    end else begin
      //  N�mero aleat�rio
      Randomize;
      ide_cMDF := 0;
      while (ide_cMDF <= 0) or (ide_cMDF > 99999998) do
        ide_cMDF := Random(100000000);
    end;
    //
    AleMDFe := FormatFloat('00000000', ide_cMDF);
    if Length(AleMDFe) <> 8 then
      Mensagem := 'C�digo num�rico da chave da MDFe inv�lido: ' + AleMDFe;
    //
    tpEmis := FormatFloat('0', ide_tpEmis);
    if (ide_tpEmis < 1) or (ide_tpEmis > 5) then
      Mensagem := 'Tipo de emiss�o (#25 ID: B21) inv�lido: ' + tpEmis;
    //
    if ((ide_tpEmis <> 3) and (Serie >= 900))
    or ((ide_tpEmis = 3) and (Serie < 900)) then
      Mensagem := 'Tipo de emiss�o (#25 ID: B21): "' + tpEmis +
      '" � incompat�vel com a s�rie: "' + SerMDFe + '"';

    // Nova concatena��o para a chave de acesso na MDFe 2.0
    ChaveDeAcesso :=
      UF_IBGE + AAMM + CNPJ + ModMDFe + SerMDFe + NumMDFe + tpEmis + AleMDFe;
  end;
  cDV := Geral.Modulo11_2a9_Back(ChaveDeAcesso, 0);
  ChaveDeAcesso := ChaveDeAcesso + cDV;
  if Length(ChaveDeAcesso) <> 44 then
    Mensagem := 'Tamanho da chave de acesso inv�lido. Deveria ser 44 mas � ' +
    IntToStr(Length(ChaveDeAcesso));
  if Mensagem <> '' then
    Geral.MB_Aviso(Mensagem + #13+#10 + 'Function: "MontaChaveDeAcesso"')
  else
    Result := True;
end;

procedure TUnMDFe_PF.MostraFormMDFeJust;
begin
  if DBCheck.CriaFm(TFmMDFeJust, FmMDFeJust, afmoNegarComAviso) then
  begin
    FmMDFeJust.ShowModal;
    FmMDFeJust.Destroy;
  end;
end;

procedure TUnMDFe_PF.MostraFormMDFeLayout_0100;
begin
  Application.CreateForm(TFmMDFeLayout_0100, FmMDFeLayout_0100);
  FmMDFeLayout_0100.ShowModal;
  FmMDFeLayout_0100.Destroy;
end;

procedure TUnMDFe_PF.MostraFormMDFeLEnc(Codigo: Integer);
var
  VersaoMDFe: Integer;
begin
  VersaoMDFe := VersaoMDFeEmUso();
  //
  case VersaoMDFe of
    100:
    if DBCheck.CriaFm(TFmMDFeLEnc_0100a, FmMDFeLEnc_0100a, afmoNegarComAviso) then
    begin
      if Codigo <> 0 then
        FmMDFeLEnc_0100a.LocCod(Codigo, Codigo);
      FmMDFeLEnc_0100a.ShowModal;
      FmMDFeLEnc_0100a.Destroy;
    end;
    else AvisoNaoImplemVerMDFe(VersaoMDFe);
  end;
end;

procedure TUnMDFe_PF.MostraFormMDFeMDFaEdit_0100a(Empresa, FrtMnfCab: Integer;
  QrFrtMnfCab: TmySQLQuery; SerieMDFe, NumeroMDFe: Integer);
begin
  Application.CreateForm(TFmMDFeMDFaEdit_0100a, FmMDFeMDFaEdit_0100a);
  //
  FmMDFeMDFaEdit_0100a.EdCodigo.ValueVariant    := FrtMnfCab;
  FmMDFeMDFaEdit_0100a.FQrFrtMnfCab             := QrFrtMnfCab;
  //
  FmMDFeMDFaEdit_0100a.EdEmpresa.ValueVariant   := Empresa;
  FmMDFeMDFaEdit_0100a.EdSerieMDFe.ValueVariant  := SerieMDFe;
  FmMDFeMDFaEdit_0100a.EdNumeroMDFe.ValueVariant := NumeroMDFe;
  //
  FmMDFeMDFaEdit_0100a.ShowModal;
  FmMDFeMDFaEdit_0100a.Destroy;
end;

procedure TUnMDFe_PF.MostraFormMDFePesq(AbrirEmAba: Boolean;
  InOwner: TWincontrol; AdvToolBarPager: TAdvToolBarPager);
var
  Form: TForm;
begin
  if AbrirEmAba then
  begin
    Form := MyObjects.FormTDICria(TFmMDFe_Pesq_0000, InOwner, AdvToolBarPager, True, True);
    //
(*
    if Cliente <> 0 then
    begin
      if Form <> nil then
      begin
        TFmMDFe_Pesq_0000(Form).EdCliente.ValueVariant := Cliente;
        TFmMDFe_Pesq_0000(Form).CBCliente.KeyValue     := Cliente;
      end;
    end;
*)
  end else
  begin
    if DBCheck.CriaFm(TFmMDFe_Pesq_0000, FmMDFe_Pesq_0000, afmoNegarComAviso) then
    begin
(*
      if Cliente <> 0 then
      begin
        FmMDFe_Pesq_0000.EdCliente.ValueVariant := Cliente;
        FmMDFe_Pesq_0000.CBCliente.KeyValue     := Cliente;
      end;
*)
      FmMDFe_Pesq_0000.ShowModal;
      FmMDFe_Pesq_0000.Destroy;
    end;
  end;
end;

procedure TUnMDFe_PF.MostraFormStepsMDFe_Cancelamento(Empresa, Serie, nMDF,
  nSeqEvento, Justif: Integer; Modelo, nProtCan, chMDFe, XML_Evento: String;
  Qry: TmySQLQuery);
var
  VersaoMDFe: Integer;
begin
  VersaoMDFe := VersaoMDFeEmUso();
  case VersaoMDFe of
    100:
    if DBCheck.CriaFm(TFmMDFeSteps_0100a, FmMDFeSteps_0100a, afmoNegarComAviso) then
    begin
      //
      FmMDFeSteps_0100a.EdVersaoAcao.ValueVariant := DmMDFe_0000.QrOpcoesMDFeMDFeVerCanMDFe.Value;
      FmMDFeSteps_0100a.Show;
      //
      FmMDFeSteps_0100a.RGAcao.ItemIndex := Integer(TMDFeServicoStep.mdfesrvEventoCancelamento); // 3 -Evento de cancelamento
      FmMDFeSteps_0100a.PreparaCancelamentoDeMDFe(Empresa, Serie, nMDF, nSeqEvento,
        Justif, Modelo, nProtCan, chMDFe, XML_Evento);
      FmMDFeSteps_0100a.FQry := Qry;
      //
      //
      //FmMDFeSteps_0100a.Destroy;
      //
    end;
    else AvisoNaoImplemVerMDFe(VersaoMDFe);
  end;
end;

procedure TUnMDFe_PF.MostraFormStepsMDFe_ConsultaMDFe(Empresa, IDCtrl: Integer;
  ChaveMDFe: String; MeXML: TMemo);
var
  VersaoMDFe: Integer;
begin
  if MyObjects.FIC(Length(ChaveMDFe) <> 44, nil, 'Informe a chave da MDFe') then
    Exit;
  VersaoMDFe := VersaoMDFeEmUso();
  case VersaoMDFe of
    100:
    if DBCheck.CriaFm(TFmMDFeSteps_0100a, FmMDFeSteps_0100a, afmoNegarComAviso) then
    begin
      //
      FmMDFeSteps_0100a.EdVersaoAcao.ValueVariant := DmMDFe_0000.QrOpcoesMDFeMDFeVerConMDFe.Value; // Consulta MDFe
      FmMDFeSteps_0100a.Show;
      //
      FmMDFeSteps_0100a.RGAcao.ItemIndex := Integer(TMDFeServicoStep(mdfesrvConsultarMDFe)); // Consulta MDFe
      //DefineVarsDePreparacao();
      FmMDFeSteps_0100a.PreparaConsultaMDFe(Empresa, IDCtrl, ChaveMDFe);
      //
      //
      //FmMDFeSteps_0100a.Destroy;
      //
    end;
  end;
end;

procedure TUnMDFe_PF.MostraFormStepsMDFe_Encerramento(Empresa: Integer; nProt,
  chMDFe, xMun: String; dtEnc: TDateTime; cUF, cMun: Integer; XML_Evento:
  String; Qry: TmySQLQuery);
var
  VersaoMDFe: Integer;
begin
  VersaoMDFe := VersaoMDFeEmUso();
  case VersaoMDFe of
    100:
    if DBCheck.CriaFm(TFmMDFeSteps_0100a, FmMDFeSteps_0100a, afmoNegarComAviso) then
    begin
      //
      FmMDFeSteps_0100a.EdVersaoAcao.ValueVariant := DmMDFe_0000.QrOpcoesMDFeMDFeVerLotEve.Value;
      FmMDFeSteps_0100a.Show;
      //
      FmMDFeSteps_0100a.RGAcao.ItemIndex := Integer(TMDFeServicoStep.mdfesrvEventoEncerramento); // 8 - Encerramento de MDF-e
      FmMDFeSteps_0100a.PreparaEncerramentoDeMDFe(Empresa, chMDFe, xMun, nProt,
      dtEnc, cUF, cMun, XML_Evento);
      FmMDFeSteps_0100a.FQry := Qry;
      //
      //
      //FmMDFeSteps_0100a.Destroy;
      //
    end;
    else AvisoNaoImplemVerMDFe(VersaoMDFe);
  end;
end;

procedure TUnMDFe_PF.MostraFormStepsMDFe_IncCondutor(Empresa: Integer; nProt,
  chMDFe, xNome, CPF, XML_Evento: String; Qry: TmySQLQuery);
var
  VersaoMDFe: Integer;
begin
  VersaoMDFe := VersaoMDFeEmUso();
  case VersaoMDFe of
    100:
    if DBCheck.CriaFm(TFmMDFeSteps_0100a, FmMDFeSteps_0100a, afmoNegarComAviso) then
    begin
      //
      FmMDFeSteps_0100a.EdVersaoAcao.ValueVariant := DmMDFe_0000.QrOpcoesMDFeMDFeVerLotEve.Value;
      FmMDFeSteps_0100a.Show;
      //
      FmMDFeSteps_0100a.RGAcao.ItemIndex := Integer(TMDFeServicoStep.mdfesrvEventoIncCondutor);  //6? - Inclusao de Condutor
      FmMDFeSteps_0100a.PreparaEventoIncCondutor(Empresa, chMDFe, nProt,
      xNome, CPF, XML_Evento);
      FmMDFeSteps_0100a.FQry := Qry;
      //
      //
      //FmMDFeSteps_0100a.Destroy;
      //
    end;
    else AvisoNaoImplemVerMDFe(VersaoMDFe);
  end;
end;

procedure TUnMDFe_PF.MostraFormStepsMDFe_NaoEncerrados;
var
  VersaoMDFe: Integer;
begin
  VersaoMDFe := VersaoMDFeEmUso();
  //
  case VersaoMDFe of
    100:
    if DBCheck.CriaFm(TFmMDFeSteps_0100a, FmMDFeSteps_0100a, afmoNegarComAviso) then
    begin
      //
      //FmMDFeSteps_0100a.PnLoteEnv.Visible := True;
      FmMDFeSteps_0100a.EdVersaoAcao.ValueVariant := DmMDFe_0000.QrOpcoesMDFeMDFeVerNaoEnc.Value;
      FmMDFeSteps_0100a.Show;
      //
      FmMDFeSteps_0100a.RGAcao.ItemIndex := 9; // Consulta nao encerrados
      FmMDFeSteps_0100a.PreparaConsultaNaoEncerrados(DmodG.QrFiliLogCodigo.Value);
      //
      //FmMDFeSteps_0100a.Destroy;
      //
    end;
    else AvisoNaoImplemVerMDFe(VersaoMDFe);
  end;
end;

procedure TUnMDFe_PF.MostraFormStepsMDFe_StatusServico();
var
  VersaoMDFe: Integer;
begin
  VersaoMDFe := VersaoMDFeEmUso();
  //
  case VersaoMDFe of
    100:
    if DBCheck.CriaFm(TFmMDFeSteps_0100a, FmMDFeSteps_0100a, afmoNegarComAviso) then
    begin
      //
      //FmMDFeSteps_0100a.PnLoteEnv.Visible := True;
      FmMDFeSteps_0100a.EdVersaoAcao.ValueVariant := DmMDFe_0000.QrOpcoesMDFeMDFeVerStaSer.Value;
      FmMDFeSteps_0100a.Show;
      //
      FmMDFeSteps_0100a.RGAcao.ItemIndex := 0; // Status do servi�o
      FmMDFeSteps_0100a.PreparaVerificacaoStatusServico(DmodG.QrFiliLogCodigo.Value);
      //
      //FmMDFeSteps_0100a.Destroy;
      //
    end;
    else AvisoNaoImplemVerMDFe(VersaoMDFe);
  end;
end;

function TUnMDFe_PF.ObtemURLWebServerMDFe(UF, Servico, Versao: String;
  tpAmb: Integer): String;
var
  UF_Str, Servico_Str, tpAmb_Str: String;
begin
  Result := '';
  //
  try
    UF_Str      := Trim(LowerCase(UF));
    Servico_Str := Trim(LowerCase(Servico));
    tpAmb_Str   := Geral.FF0(tpAmb);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAllAux, DModG.AllID_DB, [
      'SELECT URL ',
      'FROM MDFe_ws ',
      'WHERE LOWER(UF) = "' + UF_Str + '" ',
      'AND LOWER(Servico) = "' + Servico_Str + '" ',
      'AND tpAmb = "' + tpAmb_Str + '" ',
      'AND Versao = "' + Versao + '" ',
      '']);
    if DModG.QrAllAux.RecordCount > 0 then
      Result := DModG.QrAllAux.FieldByName('URL').AsString;
  finally
    DModG.QrAllAux.Close;
  end;
end;

function TUnMDFe_PF.Obtem_Serie_e_NumMDF(const SerieDesfeita, MDFeDesfeita,
  ParamsMDFe, FatID, FatNum, Empresa: Integer; const Tabela: String;
  var SerieMDFe, NumeroMDFe: Integer): Boolean;
var
  SerieNormal, MaxSeqLib, SerieDesfe, MDFeDesfe: Integer;
  Qry: TmySQLQuery;
begin
  Result := False;
  DModG.ReopenParamsEmp(Empresa);
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT SerieMDFe, Sequencial, MaxSeqLib  ',
    'FROM paramsmdfe ',
    'WHERE Controle=' + Geral.FF0(ParamsMDFe),
    '']);
    SerieNormal := Qry.FieldByName('SerieMDFe').AsInteger;
    MaxSeqLib   := Qry.FieldByName('MaxSeqLib').AsInteger;
    //
  finally
    Qry.Free;
  end;
  //
  //Geral.MB_Info('Desmarcar aqui [1]');
  if (SerieDesfeita = SerieNormal) and (MDFeDesfeita <> 0) then
  begin
    Geral.MB_Info('Foi detectado que j� havia sido usado o n�mero ' +
    FormatFloat('000000000', MDFeDesfeita) +
    ' para esta MDFe. Ser� usado este mesmo n�mero novamente na recria��o!');
    SerieMDFe  := SerieDesfeita;
    NumeroMDFe := MDFeDesfeita;
    Result := True;
  end else
  begin
    NumeroMDFe := 0;
    case DmMDFe_0000.QropcoesMDFeMDFetpEmis.Value of
      1:
      begin
        SerieMDFe := SerieNormal;
        Result   := DModG.BuscaProximoCodigoInt_Novo('paramsmdfe', 'Sequencial',
        'WHERE Controle=' + dmkPF.FFP(ParamsMDFe, 0), 0, 'S�rie: ' +
        FormatFloat('000', SerieMDFe) +
        sLineBreak + 'Empresa: ' + dmkPF.FFP(Empresa, 0) + sLineBreak +
        'Verifique a configura��o da MDF-e da filal informada!',
        NumeroMDFe, MaxSeqLib);
      end;
      3:
      begin
        if Geral.MB_Pergunta(
        'A filial selecionada est� configurada para envio de MDF-e em conting�ncia!'
        + sLineBreak + 'Tem certeza que deseja continuar?') = ID_YES then
        begin
          Result := DModG.BuscaProximoCodigoInt_Novo('paramsemp', 'SCAN_nMDF',
          'WHERE Codigo=' + dmkPF.FFP(Empresa, 0), 0, 'S�rie: ' +
          FormatFloat('000', SerieMDFe) +
          sLineBreak + 'Empresa: ' + dmkPF.FFP(Empresa, 0) + sLineBreak +
          'Verifique a configura��o da MDF-e em SCAN da filal informada!',
          NumeroMDFe, MaxSeqLib);
        end;
      end;
    end;
    //
    if NumeroMDFe > 0 then
    begin
      SerieDesfe := SerieMDFe;
      MDFeDesfe    := NumeroMDFe;
      if Lowercase(Tabela) = 'frtmnfcab' then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'frtmnfcab', False, [
        'SerieDesfe', 'MDFDesfeita'], [
        'Codigo'], [
        SerieDesfe, MDFeDesfe], [
        FatNum], True);
      end else
        Geral.MB_Aviso('Tabela desconhecida: "' + Tabela +
        '" para S�rie e N�mero de MDFe desfeito!');
    end;
  end;
end;

function TUnMDFe_PF.Texto_StatusMDFe(Status: Integer;
  VersaoMDFe: Double): String;
var
  Versao: Integer;
begin
  Versao := Trunc(VersaoMDFe * 100 + 0.5);
  if Versao = 0 then
    Versao := Trunc(CTE_EMI_VERSAO_MAX_USO * 100 + 0.5);
  case Versao of
    200:
    case Status of
      0  : Result := '';
      //
      10 : Result := 'Registros da MDF-e inclu�dos na Base da Dados';
      20 : Result := 'XML da MDF-e foi gerada';
      30 : Result := 'XML da MDF-e est� assinado';
      40 : Result := 'Lote de MDFe(s) rejeitado';
      50 : Result := 'MDF-e adicionada ao lote';
      60 : Result := 'Lote ao qual a MDF-e pertence foi enviado';
      70 : Result := 'Lote ao qual a MDF-e pertence em fase de consulta';
      //
      //Tabela de C�digos de Erros e Descri��es de Mensagens de Erros
      //C�DIGO RESULTADO DO PROCESSAMENTO DA SOLICITA��O
     100 : Result := 'Autorizado o uso do MDF-e';
     101 : Result := 'Cancelamento de MDF-e homologado';
     103 : Result := 'Arquivo recebido com sucesso';
     104 : Result := 'Arquivo processado';
     105 : Result := 'Arquivo em processamento';
     106 : Result := 'Arquivo n�o localizado';
     107 : Result := 'Servi�o em Opera��o';
     108 : Result := 'Servi�o Paralisado Momentaneamente (curto prazo)';
     109 : Result := 'Servi�o Paralisado sem Previs�o';
     111 : Result := 'Consulta N�o Encerrados localizou MDF-e nessa situa��o';
     112 : Result := 'Consulta N�o Encerrados n�o localizou MDF-e nessa situa��o';
     132 : Result := 'Encerramento de MDF-e homologado';
     135 : Result := 'Evento registrado e vinculado a MDF-e';
     136 : Result := 'Evento registrado, mas n�o vinculado a MDF-e';
     137 : Result := 'Evento registrado com alerta para situa��o';

     //C�DIGO MOTIVOS DE N�O ATENDIMENTO DA SOLICITA��O

     203 : Result := 'Rejei��o: Emissor n�o habilitado para emiss�o do MDF-e';
     204 : Result := 'Rejei��o: Duplicidade de MDF-e [nRec:999999999999999].';
     207 : Result := 'Rejei��o: CNPJ do emitente inv�lido';
     209 : Result := 'Rejei��o: IE do emitente inv�lida';
     212 : Result := 'Rejei��o: Data de emiss�o MDF-e posterior a data de recebimento';
     213 : Result := 'Rejei��o: CNPJ-Base do Emitente difere do CNPJ-Base do Certificado Digital';
     214 : Result := 'Rejei��o: Tamanho da mensagem excedeu o limite estabelecido';
     215 : Result := 'Rejei��o: Falha no schema XML';
     216 : Result := 'Rejei��o: Chave de Acesso difere da cadastrada';
     217 : Result := 'Rejei��o: MDF-e n�o consta na base de dados da SEFAZ';
     218 : Result := 'Rejei��o: MDF-e j� est� cancelado na base de dados da SEFAZ.';
     219 : Result := 'Rejei��o: Circula��o do MDF-e verificada';
     220 : Result := 'Rejei��o: MDF-e autorizado h� mais de 24 horas';
     222 : Result := 'Rejei��o: Protocolo de Autoriza��o de Uso difere do cadastrado';
     223 : Result := 'Rejei��o: CNPJ do transmissor do arquivo difere do CNPJ do transmissor da consulta';
     225 : Result := 'Rejei��o: Falha no Schema XML do MDF-e';
     226 : Result := 'Rejei��o: C�digo da UF do Emitente diverge da UF autorizadora';
     227 : Result := 'Rejei��o: Erro na composi��o do Campo ID';
     228 : Result := 'Rejei��o: Data de Emiss�o muito atrasada';
     229 : Result := 'Rejei��o: IE do emitente n�o informada';
     230 : Result := 'Rejei��o: IE do emitente n�o cadastrada';
     231 : Result := 'Rejei��o:IE do emitente n�o vinculada ao CNPJ';
     236 : Result := 'Rejei��o: Chave de Acesso com d�gito verificador inv�lido';
     238 : Result := 'Rejei��o: Cabe�alho - Vers�o do arquivo XML superior a Vers�o vigente';
     239 : Result := 'Rejei��o: Cabe�alho - Vers�o do arquivo XML n�o suportada';
     242 : Result := 'Rejei��o: Elemento mdfeCabecMsg inexistente no SOAP Header';
     243 : Result := 'Rejei��o: XML Mal Formado';
     245 : Result := 'Rejei��o: CNPJ Emitente n�o cadastrado';
     247 : Result := 'Rejei��o: Sigla da UF do Emitente diverge da UF autorizadora';
     248 : Result := 'Rejei��o: UF do Recibo diverge da UF autorizadora';
     249 : Result := 'Rejei��o: UF da Chave de Acesso diverge da UF autorizadora';
     250 : Result := 'Rejei��o: UF diverge da UF autorizadora';
     252 : Result := 'Rejei��o: Ambiente informado diverge do Ambiente de recebimento';
     253 : Result := 'Rejei��o: Digito Verificador da chave de acesso composta inv�lido';
     280 : Result := 'Rejei��o: Certificado Transmissor inv�lido';
     281 : Result := 'Rejei��o: Certificado Transmissor Data Validade';
     282 : Result := 'Rejei��o: Certificado Transmissor sem CNPJ';
     283 : Result := 'Rejei��o: Certificado Transmissor - erro Cadeia de Certifica��o';
     284 : Result := 'Rejei��o: Certificado Transmissor revogado';
     285 : Result := 'Rejei��o: Certificado Transmissor difere ICP-Brasil';
     286 : Result := 'Rejei��o: Certificado Transmissor erro no acesso a LCR';
     290 : Result := 'Rejei��o: Certificado Assinatura inv�lido';
     291 : Result := 'Rejei��o: Certificado Assinatura Data Validade';
     292 : Result := 'Rejei��o: Certificado Assinatura sem CNPJ';
     293 : Result := 'Rejei��o: Certificado Assinatura - erro Cadeia de Certifica��o';
     294 : Result := 'Rejei��o: Certificado Assinatura revogado';
     295 : Result := 'Rejei��o: Certificado Assinatura difere ICP-Brasil';
     296 : Result := 'Rejei��o: Certificado Assinatura erro no acesso a LCR';
     297 : Result := 'Rejei��o: Assinatura difere do calculado';
     298 : Result := 'Rejei��o: Assinatura difere do padr�o do Projeto';
     299 : Result := 'Rejei��o: XML da �rea de cabe�alho com codifica��o diferente de UTF-8';
     402 : Result := 'Rejei��o: XML da �rea de dados com codifica��o diferente de UTF-8';
     404 : Result := 'Rejei��o: Uso de prefixo de namespace n�o permitido';
     409 : Result := 'Rejei��o: Campo cUF inexistente no elemento mdfeCabecMsg do SOAP Header';
     410 : Result := 'Rejei��o: UF informada no campo cUF n�o � atendida pelo WebService';
     411 : Result := 'Rejei��o: Campo versaoDados inexistente no elemento mdfeCabecMsg do SOAP Header';
     455 : Result := 'Rejei��o: C�digo de Munic�pio de Carregamento do MDF-e: d�gito inv�lido';
     456 : Result := 'Rejei��o: C�digo de Munic�pio diverge da UF de Carregamento do MDF-e';
     473 : Result := 'Rejei��o: Tipo Autorizador do Recibo diverge do �rg�o Autorizador';
     494 : Result := 'Rejei��o: Processo de emiss�o informado inv�lido';
     539 : Result := 'Rejeicao: Duplicidade de MDF-e, com diferen�a na Chave de Acesso [chMDFe: 99999999999999999999999999999999999999999999][nRec:999999999999999]';
     579 : Result := 'Rejei��o: Vers�o informada para o modal n�o suportada';
     580 : Result := 'Rejei��o: Falha no Schema XML espec�fico para o modal';
     592 : Result := 'Rejei��o: Chave de acesso inv�lida (Ano < 2012 ou Ano maior que Ano corrente)';
     593 : Result := 'Rejei��o: Chave de acesso inv�lida (M�s = 0 ou M�s > 12)';
     594 : Result := 'Rejei��o: Chave de acesso inv�lida (CNPJ zerado ou digito inv�lido)';
     595 : Result := 'Rejei��o: Chave de acesso inv�lida (modelo diferente de 58)';
     596 : Result := 'Rejei��o: Chave de acesso inv�lida (numero MDFe = 0)';
     598 : Result := 'Rejei��o: Usar somente o namespace padrao do MDF-e';
     599 : Result := 'Rejei��o: Nao eh permitida a presenca de caracteres de edicao no inicio/fim da mensagem ou entre as tags da mensagem';
     600 : Result := 'Rejei��o: Chave de Acesso difere da existente em BD';
     601 : Result := 'Rejei��o: Chave de acesso do CT-e informado inv�lida';
     602 : Result := 'Rejei��o: Segundo C�digo de Barras deve ser informado para CT-e em conting�ncia FS-DA';
     603 : Result := 'Rejei��o: Segundo C�digo de Barras n�o deve ser informado para CT-e com este tipo de emiss�o';
     604 : Result := 'Rejei��o: Chave de acesso da NF-e informada inv�lida';
     605 : Result := 'Rejei��o: NF-e emitida por empresa diferente da empresa emitente do MDF-e';
     606 : Result := 'Rejei��o: Segundo C�digo de Barras deve ser informado para NF-e em conting�ncia (FS-DA e FSIA)';
     607 : Result := 'Rejei��o: Segundo C�digo de Barras n�o deve ser informado para NF-e com este tipo de emiss�o';
     608 : Result := 'Rejei��o: NF emitida por empresa diferente da empresa emitente do MDF-e';
     609 : Result := 'Rejei��o: MDF-e j� est� encerrado na base de dados da SEFAZ';
     610 : Result := 'Rejei��o: Existe MDF-e n�o encerrado para esta placa, UF carregamento e UF descarregamento em data de emiss�o diferente. [chMDFe: 99999999999999999999999999999999999999999999][nProt:999999999999999]';
     611 : Result := 'Rejei��o: C�digo de Munic�pio de descarregamento: d�gito inv�lido';
     612 : Result := 'Rejei��o: C�digo de Munic�pio diverge da UF de descarregamento do MDF-e';
     613 : Result := 'Rejei��o: C�digo de Munic�pio de encerramento: d�gito inv�lido';
     614 : Result := 'Rejei��o: C�digo de Munic�pio diverge da UF de encerramento do MDF-e';
     615 : Result := 'Rejei��o: Data de encerramento anterior � data de autoriza��o do MDF-e';
     616 : Result := 'Rejei��o: Nenhum grupo de documentos foi informado (CT-e, CT, NF-e, NF, MDF-e) Retornar Munic�pio de Descarregamento sem DFe';
     617 : Result := 'Rejei��o: Chave de acesso de CT-e inv�lida (Ano < 2009 ou Ano maior que Ano corrente)';
     618 : Result := 'Rejei��o: Chave de acesso de CT-e inv�lida (M�s = 0 ou M�s > 12)';
     619 : Result := 'Rejei��o: Chave de acesso de CT-e inv�lida (CNPJ zerado ou digito inv�lido)';
     620 : Result := 'Rejei��o: Chave de acesso de CT-e inv�lida (modelo diferente de 57)';
     621 : Result := 'Rejei��o: Chave de acesso de CT-e inv�lida (numero CT = 0)';
     622 : Result := 'Rejei��o: Chave de acesso de NF-e inv�lida (Ano < 2005 ou Ano maior que Ano corrente)';
     623 : Result := 'Rejei��o: Chave de acesso de NF-e inv�lida (M�s = 0 ou M�s > 12)';
     624 : Result := 'Rejei��o: Chave de acesso de NF-e inv�lida (CNPJ zerado ou digito inv�lido)';
     625 : Result := 'Rejei��o: Chave de acesso de NF-e inv�lida (modelo diferente de 55)';
     626 : Result := 'Rejei��o: Chave de acesso de NF-e inv�lida (numero NF = 0)';
     627 : Result := 'Rejei��o: CNPJ do autor do evento inv�lido';
     628 : Result := 'Rejei��o: Erro Atributo ID do evento n�o corresponde � concatena��o dos campos (�ID� + tpEvento + chMDFe + nSeqEvento)';
     629 : Result := 'Rejei��o: O tpEvento informado inv�lido';
     630 : Result := 'Rejei��o: Falha no Schema XML espec�fico para o evento';
     631 : Result := 'Rejei��o: Duplicidade de evento';
     632 : Result := 'Rejei��o: O autor do evento diverge do emissor do MDF-e';
     633 : Result := 'Rejei��o: O autor do evento n�o � um �rg�o autorizado a gerar o evento';
     634 : Result := 'Rejei��o: A data do evento n�o pode ser menor que a data de emiss�o do MDF-e';
     635 : Result := 'Rejei��o: A data do evento n�o pode ser maior que a data do processamento';
     636 : Result := 'Rejei��o: O numero sequencial do evento � maior que o permitido';
     637 : Result := 'Rejei��o: A data do evento n�o pode ser menor que a data de autoriza��o do MDF-e';
     638 : Result := 'Rejei��o: N�o deve ser informada Nota Fiscal para tipo de emitente Prestador Servi�o de Transporte.';
     639 : Result := 'Rejei��o: N�o deve ser informado Conhecimento de Transporte para tipo de emitente Transporte de Carga Pr�pria.';
     640 : Result := 'Rejei��o: CPF do Funcion�rio do registro de passagem inv�lido';
     641 : Result := 'Rejei��o: UF autorizadora difere da UF de Passagem';
     642 : Result := 'Rejei��o: Registro de Passagem descartado';
     643 : Result := 'Rejei��o: Placa do ve�culo de tra��o deve ser informada no registro de passagem para o modal rodovi�rio';
     644 : Result := 'Rejei��o: Evento de inclus�o de condutor s� pode ser registrado para o modal rodovi�rio';
     645 : Result := 'Rejei��o: CPF do condutor inv�lido';
     646 : Result := 'Rejei��o: Placa de ve�culo formato inv�lido (UF Carregamento e Descarregamento <> �EX�)';
     647 : Result := 'Rejei��o: MDF-e s� pode ser referenciado por manifesto do modal aquavi�rio';
     648 : Result := 'Rejei��o: MDF-e s� pode ser referenciado quando UF de Carregamento/Descarregamento for igual a AM ou AP ';
     649 : Result := 'Rejei��o: Chave de acesso de MDF-e informada inv�lida';
     650 : Result := 'Rejei��o: Chave de acesso de MDF-e inv�lida (Ano < 2013 ou Ano maior que Ano corrente)';
     651 : Result := 'Rejei��o: Chave de acesso de MDF-e inv�lida (M�s = 0 ou M�s > 12)';
     652 : Result := 'Rejei��o: Chave de acesso de MDF-e inv�lida (CNPJ zerado ou digito inv�lido)';
     653 : Result := 'Rejei��o: Chave de acesso de MDF-e inv�lida (modelo diferente de 58)';
     654 : Result := 'Rejei��o: Chave de acesso de MDF-e inv�lida (numero MDF = 0)';
     655 : Result := 'Rejei��o: MDF-e referenciado n�o existe na base de dados da SEFAZ';
     656 : Result := 'Rejei��o: Chave de Acesso do MDF-e referenciado difere da existente em BD';
     657 : Result := 'Rejei��o: MDF-e referenciado j� est� cancelado na base de dados da SEFAZ';
     658 : Result := 'Rejei��o: Modal do MDF-e referenciado diferente de Rodovi�rio';
     659 : Result := 'Rejei��o: Tipo do Emitente do MDF-e referenciado difere de Transportador de Carga Pr�pria';
     660 : Result := 'Rejei��o: CNPJ autorizado para download inv�lido';
     661 : Result := 'Rejei��o: CPF autorizado para download inv�lido';
     662 : Result := 'Rejei��o: Existe MDF-e aberto para a placa do ve�culo tra��o contendo documento relacionado no MDF-e. [chDFe: 99999999999999999999999999999999999999999999]';
     663 : Result := 'Rejei��o: Percurso informado inv�lido';
     664 : Result := 'Rejei��o: Vedada a emiss�o de conhecimento de transporte em papel. CT-e obrigat�rio.';
     665 : Result := 'Rejei��o: Emitente da Nota Fiscal � obrigado � emiss�o de Nota Fiscal Eletr�nica';
     666 : Result := 'Rejei��o: Ano do MDF-e informado na chave de acesso inv�lido';
     667 : Result := 'Rejei��o: Quantidade informada no grupo de totalizadores n�o confere com a quantidade de documentos relacionada ';
     668 : Result := 'Rejei��o: Chave de Acesso de CT-e duplicada [chCTe: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX]';
     669 : Result := 'Rejei��o: Chave de Acesso de NF-e duplicada [chNFe: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX]';
     670 : Result := 'Rejei��o: Chave de Acesso de CT-e inv�lida (Tipo de Emiss�o inv�lido)';
     671 : Result := 'Rejei��o: CT-e informado n�o existe na base de dados da SEFAZ [chCTe: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX]';
     672 : Result := 'Rejei��o: CT-e informado com diferen�a de chave de acesso [chCTe: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX]';
     673 : Result := 'Rejei��o: CT-e informado n�o pode estar cancelado/denegado na base da SEFAZ [chCTe: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX]';
     674 : Result := 'Rejei��o: Chave de Acesso de NF-e inv�lida (Tipo de Emiss�o inv�lido)';
     675 : Result := 'Rejei��o: NF-e informada n�o existe na base de dados da SEFAZ [chNFe: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX]';
     676 : Result := 'Rejei��o: NF-e informada com diferen�a de chave de acesso [chNFe: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX]';
     677 : Result := 'Rejei��o: NF-e informada n�o pode estar cancelada/denegada na base da SEFAZ [chNFe: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX]';
     678 : Result := 'Rejei��o: Uso Indevido';
     679 : Result := 'Rejei��o: Chave de Acesso de MDF-e inv�lida (Tipo de Emiss�o inv�lido)';
     680 : Result := 'Rejei��o: Munic�pio de descarregamento duplicado no MDF-e';
     681 : Result := 'Rejei��o: RNTRC informado inexistente.';
     682 : Result := 'Rejei��o: RNTRC situa��o inv�lida.';
     683 : Result := 'Rejei��o: Placa do ve�culo de tra��o n�o vinculada ao RNTRC informado.';
     684 : Result := 'Rejei��o: CIOT obrigat�rio para RNTRC informado.';
     685 : Result := 'Rejei��o: Municipio de carregamento duplicado no MDF-e';
     686 : Result := 'Rejei��o: Existe MDF-e n�o encerrado h� mais de 30 dias para o emitente [chMDFe: 99999999999999999999999999999999999999999999][nProt:999999999999999]';
     999 : Result := 'Rejei��o: Erro n�o catalogado (informar a msg de erro capturado no tratamento da exce��o)';
      //
      else Result := 'Status n�o definido! Informe a DERMATEK';
    end;
  end;
end;

procedure TUnMDFe_PF.TotaisMDFe(Codigo: Integer; QrFrtFatCab: TmySQLQuery);
var
  Qry: TmySQLQuery;
  Tot_qCTe, Tot_qNFe, Tot_qMDFe: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT ',
    'SUM(IF(DocMod=57, 1, 0)) Tot_qCTe,',
    'SUM(IF(DocMod=55, 1, 0)) Tot_qNFe,',
    'SUM(IF(DocMod=58, 1, 0)) Tot_qMDFe',
    'FROM frtmnfinfdoc',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    Tot_qCTe  := Qry.FieldByName('Tot_qCTe').AsInteger;
    Tot_qNFe  := Qry.FieldByName('Tot_qNFe').AsInteger;
    Tot_qMDFe := Qry.FieldByName('Tot_qMDFe').AsInteger;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'frtmnfcab', False, [
    'Tot_qCTe', 'Tot_qNFe', 'Tot_qMDFe'(*,
    'Tot_vCarga', 'Tot_cUnid', 'Tot_qCarga'*)], [
    'Codigo'
    ], [
    Tot_qCTe, Tot_qNFe, Tot_qMDFe(*,
    Tot_vCarga, Tot_cUnid,
    Tot_qCarga*)], [
    Codigo
    ], True) then
    begin
      if QrFrtFatCab <> nil then
        UnDmkDAC_PF.AbreQuery(QrFrtFatCab, QrFrtFatCab.DataBase);
    end;
  finally
    Qry.Free;
  end;
end;

function TUnMDFe_PF.TxtNo_SitEnc(): String;
begin
  Result :=
  'ELT(cSitEnc+2, "Indefinido", "N�o encerrado", "Encerrado", "? ? ?") NO_cSitEnc, ';
end;

function TUnMDFe_PF.VDom(const Grupo, Codigo: Integer; const Campo, Valor,
  Dominio: String; var AllMsg: String): Boolean;
const
  Dominios: array [0..13] of String = ('D0', 'D1', 'D2', 'D3', 'D4', 'D5',
  'D6', 'D7', 'D8', 'D9', 'D10', 'D11', 'D12', 'D13');
  D0: array [0..0] of String = ('');
  D1: array [0..026] of String = ('11', '12', '13', '14', '15', '16', '17', '21', '22', '23', '24', '25', '26', '27', '28', '29', '31', '32', '33', '35', '41', '42', '43', '50', '51', '52', '53');
  D2: array [0..029] of String = ('11', '12', '13', '14', '15', '16', '17', '21', '22', '23', '24', '25', '26', '27', '28', '29', '31', '32', '33', '35', '41', '42', '43', '50', '51', '52', '53', '90', '91', '92');
  D3: array [0..000] of String = ('58');
  D4: array [0..027] of String = ('AC', 'AL', 'AM', 'AP', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MG', 'MS', 'MT', 'PA', 'PB', 'PE', 'PI', 'PR', 'RJ', 'RN', 'RO', 'RR', 'RS', 'SC', 'SE', 'SP', 'TO', 'EX');
  D5: array [0..001] of String = ('1', '2');
  D6: array [0..006] of String = ('1', '2', '3', '4', '5', '6', '7');
  D7: array [0..003] of String = ('1', '2', '3', '4');
  D8: array [0..001] of String = ('01', '02');
  D9: array [0..001] of String = ('0', '3');
  D10: array [0..028] of String = ('01', '1B', '02', '2D', '2E', '04', '06', '07', '08', '8B', '09', '10', '11', '13', '14', '15', '16', '17', '18', '20', '21', '22', '23', '24', '25', '26', '27', '28', '55');
  // Erro no manual!!!
  D11: array [0..002] of String = ('0', '1', '2');
  D12: array [0..006] of String = ('00', '01', '02', '03', '04', '05', '06');
  D13: array [0..005] of String = ('00', '01', '02', '03', '04', '05');
  // FIM - Erro no manual!!!
var
  Lista: Integer;
  Letra, Nums, Msg: String;
begin
  if Trim(Dominio) = '' then
  begin
    Result := True;
    Exit;
  end else
    Result := False;
  Lista := AnsiIndexStr(Dominio, Dominios);
  if Lista > -1 then
  begin
    case Lista of
       0: Result := AnsiMatchStr(Valor, D0);
       1: Result := AnsiMatchStr(Valor, D1);
       2: Result := AnsiMatchStr(Valor, D2);
       3: Result := AnsiMatchStr(Valor, D3);
       4: Result := AnsiMatchStr(Valor, D4);
       5: Result := AnsiMatchStr(Valor, D5);
       6: Result := AnsiMatchStr(Valor, D6);
       7: Result := AnsiMatchStr(Valor, D7);
       8: Result := AnsiMatchStr(Valor, D8);
       9: Result := AnsiMatchStr(Valor, D9);
      10: Result := AnsiMatchStr(Valor, D10);
      // Erro no manual!!!
      11: Result := AnsiMatchStr(Valor, D11);
      12: Result := AnsiMatchStr(Valor, D12);
      13: Result := AnsiMatchStr(Valor, D13);
      // FIM Erro no manual!!!
      //
      else Geral.MB_Aviso('Dominio MDF-e "' + Dominio + '" inv�lido! [1]');
    end;
  end else
    Geral.MB_Aviso('Dominio MDF-e "' + Dominio + '" inv�lido! [2]');
  if not Result then
  begin
    Msg := Msg + 'MDF-e item #' + Geral.FF0(Grupo) + '.' + Geral.FF0(Codigo) +
    ' campo "' + Campo + '": ' + 'Valor inv�lido: "' + Valor + '" no Dom�nio: '
    + Dominio + sLineBreak;
    Geral.MB_Aviso(Msg);
    //
    AllMsg := AllMsg + Msg;
  end;
  //
end;

function TUnMDFe_PF.VersaoMDFeEmUso: Integer;
var
  Empresa: Integer;
begin
  Empresa := Geral.IMV(VAR_LIB_EMPRESAS);
  DmMDFe_0000.ReopenOpcoesMDFe(DmMDFe_0000.QrOpcoesMDFe, Empresa, True);
  Result := Trunc((DmMDFe_0000.QrOpcoesMDFeMDFeversao.Value  * 100) + 0.5);
end;

function TUnMDFe_PF.VRegExp(const Grupo, Codigo: Integer; const Campo, Valor,
  ExpressaoRegular: String; var AllMsg: String): Boolean;
const
  RegExps: array [0..53] of string = ('ER0',
  'ER1', 'ER2', 'ER3', 'ER4', 'ER5', 'ER6', 'ER7', 'ER8', 'ER9', 'ER10',
  'ER11', 'ER12', 'ER13', 'ER14', 'ER15', 'ER16', 'ER17', 'ER18', 'ER19',
  'ER20', 'ER21', 'ER22', 'ER23', 'ER24', 'ER25', 'ER26', 'ER27', 'ER28',
  'ER29', 'ER30', 'ER31', 'ER32', 'ER33', 'ER34', 'ER35', 'ER36', 'ER37',
  'ER38', 'ER39', 'ER40', 'ER41', 'ER42', 'ER43', 'ER44', 'ER45', 'ER46',
  'ER47', 'ER48', 'ER49', 'ER50', 'ER51', 'ER52', 'ER53'
  );
const
  ER0 = '';
  ER1 = '[0-9]{7}';
  ER2 = '[0-9]{44}';
  ER3 = '[0-9]{36}';
  ER4 = '[0-9]{15}';
  ER5 = '[0-9]{3}';
  ER6 = '[0-9]{14}';
  ER7 = '[0-9]{3,14}';
  ER8 = '[0-9]{0}|[0-9]{14}';
  ER9 = '[0-9]{11}';
  ER10 = '[0-9]{3,11}';
  ER11 = '0|0\.[0-9]{2}|[1-9]{1}[0-9]{0,2}(\.[0-9]{2})?';
  ER12 = '0|0\.[0-9]{2}|[1-9]{1}[0-9]{0,2}(\.[0-9]{3})?';
  ER13 = '0\.[0-9]{1}[1-9]{1}|0\.[1-9]{1}[0-9]{1}|[1-9]{1}[0-9]{0,2}(\.[0-9]{2})?';
  ER14 = '0|0\.[0-9]{3}|[1-9]{1}[0-9]{0,7}(\.[0-9]{3})?';
  ER15 = '0\.[1-9]{1}[0-9]{2}|0\.[0-9]{2}[1-9]{1}|0\.[0-9]{1}[1-9]{1}[0-9]{1}|[1-9]{1}[0-9]{0,7}(\.[0-9]{3})?';
  ER16 = '0|0\.[0-9]{4}|[1-9]{1}[0-9]{0,7}(\.[0-9]{4})?';
  ER17 = '0\.[1-9]{1}[0-9]{3}|0\.[0-9]{3}[1-9]{1}|0\.[0-9]{2}[1-9]{1}[0-9]{1}|0\.[0-9]{1}[1-9]{1}[0-9]{2}|[1-9]{1}[0-9]{0,7}(\.[0-9]{4})?';
  ER18 = '0\.[1-9]{1}[0-9]{5}|0\.[0-9]{1}[1-9]{1}[0-9]{4}|0\.[0-9]{2}[1-9]{1}[0-9]{3}|0\.[0-9]{3}[1-9]{1}[0-9]{2}|0\.[0-9]{4}[1-9]{1}[0-9]{1}|0\.[0-9]{5}[1-9]{1}|[1-9]{1}[0-9]{0,8}(\.[0-9]{6})?';
  ER19 = '0|0\.[0-9]{4}|[1-9]{1}[0-9]{0,10}(\.[0-9]{4})?';
  ER20 = '0\.[1-9]{1}[0-9]{3}|0\.[0-9]{3}[1-9]{1}|0\.[0-9]{2}[1-9]{1}[0-9]{1}|0\.[0-9]{1}[1-9]{1}[0-9]{2}|[1-9]{1}[0-9]{0,10}(\.[0-9]{4})?';
  ER21 = '0|0\.[0-9]{3}|[1-9]{1}[0-9]{0,11}(\.[0-9]{3})?';
  ER22 = '0\.[1-9]{1}[0-9]{2}|0\.[0-9]{2}[1-9]{1}|0\.[0-9]{1}[1-9]{1}[0-9]{1}|[1-9]{1}[0-9]{0,11}(\.[0-9]{3})?';
  ER23 = '0|0\.[0-9]{4}|[1-9]{1}[0-9]{0,11}(\.[0-9]{4})?';
  ER24 = '0\.[1-9]{1}[0-9]{3}|0\.[0-9]{3}[1-9]{1}|0\.[0-9]{2}[1-9]{1}[0-9]{1}|0\.[0-9]{1}[1-9]{1}[0-9]{2}|[1-9]{1}[0-9]{0,11}(\.[0-9]{4})?';
  ER25 = '0|0\.[0-9]{2}|[1-9]{1}[0-9]{0,12}(\.[0-9]{2})?';
  ER26 = '0\.[0-9]{1}[1-9]{1}|0\.[1-9]{1}[0-9]{1}|[1-9]{1}[0-9]{0,12}(\.[0-9]{2})?';
  ER27 = '[0-9]{0,14}|ISENTO|PR[0-9]{4,8}';
  ER28 = '[0-9]{2,14}';
  ER29 = '[1-9]{1}[0-9]{0,8}';
  ER30 = '0|[1-9]{1}[0-9]{0,2}';
  ER31 = '[0-9]{2}';
  ER32 = '[0-9]{1,4}';
  ER33 = '[!-�]{1}[ -�]{0,}[!-�]{1}|[!-�]{1}';
  ER34 = '(((20(([02468][048])|([13579][26]))-02-29))|(20[0-9][0-9])-((((0[1-9])|(1[0-2]))-((0[1-9])|(1\d)|(2[0-8])))|((((0[13578])|(1[02]))-31)|(((0[1,3-9])|(1[0-2]))-(29|30)))))';
  ER35 = '[0-9]\.[0-9]{6}|[1-8][0-9]\.[0-9]{6}|90\.[0-9]{6}|-[0-9]\.[0-9]{6}|-[1-8][0-9]\.[0-9]{6}|-90\.[0-9]{6}';
  ER36 = '[0-9]\.[0-9]{6}|[1-9][0-9]\.[0-9]{6}|1[0-7][0-9]\.[0-9]{6}|180\.[0-9]{6}|-[0-9]\.[0-9]{6}|-[1-9][0-9]\.[0-9]{6}|-1[0-7][0-9]\.[0-9]{6}|-180\.[0-9]{6}';
  ER37 = '[A-Z]{2,3}[0-9]{4}|[A-Z]{3,4}[0-9]{3}';
  ER38 = '[0-9]{8}';
  ER39 = '[0-9]{1}';
  ER40 = '(((20(([02468][048])|([13579][26]))-02-29))|(20[0-9][0-9])-((((0[1-9])|(1[0-2]))-((0[1-9])|(1\d)|(2[0-8])))|((((0[13578])|(1[02]))-31)|(((0[1,3-9])|(1[0-2]))-(29|30)))))T(20|21|22|23|[0-1]\d):[0-5]\d:[0-5]\d';
  ER41 = '1\.[0-9]{2}';
  ER42 = '2000|1[0-9]{3}|[1-9][0-9]{2}|[1-9][0-9]|[1-9]';
  ER43 = 'MDFe[0-9]{44}';
  ER44 = '[0-9]{7,12}';
  ER45 = '[0-9]{7,10}';
  ER46 = '[0-9]{1,15}';
  ER47 = '1\.00';
  ER48 = '(([0-1][0-9])|([2][0-3])):([0-5][0-9]):([0-5][0-9])';
  ER49 = '[1-9]{1}[0-9]{1,8}';
  ER50 = '[A-Z0-9]+';
  ER51 = '[^@]+@[^\.]+\..+';
  // Erro no manual!!!
  ER52 = '';
  ER53 = '0|[1-9]{1}[0-9]{0,5}';
  //ER53 = '[0-9]{20}';
  // FIM - Erro no manual!!!
var
  Lista: Integer;
  Letra, Nums, Msg: String;
  Data: TDateTime;
begin
  try
  if Trim(ExpressaoRegular) = '' then
  begin
    Result := True;
    Exit;
  end else
    Result := False;
  //if AnsiMatchStr(Valor, D1) then
  Lista := AnsiIndexStr(ExpressaoRegular, RegExps);
  if Lista > -1 then
  begin
    case Lista of
      //Bool: ExecRegExpr('\d{3}-(\d{2}-\d{2}|\d{4})', edTextWithPhone.Text);
       0: Result := ExecRegExpr(ER0, Valor);
       1: Result := ExecRegExpr(ER1, Valor);
       2: Result := ExecRegExpr(ER2, Valor);
       3: Result := ExecRegExpr(ER3, Valor);
       4: Result := ExecRegExpr(ER4, Valor);
       5: Result := ExecRegExpr(ER5, Valor);
       6: Result := ExecRegExpr(ER6, Valor);
       7: Result := ExecRegExpr(ER7, Valor);
       8: Result := ExecRegExpr(ER8, Valor);
       9: Result := ExecRegExpr(ER9, Valor);
      //10: Result := ExecRegExpr(ER10, Valor);
      10:
      begin
        Data := Geral.ValidaData_YYYY_MM_DD(Valor);
        Result := Data > 2;
      end;
      11: Result := ExecRegExpr(ER11, Valor);
      12: Result := ExecRegExpr(ER12, Valor);
      13: Result := ExecRegExpr(ER13, Valor);
      14: Result := ExecRegExpr(ER14, Valor);
      15: Result := ExecRegExpr(ER15, Valor);
      16: Result := ExecRegExpr(ER16, Valor);
      17: Result := ExecRegExpr(ER17, Valor);
      18: Result := ExecRegExpr(ER18, Valor);
      19: Result := ExecRegExpr(ER19, Valor);
      20: Result := ExecRegExpr(ER20, Valor);
      21: Result := ExecRegExpr(ER21, Valor);
      22: Result := ExecRegExpr(ER22, Valor);
      23: Result := ExecRegExpr(ER23, Valor);
      24: Result := ExecRegExpr(ER24, Valor);
      25: Result := ExecRegExpr(ER25, Valor);
      26: Result := ExecRegExpr(ER26, Valor);
      27: Result := ExecRegExpr(ER27, Valor);
      28: Result := ExecRegExpr(ER28, Valor);
      29: Result := ExecRegExpr(ER29, Valor);
      30: Result := ExecRegExpr(ER30, Valor);
      31: Result := ExecRegExpr(ER31, Valor);
      32: Result := ExecRegExpr(ER32, Valor);
      33: Result := ExecRegExpr(ER33, Valor);
      34: Result := ExecRegExpr(ER34, Valor);
      35: Result := ExecRegExpr(ER35, Valor);
      36: Result := ExecRegExpr(ER36, Valor);
      37: Result := ExecRegExpr(ER37, Valor);
      38: Result := ExecRegExpr(ER38, Valor);
      39: Result := ExecRegExpr(ER39, Valor);
      40: Result := ExecRegExpr(ER40, Valor);
      41: Result := ExecRegExpr(ER41, Valor);
      42: Result := ExecRegExpr(ER42, Valor);
      43: Result := ExecRegExpr(ER43, Valor);
      44: Result := ExecRegExpr(ER44, Valor);
      45: Result := ExecRegExpr(ER45, Valor);
      46: Result := ExecRegExpr(ER46, Valor);
      47: Result := ExecRegExpr(ER47, Valor);
      48: Result := ExecRegExpr(ER48, Valor);
      49: Result := ExecRegExpr(ER49, Valor);
      50: Result := ExecRegExpr(ER50, Valor);
      51: Result := ExecRegExpr(ER51, Valor);
      // Erro no manual!!
      53: Result := ExecRegExpr(ER53, Valor);
      //
      else Geral.MB_Aviso('Express�o regular "' + ExpressaoRegular + '" inv�lida [1]!');
    end;
  end else
    Geral.MB_Aviso('Express�o regular "' + ExpressaoRegular + '" inv�lida [2]!');
  if not Result then
  begin
    Msg := 'MDF-e item #' + Geral.FF0(Grupo) + '.' + Geral.FF0(Codigo) +
    ' campo "' + Campo + '": Valor inv�lido: "' + Valor +
    '" na Express�o Regular: ' + ExpressaoRegular + sLineBreak;
    Geral.MB_Aviso(Msg);
    AllMsg := AllMsg + Msg;
  end;
  except
    on E: Exception do
    begin
      Geral.MB_Erro(E.Message + sLineBreak +
      'Express�o regular: ' + ExpressaoRegular + sLineBreak +
      'Grupo:' + Geral.FF0(Grupo) + sLineBreak +
      'Codigo:' + Geral.FF0(Codigo) + sLineBreak +
      'Grupo:' + Geral.FF0(Grupo) + sLineBreak +
      'Campo:' + Campo + sLineBreak +
      'Valor:' + Valor + sLineBreak +
      '');
    end else
    begin
      Geral.MB_Erro('Express�o regular: ' + ExpressaoRegular + sLineBreak +
      'Grupo:' + Geral.FF0(Grupo) + sLineBreak +
      'Codigo:' + Geral.FF0(Codigo) + sLineBreak +
      'Grupo:' + Geral.FF0(Grupo) + sLineBreak +
      'Campo:' + Campo + sLineBreak +
      'Valor:' + Valor + sLineBreak +
      '');
      raise;
    end;
  end;
end;

end.
