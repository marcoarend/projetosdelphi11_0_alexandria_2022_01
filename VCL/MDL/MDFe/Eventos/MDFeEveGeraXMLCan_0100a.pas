unit MDFeEveGeraXMLCan_0100a;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, UnMLAGeral, Variants, Math,
  IBCustomDataSet, IBQuery, Clipbrd, DB, DBClient, dmkGeral, UrlMon,
  InvokeRegistry, SOAPHTTPClient, CAPICOM_TLB, SOAPHTTPTrans, JwaWinCrypt,
  WinInet, XMLDoc, xmldom, XMLIntf, msxmldom, MSXML2_TLB, UnDmkProcFunc,
  UnMDFe_PF, UnXXe_PF,
  // Cancelamento
  evCancMDFe_v100, eventoMDFe_v100;

const
 { TODO : Colocar aqui os stat da Can }
{
  MDFe_AllModelos      = '55';
  MDFe_CodAutorizaTxt  = '100';
  MDFe_CodCanceladTxt  = '101';
  MDFe_CodInutilizTxt  = '102';
  MDFe_CodDenegadoTxt  = '110,301,302';
}
  // XML
  sXML_Version        = '1.0';
  sXML_Encoding       = 'UTF-8';
  ENCODING_UTF8 = '?xml version="1.0" encoding="UTF-8"?';
  ENCODING_UTF8_STD = '?xml version="1.0" encoding="UTF-8" standalone="no"?';
  NAME_SPACE = 'xmlns="http://www.portalfiscal.inf.br/mdfe"';
  sCampoNulo = '#NULL#';
  ///
  ///
  ///
  verEventosCan_Layout        = '1.00'; // Evento - Cancelamento da MDFe - Vers�o do layout
  verEventosCan_Evento        = '1.00'; // Evento - Cancelamento da MDFe - Vers�o do Evento
  verEventosCan_Cancel        = '1.00'; // Evento - Cancelamento da MDFe - Vers�o do Cancelamento

type
  TMDFeEveGeraXMLCan_0100a = class(TObject)
  private
    {private declaration}
     function  GeraXML_Can(const FatID, FatNum, Empresa: Integer; nProt,
               xJust: String; const LaAviso1, LaAviso2: TLabel;
               var XML: String): Boolean;
  public
    {public declaration}
    function  CriarDocumentoEveCan(const FatID, FatNum, Empresa:
              Integer; const Id: String; const cOrgao, tpAmb, Tipo: Integer;
              const CNPJ, CPF, chMDFe: String;
              const dhEvento: TDateTime; const TZD_UTC: Double;
              const tpEvento, nSeqEvento: Integer; verEvento: Double;
              const versao: Double; descEvento, nProt, xJust: String;
              var XMLAssinado: String;
              const LaAviso1, LaAviso2: TLabel): Boolean;
  end;

var
  UnMDFeEveGeraXMLCan_0100a: TMDFeEveGeraXMLCan_0100a;

implementation

uses ModuleMDFe_0000;

var
  //  ACBr
  //CertStore     : IStore3;
  //CertStoreMem  : IStore3;
  //PrivateKey    : IPrivateKey;
  //Certs         : ICertificates2;
  Cert          : ICertificate2;
  //NumCertCarregado : String;
  // XML
  EveXML: IXMLTEvento;
  CanXML: IXMLEvCancMDFe;
  arqXML: TXMLDocument;
  //
  FLaAviso1: TLabel;
  FLaAviso2: TLabel;
  FFatID, FFatNum, FEmpresa: Integer;

{ TEveGeraXMLCan_0100a }

function TMDFeEveGeraXMLCan_0100a.CriarDocumentoEveCan(const FatID, FatNum,
  Empresa: Integer; const Id: String; const cOrgao, tpAmb, Tipo: Integer;
  const CNPJ, CPF, chMDFe: String; const dhEvento: TDateTime;
  const TZD_UTC: Double; const tpEvento, nSeqEvento: Integer; verEvento: Double;
  const versao: Double; descEvento, nProt, xJust: String;
  var XMLAssinado: String; const LaAviso1, LaAviso2: TLabel): Boolean;
var
  //strChaveAcesso,
  NumeroSerial: String;
  //DadosTxt,
  Texto, XMLAny: String;
  AnyNode: IXMLNode;
begin
  Result := False;
  FLaAviso1 := LaAviso1;
  FLaAviso2 := LaAviso2;
  //
  FFatID   := FatID;
  FFatNum  := FatNum;
  FEmpresa := Empresa;
  //
  if GeraXML_Can(FatID, FatNum, Empresa, nProt, xJust, LaAviso1, LaAviso2,
  XMLAny) then
  begin
    (* Criando o Documento XML e Gravando cabe�alho... *)
    arqXML := TXMLDocument.Create(nil);
    arqXML.Active := False;
    arqXML.FileName := '';
    EveXML := GetEvento(arqXML);
    arqXML.Version := sXML_Version;
    arqXML.Encoding := sXML_Encoding;
    (*
    arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/1000/09/xmldsig#';
    arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/1001/XMLSchema-instance';
    arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviMDFe_v1.12.xsd';
    *)

    EveXML.versao := verEventosCan_Layout;
    EveXML.InfEvento.Id := Id;
    EveXML.InfEvento.COrgao := Geral.FFN(cOrgao, 2);
    EveXML.InfEvento.TpAmb := Geral.FF0(tpAmb);
    EveXML.InfEvento.CNPJ := Geral.SoNumero_TT(CNPJ);
    EveXML.InfEvento.ChMDFe := Geral.SoNumero_TT(chMDFe);
    //EveXML.InfEvento.DhEvento := dmkPF.FDT_XXe_UTC(dhEvento, TZD_UTC);
    EveXML.InfEvento.DhEvento := FormatDateTime('YYYY-MM-DD"T"HH:NN:SS', dhEvento);
    EveXML.InfEvento.TpEvento := Geral.FF0(tpEvento);
    EveXML.InfEvento.NSeqEvento := Geral.FF0(nSeqEvento);
    //EveXML.InfEvento.VerEvento := verEventosCan_Evento;
    EveXML.InfEvento.DetEvento.VersaoEvento := verEventosCan_Cancel;
    AnyNode := EveXML.InfEvento.DetEvento.AddChild('_');
    AnyNode.Text := '';
(*
    EveXML.InfEvento.DetEvento.DescEvento := descEvento;
    EveXML.InfEvento.DetEvento.NProt := nProt;
    EveXML.InfEvento.DetEvento.XJust := xJust;
*)
    //

    Texto := EveXML.XML;
    Texto := Geral.Substitui(Texto, '<_></_>', XMLAny);
    Texto := StringReplace(Texto, #10, '', [rfReplaceAll] );
    Texto := StringReplace(Texto, #13, '', [rfReplaceAll] );

    // Assinar!
    NumeroSerial := DmMDFe_0000.QrFilialMDFeSerNum.Value;
    if XXe_PF.ObtemCertificado(NumeroSerial, Cert) then
      Result := MDFe_PF.AssinarMSXML(Texto, Cert, XMLAssinado);
    arqXML := nil;
  end else
    Geral.MB_Erro('N�o foi possivel gerar o XML any!');
end;

function TMDFeEveGeraXMLCan_0100a.GeraXML_Can(const FatID, FatNum, Empresa: Integer;
  nProt, xJust: String; const LaAviso1, LaAviso2: TLabel; var XML: String): Boolean;
var
  //strChaveAcesso,
  NumeroSerial: String;
  //DadosTxt,
  Texto: String;
begin
  Result := False;
  FLaAviso1 := LaAviso1;
  FLaAviso2 := LaAviso2;
  //
  FFatID   := FatID;
  FFatNum  := FatNum;
  FEmpresa := Empresa;
  //
(* Criando o Documento XML e Gravando cabe�alho... *)
  arqXML := TXMLDocument.Create(nil);
  arqXML.Active := False;
  arqXML.FileName := '';
  CanXML := GetevCancMDFe(arqXML);
  arqXML.Version := sXML_Version;
  arqXML.Encoding := sXML_Encoding;
  (*
  arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/1000/09/xmldsig#';
  arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/1001/XMLSchema-instance';
  arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/mdfe/enviMDFe_v1.12.xsd';
  *)

  //CanXML.versao := verEventosCan_Layout;
  CanXML.DescEvento := 'Cancelamento';
  CanXML.NProt := nProt;
  CanXML.XJust := xJust;
  //
  Texto := CanXML.XML;
  Texto := StringReplace(Texto, #10, '', [rfReplaceAll] );
  Texto := StringReplace(Texto, #13, '', [rfReplaceAll] );

  //modalXML := rodoXml.XML.Text;
  XML := Geral.Substitui(Texto, ' xmlns="http://www.portalfiscal.inf.br/mdfe"', '');
  XML := Geral.Substitui(Texto, slineBreak, '');
  //
(*
  // Assinar!
  NumeroSerial := DmMDFe_0000.QrFilialMDFeSerNum.Value;
  if XXe_PF.ObtemCertificado(NumeroSerial, Cert) then
    Result := MDFe_PF.AssinarMSXML(Texto, Cert, XMLAssinado);
  arqXML := nil;
*)
  Result := True;
end;

end.
