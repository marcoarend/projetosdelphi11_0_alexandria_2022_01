unit MDFeEveRCan;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkRadioGroup, dmkMemo, UnMDFe_PF, UnDmkEnums, UnXXe_PF;

type
  TFmMDFeEveRCan = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Painel1: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrJust: TmySQLQuery;
    QrJustCodigo: TIntegerField;
    QrJustNome: TWideStringField;
    QrJustCodUsu: TIntegerField;
    QrJustAplicacao: TIntegerField;
    DsJust: TDataSource;
    GroupBox2: TGroupBox;
    PnJustificativa: TPanel;
    Label7: TLabel;
    EdJust: TdmkEditCB;
    CBJust: TdmkDBLookupComboBox;
    Panel2: TPanel;
    Label1: TLabel;
    Edide_serie: TdmkEdit;
    Label2: TLabel;
    Edide_nMDF: TdmkEdit;
    Label3: TLabel;
    EdnProt: TdmkEdit;
    Memo1: TMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdJustChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmMDFeEveRCan: TFmMDFeEveRCan;

implementation

uses UnMyObjects, Module, UnMDFe_Tabs, MDFeEveRCab, ModuleGeral, UMySQLModule,
  MDFeEveGeraXMLCan_0100a, ModuleMDFe_0000;

{$R *.DFM}

procedure TFmMDFeEveRCan.BtOKClick(Sender: TObject);
var
  Destino, Dir, nProt, xJust: String;
  Status, FatID, FatNum, Empresa, Controle, Conta, nJust: Integer;
  // XML
  XML_Eve: String;
begin
  if not DmMDFe_0000.ObtemDirXML(MDFE_EXT_EVE_ENV_CAN_XML, Dir, True) then
    Exit;
  if ImgTipo.SQLType = stIns then
  begin
    if not FmMDFeEveRCab.IncluiMDFeEveRCab(0, evexxe110111Can, FatID, FatNum, Empresa,
    Controle) then
    begin
      Geral.MensagemBox('ERRO ao incluir o cabe�alho gen�rico de evento!',
      'ERRO', MB_OK+MB_ICONERROR);
      Exit;
    end;
    Conta := DModG.BuscaProximoCodigoInt('mdfectrl', 'mdfeevercan', '', 0);
  end else begin
    FatID := FmMDFeEveRCab.QrMDFeEveRCabFatID.Value;
    FatNum := FmMDFeEveRCab.QrMDFeEveRCabFatNum.Value;
    Empresa := FmMDFeEveRCab.QrMDFeEveRCabEmpresa.Value;
    Controle := FmMDFeEveRCab.QrMDFeEveRCabControle.Value;
    Conta := FmMDFeEveRCab.QrMDFeEveRCanConta.Value;
  end;
  //
  nProt := EdnProt.Text;
  nJust := EdJust.ValueVariant;
  xJust :=  Trim(XXe_PF.ValidaTexto_XML(QrJustNome.Value, 'eve.can.xjust', 'eve.can.xjust'));
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'mdfeevercan', False, [
  'nProt', 'nJust', 'xJust'], [
  'FatID', 'FatNum', 'Empresa', 'Controle', 'Conta'], [
  nProt, nJust, xJust], [
  FatID, FatNum, Empresa, Controle, Conta], True) then
  begin
    if UnMDFeEveGeraXMLCan_0100a.CriarDocumentoEveCan(FatID, FatNum, Empresa,
    FmMDFeEveRCab.QrERCId.Value, FmMDFeEveRCab.QrERCcOrgao.Value,
    FmMDFeEveRCab.QrERCtpAmb.Value, FmMDFeEveRCab.QrERCTipoEnt.Value,
    FmMDFeEveRCab.QrERCCNPJ.Value, FmMDFeEveRCab.QrERCCPF.Value,
    FmMDFeEveRCab.QrERCchMDFe.Value, FmMDFeEveRCab.QrERCdhEvento.Value,
    FmMDFeEveRCab.QrERCTZD_UTC.Value, FmMDFeEveRCab.QrERCtpEvento.Value,
    FmMDFeEveRCab.QrERCnSeqEvento.Value, FmMDFeEveRCab.QrERCverEvento.Value,
    FmMDFeEveRCab.QrERCversao.Value, FmMDFeEveRCab.QrERCdescEvento.Value,
    nProt, xJust,
    XML_Eve, LaAviso1, LaAviso2) then
    begin
      Destino := DmMDFe_0000.SalvaXML(MDFE_EXT_EVE_ENV_CAN_XML,
      FmMDFeEveRCab.QrERCId.Value, XML_Eve, Memo1, True);
      if FileExists(Destino) then
      begin
        Status := Integer(mdfemystatusMDFeAssinada);
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfeevercab', False, [
        'XML_Eve', 'Status'], [
        'FatID', 'FatNum', 'Empresa', 'Controle'], [
        Geral.WideStringToSQLString(XML_Eve), Status], [
        FatID, FatNum, Empresa, Controle], True) then
        begin
          FmMDFeEveRCab.ReopenMDFeEveRCab(Controle);
          //
          Empresa := FmMDFeEveRCab.QrMDFeEveRCabEmpresa.Value;
          MDFe_PF.MostraFormStepsMDFe_Cancelamento(Empresa,
          Edide_Serie.ValueVariant, Edide_nMDF.ValueVariant,
          FmMDFeEveRCab.QrERCnSeqEvento.Value, EdJust.ValueVariant,
          MDFe_AllModelos, EdnProt.ValueVariant, FmMDFeEveRCab.QrERCchMDFe.Value,
          XML_Eve, FmMDFeEveRCab.QrA);
        end;
      end;
    end;
    //
    FmMDFeEveRCab.ReopenMDFeEveRCan(0);
    Close;
  end;
end;

procedure TFmMDFeEveRCan.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMDFeEveRCan.EdJustChange(Sender: TObject);
begin
  BtOK.Enabled := EdJust.ValueVariant;
end;

procedure TFmMDFeEveRCan.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMDFeEveRCan.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  UMyMod.AbreQuery(QrJust, Dmod.MyDB);
end;

procedure TFmMDFeEveRCan.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
