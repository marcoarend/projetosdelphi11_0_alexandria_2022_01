object FmMDFeEveREnc: TFmMDFeEveREnc
  Left = 339
  Top = 185
  Caption = 'MDF-EVENT-002 :: Encerramento de MDF-e'
  ClientHeight = 361
  ClientWidth = 555
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 555
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 507
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 459
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 298
        Height = 32
        Caption = 'Encerramento de MDF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 298
        Height = 32
        Caption = 'Encerramento de MDF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 298
        Height = 32
        Caption = 'Encerramento de MDF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Painel1: TPanel
    Left = 0
    Top = 48
    Width = 555
    Height = 199
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 143
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 555
      Height = 199
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitHeight = 143
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 555
        Height = 105
        Align = alTop
        Caption = ' Dados do MDF-e: '
        Enabled = False
        TabOrder = 0
        object Panel2: TPanel
          Left = 2
          Top = 15
          Width = 283
          Height = 88
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitHeight = 108
          object Label1: TLabel
            Left = 8
            Top = 8
            Width = 27
            Height = 13
            Caption = 'S'#233'rie:'
          end
          object Label2: TLabel
            Left = 48
            Top = 8
            Width = 41
            Height = 13
            Caption = 'N'#186' MDF:'
          end
          object Label3: TLabel
            Left = 132
            Top = 8
            Width = 121
            Height = 13
            Caption = 'Protocolo de autoriza'#231#227'o:'
          end
          object Label4: TLabel
            Left = 8
            Top = 48
            Width = 69
            Height = 13
            Caption = 'Chave MDF-e:'
          end
          object Edide_serie: TdmkEdit
            Left = 8
            Top = 24
            Width = 37
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object Edide_nMDF: TdmkEdit
            Left = 48
            Top = 24
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdnProt: TdmkEdit
            Left = 132
            Top = 24
            Width = 128
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdchMDFe: TdmkEdit
            Left = 8
            Top = 64
            Width = 272
            Height = 21
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object Memo1: TMemo
          Left = 285
          Top = 15
          Width = 268
          Height = 88
          Align = alClient
          TabOrder = 1
          ExplicitLeft = 352
          ExplicitTop = 36
          ExplicitWidth = 185
          ExplicitHeight = 89
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 105
        Width = 555
        Height = 94
        Align = alClient
        Caption = ' Cancelamento: '
        TabOrder = 1
        ExplicitTop = 73
        ExplicitHeight = 70
        object PnJustificativa: TPanel
          Left = 2
          Top = 15
          Width = 551
          Height = 77
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitHeight = 53
          object Label24: TLabel
            Left = 9
            Top = 8
            Width = 17
            Height = 13
            Caption = 'UF:'
          end
          object Label105: TLabel
            Left = 44
            Top = 8
            Width = 36
            Height = 13
            Caption = 'Cidade:'
          end
          object Label36: TLabel
            Left = 416
            Top = 8
            Width = 109
            Height = 13
            Caption = 'Data do encerramento:'
          end
          object EdcUF: TdmkEdit
            Left = 9
            Top = 24
            Width = 28
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtUF
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnRedefinido = EdcUFRedefinido
          end
          object EdcMun: TdmkEditCB
            Left = 44
            Top = 24
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBcMun
            IgnoraDBLookupComboBox = True
          end
          object CBcMun: TdmkDBLookupComboBox
            Left = 105
            Top = 24
            Width = 305
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsDTB_Munici
            TabOrder = 2
            dmkEditCB = EdcMun
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object TPdtEnc: TdmkEditDateTimePicker
            Left = 415
            Top = 24
            Width = 116
            Height = 21
            Date = 40137.793551006940000000
            Time = 40137.793551006940000000
            TabOrder = 3
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 247
    Width = 555
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 191
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 551
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 291
    Width = 555
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 235
    object PnSaiDesis: TPanel
      Left = 409
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 407
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrDTB_Munici: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM dtb_munici'
      'ORDER BY Nome')
    Left = 468
    Top = 124
    object QrDTB_MuniciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDTB_MuniciNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsDTB_Munici: TDataSource
    DataSet = QrDTB_Munici
    Left = 468
    Top = 168
  end
end
