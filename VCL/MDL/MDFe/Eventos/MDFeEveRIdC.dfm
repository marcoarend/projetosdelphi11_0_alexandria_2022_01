object FmMDFeEveRIdC: TFmMDFeEveRIdC
  Left = 339
  Top = 185
  Caption = 'MDF-EVENT-004 :: Inclus'#227'o de Condutor em MDF-e'
  ClientHeight = 373
  ClientWidth = 538
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 538
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 555
    object GB_R: TGroupBox
      Left = 490
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 507
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 442
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 459
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 394
        Height = 32
        Caption = 'Inclus'#227'o de Condutor em MDF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 394
        Height = 32
        Caption = 'Inclus'#227'o de Condutor em MDF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 394
        Height = 32
        Caption = 'Inclus'#227'o de Condutor em MDF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Painel1: TPanel
    Left = 0
    Top = 48
    Width = 538
    Height = 211
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 555
    ExplicitHeight = 199
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 538
      Height = 211
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 555
      ExplicitHeight = 199
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 538
        Height = 105
        Align = alTop
        Caption = ' Dados do MDF-e: '
        Enabled = False
        TabOrder = 0
        ExplicitWidth = 555
        object Panel2: TPanel
          Left = 2
          Top = 15
          Width = 283
          Height = 88
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 8
            Width = 27
            Height = 13
            Caption = 'S'#233'rie:'
          end
          object Label2: TLabel
            Left = 48
            Top = 8
            Width = 41
            Height = 13
            Caption = 'N'#186' MDF:'
          end
          object Label3: TLabel
            Left = 132
            Top = 8
            Width = 121
            Height = 13
            Caption = 'Protocolo de autoriza'#231#227'o:'
          end
          object Label4: TLabel
            Left = 8
            Top = 48
            Width = 69
            Height = 13
            Caption = 'Chave MDF-e:'
          end
          object Edide_serie: TdmkEdit
            Left = 8
            Top = 24
            Width = 37
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object Edide_nMDF: TdmkEdit
            Left = 48
            Top = 24
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdnProt: TdmkEdit
            Left = 132
            Top = 24
            Width = 128
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdchMDFe: TdmkEdit
            Left = 8
            Top = 64
            Width = 272
            Height = 21
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object Memo1: TMemo
          Left = 285
          Top = 15
          Width = 251
          Height = 88
          Align = alClient
          TabOrder = 1
          ExplicitWidth = 268
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 105
        Width = 538
        Height = 106
        Align = alClient
        Caption = ' Cancelamento: '
        TabOrder = 1
        ExplicitWidth = 555
        ExplicitHeight = 94
        object PnJustificativa: TPanel
          Left = 2
          Top = 15
          Width = 534
          Height = 89
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitWidth = 551
          ExplicitHeight = 77
          object Label5: TLabel
            Left = 8
            Top = 4
            Width = 45
            Height = 13
            Caption = 'Entidade:'
          end
          object SpeedButton1: TSpeedButton
            Left = 500
            Top = 20
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton1Click
          end
          object LaxNome: TLabel
            Left = 8
            Top = 44
            Width = 31
            Height = 13
            Caption = 'Nome:'
          end
          object LaCPF: TLabel
            Left = 408
            Top = 44
            Width = 23
            Height = 13
            Caption = 'CPF:'
          end
          object EdEntidade: TdmkEditCB
            Left = 8
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Cargo'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnRedefinido = EdEntidadeRedefinido
            DBLookupComboBox = CBEntidade
            IgnoraDBLookupComboBox = False
          end
          object CBEntidade: TdmkDBLookupComboBox
            Left = 68
            Top = 20
            Width = 429
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsEntidades
            TabOrder = 1
            dmkEditCB = EdEntidade
            QryCampo = 'Cargo'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdxNome: TdmkEdit
            Left = 8
            Top = 60
            Width = 397
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'xNome'
            UpdCampo = 'xNome'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCPF: TdmkEdit
            Left = 408
            Top = 60
            Width = 113
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtCPFJ
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'CPF'
            UpdCampo = 'CPF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 259
    Width = 538
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 247
    ExplicitWidth = 555
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 534
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 551
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 303
    Width = 538
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 291
    ExplicitWidth = 555
    object PnSaiDesis: TPanel
      Left = 392
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 409
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 390
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 407
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, CPF '
      'FROM entidades'
      'WHERE Tipo=1'
      'ORDER BY Nome')
    Left = 376
    Top = 68
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNome: TWideStringField
      DisplayWidth = 100
      FieldName = 'Nome'
      Size = 100
    end
    object QrEntidadesCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 376
    Top = 116
  end
end
