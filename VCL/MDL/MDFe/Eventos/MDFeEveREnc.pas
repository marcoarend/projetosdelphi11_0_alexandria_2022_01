unit MDFeEveREnc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkRadioGroup, dmkMemo, UnMDFe_PF, UnDmkEnums, UnXXe_PF;

type
  TFmMDFeEveREnc = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Painel1: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox2: TGroupBox;
    PnJustificativa: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    Edide_serie: TdmkEdit;
    Label2: TLabel;
    Edide_nMDF: TdmkEdit;
    Label3: TLabel;
    EdnProt: TdmkEdit;
    Label24: TLabel;
    EdcUF: TdmkEdit;
    EdcMun: TdmkEditCB;
    Label105: TLabel;
    CBcMun: TdmkDBLookupComboBox;
    QrDTB_Munici: TmySQLQuery;
    QrDTB_MuniciCodigo: TIntegerField;
    QrDTB_MuniciNome: TWideStringField;
    DsDTB_Munici: TDataSource;
    TPdtEnc: TdmkEditDateTimePicker;
    Label36: TLabel;
    Memo1: TMemo;
    EdchMDFe: TdmkEdit;
    Label4: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdJustChange(Sender: TObject);
    procedure EdcUFRedefinido(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmMDFeEveREnc: TFmMDFeEveREnc;

implementation

uses UnMyObjects, Module, UnMDFe_Tabs, MDFeEveRCab, ModuleGeral, UMySQLModule,
  MDFeEveGeraXMLEnc_0100a, ModuleMDFe_0000;

{$R *.DFM}

procedure TFmMDFeEveREnc.BtOKClick(Sender: TObject);
var
  Destino, Dir, nProt, dtEnc: String;
  Status, FatID, FatNum, Empresa, Controle, Conta, cUF, cMun: Integer;
  // XML
  XML_Eve: String;
  SQLType: TSQLType;
begin
  if not DmMDFe_0000.ObtemDirXML(MDFE_EXT_EVE_ENV_ENC_XML, Dir, True) then
    Exit;
  SQLType := ImgTipo.SQLType;
  nProt          := EdnProt.Text;
  dtEnc          := Geral.FDT(TPdtEnc.Date, 1);
  cUF            := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(EdcUF.ValueVariant);
  cMun           := EdcMun.ValueVariant;
  if MyObjects.FIC(TPdtEnc.Date < 2, TPdtEnc,
    'Informe a data de encerramento!') then Exit;
  if MyObjects.FIC(cUF = 0, EdCUF,
    'Informe a UF de encerramento!') then Exit;
  if MyObjects.FIC(cMun = 0, EdCMun,
    'Informe o munic�pio de encerramento!') then Exit;
  //
  if SQLType = stIns then
  begin
    if not FmMDFeEveRCab.IncluiMDFeEveRCab(0, TEventoXXe.evexxe110112Enc,
    FatID, FatNum, Empresa, Controle) then
    begin
      Geral.MB_Erro('ERRO ao incluir o cabe�alho gen�rico de evento!');
      Exit;
    end;
    Conta := DModG.BuscaProximoCodigoInt('mdfectrl', 'mdfeeverenc', '', 0);
  end else begin
    FatID := FmMDFeEveRCab.QrMDFeEveRCabFatID.Value;
    FatNum := FmMDFeEveRCab.QrMDFeEveRCabFatNum.Value;
    Empresa := FmMDFeEveRCab.QrMDFeEveRCabEmpresa.Value;
    Controle := FmMDFeEveRCab.QrMDFeEveRCabControle.Value;
    Conta := FmMDFeEveRCab.QrMDFeEveREncConta.Value;
  end;
  //

  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'mdfeeverenc', False, [
  'nProt', 'dtEnc', 'cUF',
  'cMun'], [
  'FatID', 'FatNum', 'Empresa', 'Controle', 'Conta'], [
  nProt, dtEnc, cUF,
  cMun], [
  FatID, FatNum, Empresa, Controle, Conta], True) then
  begin
    if UnMDFeEveGeraXMLEnc_0100a.CriarDocumentoEveEnc(FatID, FatNum, Empresa,
    FmMDFeEveRCab.QrERCId.Value, FmMDFeEveRCab.QrERCcOrgao.Value,
    FmMDFeEveRCab.QrERCtpAmb.Value, (*FmMDFeEveRCab.QrERCTipoEnt.Value,*)
    FmMDFeEveRCab.QrERCCNPJ.Value, (*FmMDFeEveRCab.QrERCCPF.Value,*)
    FmMDFeEveRCab.QrERCchMDFe.Value, FmMDFeEveRCab.QrERCdhEvento.Value,
    (*FmMDFeEveRCab.QrERCTZD_UTC.Value,*) FmMDFeEveRCab.QrERCtpEvento.Value,
    FmMDFeEveRCab.QrERCnSeqEvento.Value,
    nProt, dtEnc, cUF, cMun, XML_Eve, LaAviso1, LaAviso2) then
(*
    UnMDFeEveGeraXMLEnc_0100a.CriarDocumentoEveEnc(FatID, FatNum, Empresa,
    FmMDFeEveRCab.QrERCId.Value, FmMDFeEveRCab.QrERCcOrgao.Value,
    FmMDFeEveRCab.QrERCtpAmb.Value, FmMDFeEveRCab.QrERCTipoEnt.Value,
    FmMDFeEveRCab.QrERCCNPJ.Value, FmMDFeEveRCab.QrERCCPF.Value,
    FmMDFeEveRCab.QrERCchMDFe.Value, FmMDFeEveRCab.QrERCdhEvento.Value,
    FmMDFeEveRCab.QrERCTZD_UTC.Value, FmMDFeEveRCab.QrERCtpEvento.Value,
    FmMDFeEveRCab.QrERCnSeqEvento.Value, FmMDFeEveRCab.QrERCverEvento.Value,
    FmMDFeEveRCab.QrERCversao.Value, FmMDFeEveRCab.QrERCdescEvento.Value,
    nProt, xJust,
    XML_Eve, LaAviso1, LaAviso2) then
*)
    begin
      Destino := DmMDFe_0000.SalvaXML(MDFE_EXT_EVE_ENV_ENC_XML,
      FmMDFeEveRCab.QrERCId.Value, XML_Eve, Memo1, True);
      if FileExists(Destino) then
      begin
        Status := Integer(mdfemystatusMDFeAssinada);
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfeevercab', False, [
        'XML_Eve', 'Status'], [
        'FatID', 'FatNum', 'Empresa', 'Controle'], [
        Geral.WideStringToSQLString(XML_Eve), Status], [
        FatID, FatNum, Empresa, Controle], True) then
        begin
          FmMDFeEveRCab.ReopenMDFeEveRCab(Controle);
          //
          Empresa := FmMDFeEveRCab.QrMDFeEveRCabEmpresa.Value;
          MDFe_PF.MostraFormStepsMDFe_Encerramento(Empresa, nProt,
          EdchMDFe.Text, CBcMun.Text, TPdtEnc.Date, cUF, cMun, XML_Eve,
          FmMDFeEveRCab.QrA);
        end;
      end;
    end;
    //
    FmMDFeEveRCab.ReopenMDFeEveRCan(0);
    Close;
  end;
end;

procedure TFmMDFeEveREnc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMDFeEveREnc.EdcUFRedefinido(Sender: TObject);
begin
  DModG.ReopenMunici(QrDTB_Munici, EdcUF.ValueVariant);
end;

procedure TFmMDFeEveREnc.EdJustChange(Sender: TObject);
begin
  //BtOK.Enabled := EdJust.ValueVariant;
end;

procedure TFmMDFeEveREnc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMDFeEveREnc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
 //TPdtEnc.Date := DmodG.ObtemAgora();
end;

procedure TFmMDFeEveREnc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
