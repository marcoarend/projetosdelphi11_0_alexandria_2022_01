unit MDFeEveRIdC;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkRadioGroup, dmkMemo, UnMDFe_PF, UnDmkEnums, UnXXe_PF;

type
  TFmMDFeEveRIdC = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Painel1: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox2: TGroupBox;
    PnJustificativa: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    Edide_serie: TdmkEdit;
    Label2: TLabel;
    Edide_nMDF: TdmkEdit;
    Label3: TLabel;
    EdnProt: TdmkEdit;
    Memo1: TMemo;
    EdchMDFe: TdmkEdit;
    Label4: TLabel;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNome: TWideStringField;
    QrEntidadesCPF: TWideStringField;
    DsEntidades: TDataSource;
    Label5: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    LaxNome: TLabel;
    EdxNome: TdmkEdit;
    LaCPF: TLabel;
    EdCPF: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdJustChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdEntidadeRedefinido(Sender: TObject);
  private
    { Private declarations }
    FCriando: Boolean;
  public
    { Public declarations }
  end;

  var
  FmMDFeEveRIdC: TFmMDFeEveRIdC;

implementation

uses UnMyObjects, Module, UnMDFe_Tabs, MDFeEveRCab, ModuleGeral, UMySQLModule,
  MDFeEveGeraXMLIdC_0100a, ModuleMDFe_0000, DmkDAC_PF;

{$R *.DFM}

procedure TFmMDFeEveRIdC.BtOKClick(Sender: TObject);
var
  Destino, Dir, xNome, CPF: String;
  Status, FatID, FatNum, Empresa, Controle, Conta, Entidade: Integer;
  // XML
  XML_Eve: String;
  SQLType: TSQLType;
begin
  if not DmMDFe_0000.ObtemDirXML(MDFE_EXT_EVE_ENV_ENC_XML, Dir, True) then
    Exit;
  SQLType := ImgTipo.SQLType;
  Entidade       := EdEntidade.ValueVariant;
  xNome          := EdxNome.Text;
  CPF            := Geral.SoNumero_TT(EdCPF.Text);
  //
  if MyObjects.FIC(Trim(EdxNome.Text) = '', EdxNome, 'Informe o nome do condutor!') then
    Exit;
  if MyObjects.FIC(Trim(EdCPF.Text) = '', EdCPF, 'Informe o CPF do condutor!') then
    Exit;
  //
  //
  if SQLType = stIns then
  begin
    if not FmMDFeEveRCab.IncluiMDFeEveRCab(0, evexxe110114IncCondutor,
    FatID, FatNum, Empresa, Controle) then
    begin
      Geral.MB_Erro('ERRO ao incluir o cabe�alho gen�rico de evento!');
      Exit;
    end;
    Conta := DModG.BuscaProximoCodigoInt('mdfectrl', 'mdfeeveridc', '', 0);
  end else begin
    FatID := FmMDFeEveRCab.QrMDFeEveRCabFatID.Value;
    FatNum := FmMDFeEveRCab.QrMDFeEveRCabFatNum.Value;
    Empresa := FmMDFeEveRCab.QrMDFeEveRCabEmpresa.Value;
    Controle := FmMDFeEveRCab.QrMDFeEveRCabControle.Value;
    Conta := FmMDFeEveRCab.QrMDFeEveRIdCConta.Value;
  end;
  //

  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'mdfeeveridc', False, [
  'Entidade', 'xNome', 'CPF'], [
  'FatID', 'FatNum', 'Empresa', 'Controle', 'Conta'], [
  Entidade, xNome, CPF], [
  FatID, FatNum, Empresa, Controle, Conta], True) then
  begin
    if UnMDFeEveGeraXMLIdC_0100a.CriarDocumentoEveIdC(FatID, FatNum, Empresa,
    FmMDFeEveRCab.QrERCId.Value, FmMDFeEveRCab.QrERCcOrgao.Value,
    FmMDFeEveRCab.QrERCtpAmb.Value, (*FmMDFeEveRCab.QrERCTipoEnt.Value,*)
    FmMDFeEveRCab.QrERCCNPJ.Value, (*FmMDFeEveRCab.QrERCCPF.Value,*)
    FmMDFeEveRCab.QrERCchMDFe.Value, FmMDFeEveRCab.QrERCdhEvento.Value,
    (*FmMDFeEveRCab.QrERCTZD_UTC.Value,*) FmMDFeEveRCab.QrERCtpEvento.Value,
    FmMDFeEveRCab.QrERCnSeqEvento.Value,
   xNome, CPF, XML_Eve, LaAviso1, LaAviso2) then
    begin
      Destino := DmMDFe_0000.SalvaXML(MDFE_EXT_EVE_ENV_ENC_XML,
      FmMDFeEveRCab.QrERCId.Value, XML_Eve, Memo1, True);
      if FileExists(Destino) then
      begin
        Status := Integer(mdfemystatusMDFeAssinada);
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'mdfeevercab', False, [
        'XML_Eve', 'Status'], [
        'FatID', 'FatNum', 'Empresa', 'Controle'], [
        Geral.WideStringToSQLString(XML_Eve), Status], [
        FatID, FatNum, Empresa, Controle], True) then
        begin
          FmMDFeEveRCab.ReopenMDFeEveRCab(Controle);
          //
          Empresa := FmMDFeEveRCab.QrMDFeEveRCabEmpresa.Value;
          MDFe_PF.MostraFormStepsMDFe_IncCondutor(Empresa, EdnProt.Text,
          EdchMDFe.Text, xNome, CPF, XML_Eve, FmMDFeEveRCab.QrA);
        end;
      end;
    end;
    //
    FmMDFeEveRCab.ReopenMDFeEveRCan(0);
    Close;
  end;
end;

procedure TFmMDFeEveRIdC.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMDFeEveRIdC.EdEntidadeRedefinido(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := EdEntidade.ValueVariant = 0;
  //
  LaxNome.Enabled := Habilita;
  EdxNome.Enabled := Habilita;
  LaCPF.Enabled := Habilita;
  EdCPF.Enabled := Habilita;
  //
  if (not FCriando) then
  begin
    if not Habilita then
    begin
      EdxNome.Text := QrEntidadesNome.Value;
      EdCPF.ValueVariant := QrEntidadesCPF.Value;
    end;
  end;
end;

procedure TFmMDFeEveRIdC.EdJustChange(Sender: TObject);
begin
  //BtOK.Enabled := EdJust.ValueVariant;
end;

procedure TFmMDFeEveRIdC.FormActivate(Sender: TObject);
begin
  FCriando := False;
  MyObjects.CorIniComponente();
end;

procedure TFmMDFeEveRIdC.FormCreate(Sender: TObject);
begin
  FCriando := True;
  ImgTipo.SQLType := stPsq;
  //
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
end;

procedure TFmMDFeEveRIdC.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMDFeEveRIdC.SpeedButton1Click(Sender: TObject);
begin
  DModG.CadastroESelecaoDeEntidade(EdEntidade.ValueVariant, QrEntidades,
    EdEntidade, CBEntidade);
end;

end.
