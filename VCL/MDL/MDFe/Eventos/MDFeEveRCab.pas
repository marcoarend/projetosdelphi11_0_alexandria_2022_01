unit MDFeEveRCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkDBGrid, Menus, DmkDAC_PF, (*EveGeraXMLCCe_0200, evCCeMDFe_v200,
  EveGeraXMLCan_0200, evCancMDFe_v200,*) frxClass, frxDBSet, UnDmkProcFunc,
  frxBarcode, UnDmkEnums, UnXXe_PF, UnMDFe_PF;

type
  TFmMDFeEveRcab = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtEnc: TBitBtn;
    LaTitulo1C: TLabel;
    DsA: TDataSource;
    DBGrid1: TDBGrid;
    PMEnc: TPopupMenu;
    QrMDFeEveRCab: TmySQLQuery;
    DsMDFeEveRCab: TDataSource;
    QrMDFeEveRCabFatID: TIntegerField;
    QrMDFeEveRCabFatNum: TIntegerField;
    QrMDFeEveRCabEmpresa: TIntegerField;
    QrMDFeEveRCabControle: TIntegerField;
    QrMDFeEveRCabEventoLote: TIntegerField;
    QrMDFeEveRCabId: TWideStringField;
    QrMDFeEveRCabcOrgao: TSmallintField;
    QrMDFeEveRCabtpAmb: TSmallintField;
    QrMDFeEveRCabTipoEnt: TSmallintField;
    QrMDFeEveRCabCNPJ: TWideStringField;
    QrMDFeEveRCabCPF: TWideStringField;
    QrMDFeEveRCabchMDFe: TWideStringField;
    QrMDFeEveRCabdhEvento: TDateTimeField;
    QrMDFeEveRCabverEvento: TFloatField;
    QrMDFeEveRCabtpEvento: TIntegerField;
    QrMDFeEveRCabnSeqEvento: TSmallintField;
    QrMDFeEveRCabversao: TFloatField;
    QrMDFeEveRCabdescEvento: TWideStringField;
    QrMDFeEveRCabLk: TIntegerField;
    QrMDFeEveRCabDataCad: TDateField;
    QrMDFeEveRCabDataAlt: TDateField;
    QrMDFeEveRCabUserCad: TIntegerField;
    QrMDFeEveRCabUserAlt: TIntegerField;
    QrMDFeEveRCabAlterWeb: TSmallintField;
    QrMDFeEveRCabAtivo: TSmallintField;
    QrMDFeEveREnc: TmySQLQuery;
    DsMDFeEveREnc: TDataSource;
    DBGrid2: TDBGrid;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet0: TTabSheet;
    DBGrid4: TDBGrid;
    QrERC: TmySQLQuery;
    QrERCFatID: TIntegerField;
    QrERCFatNum: TIntegerField;
    QrERCEmpresa: TIntegerField;
    QrERCControle: TIntegerField;
    QrERCEventoLote: TIntegerField;
    QrERCId: TWideStringField;
    QrERCcOrgao: TSmallintField;
    QrERCtpAmb: TSmallintField;
    QrERCTipoEnt: TSmallintField;
    QrERCCNPJ: TWideStringField;
    QrERCCPF: TWideStringField;
    QrERCchMDFe: TWideStringField;
    QrERCdhEvento: TDateTimeField;
    QrERCverEvento: TFloatField;
    QrERCtpEvento: TIntegerField;
    QrERCnSeqEvento: TSmallintField;
    QrERCversao: TFloatField;
    QrERCdescEvento: TWideStringField;
    QrMDFeEveRCabTZD_UTC: TFloatField;
    QrERCTZD_UTC: TFloatField;
    Splitter1: TSplitter;
    QrMDFeEveRCabtpAmb_TXT: TWideStringField;
    QrMDFeEveRCabStatus: TIntegerField;
    BtCan: TBitBtn;
    PMCan: TPopupMenu;
    IncluicancelamentodaMDFe1: TMenuItem;
    ExcluicancelamentodaMDFe1: TMenuItem;
    QrMDFeEveRCan: TmySQLQuery;
    DsMDFeEveRCan: TDataSource;
    QrMDFeEveRCanFatID: TIntegerField;
    QrMDFeEveRCanFatNum: TIntegerField;
    QrMDFeEveRCanEmpresa: TIntegerField;
    QrMDFeEveRCanControle: TIntegerField;
    QrMDFeEveRCanConta: TIntegerField;
    QrMDFeEveRCannProt: TLargeintField;
    QrMDFeEveRCannJust: TIntegerField;
    QrMDFeEveRCanxJust: TWideStringField;
    QrMDFeEveRCanLk: TIntegerField;
    BtIdc: TBitBtn;
    PMIdC: TPopupMenu;
    QrMDFeEveRIdC: TmySQLQuery;
    DsMDFeEveRIdC: TDataSource;
    TabSheet3: TTabSheet;
    QrMDFeEveRCabXML_Eve: TWideMemoField;
    QrMDFeEveRCabXML_retEve: TWideMemoField;
    QrMDFeEveRCabret_versao: TFloatField;
    QrMDFeEveRCabret_Id: TWideStringField;
    QrMDFeEveRCabret_tpAmb: TSmallintField;
    QrMDFeEveRCabret_verAplic: TWideStringField;
    QrMDFeEveRCabret_cOrgao: TSmallintField;
    QrMDFeEveRCabret_cStat: TIntegerField;
    QrMDFeEveRCabret_xMotivo: TWideStringField;
    QrMDFeEveRCabret_chMDFe: TWideStringField;
    QrMDFeEveRCabret_tpEvento: TIntegerField;
    QrMDFeEveRCabret_xEvento: TWideStringField;
    QrMDFeEveRCabret_nSeqEvento: TIntegerField;
    QrMDFeEveRCabret_CNPJDest: TWideStringField;
    QrMDFeEveRCabret_CPFDest: TWideStringField;
    QrMDFeEveRCabret_emailDest: TWideStringField;
    QrMDFeEveRCabret_dhRegEvento: TDateTimeField;
    QrMDFeEveRCabret_TZD_UTC: TFloatField;
    QrMDFeEveRCabret_nProt: TWideStringField;
    BtLote: TBitBtn;
    QrA: TmySQLQuery;
    QrAFatID: TIntegerField;
    QrAFatNum: TIntegerField;
    QrALoteEnv: TIntegerField;
    QrAId: TWideStringField;
    QrAEmpresa: TIntegerField;
    QrAide_serie: TIntegerField;
    QrAide_nMDF: TIntegerField;
    QrAide_dEmi: TDateField;
    QrAnProt: TWideStringField;
    QrAxMotivo: TWideStringField;
    QrAdhRecbto: TWideStringField;
    QrAIDCtrl: TIntegerField;
    QrAversao: TFloatField;
    QrAide_tpEmis: TSmallintField;
    QrANOME_tpEmis: TWideStringField;
    QrANOME_tpMDFe: TWideStringField;
    QrAinfCanc_xJust: TWideStringField;
    QrAStatus: TIntegerField;
    QrAinfCanc_dhRecbto: TDateTimeField;
    QrAinfCanc_nProt: TWideStringField;
    QrAcStat: TFloatField;
    QrAcSitMDFe: TSmallintField;
    QrAcSitEnc: TSmallintField;
    QrANO_cSitEnc: TWideStringField;
    QrANO_Emi: TWideStringField;
    QrAide_Modal: TSmallintField;
    QrACodInfoEmite: TIntegerField;
    N2: TMenuItem;
    Solicitacancelamento1: TMenuItem;
    QrOpcoesMDFe: TmySQLQuery;
    QrOpcoesMDFeMDFeversao: TFloatField;
    QrOpcoesMDFeMDFeide_mod: TSmallintField;
    QrOpcoesMDFeSimplesFed: TSmallintField;
    QrOpcoesMDFeDirMDFeGer: TWideStringField;
    QrOpcoesMDFeDirMDFeAss: TWideStringField;
    QrOpcoesMDFeDirMDFeEnvLot: TWideStringField;
    QrOpcoesMDFeDirMDFeRec: TWideStringField;
    QrOpcoesMDFeDirMDFePedRec: TWideStringField;
    QrOpcoesMDFeDirMDFeProRec: TWideStringField;
    QrOpcoesMDFeDirMDFeDen: TWideStringField;
    QrOpcoesMDFeDirMDFePedCan: TWideStringField;
    QrOpcoesMDFeDirMDFeCan: TWideStringField;
    QrOpcoesMDFeDirMDFePedInu: TWideStringField;
    QrOpcoesMDFeDirMDFeInu: TWideStringField;
    QrOpcoesMDFeDirMDFePedSit: TWideStringField;
    QrOpcoesMDFeDirMDFeSit: TWideStringField;
    QrOpcoesMDFeDirMDFePedSta: TWideStringField;
    QrOpcoesMDFeDirMDFeSta: TWideStringField;
    QrOpcoesMDFeMDFeUF_WebServ: TWideStringField;
    QrOpcoesMDFeMDFeUF_Servico: TWideStringField;
    QrOpcoesMDFePathLogoMDFe: TWideStringField;
    QrOpcoesMDFeCtaFretPrest: TIntegerField;
    QrOpcoesMDFeTxtFretPrest: TWideStringField;
    QrOpcoesMDFeDupFretPrest: TWideStringField;
    QrOpcoesMDFeMDFeSerNum: TWideStringField;
    QrOpcoesMDFeMDFeSerVal: TDateField;
    QrOpcoesMDFeMDFeSerAvi: TSmallintField;
    QrOpcoesMDFeDirDAMDFes: TWideStringField;
    QrOpcoesMDFeDirMDFeProt: TWideStringField;
    QrOpcoesMDFeMDFePreMailAut: TIntegerField;
    QrOpcoesMDFeMDFePreMailCan: TIntegerField;
    QrOpcoesMDFeMDFePreMailEveCCe: TIntegerField;
    QrOpcoesMDFeMDFeVerStaSer: TFloatField;
    QrOpcoesMDFeMDFeVerEnvLot: TFloatField;
    QrOpcoesMDFeMDFeVerConLot: TFloatField;
    QrOpcoesMDFeMDFeVerCanMDFe: TFloatField;
    QrOpcoesMDFeMDFeVerInuNum: TFloatField;
    QrOpcoesMDFeMDFeVerConMDFe: TFloatField;
    QrOpcoesMDFeMDFeVerLotEve: TFloatField;
    QrOpcoesMDFeMDFeide_tpImp: TSmallintField;
    QrOpcoesMDFeMDFeide_tpAmb: TSmallintField;
    QrOpcoesMDFeMDFeAppCode: TSmallintField;
    QrOpcoesMDFeMDFeAssDigMode: TSmallintField;
    QrOpcoesMDFeMDFeEntiTipCto: TIntegerField;
    QrOpcoesMDFeMDFeEntiTipCt1: TIntegerField;
    QrOpcoesMDFeMyEmailMDFe: TWideStringField;
    QrOpcoesMDFeDirMDFeSchema: TWideStringField;
    QrOpcoesMDFeDirMDFeRWeb: TWideStringField;
    QrOpcoesMDFeDirMDFeEveEnvLot: TWideStringField;
    QrOpcoesMDFeDirMDFeEveRetLot: TWideStringField;
    QrOpcoesMDFeDirMDFeEvePedEnc: TWideStringField;
    QrOpcoesMDFeDirMDFeEveRetEnc: TWideStringField;
    QrOpcoesMDFeDirMDFeEveProcEnc: TWideStringField;
    QrOpcoesMDFeDirMDFeEvePedCan: TWideStringField;
    QrOpcoesMDFeDirMDFeEveRetCan: TWideStringField;
    QrOpcoesMDFeDirMDFeRetMDFeDes: TWideStringField;
    QrOpcoesMDFeMDFeMDFeVerConDes: TFloatField;
    QrOpcoesMDFeDirMDFeEvePedMDe: TWideStringField;
    QrOpcoesMDFeDirMDFeEveRetMDe: TWideStringField;
    QrOpcoesMDFeDirDowMDFeDes: TWideStringField;
    QrOpcoesMDFeDirDowMDFeMDFe: TWideStringField;
    QrOpcoesMDFeMDFeInfCpl: TIntegerField;
    QrOpcoesMDFeMDFeVerConsCad: TFloatField;
    QrOpcoesMDFeMDFeVerDistDFeInt: TFloatField;
    QrOpcoesMDFeMDFeVerDowMDFe: TFloatField;
    QrOpcoesMDFeDirDowMDFeCnf: TWideStringField;
    QrOpcoesMDFeMDFeItsLin: TSmallintField;
    QrOpcoesMDFeMDFeFTRazao: TSmallintField;
    QrOpcoesMDFeMDFeFTEnder: TSmallintField;
    QrOpcoesMDFeMDFeFTFones: TSmallintField;
    QrOpcoesMDFeMDFeMaiusc: TSmallintField;
    QrOpcoesMDFeMDFeShowURL: TWideStringField;
    QrOpcoesMDFeMDFetpEmis: TSmallintField;
    QrOpcoesMDFeMDFeSCAN_Ser: TIntegerField;
    QrOpcoesMDFeMDFeSCAN_nMDF: TIntegerField;
    QrOpcoesMDFeMDFe_indFinalCpl: TSmallintField;
    QrOpcoesMDFeTZD_UTC_Auto_TermoAceite: TIntegerField;
    QrOpcoesMDFeTZD_UTC_Auto_DataHoraAceite: TDateTimeField;
    QrOpcoesMDFeTZD_UTC_Auto_UsuarioAceite: TIntegerField;
    QrOpcoesMDFeTZD_UTC_Auto: TSmallintField;
    QrOpcoesMDFeTZD_UTC: TFloatField;
    QrOpcoesMDFehVeraoAsk: TDateField;
    QrOpcoesMDFehVeraoIni: TDateField;
    QrOpcoesMDFehVeraoFim: TDateField;
    QrOpcoesMDFeParamsMDFe: TIntegerField;
    QrOpcoesMDFeCodigo: TIntegerField;
    EncerrarMDFe1: TMenuItem;
    QrMDFeEveREncFatID: TIntegerField;
    QrMDFeEveREncFatNum: TIntegerField;
    QrMDFeEveREncEmpresa: TIntegerField;
    QrMDFeEveREncControle: TIntegerField;
    QrMDFeEveREncConta: TIntegerField;
    QrMDFeEveREncnProt: TWideStringField;
    QrMDFeEveREncdtEnc: TDateField;
    QrMDFeEveREnccUF: TSmallintField;
    QrMDFeEveREnccMun: TIntegerField;
    QrMDFeEveREncLk: TIntegerField;
    QrMDFeEveREncDataCad: TDateField;
    QrMDFeEveREncDataAlt: TDateField;
    QrMDFeEveREncUserCad: TIntegerField;
    QrMDFeEveREncUserAlt: TIntegerField;
    QrMDFeEveREncAlterWeb: TSmallintField;
    QrMDFeEveREncAtivo: TSmallintField;
    DBGrid3: TDBGrid;
    DBGrid5: TDBGrid;
    QrAide_UFFim: TWideStringField;
    Excluiencerramento1: TMenuItem;
    ExcluiCondutor1: TMenuItem;
    AdicionaInclusaodeCondutor1: TMenuItem;
    QrMDFeEveRIdCFatID: TIntegerField;
    QrMDFeEveRIdCFatNum: TIntegerField;
    QrMDFeEveRIdCEmpresa: TIntegerField;
    QrMDFeEveRIdCControle: TIntegerField;
    QrMDFeEveRIdCConta: TIntegerField;
    QrMDFeEveRIdCEntidade: TIntegerField;
    QrMDFeEveRIdCxNome: TWideStringField;
    QrMDFeEveRIdCCPF: TWideStringField;
    QrMDFeEveRIdCLk: TIntegerField;
    QrMDFeEveRIdCDataCad: TDateField;
    QrMDFeEveRIdCDataAlt: TDateField;
    QrMDFeEveRIdCUserCad: TIntegerField;
    QrMDFeEveRIdCUserAlt: TIntegerField;
    QrMDFeEveRIdCAlterWeb: TSmallintField;
    QrMDFeEveRIdCAtivo: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtEncClick(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure QrAAfterOpen(DataSet: TDataSet);
    procedure QrAAfterScroll(DataSet: TDataSet);
    procedure QrABeforeClose(DataSet: TDataSet);
    procedure QrMDFeEveRCabBeforeClose(DataSet: TDataSet);
    procedure QrMDFeEveRCabAfterScroll(DataSet: TDataSet);
    procedure QrMDFeEveRCabCalcFields(DataSet: TDataSet);
    procedure PMEncPopup(Sender: TObject);
    procedure BtCanClick(Sender: TObject);
    procedure IncluicancelamentodaMDFe1Click(Sender: TObject);
    procedure ExcluicancelamentodaMDFe1Click(Sender: TObject);
    procedure PMCanPopup(Sender: TObject);
    procedure QrACalcFields(DataSet: TDataSet);
    procedure BtIdcClick(Sender: TObject);
    procedure PMIdCPopup(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtLoteClick(Sender: TObject);
    procedure Solicitacancelamento1Click(Sender: TObject);
    procedure EncerrarMDFe1Click(Sender: TObject);
    procedure Excluiencerramento1Click(Sender: TObject);
    procedure ExcluiCondutor1Click(Sender: TObject);
    procedure AdicionaInclusaodeCondutor1Click(Sender: TObject);
  private
    { Private declarations }
    procedure MostraFormMDFeEveREnc(SQLType: TSQLType);
    procedure MostraFormMDFeEveRIdC(SQLType: TSQLType);
    procedure MostraFormCancelamentoMDFe(SQLType: TSQLType);
  public
    { Public declarations }
    procedure ReopenMDFeCabA(FatID, FatNum, Empresa: Integer);
    function  IncluiMDFeEveRCab(const EvePreDef: Integer; const EventoMDFe:
              TEventoXXe; var FatID, FatNum, Empresa, Controle: Integer):
              Boolean;
    procedure AbreDadosParaXML(Controle: Integer);
    procedure ReopenMDFeEveRCab(Controle: Integer);
    procedure ReopenMDFeEveREnc(Conta: Integer);
    procedure ReopenMDFeEveRCan(Conta: Integer);
    procedure ReopenMDFeEveRIdC(Conta: Integer);
  end;

  var
  FmMDFeEveRcab: TFmMDFeEveRcab;

implementation

uses UnMyObjects, Module, UMySQLModule, MyDBCheck, ModuleMDFe_0000, UnMDFe_Tabs,
  ModuleGeral, MDFeGeraXML_0100a, MDFeEveREnc, MDFeEveRIdC, MDFeEveRCan;

{$R *.DFM}

procedure TFmMDFeEveRcab.AbreDadosParaXML(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrERC, Dmod.MyDB, [
  'SELECT * FROM mdfeevercab ',
  'WHERE Controle=' + Geral.FF0(Controle),
  '']);
end;

procedure TFmMDFeEveRcab.AdicionaInclusaodeCondutor1Click(Sender: TObject);
begin
  MostraFormMDFeEveRIdC(stIns);
end;

procedure TFmMDFeEveRcab.BtCanClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 2;
  //
  MyObjects.MostraPopUpDeBotao(PMCan, BtCan);
end;

procedure TFmMDFeEveRcab.BtEncClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  //
  if QrAStatus.Value = 100 then
    MyObjects.MostraPopUpDeBotao(PMEnc, BtEnc)
  else
    Geral.MB_Aviso(
    'O MDF-e n�o pode ser encerrado porque seu status n�o � 100 - "Uso autorizado"');
end;

procedure TFmMDFeEveRcab.BtLoteClick(Sender: TObject);
var
  LoteEnv, Controle: Integer;
begin
(*****
  if (QrMDFeEveRCab.State <> dsInactive) and (QrMDFeEveRCab.RecordCount > 0) then
  begin
    Controle := QrMDFeEveRCabControle.Value;
    LoteEnv  := QrMDFeEveRCabEventoLote.Value;
  end else
  begin
    Controle := 0;
    LoteEnv  := 0;
  end;
  //
  MDFe_PF.MostraFormMDFeEveRLoE(LoteEnv);
  //
  if Controle <> 0 then
    ReopenMDFeEveRCab(Controle);
*)
end;

procedure TFmMDFeEveRcab.BtIdcClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMIdC, BtIdc);
end;

procedure TFmMDFeEveRcab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMDFeEveRcab.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  DmMDFe_0000.ColoreStatusMDFeEmDBGrid(DBGrid1, Rect, DataCol, Column,
    State, Trunc(QrAcStat.Value));
end;

procedure TFmMDFeEveRcab.EncerrarMDFe1Click(Sender: TObject);
begin
  MostraFormMDFeEveREnc(stIns);
end;

procedure TFmMDFeEveRcab.ExcluicancelamentodaMDFe1Click(Sender: TObject);
begin
  if QrMDFeEveRCabtpEvento.Value = CO_COD_evexxe110111Can then
  begin
    if not (QrMDFeEveRCabStatus.Value in ([135, 136])) then
    begin
      if Geral.MB_Pergunta('Confirma a exclus�o do cancelamento da MDFe?') = ID_YES then
      begin
        DBCheck.ExcluiRegistro(Dmod.QrUpd, QrMDFeEveRCan, 'mdfeevercan',
        ['Conta'], ['Conta'], True);
        //
        DBCheck.ExcluiRegistro(Dmod.QrUpd, QrMDFeEveRCab, 'mdfeevercab',
        ['Controle'], ['Controle'], True);
        //
        ReopenMDFeEveRCab(0);
      end;
    end else
      Geral.MB_Aviso(
      'O evento n�o pode ser exclu�do porque j� est� registrado!');
  end else
    Geral.MB_Aviso(
    'O evento n�o � cancelamento!');
end;

procedure TFmMDFeEveRcab.ExcluiCondutor1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o da inclus�o de condutor?') =
  ID_YES then
  begin
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrMDFeEveRIdC, 'mdfeeveridc',
    ['Conta'], ['Conta'], True);
    //
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrMDFeEveRCab, 'mdfeevercab',
    ['Controle'], ['Controle'], True);
    //
    ReopenMDFeEveRCab(0);
  end;
end;

procedure TFmMDFeEveRcab.Excluiencerramento1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do encerramento selecionado?') =
  ID_YES then
  begin
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrMDFeEveREnc, 'mdfeeverenc',
    ['Conta'], ['Conta'], True);
    //
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrMDFeEveRCab, 'mdfeevercab',
    ['Controle'], ['Controle'], True);
    //
    ReopenMDFeEveRCab(0);
  end;
end;

procedure TFmMDFeEveRcab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmMDFeEveRcab.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmMDFeEveRcab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMDFeEveRcab.IncluicancelamentodaMDFe1Click(Sender: TObject);
begin
  if QrAStatus.Value = 100 then
    MostraFormCancelamentoMDFe(stIns)
  else
    Geral.MB_Aviso(
    'A MDFe n�o pode ser cancelada porque seu status n�o � 100 - "Uso autorizado"');
end;

function TFmMDFeEveRcab.IncluiMDFeEveRCab(const EvePreDef: Integer;
const EventoMDFe: TEventoXXe; var
FatID, FatNum, Empresa, Controle: Integer): Boolean;
var
  Id, CNPJ, CPF, chMDFe, dhEvento, descEvento, verEvento: String;
  //EventoLote,
  cOrgao, tpAmb, TipoEnt, tpEvento, nSeqEvento: Integer;
  TZD_UTC, versao: Double;
begin
  Result := False;
  FatID          := QrAFatID.Value;
  FatNum         := QrAFatNum.Value;
  Empresa        := QrAEmpresa.Value;

  DmMDFe_0000.ReopenEmpresa(Empresa);
  if DmMDFe_0000.QrEmpresaCodigo.Value <> Empresa then
    Exit;
  if not DmMDFe_0000.ReopenOpcoesMDFe(DmMDFe_0000.QrOpcoesMDFe, Empresa, True) then
    Exit;
  //
(*
  if EventoMDFe = evexxMDe then
    cOrgao         := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(
                      DmMDFe_0000.QrOpcoesMDFeUF_MDeMDe.Value) // 91 = Ambiente Nacional
  else
*)
    cOrgao         := Geral.IMV(DmMDFe_0000.QrEmpresaDTB_UF.Value);
  tpAmb          := DmMDFe_0000.QrOpcoesMDFeMDFeide_tpAmb.Value;
  TipoEnt        := DmMDFe_0000.QrEmpresaTipo.Value;
  if TipoEnt = 0 then
  begin
    CNPJ           := Geral.SoNumero_TT(DmMDFe_0000.QrEmpresaCNPJ.Value);
    CPF            := '';
  end else
  begin
    CNPJ           := '';
    CPF            := Geral.SoNumero_TT(DmMDFe_0000.QrEmpresaCPF.Value);
  end;
  chMDFe          := QrAId.Value;

  dhEvento       := Geral.FDT(DmodG.ObtemAgora(), 109);
  //TZD_UTC        := DModG.UTC_ObtemDiferenca();
  //TZD_UTC        := DmodG.QrParamsEmpTZD_UTC.Value;
  TZD_UTC        := DmodG.ObtemHorarioTZD_JahCorrigido(Empresa);
  verEvento      := Geral.FFT_Dot(DmMDFe_0000.QrOpcoesMDFeMDFeVerLotEve.Value, 2, siPositivo);
  MDFe_PF.Evento_Obtem_DadosEspecificos(EvePreDef, EventoMDFe, tpEvento, versao,
    descEvento);
  nSeqEvento     := DmMDFe_0000.Evento_Obtem_nSeqEvento(FatID, FatNum, Empresa, tpEvento);
  Id             := MDFe_PF.Evento_MontaId(tpEvento, QrAId.Value, nSeqEvento);
  //
  Controle := DModG.BuscaProximoCodigoInt('mdfectrl', 'mdfeevercab', '', 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'mdfeevercab', False, [
  (*'EventoLote',*) 'Id', 'cOrgao',
  'tpAmb', 'TipoEnt', 'CNPJ',
  'CPF', 'chMDFe', 'dhEvento',
  'TZD_UTC', 'verEvento', 'tpEvento',
  'nSeqEvento', 'versao', 'descEvento'], [
  'FatID', 'FatNum', 'Empresa', 'Controle'], [
  (*EventoLote,*) Id, cOrgao,
  tpAmb, TipoEnt, CNPJ,
  CPF, chMDFe, dhEvento,
  TZD_UTC, verEvento, tpEvento,
  nSeqEvento, versao, descEvento], [
  FatID, FatNum, Empresa, Controle], True) then
  begin
    AbreDadosParaXML(Controle);
    Result := True;
  end;
end;

procedure TFmMDFeEveRcab.MostraFormCancelamentoMDFe(SQLType: TSQLType);
begin
  if (not XXe_PF.PermiteCriacaoDeEvento(tipoxxeMDFe, evexxe110111Can,
  QrAFatID.Value, QrAFatNum.Value, QrAEmpresa.Value)) and (SQLType = stIns) then
    Exit;
  //
  if DBCheck.CriaFm(TFmMDFeEveRCan, FmMDFeEveRCan, afmoNegarComAviso) then
  begin
    FmMDFeEveRCan.ImgTipo.SQLType := SQLType;
    FmMDFeEveRCan.Edide_serie.ValueVariant := QrAide_serie.Value;
    FmMDFeEveRCan.Edide_nMDF.ValueVariant := QrAide_nMDF.Value;
    FmMDFeEveRCan.EdnProt.Text := QrAnProt.Value;
    if SQLType = stUpd then
    begin
      // N�o tem upd
    end;
    FmMDFeEveRCan.ShowModal;
    FmMDFeEveRCan.Destroy;
  end;
end;

procedure TFmMDFeEveRcab.MostraFormMDFeEveREnc(SQLType: TSQLType);
begin
  if (not XXe_PF.PermiteCriacaoDeEvento(tipoxxeMDFe, evexxe110112Enc,
  QrAFatID.Value, QrAFatNum.Value, QrAEmpresa.Value)) and (SQLType = stIns) then
    Exit;
  //
  if DBCheck.CriaFm(TFmMDFeEveREnc, FmMDFeEveREnc, afmoNegarComAviso) then
  begin
    FmMDFeEveREnc.ImgTipo.SQLType := SQLType;
    FmMDFeEveREnc.EdchMDFe.ValueVariant     := QrAId.Value;
    FmMDFeEveREnc.Edide_serie.ValueVariant  := QrAide_serie.Value;
    FmMDFeEveREnc.Edide_nMDF.ValueVariant   := QrAide_nMDF.Value;
    FmMDFeEveREnc.EdnProt.ValueVariant      := QrAnProt.Value;
    if SQLType = stUpd then
    begin
      FmMDFeEveREnc.EdcUF.ValueVariant  := QrMDFeEveREnccUF.Value;
      FmMDFeEveREnc.EdCMun.ValueVariant := QrMDFeEveREnccMun.Value;
      FmMDFeEveREnc.CBCMun.KeyValue     := QrMDFeEveREnccMun.Value;
      FmMDFeEveREnc.TpdtEnc.Date        := QrMDFeEveREncdtEnc.Value;
    end else
    begin
      FmMDFeEveREnc.EdcUF.ValueVariant  := Geral.GetCodigoUF_da_SiglaUF(QrAide_UFFim.Value);
      FmMDFeEveREnc.EdCMun.ValueVariant := 0; // Ver o que fazer
      FmMDFeEveREnc.CBCMun.KeyValue     := 0; // Ver o que fazer
      FmMDFeEveREnc.TpdtEnc.Date        := DModG.ObtemAgora();
    end;
    FmMDFeEveREnc.ShowModal;
    FmMDFeEveREnc.Destroy;
  end;
end;

procedure TFmMDFeEveRcab.MostraFormMDFeEveRIdC(SQLType: TSQLType);
begin
  if (not XXe_PF.PermiteCriacaoDeEvento(tipoxxeMDFe, evexxe110114IncCondutor,
  QrAFatID.Value, QrAFatNum.Value, QrAEmpresa.Value)) and (SQLType = stIns) then
    Exit;
  //
  if DBCheck.CriaFm(TFmMDFeEveRIdC, FmMDFeEveRIdC, afmoNegarComAviso) then
  begin
    FmMDFeEveRIdC.ImgTipo.SQLType := SQLType;
    FmMDFeEveRIdC.EdchMDFe.ValueVariant     := QrAId.Value;
    FmMDFeEveRIdC.Edide_serie.ValueVariant  := QrAide_serie.Value;
    FmMDFeEveRIdC.Edide_nMDF.ValueVariant   := QrAide_nMDF.Value;
    FmMDFeEveRIdC.EdnProt.ValueVariant      := QrAnProt.Value;
    if SQLType = stUpd then
    begin
      FmMDFeEveRIdC.EdEntidade.ValueVariant  := QrMDFeEveRIdCEntidade.Value;
      FmMDFeEveRIdC.CBEntidade.KeyValue      := QrMDFeEveRIdCEntidade.Value;
      FmMDFeEveRIdC.EdxNome.ValueVariant     := QrMDFeEveRIdCxNome.Value;
      FmMDFeEveRIdC.EdCPF.ValueVariant       := QrMDFeEveRIdCCPF.Value;
    end;
    FmMDFeEveRIdC.ShowModal;
    FmMDFeEveRIdC.Destroy;
  end;
end;

procedure TFmMDFeEveRcab.PMCanPopup(Sender: TObject);
begin
  ExcluicancelamentodaMDFe1.Enabled :=
    (QrAStatus.Value = 100) and
    (QrMDFeEveRCabtpEvento.Value = CO_COD_evexxe110111Can)
    and (QrMDFeEveRCab.RecNo = 1) and
    ((QrMDFeEveRCabStatus.Value <= 30) or
    (QrMDFeEveRCabStatus.Value > 200));
end;

procedure TFmMDFeEveRcab.PMEncPopup(Sender: TObject);
begin
(*
  Alteracorreo1.Enabled :=
    (QrAStatus.Value = 100) and
    (QrMDFeEveRCabtpEvento.Value = MDFe_CodEventoEnc)
    and (QrMDFeEveRCab.RecNo = 1) and
    ((QrMDFeEveRCabStatus.Value <= 30) or
    (QrMDFeEveRCabStatus.Value > 200));
  Excluicorreo1.Enabled := Alteracorreo1.Enabled;
  //
  Imprime1.Enabled := (QrMDFeEveREnc.State <> dsInactive) and (QrMDFeEveREnc.RecordCount > 0);
*)
end;

procedure TFmMDFeEveRcab.PMIdCPopup(Sender: TObject);
var
  EhMDe: Boolean;
begin
{
  EhMDe :=
    (QrMDFeEveRCabtpEvento.Value = MDFe_CodEventoMDeConfirmacao) or
    (QrMDFeEveRCabtpEvento.Value = MDFe_CodEventoMDeCiencia) or
    (QrMDFeEveRCabtpEvento.Value = MDFe_CodEventoMDeDesconhece) or
    (QrMDFeEveRCabtpEvento.Value = MDFe_CodEventoMDeNaoRealizou);
  //
  Excluimanifestaodedestinatrio1.Enabled := EhMde and
    ((QrMDFeEveRCabStatus.Value <= 30) or
    (QrMDFeEveRCabStatus.Value > 200));
}
end;

procedure TFmMDFeEveRcab.QrACalcFields(DataSet: TDataSet);
begin
{
  QrAId_TXT.Value := XXe_PF.FormataID_XXe(QrAId.Value);
  //
  QrAEMIT_ENDERECO.Value := QrAemit_xLgr.Value + ', ' + Geral.FormataNumeroDeRua(
    QrAemit_xLgr.Value,
    QrAemit_nro.Value, True) + ' ' + QrAemit_xCpl.Value + #13#10 +
    QrAemit_xBairro.Value + #13#10 + QrAemit_xMun.Value + ' - ' +
    QrAemit_UF.Value + #13#10 +  'CEP: ' + Geral.FormataCEP_TT(IntToStr(
    QrAemit_CEP.Value)) + '  ' + QrAemit_xPais.Value;

  //
  QrAEMIT_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrAemit_fone.Value);
  //
  QrAEMIT_IE_TXT.Value := Geral.Formata_IE(QrAemit_IE.Value, QrAemit_UF.Value, '??');
  QrAEMIT_IEST_TXT.Value := Geral.Formata_IE(QrAemit_IEST.Value, QrAemit_UF.Value, '??');
  //
  if QrAemit_CNPJ.Value <> '' then
    QrAEMIT_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAemit_CNPJ.Value)
  else
    QrAEMIT_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAemit_CPF.Value);
  //
  //
  ////// DESTINAT�RIO
  //
  //
  if QrAdest_CNPJ.Value <> '' then
    QrADEST_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAdest_CNPJ.Value)
  else
    QrADEST_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAdest_CPF.Value);
  //
  QrADEST_ENDERECO.Value := QrAdest_xLgr.Value + ', ' + Geral.FormataNumeroDeRua(
    QrAdest_xLgr.Value, QrAdest_nro.Value, True) + ' ' + QrAdest_xCpl.Value;
  //
  // 2011-08-25
  if DModG.QrParamsEmpMDFeShowURL.Value <> ''  then
    QrAEMIT_ENDERECO.Value := QrAEMIT_ENDERECO.Value + #13#10 +
      DModG.QrParamsEmpMDFeShowURL.Value;
  if DModG.QrParamsEmpMDFeMaiusc.Value = 1  then
  begin
    QrADEST_ENDERECO.Value := AnsiUpperCase(QrADEST_ENDERECO.Value);
    QrADEST_XMUN_TXT.Value := AnsiUpperCase(QrAdest_xMun.Value);
  end else
  begin
    QrADEST_XMUN_TXT.Value := QrAdest_xMun.Value;
  end;
  // Fim 2011-08-25
  //
  QrADEST_CEP_TXT.Value := Geral.FormataCEP_TT(QrAdest_CEP.Value);
  QrADEST_FONE_TXT.Value := Geral.FormataTelefone_TT_Curto(QrAdest_fone.Value);
  //
  QrADEST_IE_TXT.Value := Geral.Formata_IE(QrAdest_IE.Value, QrAdest_UF.Value, '??');
  //
  //
  //
  ////// TRANSPORTADORA - REBOQUE???
  //
  //
  if Geral.SoNumero1a9_TT(QrAtransporta_CNPJ.Value) <> '' then
    QrATRANSPORTA_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAtransporta_CNPJ.Value)
  else
    QrATRANSPORTA_CNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrAtransporta_CPF.Value);
  //
  QrATRANSPORTA_IE_TXT.Value := Geral.Formata_IE(QrAtransporta_IE.Value, QrAtransporta_UF.Value, '??');
  //
  if QrAide_tpAmb.Value = 2 then
  begin
    QrADOC_SEM_VLR_JUR.Value :=
      'MDF-e emitido em ambiente de homologa��o. N�O POSSUI VALIDADE JUR�DICA';
    QrADOC_SEM_VLR_FIS.Value := 'SEM VALOR FISCAL';
  end else begin
    QrADOC_SEM_VLR_JUR.Value := '';
    QrADOC_SEM_VLR_FIS.Value := QrAinfProt_nProt.Value + ' ' + Geral.FDT(QrAinfProt_dhRecbto.Value, 00);
  end;
  QrAide_DSaiEnt_Txt.Value := dmkPF.FDT_NULO(QrAide_DSaiEnt.Value, 2);
  //
  //
(* p�g 12 da Nota T�cnica 2010/004 de junho/2010
0 � Emitente;
1 � Dest/Rem;
2 � Terceiros;
9 � Sem Frete;
*)
  case QrAModFrete.Value of
    0: QrAMODFRETE_TXT.Value := '0 � Emitente';
    1: QrAMODFRETE_TXT.Value := '1 � Dest/Rem';
    2: QrAMODFRETE_TXT.Value := '2 � Terceiros';
    9: QrAMODFRETE_TXT.Value := '9 � Sem Frete';
    else QrAMODFRETE_TXT.Value := FormatFloat('0', QrAModFrete.Value) + ' - ? ? ? ? ';
  end;
}
end;

procedure TFmMDFeEveRcab.QrAAfterOpen(DataSet: TDataSet);
begin
  DmMDFe_0000.ReopenEmpresa(QrAEmpresa.Value);
end;

procedure TFmMDFeEveRcab.QrAAfterScroll(DataSet: TDataSet);
begin
  BtEnc.Enabled := QrAEmpresa.Value = QrACodInfoEmite.Value;
  BtCan.Enabled := QrAEmpresa.Value = QrACodInfoEmite.Value;
  //BtMDe.Enabled := QrAEmpresa.Value = QrACodInfoDest.Value;
  ReopenMDFeEveRCab(0);
  DmMDFe_0000.ReopenOpcoesMDFe(QrOpcoesMDFe, QrAEmpresa.Value, True)
end;

procedure TFmMDFeEveRcab.QrABeforeClose(DataSet: TDataSet);
begin
  QrMDFeEveRCab.Close;
end;

procedure TFmMDFeEveRcab.QrMDFeEveRCabAfterScroll(DataSet: TDataSet);
begin
  QrMDFeEveREnc.Close;
  QrMDFeEveRCan.Close;
  ReopenMDFeEveREnc(0);
  ReopenMDFeEveRCan(0);
  ReopenMDFeEveRIdC(0);
end;

procedure TFmMDFeEveRcab.QrMDFeEveRCabBeforeClose(DataSet: TDataSet);
begin
  QrMDFeEveREnc.Close;
end;

procedure TFmMDFeEveRcab.QrMDFeEveRCabCalcFields(DataSet: TDataSet);
begin
  QrMDFeEveRCabtpAmb_TXT.Value := XXe_PF.TextoAmbiente(QrMDFeEveRCabtpAmb.Value);
end;

procedure TFmMDFeEveRcab.ReopenMDFeCabA(FatID, FatNum, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrA, Dmod.MyDB, [
  'SELECT nfa.ide_UFFim, ',
  'IF(emi.Tipo=0, emi.RazaoSocial, emi.Nome) NO_Emi, ',
  'nfa.FatID, nfa.FatNum, nfa.Empresa, nfa.LoteEnv, nfa.Id, ',
  'ide_serie, ide_nMDF, ide_dEmi, ',
  'IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat) + 0.000 cStat, ',
  'IF(infCanc_cStat>0, infCanc_nProt, infProt_nProt) nProt, ',
  'IF(infCanc_cStat>0, infCanc_xMotivo, infProt_xMotivo) xMotivo, ',
  'IF(infCanc_cStat>0, DATE_FORMAT(infCanc_dhRecbto, "%d/%m/%Y %H:%i:%S"), ',
  'DATE_FORMAT(infProt_dhRecbto, "%d/%m/%Y %H:%i:%S")) dhRecbto, ',
  'IDCtrl, versao,  ',
  'ide_tpEmis, infCanc_xJust, Status,  ',
  'nfa.CodInfoEmite, ',
  'cSitMDFe, cSitEnc, ',
  MDFe_PF.TxtNo_SitEnc(),
  'nfa.infCanc_dhRecbto, nfa.infCanc_nProt,  ',
  'nfa.ide_Modal ',
  'FROM mdfecaba nfa ',
  'LEFT JOIN entidades emi ON emi.Codigo=nfa.CodInfoEmite ',
  ' ',
  'WHERE nfa.FatID=' + Geral.FF0(FatID),
  'AND nfa.FatNum=' + Geral.FF0(FatNum),
  'AND nfa.Empresa=' + Geral.FF0(Empresa),
  'ORDER BY nfa.ide_dEmi DESC, nfa.ide_nMDF DESC ',
  '']);
end;

procedure TFmMDFeEveRcab.ReopenMDFeEveRCab(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeEveRCab, Dmod.MyDB, [
  'SELECT * FROM mdfeevercab ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  'ORDER BY Controle DESC ',
  '']);
  //
  if (Controle <> 0) then
    QrMDFeEveRCab.Locate('Controle', Controle, []);
end;

procedure TFmMDFeEveRcab.ReopenMDFeEveRCan(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeEveRCan, Dmod.MyDB, [
  'SELECT * FROM mdfeevercan ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  'AND Controle=' + Geral.FF0(QrMDFeEveRCabControle.Value),
  'ORDER BY Conta DESC ',
  '']);
  //
  if (Conta <> 0) then
    QrMDFeEveRCan.Locate('Conta', Conta, []);
end;

procedure TFmMDFeEveRcab.ReopenMDFeEveREnc(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeEveREnc, Dmod.MyDB, [
  'SELECT * FROM mdfeeverenc ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  'AND Controle=' + Geral.FF0(QrMDFeEveRCabControle.Value),
  'ORDER BY Conta DESC ',
  '']);
  //
  if (Conta <> 0) then
    QrMDFeEveREnc.Locate('Conta', Conta, []);
end;

procedure TFmMDFeEveRcab.Solicitacancelamento1Click(Sender: TObject);
var
  Empresa, Modelo, Serie, nCT, nSeqEvento, Justif: Integer;
  nProtCan, chMDFe: String;
begin
(*
  Empresa     := QrMDFeEveRCanEmpresa.Value;
  Modelo      := QrMDFeEveRCan.Value;
  Serie       := QrMDFeEveRCan.Value;
  nCT         := QrMDFeEveRCan.Value;
  nSeqEvento  := QrMDFeEveRCan.Value;
  Justif      := QrMDFeEveRCan.Value;
  nProtCan    := QrMDFeEveRCan.Value;
  chMDFe       := QrMDFeEveRCan.Value;
  //
  MDFe_PF.MostraFormStepsMDFe_Cancelamento(Empresa, Modelo, Serie, nCT,
              nSeqEvento, Justif: Integer; nProtCan, chMDFe: String);
*)
end;

procedure TFmMDFeEveRcab.ReopenMDFeEveRIdC(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMDFeEveRIdC, Dmod.MyDB, [
  'SELECT * FROM mdfeeveridc ',
  'WHERE FatID=' + Geral.FF0(QrAFatID.Value),
  'AND FatNum=' + Geral.FF0(QrAFatNum.Value),
  'AND Empresa=' + Geral.FF0(QrAEmpresa.Value),
  'AND Controle=' + Geral.FF0(QrMDFeEveRCabControle.Value),
  'ORDER BY Conta DESC ',
  '']);
  //
  if (Conta <> 0) then
    QrMDFeEveRIdC.Locate('Conta', Conta, []);
end;

// TODO :      ver retirada de evento de lote }
{ TODO :      mostrar status do evento processado }

end.
