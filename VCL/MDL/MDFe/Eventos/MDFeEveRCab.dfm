object FmMDFeEveRcab: TFmMDFeEveRcab
  Tag = 526
  Left = 339
  Top = 185
  Caption = 'MDF-EVENT-001 :: Eventos da MDF-e'
  ClientHeight = 492
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
      object BtLote: TBitBtn
        Tag = 570
        Left = 5
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtLoteClick
      end
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 226
        Height = 32
        Caption = 'Eventos da MDF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 226
        Height = 32
        Caption = 'Eventos da MDF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 226
        Height = 32
        Caption = 'Eventos da MDF-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 330
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 330
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Splitter1: TSplitter
        Left = 0
        Top = 180
        Width = 784
        Height = 5
        Cursor = crVSplit
        Align = alTop
        ExplicitTop = 160
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 784
        Height = 60
        Align = alTop
        DataSource = DsA
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = DBGrid1DrawColumnCell
        Columns = <
          item
            Expanded = False
            FieldName = 'ide_serie'
            Title.Caption = 'S'#233'rie'
            Width = 29
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ide_nMDF'
            Title.Caption = 'N'#186' CT'
            Width = 54
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'cStat'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'Status'
            Width = 35
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'xMotivo'
            Title.Caption = 'Motivo do status'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'dhRecbto'
            Title.Caption = 'Data/hora fisco'
            Width = 106
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Id'
            Title.Caption = 'Chave de acesso da NF-e'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ide_dEmi'
            Title.Caption = 'Emiss'#227'o'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nProt'
            Title.Caption = 'N'#186' do protocolo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FatID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FatNum'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LoteEnv'
            Title.Caption = 'Lote CTe'
            Width = 51
            Visible = True
          end>
      end
      object DBGrid2: TDBGrid
        Left = 0
        Top = 60
        Width = 784
        Height = 120
        Align = alTop
        DataSource = DsMDFeEveRCab
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'cOrgao'
            Title.Caption = 'UF'
            Width = 21
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EventoLote'
            Title.Caption = 'Lote'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'descEvento'
            Title.Caption = 'Tipo de evento'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'dhEvento'
            Title.Caption = 'Data / hora gera'#231#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TZD_UTC'
            Title.Caption = 'Dif UTC'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'nSeqEvento'
            Title.Caption = 'nSeq'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'tpAmb'
            Title.Caption = 'Amb'
            Width = 81
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Status'
            Width = 38
            Visible = True
          end>
      end
      object PageControl1: TPageControl
        Left = 0
        Top = 185
        Width = 784
        Height = 145
        ActivePage = TabSheet3
        Align = alClient
        TabOrder = 2
        object TabSheet3: TTabSheet
          Caption = 'Inclus'#227'o de Condutor'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid5: TDBGrid
            Left = 0
            Top = 0
            Width = 776
            Height = 117
            Align = alClient
            DataSource = DsMDFeEveRIdC
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
        object TabSheet1: TTabSheet
          Caption = 'Encerramento'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid3: TDBGrid
            Left = 0
            Top = 0
            Width = 776
            Height = 117
            Align = alClient
            DataSource = DsMDFeEveREnc
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
        object TabSheet0: TTabSheet
          Caption = 'Cancelamento'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid4: TDBGrid
            Left = 0
            Top = 0
            Width = 776
            Height = 117
            Align = alClient
            DataSource = DsMDFeEveRCan
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 378
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 422
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtEnc: TBitBtn
        Left = 132
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Encerrar MDF-e'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtEncClick
      end
      object BtCan: TBitBtn
        Tag = 455
        Left = 252
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Cancelar MDF-e.'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtCanClick
      end
      object BtIdc: TBitBtn
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Incl.Cond.'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtIdcClick
      end
    end
  end
  object DsA: TDataSource
    DataSet = QrA
    Left = 52
    Top = 180
  end
  object PMEnc: TPopupMenu
    OnPopup = PMEncPopup
    Left = 192
    Top = 400
    object EncerrarMDFe1: TMenuItem
      Caption = '&Encerrar MDF-e'
      OnClick = EncerrarMDFe1Click
    end
    object Excluiencerramento1: TMenuItem
      Caption = '&Exclui encerramento'
      OnClick = Excluiencerramento1Click
    end
  end
  object QrMDFeEveRCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrMDFeEveRCabBeforeClose
    AfterScroll = QrMDFeEveRCabAfterScroll
    OnCalcFields = QrMDFeEveRCabCalcFields
    SQL.Strings = (
      'SELECT * FROM nfeevercab'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'ORDER BY Controle DESC')
    Left = 224
    Top = 132
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrMDFeEveRCabFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeEveRCabFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeEveRCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeEveRCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMDFeEveRCabEventoLote: TIntegerField
      FieldName = 'EventoLote'
    end
    object QrMDFeEveRCabId: TWideStringField
      FieldName = 'Id'
      Size = 54
    end
    object QrMDFeEveRCabcOrgao: TSmallintField
      FieldName = 'cOrgao'
    end
    object QrMDFeEveRCabtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrMDFeEveRCabTipoEnt: TSmallintField
      FieldName = 'TipoEnt'
    end
    object QrMDFeEveRCabCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrMDFeEveRCabCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrMDFeEveRCabchMDFe: TWideStringField
      FieldName = 'chMDFe'
      Size = 44
    end
    object QrMDFeEveRCabdhEvento: TDateTimeField
      FieldName = 'dhEvento'
    end
    object QrMDFeEveRCabverEvento: TFloatField
      FieldName = 'verEvento'
    end
    object QrMDFeEveRCabtpEvento: TIntegerField
      FieldName = 'tpEvento'
    end
    object QrMDFeEveRCabnSeqEvento: TSmallintField
      FieldName = 'nSeqEvento'
    end
    object QrMDFeEveRCabversao: TFloatField
      FieldName = 'versao'
    end
    object QrMDFeEveRCabdescEvento: TWideStringField
      FieldName = 'descEvento'
      Size = 60
    end
    object QrMDFeEveRCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMDFeEveRCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMDFeEveRCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMDFeEveRCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMDFeEveRCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMDFeEveRCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrMDFeEveRCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrMDFeEveRCabTZD_UTC: TFloatField
      FieldName = 'TZD_UTC'
    end
    object QrMDFeEveRCabtpAmb_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'tpAmb_TXT'
      Size = 15
      Calculated = True
    end
    object QrMDFeEveRCabStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrMDFeEveRCabXML_Eve: TWideMemoField
      FieldName = 'XML_Eve'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrMDFeEveRCabXML_retEve: TWideMemoField
      FieldName = 'XML_retEve'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrMDFeEveRCabret_versao: TFloatField
      FieldName = 'ret_versao'
    end
    object QrMDFeEveRCabret_Id: TWideStringField
      FieldName = 'ret_Id'
      Size = 67
    end
    object QrMDFeEveRCabret_tpAmb: TSmallintField
      FieldName = 'ret_tpAmb'
    end
    object QrMDFeEveRCabret_verAplic: TWideStringField
      FieldName = 'ret_verAplic'
    end
    object QrMDFeEveRCabret_cOrgao: TSmallintField
      FieldName = 'ret_cOrgao'
    end
    object QrMDFeEveRCabret_cStat: TIntegerField
      FieldName = 'ret_cStat'
    end
    object QrMDFeEveRCabret_xMotivo: TWideStringField
      FieldName = 'ret_xMotivo'
      Size = 255
    end
    object QrMDFeEveRCabret_chMDFe: TWideStringField
      FieldName = 'ret_chMDFe'
      Size = 44
    end
    object QrMDFeEveRCabret_tpEvento: TIntegerField
      FieldName = 'ret_tpEvento'
    end
    object QrMDFeEveRCabret_xEvento: TWideStringField
      FieldName = 'ret_xEvento'
      Size = 60
    end
    object QrMDFeEveRCabret_nSeqEvento: TIntegerField
      FieldName = 'ret_nSeqEvento'
    end
    object QrMDFeEveRCabret_CNPJDest: TWideStringField
      FieldName = 'ret_CNPJDest'
      Size = 18
    end
    object QrMDFeEveRCabret_CPFDest: TWideStringField
      FieldName = 'ret_CPFDest'
      Size = 18
    end
    object QrMDFeEveRCabret_emailDest: TWideStringField
      FieldName = 'ret_emailDest'
      Size = 60
    end
    object QrMDFeEveRCabret_dhRegEvento: TDateTimeField
      FieldName = 'ret_dhRegEvento'
    end
    object QrMDFeEveRCabret_TZD_UTC: TFloatField
      FieldName = 'ret_TZD_UTC'
    end
    object QrMDFeEveRCabret_nProt: TWideStringField
      FieldName = 'ret_nProt'
      Size = 15
    end
  end
  object DsMDFeEveRCab: TDataSource
    DataSet = QrMDFeEveRCab
    Left = 224
    Top = 180
  end
  object QrMDFeEveREnc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM mdfeeverenc'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND Controle=:P3'
      'ORDER BY Conta DESC')
    Left = 308
    Top = 132
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrMDFeEveREncFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeEveREncFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeEveREncEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeEveREncControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMDFeEveREncConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrMDFeEveREncnProt: TWideStringField
      FieldName = 'nProt'
      Size = 15
    end
    object QrMDFeEveREncdtEnc: TDateField
      FieldName = 'dtEnc'
    end
    object QrMDFeEveREnccUF: TSmallintField
      FieldName = 'cUF'
    end
    object QrMDFeEveREnccMun: TIntegerField
      FieldName = 'cMun'
    end
    object QrMDFeEveREncLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMDFeEveREncDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMDFeEveREncDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMDFeEveREncUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMDFeEveREncUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMDFeEveREncAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrMDFeEveREncAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsMDFeEveREnc: TDataSource
    DataSet = QrMDFeEveREnc
    Left = 308
    Top = 180
  end
  object QrERC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfeevercab '
      'WHERE Controle=:P0')
    Left = 160
    Top = 132
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrERCFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrERCFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrERCEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrERCControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrERCEventoLote: TIntegerField
      FieldName = 'EventoLote'
    end
    object QrERCId: TWideStringField
      FieldName = 'Id'
      Size = 54
    end
    object QrERCcOrgao: TSmallintField
      FieldName = 'cOrgao'
    end
    object QrERCtpAmb: TSmallintField
      FieldName = 'tpAmb'
    end
    object QrERCTipoEnt: TSmallintField
      FieldName = 'TipoEnt'
    end
    object QrERCCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 14
    end
    object QrERCCPF: TWideStringField
      FieldName = 'CPF'
      Size = 11
    end
    object QrERCchMDFe: TWideStringField
      FieldName = 'chMDFe'
      Size = 44
    end
    object QrERCdhEvento: TDateTimeField
      FieldName = 'dhEvento'
    end
    object QrERCTZD_UTC: TFloatField
      FieldName = 'TZD_UTC'
    end
    object QrERCverEvento: TFloatField
      FieldName = 'verEvento'
    end
    object QrERCtpEvento: TIntegerField
      FieldName = 'tpEvento'
    end
    object QrERCnSeqEvento: TSmallintField
      FieldName = 'nSeqEvento'
    end
    object QrERCversao: TFloatField
      FieldName = 'versao'
    end
    object QrERCdescEvento: TWideStringField
      FieldName = 'descEvento'
      Size = 60
    end
  end
  object PMCan: TPopupMenu
    OnPopup = PMCanPopup
    Left = 280
    Top = 400
    object IncluicancelamentodaMDFe1: TMenuItem
      Caption = '&Inclui cancelamento da MDFe'
      OnClick = IncluicancelamentodaMDFe1Click
    end
    object ExcluicancelamentodaMDFe1: TMenuItem
      Caption = '&Exclui cancelamento da MDFe'
      OnClick = ExcluicancelamentodaMDFe1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Solicitacancelamento1: TMenuItem
      Caption = '&Solicita cancelamento'
      Enabled = False
      OnClick = Solicitacancelamento1Click
    end
  end
  object QrMDFeEveRCan: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfeevercan'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND Controle=:P3'
      'ORDER BY Conta DESC')
    Left = 388
    Top = 132
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrMDFeEveRCanFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeEveRCanFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeEveRCanEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeEveRCanControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMDFeEveRCanConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrMDFeEveRCannProt: TLargeintField
      FieldName = 'nProt'
    end
    object QrMDFeEveRCannJust: TIntegerField
      FieldName = 'nJust'
    end
    object QrMDFeEveRCanxJust: TWideStringField
      FieldName = 'xJust'
      Size = 255
    end
    object QrMDFeEveRCanLk: TIntegerField
      FieldName = 'Lk'
    end
  end
  object DsMDFeEveRCan: TDataSource
    DataSet = QrMDFeEveRCan
    Left = 388
    Top = 180
  end
  object PMIdC: TPopupMenu
    OnPopup = PMIdCPopup
    Left = 84
    Top = 400
    object AdicionaInclusaodeCondutor1: TMenuItem
      Caption = '&Adiciona Inclus'#227'o de Condutor'
      OnClick = AdicionaInclusaodeCondutor1Click
    end
    object ExcluiCondutor1: TMenuItem
      Caption = '&Remove Inclus'#227'o de  Condutor'
      OnClick = ExcluiCondutor1Click
    end
  end
  object QrMDFeEveRIdC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM mdfeeveridc'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'AND Empresa=:P2'
      'AND Controle=:P3'
      'ORDER BY Conta DESC')
    Left = 472
    Top = 132
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrMDFeEveRIdCFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrMDFeEveRIdCFatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrMDFeEveRIdCEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrMDFeEveRIdCControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMDFeEveRIdCConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrMDFeEveRIdCEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrMDFeEveRIdCxNome: TWideStringField
      FieldName = 'xNome'
      Size = 60
    end
    object QrMDFeEveRIdCCPF: TWideStringField
      FieldName = 'CPF'
      Size = 60
    end
    object QrMDFeEveRIdCLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMDFeEveRIdCDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMDFeEveRIdCDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMDFeEveRIdCUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMDFeEveRIdCUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMDFeEveRIdCAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrMDFeEveRIdCAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsMDFeEveRIdC: TDataSource
    DataSet = QrMDFeEveRIdC
    Left = 472
    Top = 180
  end
  object QrA: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrAAfterOpen
    BeforeClose = QrABeforeClose
    AfterScroll = QrAAfterScroll
    OnCalcFields = QrACalcFields
    SQL.Strings = (
      'SELECT '
      'IF(emi.Tipo=0, emi.RazaoSocial, emi.Nome) NO_Emi, '
      'nfa.FatID, nfa.FatNum, nfa.Empresa, nfa.LoteEnv, nfa.Id, '
      'ide_serie, ide_nMDF, ide_dEmi, '
      
        'IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat) + 0.000 cStat,' +
        ' '
      'IF(infCanc_cStat>0, infCanc_nProt, infProt_nProt) nProt, '
      'IF(infCanc_cStat>0, infCanc_xMotivo, infProt_xMotivo) xMotivo, '
      
        'IF(infCanc_cStat>0, DATE_FORMAT(infCanc_dhRecbto, "%d/%m/%Y %H:%' +
        'i:%S"), '
      'DATE_FORMAT(infProt_dhRecbto, "%d/%m/%Y %H:%i:%S")) dhRecbto, '
      'IDCtrl, versao,  '
      'ide_tpEmis, infCanc_xJust, Status,  '
      'nfa.CodInfoEmite, '
      'cSitMDFe, cSitConf, '
      
        'ELT(cSitConf+2, "N'#227'o consultada", "Sem manifesta'#231#227'o", "Confirmad' +
        'a", '
      
        '"Desconhecida", "N'#227'o realizada", "Ci'#234'ncia", "? ? ?") NO_cSitConf' +
        ', '
      'nfa.infCanc_dhRecbto, nfa.infCanc_nProt,  '
      'nfa.ide_Modal '
      'FROM mdfecaba nfa '
      'LEFT JOIN entidades emi ON emi.Codigo=nfa.CodInfoEmite '
      ' '
      'WHERE nfa.FatID=5001'
      'AND nfa.FatNum=2'
      'AND nfa.Empresa=-11'
      'ORDER BY nfa.ide_dEmi DESC, nfa.ide_nMDF DESC ')
    Left = 52
    Top = 132
    object QrAFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'mdfecaba.FatID'
    end
    object QrAFatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'mdfecaba.FatNum'
    end
    object QrALoteEnv: TIntegerField
      FieldName = 'LoteEnv'
      Origin = 'mdfecaba.LoteEnv'
    end
    object QrAId: TWideStringField
      FieldName = 'Id'
      Origin = 'mdfecaba.Id'
      Size = 44
    end
    object QrAEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'mdfecaba.Empresa'
    end
    object QrAide_serie: TIntegerField
      FieldName = 'ide_serie'
      Origin = 'mdfecaba.ide_serie'
    end
    object QrAide_nMDF: TIntegerField
      FieldName = 'ide_nMDF'
      Origin = 'mdfecaba.ide_nMDF'
    end
    object QrAide_dEmi: TDateField
      FieldName = 'ide_dEmi'
      Origin = 'mdfecaba.ide_dEmi'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrAnProt: TWideStringField
      FieldName = 'nProt'
      Size = 15
    end
    object QrAxMotivo: TWideStringField
      FieldName = 'xMotivo'
      Size = 255
    end
    object QrAdhRecbto: TWideStringField
      FieldName = 'dhRecbto'
      Required = True
      Size = 19
    end
    object QrAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'mdfecaba.IDCtrl'
    end
    object QrAversao: TFloatField
      FieldName = 'versao'
      Origin = 'mdfecaba.versao'
    end
    object QrAide_tpEmis: TSmallintField
      FieldName = 'ide_tpEmis'
      Origin = 'mdfecaba.ide_tpEmis'
    end
    object QrANOME_tpEmis: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_tpEmis'
      Size = 30
      Calculated = True
    end
    object QrANOME_tpMDFe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_tpMDFe'
      Size = 1
      Calculated = True
    end
    object QrAinfCanc_xJust: TWideStringField
      FieldName = 'infCanc_xJust'
      Origin = 'mdfecaba.infCanc_xJust'
      Size = 255
    end
    object QrAStatus: TIntegerField
      FieldName = 'Status'
      Origin = 'mdfecaba.Status'
    end
    object QrAinfCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
      Origin = 'mdfecaba.infCanc_dhRecbto'
    end
    object QrAinfCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Origin = 'mdfecaba.infCanc_nProt'
      Size = 15
    end
    object QrAcStat: TFloatField
      FieldName = 'cStat'
    end
    object QrAcSitMDFe: TSmallintField
      FieldName = 'cSitMDFe'
      Origin = 'mdfecaba.cSitMDFe'
    end
    object QrAcSitEnc: TSmallintField
      FieldName = 'cSitEnc'
      Origin = 'mdfecaba.cSitEnc'
    end
    object QrANO_cSitEnc: TWideStringField
      FieldName = 'NO_cSitEnc'
      Size = 16
    end
    object QrANO_Emi: TWideStringField
      FieldName = 'NO_Emi'
      Size = 100
    end
    object QrAide_Modal: TSmallintField
      FieldName = 'ide_Modal'
      Origin = 'mdfecaba.ide_Modal'
    end
    object QrACodInfoEmite: TIntegerField
      FieldName = 'CodInfoEmite'
      Origin = 'mdfecaba.CodInfoEmite'
    end
    object QrAide_UFFim: TWideStringField
      FieldName = 'ide_UFFim'
      Size = 2
    end
  end
  object QrOpcoesMDFe: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT etc0.Nome NO_ETC_0, etc1.Nome NO_ETC_1,  ctr.*'
      'FROM paramsemp ctr'
      'LEFT JOIN entitipcto etc0 ON etc0.Codigo=ctr.EntiTipCto'
      'LEFT JOIN entitipcto etc1 ON etc1.Codigo=ctr.EntiTipCt1'
      'WHERE ctr.Codigo=:P0')
    Left = 676
    Top = 132
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOpcoesMDFeMDFeversao: TFloatField
      FieldName = 'MDFeversao'
    end
    object QrOpcoesMDFeMDFeide_mod: TSmallintField
      FieldName = 'MDFeide_mod'
    end
    object QrOpcoesMDFeSimplesFed: TSmallintField
      FieldName = 'SimplesFed'
    end
    object QrOpcoesMDFeDirMDFeGer: TWideStringField
      FieldName = 'DirMDFeGer'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeAss: TWideStringField
      FieldName = 'DirMDFeAss'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEnvLot: TWideStringField
      FieldName = 'DirMDFeEnvLot'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeRec: TWideStringField
      FieldName = 'DirMDFeRec'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFePedRec: TWideStringField
      FieldName = 'DirMDFePedRec'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeProRec: TWideStringField
      FieldName = 'DirMDFeProRec'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeDen: TWideStringField
      FieldName = 'DirMDFeDen'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFePedCan: TWideStringField
      FieldName = 'DirMDFePedCan'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeCan: TWideStringField
      FieldName = 'DirMDFeCan'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFePedInu: TWideStringField
      FieldName = 'DirMDFePedInu'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeInu: TWideStringField
      FieldName = 'DirMDFeInu'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFePedSit: TWideStringField
      FieldName = 'DirMDFePedSit'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeSit: TWideStringField
      FieldName = 'DirMDFeSit'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFePedSta: TWideStringField
      FieldName = 'DirMDFePedSta'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeSta: TWideStringField
      FieldName = 'DirMDFeSta'
      Size = 255
    end
    object QrOpcoesMDFeMDFeUF_WebServ: TWideStringField
      FieldName = 'MDFeUF_WebServ'
      Size = 2
    end
    object QrOpcoesMDFeMDFeUF_Servico: TWideStringField
      FieldName = 'MDFeUF_Servico'
      Size = 10
    end
    object QrOpcoesMDFePathLogoMDFe: TWideStringField
      FieldName = 'PathLogoMDFe'
      Size = 255
    end
    object QrOpcoesMDFeCtaFretPrest: TIntegerField
      FieldName = 'CtaFretPrest'
    end
    object QrOpcoesMDFeTxtFretPrest: TWideStringField
      FieldName = 'TxtFretPrest'
      Size = 100
    end
    object QrOpcoesMDFeDupFretPrest: TWideStringField
      FieldName = 'DupFretPrest'
      Size = 3
    end
    object QrOpcoesMDFeMDFeSerNum: TWideStringField
      FieldName = 'MDFeSerNum'
      Size = 255
    end
    object QrOpcoesMDFeMDFeSerVal: TDateField
      FieldName = 'MDFeSerVal'
    end
    object QrOpcoesMDFeMDFeSerAvi: TSmallintField
      FieldName = 'MDFeSerAvi'
    end
    object QrOpcoesMDFeDirDAMDFes: TWideStringField
      FieldName = 'DirDAMDFes'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeProt: TWideStringField
      FieldName = 'DirMDFeProt'
      Size = 255
    end
    object QrOpcoesMDFeMDFePreMailAut: TIntegerField
      FieldName = 'MDFePreMailAut'
    end
    object QrOpcoesMDFeMDFePreMailCan: TIntegerField
      FieldName = 'MDFePreMailCan'
    end
    object QrOpcoesMDFeMDFePreMailEveCCe: TIntegerField
      FieldName = 'MDFePreMailEveCCe'
    end
    object QrOpcoesMDFeMDFeVerStaSer: TFloatField
      FieldName = 'MDFeVerStaSer'
    end
    object QrOpcoesMDFeMDFeVerEnvLot: TFloatField
      FieldName = 'MDFeVerEnvLot'
    end
    object QrOpcoesMDFeMDFeVerConLot: TFloatField
      FieldName = 'MDFeVerConLot'
    end
    object QrOpcoesMDFeMDFeVerCanMDFe: TFloatField
      FieldName = 'MDFeVerCanMDFe'
    end
    object QrOpcoesMDFeMDFeVerInuNum: TFloatField
      FieldName = 'MDFeVerInuNum'
    end
    object QrOpcoesMDFeMDFeVerConMDFe: TFloatField
      FieldName = 'MDFeVerConMDFe'
    end
    object QrOpcoesMDFeMDFeVerLotEve: TFloatField
      FieldName = 'MDFeVerLotEve'
    end
    object QrOpcoesMDFeMDFeide_tpImp: TSmallintField
      FieldName = 'MDFeide_tpImp'
    end
    object QrOpcoesMDFeMDFeide_tpAmb: TSmallintField
      FieldName = 'MDFeide_tpAmb'
    end
    object QrOpcoesMDFeMDFeAppCode: TSmallintField
      FieldName = 'MDFeAppCode'
    end
    object QrOpcoesMDFeMDFeAssDigMode: TSmallintField
      FieldName = 'MDFeAssDigMode'
    end
    object QrOpcoesMDFeMDFeEntiTipCto: TIntegerField
      FieldName = 'MDFeEntiTipCto'
    end
    object QrOpcoesMDFeMDFeEntiTipCt1: TIntegerField
      FieldName = 'MDFeEntiTipCt1'
    end
    object QrOpcoesMDFeMyEmailMDFe: TWideStringField
      FieldName = 'MyEmailMDFe'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeSchema: TWideStringField
      FieldName = 'DirMDFeSchema'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeRWeb: TWideStringField
      FieldName = 'DirMDFeRWeb'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEveEnvLot: TWideStringField
      FieldName = 'DirMDFeEveEnvLot'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEveRetLot: TWideStringField
      FieldName = 'DirMDFeEveRetLot'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEvePedEnc: TWideStringField
      FieldName = 'DirMDFeEvePedEnc'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEveRetEnc: TWideStringField
      FieldName = 'DirMDFeEveRetEnc'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEveProcEnc: TWideStringField
      FieldName = 'DirMDFeEveProcEnc'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEvePedCan: TWideStringField
      FieldName = 'DirMDFeEvePedCan'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEveRetCan: TWideStringField
      FieldName = 'DirMDFeEveRetCan'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeRetMDFeDes: TWideStringField
      FieldName = 'DirMDFeRetMDFeDes'
      Size = 255
    end
    object QrOpcoesMDFeMDFeMDFeVerConDes: TFloatField
      FieldName = 'MDFeMDFeVerConDes'
    end
    object QrOpcoesMDFeDirMDFeEvePedMDe: TWideStringField
      FieldName = 'DirMDFeEvePedMDe'
      Size = 255
    end
    object QrOpcoesMDFeDirMDFeEveRetMDe: TWideStringField
      FieldName = 'DirMDFeEveRetMDe'
      Size = 255
    end
    object QrOpcoesMDFeDirDowMDFeDes: TWideStringField
      FieldName = 'DirDowMDFeDes'
      Size = 255
    end
    object QrOpcoesMDFeDirDowMDFeMDFe: TWideStringField
      FieldName = 'DirDowMDFeMDFe'
      Size = 255
    end
    object QrOpcoesMDFeMDFeInfCpl: TIntegerField
      FieldName = 'MDFeInfCpl'
    end
    object QrOpcoesMDFeMDFeVerConsCad: TFloatField
      FieldName = 'MDFeVerConsCad'
    end
    object QrOpcoesMDFeMDFeVerDistDFeInt: TFloatField
      FieldName = 'MDFeVerDistDFeInt'
    end
    object QrOpcoesMDFeMDFeVerDowMDFe: TFloatField
      FieldName = 'MDFeVerDowMDFe'
    end
    object QrOpcoesMDFeDirDowMDFeCnf: TWideStringField
      FieldName = 'DirDowMDFeCnf'
      Size = 255
    end
    object QrOpcoesMDFeMDFeItsLin: TSmallintField
      FieldName = 'MDFeItsLin'
    end
    object QrOpcoesMDFeMDFeFTRazao: TSmallintField
      FieldName = 'MDFeFTRazao'
    end
    object QrOpcoesMDFeMDFeFTEnder: TSmallintField
      FieldName = 'MDFeFTEnder'
    end
    object QrOpcoesMDFeMDFeFTFones: TSmallintField
      FieldName = 'MDFeFTFones'
    end
    object QrOpcoesMDFeMDFeMaiusc: TSmallintField
      FieldName = 'MDFeMaiusc'
    end
    object QrOpcoesMDFeMDFeShowURL: TWideStringField
      FieldName = 'MDFeShowURL'
      Size = 255
    end
    object QrOpcoesMDFeMDFetpEmis: TSmallintField
      FieldName = 'MDFetpEmis'
    end
    object QrOpcoesMDFeMDFeSCAN_Ser: TIntegerField
      FieldName = 'MDFeSCAN_Ser'
    end
    object QrOpcoesMDFeMDFeSCAN_nMDF: TIntegerField
      FieldName = 'MDFeSCAN_nMDF'
    end
    object QrOpcoesMDFeMDFe_indFinalCpl: TSmallintField
      FieldName = 'MDFe_indFinalCpl'
    end
    object QrOpcoesMDFeTZD_UTC_Auto_TermoAceite: TIntegerField
      FieldName = 'TZD_UTC_Auto_TermoAceite'
    end
    object QrOpcoesMDFeTZD_UTC_Auto_DataHoraAceite: TDateTimeField
      FieldName = 'TZD_UTC_Auto_DataHoraAceite'
    end
    object QrOpcoesMDFeTZD_UTC_Auto_UsuarioAceite: TIntegerField
      FieldName = 'TZD_UTC_Auto_UsuarioAceite'
    end
    object QrOpcoesMDFeTZD_UTC_Auto: TSmallintField
      FieldName = 'TZD_UTC_Auto'
    end
    object QrOpcoesMDFeTZD_UTC: TFloatField
      FieldName = 'TZD_UTC'
    end
    object QrOpcoesMDFehVeraoAsk: TDateField
      FieldName = 'hVeraoAsk'
    end
    object QrOpcoesMDFehVeraoIni: TDateField
      FieldName = 'hVeraoIni'
    end
    object QrOpcoesMDFehVeraoFim: TDateField
      FieldName = 'hVeraoFim'
    end
    object QrOpcoesMDFeParamsMDFe: TIntegerField
      FieldName = 'ParamsMDFe'
    end
    object QrOpcoesMDFeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
end
