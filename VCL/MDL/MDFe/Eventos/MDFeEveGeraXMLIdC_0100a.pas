unit MDFeEveGeraXMLIdC_0100a;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, UnMLAGeral, Variants, Math,
  IBCustomDataSet, IBQuery, Clipbrd, DB, DBClient, dmkGeral, UrlMon,
  InvokeRegistry, SOAPHTTPClient, CAPICOM_TLB, SOAPHTTPTrans, JwaWinCrypt,
  WinInet, XMLDoc, xmldom, XMLIntf, msxmldom, MSXML2_TLB, UnDmkProcFunc,
  UnMDFe_PF, UnXXe_PF,
  // Inclusao de Condutor
  evIncCondutorMDFe_v100, eventoMDFe_v100;

const
  // XML
  sXML_Version        = '1.0';
  sXML_Encoding       = 'UTF-8';
  ENCODING_UTF8 = '?xml version="1.0" encoding="UTF-8"?';
  ENCODING_UTF8_STD = '?xml version="1.0" encoding="UTF-8" standalone="no"?';
  NAME_SPACE = 'xmlns="http://www.portalfiscal.inf.br/cte"';
  sCampoNulo = '#NULL#';
  ///
  ///
  ///
  verEventosIdC_Layout        = '1.00'; // Evento - Inclusao de condutor - Vers�o do layout
  verEventosIdC_Evento        = '1.00'; // Evento - Inclusao de condutor - Vers�o do Evento
  verEventosIdC_IdCerr        = '1.00'; // Evento - Inclusao de condutor - Vers�o da Inclusao de Condutor

type
  TMDFeEveGeraXMLIdC_0100a = class(TObject)
  private
    {private declaration}
  public
    {public declaration}
    function  CriarDocumentoEveIdC(const FatID, FatNum, Empresa: Integer; ID:
              String; cOrgao, tpAmb: Integer; CNPJ, chMDFe: String; dhEvento:
              TDateTime; tpEvento, nSeqEvento: Integer; const xNome, CPF:
              String; var XMLAssinado: String; const LaAviso1, LaAviso2:
              TLabel): Boolean;
    function  GeraXML_IdC(const xNome, CPF: String; var XML: String;
              const LaAviso1, LaAviso2: TLabel): Boolean;
  end;

var
  UnMDFeEveGeraXMLIdC_0100a: TMDFeEveGeraXMLIdC_0100a;

implementation

uses ModuleMDFe_0000;

var
  //  ACBr
  //CertStore     : IStore3;
  //CertStoreMem  : IStore3;
  //PrivateKey    : IPrivateKey;
  //Certs         : ICertificates2;
  Cert          : ICertificate2;
  //NumCertCarregado : String;
  // XML  IXMLEvIdCMDFe
  EveXML: IXMLTEvento;
  IdCXML: IXMLEvIncCondutorMDFe;
  arqXML: TXMLDocument;
  //
  FLaAviso1: TLabel;
  FLaAviso2: TLabel;
  FFatID, FFatNum, FEmpresa: Integer;

{ TMDFeEveGeraXMLIdC_0100a }

function TMDFeEveGeraXMLIdC_0100a.CriarDocumentoEveIdC(const FatID, FatNum,
  Empresa: Integer; ID: String; cOrgao, tpAmb: Integer; CNPJ, chMDFe: String;
  dhEvento: TDateTime; tpEvento, nSeqEvento: Integer; const xNome, CPF: String;
  var XMLAssinado: String; const LaAviso1, LaAviso2: TLabel): Boolean;
var
  //strChaveAcesso,
  NumeroSerial: String;
  //DadosTxt,
  Texto, XMLAny: String;
  AnyNode: IXMLNode;
begin
  Result := False;
  FLaAviso1 := LaAviso1;
  FLaAviso2 := LaAviso2;
  //
  FFatID   := FatID;
  FFatNum  := FatNum;
  FEmpresa := Empresa;
  //
  if GeraXML_IdC(xNome, CPF, XMLAny, LaAviso1, LaAviso2) then
  begin
    (* Criando o Documento XML e Gravando cabe�alho... *)
    arqXML := TXMLDocument.Create(nil);
    arqXML.Active := False;
    arqXML.FileName := '';
    EveXML := GetEvento(arqXML);
    arqXML.Version := sXML_Version;
    arqXML.Encoding := sXML_Encoding;
    (*
    arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
    arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
    arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/nfe/enviMDFe_v1.12.xsd';
    *)

    EveXML.versao := verEventosIdC_Layout;
    EveXML.InfEvento.Id := Id;
    EveXML.InfEvento.COrgao := Geral.FFN(cOrgao, 2);
    EveXML.InfEvento.TpAmb := Geral.FF0(tpAmb);
    EveXML.InfEvento.CNPJ := Geral.SoNumero_TT(CNPJ);
    EveXML.InfEvento.ChMDFe := Geral.SoNumero_TT(chMDFe);
    //EveXML.InfEvento.DhEvento := dmkPF.FDT_XXe_UTC(dhEvento, TZD_UTC);
    EveXML.InfEvento.DhEvento := FormatDateTime('YYYY-MM-DD"T"HH:NN:SS', dhEvento);
    EveXML.InfEvento.TpEvento := Geral.FF0(tpEvento);
    EveXML.InfEvento.NSeqEvento := Geral.FF0(nSeqEvento);
    //EveXML.InfEvento.VerEvento := verEventosCan_Evento;
    EveXML.InfEvento.DetEvento.VersaoEvento := verEventosIdC_IdCerr;
    AnyNode := EveXML.InfEvento.DetEvento.AddChild('_');
    AnyNode.Text := '';
    //

    Texto := EveXML.XML;
    Texto := Geral.Substitui(Texto, '<_></_>', XMLAny);
    Texto := StringReplace(Texto, #10, '', [rfReplaceAll] );
    Texto := StringReplace(Texto, #13, '', [rfReplaceAll] );

    // Assinar!
    NumeroSerial := DmMDFe_0000.QrFilialMDFeSerNum.Value;
    if XXe_PF.ObtemCertificado(NumeroSerial, Cert) then
      Result := MDFe_PF.AssinarMSXML(Texto, Cert, XMLAssinado);
    arqXML := nil;
  end else
    Geral.MB_Erro('N�o foi possivel gerar o XML any!');
end;

function TMDFeEveGeraXMLIdC_0100a.GeraXML_IdC(const xNome, CPF: String;
  var XML: String; const LaAviso1, LaAviso2: TLabel): Boolean;
var
  //strChaveAcesso,
  NumeroSerial: String;
  //DadosTxt,
  Texto: String;
begin
  Result := False;
  FLaAviso1 := LaAviso1;
  FLaAviso2 := LaAviso2;
  //
(* Criando o Documento XML e Gravando cabe�alho... *)
  arqXML := TXMLDocument.Create(nil);
  arqXML.Active := False;
  arqXML.FileName := '';
  IdCXML := GetevIncCondutorMDFe(arqXML);
  arqXML.Version := sXML_Version;
  arqXML.Encoding := sXML_Encoding;
  (*
  arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/1000/09/xmldsig#';
  arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/1001/XMLSchema-instance';
  arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.portalfiscal.inf.br/cte/enviMDFe_v1.12.xsd';
  *)

  //IdCXML.versao := verEventosIdC_Layout;
  IdCXML.DescEvento := 'Inclusao Condutor';
  IdCXML.Condutor.XNome      := xNome;
  IdCXML.Condutor.CPF        := Geral.SoNumero_TT(CPF);
  //

  Texto := IdCXML.XML;
  Texto := StringReplace(Texto, #10, '', [rfReplaceAll] );
  Texto := StringReplace(Texto, #13, '', [rfReplaceAll] );

  XML := Geral.Substitui(Texto, ' xmlns="http://www.portalfiscal.inf.br/mdfe"', '');
  XML := Geral.Substitui(Texto, slineBreak, '');
  //
  arqXML := nil;
  Result := XML <> '';
end;

end.
