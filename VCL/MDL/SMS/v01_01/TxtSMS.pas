unit TxtSMS;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkCheckGroup, UnDmkEnums;

type
  TFmTxtSMS = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrTxtSMS: TmySQLQuery;
    DsTxtSMS: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrTxtSMSCodigo: TIntegerField;
    QrTxtSMSNome: TWideStringField;
    QrTxtSMSFoneSorce: TWideStringField;
    QrTxtSMSPIN: TWideStringField;
    QrTxtSMSPorta: TWideStringField;
    QrTxtSMSDiasSem: TIntegerField;
    QrTxtSMSAMFim: TTimeField;
    QrTxtSMSPMIni: TTimeField;
    QrTxtSMSPMFim: TTimeField;
    QrTxtSMSLk: TIntegerField;
    QrTxtSMSDataCad: TDateField;
    QrTxtSMSDataAlt: TDateField;
    QrTxtSMSUserCad: TIntegerField;
    QrTxtSMSUserAlt: TIntegerField;
    QrTxtSMSAlterWeb: TSmallintField;
    QrTxtSMSAtivo: TSmallintField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    dmkDBCheckGroup1: TdmkDBCheckGroup;
    GroupBox1: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    CGDiasSem: TdmkCheckGroup;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    GroupBox6: TGroupBox;
    Label17: TLabel;
    Label18: TLabel;
    EdPIN: TdmkEdit;
    EdPorta: TdmkEdit;
    EdAMIni: TdmkEdit;
    EdAMFim: TdmkEdit;
    EdPMIni: TdmkEdit;
    EdPMFim: TdmkEdit;
    EdFoneSorce: TdmkEdit;
    SBPorta: TSpeedButton;
    QrTxtSMSAMIni: TTimeField;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label8: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    GroupBox3: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    QrTxtSMSAMIni_TXT: TWideStringField;
    QrTxtSMSAMFim_TXT: TWideStringField;
    QrTxtSMSPMIni_TXT: TWideStringField;
    QrTxtSMSPMFim_TXT: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTxtSMSAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTxtSMSBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SBPortaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmTxtSMS: TFmTxtSMS;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, COMSelPorta;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTxtSMS.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTxtSMS.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTxtSMSCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTxtSMS.DefParams;
begin
  VAR_GOTOTABELA := 'txtsms';
  VAR_GOTOMYSQLTABLE := QrTxtSMS;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *, ');
  VAR_SQLx.Add('IF(AMIni <= "00:00:00", "00:00:00", DATE_FORMAT(AMIni, " %H:%i:%S")) AMIni_TXT, ');
  VAR_SQLx.Add('IF(AMFim <= "00:00:00", "00:00:00", DATE_FORMAT(AMFim, " %H:%i:%S")) AMFim_TXT, ');
  VAR_SQLx.Add('IF(PMIni <= "00:00:00", "00:00:00", DATE_FORMAT(PMIni, " %H:%i:%S")) PMIni_TXT, ');
  VAR_SQLx.Add('IF(PMFim <= "00:00:00", "00:00:00", DATE_FORMAT(PMFim, " %H:%i:%S")) PMFim_TXT ');
  VAR_SQLx.Add('FROM txtsms ');
  VAR_SQLx.Add('WHERE Codigo > 0 ');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmTxtSMS.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTxtSMS.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTxtSMS.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTxtSMS.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTxtSMS.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTxtSMS.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTxtSMS.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTxtSMS.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTxtSMS.BtAlteraClick(Sender: TObject);
begin
  if (QrTxtSMS.State <> dsInactive) and (QrTxtSMS.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrTxtSMS, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'txtsms');
    //
    EdAMIni.ValueVariant := QrTxtSMSAMIni_TXT.Value;
    EdAMFim.ValueVariant := QrTxtSMSAMFim_TXT.Value;
    EdPMIni.ValueVariant := QrTxtSMSPMIni_TXT.Value;
    EdPMFim.ValueVariant := QrTxtSMSPMFim_TXT.Value;
  end;
end;

procedure TFmTxtSMS.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTxtSMSCodigo.Value;
  Close;
end;

procedure TFmTxtSMS.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('txtsms', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrTxtSMSCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'txtsms', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmTxtSMS.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'txtsms', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmTxtSMS.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrTxtSMS, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'txtsms');
  //
  EdAMIni.ValueVariant := '00:00:00';
  EdAMFim.ValueVariant := '00:00:00';
  EdPMIni.ValueVariant := '00:00:00';
  EdPMFim.ValueVariant := '00:00:00';
end;

procedure TFmTxtSMS.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmTxtSMS.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTxtSMSCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTxtSMS.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmTxtSMS.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTxtSMS.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
  //LaRegistro.Caption := GOTOy.CodUsu(QrTxtSMSCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTxtSMS.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTxtSMS.QrTxtSMSAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTxtSMS.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTxtSMS.SbQueryClick(Sender: TObject);
begin
  LocCod(QrTxtSMSCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'txtsms', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmTxtSMS.SBPortaClick(Sender: TObject);
var
  OK: Boolean;
  Txt: String;
begin
  if DBCheck.CriaFm(TFmCOMSelPorta, FmCOMSelPorta, afmoNegarComAviso) then
  begin
    FmCOMSelPorta.ShowModal;
    OK := FmCOMSelPorta.FResult;
    Txt := FmCOMSelPorta.EdDevice.Text;
    FmCOMSelPorta.Destroy;
    if OK then
      EdPorta.Text := Txt;
  end;
end;

procedure TFmTxtSMS.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTxtSMS.QrTxtSMSBeforeOpen(DataSet: TDataSet);
begin
  QrTxtSMSCodigo.DisplayFormat := FFormatFloat;
end;

end.

