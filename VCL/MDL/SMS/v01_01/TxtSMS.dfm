object FmTxtSMS: TFmTxtSMS
  Left = 368
  Top = 194
  Caption = 'SMS-TEXTO-001 :: Cadastro de Textos de SMS'
  ClientHeight = 412
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 316
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBConfirma: TGroupBox
      Left = 0
      Top = 253
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 217
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 56
        Height = 13
        Caption = 'Texto SMS:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label12: TLabel
        Left = 16
        Top = 56
        Width = 104
        Height = 13
        Caption = 'Telefone que enviar'#225':'
        FocusControl = DBEdit1
      end
      object Label13: TLabel
        Left = 132
        Top = 56
        Width = 21
        Height = 13
        Caption = 'PIN:'
        FocusControl = DBEdit2
      end
      object Label14: TLabel
        Left = 196
        Top = 56
        Width = 344
        Height = 13
        Caption = 
          'Porta de comunica'#231#227'o do (no) servidor de SMS com o aparelho de S' +
          'MS:'
        FocusControl = DBEdit3
      end
      object SBPorta: TSpeedButton
        Left = 748
        Top = 71
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SBPortaClick
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 693
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object CGDiasSem: TdmkCheckGroup
        Left = 16
        Top = 96
        Width = 753
        Height = 49
        Caption = ' Dias da semana permitidos para envio da mensagem: '
        Columns = 7
        Items.Strings = (
          'Domingo'
          'Segunda'
          'Ter'#231'a'
          'Quarta'
          'Quinta'
          'Sexta'
          'S'#225'bado')
        TabOrder = 5
        QryCampo = 'DiasSem'
        UpdCampo = 'DiasSem'
        UpdType = utYes
        Value = 0
        OldValor = 0
      end
      object GroupBox4: TGroupBox
        Left = 16
        Top = 148
        Width = 753
        Height = 69
        Caption = 
          ' Turnos de hor'#225'rios permitidos para envio da mensagem nos dias d' +
          'a semana selecionados: '
        TabOrder = 6
        object GroupBox5: TGroupBox
          Left = 2
          Top = 15
          Width = 374
          Height = 52
          Align = alLeft
          Caption = ' Turno 1: '
          TabOrder = 0
          object Label15: TLabel
            Left = 12
            Top = 24
            Width = 33
            Height = 13
            Caption = 'In'#237'cio: '
          end
          object Label16: TLabel
            Left = 188
            Top = 24
            Width = 41
            Height = 13
            Caption = 'T'#233'rmino:'
          end
          object EdAMIni: TdmkEdit
            Left = 52
            Top = 20
            Width = 80
            Height = 21
            TabOrder = 0
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfLong
            Texto = '00:00:00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdAMFim: TdmkEdit
            Left = 240
            Top = 20
            Width = 80
            Height = 21
            TabOrder = 1
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfLong
            Texto = '00:00:00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
        end
        object GroupBox6: TGroupBox
          Left = 376
          Top = 15
          Width = 375
          Height = 52
          Align = alClient
          Caption = ' Turno 2: '
          TabOrder = 1
          object Label17: TLabel
            Left = 12
            Top = 24
            Width = 33
            Height = 13
            Caption = 'In'#237'cio: '
          end
          object Label18: TLabel
            Left = 188
            Top = 24
            Width = 41
            Height = 13
            Caption = 'T'#233'rmino:'
          end
          object EdPMIni: TdmkEdit
            Left = 52
            Top = 20
            Width = 80
            Height = 21
            TabOrder = 0
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfLong
            Texto = '00:00:00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdPMFim: TdmkEdit
            Left = 240
            Top = 20
            Width = 80
            Height = 21
            TabOrder = 1
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfLong
            Texto = '00:00:00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
        end
      end
      object EdPIN: TdmkEdit
        Left = 132
        Top = 72
        Width = 60
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'PIN'
        UpdCampo = 'PIN'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdPorta: TdmkEdit
        Left = 196
        Top = 72
        Width = 549
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Porta'
        UpdCampo = 'Porta'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdFoneSorce: TdmkEdit
        Left = 16
        Top = 72
        Width = 112
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtTelLongo
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'FoneSorce'
        UpdCampo = 'FoneSorce'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 316
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 217
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 16
        Top = 56
        Width = 104
        Height = 13
        Caption = 'Telefone que enviar'#225':'
        FocusControl = DBEdit1
      end
      object Label4: TLabel
        Left = 132
        Top = 56
        Width = 21
        Height = 13
        Caption = 'PIN:'
        FocusControl = DBEdit2
      end
      object Label5: TLabel
        Left = 196
        Top = 56
        Width = 344
        Height = 13
        Caption = 
          'Porta de comunica'#231#227'o do (no) servidor de SMS com o aparelho de S' +
          'MS:'
        FocusControl = DBEdit3
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsTxtSMS
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 693
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsTxtSMS
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 16
        Top = 72
        Width = 112
        Height = 21
        DataField = 'FoneSorce'
        DataSource = DsTxtSMS
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 132
        Top = 72
        Width = 60
        Height = 21
        DataField = 'PIN'
        DataSource = DsTxtSMS
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 196
        Top = 72
        Width = 573
        Height = 21
        DataField = 'Porta'
        DataSource = DsTxtSMS
        TabOrder = 4
      end
      object dmkDBCheckGroup1: TdmkDBCheckGroup
        Left = 16
        Top = 96
        Width = 753
        Height = 49
        Caption = ' Dias da semana permitidos para envio da mensagem: '
        Columns = 7
        DataField = 'DiasSem'
        DataSource = DsTxtSMS
        Items.Strings = (
          'Domingo'
          'Segunda'
          'Ter'#231'a'
          'Quarta'
          'Quinta'
          'Sexta'
          'S'#225'bado')
        ParentBackground = False
        TabOrder = 5
      end
      object GroupBox1: TGroupBox
        Left = 16
        Top = 148
        Width = 753
        Height = 69
        Caption = 
          ' Turnos de hor'#225'rios permitidos para envio da mensagem nos dias d' +
          'a semana selecionados: '
        TabOrder = 6
        object GroupBox2: TGroupBox
          Left = 2
          Top = 15
          Width = 374
          Height = 52
          Align = alLeft
          Caption = ' Turno 1: '
          TabOrder = 0
          object Label6: TLabel
            Left = 12
            Top = 24
            Width = 33
            Height = 13
            Caption = 'In'#237'cio: '
          end
          object Label8: TLabel
            Left = 188
            Top = 24
            Width = 41
            Height = 13
            Caption = 'T'#233'rmino:'
          end
          object DBEdit4: TDBEdit
            Left = 48
            Top = 20
            Width = 134
            Height = 21
            DataField = 'AMIni_TXT'
            DataSource = DsTxtSMS
            TabOrder = 0
          end
          object DBEdit5: TDBEdit
            Left = 232
            Top = 20
            Width = 134
            Height = 21
            DataField = 'AMFim_TXT'
            DataSource = DsTxtSMS
            TabOrder = 1
          end
        end
        object GroupBox3: TGroupBox
          Left = 376
          Top = 15
          Width = 375
          Height = 52
          Align = alClient
          Caption = ' Turno 2: '
          TabOrder = 1
          object Label10: TLabel
            Left = 12
            Top = 24
            Width = 33
            Height = 13
            Caption = 'In'#237'cio: '
          end
          object Label11: TLabel
            Left = 188
            Top = 24
            Width = 41
            Height = 13
            Caption = 'T'#233'rmino:'
          end
          object DBEdit6: TDBEdit
            Left = 44
            Top = 20
            Width = 134
            Height = 21
            DataField = 'PMIni_TXT'
            DataSource = DsTxtSMS
            TabOrder = 0
          end
          object DBEdit7: TDBEdit
            Left = 232
            Top = 20
            Width = 134
            Height = 21
            DataField = 'PMFim_TXT'
            DataSource = DsTxtSMS
            TabOrder = 1
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 252
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 87
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 339
        Height = 32
        Caption = 'Cadastro de Textos de SMS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 339
        Height = 32
        Caption = 'Cadastro de Textos de SMS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 339
        Height = 32
        Caption = 'Cadastro de Textos de SMS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrTxtSMS: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrTxtSMSBeforeOpen
    AfterOpen = QrTxtSMSAfterOpen
    SQL.Strings = (
      'SELECT *,'
      
        'IF(AMIni <= "00:00:00", "00:00:00", DATE_FORMAT(AMIni, " %H:%i:%' +
        'S")) AMIni_TXT,'
      
        'IF(AMFim <= "00:00:00", "00:00:00", DATE_FORMAT(AMFim, " %H:%i:%' +
        'S")) AMFim_TXT,'
      
        'IF(PMIni <= "00:00:00", "00:00:00", DATE_FORMAT(PMIni, " %H:%i:%' +
        'S")) PMIni_TXT,'
      
        'IF(PMFim <= "00:00:00", "00:00:00", DATE_FORMAT(PMFim, " %H:%i:%' +
        'S")) PMFim_TXT'
      'FROM txtsms')
    Left = 64
    Top = 64
    object QrTxtSMSCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTxtSMSNome: TWideStringField
      FieldName = 'Nome'
      Size = 140
    end
    object QrTxtSMSFoneSorce: TWideStringField
      FieldName = 'FoneSorce'
      Size = 18
    end
    object QrTxtSMSPIN: TWideStringField
      FieldName = 'PIN'
    end
    object QrTxtSMSPorta: TWideStringField
      FieldName = 'Porta'
      Size = 255
    end
    object QrTxtSMSDiasSem: TIntegerField
      FieldName = 'DiasSem'
    end
    object QrTxtSMSAMFim: TTimeField
      FieldName = 'AMFim'
    end
    object QrTxtSMSPMIni: TTimeField
      FieldName = 'PMIni'
    end
    object QrTxtSMSPMFim: TTimeField
      FieldName = 'PMFim'
    end
    object QrTxtSMSLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTxtSMSDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTxtSMSDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTxtSMSUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTxtSMSUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTxtSMSAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTxtSMSAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTxtSMSAMIni: TTimeField
      FieldName = 'AMIni'
    end
    object QrTxtSMSAMIni_TXT: TWideStringField
      FieldName = 'AMIni_TXT'
      Size = 8
    end
    object QrTxtSMSAMFim_TXT: TWideStringField
      FieldName = 'AMFim_TXT'
      Size = 8
    end
    object QrTxtSMSPMIni_TXT: TWideStringField
      FieldName = 'PMIni_TXT'
      Size = 8
    end
    object QrTxtSMSPMFim_TXT: TWideStringField
      FieldName = 'PMFim_TXT'
      Size = 8
    end
  end
  object DsTxtSMS: TDataSource
    DataSet = QrTxtSMS
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
end
