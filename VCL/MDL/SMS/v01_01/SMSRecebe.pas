unit SMSRecebe;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmSMSRecebe = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrSMSRecebe: TmySQLQuery;
    DsSMSRecebe: TDataSource;
    QrSMSRecebeCodigo: TLargeintField;
    QrSMSRecebeNome: TWideStringField;
    QrSMSRecebeFromAddr: TWideStringField;
    QrSMSRecebeDataObtida: TDateTimeField;
    QrSMSRecebeDataLida: TDateTimeField;
    QrSMSRecebeUserLida: TIntegerField;
    DBGrid1: TDBGrid;
    RGExibir: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGExibirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ReopenSMSRecebe(Codigo: Integer);
  end;

  var
  FmSMSRecebe: TFmSMSRecebe;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

procedure TFmSMSRecebe.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSMSRecebe.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if RGExibir.Itemindex = -1 then
    RGExibir.Itemindex := 0;
end;

procedure TFmSMSRecebe.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmSMSRecebe.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSMSRecebe.ReopenSMSRecebe(Codigo: Integer);
var
  SQL: String;
begin
  case RGExibir.ItemIndex of
    0: SQL := 'WHERE DataLida < "1900-01-01"';
    1: SQL := 'WHERE DataLida >= "1900-01-01"';
    else SQL := '';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrSMSRecebe, Dmod.MyDB, [
  'SELECT * ',
  'FROM smsrecebe ',
  SQL,
  'ORDER BY DataObtida DESC ',
  '']);
  //
  QrSMSRecebe.Locate('Codigo', Codigo, []);
end;

procedure TFmSMSRecebe.RGExibirClick(Sender: TObject);
begin
  ReopenSMSRecebe(0);
end;

end.
