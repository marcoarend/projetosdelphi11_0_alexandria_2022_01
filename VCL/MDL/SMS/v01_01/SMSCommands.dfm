object FmSMSCommands: TFmSMSCommands
  Left = 339
  Top = 185
  Caption = 'SMS-TEXTO-003 :: Gerencimento de Aparelhos GSM'
  ClientHeight = 492
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 407
        Height = 32
        Caption = 'Gerencimento de Aparelhos GSM'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 407
        Height = 32
        Caption = 'Gerencimento de Aparelhos GSM'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 407
        Height = 32
        Caption = 'Gerencimento de Aparelhos GSM'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 330
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 330
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 330
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 207
          Height = 313
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object DBGrid1: TDBGrid
            Left = 0
            Top = 105
            Width = 207
            Height = 157
            Align = alLeft
            DataSource = DsDevices
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Porta'
                Width = 134
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PIN'
                Width = 32
                Visible = True
              end>
          end
          object RGComando: TRadioGroup
            Left = 0
            Top = 0
            Width = 207
            Height = 105
            Align = alTop
            Caption = ' Tipo de comando'
            ItemIndex = 2
            Items.Strings = (
              'Envio de mensagem'
              'Ler mensagens recebidas'
              'Envio de comando USSD'
              'Ler relat'#243'rios')
            TabOrder = 1
            OnClick = RGComandoClick
            ExplicitLeft = 37
            ExplicitTop = 59
            ExplicitWidth = 205
          end
          object GBUSSD: TGroupBox
            Left = 0
            Top = 262
            Width = 207
            Height = 51
            Align = alBottom
            Caption = ' Comando USSD  a ser enviado: '
            TabOrder = 2
            ExplicitLeft = 9
            ExplicitTop = 300
            ExplicitWidth = 205
            object Panel6: TPanel
              Left = 2
              Top = 15
              Width = 203
              Height = 34
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              ExplicitLeft = 1
              ExplicitTop = 268
              ExplicitWidth = 205
              ExplicitHeight = 44
              object EdComando: TEdit
                Left = 8
                Top = 4
                Width = 193
                Height = 21
                TabOrder = 0
                Text = 'AT+CUSD=1,"*544#"'
              end
            end
          end
        end
        object Panel7: TPanel
          Left = 209
          Top = 15
          Width = 573
          Height = 313
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitLeft = 2
          ExplicitWidth = 780
          ExplicitHeight = 222
          object GBSMS: TGroupBox
            Left = 0
            Top = 0
            Width = 573
            Height = 64
            Align = alTop
            Caption = ' Envio de SMS: '
            TabOrder = 0
            Visible = False
            ExplicitLeft = 1
            ExplicitTop = 1
            ExplicitWidth = 778
            object Panel8: TPanel
              Left = 2
              Top = 15
              Width = 569
              Height = 47
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              ExplicitTop = -47
              ExplicitWidth = 774
              ExplicitHeight = 152
              object Label10: TLabel
                Left = 144
                Top = 4
                Width = 55
                Height = 13
                Caption = 'Mensagem:'
              end
              object Label5: TLabel
                Left = 4
                Top = 4
                Width = 120
                Height = 13
                Caption = 'Fone destino (com DDD):'
              end
              object EdDestino: TdmkEdit
                Left = 4
                Top = 20
                Width = 137
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
              object EdMensagem: TdmkEdit
                Left = 145
                Top = 20
                Width = 320
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'Teste ...'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'Teste ...'
              end
            end
          end
          object Memo5: TMemo
            Left = 0
            Top = 64
            Width = 573
            Height = 249
            Align = alClient
            TabOrder = 1
            ExplicitLeft = 209
            ExplicitTop = 15
            ExplicitHeight = 313
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 378
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 422
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Envia'
        TabOrder = 0
        OnClick = BtOKClick
        NumGlyphs = 2
      end
    end
  end
  object QrDevices: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT sms.Porta, SMS.PIN '
      'FROM txtsms sms')
    Left = 388
    Top = 252
    object QrDevicesPorta: TWideStringField
      FieldName = 'Porta'
      Size = 255
    end
    object QrDevicesPIN: TWideStringField
      FieldName = 'PIN'
    end
  end
  object DsDevices: TDataSource
    DataSet = QrDevices
    Left = 416
    Top = 252
  end
end
