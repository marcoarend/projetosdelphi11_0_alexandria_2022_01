unit SMSCommands;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmSMSCommands = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrDevices: TmySQLQuery;
    DsDevices: TDataSource;
    QrDevicesPorta: TWideStringField;
    QrDevicesPIN: TWideStringField;
    Panel5: TPanel;
    DBGrid1: TDBGrid;
    RGComando: TRadioGroup;
    Panel7: TPanel;
    GBSMS: TGroupBox;
    Panel8: TPanel;
    Label10: TLabel;
    Label5: TLabel;
    EdDestino: TdmkEdit;
    EdMensagem: TdmkEdit;
    Memo5: TMemo;
    GBUSSD: TGroupBox;
    Panel6: TPanel;
    EdComando: TEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGComandoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmSMSCommands: TFmSMSCommands;

implementation

uses UnMyObjects, Module, ModCobranca, DmkDAC_PF;

{$R *.DFM}

procedure TFmSMSCommands.BtOKClick(Sender: TObject);
begin
  if DmModCobranca.AparelhoGSM_Conecta(Memo5,
  QrDevicesPorta.Value, QrDevicesPIN.Value) then
  begin
    try
      case RGComando.ItemIndex of
        0: // Envio de SMS
          DmModCobranca.AparelhoGSM_EnviaSMS(EdDestino.Text, EdMensagem.Text, Memo5);
        1: // Ler SMSs do SIM card
          DmModCobranca.AparelhoGSM_RecebeSMSs(Memo5);
        2: // Envio de "USSD" - Unstructured Supplementary Services Data
          //StrCommand := 'AT+CUSD=1,"*544#"';
          DmModCobranca.AparelhoGSM_ComandoAT(EdComando.Text, Memo5);
        3: DmModCobranca.AparelhoGSM_RecebeRelatorios(Memo5)
      end;
    finally
      DmModCobranca.AparelhoGSM_Desconecta();
    end;
  end;
end;

procedure TFmSMSCommands.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSMSCommands.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSMSCommands.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrDevices, Dmod.MyDB, [
  'SELECT DISTINCT sms.Porta, SMS.PIN ',
  'FROM txtsms sms ',
  '']);
end;

procedure TFmSMSCommands.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSMSCommands.RGComandoClick(Sender: TObject);
begin
  GBSMS.Visible := RGComando.ItemIndex = 0;
  GBUSSD.Visible := RGComando.ItemIndex = 2;
end;

end.
