unit AxSms_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 8291 $
// File generated on 08/01/2013 17:49:53 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Program Files\Common Files\ActiveXperts\AxSms32.dll (1)
// LIBID: {B00F4729-F464-4690-88A0-E059489AF063}
// LCID: 0
// Helpfile: 
// HelpString: ActiveXperts SMS Component 6.2 Type Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
// Errors:
//   Hint: Parameter 'In' of IHttp.UrlEncode changed to 'In_'
//   Hint: Parameter 'In' of IHttp.Base64Encode changed to 'In_'
//   Hint: Parameter 'In' of IHttp.HexEncode changed to 'In_'
//   Hint: Parameter 'Type' of ISmpp.Bind changed to 'Type_'
//   Error creating palette bitmap of (TConstants) : Server C:\Program Files\Common Files\ActiveXperts\AxSms32.dll contains no icons
//   Error creating palette bitmap of (TMessage) : Server C:\Program Files\Common Files\ActiveXperts\AxSms32.dll contains no icons
//   Error creating palette bitmap of (TTlv) : Server C:\Program Files\Common Files\ActiveXperts\AxSms32.dll contains no icons
//   Error creating palette bitmap of (TGsmDeliveryReport) : Server C:\Program Files\Common Files\ActiveXperts\AxSms32.dll contains no icons
//   Error creating palette bitmap of (TDialup) : Server C:\Program Files\Common Files\ActiveXperts\AxSms32.dll contains no icons
//   Error creating palette bitmap of (TGsm) : Server C:\Program Files\Common Files\ActiveXperts\AxSms32.dll contains no icons
//   Error creating palette bitmap of (THttp) : Server C:\Program Files\Common Files\ActiveXperts\AxSms32.dll contains no icons
//   Error creating palette bitmap of (TSnpp) : Server C:\Program Files\Common Files\ActiveXperts\AxSms32.dll contains no icons
//   Error creating palette bitmap of (TSmpp) : Server C:\Program Files\Common Files\ActiveXperts\AxSms32.dll contains no icons
//   Error creating palette bitmap of (TSmppServer) : Server C:\Program Files\Common Files\ActiveXperts\AxSms32.dll contains no icons
//   Error creating palette bitmap of (TSmppSession) : Server C:\Program Files\Common Files\ActiveXperts\AxSms32.dll contains no icons
//   Error creating palette bitmap of (TTemplateWapPush) : Server C:\Program Files\Common Files\ActiveXperts\AxSms32.dll contains no icons
//   Error creating palette bitmap of (TTemplatevCard) : Server C:\Program Files\Common Files\ActiveXperts\AxSms32.dll contains no icons
//   Error creating palette bitmap of (TDemoAccount) : Server C:\Program Files\Common Files\ActiveXperts\AxSms32.dll contains no icons
// ************************************************************************ //
// *************************************************************************//
// NOTE:                                                                      
// Items guarded by $IFDEF_LIVE_SERVER_AT_DESIGN_TIME are used by properties  
// which return objects that may need to be explicitly created via a function 
// call prior to any access via the property. These items have been disabled  
// in order to prevent accidental use from within the object inspector. You   
// may enable them by defining LIVE_SERVER_AT_DESIGN_TIME or by selectively   
// removing them from the $IFDEF blocks. However, such items must still be    
// programmatically created via a method of the appropriate CoClass before    
// they can be used.                                                          
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  AxSmsMajorVersion = 1;
  AxSmsMinorVersion = 0;

  LIBID_AxSms: TGUID = '{B00F4729-F464-4690-88A0-E059489AF063}';

  IID_IConstants: TGUID = '{7D47E1D0-4B72-4A5D-B174-A0E82AF73892}';
  CLASS_Constants: TGUID = '{41A6CF7D-3C5B-40B7-82AC-3AB3BBBF1A84}';
  IID_IMessage: TGUID = '{DFD486FC-45B4-49AB-AD5A-98256AE057FC}';
  CLASS_Message: TGUID = '{7524134C-0186-43C6-958C-2C3A49FE40FC}';
  IID_ITlv: TGUID = '{4687CEE3-3966-46A2-AEE3-5A747F455B84}';
  CLASS_Tlv: TGUID = '{461B017B-C733-4145-A3AA-040B3A1E86D5}';
  IID_IGsmDeliveryReport: TGUID = '{6962C3D8-88FE-4660-A4EC-901A244A57D2}';
  CLASS_GsmDeliveryReport: TGUID = '{035EC473-4171-4D8C-BB09-000F1BBB79B5}';
  IID_IDialup: TGUID = '{3E9851DF-D82F-4806-8716-769C015C23F2}';
  CLASS_Dialup: TGUID = '{265F828D-DB37-4676-8E3E-B73B5A653A48}';
  IID_IGsm: TGUID = '{B25B188D-42A8-45EF-887F-653CBC9E234B}';
  CLASS_Gsm: TGUID = '{4AB18312-548A-4607-97E1-3DE7FC27CDD2}';
  IID_IHttp: TGUID = '{1825A7C6-DBAB-4B99-AAA1-FDEDF36A4982}';
  CLASS_Http: TGUID = '{5092DA94-3952-4FDA-9B8E-4B387F0164E6}';
  IID_ISnpp: TGUID = '{C4AAB6D2-F834-4FE6-A4AE-B1D5794BC3B4}';
  CLASS_Snpp: TGUID = '{929AEEE5-56B2-4A67-BCC8-45E5D4FA3739}';
  IID_ISmpp: TGUID = '{9E5FC25E-613A-4951-9C8F-37923C7AE7AE}';
  CLASS_Smpp: TGUID = '{A1453018-3C02-4C22-A136-3CF9B78CEAC8}';
  IID_ISmppServer: TGUID = '{EBE10239-A8C6-46CA-BC48-0CFA6CFD835C}';
  CLASS_SmppServer: TGUID = '{6F2D0285-924E-4F76-A0EA-FC9282D4C761}';
  IID_ISmppSession: TGUID = '{3E1FF0C2-4F98-4FA2-954B-BF5730FB6CF9}';
  CLASS_SmppSession: TGUID = '{A0D488D4-C668-4346-9D44-3CD00832723E}';
  IID_ITemplateWapPush: TGUID = '{54DD400B-F0D6-42AF-964B-9F38CD57D8CC}';
  CLASS_TemplateWapPush: TGUID = '{FA1CDBE0-666B-430F-BCC5-838661432F76}';
  IID_ITemplatevCard: TGUID = '{FCB5DCC2-4DDD-465D-936F-40AEBDDD17C3}';
  CLASS_TemplatevCard: TGUID = '{72B516D0-8B84-4B92-A297-474353A2C90A}';
  IID_IDemoAccount: TGUID = '{231F4503-C639-479F-B799-4C9FCE0BDF9C}';
  CLASS_DemoAccount: TGUID = '{634B8464-5D45-4E0E-BCC7-EB04500BC159}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IConstants = interface;
  IConstantsDisp = dispinterface;
  IMessage = interface;
  IMessageDisp = dispinterface;
  ITlv = interface;
  ITlvDisp = dispinterface;
  IGsmDeliveryReport = interface;
  IGsmDeliveryReportDisp = dispinterface;
  IDialup = interface;
  IDialupDisp = dispinterface;
  IGsm = interface;
  IGsmDisp = dispinterface;
  IHttp = interface;
  IHttpDisp = dispinterface;
  ISnpp = interface;
  ISnppDisp = dispinterface;
  ISmpp = interface;
  ISmppDisp = dispinterface;
  ISmppServer = interface;
  ISmppServerDisp = dispinterface;
  ISmppSession = interface;
  ISmppSessionDisp = dispinterface;
  ITemplateWapPush = interface;
  ITemplateWapPushDisp = dispinterface;
  ITemplatevCard = interface;
  ITemplatevCardDisp = dispinterface;
  IDemoAccount = interface;
  IDemoAccountDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  Constants = IConstants;
  Message = IMessage;
  Tlv = ITlv;
  GsmDeliveryReport = IGsmDeliveryReport;
  Dialup = IDialup;
  Gsm = IGsm;
  Http = IHttp;
  Snpp = ISnpp;
  Smpp = ISmpp;
  SmppServer = ISmppServer;
  SmppSession = ISmppSession;
  TemplateWapPush = ITemplateWapPush;
  TemplatevCard = ITemplatevCard;
  DemoAccount = IDemoAccount;


// *********************************************************************//
// Interface: IConstants
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {7D47E1D0-4B72-4A5D-B174-A0E82AF73892}
// *********************************************************************//
  IConstants = interface(IDispatch)
    ['{7D47E1D0-4B72-4A5D-B174-A0E82AF73892}']
    function Get_SMPP_BIND_TRANSMITTER: Integer; safecall;
    function Get_SMPP_BIND_TRANSCEIVER: Integer; safecall;
    function Get_SMPP_BIND_RECEIVER: Integer; safecall;
    function Get_SMPP_VERSION_33: Integer; safecall;
    function Get_SMPP_VERSION_34: Integer; safecall;
    function Get_SMPP_VERSION_50: Integer; safecall;
    function Get_TON_UNKNOWN: Integer; safecall;
    function Get_TON_INTERNATIONAL: Integer; safecall;
    function Get_TON_NATIONAL: Integer; safecall;
    function Get_TON_NETWORK_SPECIFIC: Integer; safecall;
    function Get_TON_SUBSCRIBER_NUMBER: Integer; safecall;
    function Get_TON_ALPHANUMERIC: Integer; safecall;
    function Get_SMPP_TON_ABBREVIATED: Integer; safecall;
    function Get_NPI_UNKNOWN: Integer; safecall;
    function Get_NPI_ISDN: Integer; safecall;
    function Get_NPI_DATA: Integer; safecall;
    function Get_NPI_TELEX: Integer; safecall;
    function Get_NPI_NATIONAL: Integer; safecall;
    function Get_NPI_PRIVATE: Integer; safecall;
    function Get_NPI_ERMES: Integer; safecall;
    function Get_SMPP_NPI_INTERNET: Integer; safecall;
    function Get_NPI_LAND_MOBILE: Integer; safecall;
    function Get_MULTIPART_ACCEPT: Integer; safecall;
    function Get_MULTIPART_TRUNCATE: Integer; safecall;
    function Get_MULTIPART_REJECT: Integer; safecall;
    function Get_BODYFORMAT_TEXT: Integer; safecall;
    function Get_BODYFORMAT_HEX: Integer; safecall;
    function Get_SMPP_ESM_2ESME_DEFAULT: Integer; safecall;
    function Get_SMPP_ESM_2ESME_DELIVERY_RECEIPT: Integer; safecall;
    function Get_SMPP_ESM_2ESME_DELIVERY_ACK: Integer; safecall;
    function Get_SMPP_ESM_2ESME_MANUAL_ACK: Integer; safecall;
    function Get_SMPP_ESM_2ESME_CONVERSATION_ABORT: Integer; safecall;
    function Get_SMPP_ESM_2ESME_INTERMEDIATE_DELIVERY_NOTIFY: Integer; safecall;
    function Get_SMPP_ESM_2SMSC_MODE_DEFAULT: Integer; safecall;
    function Get_SMPP_ESM_2SMSC_MODE_STOREFORWARD: Integer; safecall;
    function Get_SMPP_ESM_2SMSC_MODE_DATAGRAM: Integer; safecall;
    function Get_SMPP_ESM_2SMSC_MODE_FORWARD: Integer; safecall;
    function Get_SMPP_ESM_2SMSC_TYPE_DEFAULT: Integer; safecall;
    function Get_SMPP_ESM_2SMSC_TYPE_DELIVERY_ACK: Integer; safecall;
    function Get_SMPP_ESM_2SMSC_TYPE_MANUAL_ACK: Integer; safecall;
    function Get_SMPP_ESM_2SMSC_FEAT_NOTHING: Integer; safecall;
    function Get_SMPP_ESM_2SMSC_FEAT_UDHI: Integer; safecall;
    function Get_SMPP_ESM_2SMSC_FEAT_SRP: Integer; safecall;
    function Get_SMPP_USEGSMENCODING_DISABLED: Integer; safecall;
    function Get_SMPP_USEGSMENCODING_INANDOUT: Integer; safecall;
    function Get_SMPP_USEGSMENCODING_INCOMING: Integer; safecall;
    function Get_SMPP_USEGSMENCODING_OUTGOING: Integer; safecall;
    function Get_SMPP_DATACODING_ASCII: Integer; safecall;
    function Get_SMPP_DATACODING_OCTET_UNSPEC: Integer; safecall;
    function Get_SMPP_DATACODING_LATIN: Integer; safecall;
    function Get_SMPP_DATACODING_JIS_KANJI: Integer; safecall;
    function Get_SMPP_DATACODING_CYRILLIC: Integer; safecall;
    function Get_SMPP_DATACODING_LATIN_HEBREW: Integer; safecall;
    function Get_SMPP_DATACODING_PICTOGRAM: Integer; safecall;
    function Get_SMPP_DATACODING_ISO_2022_JP: Integer; safecall;
    function Get_SMPP_DATACODING_EXTENDED_KANJI_JIS: Integer; safecall;
    function Get_SMPP_DATACODING_KS_C_5601: Integer; safecall;
    function Get_SMPP_PRIORITYFLAG_BULK: Integer; safecall;
    function Get_SMPP_PRIORITYFLAG_NORMAL: Integer; safecall;
    function Get_SMPP_PRIORITYFLAG_URGENT: Integer; safecall;
    function Get_SMPP_PRIORITYFLAG_VERY_URGENT: Integer; safecall;
    function Get_SMPP_MESSAGESTATE_AX_WAITRESP: Integer; safecall;
    function Get_SMPP_MESSAGESTATE_ENROUTE: Integer; safecall;
    function Get_SMPP_MESSAGESTATE_DELIVERED: Integer; safecall;
    function Get_SMPP_MESSAGESTATE_EXPIRED: Integer; safecall;
    function Get_SMPP_MESSAGESTATE_DELETED: Integer; safecall;
    function Get_SMPP_MESSAGESTATE_UNDELIVERABLE: Integer; safecall;
    function Get_SMPP_MESSAGESTATE_ACCEPTED: Integer; safecall;
    function Get_SMPP_MESSAGESTATE_UNKNOWN: Integer; safecall;
    function Get_SMPP_MESSAGESTATE_REJECTED: Integer; safecall;
    function Get_SMPP_MESSAGESTATE_AX_RESPERROR: Integer; safecall;
    function Get_SMPP_MESSAGESTATE_AX_NOCREDITS: Integer; safecall;
    function Get_SMPP_MESSAGESTATE_AX_RESPTO: Integer; safecall;
    function Get_SMPP_MESSAGESTATE_AX_RESPONDED: Integer; safecall;
    function Get_SMPP_SESSIONSTATE_CONNECTED: Integer; safecall;
    function Get_SMPP_SESSIONSTATE_DISCONNECTED: Integer; safecall;
    function Get_SMPP_SESSIONSTATE_BINDING: Integer; safecall;
    function Get_SMPP_SESSIONSTATE_BOUND_TX: Integer; safecall;
    function Get_SMPP_SESSIONSTATE_BOUND_RX: Integer; safecall;
    function Get_SMPP_SESSIONSTATE_BOUND_TRX: Integer; safecall;
    function Get_SMPP_TLV_DEST_ADDR_SUBUNIT: Integer; safecall;
    function Get_SMPP_TLV_DEST_NETWORK_TYPE: Integer; safecall;
    function Get_SMPP_TLV_DEST_BEARER_TYPE: Integer; safecall;
    function Get_SMPP_TLV_DEST_TELEMATICS_ID: Integer; safecall;
    function Get_SMPP_TLV_SOURCE_ADDR_SUBUNIT: Integer; safecall;
    function Get_SMPP_TLV_SOURCE_NETWORK_TYPE: Integer; safecall;
    function Get_SMPP_TLV_SOURCE_BEARER_TYPE: Integer; safecall;
    function Get_SMPP_TLV_SOURCE_TELEMATICS_ID: Integer; safecall;
    function Get_SMPP_TLV_QOS_TIME_TO_LIVE: Integer; safecall;
    function Get_SMPP_TLV_PAYLOAD_TYPE: Integer; safecall;
    function Get_SMPP_TLV_ADDITIONAL_STATUS_INFO_TEXT: Integer; safecall;
    function Get_SMPP_TLV_RECEIPTED_MESSAGE_ID: Integer; safecall;
    function Get_SMPP_TLV_MS_MSG_WAIT_FACILITIES: Integer; safecall;
    function Get_SMPP_TLV_PRIVACY_INDICATOR: Integer; safecall;
    function Get_SMPP_TLV_SOURCE_SUBADDRESS: Integer; safecall;
    function Get_SMPP_TLV_DEST_SUBADDRESS: Integer; safecall;
    function Get_SMPP_TLV_USER_MESSAGE_REFERENCE: Integer; safecall;
    function Get_SMPP_TLV_USER_RESPONSE_CODE: Integer; safecall;
    function Get_SMPP_TLV_SOURCE_PORT: Integer; safecall;
    function Get_SMPP_TLV_DESTINATION_PORT: Integer; safecall;
    function Get_SMPP_TLV_SAR_MSG_REF_NUM: Integer; safecall;
    function Get_SMPP_TLV_LANGUAGE_INDICATOR: Integer; safecall;
    function Get_SMPP_TLV_SAR_TOTAL_SEGMENTS: Integer; safecall;
    function Get_SMPP_TLV_SAR_SEGMENT_SEQNUM: Integer; safecall;
    function Get_SMPP_TLV_SC_INTERFACE_VERSION: Integer; safecall;
    function Get_SMPP_TLV_CALLBACK_NUM_PRES_IND: Integer; safecall;
    function Get_SMPP_TLV_CALLBACK_NUM_ATAG: Integer; safecall;
    function Get_SMPP_TLV_NUMBER_OF_MESSAGES: Integer; safecall;
    function Get_SMPP_TLV_CALLBACK_NUM: Integer; safecall;
    function Get_SMPP_TLV_DPF_RESULT: Integer; safecall;
    function Get_SMPP_TLV_SET_DPF: Integer; safecall;
    function Get_SMPP_TLV_MS_AVAILABILITY_STATUS: Integer; safecall;
    function Get_SMPP_TLV_NETWORK_ERROR_CODE: Integer; safecall;
    function Get_SMPP_TLV_MESSAGE_PAYLOAD: Integer; safecall;
    function Get_SMPP_TLV_DELIVERY_FAILURE_REASON: Integer; safecall;
    function Get_SMPP_TLV_MORE_MESSAGES_TO_SEND: Integer; safecall;
    function Get_SMPP_TLV_MESSAGE_STATE: Integer; safecall;
    function Get_SMPP_TLV_CONGESTION_STATE: Integer; safecall;
    function Get_SMPP_TLV_USSD_SERVICE_OP: Integer; safecall;
    function Get_SMPP_TLV_DISPLAY_TIME: Integer; safecall;
    function Get_SMPP_TLV_SMS_SIGNAL: Integer; safecall;
    function Get_SMPP_TLV_MS_VALIDITY: Integer; safecall;
    function Get_SMPP_TLV_ALERT_ON_MESSAGE_DELIVERY: Integer; safecall;
    function Get_SMPP_TLV_ITS_REPLY_TYPE: Integer; safecall;
    function Get_SMPP_TLV_ITS_SESSION_INFO: Integer; safecall;
    function Get_SMPP_MULTIPARTMODE_UDH: Integer; safecall;
    function Get_SMPP_MULTIPARTMODE_UDH16BIT: Integer; safecall;
    function Get_SMPP_MULTIPARTMODE_SARTLV: Integer; safecall;
    function Get_SMPP_MULTIPARTMODE_PAYLOADTLV: Integer; safecall;
    function Get_SMPP_SUBMITMODE_SUBMITSM: Integer; safecall;
    function Get_SMPP_SUBMITMODE_DATASM: Integer; safecall;
    function Get_SMPP_USEGSMENCODING_INCHARSET: Integer; safecall;
    function Get_SMPP_USEGSMENCODING_OUTCHARSET: Integer; safecall;
    function Get_SMPP_USEGSMENCODING_INOUTCHARS: Integer; safecall;
    function Get_SMPP_IPVERSION_4: Integer; safecall;
    function Get_SMPP_IPVERSION_6: Integer; safecall;
    function Get_SMPP_IPVERSION_BOTH: Integer; safecall;
    function Get_SMPP_ESME_ROK: Integer; safecall;
    function Get_SMPP_ESME_RINVMSGLEN: Integer; safecall;
    function Get_SMPP_ESME_RINVCMDLEN: Integer; safecall;
    function Get_SMPP_ESME_RINVCMDID: Integer; safecall;
    function Get_SMPP_ESME_RINVBNDSTS: Integer; safecall;
    function Get_SMPP_ESME_RALYBND: Integer; safecall;
    function Get_SMPP_ESME_RINVPRTFLG: Integer; safecall;
    function Get_SMPP_ESME_RINVREGDLVFLG: Integer; safecall;
    function Get_SMPP_ESME_RSYSERR: Integer; safecall;
    function Get_SMPP_ESME_RINVSRCADR: Integer; safecall;
    function Get_SMPP_ESME_RINVDSTADR: Integer; safecall;
    function Get_SMPP_ESME_RINVMSGID: Integer; safecall;
    function Get_SMPP_ESME_RBINDFAIL: Integer; safecall;
    function Get_SMPP_ESME_RINVPASWD: Integer; safecall;
    function Get_SMPP_ESME_RINVSYSID: Integer; safecall;
    function Get_SMPP_ESME_RCANCELFAIL: Integer; safecall;
    function Get_SMPP_ESME_RREPLACEFAIL: Integer; safecall;
    function Get_SMPP_ESME_RMSGQFUL: Integer; safecall;
    function Get_SMPP_ESME_RINVSERTYP: Integer; safecall;
    function Get_SMPP_ESME_RINVNUMDESTS: Integer; safecall;
    function Get_SMPP_ESME_RINVDLNAME: Integer; safecall;
    function Get_SMPP_ESME_RINVDESTFLAG: Integer; safecall;
    function Get_SMPP_ESME_RINVSUBREP: Integer; safecall;
    function Get_SMPP_ESME_RINVESMCLASS: Integer; safecall;
    function Get_SMPP_ESME_RCNTSUBDL: Integer; safecall;
    function Get_SMPP_ESME_RSUBMITFAIL: Integer; safecall;
    function Get_SMPP_ESME_RINVSRCTON: Integer; safecall;
    function Get_SMPP_ESME_RINVSRCNPI: Integer; safecall;
    function Get_SMPP_ESME_RINVDSTTON: Integer; safecall;
    function Get_SMPP_ESME_RINVDSTNPI: Integer; safecall;
    function Get_SMPP_ESME_RINVSYSTYP: Integer; safecall;
    function Get_SMPP_ESME_RINVREPFLAG: Integer; safecall;
    function Get_SMPP_ESME_RINVNUMMSGS: Integer; safecall;
    function Get_SMPP_ESME_RTHROTTLED: Integer; safecall;
    function Get_SMPP_ESME_RINVSCHED: Integer; safecall;
    function Get_SMPP_ESME_RINVEXPIRY: Integer; safecall;
    function Get_SMPP_ESME_RINVDFTMSGID: Integer; safecall;
    function Get_SMPP_ESME_RX_T_APPN: Integer; safecall;
    function Get_SMPP_ESME_RX_P_APPN: Integer; safecall;
    function Get_SMPP_ESME_RX_R_APPN: Integer; safecall;
    function Get_SMPP_ESME_RQUERYFAIL: Integer; safecall;
    function Get_SMPP_ESME_RINVOPTPARSTREAM: Integer; safecall;
    function Get_SMPP_ESME_ROPTPARNOTALLWD: Integer; safecall;
    function Get_SMPP_ESME_RINVPARLEN: Integer; safecall;
    function Get_SMPP_ESME_RMISSINGOPTPARAM: Integer; safecall;
    function Get_SMPP_ESME_RINVOPTPARAMVAL: Integer; safecall;
    function Get_SMPP_ESME_RDELIVERYFAILURE: Integer; safecall;
    function Get_SMPP_ESME_RUNKNOWNERR: Integer; safecall;
    function Get_GSM_STATUS_MESSAGE_DELIVERED_SUCCESSFULLY: Integer; safecall;
    function Get_GSM_STATUS_FORWARDED_STATUS_UNKNOWN: Integer; safecall;
    function Get_GSM_STATUS_REPLACED: Integer; safecall;
    function Get_GSM_STATUS_CONGESTION_STILL_TRYING: Integer; safecall;
    function Get_GSM_STATUS_RECIPIENT_BUSY_STILL_TRYING: Integer; safecall;
    function Get_GSM_STATUS_NO_RESPONSE_STILL_TRYING: Integer; safecall;
    function Get_GSM_STATUS_SERVICE_REJECTED_STILL_TRYING: Integer; safecall;
    function Get_GSM_STATUS_QOS_NOT_AVAILABLE_STILL_TRYING: Integer; safecall;
    function Get_GSM_STATUS_RECIPIENT_ERROR_STILL_TRYING: Integer; safecall;
    function Get_GSM_STATUS_RPC_ERROR: Integer; safecall;
    function Get_GSM_STATUS_INCOMPATIBLE_DESTINATION: Integer; safecall;
    function Get_GSM_STATUS_CONNECTION_REJECTED: Integer; safecall;
    function Get_GSM_STATUS_NOT_OBTAINABLE: Integer; safecall;
    function Get_GSM_STATUS_QOS_NOT_AVAILABLE: Integer; safecall;
    function Get_GSM_STATUS_NO_INTERNETWORKING_AVAILABLE: Integer; safecall;
    function Get_GSM_STATUS_MESSAGE_EXPIRED: Integer; safecall;
    function Get_GSM_STATUS_MESSAGE_DELETED_BY_SENDER: Integer; safecall;
    function Get_GSM_STATUS_MESSAGE_DELETED_BY_SMSC: Integer; safecall;
    function Get_GSM_STATUS_DOES_NOT_EXIST: Integer; safecall;
    function Get_GSM_STORAGETYPE_SIM: Integer; safecall;
    function Get_GSM_STORAGETYPE_MEMORY: Integer; safecall;
    function Get_GSM_STORAGETYPE_COMBINED: Integer; safecall;
    function Get_GSM_STORAGETYPE_STATUS: Integer; safecall;
    function Get_GSM_STORAGETYPE_ALL: Integer; safecall;
    function Get_GSM_FO_REPLYPATH_EXISTS: Integer; safecall;
    function Get_GSM_FO_UDHI: Integer; safecall;
    function Get_GSM_FO_STATUS_REPORT: Integer; safecall;
    function Get_GSM_FO_VALIDITY_NONE: Integer; safecall;
    function Get_GSM_FO_VALIDITY_RELATIVE: Integer; safecall;
    function Get_GSM_FO_VALIDITY_ENHANCED: Integer; safecall;
    function Get_GSM_FO_VALIDITY_ABSOLUTE: Integer; safecall;
    function Get_GSM_FO_REJECT_DUPLICATES: Integer; safecall;
    function Get_GSM_FO_SUBMIT_SM: Integer; safecall;
    function Get_GSM_FO_DELIVER_SM: Integer; safecall;
    function Get_GSM_FO_STATUS_SM: Integer; safecall;
    function Get_DATACODING_DEFAULT: Integer; safecall;
    function Get_DATACODING_8BIT_DATA: Integer; safecall;
    function Get_DATACODING_UNICODE: Integer; safecall;
    function Get_GSM_DATACODING_ME_SPECIFIC: Integer; safecall;
    function Get_GSM_DATACODING_SIM_SPECIFIC: Integer; safecall;
    function Get_GSM_DATACODING_TE_SPECIFIC: Integer; safecall;
    function Get_DATACODING_FLASH: Integer; safecall;
    function Get_GSM_BAUDRATE_110: Integer; safecall;
    function Get_GSM_BAUDRATE_300: Integer; safecall;
    function Get_GSM_BAUDRATE_600: Integer; safecall;
    function Get_GSM_BAUDRATE_1200: Integer; safecall;
    function Get_GSM_BAUDRATE_2400: Integer; safecall;
    function Get_GSM_BAUDRATE_4800: Integer; safecall;
    function Get_GSM_BAUDRATE_9600: Integer; safecall;
    function Get_GSM_BAUDRATE_14400: Integer; safecall;
    function Get_GSM_BAUDRATE_19200: Integer; safecall;
    function Get_GSM_BAUDRATE_38400: Integer; safecall;
    function Get_GSM_BAUDRATE_56000: Integer; safecall;
    function Get_GSM_BAUDRATE_57600: Integer; safecall;
    function Get_GSM_BAUDRATE_64000: Integer; safecall;
    function Get_GSM_BAUDRATE_115200: Integer; safecall;
    function Get_GSM_BAUDRATE_128000: Integer; safecall;
    function Get_GSM_BAUDRATE_230400: Integer; safecall;
    function Get_GSM_BAUDRATE_256000: Integer; safecall;
    function Get_GSM_BAUDRATE_460800: Integer; safecall;
    function Get_GSM_BAUDRATE_921600: Integer; safecall;
    function Get_GSM_BAUDRATE_DEFAULT: Integer; safecall;
    function Get_GSM_MESSAGESTATE_RECEIVED_UNREAD: Integer; safecall;
    function Get_GSM_MESSAGESTATE_RECEIVED_READ: Integer; safecall;
    function Get_GSM_MESSAGESTATE_STORED_UNSENT: Integer; safecall;
    function Get_GSM_MESSAGESTATE_STORED_SENT: Integer; safecall;
    function Get_GSM_MESSAGESTATE_ALL: Integer; safecall;
    function Get_GSM_MESSAGEFORMAT_PDU: Integer; safecall;
    function Get_GSM_MESSAGEFORMAT_TEXT: Integer; safecall;
    function Get_GSM_MESSAGEFORMAT_AUTO: Integer; safecall;
    function Get_HTTP_PLACEHOLDER_USERTAG: WideString; safecall;
    function Get_HTTP_PLACEHOLDER_TOADDRESS: WideString; safecall;
    function Get_HTTP_PLACEHOLDER_FROMADDRESS: WideString; safecall;
    function Get_HTTP_PLACEHOLDER_BODY: WideString; safecall;
    function Get_HTTP_PLACEHOLDER_BODYASHEX: WideString; safecall;
    function Get_HTTP_PLACEHOLDER_BODYASBASE64: WideString; safecall;
    function Get_HTTP_PLACEHOLDER_DELIVERYREPORT: WideString; safecall;
    function Get_HTTP_PLACEHOLDER_TOADDRESSTON: WideString; safecall;
    function Get_HTTP_PLACEHOLDER_TOADDRESSNPI: WideString; safecall;
    function Get_HTTP_PLACEHOLDER_FROMADDRESSTON: WideString; safecall;
    function Get_HTTP_PLACEHOLDER_FROMADDRESSNPI: WideString; safecall;
    function Get_HTTP_PLACEHOLDER_PROTOCOLID: WideString; safecall;
    function Get_HTTP_PLACEHOLDER_UDHI: WideString; safecall;
    function Get_HTTP_PLACEHOLDER_DATACODING: WideString; safecall;
    function Get_GSM_PREFIXSMSC_ENABLED: Integer; safecall;
    function Get_GSM_PREFIXSMSC_DISABLED: Integer; safecall;
    function Get_GSM_PREFIXSMSC_AUTO: Integer; safecall;
    function Get_WAPPUSH_SIGNAL_NONE: Integer; safecall;
    function Get_WAPPUSH_SIGNAL_LOW: Integer; safecall;
    function Get_WAPPUSH_SIGNAL_MEDIUM: Integer; safecall;
    function Get_WAPPUSH_SIGNAL_HIGH: Integer; safecall;
    function Get_WAPPUSH_SIGNAL_DELETE: Integer; safecall;
    function Get_DIALUP_PROVIDERTYPE_UCP: Integer; safecall;
    function Get_DIALUP_PROVIDERTYPE_TAP_DEFAULT: Integer; safecall;
    function Get_DIALUP_PROVIDERTYPE_TAP_NOLF: Integer; safecall;
    function Get_DIALUP_PROVIDERTYPE_TAP_NOEOT: Integer; safecall;
    function Get_DIALUP_DIALMODE_TONE: Integer; safecall;
    function Get_DIALUP_DIALMODE_PULSE: Integer; safecall;
    function Get_DIALUP_DEVICESETTINGS_DEFAULT: Integer; safecall;
    function Get_DIALUP_DEVICESETTINGS_8N1: Integer; safecall;
    function Get_DIALUP_DEVICESETTINGS_7E1: Integer; safecall;
    function SmppBindToString(lVal: Integer): WideString; safecall;
    function SmppVersionToString(lVal: Integer): WideString; safecall;
    function TonToString(lVal: Integer): WideString; safecall;
    function NpiToString(lVal: Integer): WideString; safecall;
    function MultipartToString(lVal: Integer): WideString; safecall;
    function BodyformatToString(lVal: Integer): WideString; safecall;
    function SmppEsm2SmscToString(lVal: Integer): WideString; safecall;
    function SmppEsm2EsmeToString(lVal: Integer): WideString; safecall;
    function SmppUseGsmEncodingToString(lVal: Integer): WideString; safecall;
    function SmppDataCodingToString(lVal: Integer): WideString; safecall;
    function SmppPriorityFlagToString(lVal: Integer): WideString; safecall;
    function SmppMessageStateToString(lVal: Integer): WideString; safecall;
    function SmppSessionStateToString(lVal: Integer): WideString; safecall;
    function SmppTlvToString(lVal: Integer): WideString; safecall;
    function SmppMultipartModeToString(lVal: Integer): WideString; safecall;
    function SmppSubmitModeToString(lVal: Integer): WideString; safecall;
    function SmppEsmeToString(lVal: Integer): WideString; safecall;
    function GsmStatusToString(lVal: Integer): WideString; safecall;
    function GsmStorageTypeToString(lVal: Integer): WideString; safecall;
    function GsmFoToString(lVal: Integer): WideString; safecall;
    function GsmDataCodingToString(lVal: Integer): WideString; safecall;
    function GsmBaudrateToString(lVal: Integer): WideString; safecall;
    function GsmMessageStateToString(lVal: Integer): WideString; safecall;
    function GsmMessageFormatToString(lVal: Integer): WideString; safecall;
    function GsmPrefixSmscToString(lVal: Integer): WideString; safecall;
    function WapPushSignalToString(lVal: Integer): WideString; safecall;
    function DialupProviderTypeToString(lVal: Integer): WideString; safecall;
    function DialupDialModeToString(lVal: Integer): WideString; safecall;
    function DialupDeviceSettingsToString(lVal: Integer): WideString; safecall;
    property SMPP_BIND_TRANSMITTER: Integer read Get_SMPP_BIND_TRANSMITTER;
    property SMPP_BIND_TRANSCEIVER: Integer read Get_SMPP_BIND_TRANSCEIVER;
    property SMPP_BIND_RECEIVER: Integer read Get_SMPP_BIND_RECEIVER;
    property SMPP_VERSION_33: Integer read Get_SMPP_VERSION_33;
    property SMPP_VERSION_34: Integer read Get_SMPP_VERSION_34;
    property SMPP_VERSION_50: Integer read Get_SMPP_VERSION_50;
    property TON_UNKNOWN: Integer read Get_TON_UNKNOWN;
    property TON_INTERNATIONAL: Integer read Get_TON_INTERNATIONAL;
    property TON_NATIONAL: Integer read Get_TON_NATIONAL;
    property TON_NETWORK_SPECIFIC: Integer read Get_TON_NETWORK_SPECIFIC;
    property TON_SUBSCRIBER_NUMBER: Integer read Get_TON_SUBSCRIBER_NUMBER;
    property TON_ALPHANUMERIC: Integer read Get_TON_ALPHANUMERIC;
    property SMPP_TON_ABBREVIATED: Integer read Get_SMPP_TON_ABBREVIATED;
    property NPI_UNKNOWN: Integer read Get_NPI_UNKNOWN;
    property NPI_ISDN: Integer read Get_NPI_ISDN;
    property NPI_DATA: Integer read Get_NPI_DATA;
    property NPI_TELEX: Integer read Get_NPI_TELEX;
    property NPI_NATIONAL: Integer read Get_NPI_NATIONAL;
    property NPI_PRIVATE: Integer read Get_NPI_PRIVATE;
    property NPI_ERMES: Integer read Get_NPI_ERMES;
    property SMPP_NPI_INTERNET: Integer read Get_SMPP_NPI_INTERNET;
    property NPI_LAND_MOBILE: Integer read Get_NPI_LAND_MOBILE;
    property MULTIPART_ACCEPT: Integer read Get_MULTIPART_ACCEPT;
    property MULTIPART_TRUNCATE: Integer read Get_MULTIPART_TRUNCATE;
    property MULTIPART_REJECT: Integer read Get_MULTIPART_REJECT;
    property BODYFORMAT_TEXT: Integer read Get_BODYFORMAT_TEXT;
    property BODYFORMAT_HEX: Integer read Get_BODYFORMAT_HEX;
    property SMPP_ESM_2ESME_DEFAULT: Integer read Get_SMPP_ESM_2ESME_DEFAULT;
    property SMPP_ESM_2ESME_DELIVERY_RECEIPT: Integer read Get_SMPP_ESM_2ESME_DELIVERY_RECEIPT;
    property SMPP_ESM_2ESME_DELIVERY_ACK: Integer read Get_SMPP_ESM_2ESME_DELIVERY_ACK;
    property SMPP_ESM_2ESME_MANUAL_ACK: Integer read Get_SMPP_ESM_2ESME_MANUAL_ACK;
    property SMPP_ESM_2ESME_CONVERSATION_ABORT: Integer read Get_SMPP_ESM_2ESME_CONVERSATION_ABORT;
    property SMPP_ESM_2ESME_INTERMEDIATE_DELIVERY_NOTIFY: Integer read Get_SMPP_ESM_2ESME_INTERMEDIATE_DELIVERY_NOTIFY;
    property SMPP_ESM_2SMSC_MODE_DEFAULT: Integer read Get_SMPP_ESM_2SMSC_MODE_DEFAULT;
    property SMPP_ESM_2SMSC_MODE_STOREFORWARD: Integer read Get_SMPP_ESM_2SMSC_MODE_STOREFORWARD;
    property SMPP_ESM_2SMSC_MODE_DATAGRAM: Integer read Get_SMPP_ESM_2SMSC_MODE_DATAGRAM;
    property SMPP_ESM_2SMSC_MODE_FORWARD: Integer read Get_SMPP_ESM_2SMSC_MODE_FORWARD;
    property SMPP_ESM_2SMSC_TYPE_DEFAULT: Integer read Get_SMPP_ESM_2SMSC_TYPE_DEFAULT;
    property SMPP_ESM_2SMSC_TYPE_DELIVERY_ACK: Integer read Get_SMPP_ESM_2SMSC_TYPE_DELIVERY_ACK;
    property SMPP_ESM_2SMSC_TYPE_MANUAL_ACK: Integer read Get_SMPP_ESM_2SMSC_TYPE_MANUAL_ACK;
    property SMPP_ESM_2SMSC_FEAT_NOTHING: Integer read Get_SMPP_ESM_2SMSC_FEAT_NOTHING;
    property SMPP_ESM_2SMSC_FEAT_UDHI: Integer read Get_SMPP_ESM_2SMSC_FEAT_UDHI;
    property SMPP_ESM_2SMSC_FEAT_SRP: Integer read Get_SMPP_ESM_2SMSC_FEAT_SRP;
    property SMPP_USEGSMENCODING_DISABLED: Integer read Get_SMPP_USEGSMENCODING_DISABLED;
    property SMPP_USEGSMENCODING_INANDOUT: Integer read Get_SMPP_USEGSMENCODING_INANDOUT;
    property SMPP_USEGSMENCODING_INCOMING: Integer read Get_SMPP_USEGSMENCODING_INCOMING;
    property SMPP_USEGSMENCODING_OUTGOING: Integer read Get_SMPP_USEGSMENCODING_OUTGOING;
    property SMPP_DATACODING_ASCII: Integer read Get_SMPP_DATACODING_ASCII;
    property SMPP_DATACODING_OCTET_UNSPEC: Integer read Get_SMPP_DATACODING_OCTET_UNSPEC;
    property SMPP_DATACODING_LATIN: Integer read Get_SMPP_DATACODING_LATIN;
    property SMPP_DATACODING_JIS_KANJI: Integer read Get_SMPP_DATACODING_JIS_KANJI;
    property SMPP_DATACODING_CYRILLIC: Integer read Get_SMPP_DATACODING_CYRILLIC;
    property SMPP_DATACODING_LATIN_HEBREW: Integer read Get_SMPP_DATACODING_LATIN_HEBREW;
    property SMPP_DATACODING_PICTOGRAM: Integer read Get_SMPP_DATACODING_PICTOGRAM;
    property SMPP_DATACODING_ISO_2022_JP: Integer read Get_SMPP_DATACODING_ISO_2022_JP;
    property SMPP_DATACODING_EXTENDED_KANJI_JIS: Integer read Get_SMPP_DATACODING_EXTENDED_KANJI_JIS;
    property SMPP_DATACODING_KS_C_5601: Integer read Get_SMPP_DATACODING_KS_C_5601;
    property SMPP_PRIORITYFLAG_BULK: Integer read Get_SMPP_PRIORITYFLAG_BULK;
    property SMPP_PRIORITYFLAG_NORMAL: Integer read Get_SMPP_PRIORITYFLAG_NORMAL;
    property SMPP_PRIORITYFLAG_URGENT: Integer read Get_SMPP_PRIORITYFLAG_URGENT;
    property SMPP_PRIORITYFLAG_VERY_URGENT: Integer read Get_SMPP_PRIORITYFLAG_VERY_URGENT;
    property SMPP_MESSAGESTATE_AX_WAITRESP: Integer read Get_SMPP_MESSAGESTATE_AX_WAITRESP;
    property SMPP_MESSAGESTATE_ENROUTE: Integer read Get_SMPP_MESSAGESTATE_ENROUTE;
    property SMPP_MESSAGESTATE_DELIVERED: Integer read Get_SMPP_MESSAGESTATE_DELIVERED;
    property SMPP_MESSAGESTATE_EXPIRED: Integer read Get_SMPP_MESSAGESTATE_EXPIRED;
    property SMPP_MESSAGESTATE_DELETED: Integer read Get_SMPP_MESSAGESTATE_DELETED;
    property SMPP_MESSAGESTATE_UNDELIVERABLE: Integer read Get_SMPP_MESSAGESTATE_UNDELIVERABLE;
    property SMPP_MESSAGESTATE_ACCEPTED: Integer read Get_SMPP_MESSAGESTATE_ACCEPTED;
    property SMPP_MESSAGESTATE_UNKNOWN: Integer read Get_SMPP_MESSAGESTATE_UNKNOWN;
    property SMPP_MESSAGESTATE_REJECTED: Integer read Get_SMPP_MESSAGESTATE_REJECTED;
    property SMPP_MESSAGESTATE_AX_RESPERROR: Integer read Get_SMPP_MESSAGESTATE_AX_RESPERROR;
    property SMPP_MESSAGESTATE_AX_NOCREDITS: Integer read Get_SMPP_MESSAGESTATE_AX_NOCREDITS;
    property SMPP_MESSAGESTATE_AX_RESPTO: Integer read Get_SMPP_MESSAGESTATE_AX_RESPTO;
    property SMPP_MESSAGESTATE_AX_RESPONDED: Integer read Get_SMPP_MESSAGESTATE_AX_RESPONDED;
    property SMPP_SESSIONSTATE_CONNECTED: Integer read Get_SMPP_SESSIONSTATE_CONNECTED;
    property SMPP_SESSIONSTATE_DISCONNECTED: Integer read Get_SMPP_SESSIONSTATE_DISCONNECTED;
    property SMPP_SESSIONSTATE_BINDING: Integer read Get_SMPP_SESSIONSTATE_BINDING;
    property SMPP_SESSIONSTATE_BOUND_TX: Integer read Get_SMPP_SESSIONSTATE_BOUND_TX;
    property SMPP_SESSIONSTATE_BOUND_RX: Integer read Get_SMPP_SESSIONSTATE_BOUND_RX;
    property SMPP_SESSIONSTATE_BOUND_TRX: Integer read Get_SMPP_SESSIONSTATE_BOUND_TRX;
    property SMPP_TLV_DEST_ADDR_SUBUNIT: Integer read Get_SMPP_TLV_DEST_ADDR_SUBUNIT;
    property SMPP_TLV_DEST_NETWORK_TYPE: Integer read Get_SMPP_TLV_DEST_NETWORK_TYPE;
    property SMPP_TLV_DEST_BEARER_TYPE: Integer read Get_SMPP_TLV_DEST_BEARER_TYPE;
    property SMPP_TLV_DEST_TELEMATICS_ID: Integer read Get_SMPP_TLV_DEST_TELEMATICS_ID;
    property SMPP_TLV_SOURCE_ADDR_SUBUNIT: Integer read Get_SMPP_TLV_SOURCE_ADDR_SUBUNIT;
    property SMPP_TLV_SOURCE_NETWORK_TYPE: Integer read Get_SMPP_TLV_SOURCE_NETWORK_TYPE;
    property SMPP_TLV_SOURCE_BEARER_TYPE: Integer read Get_SMPP_TLV_SOURCE_BEARER_TYPE;
    property SMPP_TLV_SOURCE_TELEMATICS_ID: Integer read Get_SMPP_TLV_SOURCE_TELEMATICS_ID;
    property SMPP_TLV_QOS_TIME_TO_LIVE: Integer read Get_SMPP_TLV_QOS_TIME_TO_LIVE;
    property SMPP_TLV_PAYLOAD_TYPE: Integer read Get_SMPP_TLV_PAYLOAD_TYPE;
    property SMPP_TLV_ADDITIONAL_STATUS_INFO_TEXT: Integer read Get_SMPP_TLV_ADDITIONAL_STATUS_INFO_TEXT;
    property SMPP_TLV_RECEIPTED_MESSAGE_ID: Integer read Get_SMPP_TLV_RECEIPTED_MESSAGE_ID;
    property SMPP_TLV_MS_MSG_WAIT_FACILITIES: Integer read Get_SMPP_TLV_MS_MSG_WAIT_FACILITIES;
    property SMPP_TLV_PRIVACY_INDICATOR: Integer read Get_SMPP_TLV_PRIVACY_INDICATOR;
    property SMPP_TLV_SOURCE_SUBADDRESS: Integer read Get_SMPP_TLV_SOURCE_SUBADDRESS;
    property SMPP_TLV_DEST_SUBADDRESS: Integer read Get_SMPP_TLV_DEST_SUBADDRESS;
    property SMPP_TLV_USER_MESSAGE_REFERENCE: Integer read Get_SMPP_TLV_USER_MESSAGE_REFERENCE;
    property SMPP_TLV_USER_RESPONSE_CODE: Integer read Get_SMPP_TLV_USER_RESPONSE_CODE;
    property SMPP_TLV_SOURCE_PORT: Integer read Get_SMPP_TLV_SOURCE_PORT;
    property SMPP_TLV_DESTINATION_PORT: Integer read Get_SMPP_TLV_DESTINATION_PORT;
    property SMPP_TLV_SAR_MSG_REF_NUM: Integer read Get_SMPP_TLV_SAR_MSG_REF_NUM;
    property SMPP_TLV_LANGUAGE_INDICATOR: Integer read Get_SMPP_TLV_LANGUAGE_INDICATOR;
    property SMPP_TLV_SAR_TOTAL_SEGMENTS: Integer read Get_SMPP_TLV_SAR_TOTAL_SEGMENTS;
    property SMPP_TLV_SAR_SEGMENT_SEQNUM: Integer read Get_SMPP_TLV_SAR_SEGMENT_SEQNUM;
    property SMPP_TLV_SC_INTERFACE_VERSION: Integer read Get_SMPP_TLV_SC_INTERFACE_VERSION;
    property SMPP_TLV_CALLBACK_NUM_PRES_IND: Integer read Get_SMPP_TLV_CALLBACK_NUM_PRES_IND;
    property SMPP_TLV_CALLBACK_NUM_ATAG: Integer read Get_SMPP_TLV_CALLBACK_NUM_ATAG;
    property SMPP_TLV_NUMBER_OF_MESSAGES: Integer read Get_SMPP_TLV_NUMBER_OF_MESSAGES;
    property SMPP_TLV_CALLBACK_NUM: Integer read Get_SMPP_TLV_CALLBACK_NUM;
    property SMPP_TLV_DPF_RESULT: Integer read Get_SMPP_TLV_DPF_RESULT;
    property SMPP_TLV_SET_DPF: Integer read Get_SMPP_TLV_SET_DPF;
    property SMPP_TLV_MS_AVAILABILITY_STATUS: Integer read Get_SMPP_TLV_MS_AVAILABILITY_STATUS;
    property SMPP_TLV_NETWORK_ERROR_CODE: Integer read Get_SMPP_TLV_NETWORK_ERROR_CODE;
    property SMPP_TLV_MESSAGE_PAYLOAD: Integer read Get_SMPP_TLV_MESSAGE_PAYLOAD;
    property SMPP_TLV_DELIVERY_FAILURE_REASON: Integer read Get_SMPP_TLV_DELIVERY_FAILURE_REASON;
    property SMPP_TLV_MORE_MESSAGES_TO_SEND: Integer read Get_SMPP_TLV_MORE_MESSAGES_TO_SEND;
    property SMPP_TLV_MESSAGE_STATE: Integer read Get_SMPP_TLV_MESSAGE_STATE;
    property SMPP_TLV_CONGESTION_STATE: Integer read Get_SMPP_TLV_CONGESTION_STATE;
    property SMPP_TLV_USSD_SERVICE_OP: Integer read Get_SMPP_TLV_USSD_SERVICE_OP;
    property SMPP_TLV_DISPLAY_TIME: Integer read Get_SMPP_TLV_DISPLAY_TIME;
    property SMPP_TLV_SMS_SIGNAL: Integer read Get_SMPP_TLV_SMS_SIGNAL;
    property SMPP_TLV_MS_VALIDITY: Integer read Get_SMPP_TLV_MS_VALIDITY;
    property SMPP_TLV_ALERT_ON_MESSAGE_DELIVERY: Integer read Get_SMPP_TLV_ALERT_ON_MESSAGE_DELIVERY;
    property SMPP_TLV_ITS_REPLY_TYPE: Integer read Get_SMPP_TLV_ITS_REPLY_TYPE;
    property SMPP_TLV_ITS_SESSION_INFO: Integer read Get_SMPP_TLV_ITS_SESSION_INFO;
    property SMPP_MULTIPARTMODE_UDH: Integer read Get_SMPP_MULTIPARTMODE_UDH;
    property SMPP_MULTIPARTMODE_UDH16BIT: Integer read Get_SMPP_MULTIPARTMODE_UDH16BIT;
    property SMPP_MULTIPARTMODE_SARTLV: Integer read Get_SMPP_MULTIPARTMODE_SARTLV;
    property SMPP_MULTIPARTMODE_PAYLOADTLV: Integer read Get_SMPP_MULTIPARTMODE_PAYLOADTLV;
    property SMPP_SUBMITMODE_SUBMITSM: Integer read Get_SMPP_SUBMITMODE_SUBMITSM;
    property SMPP_SUBMITMODE_DATASM: Integer read Get_SMPP_SUBMITMODE_DATASM;
    property SMPP_USEGSMENCODING_INCHARSET: Integer read Get_SMPP_USEGSMENCODING_INCHARSET;
    property SMPP_USEGSMENCODING_OUTCHARSET: Integer read Get_SMPP_USEGSMENCODING_OUTCHARSET;
    property SMPP_USEGSMENCODING_INOUTCHARS: Integer read Get_SMPP_USEGSMENCODING_INOUTCHARS;
    property SMPP_IPVERSION_4: Integer read Get_SMPP_IPVERSION_4;
    property SMPP_IPVERSION_6: Integer read Get_SMPP_IPVERSION_6;
    property SMPP_IPVERSION_BOTH: Integer read Get_SMPP_IPVERSION_BOTH;
    property SMPP_ESME_ROK: Integer read Get_SMPP_ESME_ROK;
    property SMPP_ESME_RINVMSGLEN: Integer read Get_SMPP_ESME_RINVMSGLEN;
    property SMPP_ESME_RINVCMDLEN: Integer read Get_SMPP_ESME_RINVCMDLEN;
    property SMPP_ESME_RINVCMDID: Integer read Get_SMPP_ESME_RINVCMDID;
    property SMPP_ESME_RINVBNDSTS: Integer read Get_SMPP_ESME_RINVBNDSTS;
    property SMPP_ESME_RALYBND: Integer read Get_SMPP_ESME_RALYBND;
    property SMPP_ESME_RINVPRTFLG: Integer read Get_SMPP_ESME_RINVPRTFLG;
    property SMPP_ESME_RINVREGDLVFLG: Integer read Get_SMPP_ESME_RINVREGDLVFLG;
    property SMPP_ESME_RSYSERR: Integer read Get_SMPP_ESME_RSYSERR;
    property SMPP_ESME_RINVSRCADR: Integer read Get_SMPP_ESME_RINVSRCADR;
    property SMPP_ESME_RINVDSTADR: Integer read Get_SMPP_ESME_RINVDSTADR;
    property SMPP_ESME_RINVMSGID: Integer read Get_SMPP_ESME_RINVMSGID;
    property SMPP_ESME_RBINDFAIL: Integer read Get_SMPP_ESME_RBINDFAIL;
    property SMPP_ESME_RINVPASWD: Integer read Get_SMPP_ESME_RINVPASWD;
    property SMPP_ESME_RINVSYSID: Integer read Get_SMPP_ESME_RINVSYSID;
    property SMPP_ESME_RCANCELFAIL: Integer read Get_SMPP_ESME_RCANCELFAIL;
    property SMPP_ESME_RREPLACEFAIL: Integer read Get_SMPP_ESME_RREPLACEFAIL;
    property SMPP_ESME_RMSGQFUL: Integer read Get_SMPP_ESME_RMSGQFUL;
    property SMPP_ESME_RINVSERTYP: Integer read Get_SMPP_ESME_RINVSERTYP;
    property SMPP_ESME_RINVNUMDESTS: Integer read Get_SMPP_ESME_RINVNUMDESTS;
    property SMPP_ESME_RINVDLNAME: Integer read Get_SMPP_ESME_RINVDLNAME;
    property SMPP_ESME_RINVDESTFLAG: Integer read Get_SMPP_ESME_RINVDESTFLAG;
    property SMPP_ESME_RINVSUBREP: Integer read Get_SMPP_ESME_RINVSUBREP;
    property SMPP_ESME_RINVESMCLASS: Integer read Get_SMPP_ESME_RINVESMCLASS;
    property SMPP_ESME_RCNTSUBDL: Integer read Get_SMPP_ESME_RCNTSUBDL;
    property SMPP_ESME_RSUBMITFAIL: Integer read Get_SMPP_ESME_RSUBMITFAIL;
    property SMPP_ESME_RINVSRCTON: Integer read Get_SMPP_ESME_RINVSRCTON;
    property SMPP_ESME_RINVSRCNPI: Integer read Get_SMPP_ESME_RINVSRCNPI;
    property SMPP_ESME_RINVDSTTON: Integer read Get_SMPP_ESME_RINVDSTTON;
    property SMPP_ESME_RINVDSTNPI: Integer read Get_SMPP_ESME_RINVDSTNPI;
    property SMPP_ESME_RINVSYSTYP: Integer read Get_SMPP_ESME_RINVSYSTYP;
    property SMPP_ESME_RINVREPFLAG: Integer read Get_SMPP_ESME_RINVREPFLAG;
    property SMPP_ESME_RINVNUMMSGS: Integer read Get_SMPP_ESME_RINVNUMMSGS;
    property SMPP_ESME_RTHROTTLED: Integer read Get_SMPP_ESME_RTHROTTLED;
    property SMPP_ESME_RINVSCHED: Integer read Get_SMPP_ESME_RINVSCHED;
    property SMPP_ESME_RINVEXPIRY: Integer read Get_SMPP_ESME_RINVEXPIRY;
    property SMPP_ESME_RINVDFTMSGID: Integer read Get_SMPP_ESME_RINVDFTMSGID;
    property SMPP_ESME_RX_T_APPN: Integer read Get_SMPP_ESME_RX_T_APPN;
    property SMPP_ESME_RX_P_APPN: Integer read Get_SMPP_ESME_RX_P_APPN;
    property SMPP_ESME_RX_R_APPN: Integer read Get_SMPP_ESME_RX_R_APPN;
    property SMPP_ESME_RQUERYFAIL: Integer read Get_SMPP_ESME_RQUERYFAIL;
    property SMPP_ESME_RINVOPTPARSTREAM: Integer read Get_SMPP_ESME_RINVOPTPARSTREAM;
    property SMPP_ESME_ROPTPARNOTALLWD: Integer read Get_SMPP_ESME_ROPTPARNOTALLWD;
    property SMPP_ESME_RINVPARLEN: Integer read Get_SMPP_ESME_RINVPARLEN;
    property SMPP_ESME_RMISSINGOPTPARAM: Integer read Get_SMPP_ESME_RMISSINGOPTPARAM;
    property SMPP_ESME_RINVOPTPARAMVAL: Integer read Get_SMPP_ESME_RINVOPTPARAMVAL;
    property SMPP_ESME_RDELIVERYFAILURE: Integer read Get_SMPP_ESME_RDELIVERYFAILURE;
    property SMPP_ESME_RUNKNOWNERR: Integer read Get_SMPP_ESME_RUNKNOWNERR;
    property GSM_STATUS_MESSAGE_DELIVERED_SUCCESSFULLY: Integer read Get_GSM_STATUS_MESSAGE_DELIVERED_SUCCESSFULLY;
    property GSM_STATUS_FORWARDED_STATUS_UNKNOWN: Integer read Get_GSM_STATUS_FORWARDED_STATUS_UNKNOWN;
    property GSM_STATUS_REPLACED: Integer read Get_GSM_STATUS_REPLACED;
    property GSM_STATUS_CONGESTION_STILL_TRYING: Integer read Get_GSM_STATUS_CONGESTION_STILL_TRYING;
    property GSM_STATUS_RECIPIENT_BUSY_STILL_TRYING: Integer read Get_GSM_STATUS_RECIPIENT_BUSY_STILL_TRYING;
    property GSM_STATUS_NO_RESPONSE_STILL_TRYING: Integer read Get_GSM_STATUS_NO_RESPONSE_STILL_TRYING;
    property GSM_STATUS_SERVICE_REJECTED_STILL_TRYING: Integer read Get_GSM_STATUS_SERVICE_REJECTED_STILL_TRYING;
    property GSM_STATUS_QOS_NOT_AVAILABLE_STILL_TRYING: Integer read Get_GSM_STATUS_QOS_NOT_AVAILABLE_STILL_TRYING;
    property GSM_STATUS_RECIPIENT_ERROR_STILL_TRYING: Integer read Get_GSM_STATUS_RECIPIENT_ERROR_STILL_TRYING;
    property GSM_STATUS_RPC_ERROR: Integer read Get_GSM_STATUS_RPC_ERROR;
    property GSM_STATUS_INCOMPATIBLE_DESTINATION: Integer read Get_GSM_STATUS_INCOMPATIBLE_DESTINATION;
    property GSM_STATUS_CONNECTION_REJECTED: Integer read Get_GSM_STATUS_CONNECTION_REJECTED;
    property GSM_STATUS_NOT_OBTAINABLE: Integer read Get_GSM_STATUS_NOT_OBTAINABLE;
    property GSM_STATUS_QOS_NOT_AVAILABLE: Integer read Get_GSM_STATUS_QOS_NOT_AVAILABLE;
    property GSM_STATUS_NO_INTERNETWORKING_AVAILABLE: Integer read Get_GSM_STATUS_NO_INTERNETWORKING_AVAILABLE;
    property GSM_STATUS_MESSAGE_EXPIRED: Integer read Get_GSM_STATUS_MESSAGE_EXPIRED;
    property GSM_STATUS_MESSAGE_DELETED_BY_SENDER: Integer read Get_GSM_STATUS_MESSAGE_DELETED_BY_SENDER;
    property GSM_STATUS_MESSAGE_DELETED_BY_SMSC: Integer read Get_GSM_STATUS_MESSAGE_DELETED_BY_SMSC;
    property GSM_STATUS_DOES_NOT_EXIST: Integer read Get_GSM_STATUS_DOES_NOT_EXIST;
    property GSM_STORAGETYPE_SIM: Integer read Get_GSM_STORAGETYPE_SIM;
    property GSM_STORAGETYPE_MEMORY: Integer read Get_GSM_STORAGETYPE_MEMORY;
    property GSM_STORAGETYPE_COMBINED: Integer read Get_GSM_STORAGETYPE_COMBINED;
    property GSM_STORAGETYPE_STATUS: Integer read Get_GSM_STORAGETYPE_STATUS;
    property GSM_STORAGETYPE_ALL: Integer read Get_GSM_STORAGETYPE_ALL;
    property GSM_FO_REPLYPATH_EXISTS: Integer read Get_GSM_FO_REPLYPATH_EXISTS;
    property GSM_FO_UDHI: Integer read Get_GSM_FO_UDHI;
    property GSM_FO_STATUS_REPORT: Integer read Get_GSM_FO_STATUS_REPORT;
    property GSM_FO_VALIDITY_NONE: Integer read Get_GSM_FO_VALIDITY_NONE;
    property GSM_FO_VALIDITY_RELATIVE: Integer read Get_GSM_FO_VALIDITY_RELATIVE;
    property GSM_FO_VALIDITY_ENHANCED: Integer read Get_GSM_FO_VALIDITY_ENHANCED;
    property GSM_FO_VALIDITY_ABSOLUTE: Integer read Get_GSM_FO_VALIDITY_ABSOLUTE;
    property GSM_FO_REJECT_DUPLICATES: Integer read Get_GSM_FO_REJECT_DUPLICATES;
    property GSM_FO_SUBMIT_SM: Integer read Get_GSM_FO_SUBMIT_SM;
    property GSM_FO_DELIVER_SM: Integer read Get_GSM_FO_DELIVER_SM;
    property GSM_FO_STATUS_SM: Integer read Get_GSM_FO_STATUS_SM;
    property DATACODING_DEFAULT: Integer read Get_DATACODING_DEFAULT;
    property DATACODING_8BIT_DATA: Integer read Get_DATACODING_8BIT_DATA;
    property DATACODING_UNICODE: Integer read Get_DATACODING_UNICODE;
    property GSM_DATACODING_ME_SPECIFIC: Integer read Get_GSM_DATACODING_ME_SPECIFIC;
    property GSM_DATACODING_SIM_SPECIFIC: Integer read Get_GSM_DATACODING_SIM_SPECIFIC;
    property GSM_DATACODING_TE_SPECIFIC: Integer read Get_GSM_DATACODING_TE_SPECIFIC;
    property DATACODING_FLASH: Integer read Get_DATACODING_FLASH;
    property GSM_BAUDRATE_110: Integer read Get_GSM_BAUDRATE_110;
    property GSM_BAUDRATE_300: Integer read Get_GSM_BAUDRATE_300;
    property GSM_BAUDRATE_600: Integer read Get_GSM_BAUDRATE_600;
    property GSM_BAUDRATE_1200: Integer read Get_GSM_BAUDRATE_1200;
    property GSM_BAUDRATE_2400: Integer read Get_GSM_BAUDRATE_2400;
    property GSM_BAUDRATE_4800: Integer read Get_GSM_BAUDRATE_4800;
    property GSM_BAUDRATE_9600: Integer read Get_GSM_BAUDRATE_9600;
    property GSM_BAUDRATE_14400: Integer read Get_GSM_BAUDRATE_14400;
    property GSM_BAUDRATE_19200: Integer read Get_GSM_BAUDRATE_19200;
    property GSM_BAUDRATE_38400: Integer read Get_GSM_BAUDRATE_38400;
    property GSM_BAUDRATE_56000: Integer read Get_GSM_BAUDRATE_56000;
    property GSM_BAUDRATE_57600: Integer read Get_GSM_BAUDRATE_57600;
    property GSM_BAUDRATE_64000: Integer read Get_GSM_BAUDRATE_64000;
    property GSM_BAUDRATE_115200: Integer read Get_GSM_BAUDRATE_115200;
    property GSM_BAUDRATE_128000: Integer read Get_GSM_BAUDRATE_128000;
    property GSM_BAUDRATE_230400: Integer read Get_GSM_BAUDRATE_230400;
    property GSM_BAUDRATE_256000: Integer read Get_GSM_BAUDRATE_256000;
    property GSM_BAUDRATE_460800: Integer read Get_GSM_BAUDRATE_460800;
    property GSM_BAUDRATE_921600: Integer read Get_GSM_BAUDRATE_921600;
    property GSM_BAUDRATE_DEFAULT: Integer read Get_GSM_BAUDRATE_DEFAULT;
    property GSM_MESSAGESTATE_RECEIVED_UNREAD: Integer read Get_GSM_MESSAGESTATE_RECEIVED_UNREAD;
    property GSM_MESSAGESTATE_RECEIVED_READ: Integer read Get_GSM_MESSAGESTATE_RECEIVED_READ;
    property GSM_MESSAGESTATE_STORED_UNSENT: Integer read Get_GSM_MESSAGESTATE_STORED_UNSENT;
    property GSM_MESSAGESTATE_STORED_SENT: Integer read Get_GSM_MESSAGESTATE_STORED_SENT;
    property GSM_MESSAGESTATE_ALL: Integer read Get_GSM_MESSAGESTATE_ALL;
    property GSM_MESSAGEFORMAT_PDU: Integer read Get_GSM_MESSAGEFORMAT_PDU;
    property GSM_MESSAGEFORMAT_TEXT: Integer read Get_GSM_MESSAGEFORMAT_TEXT;
    property GSM_MESSAGEFORMAT_AUTO: Integer read Get_GSM_MESSAGEFORMAT_AUTO;
    property HTTP_PLACEHOLDER_USERTAG: WideString read Get_HTTP_PLACEHOLDER_USERTAG;
    property HTTP_PLACEHOLDER_TOADDRESS: WideString read Get_HTTP_PLACEHOLDER_TOADDRESS;
    property HTTP_PLACEHOLDER_FROMADDRESS: WideString read Get_HTTP_PLACEHOLDER_FROMADDRESS;
    property HTTP_PLACEHOLDER_BODY: WideString read Get_HTTP_PLACEHOLDER_BODY;
    property HTTP_PLACEHOLDER_BODYASHEX: WideString read Get_HTTP_PLACEHOLDER_BODYASHEX;
    property HTTP_PLACEHOLDER_BODYASBASE64: WideString read Get_HTTP_PLACEHOLDER_BODYASBASE64;
    property HTTP_PLACEHOLDER_DELIVERYREPORT: WideString read Get_HTTP_PLACEHOLDER_DELIVERYREPORT;
    property HTTP_PLACEHOLDER_TOADDRESSTON: WideString read Get_HTTP_PLACEHOLDER_TOADDRESSTON;
    property HTTP_PLACEHOLDER_TOADDRESSNPI: WideString read Get_HTTP_PLACEHOLDER_TOADDRESSNPI;
    property HTTP_PLACEHOLDER_FROMADDRESSTON: WideString read Get_HTTP_PLACEHOLDER_FROMADDRESSTON;
    property HTTP_PLACEHOLDER_FROMADDRESSNPI: WideString read Get_HTTP_PLACEHOLDER_FROMADDRESSNPI;
    property HTTP_PLACEHOLDER_PROTOCOLID: WideString read Get_HTTP_PLACEHOLDER_PROTOCOLID;
    property HTTP_PLACEHOLDER_UDHI: WideString read Get_HTTP_PLACEHOLDER_UDHI;
    property HTTP_PLACEHOLDER_DATACODING: WideString read Get_HTTP_PLACEHOLDER_DATACODING;
    property GSM_PREFIXSMSC_ENABLED: Integer read Get_GSM_PREFIXSMSC_ENABLED;
    property GSM_PREFIXSMSC_DISABLED: Integer read Get_GSM_PREFIXSMSC_DISABLED;
    property GSM_PREFIXSMSC_AUTO: Integer read Get_GSM_PREFIXSMSC_AUTO;
    property WAPPUSH_SIGNAL_NONE: Integer read Get_WAPPUSH_SIGNAL_NONE;
    property WAPPUSH_SIGNAL_LOW: Integer read Get_WAPPUSH_SIGNAL_LOW;
    property WAPPUSH_SIGNAL_MEDIUM: Integer read Get_WAPPUSH_SIGNAL_MEDIUM;
    property WAPPUSH_SIGNAL_HIGH: Integer read Get_WAPPUSH_SIGNAL_HIGH;
    property WAPPUSH_SIGNAL_DELETE: Integer read Get_WAPPUSH_SIGNAL_DELETE;
    property DIALUP_PROVIDERTYPE_UCP: Integer read Get_DIALUP_PROVIDERTYPE_UCP;
    property DIALUP_PROVIDERTYPE_TAP_DEFAULT: Integer read Get_DIALUP_PROVIDERTYPE_TAP_DEFAULT;
    property DIALUP_PROVIDERTYPE_TAP_NOLF: Integer read Get_DIALUP_PROVIDERTYPE_TAP_NOLF;
    property DIALUP_PROVIDERTYPE_TAP_NOEOT: Integer read Get_DIALUP_PROVIDERTYPE_TAP_NOEOT;
    property DIALUP_DIALMODE_TONE: Integer read Get_DIALUP_DIALMODE_TONE;
    property DIALUP_DIALMODE_PULSE: Integer read Get_DIALUP_DIALMODE_PULSE;
    property DIALUP_DEVICESETTINGS_DEFAULT: Integer read Get_DIALUP_DEVICESETTINGS_DEFAULT;
    property DIALUP_DEVICESETTINGS_8N1: Integer read Get_DIALUP_DEVICESETTINGS_8N1;
    property DIALUP_DEVICESETTINGS_7E1: Integer read Get_DIALUP_DEVICESETTINGS_7E1;
  end;

// *********************************************************************//
// DispIntf:  IConstantsDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {7D47E1D0-4B72-4A5D-B174-A0E82AF73892}
// *********************************************************************//
  IConstantsDisp = dispinterface
    ['{7D47E1D0-4B72-4A5D-B174-A0E82AF73892}']
    property SMPP_BIND_TRANSMITTER: Integer readonly dispid 101;
    property SMPP_BIND_TRANSCEIVER: Integer readonly dispid 102;
    property SMPP_BIND_RECEIVER: Integer readonly dispid 103;
    property SMPP_VERSION_33: Integer readonly dispid 204;
    property SMPP_VERSION_34: Integer readonly dispid 205;
    property SMPP_VERSION_50: Integer readonly dispid 206;
    property TON_UNKNOWN: Integer readonly dispid 307;
    property TON_INTERNATIONAL: Integer readonly dispid 308;
    property TON_NATIONAL: Integer readonly dispid 309;
    property TON_NETWORK_SPECIFIC: Integer readonly dispid 310;
    property TON_SUBSCRIBER_NUMBER: Integer readonly dispid 311;
    property TON_ALPHANUMERIC: Integer readonly dispid 312;
    property SMPP_TON_ABBREVIATED: Integer readonly dispid 313;
    property NPI_UNKNOWN: Integer readonly dispid 414;
    property NPI_ISDN: Integer readonly dispid 415;
    property NPI_DATA: Integer readonly dispid 416;
    property NPI_TELEX: Integer readonly dispid 417;
    property NPI_NATIONAL: Integer readonly dispid 418;
    property NPI_PRIVATE: Integer readonly dispid 419;
    property NPI_ERMES: Integer readonly dispid 420;
    property SMPP_NPI_INTERNET: Integer readonly dispid 421;
    property NPI_LAND_MOBILE: Integer readonly dispid 422;
    property MULTIPART_ACCEPT: Integer readonly dispid 522;
    property MULTIPART_TRUNCATE: Integer readonly dispid 523;
    property MULTIPART_REJECT: Integer readonly dispid 524;
    property BODYFORMAT_TEXT: Integer readonly dispid 625;
    property BODYFORMAT_HEX: Integer readonly dispid 627;
    property SMPP_ESM_2ESME_DEFAULT: Integer readonly dispid 731;
    property SMPP_ESM_2ESME_DELIVERY_RECEIPT: Integer readonly dispid 732;
    property SMPP_ESM_2ESME_DELIVERY_ACK: Integer readonly dispid 733;
    property SMPP_ESM_2ESME_MANUAL_ACK: Integer readonly dispid 734;
    property SMPP_ESM_2ESME_CONVERSATION_ABORT: Integer readonly dispid 735;
    property SMPP_ESM_2ESME_INTERMEDIATE_DELIVERY_NOTIFY: Integer readonly dispid 736;
    property SMPP_ESM_2SMSC_MODE_DEFAULT: Integer readonly dispid 737;
    property SMPP_ESM_2SMSC_MODE_STOREFORWARD: Integer readonly dispid 738;
    property SMPP_ESM_2SMSC_MODE_DATAGRAM: Integer readonly dispid 739;
    property SMPP_ESM_2SMSC_MODE_FORWARD: Integer readonly dispid 740;
    property SMPP_ESM_2SMSC_TYPE_DEFAULT: Integer readonly dispid 741;
    property SMPP_ESM_2SMSC_TYPE_DELIVERY_ACK: Integer readonly dispid 742;
    property SMPP_ESM_2SMSC_TYPE_MANUAL_ACK: Integer readonly dispid 743;
    property SMPP_ESM_2SMSC_FEAT_NOTHING: Integer readonly dispid 744;
    property SMPP_ESM_2SMSC_FEAT_UDHI: Integer readonly dispid 745;
    property SMPP_ESM_2SMSC_FEAT_SRP: Integer readonly dispid 746;
    property SMPP_USEGSMENCODING_DISABLED: Integer readonly dispid 848;
    property SMPP_USEGSMENCODING_INANDOUT: Integer readonly dispid 849;
    property SMPP_USEGSMENCODING_INCOMING: Integer readonly dispid 850;
    property SMPP_USEGSMENCODING_OUTGOING: Integer readonly dispid 851;
    property SMPP_DATACODING_ASCII: Integer readonly dispid 952;
    property SMPP_DATACODING_OCTET_UNSPEC: Integer readonly dispid 953;
    property SMPP_DATACODING_LATIN: Integer readonly dispid 954;
    property SMPP_DATACODING_JIS_KANJI: Integer readonly dispid 956;
    property SMPP_DATACODING_CYRILLIC: Integer readonly dispid 957;
    property SMPP_DATACODING_LATIN_HEBREW: Integer readonly dispid 958;
    property SMPP_DATACODING_PICTOGRAM: Integer readonly dispid 960;
    property SMPP_DATACODING_ISO_2022_JP: Integer readonly dispid 961;
    property SMPP_DATACODING_EXTENDED_KANJI_JIS: Integer readonly dispid 962;
    property SMPP_DATACODING_KS_C_5601: Integer readonly dispid 963;
    property SMPP_PRIORITYFLAG_BULK: Integer readonly dispid 1070;
    property SMPP_PRIORITYFLAG_NORMAL: Integer readonly dispid 1071;
    property SMPP_PRIORITYFLAG_URGENT: Integer readonly dispid 1072;
    property SMPP_PRIORITYFLAG_VERY_URGENT: Integer readonly dispid 1073;
    property SMPP_MESSAGESTATE_AX_WAITRESP: Integer readonly dispid 1100;
    property SMPP_MESSAGESTATE_ENROUTE: Integer readonly dispid 1101;
    property SMPP_MESSAGESTATE_DELIVERED: Integer readonly dispid 1102;
    property SMPP_MESSAGESTATE_EXPIRED: Integer readonly dispid 1103;
    property SMPP_MESSAGESTATE_DELETED: Integer readonly dispid 1104;
    property SMPP_MESSAGESTATE_UNDELIVERABLE: Integer readonly dispid 1105;
    property SMPP_MESSAGESTATE_ACCEPTED: Integer readonly dispid 1106;
    property SMPP_MESSAGESTATE_UNKNOWN: Integer readonly dispid 1107;
    property SMPP_MESSAGESTATE_REJECTED: Integer readonly dispid 1108;
    property SMPP_MESSAGESTATE_AX_RESPERROR: Integer readonly dispid 1109;
    property SMPP_MESSAGESTATE_AX_NOCREDITS: Integer readonly dispid 1110;
    property SMPP_MESSAGESTATE_AX_RESPTO: Integer readonly dispid 1111;
    property SMPP_MESSAGESTATE_AX_RESPONDED: Integer readonly dispid 1112;
    property SMPP_SESSIONSTATE_CONNECTED: Integer readonly dispid 1203;
    property SMPP_SESSIONSTATE_DISCONNECTED: Integer readonly dispid 1204;
    property SMPP_SESSIONSTATE_BINDING: Integer readonly dispid 1205;
    property SMPP_SESSIONSTATE_BOUND_TX: Integer readonly dispid 1206;
    property SMPP_SESSIONSTATE_BOUND_RX: Integer readonly dispid 1207;
    property SMPP_SESSIONSTATE_BOUND_TRX: Integer readonly dispid 1208;
    property SMPP_TLV_DEST_ADDR_SUBUNIT: Integer readonly dispid 1300;
    property SMPP_TLV_DEST_NETWORK_TYPE: Integer readonly dispid 1301;
    property SMPP_TLV_DEST_BEARER_TYPE: Integer readonly dispid 1302;
    property SMPP_TLV_DEST_TELEMATICS_ID: Integer readonly dispid 1303;
    property SMPP_TLV_SOURCE_ADDR_SUBUNIT: Integer readonly dispid 1304;
    property SMPP_TLV_SOURCE_NETWORK_TYPE: Integer readonly dispid 1305;
    property SMPP_TLV_SOURCE_BEARER_TYPE: Integer readonly dispid 1306;
    property SMPP_TLV_SOURCE_TELEMATICS_ID: Integer readonly dispid 1307;
    property SMPP_TLV_QOS_TIME_TO_LIVE: Integer readonly dispid 1308;
    property SMPP_TLV_PAYLOAD_TYPE: Integer readonly dispid 1309;
    property SMPP_TLV_ADDITIONAL_STATUS_INFO_TEXT: Integer readonly dispid 1310;
    property SMPP_TLV_RECEIPTED_MESSAGE_ID: Integer readonly dispid 1311;
    property SMPP_TLV_MS_MSG_WAIT_FACILITIES: Integer readonly dispid 1312;
    property SMPP_TLV_PRIVACY_INDICATOR: Integer readonly dispid 1313;
    property SMPP_TLV_SOURCE_SUBADDRESS: Integer readonly dispid 1314;
    property SMPP_TLV_DEST_SUBADDRESS: Integer readonly dispid 1315;
    property SMPP_TLV_USER_MESSAGE_REFERENCE: Integer readonly dispid 1316;
    property SMPP_TLV_USER_RESPONSE_CODE: Integer readonly dispid 1317;
    property SMPP_TLV_SOURCE_PORT: Integer readonly dispid 1318;
    property SMPP_TLV_DESTINATION_PORT: Integer readonly dispid 1319;
    property SMPP_TLV_SAR_MSG_REF_NUM: Integer readonly dispid 1320;
    property SMPP_TLV_LANGUAGE_INDICATOR: Integer readonly dispid 1321;
    property SMPP_TLV_SAR_TOTAL_SEGMENTS: Integer readonly dispid 1322;
    property SMPP_TLV_SAR_SEGMENT_SEQNUM: Integer readonly dispid 1323;
    property SMPP_TLV_SC_INTERFACE_VERSION: Integer readonly dispid 1324;
    property SMPP_TLV_CALLBACK_NUM_PRES_IND: Integer readonly dispid 1325;
    property SMPP_TLV_CALLBACK_NUM_ATAG: Integer readonly dispid 1326;
    property SMPP_TLV_NUMBER_OF_MESSAGES: Integer readonly dispid 1327;
    property SMPP_TLV_CALLBACK_NUM: Integer readonly dispid 1328;
    property SMPP_TLV_DPF_RESULT: Integer readonly dispid 1329;
    property SMPP_TLV_SET_DPF: Integer readonly dispid 1330;
    property SMPP_TLV_MS_AVAILABILITY_STATUS: Integer readonly dispid 1331;
    property SMPP_TLV_NETWORK_ERROR_CODE: Integer readonly dispid 1332;
    property SMPP_TLV_MESSAGE_PAYLOAD: Integer readonly dispid 1333;
    property SMPP_TLV_DELIVERY_FAILURE_REASON: Integer readonly dispid 1334;
    property SMPP_TLV_MORE_MESSAGES_TO_SEND: Integer readonly dispid 1335;
    property SMPP_TLV_MESSAGE_STATE: Integer readonly dispid 1336;
    property SMPP_TLV_CONGESTION_STATE: Integer readonly dispid 1337;
    property SMPP_TLV_USSD_SERVICE_OP: Integer readonly dispid 1338;
    property SMPP_TLV_DISPLAY_TIME: Integer readonly dispid 1339;
    property SMPP_TLV_SMS_SIGNAL: Integer readonly dispid 1340;
    property SMPP_TLV_MS_VALIDITY: Integer readonly dispid 1341;
    property SMPP_TLV_ALERT_ON_MESSAGE_DELIVERY: Integer readonly dispid 1342;
    property SMPP_TLV_ITS_REPLY_TYPE: Integer readonly dispid 1343;
    property SMPP_TLV_ITS_SESSION_INFO: Integer readonly dispid 1344;
    property SMPP_MULTIPARTMODE_UDH: Integer readonly dispid 1401;
    property SMPP_MULTIPARTMODE_UDH16BIT: Integer readonly dispid 1402;
    property SMPP_MULTIPARTMODE_SARTLV: Integer readonly dispid 1403;
    property SMPP_MULTIPARTMODE_PAYLOADTLV: Integer readonly dispid 1404;
    property SMPP_SUBMITMODE_SUBMITSM: Integer readonly dispid 1506;
    property SMPP_SUBMITMODE_DATASM: Integer readonly dispid 1507;
    property SMPP_USEGSMENCODING_INCHARSET: Integer readonly dispid 1551;
    property SMPP_USEGSMENCODING_OUTCHARSET: Integer readonly dispid 1552;
    property SMPP_USEGSMENCODING_INOUTCHARS: Integer readonly dispid 1553;
    property SMPP_IPVERSION_4: Integer readonly dispid 1581;
    property SMPP_IPVERSION_6: Integer readonly dispid 1582;
    property SMPP_IPVERSION_BOTH: Integer readonly dispid 1583;
    property SMPP_ESME_ROK: Integer readonly dispid 1601;
    property SMPP_ESME_RINVMSGLEN: Integer readonly dispid 1602;
    property SMPP_ESME_RINVCMDLEN: Integer readonly dispid 1603;
    property SMPP_ESME_RINVCMDID: Integer readonly dispid 1604;
    property SMPP_ESME_RINVBNDSTS: Integer readonly dispid 1605;
    property SMPP_ESME_RALYBND: Integer readonly dispid 1606;
    property SMPP_ESME_RINVPRTFLG: Integer readonly dispid 1607;
    property SMPP_ESME_RINVREGDLVFLG: Integer readonly dispid 1608;
    property SMPP_ESME_RSYSERR: Integer readonly dispid 1609;
    property SMPP_ESME_RINVSRCADR: Integer readonly dispid 1610;
    property SMPP_ESME_RINVDSTADR: Integer readonly dispid 1611;
    property SMPP_ESME_RINVMSGID: Integer readonly dispid 1612;
    property SMPP_ESME_RBINDFAIL: Integer readonly dispid 1613;
    property SMPP_ESME_RINVPASWD: Integer readonly dispid 1614;
    property SMPP_ESME_RINVSYSID: Integer readonly dispid 1615;
    property SMPP_ESME_RCANCELFAIL: Integer readonly dispid 1616;
    property SMPP_ESME_RREPLACEFAIL: Integer readonly dispid 1617;
    property SMPP_ESME_RMSGQFUL: Integer readonly dispid 1618;
    property SMPP_ESME_RINVSERTYP: Integer readonly dispid 1619;
    property SMPP_ESME_RINVNUMDESTS: Integer readonly dispid 1620;
    property SMPP_ESME_RINVDLNAME: Integer readonly dispid 1621;
    property SMPP_ESME_RINVDESTFLAG: Integer readonly dispid 1622;
    property SMPP_ESME_RINVSUBREP: Integer readonly dispid 1623;
    property SMPP_ESME_RINVESMCLASS: Integer readonly dispid 1624;
    property SMPP_ESME_RCNTSUBDL: Integer readonly dispid 1625;
    property SMPP_ESME_RSUBMITFAIL: Integer readonly dispid 1626;
    property SMPP_ESME_RINVSRCTON: Integer readonly dispid 1627;
    property SMPP_ESME_RINVSRCNPI: Integer readonly dispid 1628;
    property SMPP_ESME_RINVDSTTON: Integer readonly dispid 1629;
    property SMPP_ESME_RINVDSTNPI: Integer readonly dispid 1630;
    property SMPP_ESME_RINVSYSTYP: Integer readonly dispid 1631;
    property SMPP_ESME_RINVREPFLAG: Integer readonly dispid 1632;
    property SMPP_ESME_RINVNUMMSGS: Integer readonly dispid 1633;
    property SMPP_ESME_RTHROTTLED: Integer readonly dispid 1634;
    property SMPP_ESME_RINVSCHED: Integer readonly dispid 1635;
    property SMPP_ESME_RINVEXPIRY: Integer readonly dispid 1636;
    property SMPP_ESME_RINVDFTMSGID: Integer readonly dispid 1637;
    property SMPP_ESME_RX_T_APPN: Integer readonly dispid 1638;
    property SMPP_ESME_RX_P_APPN: Integer readonly dispid 1639;
    property SMPP_ESME_RX_R_APPN: Integer readonly dispid 1640;
    property SMPP_ESME_RQUERYFAIL: Integer readonly dispid 1641;
    property SMPP_ESME_RINVOPTPARSTREAM: Integer readonly dispid 1642;
    property SMPP_ESME_ROPTPARNOTALLWD: Integer readonly dispid 1643;
    property SMPP_ESME_RINVPARLEN: Integer readonly dispid 1644;
    property SMPP_ESME_RMISSINGOPTPARAM: Integer readonly dispid 1645;
    property SMPP_ESME_RINVOPTPARAMVAL: Integer readonly dispid 1646;
    property SMPP_ESME_RDELIVERYFAILURE: Integer readonly dispid 1647;
    property SMPP_ESME_RUNKNOWNERR: Integer readonly dispid 1648;
    property GSM_STATUS_MESSAGE_DELIVERED_SUCCESSFULLY: Integer readonly dispid 1751;
    property GSM_STATUS_FORWARDED_STATUS_UNKNOWN: Integer readonly dispid 1752;
    property GSM_STATUS_REPLACED: Integer readonly dispid 1753;
    property GSM_STATUS_CONGESTION_STILL_TRYING: Integer readonly dispid 1754;
    property GSM_STATUS_RECIPIENT_BUSY_STILL_TRYING: Integer readonly dispid 1755;
    property GSM_STATUS_NO_RESPONSE_STILL_TRYING: Integer readonly dispid 1756;
    property GSM_STATUS_SERVICE_REJECTED_STILL_TRYING: Integer readonly dispid 1757;
    property GSM_STATUS_QOS_NOT_AVAILABLE_STILL_TRYING: Integer readonly dispid 1758;
    property GSM_STATUS_RECIPIENT_ERROR_STILL_TRYING: Integer readonly dispid 1759;
    property GSM_STATUS_RPC_ERROR: Integer readonly dispid 1760;
    property GSM_STATUS_INCOMPATIBLE_DESTINATION: Integer readonly dispid 1761;
    property GSM_STATUS_CONNECTION_REJECTED: Integer readonly dispid 1762;
    property GSM_STATUS_NOT_OBTAINABLE: Integer readonly dispid 1763;
    property GSM_STATUS_QOS_NOT_AVAILABLE: Integer readonly dispid 1764;
    property GSM_STATUS_NO_INTERNETWORKING_AVAILABLE: Integer readonly dispid 1765;
    property GSM_STATUS_MESSAGE_EXPIRED: Integer readonly dispid 1766;
    property GSM_STATUS_MESSAGE_DELETED_BY_SENDER: Integer readonly dispid 1767;
    property GSM_STATUS_MESSAGE_DELETED_BY_SMSC: Integer readonly dispid 1768;
    property GSM_STATUS_DOES_NOT_EXIST: Integer readonly dispid 1769;
    property GSM_STORAGETYPE_SIM: Integer readonly dispid 1871;
    property GSM_STORAGETYPE_MEMORY: Integer readonly dispid 1872;
    property GSM_STORAGETYPE_COMBINED: Integer readonly dispid 1873;
    property GSM_STORAGETYPE_STATUS: Integer readonly dispid 1874;
    property GSM_STORAGETYPE_ALL: Integer readonly dispid 1875;
    property GSM_FO_REPLYPATH_EXISTS: Integer readonly dispid 1901;
    property GSM_FO_UDHI: Integer readonly dispid 1902;
    property GSM_FO_STATUS_REPORT: Integer readonly dispid 1903;
    property GSM_FO_VALIDITY_NONE: Integer readonly dispid 1904;
    property GSM_FO_VALIDITY_RELATIVE: Integer readonly dispid 1905;
    property GSM_FO_VALIDITY_ENHANCED: Integer readonly dispid 1906;
    property GSM_FO_VALIDITY_ABSOLUTE: Integer readonly dispid 1907;
    property GSM_FO_REJECT_DUPLICATES: Integer readonly dispid 1908;
    property GSM_FO_SUBMIT_SM: Integer readonly dispid 1909;
    property GSM_FO_DELIVER_SM: Integer readonly dispid 1911;
    property GSM_FO_STATUS_SM: Integer readonly dispid 1912;
    property DATACODING_DEFAULT: Integer readonly dispid 2021;
    property DATACODING_8BIT_DATA: Integer readonly dispid 2022;
    property DATACODING_UNICODE: Integer readonly dispid 2023;
    property GSM_DATACODING_ME_SPECIFIC: Integer readonly dispid 2024;
    property GSM_DATACODING_SIM_SPECIFIC: Integer readonly dispid 2025;
    property GSM_DATACODING_TE_SPECIFIC: Integer readonly dispid 2026;
    property DATACODING_FLASH: Integer readonly dispid 2027;
    property GSM_BAUDRATE_110: Integer readonly dispid 2131;
    property GSM_BAUDRATE_300: Integer readonly dispid 2132;
    property GSM_BAUDRATE_600: Integer readonly dispid 2133;
    property GSM_BAUDRATE_1200: Integer readonly dispid 2134;
    property GSM_BAUDRATE_2400: Integer readonly dispid 2135;
    property GSM_BAUDRATE_4800: Integer readonly dispid 2136;
    property GSM_BAUDRATE_9600: Integer readonly dispid 2137;
    property GSM_BAUDRATE_14400: Integer readonly dispid 2138;
    property GSM_BAUDRATE_19200: Integer readonly dispid 2139;
    property GSM_BAUDRATE_38400: Integer readonly dispid 2140;
    property GSM_BAUDRATE_56000: Integer readonly dispid 2141;
    property GSM_BAUDRATE_57600: Integer readonly dispid 2142;
    property GSM_BAUDRATE_64000: Integer readonly dispid 2143;
    property GSM_BAUDRATE_115200: Integer readonly dispid 2144;
    property GSM_BAUDRATE_128000: Integer readonly dispid 2145;
    property GSM_BAUDRATE_230400: Integer readonly dispid 2146;
    property GSM_BAUDRATE_256000: Integer readonly dispid 2147;
    property GSM_BAUDRATE_460800: Integer readonly dispid 2148;
    property GSM_BAUDRATE_921600: Integer readonly dispid 2149;
    property GSM_BAUDRATE_DEFAULT: Integer readonly dispid 2150;
    property GSM_MESSAGESTATE_RECEIVED_UNREAD: Integer readonly dispid 2261;
    property GSM_MESSAGESTATE_RECEIVED_READ: Integer readonly dispid 2262;
    property GSM_MESSAGESTATE_STORED_UNSENT: Integer readonly dispid 2263;
    property GSM_MESSAGESTATE_STORED_SENT: Integer readonly dispid 2264;
    property GSM_MESSAGESTATE_ALL: Integer readonly dispid 2265;
    property GSM_MESSAGEFORMAT_PDU: Integer readonly dispid 2371;
    property GSM_MESSAGEFORMAT_TEXT: Integer readonly dispid 2372;
    property GSM_MESSAGEFORMAT_AUTO: Integer readonly dispid 2373;
    property HTTP_PLACEHOLDER_USERTAG: WideString readonly dispid 2401;
    property HTTP_PLACEHOLDER_TOADDRESS: WideString readonly dispid 2402;
    property HTTP_PLACEHOLDER_FROMADDRESS: WideString readonly dispid 2403;
    property HTTP_PLACEHOLDER_BODY: WideString readonly dispid 2404;
    property HTTP_PLACEHOLDER_BODYASHEX: WideString readonly dispid 2405;
    property HTTP_PLACEHOLDER_BODYASBASE64: WideString readonly dispid 2406;
    property HTTP_PLACEHOLDER_DELIVERYREPORT: WideString readonly dispid 2407;
    property HTTP_PLACEHOLDER_TOADDRESSTON: WideString readonly dispid 2408;
    property HTTP_PLACEHOLDER_TOADDRESSNPI: WideString readonly dispid 2409;
    property HTTP_PLACEHOLDER_FROMADDRESSTON: WideString readonly dispid 2410;
    property HTTP_PLACEHOLDER_FROMADDRESSNPI: WideString readonly dispid 2411;
    property HTTP_PLACEHOLDER_PROTOCOLID: WideString readonly dispid 2412;
    property HTTP_PLACEHOLDER_UDHI: WideString readonly dispid 2413;
    property HTTP_PLACEHOLDER_DATACODING: WideString readonly dispid 2414;
    property GSM_PREFIXSMSC_ENABLED: Integer readonly dispid 2501;
    property GSM_PREFIXSMSC_DISABLED: Integer readonly dispid 2502;
    property GSM_PREFIXSMSC_AUTO: Integer readonly dispid 2503;
    property WAPPUSH_SIGNAL_NONE: Integer readonly dispid 2611;
    property WAPPUSH_SIGNAL_LOW: Integer readonly dispid 2612;
    property WAPPUSH_SIGNAL_MEDIUM: Integer readonly dispid 2613;
    property WAPPUSH_SIGNAL_HIGH: Integer readonly dispid 2614;
    property WAPPUSH_SIGNAL_DELETE: Integer readonly dispid 2615;
    property DIALUP_PROVIDERTYPE_UCP: Integer readonly dispid 2701;
    property DIALUP_PROVIDERTYPE_TAP_DEFAULT: Integer readonly dispid 2702;
    property DIALUP_PROVIDERTYPE_TAP_NOLF: Integer readonly dispid 2703;
    property DIALUP_PROVIDERTYPE_TAP_NOEOT: Integer readonly dispid 2704;
    property DIALUP_DIALMODE_TONE: Integer readonly dispid 2811;
    property DIALUP_DIALMODE_PULSE: Integer readonly dispid 2812;
    property DIALUP_DEVICESETTINGS_DEFAULT: Integer readonly dispid 2921;
    property DIALUP_DEVICESETTINGS_8N1: Integer readonly dispid 2922;
    property DIALUP_DEVICESETTINGS_7E1: Integer readonly dispid 2923;
    function SmppBindToString(lVal: Integer): WideString; dispid 10001;
    function SmppVersionToString(lVal: Integer): WideString; dispid 10002;
    function TonToString(lVal: Integer): WideString; dispid 10003;
    function NpiToString(lVal: Integer): WideString; dispid 10004;
    function MultipartToString(lVal: Integer): WideString; dispid 10005;
    function BodyformatToString(lVal: Integer): WideString; dispid 10006;
    function SmppEsm2SmscToString(lVal: Integer): WideString; dispid 10007;
    function SmppEsm2EsmeToString(lVal: Integer): WideString; dispid 10008;
    function SmppUseGsmEncodingToString(lVal: Integer): WideString; dispid 10009;
    function SmppDataCodingToString(lVal: Integer): WideString; dispid 10010;
    function SmppPriorityFlagToString(lVal: Integer): WideString; dispid 10011;
    function SmppMessageStateToString(lVal: Integer): WideString; dispid 10012;
    function SmppSessionStateToString(lVal: Integer): WideString; dispid 10013;
    function SmppTlvToString(lVal: Integer): WideString; dispid 10014;
    function SmppMultipartModeToString(lVal: Integer): WideString; dispid 10015;
    function SmppSubmitModeToString(lVal: Integer): WideString; dispid 10016;
    function SmppEsmeToString(lVal: Integer): WideString; dispid 10017;
    function GsmStatusToString(lVal: Integer): WideString; dispid 10018;
    function GsmStorageTypeToString(lVal: Integer): WideString; dispid 10019;
    function GsmFoToString(lVal: Integer): WideString; dispid 10020;
    function GsmDataCodingToString(lVal: Integer): WideString; dispid 10021;
    function GsmBaudrateToString(lVal: Integer): WideString; dispid 10022;
    function GsmMessageStateToString(lVal: Integer): WideString; dispid 10023;
    function GsmMessageFormatToString(lVal: Integer): WideString; dispid 10024;
    function GsmPrefixSmscToString(lVal: Integer): WideString; dispid 10025;
    function WapPushSignalToString(lVal: Integer): WideString; dispid 10026;
    function DialupProviderTypeToString(lVal: Integer): WideString; dispid 10027;
    function DialupDialModeToString(lVal: Integer): WideString; dispid 10028;
    function DialupDeviceSettingsToString(lVal: Integer): WideString; dispid 10029;
  end;

// *********************************************************************//
// Interface: IMessage
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {DFD486FC-45B4-49AB-AD5A-98256AE057FC}
// *********************************************************************//
  IMessage = interface(IDispatch)
    ['{DFD486FC-45B4-49AB-AD5A-98256AE057FC}']
    procedure Clear; safecall;
    function Get_UserTag: Integer; safecall;
    procedure Set_UserTag(Val: Integer); safecall;
    function Get_ToAddress: WideString; safecall;
    procedure Set_ToAddress(const Val: WideString); safecall;
    function Get_FromAddress: WideString; safecall;
    procedure Set_FromAddress(const Val: WideString); safecall;
    function Get_Body: WideString; safecall;
    procedure Set_Body(const Val: WideString); safecall;
    function Get_RequestDeliveryReport: WordBool; safecall;
    procedure Set_RequestDeliveryReport(Val: WordBool); safecall;
    function Get_ToAddressTON: Integer; safecall;
    procedure Set_ToAddressTON(Val: Integer); safecall;
    function Get_ToAddressNPI: Integer; safecall;
    procedure Set_ToAddressNPI(Val: Integer); safecall;
    function Get_FromAddressTON: Integer; safecall;
    procedure Set_FromAddressTON(Val: Integer); safecall;
    function Get_FromAddressNPI: Integer; safecall;
    procedure Set_FromAddressNPI(Val: Integer); safecall;
    function Get_ProtocolId: Integer; safecall;
    procedure Set_ProtocolId(Val: Integer); safecall;
    function Get_ValidityPeriod: Integer; safecall;
    procedure Set_ValidityPeriod(Val: Integer); safecall;
    function Get_Reference: WideString; safecall;
    procedure Set_Reference(const Val: WideString); safecall;
    function Get_DataCoding: Integer; safecall;
    procedure Set_DataCoding(Val: Integer); safecall;
    function Get_BodyFormat: Integer; safecall;
    procedure Set_BodyFormat(Val: Integer); safecall;
    function Get_TotalParts: Integer; safecall;
    function Get_PartNumber: Integer; safecall;
    function Get_ReceiveTime: WideString; safecall;
    function Get_ReceiveTimeInSeconds: Integer; safecall;
    function Get_HasUdh: WordBool; safecall;
    procedure Set_HasUdh(Val: WordBool); safecall;
    function Get_Incomplete: WordBool; safecall;
    function Get_GsmFirstOctet: Integer; safecall;
    procedure Set_GsmFirstOctet(Val: Integer); safecall;
    function Get_GsmSmscAddress: WideString; safecall;
    function Get_GsmSmscAddressTON: Integer; safecall;
    function Get_GsmSmscAddressNPI: Integer; safecall;
    function Get_GsmMemoryIndex: WideString; safecall;
    procedure Set_GsmMemoryIndex(const Val: WideString); safecall;
    function Get_GsmMemoryLocation: WideString; safecall;
    procedure Set_GsmMemoryLocation(const Val: WideString); safecall;
    function Get_SmppPriority: Integer; safecall;
    procedure Set_SmppPriority(Val: Integer); safecall;
    function Get_SmppServiceType: WideString; safecall;
    procedure Set_SmppServiceType(const Val: WideString); safecall;
    function Get_SmppEsmClass: Integer; safecall;
    procedure Set_SmppEsmClass(Val: Integer); safecall;
    function Get_SmppIsDeliveryReport: WordBool; safecall;
    procedure Set_SmppIsDeliveryReport(Val: WordBool); safecall;
    function Get_SmppStatus: Integer; safecall;
    procedure Set_SmppStatus(Val: Integer); safecall;
    function Get_SmppError: Integer; safecall;
    procedure Set_SmppError(Val: Integer); safecall;
    function Get_SmppCommandStatus: Integer; safecall;
    procedure Set_SmppCommandStatus(Val: Integer); safecall;
    function Get_SmppSequenceNumber: Integer; safecall;
    function Get_SmppServerSubmitDate: WideString; safecall;
    function Get_SmppServerFinalDate: WideString; safecall;
    procedure SmppAddTlv(const Tlv: ITlv); safecall;
    function SmppGetFirstTlv: ITlv; safecall;
    function SmppGetNextTlv: ITlv; safecall;
    function SmppGetTlv(lTag: Integer): ITlv; safecall;
    function Get_LastError: Integer; safecall;
    function GetErrorDescription(Error: Integer): WideString; safecall;
    property UserTag: Integer read Get_UserTag write Set_UserTag;
    property ToAddress: WideString read Get_ToAddress write Set_ToAddress;
    property FromAddress: WideString read Get_FromAddress write Set_FromAddress;
    property Body: WideString read Get_Body write Set_Body;
    property RequestDeliveryReport: WordBool read Get_RequestDeliveryReport write Set_RequestDeliveryReport;
    property ToAddressTON: Integer read Get_ToAddressTON write Set_ToAddressTON;
    property ToAddressNPI: Integer read Get_ToAddressNPI write Set_ToAddressNPI;
    property FromAddressTON: Integer read Get_FromAddressTON write Set_FromAddressTON;
    property FromAddressNPI: Integer read Get_FromAddressNPI write Set_FromAddressNPI;
    property ProtocolId: Integer read Get_ProtocolId write Set_ProtocolId;
    property ValidityPeriod: Integer read Get_ValidityPeriod write Set_ValidityPeriod;
    property Reference: WideString read Get_Reference write Set_Reference;
    property DataCoding: Integer read Get_DataCoding write Set_DataCoding;
    property BodyFormat: Integer read Get_BodyFormat write Set_BodyFormat;
    property TotalParts: Integer read Get_TotalParts;
    property PartNumber: Integer read Get_PartNumber;
    property ReceiveTime: WideString read Get_ReceiveTime;
    property ReceiveTimeInSeconds: Integer read Get_ReceiveTimeInSeconds;
    property HasUdh: WordBool read Get_HasUdh write Set_HasUdh;
    property Incomplete: WordBool read Get_Incomplete;
    property GsmFirstOctet: Integer read Get_GsmFirstOctet write Set_GsmFirstOctet;
    property GsmSmscAddress: WideString read Get_GsmSmscAddress;
    property GsmSmscAddressTON: Integer read Get_GsmSmscAddressTON;
    property GsmSmscAddressNPI: Integer read Get_GsmSmscAddressNPI;
    property GsmMemoryIndex: WideString read Get_GsmMemoryIndex write Set_GsmMemoryIndex;
    property GsmMemoryLocation: WideString read Get_GsmMemoryLocation write Set_GsmMemoryLocation;
    property SmppPriority: Integer read Get_SmppPriority write Set_SmppPriority;
    property SmppServiceType: WideString read Get_SmppServiceType write Set_SmppServiceType;
    property SmppEsmClass: Integer read Get_SmppEsmClass write Set_SmppEsmClass;
    property SmppIsDeliveryReport: WordBool read Get_SmppIsDeliveryReport write Set_SmppIsDeliveryReport;
    property SmppStatus: Integer read Get_SmppStatus write Set_SmppStatus;
    property SmppError: Integer read Get_SmppError write Set_SmppError;
    property SmppCommandStatus: Integer read Get_SmppCommandStatus write Set_SmppCommandStatus;
    property SmppSequenceNumber: Integer read Get_SmppSequenceNumber;
    property SmppServerSubmitDate: WideString read Get_SmppServerSubmitDate;
    property SmppServerFinalDate: WideString read Get_SmppServerFinalDate;
    property LastError: Integer read Get_LastError;
  end;

// *********************************************************************//
// DispIntf:  IMessageDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {DFD486FC-45B4-49AB-AD5A-98256AE057FC}
// *********************************************************************//
  IMessageDisp = dispinterface
    ['{DFD486FC-45B4-49AB-AD5A-98256AE057FC}']
    procedure Clear; dispid 1;
    property UserTag: Integer dispid 2;
    property ToAddress: WideString dispid 3;
    property FromAddress: WideString dispid 4;
    property Body: WideString dispid 5;
    property RequestDeliveryReport: WordBool dispid 7;
    property ToAddressTON: Integer dispid 8;
    property ToAddressNPI: Integer dispid 9;
    property FromAddressTON: Integer dispid 10;
    property FromAddressNPI: Integer dispid 11;
    property ProtocolId: Integer dispid 12;
    property ValidityPeriod: Integer dispid 13;
    property Reference: WideString dispid 14;
    property DataCoding: Integer dispid 15;
    property BodyFormat: Integer dispid 16;
    property TotalParts: Integer readonly dispid 17;
    property PartNumber: Integer readonly dispid 18;
    property ReceiveTime: WideString readonly dispid 19;
    property ReceiveTimeInSeconds: Integer readonly dispid 20;
    property HasUdh: WordBool dispid 21;
    property Incomplete: WordBool readonly dispid 22;
    property GsmFirstOctet: Integer dispid 51;
    property GsmSmscAddress: WideString readonly dispid 52;
    property GsmSmscAddressTON: Integer readonly dispid 53;
    property GsmSmscAddressNPI: Integer readonly dispid 54;
    property GsmMemoryIndex: WideString dispid 55;
    property GsmMemoryLocation: WideString dispid 56;
    property SmppPriority: Integer dispid 71;
    property SmppServiceType: WideString dispid 72;
    property SmppEsmClass: Integer dispid 73;
    property SmppIsDeliveryReport: WordBool dispid 74;
    property SmppStatus: Integer dispid 75;
    property SmppError: Integer dispid 76;
    property SmppCommandStatus: Integer dispid 77;
    property SmppSequenceNumber: Integer readonly dispid 78;
    property SmppServerSubmitDate: WideString readonly dispid 79;
    property SmppServerFinalDate: WideString readonly dispid 80;
    procedure SmppAddTlv(const Tlv: ITlv); dispid 91;
    function SmppGetFirstTlv: ITlv; dispid 92;
    function SmppGetNextTlv: ITlv; dispid 93;
    function SmppGetTlv(lTag: Integer): ITlv; dispid 94;
    property LastError: Integer readonly dispid 101;
    function GetErrorDescription(Error: Integer): WideString; dispid 102;
  end;

// *********************************************************************//
// Interface: ITlv
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {4687CEE3-3966-46A2-AEE3-5A747F455B84}
// *********************************************************************//
  ITlv = interface(IDispatch)
    ['{4687CEE3-3966-46A2-AEE3-5A747F455B84}']
    procedure Clear; safecall;
    function Get_Tag: Integer; safecall;
    procedure Set_Tag(Val: Integer); safecall;
    function Get_ValueAsString: WideString; safecall;
    procedure Set_ValueAsString(const Val: WideString); safecall;
    function Get_ValueAsHexString: WideString; safecall;
    procedure Set_ValueAsHexString(const Val: WideString); safecall;
    function Get_ValueAsInt32: Integer; safecall;
    procedure Set_ValueAsInt32(Val: Integer); safecall;
    function Get_ValueAsInt16: Integer; safecall;
    procedure Set_ValueAsInt16(Val: Integer); safecall;
    function Get_ValueAsInt8: Integer; safecall;
    procedure Set_ValueAsInt8(Val: Integer); safecall;
    function Get_Length: Integer; safecall;
    property Tag: Integer read Get_Tag write Set_Tag;
    property ValueAsString: WideString read Get_ValueAsString write Set_ValueAsString;
    property ValueAsHexString: WideString read Get_ValueAsHexString write Set_ValueAsHexString;
    property ValueAsInt32: Integer read Get_ValueAsInt32 write Set_ValueAsInt32;
    property ValueAsInt16: Integer read Get_ValueAsInt16 write Set_ValueAsInt16;
    property ValueAsInt8: Integer read Get_ValueAsInt8 write Set_ValueAsInt8;
    property Length: Integer read Get_Length;
  end;

// *********************************************************************//
// DispIntf:  ITlvDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {4687CEE3-3966-46A2-AEE3-5A747F455B84}
// *********************************************************************//
  ITlvDisp = dispinterface
    ['{4687CEE3-3966-46A2-AEE3-5A747F455B84}']
    procedure Clear; dispid 1;
    property Tag: Integer dispid 2;
    property ValueAsString: WideString dispid 3;
    property ValueAsHexString: WideString dispid 4;
    property ValueAsInt32: Integer dispid 5;
    property ValueAsInt16: Integer dispid 6;
    property ValueAsInt8: Integer dispid 7;
    property Length: Integer readonly dispid 8;
  end;

// *********************************************************************//
// Interface: IGsmDeliveryReport
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {6962C3D8-88FE-4660-A4EC-901A244A57D2}
// *********************************************************************//
  IGsmDeliveryReport = interface(IDispatch)
    ['{6962C3D8-88FE-4660-A4EC-901A244A57D2}']
    procedure Clear; safecall;
    function Get_Reference: WideString; safecall;
    function Get_UserTag: Integer; safecall;
    procedure Set_UserTag(Val: Integer); safecall;
    function Get_SmscAddress: WideString; safecall;
    function Get_SmscTime: WideString; safecall;
    function Get_SmscTimeInSeconds: Integer; safecall;
    function Get_DischargeTime: WideString; safecall;
    function Get_DischargeTimeInSeconds: Integer; safecall;
    function Get_MemoryIndex: WideString; safecall;
    function Get_FirstOctet: Integer; safecall;
    function Get_SmscTON: Integer; safecall;
    function Get_SmscNPI: Integer; safecall;
    function Get_TON: Integer; safecall;
    function Get_NPI: Integer; safecall;
    function Get_Status: Integer; safecall;
    function Get_FromAddress: WideString; safecall;
    function Get_MemoryLocation: WideString; safecall;
    property Reference: WideString read Get_Reference;
    property UserTag: Integer read Get_UserTag write Set_UserTag;
    property SmscAddress: WideString read Get_SmscAddress;
    property SmscTime: WideString read Get_SmscTime;
    property SmscTimeInSeconds: Integer read Get_SmscTimeInSeconds;
    property DischargeTime: WideString read Get_DischargeTime;
    property DischargeTimeInSeconds: Integer read Get_DischargeTimeInSeconds;
    property MemoryIndex: WideString read Get_MemoryIndex;
    property FirstOctet: Integer read Get_FirstOctet;
    property SmscTON: Integer read Get_SmscTON;
    property SmscNPI: Integer read Get_SmscNPI;
    property TON: Integer read Get_TON;
    property NPI: Integer read Get_NPI;
    property Status: Integer read Get_Status;
    property FromAddress: WideString read Get_FromAddress;
    property MemoryLocation: WideString read Get_MemoryLocation;
  end;

// *********************************************************************//
// DispIntf:  IGsmDeliveryReportDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {6962C3D8-88FE-4660-A4EC-901A244A57D2}
// *********************************************************************//
  IGsmDeliveryReportDisp = dispinterface
    ['{6962C3D8-88FE-4660-A4EC-901A244A57D2}']
    procedure Clear; dispid 1;
    property Reference: WideString readonly dispid 2;
    property UserTag: Integer dispid 3;
    property SmscAddress: WideString readonly dispid 4;
    property SmscTime: WideString readonly dispid 5;
    property SmscTimeInSeconds: Integer readonly dispid 6;
    property DischargeTime: WideString readonly dispid 7;
    property DischargeTimeInSeconds: Integer readonly dispid 8;
    property MemoryIndex: WideString readonly dispid 9;
    property FirstOctet: Integer readonly dispid 10;
    property SmscTON: Integer readonly dispid 11;
    property SmscNPI: Integer readonly dispid 12;
    property TON: Integer readonly dispid 13;
    property NPI: Integer readonly dispid 14;
    property Status: Integer readonly dispid 15;
    property FromAddress: WideString readonly dispid 16;
    property MemoryLocation: WideString readonly dispid 17;
  end;

// *********************************************************************//
// Interface: IDialup
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {3E9851DF-D82F-4806-8716-769C015C23F2}
// *********************************************************************//
  IDialup = interface(IDispatch)
    ['{3E9851DF-D82F-4806-8716-769C015C23F2}']
    function Get_Version: WideString; safecall;
    function Get_Build: WideString; safecall;
    function Get_Module: WideString; safecall;
    function Get_LicenseStatus: WideString; safecall;
    function Get_LicenseKey: WideString; safecall;
    procedure Set_LicenseKey(const LicenseKey: WideString); safecall;
    procedure SaveLicenseKey; safecall;
    function Get_LogFile: WideString; safecall;
    procedure Set_LogFile(const LogFile: WideString); safecall;
    procedure Sleep(Ms: Integer); safecall;
    procedure Clear; safecall;
    function Get_LastError: Integer; safecall;
    function GetErrorDescription(ErrorCode: Integer): WideString; safecall;
    function Get_Device: WideString; safecall;
    procedure Set_Device(const Val: WideString); safecall;
    function Get_DeviceSpeed: Integer; safecall;
    procedure Set_DeviceSpeed(Val: Integer); safecall;
    function Get_DeviceSettings: Integer; safecall;
    procedure Set_DeviceSettings(Val: Integer); safecall;
    function Get_DeviceInitString: WideString; safecall;
    procedure Set_DeviceInitString(const Val: WideString); safecall;
    function Get_DialMode: Integer; safecall;
    procedure Set_DialMode(Val: Integer); safecall;
    function Get_ProviderDialString: WideString; safecall;
    procedure Set_ProviderDialString(const Val: WideString); safecall;
    function Get_ProviderPassword: WideString; safecall;
    procedure Set_ProviderPassword(const Val: WideString); safecall;
    function Get_ProviderType: Integer; safecall;
    procedure Set_ProviderType(Val: Integer); safecall;
    function Get_ProviderResponse: WideString; safecall;
    function Get_AdvancedSettings: WideString; safecall;
    procedure Set_AdvancedSettings(const Val: WideString); safecall;
    function GetDeviceCount: Integer; safecall;
    function GetDevice(lIndex: Integer): WideString; safecall;
    procedure Send(const Message: IMessage); safecall;
    procedure ProviderLoadConfig(const FileName: WideString); safecall;
    procedure ProviderSaveConfig(const FileName: WideString); safecall;
    property Version: WideString read Get_Version;
    property Build: WideString read Get_Build;
    property Module: WideString read Get_Module;
    property LicenseStatus: WideString read Get_LicenseStatus;
    property LicenseKey: WideString read Get_LicenseKey write Set_LicenseKey;
    property LogFile: WideString read Get_LogFile write Set_LogFile;
    property LastError: Integer read Get_LastError;
    property Device: WideString read Get_Device write Set_Device;
    property DeviceSpeed: Integer read Get_DeviceSpeed write Set_DeviceSpeed;
    property DeviceSettings: Integer read Get_DeviceSettings write Set_DeviceSettings;
    property DeviceInitString: WideString read Get_DeviceInitString write Set_DeviceInitString;
    property DialMode: Integer read Get_DialMode write Set_DialMode;
    property ProviderDialString: WideString read Get_ProviderDialString write Set_ProviderDialString;
    property ProviderPassword: WideString read Get_ProviderPassword write Set_ProviderPassword;
    property ProviderType: Integer read Get_ProviderType write Set_ProviderType;
    property ProviderResponse: WideString read Get_ProviderResponse;
    property AdvancedSettings: WideString read Get_AdvancedSettings write Set_AdvancedSettings;
  end;

// *********************************************************************//
// DispIntf:  IDialupDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {3E9851DF-D82F-4806-8716-769C015C23F2}
// *********************************************************************//
  IDialupDisp = dispinterface
    ['{3E9851DF-D82F-4806-8716-769C015C23F2}']
    property Version: WideString readonly dispid 1;
    property Build: WideString readonly dispid 2;
    property Module: WideString readonly dispid 3;
    property LicenseStatus: WideString readonly dispid 100;
    property LicenseKey: WideString dispid 101;
    procedure SaveLicenseKey; dispid 102;
    property LogFile: WideString dispid 201;
    procedure Sleep(Ms: Integer); dispid 300;
    procedure Clear; dispid 301;
    property LastError: Integer readonly dispid 400;
    function GetErrorDescription(ErrorCode: Integer): WideString; dispid 401;
    property Device: WideString dispid 501;
    property DeviceSpeed: Integer dispid 502;
    property DeviceSettings: Integer dispid 503;
    property DeviceInitString: WideString dispid 504;
    property DialMode: Integer dispid 514;
    property ProviderDialString: WideString dispid 515;
    property ProviderPassword: WideString dispid 516;
    property ProviderType: Integer dispid 517;
    property ProviderResponse: WideString readonly dispid 520;
    property AdvancedSettings: WideString dispid 527;
    function GetDeviceCount: Integer; dispid 601;
    function GetDevice(lIndex: Integer): WideString; dispid 602;
    procedure Send(const Message: IMessage); dispid 603;
    procedure ProviderLoadConfig(const FileName: WideString); dispid 706;
    procedure ProviderSaveConfig(const FileName: WideString); dispid 707;
  end;

// *********************************************************************//
// Interface: IGsm
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {B25B188D-42A8-45EF-887F-653CBC9E234B}
// *********************************************************************//
  IGsm = interface(IDispatch)
    ['{B25B188D-42A8-45EF-887F-653CBC9E234B}']
    function Get_Version: WideString; safecall;
    function Get_Build: WideString; safecall;
    function Get_Module: WideString; safecall;
    function Get_LicenseStatus: WideString; safecall;
    function Get_LicenseKey: WideString; safecall;
    procedure Set_LicenseKey(const LicenseKey: WideString); safecall;
    procedure SaveLicenseKey; safecall;
    function Get_LogFile: WideString; safecall;
    procedure Set_LogFile(const LogFile: WideString); safecall;
    function Get_ActivityFile: WideString; safecall;
    procedure Set_ActivityFile(const ActivityFile: WideString); safecall;
    procedure Sleep(Ms: Integer); safecall;
    procedure Clear; safecall;
    function Get_LastError: Integer; safecall;
    function GetErrorDescription(ErrorCode: Integer): WideString; safecall;
    function FindFirstPort: WideString; safecall;
    function FindNextPort: WideString; safecall;
    function FindFirstDevice: WideString; safecall;
    function FindNextDevice: WideString; safecall;
    procedure Open(const strName: WideString; const strPin: WideString; lBaudrate: Integer); safecall;
    procedure Close; safecall;
    procedure SendCommand(const strCommand: WideString); safecall;
    function ReadResponse(lTimeout: Integer): WideString; safecall;
    function Get_Manufacturer: WideString; safecall;
    function Get_Model: WideString; safecall;
    function Get_Revision: WideString; safecall;
    function Get_SerialNr: WideString; safecall;
    function Get_SendEnabled: WordBool; safecall;
    function Get_ReceiveEnabled: WordBool; safecall;
    function Get_ReportEnabled: WordBool; safecall;
    function SendSms(const Val: IMessage; lMultipartFlag: Integer; lTimeout: Integer): WideString; safecall;
    procedure Receive(lType: Integer; bDelete: WordBool; lStorageType: Integer; lTimeout: Integer); safecall;
    function GetFirstSms: IMessage; safecall;
    function GetNextSms: IMessage; safecall;
    function GetFirstReport: IGsmDeliveryReport; safecall;
    function GetNextReport: IGsmDeliveryReport; safecall;
    procedure DeleteSms(const pSms: IMessage); safecall;
    procedure DeleteReport(const pReport: IGsmDeliveryReport); safecall;
    function Get_ExtractApplicationPort: WordBool; safecall;
    procedure Set_ExtractApplicationPort(Val: WordBool); safecall;
    function Get_MultipartTimeout: Integer; safecall;
    procedure Set_MultipartTimeout(Val: Integer); safecall;
    function Get_AssembleMultipart: WordBool; safecall;
    procedure Set_AssembleMultipart(Val: WordBool); safecall;
    function Get_MessageMode: Integer; safecall;
    procedure Set_MessageMode(Val: Integer); safecall;
    function Get_PrefixSmscMode: Integer; safecall;
    procedure Set_PrefixSmscMode(Val: Integer); safecall;
    function Get_NetworkTimeout: Integer; safecall;
    procedure Set_NetworkTimeout(Val: Integer); safecall;
    function Get_InterCommandDelay: Integer; safecall;
    procedure Set_InterCommandDelay(Val: Integer); safecall;
    function Get_InterCharacterDelay: Integer; safecall;
    procedure Set_InterCharacterDelay(Val: Integer); safecall;
    function Get_WaitForNetwork: WordBool; safecall;
    procedure Set_WaitForNetwork(Val: WordBool); safecall;
    function Get_PreferredSmsc: WideString; safecall;
    procedure Set_PreferredSmsc(const Val: WideString); safecall;
    property Version: WideString read Get_Version;
    property Build: WideString read Get_Build;
    property Module: WideString read Get_Module;
    property LicenseStatus: WideString read Get_LicenseStatus;
    property LicenseKey: WideString read Get_LicenseKey write Set_LicenseKey;
    property LogFile: WideString read Get_LogFile write Set_LogFile;
    property ActivityFile: WideString read Get_ActivityFile write Set_ActivityFile;
    property LastError: Integer read Get_LastError;
    property Manufacturer: WideString read Get_Manufacturer;
    property Model: WideString read Get_Model;
    property Revision: WideString read Get_Revision;
    property SerialNr: WideString read Get_SerialNr;
    property SendEnabled: WordBool read Get_SendEnabled;
    property ReceiveEnabled: WordBool read Get_ReceiveEnabled;
    property ReportEnabled: WordBool read Get_ReportEnabled;
    property ExtractApplicationPort: WordBool read Get_ExtractApplicationPort write Set_ExtractApplicationPort;
    property MultipartTimeout: Integer read Get_MultipartTimeout write Set_MultipartTimeout;
    property AssembleMultipart: WordBool read Get_AssembleMultipart write Set_AssembleMultipart;
    property MessageMode: Integer read Get_MessageMode write Set_MessageMode;
    property PrefixSmscMode: Integer read Get_PrefixSmscMode write Set_PrefixSmscMode;
    property NetworkTimeout: Integer read Get_NetworkTimeout write Set_NetworkTimeout;
    property InterCommandDelay: Integer read Get_InterCommandDelay write Set_InterCommandDelay;
    property InterCharacterDelay: Integer read Get_InterCharacterDelay write Set_InterCharacterDelay;
    property WaitForNetwork: WordBool read Get_WaitForNetwork write Set_WaitForNetwork;
    property PreferredSmsc: WideString read Get_PreferredSmsc write Set_PreferredSmsc;
  end;

// *********************************************************************//
// DispIntf:  IGsmDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {B25B188D-42A8-45EF-887F-653CBC9E234B}
// *********************************************************************//
  IGsmDisp = dispinterface
    ['{B25B188D-42A8-45EF-887F-653CBC9E234B}']
    property Version: WideString readonly dispid 1;
    property Build: WideString readonly dispid 2;
    property Module: WideString readonly dispid 3;
    property LicenseStatus: WideString readonly dispid 100;
    property LicenseKey: WideString dispid 101;
    procedure SaveLicenseKey; dispid 102;
    property LogFile: WideString dispid 201;
    property ActivityFile: WideString dispid 202;
    procedure Sleep(Ms: Integer); dispid 300;
    procedure Clear; dispid 301;
    property LastError: Integer readonly dispid 400;
    function GetErrorDescription(ErrorCode: Integer): WideString; dispid 401;
    function FindFirstPort: WideString; dispid 501;
    function FindNextPort: WideString; dispid 502;
    function FindFirstDevice: WideString; dispid 503;
    function FindNextDevice: WideString; dispid 504;
    procedure Open(const strName: WideString; const strPin: WideString; lBaudrate: Integer); dispid 601;
    procedure Close; dispid 602;
    procedure SendCommand(const strCommand: WideString); dispid 603;
    function ReadResponse(lTimeout: Integer): WideString; dispid 604;
    property Manufacturer: WideString readonly dispid 701;
    property Model: WideString readonly dispid 702;
    property Revision: WideString readonly dispid 703;
    property SerialNr: WideString readonly dispid 704;
    property SendEnabled: WordBool readonly dispid 801;
    property ReceiveEnabled: WordBool readonly dispid 802;
    property ReportEnabled: WordBool readonly dispid 803;
    function SendSms(const Val: IMessage; lMultipartFlag: Integer; lTimeout: Integer): WideString; dispid 901;
    procedure Receive(lType: Integer; bDelete: WordBool; lStorageType: Integer; lTimeout: Integer); dispid 902;
    function GetFirstSms: IMessage; dispid 903;
    function GetNextSms: IMessage; dispid 904;
    function GetFirstReport: IGsmDeliveryReport; dispid 905;
    function GetNextReport: IGsmDeliveryReport; dispid 906;
    procedure DeleteSms(const pSms: IMessage); dispid 907;
    procedure DeleteReport(const pReport: IGsmDeliveryReport); dispid 908;
    property ExtractApplicationPort: WordBool dispid 1001;
    property MultipartTimeout: Integer dispid 1002;
    property AssembleMultipart: WordBool dispid 1003;
    property MessageMode: Integer dispid 1004;
    property PrefixSmscMode: Integer dispid 1005;
    property NetworkTimeout: Integer dispid 1007;
    property InterCommandDelay: Integer dispid 1008;
    property InterCharacterDelay: Integer dispid 1009;
    property WaitForNetwork: WordBool dispid 1010;
    property PreferredSmsc: WideString dispid 1011;
  end;

// *********************************************************************//
// Interface: IHttp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {1825A7C6-DBAB-4B99-AAA1-FDEDF36A4982}
// *********************************************************************//
  IHttp = interface(IDispatch)
    ['{1825A7C6-DBAB-4B99-AAA1-FDEDF36A4982}']
    function Get_Version: WideString; safecall;
    function Get_Build: WideString; safecall;
    function Get_Module: WideString; safecall;
    function Get_LicenseStatus: WideString; safecall;
    function Get_LicenseKey: WideString; safecall;
    procedure Set_LicenseKey(const LicenseKey: WideString); safecall;
    procedure SaveLicenseKey; safecall;
    function Get_LogFile: WideString; safecall;
    procedure Set_LogFile(const LogFile: WideString); safecall;
    function Get_ActivityFile: WideString; safecall;
    procedure Set_ActivityFile(const ActivityFile: WideString); safecall;
    procedure Sleep(Ms: Integer); safecall;
    procedure Clear; safecall;
    function Get_LastError: Integer; safecall;
    function GetErrorDescription(ErrorCode: Integer): WideString; safecall;
    function Get(const strUrl: WideString): WideString; safecall;
    function Post(const strUrl: WideString; const strPostBody: WideString): WideString; safecall;
    function Get_Url: WideString; safecall;
    procedure Set_Url(const Val: WideString); safecall;
    function Get_PostBody: WideString; safecall;
    procedure Set_PostBody(const Val: WideString); safecall;
    function SendSms(const Sms: IMessage; MultipartFlag: Integer): WideString; safecall;
    function Get_WebAccount: WideString; safecall;
    procedure Set_WebAccount(const Val: WideString); safecall;
    function Get_WebPassword: WideString; safecall;
    procedure Set_WebPassword(const Val: WideString); safecall;
    function Get_ProxyServer: WideString; safecall;
    procedure Set_ProxyServer(const Val: WideString); safecall;
    function Get_ProxyAccount: WideString; safecall;
    procedure Set_ProxyAccount(const Val: WideString); safecall;
    function Get_ProxyPassword: WideString; safecall;
    procedure Set_ProxyPassword(const Val: WideString); safecall;
    function Get_RequestTimeout: Integer; safecall;
    procedure Set_RequestTimeout(Val: Integer); safecall;
    procedure SetHeader(const Header: WideString; const Value: WideString); safecall;
    function Get_LastResponseCode: Integer; safecall;
    function UrlEncode(const In_: WideString): WideString; safecall;
    function Base64Encode(const In_: WideString): WideString; safecall;
    function Base64EncodeFile(const FileName: WideString): WideString; safecall;
    function HexEncode(const In_: WideString): WideString; safecall;
    property Version: WideString read Get_Version;
    property Build: WideString read Get_Build;
    property Module: WideString read Get_Module;
    property LicenseStatus: WideString read Get_LicenseStatus;
    property LicenseKey: WideString read Get_LicenseKey write Set_LicenseKey;
    property LogFile: WideString read Get_LogFile write Set_LogFile;
    property ActivityFile: WideString read Get_ActivityFile write Set_ActivityFile;
    property LastError: Integer read Get_LastError;
    property Url: WideString read Get_Url write Set_Url;
    property PostBody: WideString read Get_PostBody write Set_PostBody;
    property WebAccount: WideString read Get_WebAccount write Set_WebAccount;
    property WebPassword: WideString read Get_WebPassword write Set_WebPassword;
    property ProxyServer: WideString read Get_ProxyServer write Set_ProxyServer;
    property ProxyAccount: WideString read Get_ProxyAccount write Set_ProxyAccount;
    property ProxyPassword: WideString read Get_ProxyPassword write Set_ProxyPassword;
    property RequestTimeout: Integer read Get_RequestTimeout write Set_RequestTimeout;
    property LastResponseCode: Integer read Get_LastResponseCode;
  end;

// *********************************************************************//
// DispIntf:  IHttpDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {1825A7C6-DBAB-4B99-AAA1-FDEDF36A4982}
// *********************************************************************//
  IHttpDisp = dispinterface
    ['{1825A7C6-DBAB-4B99-AAA1-FDEDF36A4982}']
    property Version: WideString readonly dispid 1;
    property Build: WideString readonly dispid 2;
    property Module: WideString readonly dispid 3;
    property LicenseStatus: WideString readonly dispid 100;
    property LicenseKey: WideString dispid 101;
    procedure SaveLicenseKey; dispid 102;
    property LogFile: WideString dispid 201;
    property ActivityFile: WideString dispid 202;
    procedure Sleep(Ms: Integer); dispid 300;
    procedure Clear; dispid 301;
    property LastError: Integer readonly dispid 400;
    function GetErrorDescription(ErrorCode: Integer): WideString; dispid 401;
    function Get(const strUrl: WideString): WideString; dispid 501;
    function Post(const strUrl: WideString; const strPostBody: WideString): WideString; dispid 502;
    property Url: WideString dispid 601;
    property PostBody: WideString dispid 602;
    function SendSms(const Sms: IMessage; MultipartFlag: Integer): WideString; dispid 650;
    property WebAccount: WideString dispid 701;
    property WebPassword: WideString dispid 702;
    property ProxyServer: WideString dispid 703;
    property ProxyAccount: WideString dispid 704;
    property ProxyPassword: WideString dispid 705;
    property RequestTimeout: Integer dispid 707;
    procedure SetHeader(const Header: WideString; const Value: WideString); dispid 708;
    property LastResponseCode: Integer readonly dispid 750;
    function UrlEncode(const In_: WideString): WideString; dispid 801;
    function Base64Encode(const In_: WideString): WideString; dispid 802;
    function Base64EncodeFile(const FileName: WideString): WideString; dispid 803;
    function HexEncode(const In_: WideString): WideString; dispid 804;
  end;

// *********************************************************************//
// Interface: ISnpp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {C4AAB6D2-F834-4FE6-A4AE-B1D5794BC3B4}
// *********************************************************************//
  ISnpp = interface(IDispatch)
    ['{C4AAB6D2-F834-4FE6-A4AE-B1D5794BC3B4}']
    function Get_Version: WideString; safecall;
    function Get_Build: WideString; safecall;
    function Get_Module: WideString; safecall;
    function Get_LicenseStatus: WideString; safecall;
    function Get_LicenseKey: WideString; safecall;
    procedure Set_LicenseKey(const LicenseKey: WideString); safecall;
    procedure SaveLicenseKey; safecall;
    function Get_LogFile: WideString; safecall;
    procedure Set_LogFile(const LogFile: WideString); safecall;
    procedure Clear; safecall;
    function Get_LastError: Integer; safecall;
    function GetErrorDescription(ErrorCode: Integer): WideString; safecall;
    function Get_Server: WideString; safecall;
    procedure Set_Server(const Val: WideString); safecall;
    function Get_ServerPort: Integer; safecall;
    procedure Set_ServerPort(Val: Integer); safecall;
    function Get_ServerTimeout: Integer; safecall;
    procedure Set_ServerTimeout(Val: Integer); safecall;
    function Get_ProviderPassword: WideString; safecall;
    procedure Set_ProviderPassword(const Val: WideString); safecall;
    function Get_ProviderUsername: WideString; safecall;
    procedure Set_ProviderUsername(const Val: WideString); safecall;
    function Get_ProviderResponse: WideString; safecall;
    procedure Send(const ToAddress: WideString; const Message: WideString); safecall;
    property Version: WideString read Get_Version;
    property Build: WideString read Get_Build;
    property Module: WideString read Get_Module;
    property LicenseStatus: WideString read Get_LicenseStatus;
    property LicenseKey: WideString read Get_LicenseKey write Set_LicenseKey;
    property LogFile: WideString read Get_LogFile write Set_LogFile;
    property LastError: Integer read Get_LastError;
    property Server: WideString read Get_Server write Set_Server;
    property ServerPort: Integer read Get_ServerPort write Set_ServerPort;
    property ServerTimeout: Integer read Get_ServerTimeout write Set_ServerTimeout;
    property ProviderPassword: WideString read Get_ProviderPassword write Set_ProviderPassword;
    property ProviderUsername: WideString read Get_ProviderUsername write Set_ProviderUsername;
    property ProviderResponse: WideString read Get_ProviderResponse;
  end;

// *********************************************************************//
// DispIntf:  ISnppDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {C4AAB6D2-F834-4FE6-A4AE-B1D5794BC3B4}
// *********************************************************************//
  ISnppDisp = dispinterface
    ['{C4AAB6D2-F834-4FE6-A4AE-B1D5794BC3B4}']
    property Version: WideString readonly dispid 1;
    property Build: WideString readonly dispid 2;
    property Module: WideString readonly dispid 3;
    property LicenseStatus: WideString readonly dispid 100;
    property LicenseKey: WideString dispid 101;
    procedure SaveLicenseKey; dispid 102;
    property LogFile: WideString dispid 201;
    procedure Clear; dispid 301;
    property LastError: Integer readonly dispid 400;
    function GetErrorDescription(ErrorCode: Integer): WideString; dispid 401;
    property Server: WideString dispid 501;
    property ServerPort: Integer dispid 502;
    property ServerTimeout: Integer dispid 503;
    property ProviderPassword: WideString dispid 504;
    property ProviderUsername: WideString dispid 505;
    property ProviderResponse: WideString readonly dispid 510;
    procedure Send(const ToAddress: WideString; const Message: WideString); dispid 601;
  end;

// *********************************************************************//
// Interface: ISmpp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {9E5FC25E-613A-4951-9C8F-37923C7AE7AE}
// *********************************************************************//
  ISmpp = interface(IDispatch)
    ['{9E5FC25E-613A-4951-9C8F-37923C7AE7AE}']
    function Get_Version: WideString; safecall;
    function Get_Build: WideString; safecall;
    function Get_Module: WideString; safecall;
    function Get_LicenseStatus: WideString; safecall;
    function Get_LicenseKey: WideString; safecall;
    procedure Set_LicenseKey(const LicenseKey: WideString); safecall;
    procedure SaveLicenseKey; safecall;
    function Get_LogFile: WideString; safecall;
    procedure Set_LogFile(const LogFile: WideString); safecall;
    function Get_ActivityFile: WideString; safecall;
    procedure Set_ActivityFile(const ActivityFile: WideString); safecall;
    function Get_LogPduDetails: WordBool; safecall;
    procedure Set_LogPduDetails(LogDetails: WordBool); safecall;
    procedure Sleep(Ms: Integer); safecall;
    procedure Clear; safecall;
    function Get_LastError: Integer; safecall;
    function GetErrorDescription(ErrorCode: Integer): WideString; safecall;
    procedure Connect(const Host: WideString; Port: Integer; Timeout: Integer); safecall;
    procedure Bind(Type_: Integer; const SystemId: WideString; const SystemPassword: WideString; 
                   const SystemType: WideString; Version: Integer; TON: Integer; NPI: Integer; 
                   const AddressRange: WideString; Timeout: Integer); safecall;
    procedure Unbind; safecall;
    procedure Disconnect; safecall;
    procedure AddBindTlv(const Tlv: ITlv); safecall;
    function SubmitSms(const Sms: IMessage; MultipartFlag: Integer): Integer; safecall;
    function WaitForSmsUpdate(TimeoutMs: Integer): WordBool; safecall;
    function FetchSmsUpdate: IMessage; safecall;
    procedure QuerySms(const Sms: IMessage); safecall;
    function ReceiveMessage: IMessage; safecall;
    function CountSmsReceived: Integer; safecall;
    function CountSmsSubmitSpace: Integer; safecall;
    function CountSmsQuerySpace: Integer; safecall;
    function Get_IsConnected: WordBool; safecall;
    function Get_IsBound: WordBool; safecall;
    function Get_MaxOutPendingPdus: Integer; safecall;
    procedure Set_MaxOutPendingPdus(Val: Integer); safecall;
    function Get_MaxSmsSubmissions: Integer; safecall;
    procedure Set_MaxSmsSubmissions(Val: Integer); safecall;
    function Get_MaxSmsQueries: Integer; safecall;
    procedure Set_MaxSmsQueries(Val: Integer); safecall;
    function Get_PduTimeout: Integer; safecall;
    procedure Set_PduTimeout(Val: Integer); safecall;
    function Get_EnquireInterval: Integer; safecall;
    procedure Set_EnquireInterval(Val: Integer); safecall;
    function Get_MultipartTimeout: Integer; safecall;
    procedure Set_MultipartTimeout(Val: Integer); safecall;
    function Get_UseGsmEncoding: Integer; safecall;
    procedure Set_UseGsmEncoding(Val: Integer); safecall;
    function Get_AssembleMultipart: WordBool; safecall;
    procedure Set_AssembleMultipart(Val: WordBool); safecall;
    function Get_MultipartMode: Integer; safecall;
    procedure Set_MultipartMode(Val: Integer); safecall;
    function Get_ExtractApplicationPort: WordBool; safecall;
    procedure Set_ExtractApplicationPort(Val: WordBool); safecall;
    function Get_MaxSmsReceived: Integer; safecall;
    procedure Set_MaxSmsReceived(Val: Integer); safecall;
    function Get_SubmitMode: Integer; safecall;
    procedure Set_SubmitMode(Val: Integer); safecall;
    function Get_SmsSentPerSecond: Integer; safecall;
    function Get_SmsReceivedPerSecond: Integer; safecall;
    property Version: WideString read Get_Version;
    property Build: WideString read Get_Build;
    property Module: WideString read Get_Module;
    property LicenseStatus: WideString read Get_LicenseStatus;
    property LicenseKey: WideString read Get_LicenseKey write Set_LicenseKey;
    property LogFile: WideString read Get_LogFile write Set_LogFile;
    property ActivityFile: WideString read Get_ActivityFile write Set_ActivityFile;
    property LogPduDetails: WordBool read Get_LogPduDetails write Set_LogPduDetails;
    property LastError: Integer read Get_LastError;
    property IsConnected: WordBool read Get_IsConnected;
    property IsBound: WordBool read Get_IsBound;
    property MaxOutPendingPdus: Integer read Get_MaxOutPendingPdus write Set_MaxOutPendingPdus;
    property MaxSmsSubmissions: Integer read Get_MaxSmsSubmissions write Set_MaxSmsSubmissions;
    property MaxSmsQueries: Integer read Get_MaxSmsQueries write Set_MaxSmsQueries;
    property PduTimeout: Integer read Get_PduTimeout write Set_PduTimeout;
    property EnquireInterval: Integer read Get_EnquireInterval write Set_EnquireInterval;
    property MultipartTimeout: Integer read Get_MultipartTimeout write Set_MultipartTimeout;
    property UseGsmEncoding: Integer read Get_UseGsmEncoding write Set_UseGsmEncoding;
    property AssembleMultipart: WordBool read Get_AssembleMultipart write Set_AssembleMultipart;
    property MultipartMode: Integer read Get_MultipartMode write Set_MultipartMode;
    property ExtractApplicationPort: WordBool read Get_ExtractApplicationPort write Set_ExtractApplicationPort;
    property MaxSmsReceived: Integer read Get_MaxSmsReceived write Set_MaxSmsReceived;
    property SubmitMode: Integer read Get_SubmitMode write Set_SubmitMode;
    property SmsSentPerSecond: Integer read Get_SmsSentPerSecond;
    property SmsReceivedPerSecond: Integer read Get_SmsReceivedPerSecond;
  end;

// *********************************************************************//
// DispIntf:  ISmppDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {9E5FC25E-613A-4951-9C8F-37923C7AE7AE}
// *********************************************************************//
  ISmppDisp = dispinterface
    ['{9E5FC25E-613A-4951-9C8F-37923C7AE7AE}']
    property Version: WideString readonly dispid 1;
    property Build: WideString readonly dispid 2;
    property Module: WideString readonly dispid 3;
    property LicenseStatus: WideString readonly dispid 100;
    property LicenseKey: WideString dispid 101;
    procedure SaveLicenseKey; dispid 102;
    property LogFile: WideString dispid 201;
    property ActivityFile: WideString dispid 202;
    property LogPduDetails: WordBool dispid 250;
    procedure Sleep(Ms: Integer); dispid 300;
    procedure Clear; dispid 301;
    property LastError: Integer readonly dispid 400;
    function GetErrorDescription(ErrorCode: Integer): WideString; dispid 401;
    procedure Connect(const Host: WideString; Port: Integer; Timeout: Integer); dispid 500;
    procedure Bind(Type_: Integer; const SystemId: WideString; const SystemPassword: WideString; 
                   const SystemType: WideString; Version: Integer; TON: Integer; NPI: Integer; 
                   const AddressRange: WideString; Timeout: Integer); dispid 501;
    procedure Unbind; dispid 502;
    procedure Disconnect; dispid 503;
    procedure AddBindTlv(const Tlv: ITlv); dispid 510;
    function SubmitSms(const Sms: IMessage; MultipartFlag: Integer): Integer; dispid 551;
    function WaitForSmsUpdate(TimeoutMs: Integer): WordBool; dispid 552;
    function FetchSmsUpdate: IMessage; dispid 553;
    procedure QuerySms(const Sms: IMessage); dispid 554;
    function ReceiveMessage: IMessage; dispid 555;
    function CountSmsReceived: Integer; dispid 571;
    function CountSmsSubmitSpace: Integer; dispid 572;
    function CountSmsQuerySpace: Integer; dispid 573;
    property IsConnected: WordBool readonly dispid 601;
    property IsBound: WordBool readonly dispid 602;
    property MaxOutPendingPdus: Integer dispid 701;
    property MaxSmsSubmissions: Integer dispid 702;
    property MaxSmsQueries: Integer dispid 703;
    property PduTimeout: Integer dispid 801;
    property EnquireInterval: Integer dispid 802;
    property MultipartTimeout: Integer dispid 803;
    property UseGsmEncoding: Integer dispid 901;
    property AssembleMultipart: WordBool dispid 902;
    property MultipartMode: Integer dispid 903;
    property ExtractApplicationPort: WordBool dispid 904;
    property MaxSmsReceived: Integer dispid 905;
    property SubmitMode: Integer dispid 906;
    property SmsSentPerSecond: Integer readonly dispid 1000;
    property SmsReceivedPerSecond: Integer readonly dispid 1001;
  end;

// *********************************************************************//
// Interface: ISmppServer
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {EBE10239-A8C6-46CA-BC48-0CFA6CFD835C}
// *********************************************************************//
  ISmppServer = interface(IDispatch)
    ['{EBE10239-A8C6-46CA-BC48-0CFA6CFD835C}']
    function Get_Version: WideString; safecall;
    function Get_Build: WideString; safecall;
    function Get_Module: WideString; safecall;
    function Get_LicenseStatus: WideString; safecall;
    function Get_LicenseKey: WideString; safecall;
    procedure Set_LicenseKey(const LicenseKey: WideString); safecall;
    procedure SaveLicenseKey; safecall;
    function Get_LogFile: WideString; safecall;
    procedure Set_LogFile(const LogFile: WideString); safecall;
    function Get_ActivityFile: WideString; safecall;
    procedure Set_ActivityFile(const ActivityFile: WideString); safecall;
    procedure Sleep(Ms: Integer); safecall;
    procedure Clear; safecall;
    function Get_LastError: Integer; safecall;
    function GetErrorDescription(ErrorCode: Integer): WideString; safecall;
    procedure Start(Port: Integer; IpVersion: Integer); safecall;
    procedure Stop; safecall;
    function GetFirstSession: ISmppSession; safecall;
    function GetNextSession: ISmppSession; safecall;
    function GetSession(Id: Integer): ISmppSession; safecall;
    function Get_IsStarted: WordBool; safecall;
    function Get_LastReference: Integer; safecall;
    procedure Set_LastReference(Val: Integer); safecall;
    property Version: WideString read Get_Version;
    property Build: WideString read Get_Build;
    property Module: WideString read Get_Module;
    property LicenseStatus: WideString read Get_LicenseStatus;
    property LicenseKey: WideString read Get_LicenseKey write Set_LicenseKey;
    property LogFile: WideString read Get_LogFile write Set_LogFile;
    property ActivityFile: WideString read Get_ActivityFile write Set_ActivityFile;
    property LastError: Integer read Get_LastError;
    property IsStarted: WordBool read Get_IsStarted;
    property LastReference: Integer read Get_LastReference write Set_LastReference;
  end;

// *********************************************************************//
// DispIntf:  ISmppServerDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {EBE10239-A8C6-46CA-BC48-0CFA6CFD835C}
// *********************************************************************//
  ISmppServerDisp = dispinterface
    ['{EBE10239-A8C6-46CA-BC48-0CFA6CFD835C}']
    property Version: WideString readonly dispid 1;
    property Build: WideString readonly dispid 2;
    property Module: WideString readonly dispid 3;
    property LicenseStatus: WideString readonly dispid 100;
    property LicenseKey: WideString dispid 101;
    procedure SaveLicenseKey; dispid 102;
    property LogFile: WideString dispid 201;
    property ActivityFile: WideString dispid 202;
    procedure Sleep(Ms: Integer); dispid 300;
    procedure Clear; dispid 301;
    property LastError: Integer readonly dispid 400;
    function GetErrorDescription(ErrorCode: Integer): WideString; dispid 401;
    procedure Start(Port: Integer; IpVersion: Integer); dispid 501;
    procedure Stop; dispid 502;
    function GetFirstSession: ISmppSession; dispid 601;
    function GetNextSession: ISmppSession; dispid 602;
    function GetSession(Id: Integer): ISmppSession; dispid 603;
    property IsStarted: WordBool readonly dispid 701;
    property LastReference: Integer dispid 801;
  end;

// *********************************************************************//
// Interface: ISmppSession
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {3E1FF0C2-4F98-4FA2-954B-BF5730FB6CF9}
// *********************************************************************//
  ISmppSession = interface(IDispatch)
    ['{3E1FF0C2-4F98-4FA2-954B-BF5730FB6CF9}']
    function Get_LogFile: WideString; safecall;
    procedure Set_LogFile(const LogFile: WideString); safecall;
    function Get_ActivityFile: WideString; safecall;
    procedure Set_ActivityFile(const ActivityFile: WideString); safecall;
    function Get_LogPduDetails: WordBool; safecall;
    procedure Set_LogPduDetails(LogDetails: WordBool); safecall;
    function Get_LastError: Integer; safecall;
    function GetErrorDescription(ErrorCode: Integer): WideString; safecall;
    function Get_Ip: WideString; safecall;
    function Get_Port: Integer; safecall;
    function Get_Version: Integer; safecall;
    function Get_SystemId: WideString; safecall;
    function Get_Password: WideString; safecall;
    function Get_SystemType: WideString; safecall;
    function Get_AddressRange: WideString; safecall;
    function Get_AddressRangeNpi: Integer; safecall;
    function Get_AddressRangeTon: Integer; safecall;
    function Get_ConnectionState: Integer; safecall;
    function Get_RequestedBind: Integer; safecall;
    function Get_Id: Integer; safecall;
    procedure RespondToBind(Status: Integer); safecall;
    procedure RespondToSubmitSms(const Sms: IMessage); safecall;
    function ReceiveSubmitSms: IMessage; safecall;
    procedure RespondToQuerySms(const Sms: IMessage); safecall;
    function ReceiveQuerySms: IMessage; safecall;
    procedure DeliverSms(const Sms: IMessage); safecall;
    procedure DeliverReport(const Sms: IMessage); safecall;
    function ReceiveDeliverResponse: IMessage; safecall;
    procedure Disconnect; safecall;
    function Get_MaxSmsDeliveries: Integer; safecall;
    procedure Set_MaxSmsDeliveries(Val: Integer); safecall;
    function Get_MaxSmsSubmission: Integer; safecall;
    procedure Set_MaxSmsSubmission(Val: Integer); safecall;
    function Get_MaxSmsQueries: Integer; safecall;
    procedure Set_MaxSmsQueries(Val: Integer); safecall;
    function CountSmsSubmissions: Integer; safecall;
    function CountSmsQueries: Integer; safecall;
    function CountSmsDeliverySpace: Integer; safecall;
    function Get_SmsSentPerSecond: Integer; safecall;
    function Get_SmsReceivedPerSecond: Integer; safecall;
    property LogFile: WideString read Get_LogFile write Set_LogFile;
    property ActivityFile: WideString read Get_ActivityFile write Set_ActivityFile;
    property LogPduDetails: WordBool read Get_LogPduDetails write Set_LogPduDetails;
    property LastError: Integer read Get_LastError;
    property Ip: WideString read Get_Ip;
    property Port: Integer read Get_Port;
    property Version: Integer read Get_Version;
    property SystemId: WideString read Get_SystemId;
    property Password: WideString read Get_Password;
    property SystemType: WideString read Get_SystemType;
    property AddressRange: WideString read Get_AddressRange;
    property AddressRangeNpi: Integer read Get_AddressRangeNpi;
    property AddressRangeTon: Integer read Get_AddressRangeTon;
    property ConnectionState: Integer read Get_ConnectionState;
    property RequestedBind: Integer read Get_RequestedBind;
    property Id: Integer read Get_Id;
    property MaxSmsDeliveries: Integer read Get_MaxSmsDeliveries write Set_MaxSmsDeliveries;
    property MaxSmsSubmission: Integer read Get_MaxSmsSubmission write Set_MaxSmsSubmission;
    property MaxSmsQueries: Integer read Get_MaxSmsQueries write Set_MaxSmsQueries;
    property SmsSentPerSecond: Integer read Get_SmsSentPerSecond;
    property SmsReceivedPerSecond: Integer read Get_SmsReceivedPerSecond;
  end;

// *********************************************************************//
// DispIntf:  ISmppSessionDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {3E1FF0C2-4F98-4FA2-954B-BF5730FB6CF9}
// *********************************************************************//
  ISmppSessionDisp = dispinterface
    ['{3E1FF0C2-4F98-4FA2-954B-BF5730FB6CF9}']
    property LogFile: WideString dispid 201;
    property ActivityFile: WideString dispid 202;
    property LogPduDetails: WordBool dispid 250;
    property LastError: Integer readonly dispid 400;
    function GetErrorDescription(ErrorCode: Integer): WideString; dispid 401;
    property Ip: WideString readonly dispid 501;
    property Port: Integer readonly dispid 502;
    property Version: Integer readonly dispid 503;
    property SystemId: WideString readonly dispid 504;
    property Password: WideString readonly dispid 505;
    property SystemType: WideString readonly dispid 506;
    property AddressRange: WideString readonly dispid 507;
    property AddressRangeNpi: Integer readonly dispid 508;
    property AddressRangeTon: Integer readonly dispid 509;
    property ConnectionState: Integer readonly dispid 510;
    property RequestedBind: Integer readonly dispid 511;
    property Id: Integer readonly dispid 550;
    procedure RespondToBind(Status: Integer); dispid 601;
    procedure RespondToSubmitSms(const Sms: IMessage); dispid 602;
    function ReceiveSubmitSms: IMessage; dispid 603;
    procedure RespondToQuerySms(const Sms: IMessage); dispid 604;
    function ReceiveQuerySms: IMessage; dispid 605;
    procedure DeliverSms(const Sms: IMessage); dispid 606;
    procedure DeliverReport(const Sms: IMessage); dispid 607;
    function ReceiveDeliverResponse: IMessage; dispid 608;
    procedure Disconnect; dispid 650;
    property MaxSmsDeliveries: Integer dispid 701;
    property MaxSmsSubmission: Integer dispid 702;
    property MaxSmsQueries: Integer dispid 703;
    function CountSmsSubmissions: Integer; dispid 801;
    function CountSmsQueries: Integer; dispid 802;
    function CountSmsDeliverySpace: Integer; dispid 803;
    property SmsSentPerSecond: Integer readonly dispid 901;
    property SmsReceivedPerSecond: Integer readonly dispid 902;
  end;

// *********************************************************************//
// Interface: ITemplateWapPush
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {54DD400B-F0D6-42AF-964B-9F38CD57D8CC}
// *********************************************************************//
  ITemplateWapPush = interface(IDispatch)
    ['{54DD400B-F0D6-42AF-964B-9F38CD57D8CC}']
    function Get_Url: WideString; safecall;
    procedure Set_Url(const pVal: WideString); safecall;
    function Get_Description: WideString; safecall;
    procedure Set_Description(const pVal: WideString); safecall;
    function Get_SignalAction: Integer; safecall;
    procedure Set_SignalAction(pVal: Integer); safecall;
    function Get_Data: WideString; safecall;
    function Get_LastError: Integer; safecall;
    procedure Encode; safecall;
    function GetErrorDescription(lCode: Integer): WideString; safecall;
    procedure Clear; safecall;
    property Url: WideString read Get_Url write Set_Url;
    property Description: WideString read Get_Description write Set_Description;
    property SignalAction: Integer read Get_SignalAction write Set_SignalAction;
    property Data: WideString read Get_Data;
    property LastError: Integer read Get_LastError;
  end;

// *********************************************************************//
// DispIntf:  ITemplateWapPushDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {54DD400B-F0D6-42AF-964B-9F38CD57D8CC}
// *********************************************************************//
  ITemplateWapPushDisp = dispinterface
    ['{54DD400B-F0D6-42AF-964B-9F38CD57D8CC}']
    property Url: WideString dispid 1;
    property Description: WideString dispid 2;
    property SignalAction: Integer dispid 3;
    property Data: WideString readonly dispid 4;
    property LastError: Integer readonly dispid 5;
    procedure Encode; dispid 6;
    function GetErrorDescription(lCode: Integer): WideString; dispid 7;
    procedure Clear; dispid 8;
  end;

// *********************************************************************//
// Interface: ITemplatevCard
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {FCB5DCC2-4DDD-465D-936F-40AEBDDD17C3}
// *********************************************************************//
  ITemplatevCard = interface(IDispatch)
    ['{FCB5DCC2-4DDD-465D-936F-40AEBDDD17C3}']
    procedure Clear; safecall;
    function GetErrorDescription(lError: Integer): WideString; safecall;
    function Get_Url: WideString; safecall;
    procedure Set_Url(const pVal: WideString); safecall;
    function Get_Title: WideString; safecall;
    procedure Set_Title(const pVal: WideString); safecall;
    function Get_EMail: WideString; safecall;
    procedure Set_EMail(const pVal: WideString); safecall;
    function Get_Fax: WideString; safecall;
    procedure Set_Fax(const pVal: WideString); safecall;
    function Get_Pager: WideString; safecall;
    procedure Set_Pager(const pVal: WideString); safecall;
    function Get_Mobile: WideString; safecall;
    procedure Set_Mobile(const pVal: WideString); safecall;
    function Get_PhoneHome: WideString; safecall;
    procedure Set_PhoneHome(const pVal: WideString); safecall;
    function Get_PhoneWork: WideString; safecall;
    procedure Set_PhoneWork(const pVal: WideString); safecall;
    function Get_Phone: WideString; safecall;
    procedure Set_Phone(const pVal: WideString); safecall;
    function Get_Name: WideString; safecall;
    procedure Set_Name(const pVal: WideString); safecall;
    procedure Encode; safecall;
    function Get_Data: WideString; safecall;
    function Get_LastError: Integer; safecall;
    property Url: WideString read Get_Url write Set_Url;
    property Title: WideString read Get_Title write Set_Title;
    property EMail: WideString read Get_EMail write Set_EMail;
    property Fax: WideString read Get_Fax write Set_Fax;
    property Pager: WideString read Get_Pager write Set_Pager;
    property Mobile: WideString read Get_Mobile write Set_Mobile;
    property PhoneHome: WideString read Get_PhoneHome write Set_PhoneHome;
    property PhoneWork: WideString read Get_PhoneWork write Set_PhoneWork;
    property Phone: WideString read Get_Phone write Set_Phone;
    property Name: WideString read Get_Name write Set_Name;
    property Data: WideString read Get_Data;
    property LastError: Integer read Get_LastError;
  end;

// *********************************************************************//
// DispIntf:  ITemplatevCardDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {FCB5DCC2-4DDD-465D-936F-40AEBDDD17C3}
// *********************************************************************//
  ITemplatevCardDisp = dispinterface
    ['{FCB5DCC2-4DDD-465D-936F-40AEBDDD17C3}']
    procedure Clear; dispid 1;
    function GetErrorDescription(lError: Integer): WideString; dispid 2;
    property Url: WideString dispid 3;
    property Title: WideString dispid 4;
    property EMail: WideString dispid 5;
    property Fax: WideString dispid 6;
    property Pager: WideString dispid 7;
    property Mobile: WideString dispid 8;
    property PhoneHome: WideString dispid 9;
    property PhoneWork: WideString dispid 10;
    property Phone: WideString dispid 11;
    property Name: WideString dispid 12;
    procedure Encode; dispid 13;
    property Data: WideString readonly dispid 14;
    property LastError: Integer readonly dispid 15;
  end;

// *********************************************************************//
// Interface: IDemoAccount
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {231F4503-C639-479F-B799-4C9FCE0BDF9C}
// *********************************************************************//
  IDemoAccount = interface(IDispatch)
    ['{231F4503-C639-479F-B799-4C9FCE0BDF9C}']
    function Get_SystemId: WideString; safecall;
    function Get_Password: WideString; safecall;
    property SystemId: WideString read Get_SystemId;
    property Password: WideString read Get_Password;
  end;

// *********************************************************************//
// DispIntf:  IDemoAccountDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {231F4503-C639-479F-B799-4C9FCE0BDF9C}
// *********************************************************************//
  IDemoAccountDisp = dispinterface
    ['{231F4503-C639-479F-B799-4C9FCE0BDF9C}']
    property SystemId: WideString readonly dispid 1;
    property Password: WideString readonly dispid 2;
  end;

// *********************************************************************//
// The Class CoConstants provides a Create and CreateRemote method to          
// create instances of the default interface IConstants exposed by              
// the CoClass Constants. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoConstants = class
    class function Create: IConstants;
    class function CreateRemote(const MachineName: string): IConstants;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TConstants
// Help String      : Constants Class
// Default Interface: IConstants
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TConstantsProperties= class;
{$ENDIF}
  TConstants = class(TOleServer)
  private
    FIntf: IConstants;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps: TConstantsProperties;
    function GetServerProperties: TConstantsProperties;
{$ENDIF}
    function GetDefaultInterface: IConstants;
  protected
    procedure InitServerData; override;
    function Get_SMPP_BIND_TRANSMITTER: Integer;
    function Get_SMPP_BIND_TRANSCEIVER: Integer;
    function Get_SMPP_BIND_RECEIVER: Integer;
    function Get_SMPP_VERSION_33: Integer;
    function Get_SMPP_VERSION_34: Integer;
    function Get_SMPP_VERSION_50: Integer;
    function Get_TON_UNKNOWN: Integer;
    function Get_TON_INTERNATIONAL: Integer;
    function Get_TON_NATIONAL: Integer;
    function Get_TON_NETWORK_SPECIFIC: Integer;
    function Get_TON_SUBSCRIBER_NUMBER: Integer;
    function Get_TON_ALPHANUMERIC: Integer;
    function Get_SMPP_TON_ABBREVIATED: Integer;
    function Get_NPI_UNKNOWN: Integer;
    function Get_NPI_ISDN: Integer;
    function Get_NPI_DATA: Integer;
    function Get_NPI_TELEX: Integer;
    function Get_NPI_NATIONAL: Integer;
    function Get_NPI_PRIVATE: Integer;
    function Get_NPI_ERMES: Integer;
    function Get_SMPP_NPI_INTERNET: Integer;
    function Get_NPI_LAND_MOBILE: Integer;
    function Get_MULTIPART_ACCEPT: Integer;
    function Get_MULTIPART_TRUNCATE: Integer;
    function Get_MULTIPART_REJECT: Integer;
    function Get_BODYFORMAT_TEXT: Integer;
    function Get_BODYFORMAT_HEX: Integer;
    function Get_SMPP_ESM_2ESME_DEFAULT: Integer;
    function Get_SMPP_ESM_2ESME_DELIVERY_RECEIPT: Integer;
    function Get_SMPP_ESM_2ESME_DELIVERY_ACK: Integer;
    function Get_SMPP_ESM_2ESME_MANUAL_ACK: Integer;
    function Get_SMPP_ESM_2ESME_CONVERSATION_ABORT: Integer;
    function Get_SMPP_ESM_2ESME_INTERMEDIATE_DELIVERY_NOTIFY: Integer;
    function Get_SMPP_ESM_2SMSC_MODE_DEFAULT: Integer;
    function Get_SMPP_ESM_2SMSC_MODE_STOREFORWARD: Integer;
    function Get_SMPP_ESM_2SMSC_MODE_DATAGRAM: Integer;
    function Get_SMPP_ESM_2SMSC_MODE_FORWARD: Integer;
    function Get_SMPP_ESM_2SMSC_TYPE_DEFAULT: Integer;
    function Get_SMPP_ESM_2SMSC_TYPE_DELIVERY_ACK: Integer;
    function Get_SMPP_ESM_2SMSC_TYPE_MANUAL_ACK: Integer;
    function Get_SMPP_ESM_2SMSC_FEAT_NOTHING: Integer;
    function Get_SMPP_ESM_2SMSC_FEAT_UDHI: Integer;
    function Get_SMPP_ESM_2SMSC_FEAT_SRP: Integer;
    function Get_SMPP_USEGSMENCODING_DISABLED: Integer;
    function Get_SMPP_USEGSMENCODING_INANDOUT: Integer;
    function Get_SMPP_USEGSMENCODING_INCOMING: Integer;
    function Get_SMPP_USEGSMENCODING_OUTGOING: Integer;
    function Get_SMPP_DATACODING_ASCII: Integer;
    function Get_SMPP_DATACODING_OCTET_UNSPEC: Integer;
    function Get_SMPP_DATACODING_LATIN: Integer;
    function Get_SMPP_DATACODING_JIS_KANJI: Integer;
    function Get_SMPP_DATACODING_CYRILLIC: Integer;
    function Get_SMPP_DATACODING_LATIN_HEBREW: Integer;
    function Get_SMPP_DATACODING_PICTOGRAM: Integer;
    function Get_SMPP_DATACODING_ISO_2022_JP: Integer;
    function Get_SMPP_DATACODING_EXTENDED_KANJI_JIS: Integer;
    function Get_SMPP_DATACODING_KS_C_5601: Integer;
    function Get_SMPP_PRIORITYFLAG_BULK: Integer;
    function Get_SMPP_PRIORITYFLAG_NORMAL: Integer;
    function Get_SMPP_PRIORITYFLAG_URGENT: Integer;
    function Get_SMPP_PRIORITYFLAG_VERY_URGENT: Integer;
    function Get_SMPP_MESSAGESTATE_AX_WAITRESP: Integer;
    function Get_SMPP_MESSAGESTATE_ENROUTE: Integer;
    function Get_SMPP_MESSAGESTATE_DELIVERED: Integer;
    function Get_SMPP_MESSAGESTATE_EXPIRED: Integer;
    function Get_SMPP_MESSAGESTATE_DELETED: Integer;
    function Get_SMPP_MESSAGESTATE_UNDELIVERABLE: Integer;
    function Get_SMPP_MESSAGESTATE_ACCEPTED: Integer;
    function Get_SMPP_MESSAGESTATE_UNKNOWN: Integer;
    function Get_SMPP_MESSAGESTATE_REJECTED: Integer;
    function Get_SMPP_MESSAGESTATE_AX_RESPERROR: Integer;
    function Get_SMPP_MESSAGESTATE_AX_NOCREDITS: Integer;
    function Get_SMPP_MESSAGESTATE_AX_RESPTO: Integer;
    function Get_SMPP_MESSAGESTATE_AX_RESPONDED: Integer;
    function Get_SMPP_SESSIONSTATE_CONNECTED: Integer;
    function Get_SMPP_SESSIONSTATE_DISCONNECTED: Integer;
    function Get_SMPP_SESSIONSTATE_BINDING: Integer;
    function Get_SMPP_SESSIONSTATE_BOUND_TX: Integer;
    function Get_SMPP_SESSIONSTATE_BOUND_RX: Integer;
    function Get_SMPP_SESSIONSTATE_BOUND_TRX: Integer;
    function Get_SMPP_TLV_DEST_ADDR_SUBUNIT: Integer;
    function Get_SMPP_TLV_DEST_NETWORK_TYPE: Integer;
    function Get_SMPP_TLV_DEST_BEARER_TYPE: Integer;
    function Get_SMPP_TLV_DEST_TELEMATICS_ID: Integer;
    function Get_SMPP_TLV_SOURCE_ADDR_SUBUNIT: Integer;
    function Get_SMPP_TLV_SOURCE_NETWORK_TYPE: Integer;
    function Get_SMPP_TLV_SOURCE_BEARER_TYPE: Integer;
    function Get_SMPP_TLV_SOURCE_TELEMATICS_ID: Integer;
    function Get_SMPP_TLV_QOS_TIME_TO_LIVE: Integer;
    function Get_SMPP_TLV_PAYLOAD_TYPE: Integer;
    function Get_SMPP_TLV_ADDITIONAL_STATUS_INFO_TEXT: Integer;
    function Get_SMPP_TLV_RECEIPTED_MESSAGE_ID: Integer;
    function Get_SMPP_TLV_MS_MSG_WAIT_FACILITIES: Integer;
    function Get_SMPP_TLV_PRIVACY_INDICATOR: Integer;
    function Get_SMPP_TLV_SOURCE_SUBADDRESS: Integer;
    function Get_SMPP_TLV_DEST_SUBADDRESS: Integer;
    function Get_SMPP_TLV_USER_MESSAGE_REFERENCE: Integer;
    function Get_SMPP_TLV_USER_RESPONSE_CODE: Integer;
    function Get_SMPP_TLV_SOURCE_PORT: Integer;
    function Get_SMPP_TLV_DESTINATION_PORT: Integer;
    function Get_SMPP_TLV_SAR_MSG_REF_NUM: Integer;
    function Get_SMPP_TLV_LANGUAGE_INDICATOR: Integer;
    function Get_SMPP_TLV_SAR_TOTAL_SEGMENTS: Integer;
    function Get_SMPP_TLV_SAR_SEGMENT_SEQNUM: Integer;
    function Get_SMPP_TLV_SC_INTERFACE_VERSION: Integer;
    function Get_SMPP_TLV_CALLBACK_NUM_PRES_IND: Integer;
    function Get_SMPP_TLV_CALLBACK_NUM_ATAG: Integer;
    function Get_SMPP_TLV_NUMBER_OF_MESSAGES: Integer;
    function Get_SMPP_TLV_CALLBACK_NUM: Integer;
    function Get_SMPP_TLV_DPF_RESULT: Integer;
    function Get_SMPP_TLV_SET_DPF: Integer;
    function Get_SMPP_TLV_MS_AVAILABILITY_STATUS: Integer;
    function Get_SMPP_TLV_NETWORK_ERROR_CODE: Integer;
    function Get_SMPP_TLV_MESSAGE_PAYLOAD: Integer;
    function Get_SMPP_TLV_DELIVERY_FAILURE_REASON: Integer;
    function Get_SMPP_TLV_MORE_MESSAGES_TO_SEND: Integer;
    function Get_SMPP_TLV_MESSAGE_STATE: Integer;
    function Get_SMPP_TLV_CONGESTION_STATE: Integer;
    function Get_SMPP_TLV_USSD_SERVICE_OP: Integer;
    function Get_SMPP_TLV_DISPLAY_TIME: Integer;
    function Get_SMPP_TLV_SMS_SIGNAL: Integer;
    function Get_SMPP_TLV_MS_VALIDITY: Integer;
    function Get_SMPP_TLV_ALERT_ON_MESSAGE_DELIVERY: Integer;
    function Get_SMPP_TLV_ITS_REPLY_TYPE: Integer;
    function Get_SMPP_TLV_ITS_SESSION_INFO: Integer;
    function Get_SMPP_MULTIPARTMODE_UDH: Integer;
    function Get_SMPP_MULTIPARTMODE_UDH16BIT: Integer;
    function Get_SMPP_MULTIPARTMODE_SARTLV: Integer;
    function Get_SMPP_MULTIPARTMODE_PAYLOADTLV: Integer;
    function Get_SMPP_SUBMITMODE_SUBMITSM: Integer;
    function Get_SMPP_SUBMITMODE_DATASM: Integer;
    function Get_SMPP_USEGSMENCODING_INCHARSET: Integer;
    function Get_SMPP_USEGSMENCODING_OUTCHARSET: Integer;
    function Get_SMPP_USEGSMENCODING_INOUTCHARS: Integer;
    function Get_SMPP_IPVERSION_4: Integer;
    function Get_SMPP_IPVERSION_6: Integer;
    function Get_SMPP_IPVERSION_BOTH: Integer;
    function Get_SMPP_ESME_ROK: Integer;
    function Get_SMPP_ESME_RINVMSGLEN: Integer;
    function Get_SMPP_ESME_RINVCMDLEN: Integer;
    function Get_SMPP_ESME_RINVCMDID: Integer;
    function Get_SMPP_ESME_RINVBNDSTS: Integer;
    function Get_SMPP_ESME_RALYBND: Integer;
    function Get_SMPP_ESME_RINVPRTFLG: Integer;
    function Get_SMPP_ESME_RINVREGDLVFLG: Integer;
    function Get_SMPP_ESME_RSYSERR: Integer;
    function Get_SMPP_ESME_RINVSRCADR: Integer;
    function Get_SMPP_ESME_RINVDSTADR: Integer;
    function Get_SMPP_ESME_RINVMSGID: Integer;
    function Get_SMPP_ESME_RBINDFAIL: Integer;
    function Get_SMPP_ESME_RINVPASWD: Integer;
    function Get_SMPP_ESME_RINVSYSID: Integer;
    function Get_SMPP_ESME_RCANCELFAIL: Integer;
    function Get_SMPP_ESME_RREPLACEFAIL: Integer;
    function Get_SMPP_ESME_RMSGQFUL: Integer;
    function Get_SMPP_ESME_RINVSERTYP: Integer;
    function Get_SMPP_ESME_RINVNUMDESTS: Integer;
    function Get_SMPP_ESME_RINVDLNAME: Integer;
    function Get_SMPP_ESME_RINVDESTFLAG: Integer;
    function Get_SMPP_ESME_RINVSUBREP: Integer;
    function Get_SMPP_ESME_RINVESMCLASS: Integer;
    function Get_SMPP_ESME_RCNTSUBDL: Integer;
    function Get_SMPP_ESME_RSUBMITFAIL: Integer;
    function Get_SMPP_ESME_RINVSRCTON: Integer;
    function Get_SMPP_ESME_RINVSRCNPI: Integer;
    function Get_SMPP_ESME_RINVDSTTON: Integer;
    function Get_SMPP_ESME_RINVDSTNPI: Integer;
    function Get_SMPP_ESME_RINVSYSTYP: Integer;
    function Get_SMPP_ESME_RINVREPFLAG: Integer;
    function Get_SMPP_ESME_RINVNUMMSGS: Integer;
    function Get_SMPP_ESME_RTHROTTLED: Integer;
    function Get_SMPP_ESME_RINVSCHED: Integer;
    function Get_SMPP_ESME_RINVEXPIRY: Integer;
    function Get_SMPP_ESME_RINVDFTMSGID: Integer;
    function Get_SMPP_ESME_RX_T_APPN: Integer;
    function Get_SMPP_ESME_RX_P_APPN: Integer;
    function Get_SMPP_ESME_RX_R_APPN: Integer;
    function Get_SMPP_ESME_RQUERYFAIL: Integer;
    function Get_SMPP_ESME_RINVOPTPARSTREAM: Integer;
    function Get_SMPP_ESME_ROPTPARNOTALLWD: Integer;
    function Get_SMPP_ESME_RINVPARLEN: Integer;
    function Get_SMPP_ESME_RMISSINGOPTPARAM: Integer;
    function Get_SMPP_ESME_RINVOPTPARAMVAL: Integer;
    function Get_SMPP_ESME_RDELIVERYFAILURE: Integer;
    function Get_SMPP_ESME_RUNKNOWNERR: Integer;
    function Get_GSM_STATUS_MESSAGE_DELIVERED_SUCCESSFULLY: Integer;
    function Get_GSM_STATUS_FORWARDED_STATUS_UNKNOWN: Integer;
    function Get_GSM_STATUS_REPLACED: Integer;
    function Get_GSM_STATUS_CONGESTION_STILL_TRYING: Integer;
    function Get_GSM_STATUS_RECIPIENT_BUSY_STILL_TRYING: Integer;
    function Get_GSM_STATUS_NO_RESPONSE_STILL_TRYING: Integer;
    function Get_GSM_STATUS_SERVICE_REJECTED_STILL_TRYING: Integer;
    function Get_GSM_STATUS_QOS_NOT_AVAILABLE_STILL_TRYING: Integer;
    function Get_GSM_STATUS_RECIPIENT_ERROR_STILL_TRYING: Integer;
    function Get_GSM_STATUS_RPC_ERROR: Integer;
    function Get_GSM_STATUS_INCOMPATIBLE_DESTINATION: Integer;
    function Get_GSM_STATUS_CONNECTION_REJECTED: Integer;
    function Get_GSM_STATUS_NOT_OBTAINABLE: Integer;
    function Get_GSM_STATUS_QOS_NOT_AVAILABLE: Integer;
    function Get_GSM_STATUS_NO_INTERNETWORKING_AVAILABLE: Integer;
    function Get_GSM_STATUS_MESSAGE_EXPIRED: Integer;
    function Get_GSM_STATUS_MESSAGE_DELETED_BY_SENDER: Integer;
    function Get_GSM_STATUS_MESSAGE_DELETED_BY_SMSC: Integer;
    function Get_GSM_STATUS_DOES_NOT_EXIST: Integer;
    function Get_GSM_STORAGETYPE_SIM: Integer;
    function Get_GSM_STORAGETYPE_MEMORY: Integer;
    function Get_GSM_STORAGETYPE_COMBINED: Integer;
    function Get_GSM_STORAGETYPE_STATUS: Integer;
    function Get_GSM_STORAGETYPE_ALL: Integer;
    function Get_GSM_FO_REPLYPATH_EXISTS: Integer;
    function Get_GSM_FO_UDHI: Integer;
    function Get_GSM_FO_STATUS_REPORT: Integer;
    function Get_GSM_FO_VALIDITY_NONE: Integer;
    function Get_GSM_FO_VALIDITY_RELATIVE: Integer;
    function Get_GSM_FO_VALIDITY_ENHANCED: Integer;
    function Get_GSM_FO_VALIDITY_ABSOLUTE: Integer;
    function Get_GSM_FO_REJECT_DUPLICATES: Integer;
    function Get_GSM_FO_SUBMIT_SM: Integer;
    function Get_GSM_FO_DELIVER_SM: Integer;
    function Get_GSM_FO_STATUS_SM: Integer;
    function Get_DATACODING_DEFAULT: Integer;
    function Get_DATACODING_8BIT_DATA: Integer;
    function Get_DATACODING_UNICODE: Integer;
    function Get_GSM_DATACODING_ME_SPECIFIC: Integer;
    function Get_GSM_DATACODING_SIM_SPECIFIC: Integer;
    function Get_GSM_DATACODING_TE_SPECIFIC: Integer;
    function Get_DATACODING_FLASH: Integer;
    function Get_GSM_BAUDRATE_110: Integer;
    function Get_GSM_BAUDRATE_300: Integer;
    function Get_GSM_BAUDRATE_600: Integer;
    function Get_GSM_BAUDRATE_1200: Integer;
    function Get_GSM_BAUDRATE_2400: Integer;
    function Get_GSM_BAUDRATE_4800: Integer;
    function Get_GSM_BAUDRATE_9600: Integer;
    function Get_GSM_BAUDRATE_14400: Integer;
    function Get_GSM_BAUDRATE_19200: Integer;
    function Get_GSM_BAUDRATE_38400: Integer;
    function Get_GSM_BAUDRATE_56000: Integer;
    function Get_GSM_BAUDRATE_57600: Integer;
    function Get_GSM_BAUDRATE_64000: Integer;
    function Get_GSM_BAUDRATE_115200: Integer;
    function Get_GSM_BAUDRATE_128000: Integer;
    function Get_GSM_BAUDRATE_230400: Integer;
    function Get_GSM_BAUDRATE_256000: Integer;
    function Get_GSM_BAUDRATE_460800: Integer;
    function Get_GSM_BAUDRATE_921600: Integer;
    function Get_GSM_BAUDRATE_DEFAULT: Integer;
    function Get_GSM_MESSAGESTATE_RECEIVED_UNREAD: Integer;
    function Get_GSM_MESSAGESTATE_RECEIVED_READ: Integer;
    function Get_GSM_MESSAGESTATE_STORED_UNSENT: Integer;
    function Get_GSM_MESSAGESTATE_STORED_SENT: Integer;
    function Get_GSM_MESSAGESTATE_ALL: Integer;
    function Get_GSM_MESSAGEFORMAT_PDU: Integer;
    function Get_GSM_MESSAGEFORMAT_TEXT: Integer;
    function Get_GSM_MESSAGEFORMAT_AUTO: Integer;
    function Get_HTTP_PLACEHOLDER_USERTAG: WideString;
    function Get_HTTP_PLACEHOLDER_TOADDRESS: WideString;
    function Get_HTTP_PLACEHOLDER_FROMADDRESS: WideString;
    function Get_HTTP_PLACEHOLDER_BODY: WideString;
    function Get_HTTP_PLACEHOLDER_BODYASHEX: WideString;
    function Get_HTTP_PLACEHOLDER_BODYASBASE64: WideString;
    function Get_HTTP_PLACEHOLDER_DELIVERYREPORT: WideString;
    function Get_HTTP_PLACEHOLDER_TOADDRESSTON: WideString;
    function Get_HTTP_PLACEHOLDER_TOADDRESSNPI: WideString;
    function Get_HTTP_PLACEHOLDER_FROMADDRESSTON: WideString;
    function Get_HTTP_PLACEHOLDER_FROMADDRESSNPI: WideString;
    function Get_HTTP_PLACEHOLDER_PROTOCOLID: WideString;
    function Get_HTTP_PLACEHOLDER_UDHI: WideString;
    function Get_HTTP_PLACEHOLDER_DATACODING: WideString;
    function Get_GSM_PREFIXSMSC_ENABLED: Integer;
    function Get_GSM_PREFIXSMSC_DISABLED: Integer;
    function Get_GSM_PREFIXSMSC_AUTO: Integer;
    function Get_WAPPUSH_SIGNAL_NONE: Integer;
    function Get_WAPPUSH_SIGNAL_LOW: Integer;
    function Get_WAPPUSH_SIGNAL_MEDIUM: Integer;
    function Get_WAPPUSH_SIGNAL_HIGH: Integer;
    function Get_WAPPUSH_SIGNAL_DELETE: Integer;
    function Get_DIALUP_PROVIDERTYPE_UCP: Integer;
    function Get_DIALUP_PROVIDERTYPE_TAP_DEFAULT: Integer;
    function Get_DIALUP_PROVIDERTYPE_TAP_NOLF: Integer;
    function Get_DIALUP_PROVIDERTYPE_TAP_NOEOT: Integer;
    function Get_DIALUP_DIALMODE_TONE: Integer;
    function Get_DIALUP_DIALMODE_PULSE: Integer;
    function Get_DIALUP_DEVICESETTINGS_DEFAULT: Integer;
    function Get_DIALUP_DEVICESETTINGS_8N1: Integer;
    function Get_DIALUP_DEVICESETTINGS_7E1: Integer;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IConstants);
    procedure Disconnect; override;
    function SmppBindToString(lVal: Integer): WideString;
    function SmppVersionToString(lVal: Integer): WideString;
    function TonToString(lVal: Integer): WideString;
    function NpiToString(lVal: Integer): WideString;
    function MultipartToString(lVal: Integer): WideString;
    function BodyformatToString(lVal: Integer): WideString;
    function SmppEsm2SmscToString(lVal: Integer): WideString;
    function SmppEsm2EsmeToString(lVal: Integer): WideString;
    function SmppUseGsmEncodingToString(lVal: Integer): WideString;
    function SmppDataCodingToString(lVal: Integer): WideString;
    function SmppPriorityFlagToString(lVal: Integer): WideString;
    function SmppMessageStateToString(lVal: Integer): WideString;
    function SmppSessionStateToString(lVal: Integer): WideString;
    function SmppTlvToString(lVal: Integer): WideString;
    function SmppMultipartModeToString(lVal: Integer): WideString;
    function SmppSubmitModeToString(lVal: Integer): WideString;
    function SmppEsmeToString(lVal: Integer): WideString;
    function GsmStatusToString(lVal: Integer): WideString;
    function GsmStorageTypeToString(lVal: Integer): WideString;
    function GsmFoToString(lVal: Integer): WideString;
    function GsmDataCodingToString(lVal: Integer): WideString;
    function GsmBaudrateToString(lVal: Integer): WideString;
    function GsmMessageStateToString(lVal: Integer): WideString;
    function GsmMessageFormatToString(lVal: Integer): WideString;
    function GsmPrefixSmscToString(lVal: Integer): WideString;
    function WapPushSignalToString(lVal: Integer): WideString;
    function DialupProviderTypeToString(lVal: Integer): WideString;
    function DialupDialModeToString(lVal: Integer): WideString;
    function DialupDeviceSettingsToString(lVal: Integer): WideString;
    property DefaultInterface: IConstants read GetDefaultInterface;
    property SMPP_BIND_TRANSMITTER: Integer read Get_SMPP_BIND_TRANSMITTER;
    property SMPP_BIND_TRANSCEIVER: Integer read Get_SMPP_BIND_TRANSCEIVER;
    property SMPP_BIND_RECEIVER: Integer read Get_SMPP_BIND_RECEIVER;
    property SMPP_VERSION_33: Integer read Get_SMPP_VERSION_33;
    property SMPP_VERSION_34: Integer read Get_SMPP_VERSION_34;
    property SMPP_VERSION_50: Integer read Get_SMPP_VERSION_50;
    property TON_UNKNOWN: Integer read Get_TON_UNKNOWN;
    property TON_INTERNATIONAL: Integer read Get_TON_INTERNATIONAL;
    property TON_NATIONAL: Integer read Get_TON_NATIONAL;
    property TON_NETWORK_SPECIFIC: Integer read Get_TON_NETWORK_SPECIFIC;
    property TON_SUBSCRIBER_NUMBER: Integer read Get_TON_SUBSCRIBER_NUMBER;
    property TON_ALPHANUMERIC: Integer read Get_TON_ALPHANUMERIC;
    property SMPP_TON_ABBREVIATED: Integer read Get_SMPP_TON_ABBREVIATED;
    property NPI_UNKNOWN: Integer read Get_NPI_UNKNOWN;
    property NPI_ISDN: Integer read Get_NPI_ISDN;
    property NPI_DATA: Integer read Get_NPI_DATA;
    property NPI_TELEX: Integer read Get_NPI_TELEX;
    property NPI_NATIONAL: Integer read Get_NPI_NATIONAL;
    property NPI_PRIVATE: Integer read Get_NPI_PRIVATE;
    property NPI_ERMES: Integer read Get_NPI_ERMES;
    property SMPP_NPI_INTERNET: Integer read Get_SMPP_NPI_INTERNET;
    property NPI_LAND_MOBILE: Integer read Get_NPI_LAND_MOBILE;
    property MULTIPART_ACCEPT: Integer read Get_MULTIPART_ACCEPT;
    property MULTIPART_TRUNCATE: Integer read Get_MULTIPART_TRUNCATE;
    property MULTIPART_REJECT: Integer read Get_MULTIPART_REJECT;
    property BODYFORMAT_TEXT: Integer read Get_BODYFORMAT_TEXT;
    property BODYFORMAT_HEX: Integer read Get_BODYFORMAT_HEX;
    property SMPP_ESM_2ESME_DEFAULT: Integer read Get_SMPP_ESM_2ESME_DEFAULT;
    property SMPP_ESM_2ESME_DELIVERY_RECEIPT: Integer read Get_SMPP_ESM_2ESME_DELIVERY_RECEIPT;
    property SMPP_ESM_2ESME_DELIVERY_ACK: Integer read Get_SMPP_ESM_2ESME_DELIVERY_ACK;
    property SMPP_ESM_2ESME_MANUAL_ACK: Integer read Get_SMPP_ESM_2ESME_MANUAL_ACK;
    property SMPP_ESM_2ESME_CONVERSATION_ABORT: Integer read Get_SMPP_ESM_2ESME_CONVERSATION_ABORT;
    property SMPP_ESM_2ESME_INTERMEDIATE_DELIVERY_NOTIFY: Integer read Get_SMPP_ESM_2ESME_INTERMEDIATE_DELIVERY_NOTIFY;
    property SMPP_ESM_2SMSC_MODE_DEFAULT: Integer read Get_SMPP_ESM_2SMSC_MODE_DEFAULT;
    property SMPP_ESM_2SMSC_MODE_STOREFORWARD: Integer read Get_SMPP_ESM_2SMSC_MODE_STOREFORWARD;
    property SMPP_ESM_2SMSC_MODE_DATAGRAM: Integer read Get_SMPP_ESM_2SMSC_MODE_DATAGRAM;
    property SMPP_ESM_2SMSC_MODE_FORWARD: Integer read Get_SMPP_ESM_2SMSC_MODE_FORWARD;
    property SMPP_ESM_2SMSC_TYPE_DEFAULT: Integer read Get_SMPP_ESM_2SMSC_TYPE_DEFAULT;
    property SMPP_ESM_2SMSC_TYPE_DELIVERY_ACK: Integer read Get_SMPP_ESM_2SMSC_TYPE_DELIVERY_ACK;
    property SMPP_ESM_2SMSC_TYPE_MANUAL_ACK: Integer read Get_SMPP_ESM_2SMSC_TYPE_MANUAL_ACK;
    property SMPP_ESM_2SMSC_FEAT_NOTHING: Integer read Get_SMPP_ESM_2SMSC_FEAT_NOTHING;
    property SMPP_ESM_2SMSC_FEAT_UDHI: Integer read Get_SMPP_ESM_2SMSC_FEAT_UDHI;
    property SMPP_ESM_2SMSC_FEAT_SRP: Integer read Get_SMPP_ESM_2SMSC_FEAT_SRP;
    property SMPP_USEGSMENCODING_DISABLED: Integer read Get_SMPP_USEGSMENCODING_DISABLED;
    property SMPP_USEGSMENCODING_INANDOUT: Integer read Get_SMPP_USEGSMENCODING_INANDOUT;
    property SMPP_USEGSMENCODING_INCOMING: Integer read Get_SMPP_USEGSMENCODING_INCOMING;
    property SMPP_USEGSMENCODING_OUTGOING: Integer read Get_SMPP_USEGSMENCODING_OUTGOING;
    property SMPP_DATACODING_ASCII: Integer read Get_SMPP_DATACODING_ASCII;
    property SMPP_DATACODING_OCTET_UNSPEC: Integer read Get_SMPP_DATACODING_OCTET_UNSPEC;
    property SMPP_DATACODING_LATIN: Integer read Get_SMPP_DATACODING_LATIN;
    property SMPP_DATACODING_JIS_KANJI: Integer read Get_SMPP_DATACODING_JIS_KANJI;
    property SMPP_DATACODING_CYRILLIC: Integer read Get_SMPP_DATACODING_CYRILLIC;
    property SMPP_DATACODING_LATIN_HEBREW: Integer read Get_SMPP_DATACODING_LATIN_HEBREW;
    property SMPP_DATACODING_PICTOGRAM: Integer read Get_SMPP_DATACODING_PICTOGRAM;
    property SMPP_DATACODING_ISO_2022_JP: Integer read Get_SMPP_DATACODING_ISO_2022_JP;
    property SMPP_DATACODING_EXTENDED_KANJI_JIS: Integer read Get_SMPP_DATACODING_EXTENDED_KANJI_JIS;
    property SMPP_DATACODING_KS_C_5601: Integer read Get_SMPP_DATACODING_KS_C_5601;
    property SMPP_PRIORITYFLAG_BULK: Integer read Get_SMPP_PRIORITYFLAG_BULK;
    property SMPP_PRIORITYFLAG_NORMAL: Integer read Get_SMPP_PRIORITYFLAG_NORMAL;
    property SMPP_PRIORITYFLAG_URGENT: Integer read Get_SMPP_PRIORITYFLAG_URGENT;
    property SMPP_PRIORITYFLAG_VERY_URGENT: Integer read Get_SMPP_PRIORITYFLAG_VERY_URGENT;
    property SMPP_MESSAGESTATE_AX_WAITRESP: Integer read Get_SMPP_MESSAGESTATE_AX_WAITRESP;
    property SMPP_MESSAGESTATE_ENROUTE: Integer read Get_SMPP_MESSAGESTATE_ENROUTE;
    property SMPP_MESSAGESTATE_DELIVERED: Integer read Get_SMPP_MESSAGESTATE_DELIVERED;
    property SMPP_MESSAGESTATE_EXPIRED: Integer read Get_SMPP_MESSAGESTATE_EXPIRED;
    property SMPP_MESSAGESTATE_DELETED: Integer read Get_SMPP_MESSAGESTATE_DELETED;
    property SMPP_MESSAGESTATE_UNDELIVERABLE: Integer read Get_SMPP_MESSAGESTATE_UNDELIVERABLE;
    property SMPP_MESSAGESTATE_ACCEPTED: Integer read Get_SMPP_MESSAGESTATE_ACCEPTED;
    property SMPP_MESSAGESTATE_UNKNOWN: Integer read Get_SMPP_MESSAGESTATE_UNKNOWN;
    property SMPP_MESSAGESTATE_REJECTED: Integer read Get_SMPP_MESSAGESTATE_REJECTED;
    property SMPP_MESSAGESTATE_AX_RESPERROR: Integer read Get_SMPP_MESSAGESTATE_AX_RESPERROR;
    property SMPP_MESSAGESTATE_AX_NOCREDITS: Integer read Get_SMPP_MESSAGESTATE_AX_NOCREDITS;
    property SMPP_MESSAGESTATE_AX_RESPTO: Integer read Get_SMPP_MESSAGESTATE_AX_RESPTO;
    property SMPP_MESSAGESTATE_AX_RESPONDED: Integer read Get_SMPP_MESSAGESTATE_AX_RESPONDED;
    property SMPP_SESSIONSTATE_CONNECTED: Integer read Get_SMPP_SESSIONSTATE_CONNECTED;
    property SMPP_SESSIONSTATE_DISCONNECTED: Integer read Get_SMPP_SESSIONSTATE_DISCONNECTED;
    property SMPP_SESSIONSTATE_BINDING: Integer read Get_SMPP_SESSIONSTATE_BINDING;
    property SMPP_SESSIONSTATE_BOUND_TX: Integer read Get_SMPP_SESSIONSTATE_BOUND_TX;
    property SMPP_SESSIONSTATE_BOUND_RX: Integer read Get_SMPP_SESSIONSTATE_BOUND_RX;
    property SMPP_SESSIONSTATE_BOUND_TRX: Integer read Get_SMPP_SESSIONSTATE_BOUND_TRX;
    property SMPP_TLV_DEST_ADDR_SUBUNIT: Integer read Get_SMPP_TLV_DEST_ADDR_SUBUNIT;
    property SMPP_TLV_DEST_NETWORK_TYPE: Integer read Get_SMPP_TLV_DEST_NETWORK_TYPE;
    property SMPP_TLV_DEST_BEARER_TYPE: Integer read Get_SMPP_TLV_DEST_BEARER_TYPE;
    property SMPP_TLV_DEST_TELEMATICS_ID: Integer read Get_SMPP_TLV_DEST_TELEMATICS_ID;
    property SMPP_TLV_SOURCE_ADDR_SUBUNIT: Integer read Get_SMPP_TLV_SOURCE_ADDR_SUBUNIT;
    property SMPP_TLV_SOURCE_NETWORK_TYPE: Integer read Get_SMPP_TLV_SOURCE_NETWORK_TYPE;
    property SMPP_TLV_SOURCE_BEARER_TYPE: Integer read Get_SMPP_TLV_SOURCE_BEARER_TYPE;
    property SMPP_TLV_SOURCE_TELEMATICS_ID: Integer read Get_SMPP_TLV_SOURCE_TELEMATICS_ID;
    property SMPP_TLV_QOS_TIME_TO_LIVE: Integer read Get_SMPP_TLV_QOS_TIME_TO_LIVE;
    property SMPP_TLV_PAYLOAD_TYPE: Integer read Get_SMPP_TLV_PAYLOAD_TYPE;
    property SMPP_TLV_ADDITIONAL_STATUS_INFO_TEXT: Integer read Get_SMPP_TLV_ADDITIONAL_STATUS_INFO_TEXT;
    property SMPP_TLV_RECEIPTED_MESSAGE_ID: Integer read Get_SMPP_TLV_RECEIPTED_MESSAGE_ID;
    property SMPP_TLV_MS_MSG_WAIT_FACILITIES: Integer read Get_SMPP_TLV_MS_MSG_WAIT_FACILITIES;
    property SMPP_TLV_PRIVACY_INDICATOR: Integer read Get_SMPP_TLV_PRIVACY_INDICATOR;
    property SMPP_TLV_SOURCE_SUBADDRESS: Integer read Get_SMPP_TLV_SOURCE_SUBADDRESS;
    property SMPP_TLV_DEST_SUBADDRESS: Integer read Get_SMPP_TLV_DEST_SUBADDRESS;
    property SMPP_TLV_USER_MESSAGE_REFERENCE: Integer read Get_SMPP_TLV_USER_MESSAGE_REFERENCE;
    property SMPP_TLV_USER_RESPONSE_CODE: Integer read Get_SMPP_TLV_USER_RESPONSE_CODE;
    property SMPP_TLV_SOURCE_PORT: Integer read Get_SMPP_TLV_SOURCE_PORT;
    property SMPP_TLV_DESTINATION_PORT: Integer read Get_SMPP_TLV_DESTINATION_PORT;
    property SMPP_TLV_SAR_MSG_REF_NUM: Integer read Get_SMPP_TLV_SAR_MSG_REF_NUM;
    property SMPP_TLV_LANGUAGE_INDICATOR: Integer read Get_SMPP_TLV_LANGUAGE_INDICATOR;
    property SMPP_TLV_SAR_TOTAL_SEGMENTS: Integer read Get_SMPP_TLV_SAR_TOTAL_SEGMENTS;
    property SMPP_TLV_SAR_SEGMENT_SEQNUM: Integer read Get_SMPP_TLV_SAR_SEGMENT_SEQNUM;
    property SMPP_TLV_SC_INTERFACE_VERSION: Integer read Get_SMPP_TLV_SC_INTERFACE_VERSION;
    property SMPP_TLV_CALLBACK_NUM_PRES_IND: Integer read Get_SMPP_TLV_CALLBACK_NUM_PRES_IND;
    property SMPP_TLV_CALLBACK_NUM_ATAG: Integer read Get_SMPP_TLV_CALLBACK_NUM_ATAG;
    property SMPP_TLV_NUMBER_OF_MESSAGES: Integer read Get_SMPP_TLV_NUMBER_OF_MESSAGES;
    property SMPP_TLV_CALLBACK_NUM: Integer read Get_SMPP_TLV_CALLBACK_NUM;
    property SMPP_TLV_DPF_RESULT: Integer read Get_SMPP_TLV_DPF_RESULT;
    property SMPP_TLV_SET_DPF: Integer read Get_SMPP_TLV_SET_DPF;
    property SMPP_TLV_MS_AVAILABILITY_STATUS: Integer read Get_SMPP_TLV_MS_AVAILABILITY_STATUS;
    property SMPP_TLV_NETWORK_ERROR_CODE: Integer read Get_SMPP_TLV_NETWORK_ERROR_CODE;
    property SMPP_TLV_MESSAGE_PAYLOAD: Integer read Get_SMPP_TLV_MESSAGE_PAYLOAD;
    property SMPP_TLV_DELIVERY_FAILURE_REASON: Integer read Get_SMPP_TLV_DELIVERY_FAILURE_REASON;
    property SMPP_TLV_MORE_MESSAGES_TO_SEND: Integer read Get_SMPP_TLV_MORE_MESSAGES_TO_SEND;
    property SMPP_TLV_MESSAGE_STATE: Integer read Get_SMPP_TLV_MESSAGE_STATE;
    property SMPP_TLV_CONGESTION_STATE: Integer read Get_SMPP_TLV_CONGESTION_STATE;
    property SMPP_TLV_USSD_SERVICE_OP: Integer read Get_SMPP_TLV_USSD_SERVICE_OP;
    property SMPP_TLV_DISPLAY_TIME: Integer read Get_SMPP_TLV_DISPLAY_TIME;
    property SMPP_TLV_SMS_SIGNAL: Integer read Get_SMPP_TLV_SMS_SIGNAL;
    property SMPP_TLV_MS_VALIDITY: Integer read Get_SMPP_TLV_MS_VALIDITY;
    property SMPP_TLV_ALERT_ON_MESSAGE_DELIVERY: Integer read Get_SMPP_TLV_ALERT_ON_MESSAGE_DELIVERY;
    property SMPP_TLV_ITS_REPLY_TYPE: Integer read Get_SMPP_TLV_ITS_REPLY_TYPE;
    property SMPP_TLV_ITS_SESSION_INFO: Integer read Get_SMPP_TLV_ITS_SESSION_INFO;
    property SMPP_MULTIPARTMODE_UDH: Integer read Get_SMPP_MULTIPARTMODE_UDH;
    property SMPP_MULTIPARTMODE_UDH16BIT: Integer read Get_SMPP_MULTIPARTMODE_UDH16BIT;
    property SMPP_MULTIPARTMODE_SARTLV: Integer read Get_SMPP_MULTIPARTMODE_SARTLV;
    property SMPP_MULTIPARTMODE_PAYLOADTLV: Integer read Get_SMPP_MULTIPARTMODE_PAYLOADTLV;
    property SMPP_SUBMITMODE_SUBMITSM: Integer read Get_SMPP_SUBMITMODE_SUBMITSM;
    property SMPP_SUBMITMODE_DATASM: Integer read Get_SMPP_SUBMITMODE_DATASM;
    property SMPP_USEGSMENCODING_INCHARSET: Integer read Get_SMPP_USEGSMENCODING_INCHARSET;
    property SMPP_USEGSMENCODING_OUTCHARSET: Integer read Get_SMPP_USEGSMENCODING_OUTCHARSET;
    property SMPP_USEGSMENCODING_INOUTCHARS: Integer read Get_SMPP_USEGSMENCODING_INOUTCHARS;
    property SMPP_IPVERSION_4: Integer read Get_SMPP_IPVERSION_4;
    property SMPP_IPVERSION_6: Integer read Get_SMPP_IPVERSION_6;
    property SMPP_IPVERSION_BOTH: Integer read Get_SMPP_IPVERSION_BOTH;
    property SMPP_ESME_ROK: Integer read Get_SMPP_ESME_ROK;
    property SMPP_ESME_RINVMSGLEN: Integer read Get_SMPP_ESME_RINVMSGLEN;
    property SMPP_ESME_RINVCMDLEN: Integer read Get_SMPP_ESME_RINVCMDLEN;
    property SMPP_ESME_RINVCMDID: Integer read Get_SMPP_ESME_RINVCMDID;
    property SMPP_ESME_RINVBNDSTS: Integer read Get_SMPP_ESME_RINVBNDSTS;
    property SMPP_ESME_RALYBND: Integer read Get_SMPP_ESME_RALYBND;
    property SMPP_ESME_RINVPRTFLG: Integer read Get_SMPP_ESME_RINVPRTFLG;
    property SMPP_ESME_RINVREGDLVFLG: Integer read Get_SMPP_ESME_RINVREGDLVFLG;
    property SMPP_ESME_RSYSERR: Integer read Get_SMPP_ESME_RSYSERR;
    property SMPP_ESME_RINVSRCADR: Integer read Get_SMPP_ESME_RINVSRCADR;
    property SMPP_ESME_RINVDSTADR: Integer read Get_SMPP_ESME_RINVDSTADR;
    property SMPP_ESME_RINVMSGID: Integer read Get_SMPP_ESME_RINVMSGID;
    property SMPP_ESME_RBINDFAIL: Integer read Get_SMPP_ESME_RBINDFAIL;
    property SMPP_ESME_RINVPASWD: Integer read Get_SMPP_ESME_RINVPASWD;
    property SMPP_ESME_RINVSYSID: Integer read Get_SMPP_ESME_RINVSYSID;
    property SMPP_ESME_RCANCELFAIL: Integer read Get_SMPP_ESME_RCANCELFAIL;
    property SMPP_ESME_RREPLACEFAIL: Integer read Get_SMPP_ESME_RREPLACEFAIL;
    property SMPP_ESME_RMSGQFUL: Integer read Get_SMPP_ESME_RMSGQFUL;
    property SMPP_ESME_RINVSERTYP: Integer read Get_SMPP_ESME_RINVSERTYP;
    property SMPP_ESME_RINVNUMDESTS: Integer read Get_SMPP_ESME_RINVNUMDESTS;
    property SMPP_ESME_RINVDLNAME: Integer read Get_SMPP_ESME_RINVDLNAME;
    property SMPP_ESME_RINVDESTFLAG: Integer read Get_SMPP_ESME_RINVDESTFLAG;
    property SMPP_ESME_RINVSUBREP: Integer read Get_SMPP_ESME_RINVSUBREP;
    property SMPP_ESME_RINVESMCLASS: Integer read Get_SMPP_ESME_RINVESMCLASS;
    property SMPP_ESME_RCNTSUBDL: Integer read Get_SMPP_ESME_RCNTSUBDL;
    property SMPP_ESME_RSUBMITFAIL: Integer read Get_SMPP_ESME_RSUBMITFAIL;
    property SMPP_ESME_RINVSRCTON: Integer read Get_SMPP_ESME_RINVSRCTON;
    property SMPP_ESME_RINVSRCNPI: Integer read Get_SMPP_ESME_RINVSRCNPI;
    property SMPP_ESME_RINVDSTTON: Integer read Get_SMPP_ESME_RINVDSTTON;
    property SMPP_ESME_RINVDSTNPI: Integer read Get_SMPP_ESME_RINVDSTNPI;
    property SMPP_ESME_RINVSYSTYP: Integer read Get_SMPP_ESME_RINVSYSTYP;
    property SMPP_ESME_RINVREPFLAG: Integer read Get_SMPP_ESME_RINVREPFLAG;
    property SMPP_ESME_RINVNUMMSGS: Integer read Get_SMPP_ESME_RINVNUMMSGS;
    property SMPP_ESME_RTHROTTLED: Integer read Get_SMPP_ESME_RTHROTTLED;
    property SMPP_ESME_RINVSCHED: Integer read Get_SMPP_ESME_RINVSCHED;
    property SMPP_ESME_RINVEXPIRY: Integer read Get_SMPP_ESME_RINVEXPIRY;
    property SMPP_ESME_RINVDFTMSGID: Integer read Get_SMPP_ESME_RINVDFTMSGID;
    property SMPP_ESME_RX_T_APPN: Integer read Get_SMPP_ESME_RX_T_APPN;
    property SMPP_ESME_RX_P_APPN: Integer read Get_SMPP_ESME_RX_P_APPN;
    property SMPP_ESME_RX_R_APPN: Integer read Get_SMPP_ESME_RX_R_APPN;
    property SMPP_ESME_RQUERYFAIL: Integer read Get_SMPP_ESME_RQUERYFAIL;
    property SMPP_ESME_RINVOPTPARSTREAM: Integer read Get_SMPP_ESME_RINVOPTPARSTREAM;
    property SMPP_ESME_ROPTPARNOTALLWD: Integer read Get_SMPP_ESME_ROPTPARNOTALLWD;
    property SMPP_ESME_RINVPARLEN: Integer read Get_SMPP_ESME_RINVPARLEN;
    property SMPP_ESME_RMISSINGOPTPARAM: Integer read Get_SMPP_ESME_RMISSINGOPTPARAM;
    property SMPP_ESME_RINVOPTPARAMVAL: Integer read Get_SMPP_ESME_RINVOPTPARAMVAL;
    property SMPP_ESME_RDELIVERYFAILURE: Integer read Get_SMPP_ESME_RDELIVERYFAILURE;
    property SMPP_ESME_RUNKNOWNERR: Integer read Get_SMPP_ESME_RUNKNOWNERR;
    property GSM_STATUS_MESSAGE_DELIVERED_SUCCESSFULLY: Integer read Get_GSM_STATUS_MESSAGE_DELIVERED_SUCCESSFULLY;
    property GSM_STATUS_FORWARDED_STATUS_UNKNOWN: Integer read Get_GSM_STATUS_FORWARDED_STATUS_UNKNOWN;
    property GSM_STATUS_REPLACED: Integer read Get_GSM_STATUS_REPLACED;
    property GSM_STATUS_CONGESTION_STILL_TRYING: Integer read Get_GSM_STATUS_CONGESTION_STILL_TRYING;
    property GSM_STATUS_RECIPIENT_BUSY_STILL_TRYING: Integer read Get_GSM_STATUS_RECIPIENT_BUSY_STILL_TRYING;
    property GSM_STATUS_NO_RESPONSE_STILL_TRYING: Integer read Get_GSM_STATUS_NO_RESPONSE_STILL_TRYING;
    property GSM_STATUS_SERVICE_REJECTED_STILL_TRYING: Integer read Get_GSM_STATUS_SERVICE_REJECTED_STILL_TRYING;
    property GSM_STATUS_QOS_NOT_AVAILABLE_STILL_TRYING: Integer read Get_GSM_STATUS_QOS_NOT_AVAILABLE_STILL_TRYING;
    property GSM_STATUS_RECIPIENT_ERROR_STILL_TRYING: Integer read Get_GSM_STATUS_RECIPIENT_ERROR_STILL_TRYING;
    property GSM_STATUS_RPC_ERROR: Integer read Get_GSM_STATUS_RPC_ERROR;
    property GSM_STATUS_INCOMPATIBLE_DESTINATION: Integer read Get_GSM_STATUS_INCOMPATIBLE_DESTINATION;
    property GSM_STATUS_CONNECTION_REJECTED: Integer read Get_GSM_STATUS_CONNECTION_REJECTED;
    property GSM_STATUS_NOT_OBTAINABLE: Integer read Get_GSM_STATUS_NOT_OBTAINABLE;
    property GSM_STATUS_QOS_NOT_AVAILABLE: Integer read Get_GSM_STATUS_QOS_NOT_AVAILABLE;
    property GSM_STATUS_NO_INTERNETWORKING_AVAILABLE: Integer read Get_GSM_STATUS_NO_INTERNETWORKING_AVAILABLE;
    property GSM_STATUS_MESSAGE_EXPIRED: Integer read Get_GSM_STATUS_MESSAGE_EXPIRED;
    property GSM_STATUS_MESSAGE_DELETED_BY_SENDER: Integer read Get_GSM_STATUS_MESSAGE_DELETED_BY_SENDER;
    property GSM_STATUS_MESSAGE_DELETED_BY_SMSC: Integer read Get_GSM_STATUS_MESSAGE_DELETED_BY_SMSC;
    property GSM_STATUS_DOES_NOT_EXIST: Integer read Get_GSM_STATUS_DOES_NOT_EXIST;
    property GSM_STORAGETYPE_SIM: Integer read Get_GSM_STORAGETYPE_SIM;
    property GSM_STORAGETYPE_MEMORY: Integer read Get_GSM_STORAGETYPE_MEMORY;
    property GSM_STORAGETYPE_COMBINED: Integer read Get_GSM_STORAGETYPE_COMBINED;
    property GSM_STORAGETYPE_STATUS: Integer read Get_GSM_STORAGETYPE_STATUS;
    property GSM_STORAGETYPE_ALL: Integer read Get_GSM_STORAGETYPE_ALL;
    property GSM_FO_REPLYPATH_EXISTS: Integer read Get_GSM_FO_REPLYPATH_EXISTS;
    property GSM_FO_UDHI: Integer read Get_GSM_FO_UDHI;
    property GSM_FO_STATUS_REPORT: Integer read Get_GSM_FO_STATUS_REPORT;
    property GSM_FO_VALIDITY_NONE: Integer read Get_GSM_FO_VALIDITY_NONE;
    property GSM_FO_VALIDITY_RELATIVE: Integer read Get_GSM_FO_VALIDITY_RELATIVE;
    property GSM_FO_VALIDITY_ENHANCED: Integer read Get_GSM_FO_VALIDITY_ENHANCED;
    property GSM_FO_VALIDITY_ABSOLUTE: Integer read Get_GSM_FO_VALIDITY_ABSOLUTE;
    property GSM_FO_REJECT_DUPLICATES: Integer read Get_GSM_FO_REJECT_DUPLICATES;
    property GSM_FO_SUBMIT_SM: Integer read Get_GSM_FO_SUBMIT_SM;
    property GSM_FO_DELIVER_SM: Integer read Get_GSM_FO_DELIVER_SM;
    property GSM_FO_STATUS_SM: Integer read Get_GSM_FO_STATUS_SM;
    property DATACODING_DEFAULT: Integer read Get_DATACODING_DEFAULT;
    property DATACODING_8BIT_DATA: Integer read Get_DATACODING_8BIT_DATA;
    property DATACODING_UNICODE: Integer read Get_DATACODING_UNICODE;
    property GSM_DATACODING_ME_SPECIFIC: Integer read Get_GSM_DATACODING_ME_SPECIFIC;
    property GSM_DATACODING_SIM_SPECIFIC: Integer read Get_GSM_DATACODING_SIM_SPECIFIC;
    property GSM_DATACODING_TE_SPECIFIC: Integer read Get_GSM_DATACODING_TE_SPECIFIC;
    property DATACODING_FLASH: Integer read Get_DATACODING_FLASH;
    property GSM_BAUDRATE_110: Integer read Get_GSM_BAUDRATE_110;
    property GSM_BAUDRATE_300: Integer read Get_GSM_BAUDRATE_300;
    property GSM_BAUDRATE_600: Integer read Get_GSM_BAUDRATE_600;
    property GSM_BAUDRATE_1200: Integer read Get_GSM_BAUDRATE_1200;
    property GSM_BAUDRATE_2400: Integer read Get_GSM_BAUDRATE_2400;
    property GSM_BAUDRATE_4800: Integer read Get_GSM_BAUDRATE_4800;
    property GSM_BAUDRATE_9600: Integer read Get_GSM_BAUDRATE_9600;
    property GSM_BAUDRATE_14400: Integer read Get_GSM_BAUDRATE_14400;
    property GSM_BAUDRATE_19200: Integer read Get_GSM_BAUDRATE_19200;
    property GSM_BAUDRATE_38400: Integer read Get_GSM_BAUDRATE_38400;
    property GSM_BAUDRATE_56000: Integer read Get_GSM_BAUDRATE_56000;
    property GSM_BAUDRATE_57600: Integer read Get_GSM_BAUDRATE_57600;
    property GSM_BAUDRATE_64000: Integer read Get_GSM_BAUDRATE_64000;
    property GSM_BAUDRATE_115200: Integer read Get_GSM_BAUDRATE_115200;
    property GSM_BAUDRATE_128000: Integer read Get_GSM_BAUDRATE_128000;
    property GSM_BAUDRATE_230400: Integer read Get_GSM_BAUDRATE_230400;
    property GSM_BAUDRATE_256000: Integer read Get_GSM_BAUDRATE_256000;
    property GSM_BAUDRATE_460800: Integer read Get_GSM_BAUDRATE_460800;
    property GSM_BAUDRATE_921600: Integer read Get_GSM_BAUDRATE_921600;
    property GSM_BAUDRATE_DEFAULT: Integer read Get_GSM_BAUDRATE_DEFAULT;
    property GSM_MESSAGESTATE_RECEIVED_UNREAD: Integer read Get_GSM_MESSAGESTATE_RECEIVED_UNREAD;
    property GSM_MESSAGESTATE_RECEIVED_READ: Integer read Get_GSM_MESSAGESTATE_RECEIVED_READ;
    property GSM_MESSAGESTATE_STORED_UNSENT: Integer read Get_GSM_MESSAGESTATE_STORED_UNSENT;
    property GSM_MESSAGESTATE_STORED_SENT: Integer read Get_GSM_MESSAGESTATE_STORED_SENT;
    property GSM_MESSAGESTATE_ALL: Integer read Get_GSM_MESSAGESTATE_ALL;
    property GSM_MESSAGEFORMAT_PDU: Integer read Get_GSM_MESSAGEFORMAT_PDU;
    property GSM_MESSAGEFORMAT_TEXT: Integer read Get_GSM_MESSAGEFORMAT_TEXT;
    property GSM_MESSAGEFORMAT_AUTO: Integer read Get_GSM_MESSAGEFORMAT_AUTO;
    property HTTP_PLACEHOLDER_USERTAG: WideString read Get_HTTP_PLACEHOLDER_USERTAG;
    property HTTP_PLACEHOLDER_TOADDRESS: WideString read Get_HTTP_PLACEHOLDER_TOADDRESS;
    property HTTP_PLACEHOLDER_FROMADDRESS: WideString read Get_HTTP_PLACEHOLDER_FROMADDRESS;
    property HTTP_PLACEHOLDER_BODY: WideString read Get_HTTP_PLACEHOLDER_BODY;
    property HTTP_PLACEHOLDER_BODYASHEX: WideString read Get_HTTP_PLACEHOLDER_BODYASHEX;
    property HTTP_PLACEHOLDER_BODYASBASE64: WideString read Get_HTTP_PLACEHOLDER_BODYASBASE64;
    property HTTP_PLACEHOLDER_DELIVERYREPORT: WideString read Get_HTTP_PLACEHOLDER_DELIVERYREPORT;
    property HTTP_PLACEHOLDER_TOADDRESSTON: WideString read Get_HTTP_PLACEHOLDER_TOADDRESSTON;
    property HTTP_PLACEHOLDER_TOADDRESSNPI: WideString read Get_HTTP_PLACEHOLDER_TOADDRESSNPI;
    property HTTP_PLACEHOLDER_FROMADDRESSTON: WideString read Get_HTTP_PLACEHOLDER_FROMADDRESSTON;
    property HTTP_PLACEHOLDER_FROMADDRESSNPI: WideString read Get_HTTP_PLACEHOLDER_FROMADDRESSNPI;
    property HTTP_PLACEHOLDER_PROTOCOLID: WideString read Get_HTTP_PLACEHOLDER_PROTOCOLID;
    property HTTP_PLACEHOLDER_UDHI: WideString read Get_HTTP_PLACEHOLDER_UDHI;
    property HTTP_PLACEHOLDER_DATACODING: WideString read Get_HTTP_PLACEHOLDER_DATACODING;
    property GSM_PREFIXSMSC_ENABLED: Integer read Get_GSM_PREFIXSMSC_ENABLED;
    property GSM_PREFIXSMSC_DISABLED: Integer read Get_GSM_PREFIXSMSC_DISABLED;
    property GSM_PREFIXSMSC_AUTO: Integer read Get_GSM_PREFIXSMSC_AUTO;
    property WAPPUSH_SIGNAL_NONE: Integer read Get_WAPPUSH_SIGNAL_NONE;
    property WAPPUSH_SIGNAL_LOW: Integer read Get_WAPPUSH_SIGNAL_LOW;
    property WAPPUSH_SIGNAL_MEDIUM: Integer read Get_WAPPUSH_SIGNAL_MEDIUM;
    property WAPPUSH_SIGNAL_HIGH: Integer read Get_WAPPUSH_SIGNAL_HIGH;
    property WAPPUSH_SIGNAL_DELETE: Integer read Get_WAPPUSH_SIGNAL_DELETE;
    property DIALUP_PROVIDERTYPE_UCP: Integer read Get_DIALUP_PROVIDERTYPE_UCP;
    property DIALUP_PROVIDERTYPE_TAP_DEFAULT: Integer read Get_DIALUP_PROVIDERTYPE_TAP_DEFAULT;
    property DIALUP_PROVIDERTYPE_TAP_NOLF: Integer read Get_DIALUP_PROVIDERTYPE_TAP_NOLF;
    property DIALUP_PROVIDERTYPE_TAP_NOEOT: Integer read Get_DIALUP_PROVIDERTYPE_TAP_NOEOT;
    property DIALUP_DIALMODE_TONE: Integer read Get_DIALUP_DIALMODE_TONE;
    property DIALUP_DIALMODE_PULSE: Integer read Get_DIALUP_DIALMODE_PULSE;
    property DIALUP_DEVICESETTINGS_DEFAULT: Integer read Get_DIALUP_DEVICESETTINGS_DEFAULT;
    property DIALUP_DEVICESETTINGS_8N1: Integer read Get_DIALUP_DEVICESETTINGS_8N1;
    property DIALUP_DEVICESETTINGS_7E1: Integer read Get_DIALUP_DEVICESETTINGS_7E1;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TConstantsProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TConstants
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TConstantsProperties = class(TPersistent)
  private
    FServer:    TConstants;
    function    GetDefaultInterface: IConstants;
    constructor Create(AServer: TConstants);
  protected
    function Get_SMPP_BIND_TRANSMITTER: Integer;
    function Get_SMPP_BIND_TRANSCEIVER: Integer;
    function Get_SMPP_BIND_RECEIVER: Integer;
    function Get_SMPP_VERSION_33: Integer;
    function Get_SMPP_VERSION_34: Integer;
    function Get_SMPP_VERSION_50: Integer;
    function Get_TON_UNKNOWN: Integer;
    function Get_TON_INTERNATIONAL: Integer;
    function Get_TON_NATIONAL: Integer;
    function Get_TON_NETWORK_SPECIFIC: Integer;
    function Get_TON_SUBSCRIBER_NUMBER: Integer;
    function Get_TON_ALPHANUMERIC: Integer;
    function Get_SMPP_TON_ABBREVIATED: Integer;
    function Get_NPI_UNKNOWN: Integer;
    function Get_NPI_ISDN: Integer;
    function Get_NPI_DATA: Integer;
    function Get_NPI_TELEX: Integer;
    function Get_NPI_NATIONAL: Integer;
    function Get_NPI_PRIVATE: Integer;
    function Get_NPI_ERMES: Integer;
    function Get_SMPP_NPI_INTERNET: Integer;
    function Get_NPI_LAND_MOBILE: Integer;
    function Get_MULTIPART_ACCEPT: Integer;
    function Get_MULTIPART_TRUNCATE: Integer;
    function Get_MULTIPART_REJECT: Integer;
    function Get_BODYFORMAT_TEXT: Integer;
    function Get_BODYFORMAT_HEX: Integer;
    function Get_SMPP_ESM_2ESME_DEFAULT: Integer;
    function Get_SMPP_ESM_2ESME_DELIVERY_RECEIPT: Integer;
    function Get_SMPP_ESM_2ESME_DELIVERY_ACK: Integer;
    function Get_SMPP_ESM_2ESME_MANUAL_ACK: Integer;
    function Get_SMPP_ESM_2ESME_CONVERSATION_ABORT: Integer;
    function Get_SMPP_ESM_2ESME_INTERMEDIATE_DELIVERY_NOTIFY: Integer;
    function Get_SMPP_ESM_2SMSC_MODE_DEFAULT: Integer;
    function Get_SMPP_ESM_2SMSC_MODE_STOREFORWARD: Integer;
    function Get_SMPP_ESM_2SMSC_MODE_DATAGRAM: Integer;
    function Get_SMPP_ESM_2SMSC_MODE_FORWARD: Integer;
    function Get_SMPP_ESM_2SMSC_TYPE_DEFAULT: Integer;
    function Get_SMPP_ESM_2SMSC_TYPE_DELIVERY_ACK: Integer;
    function Get_SMPP_ESM_2SMSC_TYPE_MANUAL_ACK: Integer;
    function Get_SMPP_ESM_2SMSC_FEAT_NOTHING: Integer;
    function Get_SMPP_ESM_2SMSC_FEAT_UDHI: Integer;
    function Get_SMPP_ESM_2SMSC_FEAT_SRP: Integer;
    function Get_SMPP_USEGSMENCODING_DISABLED: Integer;
    function Get_SMPP_USEGSMENCODING_INANDOUT: Integer;
    function Get_SMPP_USEGSMENCODING_INCOMING: Integer;
    function Get_SMPP_USEGSMENCODING_OUTGOING: Integer;
    function Get_SMPP_DATACODING_ASCII: Integer;
    function Get_SMPP_DATACODING_OCTET_UNSPEC: Integer;
    function Get_SMPP_DATACODING_LATIN: Integer;
    function Get_SMPP_DATACODING_JIS_KANJI: Integer;
    function Get_SMPP_DATACODING_CYRILLIC: Integer;
    function Get_SMPP_DATACODING_LATIN_HEBREW: Integer;
    function Get_SMPP_DATACODING_PICTOGRAM: Integer;
    function Get_SMPP_DATACODING_ISO_2022_JP: Integer;
    function Get_SMPP_DATACODING_EXTENDED_KANJI_JIS: Integer;
    function Get_SMPP_DATACODING_KS_C_5601: Integer;
    function Get_SMPP_PRIORITYFLAG_BULK: Integer;
    function Get_SMPP_PRIORITYFLAG_NORMAL: Integer;
    function Get_SMPP_PRIORITYFLAG_URGENT: Integer;
    function Get_SMPP_PRIORITYFLAG_VERY_URGENT: Integer;
    function Get_SMPP_MESSAGESTATE_AX_WAITRESP: Integer;
    function Get_SMPP_MESSAGESTATE_ENROUTE: Integer;
    function Get_SMPP_MESSAGESTATE_DELIVERED: Integer;
    function Get_SMPP_MESSAGESTATE_EXPIRED: Integer;
    function Get_SMPP_MESSAGESTATE_DELETED: Integer;
    function Get_SMPP_MESSAGESTATE_UNDELIVERABLE: Integer;
    function Get_SMPP_MESSAGESTATE_ACCEPTED: Integer;
    function Get_SMPP_MESSAGESTATE_UNKNOWN: Integer;
    function Get_SMPP_MESSAGESTATE_REJECTED: Integer;
    function Get_SMPP_MESSAGESTATE_AX_RESPERROR: Integer;
    function Get_SMPP_MESSAGESTATE_AX_NOCREDITS: Integer;
    function Get_SMPP_MESSAGESTATE_AX_RESPTO: Integer;
    function Get_SMPP_MESSAGESTATE_AX_RESPONDED: Integer;
    function Get_SMPP_SESSIONSTATE_CONNECTED: Integer;
    function Get_SMPP_SESSIONSTATE_DISCONNECTED: Integer;
    function Get_SMPP_SESSIONSTATE_BINDING: Integer;
    function Get_SMPP_SESSIONSTATE_BOUND_TX: Integer;
    function Get_SMPP_SESSIONSTATE_BOUND_RX: Integer;
    function Get_SMPP_SESSIONSTATE_BOUND_TRX: Integer;
    function Get_SMPP_TLV_DEST_ADDR_SUBUNIT: Integer;
    function Get_SMPP_TLV_DEST_NETWORK_TYPE: Integer;
    function Get_SMPP_TLV_DEST_BEARER_TYPE: Integer;
    function Get_SMPP_TLV_DEST_TELEMATICS_ID: Integer;
    function Get_SMPP_TLV_SOURCE_ADDR_SUBUNIT: Integer;
    function Get_SMPP_TLV_SOURCE_NETWORK_TYPE: Integer;
    function Get_SMPP_TLV_SOURCE_BEARER_TYPE: Integer;
    function Get_SMPP_TLV_SOURCE_TELEMATICS_ID: Integer;
    function Get_SMPP_TLV_QOS_TIME_TO_LIVE: Integer;
    function Get_SMPP_TLV_PAYLOAD_TYPE: Integer;
    function Get_SMPP_TLV_ADDITIONAL_STATUS_INFO_TEXT: Integer;
    function Get_SMPP_TLV_RECEIPTED_MESSAGE_ID: Integer;
    function Get_SMPP_TLV_MS_MSG_WAIT_FACILITIES: Integer;
    function Get_SMPP_TLV_PRIVACY_INDICATOR: Integer;
    function Get_SMPP_TLV_SOURCE_SUBADDRESS: Integer;
    function Get_SMPP_TLV_DEST_SUBADDRESS: Integer;
    function Get_SMPP_TLV_USER_MESSAGE_REFERENCE: Integer;
    function Get_SMPP_TLV_USER_RESPONSE_CODE: Integer;
    function Get_SMPP_TLV_SOURCE_PORT: Integer;
    function Get_SMPP_TLV_DESTINATION_PORT: Integer;
    function Get_SMPP_TLV_SAR_MSG_REF_NUM: Integer;
    function Get_SMPP_TLV_LANGUAGE_INDICATOR: Integer;
    function Get_SMPP_TLV_SAR_TOTAL_SEGMENTS: Integer;
    function Get_SMPP_TLV_SAR_SEGMENT_SEQNUM: Integer;
    function Get_SMPP_TLV_SC_INTERFACE_VERSION: Integer;
    function Get_SMPP_TLV_CALLBACK_NUM_PRES_IND: Integer;
    function Get_SMPP_TLV_CALLBACK_NUM_ATAG: Integer;
    function Get_SMPP_TLV_NUMBER_OF_MESSAGES: Integer;
    function Get_SMPP_TLV_CALLBACK_NUM: Integer;
    function Get_SMPP_TLV_DPF_RESULT: Integer;
    function Get_SMPP_TLV_SET_DPF: Integer;
    function Get_SMPP_TLV_MS_AVAILABILITY_STATUS: Integer;
    function Get_SMPP_TLV_NETWORK_ERROR_CODE: Integer;
    function Get_SMPP_TLV_MESSAGE_PAYLOAD: Integer;
    function Get_SMPP_TLV_DELIVERY_FAILURE_REASON: Integer;
    function Get_SMPP_TLV_MORE_MESSAGES_TO_SEND: Integer;
    function Get_SMPP_TLV_MESSAGE_STATE: Integer;
    function Get_SMPP_TLV_CONGESTION_STATE: Integer;
    function Get_SMPP_TLV_USSD_SERVICE_OP: Integer;
    function Get_SMPP_TLV_DISPLAY_TIME: Integer;
    function Get_SMPP_TLV_SMS_SIGNAL: Integer;
    function Get_SMPP_TLV_MS_VALIDITY: Integer;
    function Get_SMPP_TLV_ALERT_ON_MESSAGE_DELIVERY: Integer;
    function Get_SMPP_TLV_ITS_REPLY_TYPE: Integer;
    function Get_SMPP_TLV_ITS_SESSION_INFO: Integer;
    function Get_SMPP_MULTIPARTMODE_UDH: Integer;
    function Get_SMPP_MULTIPARTMODE_UDH16BIT: Integer;
    function Get_SMPP_MULTIPARTMODE_SARTLV: Integer;
    function Get_SMPP_MULTIPARTMODE_PAYLOADTLV: Integer;
    function Get_SMPP_SUBMITMODE_SUBMITSM: Integer;
    function Get_SMPP_SUBMITMODE_DATASM: Integer;
    function Get_SMPP_USEGSMENCODING_INCHARSET: Integer;
    function Get_SMPP_USEGSMENCODING_OUTCHARSET: Integer;
    function Get_SMPP_USEGSMENCODING_INOUTCHARS: Integer;
    function Get_SMPP_IPVERSION_4: Integer;
    function Get_SMPP_IPVERSION_6: Integer;
    function Get_SMPP_IPVERSION_BOTH: Integer;
    function Get_SMPP_ESME_ROK: Integer;
    function Get_SMPP_ESME_RINVMSGLEN: Integer;
    function Get_SMPP_ESME_RINVCMDLEN: Integer;
    function Get_SMPP_ESME_RINVCMDID: Integer;
    function Get_SMPP_ESME_RINVBNDSTS: Integer;
    function Get_SMPP_ESME_RALYBND: Integer;
    function Get_SMPP_ESME_RINVPRTFLG: Integer;
    function Get_SMPP_ESME_RINVREGDLVFLG: Integer;
    function Get_SMPP_ESME_RSYSERR: Integer;
    function Get_SMPP_ESME_RINVSRCADR: Integer;
    function Get_SMPP_ESME_RINVDSTADR: Integer;
    function Get_SMPP_ESME_RINVMSGID: Integer;
    function Get_SMPP_ESME_RBINDFAIL: Integer;
    function Get_SMPP_ESME_RINVPASWD: Integer;
    function Get_SMPP_ESME_RINVSYSID: Integer;
    function Get_SMPP_ESME_RCANCELFAIL: Integer;
    function Get_SMPP_ESME_RREPLACEFAIL: Integer;
    function Get_SMPP_ESME_RMSGQFUL: Integer;
    function Get_SMPP_ESME_RINVSERTYP: Integer;
    function Get_SMPP_ESME_RINVNUMDESTS: Integer;
    function Get_SMPP_ESME_RINVDLNAME: Integer;
    function Get_SMPP_ESME_RINVDESTFLAG: Integer;
    function Get_SMPP_ESME_RINVSUBREP: Integer;
    function Get_SMPP_ESME_RINVESMCLASS: Integer;
    function Get_SMPP_ESME_RCNTSUBDL: Integer;
    function Get_SMPP_ESME_RSUBMITFAIL: Integer;
    function Get_SMPP_ESME_RINVSRCTON: Integer;
    function Get_SMPP_ESME_RINVSRCNPI: Integer;
    function Get_SMPP_ESME_RINVDSTTON: Integer;
    function Get_SMPP_ESME_RINVDSTNPI: Integer;
    function Get_SMPP_ESME_RINVSYSTYP: Integer;
    function Get_SMPP_ESME_RINVREPFLAG: Integer;
    function Get_SMPP_ESME_RINVNUMMSGS: Integer;
    function Get_SMPP_ESME_RTHROTTLED: Integer;
    function Get_SMPP_ESME_RINVSCHED: Integer;
    function Get_SMPP_ESME_RINVEXPIRY: Integer;
    function Get_SMPP_ESME_RINVDFTMSGID: Integer;
    function Get_SMPP_ESME_RX_T_APPN: Integer;
    function Get_SMPP_ESME_RX_P_APPN: Integer;
    function Get_SMPP_ESME_RX_R_APPN: Integer;
    function Get_SMPP_ESME_RQUERYFAIL: Integer;
    function Get_SMPP_ESME_RINVOPTPARSTREAM: Integer;
    function Get_SMPP_ESME_ROPTPARNOTALLWD: Integer;
    function Get_SMPP_ESME_RINVPARLEN: Integer;
    function Get_SMPP_ESME_RMISSINGOPTPARAM: Integer;
    function Get_SMPP_ESME_RINVOPTPARAMVAL: Integer;
    function Get_SMPP_ESME_RDELIVERYFAILURE: Integer;
    function Get_SMPP_ESME_RUNKNOWNERR: Integer;
    function Get_GSM_STATUS_MESSAGE_DELIVERED_SUCCESSFULLY: Integer;
    function Get_GSM_STATUS_FORWARDED_STATUS_UNKNOWN: Integer;
    function Get_GSM_STATUS_REPLACED: Integer;
    function Get_GSM_STATUS_CONGESTION_STILL_TRYING: Integer;
    function Get_GSM_STATUS_RECIPIENT_BUSY_STILL_TRYING: Integer;
    function Get_GSM_STATUS_NO_RESPONSE_STILL_TRYING: Integer;
    function Get_GSM_STATUS_SERVICE_REJECTED_STILL_TRYING: Integer;
    function Get_GSM_STATUS_QOS_NOT_AVAILABLE_STILL_TRYING: Integer;
    function Get_GSM_STATUS_RECIPIENT_ERROR_STILL_TRYING: Integer;
    function Get_GSM_STATUS_RPC_ERROR: Integer;
    function Get_GSM_STATUS_INCOMPATIBLE_DESTINATION: Integer;
    function Get_GSM_STATUS_CONNECTION_REJECTED: Integer;
    function Get_GSM_STATUS_NOT_OBTAINABLE: Integer;
    function Get_GSM_STATUS_QOS_NOT_AVAILABLE: Integer;
    function Get_GSM_STATUS_NO_INTERNETWORKING_AVAILABLE: Integer;
    function Get_GSM_STATUS_MESSAGE_EXPIRED: Integer;
    function Get_GSM_STATUS_MESSAGE_DELETED_BY_SENDER: Integer;
    function Get_GSM_STATUS_MESSAGE_DELETED_BY_SMSC: Integer;
    function Get_GSM_STATUS_DOES_NOT_EXIST: Integer;
    function Get_GSM_STORAGETYPE_SIM: Integer;
    function Get_GSM_STORAGETYPE_MEMORY: Integer;
    function Get_GSM_STORAGETYPE_COMBINED: Integer;
    function Get_GSM_STORAGETYPE_STATUS: Integer;
    function Get_GSM_STORAGETYPE_ALL: Integer;
    function Get_GSM_FO_REPLYPATH_EXISTS: Integer;
    function Get_GSM_FO_UDHI: Integer;
    function Get_GSM_FO_STATUS_REPORT: Integer;
    function Get_GSM_FO_VALIDITY_NONE: Integer;
    function Get_GSM_FO_VALIDITY_RELATIVE: Integer;
    function Get_GSM_FO_VALIDITY_ENHANCED: Integer;
    function Get_GSM_FO_VALIDITY_ABSOLUTE: Integer;
    function Get_GSM_FO_REJECT_DUPLICATES: Integer;
    function Get_GSM_FO_SUBMIT_SM: Integer;
    function Get_GSM_FO_DELIVER_SM: Integer;
    function Get_GSM_FO_STATUS_SM: Integer;
    function Get_DATACODING_DEFAULT: Integer;
    function Get_DATACODING_8BIT_DATA: Integer;
    function Get_DATACODING_UNICODE: Integer;
    function Get_GSM_DATACODING_ME_SPECIFIC: Integer;
    function Get_GSM_DATACODING_SIM_SPECIFIC: Integer;
    function Get_GSM_DATACODING_TE_SPECIFIC: Integer;
    function Get_DATACODING_FLASH: Integer;
    function Get_GSM_BAUDRATE_110: Integer;
    function Get_GSM_BAUDRATE_300: Integer;
    function Get_GSM_BAUDRATE_600: Integer;
    function Get_GSM_BAUDRATE_1200: Integer;
    function Get_GSM_BAUDRATE_2400: Integer;
    function Get_GSM_BAUDRATE_4800: Integer;
    function Get_GSM_BAUDRATE_9600: Integer;
    function Get_GSM_BAUDRATE_14400: Integer;
    function Get_GSM_BAUDRATE_19200: Integer;
    function Get_GSM_BAUDRATE_38400: Integer;
    function Get_GSM_BAUDRATE_56000: Integer;
    function Get_GSM_BAUDRATE_57600: Integer;
    function Get_GSM_BAUDRATE_64000: Integer;
    function Get_GSM_BAUDRATE_115200: Integer;
    function Get_GSM_BAUDRATE_128000: Integer;
    function Get_GSM_BAUDRATE_230400: Integer;
    function Get_GSM_BAUDRATE_256000: Integer;
    function Get_GSM_BAUDRATE_460800: Integer;
    function Get_GSM_BAUDRATE_921600: Integer;
    function Get_GSM_BAUDRATE_DEFAULT: Integer;
    function Get_GSM_MESSAGESTATE_RECEIVED_UNREAD: Integer;
    function Get_GSM_MESSAGESTATE_RECEIVED_READ: Integer;
    function Get_GSM_MESSAGESTATE_STORED_UNSENT: Integer;
    function Get_GSM_MESSAGESTATE_STORED_SENT: Integer;
    function Get_GSM_MESSAGESTATE_ALL: Integer;
    function Get_GSM_MESSAGEFORMAT_PDU: Integer;
    function Get_GSM_MESSAGEFORMAT_TEXT: Integer;
    function Get_GSM_MESSAGEFORMAT_AUTO: Integer;
    function Get_HTTP_PLACEHOLDER_USERTAG: WideString;
    function Get_HTTP_PLACEHOLDER_TOADDRESS: WideString;
    function Get_HTTP_PLACEHOLDER_FROMADDRESS: WideString;
    function Get_HTTP_PLACEHOLDER_BODY: WideString;
    function Get_HTTP_PLACEHOLDER_BODYASHEX: WideString;
    function Get_HTTP_PLACEHOLDER_BODYASBASE64: WideString;
    function Get_HTTP_PLACEHOLDER_DELIVERYREPORT: WideString;
    function Get_HTTP_PLACEHOLDER_TOADDRESSTON: WideString;
    function Get_HTTP_PLACEHOLDER_TOADDRESSNPI: WideString;
    function Get_HTTP_PLACEHOLDER_FROMADDRESSTON: WideString;
    function Get_HTTP_PLACEHOLDER_FROMADDRESSNPI: WideString;
    function Get_HTTP_PLACEHOLDER_PROTOCOLID: WideString;
    function Get_HTTP_PLACEHOLDER_UDHI: WideString;
    function Get_HTTP_PLACEHOLDER_DATACODING: WideString;
    function Get_GSM_PREFIXSMSC_ENABLED: Integer;
    function Get_GSM_PREFIXSMSC_DISABLED: Integer;
    function Get_GSM_PREFIXSMSC_AUTO: Integer;
    function Get_WAPPUSH_SIGNAL_NONE: Integer;
    function Get_WAPPUSH_SIGNAL_LOW: Integer;
    function Get_WAPPUSH_SIGNAL_MEDIUM: Integer;
    function Get_WAPPUSH_SIGNAL_HIGH: Integer;
    function Get_WAPPUSH_SIGNAL_DELETE: Integer;
    function Get_DIALUP_PROVIDERTYPE_UCP: Integer;
    function Get_DIALUP_PROVIDERTYPE_TAP_DEFAULT: Integer;
    function Get_DIALUP_PROVIDERTYPE_TAP_NOLF: Integer;
    function Get_DIALUP_PROVIDERTYPE_TAP_NOEOT: Integer;
    function Get_DIALUP_DIALMODE_TONE: Integer;
    function Get_DIALUP_DIALMODE_PULSE: Integer;
    function Get_DIALUP_DEVICESETTINGS_DEFAULT: Integer;
    function Get_DIALUP_DEVICESETTINGS_8N1: Integer;
    function Get_DIALUP_DEVICESETTINGS_7E1: Integer;
  public
    property DefaultInterface: IConstants read GetDefaultInterface;
  published
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoMessage provides a Create and CreateRemote method to          
// create instances of the default interface IMessage exposed by              
// the CoClass Message. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoMessage = class
    class function Create: IMessage;
    class function CreateRemote(const MachineName: string): IMessage;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TMessage
// Help String      : Message Class
// Default Interface: IMessage
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TMessageProperties= class;
{$ENDIF}
  TMessage = class(TOleServer)
  private
    FIntf: IMessage;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps: TMessageProperties;
    function GetServerProperties: TMessageProperties;
{$ENDIF}
    function GetDefaultInterface: IMessage;
  protected
    procedure InitServerData; override;
    function Get_UserTag: Integer;
    procedure Set_UserTag(Val: Integer);
    function Get_ToAddress: WideString;
    procedure Set_ToAddress(const Val: WideString);
    function Get_FromAddress: WideString;
    procedure Set_FromAddress(const Val: WideString);
    function Get_Body: WideString;
    procedure Set_Body(const Val: WideString);
    function Get_RequestDeliveryReport: WordBool;
    procedure Set_RequestDeliveryReport(Val: WordBool);
    function Get_ToAddressTON: Integer;
    procedure Set_ToAddressTON(Val: Integer);
    function Get_ToAddressNPI: Integer;
    procedure Set_ToAddressNPI(Val: Integer);
    function Get_FromAddressTON: Integer;
    procedure Set_FromAddressTON(Val: Integer);
    function Get_FromAddressNPI: Integer;
    procedure Set_FromAddressNPI(Val: Integer);
    function Get_ProtocolId: Integer;
    procedure Set_ProtocolId(Val: Integer);
    function Get_ValidityPeriod: Integer;
    procedure Set_ValidityPeriod(Val: Integer);
    function Get_Reference: WideString;
    procedure Set_Reference(const Val: WideString);
    function Get_DataCoding: Integer;
    procedure Set_DataCoding(Val: Integer);
    function Get_BodyFormat: Integer;
    procedure Set_BodyFormat(Val: Integer);
    function Get_TotalParts: Integer;
    function Get_PartNumber: Integer;
    function Get_ReceiveTime: WideString;
    function Get_ReceiveTimeInSeconds: Integer;
    function Get_HasUdh: WordBool;
    procedure Set_HasUdh(Val: WordBool);
    function Get_Incomplete: WordBool;
    function Get_GsmFirstOctet: Integer;
    procedure Set_GsmFirstOctet(Val: Integer);
    function Get_GsmSmscAddress: WideString;
    function Get_GsmSmscAddressTON: Integer;
    function Get_GsmSmscAddressNPI: Integer;
    function Get_GsmMemoryIndex: WideString;
    procedure Set_GsmMemoryIndex(const Val: WideString);
    function Get_GsmMemoryLocation: WideString;
    procedure Set_GsmMemoryLocation(const Val: WideString);
    function Get_SmppPriority: Integer;
    procedure Set_SmppPriority(Val: Integer);
    function Get_SmppServiceType: WideString;
    procedure Set_SmppServiceType(const Val: WideString);
    function Get_SmppEsmClass: Integer;
    procedure Set_SmppEsmClass(Val: Integer);
    function Get_SmppIsDeliveryReport: WordBool;
    procedure Set_SmppIsDeliveryReport(Val: WordBool);
    function Get_SmppStatus: Integer;
    procedure Set_SmppStatus(Val: Integer);
    function Get_SmppError: Integer;
    procedure Set_SmppError(Val: Integer);
    function Get_SmppCommandStatus: Integer;
    procedure Set_SmppCommandStatus(Val: Integer);
    function Get_SmppSequenceNumber: Integer;
    function Get_SmppServerSubmitDate: WideString;
    function Get_SmppServerFinalDate: WideString;
    function Get_LastError: Integer;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IMessage);
    procedure Disconnect; override;
    procedure Clear;
    procedure SmppAddTlv(const Tlv: ITlv);
    function SmppGetFirstTlv: ITlv;
    function SmppGetNextTlv: ITlv;
    function SmppGetTlv(lTag: Integer): ITlv;
    function GetErrorDescription(Error: Integer): WideString;
    property DefaultInterface: IMessage read GetDefaultInterface;
    property TotalParts: Integer read Get_TotalParts;
    property PartNumber: Integer read Get_PartNumber;
    property ReceiveTime: WideString read Get_ReceiveTime;
    property ReceiveTimeInSeconds: Integer read Get_ReceiveTimeInSeconds;
    property Incomplete: WordBool read Get_Incomplete;
    property GsmSmscAddress: WideString read Get_GsmSmscAddress;
    property GsmSmscAddressTON: Integer read Get_GsmSmscAddressTON;
    property GsmSmscAddressNPI: Integer read Get_GsmSmscAddressNPI;
    property SmppSequenceNumber: Integer read Get_SmppSequenceNumber;
    property SmppServerSubmitDate: WideString read Get_SmppServerSubmitDate;
    property SmppServerFinalDate: WideString read Get_SmppServerFinalDate;
    property LastError: Integer read Get_LastError;
    property UserTag: Integer read Get_UserTag write Set_UserTag;
    property ToAddress: WideString read Get_ToAddress write Set_ToAddress;
    property FromAddress: WideString read Get_FromAddress write Set_FromAddress;
    property Body: WideString read Get_Body write Set_Body;
    property RequestDeliveryReport: WordBool read Get_RequestDeliveryReport write Set_RequestDeliveryReport;
    property ToAddressTON: Integer read Get_ToAddressTON write Set_ToAddressTON;
    property ToAddressNPI: Integer read Get_ToAddressNPI write Set_ToAddressNPI;
    property FromAddressTON: Integer read Get_FromAddressTON write Set_FromAddressTON;
    property FromAddressNPI: Integer read Get_FromAddressNPI write Set_FromAddressNPI;
    property ProtocolId: Integer read Get_ProtocolId write Set_ProtocolId;
    property ValidityPeriod: Integer read Get_ValidityPeriod write Set_ValidityPeriod;
    property Reference: WideString read Get_Reference write Set_Reference;
    property DataCoding: Integer read Get_DataCoding write Set_DataCoding;
    property BodyFormat: Integer read Get_BodyFormat write Set_BodyFormat;
    property HasUdh: WordBool read Get_HasUdh write Set_HasUdh;
    property GsmFirstOctet: Integer read Get_GsmFirstOctet write Set_GsmFirstOctet;
    property GsmMemoryIndex: WideString read Get_GsmMemoryIndex write Set_GsmMemoryIndex;
    property GsmMemoryLocation: WideString read Get_GsmMemoryLocation write Set_GsmMemoryLocation;
    property SmppPriority: Integer read Get_SmppPriority write Set_SmppPriority;
    property SmppServiceType: WideString read Get_SmppServiceType write Set_SmppServiceType;
    property SmppEsmClass: Integer read Get_SmppEsmClass write Set_SmppEsmClass;
    property SmppIsDeliveryReport: WordBool read Get_SmppIsDeliveryReport write Set_SmppIsDeliveryReport;
    property SmppStatus: Integer read Get_SmppStatus write Set_SmppStatus;
    property SmppError: Integer read Get_SmppError write Set_SmppError;
    property SmppCommandStatus: Integer read Get_SmppCommandStatus write Set_SmppCommandStatus;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TMessageProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TMessage
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TMessageProperties = class(TPersistent)
  private
    FServer:    TMessage;
    function    GetDefaultInterface: IMessage;
    constructor Create(AServer: TMessage);
  protected
    function Get_UserTag: Integer;
    procedure Set_UserTag(Val: Integer);
    function Get_ToAddress: WideString;
    procedure Set_ToAddress(const Val: WideString);
    function Get_FromAddress: WideString;
    procedure Set_FromAddress(const Val: WideString);
    function Get_Body: WideString;
    procedure Set_Body(const Val: WideString);
    function Get_RequestDeliveryReport: WordBool;
    procedure Set_RequestDeliveryReport(Val: WordBool);
    function Get_ToAddressTON: Integer;
    procedure Set_ToAddressTON(Val: Integer);
    function Get_ToAddressNPI: Integer;
    procedure Set_ToAddressNPI(Val: Integer);
    function Get_FromAddressTON: Integer;
    procedure Set_FromAddressTON(Val: Integer);
    function Get_FromAddressNPI: Integer;
    procedure Set_FromAddressNPI(Val: Integer);
    function Get_ProtocolId: Integer;
    procedure Set_ProtocolId(Val: Integer);
    function Get_ValidityPeriod: Integer;
    procedure Set_ValidityPeriod(Val: Integer);
    function Get_Reference: WideString;
    procedure Set_Reference(const Val: WideString);
    function Get_DataCoding: Integer;
    procedure Set_DataCoding(Val: Integer);
    function Get_BodyFormat: Integer;
    procedure Set_BodyFormat(Val: Integer);
    function Get_TotalParts: Integer;
    function Get_PartNumber: Integer;
    function Get_ReceiveTime: WideString;
    function Get_ReceiveTimeInSeconds: Integer;
    function Get_HasUdh: WordBool;
    procedure Set_HasUdh(Val: WordBool);
    function Get_Incomplete: WordBool;
    function Get_GsmFirstOctet: Integer;
    procedure Set_GsmFirstOctet(Val: Integer);
    function Get_GsmSmscAddress: WideString;
    function Get_GsmSmscAddressTON: Integer;
    function Get_GsmSmscAddressNPI: Integer;
    function Get_GsmMemoryIndex: WideString;
    procedure Set_GsmMemoryIndex(const Val: WideString);
    function Get_GsmMemoryLocation: WideString;
    procedure Set_GsmMemoryLocation(const Val: WideString);
    function Get_SmppPriority: Integer;
    procedure Set_SmppPriority(Val: Integer);
    function Get_SmppServiceType: WideString;
    procedure Set_SmppServiceType(const Val: WideString);
    function Get_SmppEsmClass: Integer;
    procedure Set_SmppEsmClass(Val: Integer);
    function Get_SmppIsDeliveryReport: WordBool;
    procedure Set_SmppIsDeliveryReport(Val: WordBool);
    function Get_SmppStatus: Integer;
    procedure Set_SmppStatus(Val: Integer);
    function Get_SmppError: Integer;
    procedure Set_SmppError(Val: Integer);
    function Get_SmppCommandStatus: Integer;
    procedure Set_SmppCommandStatus(Val: Integer);
    function Get_SmppSequenceNumber: Integer;
    function Get_SmppServerSubmitDate: WideString;
    function Get_SmppServerFinalDate: WideString;
    function Get_LastError: Integer;
  public
    property DefaultInterface: IMessage read GetDefaultInterface;
  published
    property UserTag: Integer read Get_UserTag write Set_UserTag;
    property ToAddress: WideString read Get_ToAddress write Set_ToAddress;
    property FromAddress: WideString read Get_FromAddress write Set_FromAddress;
    property Body: WideString read Get_Body write Set_Body;
    property RequestDeliveryReport: WordBool read Get_RequestDeliveryReport write Set_RequestDeliveryReport;
    property ToAddressTON: Integer read Get_ToAddressTON write Set_ToAddressTON;
    property ToAddressNPI: Integer read Get_ToAddressNPI write Set_ToAddressNPI;
    property FromAddressTON: Integer read Get_FromAddressTON write Set_FromAddressTON;
    property FromAddressNPI: Integer read Get_FromAddressNPI write Set_FromAddressNPI;
    property ProtocolId: Integer read Get_ProtocolId write Set_ProtocolId;
    property ValidityPeriod: Integer read Get_ValidityPeriod write Set_ValidityPeriod;
    property Reference: WideString read Get_Reference write Set_Reference;
    property DataCoding: Integer read Get_DataCoding write Set_DataCoding;
    property BodyFormat: Integer read Get_BodyFormat write Set_BodyFormat;
    property HasUdh: WordBool read Get_HasUdh write Set_HasUdh;
    property GsmFirstOctet: Integer read Get_GsmFirstOctet write Set_GsmFirstOctet;
    property GsmMemoryIndex: WideString read Get_GsmMemoryIndex write Set_GsmMemoryIndex;
    property GsmMemoryLocation: WideString read Get_GsmMemoryLocation write Set_GsmMemoryLocation;
    property SmppPriority: Integer read Get_SmppPriority write Set_SmppPriority;
    property SmppServiceType: WideString read Get_SmppServiceType write Set_SmppServiceType;
    property SmppEsmClass: Integer read Get_SmppEsmClass write Set_SmppEsmClass;
    property SmppIsDeliveryReport: WordBool read Get_SmppIsDeliveryReport write Set_SmppIsDeliveryReport;
    property SmppStatus: Integer read Get_SmppStatus write Set_SmppStatus;
    property SmppError: Integer read Get_SmppError write Set_SmppError;
    property SmppCommandStatus: Integer read Get_SmppCommandStatus write Set_SmppCommandStatus;
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoTlv provides a Create and CreateRemote method to          
// create instances of the default interface ITlv exposed by              
// the CoClass Tlv. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoTlv = class
    class function Create: ITlv;
    class function CreateRemote(const MachineName: string): ITlv;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TTlv
// Help String      : Tlv Class
// Default Interface: ITlv
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TTlvProperties= class;
{$ENDIF}
  TTlv = class(TOleServer)
  private
    FIntf: ITlv;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps: TTlvProperties;
    function GetServerProperties: TTlvProperties;
{$ENDIF}
    function GetDefaultInterface: ITlv;
  protected
    procedure InitServerData; override;
    function Get_Tag: Integer;
    procedure Set_Tag(Val: Integer);
    function Get_ValueAsString: WideString;
    procedure Set_ValueAsString(const Val: WideString);
    function Get_ValueAsHexString: WideString;
    procedure Set_ValueAsHexString(const Val: WideString);
    function Get_ValueAsInt32: Integer;
    procedure Set_ValueAsInt32(Val: Integer);
    function Get_ValueAsInt16: Integer;
    procedure Set_ValueAsInt16(Val: Integer);
    function Get_ValueAsInt8: Integer;
    procedure Set_ValueAsInt8(Val: Integer);
    function Get_Length: Integer;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: ITlv);
    procedure Disconnect; override;
    procedure Clear;
    property DefaultInterface: ITlv read GetDefaultInterface;
    property Length: Integer read Get_Length;
    property Tag: Integer read Get_Tag write Set_Tag;
    property ValueAsString: WideString read Get_ValueAsString write Set_ValueAsString;
    property ValueAsHexString: WideString read Get_ValueAsHexString write Set_ValueAsHexString;
    property ValueAsInt32: Integer read Get_ValueAsInt32 write Set_ValueAsInt32;
    property ValueAsInt16: Integer read Get_ValueAsInt16 write Set_ValueAsInt16;
    property ValueAsInt8: Integer read Get_ValueAsInt8 write Set_ValueAsInt8;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TTlvProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TTlv
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TTlvProperties = class(TPersistent)
  private
    FServer:    TTlv;
    function    GetDefaultInterface: ITlv;
    constructor Create(AServer: TTlv);
  protected
    function Get_Tag: Integer;
    procedure Set_Tag(Val: Integer);
    function Get_ValueAsString: WideString;
    procedure Set_ValueAsString(const Val: WideString);
    function Get_ValueAsHexString: WideString;
    procedure Set_ValueAsHexString(const Val: WideString);
    function Get_ValueAsInt32: Integer;
    procedure Set_ValueAsInt32(Val: Integer);
    function Get_ValueAsInt16: Integer;
    procedure Set_ValueAsInt16(Val: Integer);
    function Get_ValueAsInt8: Integer;
    procedure Set_ValueAsInt8(Val: Integer);
    function Get_Length: Integer;
  public
    property DefaultInterface: ITlv read GetDefaultInterface;
  published
    property Tag: Integer read Get_Tag write Set_Tag;
    property ValueAsString: WideString read Get_ValueAsString write Set_ValueAsString;
    property ValueAsHexString: WideString read Get_ValueAsHexString write Set_ValueAsHexString;
    property ValueAsInt32: Integer read Get_ValueAsInt32 write Set_ValueAsInt32;
    property ValueAsInt16: Integer read Get_ValueAsInt16 write Set_ValueAsInt16;
    property ValueAsInt8: Integer read Get_ValueAsInt8 write Set_ValueAsInt8;
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoGsmDeliveryReport provides a Create and CreateRemote method to          
// create instances of the default interface IGsmDeliveryReport exposed by              
// the CoClass GsmDeliveryReport. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoGsmDeliveryReport = class
    class function Create: IGsmDeliveryReport;
    class function CreateRemote(const MachineName: string): IGsmDeliveryReport;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TGsmDeliveryReport
// Help String      : GsmDeliveryReport Class
// Default Interface: IGsmDeliveryReport
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TGsmDeliveryReportProperties= class;
{$ENDIF}
  TGsmDeliveryReport = class(TOleServer)
  private
    FIntf: IGsmDeliveryReport;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps: TGsmDeliveryReportProperties;
    function GetServerProperties: TGsmDeliveryReportProperties;
{$ENDIF}
    function GetDefaultInterface: IGsmDeliveryReport;
  protected
    procedure InitServerData; override;
    function Get_Reference: WideString;
    function Get_UserTag: Integer;
    procedure Set_UserTag(Val: Integer);
    function Get_SmscAddress: WideString;
    function Get_SmscTime: WideString;
    function Get_SmscTimeInSeconds: Integer;
    function Get_DischargeTime: WideString;
    function Get_DischargeTimeInSeconds: Integer;
    function Get_MemoryIndex: WideString;
    function Get_FirstOctet: Integer;
    function Get_SmscTON: Integer;
    function Get_SmscNPI: Integer;
    function Get_TON: Integer;
    function Get_NPI: Integer;
    function Get_Status: Integer;
    function Get_FromAddress: WideString;
    function Get_MemoryLocation: WideString;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IGsmDeliveryReport);
    procedure Disconnect; override;
    procedure Clear;
    property DefaultInterface: IGsmDeliveryReport read GetDefaultInterface;
    property Reference: WideString read Get_Reference;
    property SmscAddress: WideString read Get_SmscAddress;
    property SmscTime: WideString read Get_SmscTime;
    property SmscTimeInSeconds: Integer read Get_SmscTimeInSeconds;
    property DischargeTime: WideString read Get_DischargeTime;
    property DischargeTimeInSeconds: Integer read Get_DischargeTimeInSeconds;
    property MemoryIndex: WideString read Get_MemoryIndex;
    property FirstOctet: Integer read Get_FirstOctet;
    property SmscTON: Integer read Get_SmscTON;
    property SmscNPI: Integer read Get_SmscNPI;
    property TON: Integer read Get_TON;
    property NPI: Integer read Get_NPI;
    property Status: Integer read Get_Status;
    property FromAddress: WideString read Get_FromAddress;
    property MemoryLocation: WideString read Get_MemoryLocation;
    property UserTag: Integer read Get_UserTag write Set_UserTag;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TGsmDeliveryReportProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TGsmDeliveryReport
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TGsmDeliveryReportProperties = class(TPersistent)
  private
    FServer:    TGsmDeliveryReport;
    function    GetDefaultInterface: IGsmDeliveryReport;
    constructor Create(AServer: TGsmDeliveryReport);
  protected
    function Get_Reference: WideString;
    function Get_UserTag: Integer;
    procedure Set_UserTag(Val: Integer);
    function Get_SmscAddress: WideString;
    function Get_SmscTime: WideString;
    function Get_SmscTimeInSeconds: Integer;
    function Get_DischargeTime: WideString;
    function Get_DischargeTimeInSeconds: Integer;
    function Get_MemoryIndex: WideString;
    function Get_FirstOctet: Integer;
    function Get_SmscTON: Integer;
    function Get_SmscNPI: Integer;
    function Get_TON: Integer;
    function Get_NPI: Integer;
    function Get_Status: Integer;
    function Get_FromAddress: WideString;
    function Get_MemoryLocation: WideString;
  public
    property DefaultInterface: IGsmDeliveryReport read GetDefaultInterface;
  published
    property UserTag: Integer read Get_UserTag write Set_UserTag;
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoDialup provides a Create and CreateRemote method to          
// create instances of the default interface IDialup exposed by              
// the CoClass Dialup. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoDialup = class
    class function Create: IDialup;
    class function CreateRemote(const MachineName: string): IDialup;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TDialup
// Help String      : Dialup Class
// Default Interface: IDialup
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TDialupProperties= class;
{$ENDIF}
  TDialup = class(TOleServer)
  private
    FIntf: IDialup;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps: TDialupProperties;
    function GetServerProperties: TDialupProperties;
{$ENDIF}
    function GetDefaultInterface: IDialup;
  protected
    procedure InitServerData; override;
    function Get_Version: WideString;
    function Get_Build: WideString;
    function Get_Module: WideString;
    function Get_LicenseStatus: WideString;
    function Get_LicenseKey: WideString;
    procedure Set_LicenseKey(const LicenseKey: WideString);
    function Get_LogFile: WideString;
    procedure Set_LogFile(const LogFile: WideString);
    function Get_LastError: Integer;
    function Get_Device: WideString;
    procedure Set_Device(const Val: WideString);
    function Get_DeviceSpeed: Integer;
    procedure Set_DeviceSpeed(Val: Integer);
    function Get_DeviceSettings: Integer;
    procedure Set_DeviceSettings(Val: Integer);
    function Get_DeviceInitString: WideString;
    procedure Set_DeviceInitString(const Val: WideString);
    function Get_DialMode: Integer;
    procedure Set_DialMode(Val: Integer);
    function Get_ProviderDialString: WideString;
    procedure Set_ProviderDialString(const Val: WideString);
    function Get_ProviderPassword: WideString;
    procedure Set_ProviderPassword(const Val: WideString);
    function Get_ProviderType: Integer;
    procedure Set_ProviderType(Val: Integer);
    function Get_ProviderResponse: WideString;
    function Get_AdvancedSettings: WideString;
    procedure Set_AdvancedSettings(const Val: WideString);
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IDialup);
    procedure Disconnect; override;
    procedure SaveLicenseKey;
    procedure Sleep(Ms: Integer);
    procedure Clear;
    function GetErrorDescription(ErrorCode: Integer): WideString;
    function GetDeviceCount: Integer;
    function GetDevice(lIndex: Integer): WideString;
    procedure Send(const Message: IMessage);
    procedure ProviderLoadConfig(const FileName: WideString);
    procedure ProviderSaveConfig(const FileName: WideString);
    property DefaultInterface: IDialup read GetDefaultInterface;
    property Version: WideString read Get_Version;
    property Build: WideString read Get_Build;
    property Module: WideString read Get_Module;
    property LicenseStatus: WideString read Get_LicenseStatus;
    property LastError: Integer read Get_LastError;
    property ProviderResponse: WideString read Get_ProviderResponse;
    property LicenseKey: WideString read Get_LicenseKey write Set_LicenseKey;
    property LogFile: WideString read Get_LogFile write Set_LogFile;
    property Device: WideString read Get_Device write Set_Device;
    property DeviceSpeed: Integer read Get_DeviceSpeed write Set_DeviceSpeed;
    property DeviceSettings: Integer read Get_DeviceSettings write Set_DeviceSettings;
    property DeviceInitString: WideString read Get_DeviceInitString write Set_DeviceInitString;
    property DialMode: Integer read Get_DialMode write Set_DialMode;
    property ProviderDialString: WideString read Get_ProviderDialString write Set_ProviderDialString;
    property ProviderPassword: WideString read Get_ProviderPassword write Set_ProviderPassword;
    property ProviderType: Integer read Get_ProviderType write Set_ProviderType;
    property AdvancedSettings: WideString read Get_AdvancedSettings write Set_AdvancedSettings;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TDialupProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TDialup
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TDialupProperties = class(TPersistent)
  private
    FServer:    TDialup;
    function    GetDefaultInterface: IDialup;
    constructor Create(AServer: TDialup);
  protected
    function Get_Version: WideString;
    function Get_Build: WideString;
    function Get_Module: WideString;
    function Get_LicenseStatus: WideString;
    function Get_LicenseKey: WideString;
    procedure Set_LicenseKey(const LicenseKey: WideString);
    function Get_LogFile: WideString;
    procedure Set_LogFile(const LogFile: WideString);
    function Get_LastError: Integer;
    function Get_Device: WideString;
    procedure Set_Device(const Val: WideString);
    function Get_DeviceSpeed: Integer;
    procedure Set_DeviceSpeed(Val: Integer);
    function Get_DeviceSettings: Integer;
    procedure Set_DeviceSettings(Val: Integer);
    function Get_DeviceInitString: WideString;
    procedure Set_DeviceInitString(const Val: WideString);
    function Get_DialMode: Integer;
    procedure Set_DialMode(Val: Integer);
    function Get_ProviderDialString: WideString;
    procedure Set_ProviderDialString(const Val: WideString);
    function Get_ProviderPassword: WideString;
    procedure Set_ProviderPassword(const Val: WideString);
    function Get_ProviderType: Integer;
    procedure Set_ProviderType(Val: Integer);
    function Get_ProviderResponse: WideString;
    function Get_AdvancedSettings: WideString;
    procedure Set_AdvancedSettings(const Val: WideString);
  public
    property DefaultInterface: IDialup read GetDefaultInterface;
  published
    property LicenseKey: WideString read Get_LicenseKey write Set_LicenseKey;
    property LogFile: WideString read Get_LogFile write Set_LogFile;
    property Device: WideString read Get_Device write Set_Device;
    property DeviceSpeed: Integer read Get_DeviceSpeed write Set_DeviceSpeed;
    property DeviceSettings: Integer read Get_DeviceSettings write Set_DeviceSettings;
    property DeviceInitString: WideString read Get_DeviceInitString write Set_DeviceInitString;
    property DialMode: Integer read Get_DialMode write Set_DialMode;
    property ProviderDialString: WideString read Get_ProviderDialString write Set_ProviderDialString;
    property ProviderPassword: WideString read Get_ProviderPassword write Set_ProviderPassword;
    property ProviderType: Integer read Get_ProviderType write Set_ProviderType;
    property AdvancedSettings: WideString read Get_AdvancedSettings write Set_AdvancedSettings;
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoGsm provides a Create and CreateRemote method to          
// create instances of the default interface IGsm exposed by              
// the CoClass Gsm. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoGsm = class
    class function Create: IGsm;
    class function CreateRemote(const MachineName: string): IGsm;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TGsm
// Help String      : Gsm Class
// Default Interface: IGsm
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TGsmProperties= class;
{$ENDIF}
  TGsm = class(TOleServer)
  private
    FIntf: IGsm;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps: TGsmProperties;
    function GetServerProperties: TGsmProperties;
{$ENDIF}
    function GetDefaultInterface: IGsm;
  protected
    procedure InitServerData; override;
    function Get_Version: WideString;
    function Get_Build: WideString;
    function Get_Module: WideString;
    function Get_LicenseStatus: WideString;
    function Get_LicenseKey: WideString;
    procedure Set_LicenseKey(const LicenseKey: WideString);
    function Get_LogFile: WideString;
    procedure Set_LogFile(const LogFile: WideString);
    function Get_ActivityFile: WideString;
    procedure Set_ActivityFile(const ActivityFile: WideString);
    function Get_LastError: Integer;
    function Get_Manufacturer: WideString;
    function Get_Model: WideString;
    function Get_Revision: WideString;
    function Get_SerialNr: WideString;
    function Get_SendEnabled: WordBool;
    function Get_ReceiveEnabled: WordBool;
    function Get_ReportEnabled: WordBool;
    function Get_ExtractApplicationPort: WordBool;
    procedure Set_ExtractApplicationPort(Val: WordBool);
    function Get_MultipartTimeout: Integer;
    procedure Set_MultipartTimeout(Val: Integer);
    function Get_AssembleMultipart: WordBool;
    procedure Set_AssembleMultipart(Val: WordBool);
    function Get_MessageMode: Integer;
    procedure Set_MessageMode(Val: Integer);
    function Get_PrefixSmscMode: Integer;
    procedure Set_PrefixSmscMode(Val: Integer);
    function Get_NetworkTimeout: Integer;
    procedure Set_NetworkTimeout(Val: Integer);
    function Get_InterCommandDelay: Integer;
    procedure Set_InterCommandDelay(Val: Integer);
    function Get_InterCharacterDelay: Integer;
    procedure Set_InterCharacterDelay(Val: Integer);
    function Get_WaitForNetwork: WordBool;
    procedure Set_WaitForNetwork(Val: WordBool);
    function Get_PreferredSmsc: WideString;
    procedure Set_PreferredSmsc(const Val: WideString);
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IGsm);
    procedure Disconnect; override;
    procedure SaveLicenseKey;
    procedure Sleep(Ms: Integer);
    procedure Clear;
    function GetErrorDescription(ErrorCode: Integer): WideString;
    function FindFirstPort: WideString;
    function FindNextPort: WideString;
    function FindFirstDevice: WideString;
    function FindNextDevice: WideString;
    procedure Open(const strName: WideString; const strPin: WideString; lBaudrate: Integer);
    procedure Close;
    procedure SendCommand(const strCommand: WideString);
    function ReadResponse(lTimeout: Integer): WideString;
    function SendSms(const Val: IMessage; lMultipartFlag: Integer; lTimeout: Integer): WideString;
    procedure Receive(lType: Integer; bDelete: WordBool; lStorageType: Integer; lTimeout: Integer);
    function GetFirstSms: IMessage;
    function GetNextSms: IMessage;
    function GetFirstReport: IGsmDeliveryReport;
    function GetNextReport: IGsmDeliveryReport;
    procedure DeleteSms(const pSms: IMessage);
    procedure DeleteReport(const pReport: IGsmDeliveryReport);
    property DefaultInterface: IGsm read GetDefaultInterface;
    property Version: WideString read Get_Version;
    property Build: WideString read Get_Build;
    property Module: WideString read Get_Module;
    property LicenseStatus: WideString read Get_LicenseStatus;
    property LastError: Integer read Get_LastError;
    property Manufacturer: WideString read Get_Manufacturer;
    property Model: WideString read Get_Model;
    property Revision: WideString read Get_Revision;
    property SerialNr: WideString read Get_SerialNr;
    property SendEnabled: WordBool read Get_SendEnabled;
    property ReceiveEnabled: WordBool read Get_ReceiveEnabled;
    property ReportEnabled: WordBool read Get_ReportEnabled;
    property LicenseKey: WideString read Get_LicenseKey write Set_LicenseKey;
    property LogFile: WideString read Get_LogFile write Set_LogFile;
    property ActivityFile: WideString read Get_ActivityFile write Set_ActivityFile;
    property ExtractApplicationPort: WordBool read Get_ExtractApplicationPort write Set_ExtractApplicationPort;
    property MultipartTimeout: Integer read Get_MultipartTimeout write Set_MultipartTimeout;
    property AssembleMultipart: WordBool read Get_AssembleMultipart write Set_AssembleMultipart;
    property MessageMode: Integer read Get_MessageMode write Set_MessageMode;
    property PrefixSmscMode: Integer read Get_PrefixSmscMode write Set_PrefixSmscMode;
    property NetworkTimeout: Integer read Get_NetworkTimeout write Set_NetworkTimeout;
    property InterCommandDelay: Integer read Get_InterCommandDelay write Set_InterCommandDelay;
    property InterCharacterDelay: Integer read Get_InterCharacterDelay write Set_InterCharacterDelay;
    property WaitForNetwork: WordBool read Get_WaitForNetwork write Set_WaitForNetwork;
    property PreferredSmsc: WideString read Get_PreferredSmsc write Set_PreferredSmsc;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TGsmProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TGsm
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TGsmProperties = class(TPersistent)
  private
    FServer:    TGsm;
    function    GetDefaultInterface: IGsm;
    constructor Create(AServer: TGsm);
  protected
    function Get_Version: WideString;
    function Get_Build: WideString;
    function Get_Module: WideString;
    function Get_LicenseStatus: WideString;
    function Get_LicenseKey: WideString;
    procedure Set_LicenseKey(const LicenseKey: WideString);
    function Get_LogFile: WideString;
    procedure Set_LogFile(const LogFile: WideString);
    function Get_ActivityFile: WideString;
    procedure Set_ActivityFile(const ActivityFile: WideString);
    function Get_LastError: Integer;
    function Get_Manufacturer: WideString;
    function Get_Model: WideString;
    function Get_Revision: WideString;
    function Get_SerialNr: WideString;
    function Get_SendEnabled: WordBool;
    function Get_ReceiveEnabled: WordBool;
    function Get_ReportEnabled: WordBool;
    function Get_ExtractApplicationPort: WordBool;
    procedure Set_ExtractApplicationPort(Val: WordBool);
    function Get_MultipartTimeout: Integer;
    procedure Set_MultipartTimeout(Val: Integer);
    function Get_AssembleMultipart: WordBool;
    procedure Set_AssembleMultipart(Val: WordBool);
    function Get_MessageMode: Integer;
    procedure Set_MessageMode(Val: Integer);
    function Get_PrefixSmscMode: Integer;
    procedure Set_PrefixSmscMode(Val: Integer);
    function Get_NetworkTimeout: Integer;
    procedure Set_NetworkTimeout(Val: Integer);
    function Get_InterCommandDelay: Integer;
    procedure Set_InterCommandDelay(Val: Integer);
    function Get_InterCharacterDelay: Integer;
    procedure Set_InterCharacterDelay(Val: Integer);
    function Get_WaitForNetwork: WordBool;
    procedure Set_WaitForNetwork(Val: WordBool);
    function Get_PreferredSmsc: WideString;
    procedure Set_PreferredSmsc(const Val: WideString);
  public
    property DefaultInterface: IGsm read GetDefaultInterface;
  published
    property LicenseKey: WideString read Get_LicenseKey write Set_LicenseKey;
    property LogFile: WideString read Get_LogFile write Set_LogFile;
    property ActivityFile: WideString read Get_ActivityFile write Set_ActivityFile;
    property ExtractApplicationPort: WordBool read Get_ExtractApplicationPort write Set_ExtractApplicationPort;
    property MultipartTimeout: Integer read Get_MultipartTimeout write Set_MultipartTimeout;
    property AssembleMultipart: WordBool read Get_AssembleMultipart write Set_AssembleMultipart;
    property MessageMode: Integer read Get_MessageMode write Set_MessageMode;
    property PrefixSmscMode: Integer read Get_PrefixSmscMode write Set_PrefixSmscMode;
    property NetworkTimeout: Integer read Get_NetworkTimeout write Set_NetworkTimeout;
    property InterCommandDelay: Integer read Get_InterCommandDelay write Set_InterCommandDelay;
    property InterCharacterDelay: Integer read Get_InterCharacterDelay write Set_InterCharacterDelay;
    property WaitForNetwork: WordBool read Get_WaitForNetwork write Set_WaitForNetwork;
    property PreferredSmsc: WideString read Get_PreferredSmsc write Set_PreferredSmsc;
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoHttp provides a Create and CreateRemote method to          
// create instances of the default interface IHttp exposed by              
// the CoClass Http. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoHttp = class
    class function Create: IHttp;
    class function CreateRemote(const MachineName: string): IHttp;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : THttp
// Help String      : Http Class
// Default Interface: IHttp
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  THttpProperties= class;
{$ENDIF}
  THttp = class(TOleServer)
  private
    FIntf: IHttp;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps: THttpProperties;
    function GetServerProperties: THttpProperties;
{$ENDIF}
    function GetDefaultInterface: IHttp;
  protected
    procedure InitServerData; override;
    function Get_Version: WideString;
    function Get_Build: WideString;
    function Get_Module: WideString;
    function Get_LicenseStatus: WideString;
    function Get_LicenseKey: WideString;
    procedure Set_LicenseKey(const LicenseKey: WideString);
    function Get_LogFile: WideString;
    procedure Set_LogFile(const LogFile: WideString);
    function Get_ActivityFile: WideString;
    procedure Set_ActivityFile(const ActivityFile: WideString);
    function Get_LastError: Integer;
    function Get_Url: WideString;
    procedure Set_Url(const Val: WideString);
    function Get_PostBody: WideString;
    procedure Set_PostBody(const Val: WideString);
    function Get_WebAccount: WideString;
    procedure Set_WebAccount(const Val: WideString);
    function Get_WebPassword: WideString;
    procedure Set_WebPassword(const Val: WideString);
    function Get_ProxyServer: WideString;
    procedure Set_ProxyServer(const Val: WideString);
    function Get_ProxyAccount: WideString;
    procedure Set_ProxyAccount(const Val: WideString);
    function Get_ProxyPassword: WideString;
    procedure Set_ProxyPassword(const Val: WideString);
    function Get_RequestTimeout: Integer;
    procedure Set_RequestTimeout(Val: Integer);
    function Get_LastResponseCode: Integer;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IHttp);
    procedure Disconnect; override;
    procedure SaveLicenseKey;
    procedure Sleep(Ms: Integer);
    procedure Clear;
    function GetErrorDescription(ErrorCode: Integer): WideString;
    function Get(const strUrl: WideString): WideString;
    function Post(const strUrl: WideString; const strPostBody: WideString): WideString;
    function SendSms(const Sms: IMessage; MultipartFlag: Integer): WideString;
    procedure SetHeader(const Header: WideString; const Value: WideString);
    function UrlEncode(const In_: WideString): WideString;
    function Base64Encode(const In_: WideString): WideString;
    function Base64EncodeFile(const FileName: WideString): WideString;
    function HexEncode(const In_: WideString): WideString;
    property DefaultInterface: IHttp read GetDefaultInterface;
    property Version: WideString read Get_Version;
    property Build: WideString read Get_Build;
    property Module: WideString read Get_Module;
    property LicenseStatus: WideString read Get_LicenseStatus;
    property LastError: Integer read Get_LastError;
    property LastResponseCode: Integer read Get_LastResponseCode;
    property LicenseKey: WideString read Get_LicenseKey write Set_LicenseKey;
    property LogFile: WideString read Get_LogFile write Set_LogFile;
    property ActivityFile: WideString read Get_ActivityFile write Set_ActivityFile;
    property Url: WideString read Get_Url write Set_Url;
    property PostBody: WideString read Get_PostBody write Set_PostBody;
    property WebAccount: WideString read Get_WebAccount write Set_WebAccount;
    property WebPassword: WideString read Get_WebPassword write Set_WebPassword;
    property ProxyServer: WideString read Get_ProxyServer write Set_ProxyServer;
    property ProxyAccount: WideString read Get_ProxyAccount write Set_ProxyAccount;
    property ProxyPassword: WideString read Get_ProxyPassword write Set_ProxyPassword;
    property RequestTimeout: Integer read Get_RequestTimeout write Set_RequestTimeout;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: THttpProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : THttp
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 THttpProperties = class(TPersistent)
  private
    FServer:    THttp;
    function    GetDefaultInterface: IHttp;
    constructor Create(AServer: THttp);
  protected
    function Get_Version: WideString;
    function Get_Build: WideString;
    function Get_Module: WideString;
    function Get_LicenseStatus: WideString;
    function Get_LicenseKey: WideString;
    procedure Set_LicenseKey(const LicenseKey: WideString);
    function Get_LogFile: WideString;
    procedure Set_LogFile(const LogFile: WideString);
    function Get_ActivityFile: WideString;
    procedure Set_ActivityFile(const ActivityFile: WideString);
    function Get_LastError: Integer;
    function Get_Url: WideString;
    procedure Set_Url(const Val: WideString);
    function Get_PostBody: WideString;
    procedure Set_PostBody(const Val: WideString);
    function Get_WebAccount: WideString;
    procedure Set_WebAccount(const Val: WideString);
    function Get_WebPassword: WideString;
    procedure Set_WebPassword(const Val: WideString);
    function Get_ProxyServer: WideString;
    procedure Set_ProxyServer(const Val: WideString);
    function Get_ProxyAccount: WideString;
    procedure Set_ProxyAccount(const Val: WideString);
    function Get_ProxyPassword: WideString;
    procedure Set_ProxyPassword(const Val: WideString);
    function Get_RequestTimeout: Integer;
    procedure Set_RequestTimeout(Val: Integer);
    function Get_LastResponseCode: Integer;
  public
    property DefaultInterface: IHttp read GetDefaultInterface;
  published
    property LicenseKey: WideString read Get_LicenseKey write Set_LicenseKey;
    property LogFile: WideString read Get_LogFile write Set_LogFile;
    property ActivityFile: WideString read Get_ActivityFile write Set_ActivityFile;
    property Url: WideString read Get_Url write Set_Url;
    property PostBody: WideString read Get_PostBody write Set_PostBody;
    property WebAccount: WideString read Get_WebAccount write Set_WebAccount;
    property WebPassword: WideString read Get_WebPassword write Set_WebPassword;
    property ProxyServer: WideString read Get_ProxyServer write Set_ProxyServer;
    property ProxyAccount: WideString read Get_ProxyAccount write Set_ProxyAccount;
    property ProxyPassword: WideString read Get_ProxyPassword write Set_ProxyPassword;
    property RequestTimeout: Integer read Get_RequestTimeout write Set_RequestTimeout;
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoSnpp provides a Create and CreateRemote method to          
// create instances of the default interface ISnpp exposed by              
// the CoClass Snpp. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSnpp = class
    class function Create: ISnpp;
    class function CreateRemote(const MachineName: string): ISnpp;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TSnpp
// Help String      : Snpp Class
// Default Interface: ISnpp
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TSnppProperties= class;
{$ENDIF}
  TSnpp = class(TOleServer)
  private
    FIntf: ISnpp;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps: TSnppProperties;
    function GetServerProperties: TSnppProperties;
{$ENDIF}
    function GetDefaultInterface: ISnpp;
  protected
    procedure InitServerData; override;
    function Get_Version: WideString;
    function Get_Build: WideString;
    function Get_Module: WideString;
    function Get_LicenseStatus: WideString;
    function Get_LicenseKey: WideString;
    procedure Set_LicenseKey(const LicenseKey: WideString);
    function Get_LogFile: WideString;
    procedure Set_LogFile(const LogFile: WideString);
    function Get_LastError: Integer;
    function Get_Server: WideString;
    procedure Set_Server(const Val: WideString);
    function Get_ServerPort: Integer;
    procedure Set_ServerPort(Val: Integer);
    function Get_ServerTimeout: Integer;
    procedure Set_ServerTimeout(Val: Integer);
    function Get_ProviderPassword: WideString;
    procedure Set_ProviderPassword(const Val: WideString);
    function Get_ProviderUsername: WideString;
    procedure Set_ProviderUsername(const Val: WideString);
    function Get_ProviderResponse: WideString;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: ISnpp);
    procedure Disconnect; override;
    procedure SaveLicenseKey;
    procedure Clear;
    function GetErrorDescription(ErrorCode: Integer): WideString;
    procedure Send(const ToAddress: WideString; const Message: WideString);
    property DefaultInterface: ISnpp read GetDefaultInterface;
    property Version: WideString read Get_Version;
    property Build: WideString read Get_Build;
    property Module: WideString read Get_Module;
    property LicenseStatus: WideString read Get_LicenseStatus;
    property LastError: Integer read Get_LastError;
    property ProviderResponse: WideString read Get_ProviderResponse;
    property LicenseKey: WideString read Get_LicenseKey write Set_LicenseKey;
    property LogFile: WideString read Get_LogFile write Set_LogFile;
    property Server: WideString read Get_Server write Set_Server;
    property ServerPort: Integer read Get_ServerPort write Set_ServerPort;
    property ServerTimeout: Integer read Get_ServerTimeout write Set_ServerTimeout;
    property ProviderPassword: WideString read Get_ProviderPassword write Set_ProviderPassword;
    property ProviderUsername: WideString read Get_ProviderUsername write Set_ProviderUsername;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TSnppProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TSnpp
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TSnppProperties = class(TPersistent)
  private
    FServer:    TSnpp;
    function    GetDefaultInterface: ISnpp;
    constructor Create(AServer: TSnpp);
  protected
    function Get_Version: WideString;
    function Get_Build: WideString;
    function Get_Module: WideString;
    function Get_LicenseStatus: WideString;
    function Get_LicenseKey: WideString;
    procedure Set_LicenseKey(const LicenseKey: WideString);
    function Get_LogFile: WideString;
    procedure Set_LogFile(const LogFile: WideString);
    function Get_LastError: Integer;
    function Get_Server: WideString;
    procedure Set_Server(const Val: WideString);
    function Get_ServerPort: Integer;
    procedure Set_ServerPort(Val: Integer);
    function Get_ServerTimeout: Integer;
    procedure Set_ServerTimeout(Val: Integer);
    function Get_ProviderPassword: WideString;
    procedure Set_ProviderPassword(const Val: WideString);
    function Get_ProviderUsername: WideString;
    procedure Set_ProviderUsername(const Val: WideString);
    function Get_ProviderResponse: WideString;
  public
    property DefaultInterface: ISnpp read GetDefaultInterface;
  published
    property LicenseKey: WideString read Get_LicenseKey write Set_LicenseKey;
    property LogFile: WideString read Get_LogFile write Set_LogFile;
    property Server: WideString read Get_Server write Set_Server;
    property ServerPort: Integer read Get_ServerPort write Set_ServerPort;
    property ServerTimeout: Integer read Get_ServerTimeout write Set_ServerTimeout;
    property ProviderPassword: WideString read Get_ProviderPassword write Set_ProviderPassword;
    property ProviderUsername: WideString read Get_ProviderUsername write Set_ProviderUsername;
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoSmpp provides a Create and CreateRemote method to          
// create instances of the default interface ISmpp exposed by              
// the CoClass Smpp. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSmpp = class
    class function Create: ISmpp;
    class function CreateRemote(const MachineName: string): ISmpp;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TSmpp
// Help String      : Smpp Class
// Default Interface: ISmpp
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TSmppProperties= class;
{$ENDIF}
  TSmpp = class(TOleServer)
  private
    FIntf: ISmpp;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps: TSmppProperties;
    function GetServerProperties: TSmppProperties;
{$ENDIF}
    function GetDefaultInterface: ISmpp;
  protected
    procedure InitServerData; override;
    function Get_Version: WideString;
    function Get_Build: WideString;
    function Get_Module: WideString;
    function Get_LicenseStatus: WideString;
    function Get_LicenseKey: WideString;
    procedure Set_LicenseKey(const LicenseKey: WideString);
    function Get_LogFile: WideString;
    procedure Set_LogFile(const LogFile: WideString);
    function Get_ActivityFile: WideString;
    procedure Set_ActivityFile(const ActivityFile: WideString);
    function Get_LogPduDetails: WordBool;
    procedure Set_LogPduDetails(LogDetails: WordBool);
    function Get_LastError: Integer;
    function Get_IsConnected: WordBool;
    function Get_IsBound: WordBool;
    function Get_MaxOutPendingPdus: Integer;
    procedure Set_MaxOutPendingPdus(Val: Integer);
    function Get_MaxSmsSubmissions: Integer;
    procedure Set_MaxSmsSubmissions(Val: Integer);
    function Get_MaxSmsQueries: Integer;
    procedure Set_MaxSmsQueries(Val: Integer);
    function Get_PduTimeout: Integer;
    procedure Set_PduTimeout(Val: Integer);
    function Get_EnquireInterval: Integer;
    procedure Set_EnquireInterval(Val: Integer);
    function Get_MultipartTimeout: Integer;
    procedure Set_MultipartTimeout(Val: Integer);
    function Get_UseGsmEncoding: Integer;
    procedure Set_UseGsmEncoding(Val: Integer);
    function Get_AssembleMultipart: WordBool;
    procedure Set_AssembleMultipart(Val: WordBool);
    function Get_MultipartMode: Integer;
    procedure Set_MultipartMode(Val: Integer);
    function Get_ExtractApplicationPort: WordBool;
    procedure Set_ExtractApplicationPort(Val: WordBool);
    function Get_MaxSmsReceived: Integer;
    procedure Set_MaxSmsReceived(Val: Integer);
    function Get_SubmitMode: Integer;
    procedure Set_SubmitMode(Val: Integer);
    function Get_SmsSentPerSecond: Integer;
    function Get_SmsReceivedPerSecond: Integer;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: ISmpp);
    procedure Disconnect; override;
    procedure SaveLicenseKey;
    procedure Sleep(Ms: Integer);
    procedure Clear;
    function GetErrorDescription(ErrorCode: Integer): WideString;
    procedure Connect1(const Host: WideString; Port: Integer; Timeout: Integer);
    procedure Bind(Type_: Integer; const SystemId: WideString; const SystemPassword: WideString; 
                   const SystemType: WideString; Version: Integer; TON: Integer; NPI: Integer; 
                   const AddressRange: WideString; Timeout: Integer);
    procedure Unbind;
    procedure Disconnect1;
    procedure AddBindTlv(const Tlv: ITlv);
    function SubmitSms(const Sms: IMessage; MultipartFlag: Integer): Integer;
    function WaitForSmsUpdate(TimeoutMs: Integer): WordBool;
    function FetchSmsUpdate: IMessage;
    procedure QuerySms(const Sms: IMessage);
    function ReceiveMessage: IMessage;
    function CountSmsReceived: Integer;
    function CountSmsSubmitSpace: Integer;
    function CountSmsQuerySpace: Integer;
    property DefaultInterface: ISmpp read GetDefaultInterface;
    property Version: WideString read Get_Version;
    property Build: WideString read Get_Build;
    property Module: WideString read Get_Module;
    property LicenseStatus: WideString read Get_LicenseStatus;
    property LastError: Integer read Get_LastError;
    property IsConnected: WordBool read Get_IsConnected;
    property IsBound: WordBool read Get_IsBound;
    property SmsSentPerSecond: Integer read Get_SmsSentPerSecond;
    property SmsReceivedPerSecond: Integer read Get_SmsReceivedPerSecond;
    property LicenseKey: WideString read Get_LicenseKey write Set_LicenseKey;
    property LogFile: WideString read Get_LogFile write Set_LogFile;
    property ActivityFile: WideString read Get_ActivityFile write Set_ActivityFile;
    property LogPduDetails: WordBool read Get_LogPduDetails write Set_LogPduDetails;
    property MaxOutPendingPdus: Integer read Get_MaxOutPendingPdus write Set_MaxOutPendingPdus;
    property MaxSmsSubmissions: Integer read Get_MaxSmsSubmissions write Set_MaxSmsSubmissions;
    property MaxSmsQueries: Integer read Get_MaxSmsQueries write Set_MaxSmsQueries;
    property PduTimeout: Integer read Get_PduTimeout write Set_PduTimeout;
    property EnquireInterval: Integer read Get_EnquireInterval write Set_EnquireInterval;
    property MultipartTimeout: Integer read Get_MultipartTimeout write Set_MultipartTimeout;
    property UseGsmEncoding: Integer read Get_UseGsmEncoding write Set_UseGsmEncoding;
    property AssembleMultipart: WordBool read Get_AssembleMultipart write Set_AssembleMultipart;
    property MultipartMode: Integer read Get_MultipartMode write Set_MultipartMode;
    property ExtractApplicationPort: WordBool read Get_ExtractApplicationPort write Set_ExtractApplicationPort;
    property MaxSmsReceived: Integer read Get_MaxSmsReceived write Set_MaxSmsReceived;
    property SubmitMode: Integer read Get_SubmitMode write Set_SubmitMode;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TSmppProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TSmpp
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TSmppProperties = class(TPersistent)
  private
    FServer:    TSmpp;
    function    GetDefaultInterface: ISmpp;
    constructor Create(AServer: TSmpp);
  protected
    function Get_Version: WideString;
    function Get_Build: WideString;
    function Get_Module: WideString;
    function Get_LicenseStatus: WideString;
    function Get_LicenseKey: WideString;
    procedure Set_LicenseKey(const LicenseKey: WideString);
    function Get_LogFile: WideString;
    procedure Set_LogFile(const LogFile: WideString);
    function Get_ActivityFile: WideString;
    procedure Set_ActivityFile(const ActivityFile: WideString);
    function Get_LogPduDetails: WordBool;
    procedure Set_LogPduDetails(LogDetails: WordBool);
    function Get_LastError: Integer;
    function Get_IsConnected: WordBool;
    function Get_IsBound: WordBool;
    function Get_MaxOutPendingPdus: Integer;
    procedure Set_MaxOutPendingPdus(Val: Integer);
    function Get_MaxSmsSubmissions: Integer;
    procedure Set_MaxSmsSubmissions(Val: Integer);
    function Get_MaxSmsQueries: Integer;
    procedure Set_MaxSmsQueries(Val: Integer);
    function Get_PduTimeout: Integer;
    procedure Set_PduTimeout(Val: Integer);
    function Get_EnquireInterval: Integer;
    procedure Set_EnquireInterval(Val: Integer);
    function Get_MultipartTimeout: Integer;
    procedure Set_MultipartTimeout(Val: Integer);
    function Get_UseGsmEncoding: Integer;
    procedure Set_UseGsmEncoding(Val: Integer);
    function Get_AssembleMultipart: WordBool;
    procedure Set_AssembleMultipart(Val: WordBool);
    function Get_MultipartMode: Integer;
    procedure Set_MultipartMode(Val: Integer);
    function Get_ExtractApplicationPort: WordBool;
    procedure Set_ExtractApplicationPort(Val: WordBool);
    function Get_MaxSmsReceived: Integer;
    procedure Set_MaxSmsReceived(Val: Integer);
    function Get_SubmitMode: Integer;
    procedure Set_SubmitMode(Val: Integer);
    function Get_SmsSentPerSecond: Integer;
    function Get_SmsReceivedPerSecond: Integer;
  public
    property DefaultInterface: ISmpp read GetDefaultInterface;
  published
    property LicenseKey: WideString read Get_LicenseKey write Set_LicenseKey;
    property LogFile: WideString read Get_LogFile write Set_LogFile;
    property ActivityFile: WideString read Get_ActivityFile write Set_ActivityFile;
    property LogPduDetails: WordBool read Get_LogPduDetails write Set_LogPduDetails;
    property MaxOutPendingPdus: Integer read Get_MaxOutPendingPdus write Set_MaxOutPendingPdus;
    property MaxSmsSubmissions: Integer read Get_MaxSmsSubmissions write Set_MaxSmsSubmissions;
    property MaxSmsQueries: Integer read Get_MaxSmsQueries write Set_MaxSmsQueries;
    property PduTimeout: Integer read Get_PduTimeout write Set_PduTimeout;
    property EnquireInterval: Integer read Get_EnquireInterval write Set_EnquireInterval;
    property MultipartTimeout: Integer read Get_MultipartTimeout write Set_MultipartTimeout;
    property UseGsmEncoding: Integer read Get_UseGsmEncoding write Set_UseGsmEncoding;
    property AssembleMultipart: WordBool read Get_AssembleMultipart write Set_AssembleMultipart;
    property MultipartMode: Integer read Get_MultipartMode write Set_MultipartMode;
    property ExtractApplicationPort: WordBool read Get_ExtractApplicationPort write Set_ExtractApplicationPort;
    property MaxSmsReceived: Integer read Get_MaxSmsReceived write Set_MaxSmsReceived;
    property SubmitMode: Integer read Get_SubmitMode write Set_SubmitMode;
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoSmppServer provides a Create and CreateRemote method to          
// create instances of the default interface ISmppServer exposed by              
// the CoClass SmppServer. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSmppServer = class
    class function Create: ISmppServer;
    class function CreateRemote(const MachineName: string): ISmppServer;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TSmppServer
// Help String      : SmppServer Class
// Default Interface: ISmppServer
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TSmppServerProperties= class;
{$ENDIF}
  TSmppServer = class(TOleServer)
  private
    FIntf: ISmppServer;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps: TSmppServerProperties;
    function GetServerProperties: TSmppServerProperties;
{$ENDIF}
    function GetDefaultInterface: ISmppServer;
  protected
    procedure InitServerData; override;
    function Get_Version: WideString;
    function Get_Build: WideString;
    function Get_Module: WideString;
    function Get_LicenseStatus: WideString;
    function Get_LicenseKey: WideString;
    procedure Set_LicenseKey(const LicenseKey: WideString);
    function Get_LogFile: WideString;
    procedure Set_LogFile(const LogFile: WideString);
    function Get_ActivityFile: WideString;
    procedure Set_ActivityFile(const ActivityFile: WideString);
    function Get_LastError: Integer;
    function Get_IsStarted: WordBool;
    function Get_LastReference: Integer;
    procedure Set_LastReference(Val: Integer);
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: ISmppServer);
    procedure Disconnect; override;
    procedure SaveLicenseKey;
    procedure Sleep(Ms: Integer);
    procedure Clear;
    function GetErrorDescription(ErrorCode: Integer): WideString;
    procedure Start(Port: Integer; IpVersion: Integer);
    procedure Stop;
    function GetFirstSession: ISmppSession;
    function GetNextSession: ISmppSession;
    function GetSession(Id: Integer): ISmppSession;
    property DefaultInterface: ISmppServer read GetDefaultInterface;
    property Version: WideString read Get_Version;
    property Build: WideString read Get_Build;
    property Module: WideString read Get_Module;
    property LicenseStatus: WideString read Get_LicenseStatus;
    property LastError: Integer read Get_LastError;
    property IsStarted: WordBool read Get_IsStarted;
    property LicenseKey: WideString read Get_LicenseKey write Set_LicenseKey;
    property LogFile: WideString read Get_LogFile write Set_LogFile;
    property ActivityFile: WideString read Get_ActivityFile write Set_ActivityFile;
    property LastReference: Integer read Get_LastReference write Set_LastReference;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TSmppServerProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TSmppServer
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TSmppServerProperties = class(TPersistent)
  private
    FServer:    TSmppServer;
    function    GetDefaultInterface: ISmppServer;
    constructor Create(AServer: TSmppServer);
  protected
    function Get_Version: WideString;
    function Get_Build: WideString;
    function Get_Module: WideString;
    function Get_LicenseStatus: WideString;
    function Get_LicenseKey: WideString;
    procedure Set_LicenseKey(const LicenseKey: WideString);
    function Get_LogFile: WideString;
    procedure Set_LogFile(const LogFile: WideString);
    function Get_ActivityFile: WideString;
    procedure Set_ActivityFile(const ActivityFile: WideString);
    function Get_LastError: Integer;
    function Get_IsStarted: WordBool;
    function Get_LastReference: Integer;
    procedure Set_LastReference(Val: Integer);
  public
    property DefaultInterface: ISmppServer read GetDefaultInterface;
  published
    property LicenseKey: WideString read Get_LicenseKey write Set_LicenseKey;
    property LogFile: WideString read Get_LogFile write Set_LogFile;
    property ActivityFile: WideString read Get_ActivityFile write Set_ActivityFile;
    property LastReference: Integer read Get_LastReference write Set_LastReference;
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoSmppSession provides a Create and CreateRemote method to          
// create instances of the default interface ISmppSession exposed by              
// the CoClass SmppSession. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSmppSession = class
    class function Create: ISmppSession;
    class function CreateRemote(const MachineName: string): ISmppSession;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TSmppSession
// Help String      : SmppSession Class
// Default Interface: ISmppSession
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TSmppSessionProperties= class;
{$ENDIF}
  TSmppSession = class(TOleServer)
  private
    FIntf: ISmppSession;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps: TSmppSessionProperties;
    function GetServerProperties: TSmppSessionProperties;
{$ENDIF}
    function GetDefaultInterface: ISmppSession;
  protected
    procedure InitServerData; override;
    function Get_LogFile: WideString;
    procedure Set_LogFile(const LogFile: WideString);
    function Get_ActivityFile: WideString;
    procedure Set_ActivityFile(const ActivityFile: WideString);
    function Get_LogPduDetails: WordBool;
    procedure Set_LogPduDetails(LogDetails: WordBool);
    function Get_LastError: Integer;
    function Get_Ip: WideString;
    function Get_Port: Integer;
    function Get_Version: Integer;
    function Get_SystemId: WideString;
    function Get_Password: WideString;
    function Get_SystemType: WideString;
    function Get_AddressRange: WideString;
    function Get_AddressRangeNpi: Integer;
    function Get_AddressRangeTon: Integer;
    function Get_ConnectionState: Integer;
    function Get_RequestedBind: Integer;
    function Get_Id: Integer;
    function Get_MaxSmsDeliveries: Integer;
    procedure Set_MaxSmsDeliveries(Val: Integer);
    function Get_MaxSmsSubmission: Integer;
    procedure Set_MaxSmsSubmission(Val: Integer);
    function Get_MaxSmsQueries: Integer;
    procedure Set_MaxSmsQueries(Val: Integer);
    function Get_SmsSentPerSecond: Integer;
    function Get_SmsReceivedPerSecond: Integer;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: ISmppSession);
    procedure Disconnect; override;
    function GetErrorDescription(ErrorCode: Integer): WideString;
    procedure RespondToBind(Status: Integer);
    procedure RespondToSubmitSms(const Sms: IMessage);
    function ReceiveSubmitSms: IMessage;
    procedure RespondToQuerySms(const Sms: IMessage);
    function ReceiveQuerySms: IMessage;
    procedure DeliverSms(const Sms: IMessage);
    procedure DeliverReport(const Sms: IMessage);
    function ReceiveDeliverResponse: IMessage;
    procedure Disconnect1;
    function CountSmsSubmissions: Integer;
    function CountSmsQueries: Integer;
    function CountSmsDeliverySpace: Integer;
    property DefaultInterface: ISmppSession read GetDefaultInterface;
    property LastError: Integer read Get_LastError;
    property Ip: WideString read Get_Ip;
    property Port: Integer read Get_Port;
    property Version: Integer read Get_Version;
    property SystemId: WideString read Get_SystemId;
    property Password: WideString read Get_Password;
    property SystemType: WideString read Get_SystemType;
    property AddressRange: WideString read Get_AddressRange;
    property AddressRangeNpi: Integer read Get_AddressRangeNpi;
    property AddressRangeTon: Integer read Get_AddressRangeTon;
    property ConnectionState: Integer read Get_ConnectionState;
    property RequestedBind: Integer read Get_RequestedBind;
    property Id: Integer read Get_Id;
    property SmsSentPerSecond: Integer read Get_SmsSentPerSecond;
    property SmsReceivedPerSecond: Integer read Get_SmsReceivedPerSecond;
    property LogFile: WideString read Get_LogFile write Set_LogFile;
    property ActivityFile: WideString read Get_ActivityFile write Set_ActivityFile;
    property LogPduDetails: WordBool read Get_LogPduDetails write Set_LogPduDetails;
    property MaxSmsDeliveries: Integer read Get_MaxSmsDeliveries write Set_MaxSmsDeliveries;
    property MaxSmsSubmission: Integer read Get_MaxSmsSubmission write Set_MaxSmsSubmission;
    property MaxSmsQueries: Integer read Get_MaxSmsQueries write Set_MaxSmsQueries;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TSmppSessionProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TSmppSession
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TSmppSessionProperties = class(TPersistent)
  private
    FServer:    TSmppSession;
    function    GetDefaultInterface: ISmppSession;
    constructor Create(AServer: TSmppSession);
  protected
    function Get_LogFile: WideString;
    procedure Set_LogFile(const LogFile: WideString);
    function Get_ActivityFile: WideString;
    procedure Set_ActivityFile(const ActivityFile: WideString);
    function Get_LogPduDetails: WordBool;
    procedure Set_LogPduDetails(LogDetails: WordBool);
    function Get_LastError: Integer;
    function Get_Ip: WideString;
    function Get_Port: Integer;
    function Get_Version: Integer;
    function Get_SystemId: WideString;
    function Get_Password: WideString;
    function Get_SystemType: WideString;
    function Get_AddressRange: WideString;
    function Get_AddressRangeNpi: Integer;
    function Get_AddressRangeTon: Integer;
    function Get_ConnectionState: Integer;
    function Get_RequestedBind: Integer;
    function Get_Id: Integer;
    function Get_MaxSmsDeliveries: Integer;
    procedure Set_MaxSmsDeliveries(Val: Integer);
    function Get_MaxSmsSubmission: Integer;
    procedure Set_MaxSmsSubmission(Val: Integer);
    function Get_MaxSmsQueries: Integer;
    procedure Set_MaxSmsQueries(Val: Integer);
    function Get_SmsSentPerSecond: Integer;
    function Get_SmsReceivedPerSecond: Integer;
  public
    property DefaultInterface: ISmppSession read GetDefaultInterface;
  published
    property LogFile: WideString read Get_LogFile write Set_LogFile;
    property ActivityFile: WideString read Get_ActivityFile write Set_ActivityFile;
    property LogPduDetails: WordBool read Get_LogPduDetails write Set_LogPduDetails;
    property MaxSmsDeliveries: Integer read Get_MaxSmsDeliveries write Set_MaxSmsDeliveries;
    property MaxSmsSubmission: Integer read Get_MaxSmsSubmission write Set_MaxSmsSubmission;
    property MaxSmsQueries: Integer read Get_MaxSmsQueries write Set_MaxSmsQueries;
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoTemplateWapPush provides a Create and CreateRemote method to          
// create instances of the default interface ITemplateWapPush exposed by              
// the CoClass TemplateWapPush. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoTemplateWapPush = class
    class function Create: ITemplateWapPush;
    class function CreateRemote(const MachineName: string): ITemplateWapPush;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TTemplateWapPush
// Help String      : TemplateWapPush Class
// Default Interface: ITemplateWapPush
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TTemplateWapPushProperties= class;
{$ENDIF}
  TTemplateWapPush = class(TOleServer)
  private
    FIntf: ITemplateWapPush;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps: TTemplateWapPushProperties;
    function GetServerProperties: TTemplateWapPushProperties;
{$ENDIF}
    function GetDefaultInterface: ITemplateWapPush;
  protected
    procedure InitServerData; override;
    function Get_Url: WideString;
    procedure Set_Url(const pVal: WideString);
    function Get_Description: WideString;
    procedure Set_Description(const pVal: WideString);
    function Get_SignalAction: Integer;
    procedure Set_SignalAction(pVal: Integer);
    function Get_Data: WideString;
    function Get_LastError: Integer;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: ITemplateWapPush);
    procedure Disconnect; override;
    procedure Encode;
    function GetErrorDescription(lCode: Integer): WideString;
    procedure Clear;
    property DefaultInterface: ITemplateWapPush read GetDefaultInterface;
    property Data: WideString read Get_Data;
    property LastError: Integer read Get_LastError;
    property Url: WideString read Get_Url write Set_Url;
    property Description: WideString read Get_Description write Set_Description;
    property SignalAction: Integer read Get_SignalAction write Set_SignalAction;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TTemplateWapPushProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TTemplateWapPush
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TTemplateWapPushProperties = class(TPersistent)
  private
    FServer:    TTemplateWapPush;
    function    GetDefaultInterface: ITemplateWapPush;
    constructor Create(AServer: TTemplateWapPush);
  protected
    function Get_Url: WideString;
    procedure Set_Url(const pVal: WideString);
    function Get_Description: WideString;
    procedure Set_Description(const pVal: WideString);
    function Get_SignalAction: Integer;
    procedure Set_SignalAction(pVal: Integer);
    function Get_Data: WideString;
    function Get_LastError: Integer;
  public
    property DefaultInterface: ITemplateWapPush read GetDefaultInterface;
  published
    property Url: WideString read Get_Url write Set_Url;
    property Description: WideString read Get_Description write Set_Description;
    property SignalAction: Integer read Get_SignalAction write Set_SignalAction;
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoTemplatevCard provides a Create and CreateRemote method to          
// create instances of the default interface ITemplatevCard exposed by              
// the CoClass TemplatevCard. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoTemplatevCard = class
    class function Create: ITemplatevCard;
    class function CreateRemote(const MachineName: string): ITemplatevCard;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TTemplatevCard
// Help String      : TemplatevCard Class
// Default Interface: ITemplatevCard
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TTemplatevCardProperties= class;
{$ENDIF}
  TTemplatevCard = class(TOleServer)
  private
    FIntf: ITemplatevCard;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps: TTemplatevCardProperties;
    function GetServerProperties: TTemplatevCardProperties;
{$ENDIF}
    function GetDefaultInterface: ITemplatevCard;
  protected
    procedure InitServerData; override;
    function Get_Url: WideString;
    procedure Set_Url(const pVal: WideString);
    function Get_Title: WideString;
    procedure Set_Title(const pVal: WideString);
    function Get_EMail: WideString;
    procedure Set_EMail(const pVal: WideString);
    function Get_Fax: WideString;
    procedure Set_Fax(const pVal: WideString);
    function Get_Pager: WideString;
    procedure Set_Pager(const pVal: WideString);
    function Get_Mobile: WideString;
    procedure Set_Mobile(const pVal: WideString);
    function Get_PhoneHome: WideString;
    procedure Set_PhoneHome(const pVal: WideString);
    function Get_PhoneWork: WideString;
    procedure Set_PhoneWork(const pVal: WideString);
    function Get_Phone: WideString;
    procedure Set_Phone(const pVal: WideString);
    function Get_Name: WideString;
    procedure Set_Name(const pVal: WideString);
    function Get_Data: WideString;
    function Get_LastError: Integer;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: ITemplatevCard);
    procedure Disconnect; override;
    procedure Clear;
    function GetErrorDescription(lError: Integer): WideString;
    procedure Encode;
    property DefaultInterface: ITemplatevCard read GetDefaultInterface;
    property Data: WideString read Get_Data;
    property LastError: Integer read Get_LastError;
    property Url: WideString read Get_Url write Set_Url;
    property Title: WideString read Get_Title write Set_Title;
    property EMail: WideString read Get_EMail write Set_EMail;
    property Fax: WideString read Get_Fax write Set_Fax;
    property Pager: WideString read Get_Pager write Set_Pager;
    property Mobile: WideString read Get_Mobile write Set_Mobile;
    property PhoneHome: WideString read Get_PhoneHome write Set_PhoneHome;
    property PhoneWork: WideString read Get_PhoneWork write Set_PhoneWork;
    property Phone: WideString read Get_Phone write Set_Phone;
    property Name: WideString read Get_Name write Set_Name;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TTemplatevCardProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TTemplatevCard
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TTemplatevCardProperties = class(TPersistent)
  private
    FServer:    TTemplatevCard;
    function    GetDefaultInterface: ITemplatevCard;
    constructor Create(AServer: TTemplatevCard);
  protected
    function Get_Url: WideString;
    procedure Set_Url(const pVal: WideString);
    function Get_Title: WideString;
    procedure Set_Title(const pVal: WideString);
    function Get_EMail: WideString;
    procedure Set_EMail(const pVal: WideString);
    function Get_Fax: WideString;
    procedure Set_Fax(const pVal: WideString);
    function Get_Pager: WideString;
    procedure Set_Pager(const pVal: WideString);
    function Get_Mobile: WideString;
    procedure Set_Mobile(const pVal: WideString);
    function Get_PhoneHome: WideString;
    procedure Set_PhoneHome(const pVal: WideString);
    function Get_PhoneWork: WideString;
    procedure Set_PhoneWork(const pVal: WideString);
    function Get_Phone: WideString;
    procedure Set_Phone(const pVal: WideString);
    function Get_Name: WideString;
    procedure Set_Name(const pVal: WideString);
    function Get_Data: WideString;
    function Get_LastError: Integer;
  public
    property DefaultInterface: ITemplatevCard read GetDefaultInterface;
  published
    property Url: WideString read Get_Url write Set_Url;
    property Title: WideString read Get_Title write Set_Title;
    property EMail: WideString read Get_EMail write Set_EMail;
    property Fax: WideString read Get_Fax write Set_Fax;
    property Pager: WideString read Get_Pager write Set_Pager;
    property Mobile: WideString read Get_Mobile write Set_Mobile;
    property PhoneHome: WideString read Get_PhoneHome write Set_PhoneHome;
    property PhoneWork: WideString read Get_PhoneWork write Set_PhoneWork;
    property Phone: WideString read Get_Phone write Set_Phone;
    property Name: WideString read Get_Name write Set_Name;
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoDemoAccount provides a Create and CreateRemote method to          
// create instances of the default interface IDemoAccount exposed by              
// the CoClass DemoAccount. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoDemoAccount = class
    class function Create: IDemoAccount;
    class function CreateRemote(const MachineName: string): IDemoAccount;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TDemoAccount
// Help String      : DemoAccount Class
// Default Interface: IDemoAccount
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TDemoAccountProperties= class;
{$ENDIF}
  TDemoAccount = class(TOleServer)
  private
    FIntf: IDemoAccount;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps: TDemoAccountProperties;
    function GetServerProperties: TDemoAccountProperties;
{$ENDIF}
    function GetDefaultInterface: IDemoAccount;
  protected
    procedure InitServerData; override;
    function Get_SystemId: WideString;
    function Get_Password: WideString;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IDemoAccount);
    procedure Disconnect; override;
    property DefaultInterface: IDemoAccount read GetDefaultInterface;
    property SystemId: WideString read Get_SystemId;
    property Password: WideString read Get_Password;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TDemoAccountProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TDemoAccount
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TDemoAccountProperties = class(TPersistent)
  private
    FServer:    TDemoAccount;
    function    GetDefaultInterface: IDemoAccount;
    constructor Create(AServer: TDemoAccount);
  protected
    function Get_SystemId: WideString;
    function Get_Password: WideString;
  public
    property DefaultInterface: IDemoAccount read GetDefaultInterface;
  published
  end;
{$ENDIF}


procedure Register;

resourcestring
  dtlServerPage = 'ActiveX';

  dtlOcxPage = 'ActiveX';

implementation

uses ComObj;

class function CoConstants.Create: IConstants;
begin
  Result := CreateComObject(CLASS_Constants) as IConstants;
end;

class function CoConstants.CreateRemote(const MachineName: string): IConstants;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Constants) as IConstants;
end;

procedure TConstants.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{41A6CF7D-3C5B-40B7-82AC-3AB3BBBF1A84}';
    IntfIID:   '{7D47E1D0-4B72-4A5D-B174-A0E82AF73892}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TConstants.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as IConstants;
  end;
end;

procedure TConstants.ConnectTo(svrIntf: IConstants);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TConstants.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TConstants.GetDefaultInterface: IConstants;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TConstants.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TConstantsProperties.Create(Self);
{$ENDIF}
end;

destructor TConstants.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TConstants.GetServerProperties: TConstantsProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TConstants.Get_SMPP_BIND_TRANSMITTER: Integer;
begin
    Result := DefaultInterface.SMPP_BIND_TRANSMITTER;
end;

function TConstants.Get_SMPP_BIND_TRANSCEIVER: Integer;
begin
    Result := DefaultInterface.SMPP_BIND_TRANSCEIVER;
end;

function TConstants.Get_SMPP_BIND_RECEIVER: Integer;
begin
    Result := DefaultInterface.SMPP_BIND_RECEIVER;
end;

function TConstants.Get_SMPP_VERSION_33: Integer;
begin
    Result := DefaultInterface.SMPP_VERSION_33;
end;

function TConstants.Get_SMPP_VERSION_34: Integer;
begin
    Result := DefaultInterface.SMPP_VERSION_34;
end;

function TConstants.Get_SMPP_VERSION_50: Integer;
begin
    Result := DefaultInterface.SMPP_VERSION_50;
end;

function TConstants.Get_TON_UNKNOWN: Integer;
begin
    Result := DefaultInterface.TON_UNKNOWN;
end;

function TConstants.Get_TON_INTERNATIONAL: Integer;
begin
    Result := DefaultInterface.TON_INTERNATIONAL;
end;

function TConstants.Get_TON_NATIONAL: Integer;
begin
    Result := DefaultInterface.TON_NATIONAL;
end;

function TConstants.Get_TON_NETWORK_SPECIFIC: Integer;
begin
    Result := DefaultInterface.TON_NETWORK_SPECIFIC;
end;

function TConstants.Get_TON_SUBSCRIBER_NUMBER: Integer;
begin
    Result := DefaultInterface.TON_SUBSCRIBER_NUMBER;
end;

function TConstants.Get_TON_ALPHANUMERIC: Integer;
begin
    Result := DefaultInterface.TON_ALPHANUMERIC;
end;

function TConstants.Get_SMPP_TON_ABBREVIATED: Integer;
begin
    Result := DefaultInterface.SMPP_TON_ABBREVIATED;
end;

function TConstants.Get_NPI_UNKNOWN: Integer;
begin
    Result := DefaultInterface.NPI_UNKNOWN;
end;

function TConstants.Get_NPI_ISDN: Integer;
begin
    Result := DefaultInterface.NPI_ISDN;
end;

function TConstants.Get_NPI_DATA: Integer;
begin
    Result := DefaultInterface.NPI_DATA;
end;

function TConstants.Get_NPI_TELEX: Integer;
begin
    Result := DefaultInterface.NPI_TELEX;
end;

function TConstants.Get_NPI_NATIONAL: Integer;
begin
    Result := DefaultInterface.NPI_NATIONAL;
end;

function TConstants.Get_NPI_PRIVATE: Integer;
begin
    Result := DefaultInterface.NPI_PRIVATE;
end;

function TConstants.Get_NPI_ERMES: Integer;
begin
    Result := DefaultInterface.NPI_ERMES;
end;

function TConstants.Get_SMPP_NPI_INTERNET: Integer;
begin
    Result := DefaultInterface.SMPP_NPI_INTERNET;
end;

function TConstants.Get_NPI_LAND_MOBILE: Integer;
begin
    Result := DefaultInterface.NPI_LAND_MOBILE;
end;

function TConstants.Get_MULTIPART_ACCEPT: Integer;
begin
    Result := DefaultInterface.MULTIPART_ACCEPT;
end;

function TConstants.Get_MULTIPART_TRUNCATE: Integer;
begin
    Result := DefaultInterface.MULTIPART_TRUNCATE;
end;

function TConstants.Get_MULTIPART_REJECT: Integer;
begin
    Result := DefaultInterface.MULTIPART_REJECT;
end;

function TConstants.Get_BODYFORMAT_TEXT: Integer;
begin
    Result := DefaultInterface.BODYFORMAT_TEXT;
end;

function TConstants.Get_BODYFORMAT_HEX: Integer;
begin
    Result := DefaultInterface.BODYFORMAT_HEX;
end;

function TConstants.Get_SMPP_ESM_2ESME_DEFAULT: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2ESME_DEFAULT;
end;

function TConstants.Get_SMPP_ESM_2ESME_DELIVERY_RECEIPT: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2ESME_DELIVERY_RECEIPT;
end;

function TConstants.Get_SMPP_ESM_2ESME_DELIVERY_ACK: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2ESME_DELIVERY_ACK;
end;

function TConstants.Get_SMPP_ESM_2ESME_MANUAL_ACK: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2ESME_MANUAL_ACK;
end;

function TConstants.Get_SMPP_ESM_2ESME_CONVERSATION_ABORT: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2ESME_CONVERSATION_ABORT;
end;

function TConstants.Get_SMPP_ESM_2ESME_INTERMEDIATE_DELIVERY_NOTIFY: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2ESME_INTERMEDIATE_DELIVERY_NOTIFY;
end;

function TConstants.Get_SMPP_ESM_2SMSC_MODE_DEFAULT: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2SMSC_MODE_DEFAULT;
end;

function TConstants.Get_SMPP_ESM_2SMSC_MODE_STOREFORWARD: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2SMSC_MODE_STOREFORWARD;
end;

function TConstants.Get_SMPP_ESM_2SMSC_MODE_DATAGRAM: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2SMSC_MODE_DATAGRAM;
end;

function TConstants.Get_SMPP_ESM_2SMSC_MODE_FORWARD: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2SMSC_MODE_FORWARD;
end;

function TConstants.Get_SMPP_ESM_2SMSC_TYPE_DEFAULT: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2SMSC_TYPE_DEFAULT;
end;

function TConstants.Get_SMPP_ESM_2SMSC_TYPE_DELIVERY_ACK: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2SMSC_TYPE_DELIVERY_ACK;
end;

function TConstants.Get_SMPP_ESM_2SMSC_TYPE_MANUAL_ACK: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2SMSC_TYPE_MANUAL_ACK;
end;

function TConstants.Get_SMPP_ESM_2SMSC_FEAT_NOTHING: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2SMSC_FEAT_NOTHING;
end;

function TConstants.Get_SMPP_ESM_2SMSC_FEAT_UDHI: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2SMSC_FEAT_UDHI;
end;

function TConstants.Get_SMPP_ESM_2SMSC_FEAT_SRP: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2SMSC_FEAT_SRP;
end;

function TConstants.Get_SMPP_USEGSMENCODING_DISABLED: Integer;
begin
    Result := DefaultInterface.SMPP_USEGSMENCODING_DISABLED;
end;

function TConstants.Get_SMPP_USEGSMENCODING_INANDOUT: Integer;
begin
    Result := DefaultInterface.SMPP_USEGSMENCODING_INANDOUT;
end;

function TConstants.Get_SMPP_USEGSMENCODING_INCOMING: Integer;
begin
    Result := DefaultInterface.SMPP_USEGSMENCODING_INCOMING;
end;

function TConstants.Get_SMPP_USEGSMENCODING_OUTGOING: Integer;
begin
    Result := DefaultInterface.SMPP_USEGSMENCODING_OUTGOING;
end;

function TConstants.Get_SMPP_DATACODING_ASCII: Integer;
begin
    Result := DefaultInterface.SMPP_DATACODING_ASCII;
end;

function TConstants.Get_SMPP_DATACODING_OCTET_UNSPEC: Integer;
begin
    Result := DefaultInterface.SMPP_DATACODING_OCTET_UNSPEC;
end;

function TConstants.Get_SMPP_DATACODING_LATIN: Integer;
begin
    Result := DefaultInterface.SMPP_DATACODING_LATIN;
end;

function TConstants.Get_SMPP_DATACODING_JIS_KANJI: Integer;
begin
    Result := DefaultInterface.SMPP_DATACODING_JIS_KANJI;
end;

function TConstants.Get_SMPP_DATACODING_CYRILLIC: Integer;
begin
    Result := DefaultInterface.SMPP_DATACODING_CYRILLIC;
end;

function TConstants.Get_SMPP_DATACODING_LATIN_HEBREW: Integer;
begin
    Result := DefaultInterface.SMPP_DATACODING_LATIN_HEBREW;
end;

function TConstants.Get_SMPP_DATACODING_PICTOGRAM: Integer;
begin
    Result := DefaultInterface.SMPP_DATACODING_PICTOGRAM;
end;

function TConstants.Get_SMPP_DATACODING_ISO_2022_JP: Integer;
begin
    Result := DefaultInterface.SMPP_DATACODING_ISO_2022_JP;
end;

function TConstants.Get_SMPP_DATACODING_EXTENDED_KANJI_JIS: Integer;
begin
    Result := DefaultInterface.SMPP_DATACODING_EXTENDED_KANJI_JIS;
end;

function TConstants.Get_SMPP_DATACODING_KS_C_5601: Integer;
begin
    Result := DefaultInterface.SMPP_DATACODING_KS_C_5601;
end;

function TConstants.Get_SMPP_PRIORITYFLAG_BULK: Integer;
begin
    Result := DefaultInterface.SMPP_PRIORITYFLAG_BULK;
end;

function TConstants.Get_SMPP_PRIORITYFLAG_NORMAL: Integer;
begin
    Result := DefaultInterface.SMPP_PRIORITYFLAG_NORMAL;
end;

function TConstants.Get_SMPP_PRIORITYFLAG_URGENT: Integer;
begin
    Result := DefaultInterface.SMPP_PRIORITYFLAG_URGENT;
end;

function TConstants.Get_SMPP_PRIORITYFLAG_VERY_URGENT: Integer;
begin
    Result := DefaultInterface.SMPP_PRIORITYFLAG_VERY_URGENT;
end;

function TConstants.Get_SMPP_MESSAGESTATE_AX_WAITRESP: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_AX_WAITRESP;
end;

function TConstants.Get_SMPP_MESSAGESTATE_ENROUTE: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_ENROUTE;
end;

function TConstants.Get_SMPP_MESSAGESTATE_DELIVERED: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_DELIVERED;
end;

function TConstants.Get_SMPP_MESSAGESTATE_EXPIRED: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_EXPIRED;
end;

function TConstants.Get_SMPP_MESSAGESTATE_DELETED: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_DELETED;
end;

function TConstants.Get_SMPP_MESSAGESTATE_UNDELIVERABLE: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_UNDELIVERABLE;
end;

function TConstants.Get_SMPP_MESSAGESTATE_ACCEPTED: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_ACCEPTED;
end;

function TConstants.Get_SMPP_MESSAGESTATE_UNKNOWN: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_UNKNOWN;
end;

function TConstants.Get_SMPP_MESSAGESTATE_REJECTED: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_REJECTED;
end;

function TConstants.Get_SMPP_MESSAGESTATE_AX_RESPERROR: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_AX_RESPERROR;
end;

function TConstants.Get_SMPP_MESSAGESTATE_AX_NOCREDITS: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_AX_NOCREDITS;
end;

function TConstants.Get_SMPP_MESSAGESTATE_AX_RESPTO: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_AX_RESPTO;
end;

function TConstants.Get_SMPP_MESSAGESTATE_AX_RESPONDED: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_AX_RESPONDED;
end;

function TConstants.Get_SMPP_SESSIONSTATE_CONNECTED: Integer;
begin
    Result := DefaultInterface.SMPP_SESSIONSTATE_CONNECTED;
end;

function TConstants.Get_SMPP_SESSIONSTATE_DISCONNECTED: Integer;
begin
    Result := DefaultInterface.SMPP_SESSIONSTATE_DISCONNECTED;
end;

function TConstants.Get_SMPP_SESSIONSTATE_BINDING: Integer;
begin
    Result := DefaultInterface.SMPP_SESSIONSTATE_BINDING;
end;

function TConstants.Get_SMPP_SESSIONSTATE_BOUND_TX: Integer;
begin
    Result := DefaultInterface.SMPP_SESSIONSTATE_BOUND_TX;
end;

function TConstants.Get_SMPP_SESSIONSTATE_BOUND_RX: Integer;
begin
    Result := DefaultInterface.SMPP_SESSIONSTATE_BOUND_RX;
end;

function TConstants.Get_SMPP_SESSIONSTATE_BOUND_TRX: Integer;
begin
    Result := DefaultInterface.SMPP_SESSIONSTATE_BOUND_TRX;
end;

function TConstants.Get_SMPP_TLV_DEST_ADDR_SUBUNIT: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_DEST_ADDR_SUBUNIT;
end;

function TConstants.Get_SMPP_TLV_DEST_NETWORK_TYPE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_DEST_NETWORK_TYPE;
end;

function TConstants.Get_SMPP_TLV_DEST_BEARER_TYPE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_DEST_BEARER_TYPE;
end;

function TConstants.Get_SMPP_TLV_DEST_TELEMATICS_ID: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_DEST_TELEMATICS_ID;
end;

function TConstants.Get_SMPP_TLV_SOURCE_ADDR_SUBUNIT: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SOURCE_ADDR_SUBUNIT;
end;

function TConstants.Get_SMPP_TLV_SOURCE_NETWORK_TYPE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SOURCE_NETWORK_TYPE;
end;

function TConstants.Get_SMPP_TLV_SOURCE_BEARER_TYPE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SOURCE_BEARER_TYPE;
end;

function TConstants.Get_SMPP_TLV_SOURCE_TELEMATICS_ID: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SOURCE_TELEMATICS_ID;
end;

function TConstants.Get_SMPP_TLV_QOS_TIME_TO_LIVE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_QOS_TIME_TO_LIVE;
end;

function TConstants.Get_SMPP_TLV_PAYLOAD_TYPE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_PAYLOAD_TYPE;
end;

function TConstants.Get_SMPP_TLV_ADDITIONAL_STATUS_INFO_TEXT: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_ADDITIONAL_STATUS_INFO_TEXT;
end;

function TConstants.Get_SMPP_TLV_RECEIPTED_MESSAGE_ID: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_RECEIPTED_MESSAGE_ID;
end;

function TConstants.Get_SMPP_TLV_MS_MSG_WAIT_FACILITIES: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_MS_MSG_WAIT_FACILITIES;
end;

function TConstants.Get_SMPP_TLV_PRIVACY_INDICATOR: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_PRIVACY_INDICATOR;
end;

function TConstants.Get_SMPP_TLV_SOURCE_SUBADDRESS: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SOURCE_SUBADDRESS;
end;

function TConstants.Get_SMPP_TLV_DEST_SUBADDRESS: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_DEST_SUBADDRESS;
end;

function TConstants.Get_SMPP_TLV_USER_MESSAGE_REFERENCE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_USER_MESSAGE_REFERENCE;
end;

function TConstants.Get_SMPP_TLV_USER_RESPONSE_CODE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_USER_RESPONSE_CODE;
end;

function TConstants.Get_SMPP_TLV_SOURCE_PORT: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SOURCE_PORT;
end;

function TConstants.Get_SMPP_TLV_DESTINATION_PORT: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_DESTINATION_PORT;
end;

function TConstants.Get_SMPP_TLV_SAR_MSG_REF_NUM: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SAR_MSG_REF_NUM;
end;

function TConstants.Get_SMPP_TLV_LANGUAGE_INDICATOR: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_LANGUAGE_INDICATOR;
end;

function TConstants.Get_SMPP_TLV_SAR_TOTAL_SEGMENTS: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SAR_TOTAL_SEGMENTS;
end;

function TConstants.Get_SMPP_TLV_SAR_SEGMENT_SEQNUM: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SAR_SEGMENT_SEQNUM;
end;

function TConstants.Get_SMPP_TLV_SC_INTERFACE_VERSION: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SC_INTERFACE_VERSION;
end;

function TConstants.Get_SMPP_TLV_CALLBACK_NUM_PRES_IND: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_CALLBACK_NUM_PRES_IND;
end;

function TConstants.Get_SMPP_TLV_CALLBACK_NUM_ATAG: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_CALLBACK_NUM_ATAG;
end;

function TConstants.Get_SMPP_TLV_NUMBER_OF_MESSAGES: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_NUMBER_OF_MESSAGES;
end;

function TConstants.Get_SMPP_TLV_CALLBACK_NUM: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_CALLBACK_NUM;
end;

function TConstants.Get_SMPP_TLV_DPF_RESULT: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_DPF_RESULT;
end;

function TConstants.Get_SMPP_TLV_SET_DPF: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SET_DPF;
end;

function TConstants.Get_SMPP_TLV_MS_AVAILABILITY_STATUS: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_MS_AVAILABILITY_STATUS;
end;

function TConstants.Get_SMPP_TLV_NETWORK_ERROR_CODE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_NETWORK_ERROR_CODE;
end;

function TConstants.Get_SMPP_TLV_MESSAGE_PAYLOAD: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_MESSAGE_PAYLOAD;
end;

function TConstants.Get_SMPP_TLV_DELIVERY_FAILURE_REASON: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_DELIVERY_FAILURE_REASON;
end;

function TConstants.Get_SMPP_TLV_MORE_MESSAGES_TO_SEND: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_MORE_MESSAGES_TO_SEND;
end;

function TConstants.Get_SMPP_TLV_MESSAGE_STATE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_MESSAGE_STATE;
end;

function TConstants.Get_SMPP_TLV_CONGESTION_STATE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_CONGESTION_STATE;
end;

function TConstants.Get_SMPP_TLV_USSD_SERVICE_OP: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_USSD_SERVICE_OP;
end;

function TConstants.Get_SMPP_TLV_DISPLAY_TIME: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_DISPLAY_TIME;
end;

function TConstants.Get_SMPP_TLV_SMS_SIGNAL: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SMS_SIGNAL;
end;

function TConstants.Get_SMPP_TLV_MS_VALIDITY: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_MS_VALIDITY;
end;

function TConstants.Get_SMPP_TLV_ALERT_ON_MESSAGE_DELIVERY: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_ALERT_ON_MESSAGE_DELIVERY;
end;

function TConstants.Get_SMPP_TLV_ITS_REPLY_TYPE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_ITS_REPLY_TYPE;
end;

function TConstants.Get_SMPP_TLV_ITS_SESSION_INFO: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_ITS_SESSION_INFO;
end;

function TConstants.Get_SMPP_MULTIPARTMODE_UDH: Integer;
begin
    Result := DefaultInterface.SMPP_MULTIPARTMODE_UDH;
end;

function TConstants.Get_SMPP_MULTIPARTMODE_UDH16BIT: Integer;
begin
    Result := DefaultInterface.SMPP_MULTIPARTMODE_UDH16BIT;
end;

function TConstants.Get_SMPP_MULTIPARTMODE_SARTLV: Integer;
begin
    Result := DefaultInterface.SMPP_MULTIPARTMODE_SARTLV;
end;

function TConstants.Get_SMPP_MULTIPARTMODE_PAYLOADTLV: Integer;
begin
    Result := DefaultInterface.SMPP_MULTIPARTMODE_PAYLOADTLV;
end;

function TConstants.Get_SMPP_SUBMITMODE_SUBMITSM: Integer;
begin
    Result := DefaultInterface.SMPP_SUBMITMODE_SUBMITSM;
end;

function TConstants.Get_SMPP_SUBMITMODE_DATASM: Integer;
begin
    Result := DefaultInterface.SMPP_SUBMITMODE_DATASM;
end;

function TConstants.Get_SMPP_USEGSMENCODING_INCHARSET: Integer;
begin
    Result := DefaultInterface.SMPP_USEGSMENCODING_INCHARSET;
end;

function TConstants.Get_SMPP_USEGSMENCODING_OUTCHARSET: Integer;
begin
    Result := DefaultInterface.SMPP_USEGSMENCODING_OUTCHARSET;
end;

function TConstants.Get_SMPP_USEGSMENCODING_INOUTCHARS: Integer;
begin
    Result := DefaultInterface.SMPP_USEGSMENCODING_INOUTCHARS;
end;

function TConstants.Get_SMPP_IPVERSION_4: Integer;
begin
    Result := DefaultInterface.SMPP_IPVERSION_4;
end;

function TConstants.Get_SMPP_IPVERSION_6: Integer;
begin
    Result := DefaultInterface.SMPP_IPVERSION_6;
end;

function TConstants.Get_SMPP_IPVERSION_BOTH: Integer;
begin
    Result := DefaultInterface.SMPP_IPVERSION_BOTH;
end;

function TConstants.Get_SMPP_ESME_ROK: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_ROK;
end;

function TConstants.Get_SMPP_ESME_RINVMSGLEN: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVMSGLEN;
end;

function TConstants.Get_SMPP_ESME_RINVCMDLEN: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVCMDLEN;
end;

function TConstants.Get_SMPP_ESME_RINVCMDID: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVCMDID;
end;

function TConstants.Get_SMPP_ESME_RINVBNDSTS: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVBNDSTS;
end;

function TConstants.Get_SMPP_ESME_RALYBND: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RALYBND;
end;

function TConstants.Get_SMPP_ESME_RINVPRTFLG: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVPRTFLG;
end;

function TConstants.Get_SMPP_ESME_RINVREGDLVFLG: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVREGDLVFLG;
end;

function TConstants.Get_SMPP_ESME_RSYSERR: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RSYSERR;
end;

function TConstants.Get_SMPP_ESME_RINVSRCADR: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVSRCADR;
end;

function TConstants.Get_SMPP_ESME_RINVDSTADR: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVDSTADR;
end;

function TConstants.Get_SMPP_ESME_RINVMSGID: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVMSGID;
end;

function TConstants.Get_SMPP_ESME_RBINDFAIL: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RBINDFAIL;
end;

function TConstants.Get_SMPP_ESME_RINVPASWD: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVPASWD;
end;

function TConstants.Get_SMPP_ESME_RINVSYSID: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVSYSID;
end;

function TConstants.Get_SMPP_ESME_RCANCELFAIL: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RCANCELFAIL;
end;

function TConstants.Get_SMPP_ESME_RREPLACEFAIL: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RREPLACEFAIL;
end;

function TConstants.Get_SMPP_ESME_RMSGQFUL: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RMSGQFUL;
end;

function TConstants.Get_SMPP_ESME_RINVSERTYP: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVSERTYP;
end;

function TConstants.Get_SMPP_ESME_RINVNUMDESTS: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVNUMDESTS;
end;

function TConstants.Get_SMPP_ESME_RINVDLNAME: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVDLNAME;
end;

function TConstants.Get_SMPP_ESME_RINVDESTFLAG: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVDESTFLAG;
end;

function TConstants.Get_SMPP_ESME_RINVSUBREP: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVSUBREP;
end;

function TConstants.Get_SMPP_ESME_RINVESMCLASS: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVESMCLASS;
end;

function TConstants.Get_SMPP_ESME_RCNTSUBDL: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RCNTSUBDL;
end;

function TConstants.Get_SMPP_ESME_RSUBMITFAIL: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RSUBMITFAIL;
end;

function TConstants.Get_SMPP_ESME_RINVSRCTON: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVSRCTON;
end;

function TConstants.Get_SMPP_ESME_RINVSRCNPI: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVSRCNPI;
end;

function TConstants.Get_SMPP_ESME_RINVDSTTON: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVDSTTON;
end;

function TConstants.Get_SMPP_ESME_RINVDSTNPI: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVDSTNPI;
end;

function TConstants.Get_SMPP_ESME_RINVSYSTYP: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVSYSTYP;
end;

function TConstants.Get_SMPP_ESME_RINVREPFLAG: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVREPFLAG;
end;

function TConstants.Get_SMPP_ESME_RINVNUMMSGS: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVNUMMSGS;
end;

function TConstants.Get_SMPP_ESME_RTHROTTLED: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RTHROTTLED;
end;

function TConstants.Get_SMPP_ESME_RINVSCHED: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVSCHED;
end;

function TConstants.Get_SMPP_ESME_RINVEXPIRY: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVEXPIRY;
end;

function TConstants.Get_SMPP_ESME_RINVDFTMSGID: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVDFTMSGID;
end;

function TConstants.Get_SMPP_ESME_RX_T_APPN: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RX_T_APPN;
end;

function TConstants.Get_SMPP_ESME_RX_P_APPN: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RX_P_APPN;
end;

function TConstants.Get_SMPP_ESME_RX_R_APPN: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RX_R_APPN;
end;

function TConstants.Get_SMPP_ESME_RQUERYFAIL: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RQUERYFAIL;
end;

function TConstants.Get_SMPP_ESME_RINVOPTPARSTREAM: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVOPTPARSTREAM;
end;

function TConstants.Get_SMPP_ESME_ROPTPARNOTALLWD: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_ROPTPARNOTALLWD;
end;

function TConstants.Get_SMPP_ESME_RINVPARLEN: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVPARLEN;
end;

function TConstants.Get_SMPP_ESME_RMISSINGOPTPARAM: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RMISSINGOPTPARAM;
end;

function TConstants.Get_SMPP_ESME_RINVOPTPARAMVAL: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVOPTPARAMVAL;
end;

function TConstants.Get_SMPP_ESME_RDELIVERYFAILURE: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RDELIVERYFAILURE;
end;

function TConstants.Get_SMPP_ESME_RUNKNOWNERR: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RUNKNOWNERR;
end;

function TConstants.Get_GSM_STATUS_MESSAGE_DELIVERED_SUCCESSFULLY: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_MESSAGE_DELIVERED_SUCCESSFULLY;
end;

function TConstants.Get_GSM_STATUS_FORWARDED_STATUS_UNKNOWN: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_FORWARDED_STATUS_UNKNOWN;
end;

function TConstants.Get_GSM_STATUS_REPLACED: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_REPLACED;
end;

function TConstants.Get_GSM_STATUS_CONGESTION_STILL_TRYING: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_CONGESTION_STILL_TRYING;
end;

function TConstants.Get_GSM_STATUS_RECIPIENT_BUSY_STILL_TRYING: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_RECIPIENT_BUSY_STILL_TRYING;
end;

function TConstants.Get_GSM_STATUS_NO_RESPONSE_STILL_TRYING: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_NO_RESPONSE_STILL_TRYING;
end;

function TConstants.Get_GSM_STATUS_SERVICE_REJECTED_STILL_TRYING: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_SERVICE_REJECTED_STILL_TRYING;
end;

function TConstants.Get_GSM_STATUS_QOS_NOT_AVAILABLE_STILL_TRYING: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_QOS_NOT_AVAILABLE_STILL_TRYING;
end;

function TConstants.Get_GSM_STATUS_RECIPIENT_ERROR_STILL_TRYING: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_RECIPIENT_ERROR_STILL_TRYING;
end;

function TConstants.Get_GSM_STATUS_RPC_ERROR: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_RPC_ERROR;
end;

function TConstants.Get_GSM_STATUS_INCOMPATIBLE_DESTINATION: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_INCOMPATIBLE_DESTINATION;
end;

function TConstants.Get_GSM_STATUS_CONNECTION_REJECTED: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_CONNECTION_REJECTED;
end;

function TConstants.Get_GSM_STATUS_NOT_OBTAINABLE: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_NOT_OBTAINABLE;
end;

function TConstants.Get_GSM_STATUS_QOS_NOT_AVAILABLE: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_QOS_NOT_AVAILABLE;
end;

function TConstants.Get_GSM_STATUS_NO_INTERNETWORKING_AVAILABLE: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_NO_INTERNETWORKING_AVAILABLE;
end;

function TConstants.Get_GSM_STATUS_MESSAGE_EXPIRED: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_MESSAGE_EXPIRED;
end;

function TConstants.Get_GSM_STATUS_MESSAGE_DELETED_BY_SENDER: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_MESSAGE_DELETED_BY_SENDER;
end;

function TConstants.Get_GSM_STATUS_MESSAGE_DELETED_BY_SMSC: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_MESSAGE_DELETED_BY_SMSC;
end;

function TConstants.Get_GSM_STATUS_DOES_NOT_EXIST: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_DOES_NOT_EXIST;
end;

function TConstants.Get_GSM_STORAGETYPE_SIM: Integer;
begin
    Result := DefaultInterface.GSM_STORAGETYPE_SIM;
end;

function TConstants.Get_GSM_STORAGETYPE_MEMORY: Integer;
begin
    Result := DefaultInterface.GSM_STORAGETYPE_MEMORY;
end;

function TConstants.Get_GSM_STORAGETYPE_COMBINED: Integer;
begin
    Result := DefaultInterface.GSM_STORAGETYPE_COMBINED;
end;

function TConstants.Get_GSM_STORAGETYPE_STATUS: Integer;
begin
    Result := DefaultInterface.GSM_STORAGETYPE_STATUS;
end;

function TConstants.Get_GSM_STORAGETYPE_ALL: Integer;
begin
    Result := DefaultInterface.GSM_STORAGETYPE_ALL;
end;

function TConstants.Get_GSM_FO_REPLYPATH_EXISTS: Integer;
begin
    Result := DefaultInterface.GSM_FO_REPLYPATH_EXISTS;
end;

function TConstants.Get_GSM_FO_UDHI: Integer;
begin
    Result := DefaultInterface.GSM_FO_UDHI;
end;

function TConstants.Get_GSM_FO_STATUS_REPORT: Integer;
begin
    Result := DefaultInterface.GSM_FO_STATUS_REPORT;
end;

function TConstants.Get_GSM_FO_VALIDITY_NONE: Integer;
begin
    Result := DefaultInterface.GSM_FO_VALIDITY_NONE;
end;

function TConstants.Get_GSM_FO_VALIDITY_RELATIVE: Integer;
begin
    Result := DefaultInterface.GSM_FO_VALIDITY_RELATIVE;
end;

function TConstants.Get_GSM_FO_VALIDITY_ENHANCED: Integer;
begin
    Result := DefaultInterface.GSM_FO_VALIDITY_ENHANCED;
end;

function TConstants.Get_GSM_FO_VALIDITY_ABSOLUTE: Integer;
begin
    Result := DefaultInterface.GSM_FO_VALIDITY_ABSOLUTE;
end;

function TConstants.Get_GSM_FO_REJECT_DUPLICATES: Integer;
begin
    Result := DefaultInterface.GSM_FO_REJECT_DUPLICATES;
end;

function TConstants.Get_GSM_FO_SUBMIT_SM: Integer;
begin
    Result := DefaultInterface.GSM_FO_SUBMIT_SM;
end;

function TConstants.Get_GSM_FO_DELIVER_SM: Integer;
begin
    Result := DefaultInterface.GSM_FO_DELIVER_SM;
end;

function TConstants.Get_GSM_FO_STATUS_SM: Integer;
begin
    Result := DefaultInterface.GSM_FO_STATUS_SM;
end;

function TConstants.Get_DATACODING_DEFAULT: Integer;
begin
    Result := DefaultInterface.DATACODING_DEFAULT;
end;

function TConstants.Get_DATACODING_8BIT_DATA: Integer;
begin
    Result := DefaultInterface.DATACODING_8BIT_DATA;
end;

function TConstants.Get_DATACODING_UNICODE: Integer;
begin
    Result := DefaultInterface.DATACODING_UNICODE;
end;

function TConstants.Get_GSM_DATACODING_ME_SPECIFIC: Integer;
begin
    Result := DefaultInterface.GSM_DATACODING_ME_SPECIFIC;
end;

function TConstants.Get_GSM_DATACODING_SIM_SPECIFIC: Integer;
begin
    Result := DefaultInterface.GSM_DATACODING_SIM_SPECIFIC;
end;

function TConstants.Get_GSM_DATACODING_TE_SPECIFIC: Integer;
begin
    Result := DefaultInterface.GSM_DATACODING_TE_SPECIFIC;
end;

function TConstants.Get_DATACODING_FLASH: Integer;
begin
    Result := DefaultInterface.DATACODING_FLASH;
end;

function TConstants.Get_GSM_BAUDRATE_110: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_110;
end;

function TConstants.Get_GSM_BAUDRATE_300: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_300;
end;

function TConstants.Get_GSM_BAUDRATE_600: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_600;
end;

function TConstants.Get_GSM_BAUDRATE_1200: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_1200;
end;

function TConstants.Get_GSM_BAUDRATE_2400: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_2400;
end;

function TConstants.Get_GSM_BAUDRATE_4800: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_4800;
end;

function TConstants.Get_GSM_BAUDRATE_9600: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_9600;
end;

function TConstants.Get_GSM_BAUDRATE_14400: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_14400;
end;

function TConstants.Get_GSM_BAUDRATE_19200: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_19200;
end;

function TConstants.Get_GSM_BAUDRATE_38400: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_38400;
end;

function TConstants.Get_GSM_BAUDRATE_56000: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_56000;
end;

function TConstants.Get_GSM_BAUDRATE_57600: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_57600;
end;

function TConstants.Get_GSM_BAUDRATE_64000: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_64000;
end;

function TConstants.Get_GSM_BAUDRATE_115200: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_115200;
end;

function TConstants.Get_GSM_BAUDRATE_128000: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_128000;
end;

function TConstants.Get_GSM_BAUDRATE_230400: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_230400;
end;

function TConstants.Get_GSM_BAUDRATE_256000: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_256000;
end;

function TConstants.Get_GSM_BAUDRATE_460800: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_460800;
end;

function TConstants.Get_GSM_BAUDRATE_921600: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_921600;
end;

function TConstants.Get_GSM_BAUDRATE_DEFAULT: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_DEFAULT;
end;

function TConstants.Get_GSM_MESSAGESTATE_RECEIVED_UNREAD: Integer;
begin
    Result := DefaultInterface.GSM_MESSAGESTATE_RECEIVED_UNREAD;
end;

function TConstants.Get_GSM_MESSAGESTATE_RECEIVED_READ: Integer;
begin
    Result := DefaultInterface.GSM_MESSAGESTATE_RECEIVED_READ;
end;

function TConstants.Get_GSM_MESSAGESTATE_STORED_UNSENT: Integer;
begin
    Result := DefaultInterface.GSM_MESSAGESTATE_STORED_UNSENT;
end;

function TConstants.Get_GSM_MESSAGESTATE_STORED_SENT: Integer;
begin
    Result := DefaultInterface.GSM_MESSAGESTATE_STORED_SENT;
end;

function TConstants.Get_GSM_MESSAGESTATE_ALL: Integer;
begin
    Result := DefaultInterface.GSM_MESSAGESTATE_ALL;
end;

function TConstants.Get_GSM_MESSAGEFORMAT_PDU: Integer;
begin
    Result := DefaultInterface.GSM_MESSAGEFORMAT_PDU;
end;

function TConstants.Get_GSM_MESSAGEFORMAT_TEXT: Integer;
begin
    Result := DefaultInterface.GSM_MESSAGEFORMAT_TEXT;
end;

function TConstants.Get_GSM_MESSAGEFORMAT_AUTO: Integer;
begin
    Result := DefaultInterface.GSM_MESSAGEFORMAT_AUTO;
end;

function TConstants.Get_HTTP_PLACEHOLDER_USERTAG: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_USERTAG;
end;

function TConstants.Get_HTTP_PLACEHOLDER_TOADDRESS: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_TOADDRESS;
end;

function TConstants.Get_HTTP_PLACEHOLDER_FROMADDRESS: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_FROMADDRESS;
end;

function TConstants.Get_HTTP_PLACEHOLDER_BODY: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_BODY;
end;

function TConstants.Get_HTTP_PLACEHOLDER_BODYASHEX: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_BODYASHEX;
end;

function TConstants.Get_HTTP_PLACEHOLDER_BODYASBASE64: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_BODYASBASE64;
end;

function TConstants.Get_HTTP_PLACEHOLDER_DELIVERYREPORT: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_DELIVERYREPORT;
end;

function TConstants.Get_HTTP_PLACEHOLDER_TOADDRESSTON: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_TOADDRESSTON;
end;

function TConstants.Get_HTTP_PLACEHOLDER_TOADDRESSNPI: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_TOADDRESSNPI;
end;

function TConstants.Get_HTTP_PLACEHOLDER_FROMADDRESSTON: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_FROMADDRESSTON;
end;

function TConstants.Get_HTTP_PLACEHOLDER_FROMADDRESSNPI: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_FROMADDRESSNPI;
end;

function TConstants.Get_HTTP_PLACEHOLDER_PROTOCOLID: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_PROTOCOLID;
end;

function TConstants.Get_HTTP_PLACEHOLDER_UDHI: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_UDHI;
end;

function TConstants.Get_HTTP_PLACEHOLDER_DATACODING: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_DATACODING;
end;

function TConstants.Get_GSM_PREFIXSMSC_ENABLED: Integer;
begin
    Result := DefaultInterface.GSM_PREFIXSMSC_ENABLED;
end;

function TConstants.Get_GSM_PREFIXSMSC_DISABLED: Integer;
begin
    Result := DefaultInterface.GSM_PREFIXSMSC_DISABLED;
end;

function TConstants.Get_GSM_PREFIXSMSC_AUTO: Integer;
begin
    Result := DefaultInterface.GSM_PREFIXSMSC_AUTO;
end;

function TConstants.Get_WAPPUSH_SIGNAL_NONE: Integer;
begin
    Result := DefaultInterface.WAPPUSH_SIGNAL_NONE;
end;

function TConstants.Get_WAPPUSH_SIGNAL_LOW: Integer;
begin
    Result := DefaultInterface.WAPPUSH_SIGNAL_LOW;
end;

function TConstants.Get_WAPPUSH_SIGNAL_MEDIUM: Integer;
begin
    Result := DefaultInterface.WAPPUSH_SIGNAL_MEDIUM;
end;

function TConstants.Get_WAPPUSH_SIGNAL_HIGH: Integer;
begin
    Result := DefaultInterface.WAPPUSH_SIGNAL_HIGH;
end;

function TConstants.Get_WAPPUSH_SIGNAL_DELETE: Integer;
begin
    Result := DefaultInterface.WAPPUSH_SIGNAL_DELETE;
end;

function TConstants.Get_DIALUP_PROVIDERTYPE_UCP: Integer;
begin
    Result := DefaultInterface.DIALUP_PROVIDERTYPE_UCP;
end;

function TConstants.Get_DIALUP_PROVIDERTYPE_TAP_DEFAULT: Integer;
begin
    Result := DefaultInterface.DIALUP_PROVIDERTYPE_TAP_DEFAULT;
end;

function TConstants.Get_DIALUP_PROVIDERTYPE_TAP_NOLF: Integer;
begin
    Result := DefaultInterface.DIALUP_PROVIDERTYPE_TAP_NOLF;
end;

function TConstants.Get_DIALUP_PROVIDERTYPE_TAP_NOEOT: Integer;
begin
    Result := DefaultInterface.DIALUP_PROVIDERTYPE_TAP_NOEOT;
end;

function TConstants.Get_DIALUP_DIALMODE_TONE: Integer;
begin
    Result := DefaultInterface.DIALUP_DIALMODE_TONE;
end;

function TConstants.Get_DIALUP_DIALMODE_PULSE: Integer;
begin
    Result := DefaultInterface.DIALUP_DIALMODE_PULSE;
end;

function TConstants.Get_DIALUP_DEVICESETTINGS_DEFAULT: Integer;
begin
    Result := DefaultInterface.DIALUP_DEVICESETTINGS_DEFAULT;
end;

function TConstants.Get_DIALUP_DEVICESETTINGS_8N1: Integer;
begin
    Result := DefaultInterface.DIALUP_DEVICESETTINGS_8N1;
end;

function TConstants.Get_DIALUP_DEVICESETTINGS_7E1: Integer;
begin
    Result := DefaultInterface.DIALUP_DEVICESETTINGS_7E1;
end;

function TConstants.SmppBindToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.SmppBindToString(lVal);
end;

function TConstants.SmppVersionToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.SmppVersionToString(lVal);
end;

function TConstants.TonToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.TonToString(lVal);
end;

function TConstants.NpiToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.NpiToString(lVal);
end;

function TConstants.MultipartToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.MultipartToString(lVal);
end;

function TConstants.BodyformatToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.BodyformatToString(lVal);
end;

function TConstants.SmppEsm2SmscToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.SmppEsm2SmscToString(lVal);
end;

function TConstants.SmppEsm2EsmeToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.SmppEsm2EsmeToString(lVal);
end;

function TConstants.SmppUseGsmEncodingToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.SmppUseGsmEncodingToString(lVal);
end;

function TConstants.SmppDataCodingToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.SmppDataCodingToString(lVal);
end;

function TConstants.SmppPriorityFlagToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.SmppPriorityFlagToString(lVal);
end;

function TConstants.SmppMessageStateToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.SmppMessageStateToString(lVal);
end;

function TConstants.SmppSessionStateToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.SmppSessionStateToString(lVal);
end;

function TConstants.SmppTlvToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.SmppTlvToString(lVal);
end;

function TConstants.SmppMultipartModeToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.SmppMultipartModeToString(lVal);
end;

function TConstants.SmppSubmitModeToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.SmppSubmitModeToString(lVal);
end;

function TConstants.SmppEsmeToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.SmppEsmeToString(lVal);
end;

function TConstants.GsmStatusToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.GsmStatusToString(lVal);
end;

function TConstants.GsmStorageTypeToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.GsmStorageTypeToString(lVal);
end;

function TConstants.GsmFoToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.GsmFoToString(lVal);
end;

function TConstants.GsmDataCodingToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.GsmDataCodingToString(lVal);
end;

function TConstants.GsmBaudrateToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.GsmBaudrateToString(lVal);
end;

function TConstants.GsmMessageStateToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.GsmMessageStateToString(lVal);
end;

function TConstants.GsmMessageFormatToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.GsmMessageFormatToString(lVal);
end;

function TConstants.GsmPrefixSmscToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.GsmPrefixSmscToString(lVal);
end;

function TConstants.WapPushSignalToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.WapPushSignalToString(lVal);
end;

function TConstants.DialupProviderTypeToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.DialupProviderTypeToString(lVal);
end;

function TConstants.DialupDialModeToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.DialupDialModeToString(lVal);
end;

function TConstants.DialupDeviceSettingsToString(lVal: Integer): WideString;
begin
  Result := DefaultInterface.DialupDeviceSettingsToString(lVal);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TConstantsProperties.Create(AServer: TConstants);
begin
  inherited Create;
  FServer := AServer;
end;

function TConstantsProperties.GetDefaultInterface: IConstants;
begin
  Result := FServer.DefaultInterface;
end;

function TConstantsProperties.Get_SMPP_BIND_TRANSMITTER: Integer;
begin
    Result := DefaultInterface.SMPP_BIND_TRANSMITTER;
end;

function TConstantsProperties.Get_SMPP_BIND_TRANSCEIVER: Integer;
begin
    Result := DefaultInterface.SMPP_BIND_TRANSCEIVER;
end;

function TConstantsProperties.Get_SMPP_BIND_RECEIVER: Integer;
begin
    Result := DefaultInterface.SMPP_BIND_RECEIVER;
end;

function TConstantsProperties.Get_SMPP_VERSION_33: Integer;
begin
    Result := DefaultInterface.SMPP_VERSION_33;
end;

function TConstantsProperties.Get_SMPP_VERSION_34: Integer;
begin
    Result := DefaultInterface.SMPP_VERSION_34;
end;

function TConstantsProperties.Get_SMPP_VERSION_50: Integer;
begin
    Result := DefaultInterface.SMPP_VERSION_50;
end;

function TConstantsProperties.Get_TON_UNKNOWN: Integer;
begin
    Result := DefaultInterface.TON_UNKNOWN;
end;

function TConstantsProperties.Get_TON_INTERNATIONAL: Integer;
begin
    Result := DefaultInterface.TON_INTERNATIONAL;
end;

function TConstantsProperties.Get_TON_NATIONAL: Integer;
begin
    Result := DefaultInterface.TON_NATIONAL;
end;

function TConstantsProperties.Get_TON_NETWORK_SPECIFIC: Integer;
begin
    Result := DefaultInterface.TON_NETWORK_SPECIFIC;
end;

function TConstantsProperties.Get_TON_SUBSCRIBER_NUMBER: Integer;
begin
    Result := DefaultInterface.TON_SUBSCRIBER_NUMBER;
end;

function TConstantsProperties.Get_TON_ALPHANUMERIC: Integer;
begin
    Result := DefaultInterface.TON_ALPHANUMERIC;
end;

function TConstantsProperties.Get_SMPP_TON_ABBREVIATED: Integer;
begin
    Result := DefaultInterface.SMPP_TON_ABBREVIATED;
end;

function TConstantsProperties.Get_NPI_UNKNOWN: Integer;
begin
    Result := DefaultInterface.NPI_UNKNOWN;
end;

function TConstantsProperties.Get_NPI_ISDN: Integer;
begin
    Result := DefaultInterface.NPI_ISDN;
end;

function TConstantsProperties.Get_NPI_DATA: Integer;
begin
    Result := DefaultInterface.NPI_DATA;
end;

function TConstantsProperties.Get_NPI_TELEX: Integer;
begin
    Result := DefaultInterface.NPI_TELEX;
end;

function TConstantsProperties.Get_NPI_NATIONAL: Integer;
begin
    Result := DefaultInterface.NPI_NATIONAL;
end;

function TConstantsProperties.Get_NPI_PRIVATE: Integer;
begin
    Result := DefaultInterface.NPI_PRIVATE;
end;

function TConstantsProperties.Get_NPI_ERMES: Integer;
begin
    Result := DefaultInterface.NPI_ERMES;
end;

function TConstantsProperties.Get_SMPP_NPI_INTERNET: Integer;
begin
    Result := DefaultInterface.SMPP_NPI_INTERNET;
end;

function TConstantsProperties.Get_NPI_LAND_MOBILE: Integer;
begin
    Result := DefaultInterface.NPI_LAND_MOBILE;
end;

function TConstantsProperties.Get_MULTIPART_ACCEPT: Integer;
begin
    Result := DefaultInterface.MULTIPART_ACCEPT;
end;

function TConstantsProperties.Get_MULTIPART_TRUNCATE: Integer;
begin
    Result := DefaultInterface.MULTIPART_TRUNCATE;
end;

function TConstantsProperties.Get_MULTIPART_REJECT: Integer;
begin
    Result := DefaultInterface.MULTIPART_REJECT;
end;

function TConstantsProperties.Get_BODYFORMAT_TEXT: Integer;
begin
    Result := DefaultInterface.BODYFORMAT_TEXT;
end;

function TConstantsProperties.Get_BODYFORMAT_HEX: Integer;
begin
    Result := DefaultInterface.BODYFORMAT_HEX;
end;

function TConstantsProperties.Get_SMPP_ESM_2ESME_DEFAULT: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2ESME_DEFAULT;
end;

function TConstantsProperties.Get_SMPP_ESM_2ESME_DELIVERY_RECEIPT: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2ESME_DELIVERY_RECEIPT;
end;

function TConstantsProperties.Get_SMPP_ESM_2ESME_DELIVERY_ACK: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2ESME_DELIVERY_ACK;
end;

function TConstantsProperties.Get_SMPP_ESM_2ESME_MANUAL_ACK: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2ESME_MANUAL_ACK;
end;

function TConstantsProperties.Get_SMPP_ESM_2ESME_CONVERSATION_ABORT: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2ESME_CONVERSATION_ABORT;
end;

function TConstantsProperties.Get_SMPP_ESM_2ESME_INTERMEDIATE_DELIVERY_NOTIFY: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2ESME_INTERMEDIATE_DELIVERY_NOTIFY;
end;

function TConstantsProperties.Get_SMPP_ESM_2SMSC_MODE_DEFAULT: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2SMSC_MODE_DEFAULT;
end;

function TConstantsProperties.Get_SMPP_ESM_2SMSC_MODE_STOREFORWARD: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2SMSC_MODE_STOREFORWARD;
end;

function TConstantsProperties.Get_SMPP_ESM_2SMSC_MODE_DATAGRAM: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2SMSC_MODE_DATAGRAM;
end;

function TConstantsProperties.Get_SMPP_ESM_2SMSC_MODE_FORWARD: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2SMSC_MODE_FORWARD;
end;

function TConstantsProperties.Get_SMPP_ESM_2SMSC_TYPE_DEFAULT: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2SMSC_TYPE_DEFAULT;
end;

function TConstantsProperties.Get_SMPP_ESM_2SMSC_TYPE_DELIVERY_ACK: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2SMSC_TYPE_DELIVERY_ACK;
end;

function TConstantsProperties.Get_SMPP_ESM_2SMSC_TYPE_MANUAL_ACK: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2SMSC_TYPE_MANUAL_ACK;
end;

function TConstantsProperties.Get_SMPP_ESM_2SMSC_FEAT_NOTHING: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2SMSC_FEAT_NOTHING;
end;

function TConstantsProperties.Get_SMPP_ESM_2SMSC_FEAT_UDHI: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2SMSC_FEAT_UDHI;
end;

function TConstantsProperties.Get_SMPP_ESM_2SMSC_FEAT_SRP: Integer;
begin
    Result := DefaultInterface.SMPP_ESM_2SMSC_FEAT_SRP;
end;

function TConstantsProperties.Get_SMPP_USEGSMENCODING_DISABLED: Integer;
begin
    Result := DefaultInterface.SMPP_USEGSMENCODING_DISABLED;
end;

function TConstantsProperties.Get_SMPP_USEGSMENCODING_INANDOUT: Integer;
begin
    Result := DefaultInterface.SMPP_USEGSMENCODING_INANDOUT;
end;

function TConstantsProperties.Get_SMPP_USEGSMENCODING_INCOMING: Integer;
begin
    Result := DefaultInterface.SMPP_USEGSMENCODING_INCOMING;
end;

function TConstantsProperties.Get_SMPP_USEGSMENCODING_OUTGOING: Integer;
begin
    Result := DefaultInterface.SMPP_USEGSMENCODING_OUTGOING;
end;

function TConstantsProperties.Get_SMPP_DATACODING_ASCII: Integer;
begin
    Result := DefaultInterface.SMPP_DATACODING_ASCII;
end;

function TConstantsProperties.Get_SMPP_DATACODING_OCTET_UNSPEC: Integer;
begin
    Result := DefaultInterface.SMPP_DATACODING_OCTET_UNSPEC;
end;

function TConstantsProperties.Get_SMPP_DATACODING_LATIN: Integer;
begin
    Result := DefaultInterface.SMPP_DATACODING_LATIN;
end;

function TConstantsProperties.Get_SMPP_DATACODING_JIS_KANJI: Integer;
begin
    Result := DefaultInterface.SMPP_DATACODING_JIS_KANJI;
end;

function TConstantsProperties.Get_SMPP_DATACODING_CYRILLIC: Integer;
begin
    Result := DefaultInterface.SMPP_DATACODING_CYRILLIC;
end;

function TConstantsProperties.Get_SMPP_DATACODING_LATIN_HEBREW: Integer;
begin
    Result := DefaultInterface.SMPP_DATACODING_LATIN_HEBREW;
end;

function TConstantsProperties.Get_SMPP_DATACODING_PICTOGRAM: Integer;
begin
    Result := DefaultInterface.SMPP_DATACODING_PICTOGRAM;
end;

function TConstantsProperties.Get_SMPP_DATACODING_ISO_2022_JP: Integer;
begin
    Result := DefaultInterface.SMPP_DATACODING_ISO_2022_JP;
end;

function TConstantsProperties.Get_SMPP_DATACODING_EXTENDED_KANJI_JIS: Integer;
begin
    Result := DefaultInterface.SMPP_DATACODING_EXTENDED_KANJI_JIS;
end;

function TConstantsProperties.Get_SMPP_DATACODING_KS_C_5601: Integer;
begin
    Result := DefaultInterface.SMPP_DATACODING_KS_C_5601;
end;

function TConstantsProperties.Get_SMPP_PRIORITYFLAG_BULK: Integer;
begin
    Result := DefaultInterface.SMPP_PRIORITYFLAG_BULK;
end;

function TConstantsProperties.Get_SMPP_PRIORITYFLAG_NORMAL: Integer;
begin
    Result := DefaultInterface.SMPP_PRIORITYFLAG_NORMAL;
end;

function TConstantsProperties.Get_SMPP_PRIORITYFLAG_URGENT: Integer;
begin
    Result := DefaultInterface.SMPP_PRIORITYFLAG_URGENT;
end;

function TConstantsProperties.Get_SMPP_PRIORITYFLAG_VERY_URGENT: Integer;
begin
    Result := DefaultInterface.SMPP_PRIORITYFLAG_VERY_URGENT;
end;

function TConstantsProperties.Get_SMPP_MESSAGESTATE_AX_WAITRESP: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_AX_WAITRESP;
end;

function TConstantsProperties.Get_SMPP_MESSAGESTATE_ENROUTE: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_ENROUTE;
end;

function TConstantsProperties.Get_SMPP_MESSAGESTATE_DELIVERED: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_DELIVERED;
end;

function TConstantsProperties.Get_SMPP_MESSAGESTATE_EXPIRED: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_EXPIRED;
end;

function TConstantsProperties.Get_SMPP_MESSAGESTATE_DELETED: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_DELETED;
end;

function TConstantsProperties.Get_SMPP_MESSAGESTATE_UNDELIVERABLE: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_UNDELIVERABLE;
end;

function TConstantsProperties.Get_SMPP_MESSAGESTATE_ACCEPTED: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_ACCEPTED;
end;

function TConstantsProperties.Get_SMPP_MESSAGESTATE_UNKNOWN: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_UNKNOWN;
end;

function TConstantsProperties.Get_SMPP_MESSAGESTATE_REJECTED: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_REJECTED;
end;

function TConstantsProperties.Get_SMPP_MESSAGESTATE_AX_RESPERROR: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_AX_RESPERROR;
end;

function TConstantsProperties.Get_SMPP_MESSAGESTATE_AX_NOCREDITS: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_AX_NOCREDITS;
end;

function TConstantsProperties.Get_SMPP_MESSAGESTATE_AX_RESPTO: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_AX_RESPTO;
end;

function TConstantsProperties.Get_SMPP_MESSAGESTATE_AX_RESPONDED: Integer;
begin
    Result := DefaultInterface.SMPP_MESSAGESTATE_AX_RESPONDED;
end;

function TConstantsProperties.Get_SMPP_SESSIONSTATE_CONNECTED: Integer;
begin
    Result := DefaultInterface.SMPP_SESSIONSTATE_CONNECTED;
end;

function TConstantsProperties.Get_SMPP_SESSIONSTATE_DISCONNECTED: Integer;
begin
    Result := DefaultInterface.SMPP_SESSIONSTATE_DISCONNECTED;
end;

function TConstantsProperties.Get_SMPP_SESSIONSTATE_BINDING: Integer;
begin
    Result := DefaultInterface.SMPP_SESSIONSTATE_BINDING;
end;

function TConstantsProperties.Get_SMPP_SESSIONSTATE_BOUND_TX: Integer;
begin
    Result := DefaultInterface.SMPP_SESSIONSTATE_BOUND_TX;
end;

function TConstantsProperties.Get_SMPP_SESSIONSTATE_BOUND_RX: Integer;
begin
    Result := DefaultInterface.SMPP_SESSIONSTATE_BOUND_RX;
end;

function TConstantsProperties.Get_SMPP_SESSIONSTATE_BOUND_TRX: Integer;
begin
    Result := DefaultInterface.SMPP_SESSIONSTATE_BOUND_TRX;
end;

function TConstantsProperties.Get_SMPP_TLV_DEST_ADDR_SUBUNIT: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_DEST_ADDR_SUBUNIT;
end;

function TConstantsProperties.Get_SMPP_TLV_DEST_NETWORK_TYPE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_DEST_NETWORK_TYPE;
end;

function TConstantsProperties.Get_SMPP_TLV_DEST_BEARER_TYPE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_DEST_BEARER_TYPE;
end;

function TConstantsProperties.Get_SMPP_TLV_DEST_TELEMATICS_ID: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_DEST_TELEMATICS_ID;
end;

function TConstantsProperties.Get_SMPP_TLV_SOURCE_ADDR_SUBUNIT: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SOURCE_ADDR_SUBUNIT;
end;

function TConstantsProperties.Get_SMPP_TLV_SOURCE_NETWORK_TYPE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SOURCE_NETWORK_TYPE;
end;

function TConstantsProperties.Get_SMPP_TLV_SOURCE_BEARER_TYPE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SOURCE_BEARER_TYPE;
end;

function TConstantsProperties.Get_SMPP_TLV_SOURCE_TELEMATICS_ID: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SOURCE_TELEMATICS_ID;
end;

function TConstantsProperties.Get_SMPP_TLV_QOS_TIME_TO_LIVE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_QOS_TIME_TO_LIVE;
end;

function TConstantsProperties.Get_SMPP_TLV_PAYLOAD_TYPE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_PAYLOAD_TYPE;
end;

function TConstantsProperties.Get_SMPP_TLV_ADDITIONAL_STATUS_INFO_TEXT: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_ADDITIONAL_STATUS_INFO_TEXT;
end;

function TConstantsProperties.Get_SMPP_TLV_RECEIPTED_MESSAGE_ID: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_RECEIPTED_MESSAGE_ID;
end;

function TConstantsProperties.Get_SMPP_TLV_MS_MSG_WAIT_FACILITIES: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_MS_MSG_WAIT_FACILITIES;
end;

function TConstantsProperties.Get_SMPP_TLV_PRIVACY_INDICATOR: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_PRIVACY_INDICATOR;
end;

function TConstantsProperties.Get_SMPP_TLV_SOURCE_SUBADDRESS: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SOURCE_SUBADDRESS;
end;

function TConstantsProperties.Get_SMPP_TLV_DEST_SUBADDRESS: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_DEST_SUBADDRESS;
end;

function TConstantsProperties.Get_SMPP_TLV_USER_MESSAGE_REFERENCE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_USER_MESSAGE_REFERENCE;
end;

function TConstantsProperties.Get_SMPP_TLV_USER_RESPONSE_CODE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_USER_RESPONSE_CODE;
end;

function TConstantsProperties.Get_SMPP_TLV_SOURCE_PORT: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SOURCE_PORT;
end;

function TConstantsProperties.Get_SMPP_TLV_DESTINATION_PORT: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_DESTINATION_PORT;
end;

function TConstantsProperties.Get_SMPP_TLV_SAR_MSG_REF_NUM: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SAR_MSG_REF_NUM;
end;

function TConstantsProperties.Get_SMPP_TLV_LANGUAGE_INDICATOR: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_LANGUAGE_INDICATOR;
end;

function TConstantsProperties.Get_SMPP_TLV_SAR_TOTAL_SEGMENTS: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SAR_TOTAL_SEGMENTS;
end;

function TConstantsProperties.Get_SMPP_TLV_SAR_SEGMENT_SEQNUM: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SAR_SEGMENT_SEQNUM;
end;

function TConstantsProperties.Get_SMPP_TLV_SC_INTERFACE_VERSION: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SC_INTERFACE_VERSION;
end;

function TConstantsProperties.Get_SMPP_TLV_CALLBACK_NUM_PRES_IND: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_CALLBACK_NUM_PRES_IND;
end;

function TConstantsProperties.Get_SMPP_TLV_CALLBACK_NUM_ATAG: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_CALLBACK_NUM_ATAG;
end;

function TConstantsProperties.Get_SMPP_TLV_NUMBER_OF_MESSAGES: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_NUMBER_OF_MESSAGES;
end;

function TConstantsProperties.Get_SMPP_TLV_CALLBACK_NUM: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_CALLBACK_NUM;
end;

function TConstantsProperties.Get_SMPP_TLV_DPF_RESULT: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_DPF_RESULT;
end;

function TConstantsProperties.Get_SMPP_TLV_SET_DPF: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SET_DPF;
end;

function TConstantsProperties.Get_SMPP_TLV_MS_AVAILABILITY_STATUS: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_MS_AVAILABILITY_STATUS;
end;

function TConstantsProperties.Get_SMPP_TLV_NETWORK_ERROR_CODE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_NETWORK_ERROR_CODE;
end;

function TConstantsProperties.Get_SMPP_TLV_MESSAGE_PAYLOAD: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_MESSAGE_PAYLOAD;
end;

function TConstantsProperties.Get_SMPP_TLV_DELIVERY_FAILURE_REASON: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_DELIVERY_FAILURE_REASON;
end;

function TConstantsProperties.Get_SMPP_TLV_MORE_MESSAGES_TO_SEND: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_MORE_MESSAGES_TO_SEND;
end;

function TConstantsProperties.Get_SMPP_TLV_MESSAGE_STATE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_MESSAGE_STATE;
end;

function TConstantsProperties.Get_SMPP_TLV_CONGESTION_STATE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_CONGESTION_STATE;
end;

function TConstantsProperties.Get_SMPP_TLV_USSD_SERVICE_OP: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_USSD_SERVICE_OP;
end;

function TConstantsProperties.Get_SMPP_TLV_DISPLAY_TIME: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_DISPLAY_TIME;
end;

function TConstantsProperties.Get_SMPP_TLV_SMS_SIGNAL: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_SMS_SIGNAL;
end;

function TConstantsProperties.Get_SMPP_TLV_MS_VALIDITY: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_MS_VALIDITY;
end;

function TConstantsProperties.Get_SMPP_TLV_ALERT_ON_MESSAGE_DELIVERY: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_ALERT_ON_MESSAGE_DELIVERY;
end;

function TConstantsProperties.Get_SMPP_TLV_ITS_REPLY_TYPE: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_ITS_REPLY_TYPE;
end;

function TConstantsProperties.Get_SMPP_TLV_ITS_SESSION_INFO: Integer;
begin
    Result := DefaultInterface.SMPP_TLV_ITS_SESSION_INFO;
end;

function TConstantsProperties.Get_SMPP_MULTIPARTMODE_UDH: Integer;
begin
    Result := DefaultInterface.SMPP_MULTIPARTMODE_UDH;
end;

function TConstantsProperties.Get_SMPP_MULTIPARTMODE_UDH16BIT: Integer;
begin
    Result := DefaultInterface.SMPP_MULTIPARTMODE_UDH16BIT;
end;

function TConstantsProperties.Get_SMPP_MULTIPARTMODE_SARTLV: Integer;
begin
    Result := DefaultInterface.SMPP_MULTIPARTMODE_SARTLV;
end;

function TConstantsProperties.Get_SMPP_MULTIPARTMODE_PAYLOADTLV: Integer;
begin
    Result := DefaultInterface.SMPP_MULTIPARTMODE_PAYLOADTLV;
end;

function TConstantsProperties.Get_SMPP_SUBMITMODE_SUBMITSM: Integer;
begin
    Result := DefaultInterface.SMPP_SUBMITMODE_SUBMITSM;
end;

function TConstantsProperties.Get_SMPP_SUBMITMODE_DATASM: Integer;
begin
    Result := DefaultInterface.SMPP_SUBMITMODE_DATASM;
end;

function TConstantsProperties.Get_SMPP_USEGSMENCODING_INCHARSET: Integer;
begin
    Result := DefaultInterface.SMPP_USEGSMENCODING_INCHARSET;
end;

function TConstantsProperties.Get_SMPP_USEGSMENCODING_OUTCHARSET: Integer;
begin
    Result := DefaultInterface.SMPP_USEGSMENCODING_OUTCHARSET;
end;

function TConstantsProperties.Get_SMPP_USEGSMENCODING_INOUTCHARS: Integer;
begin
    Result := DefaultInterface.SMPP_USEGSMENCODING_INOUTCHARS;
end;

function TConstantsProperties.Get_SMPP_IPVERSION_4: Integer;
begin
    Result := DefaultInterface.SMPP_IPVERSION_4;
end;

function TConstantsProperties.Get_SMPP_IPVERSION_6: Integer;
begin
    Result := DefaultInterface.SMPP_IPVERSION_6;
end;

function TConstantsProperties.Get_SMPP_IPVERSION_BOTH: Integer;
begin
    Result := DefaultInterface.SMPP_IPVERSION_BOTH;
end;

function TConstantsProperties.Get_SMPP_ESME_ROK: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_ROK;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVMSGLEN: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVMSGLEN;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVCMDLEN: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVCMDLEN;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVCMDID: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVCMDID;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVBNDSTS: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVBNDSTS;
end;

function TConstantsProperties.Get_SMPP_ESME_RALYBND: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RALYBND;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVPRTFLG: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVPRTFLG;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVREGDLVFLG: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVREGDLVFLG;
end;

function TConstantsProperties.Get_SMPP_ESME_RSYSERR: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RSYSERR;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVSRCADR: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVSRCADR;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVDSTADR: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVDSTADR;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVMSGID: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVMSGID;
end;

function TConstantsProperties.Get_SMPP_ESME_RBINDFAIL: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RBINDFAIL;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVPASWD: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVPASWD;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVSYSID: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVSYSID;
end;

function TConstantsProperties.Get_SMPP_ESME_RCANCELFAIL: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RCANCELFAIL;
end;

function TConstantsProperties.Get_SMPP_ESME_RREPLACEFAIL: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RREPLACEFAIL;
end;

function TConstantsProperties.Get_SMPP_ESME_RMSGQFUL: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RMSGQFUL;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVSERTYP: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVSERTYP;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVNUMDESTS: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVNUMDESTS;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVDLNAME: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVDLNAME;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVDESTFLAG: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVDESTFLAG;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVSUBREP: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVSUBREP;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVESMCLASS: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVESMCLASS;
end;

function TConstantsProperties.Get_SMPP_ESME_RCNTSUBDL: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RCNTSUBDL;
end;

function TConstantsProperties.Get_SMPP_ESME_RSUBMITFAIL: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RSUBMITFAIL;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVSRCTON: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVSRCTON;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVSRCNPI: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVSRCNPI;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVDSTTON: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVDSTTON;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVDSTNPI: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVDSTNPI;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVSYSTYP: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVSYSTYP;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVREPFLAG: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVREPFLAG;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVNUMMSGS: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVNUMMSGS;
end;

function TConstantsProperties.Get_SMPP_ESME_RTHROTTLED: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RTHROTTLED;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVSCHED: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVSCHED;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVEXPIRY: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVEXPIRY;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVDFTMSGID: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVDFTMSGID;
end;

function TConstantsProperties.Get_SMPP_ESME_RX_T_APPN: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RX_T_APPN;
end;

function TConstantsProperties.Get_SMPP_ESME_RX_P_APPN: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RX_P_APPN;
end;

function TConstantsProperties.Get_SMPP_ESME_RX_R_APPN: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RX_R_APPN;
end;

function TConstantsProperties.Get_SMPP_ESME_RQUERYFAIL: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RQUERYFAIL;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVOPTPARSTREAM: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVOPTPARSTREAM;
end;

function TConstantsProperties.Get_SMPP_ESME_ROPTPARNOTALLWD: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_ROPTPARNOTALLWD;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVPARLEN: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVPARLEN;
end;

function TConstantsProperties.Get_SMPP_ESME_RMISSINGOPTPARAM: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RMISSINGOPTPARAM;
end;

function TConstantsProperties.Get_SMPP_ESME_RINVOPTPARAMVAL: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RINVOPTPARAMVAL;
end;

function TConstantsProperties.Get_SMPP_ESME_RDELIVERYFAILURE: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RDELIVERYFAILURE;
end;

function TConstantsProperties.Get_SMPP_ESME_RUNKNOWNERR: Integer;
begin
    Result := DefaultInterface.SMPP_ESME_RUNKNOWNERR;
end;

function TConstantsProperties.Get_GSM_STATUS_MESSAGE_DELIVERED_SUCCESSFULLY: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_MESSAGE_DELIVERED_SUCCESSFULLY;
end;

function TConstantsProperties.Get_GSM_STATUS_FORWARDED_STATUS_UNKNOWN: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_FORWARDED_STATUS_UNKNOWN;
end;

function TConstantsProperties.Get_GSM_STATUS_REPLACED: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_REPLACED;
end;

function TConstantsProperties.Get_GSM_STATUS_CONGESTION_STILL_TRYING: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_CONGESTION_STILL_TRYING;
end;

function TConstantsProperties.Get_GSM_STATUS_RECIPIENT_BUSY_STILL_TRYING: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_RECIPIENT_BUSY_STILL_TRYING;
end;

function TConstantsProperties.Get_GSM_STATUS_NO_RESPONSE_STILL_TRYING: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_NO_RESPONSE_STILL_TRYING;
end;

function TConstantsProperties.Get_GSM_STATUS_SERVICE_REJECTED_STILL_TRYING: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_SERVICE_REJECTED_STILL_TRYING;
end;

function TConstantsProperties.Get_GSM_STATUS_QOS_NOT_AVAILABLE_STILL_TRYING: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_QOS_NOT_AVAILABLE_STILL_TRYING;
end;

function TConstantsProperties.Get_GSM_STATUS_RECIPIENT_ERROR_STILL_TRYING: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_RECIPIENT_ERROR_STILL_TRYING;
end;

function TConstantsProperties.Get_GSM_STATUS_RPC_ERROR: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_RPC_ERROR;
end;

function TConstantsProperties.Get_GSM_STATUS_INCOMPATIBLE_DESTINATION: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_INCOMPATIBLE_DESTINATION;
end;

function TConstantsProperties.Get_GSM_STATUS_CONNECTION_REJECTED: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_CONNECTION_REJECTED;
end;

function TConstantsProperties.Get_GSM_STATUS_NOT_OBTAINABLE: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_NOT_OBTAINABLE;
end;

function TConstantsProperties.Get_GSM_STATUS_QOS_NOT_AVAILABLE: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_QOS_NOT_AVAILABLE;
end;

function TConstantsProperties.Get_GSM_STATUS_NO_INTERNETWORKING_AVAILABLE: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_NO_INTERNETWORKING_AVAILABLE;
end;

function TConstantsProperties.Get_GSM_STATUS_MESSAGE_EXPIRED: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_MESSAGE_EXPIRED;
end;

function TConstantsProperties.Get_GSM_STATUS_MESSAGE_DELETED_BY_SENDER: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_MESSAGE_DELETED_BY_SENDER;
end;

function TConstantsProperties.Get_GSM_STATUS_MESSAGE_DELETED_BY_SMSC: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_MESSAGE_DELETED_BY_SMSC;
end;

function TConstantsProperties.Get_GSM_STATUS_DOES_NOT_EXIST: Integer;
begin
    Result := DefaultInterface.GSM_STATUS_DOES_NOT_EXIST;
end;

function TConstantsProperties.Get_GSM_STORAGETYPE_SIM: Integer;
begin
    Result := DefaultInterface.GSM_STORAGETYPE_SIM;
end;

function TConstantsProperties.Get_GSM_STORAGETYPE_MEMORY: Integer;
begin
    Result := DefaultInterface.GSM_STORAGETYPE_MEMORY;
end;

function TConstantsProperties.Get_GSM_STORAGETYPE_COMBINED: Integer;
begin
    Result := DefaultInterface.GSM_STORAGETYPE_COMBINED;
end;

function TConstantsProperties.Get_GSM_STORAGETYPE_STATUS: Integer;
begin
    Result := DefaultInterface.GSM_STORAGETYPE_STATUS;
end;

function TConstantsProperties.Get_GSM_STORAGETYPE_ALL: Integer;
begin
    Result := DefaultInterface.GSM_STORAGETYPE_ALL;
end;

function TConstantsProperties.Get_GSM_FO_REPLYPATH_EXISTS: Integer;
begin
    Result := DefaultInterface.GSM_FO_REPLYPATH_EXISTS;
end;

function TConstantsProperties.Get_GSM_FO_UDHI: Integer;
begin
    Result := DefaultInterface.GSM_FO_UDHI;
end;

function TConstantsProperties.Get_GSM_FO_STATUS_REPORT: Integer;
begin
    Result := DefaultInterface.GSM_FO_STATUS_REPORT;
end;

function TConstantsProperties.Get_GSM_FO_VALIDITY_NONE: Integer;
begin
    Result := DefaultInterface.GSM_FO_VALIDITY_NONE;
end;

function TConstantsProperties.Get_GSM_FO_VALIDITY_RELATIVE: Integer;
begin
    Result := DefaultInterface.GSM_FO_VALIDITY_RELATIVE;
end;

function TConstantsProperties.Get_GSM_FO_VALIDITY_ENHANCED: Integer;
begin
    Result := DefaultInterface.GSM_FO_VALIDITY_ENHANCED;
end;

function TConstantsProperties.Get_GSM_FO_VALIDITY_ABSOLUTE: Integer;
begin
    Result := DefaultInterface.GSM_FO_VALIDITY_ABSOLUTE;
end;

function TConstantsProperties.Get_GSM_FO_REJECT_DUPLICATES: Integer;
begin
    Result := DefaultInterface.GSM_FO_REJECT_DUPLICATES;
end;

function TConstantsProperties.Get_GSM_FO_SUBMIT_SM: Integer;
begin
    Result := DefaultInterface.GSM_FO_SUBMIT_SM;
end;

function TConstantsProperties.Get_GSM_FO_DELIVER_SM: Integer;
begin
    Result := DefaultInterface.GSM_FO_DELIVER_SM;
end;

function TConstantsProperties.Get_GSM_FO_STATUS_SM: Integer;
begin
    Result := DefaultInterface.GSM_FO_STATUS_SM;
end;

function TConstantsProperties.Get_DATACODING_DEFAULT: Integer;
begin
    Result := DefaultInterface.DATACODING_DEFAULT;
end;

function TConstantsProperties.Get_DATACODING_8BIT_DATA: Integer;
begin
    Result := DefaultInterface.DATACODING_8BIT_DATA;
end;

function TConstantsProperties.Get_DATACODING_UNICODE: Integer;
begin
    Result := DefaultInterface.DATACODING_UNICODE;
end;

function TConstantsProperties.Get_GSM_DATACODING_ME_SPECIFIC: Integer;
begin
    Result := DefaultInterface.GSM_DATACODING_ME_SPECIFIC;
end;

function TConstantsProperties.Get_GSM_DATACODING_SIM_SPECIFIC: Integer;
begin
    Result := DefaultInterface.GSM_DATACODING_SIM_SPECIFIC;
end;

function TConstantsProperties.Get_GSM_DATACODING_TE_SPECIFIC: Integer;
begin
    Result := DefaultInterface.GSM_DATACODING_TE_SPECIFIC;
end;

function TConstantsProperties.Get_DATACODING_FLASH: Integer;
begin
    Result := DefaultInterface.DATACODING_FLASH;
end;

function TConstantsProperties.Get_GSM_BAUDRATE_110: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_110;
end;

function TConstantsProperties.Get_GSM_BAUDRATE_300: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_300;
end;

function TConstantsProperties.Get_GSM_BAUDRATE_600: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_600;
end;

function TConstantsProperties.Get_GSM_BAUDRATE_1200: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_1200;
end;

function TConstantsProperties.Get_GSM_BAUDRATE_2400: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_2400;
end;

function TConstantsProperties.Get_GSM_BAUDRATE_4800: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_4800;
end;

function TConstantsProperties.Get_GSM_BAUDRATE_9600: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_9600;
end;

function TConstantsProperties.Get_GSM_BAUDRATE_14400: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_14400;
end;

function TConstantsProperties.Get_GSM_BAUDRATE_19200: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_19200;
end;

function TConstantsProperties.Get_GSM_BAUDRATE_38400: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_38400;
end;

function TConstantsProperties.Get_GSM_BAUDRATE_56000: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_56000;
end;

function TConstantsProperties.Get_GSM_BAUDRATE_57600: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_57600;
end;

function TConstantsProperties.Get_GSM_BAUDRATE_64000: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_64000;
end;

function TConstantsProperties.Get_GSM_BAUDRATE_115200: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_115200;
end;

function TConstantsProperties.Get_GSM_BAUDRATE_128000: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_128000;
end;

function TConstantsProperties.Get_GSM_BAUDRATE_230400: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_230400;
end;

function TConstantsProperties.Get_GSM_BAUDRATE_256000: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_256000;
end;

function TConstantsProperties.Get_GSM_BAUDRATE_460800: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_460800;
end;

function TConstantsProperties.Get_GSM_BAUDRATE_921600: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_921600;
end;

function TConstantsProperties.Get_GSM_BAUDRATE_DEFAULT: Integer;
begin
    Result := DefaultInterface.GSM_BAUDRATE_DEFAULT;
end;

function TConstantsProperties.Get_GSM_MESSAGESTATE_RECEIVED_UNREAD: Integer;
begin
    Result := DefaultInterface.GSM_MESSAGESTATE_RECEIVED_UNREAD;
end;

function TConstantsProperties.Get_GSM_MESSAGESTATE_RECEIVED_READ: Integer;
begin
    Result := DefaultInterface.GSM_MESSAGESTATE_RECEIVED_READ;
end;

function TConstantsProperties.Get_GSM_MESSAGESTATE_STORED_UNSENT: Integer;
begin
    Result := DefaultInterface.GSM_MESSAGESTATE_STORED_UNSENT;
end;

function TConstantsProperties.Get_GSM_MESSAGESTATE_STORED_SENT: Integer;
begin
    Result := DefaultInterface.GSM_MESSAGESTATE_STORED_SENT;
end;

function TConstantsProperties.Get_GSM_MESSAGESTATE_ALL: Integer;
begin
    Result := DefaultInterface.GSM_MESSAGESTATE_ALL;
end;

function TConstantsProperties.Get_GSM_MESSAGEFORMAT_PDU: Integer;
begin
    Result := DefaultInterface.GSM_MESSAGEFORMAT_PDU;
end;

function TConstantsProperties.Get_GSM_MESSAGEFORMAT_TEXT: Integer;
begin
    Result := DefaultInterface.GSM_MESSAGEFORMAT_TEXT;
end;

function TConstantsProperties.Get_GSM_MESSAGEFORMAT_AUTO: Integer;
begin
    Result := DefaultInterface.GSM_MESSAGEFORMAT_AUTO;
end;

function TConstantsProperties.Get_HTTP_PLACEHOLDER_USERTAG: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_USERTAG;
end;

function TConstantsProperties.Get_HTTP_PLACEHOLDER_TOADDRESS: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_TOADDRESS;
end;

function TConstantsProperties.Get_HTTP_PLACEHOLDER_FROMADDRESS: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_FROMADDRESS;
end;

function TConstantsProperties.Get_HTTP_PLACEHOLDER_BODY: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_BODY;
end;

function TConstantsProperties.Get_HTTP_PLACEHOLDER_BODYASHEX: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_BODYASHEX;
end;

function TConstantsProperties.Get_HTTP_PLACEHOLDER_BODYASBASE64: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_BODYASBASE64;
end;

function TConstantsProperties.Get_HTTP_PLACEHOLDER_DELIVERYREPORT: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_DELIVERYREPORT;
end;

function TConstantsProperties.Get_HTTP_PLACEHOLDER_TOADDRESSTON: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_TOADDRESSTON;
end;

function TConstantsProperties.Get_HTTP_PLACEHOLDER_TOADDRESSNPI: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_TOADDRESSNPI;
end;

function TConstantsProperties.Get_HTTP_PLACEHOLDER_FROMADDRESSTON: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_FROMADDRESSTON;
end;

function TConstantsProperties.Get_HTTP_PLACEHOLDER_FROMADDRESSNPI: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_FROMADDRESSNPI;
end;

function TConstantsProperties.Get_HTTP_PLACEHOLDER_PROTOCOLID: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_PROTOCOLID;
end;

function TConstantsProperties.Get_HTTP_PLACEHOLDER_UDHI: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_UDHI;
end;

function TConstantsProperties.Get_HTTP_PLACEHOLDER_DATACODING: WideString;
begin
    Result := DefaultInterface.HTTP_PLACEHOLDER_DATACODING;
end;

function TConstantsProperties.Get_GSM_PREFIXSMSC_ENABLED: Integer;
begin
    Result := DefaultInterface.GSM_PREFIXSMSC_ENABLED;
end;

function TConstantsProperties.Get_GSM_PREFIXSMSC_DISABLED: Integer;
begin
    Result := DefaultInterface.GSM_PREFIXSMSC_DISABLED;
end;

function TConstantsProperties.Get_GSM_PREFIXSMSC_AUTO: Integer;
begin
    Result := DefaultInterface.GSM_PREFIXSMSC_AUTO;
end;

function TConstantsProperties.Get_WAPPUSH_SIGNAL_NONE: Integer;
begin
    Result := DefaultInterface.WAPPUSH_SIGNAL_NONE;
end;

function TConstantsProperties.Get_WAPPUSH_SIGNAL_LOW: Integer;
begin
    Result := DefaultInterface.WAPPUSH_SIGNAL_LOW;
end;

function TConstantsProperties.Get_WAPPUSH_SIGNAL_MEDIUM: Integer;
begin
    Result := DefaultInterface.WAPPUSH_SIGNAL_MEDIUM;
end;

function TConstantsProperties.Get_WAPPUSH_SIGNAL_HIGH: Integer;
begin
    Result := DefaultInterface.WAPPUSH_SIGNAL_HIGH;
end;

function TConstantsProperties.Get_WAPPUSH_SIGNAL_DELETE: Integer;
begin
    Result := DefaultInterface.WAPPUSH_SIGNAL_DELETE;
end;

function TConstantsProperties.Get_DIALUP_PROVIDERTYPE_UCP: Integer;
begin
    Result := DefaultInterface.DIALUP_PROVIDERTYPE_UCP;
end;

function TConstantsProperties.Get_DIALUP_PROVIDERTYPE_TAP_DEFAULT: Integer;
begin
    Result := DefaultInterface.DIALUP_PROVIDERTYPE_TAP_DEFAULT;
end;

function TConstantsProperties.Get_DIALUP_PROVIDERTYPE_TAP_NOLF: Integer;
begin
    Result := DefaultInterface.DIALUP_PROVIDERTYPE_TAP_NOLF;
end;

function TConstantsProperties.Get_DIALUP_PROVIDERTYPE_TAP_NOEOT: Integer;
begin
    Result := DefaultInterface.DIALUP_PROVIDERTYPE_TAP_NOEOT;
end;

function TConstantsProperties.Get_DIALUP_DIALMODE_TONE: Integer;
begin
    Result := DefaultInterface.DIALUP_DIALMODE_TONE;
end;

function TConstantsProperties.Get_DIALUP_DIALMODE_PULSE: Integer;
begin
    Result := DefaultInterface.DIALUP_DIALMODE_PULSE;
end;

function TConstantsProperties.Get_DIALUP_DEVICESETTINGS_DEFAULT: Integer;
begin
    Result := DefaultInterface.DIALUP_DEVICESETTINGS_DEFAULT;
end;

function TConstantsProperties.Get_DIALUP_DEVICESETTINGS_8N1: Integer;
begin
    Result := DefaultInterface.DIALUP_DEVICESETTINGS_8N1;
end;

function TConstantsProperties.Get_DIALUP_DEVICESETTINGS_7E1: Integer;
begin
    Result := DefaultInterface.DIALUP_DEVICESETTINGS_7E1;
end;

{$ENDIF}

class function CoMessage.Create: IMessage;
begin
  Result := CreateComObject(CLASS_Message) as IMessage;
end;

class function CoMessage.CreateRemote(const MachineName: string): IMessage;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Message) as IMessage;
end;

procedure TMessage.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{7524134C-0186-43C6-958C-2C3A49FE40FC}';
    IntfIID:   '{DFD486FC-45B4-49AB-AD5A-98256AE057FC}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TMessage.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as IMessage;
  end;
end;

procedure TMessage.ConnectTo(svrIntf: IMessage);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TMessage.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TMessage.GetDefaultInterface: IMessage;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TMessage.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TMessageProperties.Create(Self);
{$ENDIF}
end;

destructor TMessage.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TMessage.GetServerProperties: TMessageProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TMessage.Get_UserTag: Integer;
begin
    Result := DefaultInterface.UserTag;
end;

procedure TMessage.Set_UserTag(Val: Integer);
begin
  DefaultInterface.Set_UserTag(Val);
end;

function TMessage.Get_ToAddress: WideString;
begin
    Result := DefaultInterface.ToAddress;
end;

procedure TMessage.Set_ToAddress(const Val: WideString);
  { Warning: The property ToAddress has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ToAddress := Val;
end;

function TMessage.Get_FromAddress: WideString;
begin
    Result := DefaultInterface.FromAddress;
end;

procedure TMessage.Set_FromAddress(const Val: WideString);
  { Warning: The property FromAddress has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.FromAddress := Val;
end;

function TMessage.Get_Body: WideString;
begin
    Result := DefaultInterface.Body;
end;

procedure TMessage.Set_Body(const Val: WideString);
  { Warning: The property Body has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Body := Val;
end;

function TMessage.Get_RequestDeliveryReport: WordBool;
begin
    Result := DefaultInterface.RequestDeliveryReport;
end;

procedure TMessage.Set_RequestDeliveryReport(Val: WordBool);
begin
  DefaultInterface.Set_RequestDeliveryReport(Val);
end;

function TMessage.Get_ToAddressTON: Integer;
begin
    Result := DefaultInterface.ToAddressTON;
end;

procedure TMessage.Set_ToAddressTON(Val: Integer);
begin
  DefaultInterface.Set_ToAddressTON(Val);
end;

function TMessage.Get_ToAddressNPI: Integer;
begin
    Result := DefaultInterface.ToAddressNPI;
end;

procedure TMessage.Set_ToAddressNPI(Val: Integer);
begin
  DefaultInterface.Set_ToAddressNPI(Val);
end;

function TMessage.Get_FromAddressTON: Integer;
begin
    Result := DefaultInterface.FromAddressTON;
end;

procedure TMessage.Set_FromAddressTON(Val: Integer);
begin
  DefaultInterface.Set_FromAddressTON(Val);
end;

function TMessage.Get_FromAddressNPI: Integer;
begin
    Result := DefaultInterface.FromAddressNPI;
end;

procedure TMessage.Set_FromAddressNPI(Val: Integer);
begin
  DefaultInterface.Set_FromAddressNPI(Val);
end;

function TMessage.Get_ProtocolId: Integer;
begin
    Result := DefaultInterface.ProtocolId;
end;

procedure TMessage.Set_ProtocolId(Val: Integer);
begin
  DefaultInterface.Set_ProtocolId(Val);
end;

function TMessage.Get_ValidityPeriod: Integer;
begin
    Result := DefaultInterface.ValidityPeriod;
end;

procedure TMessage.Set_ValidityPeriod(Val: Integer);
begin
  DefaultInterface.Set_ValidityPeriod(Val);
end;

function TMessage.Get_Reference: WideString;
begin
    Result := DefaultInterface.Reference;
end;

procedure TMessage.Set_Reference(const Val: WideString);
  { Warning: The property Reference has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Reference := Val;
end;

function TMessage.Get_DataCoding: Integer;
begin
    Result := DefaultInterface.DataCoding;
end;

procedure TMessage.Set_DataCoding(Val: Integer);
begin
  DefaultInterface.Set_DataCoding(Val);
end;

function TMessage.Get_BodyFormat: Integer;
begin
    Result := DefaultInterface.BodyFormat;
end;

procedure TMessage.Set_BodyFormat(Val: Integer);
begin
  DefaultInterface.Set_BodyFormat(Val);
end;

function TMessage.Get_TotalParts: Integer;
begin
    Result := DefaultInterface.TotalParts;
end;

function TMessage.Get_PartNumber: Integer;
begin
    Result := DefaultInterface.PartNumber;
end;

function TMessage.Get_ReceiveTime: WideString;
begin
    Result := DefaultInterface.ReceiveTime;
end;

function TMessage.Get_ReceiveTimeInSeconds: Integer;
begin
    Result := DefaultInterface.ReceiveTimeInSeconds;
end;

function TMessage.Get_HasUdh: WordBool;
begin
    Result := DefaultInterface.HasUdh;
end;

procedure TMessage.Set_HasUdh(Val: WordBool);
begin
  DefaultInterface.Set_HasUdh(Val);
end;

function TMessage.Get_Incomplete: WordBool;
begin
    Result := DefaultInterface.Incomplete;
end;

function TMessage.Get_GsmFirstOctet: Integer;
begin
    Result := DefaultInterface.GsmFirstOctet;
end;

procedure TMessage.Set_GsmFirstOctet(Val: Integer);
begin
  DefaultInterface.Set_GsmFirstOctet(Val);
end;

function TMessage.Get_GsmSmscAddress: WideString;
begin
    Result := DefaultInterface.GsmSmscAddress;
end;

function TMessage.Get_GsmSmscAddressTON: Integer;
begin
    Result := DefaultInterface.GsmSmscAddressTON;
end;

function TMessage.Get_GsmSmscAddressNPI: Integer;
begin
    Result := DefaultInterface.GsmSmscAddressNPI;
end;

function TMessage.Get_GsmMemoryIndex: WideString;
begin
    Result := DefaultInterface.GsmMemoryIndex;
end;

procedure TMessage.Set_GsmMemoryIndex(const Val: WideString);
  { Warning: The property GsmMemoryIndex has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.GsmMemoryIndex := Val;
end;

function TMessage.Get_GsmMemoryLocation: WideString;
begin
    Result := DefaultInterface.GsmMemoryLocation;
end;

procedure TMessage.Set_GsmMemoryLocation(const Val: WideString);
  { Warning: The property GsmMemoryLocation has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.GsmMemoryLocation := Val;
end;

function TMessage.Get_SmppPriority: Integer;
begin
    Result := DefaultInterface.SmppPriority;
end;

procedure TMessage.Set_SmppPriority(Val: Integer);
begin
  DefaultInterface.Set_SmppPriority(Val);
end;

function TMessage.Get_SmppServiceType: WideString;
begin
    Result := DefaultInterface.SmppServiceType;
end;

procedure TMessage.Set_SmppServiceType(const Val: WideString);
  { Warning: The property SmppServiceType has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.SmppServiceType := Val;
end;

function TMessage.Get_SmppEsmClass: Integer;
begin
    Result := DefaultInterface.SmppEsmClass;
end;

procedure TMessage.Set_SmppEsmClass(Val: Integer);
begin
  DefaultInterface.Set_SmppEsmClass(Val);
end;

function TMessage.Get_SmppIsDeliveryReport: WordBool;
begin
    Result := DefaultInterface.SmppIsDeliveryReport;
end;

procedure TMessage.Set_SmppIsDeliveryReport(Val: WordBool);
begin
  DefaultInterface.Set_SmppIsDeliveryReport(Val);
end;

function TMessage.Get_SmppStatus: Integer;
begin
    Result := DefaultInterface.SmppStatus;
end;

procedure TMessage.Set_SmppStatus(Val: Integer);
begin
  DefaultInterface.Set_SmppStatus(Val);
end;

function TMessage.Get_SmppError: Integer;
begin
    Result := DefaultInterface.SmppError;
end;

procedure TMessage.Set_SmppError(Val: Integer);
begin
  DefaultInterface.Set_SmppError(Val);
end;

function TMessage.Get_SmppCommandStatus: Integer;
begin
    Result := DefaultInterface.SmppCommandStatus;
end;

procedure TMessage.Set_SmppCommandStatus(Val: Integer);
begin
  DefaultInterface.Set_SmppCommandStatus(Val);
end;

function TMessage.Get_SmppSequenceNumber: Integer;
begin
    Result := DefaultInterface.SmppSequenceNumber;
end;

function TMessage.Get_SmppServerSubmitDate: WideString;
begin
    Result := DefaultInterface.SmppServerSubmitDate;
end;

function TMessage.Get_SmppServerFinalDate: WideString;
begin
    Result := DefaultInterface.SmppServerFinalDate;
end;

function TMessage.Get_LastError: Integer;
begin
    Result := DefaultInterface.LastError;
end;

procedure TMessage.Clear;
begin
  DefaultInterface.Clear;
end;

procedure TMessage.SmppAddTlv(const Tlv: ITlv);
begin
  DefaultInterface.SmppAddTlv(Tlv);
end;

function TMessage.SmppGetFirstTlv: ITlv;
begin
  Result := DefaultInterface.SmppGetFirstTlv;
end;

function TMessage.SmppGetNextTlv: ITlv;
begin
  Result := DefaultInterface.SmppGetNextTlv;
end;

function TMessage.SmppGetTlv(lTag: Integer): ITlv;
begin
  Result := DefaultInterface.SmppGetTlv(lTag);
end;

function TMessage.GetErrorDescription(Error: Integer): WideString;
begin
  Result := DefaultInterface.GetErrorDescription(Error);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TMessageProperties.Create(AServer: TMessage);
begin
  inherited Create;
  FServer := AServer;
end;

function TMessageProperties.GetDefaultInterface: IMessage;
begin
  Result := FServer.DefaultInterface;
end;

function TMessageProperties.Get_UserTag: Integer;
begin
    Result := DefaultInterface.UserTag;
end;

procedure TMessageProperties.Set_UserTag(Val: Integer);
begin
  DefaultInterface.Set_UserTag(Val);
end;

function TMessageProperties.Get_ToAddress: WideString;
begin
    Result := DefaultInterface.ToAddress;
end;

procedure TMessageProperties.Set_ToAddress(const Val: WideString);
  { Warning: The property ToAddress has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ToAddress := Val;
end;

function TMessageProperties.Get_FromAddress: WideString;
begin
    Result := DefaultInterface.FromAddress;
end;

procedure TMessageProperties.Set_FromAddress(const Val: WideString);
  { Warning: The property FromAddress has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.FromAddress := Val;
end;

function TMessageProperties.Get_Body: WideString;
begin
    Result := DefaultInterface.Body;
end;

procedure TMessageProperties.Set_Body(const Val: WideString);
  { Warning: The property Body has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Body := Val;
end;

function TMessageProperties.Get_RequestDeliveryReport: WordBool;
begin
    Result := DefaultInterface.RequestDeliveryReport;
end;

procedure TMessageProperties.Set_RequestDeliveryReport(Val: WordBool);
begin
  DefaultInterface.Set_RequestDeliveryReport(Val);
end;

function TMessageProperties.Get_ToAddressTON: Integer;
begin
    Result := DefaultInterface.ToAddressTON;
end;

procedure TMessageProperties.Set_ToAddressTON(Val: Integer);
begin
  DefaultInterface.Set_ToAddressTON(Val);
end;

function TMessageProperties.Get_ToAddressNPI: Integer;
begin
    Result := DefaultInterface.ToAddressNPI;
end;

procedure TMessageProperties.Set_ToAddressNPI(Val: Integer);
begin
  DefaultInterface.Set_ToAddressNPI(Val);
end;

function TMessageProperties.Get_FromAddressTON: Integer;
begin
    Result := DefaultInterface.FromAddressTON;
end;

procedure TMessageProperties.Set_FromAddressTON(Val: Integer);
begin
  DefaultInterface.Set_FromAddressTON(Val);
end;

function TMessageProperties.Get_FromAddressNPI: Integer;
begin
    Result := DefaultInterface.FromAddressNPI;
end;

procedure TMessageProperties.Set_FromAddressNPI(Val: Integer);
begin
  DefaultInterface.Set_FromAddressNPI(Val);
end;

function TMessageProperties.Get_ProtocolId: Integer;
begin
    Result := DefaultInterface.ProtocolId;
end;

procedure TMessageProperties.Set_ProtocolId(Val: Integer);
begin
  DefaultInterface.Set_ProtocolId(Val);
end;

function TMessageProperties.Get_ValidityPeriod: Integer;
begin
    Result := DefaultInterface.ValidityPeriod;
end;

procedure TMessageProperties.Set_ValidityPeriod(Val: Integer);
begin
  DefaultInterface.Set_ValidityPeriod(Val);
end;

function TMessageProperties.Get_Reference: WideString;
begin
    Result := DefaultInterface.Reference;
end;

procedure TMessageProperties.Set_Reference(const Val: WideString);
  { Warning: The property Reference has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Reference := Val;
end;

function TMessageProperties.Get_DataCoding: Integer;
begin
    Result := DefaultInterface.DataCoding;
end;

procedure TMessageProperties.Set_DataCoding(Val: Integer);
begin
  DefaultInterface.Set_DataCoding(Val);
end;

function TMessageProperties.Get_BodyFormat: Integer;
begin
    Result := DefaultInterface.BodyFormat;
end;

procedure TMessageProperties.Set_BodyFormat(Val: Integer);
begin
  DefaultInterface.Set_BodyFormat(Val);
end;

function TMessageProperties.Get_TotalParts: Integer;
begin
    Result := DefaultInterface.TotalParts;
end;

function TMessageProperties.Get_PartNumber: Integer;
begin
    Result := DefaultInterface.PartNumber;
end;

function TMessageProperties.Get_ReceiveTime: WideString;
begin
    Result := DefaultInterface.ReceiveTime;
end;

function TMessageProperties.Get_ReceiveTimeInSeconds: Integer;
begin
    Result := DefaultInterface.ReceiveTimeInSeconds;
end;

function TMessageProperties.Get_HasUdh: WordBool;
begin
    Result := DefaultInterface.HasUdh;
end;

procedure TMessageProperties.Set_HasUdh(Val: WordBool);
begin
  DefaultInterface.Set_HasUdh(Val);
end;

function TMessageProperties.Get_Incomplete: WordBool;
begin
    Result := DefaultInterface.Incomplete;
end;

function TMessageProperties.Get_GsmFirstOctet: Integer;
begin
    Result := DefaultInterface.GsmFirstOctet;
end;

procedure TMessageProperties.Set_GsmFirstOctet(Val: Integer);
begin
  DefaultInterface.Set_GsmFirstOctet(Val);
end;

function TMessageProperties.Get_GsmSmscAddress: WideString;
begin
    Result := DefaultInterface.GsmSmscAddress;
end;

function TMessageProperties.Get_GsmSmscAddressTON: Integer;
begin
    Result := DefaultInterface.GsmSmscAddressTON;
end;

function TMessageProperties.Get_GsmSmscAddressNPI: Integer;
begin
    Result := DefaultInterface.GsmSmscAddressNPI;
end;

function TMessageProperties.Get_GsmMemoryIndex: WideString;
begin
    Result := DefaultInterface.GsmMemoryIndex;
end;

procedure TMessageProperties.Set_GsmMemoryIndex(const Val: WideString);
  { Warning: The property GsmMemoryIndex has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.GsmMemoryIndex := Val;
end;

function TMessageProperties.Get_GsmMemoryLocation: WideString;
begin
    Result := DefaultInterface.GsmMemoryLocation;
end;

procedure TMessageProperties.Set_GsmMemoryLocation(const Val: WideString);
  { Warning: The property GsmMemoryLocation has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.GsmMemoryLocation := Val;
end;

function TMessageProperties.Get_SmppPriority: Integer;
begin
    Result := DefaultInterface.SmppPriority;
end;

procedure TMessageProperties.Set_SmppPriority(Val: Integer);
begin
  DefaultInterface.Set_SmppPriority(Val);
end;

function TMessageProperties.Get_SmppServiceType: WideString;
begin
    Result := DefaultInterface.SmppServiceType;
end;

procedure TMessageProperties.Set_SmppServiceType(const Val: WideString);
  { Warning: The property SmppServiceType has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.SmppServiceType := Val;
end;

function TMessageProperties.Get_SmppEsmClass: Integer;
begin
    Result := DefaultInterface.SmppEsmClass;
end;

procedure TMessageProperties.Set_SmppEsmClass(Val: Integer);
begin
  DefaultInterface.Set_SmppEsmClass(Val);
end;

function TMessageProperties.Get_SmppIsDeliveryReport: WordBool;
begin
    Result := DefaultInterface.SmppIsDeliveryReport;
end;

procedure TMessageProperties.Set_SmppIsDeliveryReport(Val: WordBool);
begin
  DefaultInterface.Set_SmppIsDeliveryReport(Val);
end;

function TMessageProperties.Get_SmppStatus: Integer;
begin
    Result := DefaultInterface.SmppStatus;
end;

procedure TMessageProperties.Set_SmppStatus(Val: Integer);
begin
  DefaultInterface.Set_SmppStatus(Val);
end;

function TMessageProperties.Get_SmppError: Integer;
begin
    Result := DefaultInterface.SmppError;
end;

procedure TMessageProperties.Set_SmppError(Val: Integer);
begin
  DefaultInterface.Set_SmppError(Val);
end;

function TMessageProperties.Get_SmppCommandStatus: Integer;
begin
    Result := DefaultInterface.SmppCommandStatus;
end;

procedure TMessageProperties.Set_SmppCommandStatus(Val: Integer);
begin
  DefaultInterface.Set_SmppCommandStatus(Val);
end;

function TMessageProperties.Get_SmppSequenceNumber: Integer;
begin
    Result := DefaultInterface.SmppSequenceNumber;
end;

function TMessageProperties.Get_SmppServerSubmitDate: WideString;
begin
    Result := DefaultInterface.SmppServerSubmitDate;
end;

function TMessageProperties.Get_SmppServerFinalDate: WideString;
begin
    Result := DefaultInterface.SmppServerFinalDate;
end;

function TMessageProperties.Get_LastError: Integer;
begin
    Result := DefaultInterface.LastError;
end;

{$ENDIF}

class function CoTlv.Create: ITlv;
begin
  Result := CreateComObject(CLASS_Tlv) as ITlv;
end;

class function CoTlv.CreateRemote(const MachineName: string): ITlv;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Tlv) as ITlv;
end;

procedure TTlv.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{461B017B-C733-4145-A3AA-040B3A1E86D5}';
    IntfIID:   '{4687CEE3-3966-46A2-AEE3-5A747F455B84}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TTlv.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as ITlv;
  end;
end;

procedure TTlv.ConnectTo(svrIntf: ITlv);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TTlv.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TTlv.GetDefaultInterface: ITlv;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TTlv.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TTlvProperties.Create(Self);
{$ENDIF}
end;

destructor TTlv.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TTlv.GetServerProperties: TTlvProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TTlv.Get_Tag: Integer;
begin
    Result := DefaultInterface.Tag;
end;

procedure TTlv.Set_Tag(Val: Integer);
begin
  DefaultInterface.Set_Tag(Val);
end;

function TTlv.Get_ValueAsString: WideString;
begin
    Result := DefaultInterface.ValueAsString;
end;

procedure TTlv.Set_ValueAsString(const Val: WideString);
  { Warning: The property ValueAsString has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ValueAsString := Val;
end;

function TTlv.Get_ValueAsHexString: WideString;
begin
    Result := DefaultInterface.ValueAsHexString;
end;

procedure TTlv.Set_ValueAsHexString(const Val: WideString);
  { Warning: The property ValueAsHexString has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ValueAsHexString := Val;
end;

function TTlv.Get_ValueAsInt32: Integer;
begin
    Result := DefaultInterface.ValueAsInt32;
end;

procedure TTlv.Set_ValueAsInt32(Val: Integer);
begin
  DefaultInterface.Set_ValueAsInt32(Val);
end;

function TTlv.Get_ValueAsInt16: Integer;
begin
    Result := DefaultInterface.ValueAsInt16;
end;

procedure TTlv.Set_ValueAsInt16(Val: Integer);
begin
  DefaultInterface.Set_ValueAsInt16(Val);
end;

function TTlv.Get_ValueAsInt8: Integer;
begin
    Result := DefaultInterface.ValueAsInt8;
end;

procedure TTlv.Set_ValueAsInt8(Val: Integer);
begin
  DefaultInterface.Set_ValueAsInt8(Val);
end;

function TTlv.Get_Length: Integer;
begin
    Result := DefaultInterface.Length;
end;

procedure TTlv.Clear;
begin
  DefaultInterface.Clear;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TTlvProperties.Create(AServer: TTlv);
begin
  inherited Create;
  FServer := AServer;
end;

function TTlvProperties.GetDefaultInterface: ITlv;
begin
  Result := FServer.DefaultInterface;
end;

function TTlvProperties.Get_Tag: Integer;
begin
    Result := DefaultInterface.Tag;
end;

procedure TTlvProperties.Set_Tag(Val: Integer);
begin
  DefaultInterface.Set_Tag(Val);
end;

function TTlvProperties.Get_ValueAsString: WideString;
begin
    Result := DefaultInterface.ValueAsString;
end;

procedure TTlvProperties.Set_ValueAsString(const Val: WideString);
  { Warning: The property ValueAsString has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ValueAsString := Val;
end;

function TTlvProperties.Get_ValueAsHexString: WideString;
begin
    Result := DefaultInterface.ValueAsHexString;
end;

procedure TTlvProperties.Set_ValueAsHexString(const Val: WideString);
  { Warning: The property ValueAsHexString has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ValueAsHexString := Val;
end;

function TTlvProperties.Get_ValueAsInt32: Integer;
begin
    Result := DefaultInterface.ValueAsInt32;
end;

procedure TTlvProperties.Set_ValueAsInt32(Val: Integer);
begin
  DefaultInterface.Set_ValueAsInt32(Val);
end;

function TTlvProperties.Get_ValueAsInt16: Integer;
begin
    Result := DefaultInterface.ValueAsInt16;
end;

procedure TTlvProperties.Set_ValueAsInt16(Val: Integer);
begin
  DefaultInterface.Set_ValueAsInt16(Val);
end;

function TTlvProperties.Get_ValueAsInt8: Integer;
begin
    Result := DefaultInterface.ValueAsInt8;
end;

procedure TTlvProperties.Set_ValueAsInt8(Val: Integer);
begin
  DefaultInterface.Set_ValueAsInt8(Val);
end;

function TTlvProperties.Get_Length: Integer;
begin
    Result := DefaultInterface.Length;
end;

{$ENDIF}

class function CoGsmDeliveryReport.Create: IGsmDeliveryReport;
begin
  Result := CreateComObject(CLASS_GsmDeliveryReport) as IGsmDeliveryReport;
end;

class function CoGsmDeliveryReport.CreateRemote(const MachineName: string): IGsmDeliveryReport;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_GsmDeliveryReport) as IGsmDeliveryReport;
end;

procedure TGsmDeliveryReport.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{035EC473-4171-4D8C-BB09-000F1BBB79B5}';
    IntfIID:   '{6962C3D8-88FE-4660-A4EC-901A244A57D2}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TGsmDeliveryReport.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as IGsmDeliveryReport;
  end;
end;

procedure TGsmDeliveryReport.ConnectTo(svrIntf: IGsmDeliveryReport);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TGsmDeliveryReport.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TGsmDeliveryReport.GetDefaultInterface: IGsmDeliveryReport;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TGsmDeliveryReport.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TGsmDeliveryReportProperties.Create(Self);
{$ENDIF}
end;

destructor TGsmDeliveryReport.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TGsmDeliveryReport.GetServerProperties: TGsmDeliveryReportProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TGsmDeliveryReport.Get_Reference: WideString;
begin
    Result := DefaultInterface.Reference;
end;

function TGsmDeliveryReport.Get_UserTag: Integer;
begin
    Result := DefaultInterface.UserTag;
end;

procedure TGsmDeliveryReport.Set_UserTag(Val: Integer);
begin
  DefaultInterface.Set_UserTag(Val);
end;

function TGsmDeliveryReport.Get_SmscAddress: WideString;
begin
    Result := DefaultInterface.SmscAddress;
end;

function TGsmDeliveryReport.Get_SmscTime: WideString;
begin
    Result := DefaultInterface.SmscTime;
end;

function TGsmDeliveryReport.Get_SmscTimeInSeconds: Integer;
begin
    Result := DefaultInterface.SmscTimeInSeconds;
end;

function TGsmDeliveryReport.Get_DischargeTime: WideString;
begin
    Result := DefaultInterface.DischargeTime;
end;

function TGsmDeliveryReport.Get_DischargeTimeInSeconds: Integer;
begin
    Result := DefaultInterface.DischargeTimeInSeconds;
end;

function TGsmDeliveryReport.Get_MemoryIndex: WideString;
begin
    Result := DefaultInterface.MemoryIndex;
end;

function TGsmDeliveryReport.Get_FirstOctet: Integer;
begin
    Result := DefaultInterface.FirstOctet;
end;

function TGsmDeliveryReport.Get_SmscTON: Integer;
begin
    Result := DefaultInterface.SmscTON;
end;

function TGsmDeliveryReport.Get_SmscNPI: Integer;
begin
    Result := DefaultInterface.SmscNPI;
end;

function TGsmDeliveryReport.Get_TON: Integer;
begin
    Result := DefaultInterface.TON;
end;

function TGsmDeliveryReport.Get_NPI: Integer;
begin
    Result := DefaultInterface.NPI;
end;

function TGsmDeliveryReport.Get_Status: Integer;
begin
    Result := DefaultInterface.Status;
end;

function TGsmDeliveryReport.Get_FromAddress: WideString;
begin
    Result := DefaultInterface.FromAddress;
end;

function TGsmDeliveryReport.Get_MemoryLocation: WideString;
begin
    Result := DefaultInterface.MemoryLocation;
end;

procedure TGsmDeliveryReport.Clear;
begin
  DefaultInterface.Clear;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TGsmDeliveryReportProperties.Create(AServer: TGsmDeliveryReport);
begin
  inherited Create;
  FServer := AServer;
end;

function TGsmDeliveryReportProperties.GetDefaultInterface: IGsmDeliveryReport;
begin
  Result := FServer.DefaultInterface;
end;

function TGsmDeliveryReportProperties.Get_Reference: WideString;
begin
    Result := DefaultInterface.Reference;
end;

function TGsmDeliveryReportProperties.Get_UserTag: Integer;
begin
    Result := DefaultInterface.UserTag;
end;

procedure TGsmDeliveryReportProperties.Set_UserTag(Val: Integer);
begin
  DefaultInterface.Set_UserTag(Val);
end;

function TGsmDeliveryReportProperties.Get_SmscAddress: WideString;
begin
    Result := DefaultInterface.SmscAddress;
end;

function TGsmDeliveryReportProperties.Get_SmscTime: WideString;
begin
    Result := DefaultInterface.SmscTime;
end;

function TGsmDeliveryReportProperties.Get_SmscTimeInSeconds: Integer;
begin
    Result := DefaultInterface.SmscTimeInSeconds;
end;

function TGsmDeliveryReportProperties.Get_DischargeTime: WideString;
begin
    Result := DefaultInterface.DischargeTime;
end;

function TGsmDeliveryReportProperties.Get_DischargeTimeInSeconds: Integer;
begin
    Result := DefaultInterface.DischargeTimeInSeconds;
end;

function TGsmDeliveryReportProperties.Get_MemoryIndex: WideString;
begin
    Result := DefaultInterface.MemoryIndex;
end;

function TGsmDeliveryReportProperties.Get_FirstOctet: Integer;
begin
    Result := DefaultInterface.FirstOctet;
end;

function TGsmDeliveryReportProperties.Get_SmscTON: Integer;
begin
    Result := DefaultInterface.SmscTON;
end;

function TGsmDeliveryReportProperties.Get_SmscNPI: Integer;
begin
    Result := DefaultInterface.SmscNPI;
end;

function TGsmDeliveryReportProperties.Get_TON: Integer;
begin
    Result := DefaultInterface.TON;
end;

function TGsmDeliveryReportProperties.Get_NPI: Integer;
begin
    Result := DefaultInterface.NPI;
end;

function TGsmDeliveryReportProperties.Get_Status: Integer;
begin
    Result := DefaultInterface.Status;
end;

function TGsmDeliveryReportProperties.Get_FromAddress: WideString;
begin
    Result := DefaultInterface.FromAddress;
end;

function TGsmDeliveryReportProperties.Get_MemoryLocation: WideString;
begin
    Result := DefaultInterface.MemoryLocation;
end;

{$ENDIF}

class function CoDialup.Create: IDialup;
begin
  Result := CreateComObject(CLASS_Dialup) as IDialup;
end;

class function CoDialup.CreateRemote(const MachineName: string): IDialup;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Dialup) as IDialup;
end;

procedure TDialup.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{265F828D-DB37-4676-8E3E-B73B5A653A48}';
    IntfIID:   '{3E9851DF-D82F-4806-8716-769C015C23F2}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TDialup.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as IDialup;
  end;
end;

procedure TDialup.ConnectTo(svrIntf: IDialup);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TDialup.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TDialup.GetDefaultInterface: IDialup;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TDialup.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TDialupProperties.Create(Self);
{$ENDIF}
end;

destructor TDialup.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TDialup.GetServerProperties: TDialupProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TDialup.Get_Version: WideString;
begin
    Result := DefaultInterface.Version;
end;

function TDialup.Get_Build: WideString;
begin
    Result := DefaultInterface.Build;
end;

function TDialup.Get_Module: WideString;
begin
    Result := DefaultInterface.Module;
end;

function TDialup.Get_LicenseStatus: WideString;
begin
    Result := DefaultInterface.LicenseStatus;
end;

function TDialup.Get_LicenseKey: WideString;
begin
    Result := DefaultInterface.LicenseKey;
end;

procedure TDialup.Set_LicenseKey(const LicenseKey: WideString);
  { Warning: The property LicenseKey has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LicenseKey := LicenseKey;
end;

function TDialup.Get_LogFile: WideString;
begin
    Result := DefaultInterface.LogFile;
end;

procedure TDialup.Set_LogFile(const LogFile: WideString);
  { Warning: The property LogFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LogFile := LogFile;
end;

function TDialup.Get_LastError: Integer;
begin
    Result := DefaultInterface.LastError;
end;

function TDialup.Get_Device: WideString;
begin
    Result := DefaultInterface.Device;
end;

procedure TDialup.Set_Device(const Val: WideString);
  { Warning: The property Device has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Device := Val;
end;

function TDialup.Get_DeviceSpeed: Integer;
begin
    Result := DefaultInterface.DeviceSpeed;
end;

procedure TDialup.Set_DeviceSpeed(Val: Integer);
begin
  DefaultInterface.Set_DeviceSpeed(Val);
end;

function TDialup.Get_DeviceSettings: Integer;
begin
    Result := DefaultInterface.DeviceSettings;
end;

procedure TDialup.Set_DeviceSettings(Val: Integer);
begin
  DefaultInterface.Set_DeviceSettings(Val);
end;

function TDialup.Get_DeviceInitString: WideString;
begin
    Result := DefaultInterface.DeviceInitString;
end;

procedure TDialup.Set_DeviceInitString(const Val: WideString);
  { Warning: The property DeviceInitString has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.DeviceInitString := Val;
end;

function TDialup.Get_DialMode: Integer;
begin
    Result := DefaultInterface.DialMode;
end;

procedure TDialup.Set_DialMode(Val: Integer);
begin
  DefaultInterface.Set_DialMode(Val);
end;

function TDialup.Get_ProviderDialString: WideString;
begin
    Result := DefaultInterface.ProviderDialString;
end;

procedure TDialup.Set_ProviderDialString(const Val: WideString);
  { Warning: The property ProviderDialString has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ProviderDialString := Val;
end;

function TDialup.Get_ProviderPassword: WideString;
begin
    Result := DefaultInterface.ProviderPassword;
end;

procedure TDialup.Set_ProviderPassword(const Val: WideString);
  { Warning: The property ProviderPassword has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ProviderPassword := Val;
end;

function TDialup.Get_ProviderType: Integer;
begin
    Result := DefaultInterface.ProviderType;
end;

procedure TDialup.Set_ProviderType(Val: Integer);
begin
  DefaultInterface.Set_ProviderType(Val);
end;

function TDialup.Get_ProviderResponse: WideString;
begin
    Result := DefaultInterface.ProviderResponse;
end;

function TDialup.Get_AdvancedSettings: WideString;
begin
    Result := DefaultInterface.AdvancedSettings;
end;

procedure TDialup.Set_AdvancedSettings(const Val: WideString);
  { Warning: The property AdvancedSettings has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.AdvancedSettings := Val;
end;

procedure TDialup.SaveLicenseKey;
begin
  DefaultInterface.SaveLicenseKey;
end;

procedure TDialup.Sleep(Ms: Integer);
begin
  DefaultInterface.Sleep(Ms);
end;

procedure TDialup.Clear;
begin
  DefaultInterface.Clear;
end;

function TDialup.GetErrorDescription(ErrorCode: Integer): WideString;
begin
  Result := DefaultInterface.GetErrorDescription(ErrorCode);
end;

function TDialup.GetDeviceCount: Integer;
begin
  Result := DefaultInterface.GetDeviceCount;
end;

function TDialup.GetDevice(lIndex: Integer): WideString;
begin
  Result := DefaultInterface.GetDevice(lIndex);
end;

procedure TDialup.Send(const Message: IMessage);
begin
  DefaultInterface.Send(Message);
end;

procedure TDialup.ProviderLoadConfig(const FileName: WideString);
begin
  DefaultInterface.ProviderLoadConfig(FileName);
end;

procedure TDialup.ProviderSaveConfig(const FileName: WideString);
begin
  DefaultInterface.ProviderSaveConfig(FileName);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TDialupProperties.Create(AServer: TDialup);
begin
  inherited Create;
  FServer := AServer;
end;

function TDialupProperties.GetDefaultInterface: IDialup;
begin
  Result := FServer.DefaultInterface;
end;

function TDialupProperties.Get_Version: WideString;
begin
    Result := DefaultInterface.Version;
end;

function TDialupProperties.Get_Build: WideString;
begin
    Result := DefaultInterface.Build;
end;

function TDialupProperties.Get_Module: WideString;
begin
    Result := DefaultInterface.Module;
end;

function TDialupProperties.Get_LicenseStatus: WideString;
begin
    Result := DefaultInterface.LicenseStatus;
end;

function TDialupProperties.Get_LicenseKey: WideString;
begin
    Result := DefaultInterface.LicenseKey;
end;

procedure TDialupProperties.Set_LicenseKey(const LicenseKey: WideString);
  { Warning: The property LicenseKey has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LicenseKey := LicenseKey;
end;

function TDialupProperties.Get_LogFile: WideString;
begin
    Result := DefaultInterface.LogFile;
end;

procedure TDialupProperties.Set_LogFile(const LogFile: WideString);
  { Warning: The property LogFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LogFile := LogFile;
end;

function TDialupProperties.Get_LastError: Integer;
begin
    Result := DefaultInterface.LastError;
end;

function TDialupProperties.Get_Device: WideString;
begin
    Result := DefaultInterface.Device;
end;

procedure TDialupProperties.Set_Device(const Val: WideString);
  { Warning: The property Device has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Device := Val;
end;

function TDialupProperties.Get_DeviceSpeed: Integer;
begin
    Result := DefaultInterface.DeviceSpeed;
end;

procedure TDialupProperties.Set_DeviceSpeed(Val: Integer);
begin
  DefaultInterface.Set_DeviceSpeed(Val);
end;

function TDialupProperties.Get_DeviceSettings: Integer;
begin
    Result := DefaultInterface.DeviceSettings;
end;

procedure TDialupProperties.Set_DeviceSettings(Val: Integer);
begin
  DefaultInterface.Set_DeviceSettings(Val);
end;

function TDialupProperties.Get_DeviceInitString: WideString;
begin
    Result := DefaultInterface.DeviceInitString;
end;

procedure TDialupProperties.Set_DeviceInitString(const Val: WideString);
  { Warning: The property DeviceInitString has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.DeviceInitString := Val;
end;

function TDialupProperties.Get_DialMode: Integer;
begin
    Result := DefaultInterface.DialMode;
end;

procedure TDialupProperties.Set_DialMode(Val: Integer);
begin
  DefaultInterface.Set_DialMode(Val);
end;

function TDialupProperties.Get_ProviderDialString: WideString;
begin
    Result := DefaultInterface.ProviderDialString;
end;

procedure TDialupProperties.Set_ProviderDialString(const Val: WideString);
  { Warning: The property ProviderDialString has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ProviderDialString := Val;
end;

function TDialupProperties.Get_ProviderPassword: WideString;
begin
    Result := DefaultInterface.ProviderPassword;
end;

procedure TDialupProperties.Set_ProviderPassword(const Val: WideString);
  { Warning: The property ProviderPassword has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ProviderPassword := Val;
end;

function TDialupProperties.Get_ProviderType: Integer;
begin
    Result := DefaultInterface.ProviderType;
end;

procedure TDialupProperties.Set_ProviderType(Val: Integer);
begin
  DefaultInterface.Set_ProviderType(Val);
end;

function TDialupProperties.Get_ProviderResponse: WideString;
begin
    Result := DefaultInterface.ProviderResponse;
end;

function TDialupProperties.Get_AdvancedSettings: WideString;
begin
    Result := DefaultInterface.AdvancedSettings;
end;

procedure TDialupProperties.Set_AdvancedSettings(const Val: WideString);
  { Warning: The property AdvancedSettings has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.AdvancedSettings := Val;
end;

{$ENDIF}

class function CoGsm.Create: IGsm;
begin
  Result := CreateComObject(CLASS_Gsm) as IGsm;
end;

class function CoGsm.CreateRemote(const MachineName: string): IGsm;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Gsm) as IGsm;
end;

procedure TGsm.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{4AB18312-548A-4607-97E1-3DE7FC27CDD2}';
    IntfIID:   '{B25B188D-42A8-45EF-887F-653CBC9E234B}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TGsm.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as IGsm;
  end;
end;

procedure TGsm.ConnectTo(svrIntf: IGsm);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TGsm.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TGsm.GetDefaultInterface: IGsm;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TGsm.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TGsmProperties.Create(Self);
{$ENDIF}
end;

destructor TGsm.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TGsm.GetServerProperties: TGsmProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TGsm.Get_Version: WideString;
begin
    Result := DefaultInterface.Version;
end;

function TGsm.Get_Build: WideString;
begin
    Result := DefaultInterface.Build;
end;

function TGsm.Get_Module: WideString;
begin
    Result := DefaultInterface.Module;
end;

function TGsm.Get_LicenseStatus: WideString;
begin
    Result := DefaultInterface.LicenseStatus;
end;

function TGsm.Get_LicenseKey: WideString;
begin
    Result := DefaultInterface.LicenseKey;
end;

procedure TGsm.Set_LicenseKey(const LicenseKey: WideString);
  { Warning: The property LicenseKey has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LicenseKey := LicenseKey;
end;

function TGsm.Get_LogFile: WideString;
begin
    Result := DefaultInterface.LogFile;
end;

procedure TGsm.Set_LogFile(const LogFile: WideString);
  { Warning: The property LogFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LogFile := LogFile;
end;

function TGsm.Get_ActivityFile: WideString;
begin
    Result := DefaultInterface.ActivityFile;
end;

procedure TGsm.Set_ActivityFile(const ActivityFile: WideString);
  { Warning: The property ActivityFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ActivityFile := ActivityFile;
end;

function TGsm.Get_LastError: Integer;
begin
    Result := DefaultInterface.LastError;
end;

function TGsm.Get_Manufacturer: WideString;
begin
    Result := DefaultInterface.Manufacturer;
end;

function TGsm.Get_Model: WideString;
begin
    Result := DefaultInterface.Model;
end;

function TGsm.Get_Revision: WideString;
begin
    Result := DefaultInterface.Revision;
end;

function TGsm.Get_SerialNr: WideString;
begin
    Result := DefaultInterface.SerialNr;
end;

function TGsm.Get_SendEnabled: WordBool;
begin
    Result := DefaultInterface.SendEnabled;
end;

function TGsm.Get_ReceiveEnabled: WordBool;
begin
    Result := DefaultInterface.ReceiveEnabled;
end;

function TGsm.Get_ReportEnabled: WordBool;
begin
    Result := DefaultInterface.ReportEnabled;
end;

function TGsm.Get_ExtractApplicationPort: WordBool;
begin
    Result := DefaultInterface.ExtractApplicationPort;
end;

procedure TGsm.Set_ExtractApplicationPort(Val: WordBool);
begin
  DefaultInterface.Set_ExtractApplicationPort(Val);
end;

function TGsm.Get_MultipartTimeout: Integer;
begin
    Result := DefaultInterface.MultipartTimeout;
end;

procedure TGsm.Set_MultipartTimeout(Val: Integer);
begin
  DefaultInterface.Set_MultipartTimeout(Val);
end;

function TGsm.Get_AssembleMultipart: WordBool;
begin
    Result := DefaultInterface.AssembleMultipart;
end;

procedure TGsm.Set_AssembleMultipart(Val: WordBool);
begin
  DefaultInterface.Set_AssembleMultipart(Val);
end;

function TGsm.Get_MessageMode: Integer;
begin
    Result := DefaultInterface.MessageMode;
end;

procedure TGsm.Set_MessageMode(Val: Integer);
begin
  DefaultInterface.Set_MessageMode(Val);
end;

function TGsm.Get_PrefixSmscMode: Integer;
begin
    Result := DefaultInterface.PrefixSmscMode;
end;

procedure TGsm.Set_PrefixSmscMode(Val: Integer);
begin
  DefaultInterface.Set_PrefixSmscMode(Val);
end;

function TGsm.Get_NetworkTimeout: Integer;
begin
    Result := DefaultInterface.NetworkTimeout;
end;

procedure TGsm.Set_NetworkTimeout(Val: Integer);
begin
  DefaultInterface.Set_NetworkTimeout(Val);
end;

function TGsm.Get_InterCommandDelay: Integer;
begin
    Result := DefaultInterface.InterCommandDelay;
end;

procedure TGsm.Set_InterCommandDelay(Val: Integer);
begin
  DefaultInterface.Set_InterCommandDelay(Val);
end;

function TGsm.Get_InterCharacterDelay: Integer;
begin
    Result := DefaultInterface.InterCharacterDelay;
end;

procedure TGsm.Set_InterCharacterDelay(Val: Integer);
begin
  DefaultInterface.Set_InterCharacterDelay(Val);
end;

function TGsm.Get_WaitForNetwork: WordBool;
begin
    Result := DefaultInterface.WaitForNetwork;
end;

procedure TGsm.Set_WaitForNetwork(Val: WordBool);
begin
  DefaultInterface.Set_WaitForNetwork(Val);
end;

function TGsm.Get_PreferredSmsc: WideString;
begin
    Result := DefaultInterface.PreferredSmsc;
end;

procedure TGsm.Set_PreferredSmsc(const Val: WideString);
  { Warning: The property PreferredSmsc has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.PreferredSmsc := Val;
end;

procedure TGsm.SaveLicenseKey;
begin
  DefaultInterface.SaveLicenseKey;
end;

procedure TGsm.Sleep(Ms: Integer);
begin
  DefaultInterface.Sleep(Ms);
end;

procedure TGsm.Clear;
begin
  DefaultInterface.Clear;
end;

function TGsm.GetErrorDescription(ErrorCode: Integer): WideString;
begin
  Result := DefaultInterface.GetErrorDescription(ErrorCode);
end;

function TGsm.FindFirstPort: WideString;
begin
  Result := DefaultInterface.FindFirstPort;
end;

function TGsm.FindNextPort: WideString;
begin
  Result := DefaultInterface.FindNextPort;
end;

function TGsm.FindFirstDevice: WideString;
begin
  Result := DefaultInterface.FindFirstDevice;
end;

function TGsm.FindNextDevice: WideString;
begin
  Result := DefaultInterface.FindNextDevice;
end;

procedure TGsm.Open(const strName: WideString; const strPin: WideString; lBaudrate: Integer);
begin
  DefaultInterface.Open(strName, strPin, lBaudrate);
end;

procedure TGsm.Close;
begin
  DefaultInterface.Close;
end;

procedure TGsm.SendCommand(const strCommand: WideString);
begin
  DefaultInterface.SendCommand(strCommand);
end;

function TGsm.ReadResponse(lTimeout: Integer): WideString;
begin
  Result := DefaultInterface.ReadResponse(lTimeout);
end;

function TGsm.SendSms(const Val: IMessage; lMultipartFlag: Integer; lTimeout: Integer): WideString;
begin
  Result := DefaultInterface.SendSms(Val, lMultipartFlag, lTimeout);
end;

procedure TGsm.Receive(lType: Integer; bDelete: WordBool; lStorageType: Integer; lTimeout: Integer);
begin
  DefaultInterface.Receive(lType, bDelete, lStorageType, lTimeout);
end;

function TGsm.GetFirstSms: IMessage;
begin
  Result := DefaultInterface.GetFirstSms;
end;

function TGsm.GetNextSms: IMessage;
begin
  Result := DefaultInterface.GetNextSms;
end;

function TGsm.GetFirstReport: IGsmDeliveryReport;
begin
  Result := DefaultInterface.GetFirstReport;
end;

function TGsm.GetNextReport: IGsmDeliveryReport;
begin
  Result := DefaultInterface.GetNextReport;
end;

procedure TGsm.DeleteSms(const pSms: IMessage);
begin
  DefaultInterface.DeleteSms(pSms);
end;

procedure TGsm.DeleteReport(const pReport: IGsmDeliveryReport);
begin
  DefaultInterface.DeleteReport(pReport);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TGsmProperties.Create(AServer: TGsm);
begin
  inherited Create;
  FServer := AServer;
end;

function TGsmProperties.GetDefaultInterface: IGsm;
begin
  Result := FServer.DefaultInterface;
end;

function TGsmProperties.Get_Version: WideString;
begin
    Result := DefaultInterface.Version;
end;

function TGsmProperties.Get_Build: WideString;
begin
    Result := DefaultInterface.Build;
end;

function TGsmProperties.Get_Module: WideString;
begin
    Result := DefaultInterface.Module;
end;

function TGsmProperties.Get_LicenseStatus: WideString;
begin
    Result := DefaultInterface.LicenseStatus;
end;

function TGsmProperties.Get_LicenseKey: WideString;
begin
    Result := DefaultInterface.LicenseKey;
end;

procedure TGsmProperties.Set_LicenseKey(const LicenseKey: WideString);
  { Warning: The property LicenseKey has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LicenseKey := LicenseKey;
end;

function TGsmProperties.Get_LogFile: WideString;
begin
    Result := DefaultInterface.LogFile;
end;

procedure TGsmProperties.Set_LogFile(const LogFile: WideString);
  { Warning: The property LogFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LogFile := LogFile;
end;

function TGsmProperties.Get_ActivityFile: WideString;
begin
    Result := DefaultInterface.ActivityFile;
end;

procedure TGsmProperties.Set_ActivityFile(const ActivityFile: WideString);
  { Warning: The property ActivityFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ActivityFile := ActivityFile;
end;

function TGsmProperties.Get_LastError: Integer;
begin
    Result := DefaultInterface.LastError;
end;

function TGsmProperties.Get_Manufacturer: WideString;
begin
    Result := DefaultInterface.Manufacturer;
end;

function TGsmProperties.Get_Model: WideString;
begin
    Result := DefaultInterface.Model;
end;

function TGsmProperties.Get_Revision: WideString;
begin
    Result := DefaultInterface.Revision;
end;

function TGsmProperties.Get_SerialNr: WideString;
begin
    Result := DefaultInterface.SerialNr;
end;

function TGsmProperties.Get_SendEnabled: WordBool;
begin
    Result := DefaultInterface.SendEnabled;
end;

function TGsmProperties.Get_ReceiveEnabled: WordBool;
begin
    Result := DefaultInterface.ReceiveEnabled;
end;

function TGsmProperties.Get_ReportEnabled: WordBool;
begin
    Result := DefaultInterface.ReportEnabled;
end;

function TGsmProperties.Get_ExtractApplicationPort: WordBool;
begin
    Result := DefaultInterface.ExtractApplicationPort;
end;

procedure TGsmProperties.Set_ExtractApplicationPort(Val: WordBool);
begin
  DefaultInterface.Set_ExtractApplicationPort(Val);
end;

function TGsmProperties.Get_MultipartTimeout: Integer;
begin
    Result := DefaultInterface.MultipartTimeout;
end;

procedure TGsmProperties.Set_MultipartTimeout(Val: Integer);
begin
  DefaultInterface.Set_MultipartTimeout(Val);
end;

function TGsmProperties.Get_AssembleMultipart: WordBool;
begin
    Result := DefaultInterface.AssembleMultipart;
end;

procedure TGsmProperties.Set_AssembleMultipart(Val: WordBool);
begin
  DefaultInterface.Set_AssembleMultipart(Val);
end;

function TGsmProperties.Get_MessageMode: Integer;
begin
    Result := DefaultInterface.MessageMode;
end;

procedure TGsmProperties.Set_MessageMode(Val: Integer);
begin
  DefaultInterface.Set_MessageMode(Val);
end;

function TGsmProperties.Get_PrefixSmscMode: Integer;
begin
    Result := DefaultInterface.PrefixSmscMode;
end;

procedure TGsmProperties.Set_PrefixSmscMode(Val: Integer);
begin
  DefaultInterface.Set_PrefixSmscMode(Val);
end;

function TGsmProperties.Get_NetworkTimeout: Integer;
begin
    Result := DefaultInterface.NetworkTimeout;
end;

procedure TGsmProperties.Set_NetworkTimeout(Val: Integer);
begin
  DefaultInterface.Set_NetworkTimeout(Val);
end;

function TGsmProperties.Get_InterCommandDelay: Integer;
begin
    Result := DefaultInterface.InterCommandDelay;
end;

procedure TGsmProperties.Set_InterCommandDelay(Val: Integer);
begin
  DefaultInterface.Set_InterCommandDelay(Val);
end;

function TGsmProperties.Get_InterCharacterDelay: Integer;
begin
    Result := DefaultInterface.InterCharacterDelay;
end;

procedure TGsmProperties.Set_InterCharacterDelay(Val: Integer);
begin
  DefaultInterface.Set_InterCharacterDelay(Val);
end;

function TGsmProperties.Get_WaitForNetwork: WordBool;
begin
    Result := DefaultInterface.WaitForNetwork;
end;

procedure TGsmProperties.Set_WaitForNetwork(Val: WordBool);
begin
  DefaultInterface.Set_WaitForNetwork(Val);
end;

function TGsmProperties.Get_PreferredSmsc: WideString;
begin
    Result := DefaultInterface.PreferredSmsc;
end;

procedure TGsmProperties.Set_PreferredSmsc(const Val: WideString);
  { Warning: The property PreferredSmsc has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.PreferredSmsc := Val;
end;

{$ENDIF}

class function CoHttp.Create: IHttp;
begin
  Result := CreateComObject(CLASS_Http) as IHttp;
end;

class function CoHttp.CreateRemote(const MachineName: string): IHttp;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Http) as IHttp;
end;

procedure THttp.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{5092DA94-3952-4FDA-9B8E-4B387F0164E6}';
    IntfIID:   '{1825A7C6-DBAB-4B99-AAA1-FDEDF36A4982}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure THttp.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as IHttp;
  end;
end;

procedure THttp.ConnectTo(svrIntf: IHttp);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure THttp.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function THttp.GetDefaultInterface: IHttp;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor THttp.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := THttpProperties.Create(Self);
{$ENDIF}
end;

destructor THttp.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function THttp.GetServerProperties: THttpProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function THttp.Get_Version: WideString;
begin
    Result := DefaultInterface.Version;
end;

function THttp.Get_Build: WideString;
begin
    Result := DefaultInterface.Build;
end;

function THttp.Get_Module: WideString;
begin
    Result := DefaultInterface.Module;
end;

function THttp.Get_LicenseStatus: WideString;
begin
    Result := DefaultInterface.LicenseStatus;
end;

function THttp.Get_LicenseKey: WideString;
begin
    Result := DefaultInterface.LicenseKey;
end;

procedure THttp.Set_LicenseKey(const LicenseKey: WideString);
  { Warning: The property LicenseKey has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LicenseKey := LicenseKey;
end;

function THttp.Get_LogFile: WideString;
begin
    Result := DefaultInterface.LogFile;
end;

procedure THttp.Set_LogFile(const LogFile: WideString);
  { Warning: The property LogFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LogFile := LogFile;
end;

function THttp.Get_ActivityFile: WideString;
begin
    Result := DefaultInterface.ActivityFile;
end;

procedure THttp.Set_ActivityFile(const ActivityFile: WideString);
  { Warning: The property ActivityFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ActivityFile := ActivityFile;
end;

function THttp.Get_LastError: Integer;
begin
    Result := DefaultInterface.LastError;
end;

function THttp.Get_Url: WideString;
begin
    Result := DefaultInterface.Url;
end;

procedure THttp.Set_Url(const Val: WideString);
  { Warning: The property Url has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Url := Val;
end;

function THttp.Get_PostBody: WideString;
begin
    Result := DefaultInterface.PostBody;
end;

procedure THttp.Set_PostBody(const Val: WideString);
  { Warning: The property PostBody has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.PostBody := Val;
end;

function THttp.Get_WebAccount: WideString;
begin
    Result := DefaultInterface.WebAccount;
end;

procedure THttp.Set_WebAccount(const Val: WideString);
  { Warning: The property WebAccount has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.WebAccount := Val;
end;

function THttp.Get_WebPassword: WideString;
begin
    Result := DefaultInterface.WebPassword;
end;

procedure THttp.Set_WebPassword(const Val: WideString);
  { Warning: The property WebPassword has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.WebPassword := Val;
end;

function THttp.Get_ProxyServer: WideString;
begin
    Result := DefaultInterface.ProxyServer;
end;

procedure THttp.Set_ProxyServer(const Val: WideString);
  { Warning: The property ProxyServer has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ProxyServer := Val;
end;

function THttp.Get_ProxyAccount: WideString;
begin
    Result := DefaultInterface.ProxyAccount;
end;

procedure THttp.Set_ProxyAccount(const Val: WideString);
  { Warning: The property ProxyAccount has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ProxyAccount := Val;
end;

function THttp.Get_ProxyPassword: WideString;
begin
    Result := DefaultInterface.ProxyPassword;
end;

procedure THttp.Set_ProxyPassword(const Val: WideString);
  { Warning: The property ProxyPassword has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ProxyPassword := Val;
end;

function THttp.Get_RequestTimeout: Integer;
begin
    Result := DefaultInterface.RequestTimeout;
end;

procedure THttp.Set_RequestTimeout(Val: Integer);
begin
  DefaultInterface.Set_RequestTimeout(Val);
end;

function THttp.Get_LastResponseCode: Integer;
begin
    Result := DefaultInterface.LastResponseCode;
end;

procedure THttp.SaveLicenseKey;
begin
  DefaultInterface.SaveLicenseKey;
end;

procedure THttp.Sleep(Ms: Integer);
begin
  DefaultInterface.Sleep(Ms);
end;

procedure THttp.Clear;
begin
  DefaultInterface.Clear;
end;

function THttp.GetErrorDescription(ErrorCode: Integer): WideString;
begin
  Result := DefaultInterface.GetErrorDescription(ErrorCode);
end;

function THttp.Get(const strUrl: WideString): WideString;
begin
  Result := DefaultInterface.Get(strUrl);
end;

function THttp.Post(const strUrl: WideString; const strPostBody: WideString): WideString;
begin
  Result := DefaultInterface.Post(strUrl, strPostBody);
end;

function THttp.SendSms(const Sms: IMessage; MultipartFlag: Integer): WideString;
begin
  Result := DefaultInterface.SendSms(Sms, MultipartFlag);
end;

procedure THttp.SetHeader(const Header: WideString; const Value: WideString);
begin
  DefaultInterface.SetHeader(Header, Value);
end;

function THttp.UrlEncode(const In_: WideString): WideString;
begin
  Result := DefaultInterface.UrlEncode(In_);
end;

function THttp.Base64Encode(const In_: WideString): WideString;
begin
  Result := DefaultInterface.Base64Encode(In_);
end;

function THttp.Base64EncodeFile(const FileName: WideString): WideString;
begin
  Result := DefaultInterface.Base64EncodeFile(FileName);
end;

function THttp.HexEncode(const In_: WideString): WideString;
begin
  Result := DefaultInterface.HexEncode(In_);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor THttpProperties.Create(AServer: THttp);
begin
  inherited Create;
  FServer := AServer;
end;

function THttpProperties.GetDefaultInterface: IHttp;
begin
  Result := FServer.DefaultInterface;
end;

function THttpProperties.Get_Version: WideString;
begin
    Result := DefaultInterface.Version;
end;

function THttpProperties.Get_Build: WideString;
begin
    Result := DefaultInterface.Build;
end;

function THttpProperties.Get_Module: WideString;
begin
    Result := DefaultInterface.Module;
end;

function THttpProperties.Get_LicenseStatus: WideString;
begin
    Result := DefaultInterface.LicenseStatus;
end;

function THttpProperties.Get_LicenseKey: WideString;
begin
    Result := DefaultInterface.LicenseKey;
end;

procedure THttpProperties.Set_LicenseKey(const LicenseKey: WideString);
  { Warning: The property LicenseKey has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LicenseKey := LicenseKey;
end;

function THttpProperties.Get_LogFile: WideString;
begin
    Result := DefaultInterface.LogFile;
end;

procedure THttpProperties.Set_LogFile(const LogFile: WideString);
  { Warning: The property LogFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LogFile := LogFile;
end;

function THttpProperties.Get_ActivityFile: WideString;
begin
    Result := DefaultInterface.ActivityFile;
end;

procedure THttpProperties.Set_ActivityFile(const ActivityFile: WideString);
  { Warning: The property ActivityFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ActivityFile := ActivityFile;
end;

function THttpProperties.Get_LastError: Integer;
begin
    Result := DefaultInterface.LastError;
end;

function THttpProperties.Get_Url: WideString;
begin
    Result := DefaultInterface.Url;
end;

procedure THttpProperties.Set_Url(const Val: WideString);
  { Warning: The property Url has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Url := Val;
end;

function THttpProperties.Get_PostBody: WideString;
begin
    Result := DefaultInterface.PostBody;
end;

procedure THttpProperties.Set_PostBody(const Val: WideString);
  { Warning: The property PostBody has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.PostBody := Val;
end;

function THttpProperties.Get_WebAccount: WideString;
begin
    Result := DefaultInterface.WebAccount;
end;

procedure THttpProperties.Set_WebAccount(const Val: WideString);
  { Warning: The property WebAccount has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.WebAccount := Val;
end;

function THttpProperties.Get_WebPassword: WideString;
begin
    Result := DefaultInterface.WebPassword;
end;

procedure THttpProperties.Set_WebPassword(const Val: WideString);
  { Warning: The property WebPassword has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.WebPassword := Val;
end;

function THttpProperties.Get_ProxyServer: WideString;
begin
    Result := DefaultInterface.ProxyServer;
end;

procedure THttpProperties.Set_ProxyServer(const Val: WideString);
  { Warning: The property ProxyServer has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ProxyServer := Val;
end;

function THttpProperties.Get_ProxyAccount: WideString;
begin
    Result := DefaultInterface.ProxyAccount;
end;

procedure THttpProperties.Set_ProxyAccount(const Val: WideString);
  { Warning: The property ProxyAccount has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ProxyAccount := Val;
end;

function THttpProperties.Get_ProxyPassword: WideString;
begin
    Result := DefaultInterface.ProxyPassword;
end;

procedure THttpProperties.Set_ProxyPassword(const Val: WideString);
  { Warning: The property ProxyPassword has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ProxyPassword := Val;
end;

function THttpProperties.Get_RequestTimeout: Integer;
begin
    Result := DefaultInterface.RequestTimeout;
end;

procedure THttpProperties.Set_RequestTimeout(Val: Integer);
begin
  DefaultInterface.Set_RequestTimeout(Val);
end;

function THttpProperties.Get_LastResponseCode: Integer;
begin
    Result := DefaultInterface.LastResponseCode;
end;

{$ENDIF}

class function CoSnpp.Create: ISnpp;
begin
  Result := CreateComObject(CLASS_Snpp) as ISnpp;
end;

class function CoSnpp.CreateRemote(const MachineName: string): ISnpp;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Snpp) as ISnpp;
end;

procedure TSnpp.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{929AEEE5-56B2-4A67-BCC8-45E5D4FA3739}';
    IntfIID:   '{C4AAB6D2-F834-4FE6-A4AE-B1D5794BC3B4}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TSnpp.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as ISnpp;
  end;
end;

procedure TSnpp.ConnectTo(svrIntf: ISnpp);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TSnpp.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TSnpp.GetDefaultInterface: ISnpp;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TSnpp.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TSnppProperties.Create(Self);
{$ENDIF}
end;

destructor TSnpp.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TSnpp.GetServerProperties: TSnppProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TSnpp.Get_Version: WideString;
begin
    Result := DefaultInterface.Version;
end;

function TSnpp.Get_Build: WideString;
begin
    Result := DefaultInterface.Build;
end;

function TSnpp.Get_Module: WideString;
begin
    Result := DefaultInterface.Module;
end;

function TSnpp.Get_LicenseStatus: WideString;
begin
    Result := DefaultInterface.LicenseStatus;
end;

function TSnpp.Get_LicenseKey: WideString;
begin
    Result := DefaultInterface.LicenseKey;
end;

procedure TSnpp.Set_LicenseKey(const LicenseKey: WideString);
  { Warning: The property LicenseKey has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LicenseKey := LicenseKey;
end;

function TSnpp.Get_LogFile: WideString;
begin
    Result := DefaultInterface.LogFile;
end;

procedure TSnpp.Set_LogFile(const LogFile: WideString);
  { Warning: The property LogFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LogFile := LogFile;
end;

function TSnpp.Get_LastError: Integer;
begin
    Result := DefaultInterface.LastError;
end;

function TSnpp.Get_Server: WideString;
begin
    Result := DefaultInterface.Server;
end;

procedure TSnpp.Set_Server(const Val: WideString);
  { Warning: The property Server has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Server := Val;
end;

function TSnpp.Get_ServerPort: Integer;
begin
    Result := DefaultInterface.ServerPort;
end;

procedure TSnpp.Set_ServerPort(Val: Integer);
begin
  DefaultInterface.Set_ServerPort(Val);
end;

function TSnpp.Get_ServerTimeout: Integer;
begin
    Result := DefaultInterface.ServerTimeout;
end;

procedure TSnpp.Set_ServerTimeout(Val: Integer);
begin
  DefaultInterface.Set_ServerTimeout(Val);
end;

function TSnpp.Get_ProviderPassword: WideString;
begin
    Result := DefaultInterface.ProviderPassword;
end;

procedure TSnpp.Set_ProviderPassword(const Val: WideString);
  { Warning: The property ProviderPassword has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ProviderPassword := Val;
end;

function TSnpp.Get_ProviderUsername: WideString;
begin
    Result := DefaultInterface.ProviderUsername;
end;

procedure TSnpp.Set_ProviderUsername(const Val: WideString);
  { Warning: The property ProviderUsername has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ProviderUsername := Val;
end;

function TSnpp.Get_ProviderResponse: WideString;
begin
    Result := DefaultInterface.ProviderResponse;
end;

procedure TSnpp.SaveLicenseKey;
begin
  DefaultInterface.SaveLicenseKey;
end;

procedure TSnpp.Clear;
begin
  DefaultInterface.Clear;
end;

function TSnpp.GetErrorDescription(ErrorCode: Integer): WideString;
begin
  Result := DefaultInterface.GetErrorDescription(ErrorCode);
end;

procedure TSnpp.Send(const ToAddress: WideString; const Message: WideString);
begin
  DefaultInterface.Send(ToAddress, Message);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TSnppProperties.Create(AServer: TSnpp);
begin
  inherited Create;
  FServer := AServer;
end;

function TSnppProperties.GetDefaultInterface: ISnpp;
begin
  Result := FServer.DefaultInterface;
end;

function TSnppProperties.Get_Version: WideString;
begin
    Result := DefaultInterface.Version;
end;

function TSnppProperties.Get_Build: WideString;
begin
    Result := DefaultInterface.Build;
end;

function TSnppProperties.Get_Module: WideString;
begin
    Result := DefaultInterface.Module;
end;

function TSnppProperties.Get_LicenseStatus: WideString;
begin
    Result := DefaultInterface.LicenseStatus;
end;

function TSnppProperties.Get_LicenseKey: WideString;
begin
    Result := DefaultInterface.LicenseKey;
end;

procedure TSnppProperties.Set_LicenseKey(const LicenseKey: WideString);
  { Warning: The property LicenseKey has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LicenseKey := LicenseKey;
end;

function TSnppProperties.Get_LogFile: WideString;
begin
    Result := DefaultInterface.LogFile;
end;

procedure TSnppProperties.Set_LogFile(const LogFile: WideString);
  { Warning: The property LogFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LogFile := LogFile;
end;

function TSnppProperties.Get_LastError: Integer;
begin
    Result := DefaultInterface.LastError;
end;

function TSnppProperties.Get_Server: WideString;
begin
    Result := DefaultInterface.Server;
end;

procedure TSnppProperties.Set_Server(const Val: WideString);
  { Warning: The property Server has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Server := Val;
end;

function TSnppProperties.Get_ServerPort: Integer;
begin
    Result := DefaultInterface.ServerPort;
end;

procedure TSnppProperties.Set_ServerPort(Val: Integer);
begin
  DefaultInterface.Set_ServerPort(Val);
end;

function TSnppProperties.Get_ServerTimeout: Integer;
begin
    Result := DefaultInterface.ServerTimeout;
end;

procedure TSnppProperties.Set_ServerTimeout(Val: Integer);
begin
  DefaultInterface.Set_ServerTimeout(Val);
end;

function TSnppProperties.Get_ProviderPassword: WideString;
begin
    Result := DefaultInterface.ProviderPassword;
end;

procedure TSnppProperties.Set_ProviderPassword(const Val: WideString);
  { Warning: The property ProviderPassword has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ProviderPassword := Val;
end;

function TSnppProperties.Get_ProviderUsername: WideString;
begin
    Result := DefaultInterface.ProviderUsername;
end;

procedure TSnppProperties.Set_ProviderUsername(const Val: WideString);
  { Warning: The property ProviderUsername has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ProviderUsername := Val;
end;

function TSnppProperties.Get_ProviderResponse: WideString;
begin
    Result := DefaultInterface.ProviderResponse;
end;

{$ENDIF}

class function CoSmpp.Create: ISmpp;
begin
  Result := CreateComObject(CLASS_Smpp) as ISmpp;
end;

class function CoSmpp.CreateRemote(const MachineName: string): ISmpp;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Smpp) as ISmpp;
end;

procedure TSmpp.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{A1453018-3C02-4C22-A136-3CF9B78CEAC8}';
    IntfIID:   '{9E5FC25E-613A-4951-9C8F-37923C7AE7AE}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TSmpp.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as ISmpp;
  end;
end;

procedure TSmpp.ConnectTo(svrIntf: ISmpp);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TSmpp.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TSmpp.GetDefaultInterface: ISmpp;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TSmpp.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TSmppProperties.Create(Self);
{$ENDIF}
end;

destructor TSmpp.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TSmpp.GetServerProperties: TSmppProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TSmpp.Get_Version: WideString;
begin
    Result := DefaultInterface.Version;
end;

function TSmpp.Get_Build: WideString;
begin
    Result := DefaultInterface.Build;
end;

function TSmpp.Get_Module: WideString;
begin
    Result := DefaultInterface.Module;
end;

function TSmpp.Get_LicenseStatus: WideString;
begin
    Result := DefaultInterface.LicenseStatus;
end;

function TSmpp.Get_LicenseKey: WideString;
begin
    Result := DefaultInterface.LicenseKey;
end;

procedure TSmpp.Set_LicenseKey(const LicenseKey: WideString);
  { Warning: The property LicenseKey has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LicenseKey := LicenseKey;
end;

function TSmpp.Get_LogFile: WideString;
begin
    Result := DefaultInterface.LogFile;
end;

procedure TSmpp.Set_LogFile(const LogFile: WideString);
  { Warning: The property LogFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LogFile := LogFile;
end;

function TSmpp.Get_ActivityFile: WideString;
begin
    Result := DefaultInterface.ActivityFile;
end;

procedure TSmpp.Set_ActivityFile(const ActivityFile: WideString);
  { Warning: The property ActivityFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ActivityFile := ActivityFile;
end;

function TSmpp.Get_LogPduDetails: WordBool;
begin
    Result := DefaultInterface.LogPduDetails;
end;

procedure TSmpp.Set_LogPduDetails(LogDetails: WordBool);
begin
  DefaultInterface.Set_LogPduDetails(LogDetails);
end;

function TSmpp.Get_LastError: Integer;
begin
    Result := DefaultInterface.LastError;
end;

function TSmpp.Get_IsConnected: WordBool;
begin
    Result := DefaultInterface.IsConnected;
end;

function TSmpp.Get_IsBound: WordBool;
begin
    Result := DefaultInterface.IsBound;
end;

function TSmpp.Get_MaxOutPendingPdus: Integer;
begin
    Result := DefaultInterface.MaxOutPendingPdus;
end;

procedure TSmpp.Set_MaxOutPendingPdus(Val: Integer);
begin
  DefaultInterface.Set_MaxOutPendingPdus(Val);
end;

function TSmpp.Get_MaxSmsSubmissions: Integer;
begin
    Result := DefaultInterface.MaxSmsSubmissions;
end;

procedure TSmpp.Set_MaxSmsSubmissions(Val: Integer);
begin
  DefaultInterface.Set_MaxSmsSubmissions(Val);
end;

function TSmpp.Get_MaxSmsQueries: Integer;
begin
    Result := DefaultInterface.MaxSmsQueries;
end;

procedure TSmpp.Set_MaxSmsQueries(Val: Integer);
begin
  DefaultInterface.Set_MaxSmsQueries(Val);
end;

function TSmpp.Get_PduTimeout: Integer;
begin
    Result := DefaultInterface.PduTimeout;
end;

procedure TSmpp.Set_PduTimeout(Val: Integer);
begin
  DefaultInterface.Set_PduTimeout(Val);
end;

function TSmpp.Get_EnquireInterval: Integer;
begin
    Result := DefaultInterface.EnquireInterval;
end;

procedure TSmpp.Set_EnquireInterval(Val: Integer);
begin
  DefaultInterface.Set_EnquireInterval(Val);
end;

function TSmpp.Get_MultipartTimeout: Integer;
begin
    Result := DefaultInterface.MultipartTimeout;
end;

procedure TSmpp.Set_MultipartTimeout(Val: Integer);
begin
  DefaultInterface.Set_MultipartTimeout(Val);
end;

function TSmpp.Get_UseGsmEncoding: Integer;
begin
    Result := DefaultInterface.UseGsmEncoding;
end;

procedure TSmpp.Set_UseGsmEncoding(Val: Integer);
begin
  DefaultInterface.Set_UseGsmEncoding(Val);
end;

function TSmpp.Get_AssembleMultipart: WordBool;
begin
    Result := DefaultInterface.AssembleMultipart;
end;

procedure TSmpp.Set_AssembleMultipart(Val: WordBool);
begin
  DefaultInterface.Set_AssembleMultipart(Val);
end;

function TSmpp.Get_MultipartMode: Integer;
begin
    Result := DefaultInterface.MultipartMode;
end;

procedure TSmpp.Set_MultipartMode(Val: Integer);
begin
  DefaultInterface.Set_MultipartMode(Val);
end;

function TSmpp.Get_ExtractApplicationPort: WordBool;
begin
    Result := DefaultInterface.ExtractApplicationPort;
end;

procedure TSmpp.Set_ExtractApplicationPort(Val: WordBool);
begin
  DefaultInterface.Set_ExtractApplicationPort(Val);
end;

function TSmpp.Get_MaxSmsReceived: Integer;
begin
    Result := DefaultInterface.MaxSmsReceived;
end;

procedure TSmpp.Set_MaxSmsReceived(Val: Integer);
begin
  DefaultInterface.Set_MaxSmsReceived(Val);
end;

function TSmpp.Get_SubmitMode: Integer;
begin
    Result := DefaultInterface.SubmitMode;
end;

procedure TSmpp.Set_SubmitMode(Val: Integer);
begin
  DefaultInterface.Set_SubmitMode(Val);
end;

function TSmpp.Get_SmsSentPerSecond: Integer;
begin
    Result := DefaultInterface.SmsSentPerSecond;
end;

function TSmpp.Get_SmsReceivedPerSecond: Integer;
begin
    Result := DefaultInterface.SmsReceivedPerSecond;
end;

procedure TSmpp.SaveLicenseKey;
begin
  DefaultInterface.SaveLicenseKey;
end;

procedure TSmpp.Sleep(Ms: Integer);
begin
  DefaultInterface.Sleep(Ms);
end;

procedure TSmpp.Clear;
begin
  DefaultInterface.Clear;
end;

function TSmpp.GetErrorDescription(ErrorCode: Integer): WideString;
begin
  Result := DefaultInterface.GetErrorDescription(ErrorCode);
end;

procedure TSmpp.Connect1(const Host: WideString; Port: Integer; Timeout: Integer);
begin
  DefaultInterface.Connect(Host, Port, Timeout);
end;

procedure TSmpp.Bind(Type_: Integer; const SystemId: WideString; const SystemPassword: WideString; 
                     const SystemType: WideString; Version: Integer; TON: Integer; NPI: Integer; 
                     const AddressRange: WideString; Timeout: Integer);
begin
  DefaultInterface.Bind(Type_, SystemId, SystemPassword, SystemType, Version, TON, NPI, 
                        AddressRange, Timeout);
end;

procedure TSmpp.Unbind;
begin
  DefaultInterface.Unbind;
end;

procedure TSmpp.Disconnect1;
begin
  DefaultInterface.Disconnect;
end;

procedure TSmpp.AddBindTlv(const Tlv: ITlv);
begin
  DefaultInterface.AddBindTlv(Tlv);
end;

function TSmpp.SubmitSms(const Sms: IMessage; MultipartFlag: Integer): Integer;
begin
  Result := DefaultInterface.SubmitSms(Sms, MultipartFlag);
end;

function TSmpp.WaitForSmsUpdate(TimeoutMs: Integer): WordBool;
begin
  Result := DefaultInterface.WaitForSmsUpdate(TimeoutMs);
end;

function TSmpp.FetchSmsUpdate: IMessage;
begin
  Result := DefaultInterface.FetchSmsUpdate;
end;

procedure TSmpp.QuerySms(const Sms: IMessage);
begin
  DefaultInterface.QuerySms(Sms);
end;

function TSmpp.ReceiveMessage: IMessage;
begin
  Result := DefaultInterface.ReceiveMessage;
end;

function TSmpp.CountSmsReceived: Integer;
begin
  Result := DefaultInterface.CountSmsReceived;
end;

function TSmpp.CountSmsSubmitSpace: Integer;
begin
  Result := DefaultInterface.CountSmsSubmitSpace;
end;

function TSmpp.CountSmsQuerySpace: Integer;
begin
  Result := DefaultInterface.CountSmsQuerySpace;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TSmppProperties.Create(AServer: TSmpp);
begin
  inherited Create;
  FServer := AServer;
end;

function TSmppProperties.GetDefaultInterface: ISmpp;
begin
  Result := FServer.DefaultInterface;
end;

function TSmppProperties.Get_Version: WideString;
begin
    Result := DefaultInterface.Version;
end;

function TSmppProperties.Get_Build: WideString;
begin
    Result := DefaultInterface.Build;
end;

function TSmppProperties.Get_Module: WideString;
begin
    Result := DefaultInterface.Module;
end;

function TSmppProperties.Get_LicenseStatus: WideString;
begin
    Result := DefaultInterface.LicenseStatus;
end;

function TSmppProperties.Get_LicenseKey: WideString;
begin
    Result := DefaultInterface.LicenseKey;
end;

procedure TSmppProperties.Set_LicenseKey(const LicenseKey: WideString);
  { Warning: The property LicenseKey has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LicenseKey := LicenseKey;
end;

function TSmppProperties.Get_LogFile: WideString;
begin
    Result := DefaultInterface.LogFile;
end;

procedure TSmppProperties.Set_LogFile(const LogFile: WideString);
  { Warning: The property LogFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LogFile := LogFile;
end;

function TSmppProperties.Get_ActivityFile: WideString;
begin
    Result := DefaultInterface.ActivityFile;
end;

procedure TSmppProperties.Set_ActivityFile(const ActivityFile: WideString);
  { Warning: The property ActivityFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ActivityFile := ActivityFile;
end;

function TSmppProperties.Get_LogPduDetails: WordBool;
begin
    Result := DefaultInterface.LogPduDetails;
end;

procedure TSmppProperties.Set_LogPduDetails(LogDetails: WordBool);
begin
  DefaultInterface.Set_LogPduDetails(LogDetails);
end;

function TSmppProperties.Get_LastError: Integer;
begin
    Result := DefaultInterface.LastError;
end;

function TSmppProperties.Get_IsConnected: WordBool;
begin
    Result := DefaultInterface.IsConnected;
end;

function TSmppProperties.Get_IsBound: WordBool;
begin
    Result := DefaultInterface.IsBound;
end;

function TSmppProperties.Get_MaxOutPendingPdus: Integer;
begin
    Result := DefaultInterface.MaxOutPendingPdus;
end;

procedure TSmppProperties.Set_MaxOutPendingPdus(Val: Integer);
begin
  DefaultInterface.Set_MaxOutPendingPdus(Val);
end;

function TSmppProperties.Get_MaxSmsSubmissions: Integer;
begin
    Result := DefaultInterface.MaxSmsSubmissions;
end;

procedure TSmppProperties.Set_MaxSmsSubmissions(Val: Integer);
begin
  DefaultInterface.Set_MaxSmsSubmissions(Val);
end;

function TSmppProperties.Get_MaxSmsQueries: Integer;
begin
    Result := DefaultInterface.MaxSmsQueries;
end;

procedure TSmppProperties.Set_MaxSmsQueries(Val: Integer);
begin
  DefaultInterface.Set_MaxSmsQueries(Val);
end;

function TSmppProperties.Get_PduTimeout: Integer;
begin
    Result := DefaultInterface.PduTimeout;
end;

procedure TSmppProperties.Set_PduTimeout(Val: Integer);
begin
  DefaultInterface.Set_PduTimeout(Val);
end;

function TSmppProperties.Get_EnquireInterval: Integer;
begin
    Result := DefaultInterface.EnquireInterval;
end;

procedure TSmppProperties.Set_EnquireInterval(Val: Integer);
begin
  DefaultInterface.Set_EnquireInterval(Val);
end;

function TSmppProperties.Get_MultipartTimeout: Integer;
begin
    Result := DefaultInterface.MultipartTimeout;
end;

procedure TSmppProperties.Set_MultipartTimeout(Val: Integer);
begin
  DefaultInterface.Set_MultipartTimeout(Val);
end;

function TSmppProperties.Get_UseGsmEncoding: Integer;
begin
    Result := DefaultInterface.UseGsmEncoding;
end;

procedure TSmppProperties.Set_UseGsmEncoding(Val: Integer);
begin
  DefaultInterface.Set_UseGsmEncoding(Val);
end;

function TSmppProperties.Get_AssembleMultipart: WordBool;
begin
    Result := DefaultInterface.AssembleMultipart;
end;

procedure TSmppProperties.Set_AssembleMultipart(Val: WordBool);
begin
  DefaultInterface.Set_AssembleMultipart(Val);
end;

function TSmppProperties.Get_MultipartMode: Integer;
begin
    Result := DefaultInterface.MultipartMode;
end;

procedure TSmppProperties.Set_MultipartMode(Val: Integer);
begin
  DefaultInterface.Set_MultipartMode(Val);
end;

function TSmppProperties.Get_ExtractApplicationPort: WordBool;
begin
    Result := DefaultInterface.ExtractApplicationPort;
end;

procedure TSmppProperties.Set_ExtractApplicationPort(Val: WordBool);
begin
  DefaultInterface.Set_ExtractApplicationPort(Val);
end;

function TSmppProperties.Get_MaxSmsReceived: Integer;
begin
    Result := DefaultInterface.MaxSmsReceived;
end;

procedure TSmppProperties.Set_MaxSmsReceived(Val: Integer);
begin
  DefaultInterface.Set_MaxSmsReceived(Val);
end;

function TSmppProperties.Get_SubmitMode: Integer;
begin
    Result := DefaultInterface.SubmitMode;
end;

procedure TSmppProperties.Set_SubmitMode(Val: Integer);
begin
  DefaultInterface.Set_SubmitMode(Val);
end;

function TSmppProperties.Get_SmsSentPerSecond: Integer;
begin
    Result := DefaultInterface.SmsSentPerSecond;
end;

function TSmppProperties.Get_SmsReceivedPerSecond: Integer;
begin
    Result := DefaultInterface.SmsReceivedPerSecond;
end;

{$ENDIF}

class function CoSmppServer.Create: ISmppServer;
begin
  Result := CreateComObject(CLASS_SmppServer) as ISmppServer;
end;

class function CoSmppServer.CreateRemote(const MachineName: string): ISmppServer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SmppServer) as ISmppServer;
end;

procedure TSmppServer.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{6F2D0285-924E-4F76-A0EA-FC9282D4C761}';
    IntfIID:   '{EBE10239-A8C6-46CA-BC48-0CFA6CFD835C}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TSmppServer.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as ISmppServer;
  end;
end;

procedure TSmppServer.ConnectTo(svrIntf: ISmppServer);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TSmppServer.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TSmppServer.GetDefaultInterface: ISmppServer;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TSmppServer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TSmppServerProperties.Create(Self);
{$ENDIF}
end;

destructor TSmppServer.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TSmppServer.GetServerProperties: TSmppServerProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TSmppServer.Get_Version: WideString;
begin
    Result := DefaultInterface.Version;
end;

function TSmppServer.Get_Build: WideString;
begin
    Result := DefaultInterface.Build;
end;

function TSmppServer.Get_Module: WideString;
begin
    Result := DefaultInterface.Module;
end;

function TSmppServer.Get_LicenseStatus: WideString;
begin
    Result := DefaultInterface.LicenseStatus;
end;

function TSmppServer.Get_LicenseKey: WideString;
begin
    Result := DefaultInterface.LicenseKey;
end;

procedure TSmppServer.Set_LicenseKey(const LicenseKey: WideString);
  { Warning: The property LicenseKey has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LicenseKey := LicenseKey;
end;

function TSmppServer.Get_LogFile: WideString;
begin
    Result := DefaultInterface.LogFile;
end;

procedure TSmppServer.Set_LogFile(const LogFile: WideString);
  { Warning: The property LogFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LogFile := LogFile;
end;

function TSmppServer.Get_ActivityFile: WideString;
begin
    Result := DefaultInterface.ActivityFile;
end;

procedure TSmppServer.Set_ActivityFile(const ActivityFile: WideString);
  { Warning: The property ActivityFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ActivityFile := ActivityFile;
end;

function TSmppServer.Get_LastError: Integer;
begin
    Result := DefaultInterface.LastError;
end;

function TSmppServer.Get_IsStarted: WordBool;
begin
    Result := DefaultInterface.IsStarted;
end;

function TSmppServer.Get_LastReference: Integer;
begin
    Result := DefaultInterface.LastReference;
end;

procedure TSmppServer.Set_LastReference(Val: Integer);
begin
  DefaultInterface.Set_LastReference(Val);
end;

procedure TSmppServer.SaveLicenseKey;
begin
  DefaultInterface.SaveLicenseKey;
end;

procedure TSmppServer.Sleep(Ms: Integer);
begin
  DefaultInterface.Sleep(Ms);
end;

procedure TSmppServer.Clear;
begin
  DefaultInterface.Clear;
end;

function TSmppServer.GetErrorDescription(ErrorCode: Integer): WideString;
begin
  Result := DefaultInterface.GetErrorDescription(ErrorCode);
end;

procedure TSmppServer.Start(Port: Integer; IpVersion: Integer);
begin
  DefaultInterface.Start(Port, IpVersion);
end;

procedure TSmppServer.Stop;
begin
  DefaultInterface.Stop;
end;

function TSmppServer.GetFirstSession: ISmppSession;
begin
  Result := DefaultInterface.GetFirstSession;
end;

function TSmppServer.GetNextSession: ISmppSession;
begin
  Result := DefaultInterface.GetNextSession;
end;

function TSmppServer.GetSession(Id: Integer): ISmppSession;
begin
  Result := DefaultInterface.GetSession(Id);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TSmppServerProperties.Create(AServer: TSmppServer);
begin
  inherited Create;
  FServer := AServer;
end;

function TSmppServerProperties.GetDefaultInterface: ISmppServer;
begin
  Result := FServer.DefaultInterface;
end;

function TSmppServerProperties.Get_Version: WideString;
begin
    Result := DefaultInterface.Version;
end;

function TSmppServerProperties.Get_Build: WideString;
begin
    Result := DefaultInterface.Build;
end;

function TSmppServerProperties.Get_Module: WideString;
begin
    Result := DefaultInterface.Module;
end;

function TSmppServerProperties.Get_LicenseStatus: WideString;
begin
    Result := DefaultInterface.LicenseStatus;
end;

function TSmppServerProperties.Get_LicenseKey: WideString;
begin
    Result := DefaultInterface.LicenseKey;
end;

procedure TSmppServerProperties.Set_LicenseKey(const LicenseKey: WideString);
  { Warning: The property LicenseKey has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LicenseKey := LicenseKey;
end;

function TSmppServerProperties.Get_LogFile: WideString;
begin
    Result := DefaultInterface.LogFile;
end;

procedure TSmppServerProperties.Set_LogFile(const LogFile: WideString);
  { Warning: The property LogFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LogFile := LogFile;
end;

function TSmppServerProperties.Get_ActivityFile: WideString;
begin
    Result := DefaultInterface.ActivityFile;
end;

procedure TSmppServerProperties.Set_ActivityFile(const ActivityFile: WideString);
  { Warning: The property ActivityFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ActivityFile := ActivityFile;
end;

function TSmppServerProperties.Get_LastError: Integer;
begin
    Result := DefaultInterface.LastError;
end;

function TSmppServerProperties.Get_IsStarted: WordBool;
begin
    Result := DefaultInterface.IsStarted;
end;

function TSmppServerProperties.Get_LastReference: Integer;
begin
    Result := DefaultInterface.LastReference;
end;

procedure TSmppServerProperties.Set_LastReference(Val: Integer);
begin
  DefaultInterface.Set_LastReference(Val);
end;

{$ENDIF}

class function CoSmppSession.Create: ISmppSession;
begin
  Result := CreateComObject(CLASS_SmppSession) as ISmppSession;
end;

class function CoSmppSession.CreateRemote(const MachineName: string): ISmppSession;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SmppSession) as ISmppSession;
end;

procedure TSmppSession.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{A0D488D4-C668-4346-9D44-3CD00832723E}';
    IntfIID:   '{3E1FF0C2-4F98-4FA2-954B-BF5730FB6CF9}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TSmppSession.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as ISmppSession;
  end;
end;

procedure TSmppSession.ConnectTo(svrIntf: ISmppSession);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TSmppSession.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TSmppSession.GetDefaultInterface: ISmppSession;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TSmppSession.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TSmppSessionProperties.Create(Self);
{$ENDIF}
end;

destructor TSmppSession.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TSmppSession.GetServerProperties: TSmppSessionProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TSmppSession.Get_LogFile: WideString;
begin
    Result := DefaultInterface.LogFile;
end;

procedure TSmppSession.Set_LogFile(const LogFile: WideString);
  { Warning: The property LogFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LogFile := LogFile;
end;

function TSmppSession.Get_ActivityFile: WideString;
begin
    Result := DefaultInterface.ActivityFile;
end;

procedure TSmppSession.Set_ActivityFile(const ActivityFile: WideString);
  { Warning: The property ActivityFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ActivityFile := ActivityFile;
end;

function TSmppSession.Get_LogPduDetails: WordBool;
begin
    Result := DefaultInterface.LogPduDetails;
end;

procedure TSmppSession.Set_LogPduDetails(LogDetails: WordBool);
begin
  DefaultInterface.Set_LogPduDetails(LogDetails);
end;

function TSmppSession.Get_LastError: Integer;
begin
    Result := DefaultInterface.LastError;
end;

function TSmppSession.Get_Ip: WideString;
begin
    Result := DefaultInterface.Ip;
end;

function TSmppSession.Get_Port: Integer;
begin
    Result := DefaultInterface.Port;
end;

function TSmppSession.Get_Version: Integer;
begin
    Result := DefaultInterface.Version;
end;

function TSmppSession.Get_SystemId: WideString;
begin
    Result := DefaultInterface.SystemId;
end;

function TSmppSession.Get_Password: WideString;
begin
    Result := DefaultInterface.Password;
end;

function TSmppSession.Get_SystemType: WideString;
begin
    Result := DefaultInterface.SystemType;
end;

function TSmppSession.Get_AddressRange: WideString;
begin
    Result := DefaultInterface.AddressRange;
end;

function TSmppSession.Get_AddressRangeNpi: Integer;
begin
    Result := DefaultInterface.AddressRangeNpi;
end;

function TSmppSession.Get_AddressRangeTon: Integer;
begin
    Result := DefaultInterface.AddressRangeTon;
end;

function TSmppSession.Get_ConnectionState: Integer;
begin
    Result := DefaultInterface.ConnectionState;
end;

function TSmppSession.Get_RequestedBind: Integer;
begin
    Result := DefaultInterface.RequestedBind;
end;

function TSmppSession.Get_Id: Integer;
begin
    Result := DefaultInterface.Id;
end;

function TSmppSession.Get_MaxSmsDeliveries: Integer;
begin
    Result := DefaultInterface.MaxSmsDeliveries;
end;

procedure TSmppSession.Set_MaxSmsDeliveries(Val: Integer);
begin
  DefaultInterface.Set_MaxSmsDeliveries(Val);
end;

function TSmppSession.Get_MaxSmsSubmission: Integer;
begin
    Result := DefaultInterface.MaxSmsSubmission;
end;

procedure TSmppSession.Set_MaxSmsSubmission(Val: Integer);
begin
  DefaultInterface.Set_MaxSmsSubmission(Val);
end;

function TSmppSession.Get_MaxSmsQueries: Integer;
begin
    Result := DefaultInterface.MaxSmsQueries;
end;

procedure TSmppSession.Set_MaxSmsQueries(Val: Integer);
begin
  DefaultInterface.Set_MaxSmsQueries(Val);
end;

function TSmppSession.Get_SmsSentPerSecond: Integer;
begin
    Result := DefaultInterface.SmsSentPerSecond;
end;

function TSmppSession.Get_SmsReceivedPerSecond: Integer;
begin
    Result := DefaultInterface.SmsReceivedPerSecond;
end;

function TSmppSession.GetErrorDescription(ErrorCode: Integer): WideString;
begin
  Result := DefaultInterface.GetErrorDescription(ErrorCode);
end;

procedure TSmppSession.RespondToBind(Status: Integer);
begin
  DefaultInterface.RespondToBind(Status);
end;

procedure TSmppSession.RespondToSubmitSms(const Sms: IMessage);
begin
  DefaultInterface.RespondToSubmitSms(Sms);
end;

function TSmppSession.ReceiveSubmitSms: IMessage;
begin
  Result := DefaultInterface.ReceiveSubmitSms;
end;

procedure TSmppSession.RespondToQuerySms(const Sms: IMessage);
begin
  DefaultInterface.RespondToQuerySms(Sms);
end;

function TSmppSession.ReceiveQuerySms: IMessage;
begin
  Result := DefaultInterface.ReceiveQuerySms;
end;

procedure TSmppSession.DeliverSms(const Sms: IMessage);
begin
  DefaultInterface.DeliverSms(Sms);
end;

procedure TSmppSession.DeliverReport(const Sms: IMessage);
begin
  DefaultInterface.DeliverReport(Sms);
end;

function TSmppSession.ReceiveDeliverResponse: IMessage;
begin
  Result := DefaultInterface.ReceiveDeliverResponse;
end;

procedure TSmppSession.Disconnect1;
begin
  DefaultInterface.Disconnect;
end;

function TSmppSession.CountSmsSubmissions: Integer;
begin
  Result := DefaultInterface.CountSmsSubmissions;
end;

function TSmppSession.CountSmsQueries: Integer;
begin
  Result := DefaultInterface.CountSmsQueries;
end;

function TSmppSession.CountSmsDeliverySpace: Integer;
begin
  Result := DefaultInterface.CountSmsDeliverySpace;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TSmppSessionProperties.Create(AServer: TSmppSession);
begin
  inherited Create;
  FServer := AServer;
end;

function TSmppSessionProperties.GetDefaultInterface: ISmppSession;
begin
  Result := FServer.DefaultInterface;
end;

function TSmppSessionProperties.Get_LogFile: WideString;
begin
    Result := DefaultInterface.LogFile;
end;

procedure TSmppSessionProperties.Set_LogFile(const LogFile: WideString);
  { Warning: The property LogFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.LogFile := LogFile;
end;

function TSmppSessionProperties.Get_ActivityFile: WideString;
begin
    Result := DefaultInterface.ActivityFile;
end;

procedure TSmppSessionProperties.Set_ActivityFile(const ActivityFile: WideString);
  { Warning: The property ActivityFile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ActivityFile := ActivityFile;
end;

function TSmppSessionProperties.Get_LogPduDetails: WordBool;
begin
    Result := DefaultInterface.LogPduDetails;
end;

procedure TSmppSessionProperties.Set_LogPduDetails(LogDetails: WordBool);
begin
  DefaultInterface.Set_LogPduDetails(LogDetails);
end;

function TSmppSessionProperties.Get_LastError: Integer;
begin
    Result := DefaultInterface.LastError;
end;

function TSmppSessionProperties.Get_Ip: WideString;
begin
    Result := DefaultInterface.Ip;
end;

function TSmppSessionProperties.Get_Port: Integer;
begin
    Result := DefaultInterface.Port;
end;

function TSmppSessionProperties.Get_Version: Integer;
begin
    Result := DefaultInterface.Version;
end;

function TSmppSessionProperties.Get_SystemId: WideString;
begin
    Result := DefaultInterface.SystemId;
end;

function TSmppSessionProperties.Get_Password: WideString;
begin
    Result := DefaultInterface.Password;
end;

function TSmppSessionProperties.Get_SystemType: WideString;
begin
    Result := DefaultInterface.SystemType;
end;

function TSmppSessionProperties.Get_AddressRange: WideString;
begin
    Result := DefaultInterface.AddressRange;
end;

function TSmppSessionProperties.Get_AddressRangeNpi: Integer;
begin
    Result := DefaultInterface.AddressRangeNpi;
end;

function TSmppSessionProperties.Get_AddressRangeTon: Integer;
begin
    Result := DefaultInterface.AddressRangeTon;
end;

function TSmppSessionProperties.Get_ConnectionState: Integer;
begin
    Result := DefaultInterface.ConnectionState;
end;

function TSmppSessionProperties.Get_RequestedBind: Integer;
begin
    Result := DefaultInterface.RequestedBind;
end;

function TSmppSessionProperties.Get_Id: Integer;
begin
    Result := DefaultInterface.Id;
end;

function TSmppSessionProperties.Get_MaxSmsDeliveries: Integer;
begin
    Result := DefaultInterface.MaxSmsDeliveries;
end;

procedure TSmppSessionProperties.Set_MaxSmsDeliveries(Val: Integer);
begin
  DefaultInterface.Set_MaxSmsDeliveries(Val);
end;

function TSmppSessionProperties.Get_MaxSmsSubmission: Integer;
begin
    Result := DefaultInterface.MaxSmsSubmission;
end;

procedure TSmppSessionProperties.Set_MaxSmsSubmission(Val: Integer);
begin
  DefaultInterface.Set_MaxSmsSubmission(Val);
end;

function TSmppSessionProperties.Get_MaxSmsQueries: Integer;
begin
    Result := DefaultInterface.MaxSmsQueries;
end;

procedure TSmppSessionProperties.Set_MaxSmsQueries(Val: Integer);
begin
  DefaultInterface.Set_MaxSmsQueries(Val);
end;

function TSmppSessionProperties.Get_SmsSentPerSecond: Integer;
begin
    Result := DefaultInterface.SmsSentPerSecond;
end;

function TSmppSessionProperties.Get_SmsReceivedPerSecond: Integer;
begin
    Result := DefaultInterface.SmsReceivedPerSecond;
end;

{$ENDIF}

class function CoTemplateWapPush.Create: ITemplateWapPush;
begin
  Result := CreateComObject(CLASS_TemplateWapPush) as ITemplateWapPush;
end;

class function CoTemplateWapPush.CreateRemote(const MachineName: string): ITemplateWapPush;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_TemplateWapPush) as ITemplateWapPush;
end;

procedure TTemplateWapPush.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{FA1CDBE0-666B-430F-BCC5-838661432F76}';
    IntfIID:   '{54DD400B-F0D6-42AF-964B-9F38CD57D8CC}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TTemplateWapPush.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as ITemplateWapPush;
  end;
end;

procedure TTemplateWapPush.ConnectTo(svrIntf: ITemplateWapPush);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TTemplateWapPush.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TTemplateWapPush.GetDefaultInterface: ITemplateWapPush;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TTemplateWapPush.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TTemplateWapPushProperties.Create(Self);
{$ENDIF}
end;

destructor TTemplateWapPush.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TTemplateWapPush.GetServerProperties: TTemplateWapPushProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TTemplateWapPush.Get_Url: WideString;
begin
    Result := DefaultInterface.Url;
end;

procedure TTemplateWapPush.Set_Url(const pVal: WideString);
  { Warning: The property Url has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Url := pVal;
end;

function TTemplateWapPush.Get_Description: WideString;
begin
    Result := DefaultInterface.Description;
end;

procedure TTemplateWapPush.Set_Description(const pVal: WideString);
  { Warning: The property Description has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Description := pVal;
end;

function TTemplateWapPush.Get_SignalAction: Integer;
begin
    Result := DefaultInterface.SignalAction;
end;

procedure TTemplateWapPush.Set_SignalAction(pVal: Integer);
begin
  DefaultInterface.Set_SignalAction(pVal);
end;

function TTemplateWapPush.Get_Data: WideString;
begin
    Result := DefaultInterface.Data;
end;

function TTemplateWapPush.Get_LastError: Integer;
begin
    Result := DefaultInterface.LastError;
end;

procedure TTemplateWapPush.Encode;
begin
  DefaultInterface.Encode;
end;

function TTemplateWapPush.GetErrorDescription(lCode: Integer): WideString;
begin
  Result := DefaultInterface.GetErrorDescription(lCode);
end;

procedure TTemplateWapPush.Clear;
begin
  DefaultInterface.Clear;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TTemplateWapPushProperties.Create(AServer: TTemplateWapPush);
begin
  inherited Create;
  FServer := AServer;
end;

function TTemplateWapPushProperties.GetDefaultInterface: ITemplateWapPush;
begin
  Result := FServer.DefaultInterface;
end;

function TTemplateWapPushProperties.Get_Url: WideString;
begin
    Result := DefaultInterface.Url;
end;

procedure TTemplateWapPushProperties.Set_Url(const pVal: WideString);
  { Warning: The property Url has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Url := pVal;
end;

function TTemplateWapPushProperties.Get_Description: WideString;
begin
    Result := DefaultInterface.Description;
end;

procedure TTemplateWapPushProperties.Set_Description(const pVal: WideString);
  { Warning: The property Description has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Description := pVal;
end;

function TTemplateWapPushProperties.Get_SignalAction: Integer;
begin
    Result := DefaultInterface.SignalAction;
end;

procedure TTemplateWapPushProperties.Set_SignalAction(pVal: Integer);
begin
  DefaultInterface.Set_SignalAction(pVal);
end;

function TTemplateWapPushProperties.Get_Data: WideString;
begin
    Result := DefaultInterface.Data;
end;

function TTemplateWapPushProperties.Get_LastError: Integer;
begin
    Result := DefaultInterface.LastError;
end;

{$ENDIF}

class function CoTemplatevCard.Create: ITemplatevCard;
begin
  Result := CreateComObject(CLASS_TemplatevCard) as ITemplatevCard;
end;

class function CoTemplatevCard.CreateRemote(const MachineName: string): ITemplatevCard;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_TemplatevCard) as ITemplatevCard;
end;

procedure TTemplatevCard.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{72B516D0-8B84-4B92-A297-474353A2C90A}';
    IntfIID:   '{FCB5DCC2-4DDD-465D-936F-40AEBDDD17C3}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TTemplatevCard.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as ITemplatevCard;
  end;
end;

procedure TTemplatevCard.ConnectTo(svrIntf: ITemplatevCard);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TTemplatevCard.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TTemplatevCard.GetDefaultInterface: ITemplatevCard;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TTemplatevCard.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TTemplatevCardProperties.Create(Self);
{$ENDIF}
end;

destructor TTemplatevCard.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TTemplatevCard.GetServerProperties: TTemplatevCardProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TTemplatevCard.Get_Url: WideString;
begin
    Result := DefaultInterface.Url;
end;

procedure TTemplatevCard.Set_Url(const pVal: WideString);
  { Warning: The property Url has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Url := pVal;
end;

function TTemplatevCard.Get_Title: WideString;
begin
    Result := DefaultInterface.Title;
end;

procedure TTemplatevCard.Set_Title(const pVal: WideString);
  { Warning: The property Title has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Title := pVal;
end;

function TTemplatevCard.Get_EMail: WideString;
begin
    Result := DefaultInterface.EMail;
end;

procedure TTemplatevCard.Set_EMail(const pVal: WideString);
  { Warning: The property EMail has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.EMail := pVal;
end;

function TTemplatevCard.Get_Fax: WideString;
begin
    Result := DefaultInterface.Fax;
end;

procedure TTemplatevCard.Set_Fax(const pVal: WideString);
  { Warning: The property Fax has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Fax := pVal;
end;

function TTemplatevCard.Get_Pager: WideString;
begin
    Result := DefaultInterface.Pager;
end;

procedure TTemplatevCard.Set_Pager(const pVal: WideString);
  { Warning: The property Pager has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Pager := pVal;
end;

function TTemplatevCard.Get_Mobile: WideString;
begin
    Result := DefaultInterface.Mobile;
end;

procedure TTemplatevCard.Set_Mobile(const pVal: WideString);
  { Warning: The property Mobile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Mobile := pVal;
end;

function TTemplatevCard.Get_PhoneHome: WideString;
begin
    Result := DefaultInterface.PhoneHome;
end;

procedure TTemplatevCard.Set_PhoneHome(const pVal: WideString);
  { Warning: The property PhoneHome has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.PhoneHome := pVal;
end;

function TTemplatevCard.Get_PhoneWork: WideString;
begin
    Result := DefaultInterface.PhoneWork;
end;

procedure TTemplatevCard.Set_PhoneWork(const pVal: WideString);
  { Warning: The property PhoneWork has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.PhoneWork := pVal;
end;

function TTemplatevCard.Get_Phone: WideString;
begin
    Result := DefaultInterface.Phone;
end;

procedure TTemplatevCard.Set_Phone(const pVal: WideString);
  { Warning: The property Phone has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Phone := pVal;
end;

function TTemplatevCard.Get_Name: WideString;
begin
    Result := DefaultInterface.Name;
end;

procedure TTemplatevCard.Set_Name(const pVal: WideString);
  { Warning: The property Name has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Name := pVal;
end;

function TTemplatevCard.Get_Data: WideString;
begin
    Result := DefaultInterface.Data;
end;

function TTemplatevCard.Get_LastError: Integer;
begin
    Result := DefaultInterface.LastError;
end;

procedure TTemplatevCard.Clear;
begin
  DefaultInterface.Clear;
end;

function TTemplatevCard.GetErrorDescription(lError: Integer): WideString;
begin
  Result := DefaultInterface.GetErrorDescription(lError);
end;

procedure TTemplatevCard.Encode;
begin
  DefaultInterface.Encode;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TTemplatevCardProperties.Create(AServer: TTemplatevCard);
begin
  inherited Create;
  FServer := AServer;
end;

function TTemplatevCardProperties.GetDefaultInterface: ITemplatevCard;
begin
  Result := FServer.DefaultInterface;
end;

function TTemplatevCardProperties.Get_Url: WideString;
begin
    Result := DefaultInterface.Url;
end;

procedure TTemplatevCardProperties.Set_Url(const pVal: WideString);
  { Warning: The property Url has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Url := pVal;
end;

function TTemplatevCardProperties.Get_Title: WideString;
begin
    Result := DefaultInterface.Title;
end;

procedure TTemplatevCardProperties.Set_Title(const pVal: WideString);
  { Warning: The property Title has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Title := pVal;
end;

function TTemplatevCardProperties.Get_EMail: WideString;
begin
    Result := DefaultInterface.EMail;
end;

procedure TTemplatevCardProperties.Set_EMail(const pVal: WideString);
  { Warning: The property EMail has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.EMail := pVal;
end;

function TTemplatevCardProperties.Get_Fax: WideString;
begin
    Result := DefaultInterface.Fax;
end;

procedure TTemplatevCardProperties.Set_Fax(const pVal: WideString);
  { Warning: The property Fax has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Fax := pVal;
end;

function TTemplatevCardProperties.Get_Pager: WideString;
begin
    Result := DefaultInterface.Pager;
end;

procedure TTemplatevCardProperties.Set_Pager(const pVal: WideString);
  { Warning: The property Pager has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Pager := pVal;
end;

function TTemplatevCardProperties.Get_Mobile: WideString;
begin
    Result := DefaultInterface.Mobile;
end;

procedure TTemplatevCardProperties.Set_Mobile(const pVal: WideString);
  { Warning: The property Mobile has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Mobile := pVal;
end;

function TTemplatevCardProperties.Get_PhoneHome: WideString;
begin
    Result := DefaultInterface.PhoneHome;
end;

procedure TTemplatevCardProperties.Set_PhoneHome(const pVal: WideString);
  { Warning: The property PhoneHome has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.PhoneHome := pVal;
end;

function TTemplatevCardProperties.Get_PhoneWork: WideString;
begin
    Result := DefaultInterface.PhoneWork;
end;

procedure TTemplatevCardProperties.Set_PhoneWork(const pVal: WideString);
  { Warning: The property PhoneWork has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.PhoneWork := pVal;
end;

function TTemplatevCardProperties.Get_Phone: WideString;
begin
    Result := DefaultInterface.Phone;
end;

procedure TTemplatevCardProperties.Set_Phone(const pVal: WideString);
  { Warning: The property Phone has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Phone := pVal;
end;

function TTemplatevCardProperties.Get_Name: WideString;
begin
    Result := DefaultInterface.Name;
end;

procedure TTemplatevCardProperties.Set_Name(const pVal: WideString);
  { Warning: The property Name has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Name := pVal;
end;

function TTemplatevCardProperties.Get_Data: WideString;
begin
    Result := DefaultInterface.Data;
end;

function TTemplatevCardProperties.Get_LastError: Integer;
begin
    Result := DefaultInterface.LastError;
end;

{$ENDIF}

class function CoDemoAccount.Create: IDemoAccount;
begin
  Result := CreateComObject(CLASS_DemoAccount) as IDemoAccount;
end;

class function CoDemoAccount.CreateRemote(const MachineName: string): IDemoAccount;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_DemoAccount) as IDemoAccount;
end;

procedure TDemoAccount.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{634B8464-5D45-4E0E-BCC7-EB04500BC159}';
    IntfIID:   '{231F4503-C639-479F-B799-4C9FCE0BDF9C}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TDemoAccount.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as IDemoAccount;
  end;
end;

procedure TDemoAccount.ConnectTo(svrIntf: IDemoAccount);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TDemoAccount.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TDemoAccount.GetDefaultInterface: IDemoAccount;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call "Connect" or "ConnectTo" before this operation');
  Result := FIntf;
end;

constructor TDemoAccount.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TDemoAccountProperties.Create(Self);
{$ENDIF}
end;

destructor TDemoAccount.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TDemoAccount.GetServerProperties: TDemoAccountProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TDemoAccount.Get_SystemId: WideString;
begin
    Result := DefaultInterface.SystemId;
end;

function TDemoAccount.Get_Password: WideString;
begin
    Result := DefaultInterface.Password;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TDemoAccountProperties.Create(AServer: TDemoAccount);
begin
  inherited Create;
  FServer := AServer;
end;

function TDemoAccountProperties.GetDefaultInterface: IDemoAccount;
begin
  Result := FServer.DefaultInterface;
end;

function TDemoAccountProperties.Get_SystemId: WideString;
begin
    Result := DefaultInterface.SystemId;
end;

function TDemoAccountProperties.Get_Password: WideString;
begin
    Result := DefaultInterface.Password;
end;

{$ENDIF}

procedure Register;
begin
  RegisterComponents(dtlServerPage, [TConstants, TMessage, TTlv, TGsmDeliveryReport, 
    TDialup, TGsm, THttp, TSnpp, TSmpp, 
    TSmppServer, TSmppSession, TTemplateWapPush, TTemplatevCard, TDemoAccount]);
end;

end.
