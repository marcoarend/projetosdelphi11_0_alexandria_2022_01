unit Module;

//Lic_Dmk.LiberaUso5

interface

uses
  Winapi.Windows, Vcl.Dialogs, StdCtrls,
  (*Messages, Graphics, Controls, Forms,
  DBTables, FileCtrl, UMySQLModule, dmkEdit,
  Winsock, MySQLBatch, frxClass, frxDBSet,
  Variants, ABSMain,  ComCtrls, IdBaseComponent, IdComponent,
  IdIPWatch, UnBugs_Tabs, UnDmkEnums, UnProjGroup_Consts;*)
  System.SysUtils, System.Classes, Data.DB, mySQLDbTables,
  Vcl.Forms,
  DmkGeral, ZCF2, UnInternalConsts, UnGrl_Vars, UnDmkEnums, UnGrl_Consts,
  mySQLDirectQuery, UnAll_Jan, UnProjGroup_Consts;

type
  TDmod = class(TDataModule)
    MyDB: TmySQLDatabase;
    QrUpd: TmySQLQuery;
    QrAux: TmySQLQuery;
    QrMas: TmySQLQuery;
    QrSQL: TmySQLQuery;
    QrIdx: TmySQLQuery;
    QrNTV: TmySQLQuery;
    MyDBn: TmySQLDatabase;
    QrControle: TmySQLQuery;
    QrNTI: TmySQLQuery;
    QrPriorNext: TmySQLQuery;
    QrAgora: TmySQLQuery;
    QrAgoraANO: TLargeintField;
    QrAgoraMES: TLargeintField;
    QrAgoraDIA: TLargeintField;
    QrAgoraHORA: TLargeintField;
    QrAgoraMINUTO: TLargeintField;
    QrAgoraSEGUNDO: TLargeintField;
    QrAgoraAGORA: TDateTimeField;
    QrTerminal: TmySQLQuery;
    QrTerminalIP: TWideStringField;
    QrTerminalTerminal: TIntegerField;
    QrUpdU: TmySQLQuery;
    ZZDB: TmySQLDatabase;
    QrUpdM: TmySQLQuery;
    QrAuxL: TmySQLQuery;
    QrMaster: TmySQLQuery;
    QrMasterCNPJ_TXT: TWideStringField;
    QrMasterTE1_TXT: TWideStringField;
    QrMasterCEP_TXT: TWideStringField;
    QrMasterEm: TWideStringField;
    QrMasterTipo: TSmallintField;
    QrMasterLogo: TBlobField;
    QrMasterDono: TIntegerField;
    QrMasterCNPJ: TWideStringField;
    QrMasterIE: TWideStringField;
    QrMasterECidade: TWideStringField;
    QrMasterNOMEUF: TWideStringField;
    QrMasterEFax: TWideStringField;
    QrMasterERua: TWideStringField;
    QrMasterEBairro: TWideStringField;
    QrMasterECompl: TWideStringField;
    QrMasterEContato: TWideStringField;
    QrMasterECel: TWideStringField;
    QrMasterETe1: TWideStringField;
    QrMasterETe2: TWideStringField;
    QrMasterETe3: TWideStringField;
    QrMasterEPais: TWideStringField;
    QrMasterRespons1: TWideStringField;
    QrMasterRespons2: TWideStringField;
    QrMasterECEP: TIntegerField;
    QrMasterLogo2: TBlobField;
    QrMasterLimite: TSmallintField;
    QrMasterSolicitaSenha: TIntegerField;
    QrMasterENumero: TFloatField;
    QrMasterUsaAccMngr: TSmallintField;
    QrControleSoMaiusculas: TWideStringField;
    QrControlePaperLef: TIntegerField;
    QrControlePaperTop: TIntegerField;
    QrControlePaperHei: TIntegerField;
    QrControlePaperWid: TIntegerField;
    QrControlePaperFcl: TIntegerField;
    QrControleMoeda: TWideStringField;
    QrControleCodigo: TIntegerField;
    QrControleDono: TIntegerField;
    QrControleUFPadrao: TIntegerField;
    QrControleTravaCidade: TSmallintField;
    QrControleCNPJ: TWideStringField;
    QrControleCidade: TWideStringField;
    QrControleVerBcoTabs: TIntegerField;
    QrOpcoesApp: TmySQLQuery;
    QrOPcoesGrl: TMySQLQuery;
    QrOPcoesGrlERPNameByCli: TWideStringField;
    DqAux: TMySQLDirectQuery;
    QrOpcoesApU: TMySQLQuery;
    QrOpcoesApUCodigo: TIntegerField;
    QrOpcoesApUHabFaccao: TSmallintField;
    QrOpcoesApUHabTextil: TSmallintField;
    QrOpcoesApUNO_HabFaccao: TWideStringField;
    QrOpcoesApUNO_HabTextil: TWideStringField;
    QrOpcoesApULogin: TWideStringField;
    QrOpcoesApUHabFacConfeccao: TSmallintField;
    QrOpcoesApUHabTexTecelagem: TSmallintField;
    QrOpcoesApUHabTexTinturaria: TSmallintField;
    QrSumBtl: TMySQLQuery;
    QrSumBtlQtReal: TFloatField;
    QrAu2: TMySQLQuery;
    QrControleCasasProd: TIntegerField;
    QrLocY: TMySQLQuery;
    QrLocYRecord: TIntegerField;
    QrAux3: TMySQLQuery;
    QrOpcoesAppDirImgDesp: TWideStringField;
    QrOpcoesAppMailsDstn: TWideMemoField;
    QrOpcoesAppMailsDsKm: TWideMemoField;
    QrOpcoesAppLogoEmpresa: TWideMemoField;
    QrMasterVersao: TIntegerField;
(*
    QrControleMoraDD: TFloatField;
    QrControleMulta: TFloatField;
    QrControleMensalSempre: TSmallintField;
    QrControleEntraSemValor: TSmallintField;
    QrControleCorRecibo: TIntegerField;
    QrControleIdleMinutos: TIntegerField;
    QrControleMyPagCar: TSmallintField;
    QrControleMyPgParc: TSmallintField;
    QrControleMyPgQtdP: TSmallintField;
    QrControleMyPgPeri: TSmallintField;
    QrControleMyPgDias: TSmallintField;
    QrControleVendaCartPg: TIntegerField;
    QrControleVendaParcPg: TIntegerField;
    QrControleVendaPeriPg: TIntegerField;
    QrControleVendaDiasPg: TIntegerField;
    QrControleCNABCtaTar: TIntegerField;
    QrControleCNABCtaJur: TIntegerField;
    QrControleCNABCtaMul: TIntegerField;
    QrControleLogoNF: TWideStringField;
    QrControleMeuLogoPath: TWideStringField;
    QrControleLogoBig1: TWideStringField;
    QrControleMoedaBr: TIntegerField;
    QrControleMyPerJuros: TFloatField;
    QrControleMyPerMulta: TFloatField;
*)
(*
    object QrControleMoraDD: TFloatField
      FieldName = 'MoraDD'
    end
    object QrControleMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrControleMensalSempre: TSmallintField
      FieldName = 'MensalSempre'
    end
    object QrControleEntraSemValor: TSmallintField
      FieldName = 'EntraSemValor'
    end
    object QrControleCorRecibo: TIntegerField
      FieldName = 'CorRecibo'
    end
    object QrControleMyPgParc: TSmallintField
      FieldName = 'MyPgParc'
      Origin = 'controle.MyPgParc'
    end
    object QrControleMyPgQtdP: TSmallintField
      FieldName = 'MyPgQtdP'
      Origin = 'controle.MyPgQtdP'
    end
    object QrControleMyPgPeri: TSmallintField
      FieldName = 'MyPgPeri'
      Origin = 'controle.MyPgPeri'
    end
    object QrControleVendaCartPg: TIntegerField
      FieldName = 'VendaCartPg'
      Origin = 'controle.VendaCartPg'
    end
    object QrControleVendaParcPg: TIntegerField
      FieldName = 'VendaParcPg'
      Origin = 'controle.VendaParcPg'
    end
    object QrControleVendaPeriPg: TIntegerField
      FieldName = 'VendaPeriPg'
      Origin = 'controle.VendaPeriPg'
    end
    object QrControleVendaDiasPg: TIntegerField
      FieldName = 'VendaDiasPg'
      Origin = 'controle.VendaDiasPg'
    end
    object QrControleMyPgDias: TSmallintField
      FieldName = 'MyPgDias'
      Origin = 'controle.MyPgDias'
    end
    object QrControleMyPagCar: TSmallintField
      FieldName = 'MyPagCar'
      Origin = 'controle.MyPagCar'
    end
    object QrControleCNABCtaTar: TIntegerField
      FieldName = 'CNABCtaTar'
      Origin = 'controle.CNABCtaTar'
    end
    object QrControleCNABCtaJur: TIntegerField
      FieldName = 'CNABCtaJur'
      Origin = 'controle.CNABCtaJur'
    end
    object QrControleCNABCtaMul: TIntegerField
      FieldName = 'CNABCtaMul'
      Origin = 'controle.CNABCtaMul'
    end
    object QrControleLogoNF: TWideStringField
      FieldName = 'LogoNF'
      Origin = 'controle.LogoNF'
      Size = 255
    end
    object QrControleMeuLogoPath: TWideStringField
      FieldName = 'MeuLogoPath'
      Origin = 'controle.MeuLogoPath'
      Size = 255
    end
    object QrControleLogoBig1: TWideStringField
      FieldName = 'LogoBig1'
      Origin = 'controle.LogoBig1'
      Size = 255
    end
    object QrControleMoedaBr: TIntegerField
      FieldName = 'MoedaBr'
      Origin = 'controle.MoedaBr'
    end
    object QrControleIdleMinutos: TIntegerField
      FieldName = 'IdleMinutos'
    end
    object QrControleMyPerJuros: TFloatField
      FieldName = 'MyPerJuros'
    end
    object QrControleMyPerMulta: TFloatField
      FieldName = 'MyPerMulta'
    end
*)
    procedure DataModuleCreate(Sender: TObject);
    procedure QrMasterAfterOpen(DataSet: TDataSet);
    procedure QrMasterCalcFields(DataSet: TDataSet);
    procedure QrControleAfterOpen(DataSet: TDataSet);
    procedure MyDBBeforeConnect(Sender: TObject);
    procedure ZZDBBeforeConnect(Sender: TObject);
    procedure MyLocDatabaseBeforeConnect(Sender: TObject);
    procedure MyDBnBeforeConnect(Sender: TObject);
    procedure MyDBAfterConnect(Sender: TObject);
    procedure QrOpcoesAppAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    FPixelsPerInch: Integer;  // Alexandria -> Tokyo
    //
    procedure ReadPixelsPerInch(Reader: TReader);  // Alexandria -> Tokyo
    procedure WritePixelsPerInch(Writer: TWriter);  // Alexandria -> Tokyo
    //
  protected  // Alexandria -> Tokyo
    { Protected declarations }
    procedure DefineProperties(Filer: TFiler); override;  // Alexandria -> Tokyo
    //
  public
    { Public declarations }
    FFmtPrc, FProdDelete: Integer;
    FStrFmtPrc, FStrFmtCus: String;
    //
    property PixelsPerInch: Integer read FPixelsPerInch write FPixelsPerInch;  // Alexandria -> Tokyo
    //
    function  AtualizaQtBtl(Codigo: Integer): Double;
    procedure CriaItensChmOcoDon(LaAviso1, LaAviso2: TLabel; Codigo: Integer);
    procedure ExcluiItensOrfaosChmOcoDon(LaAviso1, LaAviso2: TLabel; Codigo: Integer);
    function  DModG_ObtemAgora(UTC: Boolean = False): TDateTime;
    procedure ReopenControle();
    procedure ReopenParamsEspecificos(Empresa: Integer);
    procedure ReopenOpcoesApp();
    procedure ReopenOpcoesApU(Usuario: Integer);
    function  TabelasQueNaoQueroCriar(): String;
    //

  end;

var
  Dmod: TDmod;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

uses
  DmkDAC_PF, UnGOTOy, Principal, Servidor, ModuleGeral, VerifiDB, UnLic_Dmk,
  {[***Desmarcar***]
  VerifiDBi,
  [***NomeApp***]_Dmk,
  }
  //ModuleFin,
  MyListas, UnDmkWeb, UnDmkProcFunc, UnExes_ProjGroupVars,
  Descanso, UnMyObjects, UMySQLDB, UMySQLModule, MyDBCheck, ExesD_Dmk;

{ TDmod }

function TDmod.AtualizaQtBtl(Codigo: Integer): Double;
var
  QtBtl: Double;
begin
  QtBtl := 0;
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumBtl, Dmod.MyDB, [
  'SELECT SUM(QtReal) QtReal  ',
  'FROM ovgitxgerbtl ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  EmptyStr]);
  //
  QtBtl := QrSumBtlQtReal.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ovgitxgercab', False, [
  'QtBtl'], [
  'Codigo'], [
  QtBtl], [
  Codigo], True) then
    Result := QtBtl;
end;

procedure TDmod.CriaItensChmOcoDon(LaAviso1, LaAviso2: TLabel; Codigo: Integer);
  procedure InsereItemAtual();
  var
    DoneDtHr, CloseDtHr, Observacao: String;
    //Codigo,
    Controle, ChmOcoEtp, ChmOcoWho, CloseUser: Integer;
    SQLType: TSQLType;
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo item ' +
    Geral.FF0(QrAu2.RecNo) + ' de ' + Geral.FF0(QrAu2.RecordCount));
    //
    SQLType        := stIns;
    //Codigo         := ;
    Controle       := 0;
    ChmOcoEtp      := QrAu2.FieldByName('ChmOcoEtp').AsInteger;
    ChmOcoWho      := QrAu2.FieldByName('ChmOcoWho').AsInteger;
    DoneDtHr       := '0000-00-00 00:00:00';
    CloseUser      := 0;
    CloseDtHr      := '0000-00-00 00:00:00';
    Observacao     := '';
    //
    Controle := UMyMod.BPGS1I32('chmocodon', 'Controle', '', '', tsPos, SQLType, Controle);
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'chmocodon', False, [
    'Codigo', 'ChmOcoEtp', 'ChmOcoWho',
    'DoneDtHr', 'CloseUser', 'CloseDtHr',
    'Observacao'], [
    'Controle'], [
    Codigo, ChmOcoEtp, ChmOcoWho,
    DoneDtHr, CloseUser, CloseDtHr,
    Observacao], [
    Controle], True);
  end;
var
  TextoAnterior: String;
begin
  if LaAviso1 <> nil then
    TextoAnterior := LaAviso1.Caption
  else
  if LaAviso2 <> nil then
    TextoAnterior := LaAviso2.Caption
  else
    TextoAnterior := '';
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Preparando gera��o de itens de ocorr�ncias');
    UnDmkDAC_PF.AbreMySQLQuery0(QrAu2, Dmod.MyDB, [
    'DROP TABLE IF EXISTS _Temp_chmocoetp_; ',
    'CREATE TABLE _Temp_chmocoetp_ ',
    'SELECT Controle ',
    'FROM chmocoetp',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    ';',
    'DROP TABLE IF EXISTS _Temp_chmocowho_; ',
    'CREATE TABLE _Temp_chmocowho_ ',
    'SELECT Controle ',
    'FROM chmocowho',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    ';',
    'DROP TABLE IF EXISTS _Temp_chmocodon_; ',
    'CREATE TABLE _Temp_chmocodon_ ',
    'SELECT ',
    'etp.Controle chmocoetp, ',
    'who.Controle chmocowho',
    'FROM _Temp_chmocoetp_ etp',
    'INNER JOIN _Temp_chmocowho_ who',
    ';',
    'SELECT don.Controle, tmp.chmocoetp, tmp.chmocowho ',
    'FROM _Temp_chmocodon_ tmp',
    'LEFT JOIN chmocodon don ON ',
    '  don.chmocoetp=tmp.chmocoetp',
    '  AND',
    '  don.chmocowho=tmp.chmocowho',
    'WHERE don.Controle IS NULL;',
    'DROP TABLE IF EXISTS _Temp_chmocoetp_;',
    'DROP TABLE IF EXISTS _Temp_chmocowho_; ',
    'DROP TABLE IF EXISTS _Temp_chmocodon_; ',
    '']);
    QrAu2.First;
    while not QrAu2.Eof do
    begin
      InsereItemAtual();
      //
      QrAu2.Next;
    end;
  finally
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
  end;
end;

procedure TDmod.DataModuleCreate(Sender: TObject);
var
{
  Versao, VerZero: Int64;
  Resp: Integer;
  BD, OthSetCon: String;
  Verifica: Boolean;
var
  Porta, Server: Integer;
  IPServ, DataBase, OthUser: String;
}
  DataBase: String;
  VerificaDBTerc: Boolean;
begin
  FmPrincipal.FLinModErr := 2105;
  VerificaDBTerc := False;
  VAR_MyBDFINANCAS := MyDB;  // MyPagtos : CASHIER
  MyList.ConfiguracoesIniciais(1, Application.Name);
  //
  FmPrincipal.FLinModErr := 2125;
  USQLDB.VerificaDmod_1_Create_DBs([MyDB, ZZDB(*, MyLocDatabase*)]);
{
object MyLocDatabase: TMySQLDatabase
  UserName = 'root'
  Host = '127.0.0.1'
  ConnectOptions = []
  KeepConnection = False
  Params.Strings = (
    'Port=3306'
    'TIMEOUT=30'
    'Host=127.0.0.1'
    'UID=root')
  BeforeConnect = MyLocDatabaseBeforeConnect
  DatasetOptions = []
  Left = 264
  Top = 7
end

  if MyDB.Connected then
    Geral.MB_Aviso('MyDB est� connectado antes da configura��o!');
  if MyLocDataBase.Connected then
    Geral.MB_Aviso('MyLocDataBase est� connectado antes da configura��o!');
  if MyDBn.Connected then
    Geral.MB_Aviso('MyDBn est� connectado antes da configura��o!');

  MyDB.LoginPrompt := False;

  ZZDB.LoginPrompt := False;
  MyLocDatabase.LoginPrompt := False;
}

  Database := USQLDB.VerificaDmod_2_DefineVarsDBConnect();
  TLocDB := DataBase;
{

  OthSetCon := Geral.ReadAppKeyCU('OthSetCon', Application.Title, ktString, '');
  if OthSetCon = EmptyStr then
    VAR_BDSENHA := CO_USERSPNOW
  else
    VAR_BDSENHA := dmkPF.Criptografia(OthSetCon, CO_LLM_SecureStr);
  //
  VAR_PORTA := Geral.ReadAppKeyCU('Porta', 'Dermatek', ktInteger, 3306);
  Server := Geral.ReadAppKeyCU('Server', Application.Title, ktInteger, 0);
  //
  case Server of
    1:   VAR_SERVIDOR := 0;
    2:   VAR_SERVIDOR := 1;
    3:   VAR_SERVIDOR := 2;
    else VAR_SERVIDOR := -1;
  end;
  VAR_IP       := Geral.ReadAppKeyCU('IPServer', Application.Title, ktString, CO_VAZIO);
  DataBase     := Geral.ReadAppKeyCU('Database', Application.Title, ktString, 'mysql');
  TLocDB := DataBase;
  VAR_SQLUSER  := Geral.ReadAppKeyCU('OthUser', Application.Title, ktString, 'root');
  if VAR_BDSENHA = '' then
    VAR_BDSENHA := CO_USERSPNOW;
  if VAR_SQLUSER = '' then
    VAR_SQLUSER := 'root';
}

//////////////////////////  Ini Ver se precisa!!!  /////////////////////////////
  FmPrincipal.FLinModErr := 2184;
  if not GOTOy.OMySQLEstaInstalado(FmExesD_dmk.LaAviso1,
  FmExesD_dmk.LaAviso2, FmExesD_dmk.ProgressBar1,
  FmExesD_dmk.BtEntra, nil) then
    Exit;

  // N�o executar! App n�o tem acesso ao BD mysql no servidor!
  //USQLDB.VerificaDmod_3_RedefineSenhaSeNecessario(ZZDB, QrUpd);


//////////////////////////  Fim Ver se precisa!!!  /////////////////////////////


(*
  /////////////////////////////////////////
  if GOTOy.SenhaDesconhecida then
  begin
    raise EAbort.Create('Senha desconhecida');
    Exit;
  end;
*)
  ///////////////////////////////////////////////////////////
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  //
  VAR_SQLx := TStringList.Create;
  VAR_SQL1 := TStringList.Create;
  VAR_SQL2 := TStringList.Create;
  VAR_SQLa := TStringList.Create;
  //
  MAR_SQLx := TStringList.Create;
  MAR_SQL1 := TStringList.Create;
  MAR_SQL2 := TStringList.Create;
  MAR_SQLa := TStringList.Create;

  ///////////////////////////////////////////////////////////

  FmPrincipal.FLinModErr := 2254;
  USQLDB.VerificaDmod_4_PreparaParaConectar();

{
  try
    if VAR_SERVIDOR = 1 then
    begin
      if not GetSystemMetrics(SM_NETWORK) and $01 = $01 then
      begin
      Geral.MB_Aviso('M�quina cliente sem rede.');
        Application.Terminate;
      end;
    end;
  except
    Application.Terminate;
    Exit;
  end;
  if VAR_APPTERMINATE then
  begin
    Application.Terminate;
    Exit;
  end;
  Geral.DefineFormatacoes;
  //
  VAR_SERVIDOR := Geral.ReadAppKey('Server', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if not (VAR_SERVIDOR in [1,2]) then
  begin
    Application.CreateForm(TFmServidor, FmServidor);
    FmServidor.ShowModal;
    FmServidor.Destroy;
  end;
  if (VAR_SERVIDOR = 2) and (Trim(VAR_IP) = EMptyStr) then
    VAR_IP := '127.0.0.1';

  if (Trim(Database) = EmptyStr)
  or (Trim(VAR_IP) = EmptyStr)
  or (VAR_PORTA = 0)
  or (Trim(VAR_SQLUSER) = EmptyStr)
  or (Trim(VAR_BDSENHA) = EmptyStr) then
    USQLDB.ConfiguraDB(True);

  UnDmkDAC_PF.ConectaMyDB_DAC(MyDB, Database,  VAR_IP, VAR_PORTA,  VAR_SQLUSER,
    VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True, (*Conecta*)False);
  //
}

FmPrincipal.FLinModErr := 2228;
  // Neste caso espec�fico troquei o 'mysql' pela vari�vel Database, pois
  // como o banco e dados � o servidor o usu�rio n�o tem permiss�o para
  // acessar o banco de dados 'mysql'.
  //UnDmkDAC_PF.ConectaMyDB_DAC(MyDB, 'mysql',  VAR_IP,
  UnDmkDAC_PF.ConectaMyDB_DAC(MyDB, Database,  VAR_IP,
  VAR_PORTA,  VAR_SQLUSER, VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True,
  (*Conecta*)False);
  //
  //



  FmPrincipal.FLinModErr := 2236;
  USQLDB.VerificaDmod_5_DefineBDPrincipal(MyDB, QrAux);
  //

{
  try
    QrAux.Close;
    QrAux.SQL.Clear;
    QrAux.SQL.Add('SHOW DATABASES');
    UnDmkDAC_PF.AbreQuery(QrAux, MyDB);
    BD := CO_VAZIO;
    while not QrAux.Eof do
    begin
      if Uppercase(QrAux.FieldByName('Database').AsString)=Uppercase(TMeuDB) then
        BD := TMeuDB;
      QrAux.Next;
    end;
    MyDB.Close;
    MyDB.DataBaseName := BD;
    if MyDB.DataBaseName = CO_VAZIO then
    begin
      Resp := Geral.MB_Pergunta('O banco de dados ' + TMeuDB +
        ' n�o existe e deve ser criado. Confirma a cria��o?');
      if Resp = ID_YES then
      begin
        QrAux.Close;
        QrAux.SQL.Clear;
        QrAux.SQL.Add('CREATE DATABASE '+TMeuDB);
        QrAux.ExecSQL;
        MyDB.Disconnect;
        MyDB.DatabaseName := TMeuDB;
      end else if Resp = ID_CANCEL then
      begin
        Geral.MB_Aviso('O aplicativo ser� encerrado!');
        Application.Terminate;
        Exit;
      end;
    end;
  except
    on E: Exception do
    begin
      Geral.MB_Erro(E.Message);
      USQLDB.ConfiguraDB(True);
    end;
  end;
  //
  GOTOy.DefinePathMySQL;
$IfNDef SemDBLocal
}
  FmPrincipal.FLinModErr := 2275;
  GOTOy.DefinePathMySQL;
  //
  //
{
  MyLocDatabase.UserName     := VAR_SQLUSER;
  MyLocDatabase.UserPassword := VAR_BDSENHA;
  MyLocDatabase.Port         := VAR_PORTA;
  MyLocDatabase.DatabaseName := 'mysql';// existe com certeza se estiver instalado
  //
  QrAuxL.Close;
  QrAuxL.SQL.Clear;
  QrAuxL.SQL.Add('SHOW DATABASES');
  UnDmkDAC_PF.AbreQuery(QrAux, MyLocDatabase);
  BD := CO_VAZIO;
  while not QrAuxL.Eof do
  begin
    if Uppercase(QrAuxL.FieldByName('Database').AsString)=Uppercase(TLocDB) then
      BD := TLocDB;
    QrAuxL.Next;
  end;
  MyLocDatabase.Close;
  MyLocDatabase.DatabaseName := BD;
  if MyLocDatabase.DataBaseName = CO_VAZIO then
  begin
    Resp := Geral.MB_Pergunta('O banco de dados local '+TLocDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?');
    if Resp = ID_YES then
    begin
      QrAuxL.Close;
      QrAuxL.SQL.Clear;
      QrAuxL.SQL.Add('CREATE DATABASE '+TLocDB);
      QrAuxL.ExecSQL;
      MyLocDatabase.DatabaseName := TLocDB;
    end else if Resp = ID_CANCEL then
    begin
      Geral.MB_Aviso('O aplicativo ser� encerrado!');
      Application.Terminate;
      Exit;
    end;
  end;
$EndIf
}
    //
   //
  USQLDB.VerificaDmod_6_AtualizaBDSeNecessario(MyDB, QrAux, VerificaDBTerc);
{
  DBCheck.ObtemVersao(VerZero, Versao);
  Verifica := False;
  if Versao < CO_Versao then
    Verifica := True;
  if VerZero = 0 then
  begin
    Resp := Geral.MB_Pergunta('N�o h� informa��o de vers�o no registro, '+
    'se esta n�o for a primeira execu��o do aplicativo selecione cancelar e '+
    'informe o respons�vel!. Caso confirme, ser� verificado a composi��o do '+
    'banco de dados. Confirma a Verifica��o?');
    if Resp = ID_YES then
      Verifica := True
    else if Resp = ID_CANCEL then
    begin
      Application.Terminate;
      Exit;
    end;
  end;
  Mylist.ConfiguracoesIniciais(1, Application.Name);
  //
}

{
  if DModG = nil then
  begin
    try
      Application.CreateForm(TDmodG, DmodG);
    except
      Geral.MB_Erro('Imposs�vel criar Modulo de dados Geral');
      Application.Terminate;
      Exit;
    end;
  end;
}

  USQLDB.VerificaDmod_7_CriaDataModule(TDmodG, DmodG);

{
  if Verifica then
  begin
    Application.CreateForm(TFmVerifiDB, FmVerifiDB);
    with FmVerifiDb do
    begin
      BtSair.Enabled := False;
      FVerifi := True;
      ShowModal;
      FVerifi := False;
      Destroy;
    end;
  end;
  //
  try
    UnDmkDAC_PF.AbreQuery(QrMaster, Dmod.MyDB);
    VAR_EMPRESANOME := QrMasterEm.Value;
  except
    try
      Application.CreateForm(TFmVerifiDB, FmVerifiDB);
      with FmVerifiDb do
      begin
        BtSair.Enabled := False;
        FVerifi := True;
        ShowModal;
        FVerifi := False;
        Destroy;
      end;
      if ZZTerminate then
        Exit;
    except;
      MyDB.DatabaseName := CO_VAZIO;
      raise;
    end;
  end;
  //
}
  if VerificaDBTerc then
  begin
    All_Jan.MostraFormVerifiDBTerceiros(True);
    //
    VerificaDBTerc := False;
  end;

  ////////////////////////////////////////////////////////////////////////////////
///  N�o tem sentido aqui pois depende do site dermatek!
///  Ver o que fazer!
  //Lic_Dmk.LiberaUso5;
  //Geral.MB_Aviso('"Uso5" desabilitado!');
////////////////////////////////////////////////////////////////////////////////
  VAR_DB := MyDB.DatabaseName;
end;

procedure TDmod.DefineProperties(Filer: TFiler);
var
  Ancestor: TDataModule;
begin
  inherited;
  Ancestor := TDataModule(Filer.Ancestor);
  Filer.DefineProperty('PixelsPerInch', ReadPixelsPerInch, WritePixelsPerInch, True);
end;

function TDmod.DModG_ObtemAgora(UTC: Boolean): TDateTime;
begin
  Result := DModG.ObtemAgora(UTC);
end;

procedure TDmod.ExcluiItensOrfaosChmOcoDon(LaAviso1, LaAviso2: TLabel; Codigo: Integer);
  procedure ExcluiItemAtual();
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Excluindo item ' +
    Geral.FF0(QrAu2.RecNo) + ' de ' + Geral.FF0(QrAu2.RecordCount));
    //
    Dmod.MyDB.Execute('DELETE FROM chmocodon WHERE Controle=' +
      Geral.FF0(QrAu2.FieldByName('Controle').AsInteger));
  end;
var
  TextoAnterior: String;
begin
  if LaAviso1 <> nil then
    TextoAnterior := LaAviso1.Caption
  else
  if LaAviso2 <> nil then
    TextoAnterior := LaAviso2.Caption
  else
    TextoAnterior := '';
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Preparando gera��o de itens de ocorr�ncias');
    UnDmkDAC_PF.AbreMySQLQuery0(QrAu2, Dmod.MyDB, [
    'SELECT don.Codigo, don.Controle, ',
    'etp.Controle chmocoetp,   ',
    'who.Controle chmocowho   ',
    'FROM chmocodon don ',
    'LEFT JOIN chmocoetp etp ON etp.Controle=don.chmocoetp  ',
    'LEFT JOIN chmocowho who ON who.Controle=don.chmocowho ',
    'WHERE don.Codigo=' + Geral.FF0(Codigo),
    'AND ( ',
    '    (etp.Controle IS NULL) ',
    '  OR ',
    '    (who.Controle IS NULL) ',
    ') ',
   '']);
    QrAu2.First;
    while not QrAu2.Eof do
    begin
      ExcluiItemAtual();
      //
      QrAu2.Next;
    end;
  finally
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
  end;
end;

procedure TDmod.MyDBAfterConnect(Sender: TObject);
begin
  FmPrincipal.TimerPingServer.Enabled := False;
  FmPrincipal.TimerPingServer.Interval := 30000;
  FmPrincipal.TimerPingServer.Enabled := True;
end;

procedure TDmod.MyDBBeforeConnect(Sender: TObject);
begin
  USQLDB.MsgBeforeConectaDB(Sender);
end;

procedure TDmod.MyDBnBeforeConnect(Sender: TObject);
begin
  USQLDB.MsgBeforeConectaDB(Sender);
end;

procedure TDmod.MyLocDatabaseBeforeConnect(Sender: TObject);
begin
  USQLDB.MsgBeforeConectaDB(Sender);
end;

procedure TDmod.QrControleAfterOpen(DataSet: TDataSet);
begin
  FFmtPrc    := Dmod.QrControleCasasProd.Value;
  FStrFmtPrc := dmkPF.StrFmt_Double(Dmod.QrControleCasasProd.Value);
  FStrFmtCus := dmkPF.StrFmt_Double(4);
  //
  ReopenOpcoesApp();
end;

procedure TDmod.QrMasterAfterOpen(DataSet: TDataSet);
begin
{
CNPJ n�o definido ou Empresa n�o definida.
Algumas ferramentas do aplicativo poder�o ficar inacess�veis.
C�digo = ?
}
  if Trim(QrMasterCNPJ.Value) = CO_VAZIO then
  Geral.MB_Aviso('CNPJ n�o definido ou Empresa n�o definida. ' + sLineBreak +
  'Algumas ferramentas do aplicativo poder�o ficar inacess�veis.');
  FmPrincipal.Caption := Application.Title + '  ::  ' + QrMasterEm.Value +
  ' # ' + QrMasterCNPJ.Value;
  //
  ReopenControle();
  DModG.ReopenOpcoesGerl;
  //
  if QrControleSoMaiusculas.Value = 'V' then
    VAR_SOMAIUSCULAS := True;
{$IfNDef SemEntidade}
  VAR_UFPADRAO        := QrControleUFPadrao.Value;
  VAR_CIDADEPADRAO    := QrControleCidade.Value;
  VAR_TRAVACIDADE     := QrControleTravaCidade.Value;
{$EndIf}
{$IfNDef SemCotacoes}
  VAR_MOEDA           := QrControleMoeda.Value;
{$EndIf}
{$IfNDef NO_FINANCEIRO}
(*&�%$#@!"
  VAR_CARTVEN         := QrControleCartVen.Value;
  VAR_CARTCOM         := QrControleCartCom.Value;
  VAR_CARTRES         := QrControleCartReS.Value;
  VAR_CARTDES         := QrControleCartDeS.Value;
  VAR_CARTREG         := QrControleCartReG.Value;
  VAR_CARTDEG         := QrControleCartDeG.Value;
  VAR_CARTCOE         := QrControleCartCoE.Value;
  VAR_CARTCOC         := QrControleCartCoC.Value;
  VAR_CARTEMD         := QrControleCartEmD.Value;
  VAR_CARTEMA         := QrControleCartEmA.Value;
*)
{$EndIf}
  VAR_PAPERSET        := True;
  VAR_PAPERTOP        := QrControlePaperTop.Value;
  VAR_PAPERLEF        := QrControlePaperLef.Value;
  VAR_PAPERWID        := QrControlePaperWid.Value;
  VAR_PAPERHEI        := QrControlePaperHei.Value;
  VAR_PAPERFCL        := QrControlePaperFcl.Value;
  //
  if DModG = nil then
  begin
    try
      Application.CreateForm(TDModG, DModG);
    except
      Geral.MB_Aviso('N�o foi poss�vel criar o Module Geral');
      Application.Terminate;
      Exit;
    end;
  end;
  MyObjects.FormTDICria(TFmDescanso, FmPrincipal.PageControl1,
  FmPrincipal.AdvToolBarPagerNovo, False, False, afmoSemVerificar);
end;

procedure TDmod.QrMasterCalcFields(DataSet: TDataSet);
begin
  QrMasterTE1_TXT.Value  := Geral.FormataTelefone_TT(QrMasterETe1.Value);
  QrMasterCEP_TXT.Value  := Geral.FormataCEP_NT(QrMasterECEP.Value);
  QrMasterCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrMasterCNPJ.Value);
end;

procedure TDmod.QrOpcoesAppAfterOpen(DataSet: TDataSet);
begin
  VAR_DIR_FOTOS_DESPESAS := QrOpcoesAppDirImgDesp.Value;
end;

procedure TDmod.ReadPixelsPerInch(Reader: TReader);
begin
  FPixelsPerInch := Reader.ReadInteger;
end;

procedure TDmod.ReopenControle;
begin
  UnDmkDAC_PF.AbreQuery(QrControle, Dmod.MyDB);
end;


procedure TDmod.ReopenOpcoesApp();
begin
  UnDmkDAC_PF.AbreQuery(QrOpcoesApp, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrOpcoesGrl, Dmod.MyDB);
end;

procedure TDmod.ReopenOpcoesApU(Usuario: Integer);
begin
(*&�%$#@!"
  UnDmkDAC_PF.AbreMySQLQuery0(QrOpcoesApU, Dmod.MyDB, [
  'SELECT apu.Codigo, apu.HabFaccao, apu.HabTextil,  ',
  'apu.HabFacConfeccao,  ',
  'apu.HabTexTecelagem, apu.HabTexTinturaria,  ',
  'IF(apu.HabFaccao=1, "SIM", "N�O") NO_HabFaccao,  ',
  'IF(apu.HabFacConfeccao=1, "SIM", "N�O") NO_HabFacConfecaocao,  ',
  'IF(apu.HabTextil=1, "SIM", "N�O") NO_HabTextil,  ',
  'IF(apu.HabTexTecelagem=1, "SIM", "N�O") NO_HabTexTecelagem,  ',
  'IF(apu.HabTexTinturaria=1, "SIM", "N�O") NO_HabTexTinturaria,  ',
  'pwd.Login  ',
  'FROM opcoesapu apu  ',
  'LEFT JOIN senhas pwd ON pwd.Numero=apu.Codigo  ',
  'WHERE apu.Codigo=' + Geral.FF0(Usuario),
  '']);
*)
end;

procedure TDmod.ReopenParamsEspecificos(Empresa: Integer);
begin
  // Compatibilidade
end;

function TDmod.TabelasQueNaoQueroCriar: String;
begin
  // Compatibilidade
end;

procedure TDmod.WritePixelsPerInch(Writer: TWriter);
begin
  Writer.WriteInteger(FPixelsPerInch);
end;

procedure TDmod.ZZDBBeforeConnect(Sender: TObject);
begin
  USQLDB.MsgBeforeConectaDB(Sender);
end;

end.
