object Dmod: TDmod
  OnCreate = DataModuleCreate
  Height = 522
  Width = 813
  PixelsPerInch = 96
  object MyDB: TMySQLDatabase
    Connected = True
    DatabaseName = 'dermatek_exes'
    UserName = 'dermatek_exes'
    UserPassword = 'QrkrIcvv9Tb2'
    Host = '177.53.143.140'
    ConnectOptions = [coCompress]
    ConnectionTimeout = 10
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=10'
      'UID=dermatek_exes'
      'DatabaseName=dermatek_exes'
      'Host=177.53.143.140'
      'PWD=QrkrIcvv9Tb2')
    AfterConnect = MyDBAfterConnect
    BeforeConnect = MyDBBeforeConnect
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 52
    Top = 16
  end
  object QrUpd: TMySQLQuery
    Database = MyDB
    Left = 52
    Top = 68
  end
  object QrAux: TMySQLQuery
    Database = MyDB
    Left = 52
    Top = 120
  end
  object QrMas: TMySQLQuery
    Database = MyDB
    Left = 148
    Top = 320
  end
  object QrSQL: TMySQLQuery
    Database = MyDB
    Left = 120
    Top = 364
  end
  object QrIdx: TMySQLQuery
    Database = MyDB
    Left = 120
    Top = 320
  end
  object QrNTV: TMySQLQuery
    Database = MyDB
    Left = 160
    Top = 52
  end
  object MyDBn: TMySQLDatabase
    UserName = 'root'
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'UID=root')
    BeforeConnect = MyDBnBeforeConnect
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 400
    Top = 8
  end
  object QrControle: TMySQLQuery
    Database = MyDBn
    AfterOpen = QrControleAfterOpen
    SQL.Strings = (
      'SELECT *'
      'FROM controle')
    Left = 52
    Top = 312
    object QrControleSoMaiusculas: TWideStringField
      FieldName = 'SoMaiusculas'
      Origin = 'controle.SoMaiusculas'
      Size = 1
    end
    object QrControlePaperLef: TIntegerField
      FieldName = 'PaperLef'
      Origin = 'controle.PaperLef'
    end
    object QrControlePaperTop: TIntegerField
      FieldName = 'PaperTop'
      Origin = 'controle.PaperTop'
    end
    object QrControlePaperHei: TIntegerField
      FieldName = 'PaperHei'
      Origin = 'controle.PaperHei'
    end
    object QrControlePaperWid: TIntegerField
      FieldName = 'PaperWid'
      Origin = 'controle.PaperWid'
    end
    object QrControlePaperFcl: TIntegerField
      FieldName = 'PaperFcl'
      Origin = 'controle.PaperFcl'
    end
    object QrControleMoeda: TWideStringField
      FieldName = 'Moeda'
      Size = 4
    end
    object QrControleCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrControleDono: TIntegerField
      FieldName = 'Dono'
    end
    object QrControleUFPadrao: TIntegerField
      FieldName = 'UFPadrao'
    end
    object QrControleTravaCidade: TSmallintField
      FieldName = 'TravaCidade'
    end
    object QrControleCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrControleCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 100
    end
    object QrControleVerBcoTabs: TIntegerField
      FieldName = 'VerBcoTabs'
    end
    object QrControleCasasProd: TIntegerField
      FieldName = 'CasasProd'
    end
  end
  object QrNTI: TMySQLQuery
    Database = MyDBn
    Left = 160
    Top = 100
  end
  object QrPriorNext: TMySQLQuery
    Database = MyDB
    Left = 148
    Top = 364
  end
  object QrAgora: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT YEAR(NOW()) ANO, MONTH(NOW()) MES,'
      'DAYOFMONTH(NOW()) DIA,'
      'HOUR(NOW()) HORA, MINUTE(NOW()) MINUTO,'
      'SECOND(NOW()) SEGUNDO, NOW() AGORA')
    Left = 52
    Top = 216
    object QrAgoraANO: TLargeintField
      FieldName = 'ANO'
    end
    object QrAgoraMES: TLargeintField
      FieldName = 'MES'
    end
    object QrAgoraDIA: TLargeintField
      FieldName = 'DIA'
    end
    object QrAgoraHORA: TLargeintField
      FieldName = 'HORA'
    end
    object QrAgoraMINUTO: TLargeintField
      FieldName = 'MINUTO'
    end
    object QrAgoraSEGUNDO: TLargeintField
      FieldName = 'SEGUNDO'
    end
    object QrAgoraAGORA: TDateTimeField
      FieldName = 'AGORA'
      Required = True
    end
  end
  object QrTerminal: TMySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM Terminais'
      'WHERE IP=:P0')
    Left = 516
    Top = 160
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerminalIP: TWideStringField
      FieldName = 'IP'
      Size = 15
    end
    object QrTerminalTerminal: TIntegerField
      FieldName = 'Terminal'
    end
  end
  object QrUpdU: TMySQLQuery
    Database = MyDB
    Left = 104
    Top = 68
  end
  object ZZDB: TMySQLDatabase
    UserName = 'root'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'Host=127.0.0.1'
      'UID=root')
    BeforeConnect = ZZDBBeforeConnect
    SSLProperties.TLSVersion = tlsAuto
    DatasetOptions = []
    Left = 96
    Top = 16
  end
  object QrUpdM: TMySQLQuery
    Database = MyDB
    Left = 108
    Top = 116
  end
  object QrAuxL: TMySQLQuery
    Database = MyDB
    Left = 264
    Top = 52
  end
  object QrMaster: TMySQLQuery
    Database = ZZDB
    AfterOpen = QrMasterAfterOpen
    OnCalcFields = QrMasterCalcFields
    SQL.Strings = (
      'SELECT ma.Em, te.Tipo, te.Logo,  te.Logo2,cm.Dono, cm.Versao, '
      'cm.CNPJ, te.IE, te.ECidade, uf.Nome NOMEUF, te.EFax,'
      
        'te.ERua, (te.ENumero+0.000) ENumero, te.EBairro, te.ECEP, te.ECo' +
        'mpl,'
      'te.EContato, te.ECel, te.ETe1, te.ETe2, te.ETe3, te.EPais,'
      'te.Respons1, te.Respons2, ma.Limite, ma.SolicitaSenha,'
      'ma.UsaAccMngr'
      'FROM entidades te, controle cm, ufs uf, master ma'
      'WHERE te.Codigo=cm.Dono'
      
        'AND ((te.CNPJ=cm.CNPJ AND te.Tipo=0) OR (te.CPF=cm.CNPJ AND te.T' +
        'ipo=1))'
      'AND uf.Codigo=te.EUF'
      '')
    Left = 248
    Top = 172
    object QrMasterCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrMasterTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 30
      Calculated = True
    end
    object QrMasterCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrMasterEm: TWideStringField
      FieldName = 'Em'
      Origin = 'master.Em'
      Required = True
      Size = 100
    end
    object QrMasterTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrMasterLogo: TBlobField
      FieldName = 'Logo'
      Origin = 'entidades.Logo'
      Size = 4
    end
    object QrMasterDono: TIntegerField
      FieldName = 'Dono'
      Origin = 'controle.Dono'
      Required = True
    end
    object QrMasterCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Origin = 'controle.CNPJ'
      Required = True
      Size = 18
    end
    object QrMasterIE: TWideStringField
      FieldName = 'IE'
      Origin = 'entidades.IE'
      Size = 15
    end
    object QrMasterECidade: TWideStringField
      FieldName = 'ECidade'
      Origin = 'entidades.ECidade'
      Size = 15
    end
    object QrMasterNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrMasterEFax: TWideStringField
      FieldName = 'EFax'
      Origin = 'entidades.EFax'
    end
    object QrMasterERua: TWideStringField
      FieldName = 'ERua'
      Origin = 'entidades.ERua'
      Size = 30
    end
    object QrMasterEBairro: TWideStringField
      FieldName = 'EBairro'
      Origin = 'entidades.EBairro'
      Size = 30
    end
    object QrMasterECompl: TWideStringField
      FieldName = 'ECompl'
      Origin = 'entidades.ECompl'
      Size = 30
    end
    object QrMasterEContato: TWideStringField
      FieldName = 'EContato'
      Origin = 'entidades.EContato'
      Size = 60
    end
    object QrMasterECel: TWideStringField
      FieldName = 'ECel'
      Origin = 'entidades.ECel'
    end
    object QrMasterETe1: TWideStringField
      FieldName = 'ETe1'
      Origin = 'entidades.ETe1'
    end
    object QrMasterETe2: TWideStringField
      FieldName = 'ETe2'
      Origin = 'entidades.Ete2'
    end
    object QrMasterETe3: TWideStringField
      FieldName = 'ETe3'
      Origin = 'entidades.Ete3'
    end
    object QrMasterEPais: TWideStringField
      FieldName = 'EPais'
      Origin = 'entidades.EPais'
    end
    object QrMasterRespons1: TWideStringField
      FieldName = 'Respons1'
      Origin = 'entidades.Respons1'
      Required = True
      Size = 60
    end
    object QrMasterRespons2: TWideStringField
      FieldName = 'Respons2'
      Origin = 'entidades.Respons2'
      Required = True
      Size = 60
    end
    object QrMasterECEP: TIntegerField
      FieldName = 'ECEP'
      Origin = 'entidades.ECEP'
    end
    object QrMasterLogo2: TBlobField
      FieldName = 'Logo2'
      Origin = 'entidades.Logo2'
      Size = 4
    end
    object QrMasterLimite: TSmallintField
      FieldName = 'Limite'
      Origin = 'master.Limite'
      Required = True
    end
    object QrMasterSolicitaSenha: TIntegerField
      FieldName = 'SolicitaSenha'
      Origin = 'master.SolicitaSenha'
    end
    object QrMasterENumero: TFloatField
      FieldName = 'ENumero'
    end
    object QrMasterUsaAccMngr: TSmallintField
      FieldName = 'UsaAccMngr'
    end
    object QrMasterVersao: TIntegerField
      FieldName = 'Versao'
    end
  end
  object QrOpcoesApp: TMySQLQuery
    Database = ZZDB
    AfterOpen = QrOpcoesAppAfterOpen
    SQL.Strings = (
      'SELECT * '
      'FROM opcoesapp'
      'WHERE Codigo=1')
    Left = 316
    Top = 328
    object QrOpcoesAppDirImgDesp: TWideStringField
      FieldName = 'DirImgDesp'
      Size = 511
    end
    object QrOpcoesAppMailsDstn: TWideMemoField
      FieldName = 'MailsDstn'
      BlobType = ftWideMemo
    end
    object QrOpcoesAppMailsDsKm: TWideMemoField
      FieldName = 'MailsDsKm'
      BlobType = ftWideMemo
    end
    object QrOpcoesAppLogoEmpresa: TWideMemoField
      FieldName = 'LogoEmpresa'
      BlobType = ftWideMemo
    end
  end
  object QrOPcoesGrl: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT * '
      'FROM ctrlgeral'
      'WHERE Codigo=1')
    Left = 316
    Top = 376
    object QrOPcoesGrlERPNameByCli: TWideStringField
      FieldName = 'ERPNameByCli'
      Size = 60
    end
  end
  object DqAux: TMySQLDirectQuery
    Database = ZZDB
    Left = 658
    Top = 20
  end
  object QrOpcoesApU: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT apu.Codigo, apu.HabFaccao, apu.HabTextil,  '
      'IF(apu.HabFaccao=1, "SIM", "N'#195'O") NO_HabFaccao, '
      'IF(apu.HabTextil=1, "SIM", "N'#195'O") NO_HabTextil, '
      'pwd.Login '
      'FROM opcoesapu apu '
      'LEFT JOIN senhas pwd ON pwd.Numero=apu.Codigo ')
    Left = 318
    Top = 424
    object QrOpcoesApUCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOpcoesApUHabFaccao: TSmallintField
      FieldName = 'HabFaccao'
      Required = True
    end
    object QrOpcoesApUHabTextil: TSmallintField
      FieldName = 'HabTextil'
      Required = True
    end
    object QrOpcoesApULogin: TWideStringField
      FieldName = 'Login'
      Size = 30
    end
    object QrOpcoesApUNO_HabFaccao: TWideStringField
      FieldName = 'NO_HabFaccao'
      Required = True
      Size = 3
    end
    object QrOpcoesApUNO_HabTextil: TWideStringField
      FieldName = 'NO_HabTextil'
      Required = True
      Size = 3
    end
    object QrOpcoesApUHabTexTinturaria: TSmallintField
      FieldName = 'HabTexTinturaria'
      Required = True
    end
    object QrOpcoesApUHabTexTecelagem: TSmallintField
      FieldName = 'HabTexTecelagem'
      Required = True
    end
    object QrOpcoesApUHabFacConfeccao: TSmallintField
      FieldName = 'HabFacConfeccao'
      Required = True
    end
  end
  object QrSumBtl: TMySQLQuery
    Database = ZZDB
    SQL.Strings = (
      'SELECT SUM(QtReal) QtReal  '
      'FROM ovgitxgerbtl '
      'WHERE Codigo=:P0 ')
    Left = 610
    Top = 206
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumBtlQtReal: TFloatField
      FieldName = 'QtReal'
      Required = True
    end
  end
  object QrAu2: TMySQLQuery
    Database = ZZDB
    Left = 52
    Top = 168
  end
  object QrLocY: TMySQLQuery
    Database = MyDB
    Left = 428
    Top = 292
    object QrLocYRecord: TIntegerField
      FieldName = 'Record'
    end
  end
  object QrAux3: TMySQLQuery
    Database = MyDB
    Left = 52
    Top = 264
  end
end
