unit UnProjGroup_PF;

interface

uses
  Winapi.Windows, System.SysUtils, System.Classes, Vcl.Forms,
  dmkGeral, UnDMkEnums;

type
  TSvcMsgKind = (smkUnknown=0, smkRunOK=1, smkGetErr=2);
  TUnProjGroup_PF = class(TObject)
  private
    {private declaration}
  public
    {public declaration}
{
    function  FiltroDadosReleventesAlheios_Faccoes(): String;
    function  FiltroDadosReleventesAlheios_Texteis(): String;
    procedure InformaLoadCSV(Texto: String);
    //procedure AtualizaLogFile(Texto: String);
    procedure SalvaEmArquivoLog(SvcMsgKind: TSvcMsgKind; Msg: String);
}
    function  ObtemTipo_ItemDeGraGruY_CodTxt(GraGruY: Integer): String;
    function  ObtemTipo_ItemDeGraGruY_Descri(GraGruY: Integer): String;
  end;

var
  ProjGroup_PF: TUnProjGroup_PF;

implementation

uses UnExes_ProjGroupVars, DmkDAC_PF, UnMyObjects, //UnApp_Vars,
  Module, ModuleGeral;

{ TUnProjGroup_PF }

{
function TUnProjGroup_PF.FiltroDadosReleventesAlheios_Faccoes(): String;
var
  Corda,
  SQL_NrLote,
  SQL_Empresas,
  SQL_Externo,
  SQL_TipoOP,
  SQL_NrSituacaoOP,
  SQL_TipLoclz,
  SQL_TipProdOP,
  SQL_Local: String;
begin
  SQL_NrLote       := '';
  SQL_Empresas     := '';
  SQL_Externo      := '';
  SQL_TipoOP       := '';
  SQL_NrSituacaoOP := '';
  SQL_TipLoclz     := '';
  SQL_TipProdOP    := '';
  SQL_Local        := '';
  case Dmod.QrOpcoesAppLoadCSVSoLote.Value of
    //N/D
    //0: SQL_NrLote := 'WHERE fop.NrLote > -999999999';
    //Com n�mero de lote
    1: SQL_NrLote := 'WHERE fop.NrLote > 0' + sLineBreak;
    //Sem n�mero de lote
    2: SQL_NrLote := 'WHERE fop.NrLote = 0' + sLineBreak;
    //Qualquer situa��o
    3: SQL_NrLote := 'WHERE fop.NrLote > -999999999' + sLineBreak;
    else
    begin
      SQL_NrLote := 'WHERE fop.NrLote > -999999999' + sLineBreak;
      Geral.MB_Aviso(
      'Par�metro "Lote" n�o definido para em "Importa��o de dados" nas op��es espec�ficas.');
    end;
  end;
  //
  if Dmod.QrOpcoesAppLoadCSVEmpresas.Value <> '' then
    if Dmod.QrOpcoesAppLoadCSVEmpresas.Value <> '0' then
      SQL_Empresas := 'AND fop.Empresa IN (' +
      Dmod.QrOpcoesAppLoadCSVEmpresas.Value + ')'  + sLineBreak;
  //
  case Dmod.QrOpcoesAppLoadCSVIntExt.Value of
    //N/D
    //0:
    //Intermos
    1: SQL_Externo := 'AND dlo.Externo="NAO" ' + sLineBreak;
    //Externos
    2: SQL_Externo := 'AND dlo.Externo="SIM" ' + sLineBreak;
    //Ambos
    3: SQL_Externo := '';
    else
    begin
      SQL_Externo := '';
      Geral.MB_Aviso(
      'Par�metro "Locais" n�o definido para em "Importa��o de dados" nas op��es espec�ficas.');
    end;
  end;
  //
  if Dmod.QrOpcoesAppLoadCSVTipOP.Value <> '' then
    SQL_TipoOP := 'AND fop.TipoOP IN (' +
      Dmod.QrOpcoesAppLoadCSVTipOP.Value + ')' + sLineBreak;
  //
  if Dmod.QrOpcoesAppLoadCSVSitOP.Value <> '' then
    SQL_TipoOP := 'AND fop.NrSituacaoOP IN (' +
      Dmod.QrOpcoesAppLoadCSVSitOP.Value + ')' + sLineBreak;
  //
  if Dmod.QrOpcoesAppLoadCSVTipLoclz.Value <> '' then
    SQL_TipLoclz := 'AND fop.TipoLocalizacao IN (' +
      Dmod.QrOpcoesAppLoadCSVTipLoclz.Value + ')' + sLineBreak;
  //
  if Dmod.QrOpcoesAppLoadCSVTipProdOP.Value <> '' then
    SQL_TipProdOP := 'AND fop.NrTipoProducaoOP IN (' +
      Dmod.QrOpcoesAppLoadCSVTipProdOP.Value + ')' + sLineBreak;
  //
  //if Dmod.QrOpcoesAppLoadCSV A t b Valr1.Value <> '' then
  if Dmod.QrOpcoesAppLoadCSVCodValr1.Value <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT DISTINCT Codigo ',
    'FROM ovdclaslocal ',
    (*
    'WHERE  A t r ibNome='  + Dmod.QrOpcoesAppLoadCSV A t b NomLocal.Value,
    'AND A t ribValr IN (' + Dmod.QrOpcoesAppLoadCSV A t b Valr1.Value + ') ',
    *)
    'WHERE CodNome='  + Geral.FF0(Dmod.QrOpcoesAppLoadCSVCodNomLocal.Value),
    'AND CodValr IN (' + Dmod.QrOpcoesAppLoadCSVCodValr1.Value + ') ',
    '']);
    Corda := MyObjects.CordaDeQuery(Dmod.QrAux, 'Codigo');
    if Length(Corda) > 1 then
      SQL_Local := 'AND fop.Local IN (' + Corda + ')' + sLineBreak;
  end;
  //
  Result :=
    SQL_NrLote +
    SQL_Empresas +
    SQL_Externo +
    SQL_TipoOP +
    SQL_NrSituacaoOP +
    SQL_TipLoclz +
    SQL_TipProdOP +
    SQL_Local;
end;

function TUnProjGroup_PF.FiltroDadosReleventesAlheios_Texteis(): String;
var
  Corda,
  SQL_NrLot2,
  SQL_Empresa2,
  SQL_Extern2,
  SQL_TipoO2,
  SQL_NrSituacaoO2,
  SQL_TipLocl2,
  SQL_TipProdO2,
  SQL_Local: String;
begin
  SQL_NrLot2       := '';
  SQL_Empresa2     := '';
  SQL_Extern2      := '';
  SQL_TipoO2       := '';
  SQL_NrSituacaoO2 := '';
  SQL_TipLocl2     := '';
  SQL_TipProdO2    := '';
  SQL_Local        := '';
  case Dmod.QrOpcoesAppLoadCSVSoLot2.Value of
    //N/D
    //0: SQL_NrLote := 'WHERE fop.NrLote > -999999999';
    //Com n�mero de lote
    1: SQL_NrLot2 := 'WHERE fop.NrLote > 0' + sLineBreak;
    //Sem n�mero de lote
    2: SQL_NrLot2 := 'WHERE fop.NrLote = 0' + sLineBreak;
    //Qualquer situa��o
    3: SQL_NrLot2 := 'WHERE fop.NrLote > -999999999' + sLineBreak;
    else
    begin
      SQL_NrLot2 := 'WHERE fop.NrLote > -999999999' + sLineBreak;
      Geral.MB_Aviso(
      'Par�metro "Lote" n�o definido para em "Importa��o de dados" nas op��es espec�ficas.');
    end;
  end;
  //
  if Dmod.QrOpcoesAppLoadCSVEmpresa2.Value <> '' then
    if Dmod.QrOpcoesAppLoadCSVEmpresa2.Value <> '0' then
      SQL_Empresa2 := 'AND fop.Empresa IN (' +
      Dmod.QrOpcoesAppLoadCSVEmpresa2.Value + ')'  + sLineBreak;
  //
  case Dmod.QrOpcoesAppLoadCSVIntEx2.Value of
    //N/D
    //0:
    //Intermos
    1: SQL_Extern2 := 'AND dlo.Externo="NAO" ' + sLineBreak;
    //Externos
    2: SQL_Extern2 := 'AND dlo.Externo="SIM" ' + sLineBreak;
    //Ambos
    3: SQL_Extern2 := '';
    else
    begin
      SQL_Extern2 := '';
      Geral.MB_Aviso(
      'Par�metro "Locais" n�o definido para em "Importa��o de dados" nas op��es espec�ficas.');
    end;
  end;
  //
  if Dmod.QrOpcoesAppLoadCSVTipO2.Value <> '' then
    SQL_TipoO2 := 'AND fop.TipoOP IN (' +
      Dmod.QrOpcoesAppLoadCSVTipO2.Value + ')' + sLineBreak;
  //
  if Dmod.QrOpcoesAppLoadCSVSitO2.Value <> '' then
    SQL_TipoO2 := 'AND fop.NrSituacaoOP IN (' +
      Dmod.QrOpcoesAppLoadCSVSitO2.Value + ')' + sLineBreak;
  //
  if Dmod.QrOpcoesAppLoadCSVTipLocl2.Value <> '' then
    SQL_TipLocl2 := 'AND fop.TipoLocalizacao IN (' +
      Dmod.QrOpcoesAppLoadCSVTipLocl2.Value + ')' + sLineBreak;
  //
  if Dmod.QrOpcoesAppLoadCSVTipProdO2.Value <> '' then
    SQL_TipProdO2 := 'AND fop.NrTipoProducaoOP IN (' +
      Dmod.QrOpcoesAppLoadCSVTipProdO2.Value + ')' + sLineBreak;
  //
  if Dmod.QrOpcoesAppLoadCSVCodValr2.Value <> '' then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT DISTINCT Codigo ',
    'FROM ovdclaslocal ',
    'WHERE CodNome='  + Geral.FF0(Dmod.QrOpcoesAppLoadCSVCodNomLocal.Value),
    'AND CodValr IN (' + Dmod.QrOpcoesAppLoadCSVCodValr2.Value + ') ',
    '']);
    Corda := MyObjects.CordaDeQuery(Dmod.QrAux, 'Codigo');
    if Length(Corda) > 1 then
      SQL_Local := 'AND fop.Local IN (' + Corda + ')' + sLineBreak;
  end;
  //

  Result :=
    SQL_NrLot2 +
    SQL_Empresa2 +
    SQL_Extern2 +
    SQL_TipoO2 +
    SQL_NrSituacaoO2 +
    SQL_TipLocl2 +
    SQL_TipProdO2 +
    SQL_Local;
end;

procedure TUnProjGroup_PF.InformaLoadCSV(Texto: String);
begin
  if VAR_LOAD_CSV_Memo <> nil then
    VAR_LOAD_CSV_Memo.Text := Geral.FDT(DModG.ObtemAgora(), 2) + ' ' + Texto +
    sLineBreak + VAR_LOAD_CSV_Memo.Text;
end;

procedure TUnProjGroup_PF.SalvaEmArquivoLog(SvcMsgKind: TSvcMsgKind; Msg: String);
const
  Dir = 'C:\Dermatek\ExesSvc\Logs\';
var
  ArqLog, aamm, PreTxt: String;
  Lista: TStringList;
begin
  if not DirectoryExists(Dir) then
    ForceDirectories(Dir);
  //
  PreTxt := '[' + Geral.FDT(Now(), 109) + '][' + VAR_RANDM_STR_ONLY_ONE_ACESS + ']:';
  aamm := FormatDateTime('yyyy_mm', Now());
  ArqLog := Dir + 'Log_' + aamm + '.log';
  try
    // Cria lista
    Lista := TStringList.Create;
    try
      // se o arquivo existe, carrega ele;
      if FileExists(ArqLog) then
        Lista.LoadFromFile(ArqLog);
      // adiciona a nova string
      Lista.Add(PreTxt + Application.Name + ': ' + Msg);
    except
      on E: Exception do
        Lista.Add(PreTxt + E.Message);
    end;
  finally
    // Atualiza log
    Lista.SaveToFile(ArqLog);
    // Libera lista
    FreeAndNil(Lista); //.Free;
  end;
end;
}

{ TUnProjGroup_PF }

function TUnProjGroup_PF.ObtemTipo_ItemDeGraGruY_CodTxt(
  GraGruY: Integer): String;
begin
  Result := '';
{
  case GraGruY of
    CO_GraGruY_1024_TXCadNat: Result := '01';
    CO_GraGruY_2048_TXCadInd: Result := '03';
    CO_GraGruY_4096_TXCadInt: Result := '06';
    CO_GraGruY_6144_TXCadFcc: Result := '04';
    else Result := '';
  end;
}
end;

function TUnProjGroup_PF.ObtemTipo_ItemDeGraGruY_Descri(
  GraGruY: Integer): String;
begin
  Result := '';
{
  case GraGruY of
    CO_GraGruY_1024_TXCadNat: Result := '01 - Mat�ria prima';
    CO_GraGruY_2048_TXCadInd: Result := '03 - Produto em processo';
    CO_GraGruY_4096_TXCadInt: Result := '06 - Produto intermedi�rio';
    CO_GraGruY_6144_TXCadFcc: Result := '04 - Produto acabado'; // + sLineBreak +                                        '';
    else Result := '##ERRO## GraGruY indefinido: ' + Geral.FF0(GraGruY);
  end;
}
end;

end.
