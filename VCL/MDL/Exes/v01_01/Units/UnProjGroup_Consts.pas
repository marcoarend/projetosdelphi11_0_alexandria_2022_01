unit UnProjGroup_Consts;

interface

const
  //CO_VERSAO = 1502081747;
  CO_VERMCW: Int64 = 0000000000;
  //CO_VERMLA = 2111231122;
  CO_VERMLA: Int64 = 2112310001;
  CO_VERM28: Int64 = 2301250854;

  CO_SIGLA_APP = 'EXD';
  CO_HARDW_APP = 'DESKTOP';
  CO_VERSAO_BETA = False;
  //
  CO_DMKID_APP = 57;
  CO_GRADE_APP = False;
  CO_VLOCAL = True;
  //
  CO_TabLctA = 'lct'+'0001a';
  CO_EXTRA_LCT_003 = False; // True somente para Credito2
  //
  CO_ADMIN = 'kj4ewf6hew8kfg';

const
  // PushNotifications
  VAR_PushNotifications_CodigoProjeto = ''; //'205807968282';
  VAR_PushNotifications_CodigoAPI     = ''; //'AIzaSyAuoY0fIOMlbozcYpX6EUQXWtthRRCnRoI';
  //
(*&�%$#@!"
  INSTRUCAO_PUSH_NOTIFICATION_EXTRA_001 = 'DmkXtra001';
  INSTRUCAO_PUSH_NOTIFICATION_EXTRA_002 = 'DmkXtra002';
  INSTRUCAO_PUSH_NOTIFICATION_EXTRA_003 = 'DmkXtra003';
  INSTRUCAO_PUSH_NOTIFICATION_EXTRA_004 = 'DmkXtra004';
  INSTRUCAO_PUSH_NOTIFICATION_EXTRA_005 = 'DmkXtra005';
*)
  CO_MAX_INSTRUCOES_PUSH_EXTRA = 5;
(*&�%$#@!"
  CO_TAB_ChmOcoCad = 'chmococad';
*)

  CO_DATA_HORA_GRL = 'DataHora';
  CO_DATA_HORA_SMI = 'DataHora';
  CO_TAB_XX_MOV_ID_LOC = '?????';
  CO_AES_STR_PIN = 'pCZskqhE5du9u';

type
  TMyPushExtras = array[0..CO_MAX_INSTRUCOES_PUSH_EXTRA-1] of String;


implementation

end.

