unit UnExesD_PF;

interface

uses
{
  Forms, StdCtrls, ComCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes,
  Graphics, Controls, Dialogs, Variants, MaskUtils, Registry, ShellAPI, TypInfo,
{
  comobj, ShlObj, Winsock, Math, About, UnMsgInt, mySQLDbTables, DB,
  Vcl.DBCtrls, System.Rtti, dmkEdit, dmkEditCB,
  mySQLExceptions, UnInternalConsts, dmkDBGrid, UnDmkProcFunc,
  UnDmkEnums, UnGrl_Vars, mySQLDirectQuery;
}
  System.Json, (* uLkJSON, UnMyJson, *)
  System.SysUtils, System.Types, Variants,
  Vcl.Graphics, Vcl.Forms, Vcl.ExtCtrls, Vcl.Controls,
  dmkGeral, UnDMkEnums, UnProjGroupEnums, UnExes_ProjGroupVars,
  //
  jpeg,
  //pngimage,
  System.NetEncoding,
  Vcl.ExtDlgs,
  System.Classes,
  UnGrl_Geral;

type
  TDeviceAllowReason = (dalUnknown=0, dalAtivado1aVez=1, dalReativado=2,
                        dalBloqueado=3, dalPerdido=4, dalRoubado=5,
                        dalDemitido=6, dalDevcEmDesuso=7, dalDesatOutros=7);
  TDeviceAcsReason = (darUnknown=0, darOutros=1, darSincroTabs=2,
                      darInspectionUpload=3, darAtivarDispositivo=4);
  TUnExesD_PF = class(TObject)
  private
    {private declaration}
    FIniTick, FFimTick, FDifTick: DWORD;
    FCodLayout, FCodLog: Integer;
    FDataHora_TXT: String;
    FDataHora_DTH: TDateTime;
    //

  public
    {public declaration}
{
    function  AlteraZtatusOVgIspAllSta(IspZtatus, Motivo, Local, NrOP, SeqGrupo,
              NrReduzidoOP: Integer; DtHr: TDateTime): Boolean;
    function  ExcluiZtatusOVgIspAllSta(IspZtatus, Motivo, Local, NrOP, SeqGrupo,
              NrReduzidoOP: Integer; DtHr: TDateTime): Boolean;
}

    function  DefineNomeArquivoFotoDespesa(IDTabela, Codigo, MobInstlSeq,
              CodInMob, CtrlFoto, EXcIdFunc: Integer; Sigla_IdFunc: String;
              EXcStCmprv: Integer; MoedaOri: String;
              ValorOri: Double; DataHora: TDateTime; AnoMes: String): String;
    procedure EXcMobDevAlw_MudaPermissao(MobDevCad: Integer; DeviceID: String;
              AlwReason: TDeviceAllowReason; Data: TDateTime);
    procedure EXcModDevAcs_Acesso(MobDevCad: Integer; DeviceID: String;
              AcsReason: TDeviceAcsReason; Data: TDateTime);
    function  FormataAnoMes_Num(Ano, Mes: Integer): String;

{
    function  InsereOVgIspGerCab(CodAtual, Local, NrOP, SeqGrupo, NrReduzidoOP,
              OVcYnsMed, OVcYnsChk, LimiteChk, LimiteMed, ZtatusIsp, ZtatusMot,
              OVcYnsARQ, PermiFinHow: Integer; DtHrAbert, ZtatusDtH: String;
              SegmntInsp, SeccaoInsp: Integer): Integer;
    function  InsereOVgItxGerCab(SQLType: TSQLType; CodAtual, Local, NrOP, SeqGrupo, NrReduzidoOP,
              OVcYnsExg, (*OVcYnsChk, LimiteChk, LimiteMed,*) ZtatusIsp, ZtatusMot,
              (*OVcYnsARQ,*) PermiFinHow: Integer; DtHrAbert, ZtatusDtH: String;
              SegmntInsp, SeccaoInsp: Integer): Integer;
    function  ObtemSegmentoESecaoDeLocal(const Local: Integer; var SegmntInsp,
              SeccaoInsp: Integer): Boolean;
    function  ObtemSegmntInspDeSeccaoInsp(SeccaoInsp: Integer): Integer;
    function  CorrigeSeccaoInspDeTextil(): Integer;
    function  JSON_RetornoNotificacaoUnicaMobile(const JsonStr: String; var
              MsgRetorno, MsgErro: String): Boolean;
    //function  EnviaNotificaoMobileChamado(): Boolean;
}
    function  CarregaImagemRegistro(Form: TForm; Tabela, CampoB64, CampoPath:
              String; Codigo, MaxWidth, MaxHeight: Integer): Boolean;
{
    function  CarregaImagemRegistroPng(Form: TForm; Tabela, CampoB64, CampoPath:
              String; Codigo, MaxWidth, MaxHeight: Integer): Boolean;
}
    procedure MostraImagem(StrB64: String; ImgB64: TImage; PnImg: TPanel);
    function  IncrementaAnoMes(const AnoMes: String; Incremento: Integer): String;

  end;

var
  ExesD_PF: TUnExesD_PF;
  //

implementation

uses UMySQLModule, UnExesD_Consts, DmkDAC_PF, MyDBCheck,
  Module, ModuleGeral, UnMyObjects, UnDmkImg, MyGlyfs;


{ TUnExesD_PF }

{

function TUnExesD_PF.AlteraZtatusOVgIspAllSta(IspZtatus, Motivo, Local, NrOP,
  SeqGrupo, NrReduzidoOP: Integer; DtHr: TDateTime): Boolean;
var
  DataHora: String;
begin
  Result := False;
  //DataHora := Geral.FDT(DModG.ObtemAgora(), 109);
  DataHora := Geral.FDT(DtHr, 109);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ovgispallsta', False, [
  'Local', 'NrOP', 'SeqGrupo',
  'NrReduzidoOP', 'IspZtatus', 'DataHora',
  'Motivo'], [
  ], [
  Local, NrOP, SeqGrupo,
  NrReduzidoOP, IspZtatus, DataHora,
  Motivo], [
  ], True) then
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ovgisplassta', False, [
    'IspZtatus', 'DataHora', 'Motivo'], [
    'Local', 'NrOP', 'SeqGrupo', 'NrReduzidoOP'], [
    IspZtatus, DataHora, Motivo], [
    Local, NrOP, SeqGrupo, NrReduzidoOP], True);
  end;
end;

function TUnExesD_PF.CorrigeSeccaoInspDeTextil(): Integer;
begin
  Geral.MB_Info('Falta implementar: TUnExesD_PF.CorrigeSeccaoInspDeTextil()');
(*
   Tinturaria incorreto

SELECT loc.*
FROM ovdlocal loc
LEFT JOIN ovdclaslocal cla ON cla.Codigo=loc.Codigo
  AND cla. A t r ibNome="LOCAL"  Mudar para CodNome=10?
WHERE cla. A t r ibValr="TINTURARIA" Mudar para CodValr=4?
AND loc.Codigo IN (
  SELECT DISTINCT itx.Local
  FROM ovgitxgercab itx
  LEFT JOIN ovdlocal lo2 ON lo2.Codigo=itx.Local
  WHERE itx.SeccaoInsp=1024
)

  Corre��o Tinturaria:

UPDATE ovgitxgercab
SET SeccaoInsp=2048
WHERE Local IN (1341,3501,7522)



   Textil incorreto

SELECT loc.*
FROM ovdlocal loc
LEFT JOIN ovdclaslocal cla ON cla.Codigo=loc.Codigo
  AND cla. A t r ibNome="LOCAL"  Mudar para CodNome=10?
WHERE cla. A t r ibValr="TEXTIL" Mudar para CodValr=4?
AND loc.Codigo IN (
  SELECT DISTINCT itx.Local
  FROM ovgitxgercab itx
  LEFT JOIN ovdlocal lo2 ON lo2.Codigo=itx.Local
  WHERE itx.SeccaoInsp=2048
)
*)
end;

function TUnExesD_PF.ExcluiZtatusOVgIspAllSta(IspZtatus, Motivo, Local, NrOP,
  SeqGrupo, NrReduzidoOP: Integer; DtHr: TDateTime): Boolean;
var
  DataHora: String;
begin
  Result := False;
  Dmod.MyDB.Execute('DELETE FROM ovfordemproducao WHERE' +
    ' Local=' + Geral.FF0(Local) +
    ' AND NrOP=' + Geral.FF0(NrOP) +
    ' AND SeqGrupo=' + Geral.FF0(SeqGrupo) +
    ' AND NrReduzidoOP=' + Geral.FF0(NrReduzidoOP));
  Dmod.MyDB.Execute('DELETE FROM ovmordemproducao WHERE' +
    ' Local=' + Geral.FF0(Local) +
    ' AND NrOP=' + Geral.FF0(NrOP) +
    ' AND SeqGrupo=' + Geral.FF0(SeqGrupo) +
    ' AND NrReduzidoOP=' + Geral.FF0(NrReduzidoOP));
  Dmod.MyDB.Execute('DELETE FROM ovgisplassta WHERE' +
    ' Local=' + Geral.FF0(Local) +
    ' AND NrOP=' + Geral.FF0(NrOP) +
    ' AND SeqGrupo=' + Geral.FF0(SeqGrupo) +
    ' AND NrReduzidoOP=' + Geral.FF0(NrReduzidoOP));
end;

function TUnExesD_PF.InsereOVgIspGerCab(CodAtual, Local, NrOP, SeqGrupo,
  NrReduzidoOP, OVcYnsMed, OVcYnsChk, LimiteChk, LimiteMed, ZtatusIsp,
  ZtatusMot, OVcYnsARQ, PermiFinHow: Integer; DtHrAbert, ZtatusDtH: String;
  SegmntInsp, SeccaoInsp: Integer): Integer;
var
  SQLType: TSQLType;
  Agora: TDateTime;
  Codigo: Integer;
begin
  Codigo := UMyMod.BPGS1I32('ovgispgercab', 'Codigo', '', '', tsPos, SQLType, CodAtual);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovgispgercab', False, [
  'Local', 'NrOP', 'SeqGrupo',
  'NrReduzidoOP', 'DtHrAbert', (*'DtHrFecha',*)
  'OVcYnsMed', 'OVcYnsChk', 'LimiteChk',
  'LimiteMed', 'ZtatusIsp', 'ZtatusDtH',
  'ZtatusMot', 'OVcYnsARQ', 'PermiFinHow',
  'SegmntInsp', 'SeccaoInsp'], [
  'Codigo'], [
  Local, NrOP, SeqGrupo,
  NrReduzidoOP, DtHrAbert, (*DtHrFecha,*)
  OVcYnsMed, OVcYnsChk, LimiteChk,
  LimiteMed, ZtatusIsp, ZtatusDtH,
  ZtatusMot, OVcYnsARQ, PermiFinHow,
  SegmntInsp, SeccaoInsp], [
  Codigo], True) then
  begin
    //FCodigo := Codigo;
    Result := Codigo;
    if SQLType = stIns then
      ExesD_PF.AlteraZtatusOVgIspAllSta(CO_O V S_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE,
        ZtatusMot, Local, NrOP, SeqGrupo, NrReduzidoOP, Agora);
     //Close;
  end;
end;

function TUnExesD_PF.InsereOVgItxGerCab(SQLType: TSQLType; CodAtual, Local, NrOP, SeqGrupo,
  NrReduzidoOP, OVcYnsExg, ZtatusIsp, ZtatusMot, PermiFinHow: Integer;
  DtHrAbert, ZtatusDtH: String; SegmntInsp, SeccaoInsp: Integer): Integer;
var
  Agora: TDateTime;
  Codigo: Integer;
begin
  Codigo := UMyMod.BPGS1I32('ovgitxgercab', 'Codigo', '', '', tsPos, SQLType, CodAtual);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovgitxgercab', False, [
  'Local', 'NrOP', 'SeqGrupo',
  'NrReduzidoOP', 'DtHrAbert', (*'DtHrFecha',*)
  'OVcYnsExg', //'OVcYnsChk', 'LimiteChk',
  //'LimiteMed',
  'ZtatusIsp', 'ZtatusDtH',
  'ZtatusMot', (*'OVcYnsARQ',*) 'PermiFinHow',
  'SegmntInsp', 'SeccaoInsp'], [
  'Codigo'], [
  Local, NrOP, SeqGrupo,
  NrReduzidoOP, DtHrAbert, (*DtHrFecha,*)
  OVcYnsExg, //OVcYnsChk, LimiteChk,
  //LimiteMed,
  ZtatusIsp, ZtatusDtH,
  ZtatusMot, (*OVcYnsARQ,*) PermiFinHow,
  SegmntInsp, SeccaoInsp], [
  Codigo], True) then
  begin
    //FCodigo := Codigo;
    Result := Codigo;
    if SQLType = stIns then
      ExesD_PF.AlteraZtatusOVgIspAllSta(CO_O V S_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE,
        ZtatusMot, Local, NrOP, SeqGrupo, NrReduzidoOP, Agora);
     //Close;
  end;
end;

function TUnExesD_PF.JSON_RetornoNotificacaoUnicaMobile(const JsonStr: String; var
  MsgRetorno, MsgErro: String): Boolean;
var
  jsonObj   : TJSONObject;
  jsubObj   : TJSONObject;
  //
  jv        : TJSONValue;
  LProducts : TJSONValue;
  LProduct  : TJSONValue;
  //
  LJPair    : TJSONPair;
  //
  LSize, LIndex: Integer;
  s: String;
begin
  MsgRetorno := '';
  MsgErro    := '';
  //
  jsonObj := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(JsonStr), 0)
    as TJSONObject;
  jv := jsonobj.Get('success').JsonValue;

  s := jv.Value;
  Result := Geral.IMV(s) > 0;

  //

  if Result = False then
  begin
    // Mensagem de retorno:
    LProducts := jsonObj.Get('results').JsonValue;
    MsgRetorno := LProducts.ToJSON;
    //s := LProducts.GetValue<string>('results[0].error');
    //Geral.MB_Info(s);
    //EXIT;
    LSize     := TJSONArray(LProducts).Size;
    if LSize > 0 then
    begin
      LIndex   := 0;
      LProduct := TJSONArray(LProducts).Get(LIndex);
      //
      //Geral.MB_Info(LProduct.ToJSON);
      jv := TJSONObject(LProduct).Get('error').JsonValue;
      MsgErro := jv.Value;
    end;
  end;
end;

function TUnExesD_PF.ObtemSegmentoESecaoDeLocal(const Local: Integer;
  var SegmntInsp, SeccaoInsp: Integer): Boolean;
var
  sLocal,
  sNada, sConfeccao, sTecelagem, sTinturaria: String;
begin
  SegmntInsp  := 0;
  SeccaoInsp  := 0;
  //
  sLocal := Geral.FF0(Dmod.QrOpcoesAppLoadCSVCodNomLocal.Value);
  //
  sConfeccao  := '';
  sTecelagem  := '';
  sTinturaria := '';
  //
  if (Dmod.QrOpcoesAppCdScConfeccao.Value <> 0)
  or (Dmod.QrOpcoesAppCdScTecelagem.Value <> 0)
  or (Dmod.QrOpcoesAppCdScTinturaria.Value <> 0) then
  begin
    sNada := '  WHEN 0 THEN 0 ';
    //
    if Dmod.QrOpcoesAppCdScConfeccao.Value <> 0 then
      sConfeccao  := '  WHEN ' + Geral.FF0(Dmod.QrOpcoesAppCdScConfeccao.Value) +
                     ' THEN ' + Geral.FF0(Integer(TSeccaoInsp.sccinspConfeccao));

    if Dmod.QrOpcoesAppCdScTecelagem.Value <> 0 then
      sTecelagem  := '  WHEN ' + Geral.FF0(Dmod.QrOpcoesAppCdScTecelagem.Value) +
                     ' THEN ' + Geral.FF0(Integer(TSeccaoInsp.sccinspTecelagem));

    if Dmod.QrOpcoesAppCdScTinturaria.Value <> 0 then
      sTinturaria := '  WHEN ' + Geral.FF0(Dmod.QrOpcoesAppCdScTinturaria.Value) +
                     ' THEN ' + Geral.FF0(Integer(TSeccaoInsp.sccinspTinturaria));

    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT CASE cla.CodValr ',
       sNada,
       sConfeccao,
       sTecelagem,
       sTinturaria,
    '  ELSE 0  ',
    'END SeccaoInsp ',
    'FROM ovdlocal loc ',
    'LEFT JOIN ovdclaslocal cla ON cla.Codigo=loc.Codigo ',
    '  AND cla.CodNome="' + sLocal + '" ',
    'WHERE loc.Codigo=' + Geral.FF0(Local),
    'AND cla.CodNome="' + sLocal + '" ',
    EmptyStr]);
    //
    if Dmod.QrAux.RecordCount > 0 then
    begin
      SeccaoInsp := Dmod.QrAux.FieldByName('SeccaoInsp').AsInteger;
      SegmntInsp := ObtemSegmntInspDeSeccaoInsp(SeccaoInsp);
    end;
  end;
end;

function TUnExesD_PF.ObtemSegmntInspDeSeccaoInsp(SeccaoInsp: Integer): Integer;
const
  sProcName = 'TUnExesD_PF.ObtemSegmntInspDeSeccaoInsp()';
begin
  case TSeccaoInsp(SeccaoInsp) of
    sccinspNone(*0*)         : Result := CO_SGMT_INSP_0000_INDEFINIDO;
    sccinspTecelagem(*1024*) : Result := CO_SGMT_INSP_2048_TEXTIL;
    sccinspTinturaria(*2048*): Result := CO_SGMT_INSP_2048_TEXTIL;
    sccinspConfeccao(*3072*) : Result := CO_SGMT_INSP_1024_FACCAO;
    else
    begin
      Result := CO_SGMT_INSP_0000_INDEFINIDO;
      Geral.MB_Erro('SeccaoInsp n�o definido em ' + sProcName);
    end;
  end;
end;
}

function TUnExesD_PF.CarregaImagemRegistro(Form: TForm; Tabela, CampoB64,
  CampoPath: String; Codigo, MaxWidth, MaxHeight: Integer): Boolean;
const
  Titulo = 'Defini��o Imagem de bot�o';
var
  IniDir, Arquivo, Filtro, ImgB64, ImgPath: String;
  Bmp1, Bmp2: TBitmap;
begin
  Result := False;
  IniDir  := 'C:\Dermatek\Logos\Glyphs\';
  Arquivo := '';
  //Filtro  := 'Arquivos Bitmap|*.bmp;Arquivos JPEG|*.jpeg;Arquivos JPG|*.jpg';
  Filtro  := 'Arquivos Bitmap|*.bmp';
  //
  if MyObjects.FileOpenDialog(Form, IniDir, Arquivo, Titulo, Filtro, [], Arquivo) then
  begin
    if FileExists(Arquivo) then
    begin
      Screen.Cursor := crHourGlass;
      //Screen.Cursor := crHourGlass;
      try
        Bmp1 := TBitmap.Create;
        try
          Bmp1.LoadFromFile(Arquivo);
          if (Bmp1.Width > MaxWidth) or (Bmp1.Height > MaxHeight) then
          begin
            Geral.MB_Aviso('Imagem excede tamanho m�ximo: ' +
            Geral.FF0(MaxWidth) + ' x ' + Geral.FF0(MaxHeight));
            Result := False;
            Exit;
          end;
          //ImgB64 := DmkImg.Base64FromBitmap2(Bitmap);
          ImgB64 := DmkImg.Base64FromBitmap(Bmp1);
          ImgPath := Arquivo;
          //Codigo := QrEXcMdoPagCodigo.Value;
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False, [
          CampoB64, CampoPath], [
          'Codigo'], [
          ImgB64, ImgPath], [
          Codigo], True) then
          begin
            //LocCod(Codigo, Codigo);
            Result := True;
          end;
        finally
          Bmp1.Free;
        end;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

function TUnExesD_PF.DefineNomeArquivoFotoDespesa(IDTabela, Codigo, MobInstlSeq,
  CodInMob, CtrlFoto, EXcIdFunc: Integer; Sigla_IdFunc: String; EXcStCmprv:
  Integer; MoedaOri: String; ValorOri: Double;
  DataHora: TDateTime; AnoMes: String): String;
  //
  function ValToStr(Val: Double): String;
  var
    Str: String;
  begin
    Str := Geral.SoNumero_TT(Geral.FFT(Val, 2, siPositivo));
    while Length(Str) < 10 do
      Str := '0' + Str;
    Result := '_' + Str;
  end;
var
  SubDir: String;
begin
  SubDir := Sigla_IdFunc + '_' + Geral.FF0(EXcIDFunc) + '\' + AnoMes + '\';
  ForceDirectories(VAR_DIR_FOTOS_DESPESAS + SubDir);
  Result := SubDir +
            // Arquivo
            'EXS_' + IntToStr(IDTabela) + '_' +
            Geral.FFN(Codigo, 4) + '_' +
            Geral.FFN(MobInstlSeq, 3) + '_' +
            Geral.FFN(CodInMob, 3) + '_' +
            Geral.FFN(CtrlFoto, 4) + '_' +
            Geral.FFN(EXcIdFunc, 5) + '_' +
            FormatDateTime('YYYYMMDD_HHNNSS', DataHora) + '_' +
            MoedaOri + '_' +
            ValToStr(ValorOri) + '_' +
            Geral.FF0(EXcStCmprv) + '.jpg';
end;

procedure TUnExesD_PF.EXcMobDevAlw_MudaPermissao(MobDevCad: Integer;
  DeviceID: String; AlwReason: TDeviceAllowReason; Data: TDateTime);
var
  // DeviceID,
  Nome, DtaInicio: String;
  //AlwReason,
  Codigo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := stIns;
  Codigo         := 0;
  Nome           := '';
  //MobDevCad      := ;
  //DeviceID       := ;
  //AlwReason      := ; // 0=Unknown 1=Ativado 1a vez 2=Reativado 3=Bloqueado 4=Perdido 5=Roubado 6=Demitido 7=Desat.Outros
  if Data > 2 then
    DtaInicio      := Geral.FDT(Data, 109)
  else
    DtaInicio      := Geral.FDT(DModG.ObtemAgora(), 109);
  //
  Codigo := UMyMod.BPGS1I32('excmobdevalw', 'Codigo', '', '', tsPos, stIns, Codigo);
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'excmobdevalw', False, [
  'Nome', 'MobDevCad', 'DeviceID',
  'AlwReason', 'DtaInicio'], [
  'Codigo'], [
  Nome, MobDevCad, DeviceID,
  Integer(AlwReason), DtaInicio], [
  Codigo], True);
end;

procedure TUnExesD_PF.EXcModDevAcs_Acesso(MobDevCad: Integer; DeviceID: String;
  AcsReason: TDeviceAcsReason; Data: TDateTime);
var
  Nome, (*DeviceID,*) DtaAcsIni, DtaAcsFim: String;
  Codigo(*, MobDevCad, AcsReason*): Integer;
  SQLType: TSQLType;
begin
  SQLType        := stIns;
  Codigo         := 0;
  Nome           := '';
  //MobDevCad      := ;
  //DeviceID       := ;
  //AcsReason      := ; //0=Unknown 1= Outros 2=SincroTabs 3=InspectionUpload 4=Ativar dispositivo
  if Data > 2 then
    DtaAcsIni      := Geral.FDT(Data, 109)
  else
    DtaAcsIni      := Geral.FDT(DModG.ObtemAgora(), 109);
  //
  DtaAcsFim      := '0000-00-00';
  //
  Codigo := UMyMod.BPGS1I32('excmobdevacs', 'Codigo', '', '', tsPos, stIns, Codigo);
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'excmobdevacs', False, [
  'Nome', 'MobDevCad', 'DeviceID',
  'AcsReason', 'DtaAcsIni', 'DtaAcsFim'], [
  'Codigo'], [
  Nome, MobDevCad, DeviceID,
  Integer(AcsReason), DtaAcsIni, DtaAcsFim], [
  Codigo], True);
end;

function TUnExesD_PF.FormataAnoMes_Num(Ano, Mes: Integer): String;
begin
  Result := IntToStr(Ano) + '-' + Grl_Geral.FFN(Mes, 2);
end;

function TUnExesD_PF.IncrementaAnoMes(const AnoMes: String;
  Incremento: Integer): String;
var
  Ano, Mes: Integer;
  P: Integer;
  Data: TDateTime;
begin
  //Result := AnoMes;
  //
  Ano    := Grl_Geral.IMV(Copy(AnoMes, 1, 4));
  Mes    := Grl_Geral.IMV(Copy(AnoMes, 6));
  //
  Mes    := Mes + Incremento;
  while Mes > 12 do
  begin
    Ano := Ano + 1;
    Mes := Mes - 12;
  end;
  while Mes < 1 do
  begin
    Mes := Mes + 12;
    Ano := Ano - 1;
  end;
  Result := FormataAnoMes_Num(Ano, Mes);
end;

procedure TUnExesD_PF.MostraImagem(StrB64: String; ImgB64: TImage; PnImg: TPanel);
var
  Bmp2: TBitmap;
begin
  if StrB64 <> EmptyStr then
  begin
    Bmp2 := TBitmap.Create;
    try
      Bmp2 := DmkImg.BitmapFromBase64(StrB64);
      ImgB64.Picture.Bitmap.Assign(Bmp2);
      ImgB64.Visible := True;
      PnImg.DoubleBuffered := True;
      PnImg.Visible := False;
      PnImg.Visible := True;
    finally
      Bmp2.Free;
    end;
  end else
    ImgB64.Visible := False;
end;

end.
