unit UnExesD_Consts;

interface

const

  // PushNotifications
  VAR_PushNotifications_CodigoProjeto = ''; // o v e r s e e r >> '205807968282';
(*

// Status importa��o dados alheio
  // 0 ???
  CO_O V S_IMPORT_ALHEIO_0000_INDEFINIDO = 0;
  // 0512 - Exclu�do
  CO_O V S_IMPORT_ALHEIO_0512_EXCLUIDO = 512; // erro ao importar!
  // 1024 - Importado
  CO_O V S_IMPORT_ALHEIO_1024_IMPORTADO = 1024;
  // 2048 - Ignorado
  CO_O V S_IMPORT_ALHEIO_2048_DESOBRIGADO = 2048;
  // 3072 - Configurado
  CO_O V S_IMPORT_ALHEIO_3972_CONFIGURADO = 3072;
  // 4096 - Apto ao Upload (n�o usado >> direto)
  CO_O V S_IMPORT_ALHEIO_4096_APTO_UPWEBSERVR = 4096;
  // 5120 - Apto ao download mobile
  CO_O V S_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE = 5120;
  // 6144 - Item encerrado (Inspe��es encerradas)
  CO_O V S_IMPORT_ALHEIO_6144_ENCERRADO = 6144;
  //
  //
  //
// Resultado final de uma inspe��o
// 0=Indefinido,
  CO_INSP_RESUL_0000_INDEFINIDO = 0;
  // 1024 - Aprovado
  CO_INSP_RESUL_1024_APROVADO   = 1024;
  // 2048 - Aprovado com resalvas
  CO_INSP_RESUL_2048_APR_RESALV = 2048;
  // 3072 - Rejeitado
  CO_INSP_RESUL_3072_REJEITADO  = 3072;
  //
///
// Segmentos de produ��o (aptos para inspe��o)
// 0=Indefinido,
  CO_SGMT_INSP_0000_INDEFINIDO = 0;
  // 1024 - Confec��o
  CO_SGMT_INSP_1024_FACCAO   = 1024;
  // 2048 - Tecelagem, Tinturaria, etc
  CO_SGMT_INSP_2048_TEXTIL   = 2048;
  //
///
///  Tipos de tabelas em pontos negativos de inconformidades:
  CO_MEM_FLD_TAB_0_Faccao_Inconformidade = 'Inconformidade-Fac��o';
  CO_MEM_FLD_TAB_1_Faccao_Medida         = 'Medida-Fac��o';
  CO_MEM_FLD_TAB_2_Faccao_LivreTexto     = 'Livre texto-Fac��o';
  CO_MEM_FLD_TAB_3_Textil_Exigencia      = 'Exig�ncia-T�xtil';
  CO_MEM_FLD_TAB_4_Textil_LivreTexto     = 'Livre texto-T�xtil';
  //
  CO_ENCERRA_INSPECAO_0_Indefinido  = 'Indefinido';
  CO_ENCERRA_INSPECAO_1_AoFinalizar = 'Somente ao finalizar toda inspe��o';
  CO_ENCERRA_INSPECAO_2_AoReprevar  = 'Ao atingir a pontua��o de reprova��o';
  CO_ENCERRA_INSPECAO_3_QualqrMomnt = 'A qualquer momento';
  sCO_ENCERRA_INSPECAO: array[0..3] of String = (
    CO_ENCERRA_INSPECAO_0_Indefinido,
    CO_ENCERRA_INSPECAO_1_AoFinalizar,
    CO_ENCERRA_INSPECAO_2_AoReprevar,
    CO_ENCERRA_INSPECAO_3_QualqrMomnt
  );
*)

  CO_KndOnde_0_COD_INDEF      = 0; // 0 = Indefinido,
  CO_KndOnde_1_COD_LIVRE      = 1; // 1 = Livre,
  CO_KndOnde_2_COD_EstqCenCad = 2; // 2 = tab EstqCenCad
  CO_KndOnde_3_COD_OVdLocal   = 3; // 3 = tab OVdLocal

  CO_KndOnde_0_TXT_INDEF      = 'Indefinido'; // 0 = Indefinido,
  CO_KndOnde_1_TXT_LIVRE      = 'Livre';      // 1 = Livre,
  CO_KndOnde_2_TXT_EstqCenCad = 'Estoque';    // 2 = tab EstqCenCad
  CO_KndOnde_3_TXT_OVdLocal   = 'Prestador';  // 3 = tab OVdLocal

  CO_KndIsWhoDo_0_COD_Ninguem    = 0;
  CO_KndIsWhoDo_1_COD_Especifico = 1;
  CO_KndIsWhoDo_2_COD_Dentre     = 2;
  CO_KndIsWhoDo_3_COD_Alguns     = 3;
  CO_KndIsWhoDo_4_COD_Todos      = 4;

  CO_KndIsWhoDo_0_TXT_Ninguem    = 'Ningu�m';
  CO_KndIsWhoDo_1_TXT_Especifico = 'Espec�fico';
  CO_KndIsWhoDo_2_TXT_Dentre     = 'Dentre';
  CO_KndIsWhoDo_3_TXT_Alguns     = 'Alguns';
  CO_KndIsWhoDo_4_TXT_Todos      = 'Todos';

implementation

end.

