unit UnExesD_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, Menus, UnDmkEnums, mySQLDbTables, Forms,
  Grids, DBGrids, UnInternalConsts, Variants;

type
  TUnExesD_Jan = class(TObject)
  private
    { Private declarations }
    procedure AvisoDadosTerceiros();
  public
    { Public declarations }
{
    function  LocalizaOVgIspGerCab(var Codigo: Integer): Boolean;
    function  LocalizaOVgIspPrfCab(var Codigo: Integer): Boolean;

    function  LocalizaOVgItxGerCab(var Codigo: Integer): Boolean;
    function  LocalizaOVgItxPrfCab(var Codigo: Integer): Boolean;
}

    procedure MostraFormImportaCSV_ERP_01();

{
    procedure MostraFormOVdCiclo();
    procedure MostraFormOVdLocal();
    procedure MostraFormOVdReferencia();
    procedure MostraFormOVdLote();
    procedure MostraFormOVdProduto();

    procedure MostraFormOVcYnsQstTop();
    procedure MostraFormOVcYnsQstCtx();
    procedure MostraFormOVcYnsQstMag();

    procedure MostraFormOVcYnsChkCad();

    procedure MostraFormOVcYnsGraTop();

    procedure MostraFormOVcYnsARQCad();

    procedure MostraFormOVcYnsMedCad(Artigo: Integer);

    procedure MostraFormOVfOPGerFil_Fac(MostraSeEmpty: Boolean);
    procedure MostraFormOVfOPGerFil_Tex(MostraSeEmpty: Boolean);

    function  MostraFormOVgIspGerCad(SQLType: TSQLType; Codigo, Local: Integer;
              NO_Local: String; NrOP, SeqGrupo: Integer; NO_SeqGrupo: String;
              NrReduzidoOP, OVcYnsMed, OVcYnsChk, OVcYnsARQ, LimiteMed,
              LimiteChk, PermiFinHow, ZtatusIsp: Integer): Integer;
    procedure MostraFormOVgIspGerCab(Codigo: Integer);
    function  MostraFormOVgIspPrfCad(SQLType: TSQLType; Codigo, SeqGrupo:
              Integer; NO_SeqGrupo, Referencia: String; OVcYnsMed, OVcYnsChk,
              OVcYnsARQ, LimiteMed, LimiteChk, PermiFinHow, Ativo: Integer;
              Nome: String): Integer;
    procedure MostraFormOVgIspPrfCab(Codigo: Integer);
    procedure MostraFormOVmIspDevCab(Codigo: Integer);
}

    procedure MostraFormEXcMobDevCad();
    procedure MostraFormEXcIdFunc(Codigo: Integer);


{
    procedure MostraFormOVpLayEsq(Esquema: Integer);
    procedure MostraFormOVcYnsMixTop();
    procedure MostraFormOVcYnsExgCad(Artigo: Integer);

    function  MostraFormOVgItxGerCad(SQLType: TSQLType; Codigo, Local: Integer;
              NO_Local: String; NrOP, SeqGrupo: Integer; NO_SeqGrupo: String;
              NrReduzidoOP, OVcYnsExg, (*OVcYnsChk, OVcYnsARQ, LimiteMed,
              LimiteChk,*) PermiFinHow, ZtatusIsp: Integer): Integer;
    procedure MostraFormOVgItxGerCab(Codigo: Integer);
    function  MostraFormOVgItxPrfCad(SQLType: TSQLType; Codigo, SeqGrupo:
              Integer; NO_SeqGrupo, Referencia: String; OVcYnsExg, (*OVcYnsChk,
              OVcYnsARQ, LimiteMed, LimiteChk,*) PermiFinHow, Ativo: Integer;
              Nome: String): Integer;
    procedure MostraFormOVgItxPrfCab(Codigo: Integer);
    procedure MostraFormOVmItxDevCab(Codigo: Integer);

    procedure MostraFormSeccTexTinturaria(Forca: Boolean);
}
    procedure MostraFormComptncPrf(Codigo: Integer);
    procedure MostraFormEXcStCmprv(Codigo: Integer);
    procedure MostraFormEXcCtaPag(Codigo: Integer);
    procedure MostraFormEXcMdoPag(Codigo: Integer);
    procedure MostraFormEXcMoeCad(Codigo: Integer);
    procedure MostraFormEXcGruCta(Codigo: Integer);
    procedure MostraFormLoclzL10nDuo(Entidade: Integer);
    procedure MostraFormLoclzL10nCab(Codigo: Integer);
    procedure MostraFormFmPrmsCompny(Codigo: Integer);
    procedure MostraFormFmLoclzL10nEd1(QrIts: TmySQLQuery; TableName, IdxFld1,
              IdxFld2, Idx1Descri, Idx2Descri, FldTradu1, Txt1ptBR, Txt1enUS,
              TxtTradu1: String; IdxVal1, IdxVal2: Integer);
    procedure MostraFormFmLoclzL10nEd2(QrIts: TmySQLQuery; TableName, IdxFld1,
              IdxFld2, Idx1Descri, Idx2Descri, FldTradu1, FldTradu2, Txt1ptBR,
              Txt1enUS, Txt2ptBR, Txt2enUS, TxtTradu1, TxtTradu2: String;
              IdxVal1, IdxVal2: Integer);
    procedure MostraFormFmLoclzL10nEd3(QrIts: TmySQLQuery; TableName, IdxFld1,
              IdxFld2, Idx1Descri, Idx2Descri, FldTradu1, Txt1ptBR, Txt1enUS,
              TxtTradu1: String; IdxVal1, IdxVal2: Integer;
              Sigla, DocHeader, ItemText, Reference, DescrCondiPg: String;
              RegrQtdDia, RegrTipSeq, RegrDiaSem: Integer);

  end;

var
  ExesD_Jan: TUnExesD_Jan;


implementation

uses
  dmkGeral, MyDBCheck, Module,
{
  OVdCiclo, OVdLocal,  OVdProduto, OVdReferencia, (*OVdLote,*)
  OVcYnsQstTop,
  OVcYnsChkCad, OVcYnsGraTop, OVcYnsARQCad, OVcYnsMedCad,
  ImportaCSV_ERP_01, OVgIspPrfCfg,
  OVfOPGerFil_Fac, OVfOPGerFil_Tex,
  OVgIspGerCad, OVgIspGerCab, OVmIspDevCab, OVgIspPrfCab, OVgIspPrfCad,
  OVgItxPrfCad,
  OVgLocIGC, OVgLocIPC,
  OVpLayEsq,
  OVcYnsMixTop, OVcYnsExgCad,
  OVgItxGerCad, OVgItxGerCab, OVmItxDevCab, OVgItxPrfCab,
  OVgLocJGC, OVgLocJPC,
  SeccTexTinturaria, OVgItxGerBtl;
}

  CfgCadLista, EXcMobDevCad, ComptncPrf, EXcIdFunc, EXcStCmprv, EXcMdoPag,
  EXcCtaPag, EXcMoeCad, EXcGruCta, LoclzL10nDuo, LoclzL10nCab, PrmsCompny,
  LoclzL10nEd1, LoclzL10nEd2, LoclzL10nEd3, DmkDAC_PF;

{ TUnExesD_Jan }

procedure TUnExesD_Jan.AvisoDadosTerceiros();
begin
  Geral.MB_Info(
  'Os dados solicitados foram importados de base de dados de terceiros e n�o ' +
  '� aconselhavel edit�-los pois podem ser sobrescrevidos na pr�xima importa��o!!!');
end;

{
function TUnExesD_Jan.LocalizaOVgIspGerCab(var Codigo: Integer): Boolean;
begin
  if DBCheck.CriaFm(TFmOVgLocIGC, FmOVgLocIGC, afmoNegarComAviso) then
  begin
    FmOVgLocIGC.ShowModal;
    Codigo := FmOVgLocIGC.FCodigo;
    FmOVgLocIGC.Destroy;
    Result := Codigo <> 0;
  end;
end;

function TUnExesD_Jan.LocalizaOVgIspPrfCab(var Codigo: Integer): Boolean;
begin
  if DBCheck.CriaFm(TFmOVgLocIPC, FmOVgLocIPC, afmoNegarComAviso) then
  begin
    FmOVgLocIPC.ShowModal;
    Codigo := FmOVgLocIPC.FCodigo;
    FmOVgLocIPC.Destroy;
    Result := Codigo <> 0;
  end;
end;

function TUnExesD_Jan.LocalizaOVgItxGerCab(var Codigo: Integer): Boolean;
begin
  if DBCheck.CriaFm(TFmOVgLocJGC, FmOVgLocJGC, afmoNegarComAviso) then
  begin
    FmOVgLocJGC.ShowModal;
    Codigo := FmOVgLocJGC.FCodigo;
    FmOVgLocJGC.Destroy;
    Result := Codigo <> 0;
  end;
end;

function TUnExesD_Jan.LocalizaOVgItxPrfCab(var Codigo: Integer): Boolean;
begin
  if DBCheck.CriaFm(TFmOVgLocJPC, FmOVgLocJPC, afmoNegarComAviso) then
  begin
    FmOVgLocJPC.ShowModal;
    Codigo := FmOVgLocJPC.FCodigo;
    FmOVgLocJPC.Destroy;
    Result := Codigo <> 0;
  end;
end;
}

procedure TUnExesD_Jan.MostraFormImportaCSV_ERP_01();
begin
(*&�%$#@!"
  if DBCheck.CriaFm(TFmImportaCSV_ERP_01, FmImportaCSV_ERP_01, afmoNegarComAviso) then
  begin
    FmImportaCSV_ERP_01.ShowModal;
    FmImportaCSV_ERP_01.Destroy;
  end;
*)
end;

procedure TUnExesD_Jan.MostraFormLoclzL10nDuo(Entidade: Integer);
begin
  if DBCheck.CriaFm(TFmLoclzL10nDuo, FmLoclzL10nDuo, afmoNegarComAviso) then
  begin
    if Entidade <> 0 then
    begin
      if FmLoclzL10nDuo.QrCompanies.Locate('Codigo', Entidade, []) then
        //FmLoclzL10nDuo.DBGCompanies.Enabled := False
        ;
    end;
    FmLoclzL10nDuo.ShowModal;
    FmLoclzL10nDuo.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormLoclzL10nCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmLoclzL10nCab, FmLoclzL10nCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmLoclzL10nCab.LocCod(Codigo, Codigo);
    FmLoclzL10nCab.ShowModal;
    FmLoclzL10nCab.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormComptncPrf(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmComptncPrf, FmComptncPrf, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmComptncPrf.LocCod(Codigo, Codigo);
    FmComptncPrf.ShowModal;
    FmComptncPrf.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormEXcCtaPag(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmEXcCtaPag, FmEXcCtaPag, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmEXcCtaPag.LocCod(Codigo, Codigo);
    FmEXcCtaPag.ShowModal;
    FmEXcCtaPag.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormEXcGruCta(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmEXcGruCta, FmEXcGruCta, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmEXcGruCta.LocCod(Codigo, Codigo);
    FmEXcGruCta.ShowModal;
    FmEXcGruCta.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormEXcIdFunc(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmEXcIdFunc, FmEXcIdFunc, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmEXcIdFunc.LocCod(Codigo, Codigo);
    FmEXcIdFunc.ShowModal;
    FmEXcIdFunc.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormEXcMdoPag(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmEXcMdoPag, FmEXcMdoPag, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmEXcMdoPag.LocCod(Codigo, Codigo);
    FmEXcMdoPag.ShowModal;
    FmEXcMdoPag.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormEXcMobDevCad();
begin
  if DBCheck.CriaFm(TFmEXcMobDevCad, FmEXcMobDevCad, afmoNegarComAviso) then
  begin
    FmEXcMobDevCad.ShowModal;
    FmEXcMobDevCad.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormEXcMoeCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmEXcMoeCad, FmEXcMoeCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmEXcMoeCad.LocCod(Codigo, Codigo);
    FmEXcMoeCad.ShowModal;
    FmEXcMoeCad.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormEXcStCmprv(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmEXcStCmprv, FmEXcStCmprv, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmEXcStCmprv.LocCod(Codigo, Codigo);
    FmEXcStCmprv.ShowModal;
    FmEXcStCmprv.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormFmLoclzL10nEd1(QrIts: TmySQLQuery; TableName,
  IdxFld1, IdxFld2, Idx1Descri, Idx2Descri, FldTradu1, Txt1ptBR, Txt1enUS,
  TxtTradu1: String; IdxVal1, IdxVal2: Integer);
var
  SQLType: TSQLType;
begin
  if DBCheck.CriaFm(TFmLoclzL10nEd1, FmLoclzL10nEd1, afmoNegarComAviso) then
  begin
    FmLoclzL10nEd1.FQrIts   := QrIts;
    //
    FmLoclzL10nEd1.FTableName := TableName;
    FmLoclzL10nEd1.FIdxFld1   := IdxFld1;
    FmLoclzL10nEd1.FIdxFld2   := IdxFld2;
    FmLoclzL10nEd1.FFldTradu1 := FldTradu1;
    FmLoclzL10nEd1.FIdxVal1   := IdxVal1;
    FmLoclzL10nEd1.FIdxVal2   := IdxVal2;
    //
    FmLoclzL10nEd1.EdID1.ValueVariant := IdxVal1;
    FmLoclzL10nEd1.EdID2.ValueVariant := IdxVal2;
    FmLoclzL10nEd1.EdID1Descri.Text := Idx1Descri;
    FmLoclzL10nEd1.EdID2Descri.Text := Idx2Descri;
    //
    FmLoclzL10nEd1.EdTxt1ptBR.Text  := Txt1ptBR;
    FmLoclzL10nEd1.EdTxt1enUS.Text  := Txt1enUS;
    FmLoclzL10nEd1.EdTxtTradu1.Text := TxtTradu1;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT COUNT(*) FROM ' + TableName,
    'WHERE ' + IdxFld1 + '=' + Geral.FF0(IdxVal1),
    'AND ' + IdxFld2 + '=' + Geral.FF0(IdxVal2),
    '']);
    //Geral.MB_Teste(Dmod.QrAux.SQL.Text);
    if Dmod.QrAux.Fields[0].AsInteger > 0 then
      FmLoclzL10nEd1.ImgTipo.SQLType := stUpd
    else
      FmLoclzL10nEd1.ImgTipo.SQLType := stIns;
    //
    FmLoclzL10nEd1.ShowModal;
    FmLoclzL10nEd1.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormFmLoclzL10nEd2(QrIts: TmySQLQuery; TableName,  IdxFld1,
IdxFld2, Idx1Descri, Idx2Descri, FldTradu1, FldTradu2, Txt1ptBR, Txt1enUS,
Txt2ptBR, Txt2enUS, TxtTradu1, TxtTradu2: String; IdxVal1, IdxVal2: Integer);
var
  SQLType: TSQLType;
begin
  if DBCheck.CriaFm(TFmLoclzL10nEd2, FmLoclzL10nEd2, afmoNegarComAviso) then
  begin
    FmLoclzL10nEd2.FQrIts   := QrIts;
    //
    FmLoclzL10nEd2.FTableName := TableName;
    FmLoclzL10nEd2.FIdxFld1   := IdxFld1;
    FmLoclzL10nEd2.FIdxFld2   := IdxFld2;
    FmLoclzL10nEd2.FFldTradu1 := FldTradu1;
    FmLoclzL10nEd2.FFldTradu2 := FldTradu2;
    FmLoclzL10nEd2.FIdxVal1   := IdxVal1;
    FmLoclzL10nEd2.FIdxVal2   := IdxVal2;
    //
    FmLoclzL10nEd2.EdID1.ValueVariant := IdxVal1;
    FmLoclzL10nEd2.EdID2.ValueVariant := IdxVal2;
    FmLoclzL10nEd2.EdID1Descri.Text := Idx1Descri;
    FmLoclzL10nEd2.EdID2Descri.Text := Idx2Descri;
    //
    FmLoclzL10nEd2.EdTxt1ptBR.Text  := Txt1ptBR;
    FmLoclzL10nEd2.EdTxt1enUS.Text  := Txt1enUS;
    FmLoclzL10nEd2.EdTxt2ptBR.Text  := Txt2ptBR;
    FmLoclzL10nEd2.EdTxt2enUS.Text  := Txt2enUS;
    FmLoclzL10nEd2.EdTxtTradu1.Text := TxtTradu1;
    FmLoclzL10nEd2.EdTxtTradu2.Text := TxtTradu2;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT COUNT(*) FROM ' + TableName,
    'WHERE ' + IdxFld1 + '=' + Geral.FF0(IdxVal1),
    'AND ' + IdxFld2 + '=' + Geral.FF0(IdxVal2),
    '']);
    //Geral.MB_Teste(Dmod.QrAux.SQL.Text);
    if Dmod.QrAux.Fields[0].AsInteger > 0 then
      FmLoclzL10nEd2.ImgTipo.SQLType := stUpd
    else
      FmLoclzL10nEd2.ImgTipo.SQLType := stIns;
    //
    FmLoclzL10nEd2.ShowModal;
    FmLoclzL10nEd2.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormFmLoclzL10nEd3(QrIts: TmySQLQuery; TableName,
  IdxFld1, IdxFld2, Idx1Descri, Idx2Descri, FldTradu1, Txt1ptBR, Txt1enUS,
  TxtTradu1: String; IdxVal1, IdxVal2: Integer;
  Sigla, DocHeader, ItemText, Reference, DescrCondiPg: String;
  RegrQtdDia, RegrTipSeq, RegrDiaSem: Integer);
var
  SQLType: TSQLType;
begin
  if DBCheck.CriaFm(TFmLoclzL10nEd3, FmLoclzL10nEd3, afmoNegarComAviso) then
  begin
    FmLoclzL10nEd3.FQrIts   := QrIts;
    //
    FmLoclzL10nEd3.FTableName := TableName;
    FmLoclzL10nEd3.FIdxFld1   := IdxFld1;
    FmLoclzL10nEd3.FIdxFld2   := IdxFld2;
    FmLoclzL10nEd3.FFldTradu1 := FldTradu1;
    FmLoclzL10nEd3.FIdxVal1   := IdxVal1;
    FmLoclzL10nEd3.FIdxVal2   := IdxVal2;
    //
    FmLoclzL10nEd3.EdID1.ValueVariant := IdxVal1;
    FmLoclzL10nEd3.EdID2.ValueVariant := IdxVal2;
    FmLoclzL10nEd3.EdID1Descri.Text := Idx1Descri;
    FmLoclzL10nEd3.EdID2Descri.Text := Idx2Descri;
    //
    FmLoclzL10nEd3.EdTxt1ptBR.Text  := Txt1ptBR;
    FmLoclzL10nEd3.EdTxt1enUS.Text  := Txt1enUS;
    FmLoclzL10nEd3.EdTxtTradu1.Text := TxtTradu1;
    //
    FmLoclzL10nEd3.EdSigla.Text        := Sigla;
    FmLoclzL10nEd3.EdDocHeader.Text    := DocHeader;
    FmLoclzL10nEd3.EdItemText.Text     := ItemText;
    FmLoclzL10nEd3.EdReference.Text    := Reference;
    FmLoclzL10nEd3.EdDescrCondiPg.Text := DescrCondiPg;
    FmLoclzL10nEd3.EdRegrQtdDia.ValueVariant := RegrQtdDia;
    FmLoclzL10nEd3.RGRegrTipSeq.ItemIndex := RegrTipSeq;
    FmLoclzL10nEd3.RGRegrDiaSem.ItemIndex := RegrDiaSem;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT COUNT(*) FROM ' + TableName,
    'WHERE ' + IdxFld1 + '=' + Geral.FF0(IdxVal1),
    'AND ' + IdxFld2 + '=' + Geral.FF0(IdxVal2),
    '']);
    //Geral.MB_Teste(Dmod.QrAux.SQL.Text);
    if Dmod.QrAux.Fields[0].AsInteger > 0 then
      FmLoclzL10nEd3.ImgTipo.SQLType := stUpd
    else
      FmLoclzL10nEd3.ImgTipo.SQLType := stIns;
    //
    FmLoclzL10nEd3.ShowModal;
    FmLoclzL10nEd3.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormFmPrmsCompny(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmPrmsCompny, FmPrmsCompny, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmPrmsCompny.LocCod(Codigo, Codigo);
    FmPrmsCompny.ShowModal;
    FmPrmsCompny.Destroy;
  end;
end;

{
procedure TUnExesD_Jan.MostraFormOVcYnsARQCad();
begin
  if DBCheck.CriaFm(TFmOVcYnsARQCad, FmOVcYnsARQCad, afmoNegarComAviso) then
  begin
    FmOVcYnsARQCad.ShowModal;
    FmOVcYnsARQCad.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormOVcYnsChkCad();
begin
  if DBCheck.CriaFm(TFmOVcYnsChkCad, FmOVcYnsChkCad, afmoNegarComAviso) then
  begin
    FmOVcYnsChkCad.ShowModal;
    FmOVcYnsChkCad.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormOVcYnsExgCad(Artigo: Integer);
begin
  if DBCheck.CriaFm(TFmOVcYnsExgCad, FmOVcYnsExgCad, afmoNegarComAviso) then
  begin
    FmOVcYnsExgCad.FArtigo := Artigo;
    FmOVcYnsExgCad.EdArtigRef.ValueVariant := Artigo;
    FmOVcYnsExgCad.CBArtigRef.KeyValue     := Artigo;
    FmOVcYnsExgCad.ShowModal;
    FmOVcYnsExgCad.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormOVcYnsMixTop();
begin
  if DBCheck.CriaFm(TFmOVcYnsMixTop, FmOVcYnsMixTop, afmoNegarComAviso) then
  begin
    FmOVcYnsMixTop.ShowModal;
    FmOVcYnsMixTop.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormOVcYnsGraTop();
begin
  if DBCheck.CriaFm(TFmOVcYnsGraTop, FmOVcYnsGraTop, afmoNegarComAviso) then
  begin
    FmOVcYnsGraTop.ShowModal;
    FmOVcYnsGraTop.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormOVcYnsMedCad(Artigo: Integer);
begin
  if DBCheck.CriaFm(TFmOVcYnsMedCad, FmOVcYnsMedCad, afmoNegarComAviso) then
  begin
    FmOVcYnsMedCad.FArtigo := Artigo;
    FmOVcYnsMedCad.EdArtigRef.ValueVariant := Artigo;
    FmOVcYnsMedCad.CBArtigRef.KeyValue     := Artigo;
    FmOVcYnsMedCad.ShowModal;
    FmOVcYnsMedCad.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormOVcYnsQstCtx();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'OVcYnsQstCtx', 60, UnDmkEnums.ncGerlSeq1,
  'Contextos de Inconformidades',
  [], False, Null, [], [], False);
end;

procedure TUnExesD_Jan.MostraFormOVcYnsQstMag();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'OVcYnsQstMag', 60, UnDmkEnums.ncGerlSeq1,
  'Magnitudes de Inconformidade',
  [], True, Null(*Maximo 0*), [], [], False);
end;

procedure TUnExesD_Jan.MostraFormOVcYnsQstTop();
begin
  if DBCheck.CriaFm(TFmOVcYnsQstTop, FmOVcYnsQstTop, afmoNegarComAviso) then
  begin
    FmOVcYnsQstTop.ShowModal;
    FmOVcYnsQstTop.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormOVdCiclo();
begin
  if DBCheck.CriaFm(TFmOVdCiclo, FmOVdCiclo, afmoNegarComAviso) then
  begin
    FmOVdCiclo.ShowModal;
    FmOVdCiclo.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormOVdLocal;
begin
  if DBCheck.CriaFm(TFmOVdLocal, FmOVdLocal, afmoNegarComAviso) then
  begin
    FmOVdLocal.ShowModal;
    FmOVdLocal.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormOVdLote();
begin
(*
  if DBCheck.CriaFm(TFmOVdLote, FmOVdLote, afmoNegarComAviso) then
  begin
    FmOVdLote.ShowModal;
    FmOVdLote.Destroy;
  end;
*)
end;

procedure TUnExesD_Jan.MostraFormOVdProduto();
begin
  if DBCheck.CriaFm(TFmOVdProduto, FmOVdProduto, afmoNegarComAviso) then
  begin
    FmOVdProduto.ShowModal;
    FmOVdProduto.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormOVdReferencia();
begin
  if DBCheck.CriaFm(TFmOVdReferencia, FmOVdReferencia, afmoNegarComAviso) then
  begin
    FmOVdReferencia.ShowModal;
    FmOVdReferencia.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormOVfOPGerFil_Fac(MostraSeEmpty: Boolean);
begin
  if DBCheck.CriaFm(TFmOVfOPGerFil_Fac, FmOVfOPGerFil_Fac, afmoNegarComAviso) then
  begin
    FmOVfOPGerFil_Fac.ReopenNovasReduzOPs();
    //if (FmOVfOPGerFil_Fac.QrNovasReduzOPs.RecordCount > 0) or
    if (FmOVfOPGerFil_Fac.QrOVgIspLasSta.RecordCount > 0) or
    (MostraSeEmpty = True) then
      FmOVfOPGerFil_Fac.ShowModal;
    FmOVfOPGerFil_Fac.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormOVfOPGerFil_Tex(MostraSeEmpty: Boolean);
begin
  if DBCheck.CriaFm(TFmOVfOPGerFil_Tex, FmOVfOPGerFil_Tex, afmoNegarComAviso) then
  begin
    FmOVfOPGerFil_Tex.ReopenNovasReduzOPs();
    //if (FmOVfOPGerFil_Tex.QrNovasReduzOPs.RecordCount > 0) or
    if (FmOVfOPGerFil_Tex.QrOVgIspLasSta.RecordCount > 0) or
    (MostraSeEmpty = True) then
      FmOVfOPGerFil_Tex.ShowModal;
    FmOVfOPGerFil_Tex.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormOVgIspGerCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmOVgIspGerCab, FmOVgIspGerCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmOVgIspGerCab.LocCod(Codigo, Codigo);
    FmOVgIspGerCab.ShowModal;
    FmOVgIspGerCab.Destroy;
  end;
end;

function TUnExesD_Jan.MostraFormOVgIspGerCad(SQLType: TSQLType; Codigo, Local:
  Integer; NO_Local: String; NrOP, SeqGrupo: Integer; NO_SeqGrupo: String;
  NrReduzidoOP, OVcYnsMed, OVcYnsChk, OVcYnsARQ, LimiteMed, LimiteChk,
  PermiFinHow, ZtatusIsp: Integer): Integer;
begin
  Result := 0;
  if DBCheck.CriaFm(TFmOVgIspGerCad, FmOVgIspGerCad, afmoNegarComAviso) then
  begin
    FmOVgIspGerCad.ImgTipo.SQLType             := SQLType;
    FmOVgIspGerCad.FSeqGrupo                   := SeqGrupo;
    FmOVgIspGerCad.EdLocal.ValueVariant        := Local;
    FmOVgIspGerCad.EdNO_Local.ValueVariant     := NO_Local;
    FmOVgIspGerCad.EdNrOP.ValueVariant         := NrOP;
    FmOVgIspGerCad.EdSeqGrupo.ValueVariant     := SeqGrupo;
    FmOVgIspGerCad.EdNO_SeqGrupo.ValueVariant  := NO_SeqGrupo;
    FmOVgIspGerCad.EdNrReduzidoOP.ValueVariant := NrReduzidoOP;
    FmOVgIspGerCad.ReopenOVcYnsMedCad(SeqGrupo);
    if SQLTYpe = stUpd then
    begin
      FmOVgIspGerCad.EdCodigo.ValueVariant := Codigo;
      FmOVgIspGerCad.EdOVcYnsMed.ValueVariant := OVcYnsMed;
      FmOVgIspGerCad.CBOVcYnsMed.KeyValue     := OVcYnsMed;
      FmOVgIspGerCad.EdOVcYnsChk.ValueVariant := OVcYnsChk;
      FmOVgIspGerCad.CBOVcYnsChk.KeyValue     := OVcYnsChk;
      FmOVgIspGerCad.EdOVcYnsARQ.ValueVariant := OVcYnsARQ;
      FmOVgIspGerCad.CBOVcYnsARQ.KeyValue     := OVcYnsARQ;
      FmOVgIspGerCad.EdLimiteMed.ValueVariant := LimiteMed;
      FmOVgIspGerCad.EdLimiteChk.ValueVariant := LimiteChk;
      FmOVgIspGerCad.RGPermiFinHow.ItemIndex  := PermiFinHow;
    end;
    FmOVgIspGerCad.FZtatusIsp := ZtatusIsp; //CO_O V S_IMPORT_ALHEIO_3972_CONFIGURADO
    //
    FmOVgIspGerCad.ShowModal;
    Result := FmOVgIspGerCad.FCodigo;
    FmOVgIspGerCad.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormOVgIspPrfCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmOVgIspPrfCab, FmOVgIspPrfCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmOVgIspPrfCab.LocCod(Codigo, Codigo);
    FmOVgIspPrfCab.ShowModal;
    FmOVgIspPrfCab.Destroy;
  end;
end;

function TUnExesD_Jan.MostraFormOVgIspPrfCad(SQLType: TSQLType; Codigo,
  SeqGrupo: Integer; NO_SeqGrupo, Referencia: String; OVcYnsMed, OVcYnsChk,
  OVcYnsARQ, LimiteMed, LimiteChk, PermiFinHow, Ativo: Integer; Nome: String):
  Integer;
begin
  Result := 0;
  if DBCheck.CriaFm(TFmOVgIspPrfCad, FmOVgIspPrfCad, afmoNegarComAviso) then
  begin
    FmOVgIspPrfCad.ImgTipo.SQLType             := SQLType;
    FmOVgIspPrfCad.FSeqGrupo                   := SeqGrupo;
    FmOVgIspPrfCad.EdSeqGrupo.ValueVariant     := SeqGrupo;
    FmOVgIspPrfCad.EdNO_SeqGrupo.ValueVariant  := NO_SeqGrupo;
    FmOVgIspPrfCad.EdReferencia.ValueVariant   := Referencia;
    FmOVgIspPrfCad.ReopenOVcYnsMedCad(SeqGrupo);
    if SQLTYpe = stUpd then
    begin
      FmOVgIspPrfCad.EdCodigo.ValueVariant := Codigo;
      FmOVgIspPrfCad.EdOVcYnsMed.ValueVariant := OVcYnsMed;
      FmOVgIspPrfCad.CBOVcYnsMed.KeyValue     := OVcYnsMed;
      FmOVgIspPrfCad.EdOVcYnsChk.ValueVariant := OVcYnsChk;
      FmOVgIspPrfCad.CBOVcYnsChk.KeyValue     := OVcYnsChk;
      FmOVgIspPrfCad.EdOVcYnsARQ.ValueVariant := OVcYnsARQ;
      FmOVgIspPrfCad.CBOVcYnsARQ.KeyValue     := OVcYnsARQ;
      FmOVgIspPrfCad.EdLimiteMed.ValueVariant := LimiteMed;
      FmOVgIspPrfCad.EdLimiteChk.ValueVariant := LimiteChk;
      FmOVgIspPrfCad.RGPermiFinHow.ItemIndex  := PermiFinHow;
      FmOVgIspPrfCad.CkAtivo.Checked          := Geral.IntToBool(Ativo);
      FmOVgIspPrfCad.EdNome.ValueVariant      := Nome;
    end;
    //
    FmOVgIspPrfCad.ShowModal;
    Result := FmOVgIspPrfCad.FCodigo;
    FmOVgIspPrfCad.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormOVgItxGerCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmOVgItxGerCab, FmOVgItxGerCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmOVgItxGerCab.LocCod(Codigo, Codigo);
    FmOVgItxGerCab.ShowModal;
    FmOVgItxGerCab.Destroy;
  end;
end;

function TUnExesD_Jan.MostraFormOVgItxGerCad(SQLType: TSQLType; Codigo,
  Local: Integer; NO_Local: String; NrOP, SeqGrupo: Integer;
  NO_SeqGrupo: String; NrReduzidoOP, OVcYnsExg, PermiFinHow, ZtatusIsp:
  Integer): Integer;
begin
  Result := 0;
  if DBCheck.CriaFm(TFmOVgItxGerCad, FmOVgItxGerCad, afmoNegarComAviso) then
  begin
    FmOVgItxGerCad.ImgTipo.SQLType             := SQLType;
    FmOVgItxGerCad.FSeqGrupo                   := SeqGrupo;
    FmOVgItxGerCad.EdLocal.ValueVariant        := Local;
    FmOVgItxGerCad.EdNO_Local.ValueVariant     := NO_Local;
    FmOVgItxGerCad.EdNrOP.ValueVariant         := NrOP;
    FmOVgItxGerCad.EdSeqGrupo.ValueVariant     := SeqGrupo;
    FmOVgItxGerCad.EdNO_SeqGrupo.ValueVariant  := NO_SeqGrupo;
    FmOVgItxGerCad.EdNrReduzidoOP.ValueVariant := NrReduzidoOP;
    FmOVgItxGerCad.ReopenOVcYnsExgCad(SeqGrupo, True);
    if SQLTYpe = stUpd then
    begin
      FmOVgItxGerCad.EdCodigo.ValueVariant := Codigo;
      FmOVgItxGerCad.EdOVcYnsExg.ValueVariant := OVcYnsExg;
      FmOVgItxGerCad.CBOVcYnsExg.KeyValue     := OVcYnsExg;
(*
      FmOVgItxGerCad.EdOVcYnsChk.ValueVariant := OVcYnsChk;
      FmOVgItxGerCad.CBOVcYnsChk.KeyValue     := OVcYnsChk;
      FmOVgItxGerCad.EdOVcYnsARQ.ValueVariant := OVcYnsARQ;
      FmOVgItxGerCad.CBOVcYnsARQ.KeyValue     := OVcYnsARQ;
      FmOVgItxGerCad.EdLimiteMed.ValueVariant := LimiteMed;
      FmOVgItxGerCad.EdLimiteChk.ValueVariant := LimiteChk;
*)
      FmOVgItxGerCad.RGPermiFinHow.ItemIndex  := PermiFinHow;
    end;
    FmOVgItxGerCad.FZtatusIsp := ZtatusIsp; //CO_O V S_IMPORT_ALHEIO_3972_CONFIGURADO
    //
    FmOVgItxGerCad.ShowModal;
    Result := FmOVgItxGerCad.FCodigo;
    FmOVgItxGerCad.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormOVgItxPrfCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmOVgItxPrfCab, FmOVgItxPrfCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmOVgItxPrfCab.LocCod(Codigo, Codigo);
    FmOVgItxPrfCab.ShowModal;
    FmOVgItxPrfCab.Destroy;
  end;
end;

function TUnExesD_Jan.MostraFormOVgItxPrfCad(SQLType: TSQLType; Codigo,
  SeqGrupo: Integer; NO_SeqGrupo, Referencia: String; OVcYnsExg, PermiFinHow,
  Ativo: Integer; Nome: String): Integer;
begin
  Result := 0;
  if DBCheck.CriaFm(TFmOVgItxPrfCad, FmOVgItxPrfCad, afmoNegarComAviso) then
  begin
    FmOVgItxPrfCad.ImgTipo.SQLType             := SQLType;
    FmOVgItxPrfCad.FSeqGrupo                   := SeqGrupo;
    FmOVgItxPrfCad.EdSeqGrupo.ValueVariant     := SeqGrupo;
    FmOVgItxPrfCad.EdNO_SeqGrupo.ValueVariant  := NO_SeqGrupo;
    FmOVgItxPrfCad.EdReferencia.ValueVariant   := Referencia;
    FmOVgItxPrfCad.ReopenOVcYnsExgCad(SeqGrupo, True);
    if SQLTYpe = stUpd then
    begin
      FmOVgItxPrfCad.EdCodigo.ValueVariant := Codigo;
      FmOVgItxPrfCad.EdOVcYnsExg.ValueVariant := OVcYnsExg;
      FmOVgItxPrfCad.CBOVcYnsExg.KeyValue     := OVcYnsExg;
(*
      FmOVgItxPrfCad.EdOVcYnsChk.ValueVariant := OVcYnsChk;
      FmOVgItxPrfCad.CBOVcYnsChk.KeyValue     := OVcYnsChk;
      FmOVgItxPrfCad.EdOVcYnsARQ.ValueVariant := OVcYnsARQ;
      FmOVgItxPrfCad.CBOVcYnsARQ.KeyValue     := OVcYnsARQ;
      FmOVgItxPrfCad.EdLimiteMed.ValueVariant := LimiteMed;
      FmOVgItxPrfCad.EdLimiteChk.ValueVariant := LimiteChk;
*)
      FmOVgItxPrfCad.RGPermiFinHow.ItemIndex  := PermiFinHow;
      FmOVgItxPrfCad.CkAtivo.Checked          := Geral.IntToBool(Ativo);
      FmOVgItxPrfCad.EdNome.ValueVariant      := Nome;
    end;
    //
    FmOVgItxPrfCad.ShowModal;
    Result := FmOVgItxPrfCad.FCodigo;
    FmOVgItxPrfCad.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormOVmIspDevCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmOVmIspDevCab, FmOVmIspDevCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmOVmIspDevCab.LocCod(Codigo, Codigo);
    FmOVmIspDevCab.ShowModal;
    FmOVmIspDevCab.Destroy;
  end;
end;

procedure TUnExesD_Jan.MostraFormOVmItxDevCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmOVmItxDevCab, FmOVmItxDevCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmOVmItxDevCab.LocCod(Codigo, Codigo);
    FmOVmItxDevCab.ShowModal;
    FmOVmItxDevCab.Destroy;
  end;
end;
}

(*&�%$#@!"
procedure TUnExesD_Jan.MostraFormOVpLayEsq(Esquema: Integer);
begin
  if DBCheck.CriaFm(TFmOVpLayEsq, FmOVpLayEsq, afmoNegarComAviso) then
  begin
    if Esquema <> 0 then
      FmOVpLayEsq.LocCod(Esquema, Esquema);
    FmOVpLayEsq.ShowModal;
    FmOVpLayEsq.Destroy;
  end;
end;
*)

{
procedure TUnExesD_Jan.MostraFormSeccTexTinturaria(Forca: Boolean);
begin
  if DBCheck.CriaFm(TFmSeccTexTinturaria, FmSeccTexTinturaria, afmoNegarComAviso) then
  begin
    FmSeccTexTinturaria.ReopenItensAPosConfig();
    FmSeccTexTinturaria.DefineCordaInicial();
    if (FmSeccTexTinturaria.QrAptos.RecordCount > 0) or Forca then
      FmSeccTexTinturaria.ShowModal;
    FmSeccTexTinturaria.Destroy;
  end;
end;
}

end.
