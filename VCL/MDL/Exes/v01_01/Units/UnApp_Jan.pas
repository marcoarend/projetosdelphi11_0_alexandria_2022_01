unit UnApp_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, Menus, UnDmkEnums, mySQLDbTables, Forms,
  Grids, DBGrids, UnInternalConsts, dmkGeral, Controls, dmkPageControl,
  UnProjGroup_Vars, UnExes_ProjGroupVars;

type
  TUnApp_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
(*
    procedure MostraFormComandaCab(Codigo: Integer);
    procedure MostraFormComandaPan(AbrirEmAba: Boolean; InOwner: TWincontrol;
              AdvToolBarPager: TdmkPageControl; Codigo: Integer);
*)
    procedure MostraFormOpcoesApp();
    procedure MostraFormOpcoesGrl();
    procedure MostraFormExCambios(EXcIdFunc: Integer);
    procedure MostraFormExDespesaMdo();
    //procedure MostraFormExDespesaMdo(EXcIdFunc: Integer);
    procedure MostraFormEXpImpRel();
    procedure MostraFormExportaSAP1();
  end;

var
  App_Jan: TUnApp_Jan;


implementation

uses
  MyDBCheck, Module, OpcoesApp, Opcoes, ModuleGeral, UnMyObjects,
  ExDespesaMdo, ExCambios, EXpImpRel, ExportaSAP1;

{ TUnApp_Jan }

(*
procedure TUnApp_Jan.MostraFormComandaPan(AbrirEmAba: Boolean; InOwner: TWincontrol;
  AdvToolBarPager: TdmkPageControl; Codigo: Integer);
begin
  if AbrirEmAba then
  begin
    AdvToolBarPager.Visible := False;
    if FmComandaPan = nil then
    begin
      VAR_FmComandaPan := MyObjects.FormTDICria(TFmComandaPan, InOwner, AdvToolBarPager, True, True);
      //TFmComandaPan(VAR_FmComandaPan).QrComanda?.Locate('Codigo', Codigo, []);
    end;
  end else
  begin
    if DBCheck.CriaFm(TFmComandaPan, FmComandaPan, afmoNegarComAviso) then
    begin
      FmComandaPan.ShowModal;
      FmComandaPan.Destroy;
      //
      FmComandaPan := nil;
    end;
  end;
end;

procedure TUnApp_Jan.MostraFormComandaCab(Codigo: Integer);
begin
  if DModG.QrParamsEmp.State = dsInactive then
    DModG.ReopenParamsEmp(Geral.IMV(VAR_LIB_EMPRESAS));
  if DBCheck.CriaFm(TFmComandaCab, FmComandaCab, afmoSoBoss) then
  begin
    if Codigo <> 0 then
      FmComandaCab.LocCod(Codigo, Codigo);
    FmComandaCab.ShowModal;
    FmComandaCab.Destroy;
  end;
end;
*)
procedure TUnApp_Jan.MostraFormExCambios(EXcIdFunc: Integer);
begin
  if VAR_DIR_FOTOS_DESPESAS = EmptyStr then
    Geral.MB_Aviso(
    'Defina o "Diret�rio dos arquivos de imagens de documentos (NFCes, RPS, recibos e outros)" nas op��es do aplicativo');
  if DBCheck.CriaFm(TFmExCambios, FmExCambios, afmoSoBoss) then
  begin
    FmExCambios.FEXcIdFunc := EXcIdFunc;
    FmExCambios.ReopenPeriodos();
    FmExCambios.ShowModal;
    FmExCambios.Destroy;
  end;
end;

//procedure TUnApp_Jan.MostraFormExDespesaMdo(EXcIdFunc: Integer);
procedure TUnApp_Jan.MostraFormExDespesaMdo();
begin
  if VAR_DIR_FOTOS_DESPESAS = EmptyStr then
    Geral.MB_Aviso(
    'Defina o "Diret�rio dos arquivos de imagens de documentos (NFCes, RPS, recibos e outros)" nas op��es do aplicativo');
  if DBCheck.CriaFm(TFmExDespesaMdo, FmExDespesaMdo, afmoSoBoss) then
  begin
    //FmExDespesaMdo.FEXcIdFunc := EXcIdFunc;
    //FmExDespesaMdo.ReopenPeriodos();
    //FmExDespesaMdo.FEXcIdFunc := 0;
    FmExDespesaMdo.ShowModal;
    FmExDespesaMdo.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormEXpImpRel;
begin
  if DBCheck.CriaFm(TFmEXpImpRel, FmEXpImpRel, afmoSoBoss) then
  begin
    FmEXpImpRel.ShowModal;
    FmEXpImpRel.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormExportaSAP1;
begin
  if DBCheck.CriaFm(TFmExportaSAP1, FmExportaSAP1, afmoSoBoss) then
  begin
    FmExportaSAP1.ShowModal;
    FmExportaSAP1.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormOpcoesApp();
begin
  if DBCheck.CriaFm(TFmOpcoesApp, FmOpcoesApp, afmoSoBoss) then
  begin
    FmOpcoesApp.ShowModal;
    FmOpcoesApp.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormOpcoesGrl;
begin
  if DBCheck.CriaFm(TFmOpcoes, FmOpcoes, afmoSoBoss) then
  begin
    FmOpcoes.ShowModal;
    FmOpcoes.Destroy;
  end;
end;

end.
