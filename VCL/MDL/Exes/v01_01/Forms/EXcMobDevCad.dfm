object FmEXcMobDevCad: TFmEXcMobDevCad
  Left = 368
  Top = 194
  Caption = 'EXS-DEVIC-001 :: Cadastro de Equipamentos Mobile'
  ClientHeight = 578
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 482
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 193
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 16
        Top = 56
        Width = 25
        Height = 13
        Caption = 'IMEI:'
        Color = clBtnFace
        Enabled = False
        ParentColor = False
      end
      object Label5: TLabel
        Left = 200
        Top = 56
        Width = 98
        Height = 13
        Caption = 'Nome do dispositivo:'
        Color = clBtnFace
        Enabled = False
        ParentColor = False
      end
      object Label6: TLabel
        Left = 860
        Top = 56
        Width = 131
        Height = 13
        Caption = 'Resolu'#231#227'o Largura x Altura:'
        Color = clBtnFace
        Enabled = False
        ParentColor = False
      end
      object Label8: TLabel
        Left = 232
        Top = 96
        Width = 122
        Height = 13
        Caption = 'Sistema operacional (OS):'
        Color = clBtnFace
        Enabled = False
        ParentColor = False
      end
      object Label10: TLabel
        Left = 364
        Top = 96
        Width = 71
        Height = 13
        Caption = 'Apelido do OS:'
        Color = clBtnFace
        Enabled = False
        ParentColor = False
      end
      object Label11: TLabel
        Left = 680
        Top = 96
        Width = 69
        Height = 13
        Caption = 'Vers'#227'o do OS:'
        Color = clBtnFace
        Enabled = False
        ParentColor = False
      end
      object Label13: TLabel
        Left = 700
        Top = 76
        Width = 5
        Height = 13
        Caption = 'x'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 16
        Top = 96
        Width = 209
        Height = 13
        Caption = 'Nome do respons'#225'vel padr'#227'o do dispositivo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label12: TLabel
        Left = 613
        Top = 16
        Width = 172
        Height = 13
        Caption = 'Data e hora do in'#237'cio da habilita'#231#227'o:'
        Enabled = False
      end
      object Label25: TLabel
        Left = 793
        Top = 16
        Width = 159
        Height = 13
        Caption = 'Data e hora do fim da habilita'#231#227'o:'
        Enabled = False
      end
      object Label26: TLabel
        Left = 968
        Top = 16
        Width = 27
        Height = 13
        Caption = 'Ativo:'
        Enabled = False
        FocusControl = DBEdit11
      end
      object Label27: TLabel
        Left = 16
        Top = 136
        Width = 105
        Height = 13
        Caption = 'Perfil de compet'#234'ncia:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label30: TLabel
        Left = 588
        Top = 136
        Width = 63
        Height = 13
        Caption = 'ID Funcional:'
      end
      object Label31: TLabel
        Left = 684
        Top = 136
        Width = 79
        Height = 13
        Caption = 'Centro de Custo:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 533
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdDeviceID: TdmkEdit
        Left = 16
        Top = 72
        Width = 180
        Height = 21
        Enabled = False
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'DeviceID'
        UpdCampo = 'DeviceID'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdDeviceName: TdmkEdit
        Left = 200
        Top = 72
        Width = 657
        Height = 21
        Enabled = False
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'DeviceName'
        UpdCampo = 'DeviceName'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdDvcScreenW: TdmkEdit
        Left = 860
        Top = 72
        Width = 60
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'DvcScreenW'
        UpdCampo = 'DvcScreenW'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdDvcScreenH: TdmkEdit
        Left = 932
        Top = 72
        Width = 60
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'DvcScreenH'
        UpdCampo = 'DvcScreenH'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdOSName: TdmkEdit
        Left = 232
        Top = 112
        Width = 129
        Height = 21
        Enabled = False
        TabOrder = 11
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'OSName'
        UpdCampo = 'OSName'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdOSNickName: TdmkEdit
        Left = 364
        Top = 112
        Width = 313
        Height = 21
        Enabled = False
        TabOrder = 12
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'OSNickName'
        UpdCampo = 'OSNickName'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdOSVersion: TdmkEdit
        Left = 680
        Top = 112
        Width = 313
        Height = 21
        Enabled = False
        TabOrder = 13
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'OSVersion'
        UpdCampo = 'OSVersion'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object TPDtaHabIni: TdmkEditDateTimePicker
        Left = 612
        Top = 32
        Width = 112
        Height = 21
        Date = 40724.768997777780000000
        Time = 40724.768997777780000000
        Enabled = False
        TabOrder = 2
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtaHabIni'
        UpdCampo = 'DtaHabIni'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtaHabIni: TdmkEdit
        Left = 728
        Top = 32
        Width = 57
        Height = 21
        Enabled = False
        TabOrder = 3
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfLong
        Texto = '00:00:00'
        QryCampo = 'DtaHabIni'
        UpdCampo = 'DtaHabIni'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object TPDtaHabFim: TdmkEditDateTimePicker
        Left = 792
        Top = 32
        Width = 112
        Height = 21
        Date = 40724.768997777780000000
        Time = 40724.768997777780000000
        Enabled = False
        TabOrder = 4
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtaHabFim'
        UpdCampo = 'DtaHabFim'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object EdDtaHabFim: TdmkEdit
        Left = 908
        Top = 32
        Width = 57
        Height = 21
        Enabled = False
        TabOrder = 5
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfLong
        Texto = '00:00:00'
        QryCampo = 'DtaHabFim'
        UpdCampo = 'DtaHabFim'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdUserNmePdr: TdmkEdit
        Left = 16
        Top = 112
        Width = 213
        Height = 21
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'UserNmePdr'
        UpdCampo = 'UserNmePdr'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object GroupBox4: TGroupBox
        Left = 784
        Top = 140
        Width = 208
        Height = 45
        Caption = '  A'#231#245'es habilitadas: '
        TabOrder = 14
        object Panel9: TPanel
          Left = 2
          Top = 15
          Width = 204
          Height = 28
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object CkChmOcorrencias: TdmkCheckBox
            Left = 8
            Top = 4
            Width = 161
            Height = 17
            Caption = 'Chamados de ocorr'#234'ncias'
            TabOrder = 0
            QryCampo = 'ChmOcorrencias'
            UpdCampo = 'ChmOcorrencias'
            UpdType = utYes
            ValCheck = '1'
            ValUncheck = '0'
            OldValor = #0
          end
        end
      end
      object EdComptncPrf: TdmkEditCB
        Left = 16
        Top = 152
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 15
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ComptncPrf'
        UpdCampo = 'ComptncPrf'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBComptncPrf
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBComptncPrf: TdmkDBLookupComboBox
        Left = 72
        Top = 152
        Width = 509
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsComptncPrf
        TabOrder = 16
        dmkEditCB = EdComptncPrf
        QryCampo = 'ComptncPrf'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdEXcIdFunc: TdmkEdit
        Left = 588
        Top = 152
        Width = 93
        Height = 21
        Alignment = taRightJustify
        TabOrder = 17
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'EXcIdFunc'
        UpdCampo = 'EXcIdFunc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdIDCntrCst: TdmkEdit
        Left = 684
        Top = 152
        Width = 93
        Height = 21
        Alignment = taRightJustify
        TabOrder = 18
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'IDCntrCst'
        UpdCampo = 'IDCntrCst'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 419
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object EdNO_Allowed: TdmkEdit
      Left = 968
      Top = 32
      Width = 26
      Height = 21
      Enabled = False
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'NO_Allowed'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 482
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 517
      Top = 189
      Height = 229
      ExplicitLeft = 760
      ExplicitTop = 224
      ExplicitHeight = 100
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 189
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label14: TLabel
        Left = 609
        Top = 16
        Width = 172
        Height = 13
        Caption = 'Data e hora do in'#237'cio da habilita'#231#227'o:'
      end
      object Label15: TLabel
        Left = 788
        Top = 16
        Width = 159
        Height = 13
        Caption = 'Data e hora do fim da habilita'#231#227'o:'
      end
      object Label16: TLabel
        Left = 16
        Top = 56
        Width = 25
        Height = 13
        Caption = 'IMEI:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label17: TLabel
        Left = 200
        Top = 56
        Width = 98
        Height = 13
        Caption = 'Nome do dispositivo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label18: TLabel
        Left = 16
        Top = 96
        Width = 209
        Height = 13
        Caption = 'Nome do respons'#225'vel padr'#227'o do dispositivo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label19: TLabel
        Left = 232
        Top = 96
        Width = 122
        Height = 13
        Caption = 'Sistema operacional (OS):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label20: TLabel
        Left = 364
        Top = 96
        Width = 71
        Height = 13
        Caption = 'Apelido do OS:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label21: TLabel
        Left = 680
        Top = 96
        Width = 69
        Height = 13
        Caption = 'Vers'#227'o do OS:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label22: TLabel
        Left = 860
        Top = 56
        Width = 131
        Height = 13
        Caption = 'Resolu'#231#227'o Largura x Altura:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label23: TLabel
        Left = 928
        Top = 80
        Width = 5
        Height = 13
        Caption = 'x'
      end
      object Label24: TLabel
        Left = 960
        Top = 16
        Width = 27
        Height = 13
        Caption = 'Ativo:'
        FocusControl = DBEdit11
      end
      object Label28: TLabel
        Left = 16
        Top = 136
        Width = 105
        Height = 13
        Caption = 'Perfil de compet'#234'ncia:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label29: TLabel
        Left = 472
        Top = 136
        Width = 63
        Height = 13
        Caption = 'ID Funcional:'
        FocusControl = DBEdit14
      end
      object Label32: TLabel
        Left = 567
        Top = 136
        Width = 79
        Height = 13
        Caption = 'Centro de Custo:'
      end
      object Label33: TLabel
        Left = 663
        Top = 136
        Width = 21
        Height = 13
        Caption = 'PIN:'
      end
      object SbPIN: TSpeedButton
        Left = 760
        Top = 152
        Width = 25
        Height = 22
        Caption = '...'
        OnClick = SbPINClick
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsEXcMobDevCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 529
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsEXcMobDevCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 16
        Top = 72
        Width = 181
        Height = 21
        DataField = 'DeviceID'
        DataSource = DsEXcMobDevCad
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 16
        Top = 112
        Width = 213
        Height = 21
        DataField = 'UserNmePdr'
        DataSource = DsEXcMobDevCad
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 200
        Top = 72
        Width = 657
        Height = 21
        DataField = 'DeviceName'
        DataSource = DsEXcMobDevCad
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 860
        Top = 72
        Width = 60
        Height = 21
        DataField = 'DvcScreenH'
        DataSource = DsEXcMobDevCad
        TabOrder = 5
      end
      object DBEdit5: TDBEdit
        Left = 932
        Top = 72
        Width = 60
        Height = 21
        DataField = 'DvcScreenW'
        DataSource = DsEXcMobDevCad
        TabOrder = 6
      end
      object DBEdit6: TDBEdit
        Left = 232
        Top = 112
        Width = 129
        Height = 21
        DataField = 'OSName'
        DataSource = DsEXcMobDevCad
        TabOrder = 7
      end
      object DBEdit7: TDBEdit
        Left = 364
        Top = 112
        Width = 313
        Height = 21
        DataField = 'OSNickName'
        DataSource = DsEXcMobDevCad
        TabOrder = 8
      end
      object DBEdit8: TDBEdit
        Left = 680
        Top = 112
        Width = 313
        Height = 21
        DataField = 'OSVersion'
        DataSource = DsEXcMobDevCad
        TabOrder = 9
      end
      object DBEdit9: TDBEdit
        Left = 609
        Top = 32
        Width = 177
        Height = 21
        DataField = 'DtaHabIni'
        DataSource = DsEXcMobDevCad
        TabOrder = 10
      end
      object DBEdit10: TDBEdit
        Left = 788
        Top = 32
        Width = 168
        Height = 21
        DataField = 'DtaHabFim'
        DataSource = DsEXcMobDevCad
        TabOrder = 11
      end
      object DBEdit11: TDBEdit
        Left = 960
        Top = 32
        Width = 33
        Height = 21
        DataField = 'Allowed'
        DataSource = DsEXcMobDevCad
        TabOrder = 12
      end
      object GroupBox3: TGroupBox
        Left = 788
        Top = 140
        Width = 204
        Height = 45
        Caption = '  Outras habilita'#231#245'es: '
        TabOrder = 13
        object Panel8: TPanel
          Left = 2
          Top = 15
          Width = 200
          Height = 28
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object DBCheckBox4: TDBCheckBox
            Left = 8
            Top = 4
            Width = 161
            Height = 17
            Caption = 'Chamados de ocorr'#234'ncias'
            DataField = 'ChmOcorrencias'
            DataSource = DsEXcMobDevCad
            TabOrder = 0
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
        end
      end
      object DBEdit12: TDBEdit
        Left = 16
        Top = 152
        Width = 57
        Height = 21
        DataField = 'ComptncPrf'
        DataSource = DsEXcMobDevCad
        TabOrder = 14
      end
      object DBEdit13: TDBEdit
        Left = 76
        Top = 152
        Width = 393
        Height = 21
        DataField = 'NO_ComptncPrf'
        DataSource = DsEXcMobDevCad
        TabOrder = 15
      end
      object DBEdit14: TDBEdit
        Left = 472
        Top = 152
        Width = 92
        Height = 21
        DataField = 'EXcIdFunc'
        DataSource = DsEXcMobDevCad
        TabOrder = 16
      end
      object DBEdit15: TDBEdit
        Left = 568
        Top = 152
        Width = 92
        Height = 21
        DataField = 'IDCntrCst'
        DataSource = DsEXcMobDevCad
        TabOrder = 17
      end
      object DBEdit16: TDBEdit
        Left = 664
        Top = 152
        Width = 92
        Height = 21
        DataField = 'PIN_SHA2'
        DataSource = DsEXcMobDevCad
        TabOrder = 18
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 418
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 311
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 1000230
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Dispositivo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&???'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
          OnClick = BtItsClick
        end
      end
    end
    object GBAlw: TGroupBox
      Left = 0
      Top = 189
      Width = 517
      Height = 229
      Align = alLeft
      Caption = ' Permiss'#245'es e  revoga'#231#245'es: '
      TabOrder = 2
      object DGDados: TDBGrid
        Left = 2
        Top = 15
        Width = 513
        Height = 212
        Align = alClient
        DataSource = DsEXcMobDevAlw
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DtaInicio'
            Title.Caption = 'Data in'#237'cio'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AlwReason'
            Title.Caption = 'Motivo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Width = 234
            Visible = True
          end>
      end
    end
    object GBAcs: TGroupBox
      Left = 548
      Top = 189
      Width = 460
      Height = 229
      Align = alRight
      Caption = ' Acessos ao servidor: '
      TabOrder = 3
      object DBGrid1: TDBGrid
        Left = 2
        Top = 15
        Width = 456
        Height = 212
        Align = alClient
        DataSource = DsEXcMobDevAcs
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AcsReason'
            Title.Caption = 'Motivo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DtaAcsIni'
            Title.Caption = 'Data ini'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DtaAcsFim'
            Title.Caption = 'Data fim'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 418
        Height = 32
        Caption = 'Cadastro de Equipamentos Mobile'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 418
        Height = 32
        Caption = 'Cadastro de Equipamentos Mobile'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 418
        Height = 32
        Caption = 'Cadastro de Equipamentos Mobile'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrEXcMobDevCad: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeOpen = QrEXcMobDevCadBeforeOpen
    AfterOpen = QrEXcMobDevCadAfterOpen
    BeforeClose = QrEXcMobDevCadBeforeClose
    AfterScroll = QrEXcMobDevCadAfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM OVcMobDevCad'
      'WHERE Codigo > 0')
    Left = 228
    Top = 29
    object QrEXcMobDevCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEXcMobDevCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrEXcMobDevCadDeviceID: TWideStringField
      DisplayWidth = 60
      FieldName = 'DeviceID'
      Size = 60
    end
    object QrEXcMobDevCadUserNmePdr: TWideStringField
      FieldName = 'UserNmePdr'
      Size = 60
    end
    object QrEXcMobDevCadDeviceName: TWideStringField
      FieldName = 'DeviceName'
      Size = 60
    end
    object QrEXcMobDevCadDvcScreenH: TIntegerField
      FieldName = 'DvcScreenH'
      Required = True
    end
    object QrEXcMobDevCadDvcScreenW: TIntegerField
      FieldName = 'DvcScreenW'
      Required = True
    end
    object QrEXcMobDevCadOSName: TWideStringField
      FieldName = 'OSName'
      Size = 60
    end
    object QrEXcMobDevCadOSNickName: TWideStringField
      FieldName = 'OSNickName'
      Size = 60
    end
    object QrEXcMobDevCadOSVersion: TWideStringField
      FieldName = 'OSVersion'
      Size = 60
    end
    object QrEXcMobDevCadDtaHabIni: TDateTimeField
      FieldName = 'DtaHabIni'
      Required = True
    end
    object QrEXcMobDevCadDtaHabFim: TDateTimeField
      FieldName = 'DtaHabFim'
      Required = True
    end
    object QrEXcMobDevCadAllowed: TSmallintField
      FieldName = 'Allowed'
      Required = True
    end
    object QrEXcMobDevCadLastSetAlw: TIntegerField
      FieldName = 'LastSetAlw'
      Required = True
    end
    object QrEXcMobDevCadNO_Allowed: TWideStringField
      FieldName = 'NO_Allowed'
      Size = 3
    end
    object QrEXcMobDevCadChmOcorrencias: TSmallintField
      FieldName = 'ChmOcorrencias'
    end
    object QrEXcMobDevCadComptncPrf: TIntegerField
      FieldName = 'ComptncPrf'
    end
    object QrEXcMobDevCadNO_ComptncPrf: TWideStringField
      FieldName = 'NO_ComptncPrf'
      Size = 60
    end
    object QrEXcMobDevCadEXcIdFunc: TIntegerField
      FieldName = 'EXcIdFunc'
    end
    object QrEXcMobDevCadIDCntrCst: TIntegerField
      FieldName = 'IDCntrCst'
    end
    object QrEXcMobDevCadPIN_SHA2: TWideStringField
      FieldName = 'PIN_SHA2'
      Size = 255
    end
  end
  object DsEXcMobDevCad: TDataSource
    DataSet = QrEXcMobDevCad
    Left = 228
    Top = 73
  end
  object QrEXcMobDevAlw: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * '
      'FROM ovcmobdevalw'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 652
    Top = 5
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEXcMobDevAlwCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEXcMobDevAlwNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrEXcMobDevAlwMobDevCad: TIntegerField
      FieldName = 'MobDevCad'
      Required = True
    end
    object QrEXcMobDevAlwDeviceID: TWideStringField
      FieldName = 'DeviceID'
      Size = 60
    end
    object QrEXcMobDevAlwAlwReason: TSmallintField
      FieldName = 'AlwReason'
      Required = True
    end
    object QrEXcMobDevAlwDtaInicio: TDateTimeField
      FieldName = 'DtaInicio'
      Required = True
    end
    object QrEXcMobDevAlwLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrEXcMobDevAlwDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEXcMobDevAlwDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEXcMobDevAlwUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrEXcMobDevAlwUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrEXcMobDevAlwAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrEXcMobDevAlwAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrEXcMobDevAlwAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrEXcMobDevAlwAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsEXcMobDevAlw: TDataSource
    DataSet = QrEXcMobDevAlw
    Left = 652
    Top = 49
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 424
    Top = 376
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 300
    Top = 372
    object Aceitarnovo1: TMenuItem
      Caption = '&Aceitar novo'
      OnClick = Aceitarnovo1Click
    end
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      Visible = False
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrEXcMobDevAcs: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * '
      'FROM ovcmobdevacs'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 752
    Top = 5
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEXcMobDevAcsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEXcMobDevAcsNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrEXcMobDevAcsMobDevCad: TIntegerField
      FieldName = 'MobDevCad'
    end
    object QrEXcMobDevAcsDeviceID: TWideStringField
      FieldName = 'DeviceID'
      Size = 60
    end
    object QrEXcMobDevAcsAcsReason: TSmallintField
      FieldName = 'AcsReason'
      Required = True
    end
    object QrEXcMobDevAcsDtaAcsIni: TDateTimeField
      FieldName = 'DtaAcsIni'
      Required = True
    end
    object QrEXcMobDevAcsDtaAcsFim: TDateTimeField
      FieldName = 'DtaAcsFim'
      Required = True
    end
    object QrEXcMobDevAcsLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrEXcMobDevAcsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEXcMobDevAcsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEXcMobDevAcsUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrEXcMobDevAcsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrEXcMobDevAcsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrEXcMobDevAcsAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrEXcMobDevAcsAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrEXcMobDevAcsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsEXcMobDevAcs: TDataSource
    DataSet = QrEXcMobDevAcs
    Left = 752
    Top = 49
  end
  object QrComptncPrf: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeOpen = QrEXcMobDevCadBeforeOpen
    AfterOpen = QrEXcMobDevCadAfterOpen
    BeforeClose = QrEXcMobDevCadBeforeClose
    AfterScroll = QrEXcMobDevCadAfterScroll
    SQL.Strings = (
      'SELECT * '
      'FROM OVcMobDevCad'
      'WHERE Codigo > 0')
    Left = 356
    Top = 37
    object QrComptncPrfCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrComptncPrfNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsComptncPrf: TDataSource
    DataSet = QrComptncPrf
    Left = 356
    Top = 81
  end
end
