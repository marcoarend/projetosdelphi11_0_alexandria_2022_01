object FmOpcoesApp: TFmOpcoesApp
  Left = 339
  Top = 185
  Caption = 'APP-OPCAO-000 :: Op'#231#245'es Espec'#237'ficas do Sistema'
  ClientHeight = 657
  ClientWidth = 767
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 767
    Height = 501
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PCGeral: TPageControl
      Left = 0
      Top = 0
      Width = 767
      Height = 501
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Miscel'#226'nea'
        ImageIndex = 2
        object Panel10: TPanel
          Left = 0
          Top = 0
          Width = 759
          Height = 473
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label1: TLabel
            Left = 4
            Top = 96
            Width = 276
            Height = 13
            Caption = 'Email destinat'#225'rio das despesas de viagem (Controladoria):'
          end
          object Label3: TLabel
            Left = 4
            Top = 136
            Width = 220
            Height = 13
            Caption = 'Email destinat'#225'rio do uso particular do ve'#237'culo:'
          end
          object ImgPngB64: TImage
            Left = 204
            Top = 180
            Width = 192
            Height = 192
            Proportional = True
            Stretch = True
            Transparent = True
          end
          object Panel11: TPanel
            Left = 0
            Top = 0
            Width = 759
            Height = 49
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label9: TLabel
              Left = 8
              Top = 8
              Width = 129
              Height = 13
              Caption = 'Nome amig'#225'vel do sistema:'
            end
            object EdERPNameByCli: TdmkEdit
              Left = 8
              Top = 24
              Width = 205
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'ERPNameByCli'
              UpdCampo = 'ERPNameByCli'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object Panel2: TPanel
            Left = 0
            Top = 49
            Width = 759
            Height = 52
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object SbDirImgDesp: TSpeedButton
              Left = 728
              Top = 20
              Width = 23
              Height = 22
              OnClick = SbDirImgDespClick
            end
            object Label2: TLabel
              Left = 4
              Top = 4
              Width = 388
              Height = 13
              Caption = 
                'Diret'#243'rio dos arquivos de imagens de documentos (NFCes, RPS, rec' +
                'ibos e outros):'
            end
            object EdDirImgDesp: TEdit
              Left = 4
              Top = 20
              Width = 721
              Height = 21
              TabOrder = 0
            end
          end
          object EdMailsDstn: TEdit
            Left = 4
            Top = 112
            Width = 721
            Height = 21
            TabOrder = 2
          end
          object EdMailsDsKm: TEdit
            Left = 4
            Top = 152
            Width = 721
            Height = 21
            TabOrder = 3
          end
          object BtLogoEmpresa: TBitBtn
            Tag = 14
            Left = 6
            Top = 179
            Width = 119
            Height = 40
            Caption = '&Logo empresa'
            NumGlyphs = 2
            TabOrder = 4
            OnClick = BtLogoEmpresaClick
          end
        end
      end
      object TabSheet6: TTabSheet
        Caption = 'Usu'#225'rios'
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel16: TPanel
          Left = 0
          Top = 425
          Width = 759
          Height = 48
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object BtExclui: TBitBtn
            Tag = 12
            Left = 252
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Exclui'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtExcluiClick
          end
          object BtAltera: TBitBtn
            Tag = 11
            Left = 128
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Altera'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtAlteraClick
          end
          object BtInclui: TBitBtn
            Tag = 10
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Inclui'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtIncluiClick
          end
        end
        object DBGOpcoesApU: TdmkDBGridZTO
          Left = 0
          Top = 0
          Width = 759
          Height = 425
          Align = alClient
          DataSource = DsOpcoesApU
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Visible = False
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Login'
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NO_HabFaccao'
              Title.Caption = 'Fac'#231#227'o'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_HabFacConfecaocao'
              Title.Caption = 'Confec'#231#227'o'
              Width = 44
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NO_HabTextil'
              Title.Caption = 'T'#234'xtil'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_HabTexTecelagem'
              Title.Caption = 'Tecelagem'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_HabTexTinturaria'
              Title.Caption = 'Tinturaria'
              Width = 44
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 767
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 719
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 671
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 384
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Sistema'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 384
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Sistema'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 384
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Sistema'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 549
    Width = 767
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 763
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 593
    Width = 767
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel9: TPanel
      Left = 2
      Top = 15
      Width = 763
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 619
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 15
          Left = 6
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 14
        Top = 3
        Width = 119
        Height = 40
        Caption = '&Salva'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrEntiTipCto: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo,CodUsu, Nome '
      'FROM entitipcto'
      'ORDER BY Nome')
    Left = 630
    Top = 28
    object QrEntiTipCtoCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEntiTipCtoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiTipCtoNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsEntiTipCto: TDataSource
    DataSet = QrEntiTipCto
    Left = 630
    Top = 76
  end
  object dmkValUsu1: TdmkValUsu
    QryCampo = 'EntiTipCto'
    UpdCampo = 'EntiTipCto'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 64
    Top = 12
  end
  object QrOpcoesApU: TMySQLQuery
    Database = Dmod.ZZDB
    AfterOpen = QrOpcoesApUAfterOpen
    BeforeClose = QrOpcoesApUBeforeClose
    SQL.Strings = (
      'SELECT apu.Codigo, apu.HabFaccao, apu.HabTextil, '
      'apu.HabFacConfeccao, '
      'apu.HabTexTecelagem, apu.HabTexTinturaria, '
      'IF(apu.HabFaccao=1, "SIM", "N'#195'O") NO_HabFaccao, '
      'IF(apu.HabFacConfeccao=1, "SIM", "N'#195'O") NO_HabFacConfecaocao, '
      'IF(apu.HabTextil=1, "SIM", "N'#195'O") NO_HabTextil, '
      'IF(apu.HabTexTecelagem=1, "SIM", "N'#195'O") NO_HabTexTecelagem, '
      'IF(apu.HabTexTinturaria=1, "SIM", "N'#195'O") NO_HabTexTinturaria, '
      'pwd.Login '
      'FROM opcoesapu apu '
      'LEFT JOIN senhas pwd ON pwd.Numero=apu.Codigo ')
    Left = 146
    Top = 216
    object QrOpcoesApUCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOpcoesApUHabFaccao: TSmallintField
      FieldName = 'HabFaccao'
      Required = True
    end
    object QrOpcoesApUHabTextil: TSmallintField
      FieldName = 'HabTextil'
      Required = True
    end
    object QrOpcoesApUNO_HabFaccao: TWideStringField
      FieldName = 'NO_HabFaccao'
      Required = True
      Size = 3
    end
    object QrOpcoesApUNO_HabTextil: TWideStringField
      FieldName = 'NO_HabTextil'
      Required = True
      Size = 3
    end
    object QrOpcoesApULogin: TWideStringField
      FieldName = 'Login'
      Size = 30
    end
    object QrOpcoesApUHabFacConfeccao: TSmallintField
      FieldName = 'HabFacConfeccao'
      Required = True
    end
    object QrOpcoesApUHabTexTecelagem: TSmallintField
      FieldName = 'HabTexTecelagem'
      Required = True
    end
    object QrOpcoesApUHabTexTinturaria: TSmallintField
      FieldName = 'HabTexTinturaria'
      Required = True
    end
    object QrOpcoesApUNO_HabFacConfecaocao: TWideStringField
      FieldName = 'NO_HabFacConfecaocao'
      Required = True
      Size = 3
    end
    object QrOpcoesApUNO_HabTexTecelagem: TWideStringField
      FieldName = 'NO_HabTexTecelagem'
      Required = True
      Size = 3
    end
    object QrOpcoesApUNO_HabTexTinturaria: TWideStringField
      FieldName = 'NO_HabTexTinturaria'
      Required = True
      Size = 3
    end
  end
  object DsOpcoesApU: TDataSource
    DataSet = QrOpcoesApU
    Left = 146
    Top = 264
  end
end
