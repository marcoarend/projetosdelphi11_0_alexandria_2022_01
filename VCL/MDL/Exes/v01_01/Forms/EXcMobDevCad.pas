unit EXcMobDevCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, Vcl.ComCtrls, dmkEditDateTimePicker, dmkCheckBox,
  dmkDBLookupComboBox, dmkEditCB, UnProjGroup_Consts;

type
  TFmEXcMobDevCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrEXcMobDevCad: TMySQLQuery;
    DsEXcMobDevCad: TDataSource;
    QrEXcMobDevAlw: TMySQLQuery;
    DsEXcMobDevAlw: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrEXcMobDevCadCodigo: TIntegerField;
    QrEXcMobDevCadNome: TWideStringField;
    QrEXcMobDevCadDeviceID: TWideStringField;
    QrEXcMobDevCadUserNmePdr: TWideStringField;
    QrEXcMobDevCadDeviceName: TWideStringField;
    QrEXcMobDevCadDvcScreenH: TIntegerField;
    QrEXcMobDevCadDvcScreenW: TIntegerField;
    QrEXcMobDevCadOSName: TWideStringField;
    QrEXcMobDevCadOSNickName: TWideStringField;
    QrEXcMobDevCadOSVersion: TWideStringField;
    QrEXcMobDevCadDtaHabIni: TDateTimeField;
    QrEXcMobDevCadDtaHabFim: TDateTimeField;
    QrEXcMobDevCadAllowed: TSmallintField;
    QrEXcMobDevCadLastSetAlw: TIntegerField;
    EdDeviceID: TdmkEdit;
    Label3: TLabel;
    EdDeviceName: TdmkEdit;
    Label5: TLabel;
    EdDvcScreenW: TdmkEdit;
    Label6: TLabel;
    EdDvcScreenH: TdmkEdit;
    EdOSName: TdmkEdit;
    Label8: TLabel;
    EdOSNickName: TdmkEdit;
    Label10: TLabel;
    EdOSVersion: TdmkEdit;
    Label11: TLabel;
    TPDtaHabIni: TdmkEditDateTimePicker;
    EdDtaHabIni: TdmkEdit;
    TPDtaHabFim: TdmkEditDateTimePicker;
    EdDtaHabFim: TdmkEdit;
    Label13: TLabel;
    EdUserNmePdr: TdmkEdit;
    Label4: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label22: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label23: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Label24: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    EdNO_Allowed: TdmkEdit;
    QrEXcMobDevCadNO_Allowed: TWideStringField;
    Aceitarnovo1: TMenuItem;
    QrEXcMobDevAlwCodigo: TIntegerField;
    QrEXcMobDevAlwNome: TWideStringField;
    QrEXcMobDevAlwMobDevCad: TIntegerField;
    QrEXcMobDevAlwDeviceID: TWideStringField;
    QrEXcMobDevAlwAlwReason: TSmallintField;
    QrEXcMobDevAlwDtaInicio: TDateTimeField;
    QrEXcMobDevAlwLk: TIntegerField;
    QrEXcMobDevAlwDataCad: TDateField;
    QrEXcMobDevAlwDataAlt: TDateField;
    QrEXcMobDevAlwUserCad: TIntegerField;
    QrEXcMobDevAlwUserAlt: TIntegerField;
    QrEXcMobDevAlwAlterWeb: TSmallintField;
    QrEXcMobDevAlwAWServerID: TIntegerField;
    QrEXcMobDevAlwAWStatSinc: TSmallintField;
    QrEXcMobDevAlwAtivo: TSmallintField;
    QrEXcMobDevAcs: TMySQLQuery;
    DsEXcMobDevAcs: TDataSource;
    QrEXcMobDevAcsCodigo: TIntegerField;
    QrEXcMobDevAcsNome: TWideStringField;
    QrEXcMobDevAcsMobDevCad: TIntegerField;
    QrEXcMobDevAcsDeviceID: TWideStringField;
    QrEXcMobDevAcsAcsReason: TSmallintField;
    QrEXcMobDevAcsDtaAcsIni: TDateTimeField;
    QrEXcMobDevAcsDtaAcsFim: TDateTimeField;
    QrEXcMobDevAcsLk: TIntegerField;
    QrEXcMobDevAcsDataCad: TDateField;
    QrEXcMobDevAcsDataAlt: TDateField;
    QrEXcMobDevAcsUserCad: TIntegerField;
    QrEXcMobDevAcsUserAlt: TIntegerField;
    QrEXcMobDevAcsAlterWeb: TSmallintField;
    QrEXcMobDevAcsAWServerID: TIntegerField;
    QrEXcMobDevAcsAWStatSinc: TSmallintField;
    QrEXcMobDevAcsAtivo: TSmallintField;
    GBAlw: TGroupBox;
    DGDados: TDBGrid;
    Splitter1: TSplitter;
    GBAcs: TGroupBox;
    DBGrid1: TDBGrid;
    GroupBox3: TGroupBox;
    Panel8: TPanel;
    DBCheckBox4: TDBCheckBox;
    QrEXcMobDevCadChmOcorrencias: TSmallintField;
    GroupBox4: TGroupBox;
    Panel9: TPanel;
    CkChmOcorrencias: TdmkCheckBox;
    QrComptncPrf: TMySQLQuery;
    DsComptncPrf: TDataSource;
    EdComptncPrf: TdmkEditCB;
    CBComptncPrf: TdmkDBLookupComboBox;
    Label27: TLabel;
    QrComptncPrfCodigo: TIntegerField;
    QrComptncPrfNome: TWideStringField;
    QrEXcMobDevCadComptncPrf: TIntegerField;
    Label28: TLabel;
    QrEXcMobDevCadNO_ComptncPrf: TWideStringField;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    QrEXcMobDevCadEXcIdFunc: TIntegerField;
    Label29: TLabel;
    DBEdit14: TDBEdit;
    Label30: TLabel;
    EdEXcIdFunc: TdmkEdit;
    EdIDCntrCst: TdmkEdit;
    Label31: TLabel;
    QrEXcMobDevCadIDCntrCst: TIntegerField;
    Label32: TLabel;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    Label33: TLabel;
    QrEXcMobDevCadPIN_SHA2: TWideStringField;
    SbPIN: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEXcMobDevCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrEXcMobDevCadBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrEXcMobDevCadAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrEXcMobDevCadBeforeClose(DataSet: TDataSet);
    procedure Aceitarnovo1Click(Sender: TObject);
    procedure SbPINClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraEXcMobDevAlw(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenEXcMobDevAlw();
    procedure ReopenEXcMobDevAcs();

  end;

var
  FmEXcMobDevCad: TFmEXcMobDevCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF(*, EXcMobDevAlw*),
  EXcMobDevNew;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEXcMobDevCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmEXcMobDevCad.MostraEXcMobDevAlw(SQLType: TSQLType);
begin
(*
  if DBCheck.CriaFm(TFmEXcMobDevAlw, FmEXcMobDevAlw, afmoNegarComAviso) then
  begin
    FmEXcMobDevAlw.ImgTipo.SQLType := SQLType;
    FmEXcMobDevAlw.FQrCab := QrEXcMobDevCad;
    FmEXcMobDevAlw.FDsCab := DsCadastro_Com_Itens_CAB;
    FmEXcMobDevAlw.FQrIts := QrEXcMobDevAlw;
    if SQLType = stIns then
      FmEXcMobDevAlw.EdCPF1.ReadOnly := False
    else
    begin
      FmEXcMobDevAlw.EdControle.ValueVariant := QrEXcMobDevAlwControle.Value;
      //
      FmEXcMobDevAlw.EdCPF1.Text := MLAGeral.FormataCNPJ_TFT(QrEXcMobDevAlwCNPJ_CPF.Value);
      FmEXcMobDevAlw.EdNomeEmiSac.Text := QrEXcMobDevAlwNome.Value;
      FmEXcMobDevAlw.EdCPF1.ReadOnly := True;
    end;
    FmEXcMobDevAlw.ShowModal;
    FmEXcMobDevAlw.Destroy;
  end;
*)
end;

procedure TFmEXcMobDevCad.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrEXcMobDevCad);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrEXcMobDevCad, QrEXcMobDevAlw);
end;

procedure TFmEXcMobDevCad.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrEXcMobDevCad);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrEXcMobDevAlw);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrEXcMobDevAlw);
end;

procedure TFmEXcMobDevCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEXcMobDevCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEXcMobDevCad.DefParams;
begin
  VAR_GOTOTABELA := 'excmobdevcad';
  VAR_GOTOMYSQLTABLE := QrEXcMobDevCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT mdc.*, ');
  VAR_SQLx.Add('IF(mdc.Allowed=0, "N�O", "SIM") NO_Allowed,');
  VAR_SQLx.Add('cop.Nome NO_ComptncPrf');
  VAR_SQLx.Add('FROM excmobdevcad mdc');
  VAR_SQLx.Add('LEFT JOIN comptncprf cop ON cop.Codigo=mdc.ComptncPrf');
  VAR_SQLx.Add('WHERE mdc.Codigo > 0');
  //
  VAR_SQL1.Add('AND mdc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND mdc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND mdc.Nome Like :P0');
  //
end;

procedure TFmEXcMobDevCad.ItsAltera1Click(Sender: TObject);
begin
  MostraEXcMobDevAlw(stUpd);
end;

procedure TFmEXcMobDevCad.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmEXcMobDevCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmEXcMobDevCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEXcMobDevCad.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'EXcMobDevAlw', 'Controle', QrEXcMobDevAlwControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrEXcMobDevAlw,
      QrEXcMobDevAlwControle, QrEXcMobDevAlwControle.Value);
    ReopenEXcMobDevAlw(Controle);
  end;
}
end;

procedure TFmEXcMobDevCad.ReopenEXcMobDevAcs;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEXcMobDevAcs, Dmod.MyDB, [
  'SELECT * ',
  'FROM excmobdevacs ',
  'WHERE MobDevCad=' + Geral.FF0(QrEXcMobDevCadCodigo.Value),
  'ORDER BY DtaAcsIni DESC',
  '']);
  //
//  QrEXcMobDevAcs.Locate('Controle?, Controle, []);
end;

procedure TFmEXcMobDevCad.ReopenEXcMobDevAlw();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEXcMobDevAlw, Dmod.MyDB, [
  'SELECT * ',
  'FROM excmobdevalw ',
  'WHERE MobDevCad=' + Geral.FF0(QrEXcMobDevCadCodigo.Value),
  'ORDER BY DtaInicio DESC',
  '']);
  //
//  QrEXcMobDevAlw.Locate('Controle?, Controle, []);
end;


procedure TFmEXcMobDevCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEXcMobDevCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEXcMobDevCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEXcMobDevCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEXcMobDevCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEXcMobDevCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEXcMobDevCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEXcMobDevCadCodigo.Value;
  Close;
end;

procedure TFmEXcMobDevCad.ItsInclui1Click(Sender: TObject);
begin
  MostraEXcMobDevAlw(stIns);
end;

procedure TFmEXcMobDevCad.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrEXcMobDevCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'excmobdevcad');
end;

procedure TFmEXcMobDevCad.BtConfirmaClick(Sender: TObject);
var
  //DeviceID, DeviceName, OSName, OSNickName, OSVersion,
  //DtaHabIni, DtaHabFim
  Nome, UserNmePdr: String;
  //DvcScreenH, DvcScreenW,
  //Allowed, LastSetAlw,
  Codigo: Integer;
  SQLType: TSQLType;
  ComptncPrf, ChmOcorrencias, EXcIdFunc, IDCntrCst: Integer;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  //DeviceID       := EdDeviceID.ValueVariant;
  UserNmePdr     := EdUserNmePdr.ValueVariant;
  //DeviceName     := EdDeviceName.ValueVariant;
  //DvcScreenH     := EdDvcScreenH.ValueVariant;
  //DvcScreenW     := EdDvcScreenW.ValueVariant;
  //OSName         := EdOSName.ValueVariant;
  //OSNickName     := EdOSNickName.ValueVariant;
  //OSVersion      := EdOSVersion.ValueVariant;
  //DtaHabIni      := DtaHabIni ;
  //DtaHabIni      := Geral.FDT_TP_Ed(TPDtaHabIni.Date, EdDtaHabIni.Text);
  //DtaHabFim      := DtaHabFim ;
  //DtaHabFim      := Geral.FDT_TP_Ed(TPDtaHabFim.Date, EdDtaHabFim.Text);
  //Allowed        := Allowed   ;
  //LastSetAlw     := LastSetAlw;
  ComptncPrf       := EdComptncPrf.ValueVariant;
  ChmOcorrencias   := Geral.BoolToInt(CkChmOcorrencias.Checked);
  EXcIdFunc        := EdEXcIdFunc.ValueVariant;
  IDCntrCst        := EdIDCntrCst.ValueVariant;

  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Length(UserNmePdr) = 0, EdUserNmePdr,
    'Defina o nome do respons�vel padr�o do dispositivo!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('excmobdevcad', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'excmobdevcad', False, [
  'Nome', (*'DeviceID',*) 'UserNmePdr'(*,
  'DeviceName', 'DvcScreenH', 'DvcScreenW',
  'OSName', 'OSNickName', 'OSVersion',
  'DtaHabIni', 'DtaHabFim', 'Allowed',
  'LastSetAlw'*),
  'ComptncPrf', 'EXcIdFunc', 'IDCntrCst',
  'ChmOcorrencias'], [
  'Codigo'], [
  Nome, (*DeviceID,*) UserNmePdr(*,
  DeviceName, DvcScreenH, DvcScreenW,
  OSName, OSNickName, OSVersion,
  DtaHabIni, DtaHabFim, Allowed,
  LastSetAlw*),
  ComptncPrf, EXcIdFunc, IDCntrCst,
  ChmOcorrencias], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmEXcMobDevCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'excmobdevcad', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'excmobdevcad', 'Codigo');
end;

procedure TFmEXcMobDevCad.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmEXcMobDevCad.Aceitarnovo1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEXcMobDevNew, FmEXcMobDevNew, afmoNegarComAviso) then
  begin
    FmEXcMobDevNew.ShowModal;
    FmEXcMobDevNew.Destroy;
  end;

end;

procedure TFmEXcMobDevCad.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmEXcMobDevCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBAcs.Align := alClient;
  CriaOForm;
  FSeq := 0;
  UnDmkDAC_PF.AbreMySQLQuery0(QrComptncPrf, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM comptncprf ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmEXcMobDevCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrEXcMobDevCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEXcMobDevCad.SbPINClick(Sender: TObject);
var
  PIN, SQL: String;
  Codigo: Integer;
begin
  Codigo := QrEXcMobDevCadCodigo.Value;
  //
  if DBCheck.LiberaPelaSenhaBoss() then
  begin
    //Geral.MB_Info(QrEXcMobDevCadSenha.Value)
    if Geral.MB_Pergunta('Deseja alterar o PIN deste equipamento?') = ID_YES then
    begin
      PIN := '';
      if InputQuery('Defina o novo PIN', 'PIN novo', PIN) then
      begin
        if (Geral.SoNumero_TT(PIN) <> PIN) or (PIN = EmptyStr) then
        begin
          Geral.MB_Aviso('O PIN deve conter apenas n�meros!');
          Exit;
        end;
        SQL := Geral.ATS([
        'UPDATE excmobdevcad SET ',
        'PIN_SHA2=UPPER(SHA2("' + PIN + CO_AES_STR_PIN + '", 256))',
        'WHERE Codigo=' + Geral.FF0(Codigo),
        '']);
        //
        DmoD.MyDB.Execute(SQL);
        //
        LocCod(Codigo, Codigo);
        Geral.MB_Aviso('PIN alterado com sucesso!');
      end;
    end;
  end;
end;

procedure TFmEXcMobDevCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEXcMobDevCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrEXcMobDevCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmEXcMobDevCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmEXcMobDevCad.QrEXcMobDevCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmEXcMobDevCad.QrEXcMobDevCadAfterScroll(DataSet: TDataSet);
begin
  ReopenEXcMobDevAlw();
  ReopenEXcMobDevAcs();
end;

procedure TFmEXcMobDevCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrEXcMobDevCadCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmEXcMobDevCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEXcMobDevCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'excmobdevcad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEXcMobDevCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEXcMobDevCad.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrEXcMobDevCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'excmobdevcad');
end;

procedure TFmEXcMobDevCad.QrEXcMobDevCadBeforeClose(
  DataSet: TDataSet);
begin
  QrEXcMobDevAlw.Close;
  QrEXcMobDevAcs.Close;
end;

procedure TFmEXcMobDevCad.QrEXcMobDevCadBeforeOpen(DataSet: TDataSet);
begin
  QrEXcMobDevCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

