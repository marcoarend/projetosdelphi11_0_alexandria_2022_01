object FmCNAB_RetBco: TFmCNAB_RetBco
  Left = 339
  Top = 185
  Caption = 'RET-CNABx-002 :: Retorno de Arquivos CNAB - Multi Empresas'
  ClientHeight = 662
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCarrega: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 500
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnArquivos: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 469
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Splitter3: TSplitter
        Left = 0
        Top = 172
        Width = 1008
        Height = 5
        Cursor = crVSplit
        Align = alTop
        ExplicitLeft = 33
        ExplicitTop = 221
        ExplicitWidth = 1006
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 172
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object DBGBcoDir: TDBGrid
          Left = 0
          Top = 0
          Width = 1008
          Height = 92
          Align = alClient
          DataSource = DsBcoDir
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = DBGBcoDirDrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Banco'
              Width = 36
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'DirExiste'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Title.Caption = 'Dir existe'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNABDirMul'
              Title.Caption = 'Diret'#243'rio'
              Width = 480
              Visible = True
            end>
        end
        object DBGBcoArq: TDBGrid
          Left = 0
          Top = 92
          Width = 1008
          Height = 80
          Align = alBottom
          DataSource = DsBcoArq
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'SEQ'
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Arquivo'
              Width = 246
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Entidade'
              Title.Caption = 'Admin'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNPJ_CPF'
              Title.Caption = 'CNPJ / CPF admin'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Entid'
              Title.Caption = 'Nome da Empresa'
              Width = 270
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cedente'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BcoArq'
              Title.Caption = 'Banco'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Lotes'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Itens'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNAB'
              Width = 32
              Visible = True
            end>
        end
      end
      object PageControl1: TPageControl
        Left = 0
        Top = 177
        Width = 1008
        Height = 292
        ActivePage = TabSheet2
        Align = alClient
        TabOrder = 1
        object TabSheet3: TTabSheet
          Caption = 'Itens do arquivo selecionado'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Grade1_: TStringGrid
            Left = 0
            Top = 0
            Width = 1000
            Height = 73
            Align = alTop
            ColCount = 30
            DefaultColWidth = 32
            DefaultRowHeight = 18
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
            TabOrder = 0
            Visible = False
            ColWidths = (
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32)
          end
          object DBGrid1: TDBGrid
            Left = 0
            Top = 73
            Width = 1000
            Height = 191
            Align = alClient
            DataSource = DsBcoReg
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'SEQ'
                Width = 27
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CNPJ_CPF'
                Title.Caption = 'CNPJ/ CPF'
                Width = 92
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'EntCliente'
                Title.Caption = 'Empresa'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_EntClie'
                Title.Caption = 'Nome Empresa'
                Width = 211
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NossoNum'
                Title.Caption = 'Nosso n'#250'mero'
                Width = 108
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Cedente'
                Width = 80
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CartCod'
                Title.Caption = 'Carteira'
                Width = 44
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CartTxt'
                Title.Caption = 'Nome da carteira'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrCod'
                Title.Caption = 'Ocorr'#234'ncia'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrTxt'
                Title.Caption = 'Descri'#231#227'o da ocorr'#234'ncia'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrDta_TXT'
                Title.Caption = 'Dta ocor.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SeuNumer'
                Title.Caption = 'Seu n'#250'mero'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValTitul'
                Title.Caption = 'Val. t'#237'itulo'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValAbati'
                Title.Caption = 'Abatimento'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValDesco'
                Title.Caption = 'Desconto'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValEPago'
                Title.Caption = 'Valor pago'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuros'
                Title.Caption = 'Juros'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValMulta'
                Title.Caption = 'Multa'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValOutrC'
                Title.Caption = 'Outros (+)'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuMul'
                Title.Caption = 'Jur. + Mul.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValTarif'
                Title.Caption = 'Tarifa'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValErro'
                Title.Caption = 'Erro valor'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Motivos'
                Title.Caption = 'M.O.'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MotivTxt'
                Title.Caption = 'Motivos de ocorr'#234'nias'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PagtoDta_TXT'
                Title.Caption = 'Dta cr'#233'd c/c'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtaTarif_TXT'
                Title.Caption = 'Dta. tarifa'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Entidade'
                Title.Caption = 'Admin'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ID_Link'
                Title.Caption = 'ID Link'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SeqDir'
                Title.Caption = 'Seq. pasta'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SeqArq'
                Title.Caption = 'Seq. arq.'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SeqReg'
                Title.Caption = 'Seq. registro'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Banco'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NumDocum'
                Title.Caption = 'Documento'
                Width = 108
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValCBrut'
                Title.Caption = 'Val. bruto'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValOutrD'
                Title.Caption = 'Outros (-)'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VenctDta_TXT'
                Title.Caption = 'Dta. vcto'
                Width = 56
                Visible = True
              end>
          end
        end
        object TabSheet1: TTabSheet
          Caption = 'Avisos'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Memo1: TMemo
            Left = 0
            Top = 0
            Width = 1000
            Height = 264
            Align = alClient
            TabOrder = 0
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Opera'#231#245'es'
          ImageIndex = 2
          object Memo2: TMemo
            Left = 0
            Top = 0
            Width = 1000
            Height = 264
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 543
        Height = 32
        Caption = 'Retorno de Arquivos CNAB - Multi Empresas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 543
        Height = 32
        Caption = 'Retorno de Arquivos CNAB - Multi Empresas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 543
        Height = 32
        Caption = 'Retorno de Arquivos CNAB - Multi Empresas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 548
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 682
        Height = 16
        Caption = 
          '  Antes de carregar verifique se todas ocorr'#234'ncias j'#225' est'#227'o cada' +
          'stradas nos respectivos bancos (Cadastro de bancos).'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 682
        Height = 16
        Caption = 
          '  Antes de carregar verifique se todas ocorr'#234'ncias j'#225' est'#227'o cada' +
          'stradas nos respectivos bancos (Cadastro de bancos).'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 592
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 860
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtArquivos: TBitBtn
        Tag = 28
        Left = 20
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Arquivos'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtArquivosClick
      end
      object BtEdita: TBitBtn
        Tag = 11
        Left = 112
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Editar'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtEditaClick
      end
      object BtCarrega: TBitBtn
        Tag = 14
        Left = 204
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Carregar'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtCarregaClick
      end
      object CkReverter: TCheckBox
        Left = 300
        Top = 20
        Width = 77
        Height = 17
        Caption = 'Re-registrar.'
        TabOrder = 4
      end
      object BtAbertos: TBitBtn
        Tag = 10017
        Left = 380
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Abertos'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 5
        Visible = False
      end
      object BtBuffer: TBitBtn
        Left = 472
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Buffer'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 6
        Visible = False
      end
      object PB1: TProgressBar
        Left = 704
        Top = 20
        Width = 185
        Height = 17
        TabOrder = 7
        Visible = False
      end
    end
  end
  object QrBcoDir: TmySQLQuery
    Database = DModG.MyPID_DB
    BeforeClose = QrBcoDirBeforeClose
    AfterClose = QrBcoDirAfterClose
    AfterScroll = QrBcoDirAfterScroll
    SQL.Strings = (
      'SELECT * FROM cnabbcodir')
    Left = 8
    Top = 12
    object QrBcoDirCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cnabbcodir.Codigo'
    end
    object QrBcoDirNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'cnabbcodir.Nome'
      Size = 255
    end
    object QrBcoDirCNABDirMul: TWideStringField
      FieldName = 'CNABDirMul'
      Origin = 'cnabbcodir.CNABDirMul'
      Size = 255
    end
    object QrBcoDirDirExiste: TWideStringField
      FieldName = 'DirExiste'
      Origin = 'cnabbcodir.DirExiste'
      Size = 3
    end
    object QrBcoDirArquivos: TIntegerField
      FieldName = 'Arquivos'
      Origin = 'cnabbcodir.Arquivos'
    end
    object QrBcoDirAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'cnabbcodir.Ativo'
    end
  end
  object QrBcoArq: TmySQLQuery
    Database = DModG.MyPID_DB
    BeforeClose = QrBcoArqBeforeClose
    AfterScroll = QrBcoArqAfterScroll
    OnCalcFields = QrBcoArqCalcFields
    SQL.Strings = (
      'SELECT * FROM cnabbcoarq')
    Left = 64
    Top = 12
    object QrBcoArqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBcoArqControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrBcoArqArquivo: TWideStringField
      FieldName = 'Arquivo'
      Size = 255
    end
    object QrBcoArqEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrBcoArqNO_Entid: TWideStringField
      FieldName = 'NO_Entid'
      Size = 255
    end
    object QrBcoArqCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
    end
    object QrBcoArqLotes: TIntegerField
      FieldName = 'Lotes'
    end
    object QrBcoArqItens: TIntegerField
      FieldName = 'Itens'
    end
    object QrBcoArqCreditos: TFloatField
      FieldName = 'Creditos'
    end
    object QrBcoArqBcoArq: TIntegerField
      FieldName = 'BcoArq'
    end
    object QrBcoArqCNAB: TIntegerField
      FieldName = 'CNAB'
    end
    object QrBcoArqAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrBcoArqSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
  end
  object DsBcoDir: TDataSource
    DataSet = QrBcoDir
    Left = 36
    Top = 12
  end
  object DsBcoArq: TDataSource
    DataSet = QrBcoArq
    Left = 92
    Top = 12
  end
  object QrTem: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(Codigo) Itens '
      'FROM cnab_lei'
      'WHERE Step=0')
    Left = 796
    Top = 12
    object QrTemItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrLocByCed: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF   '
      'FROM carteiras car'
      'LEFT JOIN entidades ent ON ent.Codigo=car.ForneceI  '
      'WHERE car.CodCedente = :P0'
      'AND car.Banco1=:P1 '
      '   ')
    Left = 824
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocByCedCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
  end
  object QrLocEnt2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE '
      'ent.Nome END NOMEENT, ent.Codigo CLIENTE, cnd.Codigo CliInt'
      'FROM cond cnd'
      'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente'
      ''
      'WHERE cnd.Banco=:P0'
      'AND cnd.Agencia=:P1'
      'AND cnd.CodCedente=:P2')
    Left = 824
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrLocEnt2NOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrLocEnt2CLIENTE: TIntegerField
      FieldName = 'CLIENTE'
      Required = True
    end
    object QrLocEnt2CliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object QrLocEnt1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE '
      'ent.Nome END NOMEENT, ent.Codigo CLIENTE, cnd.Codigo CliInt '
      'FROM cond cnd'
      'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente'
      'WHERE (CASE WHEN ent.Tipo=0 THEN ent.CNPJ ELSE ent.CPF END)=:P0'
      '/*   '
      '  AND cnd.CodCedente=:P1 '
      '*/')
    Left = 796
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocEnt1NOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrLocEnt1CLIENTE: TIntegerField
      FieldName = 'CLIENTE'
      Required = True
    end
    object QrLocEnt1CliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object QrPesq2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '')
    Left = 852
    Top = 12
    object QrPesq2Vencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrPesq2PercMulta: TFloatField
      FieldName = 'PercMulta'
    end
    object QrPesq2PercJuros: TFloatField
      FieldName = 'PercJuros'
    end
    object QrPesq2Credito: TFloatField
      FieldName = 'Credito'
    end
  end
  object QrBcoReg: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterOpen = QrBcoRegAfterOpen
    BeforeClose = QrBcoRegBeforeClose
    OnCalcFields = QrBcoRegCalcFields
    SQL.Strings = (
      'SELECT * FROM cnabbcoreg')
    Left = 120
    Top = 12
    object QrBcoRegSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrBcoRegCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBcoRegControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrBcoRegConta: TAutoIncField
      FieldName = 'Conta'
    end
    object QrBcoRegNossoNum: TWideStringField
      FieldName = 'NossoNum'
      Size = 30
    end
    object QrBcoRegOcorrCod: TIntegerField
      FieldName = 'OcorrCod'
      DisplayFormat = '00;-00; '
    end
    object QrBcoRegOcorrTxt: TWideStringField
      FieldName = 'OcorrTxt'
      Size = 255
    end
    object QrBcoRegOcorrDta: TDateField
      FieldName = 'OcorrDta'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBcoRegSeuNumer: TFloatField
      FieldName = 'SeuNumer'
    end
    object QrBcoRegNumDocum: TWideStringField
      FieldName = 'NumDocum'
      Size = 60
    end
    object QrBcoRegValTitul: TFloatField
      FieldName = 'ValTitul'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrBcoRegValAbati: TFloatField
      FieldName = 'ValAbati'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrBcoRegValDesco: TFloatField
      FieldName = 'ValDesco'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrBcoRegValEPago: TFloatField
      FieldName = 'ValEPago'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrBcoRegValJuros: TFloatField
      FieldName = 'ValJuros'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrBcoRegValMulta: TFloatField
      FieldName = 'ValMulta'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrBcoRegValOutrC: TFloatField
      FieldName = 'ValOutrC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrBcoRegValJuMul: TFloatField
      FieldName = 'ValJuMul'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrBcoRegValTarif: TFloatField
      FieldName = 'ValTarif'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrBcoRegValErro: TFloatField
      FieldName = 'ValErro'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrBcoRegMotivos: TWideStringField
      FieldName = 'Motivos'
      Size = 30
    end
    object QrBcoRegMotivTxt: TWideStringField
      FieldName = 'MotivTxt'
      Size = 255
    end
    object QrBcoRegPagtoDta: TDateField
      FieldName = 'PagtoDta'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBcoRegDtaTarif: TDateField
      FieldName = 'DtaTarif'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBcoRegEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrBcoRegID_Link: TWideStringField
      FieldName = 'ID_Link'
      Size = 30
    end
    object QrBcoRegSeqDir: TIntegerField
      FieldName = 'SeqDir'
    end
    object QrBcoRegSeqArq: TIntegerField
      FieldName = 'SeqArq'
    end
    object QrBcoRegSeqReg: TIntegerField
      FieldName = 'SeqReg'
    end
    object QrBcoRegBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrBcoRegValCBrut: TFloatField
      FieldName = 'ValCBrut'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrBcoRegValOutrD: TFloatField
      FieldName = 'ValOutrD'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrBcoRegVenctDta: TDateField
      FieldName = 'VenctDta'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrBcoRegAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrBcoRegOcorrDta_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'OcorrDta_TXT'
      Size = 10
      Calculated = True
    end
    object QrBcoRegPagtoDta_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PagtoDta_TXT'
      Size = 10
      Calculated = True
    end
    object QrBcoRegDtaTarif_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DtaTarif_TXT'
      Size = 10
      Calculated = True
    end
    object QrBcoRegVenctDta_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'VenctDta_TXT'
      Size = 10
      Calculated = True
    end
    object QrBcoRegCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 14
    end
    object QrBcoRegEntCliente: TIntegerField
      FieldName = 'EntCliente'
    end
    object QrBcoRegNO_EntClie: TWideStringField
      FieldName = 'NO_EntClie'
      Size = 100
    end
    object QrBcoRegCedente: TWideStringField
      FieldName = 'Cedente'
      Size = 30
    end
    object QrBcoRegCartCod: TIntegerField
      FieldName = 'CartCod'
    end
    object QrBcoRegCartTxt: TWideStringField
      FieldName = 'CartTxt'
      Size = 100
    end
  end
  object DsBcoReg: TDataSource
    DataSet = QrBcoReg
    Left = 148
    Top = 12
  end
  object QrErros: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * '
      'FROM cnabbcoreg'
      'WHERE ValErro >= 0.01'
      'AND Controle=:P0')
    Left = 880
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrLocEnt3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT, '
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF, '
      'ent.Codigo CLIENTE, cnd.Codigo CliInt'
      'FROM cond cnd'
      'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente'
      ''
      'WHERE cnd.Banco=:P0'
      'AND cnd.Agencia=:P1'
      'AND cnd.Conta=:P2'
      'AND cnd.DVConta=:P3')
    Left = 852
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrLocEnt3NOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrLocEnt3CLIENTE: TIntegerField
      FieldName = 'CLIENTE'
      Required = True
    end
    object QrLocEnt3CliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLocEnt3CNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
  end
  object QrLocCar1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Codigo, car.Nome'
      'FROM carteiras car'
      'LEFT JOIN entidades ent ON ent.Codigo=car.ForneceI'
      'WHERE car.ForneceI=:P0'
      'AND car.CodCedente=:P1'
      'AND (IF (ent.Tipo=0, ent.CNPJ, ent.CPF))  = :P2'
      'ORDER BY car.Nome')
    Left = 796
    Top = 68
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrLocCar1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocCar1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrSemCart: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * '
      'FROM cnabbcoreg'
      'WHERE CartCod = 0'
      'AND Controle=:P0')
    Left = 908
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrDupl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo '
      'FROM cnab_lei'
      'WHERE Banco=:P0'
      'AND NossoNum=:P1'
      'AND SeuNum=:P2'
      'AND OcorrCodi=:P3'
      'AND Arquivo=:P4'
      'AND ItemArq=:P5'
      'AND ValTitul=:P6'
      'AND ValPago=:P7'
      'AND QuitaData=:P8')
    Left = 936
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P7'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P8'
        ParamType = ptUnknown
      end>
    object QrDuplCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrPesq3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '')
    Left = 964
    Top = 12
    object QrPesq3Credito: TFloatField
      FieldName = 'Credito'
    end
    object QrPesq3PercMulta: TFloatField
      FieldName = 'PercMulta'
    end
    object QrPesq3PercJuros: TFloatField
      FieldName = 'PercJuros'
    end
    object QrPesq3Vencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
  end
end
