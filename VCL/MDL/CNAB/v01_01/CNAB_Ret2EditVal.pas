unit CNAB_Ret2EditVal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, frxClass, frxRich, dmkGeral, DB, mySQLDbTables, frxDBSet, StdCtrls,
  ExtCtrls, Buttons;

type
  TFmCNAB_Res2EditVal = class(TForm)
    GBAvisos1: TGroupBox;
    Panel22: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PainelControle: TPanel;
    Panel5: TPanel;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AjustaValoresDoBloquetoAtual;
  end;

var
  FmCNAB_Res2EditVal: TFmCNAB_Res2EditVal;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, CNAB_Ret2a;

{$R *.dfm}

{ TFmContratApp }

procedure TFmCNAB_Res2EditVal.AjustaValoresDoBloquetoAtual;
var
  Juros, Multa, Difer, Desco: Double;
begin
  try
    Screen.Cursor := crHourGlass;
    Desco := 0;
    Multa := 0;
    Juros := 0;
    Difer := FmCNAB_Ret2a.QrLeiValTitul.Value - FmCNAB_Ret2a.QrLeiSumCredito.Value;
    if Difer > 0 then
    begin
      if FmCNAB_Ret2a.QrLeiDevMulta.Value > Difer then Multa := Difer
      else begin
        Multa := FmCNAB_Ret2a.QrLeiDevMulta.Value;
        Juros := Difer - FmCNAB_Ret2a.QrLeiDevMulta.Value
      end;
    end else Desco := - Difer;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE cnab_lei SET ValJuros=:P0, ValMulta=:P1, ');
    Dmod.QrUpd.SQL.Add('ValDesco=:P2, ValTitul=:P3 WHERE Codigo=:Pa');
    Dmod.QrUpd.Params[00].AsFloat   := Juros;
    Dmod.QrUpd.Params[01].AsFloat   := Multa;
    Dmod.QrUpd.Params[02].AsFloat   := Desco;
    Dmod.QrUpd.Params[03].AsFloat   := FmCNAB_Ret2a.QrLeiSumCredito.Value;
    //
    Dmod.QrUpd.Params[04].AsInteger := FmCNAB_Ret2a.QrLeiCodigo.Value;
    Dmod.QrUpd.ExecSQL;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCNAB_Res2EditVal.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

end.
