unit CNABRetTar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmCNABRetTar = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    CBGenero: TdmkDBLookupComboBox;
    EdGenero: TdmkEditCB;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasCodUsu: TIntegerField;
    QrContasNome: TWideStringField;
    DsContas: TDataSource;
    Label2: TLabel;
    EdOcorrencia: TdmkEdit;
    Label4: TLabel;
    EdNOMEGENERO: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdOcorrenciaChange(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmCNABRetTar: TFmCNABRetTar;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnBancos, Contas;

{$R *.DFM}

procedure TFmCNABRetTar.BtOKClick(Sender: TObject);
var
  Codigo, Controle: Integer;
begin
(*
  MyObjects.FIC(Trim(EdNome.Text) = '', EdNome, 'Informe uma descri��o!') then
    Exit;
  Codigo   := Geral.IMV(DBEdCodigo.Text);
  Controle? := EdControle.ValueVariant;
  Controle? := UMyMod.BuscaEmLivreY_Def('cadastro_com_itens_its', 'Controle', ImgTipo.SQLType, Controle);
ou > ? := UMyMod.BPGS1I32('cadastro_com_itens_its', 'Controle', '', '', tsPosNeg?, ImgTipo.SQLType, Controle);
if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, ImgTipo.SQLType?, 'cadastro_com_itens_its', auto_increment?[
capos?], [
'Controle'], [
valores?], [
Controle], UserDataAlterweb?, IGNORE?
  begin
    ReopenCadastro_Com_Itens_ITS(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdCargo.ValueVariant     := 0;
      CBCargo.KeyValue         := Null;
      EdNome.ValueVariant      := '';
      EdNome.SetFocus;
    end else Close;
  end;
*)
end;

procedure TFmCNABRetTar.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCNABRetTar.EdOcorrenciaChange(Sender: TObject);
var
  Movimento: Integer;
begin
  Movimento := EdOcorrencia.ValueVariant;
  EdNOMEGENERO.Text := UBancos.CNABTipoDeMovimento(
    FQrCab.FieldByName('Codigo').AsInteger, ecnabRetorno,
  Movimento, TamReg: Integer; Registrado: Boolean): String;

end;

procedure TFmCNABRetTar.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdCodUso.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmCNABRetTar.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(Qr_Sel_, Dmod.MyDB?);
end;

procedure TFmCNABRetTar.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCNABRetTar.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger :=
      FQrCab.FieldByName('Codigo').AsInteger;
    FQrIts.Open;
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmCNABRetTar.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    FmContas.ShowModal;
    FmContas.Destroy;
  end;
  UMyMod.SetaCodigoPesquisado(EdGenero, CBGenero, QrContas, VAR_CADASTRO);
end;

end.
