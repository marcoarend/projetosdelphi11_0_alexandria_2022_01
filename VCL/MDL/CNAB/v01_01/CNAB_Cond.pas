unit CNAB_Cond;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmCNAB_Cond = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    Panel6: TPanel;
    RGPosto: TRadioGroup;
    RGModalCobr: TRadioGroup;
    RGBanco: TRadioGroup;
    RGAgencia: TRadioGroup;
    RGConta: TRadioGroup;
    RGDVAgencia: TRadioGroup;
    RGDVConta: TRadioGroup;
    RGCarteira: TRadioGroup;
    RGIDCobranca: TRadioGroup;
    RGCedente: TRadioGroup;
    Panel7: TPanel;
    RGCartTxt: TRadioGroup;
    RGTipoCobranca: TRadioGroup;
    RGOperCodi: TRadioGroup;
    RGEspecieDoc: TRadioGroup;
    RGAgContaCed: TRadioGroup;
    RGEspecieTxt: TRadioGroup;
    RGCNAB: TRadioGroup;
    RGCtaCooper: TRadioGroup;
    RGNada: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    // meu
    procedure HabilitaBtOK(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FEncerrar: Boolean;
    FCondominio, FCNAB_Cfg: Integer;
    //
  end;

  var
  FmCNAB_Cond: TFmCNAB_Cond;

implementation

uses UnMyObjects, Module, UMySQLModule;

{$R *.DFM}

procedure TFmCNAB_Cond.BtOKClick(Sender: TObject);
  function AtualizaCNAB_CFG(): Boolean;
  var
    CedConta, CedDAC_A, CedDAC_C, CartNum, CodEmprBco, OperCodi, IDCobranca,
    CNAB, EspecieDoc, AgContaCed, EspecieTxt, CartTxt, CtaCooper: String;
    Codigo, CedBanco, CedAgencia, ModalCobr, CedPosto, TipoCobranca: Integer;
  begin
    Codigo       := FCNAB_Cfg;
    ModalCobr    := Geral.IMV(RGModalCobr.Items[RGModalCobr.ItemIndex]);
    CedBanco     := Geral.IMV(RGBanco.Items[RGBanco.ItemIndex]);
    CedAgencia   := Geral.IMV(RGAgencia.Items[RGAgencia.ItemIndex]);
    CedPosto     := Geral.IMV(RGPosto.Items[RGPosto.ItemIndex]);
    CedConta     := RGConta.Items[RGConta.ItemIndex];
    CartNum      := RGCarteira.Items[RGCarteira.ItemIndex];
    IDCobranca   := RGIDCobranca.Items[RGIDCobranca.ItemIndex];
    CodEmprBco   := RGCedente.Items[RGCedente.ItemIndex];
    TipoCobranca := Geral.IMV(RGTipoCobranca.Items[RGTipoCobranca.ItemIndex]);
    CedDAC_A     := RGDVAgencia.Items[RGDVAgencia.ItemIndex];
    CedDAC_C     := RGDVConta.Items[RGDVConta.ItemIndex];
    OperCodi     := RGOperCodi.Items[RGOperCodi.ItemIndex];
    EspecieDoc   := RGEspecieDoc.Items[RGEspecieDoc.ItemIndex];
    AgContaCed   := RGAgContaCed.Items[RGAgContaCed.ItemIndex];
    EspecieTxt   := RGEspecieTxt.Items[RGEspecieTxt.ItemIndex];
    CartTxt      := RGCartTxt.Items[RGCartTxt.ItemIndex];
    CNAB         := RGCNAB.Items[RGCNAB.ItemIndex];
    CtaCooper    := RGCtaCooper.Items[RGCtaCooper.ItemIndex];
    //
    //if
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cnab_cfg', False, [
    'ModalCobr', 'CedBanco', 'CedAgencia',
    'CedPosto', 'CedConta', 'CartNum',
    'IDCobranca', 'CodEmprBco', 'TipoCobranca',
    'CedDAC_A', 'CedDAC_C', 'OperCodi',
    'EspecieDoc', 'AgContaCed', 'EspecieTxt',
    'CartTxt', 'CNAB', 'CtaCooper'], [
    'Codigo'], [
    ModalCobr, CedBanco, CedAgencia,
    CedPosto, CedConta, CartNum,
    IDCobranca, CodEmprBco, TipoCobranca,
    CedDAC_A, CedDAC_C, OperCodi,
    EspecieDoc, AgContaCed, EspecieTxt,
    CartTxt, CNAB, CtaCooper], [
    Codigo], True);
  end;
  //
  function AtualizaCond(): Boolean;
  var
    Conta, CodCedente, Carteira, DVAgencia, DVConta, IDCobranca, OperCodi,
    EspecieDoc, AgContaCed, EspecieVal, CartTxt, CNAB, CtaCooper: String;
    Codigo, Banco, Agencia, Posto, ModalCobr, TipoCobranca: Integer;
  begin

    Codigo       := FCondominio;
    ModalCobr    := Geral.IMV(RGModalCobr   .Items[RGModalCobr   .ItemIndex]);
    Banco        := Geral.IMV(RGBanco       .Items[RGBanco       .ItemIndex]);
    Agencia      := Geral.IMV(RGAgencia     .Items[RGAgencia     .ItemIndex]);
    Posto        := Geral.IMV(RGPosto       .Items[RGPosto       .ItemIndex]);
    Conta        := RGConta       .Items[RGConta       .ItemIndex];
    Carteira     := RGCarteira    .Items[RGCarteira    .ItemIndex];
    IDCobranca   := RGIDCobranca  .Items[RGIDCobranca  .ItemIndex];
    CodCedente   := RGCedente     .Items[RGCedente     .ItemIndex];
    TipoCobranca := Geral.IMV(RGTipoCobranca.Items[RGTipoCobranca.ItemIndex]);
    DVAgencia    := RGDVAgencia   .Items[RGDVAgencia   .ItemIndex];
    DVConta      := RGDVConta     .Items[RGDVConta     .ItemIndex];
    OperCodi     := RGOperCodi    .Items[RGOperCodi    .ItemIndex];
    EspecieDoc   := RGEspecieDoc.Items[RGEspecieDoc.ItemIndex];
    AgContaCed   := RGAgContaCed.Items[RGAgContaCed.ItemIndex];
    EspecieVal   := RGEspecieTxt.Items[RGEspecieTxt.ItemIndex];
    CartTxt      := RGCartTxt.Items[RGCartTxt.ItemIndex];
    CNAB         := RGCNAB.Items[RGCNAB.ItemIndex];
    CtaCooper    := RGCtaCooper.Items[RGCtaCooper.ItemIndex];
     //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cond', False, [
    'ModalCobr', 'Banco', 'Agencia',
    'Posto', 'Conta', 'Carteira',
    'IDCobranca', 'CodCedente', 'TipoCobranca',
    'DVAgencia', 'DVConta', 'OperCodi',
    'EspecieDoc', 'AgContaCed', 'EspecieVal',
    'CartTxt', 'CNAB', 'CtaCooper'], [
    'Codigo'], [
    ModalCobr, Banco, Agencia,
    Posto, Conta, Carteira,
    IDCobranca, CodCedente, TipoCobranca,
    DVAgencia, DVConta, OperCodi,
    EspecieDoc, AgContaCed, EspecieVal,
    CartTxt, CNAB, CtaCooper], [
    Codigo], True);
  end;
begin
  if AtualizaCNAB_CFG() then
    if AtualizaCond() then
    begin
      FEncerrar := True;
      Close;
    end;
end;

procedure TFmCNAB_Cond.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCNAB_Cond.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmCNAB_Cond.FormCreate(Sender: TObject);
begin
  FEncerrar := False;
end;

procedure TFmCNAB_Cond.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCNAB_Cond.HabilitaBtOK(Sender: TObject);
var
  I: Integer;
begin
  for I := 0 to Self.ComponentCount - 1 do
  begin
    if Self.Components[I] is TRadioGroup then
    begin
      if LowerCase(TRadioGroup(Self.Components[I]).Name) <> 'rgnada' then
      begin
        if (TRadioGroup(Self.Components[I]).Itemindex = -1) and
        (TRadioGroup(Self.Components[I]).Items.Count > 0) then
        begin
          BtOK.Enabled := False;
          Exit;
        end;
      end;
    end;
  end;
  BtOK.Enabled := True;
end;

end.
