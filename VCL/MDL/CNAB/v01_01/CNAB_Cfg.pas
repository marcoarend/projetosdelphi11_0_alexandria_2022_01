unit CNAB_Cfg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2,
  UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, dmkLabel, dmkGeral, ComCtrls, dmkMemo, Variants, dmkRadioGroup,
  UnDmkProcFunc, dmkImage, dmkCheckBox, DmkDAC_PF, UnDmkEnums, Vcl.Grids,
  Vcl.DBGrids, dmkDBGridZTO, Vcl.Menus;

type
  TFmCNAB_Cfg = class(TForm)
    PainelDados: TPanel;
    DsCNAB_Cfg: TDataSource;
    QrCNAB_Cfg: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    QrBancos: TmySQLQuery;
    DsBancos: TDataSource;
    QrBancosCodigo: TIntegerField;
    QrBancosNome: TWideStringField;
    QrCNAB_CfgCodigo: TIntegerField;
    QrCNAB_CfgNome: TWideStringField;
    QrCNAB_CfgNOMEBANCO: TWideStringField;
    QrCNAB_CfgCedBanco: TIntegerField;
    QrCNAB_CfgCedAgencia: TIntegerField;
    QrCNAB_CfgCedConta: TWideStringField;
    QrCNAB_CfgCedNome: TWideStringField;
    QrCNAB_CfgTipoCart: TSmallintField;
    QrCNAB_CfgCartNum: TWideStringField;
    QrCNAB_CfgCartCod: TWideStringField;
    QrCNAB_CfgEspecieTit: TWideStringField;
    QrCNAB_CfgInstrCobr1: TWideStringField;
    QrCNAB_CfgInstrCobr2: TWideStringField;
    QrCNAB_CfgAceiteTit: TSmallintField;
    QrCNAB_CfgInstrDias: TSmallintField;
    Panel4: TPanel;
    QrCedente: TmySQLQuery;
    DsCedente: TDataSource;
    QrCedenteCodigo: TIntegerField;
    QrCedenteNOMEENT: TWideStringField;
    QrCNAB_CfgSacadAvali: TIntegerField;
    QrCNAB_CfgSacAvaNome: TWideStringField;
    QrCNAB_CfgCodEmprBco: TWideStringField;
    QrCNAB_CfgMultaTipo: TSmallintField;
    QrCNAB_CfgMultaPerc: TFloatField;
    QrCNAB_CfgTexto01: TWideStringField;
    QrCNAB_CfgTexto02: TWideStringField;
    QrCNAB_CfgTexto03: TWideStringField;
    QrCNAB_CfgTexto04: TWideStringField;
    QrCNAB_CfgTexto05: TWideStringField;
    QrCNAB_CfgTexto06: TWideStringField;
    QrCNAB_CfgTexto07: TWideStringField;
    QrCNAB_CfgTexto08: TWideStringField;
    QrCNAB_CfgTexto09: TWideStringField;
    QrCNAB_CfgTexto10: TWideStringField;
    QrCNAB_CfgLk: TIntegerField;
    QrCNAB_CfgDataCad: TDateField;
    QrCNAB_CfgDataAlt: TDateField;
    QrCNAB_CfgUserCad: TIntegerField;
    QrCNAB_CfgUserAlt: TIntegerField;
    QrCNAB_CfgAlterWeb: TSmallintField;
    QrCNAB_CfgAtivo: TSmallintField;
    QrCNAB_CfgJurosPerc: TFloatField;
    QrCNAB_CfgJurosDias: TSmallintField;
    QrCNAB_CfgJurosTipo: TSmallintField;
    QrCNAB_CfgCNAB: TIntegerField;
    QrCNAB_CfgEnvEmeio: TSmallintField;
    QrCNAB_CfgCedDAC_A: TWideStringField;
    QrCNAB_CfgCedDAC_C: TWideStringField;
    QrCNAB_CfgCedDAC_AC: TWideStringField;
    OpenDialog1: TOpenDialog;
    QrCNAB_CfgDiretorio: TWideStringField;
    QrCNAB_Cfg_237Mens1: TWideStringField;
    QrCNAB_Cfg_237Mens2: TWideStringField;
    QrCNAB_CfgCodOculto: TWideStringField;
    QrCNAB_CfgSeqArq: TIntegerField;
    QrSacador: TmySQLQuery;
    DsSacador: TDataSource;
    QrSacadorCodigo: TIntegerField;
    QrSacadorNOMEENT: TWideStringField;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label10: TLabel;
    RGCNAB: TRadioGroup;
    QrCNAB_CfgCedPosto: TIntegerField;
    QrCNAB_CfgLastNosNum: TLargeintField;
    PageControl1: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    Panel8: TPanel;
    TabSheet6: TTabSheet;
    Panel7: TPanel;
    Label30: TLabel;
    Label27: TLabel;
    Label26: TLabel;
    EdInstrDias: TdmkEdit;
    dmkMemo2: TdmkMemo;
    EdInstrCobr2: TdmkEdit;
    EdInstrCobr2_TXT: TdmkEdit;
    dmkMemo1: TdmkMemo;
    EdInstrCobr1: TdmkEdit;
    EdInstrCobr1_TXT: TdmkEdit;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    Panel6: TPanel;
    Label28: TLabel;
    EdArq: TEdit;
    MeLines: TMemo;
    TabSheet9: TTabSheet;
    PageControl3: TPageControl;
    TabSheet10: TTabSheet;
    TabSheet11: TTabSheet;
    Panel11: TPanel;
    EdCodOculto: TdmkEdit;
    Label39: TLabel;
    Label40: TLabel;
    EdSeqArq: TdmkEdit;
    EdLastNosNum: TdmkEdit;
    Label15: TLabel;
    RGAceiteTit: TRadioGroup;
    RGCorreio: TRadioGroup;
    Panel13: TPanel;
    GroupBox9: TGroupBox;
    Panel9: TPanel;
    GroupBox5: TGroupBox;
    Label4: TLabel;
    EdCartNum: TdmkEdit;
    EdCartCod: TdmkEdit;
    EdCarteira_TXT: TEdit;
    MeCarteira: TdmkMemo;
    Panel12: TPanel;
    Label16: TLabel;
    EdLocalPag: TdmkEdit;
    Label25: TLabel;
    EdEspecieTit: TdmkEdit;
    EdEspecieTit_TXT: TdmkEdit;
    Label14: TLabel;
    EdEspecieVal: TdmkEdit;
    EdEspecieVal_TXT: TdmkEdit;
    QrCNAB_CfgLocalPag: TWideStringField;
    QrCNAB_CfgEspecieVal: TWideStringField;
    Label24: TLabel;
    EdOperCodi: TdmkEdit;
    QrCNAB_CfgOperCodi: TWideStringField;
    QrCNAB_CfgIDCobranca: TWideStringField;
    EdIDCobranca: TdmkEdit;
    Label41: TLabel;
    EdAgContaCed: TdmkEdit;
    Label42: TLabel;
    QrCNAB_CfgAgContaCed: TWideStringField;
    TabSheet1: TTabSheet;
    QrCNAB_CfgDirRetorno: TWideStringField;
    TabSheet2: TTabSheet;
    Panel14: TPanel;
    Panel16: TPanel;
    Label46: TLabel;
    EdDirRetorno: TdmkEdit;
    SpeedButton6: TSpeedButton;
    QrCNAB_CfgCartRetorno: TIntegerField;
    QrCNAB_CfgPosicoesBB: TSmallintField;
    QrCNAB_CfgIndicatBB: TWideStringField;
    QrCNAB_CfgTipoCobrBB: TWideStringField;
    QrCNAB_CfgComando: TIntegerField;
    QrCNAB_CfgDdProtesBB: TIntegerField;
    QrCNAB_CfgMsg40posBB: TWideStringField;
    EdCartRetorno: TdmkEditCB;
    CBCartRetorno: TdmkDBLookupComboBox;
    Label53: TLabel;
    QrCartRetorno: TmySQLQuery;
    DsCartRetorno: TDataSource;
    SpeedButton7: TSpeedButton;
    Panel17: TPanel;
    GroupBox2: TGroupBox;
    Label8: TLabel;
    Label31: TLabel;
    EdCedNome: TdmkEdit;
    EdCedente: TdmkEditCB;
    CBCedente: TdmkDBLookupComboBox;
    GroupBox6: TGroupBox;
    Label32: TLabel;
    Label33: TLabel;
    EdSacadAvali: TdmkEditCB;
    CBSacadAvali: TdmkDBLookupComboBox;
    EdSacAvaNome: TdmkEdit;
    Panel18: TPanel;
    GroupBox7: TGroupBox;
    EdTexto01: TEdit;
    EdTexto02: TEdit;
    EdTexto03: TEdit;
    EdTexto04: TEdit;
    EdTexto05: TEdit;
    EdTexto06: TEdit;
    EdTexto07: TEdit;
    EdTexto08: TEdit;
    EdTexto09: TEdit;
    EdTexto10: TEdit;
    CkEnvEmeio: TCheckBox;
    GBMulta: TGroupBox;
    Label35: TLabel;
    EdMultaPerc: TdmkEdit;
    GroupBox8: TGroupBox;
    Label36: TLabel;
    EdJurosPerc: TdmkEdit;
    EdJurosDias: TdmkEdit;
    QrCartRetornoCodigo: TIntegerField;
    QrCartRetornoNome: TWideStringField;
    Label54: TLabel;
    EdComando: TdmkEdit;
    EdComando_TXT: TdmkEdit;
    QrCNAB_CfgQuemDistrb: TWideStringField;
    QrCNAB_CfgQuemPrint: TWideStringField;
    Label59: TLabel;
    EdMultaTipo: TdmkEdit;
    EdMultaTipo_TXT: TdmkEdit;
    Label60: TLabel;
    EdJurosTipo: TdmkEdit;
    EdJurosTipo_TXT: TdmkEdit;
    Label61: TLabel;
    QrCNAB_CfgDesco1Cod: TSmallintField;
    QrCNAB_CfgDesco1Dds: TIntegerField;
    QrCNAB_CfgDesco1Fat: TFloatField;
    QrCNAB_CfgDesco2Cod: TSmallintField;
    QrCNAB_CfgDesco2Dds: TIntegerField;
    QrCNAB_CfgDesco2Fat: TFloatField;
    QrCNAB_CfgDesco3Cod: TSmallintField;
    QrCNAB_CfgDesco3Dds: TIntegerField;
    QrCNAB_CfgDesco3Fat: TFloatField;
    GroupBox14: TGroupBox;
    EdProtesCod: TdmkEdit;
    EdProtesCod_TXT: TEdit;
    Label71: TLabel;
    Label72: TLabel;
    EdProtesDds: TdmkEdit;
    GroupBox15: TGroupBox;
    Label74: TLabel;
    EdBxaDevCod: TdmkEdit;
    EdBxaDevCod_TXT: TEdit;
    EdBxaDevDds: TdmkEdit;
    Label73: TLabel;
    QrCNAB_CfgProtesCod: TSmallintField;
    QrCNAB_CfgProtesDds: TSmallintField;
    QrCNAB_CfgBxaDevCod: TSmallintField;
    Label75: TLabel;
    EdMoedaCod: TdmkEdit;
    EdMoedaCod_TXT: TEdit;
    QrCNAB_CfgMoedaCod: TWideStringField;
    Label76: TLabel;
    EdContrato: TdmkEdit;
    QrCNAB_CfgContrato: TFloatField;
    QrCNAB_CfgBxaDevDds: TIntegerField;
    Label77: TLabel;
    EdMultaDias: TdmkEdit;
    QrCNAB_CfgMultaDias: TSmallintField;
    PageControl_001: TPageControl;
    TabSheet13: TTabSheet;
    TabSheet14: TTabSheet;
    Panel22: TPanel;
    Label50: TLabel;
    EdTipoCobrBB: TdmkEdit;
    EdTipoCobrBB_TXT: TdmkEdit;
    Label52: TLabel;
    RGIndicatBB: TdmkRadioGroup;
    EdDdProtesBB: TdmkEdit;
    EdDdProtesBB_TXT: TdmkEdit;
    RGPosicoesBB: TdmkRadioGroup;
    EdMsg40posBB: TdmkEdit;
    Panel23: TPanel;
    GroupBox10: TGroupBox;
    PageControl_237: TPageControl;
    TabSheet12: TTabSheet;
    Panel10: TPanel;
    Label37: TLabel;
    Label38: TLabel;
    Ed_237Mens1: TdmkEdit;
    Ed_237Mens2: TdmkEdit;
    PageControl_341: TPageControl;
    TabSheet3: TTabSheet;
    Panel15: TPanel;
    GroupBox16: TGroupBox;
    Label78: TLabel;
    EdConvCartCobr: TdmkEdit;
    Label79: TLabel;
    EdConvVariCart: TdmkEdit;
    QrCNAB_CfgConvCartCobr: TWideStringField;
    QrCNAB_CfgConvVariCart: TWideStringField;
    QrCNAB_CfgCedente: TIntegerField;
    GroupBox17: TGroupBox;
    EdCodLidrBco: TdmkEdit;
    Label34: TLabel;
    EdCodEmprBco: TdmkEdit;
    Label80: TLabel;
    Panel19: TPanel;
    EdQuemPrint_TXT: TEdit;
    EdQuemPrint: TdmkEdit;
    Label58: TLabel;
    Label57: TLabel;
    EdQuemDistrb: TdmkEdit;
    EdQuemDistrb_TXT: TEdit;
    QrCNAB_CfgVariacao: TIntegerField;
    QrCNAB_CfgCodLidrBco: TWideStringField;
    Label55: TLabel;
    EdTipoCart: TdmkEdit;
    EdTipoCart_TXT: TEdit;
    Label47: TLabel;
    EdVariacao: TdmkEdit;
    EdVariacao_TXT: TEdit;
    EdEspecieDoc: TdmkEdit;
    QrCNAB_CfgEspecieDoc: TWideStringField;
    Label48: TLabel;
    Label49: TLabel;
    TabSheet15: TTabSheet;
    QrCartRemessa: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsCartRemessa: TDataSource;
    QrCNAB_CfgInfNossoNum: TSmallintField;
    QrCNAB_CfgCartEmiss: TIntegerField;
    QrCNAB_CfgTipoCobranca: TIntegerField;
    EdEspecieTit_TXT2: TdmkEdit;
    EdEspecieTxt: TdmkEdit;
    EdCartTxt: TdmkEdit;
    QrCNAB_CfgCartTxt: TWideStringField;
    QrCNAB_CfgEspecieTxt: TWideStringField;
    Label83: TLabel;
    EdLayoutRet: TdmkEdit;
    EdLayoutRet_TXT: TEdit;
    QrCNAB_CfgLayoutRem: TWideStringField;
    QrCNAB_CfgLayoutRet: TWideStringField;
    TabSheet16: TTabSheet;
    PageControl2: TPageControl;
    TabSheet17: TTabSheet;
    Panel20: TPanel;
    GroupBox18: TGroupBox;
    Label85: TLabel;
    EdNaoRecebDd: TdmkEdit;
    QrCNAB_CfgNaoRecebDd: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel24: TPanel;
    BtDesiste: TBitBtn;
    BtImportar: TBitBtn;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label56: TLabel;
    Label6: TLabel;
    Edit1: TdmkEdit;
    Edit2: TdmkEdit;
    Edit3: TdmkEdit;
    Panel1: TPanel;
    Label29: TLabel;
    EdDiretorio: TdmkEdit;
    Label81: TLabel;
    EdCartEmiss: TdmkEditCB;
    CBCartEmiss: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    SpeedButton8: TSpeedButton;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtCNAB_CfOR: TBitBtn;
    BtCNAB_Cfg: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Label86: TLabel;
    EdTipBloqUso: TdmkEdit;
    EdTipBloqUso_TXT: TdmkEdit;
    EdTipBloqUso_TXT2: TdmkEdit;
    QrCNAB_CfgTipBloqUso: TWideStringField;
    GroupBox11: TGroupBox;
    Label62: TLabel;
    Label63: TLabel;
    Label64: TLabel;
    EdDesco1Fat: TdmkEdit;
    EdDesco1Cod: TdmkEdit;
    EdDesco1Cod_TXT: TdmkEdit;
    EdDesco1Dds: TdmkEdit;
    GroupBox12: TGroupBox;
    Label65: TLabel;
    Label66: TLabel;
    Label67: TLabel;
    EdDesco2Fat: TdmkEdit;
    EdDesco2Cod: TdmkEdit;
    EdDesco2Cod_TXT: TdmkEdit;
    EdDesco2Dds: TdmkEdit;
    GroupBox13: TGroupBox;
    Label68: TLabel;
    Label69: TLabel;
    Label70: TLabel;
    EdDesco3Fat: TdmkEdit;
    EdDesco3Cod: TdmkEdit;
    EdDesco3Cod_TXT: TdmkEdit;
    EdDesco3Dds: TdmkEdit;
    Label82: TLabel;
    EdLayoutRem: TdmkEdit;
    EdLayoutRem_TXT1: TEdit;
    QrCNAB_CfgConcatCod: TWideStringField;
    QrCNAB_CfgComplmCod: TWideStringField;
    Label88: TLabel;
    EdNumVersaoI3: TdmkEdit;
    QrCNAB_CfgNumVersaoI3: TIntegerField;
    RGModalCobr: TRadioGroup;
    Label89: TLabel;
    EdCtaCooper: TdmkEdit;
    QrCNAB_CfgModalCobr: TSmallintField;
    QrCNAB_CfgCtaCooper: TWideStringField;
    Edit4: TdmkEdit;
    Label90: TLabel;
    EdNosNumFxaI: TdmkEdit;
    EdNosNumFxaF: TdmkEdit;
    Label92: TLabel;
    QrCNAB_CfgNosNumFxaI: TIntegerField;
    QrCNAB_CfgNosNumFxaF: TIntegerField;
    QrCNAB_CfgNosNumFxaU: TSmallintField;
    CkNosNumFxaU: TdmkCheckBox;
    TabSheet18: TTabSheet;
    PageControl4: TPageControl;
    TabSheet19: TTabSheet;
    Panel25: TPanel;
    dmkRadioGroup1: TdmkRadioGroup;
    GroupBox19: TGroupBox;
    Label91: TLabel;
    EdCtrGarantiaNr: TdmkEdit;
    EdCtrGarantiaDV: TdmkEdit;
    Label93: TLabel;
    QrCNAB_CfgCtrGarantiaNr: TWideStringField;
    QrCNAB_CfgCtrGarantiaDV: TWideStringField;
    GBAvisos1: TGroupBox;
    Panel21: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Label3: TLabel;
    EdCedBanco: TdmkEditCB;
    CBCedBanco: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdCedAgencia: TdmkEdit;
    Label11: TLabel;
    EdCedDAC_A: TdmkEdit;
    Label7: TLabel;
    EdCedConta: TdmkEdit;
    Label12: TLabel;
    EdCedDAC_C: TdmkEdit;
    Label13: TLabel;
    EdCedDAC_AC: TdmkEdit;
    Label51: TLabel;
    EdCedPosto: TdmkEdit;
    Label84: TLabel;
    EdConcatCod: TdmkEdit;
    SpeedButton9: TSpeedButton;
    Label87: TLabel;
    EdComplmCod: TdmkEdit;
    QrCNAB_CfgVTCBBNITAR: TFloatField;
    LaProtocolCR: TLabel;
    EdProtocolCR: TdmkEditCB;
    CBProtocolCR: TdmkDBLookupComboBox;
    SBProtocolCR: TSpeedButton;
    QrProtocolCR: TmySQLQuery;
    DsProtocolCR: TDataSource;
    QrProtocolCRCodigo: TIntegerField;
    QrProtocolCRNome: TWideStringField;
    QrCNAB_CfgProtocolCR: TIntegerField;
    BtDuplica: TBitBtn;
    QrCNAB_CfOR: TmySQLQuery;
    DsCNAB_CfOR: TDataSource;
    GroupBox1: TGroupBox;
    DBGCNAB_CfOR: TdmkDBGridZTO;
    PMCNAB_Cfg: TPopupMenu;
    IncluiConfiguracaoCNAB1: TMenuItem;
    AlteraConfiguracaoCNAB1: TMenuItem;
    ExcluiConfiguracaoCNAB1: TMenuItem;
    N1: TMenuItem;
    DuplicaConfiguracaoCNAB1: TMenuItem;
    PMCNAB_CfOR: TPopupMenu;
    IncluiTarifaRetornoCNAB1: TMenuItem;
    AlteraTarifaRetornoCNAB1: TMenuItem;
    ExcluiTarifaRetornoCNAB1: TMenuItem;
    N2: TMenuItem;
    CriaTarifasConhecidasDoLayout1: TMenuItem;
    Panel27: TPanel;
    Panel26: TPanel;
    Label94: TLabel;
    Label95: TLabel;
    Label96: TLabel;
    Label97: TLabel;
    Label98: TLabel;
    Label99: TLabel;
    Label100: TLabel;
    Label101: TLabel;
    Label102: TLabel;
    Label103: TLabel;
    Label104: TLabel;
    Label105: TLabel;
    RadioGroup1: TDBRadioGroup;
    EdLayoutRem_TXT2: TEdit;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit4: TDBEdit;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DsContas: TDataSource;
    Label1: TLabel;
    EdCtaRetTar: TdmkEditCB;
    CBCtaRetTar: TdmkDBLookupComboBox;
    SbGenero: TSpeedButton;
    QrCNAB_CfgCtaRetTar: TIntegerField;
    QrCNAB_CfORCodigo: TIntegerField;
    QrCNAB_CfORControle: TIntegerField;
    QrCNAB_CfORNome: TWideStringField;
    QrCNAB_CfOROcorrencia: TIntegerField;
    QrCNAB_CfORGenero: TIntegerField;
    QrCNAB_CfORLk: TIntegerField;
    QrCNAB_CfORDataCad: TDateField;
    QrCNAB_CfORDataAlt: TDateField;
    QrCNAB_CfORUserCad: TIntegerField;
    QrCNAB_CfORUserAlt: TIntegerField;
    QrCNAB_CfORAlterWeb: TSmallintField;
    QrCNAB_CfORAtivo: TSmallintField;
    QrCNAB_CfORNOMEGENERO: TWideStringField;
    PB1: TProgressBar;
    QrCNAB_CfgCorreio: TIntegerField;
    QrCNAB_CfgTermoAceite: TIntegerField;
    QrCNAB_CfgDataHoraAceite: TDateTimeField;
    QrCNAB_CfgUsuarioAceite: TIntegerField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    Label17: TLabel;
    DBEdit5: TDBEdit;
    Label18: TLabel;
    DBEdit6: TDBEdit;
    QrCNAB_CfgStatus_TXT: TWideStringField;
    N3: TMenuItem;
    Alterastatusdaconfiguraoatual1: TMenuItem;
    QrCNAB_CfgUsuarioAceite_TXT: TWideStringField;
    QrCNAB_CfgDataHoraAceite_TXT: TWideStringField;
    TabSheet20: TTabSheet;
    RGTipoCobranca: TRadioGroup;
    EdVTCBBNITAR: TdmkEdit;
    Label189: TLabel;
    Label187: TLabel;
    GroupBox3: TGroupBox;
    Label19: TLabel;
    EdCorresBco: TdmkEditCB;
    Label20: TLabel;
    EdCorresAge: TdmkEdit;
    QrCNAB_CfgCorresBco: TIntegerField;
    Label21: TLabel;
    EdCorresCto: TdmkEdit;
    QrCNAB_CfgCorresCto: TWideStringField;
    QrCNAB_CfgCorresAge: TIntegerField;
    QrCNAB_CfgNPrinBc: TWideStringField;
    Label22: TLabel;
    EdNPrinBc: TdmkEdit;
    QrSacadorDOCENT: TWideStringField;
    SpeedButton10: TSpeedButton;
    SpeedButton11: TSpeedButton;
    Timer1: TTimer;
    GroupBox4: TGroupBox;
    CkAtivo: TdmkCheckBox;
    DBCheckBox2: TDBCheckBox;
    LbVars: TListView;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtCNAB_CfgClick(Sender: TObject);
    procedure BtCNAB_CfORClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCNAB_CfgAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCNAB_CfgBeforeOpen(DataSet: TDataSet);
    procedure EdCedNomeExit(Sender: TObject);
    procedure EdSacAvaNomeExit(Sender: TObject);
    procedure EdCodLidrBcoExit(Sender: TObject);
    procedure EdCedBancoExit(Sender: TObject);
    procedure EdInstrCobr1Exit(Sender: TObject);
    procedure EdInstrCobr2Exit(Sender: TObject);
    procedure EdCedBancoChange(Sender: TObject);
    procedure EdEspecieTitChange(Sender: TObject);
    procedure EdInstrCobr1Change(Sender: TObject);
    procedure EdInstrCobr2Change(Sender: TObject);
    procedure BtImportarClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCartNumChange(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure RGCNABClick(Sender: TObject);
    procedure EdEspecieValChange(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure RGPosicoesBBClick(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure EdCedenteChange(Sender: TObject);
    procedure EdTipoCobrBBChange(Sender: TObject);
    procedure EdComandoChange(Sender: TObject);
    procedure EdDdProtesBBChange(Sender: TObject);
    procedure EdTipoCartChange(Sender: TObject);
    procedure EdQuemDistrbChange(Sender: TObject);
    procedure EdQuemPrintChange(Sender: TObject);
    procedure EdMultaTipoChange(Sender: TObject);
    procedure EdJurosTipoChange(Sender: TObject);
    procedure EdDesco1CodChange(Sender: TObject);
    procedure EdDesco2CodChange(Sender: TObject);
    procedure EdDesco3CodChange(Sender: TObject);
    procedure EdProtesCodChange(Sender: TObject);
    procedure EdBxaDevCodChange(Sender: TObject);
    procedure EdMoedaCodChange(Sender: TObject);
    procedure EdCodEmprBcoExit(Sender: TObject);
    procedure EdVariacaoChange(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure EdLayoutRemChange(Sender: TObject);
    procedure EdLayoutRetChange(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure EdTipBloqUsoChange(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure SBProtocolCRClick(Sender: TObject);
    procedure RGModalCobrClick(Sender: TObject);
    procedure QrCNAB_CfgBeforeClose(DataSet: TDataSet);
    procedure QrCNAB_CfgAfterScroll(DataSet: TDataSet);
    procedure IncluiConfiguracaoCNAB1Click(Sender: TObject);
    procedure AlteraConfiguracaoCNAB1Click(Sender: TObject);
    procedure DuplicaConfiguracaoCNAB1Click(Sender: TObject);
    procedure ExcluiConfiguracaoCNAB1Click(Sender: TObject);
    procedure BtDuplicaClick(Sender: TObject);
    procedure PMCNAB_CfgPopup(Sender: TObject);
    procedure IncluiTarifaRetornoCNAB1Click(Sender: TObject);
    procedure AlteraTarifaRetornoCNAB1Click(Sender: TObject);
    procedure ExcluiTarifaRetornoCNAB1Click(Sender: TObject);
    procedure CriaTarifasConhecidasDoLayout1Click(Sender: TObject);
    procedure DBEdit9Change(Sender: TObject);
    procedure PMCNAB_CfORPopup(Sender: TObject);
    procedure Alterastatusdaconfiguraoatual1Click(Sender: TObject);
    procedure EdSacAvaNomeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton10Click(Sender: TObject);
    procedure SpeedButton11Click(Sender: TObject);
    procedure EdSacadAvaliChange(Sender: TObject);
    procedure EdTexto01Enter(Sender: TObject);
    procedure EdTexto01Exit(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure SbGeneroClick(Sender: TObject);
    procedure LbVarsDblClick(Sender: TObject);
  private
    FEdCondInst: TdmkEdit;
    FCorSim, FCorTvz, FCorTxz, FCorNao, FCorAju, FCorTxt: TColor;
    FDdProtesto,
    FComando,
    FEspecieTit,
    FEspecieVal,
    FTipoCart,
    FInstruCobr,
    FTipoCobrBB,
    FAvisosInstruCobr,
    FCarteiras,
    FAvisosCarteiras,
    //FCartCodBB,
    //FCadTitBco,
    FQuemDistrb,
    FQuemPrint,
    FMultaTipo,
    FJurosTipo,
    FDescontos,
    FProtesCod,
    FBxaDevCod,
    FMoedaCod,
    FVariacao,
    FLayoutRem1, FLayoutRem2,
    FLayoutRet,
    FTipBloqUso: MyArrayLista;
    //
    FListaArq: TStrings;

    function  TemInconsistencias(): Boolean;
    function  ValidaCedenteSacadAvali(Codigo, ModalCobr, CedBanco,
              CedAgencia: Integer; CedConta: String; Cedente,
              SacadAvali: Integer; AgContaCed: String; var Msg: String): Boolean;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure MostraCNAB_CfOR(SQLType: TSQLType);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure LiberaTextoLinha1;
    procedure RedefineInstrucoes();
    procedure ReconfiguraCoresEFormatacoes();
    procedure ReconfiguraTudo();
    procedure ReopenCartRetorno(Cedente, SacadAvali: Integer);
    procedure ReopenCartRemessa(Cedente, SacadAvali: Integer);
    procedure ReopenProtocolCR(Cedente, SacadAvali: Integer);
    procedure ReopenCNAB_CfOR(Controle: Integer);
    procedure TextoTipoCobrBB();
    procedure ConfiguraProtocoloCR();
    procedure AlteraStatusConfiguracao(Status: Integer);
    procedure DuplicaCfgAtual();
    procedure AtualizaLastNosNum(ModalCobr, CedBanco, CedAgencia: Integer;
              CedConta: String; LastNosNum: Double);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmCNAB_Cfg: TFmCNAB_Cfg;

const
  FFormatFloat = '00000';
  _Ini = 0; _Fim = 1; _Tam = 2; _Fil = 3; _Typ = 4; _Fmt = 5;
  _Fld = 6; _Def = 7; _Nom = 8; _Des = 9; _AjT = 10;

implementation

uses
{$IfNDef SemProtocolo}Protocolos, {$EndIf}
{$IfNDef NO_FINANCEIRO} UnFinanceiroJan, {$EndIf}
  UnMyObjects, Module, Principal, UnBancos, MyDBCheck, UnBco_Rem, ModuleBco,
  ModuleGeral, CNAB_CfOR, UnWAceites, UnitDmkTags;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCNAB_Cfg.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCNAB_Cfg.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCNAB_CfgCodigo.Value, LaRegistro.Caption[2]);
end;

function TFmCNAB_Cfg.ValidaCedenteSacadAvali(Codigo, ModalCobr, CedBanco,
  CedAgencia: Integer; CedConta: String; Cedente, SacadAvali: Integer;
  AgContaCed: String; var Msg: String): Boolean;
var
  Ced, Sac: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Cedente, SacadAvali ',
    'FROM cnab_cfg ',
    'WHERE Codigo <> ' + Geral.FF0(Codigo),
    'AND Cedente <> ' + Geral.FF0(Cedente),
    'AND SacadAvali <> ' + Geral.FF0(SacadAvali),
    'AND CedBanco=' + Geral.FF0(CedBanco),
    'AND CedAgencia=' + Geral.FF0(CedAgencia),
    'AND CedConta=' + CedConta,
    'AND ModalCobr=' + Geral.FF0(ModalCobr),
    'AND AgContaCed="' + AgContaCed + '"',
    'GROUP BY Cedente, SacadAvali ',
    '']);
  if Dmod.QrAux.RecordCount > 0 then
  begin
    Ced := Dmod.QrAux.FieldByName('Cedente').AsInteger;
    Sac := Dmod.QrAux.FieldByName('SacadAvali').AsInteger;
    Msg := 'Cedente / Sacador avalista inv�lidos para os seguintes dados: ' +
           sLineBreak + 'Banco: ' + Geral.FF0(CedBanco) + sLineBreak +
           'Ag�ncia: ' + Geral.FF0(CedAgencia) + sLineBreak + 'Conta: ' + CedConta +
           sLineBreak + sLineBreak + 'Para os dados listados acima voc� dever� preencher:' +
           sLineBreak + 'Cedente: ' + Geral.FF0(Ced) + sLineBreak +
           'Sacador Avalista: ' + Geral.FF0(Sac);
    Result := False;
  end else
    Result := True;
  //
  Dmod.QrAux.Close;
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCNAB_Cfg.DefParams;
begin
  VAR_GOTOTABELA := 'CNAB_Cfg';
  VAR_GOTOMYSQLTABLE := QrCNAB_Cfg;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;
  //
  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT ban.Nome NOMEBANCO, sen.Login UsuarioAceite_TXT, ');
  VAR_SQLx.Add('IF(cfg.TermoAceite <> 0, "Homologado", "Em homologa��o") Status_TXT, ');
  VAR_SQLx.Add('DATE_FORMAT(cfg.DataHoraAceite, "%d/%m/%Y %H:%i:%s") DataHoraAceite_TXT, cfg.*');
  VAR_SQLx.Add('FROM cnab_cfg cfg');
  VAR_SQLx.Add('LEFT JOIN bancos ban ON ban.Codigo=cfg.CedBanco');
  VAR_SQLx.Add('LEFT JOIN senhas sen ON sen.Numero=cfg.UsuarioAceite');
  VAR_SQLx.Add('WHERE cfg.Codigo <> 0');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND cfg.Codigo=:P0');
  //
  VAR_SQLa.Add('AND cfg.Nome Like :P0');
  //
end;

procedure TFmCNAB_Cfg.DuplicaCfgAtual;
var
  Codigo, Controle: Integer;
  Nome: String;
begin
  if (QrCNAB_Cfg.State = dsInactive) or (QrCNAB_Cfg.RecordCount = 0) then
    Exit;
  //
  if Geral.MB_Pergunta('Confirma a duplica��o da configura��o atual?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      QrCNAB_Cfg.DisableControls;
      QrCNAB_CfOR.DisableControls;
      //
      Codigo := UMyMod.BuscaEmLivreY_Def('cnab_cfg', 'Codigo', stIns, 0);
      Nome   := 'C�pia - ' + QrCNAB_CfgNome.Value;
      //
      if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'cnab_cfg', TMeuDB,
        ['Codigo'], [QrCNAB_CfgCodigo.Value],
        ['Codigo', 'Nome', 'TermoAceite', 'DataHoraAceite', 'UsuarioAceite'],
        [Codigo, Nome, 0, '0000-00-00 00:00:00', 0],
        '', True, LaAviso1, LaAviso2) then
      begin
        //Duplica as tarifas
        QrCNAB_CfOR.First;
        while not QrCNAB_CfOR.Eof do
        begin
          Controle := UMyMod.BPGS1I32('cnab_cfor', 'Controle', '', '', tsPos, stIns, 0);
          //
          if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'cnab_cfor', TMeuDB,
            ['Controle'], [QrCNAB_CfORControle.Value],
            ['Codigo', 'Controle'], [Codigo, Controle],
            '', True, LaAviso1, LaAviso2) then
          //
          QrCNAB_CfOR.Next;
        end;
        MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      end;
    finally
      QrCNAB_CfOR.EnableControls;
      QrCNAB_Cfg.EnableControls;
      //
      Va(vpLast);
      Screen.Cursor := crDefault;
      //
      Geral.MB_Aviso('Duplica��o finalizada!');
    end;
  end;
end;

procedure TFmCNAB_Cfg.DuplicaConfiguracaoCNAB1Click(Sender: TObject);
begin
  DuplicaCfgAtual();
end;

procedure TFmCNAB_Cfg.MostraCNAB_CfOR(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmCNAB_CfOR, FmCNAB_CfOR, afmoNegarComAviso) then
  begin
    FmCNAB_CfOR.ImgTipo.SQLType := SQLType;
    FmCNAB_CfOR.FQrCab          := QrCNAB_Cfg;
    FmCNAB_CfOR.FDsCab          := DsCNAB_Cfg;
    FmCNAB_CfOR.FQrIts          := QrCNAB_CfOR;
    FmCNAB_CfOR.FTamReg         := Geral.IMV(RGCNAB.Items[RGCNAB.ItemIndex]);
    FmCNAB_CfOR.FBanco          := QrCNAB_CfgCedBanco.Value;
    FmCNAB_CfOR.FRegistrado     := RGModalCobr.ItemIndex = 1;

    if SQLType = stIns then
    begin
      FmCNAB_CfOR.EdOcorrencia.Enabled := True;
      //
      FmCNAB_CfOR.CkContinuar.Visible := True;
      FmCNAB_CfOR.CkContinuar.Checked := False;
    end else
    begin
      FmCNAB_CfOR.EdOcorrencia.Enabled := False;
      //
      FmCNAB_CfOR.EdControle.ValueVariant   := QrCNAB_CfORControle.Value;
      //
      FmCNAB_CfOR.EdNome.Text               := QrCNAB_CfORNome.Value;
      FmCNAB_CfOR.EdOcorrencia.ValueVariant := QrCNAB_CfOROcorrencia.Value;
      FmCNAB_CfOR.EdGenero.ValueVariant     := QrCNAB_CfORGenero.Value;
      FmCNAB_CfOR.CBGenero.KeyValue         := QrCNAB_CfORGenero.Value;
      //
      FmCNAB_CfOR.EdNomeOcorrencia.Text     := UBancos.CNABTipoDeMovimento(
                                                 QrCNAB_CfgCedBanco.Value, ecnabRetorno,
                                                 QrCNAB_CfOROcorrencia.Value,
                                                 Geral.IMV(RGCNAB.Items[RGCNAB.ItemIndex]),
                                                 RGModalCobr.ItemIndex = 1,
                                                 QrCNAB_Cfg.FieldByName('LayoutRem').Value);
      //
      FmCNAB_CfOR.CkContinuar.Visible := False;
      FmCNAB_CfOR.CkContinuar.Checked := False;
    end;
    FmCNAB_CfOR.ShowModal;
    FmCNAB_CfOR.Destroy;
  end;
end;

procedure TFmCNAB_Cfg.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
var
  Cedente, SacadAvali: Integer;
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible     := True;
      PainelDados.Visible     := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text                := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text                  := '';
        EdCedBanco.ValueVariant      := 0;
        CBCedBanco.KeyValue          := Null;
        EdCedAgencia.Text            := '';
        EdCedConta.Text              := '';
        EdCedDAC_A.Text              := '';
        EdCedDAC_C.Text              := '';
        EdCedDAC_AC.Text             := '';
        EdCedPosto.ValueVariant      := 0;
        EdCedNome.ValueVariant       := '';
        EdTipoCart.ValueVariant      := 0;
        EdNosNumFxaI.ValueVariant    := 0;
        EdNosNumFxaI.ValueVariant    := 0;
        CkNosNumFxaU.Checked         := False;
        RGAceiteTit.ItemIndex        := 0;
        RGCorreio.ItemIndex          := 0;
        EdCartCod.Text               := '';
        EdCartNum.Text               := '';
        EdCartTxt.Text               := '';
        EdEspecieTit.Text            := '';
        EdEspecieTxt.Text            := '';
        EdEspecieDoc.Text            := '';
        EdInstrCobr1.ValueVariant    := '';
        EdInstrCobr2.ValueVariant    := '';
        EdInstrDias.ValueVariant     := 0;
        EdSacAvaNome.Text            := '';
        EdCedente.ValueVariant       := 0;
        CBCedente.KeyValue           := Null;
        EdSacadAvali.ValueVariant    := 0;
        CBSacadAvali.KeyValue        := Null;
        EdCodEmprBco.Text            := '';
        EdCodLidrBco.Text            := '';
        EdMultaTipo.ValueVariant     := 0;
        EdJurosTipo.ValueVariant     := 0;
        EdJurosPerc.Text             := '1,00';
        EdJurosDias.Text             := '0';
        EdMultaPerc.Text             := '2,00';
        EdMultaDias.Text             := '0';
        EdTexto01.Text               := '';
        EdTexto02.Text               := '';
        EdTexto03.Text               := '';
        EdTexto04.Text               := '';
        EdTexto05.Text               := '';
        EdTexto06.Text               := '';
        EdTexto07.Text               := '';
        EdTexto08.Text               := '';
        EdTexto09.Text               := '';
        EdTexto10.Text               := '';
        RGCNAB.ItemIndex             := 0;
        CkEnvEmeio.Checked           := False;
        EdDiretorio.Text             := '';
        Ed_237Mens1.Text             := '';
        Ed_237Mens2.Text             := '';
        EdCodOculto.Text             := '';
        EdSeqArq.ValueVariant        := 0;
        EdLastNosNum.ValueVariant    := 0;
        EdLocalPag.Text              := '';
        EdEspecieVal.Text            := '';
        EdOperCodi.Text              := '000';
        EdIDCobranca.Text            := '00';
        EdAgContaCed.Text            := '';
        EdDirRetorno.ValueVariant    := '';
        RGPosicoesBB.ItemIndex       := 6;
        RGIndicatBB.ItemIndex        := 0;
        EdTipoCobrBB.Text            := '';
        EdConvCartCobr.ValueVariant  := '';
        EdConvVariCart.ValueVariant  := '';
        EdComando.ValueVariant       := 0;
        EdDdProtesBB.ValueVariant    := 0;
        EdMsg40posBB.Text            := '';
        EdCartRetorno.ValueVariant   := 0;
        CBCartRetorno.KeyValue       := 0;
        {
        EdCartCodBB.ValueVariant     := '';
        EdCadTitBco.ValueVariant     := '';
        EdConvLider.ValueVariant     := 0;
        EdConvCobr.ValueVariant      := 0;
        EdVariaBB.ValueVariant       := 0;
        }
        EdQuemDistrb.ValueVariant    := '';
        EdQuemPrint.ValueVariant     := '';
        EdDesco1Cod.ValueVariant     := 0;
        EdDesco1Fat.ValueVariant     := 0;
        EdDesco1Dds.ValueVariant     := 0;
        EdDesco2Cod.ValueVariant     := 0;
        EdDesco2Fat.ValueVariant     := 0;
        EdDesco2Dds.ValueVariant     := 0;
        EdDesco3Cod.ValueVariant     := 0;
        EdDesco3Fat.ValueVariant     := 0;
        EdDesco3Dds.ValueVariant     := 0;
        EdProtesCod.ValueVariant     := 0;
        EdProtesDds.ValueVariant     := 0;
        EdNaoRecebDd.ValueVariant    := 0;
        EdBxaDevCod.ValueVariant     := 0;
        EdBxaDevDds.ValueVariant     := 0;
        EdMoedaCod.ValueVariant      := 0;
        EdContrato.ValueVariant      := 0;
        EdVariacao.ValueVariant      := 0;
        EdCartEmiss.ValueVariant     := 0;
        CBCartEmiss.KeyValue         := 0;

        EdLayoutRem.Text             := '';
        EdLayoutRet.Text             := '';
        EdConcatCod.Text             := '';
        EdComplmCod.Text             := '';
        EdTipBloqUso.Text            := '';
        EdNumVersaoI3.ValueVariant   := 0;
        EdCtaCooper.Text             := '';
        RGModalCobr.ItemIndex        := 0;

        EdCtrGarantiaNr.Text         := '00000';
        EdCtrGarantiaDV.Text         := '0';

        EdVTCBBNITAR.ValueVariant    := 0;
        EdCorresBco.ValueVariant     := 0;
        EdCorresAge.ValueVariant     := 0;
        EdCorresCto.ValueVariant     := '';
        EdNPrinBc.ValueVariant       := '';

        EdProtocolCR.ValueVariant    := 0;
        CBProtocolCR.KeyValue        := Null;

        EdCtaRetTar.ValueVariant     := 0;
        CBCtaRetTar.KeyValue         := 0;

        CkAtivo.Checked              := True;
      end else begin
        ReopenCartRetorno(Cedente, SacadAvali);
        ReopenCartRemessa(Cedente, SacadAvali);

        EdCodigo.Text                := DBEdCodigo.Text;
        EdNome.Text                  := DBEdNome.Text;
        EdCedBanco.ValueVariant      := QrCNAB_CfgCedBanco.Value;
        CBCedBanco.KeyValue          := QrCNAB_CfgCedBanco.Value;
        EdCedAgencia.ValueVariant    := QrCNAB_CfgCedAgencia.Value;
        EdCedConta.ValueVariant      := QrCNAB_CfgCedConta.Value;
        EdCedDAC_A.ValueVariant      := QrCNAB_CfgCedDAC_A.Value;
        EdCedDAC_C.ValueVariant      := QrCNAB_CfgCedDAC_C.Value;
        EdCedDAC_AC.ValueVariant     := QrCNAB_CfgCedDAC_AC.Value;
        EdCedPosto.ValueVariant      := QrCNAB_CfgCedPosto.Value;
        EdCedNome.ValueVariant       := QrCNAB_CfgCedNome.Value;
        EdTipoCart.ValueVariant      := QrCNAB_CfgTipoCart.Value;
        EdNosNumFxaI.ValueVariant    := QrCNAB_CfgNosNumFxaI.Value;
        EdNosNumFxaF.ValueVariant    := QrCNAB_CfgNosNumFxaF.Value;
        CkNosNumFxaU.Checked         := Geral.IntToBool(QrCNAB_CfgNosNumFxaU.Value);
        RGAceiteTit.ItemIndex        := QrCNAB_CfgAceiteTit.Value;
        RGCorreio.ItemIndex          := QrCNAB_CfgCorreio.Value;
        EdCartNum.Text               := QrCNAB_CfgCartNum.Value;
        EdCartCod.Text               := QrCNAB_CfgCartCod.Value;
        EdCartTxt.Text               := QrCNAB_CfgCartTxt.Value;
        EdEspecieTit.Text            := QrCNAB_CfgEspecieTit.Value;
        EdEspecieTxt.Text            := QrCNAB_CfgEspecieTxt.Value;
        EdEspecieDoc.Text            := QrCNAB_CfgEspecieDoc.Value;
        EdInstrCobr1.ValueVariant    := QrCNAB_CfgInstrCobr1.Value;
        EdInstrCobr2.ValueVariant    := QrCNAB_CfgInstrCobr2.Value;
        EdInstrDias.ValueVariant     := QrCNAB_CfgInstrDias.Value;
        EdSacAvaNome.Text            := QrCNAB_CfgSacAvaNome.Value;
        EdCedente.ValueVariant       := QrCNAB_CfgCedente.Value;
        CBCedente.KeyValue           := QrCNAB_CfgCedente.Value;
        EdSacadAvali.ValueVariant    := QrCNAB_CfgSacadAvali.Value;
        CBSacadAvali.KeyValue        := QrCNAB_CfgSacadAvali.Value;
        EdCodEmprBco.Text            := QrCNAB_CfgCodEmprBco.Value;
        EdCodLidrBco.Text            := QrCNAB_CfgCodLidrBco.Value;
        EdJurosPerc.ValueVariant     := QrCNAB_CfgJurosPerc.Value;
        EdJurosDias.ValueVariant     := QrCNAB_CfgJurosDias.Value;
        EdMultaTipo.ValueVariant     := QrCNAB_CfgMultaTipo.Value;
        EdMultaDias.ValueVariant     := QrCNAB_CfgMultaDias.Value;
        EdJurosTipo.ValueVariant     := QrCNAB_CfgJurosTipo.Value;
        EdMultaPerc.ValueVariant     := QrCNAB_CfgMultaPerc.Value;
        EdTexto01.Text               := QrCNAB_CfgTexto01.Value;
        EdTexto02.Text               := QrCNAB_CfgTexto02.Value;
        EdTexto03.Text               := QrCNAB_CfgTexto03.Value;
        EdTexto04.Text               := QrCNAB_CfgTexto04.Value;
        EdTexto05.Text               := QrCNAB_CfgTexto05.Value;
        EdTexto06.Text               := QrCNAB_CfgTexto06.Value;
        EdTexto07.Text               := QrCNAB_CfgTexto07.Value;
        EdTexto08.Text               := QrCNAB_CfgTexto08.Value;
        EdTexto09.Text               := QrCNAB_CfgTexto09.Value;
        EdTexto10.Text               := QrCNAB_CfgTexto10.Value;
        CkEnvEmeio.Checked           := Geral.IntToBool_0(QrCNAB_CfgEnvEmeio.Value);
        EdDiretorio.Text             := QrCNAB_CfgDiretorio.Value;
        Ed_237Mens1.Text             := QrCNAB_Cfg_237Mens1.Value;
        Ed_237Mens2.Text             := QrCNAB_Cfg_237Mens2.Value;
        EdCodOculto.Text             := QrCNAB_CfgCodOculto.Value;
        EdSeqArq.ValueVariant        := QrCNAB_CfgSeqArq.Value;
        EdLastNosNum.ValueVariant    := QrCNAB_CfgLastNosNum.Value;
        EdLocalPag.Text              := QrCNAB_CfgLocalPag.Value;
        EdEspecieVal.Text            := QrCNAB_CfgEspecieVal.Value;
        EdOperCodi.ValueVariant      := Geral.IMV(QrCNAB_CfgOperCodi.Value);
        EdIDCobranca.ValueVariant    := Geral.IMV(QrCNAB_CfgIDCobranca.Value);
        EdAgContaCed.Text            := QrCNAB_CfgAgContaCed.Value;
        EdDirRetorno.ValueVariant    := QrCNAB_CfgDirRetorno.Value;
        RGPosicoesBB.ItemIndex       := QrCNAB_CfgPosicoesBB.Value;
        RGIndicatBB.ItemIndex        := dmkPF.EscolhaDe2Int(QrCNAB_CfgIndicatBB.Value  = 'A', 1, 0);
        EdTipoCobrBB.Text            := QrCNAB_CfgTipoCobrBB.Value;
        EdConvCartCobr.Text          := QrCNAB_CfgConvCartCobr.Value;
        EdConvVariCart.Text          := QrCNAB_CfgConvVariCart.Value;
        EdComando.ValueVariant       := QrCNAB_CfgComando.Value;
        EdDdProtesBB.ValueVariant    := QrCNAB_CfgDdProtesBB.Value;
        EdMsg40posBB.Text            := QrCNAB_CfgMsg40posBB.Value;
        EdCartRetorno.ValueVariant   := QrCNAB_CfgCartRetorno.Value;
        CBCartRetorno.KeyValue       := QrCNAB_CfgCartRetorno.Value;
        {
        EdCartCodBB.ValueVariant     := QrCNAB_CfgCartCodBB.Value;
        EdCadTitBco.ValueVariant     := QrCNAB_CfgCadTitBco.Value;
        EdConvLider.ValueVariant     := QrCNAB_CfgConvLider.Value;
        EdConvCobr.ValueVariant      := QrCNAB_CfgConvCobr.Value;
        EdVariaBB.ValueVariant       := QrCNAB_CfgVariaBB.Value;
        }
        EdQuemDistrb.ValueVariant    := QrCNAB_CfgQuemDistrb.Value;
        EdQuemPrint.ValueVariant     := QrCNAB_CfgQuemPrint.Value;
        EdDesco1Cod.ValueVariant     := QrCNAB_CfgDesco1Cod.Value;
        EdDesco1Fat.ValueVariant     := QrCNAB_CfgDesco1Fat.Value;
        EdDesco1Dds.ValueVariant     := QrCNAB_CfgDesco1Dds.Value;
        EdDesco2Cod.ValueVariant     := QrCNAB_CfgDesco2Cod.Value;
        EdDesco2Fat.ValueVariant     := QrCNAB_CfgDesco2Fat.Value;
        EdDesco2Dds.ValueVariant     := QrCNAB_CfgDesco2Dds.Value;
        EdDesco3Cod.ValueVariant     := QrCNAB_CfgDesco3Cod.Value;
        EdDesco3Fat.ValueVariant     := QrCNAB_CfgDesco3Fat.Value;
        EdDesco3Dds.ValueVariant     := QrCNAB_CfgDesco3Dds.Value;
        EdProtesCod.ValueVariant     := QrCNAB_CfgProtesCod.Value;
        EdProtesDds.ValueVariant     := QrCNAB_CfgProtesDds.Value;
        EdNaoRecebDd.ValueVariant    := QrCNAB_CfgNaoRecebDd.Value;
        EdBxaDevCod.ValueVariant     := QrCNAB_CfgBxaDevCod.Value;
        EdBxaDevDds.ValueVariant     := QrCNAB_CfgBxaDevDds.Value;
        EdMoedaCod.ValueVariant      := QrCNAB_CfgMoedaCod.Value;
        EdContrato.ValueVariant      := QrCNAB_CfgContrato.Value;
        EdVariacao.ValueVariant      := QrCNAB_CfgVariacao.Value;
        EdCartEmiss.ValueVariant     := QrCNAB_CfgCartEmiss.Value;
        CBCartEmiss.KeyValue         := QrCNAB_CfgCartEmiss.Value;

        EdLayoutRem.Text             := QrCNAB_CfgLayoutRem.Value;
        EdLayoutRet.Text             := QrCNAB_CfgLayoutRet.Value;
        EdConcatCod.Text             := QrCNAB_CfgConcatCod.Value;
        EdComplmCod.Text             := QrCNAB_CfgComplmCod.Value;
        EdTipBloqUso.Text            := QrCNAB_CfgTipBloqUso.Value;
        RGModalCobr.ItemIndex        := QrCNAB_CfgModalCobr.Value;
        EdNumVersaoI3.ValueVariant   := QrCNAB_CfgNumVersaoI3.Value;
        EdCtaCooper.Text             := QrCNAB_CfgCtaCooper.Value;

        MyObjects.SetaRGCNAB(RGCNAB, QrCNAB_CfgCNAB.Value);

        EdCtrGarantiaNr.Text         := QrCNAB_CfgCtrGarantiaNr.Value;
        EdCtrGarantiaDV.Text         := QrCNAB_CfgCtrGarantiaDV.Value;

        EdVTCBBNITAR.ValueVariant    := QrCNAB_CfgVTCBBNITAR.Value;
        EdCorresBco.ValueVariant     := QrCNAB_CfgCorresBco.Value;
        EdCorresAge.ValueVariant     := QrCNAB_CfgCorresAge.Value;
        EdCorresCto.ValueVariant     := QrCNAB_CfgCorresCto.Value;
        EdNPrinBc.ValueVariant       := QrCNAB_CfgNPrinBc.Value;

        EdCtaRetTar.ValueVariant     := QrCNAB_CfgCtaRetTar.Value;
        CBCtaRetTar.KeyValue         := QrCNAB_CfgCtaRetTar.Value;

        CkAtivo.Checked              := Geral.IntToBool(QrCNAB_CfgAtivo.Value);

        ConfiguraProtocoloCR;

        Cedente    := EdCedente.ValueVariant;
        SacadAvali := EdSacadAvali.ValueVariant;

        EdProtocolCR.ValueVariant    := QrCNAB_CfgProtocolCR.Value;
        CBProtocolCR.KeyValue        := QrCNAB_CfgProtocolCR.Value;
      end;
      //
      EdNome.SetFocus;
      ReconfiguraCoresEFormatacoes();
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCNAB_Cfg.PMCNAB_CfgPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(AlteraConfiguracaoCNAB1, QrCNAB_Cfg);
  MyObjects.HabilitaMenuItemCabUpd(DuplicaConfiguracaoCNAB1, QrCNAB_Cfg);
  //MyObjects.HabilitaMenuItemCabDel(ExcluiConfiguracaoCNAB1, QrCNAB_Cfg);
  MyObjects.HabilitaMenuItemCabUpd(DuplicaConfiguracaoCNAB1, QrCNAB_Cfg);
  MyObjects.HabilitaMenuItemCabUpd(Alterastatusdaconfiguraoatual1, QrCNAB_Cfg);
  //
  if Alterastatusdaconfiguraoatual1.Enabled then
  begin
    if QrCNAB_CfgTermoAceite.Value <> 0 then
      Alterastatusdaconfiguraoatual1.Caption := 'Altera status da configura��o atual para: Em homologa��o'
    else
      Alterastatusdaconfiguraoatual1.Caption := 'Altera status da configura��o atual para: Homologado';
  end else
    Alterastatusdaconfiguraoatual1.Caption := 'Altera status da configura��o atual para: Homologado';
end;

procedure TFmCNAB_Cfg.PMCNAB_CfORPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrCNAB_Cfg.State <> dsInactive) and (QrCNAB_Cfg.RecordCount > 0);
  Enab2 := (QrCNAB_CfOR.State <> dsInactive) and (QrCNAB_CfOR.RecordCount > 0);
  //
  IncluiTarifaRetornoCNAB1.Enabled       := Enab;
  AlteraTarifaRetornoCNAB1.Enabled       := Enab and Enab2;
  ExcluiTarifaRetornoCNAB1.Enabled       := Enab and Enab2;
  CriaTarifasConhecidasDoLayout1.Enabled := Enab;
end;

procedure TFmCNAB_Cfg.ConfiguraProtocoloCR;
var
  Cedente, SacadAvali: Integer;
  Enab: Boolean;
begin
  {$IfNDef SemProtocolo}
    Enab := RGModalCobr.ItemIndex = 1; //Com registro
  {$Else}
    Enab := False; 
  {$EndIf}
  //
  LaProtocolCR.Visible := Enab;
  EdProtocolCR.Visible := Enab;
  CBProtocolCR.Visible := Enab;
  SBProtocolCR.Visible := Enab;
  //
  Cedente    := EdCedente.ValueVariant;
  SacadAvali := EdSacadAvali.ValueVariant;
  //
  if Enab then
    ReopenProtocolCR(Cedente, SacadAvali)
  else
    QrProtocolCR.Close;
end;

procedure TFmCNAB_Cfg.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCNAB_Cfg.CriaTarifasConhecidasDoLayout1Click(Sender: TObject);
const
  Genero = 0;
var
  I, Banco, TamReg, Codigo, Controle: Integer;
  Registrado: Boolean;
  Nome, TmpTxt, NomeLayout: String;
  Qry: TmySQLQuery;
begin
  if Geral.MB_Pergunta('As tarifas aqui cadastradas s� ter�o funcionalidade ' +
  'ap�s serem alteradas e terem sua conta do plano de contas informada!' +
  sLineBreak + 'Deseja continuar assim mesmo?') <> ID_YES then
    Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    Codigo     := QrCNAB_CfgCodigo.Value;
    Banco      := QrCNAB_CfgCedBanco.Value;
    TamReg     := QrCNAB_CfgCNAB.Value;
    Registrado := QrCNAB_CfgModalCobr.Value = 1;
    NomeLayout := QrCNAB_CfgLayoutRem.Value;
    //
    Qry := TmySQLQuery.Create(Dmod);
    try
      PB1.Max := CO_CNAB_HIGH_COD_OCORRENCIA;
      PB1.Position := 0;
      TmpTxt := LaAviso1.Caption;
      for I := 1 to CO_CNAB_HIGH_COD_OCORRENCIA do
      begin
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
          'Verificando ocorr�ncia n�mero ' + Geral.FF0(I));
        Nome := Trim(UBancos.CNABTipoDeMovimento(Banco, ecnabRetorno, I, TamReg,
                  Registrado, NomeLayout));
        //
        if (Nome <> '') and (Nome[1] <> '*') then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT Controle ',
          'FROM cnab_cfor ',
          'WHERE Codigo=' + Geral.FF0(Codigo),
          'AND Ocorrencia=' + Geral.FF0(I),
          '']);
          if Qry.RecordCount = 0 then
          begin
            Controle := UMyMod.BPGS1I32('cnab_cfor', 'Controle', '', '', tsPos,
              stIns, Controle);
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'cnab_cfor', False, [
            'Codigo', 'Nome', 'Ocorrencia',
            'Genero'], [
            'Controle'], [
            Codigo, Nome, (*Ocorrencia*)I,
            Genero], [
            Controle], True) then
          end;
        end;
      end;
    finally
      Qry.Free;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  ReopenCNAB_CfOR(0);
  PB1.POsition := 0;
end;

procedure TFmCNAB_Cfg.AlteraConfiguracaoCNAB1Click(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmCNAB_Cfg.AlteraRegistro;
var
  CNAB_Cfg : Integer;
begin
  CNAB_Cfg := QrCNAB_CfgCodigo.Value;
  if QrCNAB_CfgCodigo.Value = 0 then
  begin
    //BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(CNAB_Cfg, Dmod.MyDB, 'CNAB_Cfg', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(CNAB_Cfg, Dmod.MyDB, 'CNAB_Cfg', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmCNAB_Cfg.AlteraStatusConfiguracao(Status: Integer);
var
  Codigo, Usuario, Termo: Integer;
  DtaHora: TDateTime;
  DataHora: String;
begin
  //0 = Em homologa��o
  //1 = Homologado
  //
  if (QrCNAB_Cfg.State <> dsInactive) and (QrCNAB_Cfg.RecordCount > 0) then
  begin
    if Status = 0 then
    begin
      Termo    := 0;
      Usuario  := 0;
      DataHora := '0000-00-00 00:00:00';
    end else
    begin
      Termo    := QrCNAB_CfgTermoAceite.Value;
      Usuario  := QrCNAB_CfgUsuarioAceite.Value;
      DataHora := Geral.FDT(QrCNAB_CfgDataHoraAceite.Value, 109);
      //
      if Termo = 0 then
      begin
        if not UWAceites.AceitaTermos(istCfgBoleto, Termo, Usuario, DtaHora) then
        begin
          Geral.MB_Aviso('A��o abortada!');
          Exit;
        end else
          DataHora := Geral.FDT(DtaHora, 109);
      end;
    end;
    Codigo := QrCNAB_CfgCodigo.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cnab_cfg', False,
      ['TermoAceite', 'DataHoraAceite', 'UsuarioAceite'], ['Codigo'],
      [Termo, DataHora, Usuario], [Codigo], True)
    then
      LocCod(Codigo, Codigo);
  end;
end;

procedure TFmCNAB_Cfg.Alterastatusdaconfiguraoatual1Click(Sender: TObject);
var
  Status: Integer;
  Status_Txt: String;
begin
  if QrCNAB_CfgTermoAceite.Value <> 0 then
  begin
    Status     := 0;
    Status_Txt := 'EM HOMOLOGA��O';
  end else
  begin
    Status := 1;
    Status_Txt := 'HOMOLOGADO';
  end;
  //
  if Geral.MB_Pergunta('Confirma altera��o do status da configura��o atual para: ' +
    Status_Txt + '?') <> ID_YES
  then
    Exit;
  //
  AlteraStatusConfiguracao(Status);
end;

procedure TFmCNAB_Cfg.AlteraTarifaRetornoCNAB1Click(Sender: TObject);
begin
  MostraCNAB_CfOR(stUpd);
end;

procedure TFmCNAB_Cfg.AtualizaLastNosNum(ModalCobr, CedBanco, CedAgencia: Integer;
  CedConta: String; LastNosNum: Double);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cnab_cfg', False,
    ['LastNosNum'], ['CedBanco', 'CedAgencia', 'CedConta', 'ModalCobr'],
    [LastNosNum], [CedBanco, CedAgencia, CedConta, ModalCobr], True);
end;

procedure TFmCNAB_Cfg.IncluiConfiguracaoCNAB1Click(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmCNAB_Cfg.IncluiRegistro;
var
  Cursor : TCursor;
  CNAB_Cfg : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    CNAB_Cfg := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                'CNAB_Cfg', 'CNAB_Cfg', 'Codigo');
    if Length(FormatFloat(FFormatFloat, CNAB_Cfg))>Length(FFormatFloat) then
    begin
      Geral.MB_Aviso('Inclus�o cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, CNAB_Cfg);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmCNAB_Cfg.IncluiTarifaRetornoCNAB1Click(Sender: TObject);
begin
  MostraCNAB_CfOR(stIns);
end;

procedure TFmCNAB_Cfg.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCNAB_Cfg.DBEdit9Change(Sender: TObject);
var
  Texto: String;
  Banco, CNAB: Integer;
begin
  Banco := QrCNAB_CfgCedBanco.Value;
  CNAB  := QrCNAB_CfgCNAB.Value;
  //
  //ReconfiguraTudo();
  //RedefineInstrucoes();
  FLayoutRem2 := UBco_Rem.LayoutsRemessa(Banco, CNAB);
  Geral.DescricaoDeArrStrStr(DBEdit9.Text, FLayoutRem2, Texto, 0, 1);
  EdLayoutRem_TXT2.Text := Texto;
end;

procedure TFmCNAB_Cfg.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCNAB_Cfg.SBProtocolCRClick(Sender: TObject);
var
  ProtocolCR: Integer;
begin
  {$IfNDef SemProtocolo}
  VAR_CADASTRO := 0;
  ProtocolCR   := EdProtocolCR.ValueVariant;
  //
  ConfiguraProtocoloCR;
  //
  if DBCheck.CriaFm(TFmProtocolos, FmProtocolos, afmoNegarComAviso) then
  begin
    if ProtocolCR <> 0 then
      FmProtocolos.LocCod(ProtocolCR, ProtocolCR);
    FmProtocolos.ShowModal;
    FmProtocolos.Destroy;
  end;
  if VAR_CADASTRO <> 0 then
  begin
    ConfiguraProtocoloCR;
    //
    if CBProtocolCR.Enabled then
    begin
      UMyMod.SetaCodigoPesquisado(EdProtocolCR, CBProtocolCR, QrProtocolCR, VAR_CADASTRO);
      CBProtocolCR.SetFocus;
    end;
  end;
  {$EndIf}
end;

procedure TFmCNAB_Cfg.SpeedButton10Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  DModG.CadastroDeEntidade(EdCedente.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.AbreQuery(QrCedente, Dmod.MyDB);
    UMyMod.AbreQuery(QrSacador, Dmod.MyDB);
    //
    UMyMod.SetaCodigoPesquisado(EdCedente, CBCedente, QrCedente, VAR_CADASTRO);
    CBCedente.SetFocus;
  end;
end;

procedure TFmCNAB_Cfg.SpeedButton11Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  DModG.CadastroDeEntidade(EdSacadAvali.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.AbreQuery(QrCedente, Dmod.MyDB);
    UMyMod.AbreQuery(QrSacador, Dmod.MyDB);
    //
    UMyMod.SetaCodigoPesquisado(EdSacadAvali, CBSacadAvali, QrCedente, VAR_CADASTRO);
    CBSacadAvali.SetFocus;
  end;
end;

procedure TFmCNAB_Cfg.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCNAB_Cfg.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCNAB_Cfg.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCNAB_Cfg.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCNAB_Cfg.BtCNAB_CfgClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCNAB_Cfg, BtCNAB_Cfg);
end;

procedure TFmCNAB_Cfg.BtCNAB_CfORClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCNAB_CfOR, BtCNAB_CfOR);
end;

procedure TFmCNAB_Cfg.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCNAB_CfgCodigo.Value;
  Close;
end;

procedure TFmCNAB_Cfg.BtConfirmaClick(Sender: TObject);

  function LocalizaProtocolo(CNAB_Cfg, Protocolo: Integer): Integer;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT ProtocolCR ',
      'FROM cnab_cfg ',
      'WHERE Codigo <> ' + Geral.FF0(CNAB_Cfg),
      'AND ProtocolCR=' + Geral.FF0(Protocolo),
      '']);
    if Dmod.QrAux.RecordCount > 0 then
      Result := Dmod.QrAux.FieldByName('ProtocolCR').AsInteger
    else
      Result := 0;
  end;
var
  Codigo, ProtocolCR, CtaRetTar, CedBanco, CedAgencia, Cedente, SacadAvali, ModalCobr: Integer;
  Nome, IndicatBB, CtaCooper, CedConta, AgContaCed, Msg: String;
  {$IfNDef SemProtocolo} ProtocolCRDuplic: Integer; {$EndIf}
begin
  if TemInconsistencias() then Exit;
  //
  Nome       := EdNome.Text;
  CedBanco   := EdCedBanco.ValueVariant;
  CedAgencia := EdCedAgencia.ValueVariant;
  CedConta   := EdCedConta.ValueVariant;
  Cedente    := EdCedente.ValueVariant;
  SacadAvali := EdSacadAvali.ValueVariant;
  ModalCobr  := RGModalCobr.ItemIndex;
  AgContaCed := EdAgContaCed.ValueVariant;
  //
  if Length(Nome) = 0 then
  begin
    Geral.MB_Aviso('Defina uma descri��o!');
    EdNome.SetFocus;
    Exit;
  end;
  if CedBanco = 0 then
  begin
    Geral.MB_Aviso('Defina o banco!');
    EdCedBanco.SetFocus;
    Exit;
  end;
  if CedAgencia = 0 then
  begin
    Geral.MB_Aviso('Defina a ag�ncia!');
    EdCedAgencia.SetFocus;
    Exit;
  end;
  if Length(CedConta) = 0 then
  begin
    Geral.MB_Aviso('Defina a conta!');
    EdCedConta.SetFocus;
    Exit;
  end;
  if Trim(EdDiretorio.Text) = '' then
  begin
    Geral.MB_Aviso('Defina um diret�rio para o arquivo remessa!');
    PageControl1.ActivePageIndex := 6;
    EdDiretorio.SetFocus;
    Exit;
  end;
  if Trim(EdDirRetorno.ValueVariant) = '' then
  begin
    Geral.MB_Aviso('Defina um diret�rio para o arquivo retorno!');
    PageControl1.ActivePageIndex := 7;
    EdDirRetorno.SetFocus;
    Exit;
  end;
  if RGCNAB.ItemIndex = 0 then
  begin
    Geral.MB_Aviso('Defina o CNAB.');
    Exit;
  end;
  if (Cedente = 0) and (EdSacadAvali.ValueVariant = 0) then
  begin
    Geral.MB_Aviso('Defina o cedente!');
    PageControl1.ActivePageIndex := 1;
    EdCedente.SetFocus;
    Exit;
  end;
  CtaCooper := EdCtaCooper.Text;
  //
  if MyObjects.FIC(CtaCooper <> Geral.SoNumero_TT(CtaCooper), EdCtaCooper,
  'Conta corrente cooperado inv�lida: "' + CtaCooper + '"') then
    Exit;
  //
  if (EdNosNumFxaI.ValueVariant > 0) or (EdNosNumFxaF.ValueVariant > 0) then
  begin
    if not CkNosNumFxaU.Checked then
    begin
      if Geral.MB_Pergunta(
      'Foi informado uma faixa de numera��o para gera��o sequencial do nosso n�mero, mas n�o foi checado para usar!'
      + sLineBreak + 'Deseja confirmar assim mesmo?') <> ID_YES then
        Exit;
    end;
  end;
  if (EdNosNumFxaI.ValueVariant = 0) and (EdNosNumFxaF.ValueVariant = 0) then
  begin
    if CkNosNumFxaU.Checked then
    begin
      if Geral.MB_Pergunta(
      'N�o foi informado a faixa de numera��o para gera��o sequencial do nosso n�mero!'
      ) <> ID_YES then
        Exit;
    end;
  end;
  {
  Codigo := UMyMod.BuscaEmLivreY_Def('MPP', 'Codigo', ImgTipo.SQLType, QrMPPCodigo.Value);
  }
  IndicatBB := RGIndicatBB.Items[RGIndicatBB.ItemIndex][2];
  Codigo    := Geral.IMV(EdCodigo.Text);
  //
  if not ValidaCedenteSacadAvali(Codigo, ModalCobr, CedBanco, CedAgencia,
    CedConta, Cedente, SacadAvali, AgContaCed, Msg) then
  begin
    Geral.MB_Aviso(Msg);
    PageControl1.ActivePageIndex := 1;
    EdCedente.SetFocus;
    Exit;
  end;
  //
  if ModalCobr = 1 then //Com registro
  begin
    {$IfNDef SemProtocolo}
    ProtocolCR       := EdProtocolCR.ValueVariant;
    ProtocolCRDuplic := LocalizaProtocolo(Codigo, ProtocolCR);
    //
    if ProtocolCRDuplic <> 0 then
    begin
      if Geral.MB_Pergunta('Este protocolo j� foi utilizado na configura��o ID n�mero ' +
        Geral.FF0(ProtocolCRDuplic) + '!' + sLineBreak +
        'Deseja inserir mesmo assim?') <> ID_YES then
      begin
        PageControl1.ActivePageIndex := 6;
        EdProtocolCR.SetFocus;
        Exit;
      end;
    end;
    if ProtocolCR = 0 then
    begin
      Geral.MB_Aviso('Para cobran�a com registro � necess�rio configurar o protocolo de cobran�a!');
      PageControl1.ActivePageIndex := 6;
      EdProtocolCR.SetFocus;
      Exit;
    end;
    {$Else}
    ProtocolCR := 0;
    {$EndIf}
  end else
    ProtocolCR := 0;  
  //
  CtaRetTar := EdCtaRetTar.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'CNAB_Cfg', False,
  [
    'Nome', 'CedBanco',
    'Cedente', 'SacadAvali',
    'CedAgencia', 'CedConta',
    'CedDAC_A', 'CedDAC_C',
    'CedDAC_AC', 'CedNome',
    'TipoCart', 'EspecieTit',
    'CartNum', 'CartCod',
    'InstrCobr1','InstrCobr2',
    'InstrDias', 'QuemPrint',
    'AceiteTit', 'Correio', 'SacAvaNome',
    'CodEmprBco', 'CodLidrBco', 'JurosTipo',
    'JurosPerc', 'JurosDias',
    'MultaTipo', 'MultaPerc',
    'Texto01', 'Texto02', 'Texto03', 'Texto04',
    'Texto05', 'Texto06', 'Texto07', 'Texto08',
    'Texto09', 'Texto10', 'Diretorio',
    'CNAB', 'EnvEmeio',
    '_237Mens1', '_237Mens2',
    'CodOculto', 'SeqArq',
    'LastNosNum', 'LocalPag',
    'EspecieVal', 'OperCodi',
    'IDCobranca', 'AgContaCed',
    'DirRetorno', 'PosicoesBB',
    (*'ConvLider', 'ConvCobr',*)
    'IndicatBB', 'TipoCobrBB',
    (*'VariaBB',*) 'Comando',
    'DdProtesBB', 'Msg40posBB',
    'CartRetorno', (*'CartCodBB',
    'CadTitBco',*) 'QuemDistrb',
    'Desco1Cod', 'Desco1Fat', 'Desco1Dds',
    'Desco2Cod', 'Desco2Fat', 'Desco2Dds',
    'Desco3Cod', 'Desco3Fat', 'Desco3Dds',
    'ProtesCod', 'ProtesDds',
    'BxaDevCod', 'BxaDevDds',
    'MoedaCod', 'Contrato',
    'MultaDias', 'Variacao',
    'ConvCartCobr', 'ConvVariCart',
    'EspecieDoc', 'CedPosto', 'CartEmiss',
    'TipoCobranca', 'EspecieTxt', 'CartTxt',
    'LayoutRem', 'LayoutRet', 'ConcatCod',
    'NaoRecebDd', 'TipBloqUso', 'ComplmCod',
    'NumVersaoI3', 'ModalCobr', 'CtaCooper',
    'NosNumFxaI', 'NosNumFxaF', 'NosNumFxaU',
    'CtrGarantiaNr', 'CtrGarantiaDV',
    'VTCBBNITAR', 'ProtocolCR', 'CtaRetTar',
    'CorresBco', 'CorresAge',
    'CorresCto', 'NPrinBc',
    'Ativo', 'TermoAceite', 'DataHoraAceite', 'UsuarioAceite'
  ], ['Codigo'], [
    Nome, EdCedBanco.ValueVariant,
    EdCedente.ValueVariant, EdSacadAvali.Text,
    EdCedAgencia.ValueVariant, EdCedConta.Text,
    EdCedDAC_A.ValueVariant, EdCedDAC_C.ValueVariant,
    EdCedDAC_AC.ValueVariant, EdCedNome.ValueVariant,
    EdTipoCart.ValueVariant, EdEspecieTit.Text,
    EdCartNum.ValueVariant, EdCartCod.ValueVariant,
    EdInstrCobr1.ValueVariant, EdInstrCobr2.ValueVariant,
    EdInstrDias.ValueVariant, EdQuemPrint.Text,
    RGAceiteTit.ItemIndex, RGCorreio.ItemIndex, EdSacAvaNome.Text,
    EdCodEmprBco.ValueVariant, EdCodLidrBco.ValueVariant, EdJurosTipo.Text,
    EdJurosPerc.ValueVariant, EdJurosDias.ValueVariant,
    EdMultaTipo.Text, EdMultaPerc.ValueVariant,
    EdTexto01.Text, EdTexto02.Text, EdTexto03.Text, EdTexto04.Text,
    EdTexto05.Text, EdTexto06.Text, EdTexto07.Text, EdTexto08.Text,
    EdTexto09.Text, EdTexto10.Text, EdDiretorio.Text,
    RGCNAB.Items[RGCNAB.ItemIndex], Geral.BoolToInt(CkEnvEmeio.Checked),
    Ed_237Mens1.ValueVariant, Ed_237Mens2.ValueVariant,
    EdCodOculto.Text, EdSeqArq.ValueVariant,
    EdLastNosNum.ValueVariant, EdLocalPag.Text,
    EdEspecieVal.Text, EdOperCodi.Text,
    EdIDCobranca.Text, AgContaCed,
    EdDirRetorno.ValueVariant,
    RGPosicoesBB.ItemIndex, (*EdConvLider.ValueVariant,
    EdConvCobr.ValueVariant,*) IndicatBB,
    EdTipoCobrBB.Text, (*EdVariaBB.ValueVariant,*)
    EdComando.ValueVariant,
    EdDdProtesBB.ValueVariant, EdMsg40posBB.Text,
    EdCartRetorno.ValueVariant, (*EdCartCodBB.Text,
    EdCadTitBco.Text,*) EdQuemDistrb.Text,
    EdDesco1Cod.ValueVariant, EdDesco1Fat.ValueVariant, EdDesco1Dds.ValueVariant,
    EdDesco2Cod.ValueVariant, EdDesco2Fat.ValueVariant, EdDesco2Dds.ValueVariant,
    EdDesco3Cod.ValueVariant, EdDesco3Fat.ValueVariant, EdDesco3Dds.ValueVariant,
    EdProtesCod.ValueVariant, EdProtesDds.ValueVariant,
    EdBxaDevCod.ValueVariant, EdBxaDevDds.ValueVariant,
    EdMoedaCod.ValueVariant, EdContrato.ValueVariant,
    EdMultaDias.ValueVariant, EdVariacao.ValueVariant,
    EdConvCartCobr.Text, EdConvVariCart.Text,
    EdEspecieDoc.ValueVariant, EdCedPosto.ValueVariant,
    EdCartEmiss.ValueVariant,
    RGTipoCobranca.ItemIndex, EdEspecieTxt.Text, EdCartTxt.Text,
    EdLayoutRem.Text, EdLayoutRet.Text, EdConcatCod.Text,
    EdNaoRecebDd.ValueVariant, EdTipBloqUso.Text, EdComplmCod.Text,
    EdNumVersaoI3.ValueVariant, ModalCobr, CtaCooper,
    EdNosNumFxaI.ValueVariant, EdNosNumFxaF.ValueVariant,
    Geral.BoolToInt(CkNosNumFxaU.Checked),
    EdCtrGarantiaNr.Text, EdCtrGarantiaDV.Text,
    EdVTCBBNITAR.ValueVariant, ProtocolCR, CtaRetTar,
    EdCorresBco.ValueVariant, EdCorresAge.ValueVariant,
    EdCorresCto.ValueVariant, EdNPrinBc.ValueVariant,
    Geral.BoolToInt(CkAtivo.Checked), 0, '0000-00-00 00:00:00', 0
  ], [Codigo], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CNAB_Cfg', 'Codigo');
    //
    AtualizaLastNosNum(ModalCobr, EdCedBanco.ValueVariant,
      EdCedAgencia.ValueVariant, EdCedConta.Text, EdLastNosNum.ValueVariant);
    //
    MostraEdicao(0, stLok, 0);
    LocCod(Codigo,Codigo);
  end;
end;

procedure TFmCNAB_Cfg.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'CNAB_Cfg', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CNAB_Cfg', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CNAB_Cfg', 'Codigo');
end;

procedure TFmCNAB_Cfg.BtDuplicaClick(Sender: TObject);
begin
  DuplicaCfgAtual();
end;

procedure TFmCNAB_Cfg.BtExcluiClick(Sender: TObject);
begin
//
end;

procedure TFmCNAB_Cfg.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCNAB_CfgCodigo.Value,LaRegistro.Caption);
end;

procedure TFmCNAB_Cfg.SbGeneroClick(Sender: TObject);
  {$IfNDef NO_FINANCEIRO}
var
  CtaRetTar: Integer;
begin
  VAR_CADASTRO := 0;
  CtaRetTar    := EdCtaRetTar.ValueVariant;
  //
  FinanceiroJan.CadastroDeContas(CtaRetTar);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
    //
    UMyMod.SetaCodigoPesquisado(EdCtaRetTar, CBCtaRetTar, QrContas, VAR_CADASTRO);
    CBCtaRetTar.SetFocus;
  end;
{$Else}
begin
  Grl_Geral.InfoSemModulo(mdlappFinanceiro);
{$EndIf}
end;

procedure TFmCNAB_Cfg.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmCNAB_Cfg.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCNAB_Cfg.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmCNAB_Cfg.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCNAB_Cfg.QrCNAB_CfgAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCNAB_Cfg.QrCNAB_CfgAfterScroll(DataSet: TDataSet);
var
  Banco, CNAB, ModalCobr: Integer;
  NomeLayout: String;
begin
  Banco      := QrCNAB_CfgCedBanco.Value;
  CNAB       := QrCNAB_CfgCNAB.Value;
  ModalCobr  := QrCNAB_CfgModalCobr.Value;
  NomeLayout := QrCNAB_CfgLayoutRem.Value;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False,
    UBco_Rem.AvisoImplementacao(Banco, CNAB, ModalCobr, NomeLayout));
  //
  ReopenCNAB_CfOR(0);
end;

procedure TFmCNAB_Cfg.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCNAB_Cfg.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCNAB_CfgCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'CNAB_Cfg', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCNAB_Cfg.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCNAB_Cfg.QrCNAB_CfgBeforeClose(DataSet: TDataSet);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmCNAB_Cfg.QrCNAB_CfgBeforeOpen(DataSet: TDataSet);
begin
  QrCNAB_CfgCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCNAB_Cfg.EdCedNomeExit(Sender: TObject);
begin
  //18/11/2015 => O SICOOB pede alguns caracteres que n�o s�o palavras
  //EdCedNome.Text := dmkPF.ValidaPalavra(EdCedNome.Text);
end;

procedure TFmCNAB_Cfg.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CriaOForm;
  //
  Timer1.Enabled := False;
  //
  GroupBox1.Align := alClient;
  UnDmkDAC_PF.AbreQuery(QrCedente, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrSacador, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrBancos, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  //
  PageControl1.ActivePageIndex := 0;
  //PageControl2.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  PageControl_001.ActivePageIndex := 0;
  PageControl_237.ActivePageIndex := 0;
  PageControl_341.ActivePageIndex := 0;
  //
  FCorSim := clWindow;
  (*
  FCorNao := FmPrincipal.sd1.colors[csButtonFace];
  FCorTvz := FmPrincipal.sd1.colors[csTitleTextActive];
  FCorTxz := FmPrincipal.sd1.colors[csTitleTextNoActive];
  //
  FCorAju := FmPrincipal.sd1.colors[csButtonDkshadow];
  FCorTxt := FmPrincipal.sd1.colors[csButtonLight];
  *)
  FCorNao := MyObjects.SubstituiCorParaSkin(csButtonFace);
  FCorTvz := MyObjects.SubstituiCorParaSkin(csTitleTextActive);
  FCorTxz := MyObjects.SubstituiCorParaSkin(csTitleTextNoActive);
  //
  FCorAju := MyObjects.SubstituiCorParaSkin(csButtonDkshadow);
  FCorTxt := MyObjects.SubstituiCorParaSkin(csButtonLight);
  //
  Edit1.Color := FCorSim;
  Edit2.Color := FCorNao;
  Edit3.Color := FCorAju;
  Edit4.Color := FCorTvz;
  //
  EdTipoCobrBB_TXT.Color       := FCorAju;
  EdComando_TXT.Color          := FCorAju;
  EdCarteira_TXT.Color         := FCorAju;
  EdTipoCart_TXT.Color         := FCorAju;
  EdEspecieVal_TXT.Color       := FCorAju;
  EdDdProtesBB_TXT.Color       := FCorAju;
  EdEspecieTit_TXT.Color       := FCorAju;
  EdEspecieTit_TXT2.Color      := FCorAju;
  //EdCartCodBB_TXT.Color        := FCorAju;
  //EdCadTitBco_TXT.Color        := FCorAju;
  EdQuemDistrb_TXT.Color       := FCorAju;
  EdQuemPrint_TXT.Color        := FCorAju;
  EdJurosTipo_TXT.Color        := FCorAju;
  EdMultaTipo_TXT.Color        := FCorAju;
  EdDesco1Cod_TXT.Color        := FCorAju;
  EdDesco2Cod_TXT.Color        := FCorAju;
  EdDesco3Cod_TXT.Color        := FCorAju;
  EdProtesCod_TXT.Color        := FCorAju;
  EdBxaDevCod_TXT.Color        := FCorAju;
  EdMoedaCod_TXT.Color         := FCorAju;
  EdVariacao_TXT.Color         := FCorAju;
  EdLayoutRem_TXT1.Color       := FCorAju;
  EdLayoutRet_TXT.Color        := FCorAju;
  MECarteira.Color             := FCorAju;
  dmkMemo1.Color               := FCorAju;
  dmkMemo2.Color               := FCorAju;
  EdArq.Color                  := FCorAju;
  MeLines.Color                := FCorAju;
  EdTipBloqUso_TXT.Color       := FCorAju;
  EdTipBloqUso_TXT2.Color      := FCorAju;
  //
  EdTipoCobrBB_TXT.Font.Color  := FCorTxt;
  EdComando_TXT.Font.Color     := FCorTxt;
  EdCarteira_TXT.Font.Color    := FCorTxt;
  EdTipoCart_TXT.Font.Color    := FCorTxt;
  EdEspecieVal_TXT.Font.Color  := FCorTxt;
  EdDdProtesBB_TXT.Font.Color  := FCorTxt;
  EdEspecieTit_TXT.Font.Color  := FCorTxt;
  EdEspecieTit_TXT2.Font.Color := FCorTxt;
  //EdCartCodBB_TXT.Font.Color   := FCorTxt;
  //EdCadTitBco_TXT.Font.Color   := FCorTxt;
  EdQuemDistrb_TXT.Font.Color  := FCorTxt;
  EdQuemPrint_TXT.Font.Color   := FCorTxt;
  EdJurosTipo_TXT.Font.Color   := FCorTxt;
  EdMultaTipo_TXT.Font.Color   := FCorTxt;
  EdDesco1Cod_TXT.Font.Color   := FCorTxt;
  EdDesco2Cod_TXT.Font.Color   := FCorTxt;
  EdDesco3Cod_TXT.Font.Color   := FCorTxt;
  EdProtesCod_TXT.Font.Color   := FCorTxt;
  EdBxaDevCod_TXT.Font.Color   := FCorTxt;
  EdMoedaCod_TXT.Font.Color    := FCorTxt;
  EdVariacao_TXT.Font.Color    := FCorTxt;
  EdLayoutRem_TXT1.Font.Color  := FCorTxt;
  EdLayoutRet_TXT.Font.Color   := FCorTxt;
  MECarteira.Font.Color        := FCorTxt;
  dmkMemo1.Font.Color          := FCorTxt;
  dmkMemo2.Font.Color          := FCorTxt;
  EdArq.Font.Color             := FCorTxt;
  MeLines.Font.Color           := FCorTxt;
  EdTipBloqUso_TXT.Font.Color  := FCorTxt;
  EdTipBloqUso_TXT2.Font.Color := FCorTxt;
  //
  PainelEdit.Align     := alClient;
  FListaArq := TStringList.Create;
  //
  EdComando.ValueVariant := 1;
  // 2011-12-10 > Usa?
  UBancos.ConfiguraTipoCobranca(RGTipoCobranca, 2, 0);
  //
  UnDmkTags.CarregaCNAB_CfgTags(LbVars);
end;

procedure TFmCNAB_Cfg.EdSacadAvaliChange(Sender: TObject);
var
  Cedente, SacadAvali: Integer;
begin
  Cedente    := EdCedente.ValueVariant;
  SacadAvali := EdSacadAvali.ValueVariant;
  //
  ReopenCartRetorno(Cedente, SacadAvali);
  ReopenCartRemessa(Cedente, SacadAvali);
  //
  ConfiguraProtocoloCR;
end;

procedure TFmCNAB_Cfg.EdSacAvaNomeExit(Sender: TObject);
begin
  //18/11/2015 => O SICOOB pede alguns caracteres que n�o s�o palavras
  //EdSacAvaNome.Text := dmkPF.ValidaPalavra(EdSacAvaNome.Text);
end;

procedure TFmCNAB_Cfg.EdSacAvaNomeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Nome, Doc: String;
begin
  if Key = VK_F4 then
  begin
    if EdSacadAvali.ValueVariant <> 0 then
    begin
      Doc  := Geral.FormataCNPJ_TT_NFe(QrSacadorDOCENT.Value);
      Nome := QrSacadorNOMEENT.Value;
      //
      EdSacAvaNome.ValueVariant := Doc + ' - ' + Nome;
    end;
  end;
end;

procedure TFmCNAB_Cfg.EdTexto01Enter(Sender: TObject);
begin
  Timer1.Enabled := False;
  FEdCondInst := TdmkEdit(Sender);
end;

procedure TFmCNAB_Cfg.EdTexto01Exit(Sender: TObject);
begin
  Timer1.Enabled := True;
end;

procedure TFmCNAB_Cfg.EdTipBloqUsoChange(Sender: TObject);
var
  Texto1, Texto2, Texto3: String;
begin
  Geral.DescricaoDeArrStrStr3(EdTipBloqUso.Text, FTipBloqUso, Texto1, Texto2,
  Texto3, 0, 0, 1, 2);
  EdTipBloqUso.Text := Texto1;
  EdTipBloqUso_TXT.Text := Texto2;
  EdTipBloqUso_TXT2.Text := Texto3;
end;

procedure TFmCNAB_Cfg.EdTipoCartChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdTipoCart.Text, FTipoCart, Texto, 0, 1);
  EdTipoCart_TXT.Text := Texto;
end;

procedure TFmCNAB_Cfg.EdTipoCobrBBChange(Sender: TObject);
begin
  TextoTipoCobrBB();
end;

procedure TFmCNAB_Cfg.EdVariacaoChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdVariacao.ValueVariant, FVariacao, Texto, 0, 1);
  EdVariacao_TXT.Text := Texto;
end;

procedure TFmCNAB_Cfg.ExcluiConfiguracaoCNAB1Click(Sender: TObject);
begin
  //
end;

procedure TFmCNAB_Cfg.ExcluiTarifaRetornoCNAB1Click(Sender: TObject);
{
var
  Controle: Integer;
}
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'CNAB_CfOR', 'Controle', QrCNAB_CfORControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrCNAB_CfOR,
      QrCNAB_CfORControle, QrCNAB_CfORControle.Value);
    ReopenCNAB_CfOR(Controle);
  end;
}
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrCNAB_CfOR, TDBGrid(DBGCNAB_CfOR),
  'cnab_cfor', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmCNAB_Cfg.TextoTipoCobrBB();
var
  Texto, Carteira: String;
  Banco, CNAB: Integer;
begin
  Banco       := EdCedBanco.ValueVariant;
  Carteira    := EdCartNum.Text;
  CNAB        := Geral.IMV(RGCNAB.Items[RGCNAB.ItemIndex]);
  //
  if (Banco <> 0) and (CNAB <> 0) then //Para evitar erro no mostraedicao
    FTipoCobrBB := UBco_Rem.TipoDeCobrancaBB(Banco, CNAB, Carteira);
  //
  Geral.DescricaoDeArrStrStr(EdTipoCobrBB.Text, FTipoCobrBB, Texto, 0, 1);
  EdTipoCobrBB_TXT.Text := Texto;
end;

procedure TFmCNAB_Cfg.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FEdCondInst := nil;
end;

procedure TFmCNAB_Cfg.EdCodEmprBcoExit(Sender: TObject);
begin
  EdCodEmprBco.Text := Geral.SoNumero_TT(EdCodEmprBco.Text);
end;

procedure TFmCNAB_Cfg.EdCodLidrBcoExit(Sender: TObject);
begin
  EdCodLidrBco.Text := Geral.SoNumero_TT(EdCodLidrBco.Text);
end;

procedure TFmCNAB_Cfg.EdComandoChange(Sender: TObject);
begin
  RedefineInstrucoes();
end;

procedure TFmCNAB_Cfg.EdDdProtesBBChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdDdProtesBB.Text, FDdProtesto, Texto, 0, 1);
  EdDdProtesBB_TXT.Text := Texto;
end;

procedure TFmCNAB_Cfg.EdDesco1CodChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdDesco1Cod.Text, FDescontos, Texto, 0, 1);
  EdDesco1Cod_TXT.Text := Texto;
end;

procedure TFmCNAB_Cfg.EdDesco2CodChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdDesco2Cod.Text, FDescontos, Texto, 0, 1);
  EdDesco2Cod_TXT.Text := Texto;
end;

procedure TFmCNAB_Cfg.EdDesco3CodChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdDesco3Cod.Text, FDescontos, Texto, 0, 1);
  EdDesco3Cod_TXT.Text := Texto;
end;

procedure TFmCNAB_Cfg.EdCedBancoExit(Sender: TObject);
begin
  ReconfiguraCoresEFormatacoes();
end;

procedure TFmCNAB_Cfg.EdCedenteChange(Sender: TObject);
var
  Cedente, SacadAvali: Integer;
begin
  Cedente    := EdCedente.ValueVariant;
  SacadAvali := EdSacadAvali.ValueVariant;
  //
  ReopenCartRetorno(Cedente, SacadAvali);
  ReopenCartRemessa(Cedente, SacadAvali);
  //
  ConfiguraProtocoloCR;
end;

procedure TFmCNAB_Cfg.ReconfiguraCoresEFormatacoes();
var
  Banco, CNAB: Integer;
  Registrada: Boolean;
begin
  Banco      := Geral.IMV(EdCedBanco.Text);
  CNAB       := Geral.IMV(RGCNAB.Items[RGCNAB.ItemIndex]);
  Registrada := RGModalCobr.ItemIndex = 1;
  //
  {
  EdNome.Color                    := FCorSim;
  EdCedNome.Color                 := FCorSim;
  EdCedBanco.Color                := FCorSim;
  CBCedBanco.Color                := FCorSim;
  EdCedAgencia.Color              := FCorSim;
  EdCedConta.Color                := FCorSim;
  EdCartNum.Color                 := FCorSim;
  EdCartCod.Color                 := FCorSim;
  EdLayoutRem.Color               := FCorSim;
  EdEspecieTxt.Color              := FCorSim;
  }
  //
  EdCedDAC_A.Color                := FCorNao;
  EdCedDAC_C.Color                := FCorNao;
  EdCedDAC_AC.Color               := FCorNao;
  EdCedPosto.Color                := FCorNao;
  EdSacadAvali.Color              := FCorNao;
  CBSacadAvali.Color              := FCorNao;
  EdCedNome.Color                 := FCorNao;
  EdSacAvaNome.Color              := FCorNao;
  EdInstrCobr1.Color              := FCorNao;
  EdInstrCobr2.Color              := FCorNao;
  EdInstrDias.Color               := FCorNao;
  EdCodEmprBco.Color              := FCorNao;
  EdCodLidrBco.Color              := FCorNao;
  EdMultaPerc.Color               := FCorNao;
  EdCartNum.Color                 := FCorNao;
  EdCartCod.Color                 := FCorNao;
  EdinstrCobr1_TXT.Color          := FCorNao;
  EdinstrCobr2_TXT.Color          := FCorNao;
  EdTexto01.Color                 := FCorNao;
  EdTexto02.Color                 := FCorNao;
  EdTexto03.Color                 := FCorNao;
  EdTexto04.Color                 := FCorNao;
  EdTexto05.Color                 := FCorNao;
  EdTexto06.Color                 := FCorNao;
  EdTexto07.Color                 := FCorNao;
  EdTexto08.Color                 := FCorNao;
  EdTexto09.Color                 := FCorNao;
  EdTexto10.Color                 := FCorNao;
  CkEnvEmeio.Color                := FCorNao;
  EdCodOculto.Color               := FCorNao;
  EdLastNosNum.Color              := FCorSim;
  EdAgContaCed.Color              := FCorSim;
  EdOperCodi.Color                := FCorNao;
  EdIDCobranca.Color              := FCorNao;
  EdTipoCobrBB.Color              := FCorNao;
  EdDdProtesBB.Color              := FCorNao;
  EdMsg40posBB.Color              := FCorNao;
  //EdCartCodBB.Color               := FCorNao;
  EdQuemDistrb.Color              := FCorNao;
  EdQuemPrint.Color               := FCorNao;
  //EdCadTitBco.Color               := FCorNao;
  EdTipoCart.Color                := FCorNao;
  EdNosNumFxaI.Color              := FCorNao;
  EdNosNumFxaF.Color              := FCorNao;
  EdMultaTipo.Color               := FCorNao;
  EdJurosTipo.Color               := FCorNao;
  EdDesco1Cod.Color               := FCorNao;
  EdDesco1Fat.Color               := FCorNao;
  EdDesco1Dds.Color               := FCorNao;
  EdDesco2Cod.Color               := FCorNao;
  EdDesco2Fat.Color               := FCorNao;
  EdDesco2Dds.Color               := FCorNao;
  EdDesco3Cod.Color               := FCorNao;
  EdDesco3Fat.Color               := FCorNao;
  EdDesco3Dds.Color               := FCorNao;
  EdProtesCod.Color               := FCorNao;
  EdProtesDds.Color               := FCorNao;
  EdBxaDevCod.Color               := FCorNao;
  EdBxaDevDds.Color               := FCorNao;
  EdMoedaCod.Color                := FCorNao;
  EdContrato.Color                := FCorNao;
  EdConvCartCobr.Color            := FCorNao;
  EdConvVariCart.Color            := FCorNao;
  EdVariacao.Color                := FCorNao;
  EdCartTxt.Color                 := FCorNao;
  EdLayoutRet.Color               := FCorNao;
  EdConcatCod.Color               := FCorNao;
  EdComplmCod.Color               := FCorNao;
  EdInstrDias.Color               := FCorNao;
  EdNaorecebDd.Color              := FCorNao;
  EdTipBloqUso.Color              := FCorNao;
  EdNumVersaoI3.Color             := FCorNao;
  EdCtaCooper.Color               := FCorNao;
  EdCtrGarantiaNr.Color           := FCorNao;
  EdCtrGarantiaDV.Color           := FCorNao;
  // 2012-11-01  - ver quais � sim
  EdMultaDias.Color               := FCorNao;
  EdJurosDias.Color               := FCorNao;
  // Fim 2012-11-01
  //
  EdCorresBco.Color               := FCorNao;
  EdCorresAge.Color               := FCorNao;
  EdCorresCto.Color               := FCorNao;
  EdNPrinBc.Color                 := FCorNao;
  //
  EdVTCBBNITAR.Color              := FCorNao;
  //
  EdLastNosNum.LeftZeros          := 0;
  EdCodEmprBco.LeftZeros          := 0;
  EdIDCobranca.LeftZeros          := 0;
  //
  case Banco of
    001: // Banco do Brasil
    begin
      if CNAB = 240 then
      begin
        EdCedDAC_A.Color          := FCorSim;
        EdCedDAC_C.Color          := FCorSim;
        EdCodEmprBco.Color        := FCorSim;
        EdCodLidrBco.Color        := FCorSim;
        EdCartCod.Color           := FCorSim;
        EdCartNum.Color           := FCorSim;
        //EdCartCodBB.Color         := FCorSim;
        //EdCadTitBco.Color         := FCorSim;
        EdQuemDistrb.Color        := FCorSim;
        EdQuemPrint.Color         := FCorSim;
        EdTipoCart.Color          := FCorSim; // Tipo de documento
        EdJurosTipo.Color         := FCorSim;
        EdMultaTipo.Color         := FCorSim;
        EdDesco1Cod.Color         := FCorSim;
        EdDesco1Fat.Color         := FCorSim;
        EdDesco1Dds.Color         := FCorSim;
        EdDesco2Cod.Color         := FCorSim;
        EdDesco2Fat.Color         := FCorSim;
        EdDesco2Dds.Color         := FCorSim;
        EdDesco3Cod.Color         := FCorSim;
        EdDesco3Fat.Color         := FCorSim;
        EdDesco3Dds.Color         := FCorSim;
        EdProtesCod.Color         := FCorSim;
        EdProtesDds.Color         := FCorSim;
        EdBxaDevCod.Color         := FCorSim;
        EdBxaDevDds.Color         := FCorSim;
        EdMoedaCod.Color          := FCorSim;
        EdContrato.Color          := FCorSim;
        EdMultaPerc.Color         := FCorSim;
        EdVariacao.Color          := FCorSim;
      end;
      if CNAB = 400 then
      begin
        EdCedDAC_A.Color          := FCorSim;
        EdCedDAC_C.Color          := FCorSim;
        EdCodEmprBco.Color        := FCorSim;
        EdCartCod.Color           := FCorNao;
        EdCartNum.Color           := FCorSim;
        EdCodLidrBco.Color        := FCorSim;
        EdVariacao.Color          := FCorSim;
        EdInstrCobr1.Color        := FCorSim;
        EdInstrCobr2.Color        := FCorSim;
      end;
    end;
    008, 033, 353:
    begin
      EdLayoutRet.Color         := FCorSim;
      EdConcatCod.Color         := FCorSim;
      EdCodEmprBco.Color        := FCorSim;
      EdCartCod.Color           := FCorSim;
      EdCartNum.Color           := FCorSim;
      //EdCartTxt.Color           := FCorSim;
      if CNAB = 400 then
      begin
        EdComplmCod.Color       := FCorSim;
        //EdEspecieTxt.Color      := FCorSim;
        EdInstrCobr1.Color      := FCorSim;
        EdInstrCobr2.Color      := FCorSim;
        EdMultaTipo.Color       := FCorSim;
        EdMultaDias.Color       := FCorSim;
        EdMultaPerc.Color       := FCorSim;
        EdDesco1Fat.Color       := FCorSim;
        EdDesco1Dds.Color       := FCorSim;
        EdDesco2Fat.Color       := FCorSim;
        EdDesco2Dds.Color       := FCorSim;
        //EdDesco3Fat.Color       := FCorSim; // n�o tem!!
        //EdDesco3Dds.Color       := FCorSim; // n�o tem!!
        EdProtesDds.Color       := FCorSim;
        EdNumVersaoI3.Color     := FCorSim;
        //
        EdJurosDias.Color       := FCorNao; // n�o tem!!
      end;
    end;
    104: // CEF
    begin
      EdIDCobranca.LeftZeros      := 2;
    end;
    237: // Bradesco
    begin
      EdCtaCooper.Color         := FCorTvz;
      EdCtaCooper.Font.Color    := FCorTxz;
      if CNAB = 400 then
      begin
        {if EdLayoutRem.Text = CO_237_MP_4008_0121_01_Data_11_11_2010 then}
        begin
          EdCedDAC_C.Color          := FCorSim;
          EdCodEmprBco.Color        := FCorSim;
          EdMultaPerc.Color         := FCorSim;
          EdCartNum.Color           := FCorSim; // Mudei 2009-11-07
          EdQuemPrint.Color         := FCorSim;
          EdInstrCobr1.Color        := FCorSim;
          EdInstrCobr2.Color        := FCorSim;

          EdLastNosNum.LeftZeros    := 11;

          // 2012-11-03
          EdMultaTipo.Color         := FCorSim;
          EdDesco1Fat.Color         := FCorSim;
          EdDesco1Dds.Color         := FCorSim;
          // FIM 2012-11-03
        end;
      end;
    end;
    341: // Ita�
    begin
      if CNAB = 400 then
      begin
        EdCedDAC_AC.Color         := FCorSim;
        CkEnvEmeio.Color          := FCorSim;
        EdCartNum.Color           := FCorSim;
        EdTipoCart.Color          := FCorSim;
        EdCartCod.Color           := FCorSim;  
        EdInstrCobr1.Color        := FCorSim;
        EdInstrCobr2.Color        := FCorSim;
        EdInstrDias.Color         := FCorSim;
        EdNosNumFxaI.Color        := FCorSim;
        EdNosNumFxaF.Color        := FCorSim;
        EdNosNumFxaI.LeftZeros    := 8;
        EdNosNumFxaF.LeftZeros    := 8;
        EdLastNosNum.LeftZeros    := 8;
        LiberaTextoLinha1;
      end;
    end;
    399: // HSBC
    begin
      if CNAB = 400 then
      begin
        EdCartNum.Color           := FCorSim;
        EdCodEmprBco.Color        := FCorSim;
        //EdRange.Color             := FCorSim;
        //EdRange.LeftZeros         := 5; // as 5 posi��es iniciais � o Range -> CEDENTE (EEEEE) e a �ltima � o DV (D)> EEEEENNNNND
        EdLastNosNum.LeftZeros    := 5; // � 11, mas as 5 posi��es iniciais � o Cedente (EEEEE) e a �ltima � o DV dos dois (D)> EEEEENNNNND
        EdSeqArq.Color            := FCorNao;
        EdCartTxt.Color           := FCorSim;
        //EdEspecieTxt.Color        := FCorSim;
        EdInstrCobr1.Color        := FCorSim;
        EdInstrCobr2.Color        := FCorSim;
        if Registrada then
        begin
          EdCartCod.Color           := FCorSim;
          EdLayoutRet.Color         := FCorSim;
          EdMultaPerc.Color         := FCorSim;
          EdConcatCod.Color         := FCorSim;
          EdNaorecebDd.Color        := FCorSim;
          EdTipBloqUso.Color        := FcorSim;
          EdProtesCod.Color         := FCorSim;
          EdProtesDds.Color         := FCorSim;
          EdDesco1Fat.Color         := FCorSim;
          EdDesco1Dds.Color         := FCorSim;
          EdDesco2Fat.Color         := FCorSim;
          EdDesco2Dds.Color         := FCorSim;
          EdDesco3Fat.Color         := FCorSim;
          EdDesco3Dds.Color         := FCorSim;
        end;
      end;
    end;
    409: // Unibanco
    begin
      if CNAB = 400 then
      begin
        EdCodEmprBco.Color        := FCorSim;
        EdCodEmprBco.LeftZeros    := 7;
        EdCartNum.Color           := FCorSim;
        EdLastNosNum.LeftZeros    := 14;
      end;
    end;
    756: // Sicoob
    begin
      if CNAB = 240 then
      begin
        if (EdLayoutRem.Text = CO_756_CORRESPONDENTE_BB_2015) then
        begin
          EdCartNum.Color    := FCorSim;
          EdCartTxt.Color    := FCorSim;
          EdQuemPrint.Color  := FCorSim;
          EdCodLidrBco.Color := FCorSim;
          EdCodEmprBco.Color := FCorSim;
          EdCtaCooper.Color  := FCorSim;

          EdCedNome.Color    := FCorSim;
          EdSacadAvali.Color := FCorSim;
          CBSacadAvali.Color := FCorSim;
          EdSacAvaNome.Color := FCorSim;

          EdJurosTipo.Color  := FCorSim;
          EdDesco1Fat.Color  := FCorSim;
          EdDesco1Dds.Color  := FCorSim;
          EdProtesCod.Color  := FCorSim;
          EdProtesDds.Color  := FCorSim;

          EdCorresBco.Color := FCorSim;
          EdCorresAge.Color := FCorSim;
          EdCorresCto.Color := FCorSim;
        end else
        if (EdLayoutRem.Text = CO_756_240_2018) then
        begin
          EdCedBanco.Color    := FCorSim;
          EdCedAgencia.Color  := FCorSim;
          EdCedDAC_A.Color    := FCorSim;
          EdCedConta.Color    := FCorSim;
          EdCedDAC_C.Color    := FCorSim;
          EdCedDAC_AC.Color   := FCorSim;
          EdLocalPag.Color    := FCorSim;
          EdComando.Color     := FCorSim;
          EdEspecieDoc.Color  := FCorSim;
          EdEspecieVal.Color  := FCorSim;
          EdCartNum.Color     := FCorSim;
          EdQuemPrint.Color   := FCorSim;
          EdQuemDistrb.Color  := FCorSim;
          EdCodEmprBco.Color  := FCorSim;
          EdAgContaCed.Color  := FCorSim;
          EdLastNosNum.Color  := FCorSim;
          EdSeqArq.Color      := FCorSim;
          EdCedente.Color     := FCorSim;
          EdSacadAvali.Color  := FCorSim;
          CBSacadAvali.Color  := FCorSim;
          EdDesco1Cod.Color   := FCorSim;
          EdDesco1Fat.Color   := FCorSim;
          EdDesco1Dds.Color   := FCorSim;
          EdDesco2Cod.Color   := FCorSim;
          EdDesco2Fat.Color   := FCorSim;
          EdDesco2Dds.Color   := FCorSim;
          EdDesco3Cod.Color   := FCorSim;
          EdDesco3Fat.Color   := FCorSim;
          EdDesco3Dds.Color   := FCorSim;
          EdTexto01.Color     := FCorSim;
          EdTexto02.Color     := FCorSim;
          EdTexto03.Color     := FCorSim;
          EdTexto04.Color     := FCorSim;
          EdMultaTipo.Color   := FCorSim;
          EdMultaPerc.Color   := FCorSim;
          EdMultaDias.Color   := FCorSim;
          EdJurosTipo.Color   := FCorSim;
          EdJurosPerc.Color   := FCorSim;
          EdJurosDias.Color   := FCorSim;
          EdProtesCod.Color   := FCorSim;
          EdProtesDds.Color   := FCorSim;
          EdBxaDevCod.Color   := FCorSim;
          EdBxaDevDds.Color   := FCorSim;
          EdCorresBco.Color   := FCorSim;
          EdDiretorio.Color   := FCorSim;
          EdCartEmiss.Color   := FCorSim;
          EdProtocolCR.Color  := FCorSim;
          EdDirRetorno.Color  := FCorSim;
          EdCartRetorno.Color := FCorSim;
          EdCtaRetTar.Color   := FCorSim;
        end;
      end;
      if CNAB = 400 then
      begin
        EdVTCBBNITAR.Color     := FCorSim;
        EdCartNum.Color        := FCorSim;
        EdCedAgencia.Color     := FCorSim;
        EdCodEmprBco.Color     := FCorSim;
        EdCodEmprBco.LeftZeros := 7;
        //
        if (EdLayoutRem.Text = CO_756_EXCEL_2012_03_14) or
          (EdLayoutRem.Text = CO_756_EXCEL_2013_07_18) or
          (EdLayoutRem.Text = CO_756_CORRESPONDENTE_BRADESCO_2015) then
        begin
          EdCedDAC_A.Color := FCorSim;
          EdCedDAC_C.Color := FCorSim;
          //
          if (EdLayoutRem.Text <> CO_756_CORRESPONDENTE_BRADESCO_2015) then
          begin
            EdQuemDistrb.Color    := FCorSim;
            EdCtrGarantiaNr.Color := FCorSim;
            EdCtrGarantiaDV.Color := FCorSim;
          end;
          //
          EdMsg40posBB.Color := FCorSim;
          EdTexto01.Color    := FCorSim;
          EdTexto02.Color    := FCorSim;
          EdTexto03.Color    := FCorSim;
          EdTexto04.Color    := FCorSim;
          //
          if (EdLayoutRem.Text <>  CO_756_CORRESPONDENTE_BRADESCO_2015) then
          begin
            EdTexto05.Color    := FCorSim;
            EdInstrCobr1.Color := FCorSim;
            EdInstrCobr2.Color := FCorSim;
          end;
          //
          EdMultaPerc.Color := FCorSim;
          EdJurosPerc.Color := FCorSim;
          //
          //EdDesco1Cod.Color := FCorSim;  N�o tem
          EdDesco1Dds.Color := FCorSim;
          EdDesco1Fat.Color := FCorSim;
          //
          EdEspecieDoc.Color := FCorSim;
          //
          if Registrada then
          begin
            if (EdLayoutRem.Text = CO_756_CORRESPONDENTE_BRADESCO_2015) then
            begin
              EdVTCBBNITAR.Color := FCorNao;
              //
              EdProtesCod.Color := FCorSim;
              EdProtesDds.Color := FCorSim;
              EdCtaCooper.Color := FCorSim;

              EdSacadAvali.Color := FCorSim;
              CBSacadAvali.Color := FCorSim;
              EdSacAvaNome.Color := FCorSim;

              EdCorresBco.Color := FCorSim;
              EdCorresAge.Color := FCorSim;
              EdCorresCto.Color := FCorSim;
              EdNPrinBc.Color   := FCorSim;
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TFmCNAB_Cfg.EdInstrCobr1Exit(Sender: TObject);
begin
  LiberaTextoLinha1;
end;

procedure TFmCNAB_Cfg.EdInstrCobr2Exit(Sender: TObject);
begin
  LiberaTextoLinha1;
end;

procedure TFmCNAB_Cfg.EdJurosTipoChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdJurosTipo.Text, FJurosTipo, Texto, 0, 1);
  EdJurosTipo_TXT.Text := Texto;
end;

procedure TFmCNAB_Cfg.EdLayoutRemChange(Sender: TObject);
var
  Texto: String;
begin
  ReconfiguraTudo();
  RedefineInstrucoes();
  Geral.DescricaoDeArrStrStr(EdLayoutRem.Text, FLayoutRem1, Texto, 0, 1);
  EdLayoutRem_TXT1.Text := Texto;
end;

procedure TFmCNAB_Cfg.EdLayoutRetChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdLayoutRet.Text, FLayoutRet, Texto, 0, 1);
  EdLayoutRet_TXT.Text := Texto;
end;

procedure TFmCNAB_Cfg.EdMoedaCodChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdMoedaCod.Text, FMoedaCod, Texto, 0, 1);
  EdMoedaCod_TXT.Text := Texto;
end;

procedure TFmCNAB_Cfg.EdMultaTipoChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdMultaTipo.Text, FMultaTipo, Texto, 0, 1);
  EdMultaTipo_TXT.Text := Texto;
end;

procedure TFmCNAB_Cfg.EdProtesCodChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdProtesCod.Text, FProtesCod, Texto, 0, 1);
  EdProtesCod_TXT.Text := Texto;
end;

procedure TFmCNAB_Cfg.EdQuemDistrbChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdQuemDistrb.Text, FQuemDistrb, Texto, 0, 1);
  EdQuemDistrb_TXT.Text := Texto;
end;

procedure TFmCNAB_Cfg.EdQuemPrintChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdQuemPrint.Text, FQuemPrint, Texto, 0, 1);
  EdQuemPrint_TXT.Text := Texto;
end;

procedure TFmCNAB_Cfg.LiberaTextoLinha1;
var
  Banco, Instrucao1, Instrucao2, Instr: Integer;
begin
  Instr := 0;
  Banco := Geral.IMV(EdCedBanco.Text);
  Instrucao1 := Geral.IMV(EdInstrCobr1.Text);
  Instrucao2 := Geral.IMV(EdInstrCobr2.Text);
  if Instrucao1 = 94 then Instr := 94 else
  if Instrucao2 = 94 then Instr := 94 else
  if Instrucao1 = 93 then Instr := 93 else
  if Instrucao2 = 93 then Instr := 93;
  if Banco = 341 then
  begin
    if Instr = 94 then
    begin
      EdTexto01.Color     := FCorSim;
      EdTexto01.MaxLength := 40;
    end
    else if Instr = 93 then
    begin
      EdTexto01.Color     := FCorSim;
      EdTexto01.MaxLength := 30;
    end;
  end else EdTexto01.MaxLength := 0;
  if EdTexto01.MaxLength <> 0 then
  EdTexto01.Text := Copy(EdTexto01.Text, 1, EdTexto01.MaxLength);
end;

procedure TFmCNAB_Cfg.LbVarsDblClick(Sender: TObject);
begin
  UnDmkTags.AdicionaTag(LbVars, TMemo(FEdCondInst));
end;

function TFmCNAB_Cfg.TemInconsistencias(): Boolean;
var
  Banco, Instrucao1, Instrucao2: Integer;
begin
  Result := False;
  Banco := Geral.IMV(EdCedBanco.Text);
  Instrucao1 := Geral.IMV(EdInstrCobr1.Text);
  Instrucao2 := Geral.IMV(EdInstrCobr2.Text);
  case Banco of
    341:
    begin
      if ((Instrucao1 = 93) and (Instrucao2 = 94))
      or ((Instrucao1 = 94) and (Instrucao2 = 93)) then
      begin
        Result := True;
        Geral.MB_Aviso('As instru��es 93 e 94 n�o podem ser ' +
        'selecionadas ao mesmo tempo para o banco 341!');
      end;
    end;
    399:
    begin

      if ((Instrucao1 in ([15,16,19,22,24,29,73,74]))
      and (Instrucao2 in ([15,16,19,22,24,29,73,74])))

      or

         ((Instrucao1 in ([71,72]))
      and (Instrucao2 in ([71,72])))

      or

         ((Instrucao1 in ([75,76,77,84]))
      and (Instrucao2 in ([75,76,77,84]))) then

      Geral.MB_Aviso('As instru��es 1 e 2 est�o em conflito!');
    end;
  end;
end;

procedure TFmCNAB_Cfg.EdCedBancoChange(Sender: TObject);
begin
  ReconfiguraTudo();
  TextoTipoCobrBB();
end;

procedure TFmCNAB_Cfg.ReconfiguraTudo();
var
  //Posicoes,
  Comando, Banco, CNAB, ModalCobr: Integer;
  Carteira, NomeLayout: String;
begin
  if Length(EdCedBanco.Text) = 3 then
  begin
    ReconfiguraCoresEFormatacoes();
    Banco      := Geral.IMV(EdCedBanco.Text);
    CNAB       := Geral.IMV(RGCNAB.Items[RGCNAB.ItemIndex]);
    ModalCobr  := RGModalCobr.ItemIndex;
    Carteira   := EdCartCod.Text;
    Comando    := EdComando.ValueVariant;
    NomeLayout := EdLayoutRem.ValueVariant;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False,
      UBco_Rem.AvisoImplementacao(Banco, CNAB, ModalCobr, NomeLayout));
    FEspecieTit       := UBco_Rem.EspecieDoTitulo(Banco, CNAB, EdLayoutRem.Text);
    FEspecieVal       := UBco_Rem.EspecieDoValor(Banco, CNAB, EdLayoutRem.Text);
    FTipoCart         := UBco_Rem.TipoCart(Banco, CNAB, EdLayoutRem.Text);
    FInstruCobr       := UBco_Rem.InstrucaoDeCobranca(Banco, CNAB, Comando, EdLayoutRem.Text);
    FAvisosInstruCobr := UBco_Rem.AvisosDeInstrucaoDeCobranca(Banco, CNAB, EdLayoutRem.Text);
    FCarteiras        := UBco_Rem.CarteiraDeCobranca(Banco, CNAB, EdLayoutRem.Text);
    FAvisosCarteiras  := UBco_Rem.AvisosDeCarteiraDeCobranca(Banco, CNAB, EdLayoutRem.Text);

    if (Banco <> 0) and (CNAB <> 0) then
      FTipoCobrBB := UBco_Rem.TipoDeCobrancaBB(Banco, CNAB, Carteira);

    FComando          := UBco_Rem.Comando(Banco, CNAB, EdLayoutRem.Text);
    FDdProtesto       := UBco_Rem.DdProtesto(Banco, CNAB);
    //FCartCodBB        := UBco_Rem.CartCodBB(Banco, CNAB);
    //FCadTitBco        := UBco_Rem.CadastroDoTituloNoBanco(Banco, CNAB);
    FQuemDistrb       := UBco_Rem.QuemDistribuiBloqueto(Banco, CNAB, EdLayoutRem.Text);
    FQuemPrint        := UBco_Rem.QuemImprimeBloqueto(Banco, CNAB, EdLayoutRem.Text);
    FMultaTipo        := UBco_Rem.MultaTipo(Banco, CNAB, EdLayoutRem.Text);
    FJurosTipo        := UBco_Rem.JurosTipo(Banco, CNAB, EdLayoutRem.Text);
    FDescontos        := UBco_Rem.DescontosTipos(Banco, CNAB, EdLayoutRem.Text);
    FProtesCod        := UBco_Rem.CodProtestoRem(Banco, CNAB, EdLayoutRem.Text);
    FBxaDevCod        := UBco_Rem.CodBaixaDevolRem(Banco, CNAB, EdLayoutRem.Text);
    FMoedaCod         := UBco_Rem.CodMoeda(Banco, CNAB);
    FVariacao         := UBco_Rem.VariacaoCarteira(Banco, CNAB);
    FLayoutRem1       := UBco_Rem.LayoutsRemessa(Banco, CNAB);
    FLayoutRet        := UBco_Rem.LayoutsRetorno(Banco, CNAB);
    FTipBloqUso       := UBco_Rem.TipoDeBloqutoUsado(Banco, CNAB);
    //
    EdEspecieTitChange(Self);
    EdInstrCobr1Change(Self);
    EdInstrCobr2Change(Self);
    EdCartNumChange(Self);
    EdTipoCobrBBChange(Self);
    EdComandoChange(Self);
    //EdCartCodBBChange(Self);
    EdTipoCartChange(Self);
    //EdCadTitBcoChange(Self);
    EdQuemDistrbChange(Self);
    EdQuemPrintChange(Self);
    EdMultaTipoChange(Self);
    EdJurosTipoChange(Self);
    EdDesco1CodChange(Self);
    EdDesco2CodChange(Self);
    EdDesco3CodChange(Self);
    EdProtesCodChange(Self);
    EdBxaDevCodChange(Self);
    EdMoedaCodChange(Self);
    EdVariacaoChange(Self);
    EdEspecieValChange(Self);
  end;
end;

procedure TFmCNAB_Cfg.RedefineInstrucoes();
var
  Texto, Aviso: String;
  Comando, Banco, CNAB: Integer;
begin
  CNAB        := Geral.IMV(RGCNAB.Items[RGCNAB.ItemIndex]);
  Comando     := EdComando.ValueVariant;
  Banco       := EdCedBanco.ValueVariant;
  FComando    := UBco_Rem.Comando(Banco, CNAB, EdLayoutRem.Text);
  FInstruCobr := UBco_Rem.InstrucaoDeCobranca(Banco, CNAB, Comando, EdLayoutRem.Text);
  FAvisosInstruCobr := UBco_Rem.AvisosDeInstrucaoDeCobranca(Banco, CNAB, EdLayoutRem.Text);
  //
  Geral.DescricaoDeArrStrStr(EdComando.Text, FComando, Texto, 0, 1);
  EdComando_TXT.Text := Texto;
  //
  Geral.DescricaoDeArrStrStr(EdInstrCobr1.Text, FInstruCobr, Texto, 0, 1);
  EdInstrCobr1_TXT.Text := Texto;
  Geral.DescricaoEAvisosDeArrStrStr(EdInstrCobr1.Text,
    FInstruCobr, FAvisosInstruCobr, Texto, Aviso, 0, 1, 2, 0, 1);
  EdInstrCobr1_TXT.Text := Texto;
  dmkMemo1.Text            := Aviso;
  //
  //
  Geral.DescricaoDeArrStrStr(EdInstrCobr2.Text, FInstruCobr, Texto, 0, 1);
  EdInstrCobr2_TXT.Text := Texto;
  Geral.DescricaoEAvisosDeArrStrStr(EdInstrCobr2.Text,
    FInstruCobr, FAvisosInstruCobr, Texto, Aviso, 0, 1, 2, 0, 1);
  EdInstrCobr2_TXT.Text := Texto;
  dmkMemo2.Text            := Aviso;
  //
  //
end;

procedure TFmCNAB_Cfg.ReopenCartRemessa(Cedente, SacadAvali: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCartRemessa, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM carteiras ',
    'WHERE Tipo=2 ',
    'AND (ForneceI=' + Geral.FF0(Cedente) + ' OR ForneceI=' + Geral.FF0(SacadAvali) + ')',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmCNAB_Cfg.ReopenProtocolCR(Cedente, SacadAvali: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrProtocolCR, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM protocolos ',
    'WHERE Ativo = 1 ',
    'AND Tipo = 4 ',
    'AND (Def_Client=' + Geral.FF0(Cedente) + ' OR Def_Client=' + Geral.FF0(SacadAvali) + ')',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmCNAB_Cfg.ReopenCartRetorno(Cedente, SacadAvali: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCartRetorno, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM carteiras ',
    'WHERE Tipo=1 ',
    'AND (ForneceI=' + Geral.FF0(Cedente) + ' OR ForneceI=' + Geral.FF0(SacadAvali) + ')',
    'ORDER BY Nome ', 
    '']);
end;

procedure TFmCNAB_Cfg.ReopenCNAB_CfOR(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCNAB_CfOR, Dmod.MyDB, [
  'SELECT oco.*, cta.Nome NOMEGENERO ',
  'FROM cnab_cfor oco ',
  'LEFT JOIN contas cta ON cta.Codigo=oco.Genero ',
  'WHERE oco.Codigo=' + Geral.FF0(QrCNAB_CfgCodigo.Value),
  '']);
  //
  QrCNAB_CfOR.Locate('Controle', Controle, []);
end;

procedure TFmCNAB_Cfg.RGCNABClick(Sender: TObject);
begin
  ReconfiguraTudo();
end;

procedure TFmCNAB_Cfg.RGModalCobrClick(Sender: TObject);
begin
  ConfiguraProtocoloCR;
  //
  ReconfiguraTudo();
end;

procedure TFmCNAB_Cfg.RGPosicoesBBClick(Sender: TObject);
begin
  if RGPosicoesBB.ItemIndex < 6 then
     RGPosicoesBB.ItemIndex := 6;
end;

procedure TFmCNAB_Cfg.EdEspecieTitChange(Sender: TObject);
var
  Texto1, Texto2, Texto3: String;
begin
  Geral.DescricaoDeArrStrStr3(EdEspecieTit.Text, FEspecieTit, Texto1, Texto2,
  Texto3, 0, 1, 2, 3);
  EdEspecieDoc.Text := Texto1;
  EdEspecieTit_TXT.Text := Texto2;
  EdEspecieTit_TXT2.Text := Texto3;
end;

procedure TFmCNAB_Cfg.EdEspecieValChange(Sender: TObject);
var
  Texto1, Texto2: String;
begin
  (*
  Geral.DescricaoDeArrStrStr(EdEspecieVal.Text, FEspecieVal, Texto, 0, 1);
  EdEspecieVal_TXT.Text := Texto;
  *)
  Geral.DescricaoDeArrStrStr2(EdEspecieVal.Text, FEspecieVal, Texto1, Texto2, 0, 1, 2);
  EdEspecieTxt.Text     := Texto1;
  EdEspecieVal_TXT.Text := Texto2;
end;

procedure TFmCNAB_Cfg.EdInstrCobr1Change(Sender: TObject);
var
  Texto, Aviso: String;
begin
  Geral.DescricaoEAvisosDeArrStrStr(EdInstrCobr1.Text,
    FInstruCobr, FAvisosInstruCobr, Texto, Aviso, 0, 1, 2, 0, 1);
  EdInstrCobr1_TXT.Text := Texto;
  dmkMemo1.Text            := Aviso;
  //
end;

procedure TFmCNAB_Cfg.EdInstrCobr2Change(Sender: TObject);
{
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdInstrCobr2.Text, FInstruCobr, Texto, 0, 1);
  EdInstrCobr2_TXT.Text := Texto;
}
var
  Texto, Aviso: String;
begin
  Geral.DescricaoEAvisosDeArrStrStr(EdInstrCobr2.Text,
    FInstruCobr, FAvisosInstruCobr, Texto, Aviso, 0, 1, 2, 0, 1);
  EdInstrCobr2_TXT.Text := Texto;
  dmkMemo2.Text            := Aviso;
  //
end;

procedure TFmCNAB_Cfg.FormDestroy(Sender: TObject);
begin
  FListaArq.Free;
end;

procedure TFmCNAB_Cfg.BtImportarClick(Sender: TObject);
  function Titulo(Valor: String; Banco: Integer): String;
  begin
    Result := 'REMESSA CNAB' + RGCNAB.Items[RGCNAB.ItemIndex] + ' - ' +
    Valor + ' (' + FormatFloat('000', Banco) + ')';
  end;
const
  // Parei Aqui! Fazer ???
  Posicoes = -1;
var
  Valor, NomeLayout: String;
  Campo, i, j, f, CNAB, Banco, Ini, Tam, Entidade: Integer;
  ListaLayout: MyArrayLista;
  CNAB_res: TCNABResult;
  Foi1, Foi5: Boolean;
  TitCols: array[0..1] of String;
begin
  if OpenDialog1.Execute then
  begin
    Foi1 := False;
    Foi5 := False;
    FListaArq.LoadFromFile(OpenDialog1.FileName);
    MeLines.Lines := FListaArq;
    EdArq.Text := OpenDialog1.FileName;
    //
    if not UBancos.TipoArqCNAB(OpenDialog1.FileName, FListaArq, CNAB) then Exit;
    case CNAB of
      240: RGCNAB.ItemIndex := 1;
      400: RGCNAB.ItemIndex := 2;
      else RGCNAB.ItemIndex := 0;
    end;
    if CNAB <> 400 then
    begin
      Geral.MB_Aviso('O tipo de arquivo CNAB' + FormatFloat('000',
      CNAB) + ' n�o est� implementado!');
      Exit;
    end else begin
      Valor := Copy(FListaArq[0], 077, 003);
      Banco := Geral.IMV(Valor);
      EdCedBanco.Text := FormatFloat('000', Banco);
      CBCedBanco.KeyValue := Banco;
      ReconfiguraCoresEFormatacoes();
      // Ver o que fazer!
      //
      TitCols[0] := 'C�d.';
      TitCols[1] := 'Descri��o';
      NomeLayout := Geral.SelecionaItem(FLayoutRem1, 0,
      'SEL-LISTA-000 :: Layout de Remessa', TitCols, Screen.Width);
      //
      CNAB_res := UBco_Rem.LayoutCNAB400(Banco, 0, ListaLayout, Posicoes, NomeLayout);
      if not (CNAB_res  in ([cresNoField, cresOKField])) then
      begin
        UBancos.CNABResult_Msg(CNAB_res, CNAB, Banco, 0, ecnabRemessa);
        Exit;
      end;
      //
      Valor := Copy(FListaArq[0], 002, 001);
      if Valor <> '1' then
      begin
        Geral.MB_Aviso('O arquivo "' + OpenDialog1.FileName +
        '" n�o � arquivo de remessa!');
        Exit;
      end;
      //
      Valor := Copy(FListaArq[0], 010, 002);
      if Valor <> '01' then
      begin
        Geral.MB_Aviso('O arquivo "' + OpenDialog1.FileName +
        '" n�o � arquivo de cobran�a!');
        Exit;
      end;
      //
      for i := Low(ListaLayout) to High(ListaLayout) do
      begin
        Campo := Geral.IMV(ListaLayout[i][_fld]);
        Ini   := Geral.IMV(ListaLayout[i][_ini]);
        Tam   := Geral.IMV(ListaLayout[i][_Tam]);
        Valor := Copy(FListaArq[0], Ini, Tam);
        case Campo of
          //
          020: EdCedAgencia.Text := Valor;
          021: EdCedConta.Text   := Valor;
          022: EdCedDAC_A.Text   := Valor;
          023: EdCedDAC_C.Text   := Valor;
          024: EdCedDAC_AC.Text  := Valor;
          402: EdNome.Text       := Titulo(Valor, Banco);
          410: EdCodEmprBco.Text := Valor;
        end;
      end;
      //
      CNAB_res := UBco_Rem.LayoutCNAB400(Banco, 1, ListaLayout, Posicoes, NomeLayout);
      if not (CNAB_res  in ([cresNoField, cresOKField])) then
      begin
        UBancos.CNABResult_Msg(CNAB_res, CNAB, Banco, 0, ecnabRemessa);
        Exit;
      end;
      //
      for j := 1 to FListaArq.Count - 1 do
      begin
        f := Geral.IMV(FListaArq[j][1]);
        if (f = 1) and (Foi1 = False) then
        begin
          Foi1 := True;
          for i := Low(ListaLayout) to High(ListaLayout) do
          begin
            Campo := Geral.IMV(ListaLayout[i][_fld]);
            Ini   := Geral.IMV(ListaLayout[i][_ini]);
            Tam   := Geral.IMV(ListaLayout[i][_Tam]);
            Valor := Copy(FListaArq[j], Ini, Tam);
            case Campo of
              //
              020: EdCedAgencia.Text := Valor;
              021: EdCedConta.Text   := Valor;
              022: EdCedDAC_A.Text   := Valor;
              023: EdCedDAC_C.Text   := Valor;
              024: EdCedDAC_AC.Text  := Valor;
              401:
              begin
                if DmBco.PesquisaEntidadePorCNPJ(Valor, Entidade) then
                begin
                  EdCedente.Text     := IntToStr(Entidade);
                  CBCedente.KeyValue := Entidade;
                end else Geral.MB_Aviso(
                'N�o foi localizada nenhuma entidade para CPF / CNPJ ' +
                Geral.FormataCNPJ_TT(Valor) + '!');
              end;
              //410: EdCodEmprBco.Text := Valor;
              507: EdEspecieTit.Text := Valor;
              508: EdCartCod.Text := Valor;
              509: EdCartNum.Text := Valor;
              520: if (Valor = 'A') or (Valor = 'S') or (Valor = 'Y') or (Valor = '1') then RGAceiteTit.ItemIndex := 1;

              701: EdInstrCobr1.Text := Valor;
              702: EdInstrCobr1.Text := Valor;
              {
              : .Text := Valor;
              : .Text := Valor;
              : .Text := Valor;
              : .Text := Valor;
              : .Text := Valor;
              }
            end;
          end;
        end
        else if (f = 5) and (Foi5 = False) then
        begin
          Foi5 := True;
        end;
      end;
    end;
  end;
end;

procedure TFmCNAB_Cfg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
  //
  procedure MensagemVerifiLayout(TextoInstrucao: String);
  begin
    if (Trim(EdLayoutRem.Text) = '') and (Trim(TextoInstrucao) = '') then
      Geral.MB_Aviso(
      'Verifique se o layout de remessa foi definido corretamente!');
  end;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    //
    if EdEspecieTit.Focused then
      EdEspecieTit.Text := Geral.SelecionaItem(FEspecieTit, 0,
      'SEL-LISTA-000 :: Sele��o da esp�cie do T�tulo', TitCols, Screen.Width)
    else
    if EdTipoCart.Focused then
      EdTipoCart.Text := Geral.SelecionaItem(FTipoCart, 0,
      'SEL-LISTA-000 :: Sele��o da esp�cie do Valor', TitCols, Screen.Width)
    else
    if EdEspecieVal.Focused then
      EdEspecieVal.Text := Geral.SelecionaItem(FEspecieVal, 0,
      'SEL-LISTA-000 :: Sele��o da esp�cie do Valor', TitCols, Screen.Width)
    else
    if EdInstrCobr1.Focused then
    begin
      RedefineInstrucoes();
      EdInstrCobr1.Text := Geral.SelecionaItem(FInstruCobr, 0,
      'SEL-LISTA-000 :: Sele��o da instru��o de cobran�a 1', TitCols, Screen.Width);
      MensagemVerifiLayout(EdInstrCobr1.Text);
    end
    else
    if EdInstrCobr2.Focused then
    begin
      RedefineInstrucoes();
      EdInstrCobr2.Text := Geral.SelecionaItem(FInstruCobr, 0,
      'SEL-LISTA-000 :: Sele��o da instru��o de cobran�a 2', TitCols, Screen.Width);
      MensagemVerifiLayout(EdInstrCobr2.Text);
    end
    else
    if EdTipoCart.Focused or EdCartNum.Focused or EdCartCod.Focused then
      UBco_Rem.SelecionaItensCarteira(FCarteiras, EdTipoCart, TEdit(EdCartNum),
        TEdit(EdCartCod), TEdit(EdCartTxt), TEdit(EdCarteira_TXT))
    else
    if EdTipoCobrBB.Focused then
      EdTipoCobrBB.Text := Geral.SelecionaItem(FTipoCobrBB, 0,
      'SEL-LISTA-000 :: Sele��o da tipo de cobran�a BB', TitCols, Screen.Width)
    else
    if EdComando.Focused then
      EdComando.Text := Geral.SelecionaItem(FComando, 0,
      'SEL-LISTA-000 :: Comando', TitCols, Screen.Width)
    else
    if EdDdProtesBB.Focused then
      EdDdProtesBB.Text := Geral.SelecionaItem(FDdProtesto, 0,
      'SEL-LISTA-000 :: Dias para protesto', TitCols, Screen.Width)
    else
    {
    if EdCartCodBB.Focused then
      EdCartCodBB.Text := Geral.SelecionaItem(FCartCodBB,
      'SEL-LISTA-000 :: C�digo da Carteira', TitCols, Screen.Width)
    else
    if EdCadTitBco.Focused then
      EdCadTitBco.Text := Geral.SelecionaItem(FCadTitBco,
      'SEL-LISTA-000 :: Cadastro do t�tulo no banco', TitCols, Screen.Width)
    else
    }
    if EdQuemDistrb.Focused then
      EdQuemDistrb.Text := Geral.SelecionaItem(FQuemDistrb, 0,
      'SEL-LISTA-000 :: Quem distribui o bloqueto', TitCols, Screen.Width)
    else
    if EdQuemPrint.Focused then
      EdQuemPrint.Text := Geral.SelecionaItem(FQuemPrint, 0,
      'SEL-LISTA-000 :: Quem imprime', TitCols, Screen.Width)
    else
    if EdJurosTipo.Focused then
      EdJurosTipo.Text := Geral.SelecionaItem(FJurosTipo, 0,
      'SEL-LISTA-000 :: Cobran�a de juros', TitCols, Screen.Width)
    else
    if EdMultaTipo.Focused then
      EdMultaTipo.Text := Geral.SelecionaItem(FMultaTipo, 0,
      'SEL-LISTA-000 :: Cobran�a de multa', TitCols, Screen.Width)
    else
    if EdDesco1Cod.Focused then
      EdDesco1Cod.Text := Geral.SelecionaItem(FDescontos, 0,
      'SEL-LISTA-000 :: Desconto 1', TitCols, Screen.Width)
    else
    if EdDesco2Cod.Focused then
      EdDesco2Cod.Text := Geral.SelecionaItem(FDescontos, 0,
      'SEL-LISTA-000 :: Desconto 2', TitCols, Screen.Width)
    else
    if EdDesco3Cod.Focused then
      EdDesco3Cod.Text := Geral.SelecionaItem(FDescontos, 0,
      'SEL-LISTA-000 :: Desconto 3', TitCols, Screen.Width)
    else
    if EdProtesCod.Focused then
      EdProtesCod.Text := Geral.SelecionaItem(FProtesCod, 0,
      'SEL-LISTA-000 :: Protesto', TitCols, Screen.Width)
    else
    if EdBxaDevCod.Focused then
      EdBxaDevCod.Text := Geral.SelecionaItem(FBxaDevCod, 0,
      'SEL-LISTA-000 :: Baixa / devolu��o', TitCols, Screen.Width)
    else
    if EdMoedaCod.Focused then
      EdMoedaCod.Text := Geral.SelecionaItem(FMoedaCod, 0,
      'SEL-LISTA-000 :: C�digo da Moeda', TitCols, Screen.Width)
    else
    if EdVariacao.Focused then
      EdVariacao.Text := Geral.SelecionaItem(FVariacao, 0,
      'SEL-LISTA-000 :: Varia��o', TitCols, Screen.Width)
    else
    if EdLayoutRem.Focused then
      EdLayoutRem.Text := Geral.SelecionaItem(FLayoutRem1, 0,
      'SEL-LISTA-000 :: Layout de Remessa', TitCols, Screen.Width)
    else
    if EdLayoutRet.Focused then
      EdLayoutRet.Text := Geral.SelecionaItem(FLayoutRet, 0,
      'SEL-LISTA-000 :: Layout de Retorno', TitCols, Screen.Width)
    else
    if EdTipBloqUso.Focused then
      EdTipBloqUso.Text := Geral.SelecionaItem(FTipBloqUso, 0,
      'SEL-LISTA-000 :: Layout de Retorno', TitCols, Screen.Width)
    else
    //if ?
  end;
end;

procedure TFmCNAB_Cfg.EdBxaDevCodChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdBxaDevCod.Text, FBxaDevCod, Texto, 0, 1);
  EdBxaDevCod_TXT.Text := Texto;
end;

procedure TFmCNAB_Cfg.EdCartNumChange(Sender: TObject);
var
  Texto, Aviso, Codigo, TipoTxt: String;
  TipoInt: Integer;
begin
  //Geral.DescricaoEAvisosDeArrStrStr(EdCartNum.Text,
   // FCarteiras, FAvisosCarteiras, Texto, Aviso, 1, 3, 4, 0, 1);
  //EdCarteira_TXT.Text      := Texto;
  //dmkMeCarteira.Text       := Aviso;
  //
{
Res[Linha][0] :=  Codigo;
Res[Linha][1] :=  Carteira;
Res[Linha][2] :=  Tipo; // E-Escritural S-Simples D-Direta
Res[Linha][3] :=  Descricao;
Res[Linha][4] :=  Coment;
}
  Geral.DescricaoEAvisosEItensDeArrStrStr(EdCartNum.Text, FCarteiras,
    FAvisosCarteiras, Texto, Aviso, Codigo, TipoTxt, 1, 4, 5, 0, 1, 0, 3);
  if (EdCedBanco.ValueVariant <> 0) and
    (UBco_Rem.TipoDeCarteiraRemessa(EdCedBanco.ValueVariant, TipoTxt, TipoInt)) then
  begin
    EdCarteira_TXT.Text      := Texto;
    MeCarteira.Text          := Aviso;
    EdCartCod.Text           := Codigo;
    if TipoInt > -2 then
      EdTipoCart.ValueVariant   := TipoInt;
  end else begin
    EdCarteira_TXT.Text      := '';
    MeCarteira.Text          := '';
  end;
  TextoTipoCobrBB();
end;

procedure TFmCNAB_Cfg.SpeedButton5Click(Sender: TObject);
const
  Dir = 'C:\Dermatek\CNAB\Remessa\';
var
  Arquivo: String;
begin
  ForceDirectories(Dir);
  Arquivo := '';
  if MyObjects.FileOpenDialog(FmCNAB_Cfg, Dir, Arquivo,
  'Informe o diret�rio de envio do arquivo CNAB', '', [], Arquivo) then
    EdDiretorio.Text := ExtractFileDir(Arquivo);
end;

procedure TFmCNAB_Cfg.SpeedButton6Click(Sender: TObject);
const
  Dir = 'C:\Dermatek\CNAB\Retorno\';
var
  Arquivo: String;
begin
  ForceDirectories(Dir);
  Arquivo := '';
  if MyObjects.FileOpenDialog(FmCNAB_Cfg, Dir, Arquivo,
  'Informe o diret�rio de retorno do arquivo CNAB', '', [], Arquivo) then
    EdDirRetorno.Text := ExtractFileDir(Arquivo);
end;

procedure TFmCNAB_Cfg.SpeedButton7Click(Sender: TObject);
  {$IfNDef NO_FINANCEIRO}
var
  CartRetorno, Cedente, SacadAvali: Integer;
begin
  VAR_CADASTRO := 0;
  Cedente      := EdCedente.ValueVariant;
  SacadAvali   := EdSacadAvali.ValueVariant;
  CartRetorno  := EdCartRetorno.ValueVariant;
  //
  FinanceiroJan.CadastroDeCarteiras(CartRetorno);
  //
  if VAR_CADASTRO <> 0 then
  begin
    ReopenCartRetorno(Cedente, SacadAvali);
    ReopenCartRemessa(Cedente, SacadAvali);
    //
    UMyMod.SetaCodigoPesquisado(EdCartRetorno, CBCartRetorno, QrCartRetorno, VAR_CADASTRO);
    CBCartRetorno.SetFocus;
  end;
{$Else}
begin
  Grl_Geral.InfoSemModulo(mdlappFinanceiro);
{$EndIf}
end;

procedure TFmCNAB_Cfg.SpeedButton8Click(Sender: TObject);
  {$IfNDef NO_FINANCEIRO}
var
  CartEmiss, Cedente, SacadAvali: Integer;
begin
  VAR_CADASTRO := 0;
  Cedente      := EdCedente.ValueVariant;
  SacadAvali   := EdSacadAvali.ValueVariant;
  CartEmiss    := EdCartEmiss.ValueVariant;
  //
  FinanceiroJan.CadastroDeCarteiras(CartEmiss);
  //
  if VAR_CADASTRO <> 0 then
  begin
    ReopenCartRemessa(Cedente, SacadAvali);
    ReopenCartRetorno(Cedente, SacadAvali);
    //
    UMyMod.SetaCodigoPesquisado(EdCartEmiss, CBCartEmiss, QrCartRemessa, VAR_CADASTRO);
    CBCartEmiss.SetFocus;
  end;
{$Else}
begin
  Grl_Geral.InfoSemModulo(mdlappFinanceiro);
{$EndIf}
end;

procedure TFmCNAB_Cfg.SpeedButton9Click(Sender: TObject);
var
  Banco, CNAB: Integer;
  NomeLayout, Msg: String;
begin
  Banco := EdCedBanco.ValueVariant;
  NomeLayout := EdLayoutRem.Text;
  CNAB := Geral.IMV(RGCNAB.Items[RGCNAB.ItemIndex]);
  if MyObjects.FIC(Banco = 0, EdCedBanco, 'Informe o banco')  then Exit;
  if MyObjects.FIC(Trim(NomeLayout) = '', EdLayoutRem, 'Informe o Layout de remessa')  then Exit;
  if MyObjects.FIC(RGCNAB.ItemIndex < 1, nil, 'Informe o CNAB') then Exit;
  //
  Msg := '';
  case Banco of
    008, 033, 353:
    begin
      if CNAB = 240 then
        if NomeLayout = CO_033_RECEBIMENTOS_240_v01_07_2009_06 then Msg :=
          'Tipo de campo: C�DIGO DE TRANSMISS�O 15 d�gitos' + sLineBreak +
          //'Valor do campo: Concatene o c�digo da ag�ncia com o n�mero da conta.' +
          sLineBreak + 'Tamanho do texto: 15 caracteres' (*+ sLineBreak + 'Exemplo: ' + sLineBreak +
          'Ag�ncia 4321; Conta 56789-00; ent�o o valor do campo ser�: 43215678900'*);
      if CNAB = 400 then
        if NomeLayout = CO_033_RECEBIMENTOS_400_v02_00_2009_10 then Msg :=
          'Tipo de campo: C�DIGO DE TRANSMISS�O 20 d�gitos' + sLineBreak +
          //'Valor do campo: Concatene o c�digo da ag�ncia com o n�mero da conta.' +
          sLineBreak + 'Tamanho do texto: 20 caracteres' (*+ sLineBreak + 'Exemplo: ' + sLineBreak +
          'Ag�ncia 4321; Conta 56789-00; ent�o o valor do campo ser�: 43215678900'*);
    end;
    399:
    begin
      if CNAB = 400 then
        if NomeLayout = CO_399_MODULO_I_2009_10 then Msg :=
          'Tipo de campo: Ag�ncia + Conta Corrente' + sLineBreak +
          'Valor do campo: Concatene o c�digo da ag�ncia com o n�mero da conta.' +
          sLineBreak + 'Tamanho do texto: 11 caracteres' + sLineBreak + 'Exemplo: ' + sLineBreak +
          'Ag�ncia 4321; Conta 56789-00; ent�o o valor do campo ser�: 43215678900';
    end;
  end;
  if Msg = '' then
    Msg := 'N�o h� ajuda para esta configura�ao de Banco / CNAB/ Layout';
  //
  Geral.MB_Info(Msg);
end;

{
SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMEENT,
car.Nome NOMECARTEIRA, ban.Nome NOMEBANCO,
car.Banco1, ent.CliInt, dir.*
FROM cnab_cfg dir
LEFT JOIN carteiras car ON car.Codigo=dir.cartRetorno
LEFT JOIN bancos ban ON ban.Codigo=car.Banco1
/*LEFT JOIN cond cnd ON cnd.Cliente=dir.CliInt*/

LEFT JOIN entidades ent ON ent.Codigo=dir.CliInt
WHERE dir.Ativo=1
/*AND dir.Envio=2*/
ORDER BY NOMEENT
}
end.



