unit CNAB_Ret2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, DBCtrls, Grids,
  DBGrids, math, ComCtrls, Menus, dmkDBGrid, Variants, frxCross, frxClass, Mask,
  ABSMain, frxDBSet, dmkGeral, dmkEdit, UnDmkProcFunc, UnDmkEnums, DmkDAC_PF;

type
  TFmCNAB_Ret2 = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    Timer1: TTimer;
    PnCarrega: TPanel;
    PainelConfirma: TPanel;
    BtCarrega: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Button1: TButton;
    PnArquivos: TPanel;
    ListBox1: TListBox;
    MemoTam: TMemo;
    QrDupl: TmySQLQuery;
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    QrSumPg: TmySQLQuery;
    QrSumPgJuros: TFloatField;
    QrSumPgPago: TFloatField;
    QrSumPgDesco: TFloatField;
    QrSumPgValor: TFloatField;
    QrSumPgMaxData: TDateField;
    PnMovimento: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    BitBtn2: TBitBtn;
    Button2: TButton;
    QrOcorreu: TmySQLQuery;
    QrOcorreuNOMEOCORRENCIA: TWideStringField;
    QrOcorreuCodigo: TIntegerField;
    QrOcorreuLotesIts: TIntegerField;
    QrOcorreuDataO: TDateField;
    QrOcorreuOcorrencia: TIntegerField;
    QrOcorreuValor: TFloatField;
    QrOcorreuLoteQuit: TIntegerField;
    QrOcorreuLk: TIntegerField;
    QrOcorreuDataCad: TDateField;
    QrOcorreuDataAlt: TDateField;
    QrOcorreuUserCad: TIntegerField;
    QrOcorreuUserAlt: TIntegerField;
    QrOcorreuTaxaP: TFloatField;
    QrOcorreuTaxaV: TFloatField;
    QrOcorreuPago: TFloatField;
    QrOcorreuDataP: TDateField;
    QrOcorreuTaxaB: TFloatField;
    QrOcorreuATUALIZADO: TFloatField;
    QrOcorreuCLIENTELOTE: TIntegerField;
    QrOcorreuData3: TDateField;
    QrOcorreuStatus: TSmallintField;
    QrOcorreuCliente: TIntegerField;
    QrOcorreuSALDO: TFloatField;
    QrOcorreuATZ_TEXTO: TWideStringField;
    DsOcorreu: TDataSource;
    QrOcorDupl: TmySQLQuery;
    QrOcorDuplNOMEOCORBANK: TWideStringField;
    QrOcorDuplValOcorBank: TFloatField;
    QrOcorDuplOcorrbase: TIntegerField;
    QrOcorBank: TmySQLQuery;
    QrOcorBankCodigo: TIntegerField;
    QrOcorBankNome: TWideStringField;
    QrOcorBankLk: TIntegerField;
    QrOcorBankDataCad: TDateField;
    QrOcorBankDataAlt: TDateField;
    QrOcorBankUserCad: TIntegerField;
    QrOcorBankUserAlt: TIntegerField;
    QrOcorBankBase: TFloatField;
    QrOcorBankEnvio: TIntegerField;
    QrOcorBankMovimento: TIntegerField;
    QrOcorBankFormaCNAB: TSmallintField;
    Timer2: TTimer;
    QrOB2: TmySQLQuery;
    QrOB2Nome: TWideStringField;
    PageControl1: TPageControl;
    QrLocEnt1: TmySQLQuery;
    QrLocEnt1NOMEENT: TWideStringField;
    QrLocEnt1CLIENTE: TIntegerField;
    Splitter1: TSplitter;
    TabSheet3: TTabSheet;
    Grade1: TStringGrid;
    QrLei: TmySQLQuery;
    DsLei: TDataSource;
    QrLeiCodigo: TIntegerField;
    QrLeiBanco: TIntegerField;
    QrLeiSeuNum: TIntegerField;
    QrLeiOcorrData: TDateField;
    QrLeiValTitul: TFloatField;
    QrLeiValAbati: TFloatField;
    QrLeiValDesco: TFloatField;
    QrLeiValPago: TFloatField;
    QrLeiValJuros: TFloatField;
    QrLeiValMulta: TFloatField;
    QrLeiMotivo1: TWideStringField;
    QrLeiMotivo2: TWideStringField;
    QrLeiMotivo3: TWideStringField;
    QrLeiMotivo4: TWideStringField;
    QrLeiMotivo5: TWideStringField;
    QrLeiQuitaData: TDateField;
    QrLeiDiretorio: TIntegerField;
    QrLeiArquivo: TWideStringField;
    QrLeiItemArq: TIntegerField;
    QrLeiStep: TSmallintField;
    QrLeiLk: TIntegerField;
    QrLeiDataCad: TDateField;
    QrLeiDataAlt: TDateField;
    QrLeiUserCad: TIntegerField;
    QrLeiUserAlt: TIntegerField;
    TabSheet1: TTabSheet;
    BtAbertos: TBitBtn;
    QrLeiIDNum: TIntegerField;
    PnCarregados: TPanel;
    Panel5: TPanel;
    PB1: TProgressBar;
    QrLeiEntidade: TIntegerField;
    QrLeiNOMEENT: TWideStringField;
    DsLeiItens: TDataSource;
    QrBcocor: TmySQLQuery;
    QrBcocorCarrega: TSmallintField;
    StaticText1: TStaticText;
    BtExclui: TBitBtn;
    PMExclui: TPopupMenu;
    ExcluiAtual1: TMenuItem;
    ExcluiSelecionados1: TMenuItem;
    ExcluiTodos1: TMenuItem;
    BtConcilia: TBitBtn;
    PMConcilia: TPopupMenu;
    ConciliaAtual1: TMenuItem;
    ConciliaSelecionados1: TMenuItem;
    ConciliaTodos1: TMenuItem;
    QrLeiOcorrCodi: TWideStringField;
    QrLocCta: TmySQLQuery;
    QrTem: TmySQLQuery;
    QrTemItens: TLargeintField;
    BitBtn1: TBitBtn;
    QrLeiCarteira: TIntegerField;
    QrLeiTipoCart: TIntegerField;
    QrLEB: TmySQLQuery;
    QrLEBEntidade: TIntegerField;
    QrLUH: TmySQLQuery;
    QrLUHApto: TIntegerField;
    QrLUHPropriet: TIntegerField;
    Splitter2: TSplitter;
    CkReverter: TCheckBox;
    QrDuplCodigo: TIntegerField;
    QrLeiDevJuros: TFloatField;
    QrLeiDevMulta: TFloatField;
    QrLeiDJM: TFloatField;
    QrPesq2: TmySQLQuery;
    QrPesq2Vencimento: TDateField;
    QrPesq2PercMulta: TFloatField;
    QrPesq2PercJuros: TFloatField;
    Label1: TLabel;
    QrBco: TmySQLQuery;
    QrBcoPadrIni: TIntegerField;
    QrBcoPadrTam: TIntegerField;
    QrBcoBcoOrig: TIntegerField;
    QrLeiValJuMul: TFloatField;
    QrLeiValOutro: TFloatField;
    QrLeiValTarif: TFloatField;
    Label2: TLabel;
    Label3: TLabel;
    BtAgenda: TBitBtn;
    QrPesq2Credito: TFloatField;
    QrLeiNossoNum: TWideStringField;
    QrLeiDtaTarif: TDateField;
    QrLeiDTA_TARIF_TXT: TWideStringField;
    QrPesq3: TmySQLQuery;
    QrPesq3Credito: TFloatField;
    QrPesq3PercMulta: TFloatField;
    QrPesq3PercJuros: TFloatField;
    QrPesq3Vencimento: TDateField;
    QrLocCtaMensal: TWideStringField;
    QrLeiDescriCNR: TWideStringField;
    QrLocEnt2: TmySQLQuery;
    QrLocEnt2NOMEENT: TWideStringField;
    QrLocEnt2CLIENTE: TIntegerField;
    TabSheet2: TTabSheet;
    Memo2: TMemo;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    DBGLei: TdmkDBGrid;
    dmkDBGrid1: TdmkDBGrid;
    N1: TMenuItem;
    ExcluiBloquetosnolocalizados1: TMenuItem;
    BtItens: TBitBtn;
    PMItens: TPopupMenu;
    Alteravalordoitemdearrecadaoselecionado1: TMenuItem;
    Excluioitemdearrecadaoselecionado1: TMenuItem;
    Excluso1: TMenuItem;
    Ajustavaloresdobloquetoatual1: TMenuItem;
    PageControl4: TPageControl;
    TabSheet6: TTabSheet;
    DBGrid2: TDBGrid;
    TabSheet7: TTabSheet;
    QrLeiAgr: TmySQLQuery;
    DsLeiAgr: TDataSource;
    DBGrid3: TDBGrid;
    QrLeiAgrNOMEPROPRIET: TWideStringField;
    QrLeiAgrValor: TFloatField;
    QrLeiAgrData: TDateField;
    QrLeiAgrMez: TIntegerField;
    QrLeiAgrCliInt: TIntegerField;
    QrLeiAgrForneceI: TIntegerField;
    QrLeiAgrApto: TIntegerField;
    QrLeiAgrUH: TWideStringField;
    QrLeiAgrTIPO_BLOQ: TLargeintField;
    QrLeiAgrNOME_TIPO_BLOQ: TWideStringField;
    QrLeiAgrMez_TXT: TWideStringField;
    Splitter3: TSplitter;
    GradeA: TStringGrid;
    QrLeiTamReg: TIntegerField;
    DsCNAB_Dir: TDataSource;
    QrCNAB_Dir: TmySQLQuery;
    TabSheet8: TTabSheet;
    Memo3: TMemo;
    QrLeiItens: TmySQLQuery;
    QrLeiItensData: TDateField;
    QrLeiItensCarteira: TIntegerField;
    QrLeiItensGenero: TIntegerField;
    QrLeiItensDescricao: TWideStringField;
    QrLeiItensCredito: TFloatField;
    QrLeiItensCompensado: TDateField;
    QrLeiItensSit: TIntegerField;
    QrLeiItensVencimento: TDateField;
    QrLeiItensMez: TIntegerField;
    QrLeiItensFornecedor: TIntegerField;
    QrLeiItensNOMEPROPRIET: TWideStringField;
    QrLeiItensControle: TIntegerField;
    QrLeiItensSub: TSmallintField;
    QrLeiItensTipoCart: TIntegerField;
    QrLeiItensDebito: TFloatField;
    QrLeiItensNotaFiscal: TIntegerField;
    QrLeiItensSerieCH: TWideStringField;
    QrLeiItensDocumento: TFloatField;
    QrLeiItensCliente: TIntegerField;
    QrLeiItensCliInt: TIntegerField;
    QrLeiItensForneceI: TIntegerField;
    QrLeiItensDataDoc: TDateField;
    QrLeiItensApto: TIntegerField;
    QrLeiItensUH: TWideStringField;
    QrLeiItensMez_TXT: TWideStringField;
    QrLeiItensMulta: TFloatField;
    QrLeiItensCOND: TIntegerField;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrLeiSum: TmySQLQuery;
    QrLeiSumCredito: TFloatField;
    QrLeiSumID600: TFloatField;
    QrLeiSumID601: TFloatField;
    QrLeiSumID610: TFloatField;
    QrClientesNOMEENT: TWideStringField;
    QrClientesEntidade: TIntegerField;
    QrClientesCODIGO: TIntegerField;
    frxCrossObject1: TfrxCrossObject;
    QrLeiItensNOMECART: TWideStringField;
    QrLeiItensCartDest: TIntegerField;
    Panel6: TPanel;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    Label7: TLabel;
    DBEdit4: TDBEdit;
    BtBuffer: TBitBtn;
    PMBuffer: TPopupMenu;
    Limpabuffer1: TMenuItem;
    Adicionadados1: TMenuItem;
    Imprimelista1: TMenuItem;
    frxDsQuery: TfrxDBDataset;
    frxGrade: TfrxReport;
    Ordenarpelonmerodobloqueto1: TMenuItem;
    Query: TABSQuery;
    QueryBloqueto: TIntegerField;
    QueryValTitul: TFloatField;
    QueryValPago: TFloatField;
    QueryValMulta: TFloatField;
    QueryValJuros: TFloatField;
    QueryValOutros: TFloatField;
    QueryValJurMul: TFloatField;
    QueryValERRO: TFloatField;
    QueryValTarifa: TFloatField;
    QueryOcorrCod: TWideStringField;
    QueryOcorrTxt: TWideStringField;
    QueryAtivo: TSmallintField;
    QuerySEQ: TIntegerField;
    Ordenarpeladatadecrdito1: TMenuItem;
    Ordenarpeladatadepagamento1: TMenuItem;
    QueryDataOcorr: TDateField;
    QueryDataQuita: TDateField;
    QueryDataTarif: TDateField;
    QrLocByCed: TmySQLQuery;
    QrLocByCedCNPJ_CPF: TWideStringField;
    CorrigeValTitulopoisobancoenviouerrado1: TMenuItem;
    Panel7: TPanel;
    EdCliInt: TdmkEdit;
    EdNomeEnt: TdmkEdit;
    EdNOMECARTEIRA: TdmkEdit;
    EdCodigo: TdmkEdit;
    EdBanco1: TdmkEdit;
    EdNome: TdmkEdit;
    EdNomeBanco: TdmkEdit;
    QrCNAB_DirNOMEENT: TWideStringField;
    QrCNAB_DirNome: TWideStringField;
    QrCNAB_DirCarteira: TIntegerField;
    QrCNAB_DirCliInt: TIntegerField;
    QrCNAB_DirBanco1: TIntegerField;
    QrCNAB_DirCodigo: TIntegerField;
    QrCNAB_DirNOMECARTEIRA: TWideStringField;
    QrCNAB_DirNOMEBANCO: TWideStringField;
    QrLocEnt1CliInt: TIntegerField;
    QrLocEnt2CliInt: TIntegerField;
    QrLeiItensTipo: TSmallintField;
    MeAvisos: TMemo;
    QrLei3: TmySQLQuery;
    DsLei3: TDataSource;
    QrOcorreu3: TmySQLQuery;
    DsOcorreu3: TDataSource;
    QrOcorreu3NOMEOCORRENCIA: TWideStringField;
    QrOcorreu3Codigo: TIntegerField;
    QrOcorreu3LotesIts: TIntegerField;
    QrOcorreu3DataO: TDateField;
    QrOcorreu3Ocorrencia: TIntegerField;
    QrOcorreu3Valor: TFloatField;
    QrOcorreu3LoteQuit: TIntegerField;
    QrOcorreu3Lk: TIntegerField;
    QrOcorreu3DataCad: TDateField;
    QrOcorreu3DataAlt: TDateField;
    QrOcorreu3UserCad: TIntegerField;
    QrOcorreu3UserAlt: TIntegerField;
    QrOcorreu3TaxaP: TFloatField;
    QrOcorreu3TaxaV: TFloatField;
    QrOcorreu3Pago: TFloatField;
    QrOcorreu3DataP: TDateField;
    QrOcorreu3TaxaB: TFloatField;
    QrOcorreu3ATUALIZADO: TFloatField;
    QrOcorreu3CLIENTELOTE: TIntegerField;
    QrOcorreu3Data3: TDateField;
    QrOcorreu3Status: TSmallintField;
    QrOcorreu3Cliente: TIntegerField;
    QrOcorreu3SALDO: TFloatField;
    QrOcorreu3ATZ_TEXTO: TWideStringField;
    QrLei3NOMECLI: TWideStringField;
    QrLei3MeuLote: TIntegerField;
    QrLei3Duplicata: TWideStringField;
    QrLei3CPF: TWideStringField;
    QrLei3Emitente: TWideStringField;
    QrLei3Bruto: TFloatField;
    QrLei3Desco: TFloatField;
    QrLei3Quitado: TIntegerField;
    QrLei3Valor: TFloatField;
    QrLei3Emissao: TDateField;
    QrLei3DCompra: TDateField;
    QrLei3Vencto: TDateField;
    QrLei3DDeposito: TDateField;
    QrLei3Dias: TIntegerField;
    QrLei3Data3: TDateField;
    QrLei3Cobranca: TIntegerField;
    QrLei3Repassado: TSmallintField;
    QrLei3Cliente: TIntegerField;
    QrLei3Bordero: TIntegerField;
    QrLei3TIPOLOTE: TSmallintField;
    QrLei3NOMEBANCO: TWideStringField;
    QrLei3Codigo: TIntegerField;
    QrLei3Banco: TIntegerField;
    QrLei3NossoNum: TWideStringField;
    QrLei3SeuNum: TIntegerField;
    QrLei3IDNum: TIntegerField;
    QrLei3OcorrCodi: TWideStringField;
    QrLei3OcorrData: TDateField;
    QrLei3ValTitul: TFloatField;
    QrLei3ValAbati: TFloatField;
    QrLei3ValDesco: TFloatField;
    QrLei3ValPago: TFloatField;
    QrLei3ValJuros: TFloatField;
    QrLei3ValMulta: TFloatField;
    QrLei3ValJuMul: TFloatField;
    QrLei3Motivo1: TWideStringField;
    QrLei3Motivo2: TWideStringField;
    QrLei3Motivo3: TWideStringField;
    QrLei3Motivo4: TWideStringField;
    QrLei3Motivo5: TWideStringField;
    QrLei3QuitaData: TDateField;
    QrLei3Diretorio: TIntegerField;
    QrLei3Arquivo: TWideStringField;
    QrLei3ItemArq: TIntegerField;
    QrLei3Step: TSmallintField;
    QrLei3Entidade: TIntegerField;
    QrLei3Carteira: TIntegerField;
    QrLei3DevJuros: TFloatField;
    QrLei3DevMulta: TFloatField;
    QrLei3ValOutro: TFloatField;
    QrLei3ValTarif: TFloatField;
    QrLei3DtaTarif: TDateField;
    QrLei3TamReg: TIntegerField;
    QrLei3Lk: TIntegerField;
    QrLei3DataCad: TDateField;
    QrLei3DataAlt: TDateField;
    QrLei3UserCad: TIntegerField;
    QrLei3UserAlt: TIntegerField;
    QrLei3AlterWeb: TSmallintField;
    QrLei3Ativo: TSmallintField;
    QrLei3NOMETIPOLOTE: TWideStringField;
    QrLei3DATA3_TXT: TWideStringField;
    QrLei3CPF_TXT: TWideStringField;
    QrLei3NOMESTATUS: TWideStringField;
    QrLei3NOMEENVIO: TWideStringField;
    QrLei3NOMEMOVIMENTO: TWideStringField;
    QrLei3NOMEACAO: TWideStringField;
    QrLei3ValCustas: TFloatField;
    QrOcorCli: TmySQLQuery;
    QrOcorCliBase: TFloatField;
    QrOcorCliFormaCNAB: TSmallintField;
    QrLeiID_Link: TLargeintField;
    QrLei3ID_Link: TLargeintField;
    QrLocCliCreditor: TmySQLQuery;
    QrLocCliCreditorCliente: TIntegerField;
    MeMotiv: TMemo;
    Splitter4: TSplitter;
    PMMenu: TPopupMenu;
    LocalizarDuplicata1: TMenuItem;
    LocalizarLote1: TMenuItem;
    QrLocCreditor: TmySQLQuery;
    QrCNAB_DirLayoutRem: TWideStringField;
    Pn_L: TPanel;
    BtPasta: TBitBtn;
    QrLei3ValOutro2: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GradeASelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure BtCarregaClick(Sender: TObject);
    procedure Grade1DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure Grade1SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure BtAbertosClick(Sender: TObject);
    procedure QrLeiAfterScroll(DataSet: TDataSet);
    procedure QrLeiBeforeClose(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure ExcluiAtual1Click(Sender: TObject);
    procedure ExcluiSelecionados1Click(Sender: TObject);
    procedure ExcluiTodos1Click(Sender: TObject);
    procedure QrLeiAfterOpen(DataSet: TDataSet);
    procedure BtConciliaClick(Sender: TObject);
    procedure ConciliaAtual1Click(Sender: TObject);
    procedure ConciliaSelecionados1Click(Sender: TObject);
    procedure ConciliaTodos1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrCNAB_DirAfterScroll(DataSet: TDataSet);
    procedure QrLeiItensCalcFields(DataSet: TDataSet);
    procedure QrLeiCalcFields(DataSet: TDataSet);
    procedure DBGLeiDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtAgendaClick(Sender: TObject);
    procedure QrLeiAfterClose(DataSet: TDataSet);
    procedure ExcluiBloquetosnolocalizados1Click(Sender: TObject);
    procedure Alteravalordoitemdearrecadaoselecionado1Click(
      Sender: TObject);
    procedure BtItensClick(Sender: TObject);
    procedure Excluioitemdearrecadaoselecionado1Click(Sender: TObject);
    procedure Ajustavaloresdobloquetoatual1Click(Sender: TObject);
    procedure QrLeiAgrCalcFields(DataSet: TDataSet);
    procedure frxReport1BeforePrint(Sender: TfrxReportComponent);
    procedure BtBufferClick(Sender: TObject);
    procedure Limpabuffer1Click(Sender: TObject);
    procedure PMBufferPopup(Sender: TObject);
    procedure Adicionadados1Click(Sender: TObject);
    procedure frxGradeGetValue(const VarName: string; var Value: Variant);
    procedure Ordenarpelonmerodobloqueto1Click(Sender: TObject);
    procedure QueryCalcFields(DataSet: TDataSet);
    procedure Ordenarpeladatadecrdito1Click(Sender: TObject);
    procedure Ordenarpeladatadepagamento1Click(Sender: TObject);
    procedure CorrigeValTitulopoisobancoenviouerrado1Click(Sender: TObject);
    procedure PnMovimentoClick(Sender: TObject);
    procedure EdNomeBancoChange(Sender: TObject);
    procedure QrLei3CalcFields(DataSet: TDataSet);
    procedure QrOcorreu3CalcFields(DataSet: TDataSet);
    procedure QrLei3AfterScroll(DataSet: TDataSet);
    procedure Grade1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure LocalizarDuplicata1Click(Sender: TObject);
    procedure LocalizarLote1Click(Sender: TObject);
    procedure BtPastaClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
    FLinB, FLinA, FLin1, FActiveRowA, FActiveRowB, FActiveRowC,
    FAbertosCli, FAbertosCond: Integer;
    FTempo, FUltim: TDateTime;
    FNaoImprimeRecibo: Boolean;

    procedure LeArquivos(Diretorio: String; CodDir, LinA: Integer);
    procedure ReopenOcorreu(Codigo: Integer);
    //
    function GravaItens(): Integer;
    function CarregaArquivo(Arquivo: String; LinA: Integer): Boolean;
    //
    function CarregaItensRetorno(LinA, QuemChamou: Integer): Boolean;
    function CarregaItensRetornoA(Arquivo: String;
             SeqDir, SeqArq, Entidade, TamReg, Banco: Integer): Boolean;
    procedure CarregaItensRetorno240(Banco, Entidade, SeqDir, SeqArq: Integer);
    procedure CarregaItensRetorno400(Banco, Entidade, SeqDir, SeqArq: Integer);
    procedure MostraAbertos();
    procedure ReopenCNAB0Lei(Codigo: Integer);
    procedure ExcluirItens(Acao: TSelType);
    procedure ConciliaItens(Acao: TSelType);
    procedure HabilitaBotoes();
    function ErroLinha(Banco, Linha: Integer; Avisa: Boolean): Boolean;
    procedure ReopenLeiItens(Controle: Integer);
    procedure ReopenLeiAgr(Data: TDateTime;
              CliInt, ForneceI, Apto, Mez: Integer);
    {
    function LocDadoAll(const Campo, Linha: Integer; const Mensagem: String;
             var Resultado: String): Boolean;
    function LocDado240(const Campo, Linha: Integer; const Mensagem: String;
             var Resultado: String): Boolean;
    }
    procedure InfoTempo(Tempo: TDateTime; Texto: String; Inicial: Boolean);
    function ObtemActiveRowA: Integer;
    procedure ReopenLeiSum();
    procedure ReabreQueryEImprime(Ordenacao: Integer);
    procedure VerificaImplementacoes(Row: Integer);
    procedure ReopenCNAB_Dir();
    procedure ReopenLocEnt1(CNPJ: String);
    //
    procedure ReopenSQL3(ID_Link: Int64; Entidade: Integer);
    procedure ReopenLocEnt2(Banco: Integer; Agencia, Cedente: String);
    procedure ReopenQrPesq2(IDLink: Int64; Cliente: Integer);
    // Creditor
    function ExecutaItens(CNAB_Lei: Integer): Boolean;
    procedure ExcluiArquivos();
    procedure Liquidacao();
    procedure CalculaPagamento(LotesIts: Integer);
    procedure ImprimeRecibodeQuitacao();
    // Fim Creditor
    procedure MotivosDaOcorrencia(Banco, Ocorrencia: Integer; Motivo: String);
    procedure ReopenClientes();
    procedure AbreDiretorioCNAB();
  public
    { Public declarations }
    FLista: TStrings;
    FLengt: Integer;
  end;

  var
  FmCNAB_Ret2: TFmCNAB_Ret2;

implementation

{$R *.DFM}

uses UnMyObjects, Module, UMySQLModule, UnGOTOy, UnInternalConsts, Principal,
SelCod, UnBancos, UnFinanceiro, MyDBCheck, MyListas, ModuleGeral, CNAB_DuplCad,
UnDmkABS_PF;

procedure TFmCNAB_Ret2.AbreDiretorioCNAB;
begin
  if (QrCNAB_Dir.State <> dsInactive) and (QrCNAB_Dir.RecordCount > 0) and
    (DirectoryExists(QrCNAB_DirNome.Value)) then
  begin
    //Abre diret�rio
    Geral.AbreArquivo(QrCNAB_DirNome.Value);
  end;
end;

procedure TFmCNAB_Ret2.QrLei3AfterScroll(DataSet: TDataSet);
begin
  ReopenOcorreu(0);
end;

procedure TFmCNAB_Ret2.ReopenClientes();
begin
  if CO_DMKID_APP = 4 then //Syndi2
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrClientes, Dmod.MyDB, [
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ',
      'ent.Nome END NOMEENT, ent.Codigo Entidade, cnd.Codigo CODIGO ',
      'FROM cnab_lei lei ',
      'LEFT JOIN entidades ent ON ent.Codigo=lei.Entidade ',
      'LEFT JOIN cond      cnd ON cnd.Cliente=ent.Codigo ',
      'WHERE Step=0 ',
      'GROUP BY lei.Entidade ',
      'ORDER BY NOMEENT ',
    '']);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrClientes, Dmod.MyDB, [
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ',
      'ent.Nome END NOMEENT, ent.Codigo Entidade, ent.Codigo CODIGO ',
      'FROM cnab_lei lei ',
      'LEFT JOIN entidades ent ON ent.Codigo=lei.Entidade ',
      'WHERE Step=0 ',
      'GROUP BY lei.Entidade ',
      'ORDER BY NOMEENT ',
      '']);
  end;
end;

procedure TFmCNAB_Ret2.QrLei3CalcFields(DataSet: TDataSet);
var
  Envio: TEnvioCNAB;
begin
{  � s� retorno!
  case QrLei3Envio.Value of
    1: Envio := ecnabRemessa;
    2: Envio := ecnabRetorno;
    else Envio := ecnabIndefinido;
  end;
}
  case QrLei3TIPOLOTE.Value of
    0: QrLei3NOMETIPOLOTE.Value := 'Cheque';
    1: QrLei3NOMETIPOLOTE.Value := 'Duplicata';
    else QrLei3NOMETIPOLOTE.Value := 'Desconhecido';
  end;
  QrLei3DATA3_TXT.Value := Geral.FDT(QrLei3Data3.Value, 2);
  QrLei3CPF_TXT.Value := Geral.FormataCNPJ_TT(QrLei3CPF.Value);
  //////////////////////////////////////////////////////////////////////////////
  QrLei3NOMESTATUS.Value := MLAGeral.NomeStatusPgto2(QrLei3Quitado.Value,
    QrLei3DDeposito.Value, Date, QrLei3Data3.Value, QrLei3Repassado.Value);
  //////////////////////////////////////////////////////////////////////////////
  QrLei3NOMEMOVIMENTO.Value := UBancos.CNABTipoDeMovimento(1,
    Envio,  (*QrLei3OcorrCodi.Value*)Geral.IMV(QrLei3OcorrCodi.Value), 0, False, '');
  // S� retorno
  //QrLei3NOMEENVIO.Value := UBancos.CNAB240Envio(QrLei3Envio.Value);
  (* ???
  if QrLei3Acao.Value = 1 then QrLei3NOMEACAO.Value := 'Sim'
  else QrLei3NOMEACAO.Value := '';
  *)
end;

procedure TFmCNAB_Ret2.FormDestroy(Sender: TObject);
begin
  FLista.Free;
end;

procedure TFmCNAB_Ret2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCNAB_Ret2.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCNAB_Ret2.frxGradeGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_CONDOMINIO' then
    Value := QrCNAB_DirNOMEENT.Value
  else
end;

procedure TFmCNAB_Ret2.frxReport1BeforePrint(Sender: TfrxReportComponent);
var
  Cross: TfrxCrossView;
  i, j: Integer;
begin
  if Sender is TfrxCrossView then
  begin
    Cross := TfrxCrossView(Sender);
    for i := 1 to Grade1.RowCount do
      for j := 1 to Grade1.ColCount do
        Cross.AddValue([i], [j], [Grade1.Cells[j - 1, i - 1]]);
  end;
end;

procedure TFmCNAB_Ret2.FormCreate(Sender: TObject);
begin
  MeMotiv.Visible   := False;
  Splitter4.Visible := False;
  //
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  PnMovimento.Align  := alClient;
  PnArquivos.Align   := alClient;
  PnCarregados.Align := alClient;
  //
  FLengt := 0;
  FLista := TStringList.Create;
  FActiveRowA := 0;
  FActiveRowB := 0;
  FActiveRowC := 0;
  //
  GradeA.ColWidths[00] := 032;
  GradeA.ColWidths[01] := 160;
  GradeA.ColWidths[02] := 272;
  GradeA.ColWidths[03] := 112;
  GradeA.ColWidths[04] := 104;
  GradeA.ColWidths[05] := 044;
  GradeA.ColWidths[06] := 036;
  GradeA.ColWidths[07] := 028;
  GradeA.ColWidths[08] := 064;
  GradeA.ColWidths[09] := 040;
  GradeA.ColWidths[10] := 028;
  GradeA.ColWidths[11] := 036;
  //
  GradeA.Cells[00,00] := 'Seq';
  GradeA.Cells[01,00] := 'Arquivo';
  GradeA.Cells[02,00] := 'Cliente interno';
  GradeA.Cells[03,00] := 'CNPJ / CPF';
  GradeA.Cells[04,00] := 'Cedente';
  GradeA.Cells[05,00] := 'Lotes';
  GradeA.Cells[06,00] := 'Itens';
  GradeA.Cells[07,00] := 'Dir';
  GradeA.Cells[08,00] := 'Cr�ditos';
  GradeA.Cells[09,00] := 'Cliente';
  GradeA.Cells[10,00] := 'Bco';
  GradeA.Cells[11,00] := 'CNAB';
  //
  Grade1.ColWidths[00] := 032;
  Grade1.ColWidths[01] := 108;
  Grade1.ColWidths[02] := 024;
  Grade1.ColWidths[03] := 180;
  Grade1.ColWidths[04] := 056;
  Grade1.ColWidths[05] := 072;
  Grade1.ColWidths[06] := 072;
  Grade1.ColWidths[07] := 064;
  Grade1.ColWidths[08] := 064;
  Grade1.ColWidths[09] := 072;
  Grade1.ColWidths[10] := 064;
  Grade1.ColWidths[11] := 064;
  Grade1.ColWidths[12] := 064;
  Grade1.ColWidths[13] := 064;
  Grade1.ColWidths[14] := 064;
  Grade1.ColWidths[15] := 064;
  Grade1.ColWidths[16] := 072;
  Grade1.ColWidths[17] := 120;
  Grade1.ColWidths[18] := 056;
  Grade1.ColWidths[19] := 056;
  Grade1.ColWidths[20] := 064;
  Grade1.ColWidths[21] := 100;
  Grade1.ColWidths[22] := 024;
  Grade1.ColWidths[23] := 024;
  Grade1.ColWidths[24] := 024;
  Grade1.ColWidths[25] := 024;
  Grade1.ColWidths[26] := 108;
  Grade1.ColWidths[27] := 064;
  Grade1.ColWidths[28] := 064;
  Grade1.ColWidths[29] := 056;
  /////////////
  Grade1.Cells[00, 00] := 'Seq';
  Grade1.Cells[01, 00] := 'Nosso n�m.';
  Grade1.Cells[02, 00] := 'Ocorr�ncia';
  Grade1.Cells[03, 00] := 'Descri��o da ocorr�ncia';
  Grade1.Cells[04, 00] := 'Data ocor.';
  Grade1.Cells[05, 00] := 'Seu n�mero';
  Grade1.Cells[06, 00] := 'Val. t�tulo';
  Grade1.Cells[07, 00] := 'Abatimento';
  Grade1.Cells[08, 00] := 'Desconto';
  Grade1.Cells[09, 00] := 'Val. receb.';
  Grade1.Cells[10, 00] := 'Juros mora';
  Grade1.Cells[11, 00] := 'Multa';
  Grade1.Cells[12, 00] := 'Outros +';
  Grade1.Cells[13, 00] := 'Jur + Mul.';
  Grade1.Cells[14, 00] := 'Tarifa';
  Grade1.Cells[15, 00] := 'ERRO';
  Grade1.Cells[16, 00] := 'M.O.';
  Grade1.Cells[17, 00] := 'Motivos ocorr�ncia';
  Grade1.Cells[18, 00] := 'Dt.lanc.c/c';
  Grade1.Cells[19, 00] := 'Dt.D�b.tarifa';
  Grade1.Cells[20, 00] := 'Entidade';
  Grade1.Cells[21, 00] := 'ID Link';
  Grade1.Cells[22, 00] := 'Dir';
  Grade1.Cells[23, 00] := 'Arq';
  Grade1.Cells[24, 00] := 'Item';
  Grade1.Cells[25, 00] := 'Bco';
  Grade1.Cells[26, 00] := 'Documento';
  Grade1.Cells[27, 00] := 'Val. Bruto';
  Grade1.Cells[28, 00] := 'Outros -';
  Grade1.Cells[29, 00] := 'Vencto';
  //
end;

procedure TFmCNAB_Ret2.LeArquivos(Diretorio: String; CodDir, LinA: Integer);
var
  i, n: Integer;
begin
  try
    if Diretorio[Length(Diretorio)] <> '\' then Diretorio := Diretorio + '\';
    //MyObjects.GetAllFiles(False, Diretorio + '*.*', ListBox1, True, Nil, Nil);
    MyObjects.GetAllFiles(False, Diretorio + '*.ret', ListBox1, True, Nil, Nil);
    n := LinA;
    for i := 0 to ListBox1.Items.Count -1 do
    begin
      if FileExists(ListBox1.Items[i]) then
      begin
        if Trim(GradeA.Cells[01, n]) <> '' then
          n := n + 1;
        GradeA.RowCount := n + 1;
        GradeA.Cells[00, n] := Geral.FF0(n);
        GradeA.Cells[01, n] := ExtractFileName(ListBox1.Items[i]);
        GradeA.Cells[07, n] := FormatFloat('000', CodDir);
        //
        if not CarregaArquivo(ListBox1.Items[i], n) then
          Exit;
      end;
    end;
    if Trim(GradeA.Cells[1,1]) <> '' then
    CarregaItensRetorno(1, 1);
    //
    {if GradeB.RowCount > 2 then
      if GradeB.Cells[00, GradeB.RowCount -1] = '' then
        GradeB.RowCount := GradeB.RowCount - 1;
    //
    if GradeC.RowCount > 2 then
      if GradeC.Cells[00, GradeC.RowCount -1] = '' then
        GradeC.RowCount := GradeC.RowCount - 1;
    }
  except
    Geral.MensagemBox('Erro ao ler arquivos!', 'Erro', MB_OK+MB_ICONERROR);
    raise;
  end;
end;

procedure TFmCNAB_Ret2.Limpabuffer1Click(Sender: TObject);
begin
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('DROP TABLE Bafer;       ');
  Query.SQL.Add('CREATE TABLE Bafer  (   ');
  Query.SQL.Add('  Bloqueto integer     ,');
  Query.SQL.Add('  ValTitul float       ,');
  Query.SQL.Add('  ValPago float        ,');
  Query.SQL.Add('  ValMulta float       ,');
  Query.SQL.Add('  ValJuros float       ,');
  Query.SQL.Add('  ValOutros float      ,');
  Query.SQL.Add('  ValJurMul float      ,');
  Query.SQL.Add('  ValERRO float        ,');
  Query.SQL.Add('  ValTarifa float      ,');
  Query.SQL.Add('  OcorrCod varchar(10) ,');
  Query.SQL.Add('  OcorrTxt varchar(250),');
  Query.SQL.Add('  DataOcorr date       ,');
  Query.SQL.Add('  DataQuita date       ,');
  Query.SQL.Add('  DataTarif date       ,');
  Query.SQL.Add('  Ativo smallint        ');
  Query.SQL.Add(');                      ');
  Query.SQL.Add('SELECT * FROM bafer;');
  Query.Open;
  //
end;

procedure TFmCNAB_Ret2.Liquidacao();
var
  ValQuit, ValJr, Difer: Double;
  ADupPgs, MeuID, Ocorr, MsgValor: Integer;
  Data: String;
begin
  if QrLei3Quitado.Value > 1 then
  begin
    Geral.MB_Aviso('Esta duplicata j� foi quitada anteriormente!');
    Exit;
  end;
  if (QrLei3MeuLote.Value = 0) and (QrLei3Bruto.Value = 0) then
  begin
    Geral.MB_Aviso('A duplicata com o "Meu ID" n� '+ Geral.FF0(QrLei3ID_Link.Value)+
      ' n�o pode ser liquidada. Ela n�o foi localizada pelo sistema!');
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    MeuID   := QrLei3ID_Link.Value;
    ValQuit := QrLei3ValPago.Value;
    //
    // Cria ocorr�ncia se cliente pagou a menor,
    // porque n�o h� desconto p/ pagto antecipado ou no vencto.
    Difer := QrLei3Bruto.Value - QrLei3ValPago.Value;
    if Difer > 0 then
    begin
      if (Dmod.QrControle.FieldByName('LiqOcoCDi').AsInteger = 1)
      and (Dmod.QrControle.FieldByName('LiqOcoCOc').AsInteger <> 0) then
      begin
        Ocorr := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
          'Ocorreu', 'Ocorreu', 'Codigo');
        Data  := FormatDateTime(VAR_FORMATDATE, QrLei3OcorrData.Value);
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('INSERT INTO ocorreu SET AlterWeb=1, ');
        Dmod.QrUpd.SQL.Add('LotesIts=:P0, DataO=:P1, Ocorrencia=:P2, Valor=:P3, ');
        Dmod.QrUpd.SQL.Add('Codigo=:Pa');
        Dmod.QrUpd.Params[00].AsInteger := MeuID;
        Dmod.QrUpd.Params[01].AsString  := Data;
        Dmod.QrUpd.Params[02].AsInteger := Dmod.QrControle.FieldByName('LiqOcoCOc').AsInteger;
        Dmod.QrUpd.Params[03].AsFloat   := Difer;
        //
        Dmod.QrUpd.Params[04].AsInteger := Ocorr;
        Dmod.QrUpd.ExecSQL;

        // informar diferen�a no pgto se setado que quer info
        if Dmod.QrControle.FieldByName('LiqOcoShw').AsInteger = 1 then
        begin
          QrOB2.Close;
          QrOB2.Params[0].AsInteger := Dmod.QrControle.FieldByName('LiqOcoCOc').AsInteger;
          QrOB2.Open;
          //
          Geral.MB_Aviso('Foi criada a ocorr�ncia "' + QrOB2Nome.Value +
            '" no valor de '+ Dmod.QrControleMoeda.Value + ' ' +
            Geral.FFT(Difer, 2, siPositivo) + ' para a duplicata com o "Meu ID" n� '+
            Geral.FF0(QrLei3ID_Link.Value) + '. ');
        end;
      end;
    end;

    // Calcular taxa de juros caso pago em atraso.
    if Int(QrLei3OcorrData.Value) > Int(QrLei3DDeposito.Value) then
      ValJr := QrLei3ValPago.Value - QrLei3Bruto.Value
    else
      ValJr := 0;
    //
    ADupPgs := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
          'ADupPgs', 'ADupPgs', 'Controle');
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO aduppgs SET AlterWeb=1, ');
    Dmod.QrUpd.SQL.Add('Data=:P0, Juros=:P1, Desco=:P2, Pago=:P3, LotePg=:P4');
    Dmod.QrUpd.SQL.Add(', LotesIts=:Pa, Controle=:Pb');
    Dmod.QrUpd.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE,
      QrLei3OcorrData.Value);
    Dmod.QrUpd.Params[01].AsFloat   := ValJr;
    Dmod.QrUpd.Params[02].AsFloat   := 0;
    Dmod.QrUpd.Params[03].AsFloat   := ValQuit;
    Dmod.QrUpd.Params[04].AsInteger := 0; // N�o tem lote de origem - n�o pago em border�
    //
    Dmod.QrUpd.Params[05].AsInteger := MeuID;
    Dmod.QrUpd.Params[06].AsInteger := ADupPgs;
    Dmod.QrUpd.ExecSQL;
    //
    CalculaPagamento(MeuID);
    if not FNaoImprimeRecibo then
    begin
      MsgValor := MessageDlg(PChar('Deseja imprimir um recibo com a soma '+
        'dos recebimentos efetuados?'), mtWarning, [mbYes, mbNo, mbNoToAll,
        mbCancel], 0);
       if MsgValor = mrYes then ImprimeRecibodeQuitacao
       else if MsgValor = mrNoToAll then FNaoImprimeRecibo := True;
    end;
    (*
    if Geral.MB_Pergunta('Deseja imprimir um recibo com a soma '+
    'dos recebimentos efetuados?') = ID_YES then
      ImprimeRecibodeQuitacao;
    *)
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCNAB_Ret2.LocalizarDuplicata1Click(Sender: TObject);
var
  Controle: Integer;
begin
  {$IFDEF CO_DMKID_APP_003}
  try
    Controle := Geral.IMV(Grade1.Cells[21, Grade1.Row])
  except
    Controle := 0;
  end;
    if Controle > 0 then
      FmPrincipal.MostraDuplicatas2(Controle, 0);
  {$ENDIF}
end;

procedure TFmCNAB_Ret2.LocalizarLote1Click(Sender: TObject);
var
  Lote, Controle: Integer;
begin
  {$IFDEF CO_DMKID_APP_003}
  try
    Controle := Geral.IMV(Grade1.Cells[21, Grade1.Row])
  except
    Controle := 0;
  end;
    if Controle > 0 then
    begin
      QrLocCreditor.Close;
      QrLocCreditor.SQL.Clear;
      QrLocCreditor.SQL.Add('SELECT CNAB_Lot');
      QrLocCreditor.SQL.Add('FROM lotesits');
      QrLocCreditor.SQL.Add('WHERE Controle=:P0');
      QrLocCreditor.Params[0].AsInteger := Controle;
      QrLocCreditor.Open;
      if QrLocCreditor.RecordCount > 0 then
        Lote := QrLocCreditor.FieldByName('CNAB_Lot').Value
      else
        Lote := 0;
      QrLocCreditor.Close;
      //
      if (Lote > 0) and (Controle > 0) then
      begin
        FmPrincipal.MostraRemessaCNABNovo(Lote, Controle);
      end;
    end;
  {$ENDIF}
end;

procedure TFmCNAB_Ret2.GradeASelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  if ARow <> FActiveRowA then
  begin
    FActiveRowA := ARow;
    CarregaItensRetorno(FActiveRowA, 2);
  end;
end;

procedure TFmCNAB_Ret2.Button1Click(Sender: TObject);
var
  i: Integer;
begin
  MemoTam.Lines.Clear;
  for i := 0 to GradeA.ColCount -1 do
    MemoTam.Lines.Add('  GradeA.ColWidths['+FormatFloat('000', i) + '] := ' +
      FormatFloat('0000', GradeA.ColWidths[i])+';');
end;

procedure TFmCNAB_Ret2.GradeADrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  OldAlign: Integer;
begin
  if ARow = 0 then
  begin
    //
  end else if ACol = 0 then begin
    if ARow <> 0 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_CENTER);
      GradeA.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Geral.TFD(GradeA.Cells[Acol, ARow], 3, siPositivo));
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
    end;
  end else if ACol = 01 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_LEFT);
      GradeA.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 02 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_LEFT);
      GradeA.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 03 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_LEFT);
      GradeA.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 04 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_LEFT);
      GradeA.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 05 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_CENTER);
      GradeA.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 06 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_CENTER);
      GradeA.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 07 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_CENTER);
      GradeA.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 08 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_RIGHT);
      GradeA.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 09 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_RIGHT);
      GradeA.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 10 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_CENTER);
      GradeA.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 11 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_CENTER);
      GradeA.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end
end;

procedure TFmCNAB_Ret2.CalculaPagamento(LotesIts: Integer);
var
  Sit: Integer;
  Cred, Debi: Double;
  data3, Data4: String;
begin
  QrSumPg.Close;
  QrSumPg.Params[0].AsInteger := LotesIts;
  QrSumPg.Open;
  Cred := QrSumPgPago.Value + QrSumPgDesco.Value;
  Debi := QrSumPgValor.Value + QrSumPgJuros.Value;
  if QrSumPgPago.Value < 0.01 then Sit := 0 else
  begin
    if Cred < (Debi - 0.009) then Sit := 1 else
    begin
      if Cred > (Debi + 0.009) then Sit := 3 else Sit := 2;
    end;
  end;
  // Novo 2007 08 21
  Data4 := Geral.FDT(QrSumPgMaxData.Value, 1);
  if Sit > 1 then Data3 := Data4 else Data3 := '0000-00-00';
  // Fim Novo
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,  ');
  Dmod.QrUpd.SQL.Add('TotalJr=:P0, TotalDs=:P1, TotalPg=:P2, ');
  Dmod.QrUpd.SQL.Add('Data3=:P3, Data4=:P4, Quitado=:P5 WHERE Controle=:Pa');
  //
  Dmod.QrUpd.Params[00].AsFloat   := QrSumPgJuros.Value;
  Dmod.QrUpd.Params[01].AsFloat   := QrSumPgDesco.Value;
  Dmod.QrUpd.Params[02].AsFloat   := QrSumPgPago.Value;
  Dmod.QrUpd.Params[03].AsString  := Data3;
  Dmod.QrUpd.Params[04].AsString  := Data4;
  Dmod.QrUpd.Params[05].AsInteger := Sit;
  //
  Dmod.QrUpd.Params[06].AsInteger := LotesIts;
  Dmod.QrUpd.ExecSQL;
end;

function TFmCNAB_Ret2.CarregaArquivo(Arquivo: String; LinA: Integer): Boolean;
  function ObtemDado(const TamReg, Banco, Campo, Linha: Integer; const Lista:
  TStrings; const Mensagem: String): String;
  begin
    case TamReg of
      240: UBancos.LocDado240(Banco, Campo, Linha, FLista, Mensagem, '', Result);
      400: UBancos.LocDado400(Banco, Campo, Linha, FLista, Mensagem, '', Result);
    end;
  end;
  function ConfereDado(const TamReg, Banco, Campo, Linha: Integer; const Lista:
  TStrings; const Mensagem: String; var Res: String): Boolean;
  var
    Padr: String;
  begin
    Res := ObtemDado(TamReg, Banco, Campo, Linha, Lista, Mensagem);
    Padr := UBancos.ValorPadrao(TamReg, Banco, Campo);
    Result := Res = Padr;
    if not Result then
      Geral.MensagemBox(Mensagem, 'Aviso', MB_OK+MB_ICONWARNING);
  end;
var
  p, Lin, Banco, IndexReg: Integer;
  s, CodCedente, CNPJ, AgTxt, CeTxt, NomeArq: String;
begin
  Result := False;
  CodCedente := '';
  //Controle := 0;
  if not FileExists(Arquivo) then
  begin
    Geral.MensagemBox(('O arquivo "'+Arquivo+'" n�o foi localizado.'+
    ' Ele pode ter sido exclu�do!'), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  FLista.Clear;
  FLista.LoadFromFile(Arquivo);
  FLengt := -1000;
  if not UBancos.TipoArqCNAB(Arquivo, FLista, FLengt) then Exit;
  {
  k := 0;
  for i := 0 to FLista.Count -1 do
  begin
    n := Length(FLista[i]);
    if (FLengt = -1000) then
      if ( (n=240) or (n=400) ) then
        FLengt := n;
    if (n=0) or (n<>FLengt) then
    begin
      if Geral.MensagemBox(('O arquivo "'+Arquivo+'" possui '+Geral.FF0(n)+
      ' caracteres na linha '+Geral.FF0(i)+ ' quando o esperado era 240 ou 400.' +
      '. Deseja abortar?'), 'Erro', MB_YESNO+MB_ICONERROR) = ID_YES
      then Exit;
      k := k + 1;
    end;
  end;
  if k > 0 then
  begin
    Geral.MensagemBox(('O arquivo "'+Arquivo+'" possui '+Geral.FF0(k)+
    ' linhas que possuem quantidade de caracteres diferente de '+Geral.FF0(FLengt)+
    '.'), 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  }
  // Verifica o padr�o do registro (240, 400)
  IndexReg := UBancos.TamanhoLinhaCNAB_Index(FLengt);
  if IndexReg = 0 then Exit;
  Banco := Geral.IMV(ObtemDado(FLengt, 0, 001, 0, FLista, ''));
  if Banco = 0 then
  begin
    Geral.MB_Erro('N�o foi poss�vel definir o banco do arquivo "' +
      Arquivo + '". Contate a dermatek caso esse arquivo for realmente de ' +
      'remessa/retorno banc�rio!' + sLineBreak + 'Arquivo de ' +
      Geral.FF0(FLengt) + ' posi��es!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //

  // Tipo de arquivo - Lote de Servico
  if not ConfereDado(FLengt, Banco, 003, 0, FLista,
    'O arquivo "'+Arquivo+'" n�o � lote de servi�o!', s) then Exit;

  // Arquivo de retorno
  if not ConfereDado(FLengt, Banco, 007, 0, FLista,
    'O arquivo "'+Arquivo+'" n�o � arquivo de retorno!', s) then Exit;

{
  // Header de arquivo
  if not ConfereDado(FLengt, Banco, 010, 0, FLista,
    'O registro header n�o foi localizado no arquivo "'+Arquivo+'"!', s) then Exit;

  //Quantidade de registros no arquivo
  if not ConfereDado(FLengt, Banco, 301, 0, FLista,
    'Quantidade de registros detalhe n�o definido no arquivo "'+Arquivo+'"!', s) then Exit;
  //if not UBancos.CNABRetRegistrosOK(BcoCod, FLengt, FLista, Arquivo) then Exit;
}

  //////////////////////////////////////////////////////////////////////////////


  // Cliente interno
  // Somente para bancos que informam o CNPJ ou CPF
  if ((Banco = 001) and (FLengt = 400))
  or ((Banco = 399) and (FLengt = 400))
  or ((Banco = 409) and (FLengt = 400)) then
  begin
    CodCedente := ObtemDado(FLengt, Banco, 410, 0, FLista, '');
    QrLocByCed.Close;
    QrLocByCed.Params[00].AsString  := CodCedente;
    QrLocByCed.Params[01].AsInteger := Banco;
    UMyMod.AbreQuery(QrLocByCed, Dmod.MyDB, 'TFmCNAB_Ret2.CarregaArquivo(');
    case QrLocByCed.RecordCount of
      0:
      begin
        Geral.MensagemBox(('N�o foi localizado carteira para o arquivo "' +
        Arquivo+'" onde consta:' +Char(13) + Char(10) + 'Banco: "' +
        FormatFloat('000', Banco) + '"' + Char(13) + Char(10) +
        'C�digo do cedente: "'+ CodCedente + '"' + sLineBreak +
        'Informe o c�digo retorno CNAB na carteira (de extrato) correspondente em:' +
        sLineBreak + 'Cadastros -> Financeiros -> Carteiras.'),
        'Aviso', MB_OK+MB_ICONWARNING);
        Screen.Cursor := crDefault;
        Exit;
      end;
      1: CNPJ := QrLocByCedCNPJ_CPF.Value;
      else
      begin
        Geral.MensagemBox(('Foram localizados '+
        Geral.FF0(QrLocEnt2.RecordCount)+' cadastros de carteiras para o arquivo "' +
        Arquivo+'" onde consta:' +Char(13) + Char(10) + 'Banco: "' +
        FormatFloat('000', Banco) + '"' + Char(13) + Char(10) +
        'C�digo retorno CNAB: "'+ CodCedente + '"' +
        Char(13) + Char(10) + '! Para evitar erros nenhum foi considerado!'),
        'Aviso', MB_OK+MB_ICONWARNING);
        Screen.Cursor := crDefault;
        Exit;
      end;
    end;
  end else
  if Banco = 756 then
  begin
    // Pela Ag�ncia + C�digo do cedente no nome do arquivo
    //
    NomeArq := ExtractFileName(Arquivo);
    p := pos('_', NomeArq);
    //ShowMessage(Geral.FF0(p));
    if p > 0 then
    begin
      AgTxt := Copy(NomeArq, p-4, 4);
      CeTxt := Copy(NomeArq, p+1, 7);
      //ShowMessage(AgTxt + '/' + CeTxt);
      // Parei Aqui
      ReopenLocEnt2(Banco, AgTxt, CeTxt);
      //
      case QrLocEnt2.RecordCount of
        0: Geral.MensagemBox(('N�o foi localizado cliente para o arquivo "' +
        Arquivo+'" onde consta:' +Char(13) + Char(10) + 'Banco: "' +
        FormatFloat('000', Banco) + '"' + Char(13) + Char(10) +
        'Ag�ncia: "' + AgTxt + '"'  +
        Char(13) + Char(10) + 'C�digo do cedente: "'+ CeTxt + '"'),
        'Aviso', MB_OK+MB_ICONWARNING);
        1:
        begin
          // Parei Aqui
          GradeA.Cells[02, linA] := QrLocEnt2NOMEENT.Value;
          GradeA.Cells[03, linA] := Geral.FormataCNPJ_TT(CNPJ);
          GradeA.Cells[04, linA] := CodCedente;
          GradeA.Cells[09, linA] := Geral.FF0(QrLocEnt2CLIENTE.Value);
          GradeA.Cells[10, linA] := FormatFloat('000', Banco);
          GradeA.Cells[11, linA] := Geral.FF0(FLengt);
          Result := True;
        end;
        else Geral.MensagemBox(('Foram localizados '+
        Geral.FF0(QrLocEnt2.RecordCount)+' cadastros de clientes para o arquivo "' +
        Arquivo+'" onde consta:' +Char(13) + Char(10) + 'Banco: "' +
        FormatFloat('000', Banco) + '"' + Char(13) + Char(10) +
        'Ag�ncia: "' + AgTxt + '"'  +
        Char(13) + Char(10) + 'C�digo do cedente: "'+ CeTxt + '"' +
        Char(13) + Char(10) + '! Para evitar erros nenhum foi considerado!'),
        'Aviso', MB_OK+MB_ICONWARNING);
      end;

    end else Geral.MensagemBox(('Foi detectado que o arquivo "'+
    NomeArq + '" pertence ao banco ' + FormatFloat('000', Banco) +
    ', mas o nome n�o est� no formato indicado pelo banco!'), 'Erro',
    MB_OK+MB_ICONERROR);
  end else begin
    Lin := -1;
    case FLengt of
      240: Lin := 0;
      400:
      begin
        if Banco = 748 then
          Lin := 0
        else
          Lin := 1;
      end;
    end;
    CNPJ := ObtemDado(FLengt, Banco, 401, Lin, FLista, '');
    if CodCedente = '' then
      CodCedente := ObtemDado(FLengt, Banco, 410, 0, FLista, '');
    if (CNPJ = '') and (CodCedente = '') then
    begin
      if not UBancos.BancoImplementado(Banco, FLengt, ecnabRetorno) then
      begin
        Screen.Cursor := crDefault;
        Exit;
      end;
      Geral.MB_Aviso('N�o foi poss�vel localizar o CNPJ e/ou o ' +
        'c�digo do cedente no arquivo "' + Arquivo + '" onde consta:' + sLineBreak +
        'Banco: "' + Geral.FF0(Banco) + '"' + sLineBreak + 'Sem estes dados n�o � poss�vel '
        + 'definir o cliente interno!');
      Exit;
    end;
  end;
  if (Banco <> 756) then
  begin
    if CNPJ = '' then
    begin
      Geral.MB_Aviso('N�o foi poss�vel obter o CNPJ no arquivo "'
      + Arquivo + '" onde consta:' + sLineBreak +
      'Banco: "' + Geral.FF0(Banco) + '"' + sLineBreak + 'Sem o CNPJ n�o � poss�vel '
      + 'Definir o cliente interno!');
      Exit;
    end;
    ReopenLocEnt1(CNPJ);
    case QrLocEnt1.RecordCount of
      0: Geral.MensagemBox(('N�o foi localizado cliente para o arquivo "' +
      Arquivo+'" onde consta:' +Char(13) + Char(10) + 'CNPJ/CPF: "' +
      Geral.FormataCNPJ_TT(CNPJ) + '"' + Char(13) + Char(10) +
      //'C�digo do cedente: "'+ CodCedente + '"' + Char(13) + Char(10) +
      'Banco: "' + Geral.FF0(Banco) + '"'), 'Aviso', MB_OK+MB_ICONWARNING);
      1:
      begin
        GradeA.Cells[02, linA] := QrLocEnt1NOMEENT.Value;
        GradeA.Cells[03, linA] := Geral.FormataCNPJ_TT(CNPJ);
        GradeA.Cells[04, linA] := CodCedente;
        GradeA.Cells[09, linA] := Geral.FF0(QrLocEnt1CLIENTE.Value);
        GradeA.Cells[10, linA] := FormatFloat('000', Banco);
        GradeA.Cells[11, linA] := Geral.FF0(FLengt);
        Result := True;
      end;
      else
      begin
        Geral.MensagemBox(('Foram localizados '+
        Geral.FF0(QrLocEnt1.RecordCount)+' cadastros de clientes para o arquivo "' +
        Arquivo+'" onde consta o CNPJ/CPF '+Geral.FormataCNPJ_TT(CNPJ)+
        //' e  c�digo de cedente '+ CodCedente +
        '! Para evitar erros nenhum foi '+ ' considerado!'),
        'Aviso', MB_OK+MB_ICONWARNING);
        //
        if DBCheck.CriaFm(TFmCNAB_DuplCad, FmCNAB_DuplCad, afmoLiberado) then
        begin
          FmCNAB_DuplCad.DsLocEnti1.DataSet := QrLocEnt1;
          FmCNAB_DuplCad.ShowModal;
          FmCNAB_DuplCad.Destroy;
        end;
      end;
    end;
  end;
  //








  Exit;



  // Quantidade de lotes no arquivo
  GradeA.Cells[05, linA] := UBancos.CNABRetQtdeLots(Banco, FLengt, FLista, Arquivo);
  //
  (*
  case BcoCod of
    748: Result := CarregaItensRetornoSicredi(Arquivo,
                   QrCNAB_DirCodigo.Value,
                   Geral.IMV(GradeA.Cells[00, GradeA.RowCount-1]),
                   BcoCod, QrLocEntCLIENTE.Value);
    else
    begin
      Result := False;
    end;
  end;
  *)
end;

procedure TFmCNAB_Ret2.Grade1DrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  OldAlign: Integer;
begin
  if ARow = 0 then
  begin
    //
  end else if ACol = 0 then begin
    if ARow <> 0 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_CENTER);
      Grade1.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Geral.TFD(Grade1.Cells[Acol, ARow], 3, siPositivo));
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
    end;
  end else if ACol = 01 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 02 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 03 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_LEFT);
      Grade1.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 04 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_CENTER);
      Grade1.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 05 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 06 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 07 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 08 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 09 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 10 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 11 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 12 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 13 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 14 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 15 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 16 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_CENTER);
      Grade1.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 17 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_LEFT);
      Grade1.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 18 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_CENTER);
      Grade1.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 18 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_CENTER);
      Grade1.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 20 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 21 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 22 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 23 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 24 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 25 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 26 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_LEFT);
      Grade1.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 27 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 28 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 29 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_CENTER);
      Grade1.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end;
end;

procedure TFmCNAB_Ret2.Grade1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  ACol, ARow: Integer;
begin
  {$IFDEF CO_DMKID_APP_003}
    if (Button = mbRight) and (Shift = [ssRight]) then
    begin
      mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
      mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
      //
      Grade1.MouseToCell(X, Y, ACol, ARow);
      Grade1.Col := ACol;
      Grade1.Row := ARow;
      //
      PMMenu.Popup(X + Grade1.Left + FmCNAB_Ret2.Left,
        Y + Grade1.Top + FmCNAB_Ret2.Top);
    end;
  {$ENDIF}
end;

procedure TFmCNAB_Ret2.Grade1SelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  if ARow <> 0 then
  begin
    MotivosDaOcorrencia(Geral.IMV(Grade1.Cells[25, ARow]),
      Geral.IMV(Grade1.Cells[02, ARow]), Grade1.Cells[16, ARow]);
  end;
  (*if ARow <> FActiveRowC then
    FActiveRowC := ARow;
  if ARow <> FActiveRowB then
    FActiveRowB := ARow;*)
  //ShowMessage(Geral.FF0(ACol));
  //
  if ACol in ([10,11,12,13,14]) then
    Grade1.Options := Grade1.Options + [goEditing]
  else
    Grade1.Options := Grade1.Options - [goEditing];
end;

procedure TFmCNAB_Ret2.BtCarregaClick(Sender: TObject);
var
  CNAB_Lei: Integer;
begin
  CNAB_Lei := GravaItens();
  if CNAB_Lei > 0 then
  begin
    if CO_DMKID_APP = 3 (*Creditor*) then
    begin
      if ExecutaItens(CNAB_Lei) then
      // Exclui arquivos carregados
        ExcluiArquivos();
    end else
    begin
      BtCarrega.Enabled := FLin1 > 0;
      MostraAbertos();
    end;
  end;
end;

function TFmCNAB_Ret2.GravaItens(): Integer;
var
  IDNum, ID_Link: Int64;
  Codigo, Banco, SeuNum, OcorrCodi, Diretorio, ItemArq,
  i, n, a, Entidade, Carteira, Dias, Erros, CliInt, TamReg: Integer;
  OcorrData, QuitaData, Motivo1, Motivo2, Motivo3, Motivo4, Motivo5, Arquivo,
  Extensao, FileName, DestName, NossoNum, DtaTarif: String;
  ValTitul, ValAbati, ValDesco, ValPago, ValJuros, ValMulta, DevJuros, DevMulta,
  TotJuros, ValJuMul, ValTarif, ValErro, ValOutro, ValOutro2, TxaMulta, TxaJuros, JurMul: Double;
  DataPagto: TDateTime;
  Exclui, Continua, InfoJM, SeparaJM: Boolean;
  LayoutRem, Texto: String;
begin
  //tarifa de cobran�a -
  //fazer c�lculo de verifica��o de Val tit + jur + mul - tarifa - desco - abat = val pago
  Erros := 0;
  for i := 1 to Grade1.RowCount - 1 do
  begin
    Banco := Geral.IMV(Grade1.Cells[25, i]);
    if ErroLinha(Banco, i, True) then Inc(Erros, 1);
  end;
  if Erros > 0 then
  begin
    if Geral.MensagemBox(('Existem ' + Geral.FF0(Erros) +
    ' diverg�ncias no arquivo "' + ExtractFileName(GradeA.Cells[1,
    Geral.IMV(Grade1.Cells[23, 1])]) + '".' + sLineBreak +
    'Como � calculado a diverg�ncia:' + sLineBreak +
    'Valor do t�tulo + Multa + Juros de mora + (Juros e multa somados) + ' +
    'Outros cr�ditos - Abatimentos - Descontos - Despesa de cobran�a (Tarifa) - ' +
    'Valor a creditar em conta corrente' + sLineBreak +
    'Deseja continuar assim mesmo?'), 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      Result := 0;
      Exit;
    end;
  end;
  Exclui   := True;
  Codigo   := 0;
  Result   := 0;
  DevJuros := 0;
  DevMulta := 0;
  Dias     := 0;
  n        := 0;
  for i := 1 to Grade1.RowCount - 1 do
  begin
    //  pode vir com letra "P" no final no caso do Banco do Brasil
    Texto := Geral.SoNumero_TT(Trim(Grade1.Cells[1, i]));
    if Geral.DMV(Texto) >= 1 then
      n := n + 1;
  end;
  if n = 0 then
  begin
    Geral.MensagemBox('N�o h� itens a serem gravados. O arquivo ser� ' +
    'movido para a pasta "Vazios"!', 'Aviso', MB_OK+MB_ICONWARNING);
    Arquivo   := ExtractFileName(GradeA.Cells[1, ObtemActiveRowA]);
    Extensao  := '';
    FileName := dmkPF.CaminhoArquivo(QrCNAB_DirNome.Value, Arquivo, Extensao);
    if FileExists(FileName) then
    begin
      DestName := dmkPF.CaminhoArquivo(QrCNAB_DirNome.Value, 'Vazios', '');
      ForceDirectories(DestName);
      dmkPF.MoveArq(PChar(FileName), PChar(DestName));
    end;
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  PB1.Position := 0;
  PB1.Visible := True;
  PB1.Max := Grade1.RowCount - 1 + GradeA.RowCount - 1;
  TamReg := Geral.IMV(GradeA.Cells[11, Geral.IMV(Grade1.Cells[23, 1])]);//FActiveRowA]);
  for a := 1 to Grade1.RowCount - 1 do
  begin
    PB1.Position := PB1.Position + 1;
    Update;
    Application.ProcessMessages;
    //
    NossoNum  := Geral.SoNumero_TT(Grade1.Cells[01, a]);
    OcorrCodi := Geral.IMV(Grade1.Cells[02, a]);
    // Texto da ocorrencia                [03
    OcorrData := dmkPF.CDS(Grade1.Cells[04, a], 2, 1);
    SeuNum    := Geral.IMV(Grade1.Cells[05, a]);
    ValTitul  := Geral.DMV(Grade1.Cells[06, a]);
    ValAbati  := Geral.DMV(Grade1.Cells[07, a]);
    ValDesco  := Geral.DMV(Grade1.Cells[08, a]);
    ValPago   := Geral.DMV(Grade1.Cells[09, a]);
    ValJuros  := Geral.DMV(Grade1.Cells[10, a]);
    ValMulta  := Geral.DMV(Grade1.Cells[11, a]);
    ValOutro  := Geral.DMV(Grade1.Cells[12, a]);
    ValJuMul  := Geral.DMV(Grade1.Cells[13, a]);
    ValTarif  := Geral.DMV(Grade1.Cells[14, a]);
    Motivo1   := Copy(Grade1.Cells[16, a], 1, 2);
    Motivo2   := Copy(Grade1.Cells[16, a], 3, 2);
    Motivo3   := Copy(Grade1.Cells[16, a], 5, 2);
    Motivo4   := Copy(Grade1.Cells[16, a], 7, 2);
    Motivo5   := Copy(Grade1.Cells[16, a], 9, 2);
    // Texto dos motivos da ocorrencia    [13
    QuitaData := dmkPF.CDS(Grade1.Cells[18, a], 2, 1);
    DtaTarif  := dmkPF.CDS(Grade1.Cells[19, a], 2, 1);
    Diretorio := Geral.IMV(Grade1.Cells[22, a]);
    Arquivo   := ExtractFileName(GradeA.Cells[1, StrToInt(Grade1.Cells[22, a])]);
    ItemArq   := Geral.IMV(Grade1.Cells[24, a]);
    Banco     := Geral.IMV(Grade1.Cells[25, a]);
    Entidade  := Geral.IMV(Grade1.Cells[20, a]);
    ID_Link   := MLAGeral.I64(Grade1.Cells[21, a]);
    ValOutro2 := Geral.DMV(Grade1.Cells[28, a]);
    Carteira  := QrCNAB_DirCarteira.Value;
    LayoutRem := QrCNAB_DirLayoutRem.Value;
    //
    if not UBancos.SeparaJurosEMultaImplementado(Banco, LayoutRem, SeparaJM) then
    begin
      Screen.Cursor := crDefault;
      PB1.Visible := False;
      Exit;
    end;
    if not SeparaJM then
    begin
      // N�o separa Juros e multa no arquivo.
      // Fazer manual
      if (ValPago > ValTitul) and (ValJuros + ValMulta = 0) then
      begin
        if (CO_DMKID_APP = 3) and (CO_DMKID_APP = 22) then //Creditor
        begin
          //Query utilizada apenas para o aplicativo CREDITOR
          QrLocCliCreditor.Close;
          QrLocCliCreditor.Params[0].AsFloat := ID_Link;
          QrLocCliCreditor.Open;
          if QrLocCliCreditor.RecordCount > 0 then
            Entidade := QrLocCliCreditorCliente.Value;
        end;
        if DModG.DadosRetDeEntidade(Entidade, CliInt) then
        begin
          ValMulta := Round(DModG.QrLocCIPercMulta.Value * ValTitul) / 100;
          JurMul := ValPago - ValTitul;
          if ValMulta > JurMul then
          begin
            ValMulta := JurMul;
            ValJuros := 0;
          end else begin
            ValJuros := JurMul - ValMulta;
            if ValJuros < 0 then
            begin
              // precau��o
              ValMulta := ValMulta + ValJuros;
              ValJuros := 0;
            end;
          end;
        end;
      end;
    end;
    Continua := UBancos.BancoTemEntidade(Banco);
    if not Continua then
    begin
      Screen.Cursor := crDefault;
      PB1.Visible := False;
      Exit;
    end;
    Continua := UBancos.EhCodigoLiquidacao(OcorrCodi, Banco, TamReg, '');
    if not Continua then
    begin
      Continua := OcorrCodi = UBancos.CodigoTarifa(Banco, '');
      // Tentativa de eliminar a tabela BancosLei
      {QrBcocor.Close;
      QrBcocor.Params[00].AsInteger := Banco;
      QrBcocor.Params[01].AsString  := Grade1.Cells[02, a];
      UMyMod.AbreQuery(QrBcocor)

      if QrBcocor.RecordCount = 0 then
      begin
        PageControl1.ActivePageIndex := 1;
        MeAvisos.Lines.Add('O item '+Geral.FF0(ItemArq)+
          ' do arquivo '+Arquivo+' n�o foi registrado! (Ocorr�ncia n�o cadastrada no cadastro do banco)');
        Application.ProcessMessages;
      end else Continua := QrBcocorCarrega.Value <> 0;}
      if not Continua then
      begin
        Continua :=
        UBancos.OcorrenciaConhecida(Banco, ecnabRetorno, OcorrCodi, 400, False, '');
        if not Continua then
        begin
          PageControl1.ActivePageIndex := 1;
          MeAvisos.Lines.Add('O item '+Geral.FF0(ItemArq) + ' do arquivo ' +
          Arquivo + ' n�o foi registrado! (Ocorr�ncia Desconhecida: ' +
          FormatFloat('0', OcorrCodi) + ')');
        end;
        Application.ProcessMessages;
      end;
    end;
    if Continua then
    begin
      IDNum := ID_Link;
      //
      if IDNum = 0 then
      begin
        Exclui := False;
        PageControl1.ActivePageIndex := 1;
        MeAvisos.Lines.Add('O item '+Geral.FF0(ItemArq)+
          ' do arquivo '+Arquivo+' n�o foi registrado! (Banco ' +
          FormatFloat('000', Banco) + ' - n�o implementado) > (ID_Link = 0)');
        Application.ProcessMessages;
      end else begin
        QrDupl.Close;
        QrDupl.Params[00].AsInteger := Banco;
        QrDupl.Params[01].AsString  := NossoNum;
        QrDupl.Params[02].AsInteger := SeuNum;
        QrDupl.Params[03].AsInteger := OcorrCodi;
        QrDupl.Params[04].AsString  := Arquivo;
        QrDupl.Params[05].AsInteger := ItemArq;
        QrDupl.Params[06].AsFloat   := ValTitul;
        QrDupl.Params[07].AsFloat   := ValPago;
        QrDupl.Params[08].AsString  := QuitaData;
        UMyMod.AbreQuery(QrDupl, Dmod.MyDB, 'TFmCNAB_Ret2.GravaItens()');
        if QrDupl.RecordCount > 0 then
        begin
          if CkReverter.Checked then
          begin
            Dmod.QrUpd2.SQL.Clear;
            Dmod.QrUpd2.SQL.Add('UPDATE cnab_lei SET Step=0 WHERE Codigo=:P0');
            Dmod.QrUpd2.Params[0].AsInteger := QrDuplCodigo.Value;
            Dmod.QrUpd2.ExecSQL;
          end else begin
            PageControl1.ActivePageIndex := 1;
            MeAvisos.Lines.Add('O item '+Geral.FF0(ItemArq)+
              ' do arquivo '+Arquivo+' j� foi registrado anteriormente!');
            Application.ProcessMessages;
          end;
        end else
        begin
          Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB,
            'Livres', 'Controle', 'CNAB_Lei', 'CNAB_Lei', 'Codigo');
          //
          DevJuros := 0;
          DevMulta := 0;
          if CO_DMKID_APP = 3 (* Creditor *) then
            Continua := True
          else
            Continua := UBancos.EhCodigoLiquidacao(OcorrCodi, Banco, TamReg, '');
          if Continua then
          begin
            ReopenSQL3(ID_Link, Entidade);
            DataPagto := Geral.ValidaDataSimples(Grade1.Cells[04, a], True);
            if QrPesq3Vencimento.Value < DataPagto then
              Dias := UMyMod.DiasUteis(QrPesq3Vencimento.Value + 1, DataPagto);
            if Dias > 0 then
            begin
              TotJuros := dmkPF.CalculaJuroSimples(QrPesq3PercJuros.Value,
              DataPagto - QrPesq3Vencimento.Value);
              DevJuros := Round(TotJuros * QrPesq3Credito.Value) / 100;
              DevMulta := Round(QrPesq3PercMulta.Value * QrPesq3Credito.Value) / 100;
            end;
          end;
          //
          // Dve ser aqui para evitar erros
          if Continua then
          begin
            Dmod.QrUpd.SQL.Clear;
            Dmod.QrUpd.SQL.Add('INSERT INTO cnab_lei SET ');
            Dmod.QrUpd.SQL.Add('Codigo=:P0, Banco=:P1, NossoNum=:P2, SeuNum=:P3, ');
            Dmod.QrUpd.SQL.Add('OcorrCodi=:P4, OcorrData=:P5, ValTitul=:P6, ');
            Dmod.QrUpd.SQL.Add('ValAbati=:P7, ValDesco=:P8, ValPago=:P9, ');
            Dmod.QrUpd.SQL.Add('ValJuros=:P10, ValMulta=:P11, Motivo1=:P12, ');
            Dmod.QrUpd.SQL.Add('Motivo2=:P13, Motivo3=:P14, Motivo4=:P15, ');
            Dmod.QrUpd.SQL.Add('Motivo5=:P16, QuitaData=:P17, Diretorio=:P18, ');
            Dmod.QrUpd.SQL.Add('Arquivo=:P19, ItemArq=:P20, IDNum=:P21, Entidade=:P22, ');
            Dmod.QrUpd.SQL.Add('Carteira=:P23, DevJuros=:P24, DevMulta=:P25, ');
            Dmod.QrUpd.SQL.Add('ID_Link=:P26, ValJuMul=:P27, ValTarif=:P28, ');
            Dmod.QrUpd.SQL.Add('ValOutro=:P29, DtaTarif=:P30, TamReg=:P31, ');
            Dmod.QrUpd.SQL.Add('ValOutro2=:P32');
            //
            Dmod.QrUpd.Params[00].AsInteger := Codigo;
            Dmod.QrUpd.Params[01].AsInteger := Banco;
            Dmod.QrUpd.Params[02].AsString  := NossoNum;
            Dmod.QrUpd.Params[03].AsInteger := SeuNum;
            Dmod.QrUpd.Params[04].AsInteger := OcorrCodi;
            Dmod.QrUpd.Params[05].AsString  := OcorrData;
            Dmod.QrUpd.Params[06].AsFloat   := ValTitul;
            Dmod.QrUpd.Params[07].AsFloat   := ValAbati;
            Dmod.QrUpd.Params[08].AsFloat   := ValDesco;
            Dmod.QrUpd.Params[09].AsFloat   := ValPago;
            Dmod.QrUpd.Params[10].AsFloat   := ValJuros;
            Dmod.QrUpd.Params[11].AsFloat   := ValMulta;
            Dmod.QrUpd.Params[12].AsString  := Motivo1;
            Dmod.QrUpd.Params[13].AsString  := Motivo2;
            Dmod.QrUpd.Params[14].AsString  := Motivo3;
            Dmod.QrUpd.Params[15].AsString  := Motivo4;
            Dmod.QrUpd.Params[16].AsString  := Motivo5;
            Dmod.QrUpd.Params[17].AsString  := QuitaData;
            Dmod.QrUpd.Params[18].AsInteger := Diretorio;
            Dmod.QrUpd.Params[19].AsString  := Arquivo;
            Dmod.QrUpd.Params[20].AsInteger := ItemArq;
            Dmod.QrUpd.Params[21].AsInteger := IDNum;
            Dmod.QrUpd.Params[22].AsInteger := Entidade;
            Dmod.QrUpd.Params[23].AsInteger := Carteira;
            Dmod.QrUpd.Params[24].AsFloat   := DevJuros;
            Dmod.QrUpd.Params[25].AsFloat   := DevMulta;
            Dmod.QrUpd.Params[26].AsFloat   := ID_Link;
            Dmod.QrUpd.Params[27].AsFloat   := ValJuMul;
            Dmod.QrUpd.Params[28].AsFloat   := ValTarif;
            Dmod.QrUpd.Params[29].AsFloat   := ValOutro;
            Dmod.QrUpd.Params[30].AsString  := DtaTarif;
            Dmod.QrUpd.Params[31].AsInteger := TamReg;
            Dmod.QrUpd.Params[32].AsFloat   := ValOutro2;
            Dmod.QrUpd.ExecSQL;
          end;
        end;
      end;
    end;
  end;

  //

  Arquivo   := ExtractFileName(GradeA.Cells[1, ObtemActiveRowA]);
  Extensao  := '';
  FileName := dmkPF.CaminhoArquivo(QrCNAB_DirNome.Value, Arquivo, Extensao);
  if Exclui then
  begin
    if FileExists(FileName) then
    begin
      //ShowMessage('Arquivo: '+FileName);
      DestName := dmkPF.CaminhoArquivo(QrCNAB_DirNome.Value, 'Lidos', '');
      ForceDirectories(DestName);
      //ShowMessage('Destino: '+DestName);
      dmkPF.MoveArq(PChar(FileName), PChar(DestName));
      //
    end;
  end;
  //

  PB1.Visible := False;
  Screen.Cursor := crDefault;
  Result := Codigo;

  //
  // Recarrega arquivos
  Timer1.Enabled := True;

end;

procedure TFmCNAB_Ret2.BtAbertosClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    MostraAbertos;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCNAB_Ret2.MostraAbertos;
var
  Codigo: Integer;
begin
  Codigo := QrLeiCodigo.Value;
  FAbertosCli  := 0;
  FAbertosCond := 0;
  ReopenClientes();
  //
  case QrClientes.RecordCount of
    0: Geral.MensagemBox('N�o h� itens abertos!', 'Aviso', MB_OK+MB_ICONWARNING);
    1:
    begin
      FAbertosCli  := QrClientesEntidade.Value;
      FAbertosCond := QrClientesCODIGO.Value;
    end
    else begin
      Application.CreateForm(TFmSelCod, FmSelCod);
      FmSelCod.Caption := 'Condom�nio';
      FmSelCod.LaPrompt.Caption := 'Condom�nio';
      FmSelCod.CBSel.ListSource := nil;
      FmSelCod.CBSel.ListField := 'NOMEENT';
      FmSelCod.CBSel.KeyField  := 'CODIGO';
      FmSelCod.CBSel.ListSource := DsClientes;
      //
      if QrClientes.Locate('Entidade', QrCNAB_DirCliInt.Value, []) then
      begin
        FmSelCod.EdSel.Text := FormatFloat('0', QrClientesCODIGO.Value);
        FmSelCod.CBSel.KeyValue := QrClientesCODIGO.Value;
      end;
      FmSelCod.ShowModal;
      if VAR_SELCOD <> 0 then
      begin
        FAbertosCli  := QrClientesEntidade.Value;
        FAbertosCond := QrClientesCODIGO.Value;
      end;
      FmSelCod.Destroy;
    end;
  end;
  if FAbertosCli <> 0 then
  begin
    Application.ProcessMessages;
    //
    PnMovimento.Visible := True;
    PnCarrega.Visible := False;
    Application.ProcessMessages;
    //
    ReopenCNAB0Lei(Codigo);
  end;
end;


procedure TFmCNAB_Ret2.MotivosDaOcorrencia(Banco, Ocorrencia: Integer;
  Motivo: String);
  function ObtemMotivos(Ocorrencia: Integer; Motiv: String): String;
  begin
    case Ocorrencia of
      02:
      begin
        case Geral.IMV(Motiv) of
          000: Result := 'Ocorr�ncia aceita';
          001: Result := 'C�digo do Banco inv�lido';
          004: Result := 'C�digo do movimento n�o permitido para a carteira';
          015: Result := 'Caracter�sticas da cobran�a incompat�veis';
          017: Result := 'Data de vencimento anterior a data de emiss�o';
          021: Result := 'Esp�cie do T�tulo inv�lido';
          024: Result := 'Data da emiss�o inv�lida';
          027: Result := 'Valor/taxa de juros mora inv�lido';
          038: Result := 'Prazo para protesto inv�lido';
          039: Result := 'Pedido para protesto n�o permitido para t�tulo';
          043: Result := 'Prazo para baixa e devolu��o inv�lido';
          045: Result := 'Nome do Sacado inv�lido';
          046: Result := 'Tipo/num. de inscri��o do Sacado inv�lidos';
          047: Result := 'Endere�o do Sacado n�o informado';
          048: Result := 'CEP Inv�lido';
          050: Result := 'CEP referente a Banco correspondente';
          053: Result := 'N� de inscri��o do Sacador/avalista inv�lidos';
          054: Result := 'Sacador/avalista n�o informado';
          067: Result := 'D�bito autom�tico agendado';
          068: Result := 'D�bito n�o agendado - erro nos dados de remessa';
          069: Result := 'D�bito n�o agendado - Sacado n�o consta no cadastro de autorizante';
          070: Result := 'D�bito n�o agendado - Cedente n�o autorizado pelo Sacado';
          071: Result := 'D�bito n�o agendado - Cedente n�o participa da modalidade de d�b.autom�tico';
          072: Result := 'D�bito n�o agendado - C�digo de moeda diferente de R$';
          073: Result := 'D�bito n�o agendado - Data de vencimento inv�lida/vencida';
          075: Result := 'D�bito n�o agendado - Tipo do n�mero de inscri��o do sacado debitado inv�lido';
          076: Result := 'Sacado Eletr�nico DDA';
          086: Result := 'Seu n�mero do documento inv�lido';
          089: Result := 'Email Sacado n�o enviado � t�tulo com d�bito autom�tico';
          090: Result := 'Email sacado n�o enviado � t�tulo de cobran�a sem registro';
        end;
      end;
      03:
      begin
        case Geral.IMV(Motiv) of
          002: Result := 'C�digo do registro detalhe inv�lido';
          003: Result := 'C�digo da ocorr�ncia inv�lida';
          004: Result := 'C�digo de ocorr�ncia n�o permitida para a carteira';
          005: Result := 'C�digo de ocorr�ncia n�o num�rico';
          007: Result := 'Ag�ncia/conta/Digito - |Inv�lido';
          008: Result := 'Nosso n�mero inv�lido';
          009: Result := 'Nosso n�mero duplicado';
          010: Result := 'Carteira inv�lida';
          013: Result := 'Identifica��o da emiss�o do bloqueto inv�lida';
          016: Result := 'Data de vencimento inv�lida';
          018: Result := 'Vencimento fora do prazo de opera��o';
          020: Result := 'Valor do T�tulo inv�lido';
          021: Result := 'Esp�cie do T�tulo inv�lida';
          022: Result := 'Esp�cie n�o permitida para a carteira';
          024: Result := 'Data de emiss�o inv�lida';
          028: Result := 'C�digo do desconto inv�lido';
          038: Result := 'Prazo para protesto inv�lido';
          044: Result := 'Ag�ncia Cedente n�o prevista';
          045: Result := 'Nome do sacado n�o informado';
          046: Result := 'Tipo/n�mero de inscri��o do sacado inv�lidos';
          047: Result := 'Endere�o do sacado n�o informado';
          048: Result := 'CEP Inv�lido';
          050: Result := 'CEP irregular - Banco Correspondente';
          063: Result := 'Entrada para T�tulo j� cadastrado';
          065: Result := 'Limite excedido';
          066: Result := 'N�mero autoriza��o inexistente';
          068: Result := 'D�bito n�o agendado - erro nos dados de remessa';
          069: Result := 'D�bito n�o agendado - Sacado n�o consta no cadastro de autorizante';
          070: Result := 'D�bito n�o agendado - Cedente n�o autorizado pelo Sacado';
          071: Result := 'D�bito n�o agendado - Cedente n�o participa do d�bito Autom�tico';
          072: Result := 'D�bito n�o agendado - C�digo de moeda diferente de R$';
          073: Result := 'D�bito n�o agendado - Data de vencimento inv�lida';
          074: Result := 'D�bito n�o agendado - Conforme seu pedido, T�tulo n�o registrado';
          075: Result := 'D�bito n�o agendado � Tipo de n�mero de inscri��o do debitado inv�lido';
        end;
      end;
      06: //Ver com o Marco d�vidas sobre o manual
      begin
        case Geral.IMV(Motiv) of
          000: Result := 'T�tulo pago com dinheiro';
          015: Result := 'T�tulo pago com cheque';
          042: Result := 'Rateio n�o efetuado, c�d. Calculo 2 (VLR. Registro) e v';
        end;
      end;
      09:
      begin
        case Geral.IMV(Motiv) of
          000: Result := 'Ocorr�ncia Aceita';
          010: Result := 'Baixa Comandada pelo cliente';
        end;
      end;
      10:
      begin
        case Geral.IMV(Motiv) of
          000: Result := 'Baixado Conforme Instru��es da Ag�ncia';
          014: Result := 'T�tulo Protestado';
          015: Result := 'T�tulo exclu�do';
          016: Result := 'T�tulo Baixado pelo Banco por decurso Prazo';
          017: Result := 'Titulo Baixado Transferido Carteira';
          020: Result := 'Titulo Baixado e Transferido para Desconto';
        end;
      end;
      15:
      begin
        case Geral.IMV(Motiv) of
          000: Result := 'T�tulo pago com dinheiro';
          015: Result := 'T�tulo pago com cheque';
        end;
      end;
      17:
      begin
        case Geral.IMV(Motiv) of
          000: Result := 'T�tulo pago com dinheiro';
          015: Result := 'T�tulo pago com cheque';
        end;
      end;
      24:
      begin
        case Geral.IMV(Motiv) of
          048: Result := 'CEP inv�lido';  
        end;
      end;
      27:
      begin
        case Geral.IMV(Motiv) of
          004: Result := 'C�digo de ocorr�ncia n�o permitido para a carteira';
          007: Result := 'Ag�ncia/Conta/d�gito inv�lidos';
          008: Result := 'Nosso n�mero inv�lido';
          010: Result := 'Carteira inv�lida';
          015: Result := 'Carteira/Ag�ncia/Conta/nosso n�mero inv�lidos';
          040: Result := 'T�tulo com ordem de protesto emitido';
          042: Result := 'C�digo para baixa/devolu��o via Telebradesco inv�lido';
          060: Result := 'Movimento para T�tulo n�o cadastrado';
          077: Result := 'Transfer�ncia para desconto n�o permitido para a carteira';
          085: Result := 'T�tulo com pagamento vinculado';
        end;
      end;
      28:
      begin
        case Geral.IMV(Motiv) of
          002: Result := 'Tarifa de perman�ncia t�tulo cadastrado';
          003: Result := 'Tarifa de susta��o';
          004: Result := 'Tarifa de protesto';
          005: Result := 'Tarifa de outras instru��es';
          006: Result := 'Tarifa de outras ocorr�ncias';
          008: Result := 'Custas de protesto';
          012: Result := 'Tarifa de registro';
          013: Result := 'Tarifa t�tulo pago no Bradesco';
          014: Result := 'Tarifa t�tulo pago compensa��o';
          015: Result := 'Tarifa t�tulo baixado n�o pago';
          016: Result := 'Tarifa altera��o de vencimento';
          017: Result := 'Tarifa concess�o abatimento';
          018: Result := 'Tarifa cancelamento de abatimento';
          019: Result := 'Tarifa concess�o desconto';
          020: Result := 'Tarifa cancelamento desconto';
          021: Result := 'Tarifa t�tulo pago cics';
          022: Result := 'Tarifa t�tulo pago Internet';
          023: Result := 'Tarifa t�tulo pago term. gerencial servi�os';
          024: Result := 'Tarifa t�tulo pago P�g-Contas';
          025: Result := 'Tarifa t�tulo pago Fone F�cil';
          026: Result := 'Tarifa t�tulo D�b. Postagem';
          027: Result := 'Tarifa impress�o de t�tulos pendentes';
          028: Result := 'Tarifa t�tulo pago BDN';
          029: Result := 'Tarifa t�tulo pago Term. Multi Fun��o';
          030: Result := 'Impress�o de t�tulos baixados';
          031: Result := 'Impress�o de t�tulos pagos';
          032: Result := 'Tarifa t�tulo pago Pagfor';
          033: Result := 'Tarifa reg/pgto � guich� caixa';
          034: Result := 'Tarifa t�tulo pago retaguarda';
          035: Result := 'Tarifa t�tulo pago Subcentro';
          036: Result := 'Tarifa t�tulo pago Cart�o de Cr�dito';
          037: Result := 'Tarifa t�tulo pago Comp Eletr�nica';
          038: Result := 'Tarifa t�tulo Baix. Pg. Cart�rio';
          039: Result := 'Tarifa t�tulo baixado acerto BCO';
          040: Result := 'Baixa registro em duplicidade';
          041: Result := 'Tarifa t�tulo baixado decurso prazo';
          042: Result := 'Tarifa t�tulo baixado Judicialmente';
          043: Result := 'Tarifa t�tulo baixado via remessa';
          044: Result := 'Tarifa t�tulo baixado rastreamento';
          045: Result := 'Tarifa t�tulo baixado conf. Pedido';
          046: Result := 'Tarifa t�tulo baixado protestado';
          047: Result := 'Tarifa t�tulo baixado p/ devolu��o';
          048: Result := 'Tarifa t�tulo baixado franco pagto';
          049: Result := 'Tarifa t�tulo baixado SUST/RET/CART�RIO';
          050: Result := 'Tarifa t�tulo baixado SUS/SEM/REM/CART�RIO';
          051: Result := 'Tarifa t�tulo transferido desconto';
          052: Result := 'Cobrado baixa manual';
          053: Result := 'Baixa por acerto cliente';
          054: Result := 'Tarifa baixa por contabilidade';
          055: Result := 'Tr. tentativa cons deb aut';
          056: Result := 'Tr. credito online';
          057: Result := 'Tarifa reg/pagto Bradesco Expresso';
          058: Result := 'Tarifa emiss�o Papeleta';
          059: Result := 'Tarifa fornec papeleta semi preenchida';
          060: Result := 'Acondicionador de papeletas (RPB)S';
          061: Result := 'Acond. De papelatas (RPB)s PERSONAL';
          062: Result := 'Papeleta formul�rio branco';
          063: Result := 'Formul�rio A4 serrilhado';
          064: Result := 'Fornecimento de softwares transmiss';
          065: Result := 'Fornecimento de softwares consulta';
          066: Result := 'Fornecimento Micro Completo';
          067: Result := 'Fornecimento MODEN';
          068: Result := 'Fornecimento de m�quina FAX';
          069: Result := 'Fornecimento de m�quinas �ticas';
          070: Result := 'Fornecimento de Impressoras';
          071: Result := 'Reativa��o de t�tulo';
          072: Result := 'Altera��o de produto negociado';
          073: Result := 'Tarifa emiss�o de contra recibo';
          074: Result := 'Tarifa emiss�o 2� via papeleta';
          075: Result := 'Tarifa regrava��o arquivo retorno';
          076: Result := 'Arq. T�tulos a vencer mensal';
          077: Result := 'Listagem auxiliar de cr�dito';
          078: Result := 'Tarifa cadastro cartela instru��o permanente';
          079: Result := 'Canaliza��o de Cr�dito';
          080: Result := 'Cadastro de Mensagem Fixa';
          081: Result := 'Tarifa reapresenta��o autom�tica t�tulo';
          082: Result := 'Tarifa registro t�tulo d�b. Autom�tico';
          083: Result := 'Tarifa Rateio de Cr�dito';
          084: Result := 'Emiss�o papeleta sem valor';
          085: Result := 'Sem uso';
          086: Result := 'Cadastro de reembolso de diferen�a';
          087: Result := 'Relat�rio fluxo de pagto';
          088: Result := 'Emiss�o Extrato mov. Carteira';
          089: Result := 'Mensagem campo local de pagto';
          090: Result := 'Cadastro Concession�ria serv. Publ.';
          091: Result := 'Classif. Extrato Conta Corrente';
          092: Result := 'Contabilidade especial';
          093: Result := 'Realimenta��o pagto';
          094: Result := 'Repasse de Cr�ditos';
          095: Result := 'Tarifa reg. pagto Banco Postal';
          096: Result := 'Tarifa reg. Pagto outras m�dias';
          097: Result := 'Tarifa Reg/Pagto � Net Empresa';
          098: Result := 'Tarifa t�tulo pago vencido';
          099: Result := 'TR T�t. Baixado por decurso prazo';
          100: Result := 'Arquivo Retorno Antecipado';
          101: Result := 'Arq retorno Hora/Hora';
          102: Result := 'TR. Agendamento D�b Aut';
          105: Result := 'TR. Agendamento rat. Cr�dito';
          106: Result := 'TR Emiss�o aviso rateio';
          107: Result := 'Extrato de protesto';
        end;
      end;
      29:
      begin
        case Geral.IMV(Motiv) of
          078: Result := 'Sacado alega que faturamento e indevido';
          095: Result := 'Sacado aceita/reconhece o faturamento';
        end;
      end;
      30:
      begin
        case Geral.IMV(Motiv) of
          001: Result := 'C�digo do Banco inv�lido';
          004: Result := 'C�digo de ocorr�ncia n�o permitido para a carteira';
          005: Result := 'C�digo da ocorr�ncia n�o num�rico';
          008: Result := 'Nosso n�mero inv�lido';
          015: Result := 'Caracter�stica da cobran�a incompat�vel';
          016: Result := 'Data de vencimento inv�lido';
          017: Result := 'Data de vencimento anterior a data de emiss�o';
          018: Result := 'Vencimento fora do prazo de opera��o';
          024: Result := 'Data de emiss�o Inv�lida';
          026: Result := 'C�digo de juros de mora inv�lido';
          027: Result := 'Valor/taxa de juros de mora inv�lido';
          028: Result := 'C�digo de desconto inv�lido';
          029: Result := 'Valor do desconto maior/igual ao valor do T�tulo';
          030: Result := 'Desconto a conceder n�o confere';
          031: Result := 'Concess�o de desconto j� existente';
          032: Result := 'Valor do IOF inv�lido';
          033: Result := 'Valor do abatimento inv�lido';
          034: Result := 'Valor do abatimento maior/igual ao valor do T�tulo';
          038: Result := 'Prazo para protesto inv�lido';
          039: Result := 'Pedido de protesto n�o permitido para o T�tulo';
          040: Result := 'T�tulo com ordem de protesto emitido';
          042: Result := 'C�digo para baixa/devolu��o inv�lido';
          046: Result := 'Tipo/n�mero de inscri��o do sacado inv�lidos';
          048: Result := 'Cep Inv�lido';
          053: Result := 'Tipo/N�mero de inscri��o do sacador/avalista inv�lidos';
          054: Result := 'Sacador/avalista n�o informado';
          057: Result := 'C�digo da multa inv�lido';
          058: Result := 'Data da multa inv�lida';
          060: Result := 'Movimento para T�tulo n�o cadastrado';
          079: Result := 'Data de Juros de mora Inv�lida';
          080: Result := 'Data do desconto inv�lida';
          085: Result := 'T�tulo com Pagamento Vinculado';
          088: Result := 'E-mail Sacado n�o lido no prazo 5 dias';
          091: Result := 'E-mail sacado n�o recebido';
        end;
      end;
      32:
      begin
        case Geral.IMV(Motiv) of
          001: Result := 'C�digo do Banco inv�lido';
          002: Result := 'C�digo do registro detalhe inv�lido';
          004: Result := 'C�digo de ocorr�ncia n�o permitido para a carteira';
          005: Result := 'C�digo de ocorr�ncia n�o num�rico';
          007: Result := 'Ag�ncia/Conta/d�gito inv�lidos';
          008: Result := 'Nosso n�mero inv�lido';
          010: Result := 'Carteira inv�lida';
          015: Result := 'Caracter�sticas da cobran�a incompat�veis';
          016: Result := 'Data de vencimento inv�lida';
          017: Result := 'Data de vencimento anterior a data de emiss�o';
          018: Result := 'Vencimento fora do prazo de opera��o';
          020: Result := 'Valor do t�tulo inv�lido';
          021: Result := 'Esp�cie do T�tulo inv�lida';
          022: Result := 'Esp�cie n�o permitida para a carteira';
          024: Result := 'Data de emiss�o inv�lida';
          028: Result := 'C�digo de desconto via Telebradesco inv�lido';
          029: Result := 'Valor do desconto maior/igual ao valor do T�tulo';
          030: Result := 'Desconto a conceder n�o confere';
          031: Result := 'Concess�o de desconto - J� existe desconto anterior';
          033: Result := 'Valor do abatimento inv�lido';
          034: Result := 'Valor do abatimento maior/igual ao valor do T�tulo';
          036: Result := 'Concess�o abatimento - J� existe abatimento anterior';
          038: Result := 'Prazo para protesto inv�lido';
          039: Result := 'Pedido de protesto n�o permitido para o T�tulo';
          040: Result := 'T�tulo com ordem de protesto emitido';
          041: Result := 'Pedido cancelamento/susta��o para T�tulo sem instru��o de protesto';
          042: Result := 'C�digo para baixa/devolu��o inv�lido';
          045: Result := 'Nome do Sacado n�o informado';
          046: Result := 'Tipo/n�mero de inscri��o do Sacado inv�lidos';
          047: Result := 'Endere�o do Sacado n�o informado';
          048: Result := 'CEP Inv�lido';
          050: Result := 'CEP referente a um Banco correspondente';
          053: Result := 'Tipo de inscri��o do sacador avalista inv�lidos';
          060: Result := 'Movimento para T�tulo n�o cadastrado';
          085: Result := 'T�tulo com pagamento vinculado';
          086: Result := 'Seu n�mero inv�lido';
          094: Result := 'T�tulo Penhorado � Instru��o N�o Liberada pela Ag�ncia';
        end;
      end;
      35:
      begin
        case Geral.IMV(Motiv) of
          081: Result := 'Tentativas esgotadas, baixado';
          082: Result := 'Tentativas esgotadas, pendente';
          083: Result := 'Cancelado pelo Sacado e Mantido Pendente, conforme negocia��o';
          084: Result := 'Cancelado pelo sacado e baixado, conforme negocia��o';
        end;
      end;
    end;
  end;
const
  Tam = 2;
var
  i, Ini: Integer;
  CodMotiv, Motiv: String;
begin
  MeMotiv.Text := '';
  if Banco = 237 then //Apenas o Bradesco est� implementado
  begin
    Ini := 1;
    //
    for i := 1 to 5 do
    begin
      Motiv := Copy(Motivo, Ini, Tam);
      if Motiv = '00' then
      begin
        if i = 1 then
        begin
          CodMotiv := Motiv;
          Motiv    := ObtemMotivos(Ocorrencia, Motiv)
        end else
        begin
          CodMotiv := '';
          Motiv    := '';
        end;
      end else
      begin
        CodMotiv := Motiv;
        Motiv    := ObtemMotivos(Ocorrencia, Motiv);
      end;
      //
      Ini := Ini + Tam;
      if Length(Motiv) > 0 then
      begin
        MeMotiv.Text := MeMotiv.Text + CodMotiv + ' - ' + Motiv + sLineBreak;
      end;
    end;
  end;
  if Length(MeMotiv.Text) > 0 then
  begin
    MeMotiv.Visible   := True;
    Splitter4.Visible := True;   
  end else
  begin
    MeMotiv.Visible   := False;
    Splitter4.Visible := False;   
  end;
end;

procedure TFmCNAB_Ret2.QrOcorreu3CalcFields(DataSet: TDataSet);
var
  Cliente: Integer;
begin
{$IFDEF CO_DMKID_APP_003}
  if QrOcorreu3CLIENTELOTE.Value > 0 then
    Cliente := QrOcorreu3CLIENTELOTE.Value else
    Cliente := QrOcorreu3Cliente.Value;
  //
  QrOcorreu3SALDO.Value := QrOcorreu3Valor.Value + QrOcorreu3TaxaV.Value - QrOcorreu3Pago.Value;
  //
  //if CkAtualiza.Checked then
  //begin
    QrOcorreu3ATUALIZADO.Value := DMod.ObtemValorAtualizado(
      Cliente, 1, QrOcorreu3DataO.Value,
      (*Date*) (*QrLei3OcorrData.Value*)QrLei3OcorrData.Value, QrOcorreu3Data3.Value,
      QrOcorreu3Valor.Value, QrOcorreu3TaxaV.Value, 0 (*Desco*),
      QrOcorreu3Pago.Value, (*QrOcorreu3TaxaP.Value*)0, (*False*)True);
    QrOcorreu3ATZ_TEXTO.Value := Geral.FFT(QrOcorreu3ATUALIZADO.Value, 2, siNegativo);
  (*end else begin
    QrOcorreu3ATUALIZADO.Value := QrOcorreu3SALDO.Value;
    QrOcorreu3ATZ_TEXTO.Value := 'n�o calculado';
  end;*)
{$ENDIF}
end;

procedure TFmCNAB_Ret2.ReabreQueryEImprime(Ordenacao: Integer);
var
  Ordem, Texto1, Condicao1: String;
  Agrupa: Boolean;
begin
  Agrupa := False;
  case Ordenacao of
    1:
    begin
      Ordem := 'Bloqueto';
      Agrupa := False;
      Condicao1 := '''frxDsQuery."Bloqueto"''';
      Texto1 := '''[frxDsQuery."Bloqueto"]''';
    end;
    2:
    begin
      Ordem := 'DataQuita';
      Agrupa := True;
      Condicao1 := '''frxDsQuery."DataQuita"''';
      Texto1 := '''Data do cr�dito em conta: [frxDsQuery."DataQuita"]''';
    end;
    3:
    begin
      Ordem := 'DataOcorr';
      Agrupa := True;
      Condicao1 := '''frxDsQuery."DataOcorr"''';
      Texto1 := '''Data do pagamento: [frxDsQuery."DataOcorr"]''';
    end;
  end;
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT * FROM bafer ORDER BY ' + Ordem);
  Query.Open;
  frxGrade.Variables['VARF_G1_VISIBLE'] := Agrupa;
  frxGrade.Variables['VARF_G1_COND'] := Condicao1;
  frxGrade.Variables['VARF_G1_TEXT'] := Texto1;
  //frxGrade.DesignReport;
  MyObjects.frxMostra(frxGrade, 'Bloquetos liquidados');
end;

procedure TFmCNAB_Ret2.ReopenCNAB0Lei(Codigo: Integer);
begin
  QrLei.Close;
  QrLei.Params[0].AsInteger := FAbertosCli;
  UMyMod.AbreQuery(QrLei, Dmod.MyDB, 'TFmCNAB_Ret2.ReopenCNAB0Lei()');
  //
  QrLei.Locate('Codigo', Codigo, []);
end;

procedure TFmCNAB_Ret2.ReopenCNAB_Dir();
var
  Codigo, CliInt, Banco1: String;
begin
  QrCNAB_Dir.Close;
  QrCNAB_Dir.SQL.Clear;
  if CO_DMKID_APP = 4 then //Syndi2
  begin
    QrCNAB_Dir.SQL.Add('SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMEENT,');
    QrCNAB_Dir.SQL.Add('car.Nome NOMECARTEIRA, ban.Nome NOMEBANCO,');
    QrCNAB_Dir.SQL.Add('car.Banco1, ent.CliInt, dir.Carteira, ');
    QrCNAB_Dir.SQL.Add('dir.Nome, dir.Codigo, "" LayoutRem');
    QrCNAB_Dir.SQL.Add('FROM cnab_dir dir');
    QrCNAB_Dir.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=dir.carteira');
    QrCNAB_Dir.SQL.Add('LEFT JOIN bancos ban ON ban.Codigo=car.Banco1');
    QrCNAB_Dir.SQL.Add('LEFT JOIN cond cnd ON cnd.Cliente=dir.CliInt');
    QrCNAB_Dir.SQL.Add('');
    QrCNAB_Dir.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=dir.CliInt');
    QrCNAB_Dir.SQL.Add('WHERE dir.Envio=2');
    QrCNAB_Dir.SQL.Add('AND dir.Ativo=1');
    //
    if EdCliInt.ValueVariant <> 0 then
    begin
      CliInt := FormatFloat('0', EdCliInt.ValueVariant);
      QrCNAB_Dir.SQL.Add('AND ent.CliInt=' + CliInt);
    end;
    //
    if EdNomeEnt.Text <> '' then
    begin
      QrCNAB_Dir.SQL.Add('AND IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) LIKE "%' +
      EdNomeEnt.Text + '%"');
    end;
    //
    if EdCodigo.ValueVariant <> 0 then
    begin
      Codigo := FormatFloat('0', EdCodigo.ValueVariant);
      QrCNAB_Dir.SQL.Add('AND dir.Codigo=' + Codigo);
    end;
    //
    if EdNOMECARTEIRA.Text <> '' then
    begin
      QrCNAB_Dir.SQL.Add('AND car.Nome LIKE "%' +
      EdNOMECARTEIRA.Text + '%"');
    end;
    //
    if EdBanco1.ValueVariant <> 0 then
    begin
      Banco1 := FormatFloat('0', EdBanco1.ValueVariant);
      QrCNAB_Dir.SQL.Add('AND car.Banco1=' + Banco1);
    end;
    //
    if EdNome.Text <> '' then
    begin
      QrCNAB_Dir.SQL.Add('AND dir.Nome LIKE "%' +
      EdNome.Text + '%"');
    end;
    //
    if EdNomeBanco.Text <> '' then
    begin
      QrCNAB_Dir.SQL.Add('AND ban.Nome LIKE "%' +
      EdNomeBanco.Text + '%"');
    end;
    //
    QrCNAB_Dir.SQL.Add('ORDER BY NOMEENT');
    //
  end else begin
    QrCNAB_Dir.SQL.Add('SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMEENT,');
    QrCNAB_Dir.SQL.Add('car.Nome NOMECARTEIRA, ban.Nome NOMEBANCO,');
    QrCNAB_Dir.SQL.Add('car.Banco1, ent.CliInt, dir.CartRetorno Carteira, ');
    QrCNAB_Dir.SQL.Add('dir.DirRetorno Nome, dir.Codigo, dir.LayoutRem ');
    QrCNAB_Dir.SQL.Add('FROM cnab_cfg dir');
    QrCNAB_Dir.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=dir.cartRetorno');
    QrCNAB_Dir.SQL.Add('LEFT JOIN bancos ban ON ban.Codigo=car.Banco1');
    QrCNAB_Dir.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=car.ForneceI');
    QrCNAB_Dir.SQL.Add('/*LEFT JOIN cond cnd ON cnd.Cliente=dir.CliInt*/');
    QrCNAB_Dir.SQL.Add('');
    QrCNAB_Dir.SQL.Add('WHERE dir.Ativo=1');
    QrCNAB_Dir.SQL.Add('/*AND dir.Envio=2*/');
    QrCNAB_Dir.SQL.Add('');
    QrCNAB_Dir.SQL.Add('ORDER BY NOMEENT');
  end;
  UMyMod.AbreQuery(QrCNAB_Dir, Dmod.MyDB, 'TFmCNAB_Ret2.ReopenCNAB_Dir()');
end;

procedure TFmCNAB_Ret2.QrLeiAfterScroll(DataSet: TDataSet);
begin
  ReopenLeiSum;
  ReopenLeiItens(0);
  ReopenLeiAgr(0, 0, 0, 0, 0);
end;

procedure TFmCNAB_Ret2.ReopenLeiAgr(Data: TDateTime;
  CliInt, ForneceI, Apto, Mez: Integer);
begin
  QrLeiAgr.Close;
  QrLeiAgr.SQL.Clear;
  QrLeiAgr.SQL.Add('SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROPRIET,');
  QrLeiAgr.SQL.Add('SUM(lan.Credito) Valor, lan.Data, lan.Mez, lan.CliInt,');
  QrLeiAgr.SQL.Add('lan.ForneceI, lan.Depto Apto, ');
  if CO_DMKID_APP = 4 then //Syndi2
    QrLeiAgr.SQL.Add('imv.Unidade UH, ')
  else
    QrLeiAgr.SQL.Add('"" UH, ');
  QrLeiAgr.SQL.Add('IF(lan.FatID=610, 2, 1) TIPO_BLOQ');
  QrLeiAgr.SQL.Add('FROM ' + VAR_LCT + ' lan');
  QrLeiAgr.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrLeiAgr.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=lan.Cliente');
  if CO_DMKID_APP = 4  then //Syndi2
    QrLeiAgr.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=lan.Depto');
  QrLeiAgr.SQL.Add('WHERE lan.FatID in (600,601,610)');
  QrLeiAgr.SQL.Add('AND lan.FatNum=:P0');
  QrLeiAgr.SQL.Add('AND car.Tipo=2');
  QrLeiAgr.SQL.Add('AND lan.Sit<2');
  QrLeiAgr.SQL.Add('AND car.ForneceI=:P1');
  QrLeiAgr.SQL.Add('GROUP BY lan.Data, lan.CliInt, lan.ForneceI, lan.Depto, lan.Mez');
  QrLeiAgr.Params[00].AsInteger := QrLeiIDNum.Value;
  QrLeiAgr.Params[01].AsInteger := QrLeiEntidade.Value;
  UMyMod.AbreQuery(QrLeiAgr, Dmod.MyDB, 'TFmCNAB_Ret2.ReopenLeiAgr()');
  //
  QrLeiAgr.Locate('Data;CliInt;ForneceI;Apto;Mez',
    VarArrayOf([Data, CliInt, ForneceI, Apto, Mez]), []);
end;

procedure TFmCNAB_Ret2.ReopenLeiItens(Controle: Integer);
var
  OcorCodi: Integer;
begin
  QrLeiItens.Close;
  QrLeiItens.SQL.Clear;
  QrLeiItens.SQL.Add('SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE');
  QrLeiItens.SQL.Add('ent.Nome END NOMEPROPRIET, lan.Data, lan.Genero,');
  QrLeiItens.SQL.Add('lan.Descricao, lan.Credito, lan.Debito, lan.Compensado,');
  QrLeiItens.SQL.Add('lan.Sit, lan.Vencimento, lan.Mez, lan.Fornecedor,');
  QrLeiItens.SQL.Add('lan.Controle, lan.Sub, lan.Carteira, lan.NotaFiscal,');
  QrLeiItens.SQL.Add('lan.SerieCH, lan.Documento, lan.Cliente, lan.CliInt,');
  QrLeiItens.SQL.Add('lan.ForneceI, lan.DataDoc, lan.Depto Apto, lan.Multa,');
  QrLeiItens.SQL.Add('imv.Unidade UH, car.Tipo TipoCart, imv.Codigo COND,');
  QrLeiItens.SQL.Add('car.Nome NOMECART, car.Banco CartDest, lan.Tipo ');
  QrLeiItens.SQL.Add('FROM ' + VAR_LCT + ' lan');
  QrLeiItens.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrLeiItens.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=lan.Cliente');
  QrLeiItens.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=lan.Depto');
  QrLeiItens.SQL.Add('WHERE lan.FatID in (600,601,610)');
  QrLeiItens.SQL.Add('AND lan.FatNum=:P0');
  QrLeiItens.SQL.Add('AND car.Tipo=2');
  QrLeiItens.SQL.Add('AND lan.Sit<2');
  QrLeiItens.SQL.Add('AND car.ForneceI=:P1');
  QrLeiItens.SQL.Add('AND imv.Codigo=:P2');
  QrLeiItens.SQL.Add('ORDER BY Credito DESC');
  OcorCodi := Geral.IMV(QrLeiOcorrCodi.Value);
  // Liquida��o
  if UBancos.EhCodigoLiquidacao(OcorCodi, QrLeiBanco.Value, QrLeiTamReg.Value, '')
  // vale para bancos 001 e 748; mais algum?

  //Todos?
  {and (
       (QrLeiBanco.Value = 1)
    or (QrLeiBanco.Value = 748)
      )}
  then
  begin
    QrLeiItens.Params[00].AsInteger := QrLeiIDNum.Value;
    QrLeiItens.Params[01].AsInteger := FAbertosCli;
    QrLeiItens.Params[02].AsInteger := FAbertosCond;
    UMyMod.AbreQuery(QrLeiItens, Dmod.MyDB, 'TFmCNAB_Ret2.ReopenLeiItens()');
    QrLeiItens.Locate('Controle', Controle, []);
    //
  end;
  BtAgenda.Enabled := QrLeiDJM.Value < 0;
end;

procedure TFmCNAB_Ret2.ReopenLeiSum();
begin
  QrLeiSum.Close;
  QrLeiSum.SQL.Clear;
  QrLeiSum.SQL.Add('SELECT SUM(Credito) Credito,');
  QrLeiSum.SQL.Add('SUM(IF(lan.FatID=600, 1, 0)) ID600,');
  QrLeiSum.SQL.Add('SUM(IF(lan.FatID=601, 1, 0)) ID601,');
  QrLeiSum.SQL.Add('SUM(IF(lan.FatID=610, 1, 0)) ID610');
  QrLeiSum.SQL.Add('FROM ' + VAR_LCT + ' lan');
  QrLeiSum.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrLeiSum.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=lan.Cliente');
  if CO_DMKID_APP = 4 then //Syndi2  
    QrLeiSum.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=lan.Depto');
  QrLeiSum.SQL.Add('WHERE lan.FatID in (600,601,610)');
  QrLeiSum.SQL.Add('AND lan.FatNum=:P0');
  QrLeiSum.SQL.Add('AND car.Tipo=2');
  QrLeiSum.SQL.Add('AND lan.Sit<2');
  QrLeiSum.SQL.Add('AND car.ForneceI=:P1');
  if CO_DMKID_APP = 4 then //Syndi2
    QrLeiSum.SQL.Add('AND imv.Codigo=:P2');
  QrLeiSum.Params[00].AsInteger := QrLeiIDNum.Value;
  QrLeiSum.Params[01].AsInteger := FAbertosCli;
  if CO_DMKID_APP = 4 then //Syndi2
    QrLeiSum.Params[02].AsInteger := FAbertosCond;
  UMyMod.AbreQuery(QrLeiSum, Dmod.MyDB, 'TFmCNAB_Ret2.ReopenLeiSum()');
end;

procedure TFmCNAB_Ret2.ReopenLocEnt1(CNPJ: String);
begin
  QrLocEnt1.Close;
  QrLocEnt1.SQL.Clear;
  //
  {  2009-10-21
  if CO_DMKID_APP = 4 then //Syndi2
  begin
    QrLocEnt1.SQL.Add('SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE');
    QrLocEnt1.SQL.Add('ent.Nome END NOMEENT, ent.Codigo CLIENTE, cnd.Codigo COND');
    QrLocEnt1.SQL.Add('FROM cond cnd');
    QrLocEnt1.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente');
    QrLocEnt1.SQL.Add('WHERE (CASE WHEN ent.Tipo=0 THEN ent.CNPJ ELSE ent.CPF END)=:P0');
    QrLocEnt1.SQL.Add('/*');
    QrLocEnt1.SQL.Add('  AND cnd.CodCedente=:P1');
    QrLocEnt1.SQL.Add('*/');
    QrLocEnt1.SQL.Add('');
  end else begin
  }
    QrLocEnt1.SQL.Add('SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE');
    QrLocEnt1.SQL.Add('ent.Nome END NOMEENT, ent.Codigo CLIENTE, ent.CliInt');
    QrLocEnt1.SQL.Add('FROM entidades ent ');
    QrLocEnt1.SQL.Add('WHERE (CASE WHEN ent.Tipo=0 THEN ent.CNPJ ELSE ent.CPF END)=:P0');
    QrLocEnt1.SQL.Add('');
  //end;
  QrLocEnt1.Params[00].AsString := CNPJ;
  //QrLocEnt1.Params[01].AsString := CodCedente;
  UMyMod.AbreQuery(QrLocEnt1, Dmod.MyDB, 'TFmCNAB_Ret2.CarregaArquivo()');

end;

procedure TFmCNAB_Ret2.ReopenLocEnt2(Banco: Integer; Agencia, Cedente: String);
begin
  QrLocEnt2.Close;
  if CO_DMKID_APP = 4 then //Syndi2
  begin
    QrLocEnt2.SQL.Text :=
    'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ' +
    'ent.Nome END NOMEENT, ent.Codigo CLIENTE, cnd.Codigo CliInt ' +
    'FROM cond cnd ' +
    'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente ' +
    'WHERE cnd.Banco=:P0 ' +
    'AND cnd.Agencia=:P1 ' +
    'AND cnd.CodCedente=:P2 ';
  end else
  begin
    QrLocEnt2.SQL.Text := '*** SQL N�o Definida ***'
    {
    'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ' +
    'ent.Nome END NOMEENT, ent.Codigo CLIENTE, cnd.Codigo CliInt ' +
    'FROM cond cnd ' +
    'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente ' +
    'WHERE cnd.Banco=:P0 ' +
    'AND cnd.Agencia=:P1 ' +
    'AND cnd.CodCedente=:P2 ';
    }
  end;
  QrLocEnt2.Params[00].AsInteger := Banco;
  QrLocEnt2.Params[01].AsInteger := Geral.IMV(Agencia);
  QrLocEnt2.Params[02].AsString  := Cedente;
  UMyMod.AbreQuery(QrLocEnt2, Dmod.MyDB, 'TFmCNAB_Ret2.ReopenLocEnt2()');
end;

procedure TFmCNAB_Ret2.ReopenOcorreu(Codigo: Integer);
begin
  QrOcorreu3.Close;
  QrOcorreu3.Params[00].AsInteger := QrLei3ID_Link.Value;
  QrOcorreu3.Open;
  //
  (*if Codigo <> 0 then QrOcorreu.Locate('Codigo', Codigo, [])
  else QrOcorreu.Locate('Codigo', FOcorreu, []);*)
end;

procedure TFmCNAB_Ret2.ReopenQrPesq2(IDLink: Int64; Cliente: Integer);
begin
  QrPesq2.Close;
  if CO_DMKID_APP = 4 then //Syndi2
  begin
    QrPesq2.SQL.Text :=
    'SELECT DISTINCT lan.Vencimento, lan.Credito, ' +
    'cnd.PercMulta, cnd.PercJuros ' +
    'FROM ' + VAR_LCT + ' lan ' +
    'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira ' +
    'LEFT JOIN cond cnd ON cnd.Cliente=car.ForneceI ' +
    'WHERE lan.FatID in (600,601) ' +
    'AND lan.FatNum=:P0 ' +
    'AND car.Tipo=2 ' +
    'AND car.ForneceI=:P1 ';
  end else
  begin
    QrPesq2.SQL.Text := '*** SQL N�o Definida ***';
    {
    'SELECT DISTINCT lan.Vencimento, lan.Credito, ' +
    '0.0000 PercMulta, 0.0000 PercJuros ' +
    'FROM ' + VAR_LCT + ' lan ' +
    'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira ' +
    'WHERE lan.FatID in (600,601) ' +
    'AND lan.FatNum=:P0 ' +
    'AND car.Tipo=2 ' +
    'AND car.ForneceI=:P1 ';
    }
  end;
  QrPesq2.Params[00].AsFloat := IDLink;
  QrPesq2.Params[01].AsInteger := Cliente;
  UMyMod.AbreQuery(QrPesq2, Dmod.MyDB, 'TFmCNAB_Ret2.ErroLinha()');
end;

procedure TFmCNAB_Ret2.ReopenSQL3(ID_Link: Int64; Entidade: Integer);
begin
  QrPesq3.Close;
  QrPesq3.SQL.Clear;
  if CO_DMKID_APP = 4 then //Syndi2
  begin
    QrPesq3.SQL.Add('SELECT SUM(lan.Credito) Credito,');
    QrPesq3.SQL.Add('cnd.PercMulta, cnd.PercJuros, lan.Vencimento');
    QrPesq3.SQL.Add('FROM ' + VAR_LCT + ' lan');
    QrPesq3.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    QrPesq3.SQL.Add('LEFT JOIN cond cnd ON cnd.Cliente=car.ForneceI');
    QrPesq3.SQL.Add('WHERE lan.FatID in (600,601)');
    QrPesq3.SQL.Add('AND lan.FatNum=:P0');
    QrPesq3.SQL.Add('AND car.Tipo=2');
    QrPesq3.SQL.Add('AND car.ForneceI=:P1');
    QrPesq3.SQL.Add('GROUP BY lan.FatNum');
  end else begin
    QrPesq3.SQL.Add('SELECT SUM(lan.Credito) Credito,');
    QrPesq3.SQL.Add('0.0000 PercMulta, 0.0000 PercJuros, lan.Vencimento');
    QrPesq3.SQL.Add('FROM ' + VAR_LCT + ' lan');
    QrPesq3.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    QrPesq3.SQL.Add('WHERE lan.FatID in (600,601)');
    QrPesq3.SQL.Add('AND lan.FatNum=:P0');
    QrPesq3.SQL.Add('AND car.Tipo=2');
    QrPesq3.SQL.Add('AND car.ForneceI=:P1');
    QrPesq3.SQL.Add('GROUP BY lan.FatNum');
  end;
  QrPesq3.Params[00].AsFloat := ID_Link;
  QrPesq3.Params[01].AsInteger := Entidade;
  UMyMod.AbreQuery(QrPesq3, Dmod.MyDB, 'TFmCNAB_Ret2.ReopenSQL3()');
end;

procedure TFmCNAB_Ret2.QrLeiBeforeClose(DataSet: TDataSet);
begin
  QrLeiItens.Close;
  BtConcilia.Enabled := False;
end;

procedure TFmCNAB_Ret2.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmCNAB_Ret2.ExcluiArquivos();
begin
// N�o exclui! p�e na pasta "Lidos"
end;

procedure TFmCNAB_Ret2.ExcluiAtual1Click(Sender: TObject);
begin
  ExcluirItens(istAtual);
end;

procedure TFmCNAB_Ret2.ExcluiSelecionados1Click(Sender: TObject);
begin
  ExcluirItens(istSelecionados);
end;

procedure TFmCNAB_Ret2.ExcluiTodos1Click(Sender: TObject);
begin
  ExcluirItens(istTodos);
end;

function TFmCNAB_Ret2.ExecutaItens(CNAB_Lei: Integer): Boolean;

  procedure RepassarCobranca(MeuID, Envio, Movimento: Integer;
    ValArq: Double);
  var
    Data: String;
    Status, IDAliDup, Controle: Integer;
    Valor: Double;
  begin
    // Localizar a al�nea que ser� utilizada pelo Status e pela ocorr�ncia
    QrOcorBank.Close;
    QrOcorBank.Params[00].AsInteger := Envio;
    QrOcorBank.Params[01].AsInteger := Movimento;
    QrOcorBank.Open;
    if QrOcorBankCodigo.Value <> 0 then
    begin
      // Atualiza status da duplicata
      Data  := FormatDateTime(VAR_FORMATDATE, QrLei3OcorrData.Value);

      // Define status da duplicata
      Status := 0;
      case Movimento of
        06: // Liquida��o.
        begin
          if (int(QrLei3DDeposito.Value) > int(QrLei3OcorrData.Value)) then
            Status := Dmod.QrControle.FieldByName('LiqstaAnt').AsInteger;
          if (int(QrLei3DDeposito.Value) = int(QrLei3OcorrData.Value)) then
            Status := Dmod.QrControle.FieldByName('LiqstaVct').AsInteger;
          if (int(QrLei3DDeposito.Value) < int(QrLei3OcorrData.Value)) then
            Status := Dmod.QrControle.FieldByName('LiqstaVcd').AsInteger;
        end;
      end;
      if Status = 0 then Status := QrOcorBankCodigo.Value;
      //

      IDAliDup := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'ADupIts',
        'ADupIts', 'Controle');
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO adupits SET Alinea=:P0, DataA=:P1, ');
      Dmod.QrUpd.SQL.Add('Controle=:Pa, LotesIts=:Pb');
      //
      Dmod.QrUpd.Params[00].AsInteger := Status;
      Dmod.QrUpd.Params[01].AsString  := Data;
      //
      Dmod.QrUpd.Params[02].AsInteger := IDAliDup;
      Dmod.QrUpd.Params[03].AsInteger := MeuID;
      Dmod.QrUpd.ExecSQL;
      //

      // Ocorrencia associada ao movimento
      case QrOcorBankFormaCNAB.Value of
        0: Valor := 0;
        1: Valor := ValArq;
        2: Valor := QrOcorBankBase.Value;
        else Valor := 0;
      end;

      // Se o cliente tiver em seu cadastro (FmEntiJur1) uma configura��o
      // para a ocorerncia, ent�o, seguir orienta��o dela
      // mudadndo o valor caso configurado
      QrOcorCli.Close;
      QrOcorCli.Params[0].AsInteger := QrLei3Cliente.Value;
      QrOcorCli.Params[1].AsInteger := QrOcorBankCodigo.Value;
      QrOcorCli.Open;
      //
      if QrOcorCli.RecordCount > 0 then
      begin
        case QrOcorCliFormaCNAB.Value of
          1: Valor := ValArq; 
          2: Valor := QrOcorCliBase.Value;
          3: ;//Valor := Valor; o valor j� est� definido
          else Valor := 0;
        end;
      end;
      //

      if Valor > 0 then
      begin
        Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
          'Ocorreu', 'Ocorreu', 'Codigo');
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('INSERT INTO ocorreu SET AlterWeb=1, ');
        Dmod.QrUpd.SQL.Add('LotesIts=:P0, DataO=:P1, Ocorrencia=:P2, Valor=:P3, ');
        Dmod.QrUpd.SQL.Add('Codigo=:Pa');
        Dmod.QrUpd.Params[00].AsInteger := MeuID;
        Dmod.QrUpd.Params[01].AsString  := Data;
        Dmod.QrUpd.Params[02].AsInteger := QrOcorBankCodigo.Value; //
        Dmod.QrUpd.Params[03].AsFloat   := Valor;
        //
        Dmod.QrUpd.Params[04].AsInteger := Controle;
        Dmod.QrUpd.ExecSQL;
      end;
    end else
      Geral.MB_Aviso('N�o foi localizado c�digo de '+
        'status e/ou ocorr�ncia para o movimento '+Geral.FF0(Movimento)+'!');
  end;
var
  //Status, IDAliDup, MeuID, Controle: Integer;
  MeuID: Int64;
  Movimento, Envio: Integer;
  Valor: Double;
  //Data: String;
  //Valor: Double;
begin
  Screen.Cursor := crHourGlass;

  FNaoImprimeRecibo := False;

  QrLei3.Close;
  QrLei3.Open;
  //
  QrLei3.First;
  while not QrLei3.Eof do
  begin
    // Tarifas
    MeuID     := QrLei3ID_Link.Value;
    Envio     := Integer(ecnabRetorno)(*2*); //QrLei3Envio.Value;
    Movimento := (*QrLei3OcorrCodi.Value*)Geral.IMV(QrLei3OcorrCodi.Value);
    Valor     := QrLei3ValTarif.Value;
    //
    if Valor = 0 then
      Valor := QrLei3ValOutro2.Value;
    //
    RepassarCobranca(MeuID, Envio, Movimento, Valor);
    //
    //Custas de Processo
    if QrLei3ValCustas.Value > 0 then
    begin
      Geral.MensagemBox(
      'Cobran�a de custas n�o implementado. Solicite � dermatek!',
      'Aviso', MB_OK+MB_ICONINFORMATION);
{
      Envio     := Integer(ecnabRetorno)(*2*); //QrLei3Envio.Value;
      Movimento := ocorr�ncia=28 e motivo=08?;(*QrLei3OcorrCodi.Value*)Geral.IMV(QrLei3OcorrCodi.Value);
      RepassarCobranca(MeuID, Envio, Movimento, QrLei3ValCustas.Value);
}
    end;
    // A��es espec�ficas do movimento
    case Movimento of
      02: ;// Nada - s� recebimento;
      06: Liquidacao();
      else begin
        (*if QrLei3ValTarif.Value > 0 then
        begin
          Data  := FormatDateTime(VAR_FORMATDATE, QrLei3OcorrData.Value);
          Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
            'Ocorreu', 'Ocorreu', 'Codigo');
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('INSERT INTO ocorreu SET AlterWeb=1, ');
          Dmod.QrUpd.SQL.Add('LotesIts=:P0, DataO=:P1, Ocorrencia=:P2, ');
          Dmod.QrUpd.SQL.Add('Valor=:P3, MoviBank=:P4, Codigo=:Pa');
          Dmod.QrUpd.Params[00].AsInteger := MeuID;
          Dmod.QrUpd.Params[01].AsString  := Data;
          Dmod.QrUpd.Params[02].AsInteger := -1; // Ocorrencia banc�ria
          Dmod.QrUpd.Params[03].AsFloat   := QrLei3ValCustas.Value;
          Dmod.QrUpd.Params[04].AsInteger := Movimento;
          //
          Dmod.QrUpd.Params[05].AsInteger := Controle;
          Dmod.QrUpd.ExecSQL;
        end (*else
          Geral.MB_Aviso('N�o h� a��o espec�fica implementada '+
            'para o movimento "'+QrLei3OcorrCodi.Value+' - '+
            UBancos.CNABTipoDeMovimento(1, ecnabRetorno, Movimento,
            0, siPositivo)+'"!');*)
      end;
    end;
    // Considerar feito
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE cnab_lei SET Step=1 ');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrLei3Codigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    QrLei3.Next;
  end;
  QrLei3.Close;
  QrLei3.Open;
  Result := True;
  Screen.Cursor := crDefault;
end;

procedure TFmCNAB_Ret2.ExcluiBloquetosnolocalizados1Click(Sender: TObject);
begin
  ExcluirItens(istExtra1);
end;

procedure TFmCNAB_Ret2.ExcluirItens(Acao: TSelType);
  procedure ExcluiAtual;
  begin
    Screen.Cursor := crHourGlass;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM cnab_lei WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrLeiCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    Screen.Cursor := crDefault;
  end;
var
  Prox, i: Integer;
begin
  if Acao = istSelecionados then
    if DBGLei.SelectedRows.Count < 2 then Acao := istAtual;
  case Acao of
    istAtual:
    begin
      if Geral.MensagemBox(('Confirma a exclus�o do item selecionado?'),
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then ExcluiAtual;
    end;
    istSelecionados:
    begin
      if Geral.MensagemBox(('Confirma a exclus�o dos ' + Geral.FF0(
      DBGLei.SelectedRows.Count) + ' itens selecionados?'), 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        with DBGLei.DataSource.DataSet do
        for i:= 0 to DBGLei.SelectedRows.Count-1 do
        begin
          GotoBookmark(pointer(DBGLei.SelectedRows.Items[i]));
          ExcluiAtual;
        end;
      end;
    end;
    istTodos:
    begin
      if Geral.MensagemBox('Confirma a exclus�o de todos itens ?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        QrLei.First;
        while not QrLei.Eof do
        begin
          ExcluiAtual;
          QrLei.Next;
        end;
      end;
    end;
    istExtra1:
    begin
      if Geral.MensagemBox('Confirma a exclus�o de todos boletos n�o localizados?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        QrLei.First;
        while not QrLei.Eof do
        begin
          if (QrLeiItens.State = dsInactive)
          or (QrLeiItens.RecordCount = 0) then
            ExcluiAtual;
          QrLei.Next;
        end;
      end;
    end;
  end;
  //
  Prox := UMyMod.ProximoRegistro(QrLei, 'Codigo', QrLeiCodigo.Value);
  ReopenCNAB0Lei(Prox);
end;

procedure TFmCNAB_Ret2.QrLeiAfterOpen(DataSet: TDataSet);
begin
  BtConcilia.Enabled := QrLei.RecordCount > 0;
end;

procedure TFmCNAB_Ret2.BtConciliaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMConcilia, BtConcilia);
end;

procedure TFmCNAB_Ret2.ConciliaAtual1Click(Sender: TObject);
begin
  ConciliaItens(istAtual);
end;

procedure TFmCNAB_Ret2.ConciliaSelecionados1Click(Sender: TObject);
begin
  ConciliaItens(istSelecionados);
end;

procedure TFmCNAB_Ret2.ConciliaTodos1Click(Sender: TObject);
begin
  ConciliaItens(istTodos);
end;

procedure TFmCNAB_Ret2.ConciliaItens(Acao: TSelType);
  function EntidadeDeBanco(var EntBanco: Integer; const Banco: Integer): Boolean;
  begin
    QrLEB.Close;
    QrLEB.Params[0].AsInteger := Banco;
    UMyMod.AbreQuery(QrLEB, Dmod.MyDB, 'TFmCNAB_Ret2.ConciliaItens()');
    EntBanco := QrLEBEntidade.Value;
    Result := EntBanco > 0;
    if not Result then Geral.MensagemBox(('Entidade banc�ria n�o ' +
    'localizada para o banco: '+FormatFloat('000', Banco)+'!'), 'Erro',
    MB_OK+MB_ICONERROR);
    if not Result then Screen.Cursor := crDefault;
  end;
  function UnidadeHabitacional(var Dono: Integer; var Unidade: Integer;
    const IDNum: Integer; const (*T600, T601,*) T610: Integer): Boolean;
  begin
    if T610 > 0 then
    begin
      QrLUH.Close;
      QrLUH.SQL.Clear;
      QrLUH.SQL.Add('SELECT bp.CodigoEnt Propriet, bp.CodigoEsp Apto');
      QrLUH.SQL.Add('FROM bloqparcpar bpp');
      QrLUH.SQL.Add('LEFT JOIN bloqparc bp ON bp.Codigo=bpp.Codigo');
      QrLUH.SQL.Add('LEFT JOIN ' + VAR_LCT + ' lan ON lan.FatNum=bpp.FatNum');
      QrLUH.SQL.Add('  AND lan.ForneceI=bp.CodigoEnt');
      QrLUH.SQL.Add('  AND lan.Depto=bp.CodigoEsp');
      QrLUH.SQL.Add('WHERE lan.Sit < 2');
      QrLUH.SQL.Add('AND lan.Tipo=2');
      QrLUH.SQL.Add('AND bpp.FatNum=' + Geral.FF0(IDNum));
    end else begin
      QrLUH.Close;
      QrLUH.SQL.Clear;
      QrLUH.SQL.Add('SELECT DISTINCT Apto, Propriet');
      QrLUH.SQL.Add('FROM arreits');
      QrLUH.SQL.Add('WHERE Boleto=' + Geral.FF0(IDNum));
      QrLUH.SQL.Add('');
      QrLUH.SQL.Add('UNION');
      QrLUH.SQL.Add('');
      QrLUH.SQL.Add('SELECT DISTINCT Apto, Propriet');
      QrLUH.SQL.Add('FROM consits');
      QrLUH.SQL.Add('WHERE Boleto=' + Geral.FF0(IDNum));
      QrLUH.SQL.Add('');
      //QrLUH.Params[0].AsInteger := IDNum;
      //QrLUH.Params[1].AsInteger := IDNUm;
    end;
    UMyMod.AbreQuery(QrLUH, Dmod.MyDB, 'TFmCNAB_Ret2.ConciliaItens()');
    Unidade := QrLUHApto.Value;
    Dono := QrLUHPropriet.Value;
    Result := (Unidade > 0) and (Dono <> 0);
    if not Result then
    begin
      Unidade := QrLeiItensApto.Value;
      Dono    := QrLeiItensForneceI.Value;
      Result := (Unidade > 0) and (Dono <> 0);
      if not Result then
      begin
        Unidade := QrLeiAgrApto.Value;
        Dono    := QrLeiAgrForneceI.Value;
        Result := (Unidade > 0) and (Dono <> 0);
      end;
    end;
    if not Result then Geral.MensagemBox(('Unidade habitacional n�o ' +
    'localizada para o "IDNum": '+Geral.FF0(IDNum)+'!'), 'Erro',
    MB_OK+MB_ICONERROR);
    if not Result then Screen.Cursor := crDefault;
  end;
  procedure ConciliaAtual;
    {function ContaDaOcorrencia(const Banco: Integer;
      const Ocorrencia: String; var Genero: Integer): Boolean;
    begin
      Genero := 0;
      QrLocOcor.Close;
      QrLocOcor.Params[0].AsInteger := Banco;
      QrLocOcor.Params[1].AsInteger := Geral.IMV(Ocorrencia);
      UMyMod.AbreQuery(QrLocOcor);
      if QrLocOcor.RecordCount = 0 then Geral.MensagemBox((
      'N�o h� cadastro da ocorr�ncia ' + Ocorrencia +
      ' no banco ' + FormatFloat('000', Banco) + '!'), 'Aviso',
      MB_OK+MB_ICONWARNING) else if QrLocOcorGenero.Value < 1 then
      Geral.MensagemBox(('N�o foi definida nenhuma conta no ' +
      'cadastro da ocorr�ncia ' + Ocorrencia +
      ' no banco ' + FormatFloat('000', Banco) + '!'), 'Aviso',
      MB_OK+MB_ICONWARNING) else Genero := QrLocOcorGenero.Value;
      Result := Genero > 0;
      if not Result then Screen.Cursor := crDefault;
      // N�o fechar QrLocOcor !!!!
      //QrLocOcor.Close;
    end;}
    function QuitaDocumento(Credito, MoraVal, MultaVal: Double): Integer;
    var
      Controle2: Integer;
      Compensado, PagoBanco: String;
    begin
      Result := 0;
      PagoBanco := Geral.FDT(QrLeiOcorrData.Value, 1);
      Compensado := Geral.FDT(QrLeiQuitaData.Value, 1);
      //
      Controle2 := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
        'Controle', VAR_LCT, VAR_LCT, 'Controle');
      UFinanceiro.LancamentoDefaultVARS;
      //

      // CNAB_Sit = 2 -> Baixa pelo CNAB
      FLAN_Tipo           := 1;
      FLAN_Sit            := 3;
      FLAN_CNAB_Sit       := 2;
      //
      FLAN_Data           := Compensado;
      FLAN_Controle       := Controle2;
      FLAN_Descricao      := QrLeiItensDescricao.Text;
      FLAN_NotaFiscal     := QrLeiItensNotaFiscal.Value;
      FLAN_Debito         := QrLeiItensDebito.Value;
      FLAN_Credito        := Credito;
      FLAN_Compensado     := PagoBanco; // antes de compensar (D+ do banco)
      FLAN_SerieCH        := QrLeiItensSerieCH.Value;
      FLAN_Documento      := Trunc(QrLeiItensDocumento.Value + 0.01);
      FLAN_Cliente        := QrLeiItensCliente.Value;
      FLAN_Fornecedor     := QrLeiItensFornecedor.Value;
      FLAN_ID_Pgto        := QrLeiItensControle.Value;
      FLAN_Sub            := QrLeiItensSub.Value;
      FLAN_DataCad        := FormatDateTime(VAR_FORMATDATE, Date);
      FLAN_UserCad        := VAR_USUARIO;
      FLAN_DataDoc        := FormatDateTime(VAR_FORMATDATE, QrLeiItensDataDoc.Value);
      FLAN_Vencimento     := FormatDateTime(VAR_FORMATDATE, QrLeiItensVencimento.Value);
      FLAN_Carteira       := QrLeiItensCartDest.Value;
      FLAN_CliInt         := QrLeiItensCliInt.Value;
      FLAN_ForneceI       := QrLeiItensForneceI.Value;
      FLAN_Depto          := QrLeiItensApto.Value;
      FLAN_Genero         := QrLeiItensGenero.Value;
      FLAN_MoraVal        := MoraVal;
      FLAN_MultaVal       := MultaVal;
      // ERRO!!!
      //FLAN_Mez            :=Geral.TFT_NULL(Geral.FF0(QrLeiItensMez.Value), 0, siNegativo);
      FLAN_Mez            := FormatFloat('0', QrLeiItensMez.Value);
      //
      UFinanceiro.InsereLancamento;
      Result := Result + 1;

      {
      Dmod.QrUpdM.SQL.Clear;
      Dmod.QrUpdM.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Sit=3, ');
      Dmod.QrUpdM.SQL.Add('Compensado=:P0, DataAlt=:P1, UserAlt=:P2');
      Dmod.QrUpdM.SQL.Add('WHERE Controle=:P3 AND Sub=:P4 AND Tipo=2');
      Dmod.QrUpdM.Params[00].AsString  := Compensado;
      Dmod.QrUpdM.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
      Dmod.QrUpdM.Params[02].AsInteger := VAR_USUARIO;
      Dmod.QrUpdM.Params[03].AsFloat   := QrLeiItensControle.Value;
      Dmod.QrUpdM.Params[04].AsInteger := QrLeiItensSub.Value;
      Dmod.QrUpdM.ExecSQL;
      }
      UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
      'Sit', 'Compensado'], ['Controle', 'Sub', 'Tipo'], [3, Compensado], [
      QrLeiItensControle.Value, QrLeiItensSub.Value, 2], True, '');
      //
      Result := Result + 2;
      //
    end;
    //

  var
    Dono, Unidade, Ocorrencia, Genero, EntBanco, SomaQ, CliInt: Integer;
    Fator, SomaVal, Valor, Multa, Juros, Diferenca, FatorM{, FatorJ}, ValTarif: Double;
    InfoTar: Boolean;
    OcorrTxt: String;
  begin
    InfoTempo(Now, 'Nova concilia��o', True);
    if not EntidadeDeBanco(EntBanco, QrLeiBanco.Value) then Exit;
    InfoTempo(Now, 'Entidade de banco', False);
    if not UnidadeHabitacional(Dono, Unidade, QrLeiIDNum.Value,
      Trunc(QrLeiSumID610.Value)) then Exit;
    InfoTempo(Now, 'Unidade habitacional', False);

    //
    // Verifica antes de conciliar se a conta da ocorr�ncia
    // j� est� configurada
    UBancos.InformaTarifaDeCobrancaImplementado(QrLeiBanco.Value, InfoTar);
    // Quando a tarifa vem na mesma linha do pagamento ...
    if (QrLeiValTarif.Value > 0)
    // ou quando o banco (756) n�o informa no arquivo mas � informado
    // o valor no cadastro do condom�nio (QrCond)
    or (InfoTar = False) then
    begin
      Genero := 0;
      OcorrTxt := QrLeiOcorrCodi.Value;//-1;
      if not UBancos.ContaDaOcorrencia(QrLeiBanco.Value,
      QrLeiTamReg.Value, OcorrTxt, '', Genero) then  Exit;
    end;
    InfoTempo(Now, 'Conta da ocorr�ncia', False);
    //

    Screen.Cursor := crHourGlass;
    Ocorrencia := Geral.IMV(QrLeiOcorrCodi.Value);
    EntBanco := 0;
    if UBancos.EhCodigoLiquidacao(Ocorrencia, QrLeiBanco.Value, QrLeiTamReg.Value, '') then
    begin
      InfoTempo(Now, 'C�digo da liquida��o', False);
      if QrLeiSumCredito.Value <= 0 then
      begin
        Geral.MensagemBox(('Valor do boleto zerado para o IDNum = '+
        Geral.FF0(QrLeiIDNum.Value)+'!'), 'Erro', MB_OK+MB_ICONERROR);
        Screen.Cursor := crDefault;
        Exit;
      end;
      if (QrLeiSumCredito.Value <> QrLeiValTitul.Value)
      and (QrLeiValTitul.Value > 0) then
      begin
        Geral.MB_Erro('Valores n�o conferem para o IDNum = ' +
          Geral.FF0(QrLeiIDNum.Value) + '!' + sLineBreak +
          'Se este bloqueto foi re-gerado pela internet com juros e multa,' + sLineBreak +
          'ou se o banco cobrou juros/multa e n�o informou nos campos' + sLineBreak +
          'corretos, utilize a op��o "Ajusta valores do bloqueto atual"' + sLineBreak +
          'do bot�o "Bloqueto" antes de conciliar este bloqueto!');
        Screen.Cursor := crDefault;
        Exit;
      end;
      UBancos.FatorDeRecebimento(QrLeiBanco.Value, QrLeiValPago.Value,
        QrLeiValTarif.Value, QrLeiSumCredito.Value, Fator);
      InfoTempo(Now, 'Fator de recebimento', False);
      QrLeiItens.First;
      SomaVal := 0;
      while not QrLeiItens.Eof do
      begin
        Valor := Round(Fator * QrLeiItensCredito.Value) / 100;
        SomaVal  := SomaVal + Valor;
        //
        QrLeiItens.Next;
      end;
      UBancos.DiferencaDeRecebimento(QrLeiBanco.Value, QrLeiValPago.Value,
        QrLeiValTarif.Value, SomaVal, Diferenca);
      {Geral.MensagemBox(('Diferen�a: '+Geral.FFT(
        Diferenca, 4, siNegativo)), 'Mensagem',
        MB_OK+MB_ICONINFORMATION);}
      //
      if QrLeiValMulta.Value < 0.01 then FatorM := 0 else
      UBancos.FatorMultaDeRecebimento(QrLeiBanco.Value, QrLeiValPago.Value,
        QrLeiValTarif.Value, QrLeiValMulta.Value, FatorM);
      //
      SomaQ := 0;
      InfoTempo(Now, 'Total, diferen�a e fator de multa', False);
      QrLeiItens.First;
      while not QrLeiItens.Eof do
      begin
        Valor := Round(Fator * QrLeiItensCredito.Value) / 100;
        if QrLeiItens.RecNo = 1 then Valor := Valor + Diferenca;

        //
        Multa := 0;
        Juros := 0;
        // Quando juros e multa s�o juntos
        if QrLeiValJuMul.Value >= 0.01 then
        begin
          Multa := Round(QrLeiItensCredito.Value * QrLeiItensMulta.Value) / 100;
          Juros := Valor - Multa - QrLeiItensCredito.Value;
          if Juros  < 0 then
          begin
            Juros := 0;
            Multa := Valor - QrLeiItensCredito.Value;
          end;
        end else
        if  (QrLeiValMulta.Value >= 0.01)
        and (QrLeiValJuros.Value >= 0.01) then
        begin
          Multa := Valor * FatorM;
          Juros := Valor - Multa - QrLeiItensCredito.Value;
        end else if QrLeiValMulta.Value <> 0 then
          Multa := Valor - QrLeiItensCredito.Value
        else
          Juros := Valor - QrLeiItensCredito.Value;
        //
        SomaQ := SomaQ + QuitaDocumento(Valor, Juros, Multa);
        InfoTempo(Now, 'Quita documento', False);
        QrLeiItens.Next;
      end;
      //

      // INSERE TARIFA DE COBRAN�A BANC�RIA
      //InfoTar := UBancos.InformaTarifaDeCobranca(QrLeiBanco.Value);
      // Quando a tarifa vem na mesma linha do pagamento ...
      if (QrLeiValTarif.Value > 0)
      // ou quando o banco (756) n�o informa no arquivo mas � informado
      // o valor no cadastro do condom�nio (QrCond)
      or (InfoTar = False) then
      begin
        Genero := 0;
        OcorrTxt := QrLeiOcorrCodi.Value;//-1;
        Unidade := 0;
        Dono    := 0;
        if not UBancos.ContaDaOcorrencia(QrLeiBanco.Value,
        QrLeiTamReg.Value, OcorrTxt, '', Genero) then
        begin
          Exit;
          Screen.Cursor := crDefault;
        end;
        //fatid: como saber? pode ter varios!
        // J� feito acima
        {
        if not UnidadeHabitacional(Dono, Unidade, QrLeiIDNum.Value,
        QrLeiTIPO_BOL.Value) then
        begin
          Exit;
          Screen.Cursor := crDefault;
        end;
        }
        QrLocCta.Close;
        QrLocCta.Params[0].AsInteger := Genero;
        UMyMod.AbreQuery(QrLocCta, Dmod.MyDB, 'TFmCNAB_Ret2.ConciliaItens()');
        if QrLocCtaMensal.Value <> 'V' then FLAN_Mez := '' else
        FLAN_Mez :=
          Geral.FF0(dmkPF.DataToAnoMes(QrLeiQuitaData.Value));
        //
        if QrLeiDtaTarif.Value > 0 then
        begin
          FLAN_DataDoc    := Geral.FDT(QrLeiOcorrData.Value, 1);
          FLAN_Data       := Geral.FDT(QrLeiDtaTarif.Value, 1);
          FLAN_Vencimento := Geral.FDT(QrLeiDtaTarif.Value, 1);
          FLAN_DataCad    := Geral.FDT(Date, 1);
        end else begin
          FLAN_DataDoc    := Geral.FDT(QrLeiOcorrData.Value, 1);
          FLAN_Data       := Geral.FDT(QrLeiQuitaData.Value, 1);
          FLAN_Vencimento := Geral.FDT(QrLeiQuitaData.Value, 1);
          FLAN_DataCad    := Geral.FDT(Date, 1);
        end;
        if InfoTar then ValTarif := QrLeiValTarif.Value else
        begin
        //if
          DmodG.DadosRetDeEntidade(QrLeiEntidade.Value, CliInt); //then
          ValTarif := DModG.QrLocCIVTCBBNITAR.Value;
        end;
        FLAN_Descricao  := QrLeiDescriCNR.Value;
        FLAN_Compensado := Geral.FDT(QrLeiQuitaData.Value, 1);
        //FLAN_Duplicata  := '';
        FLAN_Doc2       := Geral.FF0(QrLeiIDNum.Value);
        //FLAN_Serie      := '';
        FLAN_Documento  := QrLeiIDNum.Value;
        FLAN_Tipo       := QrLeiTipoCart.Value;
        FLAN_Carteira   := QrLeiItensCartDest.Value;
        FLAN_Genero     := Genero;
        //FLAN_NotaFiscal := 0;
        FLAN_Sit        := 3;
        FLAN_Controle   := 0;
        //
        FLAN_Credito    := 0;
        FLAN_Cliente    := 0;
        //
        FLAN_Debito     := ValTarif;
        FLAN_Fornecedor := EntBanco;
        //
        FLAN_UserCad    := VAR_USUARIO;
        FLAN_CliInt     := QrLeiEntidade.Value;
        FLAN_Depto      := Unidade;
        FLAN_ForneceI   := Dono;
        FLAN_FatID      := 0;
        FLAN_FatID_Sub  := 0;
        FLAN_FatNum     := 0;
        FLAN_FatParcela := 0;
        //
        FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB,
          'Livres', 'Controle', VAR_LCT, VAR_LCT, 'Controle');
        //
        if UFinanceiro.InsereLancamento then ;
        InfoTempo(Now, 'Tarifa de cobran�a', False);
      end;

      if SomaQ = QrLeiItens.RecordCount * 3 then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE cnab_lei SET Step=1 ');
        Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := QrLeiCodigo.Value;
        Dmod.QrUpd.ExecSQL;
      end;
    end else begin
      Genero := 0;
      if not UBancos.ContaDaOcorrencia(QrLeiBanco.Value, QrLeiTamReg.Value,
      QrLeiOcorrCodi.Value, '', Genero) then
      begin
        Exit;
        Screen.Cursor := crDefault;
      end;
      InfoTempo(Now, 'Conta da ocorr�ncia', False);
      Unidade := 0;
      Dono    := 0;
      if not UnidadeHabitacional(Dono, Unidade, QrLeiIDNum.Value,
        Trunc(QrLeiSumID610.Value)) then
      begin
        Exit;
        Screen.Cursor := crDefault;
      end;
      InfoTempo(Now, 'Unidade habitacional', False);







      case Ocorrencia of
        028:
        begin
          case QrLeiBanco.Value of
            748:
            begin
              QrLocCta.Close;
              QrLocCta.Params[0].AsInteger := Genero;
              UMyMod.AbreQuery(QrLocCta, Dmod.MyDB, 'TFmCNAB_Ret2.ConciliaItens()');
              //
              if QrLocCtaMensal.Value <> 'V' then FLAN_Mez := '' else
              FLAN_Mez :=
                Geral.FF0(dmkPF.DataToAnoMes(QrLeiQuitaData.Value));
              //
              FLAN_Data       := Geral.FDT(QrLeiQuitaData.Value, 1);
              FLAN_Vencimento := Geral.FDT(QrLeiQuitaData.Value, 1);
              FLAN_DataCad    := Geral.FDT(QrLeiOcorrData.Value, 1);

              FLAN_Descricao  := QrLeiDescriCNR.Value;
              FLAN_Compensado := Geral.FDT(QrLeiQuitaData.Value, 1);
              //FLAN_Duplicata  := '';
              FLAN_Doc2       := Geral.FF0(QrLeiIDNum.Value);
              //FLAN_Serie      := '';
              FLAN_Documento  := QrLeiIDNum.Value;
              FLAN_Tipo       := QrLeiTipoCart.Value;
              FLAN_Carteira   := QrLeiItensCartDest.Value;
              FLAN_Genero     := Genero;
              //FLAN_NotaFiscal := 0;
              FLAN_Sit        := 3;
              FLAN_Controle   := 0;
              //
              FLAN_Credito    := 0;
              FLAN_Cliente    := 0;
              //
              FLAN_Debito     := QrLeiValTitul.Value;
              FLAN_Fornecedor := EntBanco;
              //
              FLAN_UserCad    := VAR_USUARIO;
              FLAN_DataDoc    := Geral.FDT(QrLeiOcorrData.Value, 1);
              FLAN_CliInt     := QrLeiEntidade.Value;
              FLAN_Depto      := Unidade;
              FLAN_ForneceI   := Dono;
              FLAN_FatID      := 0;
              FLAN_FatID_Sub  := 0;
              FLAN_FatNum     := 0;
              FLAN_FatParcela := 0;
              //
              FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB,
                'Livres', 'Controle', VAR_LCT, VAR_LCT, 'Controle');
              //
              if UFinanceiro.InsereLancamento then
              begin
                Dmod.QrUpd.SQL.Clear;
                Dmod.QrUpd.SQL.Add('UPDATE cnab_lei SET Step=1 ');
                Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0');
                Dmod.QrUpd.Params[0].AsInteger := QrLeiCodigo.Value;
                Dmod.QrUpd.ExecSQL;
              end;
              InfoTempo(Now, 'Ocorr�ncia', False);
            end;
            else Geral.MensagemBox(('Este banco n�o possui a��o ' +
            'definida no aplicativo para a ocorr�cia informada!'), 'Aviso',
            MB_OK+MB_ICONWARNING);
          end;
        end else Geral.MensagemBox('Banco sem a��es definidas!', 'Aviso',
        MB_OK+MB_ICONWARNING);
      end;
    end;
    Screen.Cursor := crDefault;
  end;
var
  Prox, i: Integer;
begin
  Memo3.Lines.Clear;
  VAR_NaoReabrirLct := True;
  try
    if Acao = istSelecionados then
      if DBGLei.SelectedRows.Count < 2 then Acao := istAtual;
    case Acao of
      istAtual:
      begin
        if Geral.MensagemBox(('Confirma a concilia��o do item selecionado?'),
        'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then ConciliaAtual;
      end;
      istSelecionados:
      begin
        if Geral.MensagemBox(('Confirma a concilia��o dos ' + Geral.FF0(
        DBGLei.SelectedRows.Count) + ' itens selecionados?'), 'Pergunta',
        MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
        begin
          with DBGLei.DataSource.DataSet do
          for i:= 0 to DBGLei.SelectedRows.Count-1 do
          begin
            GotoBookmark(pointer(DBGLei.SelectedRows.Items[i]));
            ConciliaAtual;
          end;
        end;
      end;
      istTodos:
      begin
        if Geral.MensagemBox('Confirma a concilia��o de todos itens ?',
        'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
        begin
          QrLei.First;
          while not QrLei.Eof do
          begin
            ConciliaAtual;
            QrLei.Next;
          end;
        end;
      end;
    end;
  finally
    VAR_NaoReabrirLct := False;
    UFinanceiro.RecalcSaldoCarteira(QrLeiItensCartDest.Value, nil, False, False);
    UFinanceiro.RecalcSaldoCarteira(QrLeiItensCarteira.Value, nil, False, False);
    Prox := UMyMod.ProximoRegistro(QrLei, 'Codigo', QrLeiCodigo.Value);
    ReopenCNAB0Lei(Prox);
    InfoTempo(Now, 'Rec�lculos de saldos', False);
  end;
end;

procedure TFmCNAB_Ret2.HabilitaBotoes();
begin
  QrTem.Close;
  UMyMod.AbreQuery(QrTem, Dmod.MyDB, 'TFmCNAB_Ret2.HabilitaBotoes()');
  BtCarrega.Enabled := FLin1 > 0;
  //
  if (CO_DMKID_APP = 3) or (CO_DMKID_APP = 22) then //Creditor e Credito2
    BtAbertos.Enabled := False
  else
    BtAbertos.Enabled := QrTemItens.Value > 0;
end;

procedure TFmCNAB_Ret2.BitBtn1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  Timer1.Enabled := True;
  //
  PnCarrega.Visible := True;
  PnMovimento.Visible := False;
  Application.ProcessMessages;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmCNAB_Ret2.BtBufferClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMBuffer, BtBuffer);
end;

function TFmCNAB_Ret2.CarregaItensRetorno(LinA, QuemChamou: Integer): Boolean;
var
  Extensao, Arquivo, FileName: String;
  BcoCod, Posicoes: Integer;
begin
  Result := True;
  MyObjects.LimpaGrade(Grade1, 1, 1, True);
  if GradeA.Cells[1,1] = '' then
  begin
    Result := False;
    Exit;
  end;
  BcoCod   := Geral.IMV(GradeA.Cells[10, LinA]);
  Posicoes := Geral.IMV(GradeA.Cells[11, LinA]);
  //
  if BcoCod <> QrCNAB_DirBanco1.Value then
  begin
    Geral.MensagemBox('Banco informado no cadastro da carteira n�o ' +
    'confere com o banco informado no arquivo CNAB!' + sLineBreak +
    'Banco da carteira: ' + FormatFloat('000', QrCNAB_DirBanco1.Value) +
    sLineBreak + 'Banco do arquivo: ' + FormatFloat('000', BcoCod) +
    '     (linha '+Geral.FF0(LinA) + ' - Proced�ncia ' + Geral.FF0(QuemChamou) + ')',
    'Aviso de diverg�ncia!', MB_OK+MB_ICONWARNING);
    //Exit;
  end;
  Arquivo   := Trim(ExtractFileName(GradeA.Cells[1, LinA]));
  if Arquivo <> '' then
  begin
    Extensao  := '';
    FileName := dmkPF.CaminhoArquivo(QrCNAB_DirNome.Value, Arquivo, Extensao);
    CarregaItensRetornoA(FileName, QrCNAB_DirCodigo.Value,
      Geral.IMV(GradeA.Cells[00, LinA]),
      Geral.IMV(GradeA.Cells[09, LinA]), Posicoes, BcoCod);
    {case BcoCod of
      748: Result := CarregaItensRetornoSicredi(FileName,
                     QrCNAB_DirCodigo.Value,
                     Geral.IMV(GradeA.Cells[00, LinA]),
                     BcoCod, Geral.IMV(GradeA.Cells[09, LinA]));
      else
      begin
        Result := False;
      end;
    end;}
    VerificaImplementacoes(LinA);
  end else Result := False;
end;

procedure TFmCNAB_Ret2.QrCNAB_DirAfterScroll(DataSet: TDataSet);
begin
  Screen.Cursor := crHourGlass;
  MyObjects.LimpaGrade(GradeA, 1, 1, True);
  MyObjects.LimpaGrade(Grade1, 1, 1, True);
  //MyObjects.LimpaGrade(GradeB, 1, 1);
  //MyObjects.LimpaGrade(GradeC, 1, 1);
  FLinA := 0;
  FLinB := 1;
  FLin1 := 0;
  {while not QrCNAB_Dir.Eof do
  begin}
    LeArquivos(QrCNAB_DirNome.Value, QrCNAB_DirCodigo.Value, FLinA);
    FLinA := GradeA.RowCount - 1;
    {QrCNAB_Dir.Next;
  end;}
  HabilitaBotoes();
  Screen.Cursor := crDefault;
end;

procedure TFmCNAB_Ret2.QrLeiItensCalcFields(DataSet: TDataSet);
begin
  QrLeiItensMez_TXT.Value := dmkPF.MezToFDT(QrLeiItensMez.Value, 0, 104);
end;

procedure TFmCNAB_Ret2.QueryCalcFields(DataSet: TDataSet);
begin
  QuerySEQ.Value := Query.RecNo;
end;

procedure TFmCNAB_Ret2.QrLeiCalcFields(DataSet: TDataSet);
var
  OcorCodi: Integer;
begin
  OcorCodi := Geral.IMV(QrLeiOcorrCodi.Value);
  if UBancos.EhCodigoLiquidacao(OcorCodi, QrLeiBanco.Value, QrLeiTamReg.Value, '') then
    QrLeiDJM.Value := -((QrLeiDevJuros.Value + QrLeiDevMulta.Value) -
    (QrLeiValJuros.Value + QrLeiValMulta.Value + QrLeiValJuMul.Value))
  else QrLeiDJM.Value := 0;

  QrLeiDTA_TARIF_TXT.Value := Geral.FDT(QrLeiDtaTarif.Value, 3);

  {
  if Trunc(QrLeiTIPO_BOL.Value) = 610 then
    QrLeiNOME_TIPO_BOL.Value := 'Reparcelamento'
  else if QrLeiTIPO_BOL.Value = 610 then
  }

end;

{
function TFmCNAB_Ret2.LocDadoAll(const Campo, Linha: Integer; const Mensagem: String;
var Resultado: String): Boolean;
begin
  Result := True;
  if QrCampos.Locate('Campo', Campo, []) then
    Resultado :=
    Trim(Copy(FLista[Linha], QrCamposPadrIni.Value, QrCamposPadrTam.Value))
  else begin
    Result := False;
    Resultado := '';
    if Mensagem <> '' then
      Geral.MensagemBox((Mensagem), 'Erro', MB_OK+MB_ICONERROR);
  end;
  Memo2.Lines.Add(FormatFloat('00000', Campo) + ' ' +
    FormatFloat('00000', Linha) + ' ' +
    FormatFloat('000', QrCamposPadrIni.Value) + ' ' +
    FormatFloat('000', QrCamposPadrTam.Value) + ' ' + Resultado);
end;
}

{
function TFmCNAB_Ret2.LocDado240(const Campo, Linha: Integer; const Mensagem: String;
var Resultado: String): Boolean;
begin
  Result := True;
  if QrCampos.Locate('Campo', Campo, []) then
    Resultado :=
    Trim(Copy(FLista[Linha], QrCamposPadrIni.Value, QrCamposPadrTam.Value))
  else begin
    Result := False;
    Resultado := '';
    if Mensagem <> '' then
      Geral.MensagemBox((Mensagem), 'Erro', MB_OK+MB_ICONERROR);
  end;
  Memo2.Lines.Add(FormatFloat('00000', Campo) + ' ' +
    FormatFloat('00000', Linha) + ' ' +
    FormatFloat('000', QrCamposPadrIni.Value) + ' ' +
    FormatFloat('000', QrCamposPadrTam.Value) + ' ' + Resultado);
end;
}

function TFmCNAB_Ret2.CarregaItensRetornoA(Arquivo: String;
  SeqDir, SeqArq, Entidade, TamReg, Banco: Integer): Boolean;
begin
  Screen.Cursor := crHourGlass;
  Result := False;
  //
  FLista.Clear;
  FLista.LoadFromFile(Arquivo);
  BtCarrega.Enabled := FLista.Count > 0;
  MyObjects.LimpaGrade(Grade1, 1, 1, True);
  //
  FLin1 := 0;
  //
  Memo2.Lines.Clear;
  Memo2.Lines.Add('Campo Linha Pos Tam Valor');
  //
  if not UBancos.BancoImplementado(Banco, TamReg, ecnabRetorno) then
  begin
    Screen.Cursor := crDefault;
    Exit;
  end;
  {
  QrBanco.Close;
  QrBanco.Params[0].AsInteger := BcoUse;
  UMyMod.AbreQuery(QrBanco);
  if TamReg = 400 then
  begin
    if (QrBancoID_400i.Value = 0) or (QrBancoID_400t.Value = 0) then
    begin
      Geral.MensagemBox(('N�o foi definido o identificador de ' +
      'cobran�a CNAB400 para o banco '+ FormatFloat('000', BcoUse) +
      ' em seu cadastro!'), 'Aviso', MB_OK+MB_ICONWARNING);
      Screen.Cursor := crDefault;
      Exit;
    end;
  end else begin
    if (QrBancoID_240i.Value = 0) or (QrBancoID_240t.Value = 0) then
    begin
      Geral.MensagemBox(('N�o foi definido o identificador de ' +
      'cobran�a CNAB240 para o banco '+ FormatFloat('000', BcoUse) +
      ' em seu cadastro!'), 'Aviso', MB_OK+MB_ICONWARNING);
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  //
  }
  {
  QrCampos.Close;
  QrCampos.Params[0].AsInteger := BcoUse;
  QrCampos.SQL[10] := 'AND T'+FormatFloat('0', TamReg)+'=1';
  UMyMod.AbreQuery(QrCampos)
  if QrCampos.RecordCount = 0 then
  begin
    Geral.MensagemBox(('N�o h� nenhum tipo de campo de registro '+
    'detalhe definido para o arquivo "'+ Arquivo + '" do banco '+
    FormatFloat('000', BcoUse) + '!'), 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  }
  case TamReg of
    240: CarregaItensRetorno240(Banco, Entidade, SeqDir, SeqArq);
    //240: CarregaArquivo240('?', 0);
    400: CarregaItensRetorno400(Banco, Entidade, SeqDir, SeqArq);
    else Geral.MensagemBox(('Carregamento de itens de retorno n�o ' +
    'implementado para arquivo contendo ' + Geral.FF0(TamReg) + ' posi��es!'),
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Screen.Cursor := crDefault;
  {
  if nc > 0 then Geral.MensagemBox(('Existem '+Geral.FF0(nc)+
  ' registros detalhe que n�o s�o cobran�a sem registro e foram DESCONSIDERADOS '+
  'na leitura do arquivo "'+Arquivo+'"!'), 'Aviso', MB_OK+MB_ICONWARNING);
  }
end;

procedure TFmCNAB_Ret2.CarregaItensRetorno240(
  Banco, Entidade, SeqDir, SeqArq: Integer);
var
  i: Integer;
  Registro, Segmento,
  SequeReg, ID_Link,  NossoNum, OcorrCod, OcorrDta, SeuNumer, PagtoDta,
  TxtTitul, TxtAbati, TxtDesco, TxtJuros, TxtMulta,
  TxtOutrC, TxtEPago, TxtJuMul, TxtTarif, DtaTarif,
  OcorrTxt, NumDocum, TxtCBrut, TxtOutrD, VenctDta,
  Msg: String;
begin
  for i := 0 to FLista.Count - 1 do
  begin
    //ShowMessage(FLista[i]);
    // Tipo de registro
    //LocDadoAll(670, i, '', Registro);
    Registro := Copy(FLista[i], 008, 001);
    //LocDadoAll(671, i, '', Segmento);
    Segmento := Copy(FLista[i], 014, 001);
    if (Registro = '3') and (Segmento = 'T') then
    begin
      inc(FLin1, 1);
      Grade1.RowCount := FLin1 + 1;
      Grade1.Cells[00, FLin1] := Geral.FF0(FLin1);

      // ID Link
      Msg := 'N�o foi poss�vel obter o identificador do t�tulo na linha ' +
      Geral.FF0(i) + '!';
      if not UBancos.LocDado240(Banco, 1000, i, FLista, Msg, '', ID_Link) then Exit;
      Grade1.Cells[21, FLin1] := ID_Link;

      // Nosso n�mero -> ID do registro no Sicredi e no Syndic
      UBancos.LocDado240(Banco, 501, i, FLista, '', '', NossoNum);
      Grade1.Cells[01, FLin1] := NossoNum;

      // Ocorr�ncia
      UBancos.LocDado240(Banco, 504, i, FLista, '', '', OcorrCod);
      Grade1.Cells[02, FLin1] := OcorrCod;
      // Texto da ocorr�ncia
      OcorrTxt :=
        UBancos.CNABTipoDeMovimento(Banco, ecnabRetorno, Geral.IMV(OcorrCod), 240, False, '');
      Grade1.Cells[03, FLin1] := OcorrTxt;


      // Data da ocorr�ncia
      UBancos.LocDado240(Banco, 505, i+1, FLista, '', '', OcorrDta);
      try
        if (OcorrDta = '000000') or (OcorrDta = '00000000') then
        begin
          Geral.MensagemBox(('A data da ocorr�ncia "' +
          OcorrTxt + '" n�o foi definida (' + OcorrDta +') para o bloqueto ' +
          ID_Link + ' !'), 'Aviso',
          MB_OK+MB_ICONWARNING);
        end else
        Grade1.Cells[04, FLin1] := OcorrDta;
      except
        Geral.MensagemBox(('Houve um erro ao formatar a data da ' +
        'ocorr�ncia. Verifique se o FORMATO informado no cadastro do CAMPO � ' +
        'compat�vel com o que o banco solicita!'), 'Erro', MB_OK+MB_ICONERROR);
      end;

      // Seu Numero
      UBancos.LocDado240(Banco, 506, i, FLista, '', '', SeuNumer);
      Grade1.Cells[05, FLin1] := SeuNumer;

      // Documento (Texto do n�mero do documento no t�tulo)
      UBancos.LocDado240(Banco, 502, i, FLista, '', '', NumDocum);
      Grade1.Cells[05, FLin1] := NumDocum;

      // Valor do t�tulo
      UBancos.LocDado240(Banco, 550, i, FLista, '', '', TxtTitul);
      //TxtTitul := dmkPF.XFT(TxtTitul, QrCamposCasas.Value, siPositivo);
      //ValTitul := Geral.DMV(TxtTitul);
      Grade1.Cells[06, FLin1] := TxtTitul;

      // Abatimento concedido
      UBancos.LocDado240(Banco, 551, i+1, FLista, '', '', TxtAbati);
      //TxtAbati := dmkPF.XFT(TxtAbati, QrCamposCasas.Value, siPositivo);
      //ValAbati := Geral.DMV(TxtAbati);
      Grade1.Cells[07, FLin1] := TxtAbati;

      // Desconto concedido
      UBancos.LocDado240(Banco, 552, i+1, FLista, '', '', TxtDesco);
      //TxtDesco := dmkPF.XFT(TxtDesco, QrCamposCasas.Value, siPositivo);
      //ValDesco := Geral.DMV(TxtDesco);
      Grade1.Cells[08, FLin1] := TxtDesco;

      // Valor efetivamente pago
      //UBancos.LocDado240(Banco, 553, i, FLista, '', TxtEPago);
      // Valor Total pago
      UBancos.LocDado240(Banco, 578, i+1, FLista, '', '', TxtEPago);
      //TxtEPago := dmkPF.XFT(TxtEPago, QrCamposCasas.Value, siPositivo);
      //ValEPago := Geral.DMV(TxtEPago);
      Grade1.Cells[09, FLin1] := TxtEPago;

      // Valor de cr�dito bruto
      UBancos.LocDado240(Banco, 579, i+1, FLista, '', '', TxtCBrut);
      //TxtCBrut := dmkPF.XFT(TxtCBrut, QrCamposCasas.Value, siPositivo);
      //ValEPago := Geral.DMV(TxtCBrut);
      Grade1.Cells[27, FLin1] := TxtCBrut;

      // Valor de outros d�bitos
      UBancos.LocDado240(Banco, 585, i+1, FLista, '', '', TxtOutrD);
      //TxtOutrD := dmkPF.XFT(TxtOutrD, QrCamposCasas.Value, siPositivo);
      //ValEPago := Geral.DMV(TxtOutrD);
      Grade1.Cells[28, FLin1] := TxtOutrD;

      //  N�O TEM
      // Juros de mora
      UBancos.LocDado240(Banco, 555, i, FLista, '', '', TxtJuros);
      //TxtJuros := dmkPF.XFT(TxtJuros, QrCamposCasas.Value, siPositivo);
      //ValJuros := Geral.DMV(TxtJuros);
      Grade1.Cells[10, FLin1] := TxtJuros;

      //  N�O TEM
      // Multa
      UBancos.LocDado240(Banco, 556, i, FLista, '', '', TxtMulta);
      //TxtMulta := dmkPF.XFT(TxtMulta, QrCamposCasas.Value, siPositivo);
      //ValMulta := Geral.DMV(TxtMulta);
      Grade1.Cells[11, FLin1] := TxtMulta;

      //  N�O TEM
      // Outros cr�ditos
      UBancos.LocDado240(Banco, 554, i, FLista, '', '', TxtOutrC);
      //TxtOutrC := dmkPF.XFT(TxtOutrC, QrCamposCasas.Value, siPositivo);
      //ValMulta := Geral.DMV(TxtOutrC);
      Grade1.Cells[12, FLin1] := TxtOutrC;

      // Juros de Mora e Multa (Somados)
      UBancos.LocDado240(Banco, 557, i+1, FLista, '', '', TxtJuMul);
      //TxtJuMul := dmkPF.XFT(TxtJuMul, QrCamposCasas.Value, siPositivo);
      Grade1.Cells[13, FLin1] := TxtJuMul;

      // Tarifa (Despesa) de cobran�a
      UBancos.LocDado240(Banco, 570, i, FLista, '', '', TxtTarif);
      //TxtTarif := dmkPF.XFT(TxtTarif, QrCamposCasas.Value, siPositivo);
      Grade1.Cells[14, FLin1] := TxtTarif;

      // ERRO Calculado no final
      //Grade1.Cells[15, FLin1] := ERRO;

      //Parei Aqui
      // Falta fazer (sem pressa)
      {
      // Motivo da ocorrencia
      Grade1.Cells[16, FLin1] := Copy(FLista[i], 319, 010);
      // Texto do motivo da ocorrencia
      Grade1.Cells[17, FLin1] := UBancos.CNABMotivosDeTipoDeMovimento28(
        Banco, Copy(FLista[i], 319, 010));
      }

      // Data de lancamento na conta corrente
      Msg := 'Data de lan�amento na conta corrente n�o definida!' +
      sLineBreak + 'N�o � aconselh�vel conciliar o documento ' +
      ID_Link + ' (Nosso n�mero = ' + NossoNum + ')';
      UBancos.LocDado240(Banco, 581, i+1, FLista, Msg, '', PagtoDta);
      try
        // Por causa do banco 756 que diz que coloca no cabe�alho (col 380 a 385)
        // a data de cr�dito na c/c , mas esta data � colocada nos itens (col 296 a 301)
        if (PagtoDta = '') and UBancos.EhCodigoLiquidacao(Geral.IMV(OcorrCod), Banco, 240, '')
        then
        begin
          MeAvisos.Lines.Add('Data de lan�amento na conta corrente n�o definida!' +
          ' N�o � aconselh�vel conciliar o documento ' +
          ID_Link + ' (Nosso n�mero = ' + NossoNum + ').');
          PageControl1.ActivePageIndex := 1;
        end;
        Grade1.Cells[18, FLin1] := PagtoDta;
      except
        Geral.MensagemBox(('Houve um erro ao formatar a data do ' +
        'cr�dito em conta corrente. Verifique se o FORMATO informado no ' +
        'cadastro do CAMPO � compat�vel com o que o banco solicita!'), 'Erro',
        MB_OK+MB_ICONERROR);
      end;

      // Data de d�bito da tarifa de cobranca
      //UBancos.LocDado240(Banco, 582, i+1, FLista, '', DtaTarif);
      UBancos.LocDado240(Banco, 505, i+1, FLista, '', '', DtaTarif);
      try
        Grade1.Cells[19, FLin1] := DtaTarif;
      except
        Geral.MensagemBox(('Houve um erro ao formatar a data do ' +
        'd�bito da tarifa em conta corrente. Verifique se o FORMATO informado no ' +
        'cadastro do CAMPO � compat�vel com o que o banco solicita!'), 'Erro',
        MB_OK+MB_ICONERROR);
      end;

      // C�digo Condom�nio
      Grade1.Cells[20, FLin1] := Geral.FF0(Entidade);

      // Sequencia do diretorio
      Grade1.Cells[22, FLin1] := FormatFloat('000', SeqDir);

      // Sequencia do arquivo
      Grade1.Cells[23, FLin1] := FormatFloat('000', SeqArq);

      // N�mero sequencial do registro
      UBancos.LocDado240(Banco, 999, i, FLista, '', '', SequeReg);
      Grade1.Cells[24, FLin1] := FormatFloat('000', Geral.IMV(SequeReg));

      // Banco
      Grade1.Cells[25, FLin1] := FormatFloat('000', Banco);

      // Vencimento
      UBancos.LocDado240(Banco, 580, i+1, FLista, '', '', VenctDta);
      try
        Grade1.Cells[29, FLin1] := VenctDta;
      except
        Geral.MensagemBox(('Houve um erro ao formatar a data de ' +
        'vencimento. Verifique se o FORMATO informado no cadastro do CAMPO � ' +
        'compat�vel com o que o banco solicita!'), 'Erro', MB_OK+MB_ICONERROR);
      end;



      ErroLinha(Banco, FLin1, False);

      //Parei Aqui
      // Fazer rejei��o de t�tulos
    end;
  end;
end;

procedure TFmCNAB_Ret2.CarregaItensRetorno400(
  Banco, Entidade, SeqDir, SeqArq: Integer);
var
  i: Integer;
  SequeReg,
  ID_Link,  NossoNum, OcorrCod, {OcorrTxt,} OcorrDta, SeuNumer, {Mensagem,} PagtoDta,
  TxtTitul, TxtAbati, TxtDesco, TxtJuros, TxtMulta,
  TxtOutro, TxtEPago, TxtJuMul, TxtTarif, DtaTarif,
  VenctDta, Lin, Msg, Motivos: String;
begin
  if FLista.Count - 2 = 0 then
  begin
    Geral.MensagemBox('N�o h� dados de transa��es no arquivo selecionado!',
      'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  for i := 1 to FLista.Count - 2 do
  begin
    inc(FLin1, 1);
    Grade1.RowCount := FLin1 + 1;
    Grade1.Cells[00, FLin1] := Geral.FF0(FLin1);
    Lin := FLista[i];

    // ID Link
    Msg := 'N�o foi poss�vel obter o identificador do t�tulo na linha ' +
      Geral.FF0(i) + '!';
    if not UBancos.LocDado400(Banco, 1000, i, FLista, Msg, '', ID_Link) then Exit;
    Grade1.Cells[21, FLin1] := ID_Link;

    // Nosso n�mero -> ID do registro no Sicredi e no Syndic
    UBancos.LocDado400(Banco, 501, i, FLista, '', '', NossoNum);
    Grade1.Cells[01, FLin1] := NossoNum;

    // Ocorr�ncia
    UBancos.LocDado400(Banco, 504, i, FLista, '', '', OcorrCod);
    Grade1.Cells[02, FLin1] := OcorrCod;

    // Texto da ocorr�ncia
    Grade1.Cells[03, FLin1] :=
      UBancos.CNABTipoDeMovimento(Banco, ecnabRetorno, Geral.IMV(OcorrCod), 400, False, '');

    // Data da ocorr�ncia
    UBancos.LocDado400(Banco, 505, i, FLista, '', '', OcorrDta);
    try
      Grade1.Cells[04, FLin1] := OcorrDta;
    except
      Geral.MensagemBox(('Houve um erro ao formatar a data da ' +
      'ocorr�ncia. Verifique se o FORMATO informado no cadastro do CAMPO � ' +
      'compat�vel com o que o banco solicita!'), 'Erro', MB_OK+MB_ICONERROR);
    end;

    // Seu Numero
    UBancos.LocDado400(Banco, 506, i, FLista, '', '', SeuNumer);
    Grade1.Cells[05, FLin1] := SeuNumer;

    // Valor do t�tulo
    UBancos.LocDado400(Banco, 550, i, FLista, '', '', TxtTitul);
    //TxtTitul := dmkPF.XFT(TxtTitul, QrCamposCasas.Value, siPositivo);
    //ValTitul := Geral.DMV(TxtTitul);
    Grade1.Cells[06, FLin1] := TxtTitul;

    // Abatimento concedido
    UBancos.LocDado400(Banco, 551, i, FLista, '', '', TxtAbati);
    //TxtAbati := dmkPF.XFT(TxtAbati, QrCamposCasas.Value, siPositivo);
    //ValAbati := Geral.DMV(TxtAbati);
    Grade1.Cells[07, FLin1] := TxtAbati;

    // Desconto concedido
    UBancos.LocDado400(Banco, 552, i, FLista, '', '', TxtDesco);
    //TxtDesco := dmkPF.XFT(TxtDesco, QrCamposCasas.Value, siPositivo);
    //ValDesco := Geral.DMV(TxtDesco);
    Grade1.Cells[08, FLin1] := TxtDesco;

    // Valor efetivamente pago
    UBancos.LocDado400(Banco, 553, i, FLista, '', '', TxtEPago);
    //TxtEPago := dmkPF.XFT(TxtEPago, QrCamposCasas.Value, siPositivo);
    //ValEPago := Geral.DMV(TxtEPago);
    Grade1.Cells[09, FLin1] := TxtEPago;

    // Juros de mora
    UBancos.LocDado400(Banco, 555, i, FLista, '', '', TxtJuros);
    //TxtJuros := dmkPF.XFT(TxtJuros, QrCamposCasas.Value, siPositivo);
    //ValJuros := Geral.DMV(TxtJuros);
    Grade1.Cells[10, FLin1] := TxtJuros;

    // Multa
    UBancos.LocDado400(Banco, 556, i, FLista, '', '', TxtMulta);
    //TxtMulta := dmkPF.XFT(TxtMulta, QrCamposCasas.Value, siPositivo);
    //ValMulta := Geral.DMV(TxtMulta);
    Grade1.Cells[11, FLin1] := TxtMulta;

    // Outros cr�ditos
    UBancos.LocDado400(Banco, 554, i, FLista, '', '', TxtOutro);
    //TxtOutro := dmkPF.XFT(TxtOutro, QrCamposCasas.Value, siPositivo);
    //ValMulta := Geral.DMV(TxtOutro);
    Grade1.Cells[12, FLin1] := TxtOutro;

    // Juros de Mora e Multa (Somados)
    UBancos.LocDado400(Banco, 557, i, FLista, '', '', TxtJuMul);
    //TxtJuMul := dmkPF.XFT(TxtJuMul, QrCamposCasas.Value, siPositivo);
    Grade1.Cells[13, FLin1] := TxtJuMul;

    // Tarifa (Despesa) de cobran�a
    UBancos.LocDado400(Banco, 570, i, FLista, '', '', TxtTarif);
    //TxtTarif := dmkPF.XFT(TxtTarif, QrCamposCasas.Value, siPositivo);
    Grade1.Cells[14, FLin1] := TxtTarif;

    // ERRO Calculado no final
    //Grade1.Cells[15, FLin1] := ERRO;

    //Parei Aqui
    // Falta fazer (sem pressa)
    {
    // Motivo da ocorrencia
    Grade1.Cells[16, FLin1] := Copy(i, FLista, 319, 010);
    // Texto do motivo da ocorrencia
    Grade1.Cells[17, FLin1] := UBancos.CNABMotivosDeTipoDeMovimento28(
      Banco, Copy(i, FLista, 319, 010));
    }

    // Motivos
    UBancos.LocDado400(Banco, 530, i, FLista, '', '', Motivos);
    Grade1.Cells[16, FLin1] := Motivos;


    // Data de lancamento na conta corrente
    UBancos.LocDado400(Banco, 581, i, FLista, 'Data de lan�amento na conta corrente n�o definida!' +
    sLineBreak + 'N�o � aconselh�vel conciliar o documento ' +
    ID_Link + ' (Nosso n�mero = ' + NossoNum + ')', '', PagtoDta);
    try
      // Por causa do banco 756 que diz que coloca no cabe�alho (col 380 a 385)
      // a data de cr�dito na c/c , mas esta data � colocada nos itens (col 296 a 301)
      if (PagtoDta = '') and UBancos.EhCodigoLiquidacao(Geral.IMV(OcorrCod), Banco, 400, '')
      then
      begin
        MeAvisos.Lines.Add('Data de lan�amento na conta corrente n�o definida!' +
        ' N�o � aconselh�vel conciliar o documento ' +
        ID_Link + ' (Nosso n�mero = ' + NossoNum + ').');
        PageControl1.ActivePageIndex := 1;
      end;
      Grade1.Cells[18, FLin1] := PagtoDta;
    except
      Geral.MensagemBox(('Houve um erro ao formatar a data do ' +
      'cr�dito em conta corrente. Verifique se o FORMATO informado no ' +
      'cadastro do CAMPO � compat�vel com o que o banco solicita!'), 'Erro',
      MB_OK+MB_ICONERROR);
    end;

    // Data de d�bito da tarifa de cobranca
    UBancos.LocDado400(Banco, 582, i, FLista, '', '', DtaTarif);
    try
      Grade1.Cells[19, FLin1] := DtaTarif;
    except
      Geral.MensagemBox(('Houve um erro ao formatar a data do ' +
      'd�bito da tarifa em conta corrente. Verifique se o FORMATO informado no ' +
      'cadastro do CAMPO � compat�vel com o que o banco solicita!'), 'Erro',
      MB_OK+MB_ICONERROR);
    end;

    // C�digo Condom�nio
    Grade1.Cells[20, FLin1] := Geral.FF0(Entidade);

    // Sequencia do diretorio
    Grade1.Cells[22, FLin1] := FormatFloat('000', SeqDir);

    // Sequencia do arquivo
    Grade1.Cells[23, FLin1] := FormatFloat('000', SeqArq);

    // N�mero sequencial do registro
    UBancos.LocDado400(Banco, 999, i, FLista, '', '', SequeReg);
    Grade1.Cells[24, FLin1] := FormatFloat('000', Geral.IMV(SequeReg));

    // Banco
    Grade1.Cells[25, FLin1] := FormatFloat('000', Banco);


    // Data de vencimento
    UBancos.LocDado400(Banco, 580, i, FLista, '', '', VenctDta);
    try
      Grade1.Cells[29, FLin1] := VenctDta;
    except
      Geral.MensagemBox(('Houve um erro ao formatar a data de ' +
      'vencimento. Verifique se o FORMATO informado no ' +
      'cadastro do CAMPO � compat�vel com o que o banco solicita!'), 'Erro',
      MB_OK+MB_ICONERROR);
    end;

    ErroLinha(Banco, FLin1, False);

    //Parei Aqui
    // Fazer rejei��o de t�tulos
  end;
end;

procedure TFmCNAB_Ret2.DBGLeiDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  Cor: TColor;
begin
  if (Column.FieldName = 'DJM') then
  begin
    if      QrLeiDJM.Value > 0 then Cor := clBlue
    else if QrLeiDJM.Value < 0 then Cor := clRed
    else Cor := clBlack;
    MyObjects.DesenhaTextoEmDBGrid(TDbGrid(DBGLei), Rect, Cor, clWhite,
      Column.Alignment, Column.Field.DisplayText);
  end;
end;

procedure TFmCNAB_Ret2.DBGrid1DblClick(Sender: TObject);
begin
  AbreDiretorioCNAB();
end;

procedure TFmCNAB_Ret2.QrLeiAfterClose(DataSet: TDataSet);
begin
  BtAgenda.Enabled := False;
end;

procedure TFmCNAB_Ret2.BtAgendaClick(Sender: TObject);
begin
  if not FmPrincipal.AcaoEspecificaDeApp('ArreFut') then
  Geral.MensagemBox(PChar('O agendamento est� indispon�vel ' +
  'para este aplicativo! Para ativ�-lo, contate a DERMATEK!'),
  'Mensagem', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmCNAB_Ret2.CorrigeValTitulopoisobancoenviouerrado1Click(
  Sender: TObject);
var
  ValTxt, AntTxt: String;
  ValNum: Double;
  Continua: Integer;
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  ValTxt := DBEdit1.Text;
  if InputQuery('Valor do T�tulo Tnformado pelo Banco',
  'Informe o novo valor informado pelo banco:', ValTxt) then
  begin
    AntTxt := Geral.FFT(QrLeiValTitul.Value, 2, siPositivo);
    ValNum := Geral.DMV(ValTxt);
    ValTxt := Geral.FFT(ValNum, 2, siPositivo);
    //
    Continua := Geral.MensagemBox(('Confirma a altera��o do ' +
      'Val.Titulo de "' + AntTxt + '" para "' + ValTxt + '"?'), 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION);
    if Continua = ID_YES then Continua := Geral.MensagemBox((
    'Tem certeza que deseja alterar o Val.Titulo de "'+ AntTxt + '" para "' +
    ValTxt + '"?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION);
    if Continua = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE cnab_lei SET ValTitul=:P0 WHERE Codigo=:Pa');
      Dmod.QrUpd.Params[00].AsFloat   := ValNum;
      //
      Dmod.QrUpd.Params[01].AsInteger := QrLeiCodigo.Value;
      Dmod.QrUpd.ExecSQL;
      //
      ReopenCNAB0Lei(QrLeiCodigo.Value);
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmCNAB_Ret2.EdNomeBancoChange(Sender: TObject);
begin
  ReopenCNAB_Dir();
end;

function TFmCNAB_Ret2.ErroLinha(Banco, Linha: Integer; Avisa: Boolean): Boolean;
var
  ValTitul, ValAbati, ValDesco, ValPago, ValJuros, ValMulta,
  ValJuMul, ValTarif, ValErro, ValOutro, ValReceb: Double;
  Cliente: Integer;
  IDLink: Int64;
  NossoNum: String;
  //
  DefErro: Boolean;
begin
  if Geral.IMV(Grade1.Cells[02, Linha]) = 6 then // Baixa simples
  begin
    Cliente   := Geral.IMV(GradeA.Cells[09, GradeA.Row]);
    NossoNum  := Geral.SoNumero_TT(Grade1.Cells[01, Linha]);
    IDLink    := MLAGeral.I64(Grade1.Cells[21, Linha]);
    ValTitul  := Geral.DMV(Grade1.Cells[06, Linha]);
    ValAbati  := Geral.DMV(Grade1.Cells[07, Linha]);
    ValDesco  := Geral.DMV(Grade1.Cells[08, Linha]);
    ValPago   := Geral.DMV(Grade1.Cells[09, Linha]);
    ValJuros  := Geral.DMV(Grade1.Cells[10, Linha]);
    ValMulta  := Geral.DMV(Grade1.Cells[11, Linha]);
    ValOutro  := Geral.DMV(Grade1.Cells[12, Linha]);
    ValJuMul  := Geral.DMV(Grade1.Cells[13, Linha]);
    ValTarif  := Geral.DMV(Grade1.Cells[14, Linha]);
    ValReceb  := Geral.DMV(Grade1.Cells[27, Linha]);
    //
    if ValReceb > 0 then
      ValErro := ValPago - ValTarif - ValReceb
    else
      ValErro := ValTitul - ValAbati - ValDesco + ValJuros + ValMulta
               + ValOutro + ValJuMul - ValTarif - ValPago;
    if Banco = 409 then ValErro := ValErro + ValTarif;
    //
    Grade1.Cells[15, Linha] := Geral.FFT(ValErro, 2, siNegativo);
    DefErro := (ValErro >= 0.01) or (ValErro <= -0.01);
    //
    if Avisa then
    begin
      if DefErro then
      begin
        ReopenQrPesq2(IDLink, Cliente);
        //
        if QrPesq2.RecordCount > 0 then
        begin
          Geral.MensagemBox(('O documento com "Nosso n�m." = ' +
          NossoNum + ' foi localizado, mas tem diverg�ncias nos ' +
          'dados informados!'), 'Aviso', MB_OK+MB_ICONWARNING);
        end else begin
          Geral.MensagemBox(('O documento com "Nosso n�m." = ' +
          NossoNum + ' tem diverg�ncias nos dados informados, e n�o ' +
          'foi localizado nos bloquetos emitidos!'), 'Aviso', MB_OK+MB_ICONWARNING);
        end;
      end;
    end;
    Result := DefErro;
  end else Result := False;
end;

procedure TFmCNAB_Ret2.Alteravalordoitemdearrecadaoselecionado1Click(
  Sender: TObject);
var
  Valor: String;
  Novo: Double;
begin
  //
  Valor := FormatFloat('#,###,##0.00', QrLeiItensCredito.Value);
  if InputQuery('Altera��o de valor', 'Informe o novo valor:', Valor) then
  begin
    Novo := Geral.DMV(Valor);
    if Novo >= 0.01 then
    begin
      {Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Credito=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:P1 AND Sub=:P2');
      //
      Dmod.QrUpd.Params[00].AsFloat   := Novo;
      Dmod.QrUpd.Params[01].AsInteger := QrLeiItensControle.Value;
      Dmod.QrUpd.Params[02].AsInteger := QrLeiItensSub.Value;
      Dmod.QrUpd.ExecSQL;
      }
      UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, [
      'Credito'], ['Controle', 'Sub'], [Novo], [QrLeiItensControle.Value,
      QrLeiItensSub.Value], True, '');
      //
      ReopenCNAB0Lei(QrLeiCodigo.Value);
      ReopenLeiItens(QrLeiItensControle.Value);
    end else Geral.MensagemBox(('O valor informado "' + Valor +
    '" � inv�lido!'), 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmCNAB_Ret2.BtItensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmCNAB_Ret2.BtPastaClick(Sender: TObject);
begin
  AbreDiretorioCNAB();
end;

procedure TFmCNAB_Ret2.Excluioitemdearrecadaoselecionado1Click(
  Sender: TObject);
begin
  if Geral.MensagemBox(('Tem certeza que deseja excluir o item "' +
  QrLeiItensDescricao.Value + '"?'),
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
{
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add(EXCLUI_DE + VAR_LCT + ' WHERE Controle=:P0 AND Sub=:P1');
    Dmod.QrUpd.Params[00].AsInteger := QrLeiItensControle.Value;
    Dmod.QrUpd.Params[01].AsInteger := QrLeiItensSub.Value;
    Dmod.QrUpd.ExecSQL;
}
    UFinanceiro.ExcluiLct_Unico(True, Dmod.MyDB, QrLeiItensData.Value,
      QrLeiItensTipo.Value, QrLeiItensCarteira.Value, QrLeiItensControle.Value,
      QrLeiItensSub.Value, False);
    //
    ReopenCNAB0Lei(QrLeiCodigo.Value);
  end;
end;

procedure TFmCNAB_Ret2.Adicionadados1Click(Sender: TObject);
var
  OcorrDataD, QuitaDataD, TarifDataD: TDateTime;
  OcorrDataT, QuitaDataT, TarifDataT: String;
  SQL, OcorrCod, OcorrTxt, Txt: String;
  i, j, Ini, Fim: Integer;
  Bloqueto, ValTitul, ValPago, ValJuros, ValMulta, ValOutros, ValJurMul,
  ValTarifa, ValErro: Double;
begin
  Query.Close;
  Query.SQL.Clear;
  SQL := 'INSERT INTO bafer (Bloqueto,ValTitul,ValPago,ValMulta,ValJuros,' +
  'ValOutros,ValJurMul,ValERRO,ValTarifa,OcorrCod,OcorrTxt,' +
  'DataOcorr,DataQuita,DataTarif,Ativo) VALUES (';

    {
    SelectedRows.Count > 1 then
    begin
      with GradeA.DataSource.DataSet do
      for i := 0 to GradeA.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(GradeA.SelectedRows.Items[i]));
        GeraAtual(se, sq);
      end;
    end else GeraAtual(se, sq);
  end else if FQuais = istAtual then GeraAtual(se, sq);
    }
  //
  Ini := GradeA.Selection.Top;
  Fim := GradeA.Selection.Bottom;
  for j := Ini to Fim do
  begin
    GradeA.Row := j;
    for i := 1 to Grade1.RowCount - 1 do
    begin
      Txt := '';
      Bloqueto := Geral.DMV(Grade1.Cells[21,i]);
      Txt := Txt + dmkPF.FFP(Bloqueto, 0) + ',';
      //
      ValTitul := Geral.DMV(Grade1.Cells[06,i]);
      Txt := Txt + dmkPF.FFP(ValTitul, 2) + ',';
      //
      ValPago := Geral.DMV(Grade1.Cells[09,i]);
      Txt := Txt + dmkPF.FFP(ValPago, 2) + ',';
      //
      ValMulta := Geral.DMV(Grade1.Cells[11,i]);
      Txt := Txt + dmkPF.FFP(ValMulta, 2) + ',';
      //
      ValJuros := Geral.DMV(Grade1.Cells[10,i]);
      Txt := Txt + dmkPF.FFP(ValJuros, 2) + ',';
      //
      ValOutros := Geral.DMV(Grade1.Cells[12,i]);
      Txt := Txt + dmkPF.FFP(ValOutros, 2) + ',';
      //
      ValJurMul := Geral.DMV(Grade1.Cells[13,i]);
      Txt := Txt + dmkPF.FFP(ValJurMul, 2) + ',';
      //
      ValERRO := Geral.DMV(Grade1.Cells[15,i]);
      Txt := Txt + dmkPF.FFP(ValERRO, 2) + ',';
      //
      ValTarifa := Geral.DMV(Grade1.Cells[14,i]);
      Txt := Txt + dmkPF.FFP(ValTarifa, 2) + ',';
      //
      OcorrCod := Grade1.Cells[02,i];
      if Trim(OcorrCod) = '' then
        OcorrCod := '0';
      //
      OcorrTxt := Grade1.Cells[03,i];
      if Trim(OcorrTxt) = '' then
        OcorrTxt := ' ';
      Txt := Txt + '"' + OcorrCod + '",';
      Txt := Txt + '"' + OcorrTxt + '",';
      //
      OcorrDataD := Geral.ValidaDataSimples(Grade1.Cells[04,i], True);
      OcorrDataT := Geral.FDT(OcorrDataD, 1);
      Txt := Txt + '"' + OcorrDataT + '",';
      //
      QuitaDataD := Geral.ValidaDataSimples(Grade1.Cells[04,i], True);
      QuitaDataT := Geral.FDT(QuitaDataD, 1);
      Txt := Txt + '"' + QuitaDataT + '",';
      //
      TarifDataD := Geral.ValidaDataSimples(Grade1.Cells[04,i], True);
      TarifDataT := Geral.FDT(TarifDataD, 1);
      Txt := Txt + '"' + TarifDataT + '",';
      //
      Query.SQL.Add(SQL + Txt + '0);');
      //
    end;
  end;
  Query.SQL.Add('SELECT * FROM bafer;');
  DmkABS_PF.AbreQuery(Query);
end;

procedure TFmCNAB_Ret2.Ajustavaloresdobloquetoatual1Click(Sender: TObject);
var
  Juros, Multa, Difer, Desco: Double;
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  Screen.Cursor := crHourGlass;
  Desco := 0;
  Multa := 0;
  Juros := 0;
  Difer := QrLeiValTitul.Value - QrLeiSumCredito.Value;
  if Difer > 0 then
  begin
    if QrLeiDevMulta.Value > Difer then Multa := Difer
    else begin
      Multa := QrLeiDevMulta.Value;
      Juros := Difer - QrLeiDevMulta.Value
    end;
  end else Desco := - Difer;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE cnab_lei SET ValJuros=:P0, ValMulta=:P1, ');
  Dmod.QrUpd.SQL.Add('ValDesco=:P2, ValTitul=:P3 WHERE Codigo=:Pa');
  Dmod.QrUpd.Params[00].AsFloat   := Juros;
  Dmod.QrUpd.Params[01].AsFloat   := Multa;
  Dmod.QrUpd.Params[02].AsFloat   := Desco;
  Dmod.QrUpd.Params[03].AsFloat   := QrLeiSumCredito.Value;
  //
  Dmod.QrUpd.Params[04].AsInteger := QrLeiCodigo.Value;
  Dmod.QrUpd.ExecSQL;
  //
  ReopenCNAB0Lei(QrLeiCodigo.Value);
  Screen.Cursor := crDefault;
end;

procedure TFmCNAB_Ret2.QrLeiAgrCalcFields(DataSet: TDataSet);
begin
  QrLeiAgrMez_TXT.Value := dmkPF.MezToFDT(QrLeiAgrMez.Value, 0, 104);
  case QrLeiAgrTIPO_BLOQ.Value of
    1: QrLeiAgrNOME_TIPO_BLOQ.Value := 'Taxa condominial';
    2: QrLeiAgrNOME_TIPO_BLOQ.Value := 'Reparcelamento';
    else QrLeiAgrNOME_TIPO_BLOQ.Value := 'Desconhecido';
  end;
end;

procedure TFmCNAB_Ret2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if QrCNAB_Dir.State = dsInactive then Timer1.Enabled := True;
end;

procedure TFmCNAB_Ret2.Timer1Timer(Sender: TObject);
var
  Codigo: Integer;
begin
  Timer1.Enabled := False;
  if QrCNAB_Dir.State <> dsInactive then
    Codigo := QrCNAB_DirCodigo.Value
  else Codigo := 0;
  ReopenCNAB_Dir();
  if QrCNAB_Dir.RecordCount = 0 then Geral.MensagemBox((
    'Nenhum diret�rio foi definido em Cadastros -> CNAB -> Diret�rios '+
    'CNAB. Defina ao menos um diret�rio!'), 'Aviso', MB_OK+MB_ICONWARNING)
  else QrCNAB_Dir.Locate('Codigo', Codigo, []);
  HabilitaBotoes;
end;

procedure TFmCNAB_Ret2.ImprimeRecibodeQuitacao;
var
  Pagamento, Texto(*, Liga*): String;
begin
  Pagamento := 'Quita��o de d�bitos gerados at� esta data relativos a';
  Texto := Pagamento + ' duplicata n� ' + QrLei3Duplicata.Value+
    ' emitida por '+ QrLei3Emitente.Value + ' para o dia '+ FormatDateTime(
    'dd" de "mmmm" de "yyyy', QrLei3Vencto.Value);
   //Ocorr�ncia foram rec�m geradoas e n�o pagas
   // Colocar que falta d�bito ?  Parei Aqui
  (*if QrOcorreu.RecordCount > 0 then
  begin
    if QrOcorreu.RecordCount = 1 then Texto := Texto + ' e a ocorr�ncia'
    else Texto := Texto + ' e as ocorr�ncias';
    QrOcorreu.First;
    Liga := ' ';
    while not QrOcorreu.Eof do
    begin
      Texto := Texto + Liga + '"'+QrOcorreuNOMEOCORRENCIA.Value + '"';
      if QrOcorreu.RecNo+1= QrOcorreu.RecordCount then Liga := ' e '
      else liga := ', ';
      QrOcorreu.Next;
    end;
  end;*)
  //
  GOTOy.EmiteRecibo(QrLei3Cliente.Value, Dmod.QrMasterDono.Value,
    QrLei3ValPago.Value, 0, 0, 'DUX-'+FormatFloat('000000',
    QrLei3ID_Link.Value), Texto, '', '', Date, 0);
end;

procedure TFmCNAB_Ret2.InfoTempo(Tempo: TDateTime; Texto: String; Inicial: Boolean);
begin
  if Inicial then
  begin
    FTempo := Tempo;
    FUltim := Tempo;
    Memo3.Lines.Add('');
    Memo3.Lines.Add('==============================================================================');
    Memo3.Lines.Add('');
    Memo3.Lines.Add(FormatDateTime('dd/mm/yyyy  hh:nn:ss ', Tempo) +
      '- [ Total ] [Unit�ri] '+ Texto);
  end else begin
    Memo3.Lines.Add(FormatDateTime('dd/mm/yyyy  hh:nn:ss - ', Tempo)+
    FormatDateTime('nn:ss:zzz ', Tempo-FTempo) +
    FormatDateTime('nn:ss:zzz ', Tempo-FUltim) + Texto);
    FUltim := Tempo;
  end;
end;

function TFmCNAB_Ret2.ObtemActiveRowA: Integer;
begin
  // caso n�o haja linha selecionada
  if FActiveRowA = 0 then FActiveRowA := 1;
  Result := FActiveRowA;
end;

procedure TFmCNAB_Ret2.Ordenarpeladatadecrdito1Click(Sender: TObject);
begin
  ReabreQueryEImprime(2);
end;

procedure TFmCNAB_Ret2.Ordenarpeladatadepagamento1Click(Sender: TObject);
begin
  ReabreQueryEImprime(3);
end;

procedure TFmCNAB_Ret2.Ordenarpelonmerodobloqueto1Click(Sender: TObject);
begin
  ReabreQueryEImprime(1);
end;

procedure TFmCNAB_Ret2.PMBufferPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := Query.State <> dsInactive;
  Adicionadados1.Enabled := Habilita;
  Imprimelista1.Enabled := Habilita;
end;

procedure TFmCNAB_Ret2.PnMovimentoClick(Sender: TObject);
begin

end;

{
Parei aqui
tentar eliminar BancosLei:
dfm: 1908 e 1964
QrBcocor e QrLocOcor

}

procedure TFmCNAB_Ret2.VerificaImplementacoes(Row: Integer);
{
var
  CodLiq, Banco, TamReg, Genero: Integer;
  VarBool: Boolean;
  Fator, Diferenca: Double;
}
begin
  {
  Banco  := Geral.IMV(GradeA.Cells[10, Row]);
  TamReg := Geral.IMV(GradeA.Cells[11, Row]);
  CodLiq := Geral.IMV(Grade1.Cells[02, 1]);
  //
  UBancos.SeparaJurosEMultaImplementado(Banco, VarBool);
  UBancos.EhCodigoLiquidacao(CodLiq, Banco, TamReg);
  UBancos.InformaTarifaDeCobrancaImplementado(Banco, VarBool);
  UBancos.ContaDaOcorrencia(Banco, TamReg, FormatFloat('00', CodLiq), Genero);
  UBancos.FatorDeRecebimento(Banco, 1, 0.1, 1, Fator);
  UBancos.DiferencaDeRecebimento(Banco, 1, 0.1, 1, Diferenca);
  UBancos.FatorMultaDeRecebimento(Banco, 1, 0.1, 1, Fator);
  }
end;

end.


