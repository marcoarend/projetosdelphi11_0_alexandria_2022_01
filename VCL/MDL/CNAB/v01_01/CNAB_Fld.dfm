object FmCNAB_Fld: TFmCNAB_Fld
  Left = 339
  Top = 185
  Caption = 'BCO-CNAB_-005 :: Campos de Arquivos CNAB'
  ClientHeight = 564
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 402
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 100
    ExplicitHeight = 396
    object Panel24: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 402
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 782
      ExplicitHeight = 394
      object Panel25: TPanel
        Left = 0
        Top = 17
        Width = 784
        Height = 63
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 1
        ExplicitTop = 18
        ExplicitWidth = 780
        object Label27: TLabel
          Left = 4
          Top = 4
          Width = 88
          Height = 13
          Caption = 'Descri'#231#227'o parcial: '
        end
        object Label25: TLabel
          Left = 4
          Top = 44
          Width = 185
          Height = 13
          Caption = '[Esc] escolhe item selecionada na lista.'
        end
        object EdFuncionalidade: TEdit
          Left = 4
          Top = 20
          Width = 169
          Height = 21
          TabOrder = 0
          OnChange = EdFuncionalidadeChange
        end
        object BitBtn5: TBitBtn
          Tag = 5
          Left = 176
          Top = 4
          Width = 40
          Height = 40
          NumGlyphs = 2
          TabOrder = 1
          OnClick = BitBtn5Click
        end
        object BtOK: TBitBtn
          Tag = 14
          Left = 220
          Top = 4
          Width = 128
          Height = 40
          Caption = '&Recria toda lista'
          NumGlyphs = 2
          TabOrder = 2
          OnClick = BtOKClick
        end
      end
      object dmkDBGrid1: TdmkDBGrid
        Left = 0
        Top = 80
        Width = 784
        Height = 322
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Width = 42
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Width = 702
            Visible = True
          end>
        Color = clWindow
        DataSource = DsFuncionalidade
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Width = 42
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Width = 702
            Visible = True
          end>
      end
      object StaticText1: TStaticText
        Left = 0
        Top = 0
        Width = 164
        Height = 17
        Align = alTop
        Alignment = taCenter
        BevelOuter = bvRaised
        BorderStyle = sbsSunken
        Caption = 'Pesquisa de funcionalidade:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 635
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 587
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 539
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 334
        Height = 32
        Caption = 'Campos de Arquivos CNAB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 334
        Height = 32
        Caption = 'Campos de Arquivos CNAB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 334
        Height = 32
        Caption = 'Campos de Arquivos CNAB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 450
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 186
    ExplicitWidth = 635
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 494
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 230
    ExplicitWidth = 635
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object PnSaiDesis: TPanel
        Left = 636
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 487
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
    end
  end
  object frxFuncionalidade: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39641.809299583300000000
    ReportOptions.LastChange = 39641.809299583300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 91
    Top = 11
    Datasets = <
      item
        DataSet = frxDsFuncionalidade
        DataSetName = 'frxDsFuncionalidade'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object PageFooter1: TfrxPageFooter
        Height = 45.354360000000000000
        Top = 249.448980000000000000
        Width = 793.701300000000000000
        object Memo6: TfrxMemoView
          Left = 472.441250000000000000
          Width = 245.669450000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 94.488250000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo1: TfrxMemoView
          Left = 75.590600000000000000
          Top = 37.795300000000000000
          Width = 642.520100000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            
              'Lista de Funcionalidades de Campos de Arquivos Banc'#225'rios de Reme' +
              'ssa / Retorno (CNAB)')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 75.590600000000000000
          Top = 75.590600000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 170.078850000000000000
          Top = 75.590600000000000000
          Width = 548.031850000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 15.118110240000000000
        Top = 173.858380000000000000
        Width = 793.701300000000000000
        DataSet = frxDsFuncionalidade
        DataSetName = 'frxDsFuncionalidade'
        RowCount = 0
        object Memo4: TfrxMemoView
          Left = 75.590600000000000000
          Width = 94.488250000000000000
          Height = 15.118110236220500000
          ShowHint = False
          DataField = 'Codigo'
          DataSet = frxDsFuncionalidade
          DataSetName = 'frxDsFuncionalidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFuncionalidade."Codigo"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 170.078850000000000000
          Width = 548.031850000000000000
          Height = 15.118110236220500000
          ShowHint = False
          DataField = 'Nome'
          DataSet = frxDsFuncionalidade
          DataSetName = 'frxDsFuncionalidade'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFuncionalidade."Nome"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsFuncionalidade: TfrxDBDataset
    UserName = 'frxDsFuncionalidade'
    CloseDataSource = False
    DataSet = QrFuncionalidade
    BCDToCurrency = False
    Left = 63
    Top = 11
  end
  object DsFuncionalidade: TDataSource
    DataSet = QrFuncionalidade
    Left = 35
    Top = 11
  end
  object QrFuncionalidade: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM cnab_fld'
      'WHERE Nome LIKE :P0'
      ' ')
    Left = 7
    Top = 11
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFuncionalidadeCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cnab_fld.Codigo'
    end
    object QrFuncionalidadeNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'cnab_fld.Nome'
      Size = 100
    end
  end
end
