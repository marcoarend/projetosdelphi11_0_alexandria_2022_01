object FmCNAB_Rem: TFmCNAB_Rem
  Left = 377
  Top = 168
  Caption = 'BCO-CNAB_-003 :: Remessa de Arquivo CNAB'
  ClientHeight = 615
  ClientWidth = 848
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 47
    Width = 848
    Height = 456
    Align = alClient
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 848
      Height = 47
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label23: TLabel
        Left = 4
        Top = 4
        Width = 110
        Height = 13
        Caption = 'Configura'#231#227'o de envio:'
        FocusControl = DBEdit1
      end
      object DBEdit1: TDBEdit
        Left = 4
        Top = 20
        Width = 40
        Height = 21
        DataField = 'Codigo'
        DataSource = DmBco.DsCNAB_Cfg
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 47
        Top = 20
        Width = 571
        Height = 21
        DataField = 'Nome'
        DataSource = DmBco.DsCNAB_Cfg
        TabOrder = 1
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 624
        Top = 1
        Width = 148
        Height = 45
        Caption = ' CNAB: '
        Columns = 3
        DataField = 'CNAB'
        DataSource = DmBco.DsCNAB_Cfg
        Items.Strings = (
          '000'
          '240'
          '400')
        TabOrder = 2
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 47
      Width = 848
      Height = 409
      ActivePage = TabSheet3
      Align = alClient
      TabOrder = 1
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = 'Arquivos'
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 840
          Height = 381
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitWidth = 842
          ExplicitHeight = 384
          object Panel10: TPanel
            Left = 0
            Top = 332
            Width = 840
            Height = 49
            Align = alBottom
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            ExplicitTop = 335
            ExplicitWidth = 842
            object Label11: TLabel
              Left = 4
              Top = 4
              Width = 29
              Height = 13
              Caption = 'Linha:'
            end
            object Label12: TLabel
              Left = 43
              Top = 4
              Width = 36
              Height = 13
              Caption = 'Coluna:'
            end
            object Label13: TLabel
              Left = 82
              Top = 4
              Width = 27
              Height = 13
              Caption = 'Tam.:'
            end
            object Label14: TLabel
              Left = 122
              Top = 4
              Width = 87
              Height = 13
              Caption = 'Texto pesquisado:'
            end
            object Edit2: TEdit
              Left = 4
              Top = 20
              Width = 36
              Height = 21
              TabOrder = 0
              Text = '0'
              OnChange = Edit2Change
            end
            object Edit3: TEdit
              Left = 43
              Top = 20
              Width = 37
              Height = 21
              TabOrder = 1
              Text = '0'
              OnChange = Edit3Change
            end
            object Edit4: TEdit
              Left = 82
              Top = 20
              Width = 37
              Height = 21
              TabOrder = 2
              Text = '0'
              OnChange = Edit4Change
            end
            object Edit6: TEdit
              Left = 122
              Top = 20
              Width = 643
              Height = 21
              ReadOnly = True
              TabOrder = 3
            end
          end
          object PageControl2: TPageControl
            Left = 0
            Top = 0
            Width = 840
            Height = 332
            ActivePage = TabSheet10
            Align = alClient
            TabOrder = 1
            OnChange = PageControl2Change
            ExplicitHeight = 329
            object TabSheet10: TTabSheet
              Caption = 'Gerado'
              object Panel11: TPanel
                Left = 0
                Top = 0
                Width = 832
                Height = 304
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                ExplicitWidth = 835
                ExplicitHeight = 310
                object Panel8: TPanel
                  Left = 0
                  Top = 0
                  Width = 832
                  Height = 29
                  Align = alTop
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  ExplicitWidth = 835
                  object Label15: TLabel
                    Left = 8
                    Top = 8
                    Width = 39
                    Height = 13
                    Caption = 'Arquivo:'
                  end
                  object EdArqGerado: TEdit
                    Left = 51
                    Top = 4
                    Width = 706
                    Height = 21
                    ReadOnly = True
                    TabOrder = 0
                  end
                end
                object MeGerado: TMemo
                  Left = 0
                  Top = 29
                  Width = 832
                  Height = 255
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Courier New'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 1
                  WordWrap = False
                  OnEnter = MeGeradoEnter
                  OnExit = MeGeradoExit
                  ExplicitWidth = 835
                  ExplicitHeight = 261
                end
                object dmkMBGravado: TdmkMemoBar
                  Left = 0
                  Top = 284
                  Width = 832
                  Height = 20
                  Align = alBottom
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 2
                  Memo = MeGerado
                  OverwriteCaretWidth = 12
                  OverwriteCaretHeight = 16
                  ExplicitTop = 290
                  ExplicitWidth = 835
                end
              end
            end
            object TabSheet11: TTabSheet
              Caption = 'Arquivo a ser comparado'
              ImageIndex = 1
              object Panel7: TPanel
                Left = 0
                Top = 0
                Width = 832
                Height = 304
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                ExplicitWidth = 835
                ExplicitHeight = 310
                object Panel12: TPanel
                  Left = 0
                  Top = 0
                  Width = 832
                  Height = 29
                  Align = alTop
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  ExplicitWidth = 835
                  object Label28: TLabel
                    Left = 8
                    Top = 8
                    Width = 39
                    Height = 13
                    Caption = 'Arquivo:'
                  end
                  object SpeedButton1: TSpeedButton
                    Left = 737
                    Top = 4
                    Width = 20
                    Height = 21
                    Caption = '...'
                    OnClick = SpeedButton1Click
                  end
                  object EdArqCompar: TEdit
                    Left = 51
                    Top = 4
                    Width = 683
                    Height = 21
                    ReadOnly = True
                    TabOrder = 0
                  end
                end
                object MeCompar: TMemo
                  Left = 0
                  Top = 29
                  Width = 832
                  Height = 255
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Courier New'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 1
                  WordWrap = False
                  OnEnter = MeComparEnter
                  OnExit = MeComparExit
                  ExplicitWidth = 835
                  ExplicitHeight = 261
                end
                object dmkMBCompar: TdmkMemoBar
                  Left = 0
                  Top = 284
                  Width = 832
                  Height = 20
                  Align = alBottom
                  BevelOuter = bvNone
                  TabOrder = 2
                  Memo = MeCompar
                  OverwriteCaretWidth = 12
                  OverwriteCaretHeight = 16
                  ExplicitTop = 290
                  ExplicitWidth = 835
                end
              end
            end
            object TabSheet7: TTabSheet
              Caption = 'Compara'#231#227'o'
              ImageIndex = 2
              object Panel13: TPanel
                Left = 0
                Top = 0
                Width = 835
                Height = 310
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object GradeG: TStringGrid
                  Left = 0
                  Top = 85
                  Width = 835
                  Height = 166
                  Align = alClient
                  ColCount = 240
                  DefaultColWidth = 6
                  DefaultRowHeight = 10
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Courier New'
                  Font.Style = []
                  Options = [goFixedVertLine, goFixedHorzLine, goEditing]
                  ParentFont = False
                  TabOrder = 0
                  OnDrawCell = GradeGDrawCell
                  OnSelectCell = GradeGSelectCell
                end
                object Panel17: TPanel
                  Left = 0
                  Top = 0
                  Width = 835
                  Height = 68
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Panel18: TPanel
                    Left = 0
                    Top = 0
                    Width = 99
                    Height = 68
                    Align = alLeft
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object Label22: TLabel
                      Left = 0
                      Top = 47
                      Width = 96
                      Height = 13
                      Caption = 'Endere'#231'o, tamanho:'
                    end
                    object BitBtn1: TBitBtn
                      Left = 0
                      Top = 1
                      Width = 89
                      Height = 39
                      Caption = '&Compara'
                      NumGlyphs = 2
                      TabOrder = 0
                      OnClick = BitBtn1Click
                    end
                  end
                  object Panel19: TPanel
                    Left = 99
                    Top = 0
                    Width = 736
                    Height = 68
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 1
                    object Label16: TLabel
                      Left = 0
                      Top = 4
                      Width = 29
                      Height = 13
                      Caption = 'Linha:'
                    end
                    object Label17: TLabel
                      Left = 0
                      Top = 26
                      Width = 36
                      Height = 13
                      Caption = 'Coluna:'
                    end
                    object Label18: TLabel
                      Left = 86
                      Top = 47
                      Width = 36
                      Height = 13
                      Caption = 'Campo:'
                    end
                    object Label19: TLabel
                      Left = 86
                      Top = 26
                      Width = 83
                      Height = 13
                      Caption = 'Valor comparado:'
                    end
                    object Label20: TLabel
                      Left = 86
                      Top = 4
                      Width = 63
                      Height = 13
                      Caption = 'Valor gerado:'
                    end
                    object dmkEdVerifLin: TdmkEdit
                      Left = 39
                      Top = 0
                      Width = 43
                      Height = 21
                      Alignment = taRightJustify
                      ReadOnly = True
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '000'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object dmkEdVerifCol: TdmkEdit
                      Left = 39
                      Top = 22
                      Width = 43
                      Height = 20
                      Alignment = taRightJustify
                      ReadOnly = True
                      TabOrder = 1
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '000'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object dmkEdVerifValCompar: TdmkEdit
                      Left = 170
                      Top = 22
                      Width = 1890
                      Height = 20
                      ReadOnly = True
                      TabOrder = 2
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object dmkEdVerifCampoCod: TdmkEdit
                      Left = 126
                      Top = 43
                      Width = 44
                      Height = 21
                      Alignment = taRightJustify
                      ReadOnly = True
                      TabOrder = 3
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-1000'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '000'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object dmkEdVerifValGerado: TdmkEdit
                      Left = 170
                      Top = 0
                      Width = 1890
                      Height = 21
                      ReadOnly = True
                      TabOrder = 4
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object dmkEdVerifCampoTxt: TdmkEdit
                      Left = 170
                      Top = 43
                      Width = 1890
                      Height = 21
                      ReadOnly = True
                      TabOrder = 5
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object dmkEdVerifPos: TdmkEdit
                      Left = 0
                      Top = 43
                      Width = 82
                      Height = 21
                      ReadOnly = True
                      TabOrder = 6
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '000 a 000 = 000'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = '000 a 000 = 000'
                      ValWarn = False
                    end
                  end
                end
                object GradeO: TStringGrid
                  Left = 5
                  Top = 134
                  Width = 106
                  Height = 64
                  ColCount = 240
                  DefaultColWidth = 6
                  DefaultRowHeight = 10
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Courier New'
                  Font.Style = []
                  Options = [goFixedVertLine, goFixedHorzLine, goEditing]
                  ParentFont = False
                  TabOrder = 2
                  Visible = False
                end
                object GradeC: TStringGrid
                  Left = 0
                  Top = 290
                  Width = 835
                  Height = 20
                  Align = alBottom
                  ColCount = 240
                  DefaultColWidth = 19
                  DefaultRowHeight = 10
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Courier New'
                  Font.Style = []
                  Options = [goFixedVertLine, goFixedHorzLine, goEditing]
                  ParentFont = False
                  ScrollBars = ssNone
                  TabOrder = 3
                  Visible = False
                end
                object GradeI: TStringGrid
                  Left = 0
                  Top = 251
                  Width = 835
                  Height = 19
                  Align = alBottom
                  ColCount = 240
                  DefaultColWidth = 19
                  DefaultRowHeight = 10
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Courier New'
                  Font.Style = []
                  Options = [goFixedVertLine, goFixedHorzLine, goEditing]
                  ParentFont = False
                  ScrollBars = ssNone
                  TabOrder = 4
                  Visible = False
                end
                object GradeF: TStringGrid
                  Left = 0
                  Top = 270
                  Width = 835
                  Height = 20
                  Align = alBottom
                  ColCount = 240
                  DefaultColWidth = 19
                  DefaultRowHeight = 10
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'Courier New'
                  Font.Style = []
                  Options = [goFixedVertLine, goFixedHorzLine, goEditing]
                  ParentFont = False
                  ScrollBars = ssNone
                  TabOrder = 5
                  Visible = False
                end
                object PB1: TProgressBar
                  Left = 0
                  Top = 68
                  Width = 835
                  Height = 17
                  Align = alTop
                  TabOrder = 6
                end
              end
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' [0] Header '
        ImageIndex = 1
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 842
          Height = 384
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox1: TGroupBox
            Left = 0
            Top = 0
            Width = 842
            Height = 100
            Align = alTop
            Caption = ' Dados do registro'
            TabOrder = 0
            object Label1: TLabel
              Left = 8
              Top = 16
              Width = 29
              Height = 13
              Caption = 'Linha:'
            end
            object Label2: TLabel
              Left = 197
              Top = 16
              Width = 42
              Height = 13
              Caption = 'Registro:'
            end
            object Label3: TLabel
              Left = 47
              Top = 16
              Width = 39
              Height = 13
              Caption = 'Servi'#231'o:'
            end
            object Label7: TLabel
              Left = 295
              Top = 16
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object Label9: TLabel
              Left = 8
              Top = 57
              Width = 135
              Height = 13
              Caption = 'Ident. do cedente no banco:'
            end
            object Label24: TLabel
              Left = 150
              Top = 57
              Width = 58
              Height = 13
              Caption = 'Dt Gera'#231#227'o:'
            end
            object Label29: TLabel
              Left = 218
              Top = 57
              Width = 58
              Height = 13
              Caption = 'Hr Gera'#231#227'o:'
            end
            object Label30: TLabel
              Left = 552
              Top = 16
              Width = 29
              Height = 13
              Caption = 'T.I.E.:'
            end
            object Label31: TLabel
              Left = 584
              Top = 16
              Width = 133
              Height = 13
              Caption = 'N'#186' da inscri'#231#227'o da empresa:'
            end
            object dmkEd_0_000: TdmkEdit
              Left = 8
              Top = 31
              Width = 36
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 4
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0001'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 1
              ValWarn = False
            end
            object dmkEd_0_005: TdmkEdit
              Left = 197
              Top = 31
              Width = 21
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '1'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 1
              ValWarn = False
            end
            object dmkEd_0_006: TdmkEdit
              Left = 221
              Top = 31
              Width = 72
              Height = 21
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = 'REMESSA'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 'REMESSA'
              ValWarn = False
            end
            object dmkEd_0_003: TdmkEdit
              Left = 47
              Top = 31
              Width = 21
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '01'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 1
              ValWarn = False
            end
            object dmkEd_0_004: TdmkEdit
              Left = 71
              Top = 31
              Width = 123
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = 'COBRANCA'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 'COBRANCA'
              ValWarn = False
            end
            object dmkEd_0_402: TdmkEdit
              Left = 295
              Top = 31
              Width = 254
              Height = 21
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object dmkEd_0_990: TdmkEdit
              Left = 150
              Top = 72
              Width = 67
              Height = 21
              TabOrder = 9
              FormatType = dmktfDate
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0d
              ValWarn = False
            end
            object dmkEd_0_410: TdmkEdit
              Left = 8
              Top = 72
              Width = 139
              Height = 21
              TabOrder = 8
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object dmkEd_0_991: TdmkEdit
              Left = 218
              Top = 72
              Width = 68
              Height = 21
              TabOrder = 10
              FormatType = dmktfTime
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00:00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object dmkEd_0_801: TdmkEdit
              Left = 552
              Top = 31
              Width = 28
              Height = 21
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = dmkEd_0_001Change
            end
            object dmkEd_0_802: TdmkEdit
              Left = 584
              Top = 31
              Width = 142
              Height = 21
              TabOrder = 7
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object GroupBox2: TGroupBox
            Left = 0
            Top = 100
            Width = 842
            Height = 59
            Align = alTop
            Caption = ' Dados banc'#225'rios: '
            TabOrder = 1
            object Label4: TLabel
              Left = 8
              Top = 16
              Width = 42
              Height = 13
              Caption = 'Ag'#234'ncia:'
            end
            object Label5: TLabel
              Left = 67
              Top = 16
              Width = 73
              Height = 13
              Caption = 'Conta corrente:'
            end
            object Label6: TLabel
              Left = 162
              Top = 16
              Width = 25
              Height = 13
              Caption = 'DAC:'
            end
            object Label8: TLabel
              Left = 189
              Top = 16
              Width = 95
              Height = 13
              Caption = 'Institui'#231#227'o banc'#225'ria:'
            end
            object Label21: TLabel
              Left = 678
              Top = 16
              Width = 116
              Height = 13
              Caption = 'C'#243'digo oculto do banco:'
            end
            object Label25: TLabel
              Left = 533
              Top = 16
              Width = 100
              Height = 13
              Caption = 'C'#243'd. especial banco:'
            end
            object Label26: TLabel
              Left = 801
              Top = 16
              Width = 22
              Height = 13
              Caption = 'Ver.:'
            end
            object dmkEd_0_020: TdmkEdit
              Left = 8
              Top = 31
              Width = 36
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 4
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object dmkEd_0_021: TdmkEdit
              Left = 67
              Top = 31
              Width = 72
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object dmkEd_0_022: TdmkEdit
              Left = 47
              Top = 31
              Width = 17
              Height = 21
              CharCase = ecUpperCase
              MaxLength = 1
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = True
              ValueVariant = ''
              ValWarn = False
            end
            object dmkEd_0_023: TdmkEdit
              Left = 142
              Top = 31
              Width = 16
              Height = 21
              CharCase = ecUpperCase
              MaxLength = 1
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = True
              ValueVariant = ''
              ValWarn = False
            end
            object dmkEd_0_024: TdmkEdit
              Left = 162
              Top = 31
              Width = 24
              Height = 21
              CharCase = ecUpperCase
              MaxLength = 1
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = True
              ValueVariant = ''
              ValWarn = False
            end
            object dmkEdNomeBanco: TdmkEdit
              Left = 221
              Top = 31
              Width = 309
              Height = 21
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object dmkEd_0_001: TdmkEdit
              Left = 189
              Top = 31
              Width = 29
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 3
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = dmkEd_0_001Change
            end
            object dmkEd_0_699: TdmkEdit
              Left = 678
              Top = 31
              Width = 120
              Height = 21
              TabOrder = 8
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object dmkEd_0_037: TdmkEdit
              Left = 533
              Top = 31
              Width = 142
              Height = 21
              TabOrder = 7
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object dmkEd_0_891: TdmkEdit
              Left = 801
              Top = 31
              Width = 28
              Height = 21
              Alignment = taRightJustify
              TabOrder = 9
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 3
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = dmkEd_0_001Change
            end
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = ' [1] Detalhe '
        ImageIndex = 2
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 840
          Height = 381
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitWidth = 842
          ExplicitHeight = 384
          object Panel5: TPanel
            Left = 0
            Top = 351
            Width = 840
            Height = 30
            Align = alBottom
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            ExplicitTop = 354
            ExplicitWidth = 842
            object Label10: TLabel
              Left = 8
              Top = 8
              Width = 183
              Height = 13
              Caption = 'C'#243'digo de item da coluna selecionada:'
            end
            object LaCodDmk_1: TLabel
              Left = 193
              Top = 8
              Width = 22
              Height = 13
              Caption = '000'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
          end
          object PageControl3: TPageControl
            Left = 0
            Top = 0
            Width = 840
            Height = 351
            ActivePage = TabSheet8
            Align = alClient
            TabOrder = 1
            ExplicitHeight = 348
            object TabSheet8: TTabSheet
              Caption = 'CNAB 400'
              object Grade_1: TStringGrid
                Left = 0
                Top = 0
                Width = 835
                Height = 330
                Align = alClient
                ColCount = 2
                DefaultColWidth = 26
                DefaultRowHeight = 14
                RowCount = 2
                Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
                TabOrder = 0
                OnSelectCell = Grade_1SelectCell
              end
            end
            object TabSheet12: TTabSheet
              Caption = 'CNAB 240'
              ImageIndex = 1
              object PageControl4: TPageControl
                Left = 0
                Top = 0
                Width = 832
                Height = 323
                ActivePage = TabSheet13
                Align = alClient
                TabOrder = 0
                ExplicitHeight = 317
                object TabSheet13: TTabSheet
                  Caption = '[P]'
                  object Grade_240_3_P: TStringGrid
                    Left = 0
                    Top = 0
                    Width = 829
                    Height = 305
                    Align = alClient
                    ColCount = 2
                    DefaultColWidth = 26
                    DefaultRowHeight = 14
                    FixedCols = 0
                    RowCount = 2
                    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
                    TabOrder = 0
                    OnSelectCell = Grade_240_3_PSelectCell
                  end
                end
                object TabSheet14: TTabSheet
                  Caption = '[Q]'
                  ImageIndex = 1
                  object Grade_240_3_Q: TStringGrid
                    Left = 0
                    Top = 0
                    Width = 829
                    Height = 305
                    Align = alClient
                    ColCount = 2
                    DefaultColWidth = 26
                    DefaultRowHeight = 14
                    FixedCols = 0
                    RowCount = 2
                    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
                    TabOrder = 0
                    OnSelectCell = Grade_240_3_QSelectCell
                  end
                end
                object TabSheet15: TTabSheet
                  Margins.Left = 2
                  Margins.Top = 2
                  Margins.Right = 2
                  Margins.Bottom = 2
                  Caption = ' [R] '
                  ImageIndex = 2
                  object Grade_240_3_R: TStringGrid
                    Left = 0
                    Top = 0
                    Width = 829
                    Height = 305
                    Align = alClient
                    ColCount = 2
                    DefaultColWidth = 26
                    DefaultRowHeight = 14
                    FixedCols = 0
                    RowCount = 2
                    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
                    TabOrder = 0
                    OnSelectCell = Grade_240_3_RSelectCell
                  end
                end
                object TabSheet16: TTabSheet
                  Margins.Left = 2
                  Margins.Top = 2
                  Margins.Right = 2
                  Margins.Bottom = 2
                  Caption = ' [S] '
                  ImageIndex = 3
                  object Grade_240_3_S: TStringGrid
                    Left = 0
                    Top = 0
                    Width = 829
                    Height = 305
                    Align = alClient
                    ColCount = 2
                    DefaultColWidth = 26
                    DefaultRowHeight = 14
                    FixedCols = 0
                    RowCount = 2
                    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
                    TabOrder = 0
                    OnSelectCell = Grade_240_3_SSelectCell
                  end
                end
              end
            end
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = '  [2] Mensagem '
        ImageIndex = 3
        object Grade_2: TStringGrid
          Left = 0
          Top = 0
          Width = 842
          Height = 384
          Align = alClient
          ColCount = 2
          DefaultColWidth = 26
          DefaultRowHeight = 14
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
          TabOrder = 0
          OnSelectCell = Grade_1SelectCell
        end
      end
      object TabSheet5: TTabSheet
        Caption = ' [3] Rateio de cr'#233'dito '
        ImageIndex = 4
        object Panel15: TPanel
          Left = 0
          Top = 0
          Width = 842
          Height = 384
          Align = alClient
          BevelOuter = bvNone
          Caption = 'Sem implementa'#231#227'o'
          ParentBackground = False
          TabOrder = 0
        end
      end
      object TabSheet6: TTabSheet
        Caption = ' [5] Detalhe '
        ImageIndex = 5
        object Grade_5: TStringGrid
          Left = 0
          Top = 0
          Width = 842
          Height = 384
          Align = alClient
          ColCount = 2
          DefaultColWidth = 26
          DefaultRowHeight = 14
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
          TabOrder = 0
        end
      end
      object TabSheet9: TTabSheet
        Caption = ' [9] Trailer '
        ImageIndex = 8
        object Panel16: TPanel
          Left = 0
          Top = 0
          Width = 842
          Height = 384
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label27: TLabel
            Left = 8
            Top = 16
            Width = 82
            Height = 13
            Caption = 'Valor total t'#237'tulos:'
          end
          object dmkEd_9_302: TdmkEdit
            Left = 8
            Top = 31
            Width = 83
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 4
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 848
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 801
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 754
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 318
        Height = 31
        Caption = 'Remessa de Arquivo CNAB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 318
        Height = 31
        Caption = 'Remessa de Arquivo CNAB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 6
        Top = 10
        Width = 318
        Height = 31
        Caption = 'Remessa de Arquivo CNAB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 503
    Width = 848
    Height = 43
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel20: TPanel
      Left = 2
      Top = 14
      Width = 844
      Height = 28
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 546
    Width = 848
    Height = 69
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 705
      Top = 14
      Width = 141
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel21: TPanel
      Left = 2
      Top = 14
      Width = 703
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 13
        Top = 3
        Width = 118
        Height = 39
        Caption = '&Gera'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtMover: TBitBtn
        Tag = 10070
        Left = 133
        Top = 3
        Width = 176
        Height = 39
        Caption = '&Mover para: Enviados'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtMoverClick
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 541
    Top = 307
  end
end
