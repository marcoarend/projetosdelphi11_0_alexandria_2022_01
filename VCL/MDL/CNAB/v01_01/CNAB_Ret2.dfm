object FmCNAB_Ret2: TFmCNAB_Ret2
  Left = 352
  Top = 160
  Caption = 'RET-CNABx-001 :: Retorno de Arquivos CNAB'
  ClientHeight = 695
  ClientWidth = 1556
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnMovimento: TPanel
    Left = 0
    Top = 59
    Width = 1556
    Height = 636
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Color = clAppWorkSpace
    TabOrder = 2
    Visible = False
    OnClick = PnMovimentoClick
    object Panel3: TPanel
      Left = 0
      Top = 577
      Width = 1556
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object Label1: TLabel
        Left = 241
        Top = 2
        Width = 166
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '* Diferen'#231'a de juros e multa:'
      end
      object Label2: TLabel
        Left = 251
        Top = 20
        Width = 174
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Positivo (azul): pagou a mais.'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object Label3: TLabel
        Left = 251
        Top = 37
        Width = 225
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Negativo (vermelho): pagou a menos.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Panel4: TPanel
        Left = 1419
        Top = 1
        Width = 136
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 5
        object BitBtn2: TBitBtn
          Tag = 13
          Left = 2
          Top = 4
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sair'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object Button2: TButton
        Left = 423
        Top = 5
        Width = 93
        Height = 31
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Button1'
        TabOrder = 6
        Visible = False
        OnClick = Button1Click
      end
      object BtExclui: TBitBtn
        Tag = 10039
        Left = 633
        Top = 5
        Width = 110
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Bloqueto'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtExcluiClick
      end
      object BtConcilia: TBitBtn
        Tag = 10011
        Left = 11
        Top = 5
        Width = 111
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Conciliar'
        Enabled = False
        TabOrder = 0
        OnClick = BtConciliaClick
      end
      object BitBtn1: TBitBtn
        Tag = 10013
        Left = 126
        Top = 5
        Width = 110
        Height = 49
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Voltar'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BitBtn1Click
      end
      object BtAgenda: TBitBtn
        Tag = 10032
        Left = 519
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Agenda'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtAgendaClick
      end
      object BtItens: TBitBtn
        Tag = 10038
        Left = 746
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Lan'#231'to'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = BtItensClick
      end
    end
    object PnCarregados: TPanel
      Left = 0
      Top = 0
      Width = 1556
      Height = 410
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 1
      object Splitter2: TSplitter
        Left = 1
        Top = 198
        Width = 1554
        Height = 4
        Cursor = crVSplit
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        ExplicitWidth = 1553
      end
      object Panel5: TPanel
        Left = 1
        Top = 202
        Width = 1554
        Height = 207
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        Caption = 'Panel5'
        TabOrder = 0
        object PageControl4: TPageControl
          Left = 1
          Top = 1
          Width = 1552
          Height = 205
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet6
          Align = alClient
          TabOrder = 0
          object TabSheet6: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Sem agrupamento'
            object DBGrid2: TDBGrid
              Left = 0
              Top = 0
              Width = 1544
              Height = 174
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataSource = DsLeiItens
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -14
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEPROPRIET'
                  Title.Caption = 'Cond'#244'mino'
                  Width = 233
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'UH'
                  Width = 45
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Mez_TXT'
                  Title.Caption = 'Mes'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Credito'
                  Title.Caption = 'Cr'#233'dito'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Descricao'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 216
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Vencimento'
                  Title.Caption = 'Vencimen.'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Carteira'
                  Width = 47
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECART'
                  Title.Caption = 'Descri'#231#227'o carteira'
                  Width = 240
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CartDest'
                  Title.Caption = 'Cart. destino'
                  Visible = True
                end>
            end
          end
          object TabSheet7: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Com agrupamento '
            ImageIndex = 1
            object DBGrid3: TDBGrid
              Left = 0
              Top = 0
              Width = 1544
              Height = 174
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataSource = DsLeiAgr
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -14
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEPROPRIET'
                  Title.Caption = 'Cond'#244'mino'
                  Width = 233
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'UH'
                  Width = 45
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Mez_TXT'
                  Title.Caption = 'M'#234's'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME_TIPO_BLOQ'
                  Title.Caption = 'Tipo de bloqueto'
                  Width = 180
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Valor'
                  Title.Caption = 'Cr'#233'dito'
                  Visible = True
                end>
            end
          end
          object TabSheet8: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tempos'
            ImageIndex = 2
            object Memo3: TMemo
              Left = 0
              Top = 0
              Width = 1544
              Height = 174
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
          end
        end
      end
      object PageControl2: TPageControl
        Left = 1
        Top = 1
        Width = 1554
        Height = 197
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ActivePage = TabSheet4
        Align = alClient
        TabOrder = 1
        object TabSheet4: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Est'#225'vel'
          object DBGLei: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 1546
            Height = 111
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'DJM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Title.Caption = 'DJM*'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEENT'
                Title.Caption = 'Cliente (Entidade)'
                Width = 226
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IDNum'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Banco'
                Title.Caption = 'Bco'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrCodi'
                Title.Caption = 'OC'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrData'
                Title.Caption = 'Data ocor.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValTitul'
                Title.Caption = 'Val.Titulo'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValPago'
                Title.Caption = 'Val.Pago'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuros'
                Title.Caption = 'Val.Juros'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValMulta'
                Title.Caption = 'Val.Multa'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuMul'
                Title.Caption = 'Mul.+Jur.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuitaData'
                Title.Caption = 'Data quit.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DTA_TARIF_TXT'
                Title.Caption = 'Dt.d'#233'b.tar.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValAbati'
                Title.Caption = 'Val.Abatim.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NossoNum'
                Title.Caption = 'Nosso n'#250'm.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SeuNum'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValDesco'
                Title.Caption = 'Val.Desc.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DevJuros'
                Title.Caption = 'Dev.Juros'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DevMulta'
                Title.Caption = 'Dev.Multa'
                Visible = True
              end>
            Color = clWindow
            DataSource = DsLei
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDrawColumnCell = DBGLeiDrawColumnCell
            Columns = <
              item
                Expanded = False
                FieldName = 'DJM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Title.Caption = 'DJM*'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEENT'
                Title.Caption = 'Cliente (Entidade)'
                Width = 226
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IDNum'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Banco'
                Title.Caption = 'Bco'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrCodi'
                Title.Caption = 'OC'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrData'
                Title.Caption = 'Data ocor.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValTitul'
                Title.Caption = 'Val.Titulo'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValPago'
                Title.Caption = 'Val.Pago'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuros'
                Title.Caption = 'Val.Juros'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValMulta'
                Title.Caption = 'Val.Multa'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuMul'
                Title.Caption = 'Mul.+Jur.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuitaData'
                Title.Caption = 'Data quit.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DTA_TARIF_TXT'
                Title.Caption = 'Dt.d'#233'b.tar.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValAbati'
                Title.Caption = 'Val.Abatim.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NossoNum'
                Title.Caption = 'Nosso n'#250'm.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SeuNum'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValDesco'
                Title.Caption = 'Val.Desc.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DevJuros'
                Title.Caption = 'Dev.Juros'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DevMulta'
                Title.Caption = 'Dev.Multa'
                Visible = True
              end>
          end
          object Panel6: TPanel
            Left = 0
            Top = 111
            Width = 1546
            Height = 55
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            TabOrder = 1
            object Label4: TLabel
              Left = 5
              Top = 5
              Width = 91
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Valor bloqueto:'
              FocusControl = DBEdit1
            end
            object Label5: TLabel
              Left = 172
              Top = 5
              Width = 112
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Qtd. arrecada'#231#245'es'
              FocusControl = DBEdit2
            end
            object Label6: TLabel
              Left = 290
              Top = 5
              Width = 92
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Qtd. consumos:'
              FocusControl = DBEdit3
            end
            object Label7: TLabel
              Left = 409
              Top = 5
              Width = 77
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Qtd reparcel.'
              FocusControl = DBEdit4
            end
            object DBEdit1: TDBEdit
              Left = 5
              Top = 25
              Width = 165
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Credito'
              DataSource = DataSource1
              TabOrder = 0
            end
            object DBEdit2: TDBEdit
              Left = 172
              Top = 25
              Width = 114
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'ID600'
              DataSource = DataSource1
              TabOrder = 1
            end
            object DBEdit3: TDBEdit
              Left = 290
              Top = 25
              Width = 114
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'ID601'
              DataSource = DataSource1
              TabOrder = 2
            end
            object DBEdit4: TDBEdit
              Left = 409
              Top = 25
              Width = 113
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'ID610'
              DataSource = DataSource1
              TabOrder = 3
            end
          end
        end
        object TabSheet5: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Desenvolvimento'
          ImageIndex = 1
          object dmkDBGrid1: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 1546
            Height = 166
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'DJM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Title.Caption = 'DJM*'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEENT'
                Title.Caption = 'Cliente (Entidade)'
                Width = 226
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IDNum'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Banco'
                Title.Caption = 'Bco'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrCodi'
                Title.Caption = 'OC'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrData'
                Title.Caption = 'Data ocor.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VALBOLETO'
                Title.Caption = 'Val.bloq.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValTitul'
                Title.Caption = 'Val.Titulo'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValPago'
                Title.Caption = 'Val.Pago'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuros'
                Title.Caption = 'Val.Juros'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValMulta'
                Title.Caption = 'Val.Multa'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuMul'
                Title.Caption = 'Mul.+Jur.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuitaData'
                Title.Caption = 'Data quit.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DTA_TARIF_TXT'
                Title.Caption = 'Dt.d'#233'b.tar.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValAbati'
                Title.Caption = 'Val.Abatim.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NossoNum'
                Title.Caption = 'Nosso n'#250'm.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SeuNum'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValDesco'
                Title.Caption = 'Val.Desc.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DevJuros'
                Title.Caption = 'Dev.Juros'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DevMulta'
                Title.Caption = 'Dev.Multa'
                Visible = True
              end>
            Color = clWindow
            DataSource = DsLei
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'DJM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Title.Caption = 'DJM*'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEENT'
                Title.Caption = 'Cliente (Entidade)'
                Width = 226
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IDNum'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Banco'
                Title.Caption = 'Bco'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrCodi'
                Title.Caption = 'OC'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrData'
                Title.Caption = 'Data ocor.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VALBOLETO'
                Title.Caption = 'Val.bloq.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValTitul'
                Title.Caption = 'Val.Titulo'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValPago'
                Title.Caption = 'Val.Pago'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuros'
                Title.Caption = 'Val.Juros'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValMulta'
                Title.Caption = 'Val.Multa'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuMul'
                Title.Caption = 'Mul.+Jur.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuitaData'
                Title.Caption = 'Data quit.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DTA_TARIF_TXT'
                Title.Caption = 'Dt.d'#233'b.tar.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValAbati'
                Title.Caption = 'Val.Abatim.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NossoNum'
                Title.Caption = 'Nosso n'#250'm.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SeuNum'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValDesco'
                Title.Caption = 'Val.Desc.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DevJuros'
                Title.Caption = 'Dev.Juros'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DevMulta'
                Title.Caption = 'Dev.Multa'
                Visible = True
              end>
          end
        end
      end
    end
  end
  object PnCarrega: TPanel
    Left = 0
    Top = 59
    Width = 1556
    Height = 636
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clHighlight
    TabOrder = 1
    object PainelConfirma: TPanel
      Left = 1
      Top = 576
      Width = 1554
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object PB1: TProgressBar
        Left = 591
        Top = 25
        Width = 227
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 4
        Visible = False
      end
      object BtCarrega: TBitBtn
        Tag = 14
        Left = 25
        Top = 5
        Width = 110
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Carrega'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtCarregaClick
      end
      object Panel2: TPanel
        Left = 1416
        Top = 1
        Width = 137
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 4
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object Button1: TButton
        Left = 699
        Top = 20
        Width = 92
        Height = 30
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Button1'
        TabOrder = 2
        Visible = False
        OnClick = Button1Click
      end
      object BtAbertos: TBitBtn
        Tag = 10017
        Left = 241
        Top = 5
        Width = 111
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Abertos'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtAbertosClick
      end
      object CkReverter: TCheckBox
        Left = 143
        Top = 25
        Width = 95
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Re-registrar.'
        TabOrder = 5
      end
      object BtBuffer: TBitBtn
        Left = 354
        Top = 5
        Width = 111
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Buffer'
        NumGlyphs = 2
        TabOrder = 6
        OnClick = BtBufferClick
      end
    end
    object PnArquivos: TPanel
      Left = 1
      Top = 1
      Width = 1554
      Height = 502
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Splitter3: TSplitter
        Left = 0
        Top = 341
        Width = 1554
        Height = 6
        Cursor = crVSplit
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        ExplicitWidth = 1553
      end
      object ListBox1: TListBox
        Left = 241
        Top = 64
        Width = 558
        Height = 36
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 0
        Visible = False
      end
      object MemoTam: TMemo
        Left = 0
        Top = 455
        Width = 1554
        Height = 47
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Lucida Console'
        Font.Style = []
        Lines.Strings = (
          'MemoTam')
        ParentFont = False
        TabOrder = 1
        Visible = False
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1554
        Height = 341
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        Caption = 'Panel1'
        TabOrder = 2
        object Splitter1: TSplitter
          Left = 0
          Top = 185
          Width = 1554
          Height = 6
          Cursor = crVSplit
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          ExplicitWidth = 1553
        end
        object DBGrid1: TDBGrid
          Left = 0
          Top = 31
          Width = 1554
          Height = 154
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          DataSource = DsCNAB_Dir
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGrid1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'CliInt'
              Title.Caption = 'Cli. interno'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEENT'
              Title.Caption = 'Nome entidade'
              Width = 240
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Dir'
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECARTEIRA'
              Title.Caption = 'Carteira'
              Width = 212
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Banco1'
              Title.Caption = 'Bco'
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Diret'#243'rio dos arquivos retorno'
              Width = 344
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEBANCO'
              Title.Caption = 'Nome do banco'
              Width = 196
              Visible = True
            end>
        end
        object GradeA: TStringGrid
          Left = 0
          Top = 191
          Width = 1554
          Height = 150
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          ColCount = 12
          DefaultColWidth = 32
          DefaultRowHeight = 18
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
          TabOrder = 1
          OnDrawCell = GradeADrawCell
          OnSelectCell = GradeASelectCell
          ColWidths = (
            32
            160
            272
            112
            104
            44
            36
            28
            64
            40
            28
            36)
          RowHeights = (
            18
            18)
        end
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 1554
          Height = 31
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          object EdCliInt: TdmkEdit
            Left = 17
            Top = 2
            Width = 69
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdNomeBancoChange
          end
          object EdNomeEnt: TdmkEdit
            Left = 86
            Top = 2
            Width = 296
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdNomeBancoChange
          end
          object EdNOMECARTEIRA: TdmkEdit
            Left = 418
            Top = 2
            Width = 261
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdNomeBancoChange
          end
          object EdCodigo: TdmkEdit
            Left = 383
            Top = 2
            Width = 34
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdNomeBancoChange
          end
          object EdBanco1: TdmkEdit
            Left = 681
            Top = 2
            Width = 34
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdNomeBancoChange
          end
          object EdNome: TdmkEdit
            Left = 715
            Top = 2
            Width = 423
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdNomeBancoChange
          end
          object EdNomeBanco: TdmkEdit
            Left = 1140
            Top = 2
            Width = 241
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdNomeBancoChange
          end
        end
      end
      object PageControl1: TPageControl
        Left = 0
        Top = 347
        Width = 1554
        Height = 108
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ActivePage = TabSheet3
        Align = alClient
        TabOrder = 3
        object TabSheet3: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Itens de todos arquivos lidos'
          ImageIndex = 2
          object Splitter4: TSplitter
            Left = 1294
            Top = 0
            Width = 6
            Height = 77
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alRight
            ExplicitLeft = 1291
            ExplicitHeight = 70
          end
          object Grade1: TStringGrid
            Left = 0
            Top = 0
            Width = 1294
            Height = 77
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            ColCount = 30
            DefaultColWidth = 32
            DefaultRowHeight = 18
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
            PopupMenu = PMMenu
            TabOrder = 0
            OnDrawCell = Grade1DrawCell
            OnMouseDown = Grade1MouseDown
            OnSelectCell = Grade1SelectCell
            ColWidths = (
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32)
          end
          object MeMotiv: TMemo
            Left = 1300
            Top = 0
            Width = 246
            Height = 77
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alRight
            Lines.Strings = (
              'Memo1')
            TabOrder = 1
          end
        end
        object TabSheet1: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Avisos'
          ImageIndex = 1
          object MeAvisos: TMemo
            Left = 0
            Top = 0
            Width = 1546
            Height = 77
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -15
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
        end
        object TabSheet2: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Opera'#231#245'es'
          ImageIndex = 2
          object Memo2: TMemo
            Left = 0
            Top = 0
            Width = 1546
            Height = 77
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
        end
      end
    end
    object StaticText1: TStaticText
      Left = 1
      Top = 556
      Width = 1554
      Height = 20
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      Alignment = taCenter
      Caption = 
        '  Antes de carregar verifique se todas ocorr'#234'ncias j'#225' est'#227'o cada' +
        'stradas nos respectivos bancos (Cadastro de bancos).'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1556
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Retorno de Arquivos CNAB'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object Image1: TImage
      Left = 60
      Top = 1
      Width = 1495
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
      ExplicitLeft = 1
      ExplicitWidth = 1553
    end
    object Pn_L: TPanel
      Left = 1
      Top = 1
      Width = 59
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 0
      object BtPasta: TBitBtn
        Tag = 25
        Left = 5
        Top = 5
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtPastaClick
      end
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 108
    Top = 236
  end
  object QrDupl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo '
      'FROM cnab_lei'
      'WHERE Banco=:P0'
      'AND NossoNum=:P1'
      'AND SeuNum=:P2'
      'AND OcorrCodi=:P3'
      'AND Arquivo=:P4'
      'AND ItemArq=:P5'
      'AND ValTitul=:P6'
      'AND ValPago=:P7'
      'AND QuitaData=:P8')
    Left = 120
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P7'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P8'
        ParamType = ptUnknown
      end>
    object QrDuplCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrSumPg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ad.Juros) Juros, SUM(ad.Desco) Desco, '
      'SUM(ad.Pago) Pago, li.Valor, MAX(ad.Data) MaxData'
      'FROM aduppgs ad'
      'LEFT JOIN lotesits li ON li.Controle=ad.LotesIts'
      'WHERE ad.LotesIts=:P0'
      'GROUP BY ad.LotesIts')
    Left = 204
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumPgJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrSumPgPago: TFloatField
      FieldName = 'Pago'
    end
    object QrSumPgDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrSumPgValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSumPgMaxData: TDateField
      FieldName = 'MaxData'
    end
  end
  object QrOcorreu: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  lo.Cliente CLIENTELOTE, ob.Nome NOMEOCORRENCIA, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN lotesits li ON oc.LotesIts = li.Controle'
      'LEFT JOIN lotes lo ON lo.Codigo = li.Codigo'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.LotesIts=:P0'
      '')
    Left = 685
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcorreuNOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Size = 50
    end
    object QrOcorreuCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOcorreuLotesIts: TIntegerField
      FieldName = 'LotesIts'
      Required = True
    end
    object QrOcorreuDataO: TDateField
      FieldName = 'DataO'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorreuOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
      Required = True
    end
    object QrOcorreuValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorreuLoteQuit: TIntegerField
      FieldName = 'LoteQuit'
      Required = True
    end
    object QrOcorreuLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorreuDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorreuDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorreuUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorreuUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcorreuTaxaP: TFloatField
      FieldName = 'TaxaP'
      Required = True
    end
    object QrOcorreuTaxaV: TFloatField
      FieldName = 'TaxaV'
      Required = True
    end
    object QrOcorreuPago: TFloatField
      FieldName = 'Pago'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorreuDataP: TDateField
      FieldName = 'DataP'
      Required = True
    end
    object QrOcorreuTaxaB: TFloatField
      FieldName = 'TaxaB'
      Required = True
    end
    object QrOcorreuATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrOcorreuCLIENTELOTE: TIntegerField
      FieldName = 'CLIENTELOTE'
    end
    object QrOcorreuData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrOcorreuStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrOcorreuCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrOcorreuSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrOcorreuATZ_TEXTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ATZ_TEXTO'
      Size = 30
      Calculated = True
    end
  end
  object DsOcorreu: TDataSource
    DataSet = QrOcorreu
    Left = 713
    Top = 9
  end
  object QrOcorDupl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ok.Nome NOMEOCORBANK, ok.Base ValOcorBank, od.Ocorrbase '
      'FROM ocordupl od'
      'LEFT JOIN ocorbank ok ON ok.Codigo=od.Ocorrbase'
      'WHERE od.Codigo=:P0')
    Left = 741
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcorDuplNOMEOCORBANK: TWideStringField
      FieldName = 'NOMEOCORBANK'
      Size = 50
    end
    object QrOcorDuplValOcorBank: TFloatField
      FieldName = 'ValOcorBank'
    end
    object QrOcorDuplOcorrbase: TIntegerField
      FieldName = 'Ocorrbase'
      Required = True
    end
  end
  object QrOcorBank: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ocorbank'
      'WHERE Envio=:P0'
      'AND Movimento=:P1'
      '')
    Left = 769
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrOcorBankCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorBankNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrOcorBankLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorBankDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorBankDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorBankUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorBankUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcorBankBase: TFloatField
      FieldName = 'Base'
    end
    object QrOcorBankEnvio: TIntegerField
      FieldName = 'Envio'
    end
    object QrOcorBankMovimento: TIntegerField
      FieldName = 'Movimento'
    end
    object QrOcorBankFormaCNAB: TSmallintField
      FieldName = 'FormaCNAB'
    end
  end
  object Timer2: TTimer
    Enabled = False
    Interval = 100
    Left = 469
    Top = 260
  end
  object QrOB2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome'
      'FROM ocorbank'
      'WHERE Codigo=:P0')
    Left = 796
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOB2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrLocEnt1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE '
      'ent.Nome END NOMEENT, ent.Codigo CLIENTE, cnd.Codigo CliInt '
      'FROM cond cnd'
      'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente'
      'WHERE (CASE WHEN ent.Tipo=0 THEN ent.CNPJ ELSE ent.CPF END)=:P0'
      '/*   '
      '  AND cnd.CodCedente=:P1 '
      '*/')
    Left = 801
    Top = 225
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocEnt1NOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrLocEnt1CLIENTE: TIntegerField
      FieldName = 'CLIENTE'
      Required = True
    end
    object QrLocEnt1CliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object QrLei: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrLeiAfterOpen
    BeforeClose = QrLeiBeforeClose
    AfterClose = QrLeiAfterClose
    AfterScroll = QrLeiAfterScroll
    OnCalcFields = QrLeiCalcFields
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE '
      'ent.Nome END NOMEENT, car.Tipo TipoCart, '
      'ban.DescriCNR, lei.*'
      'FROM cnab_lei lei'
      'LEFT JOIN entidades ent ON ent.Codigo=lei.Entidade'
      'LEFT JOIN carteiras car ON car.Codigo=lei.Carteira'
      'LEFT JOIN bancos    ban ON ban.Codigo=lei.Banco'
      'WHERE Step=0'
      'AND lei.Entidade=:P0'
      '')
    Left = 49
    Top = 285
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLeiCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cnab_lei.Codigo'
    end
    object QrLeiIDNum: TIntegerField
      FieldName = 'IDNum'
      Origin = 'cnab_lei.IDNum'
    end
    object QrLeiBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'cnab_lei.Banco'
      DisplayFormat = '000'
    end
    object QrLeiSeuNum: TIntegerField
      FieldName = 'SeuNum'
      Origin = 'cnab_lei.SeuNum'
      DisplayFormat = '0;-0; '
    end
    object QrLeiOcorrCodi: TWideStringField
      FieldName = 'OcorrCodi'
      Origin = 'cnab_lei.OcorrCodi'
      Size = 10
    end
    object QrLeiOcorrData: TDateField
      FieldName = 'OcorrData'
      Origin = 'cnab_lei.OcorrData'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLeiValTitul: TFloatField
      FieldName = 'ValTitul'
      Origin = 'cnab_lei.ValTitul'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiValAbati: TFloatField
      FieldName = 'ValAbati'
      Origin = 'cnab_lei.ValAbati'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiValDesco: TFloatField
      FieldName = 'ValDesco'
      Origin = 'cnab_lei.ValDesco'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiValPago: TFloatField
      FieldName = 'ValPago'
      Origin = 'cnab_lei.ValPago'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiValJuros: TFloatField
      FieldName = 'ValJuros'
      Origin = 'cnab_lei.ValJuros'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiValMulta: TFloatField
      FieldName = 'ValMulta'
      Origin = 'cnab_lei.ValMulta'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiMotivo1: TWideStringField
      FieldName = 'Motivo1'
      Origin = 'cnab_lei.Motivo1'
      Size = 2
    end
    object QrLeiMotivo2: TWideStringField
      FieldName = 'Motivo2'
      Origin = 'cnab_lei.Motivo2'
      Size = 2
    end
    object QrLeiMotivo3: TWideStringField
      FieldName = 'Motivo3'
      Origin = 'cnab_lei.Motivo3'
      Size = 2
    end
    object QrLeiMotivo4: TWideStringField
      FieldName = 'Motivo4'
      Origin = 'cnab_lei.Motivo4'
      Size = 2
    end
    object QrLeiMotivo5: TWideStringField
      FieldName = 'Motivo5'
      Origin = 'cnab_lei.Motivo5'
      Size = 2
    end
    object QrLeiQuitaData: TDateField
      FieldName = 'QuitaData'
      Origin = 'cnab_lei.QuitaData'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLeiDiretorio: TIntegerField
      FieldName = 'Diretorio'
      Origin = 'cnab_lei.Diretorio'
    end
    object QrLeiArquivo: TWideStringField
      FieldName = 'Arquivo'
      Origin = 'cnab_lei.Arquivo'
    end
    object QrLeiItemArq: TIntegerField
      FieldName = 'ItemArq'
      Origin = 'cnab_lei.ItemArq'
    end
    object QrLeiStep: TSmallintField
      FieldName = 'Step'
      Origin = 'cnab_lei.Step'
    end
    object QrLeiLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'cnab_lei.Lk'
    end
    object QrLeiDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'cnab_lei.DataCad'
    end
    object QrLeiDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'cnab_lei.DataAlt'
    end
    object QrLeiUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'cnab_lei.UserCad'
    end
    object QrLeiUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'cnab_lei.UserAlt'
    end
    object QrLeiEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'cnab_lei.Entidade'
    end
    object QrLeiNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrLeiCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'cnab_lei.Carteira'
      Required = True
    end
    object QrLeiTipoCart: TIntegerField
      FieldName = 'TipoCart'
      Origin = 'carteiras.Tipo'
      Required = True
    end
    object QrLeiDevJuros: TFloatField
      FieldName = 'DevJuros'
      Origin = 'cnab_lei.DevJuros'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiDevMulta: TFloatField
      FieldName = 'DevMulta'
      Origin = 'cnab_lei.DevMulta'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiDJM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DJM'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrLeiValJuMul: TFloatField
      FieldName = 'ValJuMul'
      Origin = 'cnab_lei.ValJuMul'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiValOutro: TFloatField
      FieldName = 'ValOutro'
      Origin = 'cnab_lei.ValOutro'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiValTarif: TFloatField
      FieldName = 'ValTarif'
      Origin = 'cnab_lei.ValTarif'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiNossoNum: TWideStringField
      FieldName = 'NossoNum'
      Origin = 'cnab_lei.NossoNum'
      Required = True
    end
    object QrLeiDtaTarif: TDateField
      FieldName = 'DtaTarif'
      Origin = 'cnab_lei.DtaTarif'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLeiDTA_TARIF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTA_TARIF_TXT'
      Size = 10
      Calculated = True
    end
    object QrLeiDescriCNR: TWideStringField
      FieldName = 'DescriCNR'
      Size = 100
    end
    object QrLeiTamReg: TIntegerField
      FieldName = 'TamReg'
      Required = True
    end
    object QrLeiID_Link: TLargeintField
      FieldName = 'ID_Link'
    end
  end
  object DsLei: TDataSource
    DataSet = QrLei
    Left = 77
    Top = 285
  end
  object DsLeiItens: TDataSource
    DataSet = QrLeiItens
    Left = 77
    Top = 313
  end
  object QrBcocor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Carrega'
      'FROM bancoslei'
      'WHERE Codigo=:P0'
      'AND Ocorrencia=:P1')
    Left = 321
    Top = 273
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrBcocorCarrega: TSmallintField
      FieldName = 'Carrega'
    end
  end
  object PMExclui: TPopupMenu
    Left = 532
    Top = 480
    object Ajustavaloresdobloquetoatual1: TMenuItem
      Caption = 
        ' Ajusta valores do bloqueto pois o banco informou juros/multa co' +
        'mo parte do valor do t'#237'tulo'
      OnClick = Ajustavaloresdobloquetoatual1Click
    end
    object CorrigeValTitulopoisobancoenviouerrado1: TMenuItem
      Caption = '&Corrige Val.Titulo pois o banco informou errado'
      OnClick = CorrigeValTitulopoisobancoenviouerrado1Click
    end
    object Excluso1: TMenuItem
      Caption = '&Exclus'#227'o'
      object ExcluiAtual1: TMenuItem
        Caption = 'Exclui &Atual'
        OnClick = ExcluiAtual1Click
      end
      object ExcluiSelecionados1: TMenuItem
        Caption = 'Exclui &Selecionados'
        OnClick = ExcluiSelecionados1Click
      end
      object ExcluiTodos1: TMenuItem
        Caption = 'Exclui &Todos'
        OnClick = ExcluiTodos1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object ExcluiBloquetosnolocalizados1: TMenuItem
        Caption = 'Exclui &Bloquetos n'#227'o localizados'
        OnClick = ExcluiBloquetosnolocalizados1Click
      end
    end
  end
  object PMConcilia: TPopupMenu
    Left = 36
    Top = 432
    object ConciliaAtual1: TMenuItem
      Caption = 'Concilia &Atual'
      OnClick = ConciliaAtual1Click
    end
    object ConciliaSelecionados1: TMenuItem
      Caption = 'Concilia &Selecionados'
      OnClick = ConciliaSelecionados1Click
    end
    object ConciliaTodos1: TMenuItem
      Caption = 'Concilia &Todos'
      OnClick = ConciliaTodos1Click
    end
  end
  object QrLocCta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT con.Mensal'
      'FROM contas con '
      'WHERE con.Codigo=:P0')
    Left = 392
    Top = 124
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocCtaMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
  end
  object QrTem: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(Codigo) Itens '
      'FROM cnab_lei'
      'WHERE Step=0')
    Left = 397
    Top = 281
    object QrTemItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrLEB: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Entidade'
      'FROM bancos'
      'WHERE Codigo=:P0')
    Left = 472
    Top = 92
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLEBEntidade: TIntegerField
      FieldName = 'Entidade'
    end
  end
  object QrLUH: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Apto, Propriet '
      'FROM arreits'
      'WHERE Boleto=:P0'
      ''
      'UNION'
      ''
      'SELECT DISTINCT Apto, Propriet '
      'FROM consits'
      'WHERE Boleto=:P1')
    Left = 492
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLUHApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrLUHPropriet: TIntegerField
      FieldName = 'Propriet'
      Required = True
    end
  end
  object QrPesq2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '')
    Left = 64
    Top = 120
    object QrPesq2Vencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrPesq2PercMulta: TFloatField
      FieldName = 'PercMulta'
    end
    object QrPesq2PercJuros: TFloatField
      FieldName = 'PercJuros'
    end
    object QrPesq2Credito: TFloatField
      FieldName = 'Credito'
    end
  end
  object QrBco: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cad.* '
      'FROM cnab_cad cad'
      'WHERE cad.Campo=:P0'
      ''
      ''
      ''
      ''
      '')
    Left = 609
    Top = 121
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBcoPadrIni: TIntegerField
      FieldName = 'PadrIni'
      Required = True
    end
    object QrBcoPadrTam: TIntegerField
      FieldName = 'PadrTam'
      Required = True
    end
    object QrBcoBcoOrig: TIntegerField
      FieldName = 'BcoOrig'
      Required = True
    end
  end
  object QrPesq3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '')
    Left = 92
    Top = 120
    object QrPesq3Credito: TFloatField
      FieldName = 'Credito'
    end
    object QrPesq3PercMulta: TFloatField
      FieldName = 'PercMulta'
    end
    object QrPesq3PercJuros: TFloatField
      FieldName = 'PercJuros'
    end
    object QrPesq3Vencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
  end
  object QrLocEnt2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE '
      'ent.Nome END NOMEENT, ent.Codigo CLIENTE, cnd.Codigo CliInt'
      'FROM cond cnd'
      'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente'
      ''
      'WHERE cnd.Banco=:P0'
      'AND cnd.Agencia=:P1'
      'AND cnd.CodCedente=:P2')
    Left = 800
    Top = 252
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrLocEnt2NOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrLocEnt2CLIENTE: TIntegerField
      FieldName = 'CLIENTE'
      Required = True
    end
    object QrLocEnt2CliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object PMItens: TPopupMenu
    Left = 668
    Top = 440
    object Alteravalordoitemdearrecadaoselecionado1: TMenuItem
      Caption = '&Altera valor do item de arrecada'#231#227'o selecionado'
      OnClick = Alteravalordoitemdearrecadaoselecionado1Click
    end
    object Excluioitemdearrecadaoselecionado1: TMenuItem
      Caption = '&Exclui o item de arrecada'#231#227'o selecionado'
      OnClick = Excluioitemdearrecadaoselecionado1Click
    end
  end
  object QrLeiAgr: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLeiAgrCalcFields
    SQL.Strings = (
      '')
    Left = 49
    Top = 341
    object QrLeiAgrNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
    object QrLeiAgrValor: TFloatField
      FieldName = 'Valor'
    end
    object QrLeiAgrData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLeiAgrMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrLeiAgrCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLeiAgrForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLeiAgrApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrLeiAgrUH: TWideStringField
      FieldName = 'UH'
      Size = 10
    end
    object QrLeiAgrTIPO_BLOQ: TLargeintField
      FieldName = 'TIPO_BLOQ'
      Required = True
    end
    object QrLeiAgrNOME_TIPO_BLOQ: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_BLOQ'
      Size = 30
      Calculated = True
    end
    object QrLeiAgrMez_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Mez_TXT'
      Size = 6
      Calculated = True
    end
  end
  object DsLeiAgr: TDataSource
    DataSet = QrLeiAgr
    Left = 77
    Top = 341
  end
  object DsCNAB_Dir: TDataSource
    DataSet = QrCNAB_Dir
    Left = 40
    Top = 12
  end
  object QrCNAB_Dir: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCNAB_DirAfterScroll
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMEENT,'
      'car.Nome NOMECARTEIRA, ban.Nome NOMEBANCO,'
      'car.Banco1, ent.CliInt, dir.Carteira,'
      'dir.Nome, dir.Codigo'
      'FROM cnab_dir dir'
      'LEFT JOIN carteiras car ON car.Codigo=dir.carteira'
      'LEFT JOIN bancos ban ON ban.Codigo=car.Banco1'
      'LEFT JOIN cond cnd ON cnd.Cliente=dir.CliInt'
      ''
      'LEFT JOIN entidades ent ON ent.Codigo=dir.CliInt'
      'WHERE dir.Envio=2'
      'AND dir.Ativo=1'
      'ORDER BY NOMEENT'
      '/*'
      'SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMEENT,'
      'car.Nome NOMECARTEIRA, ban.Nome NOMEBANCO,'
      'car.Banco1, ent.CliInt, dir.CartRetorno Carteira,'
      'dir.Nome, dir.Codigo'
      'FROM cnab_cfg dir'
      'LEFT JOIN carteiras car ON car.Codigo=dir.cartRetorno'
      'LEFT JOIN bancos ban ON ban.Codigo=car.Banco1'
      ''
      'LEFT JOIN entidades ent ON ent.Codigo=car.ForneceI'
      'WHERE dir.Ativo=1'
      'ORDER BY NOMEENT'
      '*/')
    Left = 12
    Top = 12
    object QrCNAB_DirNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Origin = 'NOMEENT'
      Size = 100
    end
    object QrCNAB_DirNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'cnab_cfg.Nome'
      Size = 50
    end
    object QrCNAB_DirCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'cnab_cfg.CartRetorno'
    end
    object QrCNAB_DirCliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'entidades.CliInt'
    end
    object QrCNAB_DirBanco1: TIntegerField
      FieldName = 'Banco1'
      Origin = 'carteiras.Banco1'
    end
    object QrCNAB_DirCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cnab_cfg.Codigo'
    end
    object QrCNAB_DirNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrCNAB_DirLayoutRem: TWideStringField
      FieldName = 'LayoutRem'
      Size = 50
    end
    object QrCNAB_DirNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Origin = 'bancos.Nome'
      Size = 100
    end
  end
  object QrLeiItens: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLeiItensCalcFields
    SQL.Strings = (
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    Left = 49
    Top = 313
    object QrLeiItensData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLeiItensCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrLeiItensGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLeiItensDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLeiItensCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLeiItensCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrLeiItensSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLeiItensVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLeiItensMez: TIntegerField
      FieldName = 'Mez'
      Required = True
      DisplayFormat = '0000'
    end
    object QrLeiItensFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLeiItensNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
    object QrLeiItensControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLeiItensSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrLeiItensTipoCart: TIntegerField
      FieldName = 'TipoCart'
      Required = True
    end
    object QrLeiItensDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrLeiItensNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrLeiItensSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrLeiItensDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrLeiItensCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLeiItensCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLeiItensForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLeiItensDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLeiItensApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrLeiItensUH: TWideStringField
      FieldName = 'UH'
      Size = 10
    end
    object QrLeiItensMez_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Mez_TXT'
      Size = 6
      Calculated = True
    end
    object QrLeiItensMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLeiItensCOND: TIntegerField
      FieldName = 'COND'
    end
    object QrLeiItensNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Size = 100
    end
    object QrLeiItensCartDest: TIntegerField
      FieldName = 'CartDest'
    end
    object QrLeiItensTipo: TSmallintField
      FieldName = 'Tipo'
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE '
      'ent.Nome END NOMEENT, ent.Codigo Entidade, cnd.Codigo CODIGO'
      'FROM cnab_lei lei'
      'LEFT JOIN entidades ent ON ent.Codigo=lei.Entidade'
      'LEFT JOIN cond      cnd ON cnd.Cliente=ent.Codigo'
      'WHERE Step=0'
      'GROUP BY lei.Entidade'
      'ORDER BY NOMEENT')
    Left = 217
    Top = 300
    object QrClientesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrClientesEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrClientesCODIGO: TIntegerField
      FieldName = 'CODIGO'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 245
    Top = 301
  end
  object QrLeiSum: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    Left = 108
    Top = 284
    object QrLeiSumCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLeiSumID600: TFloatField
      FieldName = 'ID600'
    end
    object QrLeiSumID601: TFloatField
      FieldName = 'ID601'
    end
    object QrLeiSumID610: TFloatField
      FieldName = 'ID610'
    end
  end
  object frxCrossObject1: TfrxCrossObject
    Left = 56
    Top = 28
  end
  object DataSource1: TDataSource
    DataSet = QrLeiSum
    Left = 136
    Top = 284
  end
  object PMBuffer: TPopupMenu
    OnPopup = PMBufferPopup
    Left = 296
    Top = 484
    object Limpabuffer1: TMenuItem
      Caption = '&Limpa buffer'
      OnClick = Limpabuffer1Click
    end
    object Adicionadados1: TMenuItem
      Caption = '&Adiciona dados'
      Enabled = False
      OnClick = Adicionadados1Click
    end
    object Imprimelista1: TMenuItem
      Caption = '&Imprime lista'
      Enabled = False
      object Ordenarpelonmerodobloqueto1: TMenuItem
        Caption = '&Ordenar pelo n'#250'mero do bloqueto'
        OnClick = Ordenarpelonmerodobloqueto1Click
      end
      object Ordenarpeladatadecrdito1: TMenuItem
        Caption = 'Ordenar pela data de cr'#233'dito'
        OnClick = Ordenarpeladatadecrdito1Click
      end
      object Ordenarpeladatadepagamento1: TMenuItem
        Caption = 'Ordenar pela data de pagamento'
        OnClick = Ordenarpeladatadepagamento1Click
      end
    end
  end
  object frxDsQuery: TfrxDBDataset
    UserName = 'frxDsQuery'
    CloseDataSource = False
    DataSet = Query
    BCDToCurrency = False
    Left = 596
    Top = 8
  end
  object frxGrade: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    EngineOptions.MaxMemSize = 10000000
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38006.786384259300000000
    ReportOptions.LastChange = 39872.778306180600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Visivel: Boolean;                                   '
      ''
      'begin'
      '  Visivel := <VARF_G1_VISIBLE>;                                '
      
        '  GroupHeader1.Visible := Visivel;                              ' +
        '                       '
      '  GroupFooter1.Visible := Visivel;'
      '  GroupHeader1.Condition := <VARF_G1_COND>;        '
      '  Memo27.Memo.Text := <VARF_G1_TEXT>;        '
      'end.')
    OnGetValue = frxGradeGetValue
    Left = 568
    Top = 8
    Datasets = <
      item
      end
      item
      end
      item
      end
      item
        DataSet = frxDsQuery
        DataSetName = 'frxDsQuery'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 30.236240000000000000
        Top = 408.189240000000000000
        Width = 680.315400000000000000
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 75.590600000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object frxShapeView1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Rela'#231#227'o de Bloquetos Liquidados')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxMemoView1: TfrxMemoView
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CONDOMINIO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 37.795300000000000000
          Top = 60.472480000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Bloqueto')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Left = 113.385900000000000000
          Top = 60.472480000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor t'#237'tulo')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 188.976500000000000000
          Top = 60.472480000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor pago')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 264.567100000000000000
          Top = 60.472480000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dta ocorr.')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 340.157700000000000000
          Top = 60.472480000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dta quita'#231#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 415.748300000000000000
          Top = 60.472480000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ocorr'#234'ncia')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Left = 464.882190000000000000
          Top = 60.472480000000000000
          Width = 215.433210000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Top = 60.472480000000000000
          Width = 37.795300000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 64.252010000000000000
        Top = 321.260050000000000000
        Width = 680.315400000000000000
        object Memo20: TfrxMemoView
          Left = 113.385900000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsQuery."ValTitul">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 188.976500000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsQuery."ValPago">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Top = 3.779530000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 264.567100000000000000
          Top = 3.779530000000000000
          Width = 415.748300000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 200.315090000000000000
        Width = 680.315400000000000000
        DataSet = frxDsQuery
        DataSetName = 'frxDsQuery'
        RowCount = 0
        object Memo1: TfrxMemoView
          Left = 37.795300000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataField = 'Bloqueto'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsQuery."Bloqueto"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo2: TfrxMemoView
          Left = 113.385900000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataField = 'ValTitul'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsQuery."ValTitul"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 188.976500000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataField = 'ValPago'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsQuery."ValPago"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 264.567100000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataField = 'DataOcorr'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          DisplayFormat.FormatStr = 'mm/dd/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsQuery."DataOcorr"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 340.157700000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataField = 'DataQuita'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          DisplayFormat.FormatStr = 'mm/dd/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsQuery."DataQuita"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 415.748300000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          DataField = 'OcorrCod'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsQuery."OcorrCod"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 464.882190000000000000
          Width = 215.433210000000000000
          Height = 15.118120000000000000
          DataField = 'OcorrTxt'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsQuery."OcorrTxt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Width = 37.795300000000000000
          Height = 15.118120000000000000
          DataField = 'SEQ'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsQuery."SEQ"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 154.960730000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsQuery."DataQuita"'
        object Memo27: TfrxMemoView
          Width = 676.535870000000000000
          Height = 22.677180000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsQuery."OcorrTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        object Memo28: TfrxMemoView
          Top = 3.779530000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total:  ')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          Left = 264.567100000000000000
          Top = 3.779530000000000000
          Width = 415.748300000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          Left = 113.385900000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsQuery."ValTitul">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 188.976500000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsQuery."ValPago">)]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object Query: TABSQuery
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    OnCalcFields = QueryCalcFields
    Left = 624
    Top = 8
    object QueryBloqueto: TIntegerField
      FieldName = 'Bloqueto'
    end
    object QueryValTitul: TFloatField
      FieldName = 'ValTitul'
    end
    object QueryValPago: TFloatField
      FieldName = 'ValPago'
    end
    object QueryValMulta: TFloatField
      FieldName = 'ValMulta'
    end
    object QueryValJuros: TFloatField
      FieldName = 'ValJuros'
    end
    object QueryValOutros: TFloatField
      FieldName = 'ValOutros'
    end
    object QueryValJurMul: TFloatField
      FieldName = 'ValJurMul'
    end
    object QueryValERRO: TFloatField
      FieldName = 'ValERRO'
    end
    object QueryValTarifa: TFloatField
      FieldName = 'ValTarifa'
    end
    object QueryOcorrCod: TWideStringField
      FieldName = 'OcorrCod'
      Size = 10
    end
    object QueryOcorrTxt: TWideStringField
      FieldName = 'OcorrTxt'
      Size = 255
    end
    object QueryAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QuerySEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QueryDataOcorr: TDateField
      FieldName = 'DataOcorr'
    end
    object QueryDataQuita: TDateField
      FieldName = 'DataQuita'
    end
    object QueryDataTarif: TDateField
      FieldName = 'DataTarif'
    end
  end
  object QrLocByCed: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF   '
      'FROM carteiras car'
      'LEFT JOIN entidades ent ON ent.Codigo=car.ForneceI  '
      'WHERE car.CodCedente = :P0'
      'AND car.Banco1=:P1 '
      '   ')
    Left = 220
    Top = 80
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocByCedCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
  end
  object QrLei3: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrLei3AfterScroll
    OnCalcFields = QrLei3CalcFields
    SQL.Strings = (
      'SELECT CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial'
      'ELSE cli.Nome END NOMECLI, loi.Codigo MeuLote, loi.Duplicata,'
      
        'loi.CPF, loi.Emitente, loi.Bruto, loi.Desco, loi.Quitado, loi.Va' +
        'lor, '
      'loi.Emissao, loi.DCompra, loi.Vencto, loi.DDeposito, loi.Dias, '
      'loi.Data3, loi.Cobranca, loi.Repassado, loi.Cliente,'
      'lot.Lote Bordero,  lot.Tipo TIPOLOTE, ban.Nome NOMEBANCO, cna.* '
      'FROM cnab_lei cna'
      'LEFT JOIN bancos ban ON ban.Codigo=cna.Banco'
      'LEFT JOIN lotesits loi ON loi.Controle=cna.ID_Link'
      'LEFT JOIN lotes     lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades cli ON cli.Codigo=loi.Cliente'
      'WHERE cna.Step=0')
    Left = 824
    Top = 416
    object QrLei3NOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrLei3MeuLote: TIntegerField
      FieldName = 'MeuLote'
    end
    object QrLei3Duplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrLei3CPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrLei3Emitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrLei3Bruto: TFloatField
      FieldName = 'Bruto'
    end
    object QrLei3Desco: TFloatField
      FieldName = 'Desco'
    end
    object QrLei3Quitado: TIntegerField
      FieldName = 'Quitado'
    end
    object QrLei3Valor: TFloatField
      FieldName = 'Valor'
    end
    object QrLei3Emissao: TDateField
      FieldName = 'Emissao'
    end
    object QrLei3DCompra: TDateField
      FieldName = 'DCompra'
    end
    object QrLei3Vencto: TDateField
      FieldName = 'Vencto'
    end
    object QrLei3DDeposito: TDateField
      FieldName = 'DDeposito'
    end
    object QrLei3Dias: TIntegerField
      FieldName = 'Dias'
    end
    object QrLei3Data3: TDateField
      FieldName = 'Data3'
    end
    object QrLei3Cobranca: TIntegerField
      FieldName = 'Cobranca'
    end
    object QrLei3Repassado: TSmallintField
      FieldName = 'Repassado'
    end
    object QrLei3Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLei3Bordero: TIntegerField
      FieldName = 'Bordero'
    end
    object QrLei3TIPOLOTE: TSmallintField
      FieldName = 'TIPOLOTE'
    end
    object QrLei3NOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
    object QrLei3Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLei3Banco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLei3NossoNum: TWideStringField
      FieldName = 'NossoNum'
    end
    object QrLei3SeuNum: TIntegerField
      FieldName = 'SeuNum'
    end
    object QrLei3IDNum: TIntegerField
      FieldName = 'IDNum'
    end
    object QrLei3OcorrCodi: TWideStringField
      FieldName = 'OcorrCodi'
      Size = 10
    end
    object QrLei3OcorrData: TDateField
      FieldName = 'OcorrData'
    end
    object QrLei3ValTitul: TFloatField
      FieldName = 'ValTitul'
    end
    object QrLei3ValAbati: TFloatField
      FieldName = 'ValAbati'
    end
    object QrLei3ValDesco: TFloatField
      FieldName = 'ValDesco'
    end
    object QrLei3ValPago: TFloatField
      FieldName = 'ValPago'
    end
    object QrLei3ValJuros: TFloatField
      FieldName = 'ValJuros'
    end
    object QrLei3ValMulta: TFloatField
      FieldName = 'ValMulta'
    end
    object QrLei3ValJuMul: TFloatField
      FieldName = 'ValJuMul'
    end
    object QrLei3Motivo1: TWideStringField
      FieldName = 'Motivo1'
      Size = 2
    end
    object QrLei3Motivo2: TWideStringField
      FieldName = 'Motivo2'
      Size = 2
    end
    object QrLei3Motivo3: TWideStringField
      FieldName = 'Motivo3'
      Size = 2
    end
    object QrLei3Motivo4: TWideStringField
      FieldName = 'Motivo4'
      Size = 2
    end
    object QrLei3Motivo5: TWideStringField
      FieldName = 'Motivo5'
      Size = 2
    end
    object QrLei3QuitaData: TDateField
      FieldName = 'QuitaData'
    end
    object QrLei3Diretorio: TIntegerField
      FieldName = 'Diretorio'
    end
    object QrLei3Arquivo: TWideStringField
      FieldName = 'Arquivo'
    end
    object QrLei3ItemArq: TIntegerField
      FieldName = 'ItemArq'
    end
    object QrLei3Step: TSmallintField
      FieldName = 'Step'
    end
    object QrLei3Entidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrLei3Carteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrLei3DevJuros: TFloatField
      FieldName = 'DevJuros'
    end
    object QrLei3DevMulta: TFloatField
      FieldName = 'DevMulta'
    end
    object QrLei3ValOutro: TFloatField
      FieldName = 'ValOutro'
    end
    object QrLei3ValTarif: TFloatField
      FieldName = 'ValTarif'
    end
    object QrLei3DtaTarif: TDateField
      FieldName = 'DtaTarif'
    end
    object QrLei3TamReg: TIntegerField
      FieldName = 'TamReg'
    end
    object QrLei3Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLei3DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLei3DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLei3UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLei3UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLei3AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrLei3Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrLei3DATA3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA3_TXT'
      Size = 25
      Calculated = True
    end
    object QrLei3CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrLei3NOMESTATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 100
      Calculated = True
    end
    object QrLei3NOMEENVIO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEENVIO'
      Size = 10
      Calculated = True
    end
    object QrLei3NOMEMOVIMENTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEMOVIMENTO'
      Size = 100
      Calculated = True
    end
    object QrLei3NOMEACAO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEACAO'
      Size = 3
      Calculated = True
    end
    object QrLei3NOMETIPOLOTE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPOLOTE'
      Calculated = True
    end
    object QrLei3ValCustas: TFloatField
      FieldName = 'ValCustas'
    end
    object QrLei3ID_Link: TLargeintField
      FieldName = 'ID_Link'
    end
    object QrLei3ValOutro2: TFloatField
      FieldName = 'ValOutro2'
    end
  end
  object DsLei3: TDataSource
    DataSet = QrLei3
    Left = 852
    Top = 416
  end
  object QrOcorreu3: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrOcorreu3CalcFields
    SQL.Strings = (
      'SELECT  lo.Cliente CLIENTELOTE, ob.Nome NOMEOCORRENCIA, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN lotesits li ON oc.LotesIts = li.Controle'
      'LEFT JOIN lotes lo ON lo.Codigo = li.Codigo'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.LotesIts=:P0'
      '')
    Left = 824
    Top = 444
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcorreu3NOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Size = 50
    end
    object QrOcorreu3Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOcorreu3LotesIts: TIntegerField
      FieldName = 'LotesIts'
      Required = True
    end
    object QrOcorreu3DataO: TDateField
      FieldName = 'DataO'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorreu3Ocorrencia: TIntegerField
      FieldName = 'Ocorrencia'
      Required = True
    end
    object QrOcorreu3Valor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorreu3LoteQuit: TIntegerField
      FieldName = 'LoteQuit'
      Required = True
    end
    object QrOcorreu3Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorreu3DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorreu3DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorreu3UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorreu3UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcorreu3TaxaP: TFloatField
      FieldName = 'TaxaP'
      Required = True
    end
    object QrOcorreu3TaxaV: TFloatField
      FieldName = 'TaxaV'
      Required = True
    end
    object QrOcorreu3Pago: TFloatField
      FieldName = 'Pago'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorreu3DataP: TDateField
      FieldName = 'DataP'
      Required = True
    end
    object QrOcorreu3TaxaB: TFloatField
      FieldName = 'TaxaB'
      Required = True
    end
    object QrOcorreu3ATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrOcorreu3CLIENTELOTE: TIntegerField
      FieldName = 'CLIENTELOTE'
    end
    object QrOcorreu3Data3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrOcorreu3Status: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrOcorreu3Cliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrOcorreu3SALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrOcorreu3ATZ_TEXTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ATZ_TEXTO'
      Size = 30
      Calculated = True
    end
  end
  object DsOcorreu3: TDataSource
    DataSet = QrOcorreu3
    Left = 852
    Top = 444
  end
  object QrOcorCli: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ocorcli'
      'WHERE Cliente=:P0'
      'AND Ocorrencia=:P1')
    Left = 824
    Top = 472
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrOcorCliBase: TFloatField
      FieldName = 'Base'
    end
    object QrOcorCliFormaCNAB: TSmallintField
      FieldName = 'FormaCNAB'
    end
  end
  object QrLocCliCreditor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Cliente'
      'FROM lotesits'
      'WHERE Controle=:P0')
    Left = 600
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocCliCreditorCliente: TIntegerField
      FieldName = 'Cliente'
    end
  end
  object PMMenu: TPopupMenu
    Left = 960
    Top = 368
    object LocalizarDuplicata1: TMenuItem
      Caption = 'Localizar &Duplicata'
      OnClick = LocalizarDuplicata1Click
    end
    object LocalizarLote1: TMenuItem
      Caption = 'Localizar &Lote'
      OnClick = LocalizarLote1Click
    end
  end
  object QrLocCreditor: TmySQLQuery
    Database = Dmod.MyDB
    Left = 628
    Top = 268
  end
end
