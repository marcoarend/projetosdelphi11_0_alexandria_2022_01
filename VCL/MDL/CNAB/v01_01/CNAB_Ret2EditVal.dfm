object FmCNAB_Res2EditVal: TFmCNAB_Res2EditVal
  Left = 0
  Top = 0
  Caption = 
    'RET-CNABx-004 :: Retorno de Arquivos CNAB - Ajusta valores do bl' +
    'oqueto'
  ClientHeight = 94
  ClientWidth = 335
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 0
    Width = 335
    Height = 46
    Align = alClient
    Caption = ' Avisos: '
    TabOrder = 0
    ExplicitLeft = -459
    ExplicitTop = 52
    ExplicitWidth = 1008
    ExplicitHeight = 44
    object Panel22: TPanel
      Left = 2
      Top = 15
      Width = 331
      Height = 29
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 1004
      ExplicitHeight = 27
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 60
        Height = 16
        Caption = 'Aguarde...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 60
        Height = 16
        Caption = 'Aguarde...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 46
    Width = 335
    Height = 48
    Align = alBottom
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 1
    ExplicitLeft = -459
    ExplicitTop = 117
    ExplicitWidth = 1008
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 172
      Height = 48
      Align = alLeft
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
    end
    object Panel3: TPanel
      Left = -457
      Top = 0
      Width = 792
      Height = 48
      Align = alRight
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      ExplicitLeft = 216
      object Panel2: TPanel
        Left = 691
        Top = 0
        Width = 101
        Height = 48
        Align = alRight
        Alignment = taRightJustify
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
          NumGlyphs = 2
        end
      end
    end
  end
end
