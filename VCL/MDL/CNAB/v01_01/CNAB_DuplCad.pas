unit CNAB_DuplCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DB, mySQLDbTables,
  dmkGeral, dmkImage, UnDmkEnums;

type
  TFmCNAB_DuplCad = class(TForm)
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    DsLocEnti1: TDataSource;
    mySQLQuery1: TmySQLQuery;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEntidade();
  public
    { Public declarations }
  end;

  var
  FmCNAB_DuplCad: TFmCNAB_DuplCad;

implementation

uses UnMyObjects, Entidade2, MyDBCheck;

{$R *.DFM}

procedure TFmCNAB_DuplCad.BtOKClick(Sender: TObject);
begin
  MostraEntidade();
end;

procedure TFmCNAB_DuplCad.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCNAB_DuplCad.DBGrid1DblClick(Sender: TObject);
begin
  MostraEntidade();
end;

procedure TFmCNAB_DuplCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCNAB_DuplCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmCNAB_DuplCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCNAB_DuplCad.MostraEntidade();
var
  Cliente: Integer;
begin
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    // N�o pode; � chamado de mais e um form (FmCNAB_Ret2 e FmCNAB_Ret2a)
    //FmEntidade2.LocCod(FmCNAB_Ret2.QrLocEnt1CLIENTE.Value,FmCNAB_Ret2.QrLocEnt1CLIENTE.Value);
    Cliente := TmySQLQuery(DsLocEnti1.DataSet).FieldByName('CLIENTE').AsInteger;
    FmEntidade2.LocCod(Cliente, Cliente);
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
  end;
end;

end.
