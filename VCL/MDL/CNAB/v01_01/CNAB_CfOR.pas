unit CNAB_CfOR;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmCNAB_CfOR = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    CBGenero: TdmkDBLookupComboBox;
    EdGenero: TdmkEditCB;
    Label1: TLabel;
    SbGenero: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DsContas: TDataSource;
    Label2: TLabel;
    EdOcorrencia: TdmkEdit;
    Label4: TLabel;
    EdNomeOcorrencia: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbGeneroClick(Sender: TObject);
    procedure EdOcorrenciaChange(Sender: TObject);
    procedure EdNomeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    procedure ReopenCNAB_CfOR(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    FBanco, FTamReg: Integer;
    FRegistrado: Boolean;
  end;

  var
  FmCNAB_CfOR: TFmCNAB_CfOR;

implementation

uses
  {$IfNDef NO_FINANCEIRO} Contas, {$EndIf}
  UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnBancos, UnDmkProcFunc;

{$R *.DFM}

procedure TFmCNAB_CfOR.BtOKClick(Sender: TObject);
var
  Nome: String;
  Codigo, Controle, Ocorrencia, Genero: Integer;
  Qry: TmySQLQuery;
begin
  Nome       := EdNome.Text;
  Ocorrencia := EdOcorrencia.ValueVariant;
  Genero     := EdGenero.ValueVariant;
  Codigo     := Geral.IMV(DBEdCodigo.Text);
  Controle   := EdControle.ValueVariant;
  //
  if MyObjects.FIC(Trim(Nome) = '', EdNome, 'Informe a "Minha Descri��o"!') then
    Exit;
  if MyObjects.FIC(Ocorrencia = 0, EdOcorrencia, 'Informe a ocorr�ncia banc�ria!') then
    Exit;
  if MyObjects.FIC(Genero = 0, EdGenero, 'Informe a conta do plano de contas!') then
    Exit;
  // Duplicacao
  if ImgTipo.SQLType = stIns then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Controle ',
      'FROM cnab_cfor ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      'AND Ocorrencia=' + Geral.FF0(Ocorrencia),
      '']);
      if MyObjects.FIC(Qry.RecordCount > 0, EdGenero,
        'Ocorr�ncia j� inclu�da nesta configura��o CNAB!') then Exit;
    finally
      Qry.Free;
    end;
  end;
  // FIM Duplicacao
  Controle := UMyMod.BPGS1I32('cnab_cfor', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'cnab_cfor', False,
    ['Codigo', 'Nome', 'Ocorrencia', 'Genero'], ['Controle'],
    [Codigo, Nome, Ocorrencia, Genero], [Controle], True) then
  begin
    ReopenCNAB_CfOR(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdOcorrencia.ValueVariant  := 0;
      EdControle.ValueVariant    := 0;
      EdGenero.ValueVariant      := 0;
      CBGenero.KeyValue          := Null;
      EdNome.ValueVariant        := '';
      EdOcorrencia.SetFocus;
    end else Close;
  end;
end;

procedure TFmCNAB_CfOR.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCNAB_CfOR.EdNomeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_F4 then
    EdNome.ValueVariant := QrContasNome.Value;
end;

procedure TFmCNAB_CfOR.EdOcorrenciaChange(Sender: TObject);
var
  Movimento: Integer;
begin
  Movimento             := EdOcorrencia.ValueVariant;
  EdNomeOcorrencia.Text := UBancos.CNABTipoDeMovimento(FBanco,
                              ecnabRetorno, Movimento, FTamReg, FRegistrado,
                              FQrCab.FieldByName('LayoutRem').Value);
end;

procedure TFmCNAB_CfOR.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmCNAB_CfOR.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
end;

procedure TFmCNAB_CfOR.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCNAB_CfOR.ReopenCNAB_CfOR(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger :=
      FQrCab.FieldByName('Codigo').AsInteger;
      UnDmkDAC_PF.AbreQueryApenas(FQrIts);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmCNAB_CfOR.SbGeneroClick(Sender: TObject);
{$IfNDef NO_FINANCEIRO}
var
  Genero: Integer;
begin
  VAR_CADASTRO := 0;
  Genero       := EdGenero.ValueVariant;
  //
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    FmContas.LocCod(Genero, Genero);
    FmContas.ShowModal;
    FmContas.Destroy;
  end;
  UMyMod.SetaCodigoPesquisado(EdGenero, CBGenero, QrContas, VAR_CADASTRO);
    EdGenero.SetFocus;
{$Else}
begin
  Grl_Geral.InfoSemModulo(mdlappFinanceiro);
{$EndIf}
end;

end.
