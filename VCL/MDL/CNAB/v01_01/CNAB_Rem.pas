unit CNAB_Rem;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, dmkEdit, dmkLabel, Db, mySQLDbTables,
  Grids, RichEdit, dmkMemoBar, CheckLst, Mask, DBCtrls, dmkEditDateTimePicker,
  dmkGeral, dmkImage, UnDmkProcFunc, UnDmkEnums, DmkDAC_PF, UnProjGroup_Consts;

const
  MinColsArr = -2; MaxColsArr = 1000;

type
  dceAlinha = (posEsquerda, posCentro, posDireita);
  TArrayCols = array[MinColsArr..MaxColsArr] of Integer;
  TFmCNAB_Rem = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    TabSheet9: TTabSheet;
    Grade_5: TStringGrid;
    Panel9: TPanel;
    Panel5: TPanel;
    Label10: TLabel;
    LaCodDmk_1: TLabel;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    dmkEd_0_000: TdmkEdit;
    dmkEd_0_005: TdmkEdit;
    dmkEd_0_006: TdmkEdit;
    dmkEd_0_003: TdmkEdit;
    dmkEd_0_004: TdmkEdit;
    dmkEd_0_402: TdmkEdit;
    dmkEd_0_990: TdmkEdit;
    dmkEd_0_410: TdmkEdit;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    dmkEd_0_020: TdmkEdit;
    dmkEd_0_021: TdmkEdit;
    dmkEd_0_022: TdmkEdit;
    dmkEd_0_023: TdmkEdit;
    dmkEd_0_024: TdmkEdit;
    dmkEdNomeBanco: TdmkEdit;
    dmkEd_0_001: TdmkEdit;
    Panel6: TPanel;
    Panel10: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit6: TEdit;
    PageControl2: TPageControl;
    TabSheet10: TTabSheet;
    Panel11: TPanel;
    Panel8: TPanel;
    Label15: TLabel;
    EdArqGerado: TEdit;
    MeGerado: TMemo;
    dmkMBGravado: TdmkMemoBar;
    TabSheet11: TTabSheet;
    Panel7: TPanel;
    Panel12: TPanel;
    Label28: TLabel;
    SpeedButton1: TSpeedButton;
    EdArqCompar: TEdit;
    MeCompar: TMemo;
    dmkMBCompar: TdmkMemoBar;
    OpenDialog1: TOpenDialog;
    TabSheet7: TTabSheet;
    Panel13: TPanel;
    GradeG: TStringGrid;
    GradeO: TStringGrid;
    GradeC: TStringGrid;
    Panel17: TPanel;
    Panel18: TPanel;
    BitBtn1: TBitBtn;
    Panel19: TPanel;
    dmkEdVerifLin: TdmkEdit;
    Label16: TLabel;
    dmkEdVerifCol: TdmkEdit;
    Label17: TLabel;
    GradeI: TStringGrid;
    GradeF: TStringGrid;
    Label18: TLabel;
    Label19: TLabel;
    dmkEdVerifValCompar: TdmkEdit;
    dmkEdVerifCampoCod: TdmkEdit;
    Label20: TLabel;
    dmkEdVerifValGerado: TdmkEdit;
    dmkEdVerifCampoTxt: TdmkEdit;
    Label21: TLabel;
    dmkEd_0_699: TdmkEdit;
    dmkEdVerifPos: TdmkEdit;
    Label22: TLabel;
    Label23: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    PB1: TProgressBar;
    Label24: TLabel;
    dmkEd_0_037: TdmkEdit;
    Label25: TLabel;
    Panel15: TPanel;
    Panel16: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel20: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel21: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    Label26: TLabel;
    dmkEd_0_891: TdmkEdit;
    dmkEd_9_302: TdmkEdit;
    Label27: TLabel;
    Grade_2: TStringGrid;
    PageControl3: TPageControl;
    TabSheet8: TTabSheet;
    Grade_1: TStringGrid;
    TabSheet12: TTabSheet;
    PageControl4: TPageControl;
    TabSheet13: TTabSheet;
    TabSheet14: TTabSheet;
    Grade_240_3_P: TStringGrid;
    Grade_240_3_Q: TStringGrid;
    BtMover: TBitBtn;
    dmkEd_0_991: TdmkEdit;
    Label29: TLabel;
    Label30: TLabel;
    dmkEd_0_801: TdmkEdit;
    Label31: TLabel;
    dmkEd_0_802: TdmkEdit;
    TabSheet15: TTabSheet;
    Grade_240_3_R: TStringGrid;
    TabSheet16: TTabSheet;
    Grade_240_3_S: TStringGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure dmkEd_0_001Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure Grade_1SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure Edit2Change(Sender: TObject);
    procedure Edit3Change(Sender: TObject);
    procedure Edit4Change(Sender: TObject);
    procedure MeGeradoEnter(Sender: TObject);
    procedure MeGeradoExit(Sender: TObject);
    procedure MeComparEnter(Sender: TObject);
    procedure MeComparExit(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure GradeGDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure GradeGSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure PageControl2Change(Sender: TObject);
    procedure Grade_240_3_PSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure Grade_240_3_QSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure PageControl1Change(Sender: TObject);
    procedure BtMoverClick(Sender: TObject);
    procedure Grade_240_3_RSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure Grade_240_3_SSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
  private
    { Private declarations }
    FCols1, FCols2, FCols3P, FCols3Q, FCols3R, FCols3S, FCols5: TArrayCols;
    FLinArq, FLinArq2: Integer;
    //FValTitulos: Double;
    FEditor: TMemo;
    procedure PesquisaTexto;
    function  NovaLinhaArq(): String;
    function  NovaLinhaArq2(): String;
    function  Incrementa(const Grade: TStringGrid; var Contador: Integer;
              const Incremento: Integer; var ListaCols: TArrayCols;
              const MyCodField: Integer): Integer;
    procedure ConfGr(Grade: TStringGrid; Coluna, Tam: Integer;
              Titulo: String);
    function  CriaListaCNAB(const CNAB, Banco, Registro, Posicoes: Integer;
              Segmento: Char; var Lista: MyArrayLista; const NomeLayout: String;
              const SubSegmento: Integer = 0): Boolean;
    procedure VerificaCelula(Col, Row: Integer);
    function  GetCNAB(): Integer;
  public
    { Public declarations }
    //LCCod: array[0..1000] of byte;
    //LCNome: array[0..1000] of String;
    //
    FLote, FSeqArqRem, FBanco: Integer;
    FNomeLayout: String;
    FColG1, FColG2, FColG3P, FColG3Q, FColG3R, FColG3S, FColG5,
    FLinG1, FLinG2, FLinG3P, FLinG3Q, FLinG3R, FLinG3S, FLinG5,
    F1_000, F1_001, F1_003, F1_011, F1_020, F1_021, F1_022, F1_023, F1_024,
    F1_032, F1_033, F1_034, F1_035,  F1_037, F1_038,
    F1_211, F1_212,
    F1_400, F1_401, F1_402, F1_408,
    F1_501, F1_502, F1_504, F1_506, F1_507, F1_508, F1_509, F1_511, F1_512,
    F1_513, F1_514, F1_515, F1_516,
    F1_520, F1_549, F1_550, F1_551, F1_552, F1_558,
    F1_569, F1_572, F1_573, F1_574, F1_575, F1_576,
    F1_577, F1_580, F1_581, F1_583, F1_584, F1_586, F1_587,
    F1_591, F1_592, F1_593, F1_594, F1_595, F1_596, F1_597, F1_598, F1_599,
    F1_621, F1_627, F1_639, F1_640, F1_643, F1_647, F1_650, F1_410, F1_665, F1_667,
    F1_701, F1_702, F1_711, F1_712, F1_720,
    F1_801, F1_802, F1_803, F1_804, F1_805, F1_806, F1_807, F1_808, F1_811,
    F1_812, F1_813, F1_814, F1_851, F1_852, F1_853,
    F1_891,
    F1_950,
    //
    F2_000, F2_011, F2_201, F2_202, F2_203, F2_204, F2_508,
    //
    F3P_000, F3P_001, F3P_003, F3P_011, F3P_019, F3P_020, F3P_021, F3P_022,
    F3P_023, F3P_024, F3P_036, F3P_501, F3P_502, F3P_504, F3P_506,
    F3P_507, F3P_508, F3P_509, F3P_520, F3P_549, F3P_550, F3P_551, F3P_569, F3P_572,
    F3P_574, F3P_576, F3P_580, F3P_583, F3P_584, F3P_591, F3P_592, F3P_593, F3P_621,
    F3P_647, F3P_651, F3P_654, F3P_513,
    //
    F3Q_000, F3Q_001, F3Q_003, F3Q_011, F3Q_019, F3Q_081, F3Q_504, F3Q_801,
    F3Q_802, F3Q_803, F3Q_804, F3Q_805, F3Q_806, F3Q_807, F3Q_808, F3Q_851,
    F3Q_852, F3Q_853,
    //
    F3R_000, F3R_001, F3R_003, F3R_019, F3R_594, F3R_595, F3R_596,
    F3R_597, F3R_598, F3R_599, F3R_577, F3R_587, F3R_573, F3R_213, F3R_214,
    //
    F3S_000, F3S_001, F3S_003, F3S_019, F3S_197, F3S_198, F3S_199, F3S_200,
    F3S_205, F3S_206, F3S_207, F3S_208, F3S_209,
    //
    F5_000, F5_001, F5_003, F5_011,
    F5_815, F5_850, F5_851, F5_852, F5_854, F5_855, F5_856, F5_857, F5_858,
    F5_861, F5_862, F5_863, F5_864, F5_992: Integer;
    //
    function  AddLinha(var Contador: Integer; const Grade: TStringGrid): Integer;
    procedure AddCampo(Grade: TStringGrid; Linha, Coluna: Integer;
              Formato: String; Valor: Variant);
    function  AjustaString(const Texto, Compl: String; const Tamanho:
              Integer; const Ajusta: Boolean; var Res: String): Boolean;
    function  ObtemNumeroDoArquivo(var Res: String): Boolean;
    function  ObtemSubNumeroDoArquivo(var Res: String; var DataLote: TDateTime): Boolean;
    function  DAC_NossoNumero(const Bloqueto: Double; var Res: String): Boolean;
{
    function  ConcatenaAgenciaEConta(const Agencia, Conta: String;
              const Len: Integer; var AgCC: String): Boolean;
}
  end;

const
   _Ini = 0; _Fim = 1; _Tam=2; _Fil = 3; _Typ = 4; _Fmt = 5;
   _Fld = 6; _Def = 7; _Nom = 8; _Des = 9; _AjT = 10;

  var
  FmCNAB_Rem: TFmCNAB_Rem;

implementation

uses UnMyObjects, ModuleBco, UnBancos, UnBco_rem, Module, MyListas;

{$R *.DFM}

procedure TFmCNAB_Rem.PesquisaTexto;
var
  Lin, Col, Tam: Integer;
begin
  case PageControl2.ActivePageIndex of
    0: FEditor := MeGerado;
    1: FEditor := MeCompar;
    else FEditor := nil;
  end;
  if FEditor = nil then Exit;
  Lin := Geral.IMV(Edit2.Text);
  Col := Geral.IMV(Edit3.Text);
  Tam := Geral.IMV(Edit4.Text);
  //
  if (Lin > 0) and (Col > 0) and (Tam > 0) then
  begin
    Edit6.Text := Copy(FEditor.Lines[Lin-1], Col, Tam);
  end else Edit6.Text := '';
end;

function TFmCNAB_Rem.AjustaString(const Texto, Compl: String;
const Tamanho: Integer; const Ajusta: Boolean; var Res: String): Boolean;
  function CompletaString(Texto, Compl: String; Tamanho: Integer;
    Alinhamento: dceAlinha): String;
  var
    Txt: String;
    Direita: Boolean;
  begin
    Direita := True;
    Txt := Texto;
    while Length(Txt) < Tamanho do
    begin
      case Alinhamento of
        posEsquerda: Txt := Txt + Compl;
        posCentro  :
        begin
          if Direita then
            Txt := Txt + Compl
          else
            Txt := Compl + Txt;
          Direita := not Direita;
        end;
        posDireita:  Txt := Compl + Txt;
      end;
    end;
    Result := Txt;
  end;
var
  Txt: String;
  Direita: Boolean;
  Alinhamento: dceAlinha;
  //Data: TDateTime;
begin
  //Result := False;
  Direita := True;
  if Compl = '0' then
    Alinhamento := posDireita
  else
    Alinhamento := posEsquerda;
  Txt := CompletaString(Texto, Compl, Tamanho, Alinhamento);
  if Ajusta then
  begin
    while Length(Txt) > Tamanho do
    begin
      case Alinhamento of
        posEsquerda: Txt := Copy(Txt, 1, Length(Txt)-1);
        posCentro  :
        begin
          if Direita then
            Txt := Copy(Txt, 2, Length(Txt)-1)
          else
            Txt := Copy(Txt, 1, Length(Txt)-1);
          Direita := not Direita;
        end;
        posDireita: Txt := Copy(Txt, 2, Length(Txt)-1);
      end;
    end;
  end;
  Res := Txt;
  Result := Length(Txt) = Tamanho;
end;

procedure TFmCNAB_Rem.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCNAB_Rem.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCNAB_Rem.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCNAB_Rem.dmkEd_0_001Change(Sender: TObject);
begin
  DmBco.QrBco.Close;
  DmBco.QrBco.Params[0].AsInteger := dmkEd_0_001.ValueVariant;
  DmBco.QrBco.Open;
  //
  dmkEdNomeBanco.Text := DmBco.QrBcoNome.Value;
end;

procedure TFmCNAB_Rem.FormCreate(Sender: TObject);

  function OrdenaColunas(var Colunas: array of Integer): Integer;
  var
    i, k: integer;
  begin
    k := 0;
    for i := Low(Colunas) to High(Colunas) do
    begin
      Colunas[i] := k;
      inc(k, 1);
    end;
    Result := k;
  end;

var
  i: Integer;
begin
  ImgTipo.SQLType := stLok;
  BtMover.Visible := False;
  // N�o aparecer nada preto
  GradeG.ColCount := 0;
  GradeG.RowCount := 0;
  //
  PageControl1.ActivePageIndex := 1;
  //
  for i := 0 to MaxColsArr do
  begin
    FCols1[i]  := 0; // Coluna 0
    FCols2[i]  := 0; // Coluna 0
    FCols3P[i] := 0; // Coluna 0
    FCols3Q[i] := 0; // Coluna 0
    FCols3R[i] := 0; // Coluna 0
    FCols3S[i] := 0; // Coluna 0
    FCols5[i]  := 0; // Coluna 0
  end;
  FLinG3P := 0;
  FColG3P := 0;
  F3P_001 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 001);
  F3P_003 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 003);
  F3P_011 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 011);
  F3P_019 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 019);
  F3P_020 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 020);
  F3P_022 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 022);
  F3P_021 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 021);
  F3P_023 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 023);
  F3P_024 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 024);
  F3P_036 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 036);
  F3P_501 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 501);
  F3P_507 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 507);
  F3P_508 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 508);
  F3P_509 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 509);
  F3P_621 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 621);
  F3P_502 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 502);
  F3P_504 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 504);
  F3P_580 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 580);
  F3P_550 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 550);
  F3P_520 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 520);
  F3P_583 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 583);
  F3P_584 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 584);
  F3P_572 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 572);
  F3P_569 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 569);
  F3P_576 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 576);
  F3P_574 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 574);
  F3P_591 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 591);
  F3P_592 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 592);
  F3P_593 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 593);
  F3P_551 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 551);
  F3P_506 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 506);
  F3P_647 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 647);
  F3P_651 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 651);
  F3P_654 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 654);
  F3P_549 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 549);
  F3P_513 := Incrementa(Grade_240_3_P, FColG3P, 1, FCols3P, 513);
  //
  ConfGr(Grade_240_3_P, F3P_000, 032, 'Item');
  ConfGr(Grade_240_3_P, F3P_001, 032, 'Banco');
  ConfGr(Grade_240_3_P, F3P_003, 032, 'Lote de servi�o');
  ConfGr(Grade_240_3_P, F3P_011, 018, 'TR');
  ConfGr(Grade_240_3_P, F3P_019, 072, 'C�digo de movimento');
  ConfGr(Grade_240_3_P, F3P_020, 032, 'Ag�ncia');
  ConfGr(Grade_240_3_P, F3P_022, 018, 'DV ag�ncia');
  ConfGr(Grade_240_3_P, F3P_021, 072, 'N�mero conta');
  ConfGr(Grade_240_3_P, F3P_023, 072, 'DV conta');
  ConfGr(Grade_240_3_P, F3P_024, 072, 'DV Ag�ncia/Conta');
  ConfGr(Grade_240_3_P, F3P_036, 032, 'Emiss�o boleto');
  ConfGr(Grade_240_3_P, F3P_501, 056, 'Nosso n�mero');
  ConfGr(Grade_240_3_P, F3P_507, 024, 'Tip.Doc');
  ConfGr(Grade_240_3_P, F3P_508, 024, 'Carteira cobran�a');
  ConfGr(Grade_240_3_P, F3P_509, 024, 'Carteira');
  ConfGr(Grade_240_3_P, F3P_621, 018, 'Quem imprime?');
  ConfGr(Grade_240_3_P, F3P_502, 072, 'Duplicata');
  ConfGr(Grade_240_3_P, F3P_504, 024, 'Ocorrencia');
  ConfGr(Grade_240_3_P, F3P_580, 056, 'Vencto');
  ConfGr(Grade_240_3_P, F3P_550, 080, 'Valor doc.');
  ConfGr(Grade_240_3_P, F3P_520, 012, 'Aceite');
  ConfGr(Grade_240_3_P, F3P_583, 056, 'Emiss�o');
  ConfGr(Grade_240_3_P, F3P_584, 056, 'Data inicio cobran�a mora');
  ConfGr(Grade_240_3_P, F3P_572, 056, 'Valor mora/dia');
  ConfGr(Grade_240_3_P, F3P_569, 056, 'Valor IOF');
  ConfGr(Grade_240_3_P, F3P_576, 024, 'T.Mora');
  ConfGr(Grade_240_3_P, F3P_574, 080, '% mora dd');
  ConfGr(Grade_240_3_P, F3P_591, 080, 'C�d. desc. 1');
  ConfGr(Grade_240_3_P, F3P_592, 056, 'Dta.desc.1');
  ConfGr(Grade_240_3_P, F3P_593, 064, 'Val.desc.1');
  ConfGr(Grade_240_3_P, F3P_551, 064, 'Val.Abatimento');
  ConfGr(Grade_240_3_P, F3P_506, 072, 'Seu n�mero');
  ConfGr(Grade_240_3_P, F3P_647, 024, 'DDP Protesto em xx dias ap�s vencimento');
  ConfGr(Grade_240_3_P, F3P_651, 064, 'C�d.Protesto');
  ConfGr(Grade_240_3_P, F3P_654, 064, 'N� Con. Oper.');
  ConfGr(Grade_240_3_P, F3P_549, 012, 'Moeda');
  ConfGr(Grade_240_3_P, F3P_513, 012, 'Parcela');
  //
  FLinG3Q := 0;
  FColG3Q := 0;
  F3Q_001 := Incrementa(Grade_240_3_Q, FColG3Q, 1, FCols3Q, 001);
  F3Q_003 := Incrementa(Grade_240_3_Q, FColG3Q, 1, FCols3Q, 003);
  F3Q_011 := Incrementa(Grade_240_3_Q, FColG3Q, 1, FCols3Q, 011);
  F3Q_019 := Incrementa(Grade_240_3_Q, FColG3Q, 1, FCols3Q, 019);
  F3Q_081 := Incrementa(Grade_240_3_Q, FColG3Q, 1, FCols3Q, 081);
  F3Q_504 := Incrementa(Grade_240_3_Q, FColG3Q, 1, FCols3Q, 504);
  F3Q_801 := Incrementa(Grade_240_3_Q, FColG3Q, 1, FCols3Q, 801);
  F3Q_802 := Incrementa(Grade_240_3_Q, FColG3Q, 1, FCols3Q, 802);
  F3Q_803 := Incrementa(Grade_240_3_Q, FColG3Q, 1, FCols3Q, 803);
  F3Q_804 := Incrementa(Grade_240_3_Q, FColG3Q, 1, FCols3Q, 804);
  F3Q_805 := Incrementa(Grade_240_3_Q, FColG3Q, 1, FCols3Q, 805);
  F3Q_806 := Incrementa(Grade_240_3_Q, FColG3Q, 1, FCols3Q, 806);
  F3Q_807 := Incrementa(Grade_240_3_Q, FColG3Q, 1, FCols3Q, 807);
  F3Q_808 := Incrementa(Grade_240_3_Q, FColG3Q, 1, FCols3Q, 808);
  F3Q_851 := Incrementa(Grade_240_3_Q, FColG3Q, 1, FCols3Q, 851);
  F3Q_852 := Incrementa(Grade_240_3_Q, FColG3Q, 1, FCols3Q, 852);
  F3Q_853 := Incrementa(Grade_240_3_Q, FColG3Q, 1, FCols3Q, 853);
  //
  ConfGr(Grade_240_3_Q, F3Q_000, 032, 'Item');
  ConfGr(Grade_240_3_Q, F3Q_001, 032, 'Banco');
  ConfGr(Grade_240_3_Q, F3Q_003, 032, 'Lote de servi�o');
  ConfGr(Grade_240_3_Q, F3Q_011, 018, 'TR');
  ConfGr(Grade_240_3_Q, F3Q_019, 019, 'C�digo de movimento');
  ConfGr(Grade_240_3_Q, F3Q_081, 018, 'Seg. Reg.');
  ConfGr(Grade_240_3_Q, F3Q_504, 024, 'Ocorrencia');
  ConfGr(Grade_240_3_Q, F3Q_801, 020, 'TIS - Tipo inscr. sacado');
  ConfGr(Grade_240_3_Q, F3Q_802, 112, 'Inscr. Sacado');
  ConfGr(Grade_240_3_Q, F3Q_803, 210, 'Nome do Sacado');
  ConfGr(Grade_240_3_Q, F3Q_804, 280, 'Logradouro completo');
  ConfGr(Grade_240_3_Q, F3Q_805, 084, 'Bairro');
  ConfGr(Grade_240_3_Q, F3Q_806, 064, 'CEP');
  ConfGr(Grade_240_3_Q, F3Q_807, 108, 'Cidade');
  ConfGr(Grade_240_3_Q, F3Q_808, 020, 'UF');
  ConfGr(Grade_240_3_Q, F3Q_851, 020, 'Doc');
  ConfGr(Grade_240_3_Q, F3Q_852, 112, 'CNPJ / CPF sac/aval.');
  ConfGr(Grade_240_3_Q, F3Q_853, 210, 'Nome do Sacador / avalista');
  //
  FLinG3R := 0;
  FColG3R := 0;
  F3R_001 := Incrementa(Grade_240_3_R, FColG3R, 1, FCols3R, 001);
  F3R_003 := Incrementa(Grade_240_3_R, FColG3R, 1, FCols3R, 003);
  F3R_019 := Incrementa(Grade_240_3_R, FColG3R, 1, FCols3R, 019);
  F3R_213 := Incrementa(Grade_240_3_R, FColG3R, 1, FCols3R, 213);
  F3R_214 := Incrementa(Grade_240_3_R, FColG3R, 1, FCols3R, 214);
  F3R_573 := Incrementa(Grade_240_3_R, FColG3R, 1, FCols3R, 573);
  F3R_577 := Incrementa(Grade_240_3_R, FColG3R, 1, FCols3R, 577);
  F3R_587 := Incrementa(Grade_240_3_R, FColG3R, 1, FCols3R, 587);
  F3R_594 := Incrementa(Grade_240_3_R, FColG3R, 1, FCols3R, 594);
  F3R_595 := Incrementa(Grade_240_3_R, FColG3R, 1, FCols3R, 595);
  F3R_596 := Incrementa(Grade_240_3_R, FColG3R, 1, FCols3R, 596);
  F3R_597 := Incrementa(Grade_240_3_R, FColG3R, 1, FCols3R, 597);
  F3R_598 := Incrementa(Grade_240_3_R, FColG3R, 1, FCols3R, 598);
  F3R_599 := Incrementa(Grade_240_3_R, FColG3R, 1, FCols3R, 599);
  //
  ConfGr(Grade_240_3_R, F3R_000, 032, 'Item');
  ConfGr(Grade_240_3_R, F3R_001, 032, 'Banco');
  ConfGr(Grade_240_3_R, F3R_003, 032, 'Lote de servi�o');
  ConfGr(Grade_240_3_R, F3R_019, 032, 'C�digo de movimento');
  ConfGr(Grade_240_3_R, F3R_213, 210, 'Mensagem 3');
  ConfGr(Grade_240_3_R, F3R_214, 210, 'Mensagem 4');
  ConfGr(Grade_240_3_R, F3R_573, 080, 'Valor/% multa');
  ConfGr(Grade_240_3_R, F3R_577, 032, 'C�digo da multa');
  ConfGr(Grade_240_3_R, F3R_587, 080, 'Data cobra multa');
  ConfGr(Grade_240_3_R, F3R_594, 032, 'C�digo do desconto 2');
  ConfGr(Grade_240_3_R, F3R_595, 080, 'Data do desconto 2');
  ConfGr(Grade_240_3_R, F3R_596, 080, 'Val/% do desconto 2');
  ConfGr(Grade_240_3_R, F3R_597, 032, 'C�digo do desconto 3');
  ConfGr(Grade_240_3_R, F3R_598, 080, 'Data do desconto 3');
  ConfGr(Grade_240_3_R, F3R_599, 080, 'Val/% do desconto 3');
  //
  FLinG3S := 0;
  FColG3S := 0;
  F3S_001 := Incrementa(Grade_240_3_S, FColG3S, 1, FCols3S, 001);
  F3S_003 := Incrementa(Grade_240_3_S, FColG3S, 1, FCols3S, 003);
  F3S_019 := Incrementa(Grade_240_3_S, FColG3S, 1, FCols3S, 019);
  F3S_197 := Incrementa(Grade_240_3_S, FColG3S, 1, FCols3S, 197);
  F3S_198 := Incrementa(Grade_240_3_S, FColG3S, 1, FCols3S, 198);
  F3S_199 := Incrementa(Grade_240_3_S, FColG3S, 1, FCols3S, 199);
  F3S_200 := Incrementa(Grade_240_3_S, FColG3S, 1, FCols3S, 200);
  F3S_205 := Incrementa(Grade_240_3_S, FColG3S, 1, FCols3S, 205);
  F3S_206 := Incrementa(Grade_240_3_S, FColG3S, 1, FCols3S, 206);
  F3S_207 := Incrementa(Grade_240_3_S, FColG3S, 1, FCols3S, 207);
  F3S_208 := Incrementa(Grade_240_3_S, FColG3S, 1, FCols3S, 208);
  F3S_209 := Incrementa(Grade_240_3_S, FColG3S, 1, FCols3S, 209);
  //
  ConfGr(Grade_240_3_S, F3S_000, 032, 'Item');
  ConfGr(Grade_240_3_S, F3S_001, 032, 'Banco');
  ConfGr(Grade_240_3_S, F3S_003, 032, 'Lote de servi�o');
  ConfGr(Grade_240_3_S, F3S_019, 032, 'C�digo de movimento');
  ConfGr(Grade_240_3_S, F3S_197, 019, 'Identif. da impress�o');
  ConfGr(Grade_240_3_S, F3S_198, 032, 'N� da linha a imprim');
  ConfGr(Grade_240_3_S, F3S_199, 210, 'Mensagem a imprimir');
  ConfGr(Grade_240_3_S, F3S_200, 032, 'Tipo de caracter. imp.');
  ConfGr(Grade_240_3_S, F3S_205, 210, 'Mensagem 5');
  ConfGr(Grade_240_3_S, F3S_206, 210, 'Mensagem 6');
  ConfGr(Grade_240_3_S, F3S_207, 210, 'Mensagem 7');
  ConfGr(Grade_240_3_S, F3S_208, 210, 'Mensagem 8');
  ConfGr(Grade_240_3_S, F3S_209, 210, 'Mensagem 9');
  //
  FLinG1 := 0;
  FColG1 := 0;
  F1_011 := Incrementa(Grade_1, FColG1, 1, FCols1, 011);
  F1_400 := Incrementa(Grade_1, FColG1, 1, FCols1, 400);
  F1_401 := Incrementa(Grade_1, FColG1, 1, FCols1, 401);
  F1_402 := Incrementa(Grade_1, FColG1, 1, FCols1, 402);
  F1_408 := Incrementa(Grade_1, FColG1, 1, FCols1, 408);
  F1_020 := Incrementa(Grade_1, FColG1, 1, FCols1, 020);
  F1_022 := Incrementa(Grade_1, FColG1, 1, FCols1, 022);
  F1_021 := Incrementa(Grade_1, FColG1, 1, FCols1, 021);
  F1_023 := Incrementa(Grade_1, FColG1, 1, FCols1, 023);
  F1_024 := Incrementa(Grade_1, FColG1, 1, FCols1, 024);
  F1_035 := Incrementa(Grade_1, FColG1, 1, FCols1, 035);
  F1_037 := Incrementa(Grade_1, FColG1, 1, FCols1, 037);
  F1_038 := Incrementa(Grade_1, FColG1, 1, FCols1, 038);
  F1_211 := Incrementa(Grade_1, FColG1, 1, FCols1, 211);
  F1_212 := Incrementa(Grade_1, FColG1, 1, FCols1, 212);
  F1_506 := Incrementa(Grade_1, FColG1, 1, FCols1, 506);
  F1_501 := Incrementa(Grade_1, FColG1, 1, FCols1, 501);
  F1_511 := Incrementa(Grade_1, FColG1, 1, FCols1, 511);
  F1_512 := Incrementa(Grade_1, FColG1, 1, FCols1, 512);
  F1_509 := Incrementa(Grade_1, FColG1, 1, FCols1, 509);
  F1_508 := Incrementa(Grade_1, FColG1, 1, FCols1, 508);
  F1_504 := Incrementa(Grade_1, FColG1, 1, FCols1, 504);
  F1_502 := Incrementa(Grade_1, FColG1, 1, FCols1, 502);
  F1_580 := Incrementa(Grade_1, FColG1, 1, FCols1, 580);
  F1_581 := Incrementa(Grade_1, FColG1, 1, FCols1, 581);
  F1_550 := Incrementa(Grade_1, FColG1, 1, FCols1, 550);
  F1_001 := Incrementa(Grade_1, FColG1, 1, FCols1, 001);
  F1_003 := Incrementa(Grade_1, FColG1, 1, FCols1, 003);
  F1_507 := Incrementa(Grade_1, FColG1, 1, FCols1, 507);
  F1_520 := Incrementa(Grade_1, FColG1, 1, FCols1, 520);
  F1_549 := Incrementa(Grade_1, FColG1, 1, FCols1, 549);
  F1_583 := Incrementa(Grade_1, FColG1, 1, FCols1, 583);
  F1_701 := Incrementa(Grade_1, FColG1, 1, FCols1, 701);
  F1_702 := Incrementa(Grade_1, FColG1, 1, FCols1, 702);
  F1_711 := Incrementa(Grade_1, FColG1, 1, FCols1, 711);
  F1_712 := Incrementa(Grade_1, FColG1, 1, FCols1, 712);
  F1_720 := Incrementa(Grade_1, FColG1, 1, FCols1, 720);
  F1_950 := Incrementa(Grade_1, FColG1, 1, FCols1, 950);
  F1_572 := Incrementa(Grade_1, FColG1, 1, FCols1, 572);
  F1_574 := Incrementa(Grade_1, FColG1, 1, FCols1, 574);
  F1_576 := Incrementa(Grade_1, FColG1, 1, FCols1, 576);
  F1_573 := Incrementa(Grade_1, FColG1, 1, FCols1, 573);
  F1_575 := Incrementa(Grade_1, FColG1, 1, FCols1, 575);
  F1_577 := Incrementa(Grade_1, FColG1, 1, FCols1, 577);
  F1_587 := Incrementa(Grade_1, FColG1, 1, FCols1, 587);
  F1_801 := Incrementa(Grade_1, FColG1, 1, FCols1, 801);
  F1_802 := Incrementa(Grade_1, FColG1, 1, FCols1, 802);
  F1_803 := Incrementa(Grade_1, FColG1, 1, FCols1, 803);
  F1_811 := Incrementa(Grade_1, FColG1, 1, FCols1, 811);
  F1_812 := Incrementa(Grade_1, FColG1, 1, FCols1, 812);
  F1_813 := Incrementa(Grade_1, FColG1, 1, FCols1, 813);
  F1_814 := Incrementa(Grade_1, FColG1, 1, FCols1, 814);
  F1_805 := Incrementa(Grade_1, FColG1, 1, FCols1, 805);
  F1_806 := Incrementa(Grade_1, FColG1, 1, FCols1, 806);
  F1_807 := Incrementa(Grade_1, FColG1, 1, FCols1, 807);
  F1_808 := Incrementa(Grade_1, FColG1, 1, FCols1, 808);
  F1_851 := Incrementa(Grade_1, FColG1, 1, FCols1, 851);
  F1_852 := Incrementa(Grade_1, FColG1, 1, FCols1, 852);
  F1_853 := Incrementa(Grade_1, FColG1, 1, FCols1, 853);
  F1_584 := Incrementa(Grade_1, FColG1, 1, FCols1, 584);
  F1_586 := Incrementa(Grade_1, FColG1, 1, FCols1, 586);
  F1_552 := Incrementa(Grade_1, FColG1, 1, FCols1, 552);
  F1_569 := Incrementa(Grade_1, FColG1, 1, FCols1, 569);
  F1_551 := Incrementa(Grade_1, FColG1, 1, FCols1, 551);
  F1_558 := Incrementa(Grade_1, FColG1, 1, FCols1, 558);
  F1_621 := Incrementa(Grade_1, FColG1, 1, FCols1, 621);
  F1_627 := Incrementa(Grade_1, FColG1, 1, FCols1, 627);
  F1_639 := Incrementa(Grade_1, FColG1, 1, FCols1, 639);
  F1_640 := Incrementa(Grade_1, FColG1, 1, FCols1, 640);
  F1_647 := Incrementa(Grade_1, FColG1, 1, FCols1, 647);
  F1_804 := Incrementa(Grade_1, FColG1, 1, FCols1, 804);
  F1_665 := Incrementa(Grade_1, FColG1, 1, FCols1, 665);
  F1_667 := Incrementa(Grade_1, FColG1, 1, FCols1, 667);
  F1_513 := Incrementa(Grade_1, FColG1, 1, FCols1, 513);
  F1_514 := Incrementa(Grade_1, FColG1, 1, FCols1, 514);
  F1_515 := Incrementa(Grade_1, FColG1, 1, FCols1, 515);
  F1_516 := Incrementa(Grade_1, FColG1, 1, FCols1, 516);
  F1_643 := Incrementa(Grade_1, FColG1, 1, FCols1, 643);
  F1_032 := Incrementa(Grade_1, FColG1, 1, FCols1, 032);
  F1_033 := Incrementa(Grade_1, FColG1, 1, FCols1, 033);
  F1_034 := Incrementa(Grade_1, FColG1, 1, FCols1, 034);
  F1_650 := Incrementa(Grade_1, FColG1, 1, FCols1, 650);
  F1_410 := Incrementa(Grade_1, FColG1, 1, FCols1, 410);
  F1_591 := Incrementa(Grade_1, FColG1, 1, FCols1, 591);
  F1_592 := Incrementa(Grade_1, FColG1, 1, FCols1, 592);
  F1_593 := Incrementa(Grade_1, FColG1, 1, FCols1, 593);
  F1_594 := Incrementa(Grade_1, FColG1, 1, FCols1, 594);
  F1_595 := Incrementa(Grade_1, FColG1, 1, FCols1, 595);
  F1_596 := Incrementa(Grade_1, FColG1, 1, FCols1, 596);
  F1_597 := Incrementa(Grade_1, FColG1, 1, FCols1, 597);
  F1_598 := Incrementa(Grade_1, FColG1, 1, FCols1, 598);
  F1_599 := Incrementa(Grade_1, FColG1, 1, FCols1, 599);
  //
  ConfGr(Grade_1, F1_000, 032, 'Item');
  ConfGr(Grade_1, F1_011, 018, 'TR');
  ConfGr(Grade_1, F1_400, 018, 'TIE - Tipo inscr. empresa');
  ConfGr(Grade_1, F1_401, 112, 'Inscr. Empresa');
  ConfGr(Grade_1, F1_402, 210, 'Nome Empresa');
  ConfGr(Grade_1, F1_408, 032, 'C�digo do conv�nio');
  ConfGr(Grade_1, F1_020, 032, 'Ag�n.');
  ConfGr(Grade_1, F1_022, 016, 'DV');
  ConfGr(Grade_1, F1_021, 072, 'Conta corrente');
  ConfGr(Grade_1, F1_023, 016, 'DV');
  ConfGr(Grade_1, F1_024, 020, 'DAC');
  ConfGr(Grade_1, F1_506, 072, 'Seu n�mero');
  ConfGr(Grade_1, F1_501, 056, 'Nosso n�mero');
  ConfGr(Grade_1, F1_509, 024, 'Carteira');
  ConfGr(Grade_1, F1_508, 024, 'C�d.Cart');
  ConfGr(Grade_1, F1_504, 024, 'Ocorrencia');
  ConfGr(Grade_1, F1_502, 072, 'Duplicata');
  ConfGr(Grade_1, F1_511, 024, 'DAC do Nosso n�mero');
  ConfGr(Grade_1, F1_512, 056, 'Sequencial do nosso n�mero');
  ConfGr(Grade_1, F1_580, 056, 'Vencto');
  ConfGr(Grade_1, F1_581, 056, 'Data cr�dito');
  ConfGr(Grade_1, F1_550, 080, 'Valor doc.');
  ConfGr(Grade_1, F1_001, 024, 'Bco');
  ConfGr(Grade_1, F1_003, 032, 'Lote de servi�o');
  ConfGr(Grade_1, F1_507, 020, 'Esp.T�t');
  ConfGr(Grade_1, F1_520, 012, 'Aceite');
  ConfGr(Grade_1, F1_549, 012, 'Moeda');
  ConfGr(Grade_1, F1_583, 056, 'Emiss�o');
  ConfGr(Grade_1, F1_701, 020, 'I1 - instru��o 1');
  ConfGr(Grade_1, F1_711, 020, 'D1 - dias instru��o 1');
  ConfGr(Grade_1, F1_702, 020, 'I2 - instru��o 2');
  ConfGr(Grade_1, F1_712, 020, 'D2 - dias instru��o 2');
  ConfGr(Grade_1, F1_720, 020, 'NR - instru��o de n�o recebimento');
  ConfGr(Grade_1, F1_950, 020, 'Dias �nico');
  ConfGr(Grade_1, F1_572, 080, '$ mora dd');
  ConfGr(Grade_1, F1_573, 080, '$ multa');
  ConfGr(Grade_1, F1_574, 080, '% mora/m�s');
  ConfGr(Grade_1, F1_575, 080, '% multa');
  ConfGr(Grade_1, F1_576, 024, 'T.Mora');
  ConfGr(Grade_1, F1_577, 024, 'T.Multa');
  ConfGr(Grade_1, F1_587, 056, 'Dta.Multa');
  ConfGr(Grade_1, F1_801, 020, 'TIS - Tipo inscr. sacado');
  ConfGr(Grade_1, F1_802, 112, 'Inscr. Sacado');
  ConfGr(Grade_1, F1_803, 210, 'Nome do Sacado');
  ConfGr(Grade_1, F1_811, 048, 'T.logr.');
  ConfGr(Grade_1, F1_812, 140, 'Logradouro');
  ConfGr(Grade_1, F1_813, 035, 'N�');
  ConfGr(Grade_1, F1_814, 072, 'Complem.');
  ConfGr(Grade_1, F1_805, 084, 'Bairro');
  ConfGr(Grade_1, F1_806, 064, 'CEP');
  ConfGr(Grade_1, F1_807, 108, 'Cidade');
  ConfGr(Grade_1, F1_808, 020, 'UF');
  ConfGr(Grade_1, F1_851, 020, 'Doc');
  ConfGr(Grade_1, F1_852, 112, 'CNPJ / CPF sac/aval.');
  ConfGr(Grade_1, F1_853, 210, 'Nome do Sacador / avalista');
  ConfGr(Grade_1, F1_584, 056, 'Dt mora');
  ConfGr(Grade_1, F1_586, 056, 'Dt.lim.desconto');
  ConfGr(Grade_1, F1_552, 064, 'Val.Desconto');
  ConfGr(Grade_1, F1_569, 064, 'Val. IOF');
  ConfGr(Grade_1, F1_551, 064, 'Val.Abatimento');
  ConfGr(Grade_1, F1_804, 280, 'Logradouro completo');
  ConfGr(Grade_1, F1_621, 018, 'Quem imprime?');
  ConfGr(Grade_1, F1_627, 018, 'Quem distribui?');
  ConfGr(Grade_1, F1_639, 077, 'Msg 1 (237) 12 posi��es');
  ConfGr(Grade_1, F1_640, 200, 'Msg 2 (237) 60 posi��es');
  ConfGr(Grade_1, F1_647, 024, 'DDP Protesto em xx dias ap�s vencimento');
  ConfGr(Grade_1, F1_558, 064, 'Val.Bonifica��o');
  ConfGr(Grade_1, F1_035, 032, 'Sub conta (corrente)');
  ConfGr(Grade_1, F1_037, 128, 'C�d.Especial');
  ConfGr(Grade_1, F1_038, 036, 'Complem.C�d.Especial');
  ConfGr(Grade_1, F1_211, 200, 'Mensagem 1');
  ConfGr(Grade_1, F1_212, 200, 'Mensagem 2');
  ConfGr(Grade_1, F1_665, 020, 'TBU - Tipo de bloqueto utilizado');
  ConfGr(Grade_1, F1_591, 020, 'C�d.desc.1');
  ConfGr(Grade_1, F1_592, 056, 'Dta.desc.1');
  ConfGr(Grade_1, F1_593, 064, 'Val.desc.1');
  ConfGr(Grade_1, F1_594, 020, 'C�d.desc.2');
  ConfGr(Grade_1, F1_595, 056, 'Dta.desc.2');
  ConfGr(Grade_1, F1_596, 064, 'Val.desc.2');
  ConfGr(Grade_1, F1_597, 020, 'C�d.desc.3');
  ConfGr(Grade_1, F1_598, 056, 'Dta.desc.3');
  ConfGr(Grade_1, F1_599, 064, 'Val.desc.3');
  ConfGr(Grade_1, F1_667, 064, 'Val t�t outra unidade');
  ConfGr(Grade_1, F1_513, 056, 'Parcela');
  ConfGr(Grade_1, F1_514, 056, 'Contrato Garantia N�mero');
  ConfGr(Grade_1, F1_515, 056, 'Contrato Garantia DV');
  ConfGr(Grade_1, F1_516, 056, 'Border� n�mero');
  ConfGr(Grade_1, F1_643, 056, 'Indicativo de Mensagem ou Sacador/Avalista');
  ConfGr(Grade_1, F1_032, 064, 'N�mero remessa');
  ConfGr(Grade_1, F1_033, 056, 'Dta.grava��o');
  ConfGr(Grade_1, F1_034, 056, 'Forma de cadastramento do t�tulo no banco');
  ConfGr(Grade_1, F1_650, 100, 'Mensagem 40 caracteres');
  ConfGr(Grade_1, F1_410, 072, 'C�digo do Cedente');
  //
  FLinG2 := 0;
  FColG2 := 0;
  F2_011 := Incrementa(Grade_2, FColG2, 1, FCols2, 011);
  F2_201 := Incrementa(Grade_2, FColG2, 1, FCols2, 201);
  F2_202 := Incrementa(Grade_2, FColG2, 1, FCols2, 202);
  F2_203 := Incrementa(Grade_2, FColG2, 1, FCols2, 203);
  F2_204 := Incrementa(Grade_2, FColG2, 1, FCols2, 204);
  F2_508 := Incrementa(Grade_2, FColG2, 1, FCols2, 508);
   //
  ConfGr(Grade_2, F2_000, 032, 'Item');
  ConfGr(Grade_2, F2_011, 018, 'TR');
  ConfGr(Grade_2, F2_201, 200, 'Mensagem 1');
  ConfGr(Grade_2, F2_202, 200, 'Mensagem 2');
  ConfGr(Grade_2, F2_203, 200, 'Mensagem 3');
  ConfGr(Grade_2, F2_204, 200, 'Mensagem 4');
  ConfGr(Grade_2, F2_508, 056, 'Carteira');
  //
  FLinG5 := 0;
  FColG5 := 0;
  F5_000 := Incrementa(Grade_5, FColG5, 1, FCols5, 000);
  F5_001 := Incrementa(Grade_5, FColG5, 1, FCols5, 001);
  F5_003 := Incrementa(Grade_5, FColG5, 1, FCols5, 003);
  F5_011 := Incrementa(Grade_5, FColG5, 1, FCols5, 011);
  F5_815 := Incrementa(Grade_5, FColG5, 1, FCols5, 815);
  F5_850 := Incrementa(Grade_5, FColG5, 1, FCols5, 850);
  F5_851 := Incrementa(Grade_5, FColG5, 1, FCols5, 851);
  F5_852 := Incrementa(Grade_5, FColG5, 1, FCols5, 852);
  F5_861 := Incrementa(Grade_5, FColG5, 1, FCols5, 861);
  F5_862 := Incrementa(Grade_5, FColG5, 1, FCols5, 862);
  F5_863 := Incrementa(Grade_5, FColG5, 1, FCols5, 863);
  F5_864 := Incrementa(Grade_5, FColG5, 1, FCols5, 864);
  F5_855 := Incrementa(Grade_5, FColG5, 1, FCols5, 855);
  F5_856 := Incrementa(Grade_5, FColG5, 1, FCols5, 856);
  F5_857 := Incrementa(Grade_5, FColG5, 1, FCols5, 857);
  F5_858 := Incrementa(Grade_5, FColG5, 1, FCols5, 858);
  F5_854 := Incrementa(Grade_5, FColG5, 1, FCols5, 854);
  F5_992 := Incrementa(Grade_5, FColG5, 1, FCols5, 992);
  //
  ConfGr(Grade_5, 000000, 032, 'Seq.');
  ConfGr(Grade_5, F5_000, 032, 'Item');
  ConfGr(Grade_5, F5_001, 032, 'Banco');
  ConfGr(Grade_5, F5_003, 032, 'Lote de servi�o');
  ConfGr(Grade_5, F5_011, 018, 'TR');
  ConfGr(Grade_5, F5_815, 200, 'E-mail do sacado');
  ConfGr(Grade_5, F5_851, 020, 'TIS - Tipo inscr. sacador / avalista');
  ConfGr(Grade_5, F5_852, 112, 'Inscr. Sacador / avalista');
  ConfGr(Grade_5, F5_861, 048, 'T.logr. do Sacador / avalista');
  ConfGr(Grade_5, F5_862, 140, 'Logradouro do Sacador / avalista');
  ConfGr(Grade_5, F5_863, 035, 'N�do logradouro do Sacador / avalista');
  ConfGr(Grade_5, F5_864, 072, 'Complem. do endere�o do Sacador / avalista');
  ConfGr(Grade_5, F5_855, 084, 'Bairro do Sacador / avalista');
  ConfGr(Grade_5, F5_856, 064, 'CEP do Sacador / avalista');
  ConfGr(Grade_5, F5_857, 108, 'Cidade do Sacador / avalista');
  ConfGr(Grade_5, F5_858, 020, 'UF do Sacador / avalista');
  ConfGr(Grade_5, F5_854, 210, 'Logradouro completo do Sacador / avalista');
  ConfGr(Grade_5, F5_992, 032, 'Qtde registros lote');
end;

function TFmCNAB_Rem.AddLinha(var Contador: Integer; const Grade: TStringGrid): Integer;
begin
  Contador       := Contador + 1;
  Grade.RowCount := Contador + 1;
  Grade.Cells[0,Contador] := FormatFloat('0000', Contador);
  //
  Result := Contador;
end;

procedure TFmCNAB_Rem.AddCampo(Grade: TStringGrid; Linha, Coluna: Integer;
              Formato: String; Valor: Variant);
var
  Texto: String;
begin
  Texto := dmkPF.VariavelToTexto(Formato, Valor);
  Grade.Cells[Coluna, Linha] := Texto;
end;

function TFmCNAB_Rem.Incrementa(const Grade: TStringGrid; var Contador: Integer;
             const Incremento: Integer; var ListaCols: TArrayCols;
             const MyCodField: Integer): Integer;
begin
  inc(Contador, Incremento);
  Result := Contador;
  ListaCols[MyCodField] := Contador;
  if Grade.ColCount < Result + 1 then
    Grade.ColCount := Result + 1;
end;

{
function TFmCNAB_Rem.ConcatenaAgenciaEConta(const Agencia, Conta: String;
const Len: Integer; var AgCC: String): Boolean;
begin
  Result := True;
  AgCC := '';
  if Length(Agencia) + Length(Conta) = Len then
    AgCC := Agencia + Conta
  else
  if Length(Conta) = Len then
    AgCC := Conta
  else
    Result := False;
end;
}

procedure TFmCNAB_Rem.ConfGr(Grade: TStringGrid; Coluna, Tam: Integer;
Titulo: String);
begin
  Grade.ColWidths[Coluna] := Tam;
  Grade.Cells[Coluna,  0] := Titulo;
end;

procedure TFmCNAB_Rem.Grade_1SelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var
  i: Integer;
begin
  for i := 1 to 1000 do
    if FCols1[i] = ACol then
    begin
      LaCodDmk_1.Caption := FormatFloat('000', i);
      Break;
    end;
end;

procedure TFmCNAB_Rem.Grade_240_3_PSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var
  i: Integer;
begin
  for i := 1 to 1000 do
  begin
    if FCols3P[i] = ACol then
    begin
      LaCodDmk_1.Caption := FormatFloat('000', i);
      Break;
    end;
  end;
end;

procedure TFmCNAB_Rem.Grade_240_3_QSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var
  i: Integer;
begin
  for i := 1 to 1000 do
  begin
    if FCols3Q[i] = ACol then
    begin
      LaCodDmk_1.Caption := FormatFloat('000', i);
      Break;
    end;
  end;
end;

procedure TFmCNAB_Rem.Grade_240_3_RSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var
  i: Integer;
begin
  for i := 1 to 1000 do
  begin
    if FCols3R[i] = ACol then
    begin
      LaCodDmk_1.Caption := FormatFloat('000', i);
      Break;
    end;
  end;
end;

procedure TFmCNAB_Rem.Grade_240_3_SSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var
  i: Integer;
begin
  for i := 1 to 1000 do
  begin
    if FCols3S[i] = ACol then
    begin
      LaCodDmk_1.Caption := FormatFloat('000', i);
      Break;
    end;
  end;
end;

function TFmCNAB_Rem.NovaLinhaArq(): String;
begin
  inc(FLinArq, 1);
  Result := FormatFloat('0', FLinArq);
end;

function TFmCNAB_Rem.NovaLinhaArq2(): String;
begin
  inc(FLinArq2, 1);
  Result := FormatFloat('0', FLinArq2);
end;

procedure TFmCNAB_Rem.Edit2Change(Sender: TObject);
begin
  PesquisaTexto;
end;

procedure TFmCNAB_Rem.Edit3Change(Sender: TObject);
begin
  PesquisaTexto;
end;

procedure TFmCNAB_Rem.Edit4Change(Sender: TObject);
begin
  PesquisaTexto;
end;

procedure TFmCNAB_Rem.PageControl1Change(Sender: TObject);
begin
  if DBRadioGroup1.ItemIndex = 1 then
    PageControl3.ActivePageIndex := 1
  else
    PageControl3.ActivePageIndex := 0;
end;

procedure TFmCNAB_Rem.PageControl2Change(Sender: TObject);
begin
  PesquisaTexto;
end;

function TFmCNAB_Rem.CriaListaCNAB(const CNAB, Banco, Registro, Posicoes: Integer;
  Segmento: Char; var Lista: MyArrayLista; const NomeLayout: String;
  const SubSegmento: Integer = 0): Boolean;
var
  CNABResult: TCNABResult;
begin
  case CNAB of
    240: CNABResult := UBco_Rem.LayoutCNAB240(Banco, Registro, Segmento, Lista, SubSegmento, NomeLayout);
    400: CNABResult := UBco_Rem.LayoutCNAB400(Banco, Registro, Lista, Posicoes, NomeLayout);
    else CNABResult := cresNoCNAB;
  end;
  if not (CNABResult in ([cresNoField, cresOKField])) then
  begin
    UBancos.CNABResult_Msg(CNABResult, CNAB, Banco, Registro, ecnabRemessa);
    Result := False;
  end else
    Result := True;
end;

procedure TFmCNAB_Rem.BtMoverClick(Sender: TObject);
var
  ArqOri, ArqDes: String;
begin
  ArqOri := EdArqGerado.Text;
  //
  if (ArqOri <> '') and (FileExists(ArqOri)) then
  begin
    ArqDes := ExtractFilePath(ArqOri) + '\Enviados\' + ExtractFileName(ArqOri);
    //
    dmkPF.MoveArq(ArqOri, ArqDes);
  end;
  BtMover.Visible := FileExists(ArqOri);
end;

procedure TFmCNAB_Rem.BtOKClick(Sender: TObject);

  function ValorDeColunaDeGrade(const Grade: TStringGrid; const Lista:
  MyArrayLista; const ItemLista: Integer; const FCols: TArrayCols;
  const LinhaGrade: Integer; const NomeLayout: String; var Res: String): Boolean;
  var
    CodiFld, NomeFld, Texto(*, NossoNum*): String;
    Campo, Coluna: Integer;
  begin
    CodiFld := Lista[ItemLista][_Fld];
    Campo   := Geral.IMV(CodiFld);
    //if Campo = 647 then
      //ShowMessage('647');
    // Se o campo for DAC do "Nosso n�mero"  separado do seu DAC...
    {Setado na abertura do Form
    if Campo = 511 then
    // obtem o valor do "Nosso n�mero"
      Coluna := FCols[501]
    //sen�o obtem o valor do campo normal
    else
    }
    Coluna  := FCols[Campo];
    Res := '';
    //if Campo = 20 then
      //ShowMessage(Geral.FF0(Coluna));
    Result := True;
    case Campo of
       -02: Res := ' ';
       -01: Res := '0';
       000: Res := '';
       997: Res := NovaLinhaArq2();
       999: Res := NovaLinhaArq();
      else
      begin
        if Coluna > 0 then
        begin
          Res := Grade.Cells[Coluna, LinhaGrade];
          case Campo of
            // 400: Tipo de inscri��o cedente
            // 801: Tipo de inscricao sacado
            // 851: Tipo de inscricao sacador / avalista
            400, 801, 851: Result := UBco_Rem.TipoDeInscricao(Geral.IMV(Res), FBanco, GetCNAB(), Campo, NomeLayout, Res);
            // Ocorr�ncia ( motivo de enviar o arquivo)
            504: Result := UBco_Rem.CodigoDeOcorrenciaRemessa(FBanco, 01, Res);
            {Setado na abertura do Form
            // DAC do Nosso n�mero
            511: Result := DAC_NossoNumero(Geral.DMV(Res), Res);
            }
            // Aceite do t�tulo
            520: Result := UBco_Rem.CodigoDeAceiteRemessa(FBanco, Geral.IMV(Res), NomeLayout, Res);
            // Quem Imprime
            621: Result := UBco_Rem.QuemImprimeOBloqueto(FBanco, Geral.IMV(Res), NomeLayout, Res);
            // Quem distribui
            627: Result := UBco_Rem.QuemDistribuiOBloqueto(FBanco, Geral.IMV(Res), Res);
          end;
          //Result := True;
        end else
        begin
          Result := False;
          case Campo of

            // CAMPOS N�O IMPEMENTADOS !!!!!
            // Registro detalhe
            011: Result := True;
            // Registro trailer lote
            012: Result := True;
            // Identifica��o do registro
            014: Result := True;
            // Identifica��o do segmento
            015: Result := True;
            // C�digo movimento
            019: Result := True;
            // Ag�ncia cobradora
            025: Result := True;
            // DV ag�ncia cobradora
            026: Result := True;
            // Banco cobrador
            027: Result := True;
            // Tipo de servi�o
            029: Result := True;
            // Tipo de opera��o
            031: Result := True;
            // Forma de cadastramento do t�tulo no banco
            034: Result := True;
            //
            080: Result := True;
            //
            082: Result := True;
            //
            083: Result := True;
            // Descontos 1 a 3
            591..599: Result := True;
            // D�bito autom�tico no bradesco
            610..615,622,624: Result := True;
            // Rateio de cr�dito Bradesco
            623: Result := True;
            //
            // C�digo para Baixa/Devolu��
            652: Result := True;
            // N�mero de dias para baixa / devolu��o
            653: Result := True;
            // Quantidade de moeda vari�vel
            749: Result := True;
            //C�digo de instru��o / alega��o (a ser cancelada)
            //   Parei aqui!!!! Fazer quando responder a  arquivos de retorno!!!!
            750: Result := True;
            // Vers�o do Layout
            889: Result := True;
          end;
          if Result = False then
          begin
            // descobrir todos n�o implementados
            Result := True;
            //
            NomeFld := ' - "' + UBancos.DescricaoDoMeuCodigoCNAB(Campo) + '" ';
            //
            Texto := 'O c�digo de campo ' + CodiFld + NomeFld
            + ' n�o est� implementado! (Coluna ' + Geral.FF0(Coluna) + ')';
            if Coluna = 0 then
              Texto := Texto + sLineBreak + sLineBreak +
              'Verifique com a DERMATEK a necessidade de cria��o de coluna "F' +
              Grade.Name[Length(Grade.Name)] + '_' + Geral.FF0(Campo) +
              '" na grade "' + Grade.Name + '" para o campo!';
            Texto := Texto + sLineBreak + sLineBreak +
            'Verifique com a DERMATEK a necessidade de implementa��o da chamada do procedimento "AddCampo(g, n, F' +
            Grade.Name[Length(Grade.Name)] + '_' + Geral.FF0(Campo) +
            '" na janela que chama a janela de gera��o do arquivo de remessa CNAB!';
            //
            Geral.MB_Erro(Texto);
          end;
        end;
      end;
    end;
  end;

  function ValorDeCampo(const Lista: MyArrayLista; Campo: Integer;
  const Pre_Def: String; var Res: String): Boolean;
  var
    Def, Fil, Typ, Formato, Texto: String;
    Len, Fld, i, Cas: Integer;
    Ajusta, Obrigatorio: Boolean;
    Data: TDateTime;
    function ObrigatorioSem1a9(): Boolean;
    begin
      if Obrigatorio and (Geral.SoNumero1a9_TT(Texto) = '') then
      begin
        Result := True;
        Geral.MB_Aviso('Tipo de dado n�o obrigat�rio: "' +
          Typ + '" sem texto  definido em "CNAB_Rem.BtOKClick"!' + sLineBreak +
          'Campo "' + FormatFloat('000', fld) + ' - ' + Trim(Lista[Campo][_Nom])
          + '"');
        Exit;
      end else
        Result := False;
    end;
  begin
    Result := False;
    Def := Trim(Lista[Campo][_Def]);
    Fil := Lista[Campo][_Fil];
    Len := Geral.IMV(Lista[Campo][_Tam]);
    Fld := Geral.IMV(Lista[Campo][_Fld]);
    Obrigatorio :=
              (Uppercase(Lista[Campo][_AjT]) = 'X') or // X para alfanumerico
              (Uppercase(Lista[Campo][_AjT]) = '9');   // 9 para num�rico (deve ter pelo menos um n�mero diferente de zero)
    Ajusta := (Uppercase(Lista[Campo][_AjT]) = 'S') or
              (Uppercase(Lista[Campo][_AjT]) = 'X');
    //if Fld = 504 then
      //ShowMessage('504');
    Typ := Lista[Campo][_Typ];
    if Def <> '' then Texto := Def else
    begin
      if Pre_Def <> '' then Texto := Pre_def else
      begin
        // Parei Aqui buscar dados de grades
        Texto := '';
      end;

      //  Formata Data
      if Typ = 'D' then
      begin
        if ObrigatorioSem1a9() then
           Exit;
        Data    := Geral.ValidaDataSimples(Texto, True);
        if Data = 0 then
        begin
          Texto := '';
          while Length(Texto) < Len do
            Texto := Texto + Fil;
        end else
        begin
          Formato := Uppercase(Trim(Lista[Campo][_Fmt]));
          if Length(Formato) = 0 then
          begin
            Result := False;
            Geral.MB_Erro('Data sem formata��o para o campo "' +
              Lista[Campo][_Nom] + '" em "[CNAB_Rem]ValorDeCampo"!');
            Exit;
          end;
          for i := 1 to Length(Formato) do
            if Formato[i] = 'A' then Formato[i] := 'Y';
          Texto := FormatDateTime(Formato, Data);
        end;
      end else
      //  Formata Tempo
      if Typ = 'T' then
      begin
        Texto := Geral.SoNumero_TT(Texto);
      end else
      //         Texto           Zeros          Brancos       Calculado
      if (Typ = 'X') or  (Typ = 'Z') or (Typ = 'B') or (Typ = 'C') then
      begin
        Texto := Geral.SemAcento(Texto);
        Texto := Geral.Maiusculas(Texto, False);
        if Obrigatorio and (Trim(Texto) = '') then
        begin
          Geral.MB_Aviso('Tipo de dado n�o obrigat�rio: "' +
            Typ + '" sem texto  definido em "CNAB_Rem.BtOKClick"!' + sLineBreak +
            'Campo "' + FormatFloat('000', fld) + ' - ' + Trim(Lista[Campo][_Nom])
            + '"');
          Exit;
        end;
      end else
      // Formata Inteiro
      if Typ = 'I' then
      begin
         if ObrigatorioSem1a9() then
           Exit;
        Texto := Geral.SoNumero_TT(Texto);
      end else
      // Formata Inteiro
      if Typ = 'F' then
      begin
         if ObrigatorioSem1a9() then
           Exit;
        Cas := Geral.IMV(Lista[Campo][_Fmt]);

        Texto := Trim(Texto);
        Texto := Geral.TFT(Texto, Cas, siPositivo);
        Texto := Geral.SoNumero_TT(Texto);
      end else
      begin
        Geral.MB_Erro('Tipo de dado n�o implementado: "' +
          Typ + '" em "CNAB_Rem.BtOKClick"!' + sLineBreak + 'Campo "' + FormatFloat(
          '000', fld) + ' - ' + Trim(Lista[Campo][_Nom]) + '"');
        Exit;
      end;
    end;

    {case Fld of
      402, 803, 853: Corrige := True;
      else Corrige := False;
    end;}
    //
    Result := AjustaString(Texto, Fil, Len, Ajusta, Res);
    if not Result then
      Geral.MB_Erro('O tamanho do texto "' + Res + '" ficou ' +
        'com tamanho = ' + Geral.FF0(Length(Res)) + ' que � diferente do ' +
        'estipulado, que � de ' + Geral.FF0(Len) + ' posi��es para o campo "' +
        FormatFloat('000', fld) + ' - ' + Trim(Lista[Campo][_Nom]) + '"!' +
        sLineBreak + 'function ValorDeCampo()');
  end;

  procedure AvisaCancelamento(Texto: String; Lista: MyArrayLista; Campo, TamAtual: Integer);
  var
    TamCorreto: Integer;
  begin
    TamCorreto := Geral.IMV(Lista[Campo][_Fim]);
    MeGerado.Lines.Add(Texto);
    Geral.MB_Erro('Gera��o de arquivo remessa cancelado!' +
      sLineBreak + 'A linha ' + Geral.FF0(MeGerado.Lines.Count) + ' deveria ter ' +
      Geral.FF0(TamCorreto) + ' caracteres, mas est� com ' + Geral.FF0(TamAtual) +
      '!' + sLineBreak + 'Ap�s o campo "' + Lista[Campo][_Nom] + '"');
  end;

  function RemoveQuebraDeLinha(Txt: String):String;
  var
    Texto: String;
    T, K: Integer;
  begin
    //Tirar o sLineBreak
    Texto := '';
    T := Length(Txt);
    for K := 1 to T do
    begin
      if K < T - DmBco.QrCNAB_CfgCNAB.Value + 2 then
        Texto := Texto + MeGerado.Text[K]
      else begin
        if not (Txt[K] in ([#10, #13])) then
        Texto := Texto + Txt[K];
      end;
    end;
    Result := Texto;
  end;

const
  // Compatibilidade (por causa do CNAB_Rem2)
  Posicoes = 0;
var
  i, j, T, K, Item, Lin1, Banco, CNAB, Campo, Len, QtdL, QtdA: Integer;
  Envio: TEnvioCNAB;
  ListaA, ListaB, ListaC, ListaP, ListaQ, ListaR, ListaS: MyArrayLista;
  SeqArq, LinReg, ValCampo, Pre_Def, Arquivo, Texto, NomeLayout, NomeArq, Res: String;
  Continua: Boolean;
  FileEnd: Variant;
  JaFez999do9: Boolean;
  Segmento: Char;
  DataLote: TDateTime;
  Dia, Mes, Ano: Word;
begin
  Screen.Cursor := crHourGlass;
  try
    MeGerado.Clear;
    //
    EdArqGerado.Text := '';
    FLinArq          := 0;
    FLinArq2         := 0;
    Segmento         := #0;
    //
    //FValTitulos := 0.00;
    JaFez999do9 := False;
    Banco       := dmkEd_0_001.ValueVariant;
    CNAB        := DmBco.QrCNAB_CfgCNAB.Value;
    NomeLayout  := DmBco.QrCNAB_CfgLayoutRem.Value;
    //
    case dmkEd_0_005.ValueVariant of
       1: Envio := ecnabRemessa;
       2: Envio := ecnabRetorno;
      else Envio := ecnabIndefinido;
    end;
    if not UBancos.BancoImplementado(Banco, CNAB, Envio) then Exit;
    //
    ////////////////////// 0 - REGISTRO HEADER ///////////////////////////////////
    if not CriaListaCNAB(CNAB, Banco, 0, Posicoes, Segmento, ListaA, NomeLayout) then Exit;
    //
    LinReg := '';
    //
    for i := Low(ListaA) to High(ListaA) do
    begin
      Campo := Geral.IMV(ListaA[i][_Fld]);
      case Campo of
        -02: Pre_Def := ' ';
        -01: Pre_def := '0';
        020: Pre_Def := dmkEd_0_020.Text;
        021: Pre_Def := dmkEd_0_021.Text;
        022: Pre_Def := dmkEd_0_022.Text;
        023: Pre_Def := dmkEd_0_023.Text;
        024: Pre_Def := dmkEd_0_024.Text;
        037: Pre_Def := dmkEd_0_037.Text;
        //038: Pre_Def := dmkEd_0_038.Text;
(*
        begin
          Len := Geral.IMV(ListaA[i][_Tam]);
          if not ConcatenaAgenciaEConta(dmkEd_0_020.Text, dmkEd_0_021.Text,
            Len, Pre_Def) then Exit;
        end;
*)
        402: Pre_Def := dmkEd_0_402.Text;
        410: Pre_Def := dmkEd_0_410.Text;
        699: Pre_Def := dmkEd_0_699.Text;
        801:
        begin
          Res := dmkEd_0_801.Text;
          //
          UBco_Rem.TipoDeInscricao(Geral.IMV(Res), FBanco, GetCNAB(),
            Campo, NomeLayout, Res);
          //
          Pre_Def := Res;
        end;
        802: Pre_Def := dmkEd_0_802.Text;
        891: Pre_Def := dmkEd_0_891.Text;
        990: Pre_Def := dmkEd_0_990.Text;
        991: Pre_Def := dmkEd_0_991.Text;
        998:
        begin
          ObtemNumeroDoArquivo(Pre_Def);
        end;
        999: Pre_Def := NovaLinhaArq();
        else
        begin
  {
          Geral.MB_Aviso('Campo de ID = ' + Geral.FF0(Campo) +
            ' n�o implementado no registro HEADER de Remassa CNAB!');
  }
          Pre_def := '';
        end;
      end;
      if not ValorDeCampo(ListaA, i, Pre_Def, ValCampo) then Exit;
      //
      LinReg := LinReg + ValCampo;
      if Length(LinReg) <> Geral.IMV(ListaA[i][_Fim]) then
      begin
        AvisaCancelamento(LinReg, ListaA, i, Length(LinReg));
        Exit;
      end;
    end;
    if LinReg <> '' then
      MeGerado.Lines.Add(LinReg);
    //
    MeGerado.Text := RemoveQuebraDeLinha(MeGerado.Text);
    LinReg := '';
    ////////////////////// 1 - REGISTRO DETALHE OBRIGAT�RIO  /////////////////////
    if not CriaListaCNAB(CNAB, Banco, 1, Posicoes, Segmento, ListaA, NomeLayout) then Exit;
    ////////////////////// 2 - REGISTRO MENSAGEM (OPCIONAL)  /////////////////////
    if (CNAB = 400) then
    begin
      case Banco of
        008, 033, 237, 353, 756:
        begin
          if (Banco = 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BRADESCO_2015) then Exit;
          //
          if not CriaListaCNAB(CNAB, Banco, 2, Posicoes, Segmento, ListaC, NomeLayout) then Exit;
        end;
      end;
    end;
    ////////////////////// 3 - REGISTRO DETALHE (OPCIONAL) ///////////////////////
    if (CNAB = 240) then
    begin
      case Banco of
        001, 756:
        begin
          if (Banco = 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015)
            and (NomeLayout <> CO_756_240_2018) then Exit;
          //
          Segmento := 'P';
          if not CriaListaCNAB(CNAB, Banco, 3, Posicoes, Segmento, ListaP, NomeLayout) then Exit;
        end;
      end;
    end;
    if (CNAB = 240) then
    begin
      case Banco of
        001, 756:
        begin
          if (Banco = 756) and (NomeLayout <> CO_756_CORRESPONDENTE_BB_2015)
            and (NomeLayout <> CO_756_240_2018) then Exit;
          //
          Segmento := 'Q';
          if not CriaListaCNAB(CNAB, Banco, 3, Posicoes, Segmento, ListaQ, NomeLayout) then Exit;
        end;
      end;
    end;
    if (CNAB = 240) then
    begin
      case Banco of
        756:
        begin
          if (Banco = 756) and (NomeLayout = CO_756_240_2018) then
          begin
            Segmento := 'R';
            if not CriaListaCNAB(CNAB, Banco, 3, Posicoes, Segmento, ListaR, NomeLayout) then Exit;
            Segmento := 'S';
            if not CriaListaCNAB(CNAB, Banco, 3, Posicoes, Segmento, ListaS, NomeLayout, 3) then Exit;
          end;
        end;
      end;
    end;
    ////////////////////// 5 - REGISTRO DETALHE (OPCIONAL) ///////////////////////
    Segmento := #0;
    //
    if (CNAB = 400) then
    begin
      case Banco of
        341:
        begin
          if not CriaListaCNAB(CNAB, Banco, 5, Posicoes, Segmento, ListaB, NomeLayout) then Exit;
        end;
      end;
    end else
    if (NomeLayout = CO_756_240_2018) then
    begin
      FLinArq2 := 0;
      //
      if not CriaListaCNAB(CNAB, Banco, 5, Posicoes, Segmento, ListaB, NomeLayout) then Exit;
    end;

    for Item := 1 to Grade_1.RowCount - 1 do
    begin
      if (CNAB = 400) or (NomeLayout = CO_756_240_2018) then
      begin
        Lin1 := Geral.IMV(Grade_1.Cells[F1_000, Item]);
        if Lin1 <> Item then
        begin
          Geral.MB_Erro('A linha ' + Geral.FF0(Item) + ' da grade 1 ' +
            ' n�o � "Item" = ' + Geral.FF0(Item) + '!' + sLineBreak + 'Gera��o de arquivo ' +
            'cancelada!');
          Exit;
        end;

        //  Grade 1 (Detalhe obrigat�rio)
        if (NomeLayout <> CO_756_240_2018) or (Item = 1) then
        begin
          for i := Low(ListaA) to High(ListaA) do
          begin
            if not ValorDeColunaDeGrade(Grade_1, ListaA, i, FCols1, Item, NomeLayout, Pre_Def) then Exit;
            if not ValorDeCampo(ListaA, i, Pre_Def, ValCampo) then Exit;
            //
            LinReg := LinReg + ValCampo;
            QtdL := Length(LinReg);
            QtdA := Geral.IMV(ListaA[i][_Fim]);
            if QtdL <> QtdA then
            begin
              AvisaCancelamento(LinReg, ListaA, i, QtdL);
              Exit;
            end;
          end;
          if LinReg <> '' then
            MeGerado.Lines.Add(LinReg);
          //
          LinReg := '';
        end;
      end;

      // Grade 2 (Mensagem opcional)
      Lin1 := Geral.IMV(Grade_2.Cells[F2_000, Item]);
      if Lin1 <> Item then
      begin
        Geral.MB_Erro('A linha ' + Geral.FF0(Item) + ' da grade 2 ' +
          ' n�o � "Item" = ' + Geral.FF0(Item) + '!' + sLineBreak + 'Gera��o de arquivo ' +
          'cancelada!');
        Exit;
      end;
      for i := Low(ListaC) to High(ListaC) do
      begin
        if not ValorDeColunaDeGrade(Grade_2, ListaC, i, FCols2, Item, NomeLayout, Pre_Def) then Exit;
        if not ValorDeCampo(ListaC, i, Pre_Def, ValCampo) then Exit;
        //
        LinReg := LinReg + ValCampo;
        QtdL := Length(LinReg);
        QtdA := Geral.IMV(ListaC[i][_Fim]);
        if QtdL <> QtdA then
        begin
          Geral.MB_Aviso('QtdL = ' + Geral.FF0(QtdL) + ' QtdA = ' + Geral.FF0(QtdA));
          AvisaCancelamento(LinReg, ListaC, i, QtdL);
          Exit;
        end;
      end;
      if LinReg <> '' then
        MeGerado.Lines.Add(LinReg);
      //
      LinReg := '';

      //  Grade 3 (Detalhe obrigat�rio)
      //  Segmento P
      for i := Low(ListaP) to High(ListaP) do
      begin
        if not ValorDeColunaDeGrade(Grade_240_3_P, ListaP, i, FCols3P, Item, NomeLayout, Pre_Def) then Exit;
        if not ValorDeCampo(ListaP, i, Pre_Def, ValCampo) then Exit;
        //
        LinReg := LinReg + ValCampo;
        QtdL := Length(LinReg);
        QtdA := Geral.IMV(ListaP[i][_Fim]);
        if QtdL <> QtdA then
        begin
          AvisaCancelamento(LinReg, ListaP, i, QtdL);
          Exit;
        end;
      end;
      if LinReg <> '' then
        MeGerado.Lines.Add(LinReg);
      //
      LinReg := '';

      //  Grade 3 (Detalhe obrigat�rio)
      //  Segmento Q
      for i := Low(ListaQ) to High(ListaQ) do
      begin
        if not ValorDeColunaDeGrade(Grade_240_3_Q, ListaQ, i, FCols3Q, Item, NomeLayout, Pre_Def) then Exit;
        if not ValorDeCampo(ListaQ, i, Pre_Def, ValCampo) then Exit;
        //
        LinReg := LinReg + ValCampo;
        QtdL := Length(LinReg);
        QtdA := Geral.IMV(ListaQ[i][_Fim]);
        if QtdL <> QtdA then
        begin
          AvisaCancelamento(LinReg, ListaQ, i, QtdL);
          Exit;
        end;
      end;
      if LinReg <> '' then
        MeGerado.Lines.Add(LinReg);
      //
      LinReg := '';

      //  Grade 3 (Detalhe obrigat�rio)
      //  Segmento R
      for i := Low(ListaR) to High(ListaR) do
      begin
        if not ValorDeColunaDeGrade(Grade_240_3_R, ListaR, i, FCols3R, Item, NomeLayout, Pre_Def) then Exit;
        if not ValorDeCampo(ListaR, i, Pre_Def, ValCampo) then Exit;
        //
        LinReg := LinReg + ValCampo;
        QtdL   := Length(LinReg);
        QtdA   := Geral.IMV(ListaR[i][_Fim]);
        //
        if QtdL <> QtdA then
        begin
          AvisaCancelamento(LinReg, ListaR, i, QtdL);
          Exit;
        end;
      end;
      if LinReg <> '' then
        MeGerado.Lines.Add(LinReg);
      //
      LinReg := '';


      //  Grade 3 (Detalhe obrigat�rio)
      //  Segmento S
      for i := Low(ListaS) to High(ListaS) do
      begin
        if not ValorDeColunaDeGrade(Grade_240_3_S, ListaS, i, FCols3S, Item, NomeLayout, Pre_Def) then Exit;
        if not ValorDeCampo(ListaS, i, Pre_Def, ValCampo) then Exit;
        //
        LinReg := LinReg + ValCampo;
        QtdL   := Length(LinReg);
        QtdA   := Geral.IMV(ListaS[i][_Fim]);
        //
        if QtdL <> QtdA then
        begin
          AvisaCancelamento(LinReg, ListaS, i, QtdL);
          Exit;
        end;
      end;
      if LinReg <> '' then
        MeGerado.Lines.Add(LinReg);
      //
      LinReg := '';

      // Grade 5 (detalhe opcional) (Banco 341 tem mais algum?)
      // primeiro verifica se existe um detalhe 5 referente ao detalhe 1
      for j := 1 to Grade_5.RowCount -1 do
      begin
        if (NomeLayout = CO_756_240_2018) and (j < (Grade_5.RowCount -1)) then
          Continue;
        //
        if Geral.IMV(Grade_5.Cells[F5_000, j]) = Lin1 then
        begin
          for i := Low(ListaB) to High(ListaB) do
          begin
            FLinArq2 := 0;
            //
            if not ValorDeColunaDeGrade(Grade_5, ListaB, i, FCols5, Item, NomeLayout, Pre_Def) then Exit;
            if not ValorDeCampo(ListaB, i, Pre_Def, ValCampo) then Exit;
            //
            LinReg := LinReg + ValCampo;
            if Length(LinReg) <> Geral.IMV(ListaB[i][_Fim]) then
            begin
              AvisaCancelamento(LinReg, ListaB, i, Length(LinReg));
              Exit;
            end;
          end;
          if LinReg <> '' then
            MeGerado.Lines.Add(LinReg);
          //
          MeGerado.Text := RemoveQuebraDeLinha(MeGerado.Text);
          LinReg := '';
        end;
      end;
    end;
    ////////////////////// 9 - REGISTRO TRAILLER  ////////////////////////////////
    if not CriaListaCNAB(CNAB, Banco, 9, Posicoes, Segmento, ListaA, NomeLayout) then Exit;
    for i := Low(ListaA) to High(ListaA) do
    begin
      Campo := Geral.IMV(ListaA[i][_Fld]);
      case Campo of
        -02: Pre_Def := ' ';
        -01: Pre_def := '0';
        //
        //
        302: //Pre_def := Geral.SoNumero_TT(dmkEd_9_302.Text);
            Pre_def := dmkEd_9_302.Text;
        993:
          if (Banco = 756) and (NomeLayout = CO_756_240_2018) then
          begin
            if JaFez999do9 then
              Pre_def := FormatFloat('0', FLinArq)
            else
              Pre_def := FormatFloat('0', FLinArq + 1);
          end;
        994:
        begin
          //O banco 756 BB => N�o tem o registro 999 no Header
          if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
            NovaLinhaArq();
          //
          if JaFez999do9 then
            Pre_def := FormatFloat('0', FLinArq)
          else
            Pre_def := FormatFloat('0', FLinArq + 1);
        end;
        999:
        begin
          JaFez999do9 := True;
          Pre_Def := NovaLinhaArq();
        end;
        else Pre_def := '';
      end;
      if not ValorDeCampo(ListaA, i, Pre_Def, ValCampo) then Exit;
      //
      LinReg := LinReg + ValCampo;
      if Length(LinReg) <> Geral.IMV(ListaA[i][_Fim]) then
      begin
        AvisaCancelamento(LinReg, ListaA, i, Length(LinReg));
        Exit;
      end;
    end;
    if LinReg <> '' then
      MeGerado.Lines.Add(LinReg);
    //
    LinReg := '';
    //
    ObtemNumeroDoArquivo(SeqArq);
    //
    if (Banco = 756) and (NomeLayout = CO_756_CORRESPONDENTE_BB_2015) then
    begin
      DataLote := Date;
      //
      ObtemSubNumeroDoArquivo(SeqArq, DataLote);
      //
      DecodeDate(DataLote, Ano, Mes, Dia);
      //
      NomeArq := 'CBR' + FormatFloat('0000', Ano) + FormatFloat('00', Mes) +
                   FormatFloat('00', Dia) + FormatFloat('00', Geral.IMV(SeqArq));
    end else
      NomeArq := FormatFloat('0000000', Geral.IMV(SeqArq));
    //
    Arquivo := dmkPF.CaminhoArquivo(DmBco.QrCNAB_CfgDiretorio.Value, NomeArq, 'REM');
    if FileExists(Arquivo) then
      Continua := Geral.MB_Pergunta('O arquivo "' + Arquivo +
        ' j� existe. Deseja sobrescrev�-lo?') = ID_YES
    else
      Continua := True;
    if Continua then
    begin
      MeGerado.Text := RemoveQuebraDeLinha(MeGerado.Text);
      // 2012-05-01
      {mudado de:
      if Geral.SalvaTextoEmArquivo(Arquivo, MeGerado.Text, True) then
      para:}
      FileEnd := UBco_Rem.FileEnd(Banco, CNAB, NomeLayout);
      if MyObjects.ExportaMemoToFileExt(MeGerado, Arquivo, False, False,
        etxtsaSemAcento, 1310, FileEnd) then
      // Fim 2012-05-01
      begin
        EdArqGerado.Text             := Arquivo;
        PageControl1.ActivePageIndex := 0;
        PageControl2.ActivePageIndex := 0;
        //
        Geral.MB_Aviso('O arquivo remessa foi salvo como: ' + sLineBreak + Arquivo);
        //
        if Geral.MB_Pergunta('Deseja abrir o diret�rio?') = ID_YES then
          Geral.AbreArquivo(Arquivo);
        //
        BtMover.Visible := True;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCNAB_Rem.MeGeradoEnter(Sender: TObject);
begin
  FEditor := MeGerado;
end;

procedure TFmCNAB_Rem.MeGeradoExit(Sender: TObject);
begin
  FEditor := nil;
end;

procedure TFmCNAB_Rem.MeComparEnter(Sender: TObject);
begin
  FEditor := MeCompar;
end;

procedure TFmCNAB_Rem.MeComparExit(Sender: TObject);
begin
  FEditor := nil;
end;

procedure TFmCNAB_Rem.SpeedButton1Click(Sender: TObject);
begin
  if OpenDialog1.Execute then
  begin
    MeCompar.Lines.LoadFromFile(OpenDialog1.FileName);
    EdArqCompar.Text := OpenDialog1.FileName;
  end;
end;

function TFmCNAB_Rem.GetCNAB: Integer;
begin
  Result := DmBco.QrCNAB_CfgCNAB.Value;
end;

procedure TFmCNAB_Rem.GradeGDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  OldAlign: Integer;
  CorFundo, CorTexto: TColor;
begin
  if GradeG.Cells[ACol,ARow] <> GradeO.Cells[ACol,ARow] then
  begin
    CorFundo := clBlack;
    CorTexto := clWhite;
  end else begin
    CorFundo := clWhite;
    CorTexto := clBlack;
  end;
  GradeG.Canvas.Brush.Color := CorFundo;
  //FillRect(Rect);
  GradeG.Canvas.Font.Color := CorTexto;
  OldAlign := SetTextAlign(GradeG.Canvas.Handle, TA_LEFT);
  GradeG.Canvas.TextRect(Rect, Rect.Left, Rect.Top,
    GradeG.Cells[Acol, ARow]);
  SetTextAlign(GradeG.Canvas.Handle, OldAlign);
end;

procedure TFmCNAB_Rem.GradeGSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  VerificaCelula(Acol, ARow);
end;

procedure TFmCNAB_Rem.VerificaCelula(Col, Row: Integer);
var
  Ini, Fim, Campo: Integer;
  Pos, ValGera, ValComp: String;
begin
  Campo := Geral.IMV(GradeC.Cells[Col, Row]);
  Ini   := Geral.IMV(GradeI.Cells[Col, Row]);
  Fim   := Geral.IMV(GradeF.Cells[Col, Row]);
  Pos   := FormatFloat('000', Ini) + ' a ' +
           FormatFloat('000', Fim) + ' = ' +
           FormatFloat('000', Fim - Ini + 1);
  ValGera := Copy(MeGerado.Lines[Row], Ini, Fim - Ini + 1);
  ValComp := Copy(MeCompar.Lines[Row], Ini, Fim - Ini + 1);
  dmkEdVerifPos.Text                := Pos;
  dmkEdVerifCol.ValueVariant        := Col + 1;
  dmkEdVerifLin.ValueVariant        := Row + 1;
  dmkEdVerifCampoCod.ValueVariant   := Campo;
  dmkEdVerifCampoTxt.Text           := UBancos.DescricaoDoMeuCodigoCNAB(Campo);
  dmkEdVerifValGerado.Text          := Trim(ValGera);
  dmkEdVerifValCompar.Text          := Trim(ValComp);
end;

procedure TFmCNAB_Rem.BitBtn1Click(Sender: TObject);
var
  i, n, Col, Lin, NewReg, OldReg, Ini, Fim, Este: Integer;
  NomeLayout, Campo: String;
  Lista: MyArrayLista;
  CNAB: Integer;
  Segmento: Char;
const
  Posicoes = 0;
begin
  Screen.Cursor := crHourGlass;
  try
    Segmento         := #0;
    NomeLayout := DmBco.QrCNAB_CfgLayoutRem.Value;
    Ini := 0;
    Fim := 0;
    CNAB := DmBco.QrCNAB_CfgCNAB.Value;
    MyObjects.LimpaGrade(GradeG, 0, 0, True);
    MyObjects.LimpaGrade(GradeO, 0, 0, True);
    MyObjects.LimpaGrade(GradeC, 0, 0, True);
    //
    n := CNAB;
    if n <= 0 then
    begin
      Geral.MB_Erro('Quantidade de carateres n�o definida no arquivo!');
      Exit;
    end;
    GradeG.ColCount := n; // Arquivo gerado
    GradeO.ColCount := n; // Arquivo a comparar
    GradeC.ColCount := n; // Campos
    GradeI.ColCount := n; // In�cio do campo
    GradeF.ColCount := n; // Fim do campo
    //
    if MeGerado.Lines.Count <> MeCompar.Lines.Count then
    begin
      Geral.MB_Aviso('Quantidade de linhas n�o conferem!');
    end;
    n := MeGerado.Lines.Count;
    if n < MeCompar.Lines.Count then n := MeCompar.Lines.Count;
    if n = 0 then Exit;
    //
    GradeG.RowCount := n;
    GradeO.RowCount := n;
    GradeC.RowCount := n;
    GradeI.RowCount := n;
    GradeF.RowCount := n;
    OldReg := -1;
    PB1.Position := 0;
    PB1.Max := n;
    for Lin := 0 to n-1 do
    begin
      if MeGerado.Lines.Count <= Lin then
      begin
        // Evitar erro
        Exit;
      end;
      PB1.Position := PB1.Position + 1;
      NewReg := Geral.IMV(MeGerado.Lines[Lin][1]);
      if NewReg <> OldReg then
      begin
        OldReg := NewReg;
        CriaListaCNAB(CNAB, FBanco, NewReg, Posicoes, Segmento, Lista, NomeLayout);
      end;
      for Col := 0 to Length(MeGerado.Lines[Lin])-1 do
      begin
        Campo := '';
        for i := Low(Lista) to High(Lista) do
        begin
          Este := Col + 1;
          Ini := Geral.IMV(Lista[i][_Ini]);
          Fim := Geral.IMV(Lista[i][_Fim]);
          if (Este >= Ini) and (Este <= Fim) then
          begin
            Campo := Lista[i][_Fld];
            Break;
          end;
        end;
        GradeC.Cells[Col, Lin] := Campo;
        GradeI.Cells[Col, Lin] := FormatFloat('000', Ini);
        GradeF.Cells[Col, Lin] := FormatFloat('000', Fim);
        GradeO.Cells[Col, Lin] := MeCompar.Lines[Lin][Col + 1];
        GradeG.Cells[Col, Lin] := MeGerado.Lines[Lin][Col + 1];
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmCNAB_Rem.DAC_NossoNumero(const Bloqueto: Double; var Res: String): Boolean;
(*
var
  NossoNum_TXT, NossoNum_REM: String;
*)
begin
  Result := UBancos.DACNossoNumero(
              DmBco.QrCNAB_CfgModalCobr.Value,
              DmBco.QrCNAB_CfgCedBanco.Value,
              DmBco.QrCNAB_CfgCedAgencia.Value,
              DmBco.QrCNAB_CfgCedPosto.Value, Bloqueto,
              DmBco.QrCNAB_CfgCedConta.Value,
              DmBco.QrCNAB_CfgCartNum.Value,
              DmBco.QrCNAB_CfgIDCobranca.Value,
              Geral.SoNumero_TT(DmBco.QrCNAB_CfgCodEmprBco.Value),
              DmBco.QrCNAB_CfgTipoCobranca.Value,
              DmBco.QrCNAB_CfgEspecieDoc.Value,
              DmBco.QrCNAB_CfgCNAB.Value,
              DmBco.QrCNAB_CfgCtaCooper.Value,
              DmBco.QrCNAB_CfgLayoutRem.Value,
              Res);
end;

function TFmCNAB_Rem.ObtemSubNumeroDoArquivo(var Res: String; var DataLote: TDateTime): Boolean;
var
  DataCad: TDateTime;
begin
  Res := '0';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT DataCad ',
    'FROM protocopak ',
    'WHERE Controle=' + Geral.FF0(FLote),
    '']);
  DataCad := Dmod.QrAux.FieldByName('DataCad').AsDateTime;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT COUNT(Controle) Reg ',
    'FROM protocopak',
    'WHERE DataCad="' + Geral.FDT(DataCad, 01) + '"',
    'GROUP BY Controle',
    '']);
  if Dmod.QrAux.FieldByName('Reg').AsInteger = 1 then
    Res := Geral.FF0(Dmod.QrAux.FieldByName('Reg').AsInteger)
  else
    Res := Geral.FF0(Dmod.QrAux.FieldByName('Reg').AsInteger + 1);
  //
  DataLote := DataCad;
end;

function TFmCNAB_Rem.ObtemNumeroDoArquivo(var Res: String): Boolean;
begin
  //Result := False;
  if FSeqArqRem = 0 then
  begin
    FSeqArqRem := DmBco.QrCNAB_CfgSeqArq.Value + 1;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE cnab_cfg SET SeqArq=:P0');
    Dmod.QrUpd.SQL.Add('WHERE CedBanco=:P1 AND CedAgencia=:P2 AND CedConta=:P3');
    Dmod.QrUpd.Params[00].AsInteger := FSeqArqRem;
    Dmod.QrUpd.Params[01].AsInteger := DmBco.QrCNAB_CfgCedBanco.Value;
    Dmod.QrUpd.Params[02].AsInteger := DmBco.QrCNAB_CfgCedAgencia.Value;
    Dmod.QrUpd.Params[03].AsString  := DmBco.QrCNAB_CfgCedConta.Value;
    Dmod.QrUpd.ExecSQL;
    //
    DmBco.QrCNAB_Cfg.Close;
    DmBco.QrCNAB_Cfg.Open;
  end;
  if (CO_DMKID_APP <> 3) and (CO_DMKID_APP <> 22) then //Creditor Credito2
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE protocopak SET SeqArq=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Dmod.QrUpd.Params[00].AsInteger := FSeqArqRem;
    Dmod.QrUpd.Params[01].AsInteger := FLote;
    Dmod.QrUpd.ExecSQL;
  end;
  //
  Res := FormatFloat('0', FSeqArqRem);
  Result := True;
end;

end.

