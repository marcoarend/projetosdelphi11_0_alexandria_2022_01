unit CNAB_RetBcoEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel, dmkGeral,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkImage, UnDmkEnums;

type
  TFmCNAB_RetBcoEdit = class(TForm)
    Panel1: TPanel;
    EdValJuros: TdmkEditCB;
    Label1: TLabel;
    Label2: TLabel;
    EdValMulta: TdmkEditCB;
    Label3: TLabel;
    EdValOutrC: TdmkEditCB;
    Label4: TLabel;
    EdValJuMul: TdmkEditCB;
    Label5: TLabel;
    EdValTarif: TdmkEditCB;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmCNAB_RetBcoEdit: TFmCNAB_RetBcoEdit;

implementation

uses UnMyObjects, ModuleGeral, CNAB_RetBco, UMySQLModule;

{$R *.DFM}

procedure TFmCNAB_RetBcoEdit.BtOKClick(Sender: TObject);
var
  ValJuros, ValMulta, ValOutrC, ValJuMul, ValTarif, ValErro, ValTitul, ValDesco,
  ValCBrut, ValAbati, ValEPago: Double;
  Banco, Entidade, Conta: Integer;
  ID_Link, OcorrCod, NossoNum: String;
begin
  ValErro   := 0;
  ValJuros  := EdValJuros.ValueVariant;
  ValMulta  := EdValMulta.ValueVariant;
  ValOutrC  := EdValOutrC.ValueVariant;
  ValJuMul  := EdValJuMul.ValueVariant;
  ValTarif  := EdValTarif.ValueVariant;
  Conta     := FmCNAB_RetBco.QrBcoRegConta.Value;
  Entidade  := FmCNAB_RetBco.QrBcoRegEntidade.Value;
  Banco     := FmCNAB_RetBco.QrBcoRegBanco.Value;
  ID_Link   := FmCNAB_RetBco.QrBcoRegID_Link.Value;
  OcorrCod  := FormatFloat('0', FmCNAB_RetBco.QrBcoRegOcorrCod.Value);
  NossoNum  := FmCNAB_RetBco.QrBcoRegNossoNum.Value;
  ValTitul  := FmCNAB_RetBco.QrBcoRegValTitul.Value;
  ValDesco  := FmCNAB_RetBco.QrBcoRegValDesco.Value;
  ValCBrut  := FmCNAB_RetBco.QrBcoRegValCBrut.Value;
  ValAbati  := FmCNAB_RetBco.QrBcoRegValAbati.Value;
  ValEPago  := FmCNAB_RetBco.QrBcoRegValEPago.Value;
  //
  ValErro  := FmCNAB_RetBco.ErroLinha(
              Entidade, Banco, ID_Link, OcorrCod, NossoNum, False,
              ValTitul, ValAbati, ValDesco, ValEPago, ValJuros, ValMulta,
              ValJuMul, ValTarif, ValErro, ValOutrC, ValCBrut);
  //
  UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, 'cnabbcoreg', False, [
  'ValJuros', 'ValMulta', 'ValOutrC',
  'ValJuMul', 'ValTarif', 'ValErro'], [
  'Conta'], [
  ValJuros, ValMulta, ValOutrC,
  ValJuMul, ValTarif, ValErro], [
  Conta], False);
  //
  //
  FmCNAB_RetBco.ReopenBcoReg(Conta);
  Close;
end;

procedure TFmCNAB_RetBcoEdit.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCNAB_RetBcoEdit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCNAB_RetBcoEdit.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmCNAB_RetBcoEdit.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
