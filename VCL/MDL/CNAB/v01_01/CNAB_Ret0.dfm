object FmCNAB_Ret: TFmCNAB_Ret
  Left = 330
  Top = 169
  Caption = 'Retorno de Arquivos CNAB'
  ClientHeight = 539
  ClientWidth = 931
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCarrega: TPanel
    Left = 0
    Top = 48
    Width = 931
    Height = 491
    Align = alClient
    Color = clHighlight
    TabOrder = 1
    ExplicitWidth = 939
    ExplicitHeight = 493
    object PainelConfirma: TPanel
      Left = 1
      Top = 442
      Width = 929
      Height = 48
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 444
      ExplicitWidth = 937
      object PB1: TProgressBar
        Left = 364
        Top = 20
        Width = 301
        Height = 17
        TabOrder = 4
        Visible = False
      end
      object BtCarrega: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Carrega'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtCarregaClick
      end
      object Panel2: TPanel
        Left = 825
        Top = 1
        Width = 111
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object Button1: TButton
        Left = 568
        Top = 16
        Width = 75
        Height = 25
        Caption = 'Button1'
        TabOrder = 2
        Visible = False
        OnClick = Button1Click
      end
      object BtAbertos: TBitBtn
        Tag = 10017
        Left = 260
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Abertos'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtAbertosClick
      end
      object CkReverter: TCheckBox
        Left = 116
        Top = 20
        Width = 85
        Height = 17
        Caption = 'Re-registrar.'
        TabOrder = 5
      end
    end
    object PnArquivos: TPanel
      Left = 1
      Top = 1
      Width = 776
      Height = 408
      BevelOuter = bvNone
      TabOrder = 1
      object Splitter3: TSplitter
        Left = 0
        Top = 277
        Width = 776
        Height = 5
        Cursor = crVSplit
        Align = alTop
      end
      object ListBox1: TListBox
        Left = 196
        Top = 52
        Width = 453
        Height = 29
        ItemHeight = 13
        TabOrder = 0
        Visible = False
      end
      object MemoTam: TMemo
        Left = 0
        Top = 370
        Width = 776
        Height = 38
        Align = alBottom
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Lucida Console'
        Font.Style = []
        Lines.Strings = (
          'MemoTam')
        ParentFont = False
        TabOrder = 1
        Visible = False
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 277
        Align = alTop
        BevelOuter = bvNone
        Caption = 'Panel1'
        TabOrder = 2
        object Splitter1: TSplitter
          Left = 0
          Top = 125
          Width = 776
          Height = 5
          Cursor = crVSplit
          Align = alTop
        end
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 776
          Height = 125
          Align = alTop
          DataSource = DsCNAB_Dir
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Dir'
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECARTEIRA'
              Title.Caption = 'Carteira'
              Width = 305
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Banco1'
              Title.Caption = 'Bco'
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Diret'#243'rio dos arquivos retorno'
              Width = 492
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEBANCO'
              Title.Caption = 'Nome do banco'
              Width = 279
              Visible = True
            end>
        end
        object GradeA: TStringGrid
          Left = 0
          Top = 130
          Width = 776
          Height = 147
          Align = alClient
          ColCount = 12
          DefaultColWidth = 32
          DefaultRowHeight = 18
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goRowSelect]
          TabOrder = 1
          OnDrawCell = GradeADrawCell
          OnSelectCell = GradeASelectCell
          ColWidths = (
            32
            160
            272
            112
            104
            44
            36
            28
            64
            40
            28
            36)
        end
      end
      object PageControl1: TPageControl
        Left = 0
        Top = 282
        Width = 776
        Height = 88
        ActivePage = TabSheet3
        Align = alClient
        TabOrder = 3
        object TabSheet3: TTabSheet
          Caption = 'Itens de todos arquivos lidos'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Grade1: TStringGrid
            Left = 0
            Top = 0
            Width = 768
            Height = 60
            Align = alClient
            ColCount = 29
            DefaultColWidth = 32
            DefaultRowHeight = 18
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
            TabOrder = 0
            OnDrawCell = Grade1DrawCell
            OnSelectCell = Grade1SelectCell
            ColWidths = (
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32)
          end
        end
        object TabSheet1: TTabSheet
          Caption = 'Avisos'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Memo1: TMemo
            Left = 0
            Top = 0
            Width = 768
            Height = 60
            Align = alClient
            TabOrder = 0
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Opera'#231#245'es'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Memo2: TMemo
            Left = 0
            Top = 0
            Width = 768
            Height = 60
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
        end
      end
    end
    object StaticText1: TStaticText
      Left = 1
      Top = 425
      Width = 929
      Height = 17
      Align = alBottom
      Alignment = taCenter
      Caption = 
        '  Antes de carregar verifique se todas ocorr'#234'ncias j'#225' est'#227'o cada' +
        'stradas nos respectivos bancos (Cadastro de bancos).'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      ExplicitWidth = 677
    end
  end
  object PnMovimento: TPanel
    Left = 0
    Top = 48
    Width = 931
    Height = 491
    Align = alClient
    BevelOuter = bvNone
    Color = clAppWorkSpace
    TabOrder = 2
    Visible = False
    ExplicitWidth = 939
    ExplicitHeight = 493
    object Panel3: TPanel
      Left = 0
      Top = 443
      Width = 931
      Height = 48
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 445
      ExplicitWidth = 939
      object Label1: TLabel
        Left = 196
        Top = 2
        Width = 133
        Height = 13
        Caption = '* Diferen'#231'a de juros e multa:'
      end
      object Label2: TLabel
        Left = 204
        Top = 16
        Width = 137
        Height = 13
        Caption = 'Positivo (azul): pagou a mais.'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object Label3: TLabel
        Left = 204
        Top = 30
        Width = 177
        Height = 13
        Caption = 'Negativo (vermelho): pagou a menos.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Panel4: TPanel
        Left = 827
        Top = 1
        Width = 111
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 5
        object BitBtn2: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sair'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object Button2: TButton
        Left = 344
        Top = 4
        Width = 75
        Height = 25
        Caption = 'Button1'
        TabOrder = 6
        Visible = False
        OnClick = Button1Click
      end
      object BtExclui: TBitBtn
        Tag = 10039
        Left = 514
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Bloqueto'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtExcluiClick
      end
      object BtConcilia: TBitBtn
        Tag = 10011
        Left = 9
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Conciliar'
        Enabled = False
        TabOrder = 0
        OnClick = BtConciliaClick
      end
      object BitBtn1: TBitBtn
        Tag = 10013
        Left = 102
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Voltar'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BitBtn1Click
      end
      object BtAgenda: TBitBtn
        Tag = 10032
        Left = 422
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Agenda'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtAgendaClick
      end
      object BtItens: TBitBtn
        Tag = 10038
        Left = 606
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Lan'#231'to'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = BtItensClick
      end
    end
    object PnCarregados: TPanel
      Left = 0
      Top = 0
      Width = 931
      Height = 333
      Align = alTop
      TabOrder = 1
      ExplicitWidth = 939
      object Splitter2: TSplitter
        Left = 1
        Top = 161
        Width = 929
        Height = 3
        Cursor = crVSplit
        Align = alBottom
        ExplicitWidth = 937
      end
      object Panel5: TPanel
        Left = 1
        Top = 164
        Width = 929
        Height = 168
        Align = alBottom
        Caption = 'Panel5'
        TabOrder = 0
        ExplicitWidth = 937
        object PageControl4: TPageControl
          Left = 1
          Top = 1
          Width = 927
          Height = 166
          ActivePage = TabSheet6
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 935
          object TabSheet6: TTabSheet
            Caption = ' Sem agrupamento'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid2: TDBGrid
              Left = 0
              Top = 0
              Width = 927
              Height = 138
              Align = alClient
              DataSource = DsLeiItens
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEPROPRIET'
                  Title.Caption = 'Cond'#244'mino'
                  Width = 233
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'UH'
                  Width = 45
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Mez_TXT'
                  Title.Caption = 'Mes'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Credito'
                  Title.Caption = 'Cr'#233'dito'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Descricao'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 216
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Vencimento'
                  Title.Caption = 'Vencimen.'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Carteira'
                  Width = 40
                  Visible = True
                end>
            end
          end
          object TabSheet7: TTabSheet
            Caption = ' Com agrupamento '
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGrid3: TDBGrid
              Left = 0
              Top = 0
              Width = 927
              Height = 138
              Align = alClient
              DataSource = DsLeiAgr
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEPROPRIET'
                  Title.Caption = 'Cond'#244'mino'
                  Width = 233
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'UH'
                  Width = 45
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Mez_TXT'
                  Title.Caption = 'M'#234's'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME_TIPO_BLOQ'
                  Title.Caption = 'Tipo de bloqueto'
                  Width = 180
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Valor'
                  Title.Caption = 'Cr'#233'dito'
                  Visible = True
                end>
            end
          end
        end
      end
      object PageControl2: TPageControl
        Left = 1
        Top = 1
        Width = 929
        Height = 160
        ActivePage = TabSheet4
        Align = alClient
        TabOrder = 1
        ExplicitWidth = 937
        object TabSheet4: TTabSheet
          Caption = 'Est'#225'vel'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid6: TDBGrid
            Left = 0
            Top = 0
            Width = 929
            Height = 132
            Align = alClient
            DataSource = DsLei
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDrawColumnCell = DBGrid6DrawColumnCell
            Columns = <
              item
                Expanded = False
                FieldName = 'NOME_TIPO_BOL'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Title.Caption = 'Tipo de bloqueto'
                Width = 169
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DJM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Title.Caption = 'DJM*'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEENT'
                Title.Caption = 'Cliente (Entidade)'
                Width = 226
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IDNum'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Banco'
                Title.Caption = 'Bco'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrCodi'
                Title.Caption = 'OC'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrData'
                Title.Caption = 'Data ocor.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VALBOLETO'
                Title.Caption = 'Val.bloq.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValTitul'
                Title.Caption = 'Val.Titulo'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValPago'
                Title.Caption = 'Val.Pago'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuros'
                Title.Caption = 'Val.Juros'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValMulta'
                Title.Caption = 'Val.Multa'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuMul'
                Title.Caption = 'Mul.+Jur.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuitaData'
                Title.Caption = 'Data quit.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DTA_TARIF_TXT'
                Title.Caption = 'Dt.d'#233'b.tar.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValAbati'
                Title.Caption = 'Val.Abatim.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NossoNum'
                Title.Caption = 'Nosso n'#250'm.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SeuNum'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValDesco'
                Title.Caption = 'Val.Desc.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DevJuros'
                Title.Caption = 'Dev.Juros'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DevMulta'
                Title.Caption = 'Dev.Multa'
                Visible = True
              end>
          end
        end
        object TabSheet5: TTabSheet
          Caption = 'Desenvolvimento'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object dmkDBGrid1: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 929
            Height = 132
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'DJM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Title.Caption = 'DJM*'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEENT'
                Title.Caption = 'Cliente (Entidade)'
                Width = 226
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IDNum'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Banco'
                Title.Caption = 'Bco'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrCodi'
                Title.Caption = 'OC'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrData'
                Title.Caption = 'Data ocor.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VALBOLETO'
                Title.Caption = 'Val.bloq.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValTitul'
                Title.Caption = 'Val.Titulo'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValPago'
                Title.Caption = 'Val.Pago'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuros'
                Title.Caption = 'Val.Juros'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValMulta'
                Title.Caption = 'Val.Multa'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuMul'
                Title.Caption = 'Mul.+Jur.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuitaData'
                Title.Caption = 'Data quit.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DTA_TARIF_TXT'
                Title.Caption = 'Dt.d'#233'b.tar.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValAbati'
                Title.Caption = 'Val.Abatim.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NossoNum'
                Title.Caption = 'Nosso n'#250'm.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SeuNum'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValDesco'
                Title.Caption = 'Val.Desc.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DevJuros'
                Title.Caption = 'Dev.Juros'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DevMulta'
                Title.Caption = 'Dev.Multa'
                Visible = True
              end>
            Color = clWindow
            DataSource = DsLei
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'DJM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Title.Caption = 'DJM*'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEENT'
                Title.Caption = 'Cliente (Entidade)'
                Width = 226
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IDNum'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Banco'
                Title.Caption = 'Bco'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrCodi'
                Title.Caption = 'OC'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrData'
                Title.Caption = 'Data ocor.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VALBOLETO'
                Title.Caption = 'Val.bloq.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValTitul'
                Title.Caption = 'Val.Titulo'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValPago'
                Title.Caption = 'Val.Pago'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuros'
                Title.Caption = 'Val.Juros'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValMulta'
                Title.Caption = 'Val.Multa'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuMul'
                Title.Caption = 'Mul.+Jur.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuitaData'
                Title.Caption = 'Data quit.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DTA_TARIF_TXT'
                Title.Caption = 'Dt.d'#233'b.tar.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValAbati'
                Title.Caption = 'Val.Abatim.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NossoNum'
                Title.Caption = 'Nosso n'#250'm.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SeuNum'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValDesco'
                Title.Caption = 'Val.Desc.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DevJuros'
                Title.Caption = 'Dev.Juros'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DevMulta'
                Title.Caption = 'Dev.Multa'
                Visible = True
              end>
          end
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 931
    Height = 48
    Align = alTop
    Caption = 'Retorno de Arquivos CNAB'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    ExplicitWidth = 939
    object Image1: TImage
      Left = 2
      Top = 2
      Width = 935
      Height = 44
      Align = alClient
      Transparent = True
    end
  end
  object QrCNAB_Dir: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCNAB_DirAfterScroll
    SQL.Strings = (
      'SELECT car.Nome NOMECARTEIRA, ban.Nome NOMEBANCO, '
      'car.Banco1, dir.* '
      'FROM cnab_dir dir'
      'LEFT JOIN carteiras car ON car.Codigo=dir.carteira'
      'LEFT JOIN bancos ban ON ban.Codigo=car.Banco1'
      'WHERE dir.Envio=2')
    Left = 8
    Top = 12
    object QrCNAB_DirNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Origin = 'bancos.Nome'
      Size = 100
    end
    object QrCNAB_DirCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cnab240dir.Codigo'
      Required = True
      DisplayFormat = '000'
    end
    object QrCNAB_DirLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'cnab240dir.Lk'
    end
    object QrCNAB_DirDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'cnab240dir.DataCad'
    end
    object QrCNAB_DirDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'cnab240dir.DataAlt'
    end
    object QrCNAB_DirUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'cnab240dir.UserCad'
    end
    object QrCNAB_DirUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'cnab240dir.UserAlt'
    end
    object QrCNAB_DirNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'cnab240dir.Nome'
      Size = 255
    end
    object QrCNAB_DirEnvio: TSmallintField
      FieldName = 'Envio'
      Origin = 'cnab240dir.Envio'
      Required = True
    end
    object QrCNAB_DirNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrCNAB_DirCliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'cnab240dir.CliInt'
      Required = True
    end
    object QrCNAB_DirCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'cnab240dir.Carteira'
      Required = True
    end
    object QrCNAB_DirBanco1: TIntegerField
      FieldName = 'Banco1'
    end
  end
  object DsCNAB_Dir: TDataSource
    DataSet = QrCNAB_Dir
    Left = 36
    Top = 12
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 108
    Top = 236
  end
  object QrDupl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo '
      'FROM cnab_lei'
      'WHERE Banco=:P0'
      'AND NossoNum=:P1'
      'AND SeuNum=:P2'
      'AND OcorrCodi=:P3'
      'AND Arquivo=:P4'
      'AND ItemArq=:P5'
      'AND ValTitul=:P6'
      'AND ValPago=:P7'
      'AND QuitaData=:P8')
    Left = 120
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P7'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P8'
        ParamType = ptUnknown
      end>
    object QrDuplCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrLotesIts_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial'
      'ELSE cli.Nome END NOMECLI, loi.Codigo, loi.Duplicata,'
      
        'loi.CPF, loi.Emitente, loi.Bruto, loi.Desco, loi.Quitado, loi.Va' +
        'lor, '
      'loi.Emissao, loi.DCompra, loi.Vencto, loi.DDeposito, loi.Dias, '
      
        'loi.Data3, loi.Cobranca, loi.Repassado, lot.Lote, lot.Tipo TIPOL' +
        'OTE,'
      'loi.Cliente'
      'FROM lotesits loi'
      'LEFT JOIN lotes     lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades cli ON cli.Codigo=loi.Cliente'
      'WHERE loi.Controle=:P0')
    Left = 92
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLotesIts_NOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrLotesIts_Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLotesIts_Duplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrLotesIts_CPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrLotesIts_Emitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrLotesIts_Bruto: TFloatField
      FieldName = 'Bruto'
      Required = True
    end
    object QrLotesIts_Desco: TFloatField
      FieldName = 'Desco'
      Required = True
    end
    object QrLotesIts_Quitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrLotesIts_Valor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrLotesIts_Emissao: TDateField
      FieldName = 'Emissao'
      Required = True
    end
    object QrLotesIts_DCompra: TDateField
      FieldName = 'DCompra'
      Required = True
    end
    object QrLotesIts_Vencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrLotesIts_DDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
    end
    object QrLotesIts_Dias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrLotesIts_Data3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrLotesIts_Cobranca: TIntegerField
      FieldName = 'Cobranca'
      Required = True
    end
    object QrLotesIts_Repassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
    object QrLotesIts_Lote: TIntegerField
      FieldName = 'Lote'
    end
    object QrLotesIts_TIPOLOTE: TSmallintField
      FieldName = 'TIPOLOTE'
    end
    object QrLotesIts_Cliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
  end
  object QrCNAB240_: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCNAB240_AfterScroll
    OnCalcFields = QrCNAB240_CalcFields
    SQL.Strings = (
      'SELECT CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial'
      'ELSE cli.Nome END NOMECLI, loi.Codigo MeuLote, loi.Duplicata,'
      
        'loi.CPF, loi.Emitente, loi.Bruto, loi.Desco, loi.Quitado, loi.Va' +
        'lor, '
      'loi.Emissao, loi.DCompra, loi.Vencto, loi.DDeposito, loi.Dias, '
      'loi.Data3, loi.Cobranca, loi.Repassado, loi.Cliente,'
      'lot.Lote Bordero,  lot.Tipo TIPOLOTE, ban.Nome NOMEBANCO, cna.* '
      'FROM cnab240 cna'
      'LEFT JOIN bancos ban ON ban.Codigo=cna.Banco'
      'LEFT JOIN lotesits loi ON loi.Controle=cna.Controle'
      'LEFT JOIN lotes     lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades cli ON cli.Codigo=loi.Cliente'
      'WHERE cna.CNAB240L=:P0')
    Left = 148
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNAB240_CNAB240L: TIntegerField
      FieldName = 'CNAB240L'
    end
    object QrCNAB240_CNAB240I: TIntegerField
      FieldName = 'CNAB240I'
    end
    object QrCNAB240_Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCNAB240_Banco: TIntegerField
      FieldName = 'Banco'
    end
    object QrCNAB240_ConfigBB: TIntegerField
      FieldName = 'ConfigBB'
    end
    object QrCNAB240_Lote: TIntegerField
      FieldName = 'Lote'
    end
    object QrCNAB240_Item: TIntegerField
      FieldName = 'Item'
    end
    object QrCNAB240_DataOcor: TDateField
      FieldName = 'DataOcor'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCNAB240_Envio: TSmallintField
      FieldName = 'Envio'
    end
    object QrCNAB240_Movimento: TSmallintField
      FieldName = 'Movimento'
    end
    object QrCNAB240_Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCNAB240_DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCNAB240_DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCNAB240_UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCNAB240_UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCNAB240_NOMEENVIO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEENVIO'
      Size = 10
      Calculated = True
    end
    object QrCNAB240_NOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
    object QrCNAB240_NOMEMOVIMENTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEMOVIMENTO'
      Size = 100
      Calculated = True
    end
    object QrCNAB240_NOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrCNAB240_MeuLote: TIntegerField
      FieldName = 'MeuLote'
    end
    object QrCNAB240_Duplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrCNAB240_CPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrCNAB240_Emitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrCNAB240_Bruto: TFloatField
      FieldName = 'Bruto'
    end
    object QrCNAB240_Desco: TFloatField
      FieldName = 'Desco'
    end
    object QrCNAB240_Quitado: TIntegerField
      FieldName = 'Quitado'
    end
    object QrCNAB240_Valor: TFloatField
      FieldName = 'Valor'
    end
    object QrCNAB240_Emissao: TDateField
      FieldName = 'Emissao'
    end
    object QrCNAB240_DCompra: TDateField
      FieldName = 'DCompra'
    end
    object QrCNAB240_Vencto: TDateField
      FieldName = 'Vencto'
    end
    object QrCNAB240_DDeposito: TDateField
      FieldName = 'DDeposito'
    end
    object QrCNAB240_Dias: TIntegerField
      FieldName = 'Dias'
    end
    object QrCNAB240_Data3: TDateField
      FieldName = 'Data3'
    end
    object QrCNAB240_Cobranca: TIntegerField
      FieldName = 'Cobranca'
    end
    object QrCNAB240_Repassado: TSmallintField
      FieldName = 'Repassado'
    end
    object QrCNAB240_Bordero: TIntegerField
      FieldName = 'Bordero'
    end
    object QrCNAB240_TIPOLOTE: TSmallintField
      FieldName = 'TIPOLOTE'
    end
    object QrCNAB240_DATA3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA3_TXT'
      Size = 25
      Calculated = True
    end
    object QrCNAB240_NOMESTATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 100
      Calculated = True
    end
    object QrCNAB240_NOMETIPOLOTE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPOLOTE'
      Size = 10
      Calculated = True
    end
    object QrCNAB240_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrCNAB240_Custas: TFloatField
      FieldName = 'Custas'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCNAB240_IRTCLB_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'IRTCLB_TXT'
      Size = 100
      Calculated = True
    end
    object QrCNAB240_ValPago: TFloatField
      FieldName = 'ValPago'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCNAB240_ValCred: TFloatField
      FieldName = 'ValCred'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCNAB240_Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrCNAB240_Acao: TIntegerField
      FieldName = 'Acao'
      Required = True
    end
    object QrCNAB240_NOMEACAO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEACAO'
      Size = 3
      Calculated = True
    end
    object QrCNAB240_Sequencia: TIntegerField
      FieldName = 'Sequencia'
      Required = True
    end
    object QrCNAB240_Convenio: TWideStringField
      FieldName = 'Convenio'
    end
    object QrCNAB240_IRTCLB: TWideStringField
      FieldName = 'IRTCLB'
      Size = 10
    end
  end
  object DsCNAB240: TDataSource
    DataSet = QrCNAB240_
    Left = 176
    Top = 12
  end
  object QrSumPg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ad.Juros) Juros, SUM(ad.Desco) Desco, '
      'SUM(ad.Pago) Pago, li.Valor, MAX(ad.Data) MaxData'
      'FROM aduppgs ad'
      'LEFT JOIN lotesits li ON li.Controle=ad.LotesIts'
      'WHERE ad.LotesIts=:P0'
      'GROUP BY ad.LotesIts')
    Left = 204
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumPgJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrSumPgPago: TFloatField
      FieldName = 'Pago'
    end
    object QrSumPgDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrSumPgValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSumPgMaxData: TDateField
      FieldName = 'MaxData'
    end
  end
  object QrOcorreu: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT  lo.Cliente CLIENTELOTE, ob.Nome NOMEOCORRENCIA, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN lotesits li ON oc.LotesIts = li.Controle'
      'LEFT JOIN lotes lo ON lo.Codigo = li.Codigo'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.LotesIts=:P0'
      '')
    Left = 685
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcorreuNOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Size = 50
    end
    object QrOcorreuCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOcorreuLotesIts: TIntegerField
      FieldName = 'LotesIts'
      Required = True
    end
    object QrOcorreuDataO: TDateField
      FieldName = 'DataO'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorreuOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
      Required = True
    end
    object QrOcorreuValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorreuLoteQuit: TIntegerField
      FieldName = 'LoteQuit'
      Required = True
    end
    object QrOcorreuLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorreuDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorreuDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorreuUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorreuUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcorreuTaxaP: TFloatField
      FieldName = 'TaxaP'
      Required = True
    end
    object QrOcorreuTaxaV: TFloatField
      FieldName = 'TaxaV'
      Required = True
    end
    object QrOcorreuPago: TFloatField
      FieldName = 'Pago'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorreuDataP: TDateField
      FieldName = 'DataP'
      Required = True
    end
    object QrOcorreuTaxaB: TFloatField
      FieldName = 'TaxaB'
      Required = True
    end
    object QrOcorreuATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrOcorreuCLIENTELOTE: TIntegerField
      FieldName = 'CLIENTELOTE'
    end
    object QrOcorreuData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrOcorreuStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrOcorreuCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrOcorreuSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrOcorreuATZ_TEXTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ATZ_TEXTO'
      Size = 30
      Calculated = True
    end
  end
  object DsOcorreu: TDataSource
    DataSet = QrOcorreu
    Left = 713
    Top = 9
  end
  object QrOcorDupl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ok.Nome NOMEOCORBANK, ok.Base ValOcorBank, od.Ocorrbase '
      'FROM ocordupl od'
      'LEFT JOIN ocorbank ok ON ok.Codigo=od.Ocorrbase'
      'WHERE od.Codigo=:P0')
    Left = 741
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcorDuplNOMEOCORBANK: TWideStringField
      FieldName = 'NOMEOCORBANK'
      Size = 50
    end
    object QrOcorDuplValOcorBank: TFloatField
      FieldName = 'ValOcorBank'
    end
    object QrOcorDuplOcorrbase: TIntegerField
      FieldName = 'Ocorrbase'
      Required = True
    end
  end
  object QrOcorBank: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ocorbank'
      'WHERE Envio=:P0'
      'AND Movimento=:P1'
      '')
    Left = 769
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrOcorBankCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorBankNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrOcorBankLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorBankDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorBankDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorBankUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorBankUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcorBankBase: TFloatField
      FieldName = 'Base'
    end
    object QrOcorBankEnvio: TIntegerField
      FieldName = 'Envio'
    end
    object QrOcorBankMovimento: TIntegerField
      FieldName = 'Movimento'
    end
    object QrOcorBankFormaCNAB: TSmallintField
      FieldName = 'FormaCNAB'
    end
  end
  object Timer2: TTimer
    Enabled = False
    Interval = 100
    Left = 469
    Top = 260
  end
  object QrOB2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome'
      'FROM ocorbank'
      'WHERE Codigo=:P0')
    Left = 796
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOB2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrLocEnt1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE '
      'ent.Nome END NOMEENT, ent.Codigo CLIENTE, cnd.Codigo COND '
      'FROM cond cnd'
      'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente'
      'WHERE (CASE WHEN ent.Tipo=0 THEN ent.CNPJ ELSE ent.CPF END)=:P0'
      '/*   '
      '  AND cnd.CodCedente=:P1 '
      '*/')
    Left = 801
    Top = 225
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocEnt1NOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrLocEnt1CLIENTE: TIntegerField
      FieldName = 'CLIENTE'
      Required = True
    end
    object QrLocEnt1COND: TIntegerField
      FieldName = 'COND'
      Required = True
    end
  end
  object QrLei: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrLeiAfterOpen
    BeforeClose = QrLeiBeforeClose
    AfterClose = QrLeiAfterClose
    AfterScroll = QrLeiAfterScroll
    OnCalcFields = QrLeiCalcFields
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE '
      'ent.Nome END NOMEENT, car.Tipo TipoCart, '
      '(SELECT SUM(Credito) FROM lanctos lan'
      '  LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      '  LEFT JOIN entidades ent ON ent.Codigo=lan.Fornecedor'
      '  WHERE lan.FatID in (600,601,610)'
      '  AND lan.FatNum=lei.IDNum'
      '  AND car.Tipo=2'
      '  AND lan.Sit<2'
      '  AND car.ForneceI=lei.Entidade)  VALBOLETO, '
      '(SELECT SUM(lan.FatID) / COUNT(lan.FatID) FROM lanctos lan'
      '  LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      '  LEFT JOIN entidades ent ON ent.Codigo=lan.Fornecedor'
      '  WHERE lan.FatID in (600,601,610)'
      '  AND lan.FatNum=lei.IDNum'
      '  AND car.Tipo=2'
      '  AND lan.Sit<2'
      '  AND car.ForneceI=lei.Entidade)  TIPO_BOL, '
      'ban.DescriCNR, lei.*'
      'FROM cnab_lei lei'
      'LEFT JOIN entidades ent ON ent.Codigo=lei.Entidade'
      'LEFT JOIN carteiras car ON car.Codigo=lei.Carteira'
      'LEFT JOIN bancos    ban ON ban.Codigo=lei.Banco'
      'WHERE Step=0'
      'AND lei.Entidade=:P0')
    Left = 221
    Top = 125
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLeiCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cnab_lei.Codigo'
    end
    object QrLeiIDNum: TIntegerField
      FieldName = 'IDNum'
      Origin = 'cnab_lei.IDNum'
    end
    object QrLeiBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'cnab_lei.Banco'
      DisplayFormat = '000'
    end
    object QrLeiSeuNum: TIntegerField
      FieldName = 'SeuNum'
      Origin = 'cnab_lei.SeuNum'
      DisplayFormat = '0;-0; '
    end
    object QrLeiOcorrCodi: TWideStringField
      FieldName = 'OcorrCodi'
      Origin = 'cnab_lei.OcorrCodi'
      Size = 10
    end
    object QrLeiOcorrData: TDateField
      FieldName = 'OcorrData'
      Origin = 'cnab_lei.OcorrData'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLeiValTitul: TFloatField
      FieldName = 'ValTitul'
      Origin = 'cnab_lei.ValTitul'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiValAbati: TFloatField
      FieldName = 'ValAbati'
      Origin = 'cnab_lei.ValAbati'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiValDesco: TFloatField
      FieldName = 'ValDesco'
      Origin = 'cnab_lei.ValDesco'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiValPago: TFloatField
      FieldName = 'ValPago'
      Origin = 'cnab_lei.ValPago'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiValJuros: TFloatField
      FieldName = 'ValJuros'
      Origin = 'cnab_lei.ValJuros'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiValMulta: TFloatField
      FieldName = 'ValMulta'
      Origin = 'cnab_lei.ValMulta'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiMotivo1: TWideStringField
      FieldName = 'Motivo1'
      Origin = 'cnab_lei.Motivo1'
      Size = 2
    end
    object QrLeiMotivo2: TWideStringField
      FieldName = 'Motivo2'
      Origin = 'cnab_lei.Motivo2'
      Size = 2
    end
    object QrLeiMotivo3: TWideStringField
      FieldName = 'Motivo3'
      Origin = 'cnab_lei.Motivo3'
      Size = 2
    end
    object QrLeiMotivo4: TWideStringField
      FieldName = 'Motivo4'
      Origin = 'cnab_lei.Motivo4'
      Size = 2
    end
    object QrLeiMotivo5: TWideStringField
      FieldName = 'Motivo5'
      Origin = 'cnab_lei.Motivo5'
      Size = 2
    end
    object QrLeiQuitaData: TDateField
      FieldName = 'QuitaData'
      Origin = 'cnab_lei.QuitaData'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLeiDiretorio: TIntegerField
      FieldName = 'Diretorio'
      Origin = 'cnab_lei.Diretorio'
    end
    object QrLeiArquivo: TWideStringField
      FieldName = 'Arquivo'
      Origin = 'cnab_lei.Arquivo'
    end
    object QrLeiItemArq: TIntegerField
      FieldName = 'ItemArq'
      Origin = 'cnab_lei.ItemArq'
    end
    object QrLeiStep: TSmallintField
      FieldName = 'Step'
      Origin = 'cnab_lei.Step'
    end
    object QrLeiLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'cnab_lei.Lk'
    end
    object QrLeiDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'cnab_lei.DataCad'
    end
    object QrLeiDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'cnab_lei.DataAlt'
    end
    object QrLeiUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'cnab_lei.UserCad'
    end
    object QrLeiUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'cnab_lei.UserAlt'
    end
    object QrLeiEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'cnab_lei.Entidade'
    end
    object QrLeiNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrLeiVALBOLETO: TFloatField
      FieldName = 'VALBOLETO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'cnab_lei.Carteira'
      Required = True
    end
    object QrLeiTipoCart: TIntegerField
      FieldName = 'TipoCart'
      Origin = 'carteiras.Tipo'
      Required = True
    end
    object QrLeiDevJuros: TFloatField
      FieldName = 'DevJuros'
      Origin = 'cnab_lei.DevJuros'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiDevMulta: TFloatField
      FieldName = 'DevMulta'
      Origin = 'cnab_lei.DevMulta'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiDJM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DJM'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrLeiID_Link: TIntegerField
      FieldName = 'ID_Link'
      Origin = 'cnab_lei.ID_Link'
      Required = True
    end
    object QrLeiValJuMul: TFloatField
      FieldName = 'ValJuMul'
      Origin = 'cnab_lei.ValJuMul'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiValOutro: TFloatField
      FieldName = 'ValOutro'
      Origin = 'cnab_lei.ValOutro'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiValTarif: TFloatField
      FieldName = 'ValTarif'
      Origin = 'cnab_lei.ValTarif'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiNossoNum: TWideStringField
      FieldName = 'NossoNum'
      Origin = 'cnab_lei.NossoNum'
      Required = True
    end
    object QrLeiDtaTarif: TDateField
      FieldName = 'DtaTarif'
      Origin = 'cnab_lei.DtaTarif'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLeiDTA_TARIF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTA_TARIF_TXT'
      Size = 10
      Calculated = True
    end
    object QrLeiDescriCNR: TWideStringField
      FieldName = 'DescriCNR'
      Size = 100
    end
    object QrLeiTIPO_BOL: TFloatField
      FieldName = 'TIPO_BOL'
    end
    object QrLeiNOME_TIPO_BOL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_BOL'
      Size = 50
      Calculated = True
    end
    object QrLeiTamReg: TIntegerField
      FieldName = 'TamReg'
      Required = True
    end
  end
  object DsLei: TDataSource
    DataSet = QrLei
    Left = 249
    Top = 125
  end
  object QrLeiItens: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLeiItensCalcFields
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE '
      'ent.Nome END NOMEPROPRIET, lan.Data, lan.Genero, '
      'lan.Descricao, lan.Credito, lan.Debito, lan.Compensado, '
      'lan.Sit, lan.Vencimento, lan.Mez, lan.Fornecedor,'
      'lan.Controle, lan.Sub, lan.Carteira, lan.NotaFiscal,'
      'lan.SerieCH, lan.Documento, lan.Cliente, lan.CliInt, '
      'lan.ForneceI, lan.DataDoc, lan.Depto Apto, lan.Multa,'
      'imv.Unidade UH, car.Tipo TipoCart, imv.Codigo COND'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN entidades ent ON ent.Codigo=lan.Cliente'
      'LEFT JOIN condimov  imv ON imv.Conta=lan.Depto'
      'WHERE lan.FatID in (600,601,610)'
      'AND lan.FatNum=:P0'
      'AND car.Tipo=2'
      'AND lan.Sit<2'
      'AND car.ForneceI=:P1'
      'ORDER BY Credito DESC')
    Left = 221
    Top = 153
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLeiItensData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLeiItensCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrLeiItensGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLeiItensDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLeiItensCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLeiItensCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrLeiItensSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLeiItensVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLeiItensMez: TIntegerField
      FieldName = 'Mez'
      Required = True
      DisplayFormat = '0000'
    end
    object QrLeiItensFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLeiItensNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
    object QrLeiItensControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLeiItensSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrLeiItensTipoCart: TIntegerField
      FieldName = 'TipoCart'
      Required = True
    end
    object QrLeiItensDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrLeiItensNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrLeiItensSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrLeiItensDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrLeiItensCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLeiItensCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLeiItensForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLeiItensDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLeiItensApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrLeiItensUH: TWideStringField
      FieldName = 'UH'
      Size = 10
    end
    object QrLeiItensMez_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Mez_TXT'
      Size = 6
      Calculated = True
    end
    object QrLeiItensMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLeiItensCOND: TIntegerField
      FieldName = 'COND'
    end
  end
  object DsLeiItens: TDataSource
    DataSet = QrLeiItens
    Left = 249
    Top = 153
  end
  object QrBcocor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Carrega'
      'FROM bancoslei'
      'WHERE Codigo=:P0'
      'AND Ocorrencia=:P1')
    Left = 321
    Top = 273
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrBcocorCarrega: TSmallintField
      FieldName = 'Carrega'
    end
  end
  object PMExclui: TPopupMenu
    Left = 568
    Top = 444
    object Ajustavaloresdobloquetoatual1: TMenuItem
      Caption = '&Ajusta valores do bloqueto atual'
      OnClick = Ajustavaloresdobloquetoatual1Click
    end
    object Excluso1: TMenuItem
      Caption = '&Exclus'#227'o'
      object ExcluiAtual1: TMenuItem
        Caption = 'Exclui &Atual'
        OnClick = ExcluiAtual1Click
      end
      object ExcluiSelecionados1: TMenuItem
        Caption = 'Exclui &Selecionados'
        OnClick = ExcluiSelecionados1Click
      end
      object ExcluiTodos1: TMenuItem
        Caption = 'Exclui &Todos'
        OnClick = ExcluiTodos1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object ExcluiBloquetosnolocalizados1: TMenuItem
        Caption = 'Exclui &Bloquetos n'#227'o localizados'
        OnClick = ExcluiBloquetosnolocalizados1Click
      end
    end
  end
  object PMConcilia: TPopupMenu
    Left = 36
    Top = 432
    object ConciliaAtual1: TMenuItem
      Caption = 'Concilia &Atual'
      OnClick = ConciliaAtual1Click
    end
    object ConciliaSelecionados1: TMenuItem
      Caption = 'Concilia &Selecionados'
      OnClick = ConciliaSelecionados1Click
    end
    object ConciliaTodos1: TMenuItem
      Caption = 'Concilia &Todos'
      OnClick = ConciliaTodos1Click
    end
  end
  object QrLocCta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT con.Mensal'
      'FROM contas con '
      'WHERE con.Codigo=:P0')
    Left = 392
    Top = 124
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocCtaMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE '
      'ent.Nome END NOMEENT, ent.Codigo'
      'FROM cnab_lei lei'
      'LEFT JOIN entidades ent ON ent.Codigo=lei.Entidade'
      'WHERE Step=0'
      'GROUP BY lei.Entidade'
      'ORDER BY NOMEENT')
    Left = 217
    Top = 300
    object QrClientesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 245
    Top = 301
  end
  object QrTem: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(Codigo) Itens '
      'FROM cnab_lei'
      'WHERE Step=0')
    Left = 397
    Top = 281
    object QrTemItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrLEB: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Entidade'
      'FROM bancos'
      'WHERE Codigo=:P0')
    Left = 472
    Top = 92
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLEBEntidade: TIntegerField
      FieldName = 'Entidade'
    end
  end
  object QrLUH: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Apto, Propriet '
      'FROM arreits'
      'WHERE Boleto=:P0'
      ''
      'UNION'
      ''
      'SELECT DISTINCT Apto, Propriet '
      'FROM consits'
      'WHERE Boleto=:P1')
    Left = 492
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLUHApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrLUHPropriet: TIntegerField
      FieldName = 'Propriet'
      Required = True
    end
  end
  object QrPesq2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT lan.Vencimento, lan.Credito, '
      'cnd.PercMulta, cnd.PercJuros'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN cond cnd ON cnd.Cliente=car.ForneceI'
      'WHERE lan.FatID in (600,601)'
      'AND lan.FatNum=:P0'
      'AND car.Tipo=2'
      'AND car.ForneceI=:P1')
    Left = 64
    Top = 120
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPesq2Vencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrPesq2PercMulta: TFloatField
      FieldName = 'PercMulta'
    end
    object QrPesq2PercJuros: TFloatField
      FieldName = 'PercJuros'
    end
    object QrPesq2Credito: TFloatField
      FieldName = 'Credito'
    end
  end
  object QrBco: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cad.* '
      'FROM cnab_cad cad'
      'WHERE cad.Campo=:P0'
      ''
      ''
      ''
      ''
      '')
    Left = 609
    Top = 121
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBcoPadrIni: TIntegerField
      FieldName = 'PadrIni'
      Required = True
    end
    object QrBcoPadrTam: TIntegerField
      FieldName = 'PadrTam'
      Required = True
    end
    object QrBcoBcoOrig: TIntegerField
      FieldName = 'BcoOrig'
      Required = True
    end
  end
  object QrPesq1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cag.ID ID_REGISTRO, cad.* '
      'FROM cnab_cad cad'
      'LEFT JOIN cnab_cag cag ON cag.Codigo=cad.Registro'
      'WHERE cad.Campo=:P0'
      'AND cad.BcoOrig=:P1'
      ''
      ''
      '')
    Left = 637
    Top = 121
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPesq1PadrIni: TIntegerField
      FieldName = 'PadrIni'
      Required = True
    end
    object QrPesq1PadrTam: TIntegerField
      FieldName = 'PadrTam'
      Required = True
    end
    object QrPesq1PadrVal: TWideStringField
      FieldName = 'PadrVal'
      Size = 255
    end
    object QrPesq1SecoVal: TWideStringField
      FieldName = 'SecoVal'
      Size = 255
    end
    object QrPesq1AlfaNum: TSmallintField
      FieldName = 'AlfaNum'
      Required = True
    end
    object QrPesq1ID_REGISTRO: TWideStringField
      FieldName = 'ID_REGISTRO'
      Size = 10
    end
  end
  object QrCampos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cnab_cad'
      'WHERE Campo>500'
      'AND BcoOrig=:P0'
      'AND Envio=2'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    Left = 800
    Top = 116
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCamposCampo: TIntegerField
      FieldName = 'Campo'
    end
    object QrCamposPadrIni: TIntegerField
      FieldName = 'PadrIni'
    end
    object QrCamposPadrTam: TIntegerField
      FieldName = 'PadrTam'
    end
    object QrCamposFormato: TWideStringField
      FieldName = 'Formato'
    end
    object QrCamposCasas: TSmallintField
      FieldName = 'Casas'
    end
  end
  object QrBanco: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ID_400i, ID_400t, '
      'ID_240i, ID_240t, ID_240r, ID_240s'
      'FROM bancos'
      'WHERE Codigo=:P0')
    Left = 609
    Top = 149
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBancoID_400i: TIntegerField
      FieldName = 'ID_400i'
    end
    object QrBancoID_400t: TIntegerField
      FieldName = 'ID_400t'
    end
    object QrBancoID_240i: TIntegerField
      FieldName = 'ID_240i'
    end
    object QrBancoID_240t: TIntegerField
      FieldName = 'ID_240t'
    end
    object QrBancoID_240r: TSmallintField
      FieldName = 'ID_240r'
    end
    object QrBancoID_240s: TWideStringField
      FieldName = 'ID_240s'
      Size = 1
    end
  end
  object QrLocCond: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, PercJuros, PercMulta, VTCBBNITAR'
      'FROM cond'
      'WHERE Cliente=:P0'
      '')
    Left = 800
    Top = 152
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocCondCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocCondPercJuros: TFloatField
      FieldName = 'PercJuros'
    end
    object QrLocCondPercMulta: TFloatField
      FieldName = 'PercMulta'
    end
    object QrLocCondVTCBBNITAR: TFloatField
      FieldName = 'VTCBBNITAR'
    end
  end
  object QrPesq3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(lan.Credito) Credito, '
      'cnd.PercMulta, cnd.PercJuros, lan.Vencimento'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN cond cnd ON cnd.Cliente=car.ForneceI'
      'WHERE lan.FatID in (600,601)'
      'AND lan.FatNum=:P0'
      'AND car.Tipo=2'
      'AND car.ForneceI=:P1'
      'GROUP BY lan.FatNum')
    Left = 92
    Top = 120
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPesq3Credito: TFloatField
      FieldName = 'Credito'
    end
    object QrPesq3PercMulta: TFloatField
      FieldName = 'PercMulta'
    end
    object QrPesq3PercJuros: TFloatField
      FieldName = 'PercJuros'
    end
    object QrPesq3Vencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
  end
  object QrLocEnt2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE '
      'ent.Nome END NOMEENT, ent.Codigo CLIENTE, cnd.Codigo COND '
      'FROM cond cnd'
      'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente'
      ''
      'WHERE cnd.Banco=:P0'
      'AND cnd.Agencia=:P1'
      'AND cnd.CodCedente=:P2')
    Left = 800
    Top = 252
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrLocEnt2NOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrLocEnt2CLIENTE: TIntegerField
      FieldName = 'CLIENTE'
      Required = True
    end
    object QrLocEnt2COND: TIntegerField
      FieldName = 'COND'
      Required = True
    end
  end
  object PMItens: TPopupMenu
    Left = 668
    Top = 440
    object Alteravalordoitemdearrecadaoselecionado1: TMenuItem
      Caption = '&Altera valor do item de arrecada'#231#227'o selecionado'
      OnClick = Alteravalordoitemdearrecadaoselecionado1Click
    end
    object Excluioitemdearrecadaoselecionado1: TMenuItem
      Caption = '&Exclui o item de arrecada'#231#227'o selecionado'
      OnClick = Excluioitemdearrecadaoselecionado1Click
    end
  end
  object QrLeiAgr: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLeiAgrCalcFields
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROPRIET,'
      'SUM(lan.Credito) Valor, lan.Data, lan.Mez, lan.CliInt, '
      'lan.ForneceI, lan.Depto Apto, imv.Unidade UH, '
      'IF(lan.FatID=610, 2, 1) TIPO_BLOQ'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN entidades ent ON ent.Codigo=lan.Cliente'
      'LEFT JOIN condimov  imv ON imv.Conta=lan.Depto'
      'WHERE lan.FatID in (600,601,610)'
      'AND lan.FatNum=:P0'
      'AND car.Tipo=2'
      'AND lan.Sit<2'
      'AND car.ForneceI=:P1'
      'GROUP BY lan.Data, lan.CliInt, lan.ForneceI, lan.Depto, lan.Mez ')
    Left = 221
    Top = 181
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLeiAgrNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
    object QrLeiAgrValor: TFloatField
      FieldName = 'Valor'
    end
    object QrLeiAgrData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLeiAgrMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrLeiAgrCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLeiAgrForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLeiAgrApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrLeiAgrUH: TWideStringField
      FieldName = 'UH'
      Size = 10
    end
    object QrLeiAgrTIPO_BLOQ: TLargeintField
      FieldName = 'TIPO_BLOQ'
      Required = True
    end
    object QrLeiAgrNOME_TIPO_BLOQ: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_BLOQ'
      Size = 30
      Calculated = True
    end
    object QrLeiAgrMez_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Mez_TXT'
      Size = 6
      Calculated = True
    end
  end
  object DsLeiAgr: TDataSource
    DataSet = QrLeiAgr
    Left = 249
    Top = 181
  end
  object QrF240T: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM cnab_cad'
      'WHERE Envio = 2'
      'AND T240=1'
      'AND Registro=5'
      'AND BcoOrig=:P0')
    Left = 92
    Top = 420
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
end
