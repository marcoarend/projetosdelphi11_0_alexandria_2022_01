unit CNAB_Rem2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, dmkEdit, dmkLabel, Db,
  mySQLDbTables, Grids, RichEdit, dmkMemoBar, CheckLst, Mask, DBCtrls,
  dmkEditDateTimePicker, dmkGeral, Variants, UnDmkProcFunc, UnDmkEnums;

const
  MinColsArr = -2; MaxColsArr = 1000;

type
  dceAlinha = (posEsquerda, posCentro, posDireita);
  TArrayCols = array[MinColsArr..MaxColsArr] of Integer;
  TFmCNAB_Rem2 = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet4: TTabSheet;
    Panel6: TPanel;
    Panel10: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit6: TEdit;
    PageControl2: TPageControl;
    TabSheet10: TTabSheet;
    Panel11: TPanel;
    Panel8: TPanel;
    Label15: TLabel;
    EdArqGerado: TEdit;
    MeGerado: TMemo;
    dmkMBGravado: TdmkMemoBar;
    TabSheet11: TTabSheet;
    Panel7: TPanel;
    Panel12: TPanel;
    Label28: TLabel;
    SpeedButton1: TSpeedButton;
    EdArqCompar: TEdit;
    MeCompar: TMemo;
    dmkMBCompar: TdmkMemoBar;
    OpenDialog1: TOpenDialog;
    TabSheet7: TTabSheet;
    Panel13: TPanel;
    GradeG: TStringGrid;
    GradeO: TStringGrid;
    GradeC: TStringGrid;
    Panel17: TPanel;
    Panel18: TPanel;
    BitBtn1: TBitBtn;
    Panel19: TPanel;
    dmkEdVerifLin: TdmkEdit;
    Label16: TLabel;
    dmkEdVerifCol: TdmkEdit;
    Label17: TLabel;
    GradeI: TStringGrid;
    GradeF: TStringGrid;
    Label18: TLabel;
    Label19: TLabel;
    dmkEdVerifValCompar: TdmkEdit;
    dmkEdVerifCampoCod: TdmkEdit;
    Label20: TLabel;
    dmkEdVerifValGerado: TdmkEdit;
    dmkEdVerifCampoTxt: TdmkEdit;
    dmkEdVerifPos: TdmkEdit;
    Label22: TLabel;
    Label23: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    PB1: TProgressBar;
    DBEdit3: TDBEdit;
    Label25: TLabel;
    TabSheet12: TTabSheet;
    Panel20: TPanel;
    BtListar: TBitBtn;
    Grade_L: TStringGrid;
    TabSheet13: TTabSheet;
    PageControl3: TPageControl;
    TabSheet14: TTabSheet;
    Panel9: TPanel;
    Grade_1: TStringGrid;
    Panel5: TPanel;
    Label10: TLabel;
    LaCodDmk_1: TLabel;
    TabSheet3: TTabSheet;
    Panel14: TPanel;
    TabSheet15: TTabSheet;
    Panel15: TPanel;
    TabSheet5: TTabSheet;
    Grade_5: TStringGrid;
    TabSheet6: TTabSheet;
    Grade_7: TStringGrid;
    TabSheet8: TTabSheet;
    Panel16: TPanel;
    PageControl4: TPageControl;
    TabSheet9: TTabSheet;
    Grade_240_0: TStringGrid;
    TabSheet16: TTabSheet;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Label24: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label29: TLabel;
    dmkEd_0_000: TdmkEdit;
    dmkEd_0_005: TdmkEdit;
    dmkEd_0_006: TdmkEdit;
    dmkEd_0_003: TdmkEdit;
    dmkEd_0_004: TdmkEdit;
    dmkEd_0_402: TdmkEdit;
    dmkEd_0_990: TdmkEdit;
    dmkEd_0_410: TdmkEdit;
    dmkEd_0_641: TdmkEdit;
    dmkEd_0_991: TdmkEdit;
    dmkEd_0_400: TdmkEdit;
    dmkEd_0_401: TdmkEdit;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label21: TLabel;
    dmkEd_0_020: TdmkEdit;
    dmkEd_0_021: TdmkEdit;
    dmkEd_0_022: TdmkEdit;
    dmkEd_0_023: TdmkEdit;
    dmkEd_0_024: TdmkEdit;
    dmkEdNomeBanco: TdmkEdit;
    dmkEd_0_001: TdmkEdit;
    dmkEd_0_699: TdmkEdit;
    QrArqui: TmySQLQuery;
    QrArquiHoraG: TTimeField;
    QrArquiDataG: TDateField;
    TabSheet2: TTabSheet;
    Grade_240_1: TStringGrid;
    QrLotes: TmySQLQuery;
    QrLotesCedBanco: TIntegerField;
    QrLotesCedAgencia: TIntegerField;
    QrLotesCedConta: TWideStringField;
    QrLotesCedDAC_A: TWideStringField;
    QrLotesCedDAC_C: TWideStringField;
    QrLotesCedDAC_AC: TWideStringField;
    QrArquiMensagem1: TWideStringField;
    QrArquiMensagem2: TWideStringField;
    TabSheet17: TTabSheet;
    PageControl5: TPageControl;
    TabSheet18: TTabSheet;
    Grade_240_3_P: TStringGrid;
    QrItens: TmySQLQuery;
    QrLotesComando: TIntegerField;
    QrLotesQuemDistrb: TWideStringField;
    QrLotesTipoCart: TSmallintField;
    QrLotesEspecieTit: TWideStringField;
    QrLotesAceiteTit: TSmallintField;
    QrLotesQuemPrint: TWideStringField;
    QrLotesJurosTipo: TSmallintField;
    QrLotesJurosDias: TSmallintField;
    QrLotesJurosPerc: TFloatField;
    QrLotesMultaPerc: TFloatField;
    QrLotesDesco1Cod: TSmallintField;
    QrLotesDesco1Fat: TFloatField;
    QrLotesDesco1Dds: TIntegerField;
    QrLotesDesco2Cod: TSmallintField;
    QrLotesDesco2Fat: TFloatField;
    QrLotesDesco2Dds: TIntegerField;
    QrLotesDesco3Cod: TSmallintField;
    QrLotesDesco3Fat: TFloatField;
    QrLotesDesco3Dds: TIntegerField;
    QrLotesProtesCod: TSmallintField;
    QrLotesProtesDds: TSmallintField;
    QrLotesBxaDevCod: TSmallintField;
    QrLotesMoedaCod: TWideStringField;
    QrLotesContrato: TFloatField;
    QrLotesBxaDevDds: TIntegerField;
    TabSheet19: TTabSheet;
    Grade_240_3_Q: TStringGrid;
    TabSheet20: TTabSheet;
    Grade_240_3_R: TStringGrid;
    QrLotesMultaTipo: TSmallintField;
    QrLotesMultaDias: TSmallintField;
    TabSheet21: TTabSheet;
    Grade_240_5: TStringGrid;
    TabSheet22: TTabSheet;
    Grade_240_9: TStringGrid;
    QrLotesConvCartCobr: TWideStringField;
    QrLotesConvVariCart: TWideStringField;
    QrArquiNUM_REM: TIntegerField;
    QrLotesNUM_LOT: TIntegerField;
    QrLotesCedPosto: TIntegerField;
    QrLotesCartNum: TWideStringField;
    QrLotesCodEmprBco: TWideStringField;
    QrLotesVariacao: TIntegerField;
    QrLotesCartCod: TWideStringField;
    QrLotes_2401_208: TWideStringField;
    QrItensDuplicata: TWideStringField;
    QrItensEmissao: TDateField;
    QrItensVencto: TDateField;
    QrItensBruto: TFloatField;
    QrItensAbatimento: TFloatField;
    QrItensSeuNumero: TFloatField;
    QrItensSequencial: TFloatField;
    QrItensAVALISTA_TIPO: TSmallintField;
    QrItensAVALISTA_NOME: TWideStringField;
    QrItensAVALISTA_CNPJ: TWideStringField;
    QrItensAVALISTA_RUA: TWideStringField;
    QrItensAVALISTA_NUMERO: TFloatField;
    QrItensAVALISTA_COMPL: TWideStringField;
    QrItensAVALISTA_BAIRRO: TWideStringField;
    QrItensAVALISTA_CIDADE: TWideStringField;
    QrItensAVALISTA_xUF: TWideStringField;
    QrItensSACADO_CNPJ: TWideStringField;
    QrItensSACADO_NOME: TWideStringField;
    QrItensSACADO_RUA: TWideStringField;
    QrItensSACADO_NUMERO: TLargeintField;
    QrItensSACADO_COMPL: TWideStringField;
    QrItensSACADO_BAIRRO: TWideStringField;
    QrItensSACADO_CIDADE: TWideStringField;
    QrItensSACADO_xUF: TWideStringField;
    QrItensSACADO_CEP: TIntegerField;
    QrItensSACADO_TIPO: TLargeintField;
    QrItensENDERECO_SACADO: TWideStringField;
    QrItensENDERECO_AVALISTA: TWideStringField;
    QrItensAVALISTA_CEP: TFloatField;
    BtMover: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure dmkEd_0_001Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure Grade_1SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure Edit2Change(Sender: TObject);
    procedure Edit3Change(Sender: TObject);
    procedure Edit4Change(Sender: TObject);
    procedure MeGeradoEnter(Sender: TObject);
    procedure MeGeradoExit(Sender: TObject);
    procedure MeComparEnter(Sender: TObject);
    procedure MeComparExit(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure GradeGDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure GradeGSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure PageControl2Change(Sender: TObject);
    procedure BtListarClick(Sender: TObject);
    procedure QrItensCalcFields(DataSet: TDataSet);
    procedure QrLotesAfterScroll(DataSet: TDataSet);
    procedure BtMoverClick(Sender: TObject);
  private
    { Private declarations }
    FCols1, FCols5: TArrayCols;
    FLot_RegCount,
    FArq_RegCount,
    FArq_LotCount,
    FRegLote,
    FLinArq: Integer;
    FEditor: TMemo;
    FCedenteTipo, FSacadorTipo: Integer;
    FCedenteNome, FCedenteCNPJ, FSacadorNome, FSacadorCNPJ,
    FNomeCedente, FNomeSacador: String;
    //
    procedure PesquisaTexto;
    function NovaLinhaArq(): String;
    function Incrementa(const Grade: TStringGrid; var Contador: Integer;
             const Incremento: Integer; var ListaCols: TArrayCols;
             const MyCodField: Integer): Integer;
    procedure ConfGr(Grade: TStringGrid; Coluna, Tam: Integer;
              Titulo: String);
    function CriaListaCNAB(const CNAB, Banco, Registro: Integer; Segmento: Char;
             var Lista: MyArrayLista; const SubSegmento: Integer): Boolean;
    procedure VerificaCelula(Col, Row: Integer);
    function DAC_NossoNumero(const Bloqueto: Double; var Res: String): Boolean;
    // Novo 2009-10-29
    function Resgistro_240_0(Banco: Integer): Boolean;
    function Resgistro_240_1(Banco: Integer): Boolean;
    function Resgistro_240_3_P(Banco, Linha: Integer): Boolean;
    function Resgistro_240_3_Q(Banco, Linha: Integer): Boolean;
    function Resgistro_240_3_R(Banco, Linha: Integer): Boolean;
    function Resgistro_240_5(Banco: Integer): Boolean;
    function Resgistro_240_9(Banco: Integer): Boolean;
    function MsgProc(Campo, CNAB, Registro: Integer; Segmento: Char): String;
    function FormataCEP(CEP: Integer): String;
    function MoraDia_Percentual(Banco, CNAB, JurosTipo: Integer; ValorBruto,
             JurosPerc: Double): Double;
    function Multa_Valor_Percentual(Banco, CNAB, MultaTipo: Integer; ValorBruto,
             MultaPerc: Double): Double;

    procedure GeraCNAB400();
    procedure GeraCNAB240();
    function GetCNAB(): Integer;

    function SeqRegLote(Incremento: Integer): Integer;
    function DataZeroSeNenhumDia(Vencto: TDateTime; Dias, TipoData: Integer): String;
  public
    { Public declarations }
    //LCCod: array[0..1000] of byte;
    //LCNome: array[0..1000] of String;
    //
    FRowL, FLote, FSeqArqRem, FBanco: Integer;
    FColG1, FColG5,
    FLinG1, FLinG5,
    F1_000, F1_001, F1_011, F1_020, F1_021, F1_022, F1_023, F1_024,
    F1_400, F1_401,
    F1_501, F1_502, F1_504, F1_506, F1_507, F1_508, F1_509, F1_520, F1_550,
    F1_551, F1_552, F1_558, F1_569, F1_572, F1_573, F1_574, F1_575, F1_576,
    F1_577, F1_580, F1_583, F1_584, F1_586,
    F1_621, F1_639, F1_640,
    // Banco do Brasil
    F1_641, F1_642, F1_643, F1_644, F1_645, F1_646, F1_647, F1_648, F1_649, F1_650,
    // FIM Banco do Brasil
    F1_701, F1_702, F1_711, F1_712,
    F1_801, F1_802, F1_803, F1_804, F1_805, F1_806, F1_807, F1_808, F1_811,
    F1_812, F1_813, F1_814, F1_853,
    F1_950,
    //
    F5_000, F5_011,
    F5_815, F5_850, F5_851, F5_852, F5_854, F5_855, F5_856, F5_857, F5_858,
    F5_861, F5_862, F5_863, F5_864: Integer;
    //
    function AddLinha(var Contador: Integer; const Grade: TStringGrid): Integer;
    procedure AddCampo(Grade: TStringGrid; Linha, Coluna: Integer;
              Formato: String; Valor: Variant);
    function AjustaString(const Texto, Compl: String; const Tamanho:
              Integer; const Ajusta: Boolean; var Res: String): Boolean;
    function  ObtemNumeroDoArquivo(var Res: String): Boolean;
    function  ValorDeColunaDeGrade(const Grade: TStringGrid; const Lista:
              MyArrayLista; const ItemLista: Integer; const FCols: TArrayCols;
              const LinhaGrade: Integer; var Res: String): Boolean;
    function  ValorDeCampo(const Lista: MyArrayLista; Campo: Integer;
              const Pre_Def: String; var Res: String): Boolean;
    procedure AvisaCancelamento(Texto: String; TamCorreto, TamAtual: Integer);
    //  Novo 2009-10-29
    procedure AdicionaListaRow(LinhaArquivo, TipoRegistro: Integer;
              Segmento: Char; Coluna, PosIni, PosFim, TamCNAB, TamReal, IDCampo: Integer;
              Valor, NomeCampo, DescriCampo: String);
    function CarregaDadosNoForm(CNAB_Cfg, SeqArq, Lote: Integer;
             SQL_HEADE_ARQUI, SQL_HEADE_LOTES, SQL_ITENS_LOTES: String;
             DataType: array of TFieldType; Campos: array of String): Boolean;
    function DescontoAntecipado(Banco, CNAB, DescoCod, DescoDds: Integer;
             DescoFat, ValorBruto: Double; Vencimento: TDateTime): String;

  end;

const
   _Ini = 0; _Fim = 1; _Tam = 2; _Fil = 3; _Typ = 4; _Fmt = 5;
   _Fld = 6; _Def = 7; _Nom = 8; _Des = 9; _AjT = 10;

  var
  FmCNAB_Rem2: TFmCNAB_Rem2;

implementation

uses UnMyObjects, ModuleBco, UnBancos, UnBco_rem, UCreate, Module, MyDBCheck,
  UMySQLModule, Bancos, MyListas;

{$R *.DFM}

procedure TFmCNAB_Rem2.PesquisaTexto;
var
  Lin, Col, Tam: Integer;
begin
  case PageControl2.ActivePageIndex of
    0: FEditor := MeGerado;
    1: FEditor := MeCompar;
    else FEditor := nil;
  end;
  if FEditor = nil then Exit;
  Lin := Geral.IMV(Edit2.Text);
  Col := Geral.IMV(Edit3.Text);
  Tam := Geral.IMV(Edit4.Text);
  //
  if (Lin > 0) and (Col > 0) and (Tam > 0) then
  begin
    Edit6.Text := Copy(FEditor.Lines[Lin-1], Col, Tam);
  end else Edit6.Text := '';
end;

procedure TFmCNAB_Rem2.QrItensCalcFields(DataSet: TDataSet);
var
  Txt: String;
begin
  Txt := QrItensSACADO_RUA.Value + ', ';
  if QrItensSACADO_NUMERO.Value < 1 then Txt := Txt + 'S/N' else
  Txt := Txt + Geral.SoNumero_TT(FloatToStr(QrItensSACADO_NUMERO.Value));
  Txt := Txt + ' ' + QrItensSACADO_COMPL.Value;
  QrItensENDERECO_SACADO.Value := Txt;
  //
  Txt := QrItensAVALISTA_RUA.Value + ', ';
  if QrItensAVALISTA_NUMERO.Value < 1 then Txt := Txt + 'S/N' else
  Txt := Txt + Geral.SoNumero_TT(FloatToStr(QrItensAVALISTA_NUMERO.Value));
  Txt := Txt + ' ' + QrItensAVALISTA_COMPL.Value;
  QrItensENDERECO_AVALISTA.Value := Txt;
  //
end;

procedure TFmCNAB_Rem2.QrLotesAfterScroll(DataSet: TDataSet);
begin
  SeqRegLote(0);
end;

function TFmCNAB_Rem2.Resgistro_240_0(Banco: Integer): Boolean;
const
  CNAB     = 240;
  Registro = 0;
  Segmento = #0;
  SubSegmento = 0;
var
  Lista: MyArrayLista;
  I, Col, Tam, Fld: Integer;
  Txt, Val: String;
begin
  Result := False;
  if not CriaListaCNAB(CNAB, Banco, Registro, Segmento, Lista, SubSegmento) then
  begin Screen.Cursor := crDefault; Exit; end;
  FArq_RegCount := FArq_RegCount + 1;
  Col := High(Lista);
  Grade_240_0.ColCount := Col + 1;
  Grade_240_0.RowCount := 2;
  for I := 0 to Col do
  begin
    // T�tulos
    Txt := Lista[I][_Nom][1] + Geral.Minusculas(Copy(Lista[I][_Nom], 2), False);
    Grade_240_0.Cells[I, 0] := Txt;
    Tam := Trunc(Geral.IMV(Lista[I][_Tam]) * 8);
    if Tam < 32 then Tam := 32;
    Grade_240_0.ColWidths[I] := Tam;
    // Dados
    Txt := '';
    Val := '';
    Fld := Geral.IMV(Lista[I][_Fld]);
    case Fld of
      -02: Txt := '';
      -01: Txt := '';
      001: Txt := FormatFloat('000', DmBco.QrCNAB_CfgCedBanco.Value);
      002: Txt := ''; //Pega do default
      003: Txt := ''; //Pega do default
      004: Txt := ''; //Pega do default
      005: Txt := ''; //Pega do default
      006: Txt := ''; //Pega do default
      009: Txt := '1'; // Remessa
      010: Txt := ''; //Pega do default
      020: Txt := FormatFloat('0', DmBco.QrCNAB_CfgCedAgencia.Value);
      021: Txt := DmBco.QrCNAB_CfgCedConta.Value;
      022: Txt := DmBco.QrCNAB_CfgCedDAC_A.Value;
      023: Txt := DmBco.QrCNAB_CfgCedDAC_C.Value;
      024: Txt := DmBco.QrCNAB_CfgCedDAC_AC.Value;
      028: Txt := ''; //Pega do default
      030: Txt := ''; //Pega do default
      032: Txt := FormatFloat('0', QrArquiNUM_REM.Value);
      400: UBco_Rem.TipoDeInscricao(FCedenteTipo, FBanco, CNAB, 240, '', Txt);
      401: Txt := FCedenteCNPJ;
      402: Txt := FNomeCedente;
      410: Txt := DmBco.QrCNAB_CfgCodEmprBco.Value;
      699: Txt := DmBco.QrCNAB_CfgCodOculto.Value;
      888: Txt := ''; //Pega do default
      889: Txt := ''; //Pega do default
      990: Txt := Geral.FDT(QrArquiDataG.Value, 2);
      991: Txt := Geral.FDT(QrArquiHoraG.Value, 100);
      036: Txt := ''; //Pega do default
      998: Txt := FormatFloat('0', QrArquiNUM_REM.Value);
      // Banco do Brasil
      (*
      409: Txt := '';//O pr�prio aplicativo do banco n�o informa//DmBco.QrCNAB_CfgCodLidrBco.Value;
      645: Txt := '';//O pr�prio aplicativo do banco n�o informa//FormatFloat('0', DmBco.QrCNAB_CfgVariacao.Value);
      *)
      409: Txt := DmBco.QrCNAB_CfgCodLidrBco.Value;
      645: Txt := FormatFloat('0', DmBco.QrCNAB_CfgVariacao.Value);
      655: Txt := DmBco.QrCNAB_CfgConvCartCobr.Value;
      656: Txt := DmBco.QrCNAB_CfgConvVariCart.Value;
      else
      begin
        MsgProc(Fld, CNAB, Registro, Segmento);
      end;
    end;
    if not ValorDeCampo(Lista, I, Txt, Val) then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end
    else
      Grade_240_0.Cells[I, 1] := Val;
  end;
  //
  Result := True;
end;

function TFmCNAB_Rem2.Resgistro_240_1(Banco: Integer): Boolean;
const
  CNAB     = 240;
  Registro = 1;
  Segmento = #0;
  SubSegmento = 0;
var
  Lista: MyArrayLista;
  I, Col, Tam, Fld: Integer;
  Txt, Val: String;
begin
  Result := False;
  if not CriaListaCNAB(CNAB, Banco, Registro, Segmento, Lista, SubSegmento) then
  begin Screen.Cursor := crDefault; Exit; end;
  FArq_LotCount := FArq_LotCount + 1;
  FLot_RegCount := FLot_RegCount + 1;
  FArq_RegCount := FArq_RegCount + 1;
  Col := High(Lista);
  Grade_240_1.ColCount := Col + 1;
  Grade_240_1.RowCount := 2;
  for I := 0 to Col do
  begin
    // T�tulos
    Txt := Lista[I][_Nom][1] + Geral.Minusculas(Copy(Lista[I][_Nom], 2), False);
    Grade_240_1.Cells[I, 0] := Txt;
    Tam := Trunc(Geral.IMV(Lista[I][_Tam]) * 8);
    if Tam < 32 then Tam := 32;
    Grade_240_1.ColWidths[I] := Tam;
    // Dados
    Txt := '';
    Val := '';
    Fld := Geral.IMV(Lista[I][_Fld]);
    case Fld of
      -03: Txt := QrLotes_2401_208.Value;
      -02: Txt := '';
      -01: Txt := '';
      001: Txt := FormatFloat('000', QrLotesCedBanco.Value);
      003: Txt := FormatFloat('0', QrLotes.RecNo);
      013: Txt := ''; //Pega do default
      020: Txt := FormatFloat('0', QrLotesCedAgencia.Value);
      021: Txt := QrLotesCedConta.Value;
      022: Txt := QrLotesCedDAC_A.Value;
      023: Txt := QrLotesCedDAC_C.Value;
      024: Txt := QrLotesCedDAC_AC.Value;
      029: Txt := ''; //Pega do default
      031: Txt := 'R'; // Remessa
      032: Txt := FormatFloat('0', QrArquiNUM_REM.Value);
      033: Txt := Geral.FDT(QrArquiDataG.Value, 2);
      211: Txt := QrArquiMensagem1.Value;
      212: Txt := QrArquiMensagem2.Value;
      400: UBco_Rem.TipoDeInscricao(FCedenteTipo, FBanco, CNAB, 240, '',Txt);
      401: Txt := FCedenteCNPJ;
      402: Txt := FNomeCedente;
      581: Txt := ''; //Data do cr�dito ?????
      889: Txt := ''; //Pega do default
      // Banco do Brasil
      410: Txt := QrLotesCodEmprBco.Value;
      645: Txt := FormatFloat('0', QrLotesVariacao.Value);
      655: Txt := QrLotesConvCartCobr.Value;
      656: Txt := QrLotesConvVariCart.Value;
      else
      begin
        MsgProc(Fld, CNAB, Registro, Segmento);
      end;
    end;
    if not ValorDeCampo(Lista, I, Txt, Val) then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end
    else
      Grade_240_1.Cells[I, 1] := Val;
  end;
  //
  Result := True;
end;

function TFmCNAB_Rem2.Resgistro_240_3_P(Banco, Linha: Integer): Boolean;
const
  CNAB     = 240;
  Registro = 3;
  Segmento = 'P';
  SubSegmento = 0;
var
  Lista: MyArrayLista;
  I, Col, Tam, Fld: Integer;
  Txt, Txt_Rem, Val: String;
  //
  CadTitBco, Agencia, Posto: Integer;
  Sequencial: Double;
  ContaCorrente, Carteira, IDCobranca, CodigoCedente: String;
  Vencimento: TDateTime;
begin
  Result := False;
  if not CriaListaCNAB(CNAB, Banco, Registro, Segmento, Lista, SubSegmento) then
  begin Screen.Cursor := crDefault; Exit; end;
  FLot_RegCount := FLot_RegCount + 1;
  FArq_RegCount := FArq_RegCount + 1;
  Col := High(Lista);
  Grade_240_3_P.ColCount := Col + 1;
  Grade_240_3_P.RowCount := Linha + 1;
  for I := 0 to Col do
  begin
    // T�tulos
    Txt := Lista[I][_Nom][1] + Geral.Minusculas(Copy(Lista[I][_Nom], 2), False);
    Grade_240_3_P.Cells[I, 0] := Txt;
    Tam := Trunc(Geral.IMV(Lista[I][_Tam]) * 8);
    if Tam < 32 then Tam := 32;
    Grade_240_3_P.ColWidths[I] := Tam;
    // Dados
    Txt := '';
    Val := '';
    Fld := Geral.IMV(Lista[I][_Fld]);
    case Fld of
      -02: Txt := '';
      -01: Txt := '';
      // C�digo do banco
      001: Txt := FormatFloat('000', DmBco.QrCNAB_CfgCedBanco.Value);
      // Codigo do servi�o (Lote de Servi�o)
      003: Txt := FormatFloat('0', QrLotes.RecNo);
      // Identifica��o do registro detalhe (1,2,3,5,7)
      011: Txt := ''; //Pega do default
      // C�digo de movimento
      019: Txt := FormatFloat('0', QrLotesComando.Value);
      // C�digo da Ag�ncia da Empresa
      020: Txt := FormatFloat('0', QrLotesCedAgencia.Value);
      // N�mero da Conta Corrente da Empresa
      021: Txt := QrLotesCedConta.Value;
      // D�gito verificador Ag�ncia Empresa
      022: Txt := QrLotesCedDAC_A.Value;
      // D�gito verificador Conta Empresa
      023: Txt := QrLotesCedDAC_C.Value;
      // D�gito verificador Ag�ncia e Conta Empresa
      024: Txt := QrLotesCedDAC_AC.Value;
      // C�digo da Ag�ncia cobradora (o banco � que define)
      025: Txt := ''; //Pega do default
      // DV da Ag�ncia cobradora (o banco � que define)
      026: Txt := ''; //Pega do default
      // Forma de cadastramento do t�tulo no banco'; // BB > com ou sem cadastro!
      034: Txt := FormatFloat('0', QrLotesTipoCart.Value);
      // Identifica��o da emiss�o do bloqueto'; // BB > Diversas op��es
      036: Txt := QrLotesQuemDistrb.Value;
      // Identifica��o do registro P
      080: Txt := ''; // Pega do default
      // Nosso N�mero
      501:
      begin
        CadTitBco     := QrLotesTipoCart.Value;
        Agencia       := QrLotesCedAgencia.Value;
        Posto         := QrLotesCedPosto.Value;
        Sequencial    := QrItensSequencial.Value;
        ContaCorrente := QrLotesCedConta.Value;
        Carteira      := QrLotesCartNum.Value;
        IDCobranca    := Geral.FFI(QrItensSeuNumero.Value);
        CodigoCedente := QrLotesCodEmprBco.Value;
        Vencimento    := QrItensVencto.Value;
        //
        UBancos.GeraNossoNumero(CadTitBco, Banco, Agencia, Posto, Sequencial,
          ContaCorrente, Carteira, IDCobranca, CodigoCedente, Vencimento,
          DmBco.QrCNAB_CfgTipoCobranca.Value, DmBco.QrCNAB_CfgEspecieDoc.Value,
          DmBco.QrCNAB_CfgCNAB.Value, DmBco.QrCNAB_CfgCtaCooper.Value, '', Txt, Txt_Rem);
        Txt := Geral.SoNumero_TT(Txt);
      end;
      // N�mero do documento
      502: Txt := QrItensDuplicata.Value;
      // Seu n�mero (uso da empresa)
      506: Txt := FloatToStr(QrItensSeuNumero.Value);
      // Esp�cie de documento (t�tulo)
      507: Txt := QrLotesEspecieTit.Value;
      // N�mero da Carteira
      508: Txt := QrLotesCartCod.Value;
      // C�digo (N�mero) da Carteira
      509: Txt := QrLotesCartNum.Value;
      //'Seqencial do Nosso Numero'; // Itau Remessa / (retorno ?) 2012-08-24
      512: Txt := Geral.FFI(QrItensSequencial.Value);
      // Aceite do t�tulo
      520: UBco_rem.CodigoDeAceiteRemessa(Banco, QrLotesAceiteTit.Value, '', Txt);
      // C�digo da moeda
      549: Txt := QrLotesMoedaCod.Value;
      // Valor (nominal) do t�tulo
      550: Txt := FormatFloat('0.00', QrItensBruto.Value);
      // Valor do abatimento concedido
      551: Txt := FormatFloat('0.00', QrItensAbatimento.Value);
      // IOF
      569: Txt := '';
      // Valor de mora/dia a cobrar ap�s atraso
      572: Txt := FormatFloat('0.00', MoraDia_Percentual(Banco, CNAB,
           QrLotesJurosTipo.Value, QrItensBruto.Value, QrLotesJurosPerc.Value));
      // Como cobrar os juros (valor ou %)
      576: Txt := FormatFloat('0', QrLotesJurosTipo.Value);
      // Data do vencimento
      580: Txt := Geral.FDT(QrItensVencto.Value, 2);
      // Data de emiss�o do t�tulo
      583: Txt := Geral.FDT(QrItensEmissao.Value, 2);
      // Data do inicio de cobran�a de mora
      584: Txt := DataZeroSeNenhumDia(QrItensVencto.Value, QrLotesJurosDias.Value, 2);
      // Desconto 1 - 240 BB
      // Desconto 1 - C�digo
      591: Txt := FormatFloat('0', QrLotesDesco1Cod.Value);
      // Desconto 1 - Data
      592: Txt := DataZeroSeNenhumDia(QrItensVencto.Value, QrLotesDesco1Dds.Value, 2);
      // Desconto 1 - Valor / porcentagem
      593: Txt := DescontoAntecipado(Banco, CNAB, QrLotesDesco1Cod.Value,
           QrLotesDesco1Dds.Value, QrLotesDesco1Fat.Value,
           QrItensBruto.Value, QrItensVencto.Value);
      621: Txt := QrLotesQuemPrint.Value;
      // N�mero de dias para protesto
      647: Txt := FormatFloat('0', QrLotesProtesDds.Value);
      // C�digo para protesto
      651: Txt := FormatFloat('0', QrLotesProtesCod.Value);
      // C�digo para baixa / devolu��o
      652: Txt := FormatFloat('0', QrLotesBxaDevCod.Value);
      // N�mero de dias para baixa / devolu��o
      653: Txt := FormatFloat('0', QrLotesBxaDevDds.Value);
      // N�mero do contarto da opera��o de cr�dito
      654: Txt := FormatFloat('0', QrLotesContrato.Value);
      // N�mero sequencial do registro no lote
      997: Txt := FormatFloat('0', SeqRegLote(1)); // QrLotes.RecNo);
      else
      begin
        MsgProc(Fld, CNAB, Registro, Segmento);
      end;
    end;
    if not ValorDeCampo(Lista, I, Txt, Val) then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end
    else
      Grade_240_3_P.Cells[I, Linha] := Val;
  end;
  //
  Result := True;
end;

function TFmCNAB_Rem2.Resgistro_240_3_Q(Banco, Linha: Integer): Boolean;
const
  CNAB     = 240;
  Registro = 3;
  Segmento = 'Q';
  SubSegmento = 0;
var
  Lista: MyArrayLista;
  I, Col, Tam, Fld: Integer;
  Txt, Val: String;
begin
  Result := False;
  if not CriaListaCNAB(CNAB, Banco, Registro, Segmento, Lista, SubSegmento) then
  begin Screen.Cursor := crDefault; Exit; end;
  FLot_RegCount := FLot_RegCount + 1;
  FArq_RegCount := FArq_RegCount + 1;
  Col := High(Lista);
  Grade_240_3_Q.ColCount := Col + 1;
  Grade_240_3_Q.RowCount := Linha + 1;
  for I := 0 to Col do
  begin
    // T�tulos
    Txt := Lista[I][_Nom][1] + Geral.Minusculas(Copy(Lista[I][_Nom], 2), False);
    Grade_240_3_Q.Cells[I, 0] := Txt;
    Tam := Trunc(Geral.IMV(Lista[I][_Tam]) * 8);
    if Tam < 32 then Tam := 32;
    Grade_240_3_Q.ColWidths[I] := Tam;
    // Dados
    Txt := '';
    Val := '';
    Fld := Geral.IMV(Lista[I][_Fld]);
    case Fld of
      -02: Txt := '';
      -01: Txt := '';
      // C�digo do banco
      001: Txt := FormatFloat('000', DmBco.QrCNAB_CfgCedBanco.Value);
      // Codigo do servi�o (Lote de Servi�o)
      003: Txt := FormatFloat('0', QrLotes.RecNo);
      // Identifica��o do registro detalhe (1,2,3,5,7)
      011: Txt := ''; //Pega do default
      // C�digo de movimento
      019: Txt := FormatFloat('0', QrLotesComando.Value);
      // Identifica��o do registro Q
      081: Txt := ''; // Pega do default
      // Tipo de inscri��o do sacado
      801:
      begin
        UBco_Rem.TipoDeInscricao(QrItensSACADO_TIPO.Value, Banco, CNAB, 801, '', Txt);
        Txt := FormatFloat('0', Geral.IMV(Txt));
      end;
      // N�mero da inscri��o (CPF/CNPJ) do sacado
      802: Txt := Geral.SoNumero_TT(QrItensSACADO_CNPJ.Value);
      // Nome do Sacado
      803: Txt := QrItensSACADO_NOME.Value;
      // Logradouro completo do sacado (rua, n�mero e complemento)
      804: Txt := QrItensENDERECO_SACADO.Value;
      // Bairro do sacado
      805: Txt := QrItensSACADO_BAIRRO.Value;
      // CEP do sacado
      806: Txt := FormataCEP(QrItensSACADO_CEP.Value);
      // Cidade do sacado
      807: Txt := QrItensSACADO_CIDADE.Value;
      // UF do sacado
      808: Txt := QrItensSACADO_xUF.Value;
      // Tipo de inscri��o do sacador / avalista
      851:
      begin
        UBco_Rem.TipoDeInscricao(QrItensAVALISTA_TIPO.Value, Banco, CNAB, 851, '', Txt);
        Txt := FormatFloat('0', Geral.IMV(Txt));
      end;
      // N�mero da inscri��o (CPF/CNPJ) do sacador / avalista
      852: Txt := Geral.SoNumero_TT(QrItensAVALISTA_CNPJ.Value);
      // Nome do sacador / Avalista
      853: Txt := QrItensAVALISTA_NOME.Value;
      // N�mero sequencial do registro no lote
      997: Txt := FormatFloat('0', SeqRegLote(1)); // QrLotes.RecNo);
      else
      begin
        MsgProc(Fld, CNAB, Registro, Segmento);
      end;
    end;
    if not ValorDeCampo(Lista, I, Txt, Val) then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end
    else
      Grade_240_3_Q.Cells[I, Linha] := Val;
  end;
  //
  Result := True;
end;

function TFmCNAB_Rem2.Resgistro_240_3_R(Banco, Linha: Integer): Boolean;
const
  CNAB     = 240;
  Registro = 3;
  Segmento = 'R';
  SubSegmento = 0;
var
  Lista: MyArrayLista;
  I, Col, Tam, Fld: Integer;
  Txt, Val: String;
begin
  Result := False;
  if not CriaListaCNAB(CNAB, Banco, Registro, Segmento, Lista, SubSegmento) then
  begin Screen.Cursor := crDefault; Exit; end;
  FLot_RegCount := FLot_RegCount + 1;
  FArq_RegCount := FArq_RegCount + 1;
  Col := High(Lista);
  Grade_240_3_R.ColCount := Col + 1;
  Grade_240_3_R.RowCount := Linha + 1;
  for I := 0 to Col do
  begin
    // T�tulos
    Txt := Lista[I][_Nom][1] + Geral.Minusculas(Copy(Lista[I][_Nom], 2), False);
    Grade_240_3_R.Cells[I, 0] := Txt;
    Tam := Trunc(Geral.IMV(Lista[I][_Tam]) * 8);
    if Tam < 32 then Tam := 32;
    Grade_240_3_R.ColWidths[I] := Tam;
    // Dados
    Txt := '';
    Val := '';
    Fld := Geral.IMV(Lista[I][_Fld]);
    case Fld of
      -02: Txt := '';
      -01: Txt := '';
      // C�digo do banco
      001: Txt := FormatFloat('000', DmBco.QrCNAB_CfgCedBanco.Value);
      // Codigo do servi�o (Lote de Servi�o)
      003: Txt := FormatFloat('0', QrLotes.RecNo);
      // Identifica��o do registro detalhe (1,2,3,5,7)
      011: Txt := ''; //Pega do default
      // C�digo de movimento
      019: Txt := FormatFloat('0', QrLotesComando.Value);
      // Identifica��o do registro R
      082: Txt := ''; // Pega do default
      //
      // NO BB (BANCO 001) as mensagem 3 e 4 prevalecem sobre as mensagens 1 e 2!!!
      //213: Result := 'Mensagem 3 (CNAB 240 - 40 caracteres - impressa em todos bloquetos do mesmo lote)';
      //214: Result := 'Mensagem 4 (CNAB 240 - 40 caracteres - impressa em todos bloquetos do mesmo lote)';
      213: Txt := '';//Txt := QrArquiMensagem1.Value;
      214: Txt := '';//Txt := QrArquiMensagem2.Value;
      // Valor de multa a cobrar ap�s atraso
      573: Txt := FormatFloat('0.00', Multa_Valor_Percentual(Banco, CNAB,
           QrLotesMultaTipo.Value, QrItensBruto.Value, QrLotesMultaPerc.Value));
      // Como cobrar a multa (valor ou %)
      577: Txt := FormatFloat('0', QrLotesMultaTipo.Value);
      // Data da cobran�a da multa
      587: Txt := DataZeroSeNenhumDia(QrItensVencto.Value, QrLotesMultaDias.Value, 2);
      // Desconto 2 - 240 BB
      594: Txt := FormatFloat('0', QrLotesDesco2Cod.Value);
      595: Txt := DataZeroSeNenhumDia(QrItensVencto.Value,QrLotesDesco2Dds.Value, 2);
      596: Txt := DescontoAntecipado(Banco, CNAB, QrLotesDesco2Cod.Value,
           QrLotesDesco2Dds.Value, QrLotesDesco2Fat.Value,
           QrItensBruto.Value, QrItensVencto.Value);
      // Desconto 3 - 240 BB
      597: Txt := FormatFloat('0', QrLotesDesco3Cod.Value);
      598: Txt := DataZeroSeNenhumDia(QrItensVencto.Value, QrLotesDesco3Dds.Value, 2);
      599: Txt := DescontoAntecipado(Banco, CNAB, QrLotesDesco3Cod.Value,
           QrLotesDesco3Dds.Value, QrLotesDesco3Fat.Value,
           QrItensBruto.Value, QrItensVencto.Value);
      //
      // N�mero sequencial do registro no lote
      997: Txt := FormatFloat('0', SeqRegLote(1)); // QrLotes.RecNo);
      else
      begin
        MsgProc(Fld, CNAB, Registro, Segmento);
      end;
    end;
    if not ValorDeCampo(Lista, I, Txt, Val) then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end
    else
      Grade_240_3_R.Cells[I, Linha] := Val;
  end;
  //
  Result := True;
end;

function TFmCNAB_Rem2.Resgistro_240_5(Banco: Integer): Boolean;
const
  CNAB     = 240;
  Registro = 5;
  Segmento = #0;
  SubSegmento = 0;
var
  Lista: MyArrayLista;
  I, Col, Tam, Fld: Integer;
  Txt, Val: String;
begin
  Result := False;
  FLot_RegCount := FLot_RegCount + 1;
  FArq_RegCount := FArq_RegCount + 1;
  if not CriaListaCNAB(CNAB, Banco, Registro, Segmento, Lista, SubSegmento) then
  begin Screen.Cursor := crDefault; Exit; end;
  Col := High(Lista);
  Grade_240_5.ColCount := Col + 1;
  Grade_240_5.RowCount := 2;
  for I := 0 to Col do
  begin
    // T�tulos
    Txt := Lista[I][_Nom][1] + Geral.Minusculas(Copy(Lista[I][_Nom], 2), False);
    Grade_240_5.Cells[I, 0] := Txt;
    Tam := Trunc(Geral.IMV(Lista[I][_Tam]) * 8);
    if Tam < 32 then Tam := 32;
    Grade_240_5.ColWidths[I] := Tam;
    // Dados
    Txt := '';
    Val := '';
    Fld := Geral.IMV(Lista[I][_Fld]);
    case Fld of
      -02: Txt := '';
      -01: Txt := '';
      001: Txt := FormatFloat('000', DmBco.QrCNAB_CfgCedBanco.Value);
      003: Txt := FormatFloat('0', QrLotes.RecNo);
      012: Txt := ''; //Pega do default
      992: Txt := FormatFloat('0', FLot_RegCount);
      else
      begin
        MsgProc(Fld, CNAB, Registro, Segmento);
      end;
    end;
    if not ValorDeCampo(Lista, I, Txt, Val) then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end
    else
      Grade_240_5.Cells[I, 1] := Val;
  end;
  //
  Result := True;
end;

function TFmCNAB_Rem2.Resgistro_240_9(Banco: Integer): Boolean;
const
  CNAB     = 240;
  Registro = 9;
  Segmento = #0;
  SubSegmento = 0;
var
  Lista: MyArrayLista;
  I, Col, Tam, Fld: Integer;
  Txt, Val: String;
begin
  Result := False;
  FArq_RegCount := FArq_RegCount + 1;
  if not CriaListaCNAB(CNAB, Banco, Registro, Segmento, Lista, SubSegmento) then
  begin Screen.Cursor := crDefault; Exit; end;
  Col := High(Lista);
  Grade_240_9.ColCount := Col + 1;
  Grade_240_9.RowCount := 2;
  for I := 0 to Col do
  begin
    // T�tulos
    Txt := Lista[I][_Nom][1] + Geral.Minusculas(Copy(Lista[I][_Nom], 2), False);
    Grade_240_9.Cells[I, 0] := Txt;
    Tam := Trunc(Geral.IMV(Lista[I][_Tam]) * 8);
    if Tam < 32 then Tam := 32;
    Grade_240_9.ColWidths[I] := Tam;
    // Dados
    Txt := '';
    Val := '';
    Fld := Geral.IMV(Lista[I][_Fld]);
    case Fld of
      -02: Txt := '';
      -01: Txt := '';
      001: Txt := FormatFloat('000', DmBco.QrCNAB_CfgCedBanco.Value);
      003: Txt := FormatFloat('0', QrLotes.RecNo);
      012: Txt := ''; //Pega do default
      993: Txt := FormatFloat('0', FArq_LotCount);
      994: Txt := FormatFloat('0', FArq_RegCount);
      else
      begin
        MsgProc(Fld, CNAB, Registro, Segmento);
      end;
    end;
    if not ValorDeCampo(Lista, I, Txt, Val) then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end
    else
      Grade_240_9.Cells[I, 1] := Val;
  end;
  //
  Result := True;
end;

function TFmCNAB_Rem2.AjustaString(const Texto, Compl: String;
const Tamanho: Integer; const Ajusta: Boolean; var Res: String): Boolean;
  function CompletaString(Texto, Compl: String; Tamanho: Integer;
    Alinhamento: dceAlinha): String;
  var
    Txt: String;
    Direita: Boolean;
  begin
    Direita := True;
    Txt := Texto;
    while Length(Txt) < Tamanho do
    begin
      case Alinhamento of
        posEsquerda: Txt := Txt + Compl;
        posCentro  :
        begin
          if Direita then
            Txt := Txt + Compl
          else
            Txt := Compl + Txt;
          Direita := not Direita;
        end;
        posDireita:  Txt := Compl + Txt;
      end;
    end;
    Result := Txt;
  end;
var
  Txt: String;
  Direita: Boolean;
  Alinhamento: dceAlinha;
  //Data: TDateTime;
begin
  //Result := False;
  Direita := True;
  if Compl = '0' then
    Alinhamento := posDireita
  else
    Alinhamento := posEsquerda;
  Txt := CompletaString(Texto, Compl, Tamanho, Alinhamento);
  if Ajusta then
  begin
    while Length(Txt) > Tamanho do
    begin
      case Alinhamento of
        posEsquerda: Txt := Copy(Txt, 1, Length(Txt)-1);
        posCentro  :
        begin
          if Direita then
            Txt := Copy(Txt, 2, Length(Txt)-1)
          else
            Txt := Copy(Txt, 1, Length(Txt)-1);
          Direita := not Direita;
        end;
        posDireita: Txt := Copy(Txt, 2, Length(Txt)-1);
      end;
    end;
  end;
  Res := Txt;
  Result := Length(Txt) = Tamanho;
end;

procedure TFmCNAB_Rem2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCNAB_Rem2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

function TFmCNAB_Rem2.FormataCEP(CEP: Integer): String;
var
  T: Integer;
begin
  Result := FormatFloat('00000', CEP);
  T := Length(Result);
  if T = 7 then
    Result := '0' + Result;
  Result := FormatFloat('00000000', CEP);
end;

procedure TFmCNAB_Rem2.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCNAB_Rem2.dmkEd_0_001Change(Sender: TObject);
begin
  DmBco.QrBco.Close;
  DmBco.QrBco.Params[0].AsInteger := dmkEd_0_001.ValueVariant;
  DmBco.QrBco.Open;
  //
  dmkEdNomeBanco.Text := DmBco.QrBcoNome.Value;
end;

procedure TFmCNAB_Rem2.FormCreate(Sender: TObject);
  function OrdenaColunas(var Colunas: array of Integer): Integer;
  var
    i, k: integer;
  begin
    k := 0;
    for i := Low(Colunas) to High(Colunas) do
    begin
      Colunas[i] := k;
      inc(k, 1);
    end;
    Result := k;
  end;
  var
    i: Integer;
begin
  BtMover.Visible := False;
  //
  Grade_L.ColWidths[09] := 260;
  Grade_L.ColWidths[10] := 140;
  Grade_L.ColWidths[11] := 620;
  Grade_L.Cells[00, 0] := 'Linha';
  Grade_L.Cells[01, 0] := 'Tipo';
  Grade_L.Cells[02, 0] := 'Segm.';
  Grade_L.Cells[03, 0] := 'Coluna';
  Grade_L.Cells[04, 0] := 'Ini';
  Grade_L.Cells[05, 0] := 'Fim';
  Grade_L.Cells[06, 0] := 'Tam';
  Grade_L.Cells[07, 0] := 'Tam';
  Grade_L.Cells[08, 0] := 'ID';
  Grade_L.Cells[09, 0] := 'Valor';
  Grade_L.Cells[10, 0] := 'Campo';
  Grade_L.Cells[11, 0] := 'Descri��o';
  // N�o aparecer nada preto
  GradeG.ColCount := 0;
  GradeG.RowCount := 0;
  //
  PageControl1.ActivePageIndex := 1;
  PageControl2.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  PageControl4.ActivePageIndex := 0;
  //
  for i := 0 to MaxColsArr do
  begin
    FCols1[i] := 0; // Coluna 0
    FCols5[i] := 0; // Coluna 0
  end;
  FLinG1 := 0;
  FColG1 := 0;
  F1_011 := Incrementa(Grade_1, FColG1, 1, FCols1, 011);
  F1_400 := Incrementa(Grade_1, FColG1, 1, FCols1, 400);
  F1_401 := Incrementa(Grade_1, FColG1, 1, FCols1, 401);
  F1_020 := Incrementa(Grade_1, FColG1, 1, FCols1, 020);
  F1_022 := Incrementa(Grade_1, FColG1, 1, FCols1, 022);
  F1_021 := Incrementa(Grade_1, FColG1, 1, FCols1, 021);
  F1_023 := Incrementa(Grade_1, FColG1, 1, FCols1, 023);
  F1_024 := Incrementa(Grade_1, FColG1, 1, FCols1, 024);
  F1_506 := Incrementa(Grade_1, FColG1, 1, FCols1, 506);
  F1_501 := Incrementa(Grade_1, FColG1, 1, FCols1, 501);
  F1_509 := Incrementa(Grade_1, FColG1, 1, FCols1, 509);
  F1_508 := Incrementa(Grade_1, FColG1, 1, FCols1, 508);
  F1_504 := Incrementa(Grade_1, FColG1, 1, FCols1, 504);
  F1_502 := Incrementa(Grade_1, FColG1, 1, FCols1, 502);
  F1_580 := Incrementa(Grade_1, FColG1, 1, FCols1, 580);
  F1_550 := Incrementa(Grade_1, FColG1, 1, FCols1, 550);
  F1_001 := Incrementa(Grade_1, FColG1, 1, FCols1, 001);
  F1_507 := Incrementa(Grade_1, FColG1, 1, FCols1, 507);
  F1_520 := Incrementa(Grade_1, FColG1, 1, FCols1, 520);
  F1_583 := Incrementa(Grade_1, FColG1, 1, FCols1, 583);
  F1_701 := Incrementa(Grade_1, FColG1, 1, FCols1, 701);
  F1_702 := Incrementa(Grade_1, FColG1, 1, FCols1, 702);
  F1_711 := Incrementa(Grade_1, FColG1, 1, FCols1, 711);
  F1_712 := Incrementa(Grade_1, FColG1, 1, FCols1, 712);
  F1_950 := Incrementa(Grade_1, FColG1, 1, FCols1, 950);
  F1_572 := Incrementa(Grade_1, FColG1, 1, FCols1, 572);
  F1_574 := Incrementa(Grade_1, FColG1, 1, FCols1, 574);
  F1_576 := Incrementa(Grade_1, FColG1, 1, FCols1, 576);
  F1_573 := Incrementa(Grade_1, FColG1, 1, FCols1, 573);
  F1_575 := Incrementa(Grade_1, FColG1, 1, FCols1, 575);
  F1_577 := Incrementa(Grade_1, FColG1, 1, FCols1, 577);
  F1_801 := Incrementa(Grade_1, FColG1, 1, FCols1, 801);
  F1_802 := Incrementa(Grade_1, FColG1, 1, FCols1, 802);
  F1_803 := Incrementa(Grade_1, FColG1, 1, FCols1, 803);
  F1_811 := Incrementa(Grade_1, FColG1, 1, FCols1, 811);
  F1_812 := Incrementa(Grade_1, FColG1, 1, FCols1, 812);
  F1_813 := Incrementa(Grade_1, FColG1, 1, FCols1, 813);
  F1_814 := Incrementa(Grade_1, FColG1, 1, FCols1, 814);
  F1_805 := Incrementa(Grade_1, FColG1, 1, FCols1, 805);
  F1_806 := Incrementa(Grade_1, FColG1, 1, FCols1, 806);
  F1_807 := Incrementa(Grade_1, FColG1, 1, FCols1, 807);
  F1_808 := Incrementa(Grade_1, FColG1, 1, FCols1, 808);
  F1_853 := Incrementa(Grade_1, FColG1, 1, FCols1, 853);
  F1_584 := Incrementa(Grade_1, FColG1, 1, FCols1, 584);
  F1_586 := Incrementa(Grade_1, FColG1, 1, FCols1, 586);
  F1_552 := Incrementa(Grade_1, FColG1, 1, FCols1, 552);
  F1_569 := Incrementa(Grade_1, FColG1, 1, FCols1, 569);
  F1_551 := Incrementa(Grade_1, FColG1, 1, FCols1, 551);
  F1_558 := Incrementa(Grade_1, FColG1, 1, FCols1, 558);
  F1_621 := Incrementa(Grade_1, FColG1, 1, FCols1, 621);
  F1_639 := Incrementa(Grade_1, FColG1, 1, FCols1, 639);
  F1_640 := Incrementa(Grade_1, FColG1, 1, FCols1, 640);
  // Inicio Banco do Brasil
  F1_641 := Incrementa(Grade_1, FColG1, 1, FCols1, 641);
  F1_642 := Incrementa(Grade_1, FColG1, 1, FCols1, 642);
  F1_643 := Incrementa(Grade_1, FColG1, 1, FCols1, 643);
  F1_644 := Incrementa(Grade_1, FColG1, 1, FCols1, 644);
  F1_645 := Incrementa(Grade_1, FColG1, 1, FCols1, 645);
  F1_646 := Incrementa(Grade_1, FColG1, 1, FCols1, 646);
  F1_647 := Incrementa(Grade_1, FColG1, 1, FCols1, 647);
  F1_648 := Incrementa(Grade_1, FColG1, 1, FCols1, 648);
  F1_649 := Incrementa(Grade_1, FColG1, 1, FCols1, 649);
  F1_650 := Incrementa(Grade_1, FColG1, 1, FCols1, 650);
  // Fim Banco do Brasil
  F1_804 := Incrementa(Grade_1, FColG1, 1, FCols1, 804);
  //
  ConfGr(Grade_1, F1_000, 032, 'Item');
  ConfGr(Grade_1, F1_011, 018, 'TR');
  ConfGr(Grade_1, F1_400, 018, 'TIE - Tipo inscr. empresa');
  ConfGr(Grade_1, F1_401, 112, 'Inscr. Empresa');
  ConfGr(Grade_1, F1_020, 032, 'Ag�n.');
  ConfGr(Grade_1, F1_022, 016, 'DV');
  ConfGr(Grade_1, F1_021, 072, 'Conta corrente');
  ConfGr(Grade_1, F1_023, 016, 'DV');
  ConfGr(Grade_1, F1_024, 020, 'DAC');
  ConfGr(Grade_1, F1_506, 072, 'Seu n�mero');
  ConfGr(Grade_1, F1_501, 056, 'Nosso n�mero');
  ConfGr(Grade_1, F1_509, 024, 'Carteira');
  ConfGr(Grade_1, F1_508, 024, 'C�d.Cart');
  ConfGr(Grade_1, F1_504, 024, 'Ocorrencia');
  ConfGr(Grade_1, F1_502, 072, 'Duplicata');
  ConfGr(Grade_1, F1_580, 056, 'Vencto');
  ConfGr(Grade_1, F1_550, 080, 'Valor doc.');
  ConfGr(Grade_1, F1_001, 024, 'Bco');
  ConfGr(Grade_1, F1_507, 020, 'Esp.T�t');
  ConfGr(Grade_1, F1_520, 012, 'Aceite');
  ConfGr(Grade_1, F1_583, 056, 'Emiss�o');
  ConfGr(Grade_1, F1_701, 020, 'I1 - instru��o 1');
  ConfGr(Grade_1, F1_711, 020, 'D1 - dias instru��o 1');
  ConfGr(Grade_1, F1_702, 020, 'I2 - instru��o 2');
  ConfGr(Grade_1, F1_712, 020, 'D2 - dias instru��o 2');
  ConfGr(Grade_1, F1_950, 020, 'Dias �nico');
  ConfGr(Grade_1, F1_572, 080, '$ mora dd');
  ConfGr(Grade_1, F1_573, 080, '$ multa');
  ConfGr(Grade_1, F1_574, 080, '% mora/m�s');
  ConfGr(Grade_1, F1_575, 080, '% multa');
  ConfGr(Grade_1, F1_576, 024, 'T.Mora');
  ConfGr(Grade_1, F1_577, 024, 'T.Multa');
  ConfGr(Grade_1, F1_801, 020, 'TIS - Tipo inscr. sacado');
  ConfGr(Grade_1, F1_802, 112, 'Inscr. Sacado');
  ConfGr(Grade_1, F1_803, 210, 'Nome do Sacado');
  ConfGr(Grade_1, F1_811, 048, 'T.logr.');
  ConfGr(Grade_1, F1_812, 140, 'Logradouro');
  ConfGr(Grade_1, F1_813, 035, 'N�');
  ConfGr(Grade_1, F1_814, 072, 'Complem.');
  ConfGr(Grade_1, F1_805, 084, 'Bairro');
  ConfGr(Grade_1, F1_806, 064, 'CEP');
  ConfGr(Grade_1, F1_807, 108, 'Cidade');
  ConfGr(Grade_1, F1_808, 020, 'UF');
  ConfGr(Grade_1, F1_853, 210, 'Nome do Sacador / avalista');
  ConfGr(Grade_1, F1_584, 056, 'Dt mora');
  ConfGr(Grade_1, F1_586, 056, 'Dt.lim.desconto');
  ConfGr(Grade_1, F1_552, 064, 'Val.Desconto');
  ConfGr(Grade_1, F1_569, 064, 'Val. IOF');
  ConfGr(Grade_1, F1_551, 064, 'Val.Abatimento');
  ConfGr(Grade_1, F1_804, 280, 'Logradouro completo');
  ConfGr(Grade_1, F1_621, 018, 'Quem imprime?');
  ConfGr(Grade_1, F1_639, 077, 'Msg 1 (237) 12 posi��es');
  ConfGr(Grade_1, F1_640, 200, 'Msg 2 (237) 60 posi��es');
  ConfGr(Grade_1, F1_558, 064, 'Val.Bonifica��o');
  // Inicio Banco do Brasil       
  ConfGr(Grade_1, F1_641, 064, 'Conv�nio L�der');
  ConfGr(Grade_1, F1_642, 064, 'Conv�nio de Cobran�a');
  ConfGr(Grade_1, F1_643, 064, 'Indicativo de Mensagem ou Sacador/Avalista');
  ConfGr(Grade_1, F1_644, 064, 'Tipo de cobran�a (Forma de registro');
  ConfGr(Grade_1, F1_645, 064, 'Varia��o da carteira');
  ConfGr(Grade_1, F1_646, 064, 'Comando');
  ConfGr(Grade_1, F1_647, 064, 'N�mero de dias para protesto');
  //  648: Result := 'DV do Nosso N�mero (BB)';  //?????
  //  649: Result := 'Prefixo do t�tulo';
  ConfGr(Grade_1, F1_650, 100, 'Mensagem 40 cararcteres (BB)');
  // Fim Banco do Brasil
  FLinG5 := 0;
  FColG5 := 0;
  F5_000 := Incrementa(Grade_5, FColG5, 1, FCols5, 000);
  F5_011 := Incrementa(Grade_5, FColG5, 1, FCols5, 011);
  F5_815 := Incrementa(Grade_5, FColG5, 1, FCols5, 815);
  F5_850 := Incrementa(Grade_5, FColG5, 1, FCols5, 850);
  F5_851 := Incrementa(Grade_5, FColG5, 1, FCols5, 851);
  F5_852 := Incrementa(Grade_5, FColG5, 1, FCols5, 852);
  F5_861 := Incrementa(Grade_5, FColG5, 1, FCols5, 861);
  F5_862 := Incrementa(Grade_5, FColG5, 1, FCols5, 862);
  F5_863 := Incrementa(Grade_5, FColG5, 1, FCols5, 863);
  F5_864 := Incrementa(Grade_5, FColG5, 1, FCols5, 864);
  F5_855 := Incrementa(Grade_5, FColG5, 1, FCols5, 855);
  F5_856 := Incrementa(Grade_5, FColG5, 1, FCols5, 856);
  F5_857 := Incrementa(Grade_5, FColG5, 1, FCols5, 857);
  F5_858 := Incrementa(Grade_5, FColG5, 1, FCols5, 858);
  F5_854 := Incrementa(Grade_5, FColG5, 1, FCols5, 854);
  //
  ConfGr(Grade_5, 000000, 032, 'Seq.');
  ConfGr(Grade_5, F5_000, 032, 'Item');
  ConfGr(Grade_5, F5_011, 018, 'TR');
  ConfGr(Grade_5, F5_815, 200, 'Emeio do sacado');
  ConfGr(Grade_5, F5_851, 020, 'TIS - Tipo inscr. sacador / avalista');
  ConfGr(Grade_5, F5_852, 112, 'Inscr. Sacador / avalista');
  ConfGr(Grade_5, F5_861, 048, 'T.logr. do Sacador / avalista');
  ConfGr(Grade_5, F5_862, 140, 'Logradouro do Sacador / avalista');
  ConfGr(Grade_5, F5_863, 035, 'N�do logradouro do Sacador / avalista');
  ConfGr(Grade_5, F5_864, 072, 'Complem. do endere�o do Sacador / avalista');
  ConfGr(Grade_5, F5_855, 084, 'Bairro do Sacador / avalista');
  ConfGr(Grade_5, F5_856, 064, 'CEP do Sacador / avalista');
  ConfGr(Grade_5, F5_857, 108, 'Cidade do Sacador / avalista');
  ConfGr(Grade_5, F5_858, 020, 'UF do Sacador / avalista');
  ConfGr(Grade_5, F5_854, 210, 'Logradouro completo do Sacador / avalista');
end;

function TFmCNAB_Rem2.AddLinha(var Contador: Integer; const Grade: TStringGrid): Integer;
begin
  Contador       := Contador + 1;
  Grade.RowCount := Contador + 1;
  Grade.Cells[0,Contador] := FormatFloat('0000', Contador);
  //
  Result := Contador;
end;

procedure TFmCNAB_Rem2.AddCampo(Grade: TStringGrid; Linha, Coluna: Integer;
              Formato: String; Valor: Variant);
var
  Texto: String;
begin
  Texto := MLAGeral.VariavelToTexto(Formato, Valor);
  Grade.Cells[Coluna, Linha] := Texto;
end;

function TFmCNAB_Rem2.Incrementa(const Grade: TStringGrid; var Contador: Integer;
             const Incremento: Integer; var ListaCols: TArrayCols;
             const MyCodField: Integer): Integer;
begin
  inc(Contador, Incremento);
  Result := Contador;
  ListaCols[MyCodField] := Contador;
  if Grade.ColCount < Result + 1 then
    Grade.ColCount := Result + 1;
end;

function TFmCNAB_Rem2.CarregaDadosNoForm(CNAB_Cfg, SeqArq, Lote: Integer;
SQL_HEADE_ARQUI, SQL_HEADE_LOTES, SQL_ITENS_LOTES: String;
DataType: array of TFieldType; Campos: array of String): Boolean;
  function Endereco(): String;
  begin
    Result := DmBco.QrAddr3NOMELOGRAD.Value;
    if Result <> '' then Result := Result + ' ';

    Result := Result + DmBco.QrAddr3RUA.Value;
    if DmBco.QrAddr3RUA.Value <> '' then Result := Result + ', ';

    Result := Result + DmBco.QrAddr3NUMERO_TXT.Value;
    if DmBco.QrAddr3NUMERO_TXT.Value <> '' then Result := Result + ' ';

    if DmBco.QrAddr3COMPL.Value <> '' then Result := Result + '- ';
    Result := Result + DmBco.QrAddr3COMPL.Value;

  end;
const
  // PAREI AQUI Ver!!!
  Def_Client = -11;
var
  j: Integer;
  SQL: String;
  {HoraI: TDateTime;}
begin
  Result := False;
  FLot_RegCount := 0;
  FArq_RegCount := 0;
  FArq_LotCount := 0;
  if CNAB_Cfg = 0 then
  begin
    Geral.MensagemBox('Configura��o CNAB n�o definida!', 'Aviso',
      MB_OK + MB_ICONWARNING);
    Exit;
  end;
  (*
  HoraI := Now();

  dmkEdHoraI.ValueVariant := HoraI;
  *)
  Screen.Cursor := crHourGlass;
  //
  QrArqui.Close;
  QrArqui.SQL.Text := SQL_HEADE_ARQUI;
  UMyMod.AbreQuery(QrArqui, Dmod.MyDB, 'TFmCNAB_Rem.CarregaDadosNoForm()');
  //
  QrLotes.Close;
  QrLotes.SQL.Text := SQL_HEADE_LOTES;
  UMyMod.AbreQuery(QrLotes, Dmod.MyDB, 'TFmCNAB_Rem.CarregaDadosNoForm()');
  //
  // N�o precisa de confer�ncia de cliente interno ?
  SQL := 'SELECT 0 Itens';
  if not DmBco.ReopenCNAB_Cfg(CNAB_Cfg,
    Def_Client, SQL,
    FCedenteTipo, FCedenteNome, FCedenteCNPJ,
    FSacadorTipo, FSacadorNome, FSacadorCNPJ) then
  begin
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  if DmBco.QrCNAB_CfgCedNome.Value <> '' then
    FNomeCedente := DmBco.QrCNAB_CfgCedNome.Value
  else FNomeCedente := FCedenteNome;
  //
  if DmBco.QrCNAB_CfgSacAvaNome.Value <> '' then
    FNomeSacador := DmBco.QrCNAB_CfgSacAvaNome.Value
  else FNomeSacador := FSacadorNome;
  //
  if SeqArq > 0 then
  begin
    if Geral.MB_Pergunta('Este lote j� possui um n�mero sequencial ' +
      'de remessa de arquivo! Ser� utilizado o mesmo n�mero.' + sLineBreak +
      'Alguns bancos podem n�o aceitar o mesmo n�mero sequencial mais de uma vez!.' + sLineBreak +
      '- Caso voc� ainda n�o enviou este arquivo, desconsidere este aviso.' + sLineBreak +
      '- Caso queira usar um n�mero espec�fico informe o n�mero anterior a ele na configura��o de remessa correspondente.'+ sLineBreak+
      '- Caso queira zerar o n�mero cancele e use o menu do bot�o de lotes deste formul�rio'+ sLineBreak +
      '- Caso queira continuar assim mesmo confirme esta a��o') <> ID_YES then Exit;
  end;
  // Dados para gera��o do arquivo
  // Banco
  FBanco := DmBco.QrCNAB_CfgCedBanco.Value;
  // Lote
  FLote := (*QrProtocoPak*)(*QrCNAB_LotCodigo.Value*)Lote;
  // N�mero de remessa do arquivo (Bradesco)
  FSeqArqRem := (*QrProtocoPak*)(*QrCNAB_LotSeqArq.Value*)SeqArq;

  //////////////////////////////////////////////////////////////////////////////
  //  0 - H E A D E R   D E   A R Q U I V O                                   //
  //////////////////////////////////////////////////////////////////////////////
  if not Resgistro_240_0(FBanco) then Exit;
  QrLotes.First;
  while not QrLotes.Eof do
  begin
    if not Resgistro_240_1(FBanco) then Exit;
    QrItens.Close;
    QrItens.SQL.Text := SQL_ITENS_LOTES;
    for J := Low(Campos) to High(Campos) do
    begin
      case DataType[J] of
        ftInteger: QrItens.Params[J].AsInteger := QrLotes.FieldByName(Campos[J]).AsInteger
        else
        Geral.MensagemBox('Tipo de campo ("TField") n�o definido na abertura do "QrItens"!',
        'Aviso', MB_OK+MB_ICONWARNING);
      end;
    end;
    UMyMod.AbreQuery(QrItens, Dmod.MyDB, 'TFmCNAB_Rem.CarregaDadosNoForm()');
    while not QrItens.Eof do
    begin
      if not Resgistro_240_3_P(FBanco, QrItens.RecNo) then Exit;
      if not Resgistro_240_3_Q(FBanco, QrItens.RecNo) then Exit;
      if not Resgistro_240_3_R(FBanco, QrItens.RecNo) then Exit;
      //if not Resgistro_240_3_S(FBanco) then Exit;
      //
      QrItens.Next;
    end;
    if not Resgistro_240_5(FBanco) then Exit;
    QrLotes.Next;
  end;
  if not Resgistro_240_9(FBanco) then Exit;
  Result := True;
end;

procedure TFmCNAB_Rem2.ConfGr(Grade: TStringGrid; Coluna, Tam: Integer;
Titulo: String);
begin
  Grade.ColWidths[Coluna] := Tam;
  Grade.Cells[Coluna,  0] := Titulo;
end;

procedure TFmCNAB_Rem2.Grade_1SelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var
  i: Integer;
begin
  for i := 1 to 1000 do
    if FCols1[i] = ACol then
    begin
      LaCodDmk_1.Caption := FormatFloat('000', i);
      Break;
    end;
end;

function TFmCNAB_Rem2.NovaLinhaArq(): String;
begin
  inc(FLinArq, 1);
  Result := FormatFloat('0', FLinArq);
end;

procedure TFmCNAB_Rem2.Edit2Change(Sender: TObject);
begin
  PesquisaTexto;
end;

procedure TFmCNAB_Rem2.Edit3Change(Sender: TObject);
begin
  PesquisaTexto;
end;

procedure TFmCNAB_Rem2.Edit4Change(Sender: TObject);
begin
  PesquisaTexto;
end;

procedure TFmCNAB_Rem2.PageControl2Change(Sender: TObject);
begin
  PesquisaTexto;
end;

function TFmCNAB_Rem2.CriaListaCNAB(const CNAB, Banco, Registro: Integer;
Segmento: Char; var Lista: MyArrayLista; const SubSegmento: Integer): Boolean;
var
  CNABResult: TCNABResult;
begin
  case CNAB of
    240: CNABResult := UBco_Rem.LayoutCNAB240(Banco, Registro, Segmento, Lista, SubSegmento, '');
    400: CNABResult := UBco_Rem.LayoutCNAB400(Banco, Registro, Lista, SubSegmento, '');
    else CNABResult := cresNoCNAB;
  end;
  if not (CNABResult in ([cresNoField, cresOKField])) then
  begin
    UBancos.CNABResult_Msg(CNABResult, CNAB, Banco, Registro, ecnabRemessa);
    Result := False;
  end else Result := True;
end;

procedure TFmCNAB_Rem2.BtListarClick(Sender: TObject);
begin
  //Listar campos!!

end;

procedure TFmCNAB_Rem2.BtMoverClick(Sender: TObject);
var
  ArqOri, ArqDes: String;
begin
  ArqOri := EdArqGerado.Text;
  //
  if (ArqOri <> '') and (FileExists(ArqOri)) then
  begin
    ArqDes := ExtractFilePath(ArqOri) + '\Enviados\' + ExtractFileName(ArqOri);
    //
    dmkPF.MoveArq(ArqOri, ArqDes);
  end;
  BtMover.Visible := FileExists(ArqOri);
end;

procedure TFmCNAB_Rem2.BtOKClick(Sender: TObject);
begin
  case DmBco.QrCNAB_CfgCNAB.Value of
    240: GeraCNAB240();
    400: GeraCNAB400();
    else Geral.MensagemBox('CNAB n�o implementado!', 'Aviso',
         MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmCNAB_Rem2.GeraCNAB240();
const
  SubSegmento = 0;
  CNAB = 240;
var
  //Lista_: MyArrayLista;
  Lista0: MyArrayLista;
  Lista1: MyArrayLista;
  ListaP: MyArrayLista;
  ListaQ: MyArrayLista;
  ListaR: MyArrayLista;
  Lista5: MyArrayLista;
  Lista9: MyArrayLista;
  Segmento: Char;
  Lot, Lin, Col, Banco, I, J, K, Ini, Fim, Tam, IDc: Integer;
  Arquivo, SeqArq, Des, Nom, Val, LinReg: String;
  Continua: Boolean;
begin
  Screen.Cursor := crHourGlass;
  MeGerado.Lines.Clear;
  Segmento      := #0;
  Banco         := DmBco.QrCNAB_CfgCedBanco.Value;
  //
  if not CriaListaCNAB(CNAB, Banco, 0, Segmento, Lista0, SubSegmento) then
  begin Screen.Cursor := crDefault; Exit; end;
  //
  if not CriaListaCNAB(CNAB, Banco, 1, Segmento, Lista1, SubSegmento) then
  begin Screen.Cursor := crDefault; Exit; end;
  //
  if not CriaListaCNAB(CNAB, Banco, 5, Segmento, Lista5, SubSegmento) then
  begin Screen.Cursor := crDefault; Exit; end;
  //
  if not CriaListaCNAB(CNAB, Banco, 9, Segmento, Lista9, SubSegmento) then
  begin Screen.Cursor := crDefault; Exit; end;
  //
  Segmento := 'P';
  if not CriaListaCNAB(CNAB, Banco, 3, Segmento, ListaP, SubSegmento) then
  begin Screen.Cursor := crDefault; Exit; end;
  //
  Segmento := 'Q';
  if not CriaListaCNAB(CNAB, Banco, 3, Segmento, ListaQ, SubSegmento) then
  begin Screen.Cursor := crDefault; Exit; end;
  //
  Segmento := 'R';
  if not CriaListaCNAB(CNAB, Banco, 3, Segmento, ListaR, SubSegmento) then
  begin Screen.Cursor := crDefault; Exit; end;

  // Registro 0
  LinReg := '';
  Col   := High(Lista0);
  Lin := 1;
  for I := 0 to Col do
  begin
    Val := Grade_240_0.Cells[I, 1];
    Tam := Geral.IMV(Lista0[I][_Tam]);
    while Length(Val) < Tam do
      Val := Val + ' ';
    //
    LinReg := LinReg + Val;
    Ini := Geral.IMV(Lista0[i][_Ini]);
    Tam := Geral.IMV(Lista0[i][_Tam]);
    Fim := Geral.IMV(Lista0[i][_Fim]);
    Des :=              Lista0[i][_Des];
    Nom :=              Lista0[i][_Nom];
    IDc := Geral.IMV(Lista0[i][_Fld]);
    AdicionaListaRow(Lin, 0, #0, I, Ini, Fim, Tam, Length(Val), IDc, Val, Nom, Des);
    if Length(LinReg) <> Geral.IMV(Lista0[i][_Fim]) then
    begin
      AvisaCancelamento(LinReg, Geral.IMV(Lista0[i][_Fim]), Length(LinReg));
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  MeGerado.Lines.Add(LinReg);
  //


  for J := 1 to Grade_240_1.RowCount - 1 do
  begin

    //  Registro 1
    LinReg := '';
    Col   := High(Lista1);
    Lot := Geral.IMV(Grade_240_1.Cells[1, J]);
    Lin := Lin + 1;
    for I := 0 to Col do
    begin
      Val := Grade_240_1.Cells[I, J];
      Tam := Geral.IMV(Lista1[I][_Tam]);
      while Length(Val) < Tam do
        Val := Val + ' ';
      //
      LinReg := LinReg + Val;
      Ini := Geral.IMV(Lista1[i][_Ini]);
      Tam := Geral.IMV(Lista1[i][_Tam]);
      Fim := Geral.IMV(Lista1[i][_Fim]);
      Des :=              Lista1[i][_Des];
      Nom :=              Lista1[i][_Nom];
      IDc := Geral.IMV(Lista1[i][_Fld]);
      AdicionaListaRow(Lin, 1, #0, I, Ini, Fim, Tam, Length(Val), IDc, Val, Nom, Des);
      if Length(LinReg) <> Geral.IMV(Lista1[i][_Fim]) then
      begin
        AvisaCancelamento(LinReg, Geral.IMV(Lista1[i][_Fim]), Length(LinReg));
        Screen.Cursor := crDefault;
        Exit;
      end;
    end;
    MeGerado.Lines.Add(LinReg);


    for K := 1 to Grade_240_3_P.RowCount - 1 do
    begin

      if Geral.IMV(Grade_240_3_P.Cells[1, k]) = Lot then
      begin
        // Registro 3P
        LinReg := '';
        Col   := High(ListaP);
        Lin := Lin + 1;
        for I := 0 to Col do
        begin
          Val := Grade_240_3_P.Cells[I, K];
          Tam := Geral.IMV(ListaP[I][_Tam]);
          while Length(Val) < Tam do
            Val := Val + ' ';
          //
          LinReg := LinReg + Val;
          Ini := Geral.IMV(ListaP[i][_Ini]);
          Tam := Geral.IMV(ListaP[i][_Tam]);
          Fim := Geral.IMV(ListaP[i][_Fim]);
          Des :=              ListaP[i][_Des];
          Nom :=              ListaP[i][_Nom];
          IDc := Geral.IMV(ListaP[i][_Fld]);
          AdicionaListaRow(Lin, 3, 'P', I, Ini, Fim, Tam, Length(Val), IDc, Val, Nom, Des);
          if Length(LinReg) <> Geral.IMV(ListaP[i][_Fim]) then
          begin
            AvisaCancelamento(LinReg, Geral.IMV(ListaP[i][_Fim]), Length(LinReg));
            Screen.Cursor := crDefault;
            Exit;
          end;
        end;
        MeGerado.Lines.Add(LinReg);

        // Registro 3Q
        LinReg := '';
        Col   := High(ListaQ);
        Lin := Lin + 1;
        for I := 0 to Col do
        begin
          Val := Grade_240_3_Q.Cells[I, K];
          Tam := Geral.IMV(ListaQ[I][_Tam]);
          while Length(Val) < Tam do
            Val := Val + ' ';
          //
          LinReg := LinReg + Val;
          Ini := Geral.IMV(ListaQ[i][_Ini]);
          Tam := Geral.IMV(ListaQ[i][_Tam]);
          Fim := Geral.IMV(ListaQ[i][_Fim]);
          Des :=              ListaQ[i][_Des];
          Nom :=              ListaQ[i][_Nom];
          IDc := Geral.IMV(ListaQ[i][_Fld]);
          AdicionaListaRow(Lin, 3, 'Q', I, Ini, Fim, Tam, Length(Val), IDc, Val, Nom, Des);
          if Length(LinReg) <> Geral.IMV(ListaQ[i][_Fim]) then
          begin
            AvisaCancelamento(LinReg, Geral.IMV(ListaQ[i][_Fim]), Length(LinReg));
            Screen.Cursor := crDefault;
            Exit;
          end;
        end;
        MeGerado.Lines.Add(LinReg);

        // Registro 3R
        LinReg := '';
        Col   := High(ListaR);
        Lin := Lin + 1;
        for I := 0 to Col do
        begin
          Val := Grade_240_3_R.Cells[I, K];
          Tam := Geral.IMV(ListaR[I][_Tam]);
          while Length(Val) < Tam do
            Val := Val + ' ';
          //
          LinReg := LinReg + Val;
          Ini := Geral.IMV(ListaR[i][_Ini]);
          Tam := Geral.IMV(ListaR[i][_Tam]);
          Fim := Geral.IMV(ListaR[i][_Fim]);
          Des :=              ListaR[i][_Des];
          Nom :=              ListaR[i][_Nom];
          IDc := Geral.IMV(ListaR[i][_Fld]);
          AdicionaListaRow(Lin, 3, 'R', I, Ini, Fim, Tam, Length(Val), IDc, Val, Nom, Des);
          if Length(LinReg) <> Geral.IMV(ListaR[i][_Fim]) then
          begin
            AvisaCancelamento(LinReg, Geral.IMV(ListaR[i][_Fim]), Length(LinReg));
            Screen.Cursor := crDefault;
            Exit;
          end;
        end;
        MeGerado.Lines.Add(LinReg);

      end;

    end;

    //  Registro 5
    LinReg := '';
    Col   := High(Lista5);
    //Lot := Geral.IMV(Grade_240_5.Cells[1, J]);
    Lin := Lin + 1;
    for I := 0 to Col do
    begin
      Val := Grade_240_5.Cells[I, J];
      Tam := Geral.IMV(Lista5[I][_Tam]);
      while Length(Val) < Tam do
        Val := Val + ' ';
      //
      LinReg := LinReg + Val;
      Ini := Geral.IMV(Lista5[i][_Ini]);
      Tam := Geral.IMV(Lista5[i][_Tam]);
      Fim := Geral.IMV(Lista5[i][_Fim]);
      Des :=              Lista5[i][_Des];
      Nom :=              Lista5[i][_Nom];
      IDc := Geral.IMV(Lista5[i][_Fld]);
      AdicionaListaRow(Lin, 5, #0, I, Ini, Fim, Tam, Length(Val), IDc, Val, Nom, Des);
      if Length(LinReg) <> Geral.IMV(Lista5[i][_Fim]) then
      begin
        AvisaCancelamento(LinReg, Geral.IMV(Lista5[i][_Fim]), Length(LinReg));
        Screen.Cursor := crDefault;
        Exit;
      end;
    end;
    MeGerado.Lines.Add(LinReg);

  end;
  //
  //

  // Registro 9
  LinReg := '';
  Col    := High(Lista9);
  Lin    := Lin + 1;
  for I := 0 to Col do
  begin
    Val := Grade_240_9.Cells[I, 1];
    Tam := Geral.IMV(Lista9[I][_Tam]);
    while Length(Val) < Tam do
      Val := Val + ' ';
    //
    LinReg := LinReg + Val;
    Ini := Geral.IMV(Lista9[i][_Ini]);
    Tam := Geral.IMV(Lista9[i][_Tam]);
    Fim := Geral.IMV(Lista9[i][_Fim]);
    Des :=              Lista9[i][_Des];
    Nom :=              Lista9[i][_Nom];
    IDc := Geral.IMV(Lista9[i][_Fld]);
    AdicionaListaRow(Lin, 9, #0, I, Ini, Fim, Tam, Length(Val), IDc, Val, Nom, Des);
    if Length(LinReg) <> Geral.IMV(Lista9[i][_Fim]) then
    begin
      AvisaCancelamento(LinReg, Geral.IMV(Lista9[i][_Fim]), Length(LinReg));
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  MeGerado.Lines.Add(LinReg);
  //



  //
  ObtemNumeroDoArquivo(SeqArq);
  Arquivo := dmkPF.CaminhoArquivo(DmBco.QrCNAB_CfgDiretorio.Value,
    FormatFloat('0000000', Geral.IMV(SeqArq)), 'Rem');
  if FileExists(Arquivo) then
    Continua := Geral.MB_Pergunta('O arquivo "' + Arquivo +
      ' j� existe. Deseja sobrescrev�-lo?') = ID_YES
  else
    Continua := True;
  if Continua then
  begin
    //if MLAGeral.SalvaTextoEmArquivo(Arquivo, MeGerado.Text, True) then
    // Banco do Brasil CNAB 240 exige s� #10 como quebra de linha!!!
    if MLAGeral.ExportaMemoToFileExt(MeGerado, Arquivo, True, False, True, 10, null) then
    begin
      EdArqGerado.Text := Arquivo;
      PageControl1.ActivePageIndex := 0;
      PageControl2.ActivePageIndex := 0;
      //
      Geral.MB_Aviso('O arquivo remessa foi salvo como "' +
        Arquivo + '". Voc� pode copiar o caminho da caixa de edi��o na aba "' +
        'Arquivos" aba "Gerado"!');
      //
      if Geral.MB_Pergunta('Deseja abrir o diret�rio?') = ID_YES then
        Geral.AbreArquivo(ExtractFileDir(Arquivo));
      //
      BtMover.Visible := True;
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmCNAB_Rem2.GeraCNAB400();
var
  Lin, Ini, Fim, Tam, i, j, Item, Lin1, Banco, CNAB, Campo, SubSegmento, IDc: Integer;
  Envio: TEnvioCNAB;
  ListaA, ListaB: MyArrayLista;
  Nom, Des, SeqArq, LinReg, ValCampo, Pre_Def, Arquivo: String;
  Continua: Boolean;
begin
  Item := 0;
  Screen.Cursor := crHourGlass;
  FRowL := 0;
  MeGerado.Clear;
  EdArqGerado.Text := '';
  FLinArq := 0;
  Banco := dmkEd_0_001.ValueVariant;
  case Banco of
    1: SubSegmento := DmBco.QrCNAB_CfgPosicoesBB.Value;
    else SubSegmento := -1;
  end;
  CNAB := DmBco.QrCNAB_CfgCNAB.Value;
  case dmkEd_0_005.ValueVariant of
     1: Envio := ecnabRemessa;
     2: Envio := ecnabRetorno;
    else Envio := ecnabIndefinido;
  end;
  if not UBancos.BancoImplementado(Banco, CNAB, Envio) then begin Screen.Cursor := crDefault; Exit; end;
  //
  ////////////////////// 0 - REGISTRO HEADER ///////////////////////////////////
  if not CriaListaCNAB(CNAB, Banco, 0, #0, ListaA, SubSegmento) then
  begin Screen.Cursor := crDefault; Exit; end;
  LinReg := '';
  for i := Low(ListaA) to High(ListaA) do
  begin
    Campo := Geral.IMV(ListaA[i][_Fld]);
    case Campo of
      -02: Pre_Def := ' ';
      -01: Pre_def := '0';
      009: Pre_Def := '1'; // Remessa
      020: Pre_Def := dmkEd_0_020.Text;
      021: Pre_Def := dmkEd_0_021.Text;
      022: Pre_Def := dmkEd_0_022.Text;
      023: Pre_Def := dmkEd_0_023.Text;
      024: Pre_Def := dmkEd_0_024.Text;
      401: Pre_Def := dmkEd_0_401.Text;
      402: Pre_Def := dmkEd_0_402.Text;
      410: Pre_Def := dmkEd_0_410.Text;
      641: Pre_Def := dmkEd_0_641.Text;
      699: Pre_Def := dmkEd_0_699.Text;
      990: Pre_Def := dmkEd_0_990.Text;
      991: Pre_Def := dmkEd_0_991.Text;
      998: ObtemNumeroDoArquivo(Pre_Def);
      999: Pre_Def := NovaLinhaArq();
      400:
      begin
        UBco_Rem.TipoDeInscricao(dmkEd_0_400.ValueVariant, FBanco, CNAB, 400, '', Pre_Def);
        // Tirar zeros � esquerda
        Pre_Def := FormatFloat('0', Geral.IMV(Pre_Def));
      end
      else Pre_def := '';
    end;
    if not ValorDeCampo(ListaA, i, Pre_Def, ValCampo) then begin Screen.Cursor := crDefault; Exit; end;
    //
    LinReg := LinReg + ValCampo;
    //
    Lin := 1;
    Ini := Geral.IMV(ListaA[i][_Ini]);
    Tam := Geral.IMV(ListaA[i][_Tam]);
    Fim := Geral.IMV(ListaA[i][_Fim]);
    Des :=              ListaA[i][_Des];
    Nom :=              ListaA[i][_Nom];
    IDc := Geral.IMV(ListaA[i][_Fld]);
    AdicionaListaRow(Lin, 0, #0, i, Ini, Fim, Tam, Length(ValCampo), IDc, ValCampo, Nom, Des);
    if Length(LinReg) <> Geral.IMV(ListaA[i][_Fim]) then
    begin
      AvisaCancelamento(LinReg, Geral.IMV(ListaA[i][_Fim]), Length(LinReg));
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  MeGerado.Lines.Add(LinReg);
  LinReg := '';
  ////////////////////// 1 - REGISTRO DETALHE OBRIGAT�RIO  /////////////////////
  if (Banco = 1) and (SubSegmento = 7) then
  begin
    if not CriaListaCNAB(CNAB, Banco, 7, #0, ListaA, SubSegmento) then
      begin Screen.Cursor := crDefault; Exit; end;
  end else begin
    if not CriaListaCNAB(CNAB, Banco, 1, #0, ListaA, SubSegmento) then
      begin Screen.Cursor := crDefault; Exit; end;
  end;
  ////////////////////// 5 - REGISTRO DETALHE OPCIONAL /////////////////////////
  if not CriaListaCNAB(CNAB, Banco, 5, #0, ListaB, SubSegmento) then
    begin Screen.Cursor := crDefault; Exit; end;
  for Item := 1 to Grade_1.RowCount - 1 do
  begin
    Lin1 := Geral.IMV(Grade_1.Cells[F1_000, Item]);
    if Lin1 <> Item then
    begin
      Geral.MB_Erro('A linha ' + Geral.FF0(Item) + ' da grade 1 ' +
        ' n�o � "Item" = ' + Geral.FF0(Item) + '!' + sLineBreak + 'Gera��o de arquivo ' +
        'cancelada!');
        Screen.Cursor := crDefault;
      Exit;
    end;

    //  Grade 1 (Detalhe obrigat�rio)
    for i := Low(ListaA) to High(ListaA) do
    begin
      if not ValorDeColunaDeGrade(Grade_1, ListaA, i, FCols1, Item, Pre_Def) then
        begin Screen.Cursor := crDefault; Exit; end;
      if not ValorDeCampo(ListaA, i, Pre_Def, ValCampo) then
        begin Screen.Cursor := crDefault; Exit; end;
      //
      LinReg := LinReg + ValCampo;
      //
      Lin := 1 + Item;
      Ini := Geral.IMV(ListaA[i][_Ini]);
      Tam := Geral.IMV(ListaA[i][_Tam]);
      Fim := Geral.IMV(ListaA[i][_Fim]);
      Des :=              ListaA[i][_Des];
      Nom :=              ListaA[i][_Nom];
      IDc := Geral.IMV(ListaA[i][_Fld]);
      AdicionaListaRow(Lin, 1, #0, i, Ini, Fim, Tam, Length(ValCampo), IDc, ValCampo, Nom, Des);
      if Length(LinReg) <> Geral.IMV(ListaA[i][_Fim]) then
      begin
        AvisaCancelamento(LinReg, Geral.IMV(ListaA[i][_Fim]), Length(LinReg));
        Screen.Cursor := crDefault;
        Exit;
      end;
    end;
    MeGerado.Lines.Add(LinReg);
    LinReg := '';

    // Grade 5 (detalhe opcional) (Banco 341 tem mais algum?)
    // primeiro verifica se existe um detalhe 5 referente ao detalhe 1
    for j := 1 to Grade_5.RowCount -1 do
    begin
      if Geral.IMV(Grade_5.Cells[F5_000, j]) = Lin1 then
      begin
        for i := Low(ListaB) to High(ListaB) do
        begin
          if not ValorDeColunaDeGrade(Grade_5, ListaB, i, FCols5, Item, Pre_Def) then
            begin Screen.Cursor := crDefault; Exit; end;
          if not ValorDeCampo(ListaB, i, Pre_Def, ValCampo) then
            begin Screen.Cursor := crDefault; Exit; end;
          //
          LinReg := LinReg + ValCampo;
          Ini := Geral.IMV(ListaA[i][_Ini]);
          Tam := Geral.IMV(ListaA[i][_Tam]);
          Fim := Geral.IMV(ListaA[i][_Fim]);
          Des :=              ListaA[i][_Des];
          Nom :=              ListaA[i][_Nom];
          IDc := Geral.IMV(ListaA[i][_Fld]);
          AdicionaListaRow(1 + Item, 5, #0, i, Ini, Fim, Tam, Length(ValCampo), IDc, ValCampo, Nom, Des);
          if Length(LinReg) <> Geral.IMV(ListaB[i][_Fim]) then
          begin
            AvisaCancelamento(LinReg, Geral.IMV(ListaB[i][_Fim]), Length(LinReg));
            Screen.Cursor := crDefault;
            Exit;
          end;
        end;
        MeGerado.Lines.Add(LinReg);
        LinReg := '';
      end;
    end;
  end;
  ////////////////////// 9 - REGISTRO TRAILLER  ////////////////////////////////
  if not CriaListaCNAB(CNAB, Banco, 9, #0, ListaA, SubSegmento) then
    begin Screen.Cursor := crDefault; Exit; end;
  for i := Low(ListaA) to High(ListaA) do
  begin
    Campo := Geral.IMV(ListaA[i][_Fld]);
    case Campo of
      -02: Pre_Def := ' ';
      -01: Pre_def := '0';
      999: Pre_Def := NovaLinhaArq();
      else Pre_def := '';
    end;
    if not ValorDeCampo(ListaA, i, Pre_Def, ValCampo) then begin Screen.Cursor := crDefault; Exit; end;
    //
    LinReg := LinReg + ValCampo;
    Lin := Lin + 1;
    Ini := Geral.IMV(ListaA[i][_Ini]);
    Tam := Geral.IMV(ListaA[i][_Tam]);
    Fim := Geral.IMV(ListaA[i][_Fim]);
    Des :=              ListaA[i][_Des];
    Nom :=              ListaA[i][_Nom];
    IDc := Geral.IMV(ListaA[i][_Fld]);
    AdicionaListaRow(1 + Item, 9, #0, i, Ini, Fim, Tam, Length(ValCampo), IDc, ValCampo, Nom, Des);
    if Length(LinReg) <> Geral.IMV(ListaA[i][_Fim]) then
    begin
      AvisaCancelamento(LinReg, Geral.IMV(ListaA[i][_Fim]), Length(LinReg));
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  MeGerado.Lines.Add(LinReg);
  LinReg := '';

  //
  ObtemNumeroDoArquivo(SeqArq);
  Arquivo := dmkPF.CaminhoArquivo(DmBco.QrCNAB_CfgDiretorio.Value,
    FormatFloat('0000000', Geral.IMV(SeqArq)), 'Rem');
  if FileExists(Arquivo) then
    Continua := Geral.MB_Pergunta('O arquivo "' + Arquivo +
      ' j� existe. Deseja sobrescrev�-lo?') = ID_YES
  else
    Continua := True;
  if Continua then
  begin
    if Geral.SalvaTextoEmArquivo(Arquivo, MeGerado.Text, True) then
    begin
      EdArqGerado.Text := Arquivo;
      PageControl1.ActivePageIndex := 0;
      PageControl2.ActivePageIndex := 0;
      //
      Geral.MB_Aviso('O arquivo remessa foi salvo como "' +
        Arquivo + '". Voc� pode copiar o caminho da caixa de edi��o na aba "' +
        'Arquivos" aba "Gerado"!');
      //
      if Geral.MB_Pergunta('Deseja abrir o diret�rio?') = ID_YES then
        Geral.AbreArquivo(ExtractFileDir(Arquivo));
      //
      BtMover.Visible := True;
    end;
  end;
  Screen.Cursor := crDefault;
end;

function TFmCNAB_Rem2.GetCNAB(): Integer;
begin
  Result := DmBco.QrCNAB_CfgCNAB.Value;
end;

procedure TFmCNAB_Rem2.MeGeradoEnter(Sender: TObject);
begin
  FEditor := MeGerado;
end;

procedure TFmCNAB_Rem2.MeGeradoExit(Sender: TObject);
begin
  FEditor := nil;
end;

function TFmCNAB_Rem2.MoraDia_Percentual(Banco, CNAB, JurosTipo: Integer; ValorBruto,
  JurosPerc: Double): Double;
begin
  Result := 0;
  case Banco of
    001:
    begin
      if CNAB = 240 then
      begin
        case JurosTipo of
          1: Result := Trunc(ValorBruto * JurosPerc / 30) / 100;
          2: Result := JurosPerc;
          3: Result := 0;
          else Geral.MensagemBox('Tipo de juros n�o implementado para o banco ' +
          '001 na function "TFmCNAB_Rem2.MoraDia_Percentual()"!' + sLineBreak +
          'Verifique se foi selecionado um tipo v�lido para cobran�a de juros '+
          'na configura��o CNAB correspondente!',
          'Aviso', MB_OK+MB_ICONWARNING);
        end;
      end else Geral.MensagemBox('Tipo de CNAB n�o implementado para o banco ' +
          '001 na function "TFmCNAB_Rem2.MoraDia_Percentual()"!',
          'Aviso', MB_OK+MB_ICONWARNING);
    end;
    else Geral.MensagemBox('Banco sem implementa��o na informa��o de juros na ' +
    'function "TFmCNAB_Rem2.MoraDia_Percentual()"!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

function TFmCNAB_Rem2.Multa_Valor_Percentual(Banco, CNAB, MultaTipo: Integer; ValorBruto,
  MultaPerc: Double): Double;
begin
  Result := 0;
  case Banco of
    001:
    begin
      if CNAB = 240 then
      begin
        case MultaTipo of
          // Banco n�o fala no manual sobre o c�digo zero, mas obriga a us�-lo se n�o se cobra multa
          0: Result := 0;
          1: Result := ValorBruto * MultaPerc / 100;
          2: Result := MultaPerc;
          else Geral.MensagemBox('Tipo de multa n�o implementado para o banco ' +
          '001 na function "TFmCNAB_Rem2.Multa_Valor_Percentual()"!' + sLineBreak +
          'Verifique se foi selecionado um tipo v�lido para cobran�a de multa '+
          'na configura��o CNAB correspondente!',
          'Aviso', MB_OK+MB_ICONWARNING);
        end;
      end else Geral.MensagemBox(PChar('Tipo de CNAB n�o implementado para o banco ' +
          '001 na function "TFmCNAB_Rem2.Multa_Valor_Percentual()"!'),
          'Aviso', MB_OK+MB_ICONWARNING);
    end;
    else Geral.MensagemBox('Banco sem implementa��o na informa��o de juros na ' +
    'function "TFmCNAB_Rem2.Multa_Valor_Percentual()"!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

function TFmCNAB_Rem2.MsgProc(Campo, CNAB, Registro: Integer; Segmento: Char): String;
var
  CNAB_TXT, Campo_TXT, Regi_TXT, Segm_TXT: String;
begin
  Campo_TXT := FormatFloat('000', Campo);
  CNAB_TXT  := FormatFloat('000', CNAB);
  Regi_TXT  := FormatFloat('0', Registro);
  if Segmento <> #0 then Segm_TXT := '_' + Segmento else Segm_TXT := '';
  Geral.MensagemBox(PChar('O campo ' + Campo_TXT +
  ' n�o est� habilitado na procedure "Registro_' + CNAB_TXT + '_' + Regi_TXT +
  Segm_TXT + '!'), 'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmCNAB_Rem2.MeComparEnter(Sender: TObject);
begin
  FEditor := MeCompar;
end;

procedure TFmCNAB_Rem2.MeComparExit(Sender: TObject);
begin
  FEditor := nil;
end;

function TFmCNAB_Rem2.SeqRegLote(Incremento: Integer): Integer;
begin
  case Incremento of
    0: FRegLote := 0;
    1: FRegLote := FRegLote + 1;
    else
    begin
      FRegLote := FRegLote + 1;
      Geral.MensagemBox('Incremento de sequ�ncia de lote inv�lido!' +sLineBreak+
      'AVISE A DERMATEK!', 'ERRO', MB_OK+MB_ICONERROR);
    end;
  end;
  Result := FRegLote;
end;

procedure TFmCNAB_Rem2.SpeedButton1Click(Sender: TObject);
begin
  if OpenDialog1.Execute then
  begin
    MeCompar.Lines.LoadFromFile(OpenDialog1.FileName);
    EdArqCompar.Text := OpenDialog1.FileName;
  end;
end;

procedure TFmCNAB_Rem2.GradeGDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  OldAlign: Integer;
  CorFundo, CorTexto: TColor;
begin
  if GradeG.Cells[ACol,ARow] <> GradeO.Cells[ACol,ARow] then
  begin
    CorFundo := clBlack;
    CorTexto := clWhite;
  end else begin
    CorFundo := clWhite;
    CorTexto := clBlack;
  end;
  GradeG.Canvas.Brush.Color := CorFundo;
  //FillRect(Rect);
  GradeG.Canvas.Font.Color := CorTexto;
  OldAlign := SetTextAlign(GradeG.Canvas.Handle, TA_LEFT);
  GradeG.Canvas.TextRect(Rect, Rect.Left, Rect.Top,
    GradeG.Cells[Acol, ARow]);
  SetTextAlign(GradeG.Canvas.Handle, OldAlign);
end;

procedure TFmCNAB_Rem2.GradeGSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  VerificaCelula(Acol, ARow);
end;

procedure TFmCNAB_Rem2.VerificaCelula(Col, Row: Integer);
var
  Ini, Fim, Campo: Integer;
  Pos, ValGera, ValComp: String;
begin
  Campo := Geral.IMV(GradeC.Cells[Col, Row]);
  Ini   := Geral.IMV(GradeI.Cells[Col, Row]);
  Fim   := Geral.IMV(GradeF.Cells[Col, Row]);
  Pos   := FormatFloat('000', Ini) + ' a ' +
           FormatFloat('000', Fim) + ' = ' +
           FormatFloat('000', Fim - Ini + 1);
  ValGera := Copy(MeGerado.Lines[Row], Ini, Fim - Ini + 1);
  ValComp := Copy(MeCompar.Lines[Row], Ini, Fim - Ini + 1);
  dmkEdVerifPos.Text                := Pos;
  dmkEdVerifCol.ValueVariant        := Col + 1;
  dmkEdVerifLin.ValueVariant        := Row + 1;
  dmkEdVerifCampoCod.ValueVariant   := Campo;
  dmkEdVerifCampoTxt.Text           := UBancos.DescricaoDoMeuCodigoCNAB(Campo);
  dmkEdVerifValGerado.Text          := Trim(ValGera);
  dmkEdVerifValCompar.Text          := Trim(ValComp);
end;

procedure TFmCNAB_Rem2.BitBtn1Click(Sender: TObject);
var
  i, n, Col, Lin, NewReg, OldReg, Ini, Fim, Este: Integer;
  Campo: String;
  Lista: MyArrayLista;
  CNAB, SubSegmento: Integer;
begin
  Screen.Cursor := crHourGlass;
  Ini := 0;
  Fim := 0;
  CNAB := DmBco.QrCNAB_CfgCNAB.Value;
  MyObjects.LimpaGrade(GradeG, 0, 0, True);
  MyObjects.LimpaGrade(GradeO, 0, 0, True);
  MyObjects.LimpaGrade(GradeC, 0, 0, True);
  //
  n := CNAB;
  if n <= 0 then
  begin
    Geral.MB_Erro('Quantidade de carateres n�o definida no arquivo!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  GradeG.ColCount := n; // Arquivo gerado
  GradeO.ColCount := n; // Arquivo a comparar
  GradeC.ColCount := n; // Campos
  GradeI.ColCount := n; // In�cio do campo
  GradeF.ColCount := n; // Fim do campo
  //
  if MeGerado.Lines.Count <> MeCompar.Lines.Count then
  begin
    Geral.MB_Aviso('Quantidade de linhas n�o conferem!');
  end;
  n := MeGerado.Lines.Count;
  if n < MeCompar.Lines.Count then n := MeCompar.Lines.Count;
  if n = 0 then begin Screen.Cursor := crDefault; Exit; end;
  GradeG.RowCount := n;
  GradeO.RowCount := n;
  GradeC.RowCount := n;
  GradeI.RowCount := n;
  GradeF.RowCount := n;
  OldReg := -1;
  PB1.Position := 0;
  PB1.Max := n;
  for Lin := 0 to n-1 do
  begin
    if MeGerado.Lines.Count <= Lin then
    begin
      // Evitar erro
      Screen.Cursor := crDefault;
      exit;
    end;
    PB1.Position := PB1.Position + 1;
    NewReg := Geral.IMV(MeGerado.Lines[Lin][1]);
    if NewReg <> OldReg then
    begin
      OldReg := NewReg;
      // Parei Aqui Fazer???
      SubSegmento := -1;
      CriaListaCNAB(CNAB, FBanco, NewReg, #0, Lista, SubSegmento);
    end;
    for Col := 0 to Length(MeGerado.Lines[Lin])-1 do
    begin
      Campo := '';
      for i := Low(Lista) to High(Lista) do
      begin
        Este := Col + 1;
        Ini := Geral.IMV(Lista[i][_Ini]);
        Fim := Geral.IMV(Lista[i][_Fim]);
        if (Este >= Ini) and (Este <= Fim) then
        begin
          Campo := Lista[i][_Fld];
          Break;
        end;
      end;
      GradeC.Cells[Col, Lin] := Campo;
      GradeI.Cells[Col, Lin] := FormatFloat('000', Ini);
      GradeF.Cells[Col, Lin] := FormatFloat('000', Fim);
      GradeO.Cells[Col, Lin] := MeCompar.Lines[Lin][Col + 1];
      GradeG.Cells[Col, Lin] := MeGerado.Lines[Lin][Col + 1];
    end;
  end;
  Screen.Cursor := crDefault;
end;

function TFmCNAB_Rem2.DAC_NossoNumero(const Bloqueto: Double; var Res: String): Boolean;
begin
  Result := UBancos.DACNossoNumero(DmBco.QrCNAB_CfgModalCobr.Value,
              DmBco.QrCNAB_CfgCedBanco.Value,
              DmBco.QrCNAB_CfgCedAgencia.Value,
              DmBco.QrCNAB_CfgCedPosto.Value, Bloqueto,
              DmBco.QrCNAB_CfgCedConta.Value,
              DmBco.QrCNAB_CfgCartCod.Value,
              DmBco.QrCNAB_CfgCartNum.Value,
              Geral.SoNumero_TT(DmBco.QrCNAB_CfgCodEmprBco.Value),
              DmBco.QrCNAB_CfgTipoCobranca.Value,
              DmBco.QrCNAB_CfgEspecieDoc.Value,
              DmBco.QrCNAB_CfgCNAB.Value,
              DmBco.QrCNAB_CfgCtaCooper.Value,
              DmBco.QrCNAB_CfgLayoutRem.Value,
              Res);
end;

function TFmCNAB_Rem2.DataZeroSeNenhumDia(Vencto: TDateTime;
  Dias, TipoData: Integer): String;
begin
  if Dias = 0 then
    //Txt := '00000000'
    Result := '0'
  else
    Result := Geral.FDT(Vencto - Dias, TipoData);
end;

function TFmCNAB_Rem2.DescontoAntecipado(Banco, CNAB, DescoCod, DescoDds: Integer;
DescoFat, ValorBruto: Double; Vencimento: TDateTime): String;
  procedure Mensagem(Banco, CNAB: Integer);
  begin
    Geral.MensagemBox(PChar('O banco ' + FormatFloat('000', Banco) +
    ' n�o est� implementado no CNAB' + FormatFloat('000', CNAB) +
    ' para c�lculo de desconto antecipado!'), 'Aviso', MB_OK+MB_ICONWARNING);
  end;
var
  Val: Double;
begin
  Result := '0,00';
  Val := 0;
  case Banco of
    001:
    begin
      case CNAB of
        240:
        begin
          case DescoCod of
            1: Val := ValorBruto * DescoFat / 100;
            2: Val := DescoFat;
            3: Val := ValorBruto * DescoFat / 30 / 100; // Dias corridos
            4: Val := ValorBruto * DescoFat / 30 / 100; // Dias �teis - Verificar dias �teis??
            5: Val := DescoFat;
          end;
          Result := FormatFloat('#,###,##0.00', Val);
        end
        else Mensagem(Banco, CNAB);
      end;
    end;
    else Mensagem(Banco, CNAB);
  end;
end;

function TFmCNAB_Rem2.ObtemNumeroDoArquivo(var Res: String): Boolean;
begin
  //Result := False;
  if FSeqArqRem = 0 then
  begin
    FSeqArqRem := DmBco.QrCNAB_CfgSeqArq.Value + 1;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE cnab_cfg SET SeqArq=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1');
    Dmod.QrUpd.Params[00].AsInteger := FSeqArqRem;
    Dmod.QrUpd.Params[01].AsInteger := DmBco.QrCNAB_CfgCodigo.Value;
    Dmod.QrUpd.ExecSQL;
  end;
  if CO_DMKID_APP in [4, 43] then //Syndi2, Syndi3
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE protocopak SET SeqArq=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Dmod.QrUpd.Params[00].AsInteger := FSeqArqRem;
    Dmod.QrUpd.Params[01].AsInteger := FLote;
    Dmod.QrUpd.ExecSQL;
  end;
  //
  Res := FormatFloat('0', FSeqArqRem);
  Result := True;
end;

function TFmCNAB_Rem2.ValorDeColunaDeGrade(const Grade: TStringGrid; const Lista:
MyArrayLista; const ItemLista: Integer; const FCols: TArrayCols;
const LinhaGrade: Integer; var Res: String): Boolean;
var
  CodiFld, NomeFld: String;
  Campo, Coluna, Carteira: Integer;
begin
  CodiFld := Lista[ItemLista][_Fld];
  Campo   := Geral.IMV(CodiFld);
  //if Campo = 400 then
    //ShowMessage('400');
  // Se o campo for DAC do "Nosso n�mero"  separado do seu DAC...
  if Campo = 511 then
  // obtem o valor do "Nosso n�mero"
    Coluna := FCols[501]
  //sen�o obtem o valor do campo normal
  else
    Coluna  := FCols[Campo];
  Res := '';
  //if Campo = 20 then
    //ShowMessage(Geral.FF0(Coluna));
  Result := True;
  case Campo of
     -02: Res := ' ';
     -01: Res := '0';
     000: Res := '';
     999: Res := NovaLinhaArq();
    else
    begin
      if Coluna > 0 then
      begin
        Res := Grade.Cells[Coluna, LinhaGrade];
        case Campo of
          // 400: Tipo de inscri��o cedente
          // 801: Tipo de inscricao sacado
          // 851: Tipo de inscricao sacador / avalista
          400, 801, 851: Result := UBco_Rem.TipoDeInscricao(Geral.IMV(Res),
            FBanco, GetCNAB(), Campo, '', Res);
          // Nosso N�mero
          //501: Result := UBco_Rem.AjustaNossoNumero(FBanco, );
          // Ocorr�ncia ( motivo de enviar o arquivo)
          504: Result := UBco_Rem.CodigoDeOcorrenciaRemessa(FBanco, 01, Res);
          // DAC do Nosso n�mero
          511: Result := DAC_NossoNumero(Geral.DMV(Res), Res);
          // Aceite do t�tulo
          520: Result := UBco_Rem.CodigoDeAceiteRemessa(FBanco, Geral.IMV(Res), '', Res);
          // Quem Imprime
          621: Result := UBco_Rem.QuemImprimeOBloqueto(FBanco, Geral.IMV(Res), '', Res);
          //
          649:
          begin
            if FBanco = 1 then
            begin
              Carteira := Geral.IMV(Grade.Cells[F1_509, LinhaGrade]);
              case Carteira of
                12:     Res := 'AIU';
                31..51: Res := 'SD';
                else    Res := 'AI'
              end;
            end;
          end;
        end;
        //Result := True;
      end else begin
        Result := False;
        case Campo of

          // CAMPOS N�O IMPEMENTADOS !!!!!

          // Ag�ncia cobradora
          025: Result := True;
          // Banco cobrador
          027: Result := True;
          // D�bito autom�tico no bradesco
          610..615,622,624: Result := True;
          // Rateio de cr�dito Bradesco
          623: Result := True;
          // Quantidade de moeda vari�vel
          749: Result := True;
          //C�digo de instru��o / alega��o (a ser cancelada)
          //   Parei aqui!!!! Fazer quando responder a  arquivos de retorno!!!!
          750: Result := True;
        end;
        if Result = False then
        begin
          // descobrir todos n�o implementados
          Result := True;
          //
          NomeFld := ' - "' + UBancos.DescricaoDoMeuCodigoCNAB(Campo) + '" ';
            Geral.MB_Erro('O c�digo de campo ' + CodiFld + NomeFld
              + ' n�o est� implementado! (Coluna ' + Geral.FF0(Coluna) + ')');
        end;
      end;
    end;
  end;
end;

function TFmCNAB_Rem2.ValorDeCampo(const Lista: MyArrayLista; Campo: Integer;
const Pre_Def: String; var Res: String): Boolean;
var
  Def, Fil, Typ, Formato, Texto: String;
  Len, Fld, i, Cas: Integer;
  Ajusta: Boolean;
  Data: TDate;
  Hora: TTime;
begin
  Result := False;
  Def := Trim(Lista[Campo][_Def]);
  Fil := Lista[Campo][_Fil];
  Len := Geral.IMV(Lista[Campo][_Tam]);
  Fld := Geral.IMV(Lista[Campo][_Fld]);
  Ajusta := Uppercase(Lista[Campo][_AjT]) = 'S';
  //if Fld = 504 then
    //ShowMessage('504');
  Typ := Lista[Campo][_Typ];
  if Def <> '' then Texto := Def else
  begin
    if Pre_Def <> '' then Texto := Pre_def else
    begin
      // Parei Aqui buscar dados de grades
      Texto := '';
    end;

    //  Formata Data
    if Typ = 'D' then
    begin
      Data    := Geral.ValidaDataSimples(Texto, True);
      if Data = 0 then
      begin
        Texto := '';
        while Length(Texto) < Len do
          Texto := Texto + Fil;
      end else begin
        Formato := Uppercase(Trim(Lista[Campo][_Fmt]));
        if Length(Formato) = 0 then
        begin
          Result := False;
          Geral.MB_Erro('Data sem formata��o para o campo "' +
            Lista[Campo][_Nom] + '" em TFmCNAB_Rem.BtOKClick().ValorDeCampo()"!');
          Exit;
        end;
        for i := 1 to Length(Formato) do
          if Formato[i] = 'A' then Formato[i] := 'Y';
        Texto := FormatDateTime(Formato, Data);
      end;
    end else
    //  Formata Hora
    if Typ = 'T' then
    begin
      if (Texto = '') or (Texto = '0') then
        Hora := 0
      else
        Hora := StrToTime(Texto);
      if Hora = 0 then
      begin
        Texto := '';
        while Length(Texto) < Len do
          Texto := Texto + Fil;
      end else begin
        Formato := Uppercase(Trim(Lista[Campo][_Fmt]));
        if Length(Formato) = 0 then
        begin
          Result := False;
          Geral.MB_Erro('Hora sem formata��o para o campo "' +
          Lista[Campo][_Nom] + '" em TFmCNAB_Rem.BtOKClick().ValorDeCampo()"!');
          Exit;
        end;
        Texto := FormatDateTime(Formato, Hora);
      end;
    end else
    //         Texto           Zeros          Brancos
    if (Typ = 'X') or  (Typ = 'Z') or (Typ = 'B') then
    begin
      Texto := Geral.SemAcento(Texto);
      Texto := Geral.Maiusculas(Texto, False);
    end else
    // Formata Inteiro
    if Typ = 'I' then
    begin
      Texto := Geral.SoNumero_TT(Texto);
    end else
    // Formata Inteiro
    if Typ = 'F' then
    begin
      Cas := Geral.IMV(Lista[Campo][_Fmt]);

      Texto := Trim(Texto);
      Texto := Geral.TFT(Texto, Cas, siPositivo);
      Texto := Geral.SoNumero_TT(Texto);
    end else
    begin
      Geral.MB_Erro('Tipo de dado n�o implementado: "' + Typ + '" em "CNAB_Rem.BtOKClick"!');
      Exit;
    end;
  end;

  {case Fld of
    402, 803, 853: Corrige := True;
    else Corrige := False;
  end;}
  //
  Result := AjustaString(Texto, Fil, Len, Ajusta, Res);
  if not Result then
    Geral.MB_Erro('O tamanho do texto "' + Res + '" ficou ' +
      'com tamanho = ' + Geral.FF0(Length(Res)) + ' que � diferente do ' +
      'estipulado, que � de ' + Geral.FF0(Len) + ' posi��es para o campo "' +
      FormatFloat('000', fld) + ' - ' + Trim(Lista[Campo][_Nom]) + '"!');
end;

procedure TFmCNAB_Rem2.AvisaCancelamento(Texto: String; TamCorreto, TamAtual: Integer);
begin
  MeGerado.Lines.Add(Texto);
  Geral.MB_Erro('Gera��o de arquivo remessa cancelado!' +
    sLineBreak + 'A linha ' + Geral.FF0(MeGerado.Lines.Count) + ' deveria ter ' +
    Geral.FF0(TamCorreto) + ' caracteres, mas est� com ' + Geral.FF0(TamAtual) +
    '!');
end;
procedure TFmCNAB_Rem2.AdicionaListaRow(LinhaArquivo, TipoRegistro: Integer;
Segmento: Char; Coluna, PosIni, PosFim, TamCNAB, TamReal, IDCampo: Integer;
Valor, NomeCampo, DescriCampo: String);
var
  Segmento_TXT: String;
begin
  if Segmento = #0 then
    Segmento_TXT := ''
  else
    Segmento_TXT := Segmento;
  //  
  FRowL := FRowL + 1;
  Grade_L.RowCount := FRowL + 1;
  Grade_L.Cells[00, FRowL] := FormatFloat('00' , LinhaArquivo);
  Grade_L.Cells[01, FRowL] := FormatFloat('0'  , TipoRegistro);
  Grade_L.Cells[02, FRowL] := Segmento_TXT;
  Grade_L.Cells[03, FRowL] := FormatFloat('00' , Coluna);
  Grade_L.Cells[04, FRowL] := FormatFloat('000', PosIni);
  Grade_L.Cells[05, FRowL] := FormatFloat('000', PosFim);
  Grade_L.Cells[06, FRowL] := FormatFloat('000', TamCNAB);
  Grade_L.Cells[07, FRowL] := FormatFloat('000', TamReal);
  Grade_L.Cells[08, FRowL] := FormatFloat('000', IDCampo);
  Grade_L.Cells[09, FRowL] := Valor;
  Grade_L.Cells[10, FRowL] := NomeCampo;
  Grade_L.Cells[11, FRowL] := DescriCampo;
end;

end.

