object FmCNAB_Rem2: TFmCNAB_Rem2
  Left = 377
  Top = 168
  Caption = 'Remessa CNAB'
  ClientHeight = 686
  ClientWidth = 1241
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelConfirma: TPanel
    Left = 0
    Top = 626
    Width = 1241
    Height = 60
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 25
      Top = 5
      Width = 110
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Gera'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 1104
      Top = 1
      Width = 136
      Height = 58
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtMover: TBitBtn
      Tag = 10070
      Left = 138
      Top = 5
      Width = 220
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&Mover para: Enviados'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtMoverClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Remessa CNAB'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1239
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
      ExplicitWidth = 1238
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 1241
    Height = 567
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 1239
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 0
      object Label23: TLabel
        Left = 5
        Top = 5
        Width = 138
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Configura'#231#227'o de envio:'
        FocusControl = DBEdit1
      end
      object Label25: TLabel
        Left = 591
        Top = 10
        Width = 60
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Posi'#231#245'es:'
        FocusControl = DBEdit3
      end
      object DBEdit1: TDBEdit
        Left = 5
        Top = 25
        Width = 50
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Codigo'
        DataSource = DmBco.DsCNAB_Cfg
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 59
        Top = 25
        Width = 374
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Nome'
        DataSource = DmBco.DsCNAB_Cfg
        TabOrder = 1
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 438
        Top = 1
        Width = 144
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' CNAB: '
        Columns = 2
        DataField = 'CNAB'
        DataSource = DmBco.DsCNAB_Cfg
        Items.Strings = (
          '240'
          '400')
        ParentBackground = True
        TabOrder = 2
        Values.Strings = (
          '240'
          '400')
      end
      object DBEdit3: TDBEdit
        Left = 591
        Top = 30
        Width = 60
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'PosicoesBB'
        DataSource = DmBco.DsCNAB_Cfg
        TabOrder = 3
      end
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 60
      Width = 1239
      Height = 506
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Arquivos'
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 1231
          Height = 475
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel10: TPanel
            Left = 0
            Top = 414
            Width = 1231
            Height = 61
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            TabOrder = 0
            object Label11: TLabel
              Left = 5
              Top = 5
              Width = 35
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Linha:'
            end
            object Label12: TLabel
              Left = 54
              Top = 5
              Width = 45
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Coluna:'
            end
            object Label13: TLabel
              Left = 103
              Top = 5
              Width = 34
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Tam.:'
            end
            object Label14: TLabel
              Left = 153
              Top = 5
              Width = 112
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Texto pesquisado:'
            end
            object Edit2: TEdit
              Left = 5
              Top = 25
              Width = 45
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 0
              Text = '0'
              OnChange = Edit2Change
            end
            object Edit3: TEdit
              Left = 54
              Top = 25
              Width = 46
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 1
              Text = '0'
              OnChange = Edit3Change
            end
            object Edit4: TEdit
              Left = 103
              Top = 25
              Width = 46
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 2
              Text = '0'
              OnChange = Edit4Change
            end
            object Edit6: TEdit
              Left = 153
              Top = 25
              Width = 803
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              ReadOnly = True
              TabOrder = 3
            end
          end
          object PageControl2: TPageControl
            Left = 0
            Top = 0
            Width = 1231
            Height = 414
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            ActivePage = TabSheet10
            Align = alClient
            TabOrder = 1
            OnChange = PageControl2Change
            object TabSheet10: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Gerado'
              object Panel11: TPanel
                Left = 0
                Top = 0
                Width = 1223
                Height = 383
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Panel8: TPanel
                  Left = 0
                  Top = 0
                  Width = 1223
                  Height = 36
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alTop
                  TabOrder = 0
                  object Label15: TLabel
                    Left = 10
                    Top = 10
                    Width = 49
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Arquivo:'
                  end
                  object EdArqGerado: TEdit
                    Left = 64
                    Top = 5
                    Width = 882
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    ReadOnly = True
                    TabOrder = 0
                  end
                end
                object MeGerado: TMemo
                  Left = 0
                  Top = 36
                  Width = 1223
                  Height = 321
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -15
                  Font.Name = 'Courier New'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 1
                  WordWrap = False
                  OnEnter = MeGeradoEnter
                  OnExit = MeGeradoExit
                end
                object dmkMBGravado: TdmkMemoBar
                  Left = 0
                  Top = 357
                  Width = 1223
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alBottom
                  BevelOuter = bvNone
                  TabOrder = 2
                  Memo = MeGerado
                  OverwriteCaretWidth = 12
                  OverwriteCaretHeight = 16
                end
              end
            end
            object TabSheet11: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Arquivo a ser comparado'
              ImageIndex = 1
              object Panel7: TPanel
                Left = 0
                Top = 0
                Width = 1223
                Height = 383
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Panel12: TPanel
                  Left = 0
                  Top = 0
                  Width = 1223
                  Height = 36
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alTop
                  TabOrder = 0
                  object Label28: TLabel
                    Left = 10
                    Top = 10
                    Width = 49
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Arquivo:'
                  end
                  object SpeedButton1: TSpeedButton
                    Left = 921
                    Top = 5
                    Width = 25
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = '...'
                    OnClick = SpeedButton1Click
                  end
                  object EdArqCompar: TEdit
                    Left = 64
                    Top = 5
                    Width = 853
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    ReadOnly = True
                    TabOrder = 0
                  end
                end
                object MeCompar: TMemo
                  Left = 0
                  Top = 36
                  Width = 1223
                  Height = 321
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -15
                  Font.Name = 'Courier New'
                  Font.Style = []
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 1
                  WordWrap = False
                  OnEnter = MeComparEnter
                  OnExit = MeComparExit
                end
                object dmkMBCompar: TdmkMemoBar
                  Left = 0
                  Top = 357
                  Width = 1223
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alBottom
                  BevelOuter = bvNone
                  TabOrder = 2
                  Memo = MeCompar
                  OverwriteCaretWidth = 12
                  OverwriteCaretHeight = 16
                end
              end
            end
            object TabSheet7: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Compara'#231#227'o'
              ImageIndex = 2
              object Panel13: TPanel
                Left = 0
                Top = 0
                Width = 1223
                Height = 383
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object GradeG: TStringGrid
                  Left = 0
                  Top = 106
                  Width = 1223
                  Height = 203
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  ColCount = 240
                  DefaultColWidth = 7
                  DefaultRowHeight = 13
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -15
                  Font.Name = 'Courier New'
                  Font.Style = []
                  Options = [goFixedVertLine, goFixedHorzLine, goEditing]
                  ParentFont = False
                  TabOrder = 0
                  OnDrawCell = GradeGDrawCell
                  OnSelectCell = GradeGSelectCell
                end
                object Panel17: TPanel
                  Left = 0
                  Top = 0
                  Width = 1223
                  Height = 85
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Panel18: TPanel
                    Left = 0
                    Top = 0
                    Width = 124
                    Height = 85
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alLeft
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Label22: TLabel
                      Left = 0
                      Top = 59
                      Width = 120
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Endere'#231'o, tamanho:'
                    end
                    object BitBtn1: TBitBtn
                      Left = 0
                      Top = 1
                      Width = 111
                      Height = 49
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = '&Compara'
                      NumGlyphs = 2
                      TabOrder = 0
                      OnClick = BitBtn1Click
                    end
                  end
                  object Panel19: TPanel
                    Left = 124
                    Top = 0
                    Width = 1099
                    Height = 85
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 1
                    object Label16: TLabel
                      Left = 0
                      Top = 5
                      Width = 35
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Linha:'
                    end
                    object Label17: TLabel
                      Left = 0
                      Top = 32
                      Width = 45
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Coluna:'
                    end
                    object Label18: TLabel
                      Left = 108
                      Top = 59
                      Width = 47
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Campo:'
                    end
                    object Label19: TLabel
                      Left = 108
                      Top = 32
                      Width = 108
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Valor comparado:'
                    end
                    object Label20: TLabel
                      Left = 108
                      Top = 5
                      Width = 82
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Valor gerado:'
                    end
                    object dmkEdVerifLin: TdmkEdit
                      Left = 49
                      Top = 0
                      Width = 54
                      Height = 26
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Alignment = taRightJustify
                      ReadOnly = True
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '000'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object dmkEdVerifCol: TdmkEdit
                      Left = 49
                      Top = 27
                      Width = 54
                      Height = 26
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Alignment = taRightJustify
                      ReadOnly = True
                      TabOrder = 1
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '000'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object dmkEdVerifValCompar: TdmkEdit
                      Left = 212
                      Top = 27
                      Width = 2363
                      Height = 26
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      ReadOnly = True
                      TabOrder = 2
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object dmkEdVerifCampoCod: TdmkEdit
                      Left = 158
                      Top = 54
                      Width = 54
                      Height = 26
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Alignment = taRightJustify
                      ReadOnly = True
                      TabOrder = 3
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-1000'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '000'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object dmkEdVerifValGerado: TdmkEdit
                      Left = 212
                      Top = 0
                      Width = 2363
                      Height = 26
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      ReadOnly = True
                      TabOrder = 4
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object dmkEdVerifCampoTxt: TdmkEdit
                      Left = 212
                      Top = 54
                      Width = 2363
                      Height = 26
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      ReadOnly = True
                      TabOrder = 5
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object dmkEdVerifPos: TdmkEdit
                      Left = 0
                      Top = 54
                      Width = 103
                      Height = 26
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      ReadOnly = True
                      TabOrder = 6
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '000 a 000 = 000'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = '000 a 000 = 000'
                      ValWarn = False
                    end
                  end
                end
                object GradeO: TStringGrid
                  Left = 6
                  Top = 167
                  Width = 133
                  Height = 80
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  ColCount = 240
                  DefaultColWidth = 7
                  DefaultRowHeight = 13
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -15
                  Font.Name = 'Courier New'
                  Font.Style = []
                  Options = [goFixedVertLine, goFixedHorzLine, goEditing]
                  ParentFont = False
                  TabOrder = 2
                  Visible = False
                end
                object GradeC: TStringGrid
                  Left = 0
                  Top = 358
                  Width = 1223
                  Height = 25
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alBottom
                  ColCount = 240
                  DefaultColWidth = 24
                  DefaultRowHeight = 13
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -15
                  Font.Name = 'Courier New'
                  Font.Style = []
                  Options = [goFixedVertLine, goFixedHorzLine, goEditing]
                  ParentFont = False
                  ScrollBars = ssNone
                  TabOrder = 3
                  Visible = False
                end
                object GradeI: TStringGrid
                  Left = 0
                  Top = 309
                  Width = 1223
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alBottom
                  ColCount = 240
                  DefaultColWidth = 24
                  DefaultRowHeight = 13
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -15
                  Font.Name = 'Courier New'
                  Font.Style = []
                  Options = [goFixedVertLine, goFixedHorzLine, goEditing]
                  ParentFont = False
                  ScrollBars = ssNone
                  TabOrder = 4
                  Visible = False
                end
                object GradeF: TStringGrid
                  Left = 0
                  Top = 333
                  Width = 1223
                  Height = 25
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alBottom
                  ColCount = 240
                  DefaultColWidth = 24
                  DefaultRowHeight = 13
                  FixedCols = 0
                  RowCount = 2
                  FixedRows = 0
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -15
                  Font.Name = 'Courier New'
                  Font.Style = []
                  Options = [goFixedVertLine, goFixedHorzLine, goEditing]
                  ParentFont = False
                  ScrollBars = ssNone
                  TabOrder = 5
                  Visible = False
                end
                object PB1: TProgressBar
                  Left = 0
                  Top = 85
                  Width = 1223
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alTop
                  TabOrder = 6
                end
              end
            end
          end
        end
      end
      object TabSheet4: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '  CNAB 240  '
        ImageIndex = 3
        object PageControl4: TPageControl
          Left = 0
          Top = 0
          Width = 1231
          Height = 475
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet9
          Align = alClient
          TabOrder = 0
          object TabSheet9: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' [0] Header de arquivo '
            object Grade_240_0: TStringGrid
              Left = 0
              Top = 0
              Width = 1223
              Height = 444
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              ColCount = 2
              DefaultColWidth = 32
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
              TabOrder = 0
              OnSelectCell = Grade_1SelectCell
            end
          end
          object TabSheet2: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '[1] Header de lote '
            ImageIndex = 1
            object Grade_240_1: TStringGrid
              Left = 0
              Top = 0
              Width = 1223
              Height = 444
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              ColCount = 2
              DefaultColWidth = 32
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
              TabOrder = 0
              OnSelectCell = Grade_1SelectCell
            end
          end
          object TabSheet17: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '[3] Detalhes '
            ImageIndex = 2
            object PageControl5: TPageControl
              Left = 0
              Top = 0
              Width = 1223
              Height = 444
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              ActivePage = TabSheet18
              Align = alClient
              TabOrder = 0
              object TabSheet18: TTabSheet
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' [P] '
                object Grade_240_3_P: TStringGrid
                  Left = 0
                  Top = 0
                  Width = 1215
                  Height = 413
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  ColCount = 2
                  DefaultColWidth = 32
                  DefaultRowHeight = 18
                  FixedCols = 0
                  RowCount = 2
                  Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
                  TabOrder = 0
                  OnSelectCell = Grade_1SelectCell
                end
              end
              object TabSheet19: TTabSheet
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' [Q] '
                ImageIndex = 1
                object Grade_240_3_Q: TStringGrid
                  Left = 0
                  Top = 0
                  Width = 1215
                  Height = 413
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  ColCount = 2
                  DefaultColWidth = 32
                  DefaultRowHeight = 18
                  FixedCols = 0
                  RowCount = 2
                  Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
                  TabOrder = 0
                  OnSelectCell = Grade_1SelectCell
                end
              end
              object TabSheet20: TTabSheet
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' [R] '
                ImageIndex = 2
                object Grade_240_3_R: TStringGrid
                  Left = 0
                  Top = 0
                  Width = 1215
                  Height = 413
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  ColCount = 2
                  DefaultColWidth = 32
                  DefaultRowHeight = 18
                  FixedCols = 0
                  RowCount = 2
                  Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
                  TabOrder = 0
                  OnSelectCell = Grade_1SelectCell
                end
              end
            end
          end
          object TabSheet21: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '[5] Trailer de lote'
            ImageIndex = 3
            object Grade_240_5: TStringGrid
              Left = 0
              Top = 0
              Width = 1223
              Height = 444
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              ColCount = 2
              DefaultColWidth = 32
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
              TabOrder = 0
              OnSelectCell = Grade_1SelectCell
            end
          end
          object TabSheet22: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' [9] Trailer do arquivo '
            ImageIndex = 4
            object Grade_240_9: TStringGrid
              Left = 0
              Top = 0
              Width = 1223
              Height = 444
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              ColCount = 2
              DefaultColWidth = 32
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
              TabOrder = 0
              OnSelectCell = Grade_1SelectCell
            end
          end
        end
      end
      object TabSheet13: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' CNAB 400 '
        ImageIndex = 9
        object PageControl3: TPageControl
          Left = 0
          Top = 0
          Width = 1231
          Height = 475
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet16
          Align = alClient
          TabOrder = 0
          object TabSheet16: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' [0] Header '
            ImageIndex = 6
            object Panel4: TPanel
              Left = 0
              Top = 0
              Width = 1223
              Height = 444
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object GroupBox1: TGroupBox
                Left = 0
                Top = 0
                Width = 1223
                Height = 129
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Caption = ' Dados do registro'
                TabOrder = 0
                object Label1: TLabel
                  Left = 10
                  Top = 20
                  Width = 35
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Linha:'
                end
                object Label2: TLabel
                  Left = 340
                  Top = 20
                  Width = 54
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Registro:'
                end
                object Label3: TLabel
                  Left = 59
                  Top = 20
                  Width = 49
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Servi'#231'o:'
                end
                object Label7: TLabel
                  Left = 369
                  Top = 69
                  Width = 58
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Empresa:'
                end
                object Label9: TLabel
                  Left = 497
                  Top = 20
                  Width = 209
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Identifica'#231#227'o do cedente no banco:'
                end
                object Label24: TLabel
                  Left = 1034
                  Top = 20
                  Width = 72
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Dt Gera'#231#227'o:'
                end
                object Label26: TLabel
                  Left = 1122
                  Top = 20
                  Width = 93
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Conv'#234'nio Lider:'
                end
                object Label27: TLabel
                  Left = 960
                  Top = 20
                  Width = 66
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'h Gera'#231#227'o:'
                end
                object Label29: TLabel
                  Left = 10
                  Top = 69
                  Width = 179
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Tipo e n'#250'mero de documento:'
                end
                object dmkEd_0_000: TdmkEdit
                  Left = 10
                  Top = 39
                  Width = 45
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 4
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0001'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 1
                  ValWarn = False
                end
                object dmkEd_0_005: TdmkEdit
                  Left = 340
                  Top = 39
                  Width = 26
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '1'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 1
                  ValWarn = False
                end
                object dmkEd_0_006: TdmkEdit
                  Left = 369
                  Top = 39
                  Width = 123
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 4
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = 'REMESSA'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 'REMESSA'
                  ValWarn = False
                end
                object dmkEd_0_003: TdmkEdit
                  Left = 59
                  Top = 39
                  Width = 26
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 2
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '01'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 1
                  ValWarn = False
                end
                object dmkEd_0_004: TdmkEdit
                  Left = 89
                  Top = 39
                  Width = 246
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = 'COBRANCA'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 'COBRANCA'
                  ValWarn = False
                end
                object dmkEd_0_402: TdmkEdit
                  Left = 369
                  Top = 89
                  Width = 853
                  Height = 25
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 11
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object dmkEd_0_990: TdmkEdit
                  Left = 1034
                  Top = 39
                  Width = 84
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 7
                  FormatType = dmktfDate
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0d
                  ValWarn = False
                end
                object dmkEd_0_410: TdmkEdit
                  Left = 497
                  Top = 39
                  Width = 459
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 5
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object dmkEd_0_641: TdmkEdit
                  Left = 1122
                  Top = 39
                  Width = 99
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 8
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object dmkEd_0_991: TdmkEdit
                  Left = 960
                  Top = 39
                  Width = 69
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 6
                  FormatType = dmktfTime
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfLong
                  Texto = '00:00:00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object dmkEd_0_400: TdmkEdit
                  Left = 10
                  Top = 89
                  Width = 24
                  Height = 25
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 9
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object dmkEd_0_401: TdmkEdit
                  Left = 39
                  Top = 89
                  Width = 325
                  Height = 25
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 10
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
              object GroupBox2: TGroupBox
                Left = 0
                Top = 129
                Width = 1223
                Height = 74
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Caption = ' Dados banc'#225'rios: '
                TabOrder = 1
                object Label4: TLabel
                  Left = 10
                  Top = 20
                  Width = 53
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Ag'#234'ncia:'
                end
                object Label5: TLabel
                  Left = 98
                  Top = 20
                  Width = 90
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Conta corrente:'
                end
                object Label6: TLabel
                  Left = 231
                  Top = 20
                  Width = 31
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'DAC:'
                end
                object Label8: TLabel
                  Left = 281
                  Top = 20
                  Width = 118
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Institui'#231#227'o banc'#225'ria:'
                end
                object Label21: TLabel
                  Left = 1054
                  Top = 20
                  Width = 146
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'C'#243'digo oculto do banco:'
                end
                object dmkEd_0_020: TdmkEdit
                  Left = 10
                  Top = 39
                  Width = 45
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 4
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object dmkEd_0_021: TdmkEdit
                  Left = 98
                  Top = 39
                  Width = 90
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object dmkEd_0_022: TdmkEdit
                  Left = 59
                  Top = 39
                  Width = 21
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  CharCase = ecUpperCase
                  MaxLength = 1
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = True
                  ValueVariant = ''
                  ValWarn = False
                end
                object dmkEd_0_023: TdmkEdit
                  Left = 192
                  Top = 39
                  Width = 21
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  CharCase = ecUpperCase
                  MaxLength = 1
                  TabOrder = 3
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = True
                  ValueVariant = ''
                  ValWarn = False
                end
                object dmkEd_0_024: TdmkEdit
                  Left = 231
                  Top = 39
                  Width = 31
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  CharCase = ecUpperCase
                  MaxLength = 1
                  TabOrder = 4
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = True
                  ValueVariant = ''
                  ValWarn = False
                end
                object dmkEdNomeBanco: TdmkEdit
                  Left = 320
                  Top = 39
                  Width = 730
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 6
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object dmkEd_0_001: TdmkEdit
                  Left = 281
                  Top = 39
                  Width = 35
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 5
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 3
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnChange = dmkEd_0_001Change
                end
                object dmkEd_0_699: TdmkEdit
                  Left = 1054
                  Top = 39
                  Width = 168
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 7
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
            end
          end
          object TabSheet14: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' [1] Detalhe '
            object Panel9: TPanel
              Left = 0
              Top = 0
              Width = 1223
              Height = 444
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Grade_1: TStringGrid
                Left = 0
                Top = 0
                Width = 1223
                Height = 407
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                ColCount = 2
                DefaultColWidth = 32
                DefaultRowHeight = 18
                RowCount = 2
                Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
                TabOrder = 0
                OnSelectCell = Grade_1SelectCell
              end
              object Panel5: TPanel
                Left = 0
                Top = 407
                Width = 1223
                Height = 37
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alBottom
                ParentBackground = False
                TabOrder = 1
                object Label10: TLabel
                  Left = 10
                  Top = 10
                  Width = 234
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'C'#243'digo de item da coluna selecionada:'
                end
                object LaCodDmk_1: TLabel
                  Left = 241
                  Top = 10
                  Width = 25
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = '000'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -15
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
              end
            end
          end
          object TabSheet3: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' [2] Mensagem '
            ImageIndex = 1
            object Panel14: TPanel
              Left = 0
              Top = 0
              Width = 1223
              Height = 444
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Caption = 'Sem implementa'#231#227'o'
              ParentBackground = False
              TabOrder = 0
            end
          end
          object TabSheet15: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '[3] Rateio de Cr'#233'dito '
            ImageIndex = 2
            object Panel15: TPanel
              Left = 0
              Top = 0
              Width = 1223
              Height = 444
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Caption = 'Sem implementa'#231#227'o'
              ParentBackground = False
              TabOrder = 0
            end
          end
          object TabSheet5: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' [5] Detalhe '
            ImageIndex = 3
            object Grade_5: TStringGrid
              Left = 0
              Top = 0
              Width = 1223
              Height = 444
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              ColCount = 2
              DefaultColWidth = 32
              DefaultRowHeight = 18
              RowCount = 2
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
              TabOrder = 0
            end
          end
          object TabSheet6: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' [7] Detalhe '
            ImageIndex = 4
            object Grade_7: TStringGrid
              Left = 0
              Top = 0
              Width = 1223
              Height = 444
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              ColCount = 2
              DefaultColWidth = 32
              DefaultRowHeight = 18
              RowCount = 2
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
              TabOrder = 0
              OnSelectCell = Grade_1SelectCell
            end
          end
          object TabSheet8: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' [9] Trailer '
            ImageIndex = 5
            object Panel16: TPanel
              Left = 0
              Top = 0
              Width = 1223
              Height = 444
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              ParentBackground = False
              TabOrder = 0
            end
          end
        end
      end
      object TabSheet12: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Listar campos'
        ImageIndex = 8
        object Panel20: TPanel
          Left = 0
          Top = 0
          Width = 1231
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          object BtListar: TBitBtn
            Tag = 14
            Left = 5
            Top = 5
            Width = 111
            Height = 49
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Lista'
            NumGlyphs = 2
            TabOrder = 0
            Visible = False
            OnClick = BtListarClick
          end
        end
        object Grade_L: TStringGrid
          Left = 0
          Top = 59
          Width = 1231
          Height = 416
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          ColCount = 12
          DefaultColWidth = 32
          DefaultRowHeight = 18
          FixedCols = 4
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
          TabOrder = 1
        end
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 9
    Top = 11
  end
  object QrArqui: TmySQLQuery
    Database = Dmod.MyDB
    Left = 37
    Top = 11
    object QrArquiNUM_REM: TIntegerField
      FieldName = 'NUM_REM'
    end
    object QrArquiHoraG: TTimeField
      FieldName = 'HoraG'
      Required = True
    end
    object QrArquiDataG: TDateField
      FieldName = 'DataG'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrArquiMensagem1: TWideStringField
      FieldName = 'Mensagem1'
      Size = 40
    end
    object QrArquiMensagem2: TWideStringField
      FieldName = 'Mensagem2'
      Size = 40
    end
  end
  object QrLotes: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrLotesAfterScroll
    SQL.Strings = (
      'SELECT con.Nome NOMECONFIG, cob.Codigo NUM_LOT,'
      'cob.*, con.*'
      'FROM cnab_lot cob'
      'LEFT JOIN cnab_cfg con ON con.Codigo=cob.CNAB_Cfg')
    Left = 65
    Top = 11
    object QrLotesCedBanco: TIntegerField
      FieldName = 'CedBanco'
      Origin = 'cnab_cfg.CedBanco'
    end
    object QrLotesCedAgencia: TIntegerField
      FieldName = 'CedAgencia'
      Origin = 'cnab_cfg.CedAgencia'
    end
    object QrLotesCedConta: TWideStringField
      FieldName = 'CedConta'
      Origin = 'cnab_cfg.CedConta'
      Size = 30
    end
    object QrLotesCedDAC_A: TWideStringField
      FieldName = 'CedDAC_A'
      Origin = 'cnab_cfg.CedDAC_A'
      Size = 1
    end
    object QrLotesCedDAC_C: TWideStringField
      FieldName = 'CedDAC_C'
      Origin = 'cnab_cfg.CedDAC_C'
      Size = 1
    end
    object QrLotesCedDAC_AC: TWideStringField
      FieldName = 'CedDAC_AC'
      Origin = 'cnab_cfg.CedDAC_AC'
      Size = 1
    end
    object QrLotesComando: TIntegerField
      FieldName = 'Comando'
      Origin = 'cnab_cfg.Comando'
    end
    object QrLotesQuemDistrb: TWideStringField
      FieldName = 'QuemDistrb'
      Origin = 'cnab_cfg.QuemDistrb'
      Size = 1
    end
    object QrLotesTipoCart: TSmallintField
      FieldName = 'TipoCart'
      Origin = 'cnab_cfg.TipoCart'
    end
    object QrLotesEspecieTit: TWideStringField
      FieldName = 'EspecieTit'
      Origin = 'cnab_cfg.EspecieTit'
      Size = 6
    end
    object QrLotesQuemPrint: TWideStringField
      FieldName = 'QuemPrint'
      Origin = 'cnab_cfg.QuemPrint'
      Size = 1
    end
    object QrLotesAceiteTit: TSmallintField
      FieldName = 'AceiteTit'
      Origin = 'cnab_cfg.AceiteTit'
    end
    object QrLotesJurosTipo: TSmallintField
      FieldName = 'JurosTipo'
      Origin = 'cnab_cfg.JurosTipo'
    end
    object QrLotesJurosDias: TSmallintField
      FieldName = 'JurosDias'
      Origin = 'cnab_cfg.JurosDias'
    end
    object QrLotesJurosPerc: TFloatField
      FieldName = 'JurosPerc'
      Origin = 'cnab_cfg.JurosPerc'
    end
    object QrLotesMultaPerc: TFloatField
      FieldName = 'MultaPerc'
      Origin = 'cnab_cfg.MultaPerc'
    end
    object QrLotesMoedaCod: TWideStringField
      FieldName = 'MoedaCod'
      Origin = 'cnab_cfg.MoedaCod'
    end
    object QrLotesDesco1Cod: TSmallintField
      FieldName = 'Desco1Cod'
      Origin = 'cnab_cfg.Desco1Cod'
    end
    object QrLotesDesco1Fat: TFloatField
      FieldName = 'Desco1Fat'
      Origin = 'cnab_cfg.Desco1Fat'
    end
    object QrLotesDesco1Dds: TIntegerField
      FieldName = 'Desco1Dds'
      Origin = 'cnab_cfg.Desco1Dds'
    end
    object QrLotesDesco2Cod: TSmallintField
      FieldName = 'Desco2Cod'
      Origin = 'cnab_cfg.Desco2Cod'
    end
    object QrLotesDesco2Fat: TFloatField
      FieldName = 'Desco2Fat'
      Origin = 'cnab_cfg.Desco2Fat'
    end
    object QrLotesDesco2Dds: TIntegerField
      FieldName = 'Desco2Dds'
      Origin = 'cnab_cfg.Desco2Dds'
    end
    object QrLotesDesco3Cod: TSmallintField
      FieldName = 'Desco3Cod'
      Origin = 'cnab_cfg.Desco3Cod'
    end
    object QrLotesDesco3Fat: TFloatField
      FieldName = 'Desco3Fat'
      Origin = 'cnab_cfg.Desco3Fat'
    end
    object QrLotesDesco3Dds: TIntegerField
      FieldName = 'Desco3Dds'
      Origin = 'cnab_cfg.Desco3Dds'
    end
    object QrLotesProtesCod: TSmallintField
      FieldName = 'ProtesCod'
      Origin = 'cnab_cfg.ProtesCod'
    end
    object QrLotesProtesDds: TSmallintField
      FieldName = 'ProtesDds'
      Origin = 'cnab_cfg.ProtesDds'
    end
    object QrLotesBxaDevCod: TSmallintField
      FieldName = 'BxaDevCod'
      Origin = 'cnab_cfg.BxaDevCod'
    end
    object QrLotesBxaDevDds: TIntegerField
      FieldName = 'BxaDevDds'
      Origin = 'cnab_cfg.BxaDevDds'
    end
    object QrLotesContrato: TFloatField
      FieldName = 'Contrato'
      Origin = 'cnab_cfg.Contrato'
    end
    object QrLotesMultaTipo: TSmallintField
      FieldName = 'MultaTipo'
      Origin = 'cnab_cfg.MultaTipo'
    end
    object QrLotesMultaDias: TSmallintField
      FieldName = 'MultaDias'
      Origin = 'cnab_cfg.MultaDias'
    end
    object QrLotesConvCartCobr: TWideStringField
      FieldName = 'ConvCartCobr'
      Origin = 'cnab_cfg.ConvCartCobr'
      Size = 2
    end
    object QrLotesConvVariCart: TWideStringField
      FieldName = 'ConvVariCart'
      Origin = 'cnab_cfg.ConvVariCart'
      Size = 3
    end
    object QrLotesNUM_LOT: TIntegerField
      FieldName = 'NUM_LOT'
      Origin = 'cnab_lot.Codigo'
    end
    object QrLotesCedPosto: TIntegerField
      FieldName = 'CedPosto'
      Origin = 'cnab_cfg.CedPosto'
    end
    object QrLotesCartNum: TWideStringField
      FieldName = 'CartNum'
      Origin = 'cnab_cfg.CartNum'
      Size = 3
    end
    object QrLotesCodEmprBco: TWideStringField
      FieldName = 'CodEmprBco'
      Origin = 'cnab_cfg.CodEmprBco'
    end
    object QrLotesVariacao: TIntegerField
      FieldName = 'Variacao'
    end
    object QrLotesCartCod: TWideStringField
      FieldName = 'CartCod'
      Size = 3
    end
    object QrLotes_2401_208: TWideStringField
      FieldName = '_2401_208'
      Size = 33
    end
  end
  object QrItens: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrItensCalcFields
    SQL.Strings = (
      'SELECT loi.Duplicata, loi.Emissao, loi.Vencto, '
      'loi.Bruto, loi.Desco Abatimento, '
      'loi.Controle + 0.000000000000000000 SeuNumero, '
      'loi.Controle + 0.000000000000000000 Sequencial, '
      'ent.Tipo AVALISTA_TIPO,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END AVAL' +
        'ISTA_NOME, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.CNPJ    ELSE ent.CPF     END AVALI' +
        'STA_CNPJ, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.ERua    ELSE ent.PRua    END AVALI' +
        'STA_RUA, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.ENumero+0.000 ELSE ent.PNumero+0.0' +
        '00 END AVALISTA_NUMERO,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.ECompl  ELSE ent.PCompl  END AVALI' +
        'STA_COMPL,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.EBairro ELSE ent.PBairro END AVALI' +
        'STA_BAIRRO, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.ECEP    ELSE ent.PCEP    END AVALI' +
        'STA_CEP, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.ECidade ELSE ent.PCidade END AVALI' +
        'STA_CIDADE, '
      
        'CASE WHEN ent.Tipo=0 THEN ufe.Nome    ELSE ufp.Nome    END AVALI' +
        'STA_xUF, '
      'sac.CNPJ SACADO_CNPJ, sac.Nome SACADO_NOME, sac.Rua SACADO_RUA,'
      'sac.Numero SACADO_NUMERO, sac.Compl SACADO_COMPL, '
      'sac.Bairro SACADO_BAIRRO,'
      'sac.Cidade SACADO_CIDADE, sac.UF SACADO_xUF, '
      'sac.CEP SACADO_CEP, IF(LENGTH(sac.CNPJ) >=14, 0, 1) SACADO_TIPO'
      'FROM lotesits loi '
      'LEFT JOIN lotes     lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente'
      'LEFT JOIN sacados   sac ON sac.CNPJ=loi.CPF '
      'LEFT JOIN ufs       ufe ON ent.EUF=ufe.Codigo '
      'LEFT JOIN ufs       ufp ON ent.PUF=ufp.Codigo '
      'WHERE loi.CNAB_Lot=1'
      'ORDER BY Duplicata'
      '')
    Left = 93
    Top = 11
    object QrItensDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrItensEmissao: TDateField
      FieldName = 'Emissao'
    end
    object QrItensVencto: TDateField
      FieldName = 'Vencto'
    end
    object QrItensBruto: TFloatField
      FieldName = 'Bruto'
    end
    object QrItensAbatimento: TFloatField
      FieldName = 'Abatimento'
    end
    object QrItensSeuNumero: TFloatField
      FieldName = 'SeuNumero'
      Required = True
    end
    object QrItensSequencial: TFloatField
      FieldName = 'Sequencial'
      Required = True
    end
    object QrItensAVALISTA_TIPO: TSmallintField
      FieldName = 'AVALISTA_TIPO'
    end
    object QrItensAVALISTA_NOME: TWideStringField
      FieldName = 'AVALISTA_NOME'
      Size = 100
    end
    object QrItensAVALISTA_CNPJ: TWideStringField
      FieldName = 'AVALISTA_CNPJ'
      Size = 18
    end
    object QrItensAVALISTA_RUA: TWideStringField
      FieldName = 'AVALISTA_RUA'
      Size = 30
    end
    object QrItensAVALISTA_NUMERO: TFloatField
      FieldName = 'AVALISTA_NUMERO'
    end
    object QrItensAVALISTA_COMPL: TWideStringField
      FieldName = 'AVALISTA_COMPL'
      Size = 30
    end
    object QrItensAVALISTA_BAIRRO: TWideStringField
      FieldName = 'AVALISTA_BAIRRO'
      Size = 30
    end
    object QrItensAVALISTA_CIDADE: TWideStringField
      FieldName = 'AVALISTA_CIDADE'
      Size = 25
    end
    object QrItensAVALISTA_xUF: TWideStringField
      FieldName = 'AVALISTA_xUF'
      Size = 2
    end
    object QrItensSACADO_CNPJ: TWideStringField
      FieldName = 'SACADO_CNPJ'
      Required = True
      Size = 15
    end
    object QrItensSACADO_NOME: TWideStringField
      FieldName = 'SACADO_NOME'
      Size = 50
    end
    object QrItensSACADO_RUA: TWideStringField
      FieldName = 'SACADO_RUA'
      Size = 30
    end
    object QrItensSACADO_NUMERO: TLargeintField
      FieldName = 'SACADO_NUMERO'
    end
    object QrItensSACADO_COMPL: TWideStringField
      FieldName = 'SACADO_COMPL'
      Size = 30
    end
    object QrItensSACADO_BAIRRO: TWideStringField
      FieldName = 'SACADO_BAIRRO'
      Size = 30
    end
    object QrItensSACADO_CIDADE: TWideStringField
      FieldName = 'SACADO_CIDADE'
      Size = 25
    end
    object QrItensSACADO_xUF: TWideStringField
      FieldName = 'SACADO_xUF'
      Size = 2
    end
    object QrItensSACADO_CEP: TIntegerField
      FieldName = 'SACADO_CEP'
    end
    object QrItensSACADO_TIPO: TLargeintField
      FieldName = 'SACADO_TIPO'
      Required = True
    end
    object QrItensENDERECO_SACADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENDERECO_SACADO'
      Size = 255
      Calculated = True
    end
    object QrItensENDERECO_AVALISTA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENDERECO_AVALISTA'
      Size = 255
      Calculated = True
    end
    object QrItensAVALISTA_CEP: TFloatField
      FieldName = 'AVALISTA_CEP'
    end
  end
end
