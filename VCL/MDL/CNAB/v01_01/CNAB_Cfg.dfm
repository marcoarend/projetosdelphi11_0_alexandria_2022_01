object FmCNAB_Cfg: TFmCNAB_Cfg
  Left = 363
  Top = 168
  Caption = 'BCO-CNAB_-001 :: Configura'#231#245'es CNAB'
  ClientHeight = 889
  ClientWidth = 1241
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelDados: TPanel
    Left = 0
    Top = 64
    Width = 1241
    Height = 825
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    object GBCntrl: TGroupBox
      Left = 0
      Top = 746
      Width = 1241
      Height = 79
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 18
        Width = 212
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 214
        Top = 18
        Width = 384
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 598
        Top = 18
        Width = 641
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtCNAB_CfOR: TBitBtn
          Tag = 10044
          Left = 300
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Tarifas'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCNAB_CfORClick
        end
        object BtCNAB_Cfg: TBitBtn
          Tag = 10088
          Left = 5
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Config. CNAB'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtCNAB_CfgClick
        end
        object Panel2: TPanel
          Left = 507
          Top = 0
          Width = 134
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 111
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtDuplica: TBitBtn
          Tag = 177
          Left = 153
          Top = 5
          Width = 147
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Duplica'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtDuplicaClick
        end
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 148
      Width = 1241
      Height = 129
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Caption = ' Tarifas banc'#225'rias: '
      TabOrder = 1
      object DBGCNAB_CfOR: TdmkDBGridZTO
        Left = 2
        Top = 18
        Width = 1237
        Height = 109
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        DataSource = DsCNAB_CfOR
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -15
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Descri'#231#227'o'
            Width = 300
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ocorrencia'
            Title.Caption = 'Ocorr'#234'ncia'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Genero'
            Title.Caption = 'Conta'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEGENERO'
            Title.Caption = 'Descri'#231#227'o da conta do plano de contas'
            Visible = True
          end>
      end
    end
    object Panel27: TPanel
      Left = 0
      Top = 0
      Width = 1241
      Height = 148
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object Panel26: TPanel
        Left = 0
        Top = 0
        Width = 1241
        Height = 148
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label94: TLabel
          Left = 5
          Top = 10
          Width = 47
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'C'#243'digo:'
        end
        object Label95: TLabel
          Left = 118
          Top = 10
          Width = 65
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Descri'#231#227'o:'
        end
        object Label96: TLabel
          Left = 5
          Top = 38
          Width = 125
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Layout remessa [F4]:'
        end
        object Label97: TLabel
          Left = 734
          Top = 10
          Width = 42
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Banco:'
        end
        object Label98: TLabel
          Left = 5
          Top = 69
          Width = 53
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Ag'#234'ncia:'
        end
        object Label99: TLabel
          Left = 59
          Top = 69
          Width = 20
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '[A]:'
        end
        object Label100: TLabel
          Left = 162
          Top = 69
          Width = 90
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Conta corrente:'
        end
        object Label101: TLabel
          Left = 256
          Top = 69
          Width = 20
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '[C]:'
        end
        object Label102: TLabel
          Left = 276
          Top = 69
          Width = 29
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '[AC]:'
        end
        object Label103: TLabel
          Left = 458
          Top = 69
          Width = 38
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Posto:'
        end
        object Label104: TLabel
          Left = 542
          Top = 69
          Width = 192
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'C'#243'digo especial (concatenado):'
        end
        object Label105: TLabel
          Left = 975
          Top = 69
          Width = 87
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Complemento:'
        end
        object Label2: TLabel
          Left = 5
          Top = 108
          Width = 40
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Status:'
        end
        object Label17: TLabel
          Left = 241
          Top = 108
          Width = 69
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Data / hora:'
        end
        object Label18: TLabel
          Left = 511
          Top = 108
          Width = 50
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Usu'#225'rio:'
        end
        object RadioGroup1: TDBRadioGroup
          Left = 1167
          Top = 0
          Width = 74
          Height = 148
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Caption = ' CNAB: '
          DataField = 'CNAB'
          DataSource = DsCNAB_Cfg
          Items.Strings = (
            '000'
            '240'
            '400')
          TabOrder = 0
          OnClick = RGCNABClick
        end
        object EdLayoutRem_TXT2: TEdit
          Left = 414
          Top = 34
          Width = 748
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ReadOnly = True
          TabOrder = 1
        end
        object DBEdCodigo: TDBEdit
          Left = 54
          Top = 5
          Width = 59
          Height = 24
          Hint = 'N'#186' do banco'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsCNAB_Cfg
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          MaxLength = 1
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 2
        end
        object DBEdNome: TDBEdit
          Left = 187
          Top = 5
          Width = 542
          Height = 24
          Hint = 'Nome do banco'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsCNAB_Cfg
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 27
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
        end
        object DBEdit7: TDBEdit
          Left = 783
          Top = 5
          Width = 34
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'CedBanco'
          DataSource = DsCNAB_Cfg
          TabOrder = 4
        end
        object DBEdit8: TDBEdit
          Left = 817
          Top = 5
          Width = 345
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NOMEBANCO'
          DataSource = DsCNAB_Cfg
          TabOrder = 5
        end
        object DBEdit9: TDBEdit
          Left = 133
          Top = 34
          Width = 281
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'LayoutRem'
          DataSource = DsCNAB_Cfg
          TabOrder = 6
          OnChange = DBEdit9Change
        end
        object DBEdit10: TDBEdit
          Left = 79
          Top = 64
          Width = 54
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'CedAgencia'
          DataSource = DsCNAB_Cfg
          TabOrder = 7
        end
        object DBEdit11: TDBEdit
          Left = 308
          Top = 64
          Width = 96
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'CedConta'
          DataSource = DsCNAB_Cfg
          TabOrder = 8
        end
        object DBEdit13: TDBEdit
          Left = 404
          Top = 64
          Width = 24
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'CedDAC_C'
          DataSource = DsCNAB_Cfg
          TabOrder = 9
        end
        object DBEdit14: TDBEdit
          Left = 428
          Top = 64
          Width = 25
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'CedDAC_AC'
          DataSource = DsCNAB_Cfg
          TabOrder = 10
        end
        object DBEdit2: TDBEdit
          Left = 497
          Top = 64
          Width = 40
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'CedPosto'
          DataSource = DsCNAB_Cfg
          TabOrder = 11
        end
        object DBEdit3: TDBEdit
          Left = 729
          Top = 64
          Width = 243
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'ConcatCod'
          DataSource = DsCNAB_Cfg
          TabOrder = 12
        end
        object DBEdit12: TDBEdit
          Left = 133
          Top = 64
          Width = 22
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'CedDAC_A'
          DataSource = DsCNAB_Cfg
          TabOrder = 13
        end
        object DBEdit4: TDBEdit
          Left = 1058
          Top = 64
          Width = 104
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'ComplmCod'
          DataSource = DsCNAB_Cfg
          TabOrder = 14
        end
        object DBEdit1: TDBEdit
          Left = 49
          Top = 105
          Width = 185
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Status_TXT'
          DataSource = DsCNAB_Cfg
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 15
        end
        object DBEdit5: TDBEdit
          Left = 318
          Top = 105
          Width = 184
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'DataHoraAceite_TXT'
          DataSource = DsCNAB_Cfg
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 16
        end
        object DBEdit6: TDBEdit
          Left = 566
          Top = 105
          Width = 185
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'UsuarioAceite_TXT'
          DataSource = DsCNAB_Cfg
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 17
        end
        object DBCheckBox2: TDBCheckBox
          Left = 757
          Top = 106
          Width = 61
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Ativo'
          DataField = 'Ativo'
          DataSource = DsCNAB_Cfg
          TabOrder = 18
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 64
    Width = 1241
    Height = 825
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 1241
      Height = 666
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 1241
        Height = 121
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label9: TLabel
          Left = 5
          Top = 10
          Width = 47
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'C'#243'digo:'
        end
        object Label10: TLabel
          Left = 118
          Top = 10
          Width = 65
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Descri'#231#227'o:'
        end
        object Label82: TLabel
          Left = 5
          Top = 38
          Width = 125
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Layout remessa [F4]:'
        end
        object Label3: TLabel
          Left = 734
          Top = 10
          Width = 42
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Banco:'
        end
        object Label5: TLabel
          Left = 5
          Top = 69
          Width = 53
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Ag'#234'ncia:'
        end
        object Label11: TLabel
          Left = 59
          Top = 69
          Width = 20
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '[A]:'
        end
        object Label7: TLabel
          Left = 162
          Top = 69
          Width = 90
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Conta corrente:'
        end
        object Label12: TLabel
          Left = 256
          Top = 69
          Width = 20
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '[C]:'
        end
        object Label13: TLabel
          Left = 276
          Top = 69
          Width = 29
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '[AC]:'
        end
        object Label51: TLabel
          Left = 458
          Top = 69
          Width = 38
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Posto:'
        end
        object Label84: TLabel
          Left = 542
          Top = 69
          Width = 192
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'C'#243'digo especial (concatenado):'
        end
        object SpeedButton9: TSpeedButton
          Left = 945
          Top = 64
          Width = 26
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '?'
          OnClick = SpeedButton9Click
        end
        object Label87: TLabel
          Left = 975
          Top = 69
          Width = 87
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Complemento:'
        end
        object EdCodigo: TdmkEdit
          Left = 54
          Top = 5
          Width = 59
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdNome: TdmkEdit
          Left = 187
          Top = 5
          Width = 542
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object RGCNAB: TRadioGroup
          Left = 1167
          Top = 0
          Width = 74
          Height = 121
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Caption = ' CNAB: '
          ItemIndex = 0
          Items.Strings = (
            '000'
            '240'
            '400')
          TabOrder = 4
          OnClick = RGCNABClick
        end
        object EdLayoutRem: TdmkEdit
          Left = 128
          Top = 34
          Width = 282
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          CharCase = ecUpperCase
          ReadOnly = True
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdLayoutRemChange
        end
        object EdLayoutRem_TXT1: TEdit
          Left = 414
          Top = 34
          Width = 748
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ReadOnly = True
          TabOrder = 6
        end
        object EdCedBanco: TdmkEditCB
          Left = 783
          Top = 5
          Width = 35
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 3
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdCedBancoChange
          OnExit = EdCedBancoExit
          DBLookupComboBox = CBCedBanco
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCedBanco: TdmkDBLookupComboBox
          Left = 817
          Top = 5
          Width = 346
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsBancos
          TabOrder = 3
          dmkEditCB = EdCedBanco
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdCedAgencia: TdmkEdit
          Left = 79
          Top = 64
          Width = 54
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 4
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdCedDAC_A: TdmkEdit
          Left = 133
          Top = 64
          Width = 25
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          CharCase = ecUpperCase
          MaxLength = 1
          TabOrder = 8
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCedConta: TdmkEdit
          Left = 305
          Top = 64
          Width = 99
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 9
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCedDAC_C: TdmkEdit
          Left = 404
          Top = 64
          Width = 24
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          CharCase = ecUpperCase
          MaxLength = 1
          TabOrder = 10
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCedDAC_AC: TdmkEdit
          Left = 428
          Top = 64
          Width = 25
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          CharCase = ecUpperCase
          MaxLength = 1
          TabOrder = 11
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCedPosto: TdmkEdit
          Left = 497
          Top = 64
          Width = 41
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 12
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 4
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdConcatCod: TdmkEdit
          Left = 729
          Top = 64
          Width = 213
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 13
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdComplmCod: TdmkEdit
          Left = 1058
          Top = 64
          Width = 104
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 14
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object CkAtivo: TdmkCheckBox
          Left = 5
          Top = 92
          Width = 61
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Ativo'
          TabOrder = 15
          QryCampo = 'Ativo'
          UpdCampo = 'Ativo'
          UpdType = utYes
          ValCheck = '1'
          ValUncheck = '0'
          OldValor = #0
        end
      end
      object PageControl1: TPageControl
        Left = 0
        Top = 121
        Width = 1241
        Height = 545
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ActivePage = TabSheet7
        Align = alClient
        TabOrder = 1
        object TabSheet5: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Dados de cobran'#231'a A'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 1233
            Height = 514
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Panel11: TPanel
              Left = 859
              Top = 0
              Width = 374
              Height = 514
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alRight
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object Label39: TLabel
                Left = 10
                Top = 241
                Width = 146
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'C'#243'digo oculto do banco:'
              end
              object Label40: TLabel
                Left = 10
                Top = 212
                Width = 190
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Sequ'#234'ncia de envio do arquivo:'
              end
              object Label15: TLabel
                Left = 10
                Top = 182
                Width = 129
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = #218'ltimo nosso n'#250'mero:'
              end
              object Label24: TLabel
                Left = 10
                Top = 94
                Width = 254
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Opera'#231#227'o c'#243'digo (somente bco 104 [870]):'
              end
              object Label41: TLabel
                Left = 10
                Top = 123
                Width = 167
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'ID da carteira (104 [80,82,9]):'
              end
              object Label42: TLabel
                Left = 10
                Top = 153
                Width = 159
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Ag'#234'ncia / C'#243'digo cedente:'
              end
              object Label75: TLabel
                Left = 15
                Top = 330
                Width = 112
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'C'#243'digo da Moeda:'
              end
              object Label76: TLabel
                Left = 15
                Top = 359
                Width = 53
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Contrato:'
              end
              object Label83: TLabel
                Left = 15
                Top = 389
                Width = 121
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Layout retorrno  [F4]:'
              end
              object Label88: TLabel
                Left = 20
                Top = 443
                Width = 223
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Vers'#227'o de remessa ( padr'#227'o = '#39'000'#39' ):'
              end
              object Label89: TLabel
                Left = 20
                Top = 473
                Width = 259
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Conta corrente cooperado (Nosso n'#250'mero):'
              end
              object EdCodOculto: TdmkEdit
                Left = 251
                Top = 236
                Width = 115
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdSeqArq: TdmkEdit
                Left = 251
                Top = 207
                Width = 115
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 5
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdLastNosNum: TdmkEdit
                Left = 251
                Top = 177
                Width = 115
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object RGAceiteTit: TRadioGroup
                Left = 130
                Top = 261
                Width = 116
                Height = 59
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' Aceite do t'#237'tulo: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o'
                  'Sim')
                TabOrder = 8
              end
              object RGCorreio: TRadioGroup
                Left = 250
                Top = 261
                Width = 116
                Height = 59
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' Correio: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o'
                  'Sim')
                TabOrder = 9
              end
              object EdOperCodi: TdmkEdit
                Left = 271
                Top = 89
                Width = 95
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 3
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdIDCobranca: TdmkEdit
                Left = 271
                Top = 118
                Width = 95
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 3
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdAgContaCed: TdmkEdit
                Left = 172
                Top = 148
                Width = 194
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdMoedaCod: TdmkEdit
                Left = 133
                Top = 325
                Width = 39
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                CharCase = ecUpperCase
                TabOrder = 10
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdMoedaCodChange
              end
              object EdMoedaCod_TXT: TEdit
                Left = 176
                Top = 325
                Width = 190
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                ReadOnly = True
                TabOrder = 11
              end
              object EdContrato: TdmkEdit
                Left = 79
                Top = 354
                Width = 287
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                CharCase = ecUpperCase
                TabOrder = 12
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdMoedaCodChange
              end
              object GroupBox17: TGroupBox
                Left = 0
                Top = 0
                Width = 374
                Height = 84
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Caption = ' C'#243'digo cedente/ cadastro do cliente no banco / conv'#234'nio: '
                TabOrder = 0
                object Label34: TLabel
                  Left = 10
                  Top = 25
                  Width = 33
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'L'#237'der:'
                end
                object Label80: TLabel
                  Left = 10
                  Top = 54
                  Width = 62
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Cobran'#231'a:'
                end
                object EdCodLidrBco: TdmkEdit
                  Left = 167
                  Top = 49
                  Width = 197
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  MaxLength = 20
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = True
                  ValueVariant = ''
                  ValWarn = False
                  OnExit = EdCodLidrBcoExit
                end
                object EdCodEmprBco: TdmkEdit
                  Left = 167
                  Top = 20
                  Width = 197
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  MaxLength = 20
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = True
                  ValueVariant = ''
                  ValWarn = False
                  OnExit = EdCodEmprBcoExit
                end
              end
              object EdLayoutRet: TdmkEdit
                Left = 138
                Top = 384
                Width = 228
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                CharCase = ecUpperCase
                ReadOnly = True
                TabOrder = 13
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdLayoutRetChange
              end
              object EdLayoutRet_TXT: TEdit
                Left = 15
                Top = 409
                Width = 351
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                ReadOnly = True
                TabOrder = 14
              end
              object EdNumVersaoI3: TdmkEdit
                Left = 325
                Top = 438
                Width = 39
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                CharCase = ecUpperCase
                TabOrder = 15
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 3
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdMoedaCodChange
              end
              object RGModalCobr: TRadioGroup
                Left = 10
                Top = 261
                Width = 116
                Height = 59
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' Com registro: '
                Columns = 2
                ItemIndex = 1
                Items.Strings = (
                  'N'#227'o'
                  'Sim')
                TabOrder = 7
                OnClick = RGModalCobrClick
              end
              object EdCtaCooper: TdmkEdit
                Left = 276
                Top = 468
                Width = 88
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                CharCase = ecUpperCase
                TabOrder = 16
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 3
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdMoedaCodChange
              end
            end
            object Panel13: TPanel
              Left = 0
              Top = 0
              Width = 859
              Height = 514
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object GroupBox9: TGroupBox
                Left = 0
                Top = 167
                Width = 859
                Height = 347
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                Caption = ' Carteira de cobran'#231'a: '
                TabOrder = 1
                object Panel9: TPanel
                  Left = 2
                  Top = 18
                  Width = 855
                  Height = 87
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 0
                  object GroupBox5: TGroupBox
                    Left = 0
                    Top = 0
                    Width = 855
                    Height = 87
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    Caption = ' Carteira de cobran'#231'a [F4]: '
                    TabOrder = 0
                    object Label4: TLabel
                      Left = 10
                      Top = 25
                      Width = 86
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'N'#250'm/c'#243'd/blq*:'
                    end
                    object Label47: TLabel
                      Left = 10
                      Top = 54
                      Width = 58
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Varia'#231#227'o:'
                    end
                    object EdCartNum: TdmkEdit
                      Left = 148
                      Top = 20
                      Width = 39
                      Height = 26
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      CharCase = ecUpperCase
                      TabOrder = 1
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                      OnChange = EdCartNumChange
                    end
                    object EdCartCod: TdmkEdit
                      Left = 108
                      Top = 20
                      Width = 40
                      Height = 26
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      CharCase = ecUpperCase
                      TabOrder = 0
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                      OnChange = EdCartNumChange
                    end
                    object EdCarteira_TXT: TEdit
                      Left = 226
                      Top = 20
                      Width = 611
                      Height = 24
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      ReadOnly = True
                      TabOrder = 2
                    end
                    object EdVariacao: TdmkEdit
                      Left = 187
                      Top = 49
                      Width = 39
                      Height = 26
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Alignment = taRightJustify
                      CharCase = ecUpperCase
                      TabOrder = 3
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '000'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      OnChange = EdVariacaoChange
                    end
                    object EdVariacao_TXT: TEdit
                      Left = 226
                      Top = 49
                      Width = 611
                      Height = 24
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      ReadOnly = True
                      TabOrder = 4
                    end
                    object EdCartTxt: TdmkEdit
                      Left = 187
                      Top = 20
                      Width = 39
                      Height = 26
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      CharCase = ecUpperCase
                      TabOrder = 5
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 3
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                  end
                end
                object MeCarteira: TdmkMemo
                  Left = 2
                  Top = 262
                  Width = 855
                  Height = 83
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  ReadOnly = True
                  ScrollBars = ssVertical
                  TabOrder = 2
                  UpdType = utYes
                end
                object Panel19: TPanel
                  Left = 2
                  Top = 105
                  Width = 855
                  Height = 157
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Label58: TLabel
                    Left = 10
                    Top = 69
                    Width = 167
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Quem gera o bloqueto? [F4]'
                  end
                  object Label57: TLabel
                    Left = 10
                    Top = 98
                    Width = 199
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Identifica'#231#227'o da distribui'#231#227'o. [F4]:'
                  end
                  object Label55: TLabel
                    Left = 10
                    Top = 10
                    Width = 229
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Cadastramento do t'#237'tulo no banco [F4]:'
                  end
                  object Label86: TLabel
                    Left = 5
                    Top = 128
                    Width = 132
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Tipo de bloqueto [F4]:'
                  end
                  object Label92: TLabel
                    Left = 729
                    Top = 39
                    Width = 19
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'at'#233
                  end
                  object EdQuemPrint_TXT: TEdit
                    Left = 286
                    Top = 64
                    Width = 551
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    ReadOnly = True
                    TabOrder = 6
                  end
                  object EdQuemPrint: TdmkEdit
                    Left = 246
                    Top = 64
                    Width = 40
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    CharCase = ecUpperCase
                    TabOrder = 5
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                    OnChange = EdQuemPrintChange
                  end
                  object EdQuemDistrb: TdmkEdit
                    Left = 246
                    Top = 94
                    Width = 40
                    Height = 25
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    CharCase = ecUpperCase
                    TabOrder = 7
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                    OnChange = EdQuemDistrbChange
                  end
                  object EdQuemDistrb_TXT: TEdit
                    Left = 286
                    Top = 94
                    Width = 551
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    ReadOnly = True
                    TabOrder = 8
                  end
                  object EdTipoCart: TdmkEdit
                    Left = 246
                    Top = 5
                    Width = 40
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Alignment = taRightJustify
                    CharCase = ecUpperCase
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    OnChange = EdTipoCartChange
                  end
                  object EdTipoCart_TXT: TEdit
                    Left = 286
                    Top = 5
                    Width = 551
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    ReadOnly = True
                    TabOrder = 1
                  end
                  object EdTipBloqUso: TdmkEdit
                    Left = 143
                    Top = 123
                    Width = 24
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    TabOrder = 9
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                    OnChange = EdTipBloqUsoChange
                  end
                  object EdTipBloqUso_TXT: TdmkEdit
                    Left = 167
                    Top = 123
                    Width = 248
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    TabOrder = 10
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                    OnChange = EdEspecieTitChange
                  end
                  object EdTipBloqUso_TXT2: TdmkEdit
                    Left = 414
                    Top = 123
                    Width = 423
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    ReadOnly = True
                    TabOrder = 11
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                  end
                  object EdNosNumFxaI: TdmkEdit
                    Left = 664
                    Top = 34
                    Width = 65
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Alignment = taRightJustify
                    CharCase = ecUpperCase
                    TabOrder = 3
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    OnChange = EdTipoCartChange
                  end
                  object EdNosNumFxaF: TdmkEdit
                    Left = 751
                    Top = 34
                    Width = 86
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Alignment = taRightJustify
                    CharCase = ecUpperCase
                    TabOrder = 4
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    OnChange = EdTipoCartChange
                  end
                  object CkNosNumFxaU: TdmkCheckBox
                    Left = 246
                    Top = 36
                    Width = 410
                    Height = 21
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Usa faixa de numera'#231#227'o do nosso n'#250'mero fornecida pelo banco:'
                    TabOrder = 2
                    UpdType = utYes
                    ValCheck = #0
                    ValUncheck = #0
                    OldValor = #0
                  end
                end
              end
              object Panel12: TPanel
                Left = 0
                Top = 0
                Width = 859
                Height = 167
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object Label16: TLabel
                  Left = 5
                  Top = 10
                  Width = 127
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Local de pagamento:'
                end
                object Label25: TLabel
                  Left = 10
                  Top = 84
                  Width = 127
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Esp'#233'cie do doc. [F4]:'
                end
                object Label14: TLabel
                  Left = 5
                  Top = 143
                  Width = 131
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Esp'#233'cie do valor [F4]:'
                end
                object Label54: TLabel
                  Left = 5
                  Top = 38
                  Width = 126
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Comando (ocor.) [F4]'
                  Enabled = False
                end
                object Label48: TLabel
                  Left = 148
                  Top = 59
                  Width = 63
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Envio arq.:'
                end
                object Label49: TLabel
                  Left = 217
                  Top = 59
                  Width = 88
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Impr. bloqueto:'
                end
                object EdLocalPag: TdmkEdit
                  Left = 148
                  Top = 5
                  Width = 694
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdEspecieTit: TdmkEdit
                  Left = 148
                  Top = 79
                  Width = 69
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 3
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                  OnChange = EdEspecieTitChange
                end
                object EdEspecieTit_TXT: TdmkEdit
                  Left = 310
                  Top = 79
                  Width = 532
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  ReadOnly = True
                  TabOrder = 5
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdEspecieVal: TdmkEdit
                  Left = 148
                  Top = 138
                  Width = 69
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 7
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                  OnChange = EdEspecieValChange
                end
                object EdEspecieVal_TXT: TdmkEdit
                  Left = 310
                  Top = 138
                  Width = 532
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  ReadOnly = True
                  TabOrder = 9
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdComando: TdmkEdit
                  Left = 148
                  Top = 33
                  Width = 69
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  ReadOnly = True
                  TabOrder = 1
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 2
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '1'
                  ValMax = '99'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '01'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 1
                  ValWarn = False
                  OnChange = EdComandoChange
                end
                object EdComando_TXT: TdmkEdit
                  Left = 217
                  Top = 33
                  Width = 625
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  ReadOnly = True
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = 'Registro de t'#237'tulos'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 'Registro de t'#237'tulos'
                  ValWarn = False
                end
                object EdEspecieDoc: TdmkEdit
                  Left = 217
                  Top = 79
                  Width = 93
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 4
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                  OnChange = EdEspecieTitChange
                end
                object EdEspecieTit_TXT2: TdmkEdit
                  Left = 310
                  Top = 108
                  Width = 532
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  ReadOnly = True
                  TabOrder = 6
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdEspecieTxt: TdmkEdit
                  Left = 217
                  Top = 138
                  Width = 93
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 8
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
            end
          end
        end
        object TabSheet4: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Dados de cobran'#231'a B'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel17: TPanel
            Left = 0
            Top = 0
            Width = 1233
            Height = 514
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object GroupBox2: TGroupBox
              Left = 5
              Top = 6
              Width = 951
              Height = 75
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Dados do Cedente: '
              TabOrder = 0
              object Label8: TLabel
                Left = 556
                Top = 20
                Width = 203
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Nome do cedente a ser impresso:'
              end
              object Label31: TLabel
                Left = 15
                Top = 20
                Width = 54
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Cedente:'
              end
              object SpeedButton10: TSpeedButton
                Left = 528
                Top = 39
                Width = 26
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '...'
                OnClick = SpeedButton10Click
              end
              object EdCedNome: TdmkEdit
                Left = 556
                Top = 39
                Width = 386
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                CharCase = ecUpperCase
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnExit = EdCedNomeExit
              end
              object EdCedente: TdmkEditCB
                Left = 15
                Top = 39
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdCedenteChange
                DBLookupComboBox = CBCedente
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBCedente: TdmkDBLookupComboBox
                Left = 84
                Top = 39
                Width = 440
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'NOMEENT'
                ListSource = DsCedente
                TabOrder = 1
                dmkEditCB = EdCedente
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
            object GroupBox6: TGroupBox
              Left = 5
              Top = 84
              Width = 951
              Height = 75
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 
                ' Dados do Sacador / Avalista: (Preencher somente quando o cedent' +
                'e n'#227'o for o cliente interno): '
              TabOrder = 1
              object Label32: TLabel
                Left = 15
                Top = 20
                Width = 112
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Sacador / avalista:'
              end
              object Label33: TLabel
                Left = 556
                Top = 20
                Width = 287
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Nome do sacador / avalista a ser impresso: [F4]'
              end
              object SpeedButton11: TSpeedButton
                Left = 527
                Top = 39
                Width = 26
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '...'
                OnClick = SpeedButton11Click
              end
              object EdSacadAvali: TdmkEditCB
                Left = 15
                Top = 39
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdSacadAvaliChange
                DBLookupComboBox = CBSacadAvali
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBSacadAvali: TdmkDBLookupComboBox
                Left = 84
                Top = 39
                Width = 439
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'NOMEENT'
                ListSource = DsSacador
                TabOrder = 1
                dmkEditCB = EdSacadAvali
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdSacAvaNome: TdmkEdit
                Left = 556
                Top = 39
                Width = 386
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                CharCase = ecUpperCase
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnExit = EdSacAvaNomeExit
                OnKeyDown = EdSacAvaNomeKeyDown
              end
            end
            object GroupBox11: TGroupBox
              Left = 4
              Top = 172
              Width = 568
              Height = 80
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' 1'#186' desconto:  '
              TabOrder = 2
              object Label62: TLabel
                Left = 418
                Top = 25
                Width = 67
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Percentual:'
              end
              object Label63: TLabel
                Left = 15
                Top = 25
                Width = 151
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'C'#243'digo do desconto [F4]:'
              end
              object Label64: TLabel
                Left = 492
                Top = 25
                Width = 57
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Dd antes:'
              end
              object EdDesco1Fat: TdmkEdit
                Left = 418
                Top = 44
                Width = 71
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdDesco1Cod: TdmkEdit
                Left = 15
                Top = 44
                Width = 39
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdDesco1CodChange
              end
              object EdDesco1Cod_TXT: TdmkEdit
                Left = 55
                Top = 44
                Width = 360
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                ReadOnly = True
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDesco1Dds: TdmkEdit
                Left = 492
                Top = 44
                Width = 70
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
            object GroupBox12: TGroupBox
              Left = 4
              Top = 256
              Width = 568
              Height = 80
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' 2'#186' desconto:  '
              TabOrder = 3
              object Label65: TLabel
                Left = 418
                Top = 25
                Width = 67
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Percentual:'
              end
              object Label66: TLabel
                Left = 15
                Top = 25
                Width = 151
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'C'#243'digo do desconto [F4]:'
              end
              object Label67: TLabel
                Left = 492
                Top = 25
                Width = 57
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Dd antes:'
              end
              object EdDesco2Fat: TdmkEdit
                Left = 418
                Top = 44
                Width = 71
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdDesco2Cod: TdmkEdit
                Left = 15
                Top = 44
                Width = 39
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdDesco2CodChange
              end
              object EdDesco2Cod_TXT: TdmkEdit
                Left = 55
                Top = 44
                Width = 360
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                ReadOnly = True
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDesco2Dds: TdmkEdit
                Left = 492
                Top = 44
                Width = 70
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
            object GroupBox13: TGroupBox
              Left = 4
              Top = 340
              Width = 568
              Height = 80
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' 3'#186' desconto:  '
              TabOrder = 4
              object Label68: TLabel
                Left = 418
                Top = 25
                Width = 67
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Percentual:'
              end
              object Label69: TLabel
                Left = 15
                Top = 25
                Width = 151
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'C'#243'digo do desconto [F4]:'
              end
              object Label70: TLabel
                Left = 492
                Top = 25
                Width = 57
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Dd antes:'
              end
              object EdDesco3Fat: TdmkEdit
                Left = 418
                Top = 44
                Width = 71
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdDesco3Cod: TdmkEdit
                Left = 15
                Top = 44
                Width = 39
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdDesco3CodChange
              end
              object EdDesco3Cod_TXT: TdmkEdit
                Left = 55
                Top = 44
                Width = 360
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                ReadOnly = True
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDesco3Dds: TdmkEdit
                Left = 492
                Top = 44
                Width = 70
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
            object GroupBox19: TGroupBox
              Left = 581
              Top = 172
              Width = 154
              Height = 80
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Contrato de Garantia: '
              TabOrder = 5
              object Label91: TLabel
                Left = 15
                Top = 25
                Width = 51
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'N'#250'mero:'
              end
              object Label93: TLabel
                Left = 108
                Top = 25
                Width = 22
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'DV:'
              end
              object EdCtrGarantiaNr: TdmkEdit
                Left = 15
                Top = 44
                Width = 88
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 5
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '00000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdCtrGarantiaDV: TdmkEdit
                Left = 108
                Top = 44
                Width = 25
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = '0'
                ValWarn = False
                OnChange = EdDesco1CodChange
              end
            end
          end
        end
        object TabSheet6: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Instru'#231#245'es de cobran'#231'a {1} '
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 1233
            Height = 514
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label30: TLabel
              Left = 5
              Top = 404
              Width = 685
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 
                'Quantidade de dias (v'#225'lido para apenas uma instru'#231#227'o do banco 34' +
                '1 > Protesto ou n'#227'o recebimento ou devolu'#231#227'o:'
            end
            object Label27: TLabel
              Left = 5
              Top = 202
              Width = 172
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Instru'#231#227'o de cobran'#231'a 2: [F4]'
            end
            object Label26: TLabel
              Left = 5
              Top = 5
              Width = 172
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Instru'#231#227'o de cobran'#231'a 1: [F4]'
            end
            object EdInstrDias: TdmkEdit
              Left = 684
              Top = 399
              Width = 45
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = True
              ValueVariant = ''
              ValWarn = False
            end
            object dmkMemo2: TdmkMemo
              Left = 4
              Top = 251
              Width = 951
              Height = 140
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              ScrollBars = ssVertical
              TabOrder = 1
              UpdType = utYes
            end
            object EdInstrCobr2: TdmkEdit
              Left = 5
              Top = 222
              Width = 39
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdInstrCobr2Change
            end
            object EdInstrCobr2_TXT: TdmkEdit
              Left = 49
              Top = 222
              Width = 907
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              ReadOnly = True
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object dmkMemo1: TdmkMemo
              Left = 5
              Top = 54
              Width = 951
              Height = 140
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              ScrollBars = ssVertical
              TabOrder = 4
              UpdType = utYes
            end
            object EdInstrCobr1: TdmkEdit
              Left = 5
              Top = 25
              Width = 39
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdInstrCobr1Change
            end
            object EdInstrCobr1_TXT: TdmkEdit
              Left = 49
              Top = 25
              Width = 907
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              ReadOnly = True
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
        end
        object TabSheet7: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Instru'#231#245'es de cobran'#231'a {2} '
          ImageIndex = 3
          object Panel18: TPanel
            Left = 0
            Top = 0
            Width = 1233
            Height = 514
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object GroupBox7: TGroupBox
              Left = 5
              Top = 5
              Width = 631
              Height = 321
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Texto de responsabilidade do cedente:'
              TabOrder = 0
              object EdTexto01: TEdit
                Left = 10
                Top = 20
                Width = 612
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 0
                OnEnter = EdTexto01Enter
                OnExit = EdTexto01Exit
              end
              object EdTexto02: TEdit
                Left = 10
                Top = 49
                Width = 612
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 1
                OnEnter = EdTexto01Enter
                OnExit = EdTexto01Exit
              end
              object EdTexto03: TEdit
                Left = 10
                Top = 79
                Width = 612
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 2
                OnEnter = EdTexto01Enter
                OnExit = EdTexto01Exit
              end
              object EdTexto04: TEdit
                Left = 10
                Top = 108
                Width = 612
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 3
                OnEnter = EdTexto01Enter
                OnExit = EdTexto01Exit
              end
              object EdTexto05: TEdit
                Left = 10
                Top = 138
                Width = 612
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 4
                OnEnter = EdTexto01Enter
                OnExit = EdTexto01Exit
              end
              object EdTexto06: TEdit
                Left = 10
                Top = 167
                Width = 612
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 5
                OnEnter = EdTexto01Enter
                OnExit = EdTexto01Exit
              end
              object EdTexto07: TEdit
                Left = 10
                Top = 197
                Width = 612
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 6
                OnEnter = EdTexto01Enter
                OnExit = EdTexto01Exit
              end
              object EdTexto08: TEdit
                Left = 10
                Top = 226
                Width = 612
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 7
                OnEnter = EdTexto01Enter
                OnExit = EdTexto01Exit
              end
              object EdTexto09: TEdit
                Left = 10
                Top = 256
                Width = 612
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 8
                OnEnter = EdTexto01Enter
                OnExit = EdTexto01Exit
              end
              object EdTexto10: TEdit
                Left = 10
                Top = 286
                Width = 612
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 9
                OnEnter = EdTexto01Enter
                OnExit = EdTexto01Exit
              end
            end
            object CkEnvEmeio: TCheckBox
              Left = 639
              Top = 353
              Width = 480
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Desejo que o banco entregue o bloqueto por emeio ao sacado.'
              TabOrder = 1
            end
            object GBMulta: TGroupBox
              Left = 639
              Top = 5
              Width = 239
              Height = 134
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Cobran'#231'a de multa por atrazo: '
              TabOrder = 2
              object Label35: TLabel
                Left = 15
                Top = 79
                Width = 67
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Percentual:'
              end
              object Label59: TLabel
                Left = 15
                Top = 25
                Width = 147
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Forma de cobran'#231'a [F4]:'
              end
              object Label77: TLabel
                Left = 15
                Top = 108
                Width = 137
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Dias ap'#243's vencimento:'
              end
              object EdMultaPerc: TdmkEdit
                Left = 162
                Top = 74
                Width = 71
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '2,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 2.000000000000000000
                ValWarn = False
              end
              object EdMultaTipo: TdmkEdit
                Left = 15
                Top = 44
                Width = 39
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdMultaTipoChange
              end
              object EdMultaTipo_TXT: TdmkEdit
                Left = 55
                Top = 44
                Width = 178
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                ReadOnly = True
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdMultaDias: TdmkEdit
                Left = 162
                Top = 103
                Width = 71
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                MaxLength = 2
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
            object GroupBox8: TGroupBox
              Left = 880
              Top = 5
              Width = 239
              Height = 134
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Cobran'#231'a de juros por atrazo: '
              TabOrder = 3
              object Label36: TLabel
                Left = 15
                Top = 103
                Width = 137
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Dias ap'#243's vencimento:'
              end
              object Label60: TLabel
                Left = 15
                Top = 20
                Width = 147
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Forma de cobran'#231'a [F4]:'
              end
              object Label61: TLabel
                Left = 15
                Top = 74
                Width = 67
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Percentual:'
              end
              object EdJurosPerc: TdmkEdit
                Left = 158
                Top = 69
                Width = 75
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '1,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 1.000000000000000000
                ValWarn = False
              end
              object EdJurosDias: TdmkEdit
                Left = 158
                Top = 98
                Width = 75
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                MaxLength = 2
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdJurosTipo: TdmkEdit
                Left = 15
                Top = 39
                Width = 39
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdJurosTipoChange
              end
              object EdJurosTipo_TXT: TdmkEdit
                Left = 55
                Top = 39
                Width = 178
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                ReadOnly = True
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
            object GroupBox14: TGroupBox
              Left = 639
              Top = 143
              Width = 237
              Height = 99
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Protesto: '
              TabOrder = 4
              object Label71: TLabel
                Left = 15
                Top = 20
                Width = 47
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'C'#243'digo:'
              end
              object Label72: TLabel
                Left = 20
                Top = 74
                Width = 137
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Dias ap'#243's vencimento:'
              end
              object EdProtesCod: TdmkEdit
                Left = 15
                Top = 39
                Width = 39
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                CharCase = ecUpperCase
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdProtesCodChange
              end
              object EdProtesCod_TXT: TEdit
                Left = 58
                Top = 39
                Width = 170
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                ReadOnly = True
                TabOrder = 1
              end
              object EdProtesDds: TdmkEdit
                Left = 167
                Top = 69
                Width = 61
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                MaxLength = 2
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
            object GroupBox15: TGroupBox
              Left = 880
              Top = 143
              Width = 239
              Height = 99
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Baixa / devolu'#231#227'o: '
              TabOrder = 5
              object Label74: TLabel
                Left = 20
                Top = 74
                Width = 137
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Dias ap'#243's vencimento:'
              end
              object Label73: TLabel
                Left = 15
                Top = 20
                Width = 47
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'C'#243'digo:'
              end
              object EdBxaDevCod: TdmkEdit
                Left = 15
                Top = 39
                Width = 39
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                CharCase = ecUpperCase
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdBxaDevCodChange
              end
              object EdBxaDevCod_TXT: TEdit
                Left = 58
                Top = 39
                Width = 170
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                ReadOnly = True
                TabOrder = 1
              end
              object EdBxaDevDds: TdmkEdit
                Left = 167
                Top = 69
                Width = 61
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                MaxLength = 2
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
            object GroupBox18: TGroupBox
              Left = 639
              Top = 246
              Width = 237
              Height = 100
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' N'#227'o recebimento: '
              TabOrder = 6
              object Label85: TLabel
                Left = 20
                Top = 74
                Width = 137
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Dias ap'#243's vencimento:'
              end
              object EdNaoRecebDd: TdmkEdit
                Left = 167
                Top = 69
                Width = 61
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                MaxLength = 2
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
            end
            object GroupBox4: TGroupBox
              Left = 5
              Top = 332
              Width = 630
              Height = 130
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Vari'#225'veis para as instru'#231#245'es:'
              TabOrder = 7
              object LbVars: TListView
                Left = 2
                Top = 18
                Width = 626
                Height = 110
                Align = alClient
                Columns = <>
                TabOrder = 0
                OnDblClick = LbVarsDblClick
              end
            end
          end
        end
        object TabSheet8: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Arquivo pesquisado'
          ImageIndex = 4
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 1233
            Height = 36
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label28: TLabel
              Left = 10
              Top = 10
              Width = 49
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Arquivo:'
            end
            object EdArq: TEdit
              Left = 64
              Top = 5
              Width = 887
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              ReadOnly = True
              TabOrder = 0
            end
          end
          object MeLines: TMemo
            Left = 0
            Top = 36
            Width = 1233
            Height = 478
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 1
            WordWrap = False
          end
        end
        object TabSheet9: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Bancos '
          ImageIndex = 5
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object PageControl3: TPageControl
            Left = 0
            Top = 0
            Width = 1233
            Height = 514
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            ActivePage = TabSheet18
            Align = alClient
            TabOrder = 0
            object TabSheet2: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' 001 '
              ImageIndex = 2
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel14: TPanel
                Left = 0
                Top = 0
                Width = 1225
                Height = 483
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object PageControl_001: TPageControl
                  Left = 0
                  Top = 0
                  Width = 1225
                  Height = 483
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  ActivePage = TabSheet14
                  Align = alClient
                  TabOrder = 0
                  object TabSheet13: TTabSheet
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = ' CNAB 240 '
                    ExplicitLeft = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 0
                    object Panel23: TPanel
                      Left = 0
                      Top = 0
                      Width = 1217
                      Height = 452
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 0
                      object GroupBox10: TGroupBox
                        Left = 0
                        Top = 0
                        Width = 1217
                        Height = 452
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alClient
                        Caption = ' Descontos: '
                        TabOrder = 0
                        object GroupBox16: TGroupBox
                          Left = 586
                          Top = 20
                          Width = 607
                          Height = 129
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Caption = ' Informa'#231#245'es obrigat'#243'rias para cobran'#231'a cedente: '
                          TabOrder = 0
                          object Label78: TLabel
                            Left = 14
                            Top = 23
                            Width = 335
                            Height = 16
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Carteira de cobran'#231'a para o caso de cobran'#231'a cedente:'
                          end
                          object Label79: TLabel
                            Left = 14
                            Top = 53
                            Width = 410
                            Height = 16
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 
                              'Varia'#231#227'o da carteira de cobran'#231'a para o caso de cobran'#231'a cedente' +
                              ':'
                          end
                          object EdConvCartCobr: TdmkEdit
                            Left = 418
                            Top = 20
                            Width = 61
                            Height = 26
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            MaxLength = 2
                            TabOrder = 0
                            FormatType = dmktfString
                            MskType = fmtNone
                            DecimalSize = 0
                            LeftZeros = 2
                            NoEnterToTab = False
                            NoForceUppercase = False
                            ValMax = '999'
                            ForceNextYear = False
                            DataFormat = dmkdfShort
                            HoraFormat = dmkhfShort
                            UpdType = utYes
                            Obrigatorio = False
                            PermiteNulo = False
                            ValueVariant = ''
                            ValWarn = False
                          end
                          object EdConvVariCart: TdmkEdit
                            Left = 418
                            Top = 49
                            Width = 61
                            Height = 26
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            MaxLength = 3
                            TabOrder = 1
                            FormatType = dmktfString
                            MskType = fmtNone
                            DecimalSize = 0
                            LeftZeros = 3
                            NoEnterToTab = False
                            NoForceUppercase = False
                            ValMax = '999'
                            ForceNextYear = False
                            DataFormat = dmkdfShort
                            HoraFormat = dmkhfShort
                            UpdType = utYes
                            Obrigatorio = False
                            PermiteNulo = False
                            ValueVariant = ''
                            ValWarn = False
                          end
                        end
                      end
                    end
                  end
                  object TabSheet14: TTabSheet
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = ' CNAB 400 '
                    ImageIndex = 1
                    ExplicitLeft = 0
                    ExplicitTop = 0
                    ExplicitWidth = 0
                    ExplicitHeight = 0
                    object Panel22: TPanel
                      Left = 0
                      Top = 0
                      Width = 1217
                      Height = 452
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 0
                      object Label50: TLabel
                        Left = 10
                        Top = 9
                        Width = 253
                        Height = 16
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Tipo de cobran'#231'a (Forma de registro): [F4]'
                      end
                      object Label52: TLabel
                        Left = 10
                        Top = 38
                        Width = 208
                        Height = 16
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'N'#250'mero de dias para protesto: [F4]'
                      end
                      object EdTipoCobrBB: TdmkEdit
                        Left = 295
                        Top = 5
                        Width = 99
                        Height = 26
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        CharCase = ecUpperCase
                        MaxLength = 5
                        TabOrder = 0
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                        OnChange = EdTipoCobrBBChange
                      end
                      object EdTipoCobrBB_TXT: TdmkEdit
                        Left = 393
                        Top = 5
                        Width = 805
                        Height = 26
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        ReadOnly = True
                        TabOrder = 1
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = 'Registro na Modalidade Simples'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 'Registro na Modalidade Simples'
                        ValWarn = False
                      end
                      object RGIndicatBB: TdmkRadioGroup
                        Left = 5
                        Top = 69
                        Width = 804
                        Height = 109
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Indicativo de mensagem ou Sacador / Avalista:'
                        ItemIndex = 0
                        Items.Strings = (
                          '[  ] - Informa a seguinte mensagem: (40 posi'#231#245'es)'
                          '[A] - Informar o nome e CPF / CNPJ do sacador. ')
                        TabOrder = 2
                        UpdType = utYes
                        OldValor = 0
                      end
                      object EdDdProtesBB: TdmkEdit
                        Left = 295
                        Top = 34
                        Width = 99
                        Height = 26
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Alignment = taRightJustify
                        CharCase = ecUpperCase
                        MaxLength = 5
                        ReadOnly = True
                        TabOrder = 3
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 2
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '00'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 0
                        ValWarn = False
                        OnChange = EdDdProtesBBChange
                      end
                      object EdDdProtesBB_TXT: TdmkEdit
                        Left = 393
                        Top = 34
                        Width = 805
                        Height = 26
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        ReadOnly = True
                        TabOrder = 4
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                      object RGPosicoesBB: TdmkRadioGroup
                        Left = 811
                        Top = 69
                        Width = 387
                        Height = 109
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = ' Posi'#231#245'es: '
                        Columns = 8
                        ItemIndex = 6
                        Items.Strings = (
                          ''
                          ''
                          ''
                          ''
                          ''
                          ''
                          '6'
                          '7')
                        TabOrder = 5
                        OnClick = RGPosicoesBBClick
                        UpdType = utYes
                        OldValor = 0
                      end
                      object EdMsg40posBB: TdmkEdit
                        Left = 330
                        Top = 95
                        Width = 469
                        Height = 26
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        CharCase = ecUpperCase
                        MaxLength = 40
                        TabOrder = 6
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                    end
                  end
                end
              end
            end
            object TabSheet20: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '104'
              ImageIndex = 5
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object RGTipoCobranca: TRadioGroup
                Left = 5
                Top = 12
                Width = 956
                Height = 51
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' Tipo de cobran'#231'a: '
                Columns = 3
                ItemIndex = 0
                Items.Strings = (
                  'TUnMyObjects.ConfiguraTipoCobranca')
                TabOrder = 0
              end
            end
            object TabSheet10: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' 237 '
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object PageControl_237: TPageControl
                Left = 0
                Top = 0
                Width = 1225
                Height = 483
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                ActivePage = TabSheet12
                Align = alClient
                TabOrder = 0
                object TabSheet12: TTabSheet
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = ' CNAB 400 '
                  ImageIndex = 1
                  ExplicitLeft = 0
                  ExplicitTop = 0
                  ExplicitWidth = 0
                  ExplicitHeight = 0
                  object Panel10: TPanel
                    Left = 0
                    Top = 0
                    Width = 1217
                    Height = 452
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object Label37: TLabel
                      Left = 5
                      Top = 10
                      Width = 165
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Mensagem 1 (12 posi'#231#245'es):'
                    end
                    object Label38: TLabel
                      Left = 172
                      Top = 10
                      Width = 165
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Mensagem 2 (60 posi'#231#245'es):'
                    end
                    object Ed_237Mens1: TdmkEdit
                      Left = 5
                      Top = 30
                      Width = 164
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      MaxLength = 12
                      TabOrder = 0
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object Ed_237Mens2: TdmkEdit
                      Left = 172
                      Top = 30
                      Width = 591
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      MaxLength = 60
                      TabOrder = 1
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                  end
                end
              end
            end
            object TabSheet11: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' 341 '
              ImageIndex = 1
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object PageControl_341: TPageControl
                Left = 0
                Top = 0
                Width = 1225
                Height = 483
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                ActivePage = TabSheet3
                Align = alClient
                TabOrder = 0
                object TabSheet3: TTabSheet
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = ' CNAB 400 '
                  ExplicitLeft = 0
                  ExplicitTop = 0
                  ExplicitWidth = 0
                  ExplicitHeight = 0
                  object Panel15: TPanel
                    Left = 0
                    Top = 0
                    Width = 1217
                    Height = 452
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                  end
                end
              end
            end
            object TabSheet16: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '399'
              ImageIndex = 3
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object PageControl2: TPageControl
                Left = 0
                Top = 0
                Width = 1225
                Height = 483
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                ActivePage = TabSheet17
                Align = alClient
                TabOrder = 0
                object TabSheet17: TTabSheet
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = ' CNAB 400 '
                  ExplicitLeft = 0
                  ExplicitTop = 0
                  ExplicitWidth = 0
                  ExplicitHeight = 0
                  object Panel20: TPanel
                    Left = 0
                    Top = 0
                    Width = 1217
                    Height = 452
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                  end
                end
              end
            end
            object TabSheet18: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '756'
              ImageIndex = 4
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object PageControl4: TPageControl
                Left = 0
                Top = 0
                Width = 1225
                Height = 483
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                ActivePage = TabSheet19
                Align = alClient
                TabOrder = 0
                object TabSheet19: TTabSheet
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = ' CNAB 400 '
                  ExplicitLeft = 0
                  ExplicitTop = 0
                  ExplicitWidth = 0
                  ExplicitHeight = 0
                  object Panel25: TPanel
                    Left = 0
                    Top = 0
                    Width = 1217
                    Height = 452
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object Label189: TLabel
                      Left = 111
                      Top = 158
                      Width = 736
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 
                        'VTCBBNITAR '#185': Valor da tarifa de cobran'#231'a banc'#225'ria para bancos q' +
                        'ue n'#227'o informam a tarifa no arquivo retorno (banco 756).'
                    end
                    object Label187: TLabel
                      Left = 5
                      Top = 134
                      Width = 96
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'VTCBBNITAR '#185':'
                    end
                    object Label22: TLabel
                      Left = 432
                      Top = 201
                      Width = 133
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'C'#243'digo de impress'#227'o:'
                    end
                    object dmkRadioGroup1: TdmkRadioGroup
                      Left = 5
                      Top = 12
                      Width = 804
                      Height = 111
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 
                        'Indicativo de mensagem ou Sacador / Avalista: (PREENCHER ESTE ME' +
                        'SMO QUADRO DO BANCO 001 CNAB 400!!!!)'
                      TabOrder = 0
                      UpdType = utYes
                      OldValor = 0
                    end
                    object EdVTCBBNITAR: TdmkEdit
                      Left = 5
                      Top = 154
                      Width = 98
                      Height = 26
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Alignment = taRightJustify
                      TabOrder = 1
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                    end
                    object GroupBox3: TGroupBox
                      Left = 5
                      Top = 187
                      Width = 420
                      Height = 62
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Dados do banco correspondente'
                      TabOrder = 2
                      object Label19: TLabel
                        Left = 9
                        Top = 31
                        Width = 42
                        Height = 16
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Banco:'
                      end
                      object Label20: TLabel
                        Left = 101
                        Top = 31
                        Width = 53
                        Height = 16
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Ag'#234'ncia:'
                      end
                      object Label21: TLabel
                        Left = 219
                        Top = 31
                        Width = 90
                        Height = 16
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Conta corrente:'
                      end
                      object EdCorresBco: TdmkEditCB
                        Left = 58
                        Top = 27
                        Width = 36
                        Height = 26
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Alignment = taRightJustify
                        TabOrder = 0
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 3
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '-2147483647'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '000'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 0
                        ValWarn = False
                        DBLookupComboBox = CBCedBanco
                        IgnoraDBLookupComboBox = False
                        AutoSetIfOnlyOneReg = setregOnlyManual
                      end
                      object EdCorresAge: TdmkEdit
                        Left = 156
                        Top = 27
                        Width = 54
                        Height = 26
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Alignment = taRightJustify
                        TabOrder = 1
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 4
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '0000'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 0
                        ValWarn = False
                      end
                      object EdCorresCto: TdmkEdit
                        Left = 314
                        Top = 27
                        Width = 98
                        Height = 26
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        TabOrder = 2
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                    end
                    object EdNPrinBc: TdmkEdit
                      Left = 432
                      Top = 220
                      Width = 135
                      Height = 26
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      TabOrder = 3
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                  end
                end
              end
            end
          end
        end
        object TabSheet15: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Dados de remessa'
          ImageIndex = 7
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel1: TPanel
            Left = 0
            Top = 0
            Width = 1233
            Height = 514
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label29: TLabel
              Left = 5
              Top = 10
              Width = 295
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Diret'#243'rio no qual ser'#225' salvo o arquivo REMESSA:'
            end
            object Label81: TLabel
              Left = 5
              Top = 59
              Width = 297
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Carteira (Financeiro) (deve pertencer ao cedente):'
            end
            object SpeedButton5: TSpeedButton
              Left = 926
              Top = 30
              Width = 25
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              OnClick = SpeedButton5Click
            end
            object SpeedButton8: TSpeedButton
              Left = 926
              Top = 79
              Width = 25
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              OnClick = SpeedButton8Click
            end
            object LaProtocolCR: TLabel
              Left = 5
              Top = 110
              Width = 61
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Protocolo:'
            end
            object SBProtocolCR: TSpeedButton
              Left = 926
              Top = 129
              Width = 25
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              OnClick = SBProtocolCRClick
            end
            object EdDiretorio: TdmkEdit
              Left = 5
              Top = 30
              Width = 916
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCartEmiss: TdmkEditCB
              Left = 5
              Top = 79
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCartEmiss
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBCartEmiss: TdmkDBLookupComboBox
              Left = 74
              Top = 79
              Width = 847
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsCartRemessa
              TabOrder = 2
              dmkEditCB = EdCartEmiss
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdProtocolCR: TdmkEditCB
              Left = 5
              Top = 129
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBProtocolCR
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBProtocolCR: TdmkDBLookupComboBox
              Left = 74
              Top = 129
              Width = 847
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsProtocolCR
              TabOrder = 4
              dmkEditCB = EdProtocolCR
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
        end
        object TabSheet1: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Dados de retorno'
          ImageIndex = 6
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel16: TPanel
            Left = 0
            Top = 0
            Width = 1233
            Height = 514
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label46: TLabel
              Left = 5
              Top = 10
              Width = 297
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Diret'#243'rio no qual ser'#225' salvo o arquivo RETORNO:'
            end
            object SpeedButton6: TSpeedButton
              Left = 926
              Top = 30
              Width = 25
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              OnClick = SpeedButton6Click
            end
            object Label53: TLabel
              Left = 5
              Top = 59
              Width = 297
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Carteira (Financeiro) (deve pertencer ao cedente):'
            end
            object SpeedButton7: TSpeedButton
              Left = 926
              Top = 79
              Width = 25
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              OnClick = SpeedButton7Click
            end
            object Label1: TLabel
              Left = 5
              Top = 112
              Width = 580
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 
                'Conta padr'#227'o (do plano de contas) para lan'#231'ar as tarifas banc'#225'ri' +
                'as sem conta espec'#237'fica defnida:'
            end
            object SbGenero: TSpeedButton
              Left = 926
              Top = 132
              Width = 25
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              OnClick = SbGeneroClick
            end
            object EdDirRetorno: TdmkEdit
              Left = 5
              Top = 30
              Width = 916
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCartRetorno: TdmkEditCB
              Left = 5
              Top = 79
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCartRetorno
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBCartRetorno: TdmkDBLookupComboBox
              Left = 74
              Top = 79
              Width = 847
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsCartRetorno
              TabOrder = 2
              dmkEditCB = EdCartRetorno
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdCtaRetTar: TdmkEditCB
              Left = 5
              Top = 132
              Width = 69
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'CtaRetTar'
              UpdCampo = 'CtaRetTar'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCtaRetTar
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBCtaRetTar: TdmkDBLookupComboBox
              Left = 74
              Top = 132
              Width = 847
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsContas
              TabOrder = 4
              dmkEditCB = EdCtaRetTar
              QryCampo = 'CtaRetTar'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
        end
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 747
      Width = 1241
      Height = 78
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object Label43: TLabel
        Left = 351
        Top = 7
        Width = 112
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Campo obrigat'#243'rio'
      end
      object Label44: TLabel
        Left = 351
        Top = 25
        Width = 138
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Campo n'#227'o obrigat'#243'rio'
      end
      object Label45: TLabel
        Left = 351
        Top = 58
        Width = 111
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Campo explicativo'
      end
      object Label56: TLabel
        Left = 527
        Top = 25
        Width = 417
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'blq*: Texto Alternativo para impress'#227'o do campo carteira no bloq' +
          'ueto.'
      end
      object Label6: TLabel
        Left = 527
        Top = 50
        Width = 514
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'DAC = D'#237'gito de Auto Confer'#234'ncia :  [A] ag'#234'ncia , [C] conta corr' +
          'ente e [AC] para ambos.'
      end
      object Label90: TLabel
        Left = 351
        Top = 42
        Width = 128
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Campo circunstancial'
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 15
        Top = 21
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel24: TPanel
        Left = 1074
        Top = 18
        Width = 165
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 147
          Height = 50
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtImportar: TBitBtn
        Tag = 19
        Left = 169
        Top = 21
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Hint = 'Inclui novo banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'I&mporta'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtImportarClick
      end
      object Edit1: TdmkEdit
        Left = 330
        Top = 7
        Width = 16
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object Edit2: TdmkEdit
        Left = 330
        Top = 25
        Width = 16
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object Edit3: TdmkEdit
        Left = 330
        Top = 58
        Width = 16
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object Edit4: TdmkEdit
        Left = 330
        Top = 42
        Width = 16
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        ReadOnly = True
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 15
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 266
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 10
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 266
      Top = 0
      Width = 356
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 309
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Configura'#231#245'es CNAB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 309
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Configura'#231#245'es CNAB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 309
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Configura'#231#245'es CNAB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GBAvisos1: TGroupBox
      Left = 622
      Top = 0
      Width = 560
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Caption = ' Avisos: '
      TabOrder = 3
      object Panel21: TPanel
        Left = 2
        Top = 18
        Width = 556
        Height = 44
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 16
          Top = 2
          Width = 233
          Height = 19
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'BANCO N'#195'O IMPLEMENTADO!'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 15
          Top = 1
          Width = 233
          Height = 19
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'BANCO N'#195'O IMPLEMENTADO!'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object PB1: TProgressBar
          Left = 0
          Top = 23
          Width = 556
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          TabOrder = 0
        end
      end
    end
  end
  object DsCNAB_Cfg: TDataSource
    DataSet = QrCNAB_Cfg
    Left = 276
    Top = 440
  end
  object QrCNAB_Cfg: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCNAB_CfgBeforeOpen
    AfterOpen = QrCNAB_CfgAfterOpen
    BeforeClose = QrCNAB_CfgBeforeClose
    AfterScroll = QrCNAB_CfgAfterScroll
    SQL.Strings = (
      'SELECT ban.Nome NOMEBANCO, cfg.* '
      'FROM cnab_cfg cfg'
      'LEFT JOIN bancos ban ON ban.Codigo=cfg.CedBanco'
      'WHERE cfg.Codigo > 0')
    Left = 276
    Top = 392
    object QrCNAB_CfgNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Origin = 'bancos.Nome'
      Size = 100
    end
    object QrCNAB_CfgCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cnab_cfg.Codigo'
    end
    object QrCNAB_CfgNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'cnab_cfg.Nome'
      Size = 50
    end
    object QrCNAB_CfgCedBanco: TIntegerField
      FieldName = 'CedBanco'
      Origin = 'cnab_cfg.CedBanco'
      Required = True
      DisplayFormat = '000'
    end
    object QrCNAB_CfgCedAgencia: TIntegerField
      FieldName = 'CedAgencia'
      Origin = 'cnab_cfg.CedAgencia'
      Required = True
      DisplayFormat = '0000'
    end
    object QrCNAB_CfgCedConta: TWideStringField
      FieldName = 'CedConta'
      Origin = 'cnab_cfg.CedConta'
      Size = 30
    end
    object QrCNAB_CfgCedNome: TWideStringField
      FieldName = 'CedNome'
      Origin = 'cnab_cfg.CedNome'
      Required = True
      Size = 100
    end
    object QrCNAB_CfgTipoCart: TSmallintField
      FieldName = 'TipoCart'
      Origin = 'cnab_cfg.TipoCart'
    end
    object QrCNAB_CfgCartNum: TWideStringField
      FieldName = 'CartNum'
      Origin = 'cnab_cfg.CartNum'
      Size = 3
    end
    object QrCNAB_CfgCartCod: TWideStringField
      FieldName = 'CartCod'
      Origin = 'cnab_cfg.CartCod'
      Size = 1
    end
    object QrCNAB_CfgEspecieTit: TWideStringField
      FieldName = 'EspecieTit'
      Origin = 'cnab_cfg.EspecieTit'
      Size = 2
    end
    object QrCNAB_CfgInstrCobr1: TWideStringField
      FieldName = 'InstrCobr1'
      Origin = 'cnab_cfg.InstrCobr1'
      Size = 2
    end
    object QrCNAB_CfgInstrCobr2: TWideStringField
      FieldName = 'InstrCobr2'
      Origin = 'cnab_cfg.InstrCobr2'
      Size = 2
    end
    object QrCNAB_CfgAceiteTit: TSmallintField
      FieldName = 'AceiteTit'
      Origin = 'cnab_cfg.AceiteTit'
      Required = True
    end
    object QrCNAB_CfgInstrDias: TSmallintField
      FieldName = 'InstrDias'
      Origin = 'cnab_cfg.InstrDias'
      Required = True
    end
    object QrCNAB_CfgCedente: TIntegerField
      FieldName = 'Cedente'
      Origin = 'cnab_cfg.Cedente'
    end
    object QrCNAB_CfgSacadAvali: TIntegerField
      FieldName = 'SacadAvali'
      Origin = 'cnab_cfg.SacadAvali'
      Required = True
    end
    object QrCNAB_CfgSacAvaNome: TWideStringField
      FieldName = 'SacAvaNome'
      Origin = 'cnab_cfg.SacAvaNome'
      Required = True
      Size = 100
    end
    object QrCNAB_CfgCodEmprBco: TWideStringField
      FieldName = 'CodEmprBco'
      Origin = 'cnab_cfg.CodEmprBco'
    end
    object QrCNAB_CfgMultaTipo: TSmallintField
      FieldName = 'MultaTipo'
      Origin = 'cnab_cfg.MultaTipo'
      Required = True
    end
    object QrCNAB_CfgMultaPerc: TFloatField
      FieldName = 'MultaPerc'
      Origin = 'cnab_cfg.MultaPerc'
      Required = True
    end
    object QrCNAB_CfgTexto01: TWideStringField
      FieldName = 'Texto01'
      Origin = 'cnab_cfg.Texto01'
      Size = 100
    end
    object QrCNAB_CfgTexto02: TWideStringField
      FieldName = 'Texto02'
      Origin = 'cnab_cfg.Texto02'
      Size = 100
    end
    object QrCNAB_CfgTexto03: TWideStringField
      FieldName = 'Texto03'
      Origin = 'cnab_cfg.Texto03'
      Size = 100
    end
    object QrCNAB_CfgTexto04: TWideStringField
      FieldName = 'Texto04'
      Origin = 'cnab_cfg.Texto04'
      Size = 100
    end
    object QrCNAB_CfgTexto05: TWideStringField
      FieldName = 'Texto05'
      Origin = 'cnab_cfg.Texto05'
      Size = 100
    end
    object QrCNAB_CfgTexto06: TWideStringField
      FieldName = 'Texto06'
      Origin = 'cnab_cfg.Texto06'
      Size = 100
    end
    object QrCNAB_CfgTexto07: TWideStringField
      FieldName = 'Texto07'
      Origin = 'cnab_cfg.Texto07'
      Size = 100
    end
    object QrCNAB_CfgTexto08: TWideStringField
      FieldName = 'Texto08'
      Origin = 'cnab_cfg.Texto08'
      Size = 100
    end
    object QrCNAB_CfgTexto09: TWideStringField
      FieldName = 'Texto09'
      Origin = 'cnab_cfg.Texto09'
      Size = 100
    end
    object QrCNAB_CfgTexto10: TWideStringField
      FieldName = 'Texto10'
      Origin = 'cnab_cfg.Texto10'
      Size = 100
    end
    object QrCNAB_CfgLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'cnab_cfg.Lk'
    end
    object QrCNAB_CfgDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'cnab_cfg.DataCad'
    end
    object QrCNAB_CfgDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'cnab_cfg.DataAlt'
    end
    object QrCNAB_CfgUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'cnab_cfg.UserCad'
    end
    object QrCNAB_CfgUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'cnab_cfg.UserAlt'
    end
    object QrCNAB_CfgAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'cnab_cfg.AlterWeb'
      Required = True
    end
    object QrCNAB_CfgAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'cnab_cfg.Ativo'
      Required = True
    end
    object QrCNAB_CfgJurosPerc: TFloatField
      FieldName = 'JurosPerc'
      Origin = 'cnab_cfg.JurosPerc'
      Required = True
    end
    object QrCNAB_CfgJurosDias: TSmallintField
      FieldName = 'JurosDias'
      Origin = 'cnab_cfg.JurosDias'
      Required = True
    end
    object QrCNAB_CfgJurosTipo: TSmallintField
      FieldName = 'JurosTipo'
      Origin = 'cnab_cfg.JurosTipo'
      Required = True
    end
    object QrCNAB_CfgCNAB: TIntegerField
      FieldName = 'CNAB'
      Origin = 'cnab_cfg.CNAB'
      Required = True
    end
    object QrCNAB_CfgEnvEmeio: TSmallintField
      FieldName = 'EnvEmeio'
      Origin = 'cnab_cfg.EnvEmeio'
      Required = True
    end
    object QrCNAB_CfgCedDAC_A: TWideStringField
      FieldName = 'CedDAC_A'
      Origin = 'cnab_cfg.CedDAC_A'
      Size = 1
    end
    object QrCNAB_CfgCedDAC_C: TWideStringField
      FieldName = 'CedDAC_C'
      Origin = 'cnab_cfg.CedDAC_C'
      Size = 1
    end
    object QrCNAB_CfgCedDAC_AC: TWideStringField
      FieldName = 'CedDAC_AC'
      Origin = 'cnab_cfg.CedDAC_AC'
      Size = 1
    end
    object QrCNAB_CfgDiretorio: TWideStringField
      FieldName = 'Diretorio'
      Origin = 'cnab_cfg.Diretorio'
      Size = 255
    end
    object QrCNAB_Cfg_237Mens1: TWideStringField
      FieldName = '_237Mens1'
      Origin = 'cnab_cfg._237Mens1'
      Size = 12
    end
    object QrCNAB_Cfg_237Mens2: TWideStringField
      FieldName = '_237Mens2'
      Origin = 'cnab_cfg._237Mens2'
      Size = 60
    end
    object QrCNAB_CfgCodOculto: TWideStringField
      FieldName = 'CodOculto'
      Origin = 'cnab_cfg.CodOculto'
    end
    object QrCNAB_CfgSeqArq: TIntegerField
      FieldName = 'SeqArq'
      Origin = 'cnab_cfg.SeqArq'
    end
    object QrCNAB_CfgCedPosto: TIntegerField
      FieldName = 'CedPosto'
      Origin = 'cnab_cfg.CedPosto'
    end
    object QrCNAB_CfgLastNosNum: TLargeintField
      FieldName = 'LastNosNum'
      Origin = 'cnab_cfg.LastNosNum'
    end
    object QrCNAB_CfgLocalPag: TWideStringField
      FieldName = 'LocalPag'
      Origin = 'cnab_cfg.LocalPag'
      Size = 127
    end
    object QrCNAB_CfgEspecieVal: TWideStringField
      FieldName = 'EspecieVal'
      Origin = 'cnab_cfg.EspecieVal'
      Size = 5
    end
    object QrCNAB_CfgOperCodi: TWideStringField
      FieldName = 'OperCodi'
      Origin = 'cnab_cfg.OperCodi'
      Size = 3
    end
    object QrCNAB_CfgIDCobranca: TWideStringField
      FieldName = 'IDCobranca'
      Origin = 'cnab_cfg.IDCobranca'
      Size = 2
    end
    object QrCNAB_CfgAgContaCed: TWideStringField
      FieldName = 'AgContaCed'
      Origin = 'cnab_cfg.AgContaCed'
      Size = 40
    end
    object QrCNAB_CfgDirRetorno: TWideStringField
      FieldName = 'DirRetorno'
      Origin = 'cnab_cfg.DirRetorno'
      Size = 255
    end
    object QrCNAB_CfgCartRetorno: TIntegerField
      FieldName = 'CartRetorno'
      Origin = 'cnab_cfg.CartRetorno'
    end
    object QrCNAB_CfgPosicoesBB: TSmallintField
      FieldName = 'PosicoesBB'
      Origin = 'cnab_cfg.PosicoesBB'
    end
    object QrCNAB_CfgIndicatBB: TWideStringField
      FieldName = 'IndicatBB'
      Origin = 'cnab_cfg.IndicatBB'
      Size = 1
    end
    object QrCNAB_CfgTipoCobrBB: TWideStringField
      FieldName = 'TipoCobrBB'
      Origin = 'cnab_cfg.TipoCobrBB'
      Size = 5
    end
    object QrCNAB_CfgComando: TIntegerField
      FieldName = 'Comando'
      Origin = 'cnab_cfg.Comando'
    end
    object QrCNAB_CfgDdProtesBB: TIntegerField
      FieldName = 'DdProtesBB'
      Origin = 'cnab_cfg.DdProtesBB'
    end
    object QrCNAB_CfgMsg40posBB: TWideStringField
      FieldName = 'Msg40posBB'
      Origin = 'cnab_cfg.Msg40posBB'
      Size = 40
    end
    object QrCNAB_CfgQuemDistrb: TWideStringField
      FieldName = 'QuemDistrb'
      Origin = 'cnab_cfg.QuemDistrb'
      Size = 1
    end
    object QrCNAB_CfgQuemPrint: TWideStringField
      FieldName = 'QuemPrint'
      Origin = 'cnab_cfg.QuemPrint'
      Size = 1
    end
    object QrCNAB_CfgDesco1Cod: TSmallintField
      FieldName = 'Desco1Cod'
      Origin = 'cnab_cfg.Desco1Cod'
    end
    object QrCNAB_CfgDesco1Dds: TIntegerField
      FieldName = 'Desco1Dds'
      Origin = 'cnab_cfg.Desco1Dds'
    end
    object QrCNAB_CfgDesco1Fat: TFloatField
      FieldName = 'Desco1Fat'
      Origin = 'cnab_cfg.Desco1Fat'
    end
    object QrCNAB_CfgDesco2Cod: TSmallintField
      FieldName = 'Desco2Cod'
      Origin = 'cnab_cfg.Desco2Cod'
    end
    object QrCNAB_CfgDesco2Dds: TIntegerField
      FieldName = 'Desco2Dds'
      Origin = 'cnab_cfg.Desco2Dds'
    end
    object QrCNAB_CfgDesco2Fat: TFloatField
      FieldName = 'Desco2Fat'
      Origin = 'cnab_cfg.Desco2Fat'
    end
    object QrCNAB_CfgDesco3Cod: TSmallintField
      FieldName = 'Desco3Cod'
      Origin = 'cnab_cfg.Desco3Cod'
    end
    object QrCNAB_CfgDesco3Dds: TIntegerField
      FieldName = 'Desco3Dds'
      Origin = 'cnab_cfg.Desco3Dds'
    end
    object QrCNAB_CfgDesco3Fat: TFloatField
      FieldName = 'Desco3Fat'
      Origin = 'cnab_cfg.Desco3Fat'
    end
    object QrCNAB_CfgProtesCod: TSmallintField
      FieldName = 'ProtesCod'
      Origin = 'cnab_cfg.ProtesCod'
    end
    object QrCNAB_CfgProtesDds: TSmallintField
      FieldName = 'ProtesDds'
      Origin = 'cnab_cfg.ProtesDds'
    end
    object QrCNAB_CfgBxaDevCod: TSmallintField
      FieldName = 'BxaDevCod'
      Origin = 'cnab_cfg.BxaDevCod'
    end
    object QrCNAB_CfgBxaDevDds: TIntegerField
      FieldName = 'BxaDevDds'
      Origin = 'cnab_cfg.BxaDevDds'
    end
    object QrCNAB_CfgMoedaCod: TWideStringField
      FieldName = 'MoedaCod'
      Origin = 'cnab_cfg.MoedaCod'
    end
    object QrCNAB_CfgContrato: TFloatField
      FieldName = 'Contrato'
      Origin = 'cnab_cfg.Contrato'
    end
    object QrCNAB_CfgMultaDias: TSmallintField
      FieldName = 'MultaDias'
      Origin = 'cnab_cfg.MultaDias'
    end
    object QrCNAB_CfgConvCartCobr: TWideStringField
      FieldName = 'ConvCartCobr'
      Origin = 'cnab_cfg.ConvCartCobr'
      Size = 2
    end
    object QrCNAB_CfgConvVariCart: TWideStringField
      FieldName = 'ConvVariCart'
      Origin = 'cnab_cfg.ConvVariCart'
      Size = 3
    end
    object QrCNAB_CfgVariacao: TIntegerField
      FieldName = 'Variacao'
      Origin = 'cnab_cfg.Variacao'
    end
    object QrCNAB_CfgCodLidrBco: TWideStringField
      FieldName = 'CodLidrBco'
      Origin = 'cnab_cfg.CodLidrBco'
    end
    object QrCNAB_CfgEspecieDoc: TWideStringField
      FieldName = 'EspecieDoc'
      Origin = 'cnab_cfg.EspecieDoc'
    end
    object QrCNAB_CfgInfNossoNum: TSmallintField
      FieldName = 'InfNossoNum'
      Origin = 'cnab_cfg.InfNossoNum'
    end
    object QrCNAB_CfgCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
      Origin = 'cnab_cfg.CartEmiss'
    end
    object QrCNAB_CfgTipoCobranca: TIntegerField
      FieldName = 'TipoCobranca'
      Origin = 'cnab_cfg.TipoCobranca'
    end
    object QrCNAB_CfgCartTxt: TWideStringField
      FieldName = 'CartTxt'
      Origin = 'cnab_cfg.CartTxt'
      Size = 10
    end
    object QrCNAB_CfgEspecieTxt: TWideStringField
      FieldName = 'EspecieTxt'
      Origin = 'cnab_cfg.EspecieTxt'
    end
    object QrCNAB_CfgLayoutRem: TWideStringField
      FieldName = 'LayoutRem'
      Origin = 'cnab_cfg.LayoutRem'
      Size = 50
    end
    object QrCNAB_CfgLayoutRet: TWideStringField
      FieldName = 'LayoutRet'
      Origin = 'cnab_cfg.LayoutRet'
      Size = 50
    end
    object QrCNAB_CfgNaoRecebDd: TSmallintField
      FieldName = 'NaoRecebDd'
      Origin = 'cnab_cfg.NaoRecebDd'
    end
    object QrCNAB_CfgTipBloqUso: TWideStringField
      FieldName = 'TipBloqUso'
      Origin = 'cnab_cfg.TipBloqUso'
      Size = 1
    end
    object QrCNAB_CfgConcatCod: TWideStringField
      FieldName = 'ConcatCod'
      Origin = 'cnab_cfg.ConcatCod'
      Size = 50
    end
    object QrCNAB_CfgComplmCod: TWideStringField
      FieldName = 'ComplmCod'
      Origin = 'cnab_cfg.ComplmCod'
      Size = 10
    end
    object QrCNAB_CfgNumVersaoI3: TIntegerField
      FieldName = 'NumVersaoI3'
    end
    object QrCNAB_CfgModalCobr: TSmallintField
      FieldName = 'ModalCobr'
    end
    object QrCNAB_CfgCtaCooper: TWideStringField
      FieldName = 'CtaCooper'
      Size = 7
    end
    object QrCNAB_CfgNosNumFxaI: TIntegerField
      FieldName = 'NosNumFxaI'
    end
    object QrCNAB_CfgNosNumFxaF: TIntegerField
      FieldName = 'NosNumFxaF'
    end
    object QrCNAB_CfgNosNumFxaU: TSmallintField
      FieldName = 'NosNumFxaU'
    end
    object QrCNAB_CfgCtrGarantiaNr: TWideStringField
      FieldName = 'CtrGarantiaNr'
      Size = 30
    end
    object QrCNAB_CfgCtrGarantiaDV: TWideStringField
      FieldName = 'CtrGarantiaDV'
      Size = 1
    end
    object QrCNAB_CfgVTCBBNITAR: TFloatField
      FieldName = 'VTCBBNITAR'
    end
    object QrCNAB_CfgProtocolCR: TIntegerField
      FieldName = 'ProtocolCR'
    end
    object QrCNAB_CfgCtaRetTar: TIntegerField
      FieldName = 'CtaRetTar'
    end
    object QrCNAB_CfgCorreio: TIntegerField
      FieldName = 'Correio'
    end
    object QrCNAB_CfgTermoAceite: TIntegerField
      FieldName = 'TermoAceite'
    end
    object QrCNAB_CfgDataHoraAceite: TDateTimeField
      FieldName = 'DataHoraAceite'
    end
    object QrCNAB_CfgUsuarioAceite: TIntegerField
      FieldName = 'UsuarioAceite'
    end
    object QrCNAB_CfgStatus_TXT: TWideStringField
      FieldName = 'Status_TXT'
      Size = 15
    end
    object QrCNAB_CfgUsuarioAceite_TXT: TWideStringField
      FieldName = 'UsuarioAceite_TXT'
      Size = 30
    end
    object QrCNAB_CfgDataHoraAceite_TXT: TWideStringField
      FieldName = 'DataHoraAceite_TXT'
    end
    object QrCNAB_CfgCorresBco: TIntegerField
      FieldName = 'CorresBco'
    end
    object QrCNAB_CfgCorresCto: TWideStringField
      FieldName = 'CorresCto'
      Size = 30
    end
    object QrCNAB_CfgCorresAge: TIntegerField
      FieldName = 'CorresAge'
    end
    object QrCNAB_CfgNPrinBc: TWideStringField
      FieldName = 'NPrinBc'
      Size = 30
    end
  end
  object QrBancos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM bancos'
      'ORDER BY Nome')
    Left = 80
    Top = 660
    object QrBancosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsBancos: TDataSource
    DataSet = QrBancos
    Left = 108
    Top = 660
  end
  object QrCedente: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 136
    Top = 660
    object QrCedenteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCedenteNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsCedente: TDataSource
    DataSet = QrCedente
    Left = 164
    Top = 660
  end
  object OpenDialog1: TOpenDialog
    Left = 248
    Top = 660
  end
  object QrSacador: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENT,'
      'IF(Tipo=0, CNPJ, CPF) DOCENT'
      'FROM entidades'
      'ORDER BY NOMEENT')
    Left = 192
    Top = 660
    object QrSacadorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSacadorNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrSacadorDOCENT: TWideStringField
      FieldName = 'DOCENT'
      Size = 18
    end
  end
  object DsSacador: TDataSource
    DataSet = QrSacador
    Left = 220
    Top = 660
  end
  object QrCartRetorno: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM carteiras'
      'WHERE ForneceI=:P0')
    Left = 276
    Top = 660
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCartRetornoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCartRetornoNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsCartRetorno: TDataSource
    DataSet = QrCartRetorno
    Left = 304
    Top = 660
  end
  object QrCartRemessa: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM carteiras'
      'WHERE Tipo=2'
      'AND ForneceI=:P0'
      'ORDER BY Nome')
    Left = 332
    Top = 660
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsCartRemessa: TDataSource
    DataSet = QrCartRemessa
    Left = 360
    Top = 660
  end
  object QrProtocolCR: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM protocolos'
      'WHERE Ativo = 1'
      'AND Tipo = 4'
      'AND Def_Client=:P0'
      'ORDER BY Nome')
    Left = 388
    Top = 660
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProtocolCRCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProtocolCRNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsProtocolCR: TDataSource
    DataSet = QrProtocolCR
    Left = 416
    Top = 660
  end
  object QrCNAB_CfOR: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT oco.*, cta.Nome NOMEGENERO '
      'FROM cnab_cfor oco '
      'LEFT JOIN contas cta ON cta.Codigo=oco.Genero'
      'WHERE oco.Codigo>0')
    Left = 368
    Top = 392
    object QrCNAB_CfORCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCNAB_CfORControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCNAB_CfORNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrCNAB_CfOROcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
    end
    object QrCNAB_CfORGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrCNAB_CfORLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCNAB_CfORDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCNAB_CfORDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCNAB_CfORUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCNAB_CfORUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCNAB_CfORAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCNAB_CfORAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCNAB_CfORNOMEGENERO: TWideStringField
      FieldName = 'NOMEGENERO'
      Size = 50
    end
  end
  object DsCNAB_CfOR: TDataSource
    DataSet = QrCNAB_CfOR
    Left = 368
    Top = 440
  end
  object PMCNAB_Cfg: TPopupMenu
    OnPopup = PMCNAB_CfgPopup
    Left = 512
    Top = 616
    object IncluiConfiguracaoCNAB1: TMenuItem
      Caption = '&Inclui nova configura'#231#227'o CNAB'
      OnClick = IncluiConfiguracaoCNAB1Click
    end
    object AlteraConfiguracaoCNAB1: TMenuItem
      Caption = '&Altera configura'#231#227'o CNAB atual'
      OnClick = AlteraConfiguracaoCNAB1Click
    end
    object ExcluiConfiguracaoCNAB1: TMenuItem
      Caption = '&Exclui  configura'#231#227'o CNAB atual'
      Enabled = False
      OnClick = ExcluiConfiguracaoCNAB1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Alterastatusdaconfiguraoatual1: TMenuItem
      Caption = '&Altera status da configura'#231#227'o atual'
      OnClick = Alterastatusdaconfiguraoatual1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object DuplicaConfiguracaoCNAB1: TMenuItem
      Caption = '&Duplica  configura'#231#227'o CNAB atual'
      OnClick = DuplicaConfiguracaoCNAB1Click
    end
  end
  object PMCNAB_CfOR: TPopupMenu
    OnPopup = PMCNAB_CfORPopup
    Left = 996
    Top = 492
    object IncluiTarifaRetornoCNAB1: TMenuItem
      Caption = '&Inclui tarifa de retorno CNAB'
      OnClick = IncluiTarifaRetornoCNAB1Click
    end
    object AlteraTarifaRetornoCNAB1: TMenuItem
      Caption = '&Altera tarifa de retorno CNAB'
      OnClick = AlteraTarifaRetornoCNAB1Click
    end
    object ExcluiTarifaRetornoCNAB1: TMenuItem
      Caption = '&Exclui tarifa de retorno CNAB'
      OnClick = ExcluiTarifaRetornoCNAB1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object CriaTarifasConhecidasDoLayout1: TMenuItem
      Caption = '&Pr'#233'-cria conhecidos do Layout'
      OnClick = CriaTarifasConhecidasDoLayout1Click
    end
  end
  object QrContas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM contas'
      'WHERE Codigo>0'
      'ORDER BY Nome')
    Left = 432
    Top = 392
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 432
    Top = 440
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = Timer1Timer
    Left = 880
    Top = 400
  end
end
