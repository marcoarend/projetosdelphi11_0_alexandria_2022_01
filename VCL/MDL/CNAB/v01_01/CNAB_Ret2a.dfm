object FmCNAB_Ret2a: TFmCNAB_Ret2a
  Left = 352
  Top = 160
  Caption = 'RET-CNABx-001 :: Retorno de Arquivos CNAB'
  ClientHeight = 531
  ClientWidth = 993
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCarrega: TPanel
    Left = 0
    Top = 90
    Width = 993
    Height = 372
    Align = alClient
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 0
    ExplicitWidth = 1245
    ExplicitHeight = 515
    object PnArquivos: TPanel
      Left = 0
      Top = 17
      Width = 993
      Height = 402
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitTop = 16
      ExplicitWidth = 1245
      object Splitter3: TSplitter
        Left = 0
        Top = 273
        Width = 993
        Height = 5
        Cursor = crVSplit
        Align = alTop
        ExplicitWidth = 1245
      end
      object ListBox1: TListBox
        Left = 193
        Top = 51
        Width = 446
        Height = 29
        ItemHeight = 10
        TabOrder = 0
        Visible = False
      end
      object MemoTam: TMemo
        Left = 0
        Top = 364
        Width = 993
        Height = 38
        Align = alBottom
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Lucida Console'
        Font.Style = []
        Lines.Strings = (
          'MemoTam')
        ParentFont = False
        TabOrder = 1
        Visible = False
        ExplicitWidth = 1245
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 993
        Height = 273
        Align = alTop
        BevelOuter = bvNone
        Caption = 'Panel1'
        TabOrder = 2
        ExplicitWidth = 1245
        object Splitter1: TSplitter
          Left = 0
          Top = 142
          Width = 1245
          Height = 6
          Cursor = crVSplit
          Align = alTop
        end
        object GradeA: TStringGrid
          Left = 0
          Top = 148
          Width = 1245
          Height = 125
          Align = alClient
          ColCount = 12
          DefaultColWidth = 26
          DefaultRowHeight = 14
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
          TabOrder = 0
          OnDrawCell = GradeADrawCell
          OnSelectCell = GradeASelectCell
          ColWidths = (
            26
            128
            218
            90
            83
            35
            29
            22
            51
            32
            22
            29)
          RowHeights = (
            14
            14)
        end
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 1245
          Height = 25
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object EdCliInt: TdmkEdit
            Left = 14
            Top = 2
            Width = 55
            Height = 20
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdNomeBancoChange
          end
          object EdNomeEnt: TdmkEdit
            Left = 69
            Top = 2
            Width = 237
            Height = 20
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdNomeBancoChange
          end
          object EdNOMECARTEIRA: TdmkEdit
            Left = 334
            Top = 2
            Width = 209
            Height = 20
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdNomeBancoChange
          end
          object EdCodigo: TdmkEdit
            Left = 306
            Top = 2
            Width = 28
            Height = 20
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdNomeBancoChange
          end
          object EdBanco1: TdmkEdit
            Left = 545
            Top = 2
            Width = 27
            Height = 20
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdNomeBancoChange
          end
          object EdNome: TdmkEdit
            Left = 572
            Top = 2
            Width = 338
            Height = 20
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdNomeBancoChange
          end
          object EdNomeBanco: TdmkEdit
            Left = 912
            Top = 2
            Width = 193
            Height = 20
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdNomeBancoChange
          end
        end
        object dmkDBGrid2: TdmkDBGrid
          Left = 0
          Top = 25
          Width = 1245
          Height = 117
          Align = alTop
          Columns = <
            item
              Expanded = False
              FieldName = 'CliInt'
              Title.Caption = 'Cli. interno'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEENT'
              Title.Caption = 'Nome entidade'
              Width = 192
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Dir'
              Width = 22
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECARTEIRA'
              Title.Caption = 'Carteira'
              Width = 170
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Banco1'
              Title.Caption = 'Bco'
              Width = 22
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Diret'#243'rio dos arquivos retorno'
              Width = 275
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEBANCO'
              Title.Caption = 'Nome do banco'
              Width = 157
              Visible = True
            end>
          Color = clWindow
          DataSource = DsCNAB_Dir
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'CliInt'
              Title.Caption = 'Cli. interno'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEENT'
              Title.Caption = 'Nome entidade'
              Width = 192
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Dir'
              Width = 22
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECARTEIRA'
              Title.Caption = 'Carteira'
              Width = 170
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Banco1'
              Title.Caption = 'Bco'
              Width = 22
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Diret'#243'rio dos arquivos retorno'
              Width = 275
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEBANCO'
              Title.Caption = 'Nome do banco'
              Width = 157
              Visible = True
            end>
        end
      end
      object PageControl1: TPageControl
        Left = 0
        Top = 278
        Width = 993
        Height = 86
        ActivePage = TabSheet2
        Align = alClient
        TabOrder = 3
        ExplicitWidth = 1245
        object TabSheet3: TTabSheet
          Caption = 'Itens de todos arquivos lidos'
          ImageIndex = 2
          object Grade1: TStringGrid
            Left = 0
            Top = 0
            Width = 1237
            Height = 59
            Align = alClient
            ColCount = 30
            DefaultColWidth = 26
            DefaultRowHeight = 14
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
            TabOrder = 0
            OnDrawCell = Grade1DrawCell
            OnSelectCell = Grade1SelectCell
            ColWidths = (
              26
              26
              26
              26
              26
              26
              26
              26
              26
              26
              26
              26
              26
              26
              26
              26
              26
              26
              26
              26
              26
              26
              26
              26
              26
              26
              26
              26
              26
              26)
          end
        end
        object TabSheet1: TTabSheet
          Caption = 'Avisos'
          ImageIndex = 1
          object Memo1: TMemo
            Left = 0
            Top = 0
            Width = 1237
            Height = 59
            Align = alClient
            TabOrder = 0
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Opera'#231#245'es'
          ImageIndex = 2
          object Memo2: TMemo
            Left = 0
            Top = 0
            Width = 1237
            Height = 59
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
        end
      end
    end
    object StaticText1: TStaticText
      Left = 0
      Top = 0
      Width = 993
      Height = 17
      Align = alTop
      Alignment = taCenter
      Caption = 
        '  Antes de carregar verifique se todas ocorr'#234'ncias j'#225' est'#227'o cada' +
        'stradas nos respectivos bancos (Cadastro de bancos).'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      ExplicitWidth = 677
    end
  end
  object PnMovimento: TPanel
    Left = 222
    Top = 90
    Width = 1245
    Height = 508
    BevelOuter = bvNone
    Color = clAppWorkSpace
    TabOrder = 1
    Visible = False
    OnClick = PnMovimentoClick
    object Panel3: TPanel
      Left = 0
      Top = 461
      Width = 1245
      Height = 47
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 462
      object Label2: TLabel
        Left = 193
        Top = 13
        Width = 137
        Height = 13
        Caption = 'Positivo (azul): pagou a mais.'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object Label3: TLabel
        Left = 193
        Top = 26
        Width = 177
        Height = 13
        Caption = 'Negativo (vermelho): pagou a menos.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Panel4: TPanel
        Left = 1134
        Top = 1
        Width = 109
        Height = 45
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 5
        object BitBtn2: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 88
          Height = 39
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sair'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object Button2: TButton
        Left = 334
        Top = 4
        Width = 74
        Height = 25
        Caption = 'Button1'
        TabOrder = 6
        Visible = False
        OnClick = Button1Click
      end
      object BtExclui: TBitBtn
        Tag = 10039
        Left = 506
        Top = 4
        Width = 88
        Height = 39
        Cursor = crHandPoint
        Caption = '&Bloqueto'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtExcluiClick
      end
      object BtConcilia: TBitBtn
        Tag = 10011
        Left = 9
        Top = 4
        Width = 89
        Height = 39
        Caption = '&Conciliar'
        Enabled = False
        TabOrder = 0
        OnClick = BtConciliaClick
      end
      object BitBtn1: TBitBtn
        Tag = 10013
        Left = 101
        Top = 4
        Width = 88
        Height = 39
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Voltar'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BitBtn1Click
      end
      object BtAgenda: TBitBtn
        Tag = 10032
        Left = 415
        Top = 4
        Width = 89
        Height = 39
        Cursor = crHandPoint
        Caption = '&Agenda'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtAgendaClick
      end
      object BtItens: TBitBtn
        Tag = 10038
        Left = 597
        Top = 4
        Width = 89
        Height = 39
        Cursor = crHandPoint
        Caption = '&Lan'#231'to'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = BtItensClick
      end
    end
    object PnCarregados: TPanel
      Left = 0
      Top = 0
      Width = 1245
      Height = 328
      Align = alTop
      TabOrder = 1
      object Splitter2: TSplitter
        Left = 1
        Top = 158
        Width = 1243
        Height = 4
        Cursor = crVSplit
        Align = alBottom
      end
      object Panel5: TPanel
        Left = 1
        Top = 162
        Width = 1243
        Height = 165
        Align = alBottom
        Caption = 'Panel5'
        TabOrder = 0
        object PageControl4: TPageControl
          Left = 1
          Top = 1
          Width = 1241
          Height = 163
          ActivePage = TabSheet6
          Align = alClient
          TabOrder = 0
          object TabSheet6: TTabSheet
            Caption = ' Sem agrupamento'
            object DBGrid2: TDBGrid
              Left = 0
              Top = 0
              Width = 1233
              Height = 136
              Align = alClient
              DataSource = DsLeiItens
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 45
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEPROPRIET'
                  Title.Caption = 'Cond'#244'mino'
                  Width = 186
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'UH'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Mez_TXT'
                  Title.Caption = 'Mes'
                  Width = 29
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Credito'
                  Title.Caption = 'Cr'#233'dito'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Descricao'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 173
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Vencimento'
                  Title.Caption = 'Vencimen.'
                  Width = 45
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Carteira'
                  Width = 38
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECART'
                  Title.Caption = 'Descri'#231#227'o carteira'
                  Width = 192
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CartDest'
                  Title.Caption = 'Cart. destino'
                  Width = 62
                  Visible = True
                end>
            end
          end
          object TabSheet7: TTabSheet
            Caption = ' Com agrupamento '
            ImageIndex = 1
            object DBGrid3: TDBGrid
              Left = 0
              Top = 0
              Width = 1233
              Height = 136
              Align = alClient
              DataSource = DsLeiAgr
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 45
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEPROPRIET'
                  Title.Caption = 'Cond'#244'mino'
                  Width = 186
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'UH'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Mez_TXT'
                  Title.Caption = 'M'#234's'
                  Width = 29
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME_TIPO_BLOQ'
                  Title.Caption = 'Tipo de bloqueto'
                  Width = 144
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Valor'
                  Title.Caption = 'Cr'#233'dito'
                  Width = 59
                  Visible = True
                end>
            end
          end
          object TabSheet8: TTabSheet
            Caption = 'Tempos'
            ImageIndex = 2
            object Memo3: TMemo
              Left = 0
              Top = 0
              Width = 1233
              Height = 136
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
          end
        end
      end
      object PageControl2: TPageControl
        Left = 1
        Top = 1
        Width = 1243
        Height = 157
        ActivePage = TabSheet4
        Align = alClient
        TabOrder = 1
        object TabSheet4: TTabSheet
          Caption = 'Est'#225'vel'
          object DBGLei: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 1234
            Height = 86
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'DJM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Title.Caption = 'DJM*'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEENT'
                Title.Caption = 'Cliente (Entidade)'
                Width = 181
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'JURIDICO_TXT'
                Title.Caption = 'Status jur'#237'dico'
                Width = 96
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IDNum'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Banco'
                Title.Caption = 'Bco'
                Width = 19
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrCodi'
                Title.Caption = 'OC'
                Width = 19
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrData'
                Title.Caption = 'Data ocor.'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValOrig'
                Title.Caption = 'Val. Original'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValTitul'
                Title.Caption = 'Val.Titulo'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValPago'
                Title.Caption = 'Val.Pago'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuros'
                Title.Caption = 'Val.Juros'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValMulta'
                Title.Caption = 'Val.Multa'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuMul'
                Title.Caption = 'Mul.+Jur.'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuitaData'
                Title.Caption = 'Data quit.'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DTA_TARIF_TXT'
                Title.Caption = 'Dt.d'#233'b.tar.'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValAbati'
                Title.Caption = 'Val.Abatim.'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NossoNum'
                Title.Caption = 'Nosso n'#250'm.'
                Width = 115
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SeuNum'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValDesco'
                Title.Caption = 'Val.Desc.'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DevJuros'
                Title.Caption = 'Dev.Juros'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DevMulta'
                Title.Caption = 'Dev.Multa'
                Width = 59
                Visible = True
              end>
            Color = clWindow
            DataSource = DsLei
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDrawColumnCell = DBGLeiDrawColumnCell
            Columns = <
              item
                Expanded = False
                FieldName = 'DJM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Title.Caption = 'DJM*'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEENT'
                Title.Caption = 'Cliente (Entidade)'
                Width = 181
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'JURIDICO_TXT'
                Title.Caption = 'Status jur'#237'dico'
                Width = 96
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IDNum'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Banco'
                Title.Caption = 'Bco'
                Width = 19
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrCodi'
                Title.Caption = 'OC'
                Width = 19
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrData'
                Title.Caption = 'Data ocor.'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValOrig'
                Title.Caption = 'Val. Original'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValTitul'
                Title.Caption = 'Val.Titulo'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValPago'
                Title.Caption = 'Val.Pago'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuros'
                Title.Caption = 'Val.Juros'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValMulta'
                Title.Caption = 'Val.Multa'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuMul'
                Title.Caption = 'Mul.+Jur.'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuitaData'
                Title.Caption = 'Data quit.'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DTA_TARIF_TXT'
                Title.Caption = 'Dt.d'#233'b.tar.'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValAbati'
                Title.Caption = 'Val.Abatim.'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NossoNum'
                Title.Caption = 'Nosso n'#250'm.'
                Width = 115
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SeuNum'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValDesco'
                Title.Caption = 'Val.Desc.'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DevJuros'
                Title.Caption = 'Dev.Juros'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DevMulta'
                Title.Caption = 'Dev.Multa'
                Width = 59
                Visible = True
              end>
          end
          object Panel6: TPanel
            Left = 0
            Top = 86
            Width = 1234
            Height = 44
            Align = alBottom
            TabOrder = 1
            object Label4: TLabel
              Left = 4
              Top = 4
              Width = 71
              Height = 13
              Caption = 'Valor bloqueto:'
              FocusControl = DBEdit1
            end
            object Label5: TLabel
              Left = 138
              Top = 4
              Width = 88
              Height = 13
              Caption = 'Qtd. arrecada'#231#245'es'
              FocusControl = DBEdit2
            end
            object Label6: TLabel
              Left = 232
              Top = 4
              Width = 74
              Height = 13
              Caption = 'Qtd. consumos:'
              FocusControl = DBEdit3
            end
            object Label7: TLabel
              Left = 327
              Top = 4
              Width = 61
              Height = 13
              Caption = 'Qtd reparcel.'
              FocusControl = DBEdit4
            end
            object DBEdit1: TDBEdit
              Left = 4
              Top = 20
              Width = 132
              Height = 21
              DataField = 'Credito'
              DataSource = DataSource1
              TabOrder = 0
            end
            object DBEdit2: TDBEdit
              Left = 138
              Top = 20
              Width = 91
              Height = 21
              DataField = 'ID600'
              DataSource = DataSource1
              TabOrder = 1
            end
            object DBEdit3: TDBEdit
              Left = 232
              Top = 20
              Width = 91
              Height = 21
              DataField = 'ID601'
              DataSource = DataSource1
              TabOrder = 2
            end
            object DBEdit4: TDBEdit
              Left = 327
              Top = 20
              Width = 91
              Height = 21
              DataField = 'ID610'
              DataSource = DataSource1
              TabOrder = 3
            end
          end
        end
        object TabSheet5: TTabSheet
          Caption = 'Desenvolvimento'
          ImageIndex = 1
          object dmkDBGrid1: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 1234
            Height = 130
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'DJM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Title.Caption = 'DJM*'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEENT'
                Title.Caption = 'Cliente (Entidade)'
                Width = 181
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IDNum'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Banco'
                Title.Caption = 'Bco'
                Width = 19
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrCodi'
                Title.Caption = 'OC'
                Width = 19
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrData'
                Title.Caption = 'Data ocor.'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VALBOLETO'
                Title.Caption = 'Val.bloq.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValTitul'
                Title.Caption = 'Val.Titulo'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValPago'
                Title.Caption = 'Val.Pago'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuros'
                Title.Caption = 'Val.Juros'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValMulta'
                Title.Caption = 'Val.Multa'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuMul'
                Title.Caption = 'Mul.+Jur.'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuitaData'
                Title.Caption = 'Data quit.'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DTA_TARIF_TXT'
                Title.Caption = 'Dt.d'#233'b.tar.'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValAbati'
                Title.Caption = 'Val.Abatim.'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NossoNum'
                Title.Caption = 'Nosso n'#250'm.'
                Width = 115
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SeuNum'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValDesco'
                Title.Caption = 'Val.Desc.'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DevJuros'
                Title.Caption = 'Dev.Juros'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DevMulta'
                Title.Caption = 'Dev.Multa'
                Width = 59
                Visible = True
              end>
            Color = clWindow
            DataSource = DsLei
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'DJM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Title.Caption = 'DJM*'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEENT'
                Title.Caption = 'Cliente (Entidade)'
                Width = 181
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IDNum'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Banco'
                Title.Caption = 'Bco'
                Width = 19
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrCodi'
                Title.Caption = 'OC'
                Width = 19
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OcorrData'
                Title.Caption = 'Data ocor.'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'VALBOLETO'
                Title.Caption = 'Val.bloq.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValTitul'
                Title.Caption = 'Val.Titulo'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValPago'
                Title.Caption = 'Val.Pago'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuros'
                Title.Caption = 'Val.Juros'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValMulta'
                Title.Caption = 'Val.Multa'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValJuMul'
                Title.Caption = 'Mul.+Jur.'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuitaData'
                Title.Caption = 'Data quit.'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DTA_TARIF_TXT'
                Title.Caption = 'Dt.d'#233'b.tar.'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValAbati'
                Title.Caption = 'Val.Abatim.'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NossoNum'
                Title.Caption = 'Nosso n'#250'm.'
                Width = 115
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SeuNum'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValDesco'
                Title.Caption = 'Val.Desc.'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'C'#243'digo'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DevJuros'
                Title.Caption = 'Dev.Juros'
                Width = 59
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DevMulta'
                Title.Caption = 'Dev.Multa'
                Width = 59
                Visible = True
              end>
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 993
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 1245
    object GB_R: TGroupBox
      Left = 1198
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 1151
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 313
        Height = 31
        Caption = 'Retorno de Arquivos CNAB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 313
        Height = 31
        Caption = 'Retorno de Arquivos CNAB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 313
        Height = 31
        Caption = 'Retorno de Arquivos CNAB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 47
    Width = 993
    Height = 43
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitWidth = 1245
    object Panel8: TPanel
      Left = 2
      Top = 14
      Width = 1240
      Height = 28
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 462
    Width = 993
    Height = 69
    Align = alBottom
    TabOrder = 4
    ExplicitTop = 605
    ExplicitWidth = 1245
    object Panel9: TPanel
      Left = 2
      Top = 14
      Width = 1240
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 1106
        Top = 0
        Width = 135
        Height = 52
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 6
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtCarrega: TBitBtn
        Tag = 14
        Left = 22
        Top = 3
        Width = 118
        Height = 39
        Caption = '&Carrega'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtCarregaClick
      end
      object CkReverter: TCheckBox
        Left = 146
        Top = 18
        Width = 76
        Height = 17
        Caption = 'Re-registrar.'
        TabOrder = 1
      end
      object BtAbertos: TBitBtn
        Tag = 10017
        Left = 221
        Top = 3
        Width = 117
        Height = 39
        Caption = '&Abertos'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtAbertosClick
      end
      object BtBuffer: TBitBtn
        Left = 342
        Top = 3
        Width = 119
        Height = 39
        Caption = '&Buffer'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtBufferClick
      end
      object PB1: TProgressBar
        Left = 473
        Top = 18
        Width = 181
        Height = 17
        TabOrder = 4
        Visible = False
      end
      object Button1: TButton
        Left = 559
        Top = 14
        Width = 74
        Height = 25
        Caption = 'Button1'
        TabOrder = 5
        Visible = False
        OnClick = Button1Click
      end
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 108
    Top = 236
  end
  object QrDupl: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo '
      'FROM cnab_lei'
      'WHERE Banco=:P0'
      'AND NossoNum=:P1'
      'AND SeuNum=:P2'
      'AND OcorrCodi=:P3'
      'AND Arquivo=:P4'
      'AND ItemArq=:P5'
      'AND ValTitul=:P6'
      'AND ValPago=:P7'
      'AND QuitaData=:P8')
    Left = 120
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P7'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P8'
        ParamType = ptUnknown
      end>
    object QrDuplCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrSumPg: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT SUM(ad.Juros) Juros, SUM(ad.Desco) Desco, '
      'SUM(ad.Pago) Pago, li.Valor, MAX(ad.Data) MaxData'
      'FROM aduppgs ad'
      'LEFT JOIN lotesits li ON li.Controle=ad.LotesIts'
      'WHERE ad.LotesIts=:P0'
      'GROUP BY ad.LotesIts')
    Left = 204
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumPgJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrSumPgPago: TFloatField
      FieldName = 'Pago'
    end
    object QrSumPgDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrSumPgValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSumPgMaxData: TDateField
      FieldName = 'MaxData'
    end
  end
  object QrOcorreu: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT  lo.Cliente CLIENTELOTE, ob.Nome NOMEOCORRENCIA, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN lotesits li ON oc.LotesIts = li.Controle'
      'LEFT JOIN lotes lo ON lo.Codigo = li.Codigo'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.LotesIts=:P0'
      '')
    Left = 685
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcorreuNOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Size = 50
    end
    object QrOcorreuCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOcorreuLotesIts: TIntegerField
      FieldName = 'LotesIts'
      Required = True
    end
    object QrOcorreuDataO: TDateField
      FieldName = 'DataO'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorreuOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
      Required = True
    end
    object QrOcorreuValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorreuLoteQuit: TIntegerField
      FieldName = 'LoteQuit'
      Required = True
    end
    object QrOcorreuLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorreuDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorreuDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorreuUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorreuUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcorreuTaxaP: TFloatField
      FieldName = 'TaxaP'
      Required = True
    end
    object QrOcorreuTaxaV: TFloatField
      FieldName = 'TaxaV'
      Required = True
    end
    object QrOcorreuPago: TFloatField
      FieldName = 'Pago'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorreuDataP: TDateField
      FieldName = 'DataP'
      Required = True
    end
    object QrOcorreuTaxaB: TFloatField
      FieldName = 'TaxaB'
      Required = True
    end
    object QrOcorreuATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrOcorreuCLIENTELOTE: TIntegerField
      FieldName = 'CLIENTELOTE'
    end
    object QrOcorreuData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrOcorreuStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrOcorreuCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrOcorreuSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrOcorreuATZ_TEXTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ATZ_TEXTO'
      Size = 30
      Calculated = True
    end
  end
  object DsOcorreu: TDataSource
    DataSet = QrOcorreu
    Left = 713
    Top = 9
  end
  object QrOcorDupl: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT ok.Nome NOMEOCORBANK, ok.Base ValOcorBank, od.Ocorrbase '
      'FROM ocordupl od'
      'LEFT JOIN ocorbank ok ON ok.Codigo=od.Ocorrbase'
      'WHERE od.Codigo=:P0')
    Left = 741
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcorDuplNOMEOCORBANK: TWideStringField
      FieldName = 'NOMEOCORBANK'
      Size = 50
    end
    object QrOcorDuplValOcorBank: TFloatField
      FieldName = 'ValOcorBank'
    end
    object QrOcorDuplOcorrbase: TIntegerField
      FieldName = 'Ocorrbase'
      Required = True
    end
  end
  object QrOcorBank: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * '
      'FROM ocorbank'
      'WHERE Envio=:P0'
      'AND Movimento=:P1'
      '')
    Left = 769
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrOcorBankCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorBankNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrOcorBankLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorBankDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorBankDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorBankUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorBankUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcorBankBase: TFloatField
      FieldName = 'Base'
    end
    object QrOcorBankEnvio: TIntegerField
      FieldName = 'Envio'
    end
    object QrOcorBankMovimento: TIntegerField
      FieldName = 'Movimento'
    end
    object QrOcorBankFormaCNAB: TSmallintField
      FieldName = 'FormaCNAB'
    end
  end
  object Timer2: TTimer
    Enabled = False
    Interval = 100
    Left = 469
    Top = 260
  end
  object QrOB2: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Nome'
      'FROM ocorbank'
      'WHERE Codigo=:P0')
    Left = 796
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOB2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrLocEnt1: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT '
      'IF(ent.Tipo=0, ent.RazaoSocial , ent.Nome) NOMEENT,'
      'ent.Codigo CLIENTE, cnd.Codigo CliInt '
      'FROM cond cnd'
      'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente'
      'WHERE (CASE WHEN ent.Tipo=0 THEN ent.CNPJ ELSE ent.CPF END)=:P0'
      '/*   '
      '  AND cnd.CodCedente=:P1 '
      '*/')
    Left = 801
    Top = 225
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocEnt1NOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrLocEnt1CLIENTE: TIntegerField
      FieldName = 'CLIENTE'
      Required = True
    end
    object QrLocEnt1CliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object QrLei: TMySQLQuery
    Database = DModG.RV_CEP_DB
    AfterOpen = QrLeiAfterOpen
    BeforeClose = QrLeiBeforeClose
    AfterClose = QrLeiAfterClose
    AfterScroll = QrLeiAfterScroll
    OnCalcFields = QrLeiCalcFields
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE '
      'ent.Nome END NOMEENT, car.Tipo TipoCart, '
      'ban.DescriCNR, lei.*'
      'FROM cnab_lei lei'
      'LEFT JOIN entidades ent ON ent.Codigo=lei.Entidade'
      'LEFT JOIN carteiras car ON car.Codigo=lei.Carteira'
      'LEFT JOIN bancos    ban ON ban.Codigo=lei.Banco'
      'WHERE Step=0'
      'AND lei.Entidade=:P0'
      '')
    Left = 49
    Top = 285
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLeiCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cnab_lei.Codigo'
    end
    object QrLeiIDNum: TIntegerField
      FieldName = 'IDNum'
      Origin = 'cnab_lei.IDNum'
    end
    object QrLeiBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'cnab_lei.Banco'
      DisplayFormat = '000'
    end
    object QrLeiSeuNum: TIntegerField
      FieldName = 'SeuNum'
      Origin = 'cnab_lei.SeuNum'
      DisplayFormat = '0;-0; '
    end
    object QrLeiOcorrCodi: TWideStringField
      FieldName = 'OcorrCodi'
      Origin = 'cnab_lei.OcorrCodi'
      Size = 10
    end
    object QrLeiOcorrData: TDateField
      FieldName = 'OcorrData'
      Origin = 'cnab_lei.OcorrData'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLeiValTitul: TFloatField
      FieldName = 'ValTitul'
      Origin = 'cnab_lei.ValTitul'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiValAbati: TFloatField
      FieldName = 'ValAbati'
      Origin = 'cnab_lei.ValAbati'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiValDesco: TFloatField
      FieldName = 'ValDesco'
      Origin = 'cnab_lei.ValDesco'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiValPago: TFloatField
      FieldName = 'ValPago'
      Origin = 'cnab_lei.ValPago'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiValJuros: TFloatField
      FieldName = 'ValJuros'
      Origin = 'cnab_lei.ValJuros'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiValMulta: TFloatField
      FieldName = 'ValMulta'
      Origin = 'cnab_lei.ValMulta'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiMotivo1: TWideStringField
      FieldName = 'Motivo1'
      Origin = 'cnab_lei.Motivo1'
      Size = 2
    end
    object QrLeiMotivo2: TWideStringField
      FieldName = 'Motivo2'
      Origin = 'cnab_lei.Motivo2'
      Size = 2
    end
    object QrLeiMotivo3: TWideStringField
      FieldName = 'Motivo3'
      Origin = 'cnab_lei.Motivo3'
      Size = 2
    end
    object QrLeiMotivo4: TWideStringField
      FieldName = 'Motivo4'
      Origin = 'cnab_lei.Motivo4'
      Size = 2
    end
    object QrLeiMotivo5: TWideStringField
      FieldName = 'Motivo5'
      Origin = 'cnab_lei.Motivo5'
      Size = 2
    end
    object QrLeiQuitaData: TDateField
      FieldName = 'QuitaData'
      Origin = 'cnab_lei.QuitaData'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLeiDiretorio: TIntegerField
      FieldName = 'Diretorio'
      Origin = 'cnab_lei.Diretorio'
    end
    object QrLeiArquivo: TWideStringField
      FieldName = 'Arquivo'
      Origin = 'cnab_lei.Arquivo'
    end
    object QrLeiItemArq: TIntegerField
      FieldName = 'ItemArq'
      Origin = 'cnab_lei.ItemArq'
    end
    object QrLeiStep: TSmallintField
      FieldName = 'Step'
      Origin = 'cnab_lei.Step'
    end
    object QrLeiLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'cnab_lei.Lk'
    end
    object QrLeiDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'cnab_lei.DataCad'
    end
    object QrLeiDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'cnab_lei.DataAlt'
    end
    object QrLeiUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'cnab_lei.UserCad'
    end
    object QrLeiUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'cnab_lei.UserAlt'
    end
    object QrLeiEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'cnab_lei.Entidade'
    end
    object QrLeiNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrLeiCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'cnab_lei.Carteira'
      Required = True
    end
    object QrLeiTipoCart: TIntegerField
      FieldName = 'TipoCart'
      Origin = 'carteiras.Tipo'
      Required = True
    end
    object QrLeiDevJuros: TFloatField
      FieldName = 'DevJuros'
      Origin = 'cnab_lei.DevJuros'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiDevMulta: TFloatField
      FieldName = 'DevMulta'
      Origin = 'cnab_lei.DevMulta'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiDJM: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DJM'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrLeiValJuMul: TFloatField
      FieldName = 'ValJuMul'
      Origin = 'cnab_lei.ValJuMul'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiValOutro: TFloatField
      FieldName = 'ValOutro'
      Origin = 'cnab_lei.ValOutro'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiValTarif: TFloatField
      FieldName = 'ValTarif'
      Origin = 'cnab_lei.ValTarif'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLeiNossoNum: TWideStringField
      FieldName = 'NossoNum'
      Origin = 'cnab_lei.NossoNum'
      Required = True
    end
    object QrLeiDtaTarif: TDateField
      FieldName = 'DtaTarif'
      Origin = 'cnab_lei.DtaTarif'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLeiDTA_TARIF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DTA_TARIF_TXT'
      Size = 10
      Calculated = True
    end
    object QrLeiDescriCNR: TWideStringField
      FieldName = 'DescriCNR'
      Size = 100
    end
    object QrLeiTamReg: TIntegerField
      FieldName = 'TamReg'
      Required = True
    end
    object QrLeiID_Link: TLargeintField
      FieldName = 'ID_Link'
    end
    object QrLeiValOrig: TFloatField
      FieldName = 'ValOrig'
    end
    object QrLeiDPO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DPO'
      Calculated = True
    end
    object QrLeiJuridico: TIntegerField
      FieldName = 'Juridico'
    end
    object QrLeiLayoutRem: TWideStringField
      FieldName = 'LayoutRem'
      Size = 50
    end
    object QrLeiJURIDICO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'JURIDICO_TXT'
      Size = 50
      Calculated = True
    end
  end
  object DsLei: TDataSource
    DataSet = QrLei
    Left = 77
    Top = 285
  end
  object DsLeiItens: TDataSource
    DataSet = QrLeiItens
    Left = 77
    Top = 313
  end
  object QrBcocor: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Carrega'
      'FROM bancoslei'
      'WHERE Codigo=:P0'
      'AND Ocorrencia=:P1')
    Left = 321
    Top = 273
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrBcocorCarrega: TSmallintField
      FieldName = 'Carrega'
    end
  end
  object PMExclui: TPopupMenu
    Left = 532
    Top = 480
    object Ajustavaloresdobloquetoatual1: TMenuItem
      Caption = 
        ' Ajusta valores do bloqueto pois o banco informou juros/multa co' +
        'mo parte do valor do t'#237'tulo'
      OnClick = Ajustavaloresdobloquetoatual1Click
    end
    object CorrigeValTitulopoisobancoenviouerrado1: TMenuItem
      Caption = '&Corrige Val.Titulo pois o banco informou errado'
      OnClick = CorrigeValTitulopoisobancoenviouerrado1Click
    end
    object Excluso1: TMenuItem
      Caption = '&Exclus'#227'o'
      object ExcluiAtual1: TMenuItem
        Caption = 'Exclui &Atual'
        OnClick = ExcluiAtual1Click
      end
      object ExcluiSelecionados1: TMenuItem
        Caption = 'Exclui &Selecionados'
        OnClick = ExcluiSelecionados1Click
      end
      object ExcluiTodos1: TMenuItem
        Caption = 'Exclui &Todos'
        OnClick = ExcluiTodos1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object ExcluiBloquetosnolocalizados1: TMenuItem
        Caption = 'Exclui &Bloquetos n'#227'o localizados'
        OnClick = ExcluiBloquetosnolocalizados1Click
      end
    end
  end
  object PMConcilia: TPopupMenu
    Left = 36
    Top = 432
    object ConciliaAtual1: TMenuItem
      Caption = 'Concilia &Atual'
      OnClick = ConciliaAtual1Click
    end
    object ConciliaSelecionados1: TMenuItem
      Caption = 'Concilia &Selecionados'
      OnClick = ConciliaSelecionados1Click
    end
    object ConciliaTodos1: TMenuItem
      Caption = 'Concilia &Todos'
      OnClick = ConciliaTodos1Click
    end
  end
  object QrLocCta: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT con.Mensal'
      'FROM contas con '
      'WHERE con.Codigo=:P0')
    Left = 392
    Top = 124
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocCtaMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
  end
  object QrTem: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT COUNT(Codigo) Itens '
      'FROM cnab_lei'
      'WHERE Step=0')
    Left = 397
    Top = 281
    object QrTemItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrLEB: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Entidade'
      'FROM bancos'
      'WHERE Codigo=:P0')
    Left = 472
    Top = 92
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLEBEntidade: TIntegerField
      FieldName = 'Entidade'
    end
  end
  object QrLUH: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT DISTINCT Apto, Propriet '
      'FROM arreits'
      'WHERE Boleto=:P0'
      ''
      'UNION'
      ''
      'SELECT DISTINCT Apto, Propriet '
      'FROM consits'
      'WHERE Boleto=:P1')
    Left = 492
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLUHApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrLUHPropriet: TIntegerField
      FieldName = 'Propriet'
      Required = True
    end
  end
  object QrPesq2: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      '')
    Left = 72
    Top = 112
    object QrPesq2Vencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrPesq2PercMulta: TFloatField
      FieldName = 'PercMulta'
    end
    object QrPesq2PercJuros: TFloatField
      FieldName = 'PercJuros'
    end
    object QrPesq2Credito: TFloatField
      FieldName = 'Credito'
    end
  end
  object QrBco: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT cad.* '
      'FROM cnab_cad cad'
      'WHERE cad.Campo=:P0'
      ''
      ''
      ''
      ''
      '')
    Left = 609
    Top = 105
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBcoPadrIni: TIntegerField
      FieldName = 'PadrIni'
      Required = True
    end
    object QrBcoPadrTam: TIntegerField
      FieldName = 'PadrTam'
      Required = True
    end
    object QrBcoBcoOrig: TIntegerField
      FieldName = 'BcoOrig'
      Required = True
    end
  end
  object QrPesq3: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      '')
    Left = 100
    Top = 112
    object QrPesq3Credito: TFloatField
      FieldName = 'Credito'
    end
    object QrPesq3PercMulta: TFloatField
      FieldName = 'PercMulta'
    end
    object QrPesq3PercJuros: TFloatField
      FieldName = 'PercJuros'
    end
    object QrPesq3Vencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
  end
  object QrLocEnt2: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE '
      'ent.Nome END NOMEENT, ent.Codigo CLIENTE, cnd.Codigo CliInt'
      'FROM cond cnd'
      'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente'
      ''
      'WHERE cnd.Banco=:P0'
      'AND cnd.Agencia=:P1'
      'AND cnd.CodCedente=:P2')
    Left = 801
    Top = 253
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrLocEnt2NOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrLocEnt2CLIENTE: TIntegerField
      FieldName = 'CLIENTE'
      Required = True
    end
    object QrLocEnt2CliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object PMItens: TPopupMenu
    Left = 668
    Top = 440
    object Alteravalordoitemdearrecadaoselecionado1: TMenuItem
      Caption = '&Altera valor do item de arrecada'#231#227'o selecionado'
      OnClick = Alteravalordoitemdearrecadaoselecionado1Click
    end
    object Excluioitemdearrecadaoselecionado1: TMenuItem
      Caption = '&Exclui o item de arrecada'#231#227'o selecionado'
      OnClick = Excluioitemdearrecadaoselecionado1Click
    end
  end
  object QrLeiAgr: TMySQLQuery
    Database = DModG.RV_CEP_DB
    OnCalcFields = QrLeiAgrCalcFields
    SQL.Strings = (
      '')
    Left = 49
    Top = 341
    object QrLeiAgrNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
    object QrLeiAgrValor: TFloatField
      FieldName = 'Valor'
    end
    object QrLeiAgrData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLeiAgrMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrLeiAgrCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLeiAgrForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLeiAgrApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrLeiAgrUH: TWideStringField
      FieldName = 'UH'
      Size = 10
    end
    object QrLeiAgrTIPO_BLOQ: TLargeintField
      FieldName = 'TIPO_BLOQ'
      Required = True
    end
    object QrLeiAgrNOME_TIPO_BLOQ: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_BLOQ'
      Size = 30
      Calculated = True
    end
    object QrLeiAgrMez_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Mez_TXT'
      Size = 6
      Calculated = True
    end
  end
  object DsLeiAgr: TDataSource
    DataSet = QrLeiAgr
    Left = 77
    Top = 341
  end
  object DsCNAB_Dir: TDataSource
    DataSet = QrCNAB_Dir
    Left = 40
    Top = 12
  end
  object QrCNAB_Dir: TMySQLQuery
    Database = DModG.RV_CEP_DB
    AfterScroll = QrCNAB_DirAfterScroll
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) DOCUM, ent.Codigo CODENT,'
      'car.Nome NOMECARTEIRA, ban.Nome NOMEBANCO,'
      'car.Banco1, ent.CliInt, dir.Carteira,'
      'dir.Nome, dir.Codigo'
      'FROM cnab_dir dir'
      'LEFT JOIN carteiras car ON car.Codigo=dir.carteira'
      'LEFT JOIN bancos ban ON ban.Codigo=car.Banco1'
      'LEFT JOIN cond cnd ON cnd.Cliente=dir.CliInt'
      ''
      'LEFT JOIN entidades ent ON ent.Codigo=dir.CliInt'
      'WHERE dir.Envio=2'
      'AND dir.Ativo=1'
      'ORDER BY NOMEENT'
      '/*'
      'SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMEENT,'
      'car.Nome NOMECARTEIRA, ban.Nome NOMEBANCO,'
      'car.Banco1, ent.CliInt, dir.CartRetorno Carteira,'
      'dir.Nome, dir.Codigo'
      'FROM cnab_cfg dir'
      'LEFT JOIN carteiras car ON car.Codigo=dir.cartRetorno'
      'LEFT JOIN bancos ban ON ban.Codigo=car.Banco1'
      ''
      'LEFT JOIN entidades ent ON ent.Codigo=car.ForneceI'
      'WHERE dir.Ativo=1'
      'ORDER BY NOMEENT'
      '*/')
    Left = 12
    Top = 12
    object QrCNAB_DirNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Origin = 'NOMEENT'
      Size = 100
    end
    object QrCNAB_DirNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'cnab_cfg.Nome'
      Size = 50
    end
    object QrCNAB_DirCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'cnab_cfg.CartRetorno'
    end
    object QrCNAB_DirCliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'entidades.CliInt'
    end
    object QrCNAB_DirBanco1: TIntegerField
      FieldName = 'Banco1'
      Origin = 'carteiras.Banco1'
    end
    object QrCNAB_DirCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cnab_cfg.Codigo'
    end
    object QrCNAB_DirNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrCNAB_DirNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Origin = 'bancos.Nome'
      Size = 100
    end
    object QrCNAB_DirCODENT: TIntegerField
      FieldName = 'CODENT'
    end
    object QrCNAB_DirDOCUM: TWideStringField
      FieldName = 'DOCUM'
      Size = 18
    end
    object QrCNAB_DirLayoutRem: TWideStringField
      FieldName = 'LayoutRem'
      Size = 50
    end
  end
  object QrLeiItens: TMySQLQuery
    Database = DModG.RV_CEP_DB
    OnCalcFields = QrLeiItensCalcFields
    SQL.Strings = (
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    Left = 49
    Top = 313
    object QrLeiItensData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLeiItensCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrLeiItensGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLeiItensDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLeiItensCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLeiItensCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrLeiItensSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrLeiItensVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLeiItensMez: TIntegerField
      FieldName = 'Mez'
      Required = True
      DisplayFormat = '0000'
    end
    object QrLeiItensFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLeiItensNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
    object QrLeiItensControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLeiItensSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrLeiItensTipoCart: TIntegerField
      FieldName = 'TipoCart'
      Required = True
    end
    object QrLeiItensDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrLeiItensNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrLeiItensSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrLeiItensDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrLeiItensCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLeiItensCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLeiItensForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLeiItensDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLeiItensApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrLeiItensUH: TWideStringField
      FieldName = 'UH'
      Size = 10
    end
    object QrLeiItensMez_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Mez_TXT'
      Size = 6
      Calculated = True
    end
    object QrLeiItensMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLeiItensCOND: TIntegerField
      FieldName = 'COND'
    end
    object QrLeiItensNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Size = 100
    end
    object QrLeiItensCartDest: TIntegerField
      FieldName = 'CartDest'
    end
  end
  object QrClientes: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE '
      'ent.Nome END NOMEENT, ent.Codigo Entidade, cnd.Codigo CODIGO'
      'FROM cnab_lei lei'
      'LEFT JOIN entidades ent ON ent.Codigo=lei.Entidade'
      'LEFT JOIN cond      cnd ON cnd.Cliente=ent.Codigo'
      'WHERE Step=0'
      'GROUP BY lei.Entidade'
      'ORDER BY NOMEENT')
    Left = 217
    Top = 300
    object QrClientesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrClientesEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrClientesCODIGO: TIntegerField
      FieldName = 'CODIGO'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 245
    Top = 301
  end
  object QrLeiSum: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    Left = 105
    Top = 285
    object QrLeiSumCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLeiSumID600: TFloatField
      FieldName = 'ID600'
    end
    object QrLeiSumID601: TFloatField
      FieldName = 'ID601'
    end
    object QrLeiSumID610: TFloatField
      FieldName = 'ID610'
    end
  end
  object frxCrossObject1: TfrxCrossObject
    Left = 56
    Top = 28
  end
  object DataSource1: TDataSource
    DataSet = QrLeiSum
    Left = 368
    Top = 280
  end
  object PMBuffer: TPopupMenu
    OnPopup = PMBufferPopup
    Left = 296
    Top = 484
    object Limpabuffer1: TMenuItem
      Caption = '&Limpa buffer'
      OnClick = Limpabuffer1Click
    end
    object Adicionadados1: TMenuItem
      Caption = '&Adiciona dados'
      Enabled = False
      OnClick = Adicionadados1Click
    end
    object Imprimelista1: TMenuItem
      Caption = '&Imprime lista'
      Enabled = False
      object Ordenarpelonmerodobloqueto1: TMenuItem
        Caption = '&Ordenar pelo n'#250'mero do bloqueto'
        OnClick = Ordenarpelonmerodobloqueto1Click
      end
      object Ordenarpeladatadecrdito1: TMenuItem
        Caption = 'Ordenar pela data de cr'#233'dito'
        OnClick = Ordenarpeladatadecrdito1Click
      end
      object Ordenarpeladatadepagamento1: TMenuItem
        Caption = 'Ordenar pela data de pagamento'
        OnClick = Ordenarpeladatadepagamento1Click
      end
    end
  end
  object frxDsQuery: TfrxDBDataset
    UserName = 'frxDsQuery'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Bloqueto=Bloqueto'
      'ValTitul=ValTitul'
      'ValPago=ValPago'
      'ValMulta=ValMulta'
      'ValJuros=ValJuros'
      'ValOutros=ValOutros'
      'ValJurMul=ValJurMul'
      'ValERRO=ValERRO'
      'ValTarifa=ValTarifa'
      'OcorrCod=OcorrCod'
      'OcorrTxt=OcorrTxt'
      'Ativo=Ativo'
      'SEQ=SEQ'
      'DataOcorr=DataOcorr'
      'DataTarif=DataTarif'
      'DataQuita=DataQuita'
      'DataOcorr_TXT=DataOcorr_TXT'
      'DataTarif_TXT=DataTarif_TXT'
      'DataQuita_TXT=DataQuita_TXT')
    BCDToCurrency = False
    
    Left = 596
    Top = 8
  end
  object frxGrade: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    EngineOptions.MaxMemSize = 10000000
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38006.786384259300000000
    ReportOptions.LastChange = 39872.778306180600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Visivel: Boolean;                                   '
      ''
      'begin'
      '  Visivel := <VARF_G1_VISIBLE>;                                '
      
        '  GroupHeader1.Visible := Visivel;                              ' +
        '                       '
      '  GroupFooter1.Visible := Visivel;'
      '  GroupHeader1.Condition := <VARF_G1_COND>;        '
      '  Memo27.Memo.Text := <VARF_G1_TEXT>;        '
      'end.')
    OnGetValue = frxGradeGetValue
    Left = 568
    Top = 8
    Datasets = <
      item
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
      end
      item
        DataSet = frxDsQuery
        DataSetName = 'frxDsQuery'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.236240000000000000
        Top = 408.189240000000000000
        Width = 680.315400000000000000
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 75.590600000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object frxShapeView1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Rela'#231#227'o de Bloquetos Liquidados')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxMemoView1: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CONDOMINIO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 60.472480000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Bloqueto')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 60.472480000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor t'#237'tulo')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 60.472480000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor pago')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Top = 60.472480000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dta ocorr.')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 60.472480000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dta quita'#231#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748300000000000000
          Top = 60.472480000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ocorr'#234'ncia')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882190000000000000
          Top = 60.472480000000000000
          Width = 215.433210000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 37.795300000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 64.252010000000000000
        Top = 321.260050000000000000
        Width = 680.315400000000000000
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsQuery."ValTitul">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsQuery."ValPago">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Top = 3.779530000000000000
          Width = 415.748300000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 200.315090000000000000
        Width = 680.315400000000000000
        DataSet = frxDsQuery
        DataSetName = 'frxDsQuery'
        RowCount = 0
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataField = 'Bloqueto'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsQuery."Bloqueto"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataField = 'ValTitul'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsQuery."ValTitul"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataField = 'ValPago'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsQuery."ValPago"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsQuery."DataOcorr_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataField = 'DataQuita_TXT'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsQuery."DataQuita_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748300000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          DataField = 'OcorrCod'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsQuery."OcorrCod"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 464.882190000000000000
          Width = 215.433210000000000000
          Height = 15.118120000000000000
          DataField = 'OcorrTxt'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsQuery."OcorrTxt"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795300000000000000
          Height = 15.118120000000000000
          DataField = 'SEQ'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsQuery."SEQ"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 154.960730000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsQuery."DataQuita"'
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Width = 676.535870000000000000
          Height = 22.677180000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsQuery."OcorrTxt"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 238.110390000000000000
        Width = 680.315400000000000000
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sub-total:  ')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Top = 3.779530000000000000
          Width = 415.748300000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsQuery."ValTitul">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 188.976500000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 15.118120000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsQuery."ValPago">)]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object QrLocByCed: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF   '
      'FROM carteiras car'
      'LEFT JOIN entidades ent ON ent.Codigo=car.ForneceI  '
      'WHERE car.CodCedente = :P0'
      'AND car.Banco1=:P1 '
      '   ')
    Left = 220
    Top = 80
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocByCedCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
  end
end
