object FmCNAB_Cond: TFmCNAB_Cond
  Left = 339
  Top = 185
  Caption = 'BCO-CNAB_-002 :: Fus'#227'o Dados CNAB x Condom'#237'nio'
  ClientHeight = 496
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 418
        Height = 32
        Caption = 'Fus'#227'o Dados CNAB x Condom'#237'nio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 418
        Height = 32
        Caption = 'Fus'#227'o Dados CNAB x Condom'#237'nio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 418
        Height = 32
        Caption = 'Fus'#227'o Dados CNAB x Condom'#237'nio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 334
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 334
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 334
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 260
          Height = 317
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object RGPosto: TRadioGroup
            Left = 0
            Top = 104
            Width = 260
            Height = 52
            Align = alTop
            Caption = ' Posto banc'#225'rio: '
            Items.Strings = (
              'Condom'#237'nio'
              'CNAB')
            TabOrder = 2
            OnClick = HabilitaBtOK
          end
          object RGBanco: TRadioGroup
            Left = 0
            Top = 0
            Width = 260
            Height = 52
            Align = alTop
            Caption = ' Banco:'
            Items.Strings = (
              'Condom'#237'nio'
              'CNAB')
            TabOrder = 0
            OnClick = HabilitaBtOK
          end
          object RGAgencia: TRadioGroup
            Left = 0
            Top = 52
            Width = 260
            Height = 52
            Align = alTop
            Caption = ' Ag'#234'ncia: '
            Items.Strings = (
              'Condom'#237'nio'
              'CNAB')
            TabOrder = 1
            OnClick = HabilitaBtOK
          end
          object RGConta: TRadioGroup
            Left = 0
            Top = 208
            Width = 260
            Height = 52
            Align = alTop
            Caption = 'Conta corrente: '
            Items.Strings = (
              'Condom'#237'nio'
              'CNAB')
            TabOrder = 4
            OnClick = HabilitaBtOK
          end
          object RGDVAgencia: TRadioGroup
            Left = 0
            Top = 156
            Width = 260
            Height = 52
            Align = alTop
            Caption = ' DV da ag'#234'ncia: '
            Items.Strings = (
              'Condom'#237'nio'
              'CNAB')
            TabOrder = 3
            OnClick = HabilitaBtOK
          end
          object RGDVConta: TRadioGroup
            Left = 0
            Top = 260
            Width = 260
            Height = 52
            Align = alTop
            Caption = 'DV da Conta corrente: '
            Items.Strings = (
              'Condom'#237'nio'
              'CNAB')
            TabOrder = 5
            OnClick = HabilitaBtOK
          end
        end
        object Panel6: TPanel
          Left = 262
          Top = 15
          Width = 260
          Height = 317
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object RGModalCobr: TRadioGroup
            Left = 0
            Top = 104
            Width = 260
            Height = 52
            Align = alTop
            Caption = ' Modalidade de cobran'#231'a (Cadastramento do t'#237'tulo no banco): '
            Items.Strings = (
              'Condom'#237'nio'
              'CNAB')
            TabOrder = 2
            OnClick = HabilitaBtOK
          end
          object RGCarteira: TRadioGroup
            Left = 0
            Top = 156
            Width = 260
            Height = 52
            Align = alTop
            Caption = 'Carteira de lan'#231'amentos financeiros dos boletos:'
            Items.Strings = (
              'Condom'#237'nio'
              'CNAB')
            TabOrder = 3
            OnClick = HabilitaBtOK
          end
          object RGIDCobranca: TRadioGroup
            Left = 0
            Top = 208
            Width = 260
            Height = 52
            Align = alTop
            Caption = ' Identificador do tipo de cobran'#231'a (ID da carteira):'
            Items.Strings = (
              'Condom'#237'nio'
              'CNAB')
            TabOrder = 4
            OnClick = HabilitaBtOK
          end
          object RGCedente: TRadioGroup
            Left = 0
            Top = 260
            Width = 260
            Height = 52
            Align = alTop
            Caption = ' C'#243'digo do cedente (Cedente l'#237'der):'
            Items.Strings = (
              'Condom'#237'nio'
              'CNAB')
            TabOrder = 5
            OnClick = HabilitaBtOK
          end
          object RGAgContaCed: TRadioGroup
            Left = 0
            Top = 52
            Width = 260
            Height = 52
            Align = alTop
            Caption = 'Ag'#234'cia / Conta Cedente:'
            Items.Strings = (
              'Condom'#237'nio'
              'CNAB')
            TabOrder = 1
            OnClick = HabilitaBtOK
          end
          object RGEspecieTxt: TRadioGroup
            Left = 0
            Top = 0
            Width = 260
            Height = 52
            Align = alTop
            Caption = ' Esp'#233'cie do valor: '
            Items.Strings = (
              'Condom'#237'nio'
              'CNAB')
            TabOrder = 0
            OnClick = HabilitaBtOK
          end
        end
        object Panel7: TPanel
          Left = 522
          Top = 15
          Width = 260
          Height = 317
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object RGCartTxt: TRadioGroup
            Left = 0
            Top = 156
            Width = 260
            Height = 52
            Align = alTop
            Caption = '  Texto opcional para impress'#227'o da carteira no bloqueto: '
            Items.Strings = (
              'Condom'#237'nio'
              'CNAB')
            TabOrder = 3
            OnClick = HabilitaBtOK
          end
          object RGTipoCobranca: TRadioGroup
            Left = 0
            Top = 0
            Width = 260
            Height = 52
            Align = alTop
            Caption = ' Tipo de cobran'#231'a:'
            Items.Strings = (
              'Condom'#237'nio'
              'CNAB')
            TabOrder = 0
            OnClick = HabilitaBtOK
          end
          object RGOperCodi: TRadioGroup
            Left = 0
            Top = 52
            Width = 260
            Height = 52
            Align = alTop
            Caption = ' Opera'#231#227'o C'#243'digo:'
            Items.Strings = (
              'Condom'#237'nio'
              'CNAB')
            TabOrder = 1
            OnClick = HabilitaBtOK
          end
          object RGEspecieDoc: TRadioGroup
            Left = 0
            Top = 104
            Width = 260
            Height = 52
            Align = alTop
            Caption = ' Esp'#233'cie do Documento: '
            Items.Strings = (
              'Condom'#237'nio'
              'CNAB')
            TabOrder = 2
            OnClick = HabilitaBtOK
          end
          object RGCNAB: TRadioGroup
            Left = 0
            Top = 208
            Width = 260
            Height = 52
            Align = alTop
            Caption = '  CNAB (Remessa Retorno): '
            Items.Strings = (
              'Condom'#237'nio'
              'CNAB')
            TabOrder = 4
            OnClick = HabilitaBtOK
          end
          object RGCtaCooper: TRadioGroup
            Left = 0
            Top = 260
            Width = 260
            Height = 52
            Align = alTop
            Caption = ' Conta Corrente Cooperado (Nosso n'#250'mero): '
            Items.Strings = (
              'Condom'#237'nio'
              'CNAB')
            TabOrder = 5
            OnClick = HabilitaBtOK
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 382
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 426
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        TabOrder = 0
        OnClick = BtOKClick
        NumGlyphs = 2
      end
      object RGNada: TRadioGroup
        Left = 260
        Top = 0
        Width = 260
        Height = 52
        Caption = ' Orienta'#231#227'o de informa'#231#245'es: '
        Enabled = False
        ItemIndex = 0
        Items.Strings = (
          'dados Condom'#237'nio'
          'dados CNAB')
        TabOrder = 1
        OnClick = HabilitaBtOK
      end
    end
  end
end
