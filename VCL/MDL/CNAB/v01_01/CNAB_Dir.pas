unit CNAB_Dir;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Variants,
  dmkGeral, dmkDBLookupComboBox, dmkEditCB, dmkEdit, UnDmkProcFunc, dmkImage,
  DmkDAC_PF, UnDmkEnums;

type
  TFmCNAB_Dir = class(TForm)
    PainelDados: TPanel;
    DsCNAB_Dir: TDataSource;
    QrCNAB_Dir: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    SpeedButton5: TSpeedButton;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    Label3: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    DBEdit001: TDBEdit;
    RGEnvio: TRadioGroup;
    DBRadioGroup1: TDBRadioGroup;
    QrCNAB_DirCodigo: TIntegerField;
    QrCNAB_DirLk: TIntegerField;
    QrCNAB_DirDataCad: TDateField;
    QrCNAB_DirDataAlt: TDateField;
    QrCNAB_DirUserCad: TIntegerField;
    QrCNAB_DirUserAlt: TIntegerField;
    QrCNAB_DirNome: TWideStringField;
    QrCNAB_DirEnvio: TSmallintField;
    DBEdit002: TDBEdit;
    Label5: TLabel;
    QrCNAB_DirNOMECLIINT: TWideStringField;
    QrCNAB_DirCliInt: TIntegerField;
    Label6: TLabel;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMECLIENTE: TWideStringField;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCNAB_DirCarteira: TIntegerField;
    QrCNAB_DirNOMECARTEIRA: TWideStringField;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    QrCarteirasBanco1: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel2: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCNAB_DirAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrCNAB_DirAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCNAB_DirBeforeOpen(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
    procedure QrClientesAfterScroll(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure ReopenCarteiras;
    function  PesquisaNomeCli(): Integer;
  public
    { Public declarations }
    FSeq: Integer;
  end;

var
  FmCNAB_Dir: TFmCNAB_Dir;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, Curinga;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCNAB_Dir.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCNAB_Dir.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCNAB_DirCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCNAB_Dir.DefParams;
begin
  VAR_GOTOTABELA := 'CNAB_Dir';
  VAR_GOTOMYSQLTABLE := QrCNAB_Dir;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE');
  VAR_SQLx.Add('ent.Nome END NOMECLIINT, car.Nome NOMECARTEIRA, dir.*');
  VAR_SQLx.Add('FROM cnab_dir dir');
  VAR_SQLx.Add('LEFT JOIN carteiras car ON car.Codigo=dir.Carteira');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=dir.CliInt');
  VAR_SQLx.Add('WHERE dir.Codigo > 0');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND dir.Codigo=:P0');
  //
  VAR_SQLa.Add('AND dir.Nome LIKE :P0');
  //
end;

procedure TFmCNAB_Dir.MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text       := '';
        EdNome.Text         := '';
        EdCliInt.Text       := '';
        CBCliInt.KeyValue   := NULL;
        EdCarteira.Text     := '';
        CBCarteira.KeyValue := NULL;
        RGEnvio.ItemIndex   := 2;
      end else begin
        EdCodigo.Text       := IntToStr(QrCNAB_DirCodigo.Value);
        EdNome.Text         := QrCNAB_DirNome.Value;
        EdCliInt.Text       := IntToStr(QrCNAB_DirCliInt.Value);
        CBCliInt.KeyValue   := QrCNAB_DirCliInt.Value;
        //Deve ser depois do CliInt
        EdCarteira.Text     := IntToStr(QrCNAB_DirCarteira.Value);
        CBCarteira.KeyValue := QrCNAB_DirCarteira.Value;
        //
        RGEnvio.ItemIndex   := QrCNAB_DirEnvio.Value;
      end;
      EdCliInt.SetFocus;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

function TFmCNAB_Dir.PesquisaNomeCli(): Integer;
var
  Nome: String;
begin
  Nome   := '';
  Result := 0;
  //
  if InputQuery('Procura e Lista', 'Digite parte do nome:', Nome) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmodG.QrSB, Dmod.MyDB, [
    'SELECT dir.Codigo, eci.CodCliInt CodUsu, ',
    'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome ',
    'FROM cnab_dir dir ',
    'LEFT JOIN enticliint eci ON eci.CodEnti = dir.CliInt ',
    'LEFT JOIN entidades ent ON ent.Codigo = eci.CodEnti ',
    'WHERE IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) ',
    ' LIKE "%' + Nome + '%" ',
    '']);
    //
    MyObjects.FormShow(TFmCuringa, FmCuringa);
    //
    Result := VAR_CODIGO;    
  end;
end;

procedure TFmCNAB_Dir.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCNAB_Dir.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCNAB_Dir.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCNAB_Dir.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCNAB_Dir.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCNAB_Dir.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCNAB_Dir.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCNAB_Dir.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmCNAB_Dir.BtAlteraClick(Sender: TObject);
var
  CNAB_Dir : Integer;
begin
  CNAB_Dir := QrCNAB_DirCodigo.Value;
  if not UMyMod.SelLockY(CNAB_Dir, Dmod.MyDB, 'CNAB_Dir', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(CNAB_Dir, Dmod.MyDB, 'CNAB_Dir', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmCNAB_Dir.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCNAB_DirCodigo.Value;
  Close;
end;

procedure TFmCNAB_Dir.BtConfirmaClick(Sender: TObject);
var
  Codigo, CliInt, Carteira: Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MensagemBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    EdNome.SetFocus;
    Exit;
  end;
  CliInt := Geral.IMV(EdCliInt.Text);
  if CliInt = 0 then
  begin
    Geral.MensagemBox('Defina o cliente interno.', 'Erro', MB_OK+MB_ICONERROR);
    EdCliInt.SetFocus;
    Exit;
  end;
  Carteira := Geral.IMV(EdCarteira.Text);
  if Carteira = 0 then
  begin
    Geral.MensagemBox('Defina a conta corrente (Carteira).', 'Erro', MB_OK+MB_ICONERROR);
    EdCarteira.SetFocus;
    Exit;
  end;
  if QrCarteirasBanco1.Value = 0 then
  begin
    Geral.MensagemBox('A conta corrente (Carteira) selecionada ' +
    'n�o possui banco associado. A importa��o CNAB n�o ser� concretizada se ' +
    'o banco n�o for informado no cadastro da carteira (conta corrente).',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpdU.SQL.Add('INSERT INTO cnab_dir SET ');
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'CNAB_Dir', 'CNAB_Dir', 'Codigo');
  end else begin
    Dmod.QrUpdU.SQL.Add('UPDATE cnab_dir SET ');
    Codigo := QrCNAB_DirCodigo.Value;
  end;
  Dmod.QrUpdU.SQL.Add('Nome=:P0, Carteira=:P1, Envio=:P2, CliInt=:P3, ');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := Geral.IMV(EdCarteira.Text);
  Dmod.QrUpdU.Params[02].AsInteger := RGEnvio.ItemIndex;
  Dmod.QrUpdU.Params[03].AsInteger := CliInt;
  //
  Dmod.QrUpdU.Params[04].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[05].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[06].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CNAB_Dir', 'Codigo');
  MostraEdicao(0, stLok, 0);
  LocCod(Codigo,Codigo);
  if FSeq = 1 then Close;
end;

procedure TFmCNAB_Dir.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'CNAB_Dir', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CNAB_Dir', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CNAB_Dir', 'Codigo');
end;

procedure TFmCNAB_Dir.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FSeq := 0;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
  QrClientes.Open;
end;

procedure TFmCNAB_Dir.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCNAB_DirCodigo.Value,LaRegistro.Caption);
end;

procedure TFmCNAB_Dir.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmCNAB_Dir.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCNAB_Dir.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmCNAB_Dir.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  VAR_MARCA := QrCNAB_DirCodigo.Value;
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCNAB_Dir.QrCNAB_DirAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCNAB_Dir.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'CNAB_Dir', 'Livres', 99) then
  //BtInclui.Enabled := False;
  if FSeq = 1 then MostraEdicao(1, stIns, 0);
end;

procedure TFmCNAB_Dir.QrCNAB_DirAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrCNAB_DirCodigo.Value, False);
  //BtExclui.Enabled := GOTOy.BtEnabled(QrCNAB_DirCodigo.Value, False);
end;

procedure TFmCNAB_Dir.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCNAB_DirCodigo.Value,
  CuringaLoc.CriaForm('dir.Codigo', 'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome)',
    'cnab_dir dir', Dmod.MyDB, CO_VAZIO, False,
    'LEFT JOIN enticliint eci ON eci.CodEnti = dir.CliInt LEFT JOIN entidades ent ON ent.Codigo = eci.CodEnti'));
end;

procedure TFmCNAB_Dir.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCNAB_Dir.QrCNAB_DirBeforeOpen(DataSet: TDataSet);
begin
  QrCNAB_DirCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCNAB_Dir.SpeedButton5Click(Sender: TObject);
var
  Dir: String;
begin
  if MyObjects.FileOpenDialog(Self, '', '', 'Diret�rio CNAB', '', [], Dir) then
    EdNome.Text := ExtractFileDir(Dir);
end;

procedure TFmCNAB_Dir.QrClientesAfterScroll(DataSet: TDataSet);
begin
  EdCarteira.Text := '';
  CBCarteira.KeyValue := NULL;
  ReopenCarteiras;
end;

procedure TFmCNAB_Dir.ReopenCarteiras;
begin
  QrCarteiras.Close;
  QrCarteiras.Params[0].AsInteger := QrClientesCodigo.Value;
  QrCarteiras.Open;
end;

end.

