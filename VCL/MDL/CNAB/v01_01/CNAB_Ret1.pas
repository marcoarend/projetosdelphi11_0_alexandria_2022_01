unit CNAB_Ret1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, DBCtrls, Grids,
  DBGrids, math, ComCtrls, Menus, dmkDBGrid, Variants, frxCross, frxClass, Mask,
  ABSMain, frxDBSet, dmkGeral, dmkEdit, UnDmkProcFunc, UnDmkEnums;

type
  TFmCNAB_Ret1 = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    Timer1: TTimer;
    PnCarrega: TPanel;
    PainelConfirma: TPanel;
    BtCarrega: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Button1: TButton;
    PnArquivos: TPanel;
    ListBox1: TListBox;
    MemoTam: TMemo;
    QrDupl: TmySQLQuery;
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    QrSumPg: TmySQLQuery;
    QrSumPgJuros: TFloatField;
    QrSumPgPago: TFloatField;
    QrSumPgDesco: TFloatField;
    QrSumPgValor: TFloatField;
    QrSumPgMaxData: TDateField;
    PnMovimento: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    BitBtn2: TBitBtn;
    Button2: TButton;
    QrOcorreu: TmySQLQuery;
    QrOcorreuNOMEOCORRENCIA: TWideStringField;
    QrOcorreuCodigo: TIntegerField;
    QrOcorreuLotesIts: TIntegerField;
    QrOcorreuDataO: TDateField;
    QrOcorreuOcorrencia: TIntegerField;
    QrOcorreuValor: TFloatField;
    QrOcorreuLoteQuit: TIntegerField;
    QrOcorreuLk: TIntegerField;
    QrOcorreuDataCad: TDateField;
    QrOcorreuDataAlt: TDateField;
    QrOcorreuUserCad: TIntegerField;
    QrOcorreuUserAlt: TIntegerField;
    QrOcorreuTaxaP: TFloatField;
    QrOcorreuTaxaV: TFloatField;
    QrOcorreuPago: TFloatField;
    QrOcorreuDataP: TDateField;
    QrOcorreuTaxaB: TFloatField;
    QrOcorreuATUALIZADO: TFloatField;
    QrOcorreuCLIENTELOTE: TIntegerField;
    QrOcorreuData3: TDateField;
    QrOcorreuStatus: TSmallintField;
    QrOcorreuCliente: TIntegerField;
    QrOcorreuSALDO: TFloatField;
    QrOcorreuATZ_TEXTO: TWideStringField;
    DsOcorreu: TDataSource;
    QrOcorDupl: TmySQLQuery;
    QrOcorDuplNOMEOCORBANK: TWideStringField;
    QrOcorDuplValOcorBank: TFloatField;
    QrOcorDuplOcorrbase: TIntegerField;
    QrOcorBank: TmySQLQuery;
    QrOcorBankCodigo: TIntegerField;
    QrOcorBankNome: TWideStringField;
    QrOcorBankLk: TIntegerField;
    QrOcorBankDataCad: TDateField;
    QrOcorBankDataAlt: TDateField;
    QrOcorBankUserCad: TIntegerField;
    QrOcorBankUserAlt: TIntegerField;
    QrOcorBankBase: TFloatField;
    QrOcorBankEnvio: TIntegerField;
    QrOcorBankMovimento: TIntegerField;
    QrOcorBankFormaCNAB: TSmallintField;
    Timer2: TTimer;
    QrOB2: TmySQLQuery;
    QrOB2Nome: TWideStringField;
    PageControl1: TPageControl;
    QrLocEnt1: TmySQLQuery;
    QrLocEnt1NOMEENT: TWideStringField;
    QrLocEnt1CLIENTE: TIntegerField;
    QrLocEnt1COND: TIntegerField;
    Splitter1: TSplitter;
    TabSheet3: TTabSheet;
    Grade1: TStringGrid;
    QrLei: TmySQLQuery;
    DsLei: TDataSource;
    QrLeiCodigo: TIntegerField;
    QrLeiBanco: TIntegerField;
    QrLeiSeuNum: TIntegerField;
    QrLeiOcorrData: TDateField;
    QrLeiValTitul: TFloatField;
    QrLeiValAbati: TFloatField;
    QrLeiValDesco: TFloatField;
    QrLeiValPago: TFloatField;
    QrLeiValJuros: TFloatField;
    QrLeiValMulta: TFloatField;
    QrLeiMotivo1: TWideStringField;
    QrLeiMotivo2: TWideStringField;
    QrLeiMotivo3: TWideStringField;
    QrLeiMotivo4: TWideStringField;
    QrLeiMotivo5: TWideStringField;
    QrLeiQuitaData: TDateField;
    QrLeiDiretorio: TIntegerField;
    QrLeiArquivo: TWideStringField;
    QrLeiItemArq: TIntegerField;
    QrLeiStep: TSmallintField;
    QrLeiLk: TIntegerField;
    QrLeiDataCad: TDateField;
    QrLeiDataAlt: TDateField;
    QrLeiUserCad: TIntegerField;
    QrLeiUserAlt: TIntegerField;
    TabSheet1: TTabSheet;
    Memo1: TMemo;
    BtAbertos: TBitBtn;
    QrLeiIDNum: TIntegerField;
    PnCarregados: TPanel;
    Panel5: TPanel;
    PB1: TProgressBar;
    QrLeiEntidade: TIntegerField;
    QrLeiNOMEENT: TWideStringField;
    DsLeiItens: TDataSource;
    QrBcocor: TmySQLQuery;
    QrBcocorCarrega: TSmallintField;
    StaticText1: TStaticText;
    BtExclui: TBitBtn;
    PMExclui: TPopupMenu;
    ExcluiAtual1: TMenuItem;
    ExcluiSelecionados1: TMenuItem;
    ExcluiTodos1: TMenuItem;
    BtConcilia: TBitBtn;
    PMConcilia: TPopupMenu;
    ConciliaAtual1: TMenuItem;
    ConciliaSelecionados1: TMenuItem;
    ConciliaTodos1: TMenuItem;
    QrLeiOcorrCodi: TWideStringField;
    QrLocCta: TmySQLQuery;
    QrTem: TmySQLQuery;
    QrTemItens: TLargeintField;
    BitBtn1: TBitBtn;
    QrLeiCarteira: TIntegerField;
    QrLeiTipoCart: TIntegerField;
    QrLEB: TmySQLQuery;
    QrLEBEntidade: TIntegerField;
    QrLUH: TmySQLQuery;
    QrLUHApto: TIntegerField;
    QrLUHPropriet: TIntegerField;
    Splitter2: TSplitter;
    CkReverter: TCheckBox;
    QrDuplCodigo: TIntegerField;
    QrLeiDevJuros: TFloatField;
    QrLeiDevMulta: TFloatField;
    QrLeiDJM: TFloatField;
    QrPesq2: TmySQLQuery;
    QrPesq2Vencimento: TDateField;
    QrPesq2PercMulta: TFloatField;
    QrPesq2PercJuros: TFloatField;
    Label1: TLabel;
    QrBco: TmySQLQuery;
    QrBcoPadrIni: TIntegerField;
    QrBcoPadrTam: TIntegerField;
    QrBcoBcoOrig: TIntegerField;
    QrLeiValJuMul: TFloatField;
    QrLeiValOutro: TFloatField;
    QrLeiValTarif: TFloatField;
    QrLocCond: TmySQLQuery;
    Label2: TLabel;
    Label3: TLabel;
    BtAgenda: TBitBtn;
    QrLocCondCodigo: TIntegerField;
    QrPesq2Credito: TFloatField;
    QrLeiNossoNum: TWideStringField;
    QrLeiDtaTarif: TDateField;
    QrLeiDTA_TARIF_TXT: TWideStringField;
    QrPesq3: TmySQLQuery;
    QrPesq3Credito: TFloatField;
    QrPesq3PercMulta: TFloatField;
    QrPesq3PercJuros: TFloatField;
    QrPesq3Vencimento: TDateField;
    QrLocCtaMensal: TWideStringField;
    QrLeiDescriCNR: TWideStringField;
    QrLocEnt2: TmySQLQuery;
    QrLocEnt2NOMEENT: TWideStringField;
    QrLocEnt2CLIENTE: TIntegerField;
    QrLocEnt2COND: TIntegerField;
    TabSheet2: TTabSheet;
    Memo2: TMemo;
    QrLocCondPercJuros: TFloatField;
    QrLocCondPercMulta: TFloatField;
    QrLocCondVTCBBNITAR: TFloatField;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    DBGLei: TdmkDBGrid;
    dmkDBGrid1: TdmkDBGrid;
    N1: TMenuItem;
    ExcluiBloquetosnolocalizados1: TMenuItem;
    BtItens: TBitBtn;
    PMItens: TPopupMenu;
    Alteravalordoitemdearrecadaoselecionado1: TMenuItem;
    Excluioitemdearrecadaoselecionado1: TMenuItem;
    Excluso1: TMenuItem;
    Ajustavaloresdobloquetoatual1: TMenuItem;
    PageControl4: TPageControl;
    TabSheet6: TTabSheet;
    DBGrid2: TDBGrid;
    TabSheet7: TTabSheet;
    QrLeiAgr: TmySQLQuery;
    DsLeiAgr: TDataSource;
    DBGrid3: TDBGrid;
    QrLeiAgrNOMEPROPRIET: TWideStringField;
    QrLeiAgrValor: TFloatField;
    QrLeiAgrData: TDateField;
    QrLeiAgrMez: TIntegerField;
    QrLeiAgrCliInt: TIntegerField;
    QrLeiAgrForneceI: TIntegerField;
    QrLeiAgrApto: TIntegerField;
    QrLeiAgrUH: TWideStringField;
    QrLeiAgrTIPO_BLOQ: TLargeintField;
    QrLeiAgrNOME_TIPO_BLOQ: TWideStringField;
    QrLeiAgrMez_TXT: TWideStringField;
    Splitter3: TSplitter;
    GradeA: TStringGrid;
    QrLeiTamReg: TIntegerField;
    DsCNAB_Dir: TDataSource;
    QrCNAB_Dir: TmySQLQuery;
    QrCNAB_DirNOMEBANCO: TWideStringField;
    QrCNAB_DirCodigo: TIntegerField;
    QrCNAB_DirLk: TIntegerField;
    QrCNAB_DirDataCad: TDateField;
    QrCNAB_DirDataAlt: TDateField;
    QrCNAB_DirUserCad: TIntegerField;
    QrCNAB_DirUserAlt: TIntegerField;
    QrCNAB_DirNome: TWideStringField;
    QrCNAB_DirEnvio: TSmallintField;
    QrCNAB_DirNOMECARTEIRA: TWideStringField;
    QrCNAB_DirCliInt: TIntegerField;
    QrCNAB_DirCarteira: TIntegerField;
    QrCNAB_DirBanco1: TIntegerField;
    TabSheet8: TTabSheet;
    Memo3: TMemo;
    QrLeiItens: TmySQLQuery;
    QrLeiItensData: TDateField;
    QrLeiItensCarteira: TIntegerField;
    QrLeiItensGenero: TIntegerField;
    QrLeiItensDescricao: TWideStringField;
    QrLeiItensCredito: TFloatField;
    QrLeiItensCompensado: TDateField;
    QrLeiItensSit: TIntegerField;
    QrLeiItensVencimento: TDateField;
    QrLeiItensMez: TIntegerField;
    QrLeiItensFornecedor: TIntegerField;
    QrLeiItensNOMEPROPRIET: TWideStringField;
    QrLeiItensControle: TIntegerField;
    QrLeiItensSub: TSmallintField;
    QrLeiItensTipoCart: TIntegerField;
    QrLeiItensDebito: TFloatField;
    QrLeiItensNotaFiscal: TIntegerField;
    QrLeiItensSerieCH: TWideStringField;
    QrLeiItensDocumento: TFloatField;
    QrLeiItensCliente: TIntegerField;
    QrLeiItensCliInt: TIntegerField;
    QrLeiItensForneceI: TIntegerField;
    QrLeiItensDataDoc: TDateField;
    QrLeiItensApto: TIntegerField;
    QrLeiItensUH: TWideStringField;
    QrLeiItensMez_TXT: TWideStringField;
    QrLeiItensMulta: TFloatField;
    QrLeiItensCOND: TIntegerField;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrLeiSum: TmySQLQuery;
    QrLeiSumCredito: TFloatField;
    QrLeiSumID600: TFloatField;
    QrLeiSumID601: TFloatField;
    QrLeiSumID610: TFloatField;
    QrClientesNOMEENT: TWideStringField;
    QrClientesEntidade: TIntegerField;
    QrClientesCODIGO: TIntegerField;
    frxCrossObject1: TfrxCrossObject;
    QrCNAB_DirNOMEENT: TWideStringField;
    QrCNAB_DirAlterWeb: TSmallintField;
    QrCNAB_DirAtivo: TSmallintField;
    QrLeiItensNOMECART: TWideStringField;
    QrLeiItensCartDest: TIntegerField;
    Panel6: TPanel;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    Label7: TLabel;
    DBEdit4: TDBEdit;
    BtBuffer: TBitBtn;
    PMBuffer: TPopupMenu;
    Limpabuffer1: TMenuItem;
    Adicionadados1: TMenuItem;
    Imprimelista1: TMenuItem;
    frxDsQuery: TfrxDBDataset;
    frxGrade: TfrxReport;
    Ordenarpelonmerodobloqueto1: TMenuItem;
    Query: TABSQuery;
    QueryBloqueto: TIntegerField;
    QueryValTitul: TFloatField;
    QueryValPago: TFloatField;
    QueryValMulta: TFloatField;
    QueryValJuros: TFloatField;
    QueryValOutros: TFloatField;
    QueryValJurMul: TFloatField;
    QueryValERRO: TFloatField;
    QueryValTarifa: TFloatField;
    QueryOcorrCod: TWideStringField;
    QueryOcorrTxt: TWideStringField;
    QueryAtivo: TSmallintField;
    QuerySEQ: TIntegerField;
    Ordenarpeladatadecrdito1: TMenuItem;
    Ordenarpeladatadepagamento1: TMenuItem;
    QueryDataOcorr: TDateField;
    QueryDataQuita: TDateField;
    QueryDataTarif: TDateField;
    QrLocByCed: TmySQLQuery;
    QrLocByCedCNPJ_CPF: TWideStringField;
    CorrigeValTitulopoisobancoenviouerrado1: TMenuItem;
    Panel7: TPanel;
    EdCliInt: TdmkEdit;
    EdNomeEnt: TdmkEdit;
    EdNOMECARTEIRA: TdmkEdit;
    EdCodigo: TdmkEdit;
    EdBanco1: TdmkEdit;
    EdNome: TdmkEdit;
    EdNomeBanco: TdmkEdit;
    QrLeiID_Link: TLargeintField;
    QrCNAB_DirLayoutRem: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GradeASelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure BtCarregaClick(Sender: TObject);
    procedure QrCNAB240_CalcFields(DataSet: TDataSet);
    procedure QrCNAB240_AfterScroll(DataSet: TDataSet);
    procedure Grade1DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure Grade1SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure BtAbertosClick(Sender: TObject);
    procedure QrLeiAfterScroll(DataSet: TDataSet);
    procedure QrLeiBeforeClose(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure ExcluiAtual1Click(Sender: TObject);
    procedure ExcluiSelecionados1Click(Sender: TObject);
    procedure ExcluiTodos1Click(Sender: TObject);
    procedure QrLeiAfterOpen(DataSet: TDataSet);
    procedure BtConciliaClick(Sender: TObject);
    procedure ConciliaAtual1Click(Sender: TObject);
    procedure ConciliaSelecionados1Click(Sender: TObject);
    procedure ConciliaTodos1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrCNAB_DirAfterScroll(DataSet: TDataSet);
    procedure QrLeiItensCalcFields(DataSet: TDataSet);
    procedure QrLeiCalcFields(DataSet: TDataSet);
    procedure DBGLeiDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtAgendaClick(Sender: TObject);
    procedure QrLeiAfterClose(DataSet: TDataSet);
    procedure ExcluiBloquetosnolocalizados1Click(Sender: TObject);
    procedure Alteravalordoitemdearrecadaoselecionado1Click(
      Sender: TObject);
    procedure BtItensClick(Sender: TObject);
    procedure Excluioitemdearrecadaoselecionado1Click(Sender: TObject);
    procedure Ajustavaloresdobloquetoatual1Click(Sender: TObject);
    procedure QrLeiAgrCalcFields(DataSet: TDataSet);
    procedure frxReport1BeforePrint(Sender: TfrxReportComponent);
    procedure BtBufferClick(Sender: TObject);
    procedure Limpabuffer1Click(Sender: TObject);
    procedure PMBufferPopup(Sender: TObject);
    procedure Adicionadados1Click(Sender: TObject);
    procedure frxGradeGetValue(const VarName: string; var Value: Variant);
    procedure Ordenarpelonmerodobloqueto1Click(Sender: TObject);
    procedure QueryCalcFields(DataSet: TDataSet);
    procedure Ordenarpeladatadecrdito1Click(Sender: TObject);
    procedure Ordenarpeladatadepagamento1Click(Sender: TObject);
    procedure CorrigeValTitulopoisobancoenviouerrado1Click(Sender: TObject);
    procedure PnMovimentoClick(Sender: TObject);
    procedure EdNomeBancoChange(Sender: TObject);
  private
    { Private declarations }
    FLinB, FLinA, FLin1, FActiveRowA, FActiveRowB, FActiveRowC,
    FAbertosCli, FAbertosCond: Integer;
    FTempo, FUltim: TDateTime;

    procedure LeArquivos(Diretorio: String; CodDir, LinA: Integer);
    //procedure ReopenOcorreu(Codigo: Integer);
    //
    function GravaItens: Integer;
    function CarregaArquivo(Arquivo: String; LinA: Integer): Boolean;
    //
    function CarregaItensRetorno(LinA, QuemChamou: Integer): Boolean;
    function CarregaItensRetornoA(Arquivo: String;
             SeqDir, SeqArq, Entidade, TamReg, Banco: Integer): Boolean;
    procedure CarregaItensRetorno240(Banco, Entidade, SeqDir, SeqArq: Integer);
    procedure CarregaItensRetorno400(Banco, Entidade, SeqDir, SeqArq: Integer);
    procedure MostraAbertos();
    procedure ReopenCNAB0Lei(Codigo: Integer);
    procedure ExcluirItens(Acao: TSelType);
    procedure ConciliaItens(Acao: TSelType);
    procedure HabilitaBotoes();
    function ErroLinha(Banco, Linha: Integer; Avisa: Boolean): Boolean;
    function CondDeEntidade(const Entidade: Integer;
             var Cond: Integer): Boolean;
    procedure ReopenLeiItens(Controle: Integer);
    procedure ReopenLeiAgr(Data: TDateTime;
              CliInt, ForneceI, Apto, Mez: Integer);
    {
    function LocDadoAll(const Campo, Linha: Integer; const Mensagem: String;
             var Resultado: String): Boolean;
    function LocDado240(const Campo, Linha: Integer; const Mensagem: String;
             var Resultado: String): Boolean;
    }
    procedure InfoTempo(Tempo: TDateTime; Texto: String; Inicial: Boolean);
    function ObtemActiveRowA: Integer;
    procedure ReopenLeiSum();
    procedure ReabreQueryEImprime(Ordenacao: Integer);
    procedure VerificaImplementacoes(Row: Integer);
    procedure ReopenCNAB_Dir();
  public
    { Public declarations }
    FLista: TStrings;
    FLengt: Integer;
  end;

  var
  FmCNAB_Ret1: TFmCNAB_Ret1;

implementation

{$R *.DFM}

uses UnMyObjects, Module, UMySQLModule, UnGOTOy, UnInternalConsts, Principal, SelCod,
  UnBancos, UnFinanceiro, MyDBCheck, LctMudaCart, MyListas, UnDmkABS_PF;


procedure TFmCNAB_Ret1.QrCNAB240_CalcFields(DataSet: TDataSet);
begin
{
  case QrCNAB240TIPOLOTE.Value of
    0: QrCNAB240NOMETIPOLOTE.Value := 'Cheque';
    1: QrCNAB240NOMETIPOLOTE.Value := 'Duplicata';
    else QrCNAB240NOMETIPOLOTE.Value := 'Desconhecido';
  end;
  QrCNAB240DATA3_TXT.Value := Geral.FDT(QrCNAB240Data3.Value, 2);
  QrCNAB240CPF_TXT.Value := Geral.FormataCNPJ_TT(QrCNAB240CPF.Value);
  //////////////////////////////////////////////////////////////////////////////
  QrCNAB240NOMESTATUS.Value := MLAGeral.NomeStatusPgto2(QrCNAB240Quitado.Value,
    QrCNAB240DDeposito.Value, Date, QrCNAB240Data3.Value, QrCNAB240Repassado.Value);
  //////////////////////////////////////////////////////////////////////////////
  QrCNAB240NOMEMOVIMENTO.Value := MLAGeral.CNABTipoDeMovimento(
    QrCNAB240Banco.Value, QrCNAB240Envio.Value,  QrCNAB240Movimento.Value, 0);
  QrCNAB240NOMEENVIO.Value := MLAGeral.CNAB240Envio(QrCNAB240Envio.Value);
  if QrCNAB240Acao.Value = 1 then QrCNAB240NOMEACAO.Value := 'Sim'
  else QrCNAB240NOMEACAO.Value := '';
}  
end;

procedure TFmCNAB_Ret1.FormDestroy(Sender: TObject);
begin
  FLista.Free;
end;

procedure TFmCNAB_Ret1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCNAB_Ret1.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCNAB_Ret1.frxGradeGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_CONDOMINIO' then
    Value := QrCNAB_DirNOMEENT.Value
  else
end;

procedure TFmCNAB_Ret1.frxReport1BeforePrint(Sender: TfrxReportComponent);
var
  Cross: TfrxCrossView;
  i, j: Integer;
begin
  if Sender is TfrxCrossView then
  begin
    Cross := TfrxCrossView(Sender);
    for i := 1 to Grade1.RowCount do
      for j := 1 to Grade1.ColCount do
        Cross.AddValue([i], [j], [Grade1.Cells[j - 1, i - 1]]);
  end;
end;

procedure TFmCNAB_Ret1.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  PnMovimento.Align  := alClient;
  PnArquivos.Align   := alClient;
  PnCarregados.Align := alClient;
  //
  FLengt := 0;
  FLista := TStringList.Create;
  FActiveRowA := 0;
  FActiveRowB := 0;
  FActiveRowC := 0;
  //
  GradeA.ColWidths[00] := 032;
  GradeA.ColWidths[01] := 160;
  GradeA.ColWidths[02] := 272;
  GradeA.ColWidths[03] := 112;
  GradeA.ColWidths[04] := 104;
  GradeA.ColWidths[05] := 044;
  GradeA.ColWidths[06] := 036;
  GradeA.ColWidths[07] := 028;
  GradeA.ColWidths[08] := 064;
  GradeA.ColWidths[09] := 040;
  GradeA.ColWidths[10] := 028;
  GradeA.ColWidths[11] := 036;
  //
  GradeA.Cells[00,00] := 'Seq';
  GradeA.Cells[01,00] := 'Arquivo';
  GradeA.Cells[02,00] := 'Cliente interno';
  GradeA.Cells[03,00] := 'CNPJ / CPF';
  GradeA.Cells[04,00] := 'Cedente';
  GradeA.Cells[05,00] := 'Lotes';
  GradeA.Cells[06,00] := 'Itens';
  GradeA.Cells[07,00] := 'Dir';
  GradeA.Cells[08,00] := 'Cr�ditos';
  GradeA.Cells[09,00] := 'Cliente';
  GradeA.Cells[10,00] := 'Bco';
  GradeA.Cells[11,00] := 'CNAB';
  //
  Grade1.ColWidths[00] := 032;
  Grade1.ColWidths[01] := 068;
  Grade1.ColWidths[02] := 024;
  Grade1.ColWidths[03] := 180;
  Grade1.ColWidths[04] := 056;
  Grade1.ColWidths[05] := 072;
  Grade1.ColWidths[06] := 072;
  Grade1.ColWidths[07] := 064;
  Grade1.ColWidths[08] := 064;
  Grade1.ColWidths[09] := 072;
  Grade1.ColWidths[10] := 064;
  Grade1.ColWidths[11] := 064;
  Grade1.ColWidths[12] := 064;
  Grade1.ColWidths[13] := 064;
  Grade1.ColWidths[14] := 064;
  Grade1.ColWidths[15] := 064;
  Grade1.ColWidths[16] := 072;
  Grade1.ColWidths[17] := 120;
  Grade1.ColWidths[18] := 056;
  Grade1.ColWidths[19] := 056;
  Grade1.ColWidths[20] := 064;
  Grade1.ColWidths[21] := 072;
  Grade1.ColWidths[22] := 024;
  Grade1.ColWidths[23] := 024;
  Grade1.ColWidths[24] := 024;
  Grade1.ColWidths[25] := 024;
  Grade1.ColWidths[26] := 108;
  Grade1.ColWidths[27] := 064;
  Grade1.ColWidths[28] := 064;
  Grade1.ColWidths[29] := 056;
  /////////////
  Grade1.Cells[00, 00] := 'Seq';
  Grade1.Cells[01, 00] := 'Nosso n�m.';
  Grade1.Cells[02, 00] := 'Ocorr�ncia';
  Grade1.Cells[03, 00] := 'Descri��o da ocorr�ncia';
  Grade1.Cells[04, 00] := 'Data ocor.';
  Grade1.Cells[05, 00] := 'Seu n�mero';
  Grade1.Cells[06, 00] := 'Val. t�tulo';
  Grade1.Cells[07, 00] := 'Abatimento';
  Grade1.Cells[08, 00] := 'Desconto';
  Grade1.Cells[09, 00] := 'Val. receb.';
  Grade1.Cells[10, 00] := 'Juros mora';
  Grade1.Cells[11, 00] := 'Multa';
  Grade1.Cells[12, 00] := 'Outros +';
  Grade1.Cells[13, 00] := 'Jur + Mul.';
  Grade1.Cells[14, 00] := 'Tarifa';
  Grade1.Cells[15, 00] := 'ERRO';
  Grade1.Cells[16, 00] := 'M.O.';
  Grade1.Cells[17, 00] := 'Motivos ocorr�ncia';
  Grade1.Cells[18, 00] := 'Dt.lanc.c/c';
  Grade1.Cells[19, 00] := 'Dt.D�b.tarifa';
  Grade1.Cells[20, 00] := 'Entidade';
  Grade1.Cells[21, 00] := 'ID Link';
  Grade1.Cells[22, 00] := 'Dir';
  Grade1.Cells[23, 00] := 'Arq';
  Grade1.Cells[24, 00] := 'Item';
  Grade1.Cells[25, 00] := 'Bco';
  Grade1.Cells[26, 00] := 'Documento';
  Grade1.Cells[27, 00] := 'Val. Bruto';
  Grade1.Cells[28, 00] := 'Outros -';
  Grade1.Cells[29, 00] := 'Vencto';
  //
end;

procedure TFmCNAB_Ret1.LeArquivos(Diretorio: String; CodDir, LinA: Integer);
var
  i, n: Integer;
begin
  try
    if Diretorio[Length(Diretorio)] <> '\' then Diretorio := Diretorio + '\';
   MyObjects.GetAllFiles(False, Diretorio + '*.*', ListBox1, True, Nil, Nil);
    n := LinA;
    for i := 0 to ListBox1.Items.Count -1 do
    begin
      if FileExists(ListBox1.Items[i]) then
      begin
        if Trim(GradeA.Cells[01, n]) <> '' then
          n := n + 1;
        GradeA.RowCount := n + 1;
        GradeA.Cells[00, n] := IntToStr(n);
        GradeA.Cells[01, n] := ExtractFileName(ListBox1.Items[i]);
        GradeA.Cells[07, n] := FormatFloat('000', CodDir);
        //
        if not CarregaArquivo(ListBox1.Items[i], n) then
          Exit;
      end;
    end;
    if Trim(GradeA.Cells[1,1]) <> '' then
    CarregaItensRetorno(1, 1);
    //
    {if GradeB.RowCount > 2 then
      if GradeB.Cells[00, GradeB.RowCount -1] = '' then
        GradeB.RowCount := GradeB.RowCount - 1;
    //
    if GradeC.RowCount > 2 then
      if GradeC.Cells[00, GradeC.RowCount -1] = '' then
        GradeC.RowCount := GradeC.RowCount - 1;
    }
  except
    Geral.MB_Erro('Erro ao ler arquivos!');
    raise;
  end;
end;

procedure TFmCNAB_Ret1.Limpabuffer1Click(Sender: TObject);
begin
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('DROP TABLE Bafer;       ');
  Query.SQL.Add('CREATE TABLE Bafer  (   ');
  Query.SQL.Add('  Bloqueto integer     ,');
  Query.SQL.Add('  ValTitul float       ,');
  Query.SQL.Add('  ValPago float        ,');
  Query.SQL.Add('  ValMulta float       ,');
  Query.SQL.Add('  ValJuros float       ,');
  Query.SQL.Add('  ValOutros float      ,');
  Query.SQL.Add('  ValJurMul float      ,');
  Query.SQL.Add('  ValERRO float        ,');
  Query.SQL.Add('  ValTarifa float      ,');
  Query.SQL.Add('  OcorrCod varchar(10) ,');
  Query.SQL.Add('  OcorrTxt varchar(250),');
  Query.SQL.Add('  DataOcorr date       ,');
  Query.SQL.Add('  DataQuita date       ,');
  Query.SQL.Add('  DataTarif date       ,');
  Query.SQL.Add('  Ativo smallint        ');
  Query.SQL.Add(');                      ');
  Query.SQL.Add('SELECT * FROM bafer;');
  Query.Open;
  //
end;

procedure TFmCNAB_Ret1.GradeASelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  if ARow <> FActiveRowA then
  begin
    FActiveRowA := ARow;
    CarregaItensRetorno(FActiveRowA, 2);
  end;
end;

procedure TFmCNAB_Ret1.Button1Click(Sender: TObject);
var
  i: Integer;
begin
  MemoTam.Lines.Clear;
  for i := 0 to GradeA.ColCount -1 do
    MemoTam.Lines.Add('  GradeA.ColWidths['+FormatFloat('000', i) + '] := ' +
      FormatFloat('0000', GradeA.ColWidths[i])+';');
end;

procedure TFmCNAB_Ret1.GradeADrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  OldAlign: Integer;
begin
  if ARow = 0 then
  begin
    //
  end else if ACol = 0 then begin
    if ARow <> 0 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_CENTER);
      GradeA.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Geral.TFD(GradeA.Cells[Acol, ARow], 3, siPositivo));
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
    end;
  end else if ACol = 01 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_LEFT);
      GradeA.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 02 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_LEFT);
      GradeA.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 03 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_LEFT);
      GradeA.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 04 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_LEFT);
      GradeA.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 05 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_CENTER);
      GradeA.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 06 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_CENTER);
      GradeA.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 07 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_CENTER);
      GradeA.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 08 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_RIGHT);
      GradeA.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 09 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_RIGHT);
      GradeA.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 10 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_CENTER);
      GradeA.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 11 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_CENTER);
      GradeA.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end
end;

{procedure TFmCNAB_Ret.ReopenOcorreu(Codigo: Integer);
begin
  QrOcorreu.Close;
  QrOcorreu.Params[00].AsInteger := QrCNAB240Controle.Value;
  QrOcorreu.Open;
end;
}

procedure TFmCNAB_Ret1.QrCNAB240_AfterScroll(DataSet: TDataSet);
begin
  //ReopenOcorreu(0);
end;

function TFmCNAB_Ret1.CarregaArquivo(Arquivo: String; LinA: Integer): Boolean;
  function ObtemDado(const TamReg, Banco, Campo, Linha: Integer; const Lista:
  TStrings; const Mensagem: String): String;
  begin
    case TamReg of
      240: UBancos.LocDado240(Banco, Campo, Linha, FLista, Mensagem, '', Result);
      400: UBancos.LocDado400(Banco, Campo, Linha, FLista, Mensagem, '', Result);
    end;
  end;
  function ConfereDado(const TamReg, Banco, Campo, Linha: Integer; const Lista:
  TStrings; const Mensagem: String; var Res: String): Boolean;
  var
    Padr: String;
  begin
    Res := ObtemDado(TamReg, Banco, Campo, Linha, Lista, Mensagem);
    Padr := UBancos.ValorPadrao(TamReg, Banco, Campo);
    Result := Res = Padr;
    if not Result then
      Geral.MB_Aviso(Mensagem);
  end;
var
  p, Lin, Banco, IndexReg: Integer;
  s, CodCedente, CNPJ, AgTxt, CeTxt, NomeArq: String;
begin
  Result := False;
  CodCedente := '';
  //Controle := 0;
  if not FileExists(Arquivo) then
  begin
    Geral.MB_Aviso('O arquivo "'+Arquivo+'" n�o foi localizado.'+
    ' Ele pode ter sido exclu�do!');
    Exit;
  end;
  FLista.Clear;
  FLista.LoadFromFile(Arquivo);
  FLengt := -1000;
  if not UBancos.TipoArqCNAB(Arquivo, FLista, FLengt) then Exit;
  {
  k := 0;
  for i := 0 to FLista.Count -1 do
  begin
    n := Length(FLista[i]);
    if (FLengt = -1000) then
      if ( (n=240) or (n=400) ) then
        FLengt := n;
    if (n=0) or (n<>FLengt) then
    begin
      if Geral.MB_Pergunta('O arquivo "'+Arquivo+'" possui '+Geral.FF0(n)+
      ' caracteres na linha '+Geral.FF0(i)+ ' quando o esperado era 240 ou 400.' +
      '. Deseja abortar?') = ID_YES
      then Exit;
      k := k + 1;
    end;
  end;
  if k > 0 then
  begin
    Geral.MB_Erro('O arquivo "'+Arquivo+'" possui '+Geral.FF0(k)+
    ' linhas que possuem quantidade de caracteres diferente de '+Geral.FF0(FLengt)+
    '.');
    Exit;
  end;
  }
  // Verifica o padr�o do registro (240, 400)
  IndexReg := UBancos.TamanhoLinhaCNAB_Index(FLengt);
  if IndexReg = 0 then Exit;
  Banco := Geral.IMV(ObtemDado(FLengt, 0, 001, 0, FLista, ''));
  if Banco = 0 then
  begin
    Geral.MB_Erro('N�o foi poss�vel definir o banco do arquivo "' +
      Arquivo + '". Contate a dermatek caso esse arquivo for realmente de ' +
      'remessa/retorno banc�rio!' + sLineBreak + 'Arquivo de ' +
      Geral.FF0(FLengt) + ' posi��es!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //

  // Tipo de arquivo - Lote de Servico
  if not ConfereDado(FLengt, Banco, 003, 0, FLista,
    'O arquivo "'+Arquivo+'" n�o � lote de servi�o!', s) then Exit;

  // Arquivo de retorno
  if not ConfereDado(FLengt, Banco, 007, 0, FLista,
    'O arquivo "'+Arquivo+'" n�o � arquivo de retorno!', s) then Exit;

{
  // Header de arquivo
  if not ConfereDado(FLengt, Banco, 010, 0, FLista,
    'O registro header n�o foi localizado no arquivo "'+Arquivo+'"!', s) then Exit;

  //Quantidade de registros no arquivo
  if not ConfereDado(FLengt, Banco, 301, 0, FLista,
    'Quantidade de registros detalhe n�o definido no arquivo "'+Arquivo+'"!', s) then Exit;
  //if not MLAGeral.CNABRetRegistrosOK(BcoCod, FLengt, FLista, Arquivo) then Exit;
}

  //////////////////////////////////////////////////////////////////////////////


  // Cliente interno
  // Somente para bancos que informam o CNPJ ou CPF
  if ((Banco = 409) and (FLengt = 400)) 
  or ((Banco = 399) and (FLengt = 400)) then
  begin
    CodCedente := ObtemDado(FLengt, Banco, 410, 0, FLista, '');
    QrLocByCed.Close;
    QrLocByCed.Params[00].AsString  := CodCedente;
    QrLocByCed.Params[01].AsInteger := Banco;
    QrLocByCed.Open;
    case QrLocByCed.RecordCount of
      0:
      begin
        Geral.MB_Aviso('N�o foi localizado carteira para o arquivo "' +
          Arquivo+'" onde consta:' + sLineBreak + 'Banco: "' +
          FormatFloat('000', Banco) + '"' + sLineBreak +
          'C�digo do cedente: "'+ CodCedente + '"' + sLineBreak +
          'Informe o c�digo retorno CNAB na carteira (de extrato) correspondente em:' +
          sLineBreak + 'Cadastros -> Financeiros -> Carteiras.');
        Screen.Cursor := crDefault;
        Exit;
      end;
      1: CNPJ := QrLocByCedCNPJ_CPF.Value;
      else
      begin
        Geral.MB_Aviso('Foram localizados ' +
          Geral.FF0(QrLocEnt2.RecordCount)+' cadastros de carteiras para o arquivo "' +
          Arquivo + '" onde consta:' + sLineBreak + 'Banco: "' +
          FormatFloat('000', Banco) + '"' + sLineBreak +
          'C�digo retorno CNAB: "'+ CodCedente + '"' +
          sLineBreak + '! Para evitar erros nenhum foi considerado!');
        Screen.Cursor := crDefault;
        Exit;
      end;
    end;
  end else
  if Banco = 756 then
  begin
    // Pela Ag�ncia + C�digo do cedente no nome do arquivo
    //
    NomeArq := ExtractFileName(Arquivo);
    p := pos('_', NomeArq);
    //ShowMessage(IntToStr(p));
    if p > 0 then
    begin
      AgTxt := Copy(NomeArq, p-4, 4);
      CeTxt := Copy(NomeArq, p+1, 7);
      //ShowMessage(AgTxt + '/' + CeTxt);
      // Parei Aqui
      QrLocEnt2.Close;
      QrLocEnt2.Params[00].AsInteger := Banco;
      QrLocEnt2.Params[01].AsInteger := Geral.IMV(AgTxt);
      QrLocEnt2.Params[02].AsString  := CeTxt;
      QrLocEnt2.Open;
      //
      case QrLocEnt2.RecordCount of
        0: Geral.MB_Aviso('N�o foi localizado cliente para o arquivo "' +
          Arquivo+'" onde consta:' + sLineBreak + 'Banco: "' +
          FormatFloat('000', Banco) + '"' + sLineBreak +
          'Ag�ncia: "' + AgTxt + '"'  +
          sLineBreak + 'C�digo do cedente: "'+ CeTxt + '"');
        1:
        begin
          // Parei Aqui
          GradeA.Cells[02, linA] := QrLocEnt2NOMEENT.Value;
          GradeA.Cells[03, linA] := Geral.FormataCNPJ_TT(CNPJ);
          GradeA.Cells[04, linA] := CodCedente;
          GradeA.Cells[09, linA] := IntToStr(QrLocEnt2CLIENTE.Value);
          GradeA.Cells[10, linA] := FormatFloat('000', Banco);
          GradeA.Cells[11, linA] := IntToStr(FLengt);
          Result := True;
        end;
        else Geral.MB_Aviso('Foram localizados '+
          Geral.FF0(QrLocEnt2.RecordCount)+' cadastros de clientes para o arquivo "' +
          Arquivo + '" onde consta:' + sLineBreak + 'Banco: "' +
          FormatFloat('000', Banco) + '"' + sLineBreak +
          'Ag�ncia: "' + AgTxt + '"'  +
          sLineBreak + 'C�digo do cedente: "'+ CeTxt + '"' +
          sLineBreak + '! Para evitar erros nenhum foi considerado!');
      end;

    end else Geral.MB_Erro('Foi detectado que o arquivo "' +
      NomeArq + '" pertence ao banco ' + FormatFloat('000', Banco) +
      ', mas o nome n�o est� no formato indicado pelo banco!');
  end else begin
    Lin := -1;
    case FLengt of
      240: Lin := 0;
      400:
      begin
        if Banco = 748 then
          Lin := 0
        else
          Lin := 1;
      end;
    end;
    CNPJ := ObtemDado(FLengt, Banco, 401, Lin, FLista, '');
    if CodCedente = '' then
      CodCedente := ObtemDado(FLengt, Banco, 410, 0, FLista, '');
    if (CNPJ = '') and (CodCedente = '') then
    begin
      if not UBancos.BancoImplementado(Banco, FLengt, ecnabRetorno) then
      begin
        Screen.Cursor := crDefault;
        Exit;
      end;
      Geral.MB_Aviso('N�o foi poss�vel localizar o CNPJ e/ou o ' +
        'c�digo do cedente no arquivo "' + Arquivo + '" onde consta:' + sLineBreak +
        'Banco: "' + Geral.FF0(Banco) + '"' + sLineBreak + 'Sem estes dados n�o � poss�vel '
        + 'definir o cliente interno!');
      Exit;
    end;
  end;
  if (Banco <> 756) then
  begin
    if CNPJ = '' then
    begin
      Geral.MB_Aviso('N�o foi poss�vel obter o CNPJ no arquivo "' + Arquivo +
        '" onde consta:' + sLineBreak + 'Banco: "' + Geral.FF0(Banco) + '"' +
        sLineBreak + 'Sem o CNPJ n�o � poss�vel ' + 'Definir o cliente interno!');
      Exit;
    end;
    QrLocEnt1.Close;
    QrLocEnt1.Params[00].AsString := CNPJ;
    //QrLocEnt1.Params[01].AsString := CodCedente;
    QrLocEnt1.Open;
    case QrLocEnt1.RecordCount of
      0: Geral.MB_Aviso('N�o foi localizado cliente para o arquivo "' +
        Arquivo+'" onde consta:' + sLineBreak + 'CNPJ/CPF: "' +
        Geral.FormataCNPJ_TT(CNPJ) + '"' + sLineBreak +
        //'C�digo do cedente: "'+ CodCedente + '"' + sLineBreak +
        'Banco: "' + IntToStr(Banco) + '"');
      1:
      begin
        GradeA.Cells[02, linA] := QrLocEnt1NOMEENT.Value;
        GradeA.Cells[03, linA] := Geral.FormataCNPJ_TT(CNPJ);
        GradeA.Cells[04, linA] := CodCedente;
        GradeA.Cells[09, linA] := IntToStr(QrLocEnt1CLIENTE.Value);
        GradeA.Cells[10, linA] := FormatFloat('000', Banco);
        GradeA.Cells[11, linA] := IntToStr(FLengt);
        Result := True;
      end;
      else Geral.MB_Aviso('Foram localizados '+
        Geral.FF0(QrLocEnt1.RecordCount)+' cadastros de clientes para o arquivo "' +
        Arquivo+'" onde consta o CNPJ/CPF '+Geral.FormataCNPJ_TT(CNPJ)+
        //' e  c�digo de cedente '+ CodCedente +
        '! Para evitar erros nenhum foi '+ ' considerado!');
    end;
  end;
  //








  Exit;



  // Quantidade de lotes no arquivo
  GradeA.Cells[05, linA] := UBancos.CNABRetQtdeLots(Banco, FLengt, FLista, Arquivo);
  //
  (*
  case BcoCod of
    748: Result := CarregaItensRetornoSicredi(Arquivo,
                   QrCNAB_DirCodigo.Value,
                   Geral.IMV(GradeA.Cells[00, GradeA.RowCount-1]),
                   BcoCod, QrLocEntCLIENTE.Value);
    else
    begin
      Result := False;
    end;
  end;
  *)
end;

procedure TFmCNAB_Ret1.Grade1DrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  OldAlign: Integer;
begin
  if ARow = 0 then
  begin
    //
  end else if ACol = 0 then begin
    if ARow <> 0 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_CENTER);
      Grade1.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Geral.TFD(Grade1.Cells[Acol, ARow], 3, siPositivo));
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
    end;
  end else if ACol = 01 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 02 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 03 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_LEFT);
      Grade1.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 04 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_CENTER);
      Grade1.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 05 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 06 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 07 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 08 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 09 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 10 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 11 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 12 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 13 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 14 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 15 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 16 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_CENTER);
      Grade1.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 17 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_LEFT);
      Grade1.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 18 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_CENTER);
      Grade1.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 18 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_CENTER);
      Grade1.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 20 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 21 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 22 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 23 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 24 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 25 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 26 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_LEFT);
      Grade1.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 27 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 28 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_RIGHT);
      Grade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end else if ACol = 29 then begin
      OldAlign := SetTextAlign(Grade1.Canvas.Handle, TA_CENTER);
      Grade1.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Grade1.Cells[Acol, ARow]);
      SetTextAlign(Grade1.Canvas.Handle, OldAlign);
  end;
end;

procedure TFmCNAB_Ret1.Grade1SelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  (*if ARow <> FActiveRowC then
    FActiveRowC := ARow;
  if ARow <> FActiveRowB then
    FActiveRowB := ARow;*)
  //ShowMessage(IntToStr(ACol));
  //
  if ACol in ([10,11,12,13,14]) then
    Grade1.Options := Grade1.Options + [goEditing]
  else
    Grade1.Options := Grade1.Options - [goEditing];
end;

procedure TFmCNAB_Ret1.BtCarregaClick(Sender: TObject);
begin
  if GravaItens > 0 then
  begin
    BtCarrega.Enabled := FLin1 > 0;
    MostraAbertos();
  end;
end;

function TFmCNAB_Ret1.GravaItens: Integer;
var
  ID_Link, Codigo, Banco, SeuNum, OcorrCodi, Diretorio, ItemArq,
  i, n, a, IDNum, Entidade, Carteira, Dias, Erros, Cond, TamReg: Integer;
  OcorrData, QuitaData, Motivo1, Motivo2, Motivo3, Motivo4, Motivo5, Arquivo,
  Extensao, FileName, DestName, NossoNum, DtaTarif: String;
  ValTitul, ValAbati, ValDesco, ValPago, ValJuros, ValMulta, DevJuros, DevMulta,
  TotJuros, ValJuMul, ValTarif, ValErro, ValOutro, TxaMulta, TxaJuros, JurMul: Double;
  DataPagto: TDateTime;
  Exclui, Continua, InfoJM, SeparaJM: Boolean;
  LayoutRem, Texto: String;
begin
  //tarifa de cobran�a -
  //fazer c�lculo de verifica��o de Val tit + jur + mul - tarifa - desco - abat = val pago
  Erros := 0;
  for i := 1 to Grade1.RowCount - 1 do
  begin
    Banco := Geral.IMV(Grade1.Cells[25, i]);
    if ErroLinha(Banco, i, True) then Inc(Erros, 1);
  end;
  if Erros > 0 then
  begin
    if Geral.MB_Pergunta('Existem ' + Geral.FF0(Erros) +
      ' diverg�ncias no arquivo "' + ExtractFileName(GradeA.Cells[1,
      Geral.IMV(Grade1.Cells[23, 1])]) + '".' + sLineBreak +
      'Como � calculado a diverg�ncia:' + sLineBreak +
      'Valor do t�tulo + Multa + Juros de mora + (Juros e multa somados) + ' +
      'Outros cr�ditos - Abatimentos - Descontos - Despesa de cobran�a (Tarifa) - ' +
      'Valor a creditar em conta corrente' + sLineBreak +
      'Deseja continuar assim mesmo?') <> ID_YES then
    begin
      Result := 0;
      Exit;
    end;
  end;
  Exclui   := True;
  Codigo   := 0;
  Result   := 0;
  DevJuros := 0;
  DevMulta := 0;
  Dias     := 0;
  n        := 0;
  for i := 1 to Grade1.RowCount - 1 do
  begin
    //  pode vir com letra "P" no final no caso do Banco do Brasil
    Texto := Geral.SoNumero_TT(Trim(Grade1.Cells[1, i]));
    if Geral.DMV(Texto) >= 1 then
      n := n + 1;
  end;
  if n = 0 then
  begin
    Geral.MB_Aviso('N�o h� itens a serem gravados. O arquivo ser� ' +
    'movido para a pasta "Vazios"!');
    Arquivo   := ExtractFileName(GradeA.Cells[1, ObtemActiveRowA]);
    Extensao  := '';
    FileName := dmkPF.CaminhoArquivo(QrCNAB_DirNome.Value, Arquivo, Extensao);
    if FileExists(FileName) then
    begin
      DestName := dmkPF.CaminhoArquivo(QrCNAB_DirNome.Value, 'Vazios', '');
      ForceDirectories(DestName);
      dmkPF.MoveArq(PChar(FileName), PChar(DestName));
    end;
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  PB1.Position := 0;
  PB1.Visible := True;
  PB1.Max := Grade1.RowCount - 1 + GradeA.RowCount - 1;
  TamReg := Geral.IMV(GradeA.Cells[11, Geral.IMV(Grade1.Cells[23, 1])]);//FActiveRowA]);
  for a := 1 to Grade1.RowCount - 1 do
  begin
    PB1.Position := PB1.Position + 1;
    Update;
    Application.ProcessMessages;
    //
    NossoNum  := Geral.SoNumero_TT(Grade1.Cells[01, a]);
    OcorrCodi := Geral.IMV(Grade1.Cells[02, a]);
    // Texto da ocorrencia                [03
    OcorrData := dmkPF.CDS(Grade1.Cells[04, a], 2, 1);
    SeuNum    := Geral.IMV(Grade1.Cells[05, a]);
    ValTitul  := Geral.DMV(Grade1.Cells[06, a]);
    ValAbati  := Geral.DMV(Grade1.Cells[07, a]);
    ValDesco  := Geral.DMV(Grade1.Cells[08, a]);
    ValPago   := Geral.DMV(Grade1.Cells[09, a]);
    ValJuros  := Geral.DMV(Grade1.Cells[10, a]);
    ValMulta  := Geral.DMV(Grade1.Cells[11, a]);
    ValOutro  := Geral.DMV(Grade1.Cells[12, a]);
    ValJuMul  := Geral.DMV(Grade1.Cells[13, a]);
    ValTarif  := Geral.DMV(Grade1.Cells[14, a]);
    Motivo1   := Copy(Grade1.Cells[16, a], 1, 2);
    Motivo2   := Copy(Grade1.Cells[16, a], 3, 2);
    Motivo3   := Copy(Grade1.Cells[16, a], 5, 2);
    Motivo4   := Copy(Grade1.Cells[16, a], 7, 2);
    Motivo5   := Copy(Grade1.Cells[16, a], 9, 2);
    // Texto dos motivos da ocorrencia    [13
    QuitaData := dmkPF.CDS(Grade1.Cells[18, a], 2, 1);
    DtaTarif  := dmkPF.CDS(Grade1.Cells[19, a], 2, 1);
    Diretorio := Geral.IMV(Grade1.Cells[22, a]);
    Arquivo   := ExtractFileName(GradeA.Cells[1, StrToInt(Grade1.Cells[22, a])]);
    ItemArq   := Geral.IMV(Grade1.Cells[24, a]);
    Banco     := Geral.IMV(Grade1.Cells[25, a]);
    Entidade  := Geral.IMV(Grade1.Cells[20, a]);
    ID_Link   := Geral.IMV(Grade1.Cells[21, a]);
    Carteira  := QrCNAB_DirCarteira.Value;
    LayoutRem := QrCNAB_DirLayoutRem.Value;
    //
    if not UBancos.SeparaJurosEMultaImplementado(Banco, LayoutRem, SeparaJM) then
    begin
      Screen.Cursor := crDefault;
      PB1.Visible := False;
      Exit;
    end;
    if not SeparaJM then
    begin
      // N�o separa Juros e multa no arquivo.
      // Fazer manual
      if (ValPago > ValTitul) and (ValJuros + ValMulta = 0) then
      begin
        if CondDeEntidade(Entidade, Cond) then
        begin
          ValMulta := Round(QrLocCondPercMulta.Value * ValTitul) / 100;
          JurMul := ValPago - ValTitul;
          if ValMulta > JurMul then
          begin
            ValMulta := JurMul;
            ValJuros := 0;
          end else begin
            ValJuros := JurMul - ValMulta;
            if ValJuros < 0 then
            begin
              // precau��o
              ValMulta := ValMulta + ValJuros;
              ValJuros := 0;
            end;
          end;
        end;
      end;
    end;
    Continua := UBancos.BancoTemEntidade(Banco);
    if not Continua then
    begin
      Screen.Cursor := crDefault;
      PB1.Visible := False;
      Exit;
    end;
    Continua := UBancos.EhCodigoLiquidacao(OcorrCodi, Banco, TamReg, '');
    if not Continua then
    begin
      Continua := OcorrCodi = UBancos.CodigoTarifa(Banco, '');
      // Tentativa de eliminar a tabela BancosLei
      {QrBcocor.Close;
      QrBcocor.Params[00].AsInteger := Banco;
      QrBcocor.Params[01].AsString  := Grade1.Cells[02, a];
      QrBcocor.Open;

      if QrBcocor.RecordCount = 0 then
      begin
        PageControl1.ActivePageIndex := 1;
        Memo1.Lines.Add('O item '+IntToStr(ItemArq)+
          ' do arquivo '+Arquivo+' n�o foi registrado! (Ocorr�ncia n�o cadastrada no cadastro do banco)');
        Application.ProcessMessages;
      end else Continua := QrBcocorCarrega.Value <> 0;}
      if not Continua then
      begin
        PageControl1.ActivePageIndex := 1;
        Memo1.Lines.Add('O item '+IntToStr(ItemArq)+
          ' do arquivo '+Arquivo+' n�o foi registrado! (Ocorr�ncia n�o cadastrada no cadastro do banco)');
        Application.ProcessMessages;
      end;
    end;
    if Continua then
    begin
      IDNum := ID_Link;
      //
      if IDNum = 0 then
      begin
        Exclui := False;
        PageControl1.ActivePageIndex := 1;
        Memo1.Lines.Add('O item '+IntToStr(ItemArq)+
          ' do arquivo '+Arquivo+' n�o foi registrado! (Banco ' +
          FormatFloat('000', Banco) + ' - n�o implementado) > (ID_Link = 0)');
        Application.ProcessMessages;
      end else begin
        QrDupl.Close;
        QrDupl.Params[00].AsInteger := Banco;
        QrDupl.Params[01].AsString  := NossoNum;
        QrDupl.Params[02].AsInteger := SeuNum;
        QrDupl.Params[03].AsInteger := OcorrCodi;
        QrDupl.Params[04].AsString  := Arquivo;
        QrDupl.Params[05].AsInteger := ItemArq;
        QrDupl.Params[06].AsFloat   := ValTitul;
        QrDupl.Params[07].AsFloat   := ValPago;
        QrDupl.Params[08].AsString  := QuitaData;
        QrDupl.Open;
        if QrDupl.RecordCount > 0 then
        begin
          if CkReverter.Checked then
          begin
            Dmod.QrUpd2.SQL.Clear;
            Dmod.QrUpd2.SQL.Add('UPDATE cnab_lei SET Step=0 WHERE Codigo=:P0');
            Dmod.QrUpd2.Params[0].AsInteger := QrDuplCodigo.Value;
            Dmod.QrUpd2.ExecSQL;
          end else begin
            PageControl1.ActivePageIndex := 1;
            Memo1.Lines.Add('O item '+IntToStr(ItemArq)+
              ' do arquivo '+Arquivo+' j� foi registrado anteriormente!');
            Application.ProcessMessages;
          end;
        end else
        begin
          Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB,
            'Livres', 'Controle', 'CNAB_Lei', 'CNAB_Lei', 'Codigo');
          //
          DevJuros := 0;
          DevMulta := 0;
          Continua := UBancos.EhCodigoLiquidacao(OcorrCodi, Banco, TamReg, '');
          if Continua then
          begin
            QrPesq3.Close;
            QrPesq3.Params[00].AsInteger := ID_Link;
            QrPesq3.Params[01].AsInteger := Entidade;
            QrPesq3.Open;
            DataPagto := Geral.ValidaDataSimples(Grade1.Cells[04, a], True);
            if QrPesq3Vencimento.Value < DataPagto then
              Dias := UMyMod.DiasUteis(QrPesq3Vencimento.Value + 1, DataPagto);
            if Dias > 0 then
            begin
              TotJuros := dmkPF.CalculaJuroSimples(QrPesq3PercJuros.Value,
              DataPagto - QrPesq3Vencimento.Value);
              DevJuros := Round(TotJuros * QrPesq3Credito.Value) / 100;
              DevMulta := Round(QrPesq3PercMulta.Value * QrPesq3Credito.Value) / 100;
            end;
          end;
          //
          // Dve ser aqui para evitar erros
          if Continua then
          begin
            Dmod.QrUpd.SQL.Clear;
            Dmod.QrUpd.SQL.Add('INSERT INTO cnab_lei SET ');
            Dmod.QrUpd.SQL.Add('Codigo=:P0, Banco=:P1, NossoNum=:P2, SeuNum=:P3, ');
            Dmod.QrUpd.SQL.Add('OcorrCodi=:P4, OcorrData=:P5, ValTitul=:P6, ');
            Dmod.QrUpd.SQL.Add('ValAbati=:P7, ValDesco=:P8, ValPago=:P9, ');
            Dmod.QrUpd.SQL.Add('ValJuros=:P10, ValMulta=:P11, Motivo1=:P12, ');
            Dmod.QrUpd.SQL.Add('Motivo2=:P13, Motivo3=:P14, Motivo4=:P15, ');
            Dmod.QrUpd.SQL.Add('Motivo5=:P16, QuitaData=:P17, Diretorio=:P18, ');
            Dmod.QrUpd.SQL.Add('Arquivo=:P19, ItemArq=:P20, IDNum=:P21, Entidade=:P22, ');
            Dmod.QrUpd.SQL.Add('Carteira=:P23, DevJuros=:P24, DevMulta=:P25, ');
            Dmod.QrUpd.SQL.Add('ID_Link=:P26, ValJuMul=:P27, ValTarif=:P28, ');
            Dmod.QrUpd.SQL.Add('ValOutro=:P29, DtaTarif=:P30, TamReg=:P31');
            //
            Dmod.QrUpd.Params[00].AsInteger := Codigo;
            Dmod.QrUpd.Params[01].AsInteger := Banco;
            Dmod.QrUpd.Params[02].AsString  := NossoNum;
            Dmod.QrUpd.Params[03].AsInteger := SeuNum;
            Dmod.QrUpd.Params[04].AsInteger := OcorrCodi;
            Dmod.QrUpd.Params[05].AsString  := OcorrData;
            Dmod.QrUpd.Params[06].AsFloat   := ValTitul;
            Dmod.QrUpd.Params[07].AsFloat   := ValAbati;
            Dmod.QrUpd.Params[08].AsFloat   := ValDesco;
            Dmod.QrUpd.Params[09].AsFloat   := ValPago;
            Dmod.QrUpd.Params[10].AsFloat   := ValJuros;
            Dmod.QrUpd.Params[11].AsFloat   := ValMulta;
            Dmod.QrUpd.Params[12].AsString  := Motivo1;
            Dmod.QrUpd.Params[13].AsString  := Motivo2;
            Dmod.QrUpd.Params[14].AsString  := Motivo3;
            Dmod.QrUpd.Params[15].AsString  := Motivo4;
            Dmod.QrUpd.Params[16].AsString  := Motivo5;
            Dmod.QrUpd.Params[17].AsString  := QuitaData;
            Dmod.QrUpd.Params[18].AsInteger := Diretorio;
            Dmod.QrUpd.Params[19].AsString  := Arquivo;
            Dmod.QrUpd.Params[20].AsInteger := ItemArq;
            Dmod.QrUpd.Params[21].AsInteger := IDNum;
            Dmod.QrUpd.Params[22].AsInteger := Entidade;
            Dmod.QrUpd.Params[23].AsInteger := Carteira;
            Dmod.QrUpd.Params[24].AsFloat   := DevJuros;
            Dmod.QrUpd.Params[25].AsFloat   := DevMulta;
            Dmod.QrUpd.Params[26].AsInteger := ID_Link;
            Dmod.QrUpd.Params[27].AsFloat   := ValJuMul;
            Dmod.QrUpd.Params[28].AsFloat   := ValTarif;
            Dmod.QrUpd.Params[29].AsFloat   := ValOutro;
            Dmod.QrUpd.Params[30].AsString  := DtaTarif;
            Dmod.QrUpd.Params[31].AsInteger := TamReg;
            Dmod.QrUpd.ExecSQL;
          end;
        end;
      end;
    end;
  end;

  //

  Arquivo   := ExtractFileName(GradeA.Cells[1, ObtemActiveRowA]);
  Extensao  := '';
  FileName := dmkPF.CaminhoArquivo(QrCNAB_DirNome.Value, Arquivo, Extensao);
  if Exclui then
  begin
    if FileExists(FileName) then
    begin
      //ShowMessage('Arquivo: '+FileName);
      DestName := dmkPF.CaminhoArquivo(QrCNAB_DirNome.Value, 'Lidos', '');
      ForceDirectories(DestName);
      //ShowMessage('Destino: '+DestName);
      dmkPF.MoveArq(PChar(FileName), PChar(DestName));
      //
    end;
  end;
  //

  PB1.Visible := False;
  Screen.Cursor := crDefault;
  Result := Codigo;

  //
  // Recarrega arquivos
  Timer1.Enabled := True;

end;

procedure TFmCNAB_Ret1.BtAbertosClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    MostraAbertos;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCNAB_Ret1.MostraAbertos;
var
  Codigo: Integer;
begin
  Codigo := QrLeiCodigo.Value;
  FAbertosCli  := 0;
  FAbertosCond := 0;
  QrClientes.Close;
  QrClientes.Open;
  case QrClientes.RecordCount of
    0: Geral.MB_Aviso('N�o h� itens abertos!');
    1:
    begin
      FAbertosCli  := QrClientesEntidade.Value;
      FAbertosCond := QrClientesCODIGO.Value;
    end
    else begin
      Application.CreateForm(TFmSelCod, FmSelCod);
      FmSelCod.Caption := 'Condom�nio';
      FmSelCod.LaPrompt.Caption := 'Condom�nio';
      FmSelCod.CBSel.ListSource := nil;
      FmSelCod.CBSel.ListField := 'NOMEENT';
      FmSelCod.CBSel.KeyField  := 'CODIGO';
      FmSelCod.CBSel.ListSource := DsClientes;
      //
      if QrClientes.Locate('Entidade', QrCNAB_DirCliInt.Value, []) then
      begin
        FmSelCod.EdSel.Text := FormatFloat('0', QrClientesCODIGO.Value);
        FmSelCod.CBSel.KeyValue := QrClientesCODIGO.Value;
      end;
      FmSelCod.ShowModal;
      if VAR_SELCOD > 0 then
      begin
        FAbertosCli  := QrClientesEntidade.Value;
        FAbertosCond := QrClientesCODIGO.Value;
      end;
      FmSelCod.Destroy;
    end;
  end;
  if FAbertosCli > 0 then
  begin
    Application.ProcessMessages;
    //
    PnMovimento.Visible := True;
    PnCarrega.Visible := False;
    Application.ProcessMessages;
    //
    ReopenCNAB0Lei(Codigo);
  end;
end;

procedure TFmCNAB_Ret1.ReabreQueryEImprime(Ordenacao: Integer);
var
  Ordem, Texto1, Condicao1: String;
  Agrupa: Boolean;
begin
  Agrupa := False;
  case Ordenacao of
    1:
    begin
      Ordem := 'Bloqueto';
      Agrupa := False;
      Condicao1 := '''frxDsQuery."Bloqueto"''';
      Texto1 := '''[frxDsQuery."Bloqueto"]''';
    end;
    2:
    begin
      Ordem := 'DataQuita';
      Agrupa := True;
      Condicao1 := '''frxDsQuery."DataQuita"''';
      Texto1 := '''Data do cr�dito em conta: [frxDsQuery."DataQuita"]''';
    end;
    3:
    begin
      Ordem := 'DataOcorr';
      Agrupa := True;
      Condicao1 := '''frxDsQuery."DataOcorr"''';
      Texto1 := '''Data do pagamento: [frxDsQuery."DataOcorr"]''';
    end;
  end;
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT * FROM bafer ORDER BY ' + Ordem);
  Query.Open;
  frxGrade.Variables['VARF_G1_VISIBLE'] := Agrupa;
  frxGrade.Variables['VARF_G1_COND'] := Condicao1;
  frxGrade.Variables['VARF_G1_TEXT'] := Texto1;
  //frxGrade.DesignReport;
  MyObjects.frxMostra(frxGrade, 'Bloquetos liquidados');
end;

procedure TFmCNAB_Ret1.ReopenCNAB0Lei(Codigo: Integer);
begin
  QrLei.Close;
  QrLei.Params[0].AsInteger := FAbertosCli;
  QrLei.Open;
  //
  QrLei.Locate('Codigo', Codigo, []);
end;

procedure TFmCNAB_Ret1.ReopenCNAB_Dir;
var
  Codigo, CliInt, Banco1: String;
begin
  QrCNAB_Dir.Close;
  QrCNAB_Dir.SQL.Clear;
  QrCNAB_Dir.SQL.Add('SELECT IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMEENT,');
  QrCNAB_Dir.SQL.Add('car.Nome NOMECARTEIRA, ban.Nome NOMEBANCO,');
  QrCNAB_Dir.SQL.Add('car.Banco1, ent.CliInt, "" LayoutRem, dir.*');
  QrCNAB_Dir.SQL.Add('FROM cnab_dir dir');
  QrCNAB_Dir.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=dir.carteira');
  QrCNAB_Dir.SQL.Add('LEFT JOIN bancos ban ON ban.Codigo=car.Banco1');
  QrCNAB_Dir.SQL.Add('LEFT JOIN cond cnd ON cnd.Cliente=dir.CliInt');
  QrCNAB_Dir.SQL.Add('');
  QrCNAB_Dir.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=dir.CliInt');
  QrCNAB_Dir.SQL.Add('WHERE dir.Envio=2');
  QrCNAB_Dir.SQL.Add('AND dir.Ativo=1');
  //
  if EdCliInt.ValueVariant <> 0 then
  begin
    CliInt := FormatFloat('0', EdCliInt.ValueVariant);
    QrCNAB_Dir.SQL.Add('AND ent.CliInt=' + CliInt);
  end;
  //
  if EdNomeEnt.Text <> '' then
  begin
    QrCNAB_Dir.SQL.Add('AND IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) LIKE "%' +
    EdNomeEnt.Text + '%"');
  end;
  //
  if EdCodigo.ValueVariant <> 0 then
  begin
    Codigo := FormatFloat('0', EdCodigo.ValueVariant);
    QrCNAB_Dir.SQL.Add('AND dir.Codigo=' + Codigo);
  end;
  //
  if EdNOMECARTEIRA.Text <> '' then
  begin
    QrCNAB_Dir.SQL.Add('AND car.Nome LIKE "%' +
    EdNOMECARTEIRA.Text + '%"');
  end;
  //
  if EdBanco1.ValueVariant <> 0 then
  begin
    Banco1 := FormatFloat('0', EdBanco1.ValueVariant);
    QrCNAB_Dir.SQL.Add('AND car.Banco1=' + Banco1);
  end;
  //
  if EdNome.Text <> '' then
  begin
    QrCNAB_Dir.SQL.Add('AND dir.Nome LIKE "%' +
    EdNome.Text + '%"');
  end;
  //
  if EdNomeBanco.Text <> '' then
  begin
    QrCNAB_Dir.SQL.Add('AND ban.Nome LIKE "%' +
    EdNomeBanco.Text + '%"');
  end;
  //
  QrCNAB_Dir.SQL.Add('ORDER BY NOMEENT');

  QrCNAB_Dir.Open;
end;

procedure TFmCNAB_Ret1.QrLeiAfterScroll(DataSet: TDataSet);
begin
  ReopenLeiSum;
  ReopenLeiItens(0);
  ReopenLeiAgr(0, 0, 0, 0, 0);
end;

procedure TFmCNAB_Ret1.ReopenLeiAgr(Data: TDateTime;
  CliInt, ForneceI, Apto, Mez: Integer);
begin
  QrLeiAgr.Close;
  QrLeiAgr.Params[00].AsInteger := QrLeiIDNum.Value;
  QrLeiAgr.Params[01].AsInteger := QrLeiEntidade.Value;
  QrLeiAgr.Open;
  //
  QrLeiAgr.Locate('Data;CliInt;ForneceI;Apto;Mez',
    VarArrayOf([Data, CliInt, ForneceI, Apto, Mez]), []);
end;

procedure TFmCNAB_Ret1.ReopenLeiItens(Controle: Integer);
var
  OcorCodi: Integer;
begin
  QrLeiItens.Close;
  OcorCodi := Geral.IMV(QrLeiOcorrCodi.Value);
  // Liquida��o
  if UBancos.EhCodigoLiquidacao(OcorCodi, QrLeiBanco.Value, QrLeiTamReg.Value, '')
  // vale para bancos 001 e 748; mais algum?

  //Todos?
  {and (
       (QrLeiBanco.Value = 1)
    or (QrLeiBanco.Value = 748)
      )}
  then
  begin
    QrLeiItens.Params[00].AsInteger := QrLeiIDNum.Value;
    QrLeiItens.Params[01].AsInteger := FAbertosCli;
    QrLeiItens.Params[02].AsInteger := FAbertosCond;
    QrLeiItens.Open;
    QrLeiItens.Locate('Controle', Controle, []);
    //
  end;
  BtAgenda.Enabled := QrLeiDJM.Value < 0;
end;

procedure TFmCNAB_Ret1.ReopenLeiSum;
begin
  QrLeiSum.Close;
  QrLeiSum.Params[00].AsInteger := QrLeiIDNum.Value;;
  QrLeiSum.Params[01].AsInteger := FAbertosCli;
  QrLeiSum.Params[02].AsInteger := FAbertosCond;
  QrLeiSum.Open;
  //

  {
  QrLeiVALBOLETO.Value := QrLeiSumCredito.Value;
  //
  QrLeiQTD_600.Value := Trunc(QrLeiSumID600.Value);
  QrLeiQTD_601.Value := Trunc(QrLeiSumID601.Value);
  QrLeiQTD_610.Value := Trunc(QrLeiSumID610.Value);
  }
end;

procedure TFmCNAB_Ret1.QrLeiBeforeClose(DataSet: TDataSet);
begin
  QrLeiItens.Close;
  BtConcilia.Enabled := False;
end;

procedure TFmCNAB_Ret1.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmCNAB_Ret1.ExcluiAtual1Click(Sender: TObject);
begin
  ExcluirItens(istAtual);
end;

procedure TFmCNAB_Ret1.ExcluiSelecionados1Click(Sender: TObject);
begin
  ExcluirItens(istSelecionados);
end;

procedure TFmCNAB_Ret1.ExcluiTodos1Click(Sender: TObject);
begin
  ExcluirItens(istTodos);
end;

procedure TFmCNAB_Ret1.ExcluiBloquetosnolocalizados1Click(Sender: TObject);
begin
  ExcluirItens(istExtra1);
end;

procedure TFmCNAB_Ret1.ExcluirItens(Acao: TSelType);
  procedure ExcluiAtual;
  begin
    Screen.Cursor := crHourGlass;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM cnab_lei WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrLeiCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    Screen.Cursor := crDefault;
  end;
var
  Prox, i: Integer;
begin
  if Acao = istSelecionados then
    if DBGLei.SelectedRows.Count < 2 then Acao := istAtual;
  case Acao of
    istAtual:
    begin
      if Geral.MB_Pergunta('Confirma a exclus�o do item selecionado?') = ID_YES then ExcluiAtual;
    end;
    istSelecionados:
    begin
      if Geral.MB_Pergunta('Confirma a exclus�o dos ' +
        Geral.FF0(DBGLei.SelectedRows.Count) + ' itens selecionados?') = ID_YES then
      begin
        with DBGLei.DataSource.DataSet do
        for i:= 0 to DBGLei.SelectedRows.Count-1 do
        begin
          GotoBookmark(pointer(DBGLei.SelectedRows.Items[i]));
          ExcluiAtual;
        end;
      end;
    end;
    istTodos:
    begin
      if Geral.MB_Pergunta('Confirma a exclus�o de todos itens ?') = ID_YES then
      begin
        QrLei.First;
        while not QrLei.Eof do
        begin
          ExcluiAtual;
          QrLei.Next;
        end;
      end;
    end;
    istExtra1:
    begin
      if Geral.MB_Pergunta('Confirma a exclus�o de todos boletos n�o localizados?') = ID_YES then
      begin
        QrLei.First;
        while not QrLei.Eof do
        begin
          if (QrLeiItens.State = dsInactive)
          or (QrLeiItens.RecordCount = 0) then
            ExcluiAtual;
          QrLei.Next;
        end;
      end;
    end;
  end;
  //
  Prox := UMyMod.ProximoRegistro(QrLei, 'Codigo', QrLeiCodigo.Value);
  ReopenCNAB0Lei(Prox);
end;

procedure TFmCNAB_Ret1.QrLeiAfterOpen(DataSet: TDataSet);
begin
    BtConcilia.Enabled := QrLei.RecordCount > 0;
end;

procedure TFmCNAB_Ret1.BtConciliaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMConcilia, BtConcilia);
end;

procedure TFmCNAB_Ret1.ConciliaAtual1Click(Sender: TObject);
begin
  ConciliaItens(istAtual);
end;

procedure TFmCNAB_Ret1.ConciliaSelecionados1Click(Sender: TObject);
begin
  ConciliaItens(istSelecionados);
end;

procedure TFmCNAB_Ret1.ConciliaTodos1Click(Sender: TObject);
begin
  ConciliaItens(istTodos);
end;

procedure TFmCNAB_Ret1.ConciliaItens(Acao: TSelType);
  function EntidadeDeBanco(var EntBanco: Integer; const Banco: Integer): Boolean;
  begin
    QrLEB.Close;
    QrLEB.Params[0].AsInteger := Banco;
    QrLEB.Open;
    EntBanco := QrLEBEntidade.Value;
    Result := EntBanco > 0;
    if not Result then Geral.MB_Erro('Entidade banc�ria n�o ' +
      'localizada para o banco: ' + FormatFloat('000', Banco)+'!');
    if not Result then Screen.Cursor := crDefault;
  end;
  function UnidadeHabitacional(var Dono: Integer; var Unidade: Integer;
    const IDNum: Integer; const (*T600, T601,*) T610: Integer): Boolean;
  begin
    if T610 > 0 then
    begin
      QrLUH.Close;
      QrLUH.SQL.Clear;
      QrLUH.SQL.Add('SELECT bp.CodigoEnt Propriet, bp.CodigoEsp Apto');
      QrLUH.SQL.Add('FROM bloqparcpar bpp');
      QrLUH.SQL.Add('LEFT JOIN bloqparc bp ON bp.Codigo=bpp.Codigo');
      QrLUH.SQL.Add('LEFT JOIN ' + VAR_LCT  + ' lan ON lan.FatNum=bpp.FatNum');
      QrLUH.SQL.Add('  AND lan.ForneceI=bp.CodigoEnt');
      QrLUH.SQL.Add('  AND lan.Depto=bp.CodigoEsp');
      QrLUH.SQL.Add('WHERE lan.Sit < 2');
      QrLUH.SQL.Add('AND lan.Tipo=2');
      QrLUH.SQL.Add('AND bpp.FatNum=' + IntToStr(IDNum));
    end else begin
      QrLUH.Close;
      QrLUH.SQL.Clear;
      QrLUH.SQL.Add('SELECT DISTINCT Apto, Propriet');
      QrLUH.SQL.Add('FROM arreits');
      QrLUH.SQL.Add('WHERE Boleto=' + IntToStr(IDNum));
      QrLUH.SQL.Add('');
      QrLUH.SQL.Add('UNION');
      QrLUH.SQL.Add('');
      QrLUH.SQL.Add('SELECT DISTINCT Apto, Propriet');
      QrLUH.SQL.Add('FROM consits');
      QrLUH.SQL.Add('WHERE Boleto=' + IntToStr(IDNum));
      QrLUH.SQL.Add('');
      //QrLUH.Params[0].AsInteger := IDNum;
      //QrLUH.Params[1].AsInteger := IDNUm;
    end;
    QrLUH.Open;
    Unidade := QrLUHApto.Value;
    Dono := QrLUHPropriet.Value;
    Result := (Unidade > 0) and (Dono <> 0);
    if not Result then
    begin
      Unidade := QrLeiItensApto.Value;
      Dono    := QrLeiItensForneceI.Value;
      Result := (Unidade > 0) and (Dono <> 0);
      if not Result then
      begin
        Unidade := QrLeiAgrApto.Value;
        Dono    := QrLeiAgrForneceI.Value;
        Result := (Unidade > 0) and (Dono <> 0);
      end;
    end;
    if not Result then Geral.MB_Erro('Unidade habitacional n�o ' +
      'localizada para o "IDNum": '+ Geral.FF0(IDNum)+'!');
    if not Result then Screen.Cursor := crDefault;
  end;
  procedure ConciliaAtual;
    {function ContaDaOcorrencia(const Banco: Integer;
      const Ocorrencia: String; var Genero: Integer): Boolean;
    begin
      Genero := 0;
      QrLocOcor.Close;
      QrLocOcor.Params[0].AsInteger := Banco;
      QrLocOcor.Params[1].AsInteger := Geral.IMV(Ocorrencia);
      QrLocOcor.Open;
      if QrLocOcor.RecordCount = 0 then Geral.MB_Aviso(
      'N�o h� cadastro da ocorr�ncia ' + Ocorrencia +
      ' no banco ' + FormatFloat('000', Banco) + '!') else if QrLocOcorGenero.Value < 1 then
      Geral.MB_Aviso('N�o foi definida nenhuma conta no ' +
      'cadastro da ocorr�ncia ' + Ocorrencia +
      ' no banco ' + FormatFloat('000', Banco) + '!') else Genero := QrLocOcorGenero.Value;
      Result := Genero > 0;
      if not Result then Screen.Cursor := crDefault;
      // N�o fechar QrLocOcor !!!!
      //QrLocOcor.Close;
    end;}
    function QuitaDocumento(Credito, MoraVal, MultaVal: Double): Integer;
    var
      Controle2: Integer;
      Compensado, PagoBanco: String;
    begin
      Result := 0;
      PagoBanco := Geral.FDT(QrLeiOcorrData.Value, 1);
      Compensado := Geral.FDT(QrLeiQuitaData.Value, 1);
      //
      Controle2 := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
        'Controle', VAR_LCT, VAR_LCT, 'Controle');
      UFinanceiro.LancamentoDefaultVARS;
      //

      // CNAB_Sit = 2 -> Baixa pelo CNAB
      FLAN_Tipo           := 1;
      FLAN_Sit            := 3;
      FLAN_CNAB_Sit       := 2;
      //
      FLAN_Data           := Compensado;
      FLAN_Controle       := Controle2;
      FLAN_Descricao      := QrLeiItensDescricao.Text;
      FLAN_NotaFiscal     := QrLeiItensNotaFiscal.Value;
      FLAN_Debito         := QrLeiItensDebito.Value;
      FLAN_Credito        := Credito;
      FLAN_Compensado     := PagoBanco; // antes de compensar (D+ do banco)
      FLAN_SerieCH        := QrLeiItensSerieCH.Value;
      FLAN_Documento      := Trunc(QrLeiItensDocumento.Value + 0.01);
      FLAN_Cliente        := QrLeiItensCliente.Value;
      FLAN_Fornecedor     := QrLeiItensFornecedor.Value;
      FLAN_ID_Pgto        := QrLeiItensControle.Value;
      FLAN_Sub            := QrLeiItensSub.Value;
      FLAN_DataCad        := FormatDateTime(VAR_FORMATDATE, Date);
      FLAN_UserCad        := VAR_USUARIO;
      FLAN_DataDoc        := FormatDateTime(VAR_FORMATDATE, QrLeiItensDataDoc.Value);
      FLAN_Vencimento     := FormatDateTime(VAR_FORMATDATE, QrLeiItensVencimento.Value);
      FLAN_Carteira       := QrLeiItensCartDest.Value;
      FLAN_CliInt         := QrLeiItensCliInt.Value;
      FLAN_ForneceI       := QrLeiItensForneceI.Value;
      FLAN_Depto          := QrLeiItensApto.Value;
      FLAN_Genero         := QrLeiItensGenero.Value;
      FLAN_MoraVal        := MoraVal;
      FLAN_MultaVal       := MultaVal;
      FLAN_Mez            :=Geral.TFT_NULL(IntToStr(QrLeiItensMez.Value), 0, siNegativo);
      //
      {
      Dmod.QrUpdM.Params[00].AsString  := Compensado;
      Dmod.QrUpdM.Params[01].AsFloat   := Controle2;
      Dmod.QrUpdM.Params[02].AsString  := QrLeiItensDescricao.Text;
      Dmod.QrUpdM.Params[03].AsInteger := QrLeiItensNotaFiscal.Value;
      Dmod.QrUpdM.Params[04].AsFloat   := QrLeiItensDebito.Value;
      Dmod.QrUpdM.Params[05].AsFloat   := Credito;
      Dmod.QrUpdM.Params[06].AsString  := PagoBanco; // antes de compensar (D+ do banco)
      Dmod.QrUpdM.Params[07].AsString  := QrLeiItensSerieCH.Value;
      Dmod.QrUpdM.Params[08].AsFloat   := QrLeiItensDocumento.Value;
      Dmod.QrUpdM.Params[09].AsInteger := QrLeiItensCliente.Value;
      Dmod.QrUpdM.Params[10].AsInteger := QrLeiItensFornecedor.Value;
      Dmod.QrUpdM.Params[11].AsFloat   := QrLeiItensControle.Value;
      Dmod.QrUpdM.Params[12].AsInteger := QrLeiItensSub.Value;
      Dmod.QrUpdM.Params[13].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
      Dmod.QrUpdM.Params[14].AsInteger := VAR_USUARIO;
      Dmod.QrUpdM.Params[15].AsString  := FormatDateTime(VAR_FORMATDATE, QrLeiItensDataDoc.Value);
      Dmod.QrUpdM.Params[16].AsString  := FormatDateTime(VAR_FORMATDATE, QrLeiItensVencimento.Value);
      Dmod.QrUpdM.Params[17].AsInteger := QrLeiItensCartDest.Value;
      Dmod.QrUpdM.Params[18].AsInteger := QrLeiItensCliInt.Value;
      Dmod.QrUpdM.Params[19].AsInteger := QrLeiItensForneceI.Value;
      Dmod.QrUpdM.Params[20].AsInteger := QrLeiItensApto.Value;
      Dmod.QrUpdM.Params[21].AsInteger := QrLeiItensGenero.Value;
      Dmod.QrUpdM.Params[22].AsFloat   := MoraVal;
      Dmod.QrUpdM.Params[23].AsFloat   := MultaVal;
      Dmod.QrUpdM.Params[24].AsInteger := QrLeiItensMez.Value;
      //
      Dmod.QrUpdM.ExecSQL;
      }
      UFinanceiro.InsereLancamento;
      Result := Result + 1;

      Dmod.QrUpdM.SQL.Clear;
      Dmod.QrUpdM.SQL.Add('UPDATE ' + VAR_LCT  + ' SET AlterWeb=1, Sit=3, ');
      Dmod.QrUpdM.SQL.Add('Compensado=:P0, DataAlt=:P1, UserAlt=:P2');
      Dmod.QrUpdM.SQL.Add('WHERE Controle=:P3 AND Sub=:P4 AND Tipo=2');
      Dmod.QrUpdM.Params[00].AsString  := Compensado;
      Dmod.QrUpdM.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
      Dmod.QrUpdM.Params[02].AsInteger := VAR_USUARIO;
      Dmod.QrUpdM.Params[03].AsFloat   := QrLeiItensControle.Value;
      Dmod.QrUpdM.Params[04].AsInteger := QrLeiItensSub.Value;
      Dmod.QrUpdM.ExecSQL;
      Result := Result + 2;
      //
    end;
    //

  var
    Dono, Unidade, Ocorrencia, Genero, EntBanco, SomaQ, Cond: Integer;
    Fator, SomaVal, Valor, Multa, Juros, Diferenca, FatorM{, FatorJ}, ValTarif: Double;
    InfoTar: Boolean;
    OcorrTxt: String;
  begin
    InfoTempo(Now, 'Nova oncilia��o', True);
    if not EntidadeDeBanco(EntBanco, QrLeiBanco.Value) then Exit;
    InfoTempo(Now, 'Entidade de banco', False);
    if not UnidadeHabitacional(Dono, Unidade, QrLeiIDNum.Value,
      Trunc(QrLeiSumID610.Value)) then Exit;
    InfoTempo(Now, 'Unidade habitacional', False);

    //
    // Verifica antes de conciliar se a conta da ocorr�ncia
    // j� est� configurada
    UBancos.InformaTarifaDeCobrancaImplementado(QrLeiBanco.Value, InfoTar);
    // Quando a tarifa vem na mesma linha do pagamento ...
    if (QrLeiValTarif.Value > 0)
    // ou quando o banco (756) n�o informa no arquivo mas � informado
    // o valor no cadastro do condom�nio (QrCond)
    or (InfoTar = False) then
    begin
      Genero := 0;
      OcorrTxt := QrLeiOcorrCodi.Value;//-1;
      if not UBancos.ContaDaOcorrencia(QrLeiBanco.Value,
      QrLeiTamReg.Value, OcorrTxt, '', Genero) then  Exit;
    end;
    InfoTempo(Now, 'Conta da ocorr�ncia', False);
    //

    Screen.Cursor := crHourGlass;
    Ocorrencia := Geral.IMV(QrLeiOcorrCodi.Value);
    EntBanco := 0;
    if UBancos.EhCodigoLiquidacao(Ocorrencia, QrLeiBanco.Value, QrLeiTamReg.Value, '') then
    begin
      InfoTempo(Now, 'C�digo da liquida��o', False);
      if QrLeiSumCredito.Value <= 0 then
      begin
        Geral.MB_Erro('Valor do boleto zerado para o IDNum = '+
          Geral.FF0(QrLeiIDNum.Value)+'!');
        Screen.Cursor := crDefault;
        Exit;
      end;
      if (QrLeiSumCredito.Value <> QrLeiValTitul.Value)
      and (QrLeiValTitul.Value > 0) then
      begin
        Geral.MB_Erro('Valores n�o conferem para o IDNum = '+
          Geral.FF0(QrLeiIDNum.Value) + '!' + sLineBreak +
          'Se este bloqueto foi re-gerado pela internet com juros e multa,' + sLineBreak +
          'ou se o banco cobrou juros/multa e n�o informou nos campos' + sLineBreak +
          'corretos, utilize a op��o "Ajusta valores do bloqueto atual"' + sLineBreak +
          'do bot�o "Bloqueto" antes de conciliar este bloqueto!');
        Screen.Cursor := crDefault;
        Exit;
      end;
      UBancos.FatorDeRecebimento(QrLeiBanco.Value, QrLeiValPago.Value,
        QrLeiValTarif.Value, QrLeiSumCredito.Value, Fator);
      InfoTempo(Now, 'Fator de recebimento', False);
      QrLeiItens.First;
      SomaVal := 0;
      while not QrLeiItens.Eof do
      begin
        Valor := Round(Fator * QrLeiItensCredito.Value) / 100;
        SomaVal  := SomaVal + Valor;
        //
        QrLeiItens.Next;
      end;
      UBancos.DiferencaDeRecebimento(QrLeiBanco.Value, QrLeiValPago.Value,
        QrLeiValTarif.Value, SomaVal, Diferenca);
      {Geral.MB_Aviso('Diferen�a: '+Geral.FFT(
        Diferenca, 4, siNegativo));}
      //
      if QrLeiValMulta.Value < 0.01 then FatorM := 0 else
      UBancos.FatorMultaDeRecebimento(QrLeiBanco.Value, QrLeiValPago.Value,
        QrLeiValTarif.Value, QrLeiValMulta.Value, FatorM);
      //
      SomaQ := 0;
      InfoTempo(Now, 'Total, diferen�a e fator de multa', False);
      QrLeiItens.First;
      while not QrLeiItens.Eof do
      begin
        Valor := Round(Fator * QrLeiItensCredito.Value) / 100;
        if QrLeiItens.RecNo = 1 then Valor := Valor + Diferenca;

        //
        Multa := 0;
        Juros := 0;
        // Quando juros e multa s�o juntos
        if QrLeiValJuMul.Value >= 0.01 then
        begin
          Multa := Round(QrLeiItensCredito.Value * QrLeiItensMulta.Value) / 100;
          Juros := Valor - Multa - QrLeiItensCredito.Value;
          if Juros  < 0 then
          begin
            Juros := 0;
            Multa := Valor - QrLeiItensCredito.Value;
          end;
        end else
        if  (QrLeiValMulta.Value >= 0.01)
        and (QrLeiValJuros.Value >= 0.01) then
        begin
          Multa := Valor * FatorM;
          Juros := Valor - Multa - QrLeiItensCredito.Value;
        end else if QrLeiValMulta.Value <> 0 then
          Multa := Valor - QrLeiItensCredito.Value
        else
          Juros := Valor - QrLeiItensCredito.Value;
        //
        SomaQ := SomaQ + QuitaDocumento(Valor, Juros, Multa);
        InfoTempo(Now, 'Quita documento', False);
        QrLeiItens.Next;
      end;
      //

      // INSERE TARIFA DE COBRAN�A BANC�RIA
      //InfoTar := UBancos.InformaTarifaDeCobranca(QrLeiBanco.Value);
      // Quando a tarifa vem na mesma linha do pagamento ...
      if (QrLeiValTarif.Value > 0)
      // ou quando o banco (756) n�o informa no arquivo mas � informado
      // o valor no cadastro do condom�nio (QrCond)
      or (InfoTar = False) then
      begin
        Genero := 0;
        OcorrTxt := QrLeiOcorrCodi.Value;//-1;
        Unidade := 0;
        Dono    := 0;
        if not UBancos.ContaDaOcorrencia(QrLeiBanco.Value,
        QrLeiTamReg.Value, OcorrTxt, '', Genero) then
        begin
          Exit;
          Screen.Cursor := crDefault;
        end;
        //fatid: como saber? pode ter varios!
        // J� feito acima
        {
        if not UnidadeHabitacional(Dono, Unidade, QrLeiIDNum.Value,
        QrLeiTIPO_BOL.Value) then
        begin
          Exit;
          Screen.Cursor := crDefault;
        end;
        }
        QrLocCta.Close;
        QrLocCta.Params[0].AsInteger := Genero;
        QrLocCta.Open;
        if QrLocCtaMensal.Value <> 'V' then FLAN_Mez := '' else
        FLAN_Mez :=
          IntToStr(dmkPF.DataToAnoMes(QrLeiQuitaData.Value));
        //
        if QrLeiDtaTarif.Value > 0 then
        begin
          FLAN_DataDoc    := Geral.FDT(QrLeiOcorrData.Value, 1);
          FLAN_Data       := Geral.FDT(QrLeiDtaTarif.Value, 1);
          FLAN_Vencimento := Geral.FDT(QrLeiDtaTarif.Value, 1);
          FLAN_DataCad    := Geral.FDT(Date, 1);
        end else begin
          FLAN_DataDoc    := Geral.FDT(QrLeiOcorrData.Value, 1);
          FLAN_Data       := Geral.FDT(QrLeiQuitaData.Value, 1);
          FLAN_Vencimento := Geral.FDT(QrLeiQuitaData.Value, 1);
          FLAN_DataCad    := Geral.FDT(Date, 1);
        end;
        if InfoTar then ValTarif := QrLeiValTarif.Value else
        begin
        //if
          CondDeEntidade(QrLeiEntidade.Value, Cond); //then
          ValTarif := QrLocCondVTCBBNITAR.Value;
        end;
        FLAN_Descricao  := QrLeiDescriCNR.Value;
        FLAN_Compensado := Geral.FDT(QrLeiQuitaData.Value, 1);
        //FLAN_Duplicata  := '';
        FLAN_Doc2       := IntToStr(QrLeiIDNum.Value);
        //FLAN_Serie      := '';
        FLAN_Documento  := QrLeiIDNum.Value;
        FLAN_Tipo       := QrLeiTipoCart.Value;
        FLAN_Carteira   := QrLeiItensCartDest.Value;
        FLAN_Genero     := Genero;
        //FLAN_NotaFiscal := 0;
        FLAN_Sit        := 3;
        FLAN_Controle   := 0;
        //
        FLAN_Credito    := 0;
        FLAN_Cliente    := 0;
        //
        FLAN_Debito     := ValTarif;
        FLAN_Fornecedor := EntBanco;
        //
        FLAN_UserCad    := VAR_USUARIO;
        FLAN_CliInt     := QrLeiEntidade.Value;
        FLAN_Depto      := Unidade;
        FLAN_ForneceI   := Dono;
        FLAN_FatID      := 0;
        FLAN_FatID_Sub  := 0;
        FLAN_FatNum     := 0;
        FLAN_FatParcela := 0;
        //
        FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB,
          'Livres', 'Controle', VAR_LCT, VAR_LCT, 'Controle');
        //
        if UFinanceiro.InsereLancamento then ;
        InfoTempo(Now, 'Tarifa de cobran�a', False);
      end;

      if SomaQ = QrLeiItens.RecordCount * 3 then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE cnab_lei SET Step=1 ');
        Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := QrLeiCodigo.Value;
        Dmod.QrUpd.ExecSQL;
      end;
    end else begin
      Genero := 0;
      if not UBancos.ContaDaOcorrencia(QrLeiBanco.Value, QrLeiTamReg.Value,
      QrLeiOcorrCodi.Value, '', Genero) then
      begin
        Exit;
        Screen.Cursor := crDefault;
      end;
      InfoTempo(Now, 'Conta da ocorr�ncia', False);
      Unidade := 0;
      Dono    := 0;
      if not UnidadeHabitacional(Dono, Unidade, QrLeiIDNum.Value,
        Trunc(QrLeiSumID610.Value)) then
      begin
        Exit;
        Screen.Cursor := crDefault;
      end;
      InfoTempo(Now, 'Unidade habitacional', False);







      case Ocorrencia of
        028:
        begin
          case QrLeiBanco.Value of
            748:
            begin
              QrLocCta.Close;
              QrLocCta.Params[0].AsInteger := Genero;
              QrLocCta.Open;
              //
              if QrLocCtaMensal.Value <> 'V' then FLAN_Mez := '' else
              FLAN_Mez :=
                IntToStr(dmkPF.DataToAnoMes(QrLeiQuitaData.Value));
              //
              FLAN_Data       := Geral.FDT(QrLeiQuitaData.Value, 1);
              FLAN_Vencimento := Geral.FDT(QrLeiQuitaData.Value, 1);
              FLAN_DataCad    := Geral.FDT(QrLeiOcorrData.Value, 1);

              FLAN_Descricao  := QrLeiDescriCNR.Value;
              FLAN_Compensado := Geral.FDT(QrLeiQuitaData.Value, 1);
              //FLAN_Duplicata  := '';
              FLAN_Doc2       := IntToStr(QrLeiIDNum.Value);
              //FLAN_Serie      := '';
              FLAN_Documento  := QrLeiIDNum.Value;
              FLAN_Tipo       := QrLeiTipoCart.Value;
              FLAN_Carteira   := QrLeiItensCartDest.Value;
              FLAN_Genero     := Genero;
              //FLAN_NotaFiscal := 0;
              FLAN_Sit        := 3;
              FLAN_Controle   := 0;
              //
              FLAN_Credito    := 0;
              FLAN_Cliente    := 0;
              //
              FLAN_Debito     := QrLeiValTitul.Value;
              FLAN_Fornecedor := EntBanco;
              //
              FLAN_UserCad    := VAR_USUARIO;
              FLAN_DataDoc    := Geral.FDT(QrLeiOcorrData.Value, 1);
              FLAN_CliInt     := QrLeiEntidade.Value;
              FLAN_Depto      := Unidade;
              FLAN_ForneceI   := Dono;
              FLAN_FatID      := 0;
              FLAN_FatID_Sub  := 0;
              FLAN_FatNum     := 0;
              FLAN_FatParcela := 0;
              //
              FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB,
                'Livres', 'Controle', VAR_LCT, VAR_LCT, 'Controle');
              //
              if UFinanceiro.InsereLancamento then
              begin
                Dmod.QrUpd.SQL.Clear;
                Dmod.QrUpd.SQL.Add('UPDATE cnab_lei SET Step=1 ');
                Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0');
                Dmod.QrUpd.Params[0].AsInteger := QrLeiCodigo.Value;
                Dmod.QrUpd.ExecSQL;
              end;
              InfoTempo(Now, 'Ocorr�ncia', False);
            end;
            else Geral.MB_Aviso('Este banco n�o possui a��o ' +
            'definida no aplicativo para a ocorr�cia informada!');
          end;
        end else Geral.MB_Aviso('Banco sem a��es definidas!');
      end;
    end;
    Screen.Cursor := crDefault;
  end;
var
  Prox, i: Integer;
begin
  Memo3.Lines.Clear;
  VAR_NaoReabrirLct := True;
  try
    if Acao = istSelecionados then
      if DBGLei.SelectedRows.Count < 2 then Acao := istAtual;
    case Acao of
      istAtual:
      begin
        if Geral.MB_Pergunta('Confirma a concilia��o do item selecionado?') = ID_YES then
          ConciliaAtual;
      end;
      istSelecionados:
      begin
        if Geral.MB_Pergunta('Confirma a concilia��o dos ' + Geral.FF0(
          DBGLei.SelectedRows.Count) + ' itens selecionados?') = ID_YES then
        begin
          with DBGLei.DataSource.DataSet do
          for i:= 0 to DBGLei.SelectedRows.Count-1 do
          begin
            GotoBookmark(pointer(DBGLei.SelectedRows.Items[i]));
            ConciliaAtual;
          end;
        end;
      end;
      istTodos:
      begin
        if Geral.MB_Pergunta('Confirma a concilia��o de todos itens ?') = ID_YES then
        begin
          QrLei.First;
          while not QrLei.Eof do
          begin
            ConciliaAtual;
            QrLei.Next;
          end;
        end;
      end;
    end;
  finally
    VAR_NaoReabrirLct := False;
    UFinanceiro.RecalcSaldoCarteira(QrLeiItensCartDest.Value, nil, False, False);
    UFinanceiro.RecalcSaldoCarteira(QrLeiItensCarteira.Value, nil, False, False);
    Prox := UMyMod.ProximoRegistro(QrLei, 'Codigo', QrLeiCodigo.Value);
    ReopenCNAB0Lei(Prox);
    InfoTempo(Now, 'Rec�lculos de saldos', False);
  end;
end;

procedure TFmCNAB_Ret1.HabilitaBotoes;
begin
  QrTem.Close;
  QrTem.Open;
  BtCarrega.Enabled := FLin1 > 0;
  BtAbertos.Enabled := QrTemItens.Value > 0;
end;

procedure TFmCNAB_Ret1.BitBtn1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  Timer1.Enabled := True;
  //
  PnCarrega.Visible := True;
  PnMovimento.Visible := False;
  Application.ProcessMessages;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmCNAB_Ret1.BtBufferClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMBuffer, BtBuffer);
end;

function TFmCNAB_Ret1.CarregaItensRetorno(LinA, QuemChamou: Integer): Boolean;
var
  Extensao, Arquivo, FileName: String;
  BcoCod, Posicoes: Integer;
begin
  Result := True;
  MyObjects.LimpaGrade(Grade1, 1, 1, True);
  if GradeA.Cells[1,1] = '' then
  begin
    Result := False;
    Exit;
  end;
  BcoCod   := Geral.IMV(GradeA.Cells[10, LinA]);
  Posicoes := Geral.IMV(GradeA.Cells[11, LinA]);
  //
  if BcoCod <> QrCNAB_DirBanco1.Value then
  begin
    Geral.MB_Aviso('Banco informado no cadastro da carteira n�o ' +
      'confere com o banco informado no arquivo CNAB!' + sLineBreak +
      'Banco da carteira: ' + FormatFloat('000', QrCNAB_DirBanco1.Value) +
      sLineBreak + 'Banco do arquivo: ' + FormatFloat('000', BcoCod) +
      '     (linha '+ Geral.FF0(LinA) + ' - Proced�ncia ' + Geral.FF0(QuemChamou) + ')');
    //Exit;
  end;
  Arquivo   := Trim(ExtractFileName(GradeA.Cells[1, LinA]));
  if Arquivo <> '' then
  begin
    Extensao  := '';
    FileName := dmkPF.CaminhoArquivo(QrCNAB_DirNome.Value, Arquivo, Extensao);
    CarregaItensRetornoA(FileName, QrCNAB_DirCodigo.Value,
      Geral.IMV(GradeA.Cells[00, LinA]),
      Geral.IMV(GradeA.Cells[09, LinA]), Posicoes, BcoCod);
    {case BcoCod of
      748: Result := CarregaItensRetornoSicredi(FileName,
                     QrCNAB_DirCodigo.Value,
                     Geral.IMV(GradeA.Cells[00, LinA]),
                     BcoCod, Geral.IMV(GradeA.Cells[09, LinA]));
      else
      begin
        Result := False;
      end;
    end;}
    VerificaImplementacoes(LinA);
  end else Result := False;
end;

procedure TFmCNAB_Ret1.QrCNAB_DirAfterScroll(DataSet: TDataSet);
begin
  Screen.Cursor := crHourGlass;
  MyObjects.LimpaGrade(GradeA, 1, 1, True);
  MyObjects.LimpaGrade(Grade1, 1, 1, True);
  //MyObjects.LimpaGrade(GradeB, 1, 1);
  //MyObjects.LimpaGrade(GradeC, 1, 1);
  FLinA := 0;
  FLinB := 1;
  FLin1 := 0;
  {while not QrCNAB_Dir.Eof do
  begin}
    LeArquivos(QrCNAB_DirNome.Value, QrCNAB_DirCodigo.Value, FLinA);
    FLinA := GradeA.RowCount - 1;
    {QrCNAB_Dir.Next;
  end;}
  HabilitaBotoes();
  Screen.Cursor := crDefault;
end;

procedure TFmCNAB_Ret1.QrLeiItensCalcFields(DataSet: TDataSet);
begin
  QrLeiItensMez_TXT.Value := dmkPF.MezToFDT(QrLeiItensMez.Value, 0, 104);
end;

procedure TFmCNAB_Ret1.QueryCalcFields(DataSet: TDataSet);
begin
  QuerySEQ.Value := Query.RecNo;
end;

procedure TFmCNAB_Ret1.QrLeiCalcFields(DataSet: TDataSet);
var
  OcorCodi: Integer;
begin
  OcorCodi := Geral.IMV(QrLeiOcorrCodi.Value);
  if UBancos.EhCodigoLiquidacao(OcorCodi, QrLeiBanco.Value, QrLeiTamReg.Value, '') then
    QrLeiDJM.Value := -((QrLeiDevJuros.Value + QrLeiDevMulta.Value) -
    (QrLeiValJuros.Value + QrLeiValMulta.Value + QrLeiValJuMul.Value ))
  else QrLeiDJM.Value := 0;

  QrLeiDTA_TARIF_TXT.Value := Geral.FDT(QrLeiDtaTarif.Value, 3);

  {
  if Trunc(QrLeiTIPO_BOL.Value) = 610 then
    QrLeiNOME_TIPO_BOL.Value := 'Reparcelamento'
  else if QrLeiTIPO_BOL.Value = 610 then
  }

end;

{
function TFmCNAB_Ret2.LocDadoAll(const Campo, Linha: Integer; const Mensagem: String;
var Resultado: String): Boolean;
begin
  Result := True;
  if QrCampos.Locate('Campo', Campo, []) then
    Resultado :=
    Trim(Copy(FLista[Linha], QrCamposPadrIni.Value, QrCamposPadrTam.Value))
  else begin
    Result := False;
    Resultado := '';
    if Mensagem <> '' then
      Application.MessageBox(PChar(Mensagem), 'Erro', MB_OK+MB_ICONERROR);
  end;
  Memo2.Lines.Add(FormatFloat('00000', Campo) + ' ' +
    FormatFloat('00000', Linha) + ' ' +
    FormatFloat('000', QrCamposPadrIni.Value) + ' ' +
    FormatFloat('000', QrCamposPadrTam.Value) + ' ' + Resultado);
end;
}

{
function TFmCNAB_Ret2.LocDado240(const Campo, Linha: Integer; const Mensagem: String;
var Resultado: String): Boolean;
begin
  Result := True;
  if QrCampos.Locate('Campo', Campo, []) then
    Resultado :=
    Trim(Copy(FLista[Linha], QrCamposPadrIni.Value, QrCamposPadrTam.Value))
  else begin
    Result := False;
    Resultado := '';
    if Mensagem <> '' then
      Application.MessageBox(PChar(Mensagem), 'Erro', MB_OK+MB_ICONERROR);
  end;
  Memo2.Lines.Add(FormatFloat('00000', Campo) + ' ' +
    FormatFloat('00000', Linha) + ' ' +
    FormatFloat('000', QrCamposPadrIni.Value) + ' ' +
    FormatFloat('000', QrCamposPadrTam.Value) + ' ' + Resultado);
end;
}

function TFmCNAB_Ret1.CarregaItensRetornoA(Arquivo: String;
  SeqDir, SeqArq, Entidade, TamReg, Banco: Integer): Boolean;
begin
  Screen.Cursor := crHourGlass;
  Result := False;
  //
  FLista.Clear;
  FLista.LoadFromFile(Arquivo);
  BtCarrega.Enabled := FLista.Count > 0;
  MyObjects.LimpaGrade(Grade1, 1, 1, True);
  //
  FLin1 := 0;
  //
  Memo2.Lines.Clear;
  Memo2.Lines.Add('Campo Linha Pos Tam Valor');
  //
  if not UBancos.BancoImplementado(Banco, TamReg, ecnabRetorno) then
  begin
    Screen.Cursor := crDefault;
    Exit;
  end;
  {
  QrBanco.Close;
  QrBanco.Params[0].AsInteger := BcoUse;
  QrBanco.Open;
  if TamReg = 400 then
  begin
    if (QrBancoID_400i.Value = 0) or (QrBancoID_400t.Value = 0) then
    begin
      Application.MessageBox(PChar('N�o foi definido o identificador de ' +
      'cobran�a CNAB400 para o banco '+ FormatFloat('000', BcoUse) +
      ' em seu cadastro!'), 'Aviso', MB_OK+MB_ICONWARNING);
      Screen.Cursor := crDefault;
      Exit;
    end;
  end else begin
    if (QrBancoID_240i.Value = 0) or (QrBancoID_240t.Value = 0) then
    begin
      Application.MessageBox(PChar('N�o foi definido o identificador de ' +
      'cobran�a CNAB240 para o banco '+ FormatFloat('000', BcoUse) +
      ' em seu cadastro!'), 'Aviso', MB_OK+MB_ICONWARNING);
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
  //
  }
  {
  QrCampos.Close;
  QrCampos.Params[0].AsInteger := BcoUse;
  QrCampos.SQL[10] := 'AND T'+FormatFloat('0', TamReg)+'=1';
  QrCampos.Open;
  if QrCampos.RecordCount = 0 then
  begin
    Application.MessageBox(PChar('N�o h� nenhum tipo de campo de registro '+
    'detalhe definido para o arquivo "'+ Arquivo + '" do banco '+
    FormatFloat('000', BcoUse) + '!'), 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  }
  case TamReg of
    240: CarregaItensRetorno240(Banco, Entidade, SeqDir, SeqArq);
    //240: CarregaArquivo240('?', 0);
    400: CarregaItensRetorno400(Banco, Entidade, SeqDir, SeqArq);
    else Application.MessageBox(PChar('Carregamento de itens de retorno n�o ' +
    'implementado para arquivo contendo ' + IntToStr(TamReg) + ' posi��es!'),
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Screen.Cursor := crDefault;
  {
  if nc > 0 then Application.MessageBox(PChar('Existem '+IntToStr(nc)+
  ' registros detalhe que n�o s�o cobran�a sem registro e foram DESCONSIDERADOS '+
  'na leitura do arquivo "'+Arquivo+'"!'), 'Aviso', MB_OK+MB_ICONWARNING);
  }
end;

procedure TFmCNAB_Ret1.CarregaItensRetorno240(
  Banco, Entidade, SeqDir, SeqArq: Integer);
var
  i: Integer;
  Registro, Segmento,
  SequeReg, ID_Link,  NossoNum, OcorrCod, OcorrDta, SeuNumer, PagtoDta,
  TxtTitul, TxtAbati, TxtDesco, TxtJuros, TxtMulta,
  TxtOutrC, TxtEPago, TxtJuMul, TxtTarif, DtaTarif,
  OcorrTxt, NumDocum, TxtCBrut, TxtOutrD, VenctDta,
  Msg: String;
begin
  for i := 0 to FLista.Count - 1 do
  begin
    //ShowMessage(FLista[i]);
    // Tipo de registro
    //LocDadoAll(670, i, '', Registro);
    Registro := Copy(FLista[i], 008, 001);
    //LocDadoAll(671, i, '', Segmento);
    Segmento := Copy(FLista[i], 014, 001);
    if (Registro = '3') and (Segmento = 'T') then
    begin
      inc(FLin1, 1);
      Grade1.RowCount := FLin1 + 1;
      Grade1.Cells[00, FLin1] := IntToStr(FLin1);

      // ID Link
      Msg := 'N�o foi poss�vel obter o identificador do t�tulo na linha ' +
      IntToStr(i) + '!';
      if not UBancos.LocDado240(Banco, 1000, i, FLista, Msg, '', ID_Link) then Exit;
      Grade1.Cells[21, FLin1] := ID_Link;

      // Nosso n�mero -> ID do registro no Sicredi e no Syndic
      UBancos.LocDado240(Banco, 501, i, FLista, '', '', NossoNum);
      Grade1.Cells[01, FLin1] := NossoNum;

      // Ocorr�ncia
      UBancos.LocDado240(Banco, 504, i, FLista, '', '', OcorrCod);
      Grade1.Cells[02, FLin1] := OcorrCod;
      // Texto da ocorr�ncia
      OcorrTxt :=
        UBancos.CNABTipoDeMovimento(Banco, ecnabRetorno, Geral.IMV(OcorrCod), 240, False, '');
      Grade1.Cells[03, FLin1] := OcorrTxt;


      // Data da ocorr�ncia
      UBancos.LocDado240(Banco, 505, i+1, FLista, '', '', OcorrDta);
      try
        if (OcorrDta = '000000') or (OcorrDta = '00000000') then
        begin
          Application.MessageBox(PChar('A data da ocorr�ncia "' +
          OcorrTxt + '" n�o foi definida (' + OcorrDta +') para o bloqueto ' +
          ID_Link + ' !'), 'Aviso',
          MB_OK+MB_ICONWARNING);
        end else
        Grade1.Cells[04, FLin1] := OcorrDta;
      except
        Application.MessageBox(PChar('Houve um erro ao formatar a data da ' +
        'ocorr�ncia. Verifique se o FORMATO informado no cadastro do CAMPO � ' +
        'compat�vel com o que o banco solicita!'), 'Erro', MB_OK+MB_ICONERROR);
      end;

      // Seu Numero
      UBancos.LocDado240(Banco, 506, i, FLista, '', '', SeuNumer);
      Grade1.Cells[05, FLin1] := SeuNumer;

      // Documento (Texto do n�mero do documento no t�tulo)
      UBancos.LocDado240(Banco, 502, i, FLista, '', '', NumDocum);
      Grade1.Cells[05, FLin1] := NumDocum;

      // Valor do t�tulo
      UBancos.LocDado240(Banco, 550, i, FLista, '', '', TxtTitul);
      //TxtTitul := dmkPF.XFT(TxtTitul, QrCamposCasas.Value, siPositivo);
      //ValTitul := Geral.DMV(TxtTitul);
      Grade1.Cells[06, FLin1] := TxtTitul;

      // Abatimento concedido
      UBancos.LocDado240(Banco, 551, i+1, FLista, '', '', TxtAbati);
      //TxtAbati := dmkPF.XFT(TxtAbati, QrCamposCasas.Value, siPositivo);
      //ValAbati := Geral.DMV(TxtAbati);
      Grade1.Cells[07, FLin1] := TxtAbati;

      // Desconto concedido
      UBancos.LocDado240(Banco, 552, i+1, FLista, '', '', TxtDesco);
      //TxtDesco := dmkPF.XFT(TxtDesco, QrCamposCasas.Value, siPositivo);
      //ValDesco := Geral.DMV(TxtDesco);
      Grade1.Cells[08, FLin1] := TxtDesco;

      // Valor efetivamente pago
      //UBancos.LocDado240(Banco, 553, i, FLista, '', TxtEPago);
      // Valor Total pago
      UBancos.LocDado240(Banco, 578, i+1, FLista, '', '', TxtEPago);
      //TxtEPago := dmkPF.XFT(TxtEPago, QrCamposCasas.Value, siPositivo);
      //ValEPago := Geral.DMV(TxtEPago);
      Grade1.Cells[09, FLin1] := TxtEPago;

      // Valor de cr�dito bruto
      UBancos.LocDado240(Banco, 579, i+1, FLista, '', '', TxtCBrut);
      //TxtCBrut := dmkPF.XFT(TxtCBrut, QrCamposCasas.Value, siPositivo);
      //ValEPago := Geral.DMV(TxtCBrut);
      Grade1.Cells[27, FLin1] := TxtCBrut;

      // Valor de outros d�bitos
      UBancos.LocDado240(Banco, 585, i+1, FLista, '', '', TxtOutrD);
      //TxtOutrD := dmkPF.XFT(TxtOutrD, QrCamposCasas.Value, siPositivo);
      //ValEPago := Geral.DMV(TxtOutrD);
      Grade1.Cells[28, FLin1] := TxtOutrD;

      //  N�O TEM
      // Juros de mora
      UBancos.LocDado240(Banco, 555, i, FLista, '', '', TxtJuros);
      //TxtJuros := dmkPF.XFT(TxtJuros, QrCamposCasas.Value, siPositivo);
      //ValJuros := Geral.DMV(TxtJuros);
      Grade1.Cells[10, FLin1] := TxtJuros;

      //  N�O TEM
      // Multa
      UBancos.LocDado240(Banco, 556, i, FLista, '', '', TxtMulta);
      //TxtMulta := dmkPF.XFT(TxtMulta, QrCamposCasas.Value, siPositivo);
      //ValMulta := Geral.DMV(TxtMulta);
      Grade1.Cells[11, FLin1] := TxtMulta;

      //  N�O TEM
      // Outros cr�ditos
      UBancos.LocDado240(Banco, 554, i, FLista, '', '', TxtOutrC);
      //TxtOutrC := dmkPF.XFT(TxtOutrC, QrCamposCasas.Value, siPositivo);
      //ValMulta := Geral.DMV(TxtOutrC);
      Grade1.Cells[12, FLin1] := TxtOutrC;

      // Juros de Mora e Multa (Somados)
      UBancos.LocDado240(Banco, 557, i+1, FLista, '', '', TxtJuMul);
      //TxtJuMul := dmkPF.XFT(TxtJuMul, QrCamposCasas.Value, siPositivo);
      Grade1.Cells[13, FLin1] := TxtJuMul;

      // Tarifa (Despesa) de cobran�a
      UBancos.LocDado240(Banco, 570, i, FLista, '', '', TxtTarif);
      //TxtTarif := dmkPF.XFT(TxtTarif, QrCamposCasas.Value, siPositivo);
      Grade1.Cells[14, FLin1] := TxtTarif;

      // ERRO Calculado no final
      //Grade1.Cells[15, FLin1] := ERRO;

      //Parei Aqui
      // Falta fazer (sem pressa)
      {
      // Motivo da ocorrencia
      Grade1.Cells[16, FLin1] := Copy(FLista[i], 319, 010);
      // Texto do motivo da ocorrencia
      Grade1.Cells[17, FLin1] := UBancos.CNABMotivosDeTipoDeMovimento28(
        Banco, Copy(FLista[i], 319, 010));
      }

      // Data de lancamento na conta corrente
      Msg := 'Data de lan�amento na conta corrente n�o definida!' +
      sLineBreak + 'N�o � aconselh�vel conciliar o documento ' +
      ID_Link + ' (Nosso n�mero = ' + NossoNum + ')';
      UBancos.LocDado240(Banco, 581, i+1, FLista, Msg, '', PagtoDta);
      try
        // Por causa do banco 756 que diz que coloca no cabe�alho (col 380 a 385)
        // a data de cr�dito na c/c , mas esta data � colocada nos itens (col 296 a 301)
        if PagtoDta = '' then
          Application.MessageBox(
          PChar('Data de lan�amento na conta corrente n�o definida!' +
          sLineBreak + 'N�o � aconselh�vel conciliar o documento ' +
          ID_Link + ' (Nosso n�mero = ' + NossoNum + ').'), 'AVISO',
          MB_OK+MB_ICONWARNING);
        Grade1.Cells[18, FLin1] := PagtoDta;
      except
        Application.MessageBox(PChar('Houve um erro ao formatar a data do ' +
        'cr�dito em conta corrente. Verifique se o FORMATO informado no ' +
        'cadastro do CAMPO � compat�vel com o que o banco solicita!'), 'Erro',
        MB_OK+MB_ICONERROR);
      end;

      // Data de d�bito da tarifa de cobranca
      //UBancos.LocDado240(Banco, 582, i+1, FLista, '', DtaTarif);
      UBancos.LocDado240(Banco, 505, i+1, FLista, '', '', DtaTarif);
      try
        Grade1.Cells[19, FLin1] := DtaTarif;
      except
        Application.MessageBox(PChar('Houve um erro ao formatar a data do ' +
        'd�bito da tarifa em conta corrente. Verifique se o FORMATO informado no ' +
        'cadastro do CAMPO � compat�vel com o que o banco solicita!'), 'Erro',
        MB_OK+MB_ICONERROR);
      end;

      // C�digo Condom�nio
      Grade1.Cells[20, FLin1] := IntToStr(Entidade);

      // Sequencia do diretorio
      Grade1.Cells[22, FLin1] := FormatFloat('000', SeqDir);

      // Sequencia do arquivo
      Grade1.Cells[23, FLin1] := FormatFloat('000', SeqArq);

      // N�mero sequencial do registro
      UBancos.LocDado240(Banco, 999, i, FLista, '', '', SequeReg);
      Grade1.Cells[24, FLin1] := FormatFloat('000', Geral.IMV(SequeReg));

      // Banco
      Grade1.Cells[25, FLin1] := FormatFloat('000', Banco);

      // Vencimento
      UBancos.LocDado240(Banco, 580, i+1, FLista, '', '', VenctDta);
      try
        Grade1.Cells[29, FLin1] := VenctDta;
      except
        Application.MessageBox(PChar('Houve um erro ao formatar a data de ' +
        'vencimento. Verifique se o FORMATO informado no cadastro do CAMPO � ' +
        'compat�vel com o que o banco solicita!'), 'Erro', MB_OK+MB_ICONERROR);
      end;



      ErroLinha(Banco, FLin1, False);

      //Parei Aqui
      // Fazer rejei��o de t�tulos
    end;
  end;
end;

procedure TFmCNAB_Ret1.CarregaItensRetorno400(
  Banco, Entidade, SeqDir, SeqArq: Integer);
var
  i: Integer;
  SequeReg,
  ID_Link,  NossoNum, OcorrCod, {OcorrTxt,} OcorrDta, SeuNumer, {Mensagem,} PagtoDta,
  TxtTitul, TxtAbati, TxtDesco, TxtJuros, TxtMulta,
  TxtOutro, TxtEPago, TxtJuMul, TxtTarif, DtaTarif,
  VenctDta, Lin, Msg: String;
begin
  for i := 1 to FLista.Count -2 do
  begin
    inc(FLin1, 1);
    Grade1.RowCount := FLin1 + 1;
    Grade1.Cells[00, FLin1] := IntToStr(FLin1);
    Lin := FLista[i];

    // ID Link
    Msg := 'N�o foi poss�vel obter o identificador do t�tulo na linha ' +
      IntToStr(i) + '!';
    if not UBancos.LocDado400(Banco, 1000, i, FLista, Msg, '', ID_Link) then Exit;
    Grade1.Cells[21, FLin1] := ID_Link;

    // Nosso n�mero -> ID do registro no Sicredi e no Syndic
    UBancos.LocDado400(Banco, 501, i, FLista, '', '', NossoNum);
    Grade1.Cells[01, FLin1] := NossoNum;

    // Ocorr�ncia
    UBancos.LocDado400(Banco, 504, i, FLista, '', '', OcorrCod);
    Grade1.Cells[02, FLin1] := OcorrCod;

    // Texto da ocorr�ncia
    Grade1.Cells[03, FLin1] :=
      UBancos.CNABTipoDeMovimento(Banco, ecnabRetorno, Geral.IMV(OcorrCod), 400, False, '');

    // Data da ocorr�ncia
    UBancos.LocDado400(Banco, 505, i, FLista, '', '', OcorrDta);
    try
      Grade1.Cells[04, FLin1] := OcorrDta;
    except
      Application.MessageBox(PChar('Houve um erro ao formatar a data da ' +
      'ocorr�ncia. Verifique se o FORMATO informado no cadastro do CAMPO � ' +
      'compat�vel com o que o banco solicita!'), 'Erro', MB_OK+MB_ICONERROR);
    end;

    // Seu Numero
    UBancos.LocDado400(Banco, 506, i, FLista, '', '', SeuNumer);
    Grade1.Cells[05, FLin1] := SeuNumer;

    // Valor do t�tulo
    UBancos.LocDado400(Banco, 550, i, FLista, '', '', TxtTitul);
    //TxtTitul := dmkPF.XFT(TxtTitul, QrCamposCasas.Value, siPositivo);
    //ValTitul := Geral.DMV(TxtTitul);
    Grade1.Cells[06, FLin1] := TxtTitul;

    // Abatimento concedido
    UBancos.LocDado400(Banco, 551, i, FLista, '', '', TxtAbati);
    //TxtAbati := dmkPF.XFT(TxtAbati, QrCamposCasas.Value, siPositivo);
    //ValAbati := Geral.DMV(TxtAbati);
    Grade1.Cells[07, FLin1] := TxtAbati;

    // Desconto concedido
    UBancos.LocDado400(Banco, 552, i, FLista, '', '', TxtDesco);
    //TxtDesco := dmkPF.XFT(TxtDesco, QrCamposCasas.Value, siPositivo);
    //ValDesco := Geral.DMV(TxtDesco);
    Grade1.Cells[08, FLin1] := TxtDesco;

    // Valor efetivamente pago
    UBancos.LocDado400(Banco, 553, i, FLista, '', '', TxtEPago);
    //TxtEPago := dmkPF.XFT(TxtEPago, QrCamposCasas.Value, siPositivo);
    //ValEPago := Geral.DMV(TxtEPago);
    Grade1.Cells[09, FLin1] := TxtEPago;

    // Juros de mora
    UBancos.LocDado400(Banco, 555, i, FLista, '', '', TxtJuros);
    //TxtJuros := dmkPF.XFT(TxtJuros, QrCamposCasas.Value, siPositivo);
    //ValJuros := Geral.DMV(TxtJuros);
    Grade1.Cells[10, FLin1] := TxtJuros;

    // Multa
    UBancos.LocDado400(Banco, 556, i, FLista, '', '', TxtMulta);
    //TxtMulta := dmkPF.XFT(TxtMulta, QrCamposCasas.Value, siPositivo);
    //ValMulta := Geral.DMV(TxtMulta);
    Grade1.Cells[11, FLin1] := TxtMulta;

    // Outros cr�ditos
    UBancos.LocDado400(Banco, 554, i, FLista, '', '', TxtOutro);
    //TxtOutro := dmkPF.XFT(TxtOutro, QrCamposCasas.Value, siPositivo);
    //ValMulta := Geral.DMV(TxtOutro);
    Grade1.Cells[12, FLin1] := TxtOutro;

    // Juros de Mora e Multa (Somados)
    UBancos.LocDado400(Banco, 557, i, FLista, '', '', TxtJuMul);
    //TxtJuMul := dmkPF.XFT(TxtJuMul, QrCamposCasas.Value, siPositivo);
    Grade1.Cells[13, FLin1] := TxtJuMul;

    // Tarifa (Despesa) de cobran�a
    UBancos.LocDado400(Banco, 570, i, FLista, '', '', TxtTarif);
    //TxtTarif := dmkPF.XFT(TxtTarif, QrCamposCasas.Value, siPositivo);
    Grade1.Cells[14, FLin1] := TxtTarif;

    // ERRO Calculado no final
    //Grade1.Cells[15, FLin1] := ERRO;

    //Parei Aqui
    // Falta fazer (sem pressa)
    {
    // Motivo da ocorrencia
    Grade1.Cells[16, FLin1] := Copy(i, FLista, 319, 010);
    // Texto do motivo da ocorrencia
    Grade1.Cells[17, FLin1] := UBancos.CNABMotivosDeTipoDeMovimento28(
      Banco, Copy(i, FLista, 319, 010));
    }

    // Data de lancamento na conta corrente
    UBancos.LocDado400(Banco, 581, i, FLista, 'Data de lan�amento na conta corrente n�o definida!' +
    sLineBreak + 'N�o � aconselh�vel conciliar o documento ' +
    ID_Link + ' (Nosso n�mero = ' + NossoNum + ')', '', PagtoDta);
    try
      // Por causa do banco 756 que diz que coloca no cabe�alho (col 380 a 385)
      // a data de cr�dito na c/c , mas esta data � colocada nos itens (col 296 a 301)
      if PagtoDta = '' then
        Application.MessageBox(
        PChar('Data de lan�amento na conta corrente n�o definida!' +
        sLineBreak + 'N�o � aconselh�vel conciliar o documento ' +
        ID_Link + ' (Nosso n�mero = ' + NossoNum + ').'), 'AVISO',
        MB_OK+MB_ICONWARNING);
      Grade1.Cells[18, FLin1] := PagtoDta;
    except
      Application.MessageBox(PChar('Houve um erro ao formatar a data do ' +
      'cr�dito em conta corrente. Verifique se o FORMATO informado no ' +
      'cadastro do CAMPO � compat�vel com o que o banco solicita!'), 'Erro',
      MB_OK+MB_ICONERROR);
    end;

    // Data de d�bito da tarifa de cobranca
    UBancos.LocDado400(Banco, 582, i, FLista, '', '', DtaTarif);
    try
      Grade1.Cells[19, FLin1] := DtaTarif;
    except
      Application.MessageBox(PChar('Houve um erro ao formatar a data do ' +
      'd�bito da tarifa em conta corrente. Verifique se o FORMATO informado no ' +
      'cadastro do CAMPO � compat�vel com o que o banco solicita!'), 'Erro',
      MB_OK+MB_ICONERROR);
    end;

    // C�digo Condom�nio
    Grade1.Cells[20, FLin1] := IntToStr(Entidade);

    // Sequencia do diretorio
    Grade1.Cells[22, FLin1] := FormatFloat('000', SeqDir);

    // Sequencia do arquivo
    Grade1.Cells[23, FLin1] := FormatFloat('000', SeqArq);

    // N�mero sequencial do registro
    UBancos.LocDado400(Banco, 999, i, FLista, '', '', SequeReg);
    Grade1.Cells[24, FLin1] := FormatFloat('000', Geral.IMV(SequeReg));

    // Banco
    Grade1.Cells[25, FLin1] := FormatFloat('000', Banco);


    // Data de vencimento
    UBancos.LocDado400(Banco, 580, i, FLista, '', '', VenctDta);
    try
      Grade1.Cells[29, FLin1] := VenctDta;
    except
      Application.MessageBox(PChar('Houve um erro ao formatar a data de ' +
      'vencimento. Verifique se o FORMATO informado no ' +
      'cadastro do CAMPO � compat�vel com o que o banco solicita!'), 'Erro',
      MB_OK+MB_ICONERROR);
    end;

    ErroLinha(Banco, FLin1, False);

    //Parei Aqui
    // Fazer rejei��o de t�tulos
  end;
end;

procedure TFmCNAB_Ret1.DBGLeiDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  Cor: TColor;
begin
  if (Column.FieldName = 'DJM') then
  begin
    if      QrLeiDJM.Value > 0 then Cor := clBlue
    else if QrLeiDJM.Value < 0 then Cor := clRed
    else Cor := clBlack;
    MyObjects.DesenhaTextoEmDBGrid(TDbGrid(DBGLei), Rect, Cor, clWhite,
      Column.Alignment, Column.Field.DisplayText);
  end;
end;

procedure TFmCNAB_Ret1.QrLeiAfterClose(DataSet: TDataSet);
begin
  BtAgenda.Enabled := False;
end;

procedure TFmCNAB_Ret1.BtAgendaClick(Sender: TObject);
{var
  Ano, Mes, Dia: Word;
  i, Cond: Integer;}
begin
  {
  if not CondDeEntidade(QrLeiEntidade.Value, Cond) then Exit;
  Application.CreateForm(TFmCondGerArreFut, FmCondGerArreFut);
  FmCondGerArreFut.FCond := Cond;
  FmCondGerArreFut.LaTipo.Caption := CO_INCLUSAO;
  FmCondGerArreFut.QrAptos.Close;
  FmCondGerArreFut.QrAptos.Params[0].AsInteger := Cond;
  FmCondGerArreFut.QrAptos.Open;
  DecodeDate(Date, Ano, Mes, Dia);
  if QrLeiItens.RecordCount > 0 then
  begin
    with FmCondGerArreFut do
    begin
      EdApto.Text := IntToStr(QrLeiItensApto.Value);
      CBApto.KeyValue := QrLeiItensApto.Value;
      if Dmod.QrControleCNABCtaJur.Value > 0 then
      begin
        EdConta.Text := IntToStr(Dmod.QrControleCNABCtaJur.Value);
        CBConta.KeyValue := Dmod.QrControleCNABCtaJur.Value;
      end else ;
      EdValor.Text := Geral.FFT(QrLeiDJM.Value, 2, siPositivo);
      EdDescricao.Text := 'Diferen�a de juros ' + QrLeiItensMez_TXT.Value +
        ' (boleto ' + FormatFloat('00000', QrLeiIDNum.Value)+')';
      FmCondGerArreFut.CkContinuar.Visible := False;
      FmCondGerArreFut.CkContinuar.Checked := False;
      for i := 0 to FmCondGerArreFut.CBAno.Items.Count - 1 do
      begin
        if Geral.IMV(FmCondGerArreFut.CBAno.Items[i]) = Ano then
          FmCondGerArreFut.CBAno.ItemIndex := i;
      end;
      FmCondGerArreFut.CBMes.ItemIndex := Mes-1;
    end;
  end;
  FmCondGerArreFut.CkContinuar.Visible := True;
  FmCondGerArreFut.CkContinuar.Checked := True;
  //
  FmCondGerArreFut.ShowModal;
  FmCondGerArreFut.Destroy;
  }
end;

function TFmCNAB_Ret1.CondDeEntidade(const Entidade: Integer;
  var Cond: Integer): Boolean;
begin
  QrLocCond.Close;
  QrLocCond.Params[0].AsInteger := Entidade;
  QrLocCond.Open;
  if QrLocCond.RecordCount = 1 then
  begin
    Cond := QrLocCondCodigo.Value;
    Result := True;
  end else begin
    Result := False;
    Cond := 0;
    Application.MessageBox(PChar('ERRO! Foram encontrados ' + IntToStr(
    QrLocCond.RecordCount) + ' condom�nios para a entidade n� ' + IntToStr(
    Entidade) + '!'), 'Aviso de ERRO', MB_OK+MB_ICONERROR);
  end;
  // N�o Fechar por cousa de juros e multa
end;

procedure TFmCNAB_Ret1.CorrigeValTitulopoisobancoenviouerrado1Click(
  Sender: TObject);
var
  ValTxt, AntTxt: String;
  ValNum: Double;
  Continua: Integer;
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  ValTxt := DBEdit1.Text;
  if InputQuery('Valor do T�tulo Tnformado pelo Banco',
  'Informe o novo valor informado pelo banco:', ValTxt) then
  begin
    AntTxt := Geral.FFT(QrLeiValTitul.Value, 2, siPositivo);
    ValNum := Geral.DMV(ValTxt);
    ValTxt := Geral.FFT(ValNum, 2, siPositivo);
    //
    Continua := Application.MessageBox(PChar('Confirma a altera��o do ' +
      'Val.Titulo de "' + AntTxt + '" para "' + ValTxt + '"?'), 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION);
    if Continua = ID_YES then Continua := Application.MessageBox(PChar(
    'Tem certeza que deseja alterar o Val.Titulo de "'+ AntTxt + '" para "' +
    ValTxt + '"?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION);
    if Continua = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE cnab_lei SET ValTitul=:P0 WHERE Codigo=:Pa');
      Dmod.QrUpd.Params[00].AsFloat   := ValNum;
      //
      Dmod.QrUpd.Params[01].AsInteger := QrLeiCodigo.Value;
      Dmod.QrUpd.ExecSQL;
      //
      ReopenCNAB0Lei(QrLeiCodigo.Value);
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmCNAB_Ret1.EdNomeBancoChange(Sender: TObject);
begin
  ReopenCNAB_Dir();
end;

function TFmCNAB_Ret1.ErroLinha(Banco, Linha: Integer; Avisa: Boolean): Boolean;
var
  ValTitul, ValAbati, ValDesco, ValPago, ValJuros, ValMulta,
  ValJuMul, ValTarif, ValErro, ValOutro, ValReceb: Double;
  IDLink, Cliente: Integer;
  NossoNum: String;
  //
  DefErro: Boolean;
begin
  if Geral.IMV(Grade1.Cells[02, Linha]) = 6 then // Baixa simples
  begin
    Cliente   := Geral.IMV(GradeA.Cells[09, GradeA.Row]);
    NossoNum  := Geral.SoNumero_TT(Grade1.Cells[01, Linha]);
    IDLink    := Geral.IMV(Grade1.Cells[21, Linha]);
    ValTitul  := Geral.DMV(Grade1.Cells[06, Linha]);
    ValAbati  := Geral.DMV(Grade1.Cells[07, Linha]);
    ValDesco  := Geral.DMV(Grade1.Cells[08, Linha]);
    ValPago   := Geral.DMV(Grade1.Cells[09, Linha]);
    ValJuros  := Geral.DMV(Grade1.Cells[10, Linha]);
    ValMulta  := Geral.DMV(Grade1.Cells[11, Linha]);
    ValOutro  := Geral.DMV(Grade1.Cells[12, Linha]);
    ValJuMul  := Geral.DMV(Grade1.Cells[13, Linha]);
    ValTarif  := Geral.DMV(Grade1.Cells[14, Linha]);
    ValReceb  := Geral.DMV(Grade1.Cells[27, Linha]);
    //
    if ValReceb > 0 then
      ValErro := ValPago - ValTarif - ValReceb
    else
      ValErro := ValTitul - ValAbati - ValDesco + ValJuros + ValMulta
               + ValOutro + ValJuMul - ValTarif - ValPago;
    if Banco = 409 then ValErro := ValErro + ValTarif;
    //
    Grade1.Cells[15, Linha] := Geral.FFT(ValErro, 2, siNegativo);
    DefErro := (ValErro >= 0.01) or (ValErro <= -0.01);
    //
    if Avisa then
    begin
      if DefErro then
      begin
        QrPesq2.Close;
        QrPesq2.Params[00].AsInteger := IDLink;
        QrPesq2.Params[01].AsInteger := Cliente;
        QrPesq2.Open;
        //
        if QrPesq2.RecordCount > 0 then
        begin
          Application.MessageBox(PChar('O documento com "Nosso n�m." = ' +
          NossoNum + ' foi localizado, mas tem diverg�ncias nos ' +
          'dados informados!'), 'Aviso', MB_OK+MB_ICONWARNING);
        end else begin
          Application.MessageBox(PChar('O documento com "Nosso n�m." = ' +
          NossoNum + ' tem diverg�ncias nos dados informados, e n�o ' +
          'foi localizado nos bloquetos emitidos!'), 'Aviso', MB_OK+MB_ICONWARNING);
        end;
      end;
    end;
    Result := DefErro;
  end else Result := False;
end;

procedure TFmCNAB_Ret1.Alteravalordoitemdearrecadaoselecionado1Click(
  Sender: TObject);
var
  Valor: String;
  Novo: Double;
begin
  //
  Valor := FormatFloat('#,###,##0.00', QrLeiItensCredito.Value);
  if InputQuery('Altera��o de valor', 'Informe o novo valor:', Valor) then
  begin
    Novo := Geral.DMV(Valor);
    if Novo >= 0.01 then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE ' + VAR_LCT  + ' SET AlterWeb=1, Credito=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:P1 AND Sub=:P2');
      //
      Dmod.QrUpd.Params[00].AsFloat   := Novo;
      Dmod.QrUpd.Params[01].AsInteger := QrLeiItensControle.Value;
      Dmod.QrUpd.Params[02].AsInteger := QrLeiItensSub.Value;
      Dmod.QrUpd.ExecSQL;
      //
      ReopenCNAB0Lei(QrLeiCodigo.Value);
      ReopenLeiItens(QrLeiItensControle.Value);
    end else Application.MessageBox(PChar('O valor informado "' + Valor +
    '" � inv�lido!'), 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmCNAB_Ret1.BtItensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmCNAB_Ret1.Excluioitemdearrecadaoselecionado1Click(
  Sender: TObject);
begin
  if Application.MessageBox(PChar('Tem certeza que deseja excluir o item "' +
  QrLeiItensDescricao.Value + '"?'),
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM ' + VAR_LCT + ' WHERE Controle=:P0 AND Sub=:P1');
    Dmod.QrUpd.Params[00].AsInteger := QrLeiItensControle.Value;
    Dmod.QrUpd.Params[01].AsInteger := QrLeiItensSub.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenCNAB0Lei(QrLeiCodigo.Value);
  end;
end;

procedure TFmCNAB_Ret1.Adicionadados1Click(Sender: TObject);
var
  OcorrDataD, QuitaDataD, TarifDataD: TDateTime;
  OcorrDataT, QuitaDataT, TarifDataT: String;
  SQL, OcorrCod, OcorrTxt, Txt: String;
  i, j, Ini, Fim: Integer;
  Bloqueto, ValTitul, ValPago, ValJuros, ValMulta, ValOutros, ValJurMul,
  ValTarifa, ValErro: Double;
begin
  Query.Close;
  Query.SQL.Clear;
  SQL := 'INSERT INTO bafer (Bloqueto,ValTitul,ValPago,ValMulta,ValJuros,' +
  'ValOutros,ValJurMul,ValERRO,ValTarifa,OcorrCod,OcorrTxt,' +
  'DataOcorr,DataQuita,DataTarif,Ativo) VALUES (';

    {
    SelectedRows.Count > 1 then
    begin
      with GradeA.DataSource.DataSet do
      for i := 0 to GradeA.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(GradeA.SelectedRows.Items[i]));
        GeraAtual(se, sq);
      end;
    end else GeraAtual(se, sq);
  end else if FQuais = istAtual then GeraAtual(se, sq);
    }
  //
  Ini := GradeA.Selection.Top;
  Fim := GradeA.Selection.Bottom;
  for j := Ini to Fim do
  begin
    GradeA.Row := j;
    for i := 1 to Grade1.RowCount - 1 do
    begin
      Txt := '';
      Bloqueto := Geral.DMV(Grade1.Cells[21,i]);
      Txt := Txt + dmkPF.FFP(Bloqueto, 0) + ',';
      //
      ValTitul := Geral.DMV(Grade1.Cells[06,i]);
      Txt := Txt + dmkPF.FFP(ValTitul, 2) + ',';
      //
      ValPago := Geral.DMV(Grade1.Cells[09,i]);
      Txt := Txt + dmkPF.FFP(ValPago, 2) + ',';
      //
      ValMulta := Geral.DMV(Grade1.Cells[11,i]);
      Txt := Txt + dmkPF.FFP(ValMulta, 2) + ',';
      //
      ValJuros := Geral.DMV(Grade1.Cells[10,i]);
      Txt := Txt + dmkPF.FFP(ValJuros, 2) + ',';
      //
      ValOutros := Geral.DMV(Grade1.Cells[12,i]);
      Txt := Txt + dmkPF.FFP(ValOutros, 2) + ',';
      //
      ValJurMul := Geral.DMV(Grade1.Cells[13,i]);
      Txt := Txt + dmkPF.FFP(ValJurMul, 2) + ',';
      //
      ValERRO := Geral.DMV(Grade1.Cells[15,i]);
      Txt := Txt + dmkPF.FFP(ValERRO, 2) + ',';
      //
      ValTarifa := Geral.DMV(Grade1.Cells[14,i]);
      Txt := Txt + dmkPF.FFP(ValTarifa, 2) + ',';
      //
      OcorrCod := Grade1.Cells[02,i];
      if Trim(OcorrCod) = '' then
        OcorrCod := '0';
      //
      OcorrTxt := Grade1.Cells[03,i];
      if Trim(OcorrTxt) = '' then
        OcorrTxt := ' ';
      Txt := Txt + '"' + OcorrCod + '",';
      Txt := Txt + '"' + OcorrTxt + '",';
      //
      OcorrDataD := Geral.ValidaDataSimples(Grade1.Cells[04,i], True);
      OcorrDataT := Geral.FDT(OcorrDataD, 1);
      Txt := Txt + '"' + OcorrDataT + '",';
      //
      QuitaDataD := Geral.ValidaDataSimples(Grade1.Cells[04,i], True);
      QuitaDataT := Geral.FDT(QuitaDataD, 1);
      Txt := Txt + '"' + QuitaDataT + '",';
      //
      TarifDataD := Geral.ValidaDataSimples(Grade1.Cells[04,i], True);
      TarifDataT := Geral.FDT(TarifDataD, 1);
      Txt := Txt + '"' + TarifDataT + '",';
      //
      Query.SQL.Add(SQL + Txt + '0);');
      //
    end;
  end;
  Query.SQL.Add('SELECT * FROM bafer;');
  DmkABS_PF.AbreQuery(Query);
end;

procedure TFmCNAB_Ret1.Ajustavaloresdobloquetoatual1Click(Sender: TObject);
var
  Juros, Multa, Difer, Desco: Double;
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  Screen.Cursor := crHourGlass;
  Desco := 0;
  Multa := 0;
  Juros := 0;
  Difer := QrLeiValTitul.Value - QrLeiSumCredito.Value;
  if Difer > 0 then
  begin
    if QrLeiDevMulta.Value > Difer then Multa := Difer
    else begin
      Multa := QrLeiDevMulta.Value;
      Juros := Difer - QrLeiDevMulta.Value
    end;
  end else Desco := - Difer;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE cnab_lei SET ValJuros=:P0, ValMulta=:P1, ');
  Dmod.QrUpd.SQL.Add('ValDesco=:P2, ValTitul=:P3 WHERE Codigo=:Pa');
  Dmod.QrUpd.Params[00].AsFloat   := Juros;
  Dmod.QrUpd.Params[01].AsFloat   := Multa;
  Dmod.QrUpd.Params[02].AsFloat   := Desco;
  Dmod.QrUpd.Params[03].AsFloat   := QrLeiSumCredito.Value;
  //
  Dmod.QrUpd.Params[04].AsInteger := QrLeiCodigo.Value;
  Dmod.QrUpd.ExecSQL;
  //
  ReopenCNAB0Lei(QrLeiCodigo.Value);
  Screen.Cursor := crDefault;
end;

procedure TFmCNAB_Ret1.QrLeiAgrCalcFields(DataSet: TDataSet);
begin
  QrLeiAgrMez_TXT.Value := dmkPF.MezToFDT(QrLeiAgrMez.Value, 0, 104);
  case QrLeiAgrTIPO_BLOQ.Value of
    1: QrLeiAgrNOME_TIPO_BLOQ.Value := 'Taxa condominial';
    2: QrLeiAgrNOME_TIPO_BLOQ.Value := 'Reparcelamento';
    else QrLeiAgrNOME_TIPO_BLOQ.Value := 'Desconhecido';
  end;
end;

procedure TFmCNAB_Ret1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if QrCNAB_Dir.State = dsInactive then Timer1.Enabled := True;
end;

procedure TFmCNAB_Ret1.Timer1Timer(Sender: TObject);
var
  Codigo: Integer;
begin
  Timer1.Enabled := False;
  if QrCNAB_Dir.State <> dsInactive then
    Codigo := QrCNAB_DirCodigo.Value
  else Codigo := 0;
  ReopenCNAB_Dir();
  if QrCNAB_Dir.RecordCount = 0 then Application.MessageBox(PChar(
    'Nenhum diret�rio foi definido em Cadastros -> CNAB -> Diret�rios '+
    'CNAB. Defina ao menos um diret�rio!'), 'Aviso', MB_OK+MB_ICONWARNING)
  else QrCNAB_Dir.Locate('Codigo', Codigo, []);
  HabilitaBotoes;
end;

procedure TFmCNAB_Ret1.InfoTempo(Tempo: TDateTime; Texto: String; Inicial: Boolean);
begin
  if Inicial then
  begin
    FTempo := Tempo;
    FUltim := Tempo;
    Memo3.Lines.Add('');
    Memo3.Lines.Add('==============================================================================');
    Memo3.Lines.Add('');
    Memo3.Lines.Add(FormatDateTime('dd/mm/yyyy  hh:nn:ss ', Tempo) +
      '- [ Total ] [Unit�ri] '+ Texto);
  end else begin
    Memo3.Lines.Add(FormatDateTime('dd/mm/yyyy  hh:nn:ss - ', Tempo)+
    FormatDateTime('nn:ss:zzz ', Tempo-FTempo) +
    FormatDateTime('nn:ss:zzz ', Tempo-FUltim) + Texto);
    FUltim := Tempo;
  end;
end;

function TFmCNAB_Ret1.ObtemActiveRowA: Integer;
begin
  // caso n�o haja linha selecionada
  if FActiveRowA = 0 then FActiveRowA := 1;
  Result := FActiveRowA;
end;

procedure TFmCNAB_Ret1.Ordenarpeladatadecrdito1Click(Sender: TObject);
begin
  ReabreQueryEImprime(2);
end;

procedure TFmCNAB_Ret1.Ordenarpeladatadepagamento1Click(Sender: TObject);
begin
  ReabreQueryEImprime(3);
end;

procedure TFmCNAB_Ret1.Ordenarpelonmerodobloqueto1Click(Sender: TObject);
begin
  ReabreQueryEImprime(1);
end;

procedure TFmCNAB_Ret1.PMBufferPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := Query.State <> dsInactive;
  Adicionadados1.Enabled := Habilita;
  Imprimelista1.Enabled := Habilita;
end;

procedure TFmCNAB_Ret1.PnMovimentoClick(Sender: TObject);
begin

end;

{
Parei aqui
tentar eliminar BancosLei:
dfm: 1908 e 1964
QrBcocor e QrLocOcor

}

procedure TFmCNAB_Ret1.VerificaImplementacoes(Row: Integer);
{
var
  CodLiq, Banco, TamReg, Genero: Integer;
  VarBool: Boolean;
  Fator, Diferenca: Double;
}
begin
  {
  Banco  := Geral.IMV(GradeA.Cells[10, Row]);
  TamReg := Geral.IMV(GradeA.Cells[11, Row]);
  CodLiq := Geral.IMV(Grade1.Cells[02, 1]);
  //
  UBancos.SeparaJurosEMultaImplementado(Banco, VarBool);
  UBancos.EhCodigoLiquidacao(CodLiq, Banco, TamReg);
  UBancos.InformaTarifaDeCobrancaImplementado(Banco, VarBool);
  UBancos.ContaDaOcorrencia(Banco, TamReg, FormatFloat('00', CodLiq), Genero);
  UBancos.FatorDeRecebimento(Banco, 1, 0.1, 1, Fator);
  UBancos.DiferencaDeRecebimento(Banco, 1, 0.1, 1, Diferenca);
  UBancos.FatorMultaDeRecebimento(Banco, 1, 0.1, 1, Fator);
  }
end;

end.


